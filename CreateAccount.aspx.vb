﻿Imports System.IO
Imports System.Data '
Imports System.Data.SqlClient
Imports App_Code
Imports AjaxControlToolkit
Imports C1.Web.UI.Controls.C1Window
Imports System.Net
Imports System.Net.Mail
Imports System.Net.Mail.SmtpClient


Partial Class CreateAccount
    Inherits MyBasePage '   Inherits System.Web.UI.Page
    Dim FormViewRequest As New FormSQL()
    Dim FormSignOn As New FormSQL()

    Dim TokenForm As New FormSQL()

    Dim sActivitytype As String = ""
    Dim cblBinder As New CheckBoxListBinder
    'Dim FormViewRequest As New FormSQL()
    Dim AccountRequestNum As Integer
    Dim AccountSeqNum As Integer
    Dim AccountRequestType As String

    Dim tokenRequestnum As String


    Dim ItemValidCd As String
    Dim AppNum As Integer
    Dim iClientnum As Integer
    Dim iRolenum As Integer

    Private dtSubApplications As New DataTable
    Private dtApplications As New DataTable

    Dim RequestItems As RequestItems
    Dim thisRequest As New RequestItem
    Dim AccRequest As AccountRequest

    Dim TokenRequest As TokenRequest

    Dim UserInfo As UserSecurity
    Dim ActionControl As New ActionController()
    Dim ActionList As New List(Of Actions)

    Dim Mailobj As System.Net.Mail.SmtpClient = New System.Net.Mail.SmtpClient
    Dim Mailmsg As New System.Net.Mail.MailMessage
    Dim MailAddress As System.Net.Mail.MailAddress



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        FormViewRequest = New FormSQL(Session("EmployeeConn"), "SelAccountRequestV20", "InsAccountRequestV20", "", Page)


        FormSignOn = New FormSQL(Session("EmployeeConn"), "SelAccountRequestV20", "UpdFullAccountRequestV20", "", Page)

        AccountRequestNum = Request.QueryString("RequestNum")
        AccountSeqNum = Request.QueryString("account_seq_num")

        AccRequest = New AccountRequest(AccountRequestNum, Session("EmployeeConn"))
        iRolenum = AccRequest.RoleNum

        TokenRequest = New TokenRequest(AccountRequestNum, Session("EmployeeConn"))


        UserInfo = New UserSecurity(Session("LoginID"), Session("EmployeeConn"))

        FormSignOn.AddSelectParm("@account_request_num", AccountRequestNum, SqlDbType.Int, 6)

        RequestItems = New RequestItems(AccountRequestNum)
        thisRequest = RequestItems.GetRequestItemBySeqNum(AccountSeqNum)

        If thisRequest.ApplicationNum = "156" Then
            tokenRequestnum = TokenRequest.AccountRequestNum

        End If




        Dim rblAffiliateBinder As New RadioButtonListBinder(emp_type_cdrbl, "SelAffiliationsV5", Session("EmployeeConn"))
        rblAffiliateBinder.BindData("AffiliationDescLower", "AffiliationDisplay")


        first_namelabel.Text = thisRequest.FirstName
        last_namelabel.Text = thisRequest.LastName
        requestor_namelabel.Text = thisRequest.RequestorName
        requestor_nameLabel2.Text = thisRequest.RequestorName
        entered_datelabel.Text = thisRequest.EnteredDate
		AccountLabel.Text = thisRequest.ApplicationDesc
		RequestCommentlabel.Text = thisRequest.RequestComment
		AccountRequestType = thisRequest.AccountRequestCode
        AppNum = thisRequest.ApplicationNum
        ItemCompleteDateTextBox.Text = thisRequest.ItemCompleteDate
        ItemValidCd = thisRequest.ValidCd
        iClientnum = thisRequest.ReceiverClientNum

        lblLevelSubmited.Text = thisRequest.GroupDesc
        lblDollarSubmitted.Text = thisRequest.DollarAmount
        lblLastApprvedBy.Text = thisRequest.ApprovedbyClientName

        lblAppSystem.Text = thisRequest.AppSystem
        CernerPositionDescTextBox.Text = thisRequest.CernerPositionDesc
        EPCSNominationTextbox.Text = thisRequest.EPCSNomination


        If AccountRequestType = "rehire" Then
            lblRehire.Visible = True
            lblRehire.Text = "THIS IS A REHIRE EMPLOYEE CHECK FOR any DEACTIVATED ACCOUNTs , Use That account Name if Possible"

        End If


		If lblLastApprvedBy.Text <> "" Then
            financialPnl.Visible = True
        End If


        'If T1LocationOfCareIDLabel.Text.ToString = "" Then
        '    T1LocationOfCareIDLabel.Text = thisRequest.LocationOfCareID
        'End If

        'If T1CopyToLabel.Text.ToString = "" Then
        '    T1CopyToLabel.Text = thisRequest.CopyTo
        'End If

        'If T1CommLocationOfCareIDLabel.Text.ToString = "" Then
        '    T1CommLocationOfCareIDLabel.Text = thisRequest.CommLocationOfCareID
        'End If

        'If T1CommCopytoLabel.Text.ToString = "" Then
        '    T1CommCopytoLabel.Text = thisRequest.CommCopyTo
        'End If

        Select Case AppNum
            Case 4  ' get an email name
                lblAppname.Visible = True
                AppnameTextBox.Visible = True
                lblAppname.Text = "Suppply a Valid eMail"
                AppnameTextBox.Text = first_namelabel.Text + "." + last_namelabel.Text + "@crozer.org"

            Case 254  ' get an email name
                lblAppname.Visible = True
                AppnameTextBox.Visible = True
                lblAppname.Text = "Default Cerner Position:"
                AppnameTextBox.Text = CernerPositionDescTextBox.Text
                AppnameTextBox.Enabled = False


            'Case 52, 53 ' 51 EMR CKHN 53 EMR Community

            '    lblLocationofCare.Visible = True
            '    T1LocationOfCareIDLabel.Visible = True

            '    lblCopyTo.Visible = True
            '    T1CopyToLabel.Visible = True

            '    lblCommLocationofcare.Visible = True
            '    T1CommLocationOfCareIDLabel.Visible = True

            '    lblCommCopyTo.Visible = True
            '    T1CommCopytoLabel.Visible = True

            '    LblDoctormaster.Visible = True
            '    lbldoctormastertxt.Visible = True

            Case 255 ' 91 Interface CKHN 93 interface Community 

                epcspnl.Visible = True

                If EPCSNominationTextbox.Text = "" Then

                    lblneednomination.Visible = True
                    Nominaationrbl.Visible = True

                    lblneedapproval.Visible = True
                    lblneedapproval.Text = "EPCS Approved"
                    Approvalrbl.Visible = True
                    Approvalrbl.Enabled = False
                    Approvalrbl.SelectedValue = "yes"

                ElseIf EPCSNominationTextbox.Text.ToLower = "epcsnominated" Then

                    lblneedapproval.Visible = True
                    Approvalrbl.Visible = True

                End If


                LblDoctormaster.Visible = True
                lbldoctormastertxt.Visible = True


            Case 102, 103, 104, 105, 109, 110, 111, 112, 115, 116, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129
                ' expose fincial level Approve button
                btnSubmitApprovBy.Visible = True
            Case 106, 107, 108
                ' expose fincial level and Dollar amount
                financialPnl.Visible = True
                lblLevel.Visible = True
                lblLevelSubmited.Visible = True
                lbldollar.Visible = True
                lblDollarSubmitted.Visible = True
                btnSubmitApprovBy.Visible = True

            Case 117, 118, 119
                ' expose fincial level 
                financialPnl.Visible = True
                lblLevel.Visible = True
                lblLevelSubmited.Visible = True

                lbldollar.Visible = False
                lblDollarSubmitted.Visible = False

                btnSubmitApprovBy.Visible = True

			Case 250
				btnGotoPMHRequest.Visible = True

				btnCreateSubmit.Visible = False
				btnInvalid.Visible = False

		End Select



        If Not IsPostBack Then

            LoginIDTextBox.Text = thisRequest.LoginName
            CommentsTextBox.Text = thisRequest.AccountItemDesc

            FormSignOn.FillForm()

            Dim employeetype As String = emp_type_cdLabel.Text
            emp_type_cdrbl.SelectedValue = employeetype


            CheckAffiliation()

            Dim ddlBinder As New DropDownListBinder

            ddlBinder.BindData(facility_cdddl, "SelFacilityCodes", "facility_cd", "facility_short_name", Session("EmployeeConn"))
            ddlBinder.BindData(Titleddl, "SelTitles", "TitleDesc", "TitleDesc", Session("EmployeeConn"))
            ddlBinder.BindData(Specialtyddl, "SelSpecialtyCodesV2", "specialty", "specialty", Session("EmployeeConn"))
            ddlBinder.BindData(entity_cdDDL, "sel_entity_codes", "entity_cd", "hosp_desc", Session("EmployeeConn"))

            Dim ddlInvisionBinder As New DropDownListBinder(group_nameddl, "SelPhysGroupsV5", Session("EmployeeConn"))
            ddlInvisionBinder.AddDDLCriteria("@GroupTypeCd", "invision", SqlDbType.NVarChar)
            ddlInvisionBinder.AddDDLCriteria("@clientpage", "yes", SqlDbType.NVarChar)

            ddlInvisionBinder.BindData("Group_num", "group_name")

            'Dim ddlNonPerferBinder As New DropDownListBinder(Nonperferdddl, "SelPhysGroupsV5", Session("EmployeeConn"))
            'ddlNonPerferBinder.AddDDLCriteria("@GroupTypeCd", "NonPerGrp", SqlDbType.NVarChar)
            'ddlNonPerferBinder.AddDDLCriteria("@clientpage", "yes", SqlDbType.NVarChar)

            'ddlNonPerferBinder.BindData("Group_num", "group_name")

            Dim ddGnumberBinder As New DropDownListBinder(LocationOfCareIDddl, "SelPhysGroupsV5", Session("EmployeeConn"))
            ddGnumberBinder.AddDDLCriteria("@GroupTypeCd", "CHMG", SqlDbType.NVarChar)
            ddGnumberBinder.AddDDLCriteria("@clientpage", "yes", SqlDbType.NVarChar)

            ddGnumberBinder.BindData("Group_num", "group_name")

            'Dim ddCommLocationOfCare As New DropDownListBinder(CommLocationOfCareIDddl, "SelPhysGroupsV5", Session("EmployeeConn"))
            'ddCommLocationOfCare.AddDDLCriteria("@GroupTypeCd", "CommNum", SqlDbType.NVarChar)
            'ddCommLocationOfCare.AddDDLCriteria("@clientpage", "yes", SqlDbType.NVarChar)

            'ddCommLocationOfCare.BindData("Group_num", "group_name")

            'Dim ddCoverageGroupddl As New DropDownListBinder(CoverageGroupddl, "SelPhysGroupsV5", Session("EmployeeConn"))
            'ddCoverageGroupddl.AddDDLCriteria("@GroupTypeCd", "coverage", SqlDbType.NVarChar)
            'ddCoverageGroupddl.AddDDLCriteria("@clientpage", "yes", SqlDbType.NVarChar)

            'ddCoverageGroupddl.BindData("Group_num", "Group_name")


            If LocationOfCareIDLabel.Text = "" Then
                LocationOfCareIDddl.SelectedIndex = -1
            Else
                LocationOfCareIDddl.SelectedValue = GNumberGroupNumLabel.Text

                'LocationOfCareIDddl.SelectedValue = LocationOfCareIDLabel.Text
            End If

            'If CommLocationOfCareIDLabel.Text = "" Then
            '    CommLocationOfCareIDddl.SelectedIndex = -1
            'Else
            '    CommLocationOfCareIDddl.SelectedValue = CommNumberGroupNumLabel.Text

            '    'CommLocationOfCareIDddl.SelectedValue = CommLocationOfCareIDLabel.Text
            'End If

            'If CoverageGroupNumLabel.Text = "" Then
            '    CoverageGroupddl.SelectedIndex = -1
            'Else
            '    CoverageGroupddl.SelectedValue = CoverageGroupNumLabel.Text

            'End If

            If InvisionGroupNumTextBox.Text = "" Then
                group_nameddl.SelectedIndex = -1
            Else
                group_nameddl.SelectedValue = InvisionGroupNumTextBox.Text

            End If

            'If NonPerferedGroupnumLabel.Text = "" Then
            '    Nonperferdddl.SelectedIndex = -1
            'Else
            '    Nonperferdddl.SelectedValue = NonPerferedGroupnumLabel.Text

            'End If


            If entity_cdLabel.Text = "" Then
                entity_cdDDL.SelectedIndex = -1
            Else
                Dim entitycd As String = entity_cdLabel.Text

                entitycd = entitycd.Trim
                entity_cdDDL.SelectedValue = entitycd

            End If

            If facility_cdLabel.Text = "" Then
                facility_cdddl.SelectedIndex = -1
            Else
                facility_cdddl.SelectedValue = facility_cdLabel.Text
            End If

            Dim ddbDepartmentBinder As New DropDownListBinder(department_cdddl, "sel_department_codes", Session("EmployeeConn"))
            ddbDepartmentBinder.AddDDLCriteria("@entity_cd", entity_cdDDL.SelectedValue, SqlDbType.NVarChar)
            ddbDepartmentBinder.BindData("department_cd", "department_name")

            department_cdddl.SelectedValue = department_cdLabel.Text


            If department_cdLabel.Text = "" Then
                department_cdddl.SelectedIndex = -1
            Else
                department_cdddl.SelectedValue = department_cdLabel.Text

            End If

            Titleddl.SelectedValue = TitleLabel.Text
            Specialtyddl.SelectedValue = SpecialtyLabel.Text

            If TitleLabel.Text = "" Then
                Titleddl.SelectedIndex = -1
            Else
                Titleddl.SelectedValue = TitleLabel.Text

            End If

            If SpecialtyLabel.Text = "" Then
                Specialtyddl.SelectedIndex = -1
            Else
                Specialtyddl.SelectedValue = SpecialtyLabel.Text

            End If

            If group_numTextBox.Text = "" Then
                group_nameddl.SelectedValue = -1
            Else
                group_nameddl.SelectedValue = group_numTextBox.Text.ToString
            End If


            Dim ddlBuildingBinder As New DropDownListBinder
            ddlBuildingBinder = New DropDownListBinder(building_cdddl, "sel_building_by_facility", Session("EmployeeConn"))
            ddlBuildingBinder.AddDDLCriteria("@facility_cd", facility_cdddl.SelectedValue, SqlDbType.NVarChar)
            ddlBuildingBinder.BindData("building_cd", "building_name")

            If building_cdLabel.Text = "" Then
                building_cdddl.SelectedIndex = -1
            Else
                Dim sBuilding As String = building_cdLabel.Text
                building_cdddl.SelectedValue = sBuilding.Trim

                If building_nameLabel.Text <> "" Then

                    building_cdddl.SelectedItem.Text = building_nameLabel.Text
                End If

            End If



            Dim ctrl As Control = DataCheckingAndValidation.SetChildControlsRecursiveToEnabledFalse(PanDemo, False)

            btnGoToRequest.Enabled = True


            If tokenRequestnum <> "" Then
                TokenForm = New FormSQL(Session("EmployeeConn"), "SelTokenAddress", "InsUpdAccountTokenAddress", "", Page)
                TokenForm.AddSelectParm("@account_request_num", tokenRequestnum, SqlDbType.Int, 6)

                TokenForm.FillForm()

                'Dim ddlCPBinder As New DropDownListBinder
                'ddlCPBinder.BindData(CellPhoneddl, "SelCellPhoneProviders", "ProviderExtension", "ProviderName", Session("EmployeeConn"))

                'If TokenNameTextBox.Text <> "" Then
                '    CellPhoneddl.SelectedValue = TokenNameTextBox.Text

                '    CommentsTextBox.Text = CommentsTextBox.Text & " " & TokenNameTextBox.Text

                'Else
                '    CellPhoneddl.SelectedValue = "other"

                '    TokenNameTextBox.Text = "other"

                '    CellPhoneddl.SelectedIndex = -1

                'End If

                If TokenPhoneTextBox.Text <> "" Then

                    LoginIDTextBox.Text = TokenPhoneTextBox.Text
                    CommentsTextBox.Text = CommentsTextBox.Text & " " & TokenPhoneTextBox.Text
                End If

                AccountLabel.Text = thisRequest.ApplicationDesc

                pnlTokenAddress.Visible = True


            End If


            ' ADD get Table here
            'Dim thisdata As New CommandsSqlAndOleDb("SelSubAcctountsItemsV3", Session("EmployeeConn"))

            'thisdata.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int, 9)
            'thisdata.ExecNonQueryNoReturn()

            'dtSubApplications = thisdata.GetSqlDataTable

            'Dim SubAppnum As Integer
            'Dim appDeafult As String
            'Dim snotice As String = ""
            'Dim sFacility As String = ""
            'Dim sNurseStation As String = ""

            'For Each row As DataRow In dtSubApplications.Rows
            '    'SubApplicationNum FacilityCd  SubAppCd
            '    SubAppnum = row("SubApplicationNum")
            '    appDeafult = row("SubAppCd")
            '    sFacility = row("FacilityCd")
            '    gAppName = row("SubAppDesc")



            'Next

            'Nursestation check
            Select Case AppNum
                Case 97, 98, 99, 100


                    Select Case lblAppSystem.Text
                        Case "15"

                            CCMCNurseStationspan.Visible = True
                        Case "12"
                            dcmhnursepan.Visible = True
                        Case "19"
                            taylornursepan.Visible = True
                        Case "16"
                            SpringNurseStationsPan.Visible = True

                    End Select

                    LoadNursestations(AppNum)

                Case 136

                    O365Panel.Visible = True
                    Load365Locations(AppNum)



            End Select


        End If
        ' end postback

        If Receiver_eMailLabel.Text <> "" Then
            e_mailTextBox.Text = Receiver_eMailLabel.Text

        End If

        If TCLTextBox.Text <> "" Then
            pnlTCl.Visible = True
            TCLTextBox.Visible = True
            UserTypeDescTextBox.Visible = True
            UserTypeCdTextBox.Visible = True
        End If

        If hanrbl.SelectedValue.ToString = "Yes" Then
            LocationOfCareIDddl.Visible = True
            ckhnlbl.Visible = True

            LocationOfCareIDddl.BackColor() = Color.Yellow
            LocationOfCareIDddl.BorderColor() = Color.Black
            LocationOfCareIDddl.BorderStyle() = BorderStyle.Solid
            LocationOfCareIDddl.BorderWidth = Unit.Pixel(2)

            'lblprov.Visible = True
            'providerrbl.Visible = True

        Else
            LocationOfCareIDddl.Visible = False
            ckhnlbl.Visible = False

            'lblprov.Visible = False
            'providerrbl.Visible = False

        End If



        fillRequestItemsTable()
        FillCurrentAccounts() ' View of current accounts


        ActionControl.GetAllActions(AccountRequestNum, Session("EmployeeConn"))
        ActionList = ActionControl.AllActions()

        TPActionItems.Controls.Add(fillActionItemsTable(ActionControl.AllActions()))


        If ItemCompleteDateTextBox.Text <> Nothing Then
            tpheader.Visible = False

        End If

        If valid_dateLabel.Text = "" Or ItemValidCd.ToLower = "needsvalidation" Then
            btnCreateSubmit.Enabled = False
            btnInvalid.Enabled = False
            LblMainInfo.Text = "This Request or Account has not yet been Validated."
        End If

		'PMHWait

		If AccountRequestType = "addpmhact" And siemens_emp_numLabel.Text <> "" Then
			'PMHWait
			If ItemValidCd.ToLower = "pmhwait" Then
				btnCheckLDAP.Visible = True
				lblRehire.Text = "CHECK to see if the AD account has been created."
			End If
		End If

		Select Case iRolenum
            Case "782", "1197", "1194"
                If doctor_master_numTextBox.Text <> Nothing Then
                    emp_type_cdrbl.Enabled = False
                    lbldoctormastertxt.Text = doctor_master_numTextBox.Text
                End If

                If valid_dateLabel.Text = "" Or ItemValidCd.ToLower = "needsvalidation" Then
                    btnCreateSubmit.Enabled = False
                    btnInvalid.Enabled = False
                    LblMainInfo.Text = "This Request or Account has not yet been Validated."
                End If
            Case "1193"
                If doctor_master_numTextBox.Text <> "" Then
                    DoctorMasterNumLabel.Text = doctor_master_numTextBox.Text
                    DoctorMasterNumLabel.Visible = True
                    behaviorallbl.Visible = True

                End If

                PhysOfficepanel.Visible = True

            Case Else

        End Select

        If Session("SecurityLevelNum") < 66 Or Session("SecurityLevelNum") = 82 Then
            'Dim ctrl As Control = DataCheckingAndValidation.SetChildControlsRecursiveToEnabledFalse(PanDemo, False)

            Dim Mainctrl As Control = DataCheckingAndValidation.SetChildControlsRecursiveToEnabledFalse(tpheader, False)
            btnGoToRequest.Visible = False

        End If

        Dim ctrl2 As Control = DataCheckingAndValidation.SetChildControlsRecursiveToEnabledFalse(PanDemo, False)
        btnGoToRequest.Enabled = True

        'tpheader

        If Session("Usertype").ToString.ToLower = "provaccount" Then
            btnGoToRequest.Visible = False
            tpRequestItems.Visible = False

        End If
        ' btnRequestDetails.Attributes.Add("onclick", "newPopup('RequestDetail.aspx?RequestNum=" & AccountRequestNum & " ')")


    End Sub
    Protected Sub btnCreateSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCreateSubmit.Click
        If HDemployeetype.Text.ToLower <> emp_type_cdrbl.SelectedValue.ToLower Then
            If emp_type_cdrbl.SelectedValue = "physician" Or emp_type_cdrbl.SelectedValue = "allied health" Then

                lblValidation.Text = "Can Not Change Affiliation type to Physician type of any kind. You must get a PROVIDER Modifier to make this change."
                emp_type_cdrbl.SelectedValue = HDemployeetype.Text

                Exit Sub
            End If
        End If
        If AppnameTextBox.Text = "" And AppNum = 4 Then
            lblValidation.Text = "You must supply a valid eMail address"

            Exit Sub
        End If


        Dim thisData As New CommandsSqlAndOleDb("UpdAccountItemsV21", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int)
        thisData.AddSqlProcParameter("@account_request_seq_num", AccountSeqNum, SqlDbType.Int)
        thisData.AddSqlProcParameter("@ApplicationNum", AppNum, SqlDbType.Int, 30)
        thisData.AddSqlProcParameter("@new_login_name", LoginIDTextBox.Text, SqlDbType.NVarChar, 50)

        thisData.AddSqlProcParameter("@item_desc", CommentsTextBox.Text, SqlDbType.NVarChar, 255)
        thisData.AddSqlProcParameter("@account_request_type", AccountRequestType, SqlDbType.NVarChar, 255)
        thisData.AddSqlProcParameter("@nt_login", Session("LoginID"), SqlDbType.NVarChar, 50)
        If AppnameTextBox.Text <> "" And AppNum = 4 Then
            thisData.AddSqlProcParameter("@Clients_new_email", AppnameTextBox.Text, SqlDbType.NVarChar, 75)

        End If
        If AppnameTextBox.Text <> "" And AppNum = 254 Then
            thisData.AddSqlProcParameter("@CernerPositionDesc", AppnameTextBox.Text, SqlDbType.NVarChar, 50)

        End If

        If AppNum = 255 Then
            If HDNominationTextBox.Text = "yes" Then
                EPCSNominationTextbox.Text = "epcsnominated"

            End If

            If HDApprovalTextBox.Text = "yes" Then
                EPCSNominationTextbox.Text = "epcsapproved"


            End If
            thisData.AddSqlProcParameter("@EPCSNomination", EPCSNominationTextbox.Text, SqlDbType.NVarChar, 20)
        End If

        thisData.ExecNonQueryNoReturn()

        'If T1LocationOfCareIDTextBox.Text <> "" Then
        '    thisData.AddSqlProcParameter("@LocationOfCareID", T1LocationOfCareIDTextBox.Text, SqlDbType.NVarChar, 75)
        'End If

        'If T1CopyToTextBox.Text <> "" Then
        '    thisData.AddSqlProcParameter("@CopyTo", T1CopyToTextBox.Text, SqlDbType.NVarChar, 75)
        'End If

        'If T1CommLocationOfCareIDtextBox.Text <> "" Then
        '    thisData.AddSqlProcParameter("@CommLocationOfCareID", T1CommLocationOfCareIDtextBox.Text, SqlDbType.NVarChar, 75)
        'End If

        'If T1CommCopytoTextBox.Text <> "" Then
        '    thisData.AddSqlProcParameter("@CommCopyTo", T1CommCopytoTextBox.Text, SqlDbType.NVarChar, 75)
        'End If

        'If AppNum = 91 Or AppNum = 93 Then  ' 91 Interface CKHN 93 interface Community
        '    thisData.AddSqlProcParameter("@AddedDoctorMasterCloverleaf", T1CopyTorbl.SelectedItem.ToString, SqlDbType.NVarChar, 6)
        '    thisData.AddSqlProcParameter("@AddedMedAssistCloverleaf", MedAssrbl.SelectedItem.ToString, SqlDbType.NVarChar, 6)
        '    thisData.AddSqlProcParameter("@BouncedInterface", Bouncedrbl.SelectedItem.ToString, SqlDbType.NVarChar, 6)
        'End If





        AccRequest = New AccountRequest(AccountRequestNum, Session("EmployeeConn"))

        If Session("environment") = "" Then
            Dim Envoirn As New Environment()
            Session("environment") = Envoirn.getEnvironment
        End If

        ' Case for 
        Select Case AppNum
            Case 97, 98, 99, 100
                InsertNursestations()
                'Mak

            Case 136
                '365
                InsertNursestations()

        End Select

        If AccRequest.RSAToken = "yes" Then
            sendEmail()
            Response.Redirect("./MyAccountQueue.aspx")
        End If

        If AccRequest.CloseDate <> Nothing Then
            sendEmail()
            Response.Redirect("./MyAccountQueue.aspx")

        End If

        Response.Redirect("./MyAccountQueue.aspx")

        '        Response.Redirect("./AccountRequestinfo2.aspx?requestnum=" & AccountRequestNum) ' & "&account_seq_num=" & AccountSeqNum, True)



    End Sub

    Protected Sub btnSaveItemInfo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveItemInfo.Click


        Dim thisData As New CommandsSqlAndOleDb("UpdAccountItemInfoV3", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@account_request_seq_num", AccountSeqNum, SqlDbType.Int)
        thisData.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int)
        thisData.AddSqlProcParameter("@ApplicationNum", AppNum, SqlDbType.Int, 30)
        thisData.AddSqlProcParameter("@item_desc", CommentsTextBox.Text, SqlDbType.NVarChar, 255)
        thisData.AddSqlProcParameter("@login_name", LoginIDTextBox.Text, SqlDbType.NVarChar, 50)

        thisData.ExecNonQueryNoReturn()


        Response.Redirect("./CreateAccount.aspx?requestnum=" & AccountRequestNum & "&account_seq_num=" & AccountSeqNum, True)

    End Sub
    Protected Sub LoadNursestations(ByVal sAppnum As String)

        Dim thisdata As New CommandsSqlAndOleDb("SelSubAcctountsItemsV3", Session("EmployeeConn"))
        thisdata.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int, 9)

        thisdata.ExecNonQueryNoReturn()

        dtSubApplications = thisdata.GetSqlDataTable

        Dim SubAppnum As String = ""
        Dim SubAppcd As String = ""
        Dim subAppDesc As String = ""

        Select Case lblAppSystem.Text
            Case "15"

                For Each cblcItem As ListItem In cblCCMCNursestations.Items
                    cblcItem.Selected = False
                    'cblItem.Enabled = False
                Next

                Dim cblCCMCNurBinder As New CheckBoxListBinder(cblCCMCNursestations, "SelNursestationByFacility", Session("EmployeeConn"))
                cblCCMCNurBinder.AddDDLCriteria("@facilityCd", "15", SqlDbType.NVarChar)
                cblCCMCNurBinder.AddDDLCriteria("@SubApps", "yes", SqlDbType.NVarChar)
                cblCCMCNurBinder.AddDDLCriteria("@applicationnum", sAppnum, SqlDbType.NVarChar)
                cblCCMCNurBinder.BindData("SubApplicationNum", "NurseStationDesc")

                For Each row As DataRow In dtSubApplications.Rows
                    SubAppnum = row("SubApplicationNum")
                    SubAppcd = row("SubAppCd")
                    subAppDesc = row("SubAppDesc")

                    For Each cblcItem As ListItem In cblCCMCNursestations.Items
                        Dim sCurritem As String = cblcItem.Value

                        If sCurritem = SubAppnum Then
                            cblcItem.Selected = True
                        End If

                    Next
                Next

            Case "12"

                For Each cblDItem As ListItem In cblDCMHNursestataions.Items
                    cblDItem.Selected = False
                    'cblItem.Enabled = False
                Next

                Dim cblDCMHNurBinder As New CheckBoxListBinder(cblDCMHNursestataions, "SelNursestationByFacility", Session("EmployeeConn"))
                cblDCMHNurBinder.AddDDLCriteria("@facilityCd", "12", SqlDbType.NVarChar)
                cblDCMHNurBinder.AddDDLCriteria("@SubApps", "yes", SqlDbType.NVarChar)
                cblDCMHNurBinder.AddDDLCriteria("@applicationnum", sAppnum, SqlDbType.NVarChar)
                cblDCMHNurBinder.BindData("SubApplicationNum", "NurseStationDesc")



                For Each row As DataRow In dtSubApplications.Rows
                    SubAppnum = row("SubApplicationNum")
                    SubAppcd = row("SubAppCd")
                    subAppDesc = row("SubAppDesc")

                    For Each cblDItem As ListItem In cblDCMHNursestataions.Items
                        Dim sCurritem As String = cblDItem.Value

                        If sCurritem = SubAppnum Then
                            cblDItem.Selected = True
                        End If

                    Next
                Next

            Case "19"
                For Each cblTItem As ListItem In cblTaylorNursestataions.Items
                    cblTItem.Selected = False
                    'cblItem.Enabled = False
                Next

                Dim cblTaylorNurBinder As New CheckBoxListBinder(cblTaylorNursestataions, "SelNursestationByFacility", Session("EmployeeConn"))
                cblTaylorNurBinder.AddDDLCriteria("@facilityCd", "19", SqlDbType.NVarChar)
                cblTaylorNurBinder.AddDDLCriteria("@SubApps", "yes", SqlDbType.NVarChar)
                cblTaylorNurBinder.AddDDLCriteria("@applicationnum", sAppnum, SqlDbType.NVarChar)
                cblTaylorNurBinder.BindData("SubApplicationNum", "NurseStationDesc")


                For Each row As DataRow In dtSubApplications.Rows
                    SubAppnum = row("SubApplicationNum")
                    SubAppcd = row("SubAppCd")
                    subAppDesc = row("SubAppDesc")

                    For Each cblTItem As ListItem In cblTaylorNursestataions.Items
                        Dim sCurritem As String = cblTItem.Value

                        If sCurritem = SubAppnum Then
                            cblTItem.Selected = True
                        End If

                    Next
                Next

            Case "16"


                For Each cblSItem As ListItem In cblSpringfieldNursestataions.Items
                    cblSItem.Selected = False
                    'cblItem.Enabled = False
                Next

                Dim cblSpringNurBinder As New CheckBoxListBinder(cblSpringfieldNursestataions, "SelNursestationByFacility", Session("EmployeeConn"))
                cblSpringNurBinder.AddDDLCriteria("@facilityCd", "16", SqlDbType.NVarChar)
                cblSpringNurBinder.AddDDLCriteria("@SubApps", "yes", SqlDbType.NVarChar)
                cblSpringNurBinder.AddDDLCriteria("@applicationnum", sAppnum, SqlDbType.NVarChar)
                cblSpringNurBinder.BindData("SubApplicationNum", "NurseStationDesc")


                For Each row As DataRow In dtSubApplications.Rows
                    SubAppnum = row("SubApplicationNum")
                    SubAppcd = row("SubAppCd")
                    subAppDesc = row("SubAppDesc")

                    For Each cblSItem As ListItem In cblSpringfieldNursestataions.Items
                        Dim sCurritem As String = cblSItem.Value

                        If sCurritem = SubAppnum Then
                            cblSItem.Selected = True
                        End If

                    Next
                Next


        End Select

    End Sub
    Protected Sub Load365Locations(ByVal sAppnum As String)

        Dim thisdata As New CommandsSqlAndOleDb("SelSubAcctountsItemsV3", Session("EmployeeConn"))
        thisdata.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int, 9)
        thisdata.AddSqlProcParameter("@ApplicationNum", sAppnum, SqlDbType.Int, 9)

        thisdata.ExecNonQueryNoReturn()

        dtSubApplications = thisdata.GetSqlDataTable


        Dim SubAppnum As String = ""
        Dim SubAppcd As String = ""
        Dim subAppDesc As String = ""

        For Each cbl365Item As ListItem In cblo365.Items
            cbl365Item.Selected = False
            'cblItem.Enabled = False
        Next


        Dim cblo365Binder As New CheckBoxListBinder(cblo365, "Selo365Groups", Session("EmployeeConn"))
        cblo365Binder.BindData("SubApplicationNum", "SubAppDesc")

        For Each row As DataRow In dtSubApplications.Rows
            SubAppnum = row("SubApplicationNum")
            SubAppcd = row("SubAppCd")
            subAppDesc = row("SubAppDesc")

            For Each cblSItem As ListItem In cblo365.Items
                Dim sCurritem As String = cblSItem.Value

                If sCurritem = SubAppnum Then
                    cblSItem.Selected = True
                End If

            Next
        Next



    End Sub


    Protected Sub InsertNursestations()

        Dim SubAppnum As String = ""
        Dim SubAppcd As String = ""
        Dim subAppDesc As String = ""

        Select Case AppNum
            Case "136"
                'o365 accounts
                'cblo365
                For Each cblo365Item As ListItem In cblo365.Items
                    Dim sCurritem As String = cblo365Item.Value

                    If cblo365Item.Selected = True Then

                        'If sCurritem = 324 Then
                        '    Facility = 20
                        'Else
                        '    Facility = 26

                        'End If

                        Dim SubmitData As New CommandsSqlAndOleDb("UpdSubAccountItemsV6", Session("EmployeeConn"))
                        SubmitData.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int, 9)
                        SubmitData.AddSqlProcParameter("@ApplicationNum", AppNum, SqlDbType.Int, 9)
                        SubmitData.AddSqlProcParameter("@SubApplicationNum", cblo365Item.Value, SqlDbType.Int, 9)
                        SubmitData.AddSqlProcParameter("@new_login_name", LoginIDTextBox.Text, SqlDbType.NVarChar, 50)

                        'SubmitData.AddSqlProcParameter("@FacilityCd", Facility, SqlDbType.Int, 9)
                        'SubmitData.AddSqlProcParameter("@validate", ValidCd, SqlDbType.NVarChar, 20)

                        SubmitData.AddSqlProcParameter("@nt_login", Session("LoginID"), SqlDbType.NVarChar, 50)

                        SubmitData.ExecNonQueryNoReturn()


                    End If

                Next
        End Select


        Select Case lblAppSystem.Text
            Case "15"

                For Each cblcItem As ListItem In cblCCMCNursestations.Items
                    Dim sCurritem As String = cblcItem.Value

                    If cblcItem.Selected = True Then


                        Dim SubmitData As New CommandsSqlAndOleDb("UpdSubAccountItemsV6", Session("EmployeeConn"))
                        SubmitData.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int, 9)
                        SubmitData.AddSqlProcParameter("@ApplicationNum", AppNum, SqlDbType.Int, 9)
                        SubmitData.AddSqlProcParameter("@SubApplicationNum", sCurritem, SqlDbType.Int, 9)
                        'SubmitData.AddSqlProcParameter("@SubAppCd", cblcItem.Value, SqlDbType.NVarChar, 20)
                        SubmitData.AddSqlProcParameter("@new_login_name", LoginIDTextBox.Text, SqlDbType.NVarChar, 50)

                        SubmitData.AddSqlProcParameter("@nt_login", Session("LoginID"), SqlDbType.NVarChar, 50)

                        SubmitData.ExecNonQueryNoReturn()

                    End If

                Next

            Case "12"

                For Each cblDItem As ListItem In cblDCMHNursestataions.Items

                    Dim sCurritem As String = cblDItem.Value

                    If cblDItem.Selected = True Then


                        Dim SubmitData As New CommandsSqlAndOleDb("UpdSubAccountItemsV6", Session("EmployeeConn"))
                        SubmitData.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int, 9)
                        SubmitData.AddSqlProcParameter("@ApplicationNum", AppNum, SqlDbType.Int, 9)
                        SubmitData.AddSqlProcParameter("@SubApplicationNum", sCurritem, SqlDbType.Int, 9)
                        'SubmitData.AddSqlProcParameter("@SubAppCd", cblDItem.Value, SqlDbType.NVarChar, 20)
                        SubmitData.AddSqlProcParameter("@new_login_name", LoginIDTextBox.Text, SqlDbType.NVarChar, 50)

                        SubmitData.AddSqlProcParameter("@nt_login", Session("LoginID"), SqlDbType.NVarChar, 50)

                        SubmitData.ExecNonQueryNoReturn()

                    End If

                Next

            Case "19"

                For Each cblTItem As ListItem In cblTaylorNursestataions.Items

                    Dim sCurritem As String = cblTItem.Value


                    If cblTItem.Selected = True Then

                        Dim SubmitData As New CommandsSqlAndOleDb("UpdSubAccountItemsV6", Session("EmployeeConn"))
                        SubmitData.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int, 9)
                        SubmitData.AddSqlProcParameter("@ApplicationNum", AppNum, SqlDbType.Int, 9)
                        SubmitData.AddSqlProcParameter("@SubApplicationNum", sCurritem, SqlDbType.Int, 9)
                        'SubmitData.AddSqlProcParameter("@SubAppCd", cblTItem.Value, SqlDbType.NVarChar, 20)
                        SubmitData.AddSqlProcParameter("@new_login_name", LoginIDTextBox.Text, SqlDbType.NVarChar, 50)

                        SubmitData.AddSqlProcParameter("@nt_login", Session("LoginID"), SqlDbType.NVarChar, 50)

                        SubmitData.ExecNonQueryNoReturn()

                    End If

                Next

            Case "16"


                For Each cblSItem As ListItem In cblSpringfieldNursestataions.Items
                    Dim sCurritem As String = cblSItem.Value


                    If cblSItem.Selected = True Then
                        'cblItem.Enabled = False

                        Dim SubmitData As New CommandsSqlAndOleDb("UpdSubAccountItemsV6", Session("EmployeeConn"))
                        SubmitData.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int, 9)
                        SubmitData.AddSqlProcParameter("@ApplicationNum", AppNum, SqlDbType.Int, 9)
                        SubmitData.AddSqlProcParameter("@SubApplicationNum", sCurritem, SqlDbType.Int, 9)
                        'SubmitData.AddSqlProcParameter("@SubAppCd", cblSItem.Value, SqlDbType.NVarChar, 20)
                        SubmitData.AddSqlProcParameter("@new_login_name", LoginIDTextBox.Text, SqlDbType.NVarChar, 50)

                        SubmitData.AddSqlProcParameter("@nt_login", Session("LoginID"), SqlDbType.NVarChar, 50)

                        SubmitData.ExecNonQueryNoReturn()

                    End If

                Next

        End Select


    End Sub

    Private Sub sendEmail()
        Dim Request As New AccountRequest(AccountRequestNum, Session("EmployeeConn"))
        Dim RequestItems As New RequestItems(AccountRequestNum)
        Dim EmailBody As New EmailBody

        Dim sSubject As String
        Dim sBody As String

        Dim EmailList As String
        Dim sAppbase As String = ""
        Dim Link As String

        Dim sAccounts As String = ""

        If Session("environment").ToString.ToLower = "testing" Then
            'toEmail = "Jeff.Mele@crozer.org"
            Link = "http://webdev3/CSCV2/AccountRequestInfo2.aspx?RequestNum=" & AccountRequestNum.ToString
        Else
            Link = "http://Casemgmt02/CSCV2/AccountRequestInfo2.aspx?RequestNum=" & AccountRequestNum.ToString

        End If

        sSubject = "Account Request for " & Request.FirstName & " " & Request.LastName & " completed."


        sBody = "<!DOCTYPE html>" & _
"<html>" & _
"<head>" & _
"<style>" & _
"table" & _
"{" & _
"border-collapse:collapse;" & _
"font-family: 'Helvetica Neue', 'Lucida Grande', 'Segoe UI', Arial, Helvetica, Verdana, sans-serif;" & _
"}" & _
"table, th, td" & _
"{" & _
"border: 1px solid black;" & _
"padding: 3px" & _
"}" & _
"</style>" & _
"</head>" & _
"" & _
"<body>" & _
 EmailBody.EmailCompletedRequestTable(Request, RequestItems) & _
                        "</body>" & _
                        "</html>"

        sBody = sBody & "<table cellpadding='5px'>" & _
        "<tr>" & _
            "<td align='center'  style='font-size:large'>" & _
"               " & _
            "<b>View Details by clicking on link below</b>" & _
"                 " & _
             "</td>    " & _
        "</tr>" & _
        "<tr>" & _
            "<td align='center'>" & Link & "</td>" & _
        "</tr>"


        EmailList = New EmailListOne(sAppbase, AccountRequestNum, Session("EmployeeConn")).EmailListReturn

        Dim sToAddress As String = Request.SubmitterEmail

        Dim toList As String()

        If Session("environment").ToString.ToLower = "testing" Then
            '  EmailList = EmailList & ";Jeff.Mele@crozer.org"
            EmailList = "Jeff.Mele@crozer.org"
        End If

        toList = EmailList.Split(";")
        If toList.Length = 1 Then
            Mailmsg.To.Add(New System.Net.Mail.MailAddress(toList(0)))
        Else
        End If
        For i = 0 To toList.Length - 1 ' - 2
            If toList(i).Length > 10 Then ' validity check
                Mailmsg.To.Add(New System.Net.Mail.MailAddress(toList(i)))
            End If
        Next

        Mailmsg.From = New MailAddress("CSC@crozer.org")
        Mailmsg.IsBodyHtml = True

        Mailmsg.Subject = sSubject
        Mailmsg.Body = sBody

        Dim smtpClient As New SmtpClient()
        smtpClient.Host = "ah1smtprelay01.altahospitals.com"


        If Session("environment") = "Production" Then
            smtpClient.Send(Mailmsg)
        End If




        Dim thisData As New CommandsSqlAndOleDb("InsAccounteMailTo", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int)
        thisData.AddSqlProcParameter("@ToAddress", EmailList, SqlDbType.NVarChar, 4000)
        thisData.AddSqlProcParameter("@subject", sSubject, SqlDbType.NVarChar, 200)
        thisData.AddSqlProcParameter("@body", sBody, SqlDbType.NVarChar, 4000)

        thisData.ExecNonQueryNoReturn()


    End Sub

    Private Sub sendAncillaryEmail()
        Dim Request As New AccountRequest(AccountRequestNum, Session("EmployeeConn"))
        Dim RequestItems As New RequestItems(AccountRequestNum)
        Dim EmailBody As New EmailBody

        Dim sSubject As String
        Dim sBody As String

        Dim EmailList As String
        Dim sAppbase As String = ""
        Dim Link As String

        Dim sAccounts As String = ""

        If Session("environment").ToString.ToLower = "testing" Then
            'toEmail = "Jeff.Mele@crozer.org"
            Link = "http://webdev3/CSCV2/AccountRequestInfo2.aspx?RequestNum=" & AccountRequestNum.ToString
        Else
            Link = "http://Casemgmt02/CSCV2/AccountRequestInfo2.aspx?RequestNum=" & AccountRequestNum.ToString

        End If

        sSubject = "Account Request for " & Request.FirstName & " " & Request.LastName & " completed."


        sBody = "<!DOCTYPE html>" & _
"<html>" & _
"<head>" & _
"<style>" & _
"table" & _
"{" & _
"border-collapse:collapse;" & _
"font-family: 'Helvetica Neue', 'Lucida Grande', 'Segoe UI', Arial, Helvetica, Verdana, sans-serif;" & _
"}" & _
"table, th, td" & _
"{" & _
"border: 1px solid black;" & _
"padding: 3px" & _
"}" & _
"</style>" & _
"</head>" & _
"" & _
"<body>" & _
"<table cellpadding='5px'>" & _
"<tr>" & _
    "<td colspan='3' align='center'  style='font-size:large'>" & _
"               " & _
    "<b>Provider Demographic Change Complete</b>" & _
"                 " & _
     "</td>    " & _
"</tr>" & _
"<tr>" & _
    "<td style='font-weight:bold' colspan='1'>Name:</td>" & _
    "<td colspan='3'>" & Request.FirstName & " " & Request.LastName & "</td>" & _
"</tr>" & _
"<tr>" & _
    "<td style='font-weight:bold' colspan='1'>Requested By:</td>" & _
    "<td colspan='3'>" & Request.RequestorName & "</td>" & _
"</tr>" & _
   "<tr>" & _
    "<td style='font-weight:bold' colspan='1'>Requestor Email:</td>" & _
    "<td colspan='3'>" & Request.RequestorEmail & "</td>" & _
"</tr>" & _
"<tr>" & _
    "<td style='font-weight:bold' colspan='1'>Date Requested:</td>" & _
    "<td colspan='3'>" & Request.EnteredDate & "</td>" & _
"</tr>" & _
"      " & _
"     " & _
"<tr style='font-weight:bold'><td>Information </td><td> Data </td></tr>" & _
"<tr><td> First Name </td><td>" & Request.FirstName & "</td></tr>" & _
"<tr><td> Last Name </td><td>" & Request.LastName & "</td></tr>" & _
"<tr><td> CCMC Credential Date</td><td>" & credentialedDateTextBox.Text & "</td></tr>" & _
"<tr><td> DCMH Credential Date</td><td>" & credentialedDateTextBox.Text & "</td></tr>" & _
"<tr><td> Address 1 </td><td>" & address_1textbox.Text & "</td></tr>" & _
"<tr><td> Address 2 </td><td>" & address_2textbox.Text & "</td></tr>" & _
"<tr><td> City </td><td>" & citytextbox.Text & "</td></tr>" & _
"<tr><td> State </td><td>" & statetextbox.Text & "</td></tr>" & _
"<tr><td> Zip </td><td>" & ziptextbox.Text & "</td></tr>" & _
"<tr><td> Phone </td><td>" & phoneTextBox.Text & "</td></tr>" & _
"<tr><td> Fax </td><td>" & faxtextbox.Text & "</td></tr>" & _
"<tr><td> Doctor Master#</td><td>" & doctor_master_numTextBox.Text & "</td></tr>" & _
"<tr><td> NPI</td><td>" & npitextbox.Text & "</td></tr>" & _
"<tr><td> License No</td><td>" & LicensesNoTextBox.Text & "</td></tr>" & _
"<tr><td> Credential Date</td><td>" & credentialedDateTextBox.Text & "</td></tr>" & _
"</table>" & _
       "</body>" & _
        "</html>"

        sBody = sBody & "<table cellpadding='5px'>" & _
        "<tr>" & _
            "<td align='center'  style='font-size:large'>" & _
"               " & _
            "<b>View Details by clicking on link below</b>" & _
"                 " & _
             "</td>    " & _
        "</tr>" & _
        "<tr>" & _
            "<td align='center'>" & Link & "</td>" & _
        "</tr>"


        'ActionControl.GetAllActions(AccountRequestNum, Session("EmployeeConn"))

        EmailList = New EmailListOne(AppNum, AccountRequestNum, Session("EmployeeConn")).EmailListReturn


        Dim sToAddress As String = Request.SubmitterEmail

        Dim toList As String()

        toList = EmailList.Split(";")
        If toList.Length = 1 Then
            Mailmsg.To.Add(New System.Net.Mail.MailAddress(toList(0)))
        Else
        End If
        For i = 0 To toList.Length - 1 ' - 2
            If toList(i).Length > 10 Then ' validity check
                Mailmsg.To.Add(New System.Net.Mail.MailAddress(toList(i)))
            End If
        Next

        Mailmsg.From = New MailAddress("CSC@crozer.org")
        Mailmsg.IsBodyHtml = True

        Mailmsg.Subject = sSubject
        Mailmsg.Body = sBody

        Dim smtpClient As New SmtpClient()
        smtpClient.Host = "ah1smtprelay01.altahospitals.com"


        smtpClient.Send(Mailmsg)


        Dim thisData As New CommandsSqlAndOleDb("InsAccounteMailTo", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int)
        thisData.AddSqlProcParameter("@ToAddress", EmailList, SqlDbType.NVarChar, 4000)
        thisData.AddSqlProcParameter("@subject", sSubject, SqlDbType.NVarChar, 200)
        thisData.AddSqlProcParameter("@body", sBody, SqlDbType.NVarChar, 4000)

        thisData.ExecNonQueryNoReturn()


    End Sub

    Protected Sub fillRequestItemsTable()
        Dim thisData As New CommandsSqlAndOleDb("SelRequestItemsV6", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.NVarChar)

        Dim iRowCount = 1

        Dim dt As DataTable
        dt = thisData.GetSqlDataTable

        Dim rwHeader As New TableRow

        Dim clBtnheader As New TableCell
        Dim clAccountHeader As New TableCell
        Dim clRequestTypeHeader As New TableCell
        Dim clStatusHeader As New TableCell


        clBtnheader.Text = ""
        clAccountHeader.Text = "Account"
        clRequestTypeHeader.Text = "Request Type"
        clStatusHeader.Text = "Status"


        rwHeader.Cells.Add(clBtnheader)
        rwHeader.Cells.Add(clAccountHeader)
        rwHeader.Cells.Add(clRequestTypeHeader)
        rwHeader.Cells.Add(clStatusHeader)


        rwHeader.Font.Bold = True
        rwHeader.BackColor = Color.FromName("#006666")
        rwHeader.ForeColor = Color.FromName("White")
        rwHeader.Height = Unit.Pixel(36)

        tblRequestItems.Rows.Add(rwHeader)

        For Each drRow As DataRow In dt.Rows


            Dim rwData As New TableRow

            Dim clBtn As New TableCell

            Dim claccountRequest As New TableCell
            Dim claRequestSeqNum As New TableCell

            Dim clReqNum As New TableCell

            Dim clAccountData As New TableCell
            Dim clRequestTypeData As New TableCell
            Dim clStatusData As New TableCell
            Dim clRequestorData As New TableCell
            Dim clReqPhoneData As New TableCell


            clAccountData.Text = IIf(IsDBNull(drRow("ApplicationDesc")), "", drRow("ApplicationDesc"))
            clRequestTypeData.Text = IIf(IsDBNull(drRow("request_code_desc")), "", drRow("request_code_desc"))
            clStatusData.Text = IIf(IsDBNull(drRow("valid_cd")), "", drRow("valid_cd"))

            claccountRequest.Text = IIf(IsDBNull(drRow("account_request_num")), "", drRow("account_request_num"))
            claRequestSeqNum.Text = IIf(IsDBNull(drRow("account_request_seq_num")), "", drRow("account_request_seq_num"))

            clReqNum.Text = "<a href=""CreateAccount.aspx?requestnum=" & claccountRequest.Text & "&account_seq_num=" & claRequestSeqNum.Text & """ ><u><b> " & claccountRequest.Text & "</b></u></a>"

            ' clBtn.Attributes("onClick") = ' "btn_Click( " & claccountRequest.Text & " , " & claRequestSeqNum.Text & ")" '
            'Selbtn.Attributes("onClick") = Response.Redirect("./CreateAccount.aspx?requestnum=" & claccountRequest.Text & "&account_seq_num=" & claRequestSeqNum.Text, True)

            ' clBtn.Controls.Add(DirectCast(Selbtn, Button))
            If Session("SecurityLevelNum") < 60 Then
                clReqNum.Text = claccountRequest.Text

            End If

            rwData.Cells.Add(clReqNum)
            rwData.Cells.Add(clAccountData)
            rwData.Cells.Add(clRequestTypeData)
            rwData.Cells.Add(clStatusData)


            If iRowCount > 0 Then

                rwData.BackColor = Color.Bisque

            Else

                rwData.BackColor = Color.LightGray

            End If

            tblRequestItems.Rows.Add(rwData)

            iRowCount = iRowCount * -1

        Next
        tblRequestItems.CellPadding = 10
        tblRequestItems.Width = New Unit("100%")


    End Sub
    Private Sub CheckAffiliation()

        'pnlAlliedHealth.Visible = False

        HDemployeetype.Text = emp_type_cdrbl.SelectedValue



        Select Case emp_type_cdrbl.SelectedValue

            Case "employee"

                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")

                'Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                'Dim cityReq As New RequiredField(citytextbox, "City")
                'Dim stateReq As New RequiredField(statetextbox, "State")
                'Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                'Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                'Dim faxqReq As New RequiredField(faxtextbox, "Fax")
                '  Dim locReq As New RequiredField(facility_cdddl, "Location")

                ' Dim endReq As New RequiredField(end_dateTextBox, "End Date")


                'pnlAlliedHealth.Visible = False
                pnlProvider.Visible = False
                'providerrbl.SelectedIndex = -1




            Case "student"

                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")
                Dim titleReq As New RequiredField(Titleddl, "Title")
                Dim specReq As New RequiredField(Specialtyddl, "Specialty")
                Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim faxqReq As New RequiredField(faxtextbox, "Fax")


                'pnlAlliedHealth.Visible = False
                pnlProvider.Visible = False
                'providerrbl.SelectedIndex = -1

                'Dim locReq As New RequiredField(facility_cdddl, "Location")

                'Dim endReq As New RequiredField(end_dateTextBox, "End Date")

            Case "physician"
                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")
                Dim titleReq As New RequiredField(Titleddl, "Title")
                Dim specReq As New RequiredField(Specialtyddl, "Specialty")
                Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim faxqReq As New RequiredField(faxtextbox, "Fax")

                pnlProvider.Visible = True


                'pnlAlliedHealth.Visible = False
               ' providerrbl.SelectedValue = "Yes"

            Case "non staff clinician"
                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")
                Dim titleReq As New RequiredField(Titleddl, "Title")
                Dim specReq As New RequiredField(Specialtyddl, "Specialty")
                Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim faxqReq As New RequiredField(faxtextbox, "Fax")

                pnlProvider.Visible = True

                'pnlAlliedHealth.Visible = False
                'providerrbl.SelectedIndex = -1

            Case "resident"
                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")
                Dim titleReq As New RequiredField(Titleddl, "Title")
                Dim specReq As New RequiredField(Specialtyddl, "Specialty")
                Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim faxqReq As New RequiredField(faxtextbox, "Fax")

                pnlProvider.Visible = True

                'pnlAlliedHealth.Visible = False
                'providerrbl.SelectedValue = "Yes"



            Case "vendor"

                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")

                Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim vendorRequired As New RequiredField(VendorNameTextBox, "Vendor")



                'pnlAlliedHealth.Visible = False
                pnlProvider.Visible = False
                'providerrbl.SelectedIndex = -1


            Case "contract"

                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")

                Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")


                ' pnlAlliedHealth.Visible = False
                pnlProvider.Visible = False
                'providerrbl.SelectedIndex = -1

                'Case "allied health"

                '    Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                '    Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")
                '    Dim titleReq As New RequiredField(Titleddl, "Title")
                '    Dim specReq As New RequiredField(Specialtyddl, "Specialty")
                '    Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                '    Dim cityReq As New RequiredField(citytextbox, "City")
                '    Dim stateReq As New RequiredField(statetextbox, "State")
                '    Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                '    Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                '    Dim faxqReq As New RequiredField(faxtextbox, "Fax")
                '    ' Dim AffddlReq As New RequiredField(OtherAffDescddl, "Allied Dg.")

                '    Dim otherAff As New RequiredField(OtherAffDescTextBox, "Other Affiliation Description")

                '    providerrbl.SelectedIndex = 0

                '    pnlAlliedHealth.Visible = True
                '    pnlProvider.Visible = True

                '    pnlOtherAffilliation.Visible = False

                '    providerrbl.SelectedValue = "Yes"



            Case "other"

                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")

                Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")


                'pnlAlliedHealth.Visible = False
                pnlProvider.Visible = False
                'providerrbl.SelectedIndex = -1



            Case Else


                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")


                'pnlAlliedHealth.Visible = False
                pnlProvider.Visible = False
                'providerrbl.SelectedIndex = -1



        End Select


    End Sub
    Protected Sub FillCurrentAccounts()

        Dim thisData As New CommandsSqlAndOleDb("SelAllClientAccountsByNumberV4", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@client_num", iClientNum, SqlDbType.NVarChar)

        Dim iRowCount = 1

        Dim dt As DataTable
        dt = thisData.GetSqlDataTable

        Dim rwHeader As New TableRow

        Dim clApplicationDescHeader As New TableCell
        Dim clLoginNameHeader As New TableCell
        Dim clTermDateHeader As New TableCell
        Dim clCreateDateHeader As New TableCell
        Dim clCreatedByHeader As New TableCell


        clApplicationDescHeader.Text = "Application#"
        clLoginNameHeader.Text = "Account Name"

        clCreatedByHeader.Text = "Created By"
        clCreateDateHeader.Text = "Create Date"

        clTermDateHeader.Text = "Term. Date"


        rwHeader.Cells.Add(clApplicationDescHeader)
        rwHeader.Cells.Add(clLoginNameHeader)

        rwHeader.Cells.Add(clCreatedByHeader)
        rwHeader.Cells.Add(clCreateDateHeader)

        rwHeader.Cells.Add(clTermDateHeader)



        rwHeader.Font.Bold = True
        rwHeader.BackColor = Color.FromName("#006666")
        rwHeader.ForeColor = Color.FromName("White")
        rwHeader.Height = Unit.Pixel(36)

        tblCurrentAccounts.Rows.Add(rwHeader)

        'ApplicationNum
        'ApplicationDesc 
        'login_name 
        'acct_term_date
        'create_date
        'CreatedBy 
        'AppBase

        For Each drRow As DataRow In dt.Rows


            Dim rwData As New TableRow

            Dim clApplicationDesc As New TableCell
            Dim clLoginName As New TableCell

            Dim clCreatedBy As New TableCell
            Dim clCreateDate As New TableCell

            Dim clTermDate As New TableCell



            clApplicationDesc.Text = IIf(IsDBNull(drRow("ApplicationDesc")), "", drRow("ApplicationDesc"))
            clLoginName.Text = IIf(IsDBNull(drRow("login_name")), "", drRow("login_name"))
            clTermDate.Text = IIf(IsDBNull(drRow("acct_term_date")), "", drRow("acct_term_date"))

            clCreateDate.Text = IIf(IsDBNull(drRow("create_date")), "", drRow("create_date"))
            clCreatedBy.Text = IIf(IsDBNull(drRow("CreatedBy")), "", drRow("CreatedBy"))



            rwData.Cells.Add(clApplicationDesc)
            rwData.Cells.Add(clLoginName)

            rwData.Cells.Add(clCreatedBy)
            rwData.Cells.Add(clCreateDate)

            rwData.Cells.Add(clTermDate)



            If iRowCount > 0 Then

                rwData.BackColor = Color.Bisque

            Else

                rwData.BackColor = Color.LightGray

            End If

            tblCurrentAccounts.Rows.Add(rwData)

            iRowCount = iRowCount * -1

        Next
        tblCurrentAccounts.Font.Name = "Arial"
        tblCurrentAccounts.Font.Size = 8

        tblCurrentAccounts.CellPadding = 10
        tblCurrentAccounts.Width = New Unit("100%")

    End Sub
    Protected Sub btnInvalid_Click(sender As Object, e As System.EventArgs) Handles btnInvalid.Click
        FillInvalidCodes()
        If CommentsTextBox.Text <> "" Then
            InvalidDescTextBox.Text = CommentsTextBox.Text

            CommentsTextBox.Text = ""

        End If

        With PanInvalid
            .Visible = True
            With .Style
                .Add("LEFT", "260px")
                .Add("TOP", "190px")
                .Add("POSITION", "absolute")
                .Add("Z-INDEX", "999")
            End With
        End With




    End Sub
    Protected Function fillActionItemsTable(ByRef Actionlist As IList(Of Actions)) As Table

        Dim iRowCount = 1
        Dim dt As New Table
        Dim rwHeader As New TableRow

        Dim clActiondateHeader As New TableCell
        Dim clActionCodeDescheader As New TableCell
        Dim clTechHeader As New TableCell
        Dim clDescHeader As New TableCell


        clActionCodeDescheader.Text = "Action Type"
        clActiondateHeader.Text = "Action Date"
        clTechHeader.Text = "Name"
        clDescHeader.Text = "Description"


        rwHeader.Cells.Add(clActionCodeDescheader)
        rwHeader.Cells.Add(clActiondateHeader)
        rwHeader.Cells.Add(clTechHeader)
        rwHeader.Cells.Add(clDescHeader)



        rwHeader.Font.Bold = True
        rwHeader.BackColor = Color.FromName("#006666")
        rwHeader.ForeColor = Color.FromName("White")
        rwHeader.Height = Unit.Pixel(36)

        dt.Rows.Add(rwHeader)

        For Each AList In Actionlist


            Dim rwData As New TableRow

            Dim clActionDate As New TableCell
            Dim clActionCodeDesc As New TableCell
            Dim clTechName As New TableCell
            Dim clDescription As New TableCell

            clActionCodeDesc.Text = AList.ActionCodeDesc
            clActionDate.Text = AList.ActionDate
            clTechName.Text = AList.TechName
            clDescription.Text = AList.ActionDesc


            rwData.Cells.Add(clActionCodeDesc)
            rwData.Cells.Add(clActionDate)
            rwData.Cells.Add(clTechName)
            rwData.Cells.Add(clDescription)


            If iRowCount > 0 Then

                rwData.BackColor = Color.Bisque

            Else

                rwData.BackColor = Color.LightGray

            End If

            dt.Rows.Add(rwData)

            iRowCount = iRowCount * -1

        Next
        dt.CellPadding = 10
        dt.Width = New Unit("100%")


        Return dt

    End Function
    Private Sub FillInvalidCodes()
        'SelInvalidCodes
        Dim dt As DataTable
        Dim thisData As New CommandsSqlAndOleDb
        thisData = New CommandsSqlAndOleDb("SelInvalidCodes", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@invalidType", "create", SqlDbType.NVarChar, 12)

        dt = thisData.GetSqlDataTable()
        Invalidddl.DataSource = dt
        Invalidddl.DataValueField = "Invalidcd"
        Invalidddl.DataTextField = "InvalidDesc"
        Invalidddl.DataBind()
        thisData = Nothing

    End Sub
    Protected Sub btnSubmitInvalid_Click(sender As Object, e As System.EventArgs) Handles btnSubmitInvalid.Click
        Dim thisData As New CommandsSqlAndOleDb("UpdValidateRequestItemsV4", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int)
        thisData.AddSqlProcParameter("@account_request_seq_num", AccountSeqNum, SqlDbType.Int)
        thisData.AddSqlProcParameter("@nt_login", Session("LoginID"), SqlDbType.NVarChar, 50)
        thisData.AddSqlProcParameter("@validate", "invalid", SqlDbType.NVarChar, 10)
        If Invalidddl.SelectedIndex > 0 Then
            thisData.AddSqlProcParameter("@Invalidcd", Invalidddl.SelectedValue, SqlDbType.NVarChar, 75)
        End If

        thisData.AddSqlProcParameter("@action_desc", InvalidDescTextBox.Text, SqlDbType.NVarChar, 255)


        thisData.ExecNonQueryNoReturn()

        AccRequest = New AccountRequest(AccountRequestNum, Session("EmployeeConn"))

        If Session("environment") = "" Then
            Dim Envoirn As New Environment()
            Session("environment") = Envoirn.getEnvironment
        End If

        If AccRequest.RSAToken = "yes" Then
            sendEmail()
            Response.Redirect("./MyAccountQueue.aspx")
        End If

        If AccRequest.CloseDate <> Nothing Then
            sendEmail()
            'Response.Redirect("./MyAccountQueue.aspx")

        End If

        Response.Redirect("./CreateAccount.aspx?requestnum=" & AccountRequestNum & "&account_seq_num=" & AccountSeqNum, True)

    End Sub
    Protected Sub btnCancelInvalid_Click(sender As Object, e As System.EventArgs) Handles btnCancelInvalid.Click
        PanInvalid.Visible = False
        Invalidddl.SelectedIndex = -1
        InvalidDescTextBox.Text = Nothing

    End Sub

    Protected Sub facility_cdddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles facility_cdddl.SelectedIndexChanged
        Dim ddlBinder As DropDownListBinder


        ddlBinder = New DropDownListBinder(building_cdddl, "sel_building_by_facility", Session("EmployeeConn"))
        ddlBinder.AddDDLCriteria("@facility_cd", facility_cdddl.SelectedValue, SqlDbType.NVarChar)
        ddlBinder.BindData("building_cd", "building_name")

    End Sub

    Protected Sub entity_cdDDL_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles entity_cdDDL.SelectedIndexChanged
        Dim ddbDepartmentBinder As New DropDownListBinder(department_cdddl, "sel_department_codes", Session("EmployeeConn"))
        ddbDepartmentBinder.AddDDLCriteria("@entity_cd", entity_cdDDL.SelectedValue, SqlDbType.NVarChar)
        ddbDepartmentBinder.BindData("department_cd", "department_name")
        department_cdLabel.Text = ""

    End Sub
	Protected Sub btnGotoPMHRequest_Click(sender As Object, e As System.EventArgs) Handles btnGotoPMHRequest.Click
		Response.Redirect("./SignOnForm.aspx?ClientNum=" & iClientnum & "&AccountRequestNum=" & AccountRequestNum)
		'Response.Redirect("./AccountRequestInfo2.aspx?requestnum=" & AccountRequestNum, True)
	End Sub
	Protected Sub btnGoToRequest_Click(sender As Object, e As System.EventArgs) Handles btnGoToRequest.Click

		Select Case AppNum
			Case 249, 250
				Response.Redirect("./SignOnForm.aspx?ClientNum=" & iClientnum & "&AccountRequestNum=" & AccountRequestNum)
			Case Else
				Response.Redirect("./AccountRequestInfo2.aspx?requestnum=" & AccountRequestNum, True)
		End Select


	End Sub

	Protected Sub btnRequest2_Click(sender As Object, e As System.EventArgs) Handles btnRequest2.Click
        Response.Redirect("./AccountRequestInfo2.aspx?requestnum=" & AccountRequestNum, True)

    End Sub

    Protected Sub btnSubmitApprovBy_Click(sender As Object, e As System.EventArgs) Handles btnSubmitApprovBy.Click
        Dim thisData As New CommandsSqlAndOleDb("InsApproveItemBy", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int)
        thisData.AddSqlProcParameter("@ApplicationNum", AppNum, SqlDbType.Int)
        thisData.AddSqlProcParameter("@login_name", Session("LoginID"), SqlDbType.NVarChar, 50)

        thisData.ExecNonQueryNoReturn()

        lblLastApprvedBy.Text = UserInfo.UserName


    End Sub

	Private Sub btnCheckLDAP_Click(sender As Object, e As EventArgs) Handles btnCheckLDAP.Click
		'exec TestSelLDAP @employeeid='32002756'
		Dim empdt As DataTable
		Dim newlogin As String

		Dim thisData As New CommandsSqlAndOleDb("TestSelLDAP", Session("EmployeeConn"))
		thisData.AddSqlProcParameter("@employeeid", siemens_emp_numLabel.Text, SqlDbType.NVarChar)
		thisData.AddSqlProcParameter("@ReturnName", "yes", SqlDbType.NVarChar)
		thisData.AddSqlProcParameter("@AccountRequestnum", AccountRequestNum, SqlDbType.NVarChar)

		empdt = thisData.GetSqlDataTable()

		If empdt.Rows.Count > 0 Then

			newlogin = empdt.Rows(0).Item("LoginName")

			LoginIDTextBox.Text = empdt.Rows(0).Item("LoginName")
			LoginIDTextBox.Enabled = False

			Response.Redirect("./AccountRequestInfo2.aspx?requestnum=" & AccountRequestNum, True)

		Else
			lblRehire.Text = "Account has not been created CHECK back later."
		End If

	End Sub

    Private Sub Approvalrbl_SelectedIndexChanged(sender As Object, e As EventArgs) Handles Approvalrbl.SelectedIndexChanged

        HDApprovalTextBox.Text = Approvalrbl.SelectedValue.ToString
    End Sub

    Private Sub Nominaationrbl_SelectedIndexChanged(sender As Object, e As EventArgs) Handles Nominaationrbl.SelectedIndexChanged
        HDNominationTextBox.Text = Nominaationrbl.SelectedValue.ToString

    End Sub
End Class
