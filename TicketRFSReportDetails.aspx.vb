﻿Imports System.IO
Imports System.Data '
Imports System.Data.SqlClient

Imports App_Code
Imports AjaxControlToolkit
Partial Class TicketRFSReportDetails
    Inherits MyBasePage ' Inherits System.Web.UI.Page
    Dim dsQueues As New DataSet("Queues")
    Dim cblBinder As New CheckBoxListBinder

    'Dim Appinfo As New Applications
    'Dim Apps As New Applications

    Dim BaseApps As New List(Of Application)
    Dim NewBaseApps As New List(Of AllMyApplications)

    Dim AllGroupQueuesInfo As New GroupQueues
    Dim AllMyGroupInfo As New GroupQueues

    Dim dt As DataTable


    'GetAllGroupQueues

    Dim changeGroup As String

    Dim allgrouplist As New List(Of AllGroupQueues)
    'Dim MyGrouplist As New List(Of AllMyGroups)
    Dim GroupOwnership As New List(Of String)


    Dim TicketRFSReportlistD As New TicketsRFSDetailClass()
    Dim conn As New SetDBConnection()
    Dim sAccountType As String
    Dim technum As Integer
    Dim GroupNumber As Integer = 0
    Dim Groups As String = ""




    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        Dim Security As New UserSecurity(Session("LoginID"), Session("EmployeeConn"))
        Dim GroupOwnership As List(Of Integer)
        Dim SGroupOwnership As List(Of String)

        technum = Security.TechNum
        hdtechnumLabel.Text = Security.TechNum.ToString
        HDSecurityLevelLabel.Text = Security.SecurityLevelNum

        GroupOwnership = Security.MySecGroups
        SGroupOwnership = Security.MySecGroupString

        If Request.QueryString("Sortby") <> "" Then


            If Session("SortDirection") = Request.QueryString("Sortby") Then
                HDSortDirection.Text = Request.QueryString("Sortby") & "Desc"
            Else
                HDSortDirection.Text = Request.QueryString("Sortby")

            End If

            Session("SortDirection") = HDSortDirection.Text


        End If



        Dim ddlBinder As New DropDownListBinder

        If Not IsPostBack Then



            ' returns a list of applications User checked to view

            If Session("RPTStartDate") <> "" Then
                HDStartDate.Text = Session("RPTStartDate")
                txtStartDate.Text = Session("RPTStartDate")

            End If

            If Session("RPTEndDate") <> "" Then
                HDEnddate.Text = Session("RPTEndDate")
                txtEndDate.Text = Session("RPTEndDate")

            End If

            If Session("RPTGroups") <> "" Then
                Groups = Session("RPTGroups")
                'check groups selected

            End If

            If Session("RPTStatus") <> "" Then
                hdStatus.Text = Session("RPTStatus")
                'check rbl open or closed 
                rblStatus.SelectedValue = Session("RPTStatus")

            End If

            If Session("RPTType") <> "" Then
                ' Tickets or RFS
                hdType.Text = Session("RPTType")
                rbltype.SelectedValue = Session("RPTType")

            End If

            If Session("RPTPriority") <> "" Then
                ' Priority 1-2-3
                hdPriority.Text = Session("RPTPriority")
                rblP1.SelectedValue = Session("RPTPriority")

            End If

            ' HDDatetype.Text)

            If Session("RPTCategory") <> "" Then
                HDCategory.Text = Session("RPTCategory")
                categorycdddl.SelectedValue = Session("RPTCategory")

            End If

            If Session("RPTPriLevel") <> "" Then
                ' How many group levels
                HDPriorityLevel.Text = Session("RPTPriLevel")
                rblPriority.SelectedValue = Session("RPTPriLevel")



            End If

            If Session("RPTTouch") <> "" Then
                ' Who entered ticket
                HDTouch.Text = Session("RPTTouch")
                rblFirstTouch.SelectedValue = Session("RPTTouch")
            End If

            If Session("RPTDateType") <> "" Then
                ' By close date or open date
                HDDatetype.Text = Session("RPTDateType")
                rblDateType.SelectedValue = Session("RPTDateType")
            End If


            ' this returns all applications only base numbers not individual sites 31
            AllGroupQueuesInfo.GetAllGroupQueues(HttpContext.Current.Session("CSCConn").ToString, "queue", Session("LoginID"))

            'allgrouplist.Clear()

            allgrouplist = AllGroupQueuesInfo.AllGroups

            cblGroupAccountQueues.Dispose()

            cblGroupAccountQueues.DataSource = allgrouplist

            cblGroupAccountQueues.DataValueField = "GroupNum"
            cblGroupAccountQueues.DataTextField = "GroupName"
            cblGroupAccountQueues.DataBind()


            AllMyGroupInfo.GetMyQueues(HttpContext.Current.Session("CSCConn").ToString, technum)


            Dim checkBoxes = From it As ListItem In cblGroupAccountQueues.Items
                 Join own As String In GroupOwnership On it.Value Equals own.ToString
                 Select it
            For Each itm In checkBoxes
                itm.Selected = True
            Next


            If txtStartDate.Text = "" Then
                txtStartDate.Text = DateAdd(DateInterval.Day, -30, Date.Today)
                HDStartDate.Text = DateAdd(DateInterval.Day, -30, Date.Today)
            Else
                HDStartDate.Text = txtStartDate.Text
            End If

            If txtEndDate.Text = "" Then
                txtEndDate.Text = Date.Today
                HDEnddate.Text = Date.Today
            Else
                HDEnddate.Text = txtEndDate.Text
            End If

            If rblStatus.SelectedValue = "" Then
                rblStatus.SelectedValue = "both"
                hdStatus.Text = "both"

            Else
                hdStatus.Text = rblStatus.SelectedValue

            End If

            If rbltype.SelectedValue = "" Then
                rbltype.SelectedValue = "both"
                hdType.Text = "both"


            Else
                hdType.Text = rbltype.SelectedValue

            End If

            ddlBinder = New DropDownListBinder(categorycdddl, "SelCategoryCodes", Session("CSCConn"))
            ddlBinder.AddDDLCriteria("@securitylevel", Session("SecurityLevelNum"), SqlDbType.SmallInt)
            ddlBinder.BindData("Category_cd", "category_desc")


            If Session("ShowAll") = "Show" Then
                For Each ListItem As ListItem In cblGroupAccountQueues.Items
                    ListItem.Selected = True
                Next

                btnShowAll.Text = "UnCheck All"
                lblCurrentView.Text = "Now Showing All Groups from " & txtStartDate.Text & " to " & txtEndDate.Text

                'If Session("ViewRequestType") = "closed" Then
                '    lblCurrentView.Text = "Now Showing All Groups"
                'Else
                '    lblCurrentView.Text = "Now Showing All Your Groups"
                'End If
            Else
                lblCurrentView.Text = "Now Showing Selected Groups from " & txtStartDate.Text & " to " & txtEndDate.Text

                lblCurrentView.Text = " Select criteria and then Submit "


                'If Session("ViewRequestType") = "closed" Then
                '    lblCurrentView.Text = "Now Showing Your Queues with closed Cases"
                'Else
                '    lblCurrentView.Text = "Now Showing Your Queues with Open Cases"
                'End If

            End If

            'TicketRFSReportlist = New TicketsRFS(txtStartDate.Text, txtEndDate.Text, Groups, rblStatus.SelectedValue, rbltype.SelectedValue, HttpContext.Current.Session("CSCConn").ToString)
            'LoadLocalDT()

            GetQueueDataSet()

        Else
            lblCurrentView.Text = ""

            If Session("ShowAll") = "Show" Then
                lblCurrentView.Text = "Now Showing All Groups  from " & txtStartDate.Text & " to " & txtEndDate.Text

            Else
                lblCurrentView.Text = "Now Showing Selected Groups  from " & txtStartDate.Text & " to " & txtEndDate.Text

            End If

        End If





    End Sub

    Public Sub GetQueueDataSet()

        If rblStatus.SelectedValue = "both" Then
            lblCurrentView.Text = lblCurrentView.Text & " Open and Closed "
        Else
            lblCurrentView.Text = lblCurrentView.Text & " Only " & rblStatus.SelectedValue

        End If

        If rbltype.SelectedValue = "both" Then
            lblCurrentView.Text = lblCurrentView.Text & " Tickets and RFS "

        Else
            lblCurrentView.Text = lblCurrentView.Text & " " & rbltype.SelectedValue

        End If

        If rblDateType.SelectedValue = "" Then
            rblDateType.SelectedValue = "entry"
            HDDatetype.Text = "entry"

        ElseIf rblDateType.SelectedValue = "entry" Then
            HDDatetype.Text = "entry"
        ElseIf rblDateType.SelectedValue = "close" Then
            HDDatetype.Text = "close"

        End If

        'AllGroupQueuesInfo.GetAllGroupQueues(HttpContext.Current.Session("CSCConn").ToString)

        'allgrouplist.Clear()

        'allgrouplist = AllGroupQueuesInfo.AllGroups

        LoadRPTSessions()

        TicketRFSReportlistD = New TicketsRFSDetailClass(HDStartDate.Text, HDEnddate.Text, Groups, hdStatus.Text, hdType.Text, HttpContext.Current.Session("CSCConn").ToString, hdPriority.Text, HDCategory.Text, HDPriorityLevel.Text, HDTouch.Text, HDDatetype.Text, HDSortDirection.Text)

        Dim appHeaderCreated As Boolean = False
        'Dim Checkitem As Integer

        appHeaderCreated = False

        ' for each of the 31 base applications 
        'For Each cblItem As ListItem In cblGroupAccountQueues.Items
        '    ' if the app is checked then 
        '    If cblItem.Selected = True Then


        Dim iRowCount As Integer = 1

        'now for each Category that are checked
        'For Each app As AllMyGroups In AllMyGroupInfo.GroupByGroupNumber(cblItem.Value)
        'For Each groupn As AllGroupQueues In AllGroupQueuesInfo.AllGroupByGroupNumber(cblItem.Value)
        '.GroupByGroupNumber(cblItem.Value)
        'AllGroupQueuesInfo.AllGroups AllGroupQueues


        'Checkitem = cblItem.Value

        'If Session("GroupNum") <> "" Then
        '    Checkitem = Session("GroupNum")
        'End If

        For Each ari As TicketsRFSDetailClass.TickectRFSReportDetailItems In TicketRFSReportlistD.GetAllRequests ' GetAllRequestByGroupDetails(cblItem.Value)

            'GetAllRequestByGroupDetails(cblItem.Value) 'app.GroupNum.OrderByDescending(Function(x) x.AccountRequestNum)

            If appHeaderCreated = False Then

                'tblAccountQueues.Rows.Add(rowjeff)

                Dim rwHeader As New TableRow
                Dim rwColTitle As New TableRow

                Dim clHeader As New TableCell
                Dim clReqNumTitle As New TableCell
                Dim clclientNameTitle As New TableCell
                Dim clRequestTypeTitle As New TableCell

                Dim clentrydateTitle As New TableCell
                Dim clclosedateTitle As New TableCell
                Dim clcategoryTitle As New TableCell
                Dim clQueueNameTitle As New TableCell
                Dim clpriorityTitle As New TableCell
                Dim clAcknowedgeTitle As New TableCell
                Dim clTechNameTitle As New TableCell
                Dim clLevelStatusTitle As New TableCell
                Dim clCSCFirstTouchTitle As New TableCell

                rwHeader.Font.Bold = True
                rwHeader.BackColor = Color.FromName("#FFFFCC")
                rwHeader.ForeColor = Color.FromName("Black")
                rwHeader.Font.Size = 8

                clHeader.ColumnSpan = 9

                'If Session("ViewRequestType") = "open" Then
                '    clHeader.Text = ari.GroupName & " Group "

                'ElseIf Session("ViewRequestType") = "closed" Then
                '    clHeader.Text = ari.GroupName & " Closed Cases "
                'Else
                '    clHeader.Text = ari.RequestTypeDesc & " PAST 30 Days Invalid Requests"
                'End If

                clHeader.Text = ari.GroupName

                'rwHeader.Cells.Add(clHeader)

                tblGroupAccountQueues.Rows.Add(rwHeader)


                rwColTitle.Font.Bold = True
                rwColTitle.BackColor = Color.FromName("#FFFFCC")
                rwColTitle.ForeColor = Color.FromName("Black")
                'rwColTitle.Height = Unit.Pixel(36)
                rwColTitle.Font.Size = 8

                clReqNumTitle.Text = "<a href=""TicketRFSReportDetails.aspx?SortBy=request_num"" ><u><b> Ticket/RFS # </b></style></u></a> "
                clclientNameTitle.Text = "<a href=""TicketRFSReportDetails.aspx?SortBy=clientName"" ><u><b> Client Name </b></style></u></a> "
                clRequestTypeTitle.Text = "<a href=""TicketRFSReportDetails.aspx?SortBy=request_type"" ><u><b> Req. Type </b></style></u></a> "
                clentrydateTitle.Text = "<a href=""TicketRFSReportDetails.aspx?SortBy=entry_date"" ><u><b> Entry Date </b></style></u></a> "

                clclosedateTitle.Text = "<a href=""TicketRFSReportDetails.aspx?SortBy=close_date"" ><u><b> Close Date </b></style></u></a> "
                clcategoryTitle.Text = "<a href=""TicketRFSReportDetails.aspx?SortBy=category_cd"" ><u><b> Category </b></style></u></a> "
                clQueueNameTitle.Text = "<a href=""TicketRFSReportDetails.aspx?SortBy=GroupName"" ><u><b> Queue </b></style></u></a> "

                clpriorityTitle.Text = "<a href=""TicketRFSReportDetails.aspx?SortBy=priority"" ><u><b> Priority </b></style></u></a> "
                clTechNameTitle.Text = "<a href=""TicketRFSReportDetails.aspx?SortBy=TechName"" ><u><b> Tech Name </b></style></u></a> "
                clAcknowedgeTitle.Text = "<a href=""TicketRFSReportDetails.aspx?SortBy=TotalAcknowledgeTime"" ><u><b> Total Ack Time </b></style></u></a> "
                clLevelStatusTitle.Text = "<a href=""TicketRFSReportDetails.aspx?SortBy=LevelStatus"" ><u><b> Groups </b></style></u></a> "
                clCSCFirstTouchTitle.Text = "<a href=""TicketRFSReportDetails.aspx?SortBy=CSCFirstTouch"" ><u><b> Entered By </b></style></u></a> "
                'clTotalHHMMSSTitle.Text = "<a href=""TicketRFSReportDetails.aspx?SortBy=TotalHHMMSS"" ><u><b> Total Time </b></style></u></a> "




                clentrydateTitle.Width = Unit.Pixel(55)
                clclosedateTitle.Width = Unit.Pixel(55)

                'cltypeTitle.Text = "Cat. Type"


                rwColTitle.Cells.Add(clReqNumTitle)
                rwColTitle.Cells.Add(clclientNameTitle)
                rwColTitle.Cells.Add(clRequestTypeTitle)
                rwColTitle.Cells.Add(clentrydateTitle)

                rwColTitle.Cells.Add(clclosedateTitle)
                rwColTitle.Cells.Add(clcategoryTitle)
                rwColTitle.Cells.Add(clQueueNameTitle)


                rwColTitle.Cells.Add(clpriorityTitle)
                rwColTitle.Cells.Add(clTechNameTitle)
                rwColTitle.Cells.Add(clAcknowedgeTitle)

                rwColTitle.Cells.Add(clLevelStatusTitle)
                rwColTitle.Cells.Add(clCSCFirstTouchTitle)

                'rwColTitle.Cells.Add(clGroupNameTitle)


                tblGroupAccountQueues.Rows.Add(rwColTitle)

                appHeaderCreated = True
            End If
            '----------------------------------------
            ' End Header
            '----------------------------------------

            'requestnum
            'clientName
            'request_type
            'entrydate

            'closedate
            'category_cd
            'GroupName

            'priority
            'TechName
            'TotalAcknowledgeTime

            'LevelStatus
            'CSCFirstTouch


            Dim rwData As New TableRow
            Dim clReqNum As New TableCell
            Dim clClientName As New TableCell
            Dim clReqType As New TableCell
            Dim clEntryDate As New TableCell

            Dim clCloseDate As New TableCell
            Dim clCategory As New TableCell
            Dim clGroupName As New TableCell

            Dim clpriority As New TableCell
            Dim clTechName As New TableCell
            Dim clAcknowldge As New TableCell

            Dim cllevelStatus As New TableCell
            Dim clFirstTouch As New TableCell

            If Session("ViewRequestType") = "" Then
                Session("ViewRequestType") = "open"
            End If

            'If Session("ViewRequestType") = "open" Then
            '    clReqNum.Text = "<a href=""AccountRequestInfo2.aspx?requestnum=" & ari.HRRequestNum & """ ><u><b> " & ari.HRRequestNum & "</b></u></a>"
            'ElseIf Session("ViewRequestType") = "closed" Then
            '    clReqNum.Text = "<a href=""AccountRequestInfo2.aspx?requestnum=" & ari.HRRequestNum & """ ><u><b> " & ari.HRRequestNum & "</b></u></a>"
            'End If


            clReqNum.Text = "<a href=""ViewRequestTicket.aspx?RequestNum=" & ari.request_num & """ ><u><b> " & ari.request_num & "</b></u></"
            clClientName.Text = ari.clientName
            clReqType.Text = ari.request_type
            clEntryDate.Text = ari.entry_date

            'clReqNum.Text = ari.request_num

            clCloseDate.Text = ari.close_date
            clCategory.Text = ari.category_cd
            clGroupName.Text = ari.GroupName

            'clCatType.Text = ari.type_cd
            clpriority.Text = ari.priority
            clTechName.Text = ari.TechName
            clAcknowldge.Text = ari.TotalAcknowledgeTime


            cllevelStatus.Text = ari.LevelStatus
            clFirstTouch.Text = ari.CSCFirstTouch



            'requestnum
            'clientName
            'request_type
            'entrydate

            'closedate
            'category_cd
            'GroupName

            'priority
            'TechName
            'TotalAcknowledgeTime

            'LevelStatus
            'CSCFirstTouch


            rwData.Cells.Add(clReqNum)
            rwData.Cells.Add(clClientName)
            rwData.Cells.Add(clReqType)
            rwData.Cells.Add(clEntryDate)

            rwData.Cells.Add(clCloseDate)
            rwData.Cells.Add(clCategory)
            rwData.Cells.Add(clGroupName)

            rwData.Cells.Add(clpriority)
            rwData.Cells.Add(clTechName)
            rwData.Cells.Add(clAcknowldge)

            rwData.Cells.Add(cllevelStatus)
            rwData.Cells.Add(clFirstTouch)


            rwData.Font.Size = 7


            If iRowCount > 0 Then

                ' rwData.BackColor = Color.Bisque

                rwData.BackColor = Color.WhiteSmoke

            Else

                rwData.BackColor = Color.LightGray

            End If

            tblGroupAccountQueues.Rows.Add(rwData)


            iRowCount = iRowCount * -1

        Next
        ' Next
        '    End If
        'Next
        tblGroupAccountQueues.Width = New Unit("100%")
        tblGroupAccountQueues.Visible = True



    End Sub
    Protected Sub LoadRPTSessions()

        If HDStartDate.Text <> "" Then
            Session("RPTStartDate") = HDStartDate.Text
        End If

        If HDEnddate.Text <> "" Then
            Session("RPTEndDate") = HDEnddate.Text
        End If

        If Groups <> "" Then
            Session("RPTGroups") = Groups
            'check groups selected

        End If

        If hdStatus.Text <> "" Then
            Session("RPTStatus") = hdStatus.Text
            'check rbl open or closed 
        End If

        If hdType.Text <> "" Then
            ' Tickets or RFS
            Session("RPTType") = hdType.Text


        End If

        If hdPriority.Text <> "" Then
            ' Priority 1-2-3
            Session("RPTPriority") = hdPriority.Text

        End If

        ' HDDatetype.Text)

        If HDCategory.Text <> "" Then
            Session("RPTCategory") = HDCategory.Text

        End If

        If HDPriorityLevel.Text <> "" Then
            ' How many group levels
            Session("RPTPriLevel") = HDPriorityLevel.Text

        End If

        If HDTouch.Text <> "" Then
            ' Who entered ticket
            Session("RPTTouch") = HDTouch.Text

        End If

        If HDDatetype.Text <> "" Then
            ' By close date or open date
            Session("RPTDateType") = HDDatetype.Text
        End If


    End Sub
    Protected Sub btnShowAll_Click(sender As Object, e As System.EventArgs) Handles btnShowAll.Click
        If Session("ShowAll") = "Show" Then
            Session("ShowAll") = ""
        Else
            Session("ShowAll") = "Show"
        End If
        Response.Redirect("~/" & "TicketRFSReportDetails.aspx")
    End Sub
    Protected Sub cblGroupAccountQueues_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cblGroupAccountQueues.SelectedIndexChanged

        ' this returns all applications only base numbers not individual sites 31

        'AllGroupQueuesInfo.GetAllGroupQueues(HttpContext.Current.Session("CSCConn").ToString)

        'allgrouplist.Clear()

        'allgrouplist = AllGroupQueuesInfo.AllGroups


        'TicketRFSReportlist = New TicketsRFS(HDStartDate.Text, HDEnddate.Text, GroupNumber, hdStatus.Text, hdType.Text, HttpContext.Current.Session("CSCConn").ToString)


        'GetQueueDataSet()

    End Sub
    Protected Sub LoadLocalDT()


        'Dim thisData As New CommandsSqlAndOleDb("SelTicketsAndRFSReportV3", HttpContext.Current.Session("CSCConn"))
        'thisData.AddSqlProcParameter("@startDate", HDStartDate.Text, SqlDbType.NVarChar)
        'thisData.AddSqlProcParameter("@endDate", HDEnddate.Text, SqlDbType.NVarChar)
        'thisData.AddSqlProcParameter("@group_num", Groups, SqlDbType.NVarChar)
        'thisData.AddSqlProcParameter("@ticket_rfs", hdStatus.Text, SqlDbType.NVarChar)
        'thisData.AddSqlProcParameter("@close", hdType.Text, SqlDbType.NVarChar)
        'thisData.AddSqlProcParameter("@P1Only", hdPriority.Text, SqlDbType.NVarChar)

        'thisData.AddSqlProcParameter("@category_cd", HDCategory.Text, SqlDbType.NVarChar)
        'thisData.AddSqlProcParameter("@ByLevelType", HDPriorityLevel.Text, SqlDbType.NVarChar)
        'thisData.AddSqlProcParameter("@ByFirstTouch", HDTouch.Text, SqlDbType.NVarChar)
        'thisData.AddSqlProcParameter("@CloseDatetype", HDDatetype.Text, SqlDbType.NVarChar)


        'dt = thisData.GetSqlDataTable

        'GrdTickets.DataSource = thisData.GetSqlDataTable
        'GrdTickets.DataBind()


        'Session("TicketRFSRpt") = dt
        'GrdTickets.Visible = True
        'GrdTickets.SelectedIndex = -1


    End Sub
    Protected Sub btnSubmit_Click(sender As Object, e As System.EventArgs) Handles btnSubmit.Click

        Groups = ""
        hdGroups.Text = ""

        'Dim checkBoxes = From it As ListItem In cblGroupAccountQueues.Items
        'Join own As String In GroupOwnership On it.Value Equals own.ToString
        'Select it
        'For Each itm In checkBoxes
        '    itm.Selected = True
        'Next

        For Each cblItem As ListItem In cblGroupAccountQueues.Items
            If cblItem.Selected = True Then
                Groups = Groups & "," & CType(cblItem.Value, String)
                hdGroups.Text = hdGroups.Text & "," & CType(cblItem.Value, String)

                ' GroupOwnership.Add(cblItem.Value)

            End If

        Next

        lbDateType.Text = ""
        lblCurrentView.Text = ""


        If rblDateType.SelectedValue = "entry" Then
            lbDateType.Text = "Data is by Entry Date"

        Else
            lbDateType.Text = "Data is by Close Date"

        End If


        If Session("ShowAll") = "Show" Then

            lblCurrentView.Text = "Now Showing All Groups from " & txtStartDate.Text & " to " & txtEndDate.Text
        Else
            lblCurrentView.Text = "Now Showing Selected Groups from " & txtStartDate.Text & " to " & txtEndDate.Text

        End If


        AllGroupQueuesInfo.GetAllGroupQueues(HttpContext.Current.Session("CSCConn").ToString, "queue")

        allgrouplist.Clear()

        allgrouplist = AllGroupQueuesInfo.AllGroups

        'TicketRFSReportlist = New TicketsRFS(HDStartDate.Text, HDEnddate.Text, Groups, hdStatus.Text, hdType.Text, HttpContext.Current.Session("CSCConn").ToString)



        GetQueueDataSet()


    End Sub

    Protected Sub rbltype_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles rbltype.SelectedIndexChanged
        hdType.Text = rbltype.SelectedValue
        '' this returns all applications only base numbers not individual sites 31

        'AllGroupQueuesInfo.GetAllGroupQueues(HttpContext.Current.Session("CSCConn").ToString)

        'allgrouplist.Clear()

        'allgrouplist = AllGroupQueuesInfo.AllGroups


        'TicketRFSReportlist = New TicketsRFS(HDStartDate.Text, HDEnddate.Text, GroupNumber, hdStatus.Text, hdType.Text, HttpContext.Current.Session("CSCConn").ToString)


        ''rbltype.SelectedIndex = -1
        'GetQueueDataSet()

    End Sub

    Protected Sub rblPriority_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles rblPriority.SelectedIndexChanged
        HDPriorityLevel.Text = rblPriority.SelectedValue

    End Sub

    Protected Sub rblStatus_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles rblStatus.SelectedIndexChanged

        hdStatus.Text = rblStatus.SelectedValue

        '' this returns all applications only base numbers not individual sites 31

        'AllGroupQueuesInfo.GetAllGroupQueues(HttpContext.Current.Session("CSCConn").ToString)

        'allgrouplist.Clear()

        'allgrouplist = AllGroupQueuesInfo.AllGroups


        'TicketRFSReportlist = New TicketsRFS(HDStartDate.Text, HDEnddate.Text, Groups, hdStatus.Text, hdType.Text, HttpContext.Current.Session("CSCConn").ToString)

        ''rblStatus.SelectedIndex = -1

        'GetQueueDataSet()


    End Sub

    Protected Sub btnReset_Click(sender As Object, e As System.EventArgs) Handles btnReset.Click
        lblCurrentView.Text = ""

        Session("ShowAll") = ""

        Session("RPTStartDate") = ""
        
        Session("RPTEndDate") = ""

        Session("RPTGroups") = ""

        Session("RPTStatus") = ""

        Session("RPTType") = ""

        Session("RPTPriority") = ""

        Session("RPTCategory") = ""
        Session("RPTPriLevel") = ""

        Session("RPTTouch") = ""
        Session("RPTDateType") = ""


        Response.Redirect("~/" & "TicketRFSReportDetails.aspx")

    End Sub

    Protected Sub txtStartDate_TextChanged(sender As Object, e As System.EventArgs) Handles txtStartDate.TextChanged

        HDStartDate.Text = txtStartDate.Text

        'AllGroupQueuesInfo.GetAllGroupQueues(HttpContext.Current.Session("CSCConn").ToString)

        'allgrouplist.Clear()

        'allgrouplist = AllGroupQueuesInfo.AllGroups


        'TicketRFSReportlist = New TicketsRFS(HDStartDate.Text, HDEnddate.Text, Groups, hdStatus.Text, hdType.Text, HttpContext.Current.Session("CSCConn").ToString)

        'GetQueueDataSet()

    End Sub

    Protected Sub txtEndDate_TextChanged(sender As Object, e As System.EventArgs) Handles txtEndDate.TextChanged

        HDEnddate.Text = txtEndDate.Text
        'AllGroupQueuesInfo.GetAllGroupQueues(HttpContext.Current.Session("CSCConn").ToString)

        'allgrouplist.Clear()

        'allgrouplist = AllGroupQueuesInfo.AllGroups


        'TicketRFSReportlist = New TicketsRFS(HDStartDate.Text, HDEnddate.Text, Groups, hdStatus.Text, hdType.Text, HttpContext.Current.Session("CSCConn").ToString)
        'GetQueueDataSet()

    End Sub

    Protected Sub btnExcel_Click(sender As Object, e As System.EventArgs) Handles btnExcel.Click


        Dim dt As DataTable



        Dim thisData As New CommandsSqlAndOleDb("SelTicketsAndRFSReportV3", Session("CSCConn"))
        thisData.AddSqlProcParameter("@startDate", HDStartDate.Text, SqlDbType.NVarChar)
        thisData.AddSqlProcParameter("@endDate", HDEnddate.Text, SqlDbType.NVarChar)
        thisData.AddSqlProcParameter("@group_num", hdGroups.Text, SqlDbType.NVarChar)
        thisData.AddSqlProcParameter("@ticket_rfs", hdStatus.Text, SqlDbType.NVarChar)
        thisData.AddSqlProcParameter("@close", hdType.Text, SqlDbType.NVarChar)
        thisData.AddSqlProcParameter("@P1Only", hdPriority.Text, SqlDbType.NVarChar)

        thisData.AddSqlProcParameter("@category_cd", HDCategory.Text, SqlDbType.NVarChar)
        thisData.AddSqlProcParameter("@ByLevelType", HDPriorityLevel.Text, SqlDbType.NVarChar)
        thisData.AddSqlProcParameter("@ByFirstTouch", HDTouch.Text, SqlDbType.NVarChar)
        thisData.AddSqlProcParameter("@CloseDatetype", HDDatetype.Text, SqlDbType.NVarChar)

        thisData.AddSqlProcParameter("@SortBy", HDSortDirection.Text, SqlDbType.NVarChar)



        dt = thisData.GetSqlDataTable()


        GridView1.DataSource = dt
        GridView1.DataBind()

        GridView1.Visible = True


        CreateExcel2()




    End Sub

    Sub CreateExcel2()

        'Response.Clear()
        'Response.Buffer = True

        Response.AddHeader("content-disposition", "attachment;filename=FileName.xls")

        Response.Charset = String.Empty
        Response.ContentType = "application/vnd.ms-excel"

        'Response.ContentType = "application/vnd.ms-excel"

        'Response.AddHeader("content-disposition", "attachment;filename=FileName.xls")
        'Response.Charset = String.Empty



        Dim sw As System.IO.StringWriter = New System.IO.StringWriter()

        Dim hw As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(sw)

        '===============================Create form to contain grid
        Dim frm As New HtmlForm()

        'GetQueueDataSet()

        'GridView1.Columns.RemoveAt(0)

        GridView1.Parent.Controls.Add(frm)
        frm.Attributes("RunAt") = "server"
        frm.Controls.Add(GridView1)


        'If Mode = "hour" Then

        '    GridHour.Columns.RemoveAt(0)

        '    GridHour.Parent.Controls.Add(frm)
        '    frm.Attributes("RunAt") = "server"
        '    frm.Controls.Add(GridHour)



        'ElseIf Mode = "nurse" Then
        '    If HDNursestation.Text <> "" Then
        '        If hdDisplay.Text <> "" Then
        '            GridDetail.Columns.RemoveAt(0)

        '            GridDetail.Parent.Controls.Add(frm)
        '            frm.Attributes("RunAt") = "server"
        '            frm.Controls.Add(GridDetail)


        '        Else
        '            GridNurse2.Columns.RemoveAt(0)

        '            GridNurse2.Parent.Controls.Add(frm)
        '            frm.Attributes("RunAt") = "server"
        '            frm.Controls.Add(GridNurse2)


        '        End If

        '    Else
        '        GridNurse.Columns.RemoveAt(0)

        '        GridNurse.Parent.Controls.Add(frm)
        '        frm.Attributes("RunAt") = "server"
        '        frm.Controls.Add(GridNurse)

        '    End If

        'Else
        '    GridHour.Columns.RemoveAt(0)

        '    GridHour.Parent.Controls.Add(frm)
        '    frm.Attributes("RunAt") = "server"
        '    frm.Controls.Add(GridHour)


        'End If


        frm.RenderControl(hw)
        Response.Write(sw.ToString())

        GridView1.Visible = False

        Response.End()


        'If Mode = "hour" Then

        '    GridHour.Visible = True
        '    GridNurse.Visible = False

        '    GridHour.DataSource = dt
        '    GridHour.DataBind()


        'ElseIf Mode = "nurse" Then
        '    If HDNursestation.Text <> "" Then
        '        If hdDisplay.Text <> "" Then
        '            GridDetail.Font.Size = 8

        '            GridDetail.Visible = True
        '            GridDetail.DataSource = dt
        '            GridDetail.DataBind()
        '            GridNurse2.Visible = False
        '        Else
        '            GridNurse2.Visible = True
        '            GridNurse2.DataSource = dt
        '            GridNurse2.DataBind()

        '            GridDetail.Visible = False
        '        End If

        '        GridNurse.Visible = False
        '    Else
        '        GridNurse2.Visible = False
        '        GridDetail.Visible = False

        '        GridNurse.Visible = True
        '        GridNurse.DataSource = dt
        '        GridNurse.DataBind()

        '    End If
        '    GridHour.Visible = False


        'Else
        '    GridHour.Visible = True
        '    GridNurse.Visible = False

        '    GridHour.DataSource = dt
        '    GridHour.DataBind()



        'End If


    End Sub
    Protected Sub btnDashboard_Click(sender As Object, e As System.EventArgs) Handles btnDashboard.Click
        Response.Redirect("~/TicketDashboard.aspx", True)

    End Sub

    Protected Sub rblDateType_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles rblDateType.SelectedIndexChanged
        HDDatetype.Text = rblDateType.SelectedValue

    End Sub

    Protected Sub categorycdddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles categorycdddl.SelectedIndexChanged
        HDCategory.Text = categorycdddl.SelectedValue
    End Sub

    Protected Sub rblFirstTouch_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles rblFirstTouch.SelectedIndexChanged
        HDTouch.Text = rblFirstTouch.SelectedValue
    End Sub

    Protected Sub rblP1_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles rblP1.SelectedIndexChanged
        hdPriority.Text = rblP1.SelectedValue
    End Sub


End Class

