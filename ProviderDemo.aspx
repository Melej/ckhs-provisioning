﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="false" CodeFile="ProviderDemo.aspx.vb" Inherits="ProviderDemo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">

<style type="text/css">
    .style13
    {
        height: 30px;
    }
    .style15
    {
        font-size: larger;
    }
    
    .style23
    {
        width: 200px;
    }
    .style25
    {
        width: 100%;
    }
    .textHasFocus
    {
         border-color:Red;
        border-width:medium;
        border-style:outset;
    }
   
    .style26
    {
        width: 91px;
    }
    .style27
    {
        width: 110px;
    }
   
    .style28
    {
        width: 80px;
    }
    .style29
    {
        width: 94px;
    }
   
    .style30
    {
        width: 85px;
    }
   
    .style31
    {
        width: 192px;
    }
    .style32
    {
        width: 206px;
    }
   
</style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
 <asp:UpdatePanel ID="upProviderDemo" runat="server"  UpdateMode="Conditional">
   <ContentTemplate>
      <table border="3" style="width: 100%">
        <tr>
                <td align="left">
                           <asp:Label ID = "lblValidation" runat = "server" ForeColor="Red"/>
                 </td>
              </tr>
                    <tr>
                                <td class="tableRowHeader">
                                Client Demographic Request Change Form
                                </td>
                    </tr>
                     <asp:Panel runat="server" ID="pnlSubmission" Visible="False">

                        <tr>
          
                            <td class="tableRowSubHeader"  align="left">
                            Form Submission
                            </td>
            
                        </tr>
                        <tr>
                           <td align="center">
                             <table>
                                <tr>
                                
                                    <td>
                                        <asp:Button ID = "btnSubmitDemoChange" runat = "server" Text ="Submit Demographic Request " Width="200px" 
                                            BackColor="#336666" ForeColor="White" />
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                        <asp:Button ID = "btnCancelDemoChange" runat = "server" Text ="Cancel Request " Width="200px" 
                                            BackColor="#336666" ForeColor="White" />

                                    </td>
                                    <td>

                                    </td>
                                </tr>
                             </table>
                            </td>

                        </tr>
                        </asp:Panel>

                        <asp:Panel runat="server" ID="pnlProvider" Visible="False">
                        <tr>
          
                            <td class="tableRowSubHeader"  align="left">
                            Form Approval
                            </td>
            
                        </tr>
                        <tr>
                           <td align="center">
                             <table>
                                <tr>
                                
                                    <td>
                                        <asp:Button ID = "BtnComitDemoChanges" runat = "server" Text ="Apply Demographic Changes " Width="220px" 
                                            BackColor="#336666" ForeColor="White" />
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                        <asp:Button ID = "BtnDeniDemoChanges" runat = "server" Text ="Cancel Demographic Changes" Width="220px" 
                                            BackColor="#336666" ForeColor="White" />

                                    </td>
                                    <td>
                                        <asp:Button ID = "btnReturntoDemo" runat = "server" Text ="Return to Client Demographic " Width="220px" 
                                            BackColor="#336666" ForeColor="White" />

                                    </td>
                                </tr>
                             </table>
                            </td>
                        </tr>
                        <tr>
                             <td>
                                <table>
                                   <tr>
                                        <td>
                                            <asp:Label ID="lblSubmitter" Text="Submitter:" runat="server" Font-Bold="True" Font-Size="Medium">
                                            </asp:Label>
                                       
                                        </td>                            
                                        <td>
                                            <asp:Label ID="SubmitterNameLabel" runat="server"></asp:Label>                                        
                                        
                                        </td>
                                    </tr>                
                                </table>
                             </td>
                        </tr>
                        </asp:Panel>
                      <tr>
                       <td>

                          <table id="tbldemo" border="3px" width="100%">
                            <tr>
                                <td colspan="4" class="tableRowSubHeader"  align="left">
                                    Demographics
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Emplbl" runat="server" Text="Employee Number:" ></asp:Label>
                                </td>

                                <td>
                                <asp:Label ID ="siemensempnumLabel" runat="server"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="netlbl" runat="server" Text="Network Logon:" >
                                    </asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID = "loginnameTextBox" runat="server" Enabled="false"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <asp:Label ID="lblcurrname" runat="server" Text="Current Name" Font-Bold="True" Font-Size="Large"></asp:Label>
                                </td>
                            </tr>
                            <tr>

                                <td colspan="4" >
                                    <table class="style3" > 
                                      <tr>
                                        <td>
                                            First Name:
                                        </td>
                                        <td align="left"  >
                                            <asp:TextBox width="280px" ID = "FirstNameTextBox" runat="server" Enabled="false" ></asp:TextBox>
                                        </td>
                                        <td>
                                            MI:
                                        </td>
                                        <td>
                                            <asp:TextBox ID = "miTextBox" runat="server" Width="15px" Enabled="false"></asp:TextBox>
                                        </td>
                                        <td align="left" >
                                            Last Name:
                                        </td>

                                        <td align="left" >
                                            <asp:TextBox ID = "lastnameTextBox" width="280px" runat="server" Enabled="false"></asp:TextBox>
                                        </td>
                                      </tr>
                                     </table>
                                </td>
                            </tr>
                             <tr>
                                <td colspan="4">
                                     <asp:Label ID="lblNewName" runat="server" Text="New Name" Font-Bold="True" Font-Size="Large"></asp:Label>

                                </td>
                            </tr>

                            <tr>
                                <td colspan="4">
                                
                                    <table class="style3" > 
                                      <tr>
                                        <td>
                                            New First Name:
                                        </td>
                                        <td align="left"  >
                                            <asp:TextBox width="280px" ID = "NewFirstNameTextBox" runat="server" ></asp:TextBox>
                                        </td>
                                        <td>
                                             New MI:
                                        </td>
                                        <td>
                                            <asp:TextBox ID = "NewMITextBox" runat="server" Width="15px"></asp:TextBox>
                                        </td>
                                        <td align="left" >
                                            New Last Name:
                                        </td>

                                        <td align="left" >
                                            <asp:TextBox ID = "NewLastNameTextBox" width="280px" runat="server"></asp:TextBox>
                                        </td>
                                      </tr>
                                     </table>                                
                                
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                   <asp:Label ID="lblCurraddress" runat="server" Text="Current Address" Font-Bold="True" Font-Size="Large"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td  align="right">
                                    Address 1:
                                    </td>
                                <td align="left"> 
                                        <asp:TextBox ID = "address1textbox" runat="server" Width="250px" Enabled="false"></asp:TextBox>
                                    </td>
                                <td align="right" >
                                    Address 2:
                                    </td>
                                <td align="left" >
                                    <asp:TextBox ID = "address2textbox" runat="server" Width="250px" Enabled="false"></asp:TextBox>
                                    </td>

                            </tr>
                            <tr>
                                <td align="right">
                                    <asp:Label id="lblcity" runat="server" width="50px"  text="City:" />
                                </td>
                                <td align="left" colspan="3">
                                    <asp:TextBox ID = "citytextbox" runat="server" width="500px" Enabled="false"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <asp:Label ID="lblstate" runat="server" Text="State:" Width="50px" />
                                </td>
                                <td align="left">
                                    <asp:TextBox ID = "statetextbox" runat="server" Width="50px" Enabled="false"></asp:TextBox>
                                    </td>
                                <td align="right" >

                                    <asp:Label ID="lblzip" runat="server" Text="Zip:" Width="25px" />
                                </td>
                                <td align="left">
                                    <asp:TextBox ID = "ziptextbox" runat="server" Width="50px" Enabled="false"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="right"  >
                                Phone Number:
                                </td>
                                <td align="left" >
                                <asp:TextBox ID = "phoneTextBox" runat="server" Enabled="false"></asp:TextBox>
                                </td>
       
                                <td align="right" >
                                Pager Number:
                                </td>
                                <td align="left" >
                                <asp:TextBox ID = "pagerTextBox" runat="server" Enabled="false"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" >
                                Fax:
                                </td>
                                <td align="left" >
            
                                    <asp:TextBox ID = "faxtextbox" runat="server" Enabled="false"></asp:TextBox>
                                </td>
         
                                <td align="right" >
                                Email:
                                </td>
                                <td align="left" >
                                <asp:TextBox ID = "emailTextBox" runat="server" Width="250px" Enabled="false"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                   <asp:Label ID="lblnewaddress" runat="server" Text="New Address" Font-Bold="True" Font-Size="Large"></asp:Label>

                                </td>
                            </tr>
                            <tr>
                                <td  align="right">
                                    Address 1:
                                    </td>
                                <td align="left"> 
                                        <asp:TextBox ID = "Newaddress1TextBox" runat="server" Width="250px"></asp:TextBox>
                                    </td>
                                <td align="right" >
                                    Address 2:
                                    </td>
                                <td align="left" >
                                    <asp:TextBox ID = "Newaddress2TextBox" runat="server" Width="250px"></asp:TextBox>
                                    </td>

                            </tr>
                            <tr>
                                <td align="right">
                                    <asp:Label id="CityLabel2" runat="server" width="50px"  text="City:" />
                                </td>
                                <td align="left" colspan="3">
                                    <asp:TextBox ID = "NewCityTextBox" runat="server" width="500px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <asp:Label ID="StateLabel2" runat="server" Text="State:" Width="50px" />
                                </td>
                                <td align="left">
                                    <asp:TextBox ID = "NewStateTextBox" runat="server" Width="50px"></asp:TextBox>
                                    </td>
                                <td align="right" >

                                    <asp:Label ID="lblNewZip" runat="server" Text="Zip:" Width="25px" />
                                </td>
                                <td align="left">
                                    <asp:TextBox ID = "NewZipTextBox" runat="server" Width="50px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="right"  >
                                Phone Number:
                                </td>
                                <td align="left" >
                                <asp:TextBox ID = "NewPhoneTextBox" runat="server"></asp:TextBox>
                                </td>
       
                                <td align="right" >
                                Pager Number:
                                </td>
                                <td align="left" >
                                <asp:TextBox ID = "NewPagerTextBox" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" >
                                Fax:
                                </td>
                                <td align="left" >
            
                                    <asp:TextBox ID = "NewFaxTextBox" runat="server"></asp:TextBox>
                                </td>
                                <td>
                                    Account Req Num:
                                    <asp:Label id="AccountRequestNumLabel" runat="server" Visible="true"></asp:Label>
                                </td>
                                <td>
                                
                                </td>

                            </tr>
                            <tr>
                                <td>
                                
                                </td>
                                <td>
                                    <asp:Label ID="close_dateLabel" runat="server" Visible="false"></asp:Label>
                        
                                </td>
                                <td>
                                
                                    <asp:Label id="approvalLabel" runat="server" Visible="false"></asp:Label>
                                </td>
                            
                                <td>
                                    <asp:Label ID="ApprovalClientNumLabel" runat="server" Visible="false"></asp:Label>
                                    <asp:Label ID="ClientNumLabel" runat="server" Visible="false"></asp:Label>
                                    <asp:Label ID="RoleNumLabel" runat="server" Visible="false"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                            
                                <td>
                                    <asp:Label ID ="ApprovalNameLabel" runat="server" Visible="false"></asp:Label>
                                    <asp:Label ID ="CurrentFirstNameLabel" runat="server" Visible="false"></asp:Label>
                                    <asp:Label ID ="CurrentLastNameLabel" runat="server" Visible="false"></asp:Label>
                                    <asp:Label ID ="CurrentMILabel" runat="server" Visible="false"></asp:Label>
                                    <asp:Label ID ="CurrentAddress1Label" runat="server" Visible="false"></asp:Label>
                                    <asp:Label ID ="CurrentAddress2Label" runat="server" Visible="false"></asp:Label>
                                    <asp:Label ID ="CurrentCityLabel" runat="server" Visible="false"></asp:Label>

                                </td>
                                <td>
                                    <asp:Label ID ="CurrentStateLabel" runat="server" Visible="false"></asp:Label>
                                    <asp:Label ID ="CurrentZipLabel" runat="server" Visible="false"></asp:Label>
                                    <asp:Label ID ="CurrentPhoneLabel" runat="server" Visible="false"></asp:Label>
                                    <asp:Label ID ="CurrentFaxLabel" runat="server" Visible="false"></asp:Label>

                                </td>
                            </tr>

                    </table>
          </td>
        </tr>
     </table>
   </ContentTemplate>                         
  </asp:UpdatePanel>
</asp:Content>

