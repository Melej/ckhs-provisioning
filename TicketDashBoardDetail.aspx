﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="false" CodeFile="TicketDashBoardDetail.aspx.vb" Inherits="TicketDashBoardDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <style type="text/css">
        .style34
        {
            width: 189px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
  <div id="maindiv" runat="server" align="left">
  <asp:UpdatePanel runat="server" UpdateMode ="Conditional" ID="uplTicket">
   <ContentTemplate>
    <table width="100%">
      <tr>
        <td valign = "top" align="center" colspan="4">
           View Tickets Between 
           <asp:TextBox runat="server" ID = "txtBeginDate" CssClass="calenderClass"></asp:TextBox> 
           and
            <asp:TextBox runat="server" ID = "txtEndDate" CssClass="calenderClass"></asp:TextBox>
        </td>
      </tr>
              
        <tr>
            <td colspan="4" align="center">
                 Status:
                <asp:RadioButtonList ID = "Statusrbl" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="both" Selected="True">Open & Closed Tickets and RFS</asp:ListItem>
                        
                    <asp:ListItem Value="open">Open Tickets and RFS</asp:ListItem>

                    <asp:ListItem Value="closed">Closed Tickets and RFS</asp:ListItem>
                </asp:RadioButtonList>

            </td>
        </tr>
        <tr align="center">

           <td align="center" colspan="4" >
                Category:
                <asp:DropDownList ID="categorycdddl" runat="server" AutoPostBack="true" >
                </asp:DropDownList>
            </td>
        </tr>
        <tr>

            <td  align="center" colspan="4">
                Type:
                <asp:DropDownList ID="typecdddl" runat="server" AutoPostBack="true">
                </asp:DropDownList>
            </td> 

       </tr>
       <tr>
          <td  align="center" colspan="4">
            <table>
                <tr align="center">
                    <td align="center">

                            <asp:Button ID ="btnSubmit" runat = "server" Text ="Submit"  Width="130px" Height="30px"
                                                                CssClass="btnhov" BackColor="#006666" />
        
                    </td>
                    <td>
                            <asp:Button ID="btnReport" runat="server" Text ="Ticket Report" height="30px" Width="130px" />

                    </td>
                    <td align="center" >

                            <asp:Button ID = "btnReset" runat= "server" Text="Ticket DashBoard"  Width="130px" Height="30px"
                                    CssClass="btnhov" BackColor="#006666"  />

                    </td>
<%--                    <td>
                        <asp:Button ID="btnExcel" runat="server" Text="Excel" Width="120px" Height="30px" 
                                    CssClass="btnhov" BackColor="#006666" />

                   </td>
--%>
                </tr>
            </table>
          </td>
         </tr> 
         <tr>
            <td colspan ="4">
             <asp:GridView ID="gvTicketDetail" runat="server" AllowSorting="True"  AutoGenerateColumns="False" BackColor="LightGray" BorderColor="#336666"
                  BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                  EmptyDataText="No Items Found" EnableTheming="False" DataKeyNames="Request"
                  Font-Size="small" GridLines="Horizontal" Width="100%" AlternatingRowStyle-BackColor="Bisque" >
                                   
                <FooterStyle BackColor="White" ForeColor="#333333" />
                <HeaderStyle BackColor="#336666" Font-Bold="False" ForeColor="White" Font-Size="Small" HorizontalAlign="Left"    />                                    
                <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Left" Font-Size="Medium"/>
                <RowStyle BackColor="LightGray" ForeColor="#333333" />
                <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#F7F7F7" />
                <SortedAscendingHeaderStyle BackColor="#487575" />
                <SortedDescendingCellStyle BackColor="#E5E5E5" />
                <SortedDescendingHeaderStyle BackColor="#275353" />
                <Columns>
                     <asp:TemplateField HeaderText="Request"  SortExpression="Request" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>

                        <asp:HyperLink ID="HyperLink1" runat="server" 

                            NavigateUrl='<% #Eval("Request", "~/ViewRequestTicket.aspx?RequestNum={0}") %>'

                            Text='<% #Eval("Request") %>'>


                        </asp:HyperLink>
                       
                        </ItemTemplate>
                        

                    </asp:TemplateField>
<%--                    <asp:CommandField ButtonType="Button" SelectText="Select" ShowSelectButton= "true" ControlStyle-BackColor="#84A3A3" ItemStyle-Height="30px" />
--%>
                    <asp:BoundField  DataField="FullName"  HeaderText="Full Name" SortExpression="fullname" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" ItemStyle-Height="35px" ></asp:BoundField>
   
                    <asp:BoundField  DataField="category_cd"  HeaderText="Category" SortExpression="category_cd" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
				    <asp:BoundField  DataField="type_cd"  HeaderText="Type " SortExpression="Type" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>

                    <asp:BoundField  DataField="entry_date"  HeaderText="Entry Date" SortExpression="entry_date" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
				    <asp:BoundField  DataField="TechName"  HeaderText="TechName" SortExpression="TechName" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>

				    <asp:BoundField  DataField="CurrentStatus"  HeaderText="Status" SortExpression="Status" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
                

                </Columns>
            </asp:GridView>
          </td>
        </tr>


         <tr>
         
            <td colspan="4">
 
                <asp:Table  Width="100%" ID = "tblAccounts" runat = "server" CellPadding="0" CellSpacing="0">
                </asp:Table>
            </td>
         
         </tr>
        <tr>
            <td colspan="4">
            
                <asp:TextBox ID="HDStartDate" runat="server" Visible="false" ></asp:TextBox>
                <asp:TextBox ID="HDEnddate" runat="server" Visible="false" ></asp:TextBox>
                <asp:TextBox ID="HDCategory" runat="server" Visible="false" ></asp:TextBox>
                <asp:TextBox ID="HDStatus" runat="server" Visible="false" ></asp:TextBox>
                <asp:TextBox ID="HDType" runat="server" Visible="false" ></asp:TextBox>

                 
            </td>
        </tr>
         <tr>
                <td colspan="4">
                            <asp:GridView ID="GridAccounts" runat="server" AllowSorting="True" 
                           AutoGenerateColumns="False" BackColor="White" BorderColor="#336666" 
                           BorderStyle="Double" BorderWidth="3px" CellPadding="1" 
                           DataKeyNames="client_num"  Visible="false"
                           EmptyDataText="No Accounts found " 
                           EnableTheming="False" Font-Size="Medium" GridLines="Horizontal" Width="100%">
                           <FooterStyle BackColor="White" ForeColor="#333333" />
                           <HeaderStyle BackColor="#336666" Font-Bold="False" Font-Size="X-Small" 
                               ForeColor="White" HorizontalAlign="Left" VerticalAlign="Middle" />
                           <PagerStyle BackColor="#336666" Font-Size="Medium" ForeColor="White" 
                               HorizontalAlign="Left" />
                           <RowStyle BackColor="White" ForeColor="#333333" Font-Size="X-Small" />
                           <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                           <SortedAscendingCellStyle BackColor="#F7F7F7" />
                           <SortedAscendingHeaderStyle BackColor="#487575" />
                           <SortedDescendingCellStyle BackColor="#E5E5E5" />
                           <SortedDescendingHeaderStyle BackColor="#275353" />
                           <Columns>

                               <asp:CommandField ButtonType="Button" ControlStyle-BackColor="#84A3A3" 
                                      HeaderStyle-HorizontalAlign="Left" ItemStyle-Height="20px" SelectText="Select" 
                                      ShowSelectButton="true">
                                  <ControlStyle BackColor="#84A3A3" />
                                  <HeaderStyle HorizontalAlign="Left" />
                                  <ItemStyle Height="20px" />
                                  </asp:CommandField>
                            
                            	  


                               <asp:BoundField DataField="fullname"  ItemStyle-HorizontalAlign="Left" HeaderText="User Name" />

                               <asp:BoundField DataField="LoginName" ItemStyle-HorizontalAlign="Center" HeaderText="Network Login" />

                               <asp:BoundField DataField="AppLoginName" ItemStyle-HorizontalAlign="Center" HeaderText="App Login " />

                               <asp:BoundField DataField="StartDate" ItemStyle-HorizontalAlign="Center" HeaderText="Start Date" />

                               <asp:BoundField DataField="siemens_emp_num"  ItemStyle-HorizontalAlign="Center" HeaderText="Emp # " />
                               <asp:BoundField DataField="emp_type_cd"  ItemStyle-HorizontalAlign="Center" HeaderText="Emp Type " />

                           </Columns>
                       </asp:GridView>                            
                </td>
          </tr>

    </table>
    </ContentTemplate>
  </asp:UpdatePanel>
  </div>

     

</asp:Content>

