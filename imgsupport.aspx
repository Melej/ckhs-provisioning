﻿<%@ Page Language="VB" StylesheetTheme="" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
    Sub Page_Load(ByVal S As Object, ByVal E As EventArgs)
        If Not IsPostBack Then
            ' Change this to your image
            Img.Text = "<img src=""http://www.kaj-isis.net/images/layout/logo2.png"" alt=""Alt text"" title=""ToolTip"" />"
            ' Set some default text
            MainDesc.Text = "<P>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse pulvinar nulla vel enim tempus bibendum. Integer dignissim justo vel est congue bibendum. Sed lorem nisl, cursus sit amet dictum et, scelerisque sed sapien. Aenean non elit quam. Nullam vel condimentum leo. In tempus imperdiet mattis. Mauris nec est elit. Vestibulum ut sem ut leo facilisis gravida.</P>" & _
                "<P>Etiam tristique cursus ipsum eu adipiscing. Curabitur fringilla erat ut augue facilisis fermentum. Mauris venenatis pretium libero quis dictum. Duis tempus adipiscing cursus. Morbi ac pulvinar nulla. Fusce sed lorem tellus, ac imperdiet velit. Nulla facilisi. Fusce quis ipsum non magna fringilla porta. Nam dapibus congue mi in tincidunt. Donec nunc tellus, egestas vitae sollicitudin in, bibendum sit amet purus. In leo purus, congue rutrum luctus sit amet, tincidunt vitae velit.</P>"
        End If
    End Sub
    
    Sub Submit_Click(ByVal S As Object, ByVal E As EventArgs) Handles Submit.Click
        If MainDesc.Text IsNot String.Empty Then
            Dim pattern, mDesc As String
            Dim myDelegate As MatchEvaluator, RegX As Regex
            
            ' The image will get escaped into text, rework it back into an image
            pattern = "\&lt\;IMG( style=""(?<style>([^""]+))"")*( title=""(?<title>([^""]+))"")*( alt=""(?<alt>([^""]+))"")* src=""(?<src>([^""]+))""( )*\&gt\;"
            
            myDelegate = New MatchEvaluator(AddressOf ImgTagHandlerInput)
            RegX = New Regex(pattern, RegexOptions.Multiline Or RegexOptions.IgnoreCase)
            mDesc = RegX.Replace(MainDesc.Text, myDelegate)
            
            pnlOutput.InnerHtml = mDesc
            MainDesc.Text = mDesc
        End If
    End Sub
    
    Private Function ImgTagHandlerInput(ByVal m As Match) As String
        Dim Output As String
        Dim myDomain As String = HttpContext.Current.Request.Url.Host

        Output = "<img"

        If m.Groups("style").ToString() IsNot String.Empty Then
            Output &= " style=""" & m.Groups("style").ToString & """"
        End If

        If m.Groups("title").ToString() IsNot String.Empty Then
            Output &= " title=""" & m.Groups("title").ToString & """"
        End If

        If m.Groups("alt").ToString() IsNot String.Empty Then
            Output &= " alt=""" & m.Groups("alt").ToString & """"
        End If

        ' Uncomment this line if you wish to change the URL from an absoulte one (i.e. "http://www.domain.com/image.gif") to a relative one (i.e. "/image.gif")
        'If m.Groups("src").ToString().Contains(myDomain) Then
        '    Output &= " src=""" & m.Groups("src").ToString().Replace("http://", "").Replace("https://", "").Replace(myDomain, "") & """"
        'Else
        Output &= " src=""" & m.Groups("src").ToString() & """"
        'End If

        Output &= ">"

        Return Output
    End Function
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body style="margin:20px;">
    <form id="aspnetForm" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="Sm" runat="server" EnablePartialRendering="true" />
    <div>
        <h1>HTMLEditorExtender Image Support</h1>

        <table cellspacing="2" cellpadding="2" width="100%">
            <tr>
                <td style="vertical-align:top; width:50%">
                    <h2>Input</h2>
                    <p>Right-click on the image, select Copy, then right-click inside the text box below and click Paste:</p>
                    <p><asp:Label ID="Img" runat="server" /></p> 
                    <asp:TextBox ID="MainDesc" runat="server"
                        ToolTip="Enter the main description (formatting and inline objects apply here)"
                        TextMode="MultiLine" Width="490" Height="400"></asp:TextBox>
                    <p><asp:Button ID="Submit" runat="server" Text="Submit" Width="120" Height="30" /></p>

                    <ajaxToolkit:HtmlEditorExtender ID="Html_MainDesc" runat="server"
                        TargetControlID="MainDesc">
                    </ajaxToolkit:HtmlEditorExtender>
                </td>
                <td style="vertical-align:top; width:50%">
                    <h2>Output</h2>
                    <div id="pnlOutput" runat="server" style="border:solid 1px Silver; padding:5px;"></div>
                </td>
            </tr>
        </table>
        
        
    </div>
    </form>
</body>
</html>
