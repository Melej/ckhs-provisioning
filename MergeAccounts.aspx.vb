﻿Imports App_Code
Partial Class MergeAccounts
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

    End Sub
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click

        Dim thisData As New CommandsSqlAndOleDb("SelSimpleSearchV2", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@FirstName", FirstNameTextBox.Text, SqlDbType.NVarChar, 20)
        thisData.AddSqlProcParameter("@LastName", LastNameTextBox.Text, SqlDbType.NVarChar, 20)

        gvSearch.DataSource = thisData.GetSqlDataTable
        gvSearch.DataBind()

        uplSearchClient.Update()
        mpeSearchClient.Show()

    End Sub
    Protected Sub btnSearchFirstAccount_Click(sender As Object, e As System.EventArgs) Handles btnSearchFirstAccount.Click
        ViewState("sWhichAccount") = "First"
        mpeSearchClient.Show()
    End Sub
    Protected Sub btnSearchSecondAccount_Click(sender As Object, e As System.EventArgs) Handles btnSearchSecondAccount.Click

        ViewState("sWhichAccount") = "Second"
        mpeSearchClient.Show()
    End Sub
    Protected Sub gvSearch_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvSearch.RowCommand
        If e.CommandName = "Select" Then
            Dim client_num As String = gvSearch.DataKeys(Convert.ToInt32(e.CommandArgument))("client_num").ToString()


            If ViewState("sWhichAccount") = "First" Then



                ViewState("client3First") = client_num




                ViewState("sWhichAccount") = ""

            End If
            If ViewState("sWhichAccount") = "Second" Then



                ViewState("client3Second") = client_num

                ViewState("sWhichAccount") = ""
            End If


            mpeSearchClient.Hide()
            FillAccountInfo()

            uplAccounts.Update()

        End If
    End Sub

    Protected Sub FillAccountInfo()
        rwCurrentAccounts.Visible = True
        rwCurrentAccountsHeader.Visible = True
        rwMerge.Visible = True


        If ViewState("client3First") IsNot Nothing Then

            Dim Client3Contrl As New Client3Controller()
            Dim Client3 As Client3 = Client3Contrl.GetClientByClientNum(ViewState("client3First"))

            txt1stFirstName.Text = Client3.FirstName
            txt1stLastName.Text = Client3.LastName
            txt1stLoginName.Text = Client3.LoginName
            txt1stDepartmentCd.Text = Client3.DepartmentCd
            txt1stEntityCd.Text = Client3.EntityCd
            txt1stSiemensEmpNum.Text = Client3.SiemensEmpNum

            DisplayCurrentAccounts(ViewState("client3First"), gvFirstAccounts)

            If Client3.LoginName IsNot Nothing Then

                DisplayADInfo(Client3.LoginName, lbl1stLoginInfo)


            End If


        End If
        If ViewState("client3Second") IsNot Nothing Then


            Dim Client3Contr As New Client3Controller()
            Dim Client3 As Client3 = Client3Contr.GetClientByClientNum(ViewState("client3Second"))

            txt2ndFirstName.Text = Client3.FirstName
            txt2ndLastName.Text = Client3.LastName
            txt2ndLoginName.Text = Client3.LoginName
            txt2ndDepartmentCd.Text = Client3.DepartmentCd
            txt2ndEntityCd.Text = Client3.EntityCd
            txt2ndSiemensEmpNum.Text = Client3.SiemensEmpNum

            DisplayCurrentAccounts(ViewState("client3Second"), gvSecondAccounts)

            If Client3.LoginName IsNot Nothing Then

                DisplayADInfo(Client3.LoginName, lbl2ndLoginInfo)


            End If

        End If



    End Sub
    Protected Sub btnCloseOnBelafOf_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCloseSearchClient.Click

        mpeSearchClient.Hide()

    End Sub

    Protected Sub DisplayCurrentAccounts(ByVal iClientNum As Integer, ByRef gv As GridView)
        Dim ClientAccounts As New ClientAccounts(iClientNum)

        gv.DataSource = ClientAccounts.ClientAccounts
        gv.DataBind()



    End Sub

    Protected Sub DisplayADInfo(ByVal sLoginName As String, ByRef wc As WebControl)

        Dim actDir As New ActiveDir(sLoginName)
        Dim lbl As New Label
        If TypeOf (wc) Is Label Then


            lbl = DirectCast(wc, Label)




        End If
        '   SubmittingOnBehalfOfEmailLabel.Text = actDir.UserAdEntry.Properties.Item("mail").Value.ToString()
        If actDir.UserAdEntry IsNot Nothing Then



            Dim tp As New ToolTipHelper()
            lbl.Text = "Valid"


            wc.Attributes.Add("class", "masterTooltip")
            wc.Attributes.Add("title", tp.ActiveDirToolTipHtml(actDir))

        Else

            lbl.Text = "invalid"
        End If

    End Sub

End Class
