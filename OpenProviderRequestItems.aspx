﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="false" CodeFile="OpenProviderRequestItems.aspx.vb" Inherits="OpenProviderRequestItems" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">


<asp:UpdatePanel ID="uplViewRequest" runat ="server" >


<ContentTemplate>


<table class="wrapper" style="border-style: groove" align="left">

<tr>
<td>


        <table border="3" style="empty-cells:show" >
           <tr>
               <th colspan = "4" class="tableRowHeader">
                   Open Provider Request Accounts
                </th>     
           </tr>
            <tr>
                  <td colspan = "4">
                                        
                        <asp:Button ID = "btnCancel" runat = "server" Text = "Return" Width = "120px" />
                                    
                  </td>
            </tr>
           <tr>
            <td style="font-weight:bold">
               Client Name
            </td>
             <td>
                <asp:Label ID = "first_namelabel" runat = "server"></asp:Label>&nbsp<asp:Label ID = "last_namelabel" runat = "server" ></asp:Label>
            </td>

          
            <td style="font-weight:bold" >
            Requestor Name
           
            </td><td >
                <asp:Label ID = "requestor_namelabel" runat = "server"></asp:Label>
            </td>
            


           </tr>
           <tr>
               
                 
                 <td style="font-weight:bold">
                 Start Date
                  </td>
                
                  <td>
                   <asp:Label ID = "start_datelabel" runat = "server"></asp:Label>
            
                    </td>
                <td style="font-weight:bold">
                    Submit Date
                </td>
               
                 <td>
                    <asp:Label ID = "entered_datelabel" runat = "server"></asp:Label>
                </td>
                 
            </tr>
            <tr>
            <td  style="font-weight:bold" >

                Entity

            </td>

            <td>
               <asp:Label ID = "entity_namelabel" runat = "server"></asp:Label>
             
            </td>
            
           
                 <td style="font-weight:bold">
                   Department
                 </td>
                 <td >
               <asp:Label ID = "department_namelabel" runat = "server"></asp:Label>
                 </td>

                 
            </tr>
           
            <tr>
             <td style="font-weight:bold">
                Doctor Number
                </td>
                <td>
                    <asp:Label ID = "doctor_master_numLabel" runat = "server"></asp:Label>
                </td>
          
          
        <td style="font-weight:bold">
            NPI Number
                </td>
                <td>
                    <asp:Label ID = "npiLabel" runat = "server"></asp:Label>
                </td>
                </tr>
                <tr>
                
           
             <td style="font-weight:bold">
           Taxonomy
                </td>
                <td colspan = "3">

                    <asp:Label ID = "taxonomyLabel" runat = "server"></asp:Label>

                </td>

        </tr>
           
       

            

           
          
            
           
          <tr>
            
            <td colspan = "4">
            

                <asp:Table ID = "tblOpenProviderRequestItems" runat = "server">
                
                
                </asp:Table>
           

            </td>
          
          </tr>
        </table>






</td>

</tr>

</table>

   
        
</ContentTemplate>


        </asp:UpdatePanel>


</asp:Content>

