﻿Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports App_Code
Imports AjaxControlToolkit
Imports C1.Web.UI.Controls.C1Window
Imports System.Net
Imports System.Reflection
Imports System.Net.Mail
Imports System.Net.Mail.SmtpClient


Partial Class ViewRequestTicket
    Inherits System.Web.UI.Page
    Dim FormSignOn As New FormSQL
    Dim StatusForm As New FormSQL
    Dim DemoForm As New FormSQL()

    Dim UserInfo As UserSecurity
    Dim bHasError As Boolean

    Dim Mailobj As System.Net.Mail.SmtpClient = New System.Net.Mail.SmtpClient
    Dim Mailmsg As New System.Net.Mail.MailMessage
    Dim MailAddress As System.Net.Mail.MailAddress
    Dim mailAttachment As System.Net.Mail.Attachment

    Dim RequestNum As Integer
    Dim reportItemlist As RequestReportItems
    Dim RequestTickect As RequestTicket
    Dim TicketstatusClass As TicketStatus
    Dim emaillist As New EmailListOne()

    Dim imageInBytes As Byte()

    Dim fileName As String
    Dim filetype As String
    Dim fileExtension As String

    'Dim sServer As String = ""
    'Dim savePath As String = ("D:\\CSCFileUpload\")
    Dim saveImage As String '= ("D:\\CSCFileUpload\Image1.jpg")

    Dim savePath As String

    Dim iClientNum As Integer
    Dim iSecurityLevel As Integer
    Dim iTechNum As Integer
    Dim iAudit As Integer
    Dim Allitemslist As New List(Of RPTRequestItems)
    Dim RPTDistricts As List(Of String)

    'Dim Client3Requestor As Client3
    'Dim Client3CurrentTech As Client3
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        UserInfo = New UserSecurity(Session("LoginID"), Session("EmployeeConn"))


        'UserInfo = Session("objUserSecurity")
        RequestNum = Request.QueryString("RequestNum")
        'RequestNum = 220467

        RequestNumLabel.Text = Request.QueryString("RequestNum")
        RequestNumLabel2.Text = Request.QueryString("RequestNum")
        RequestNumLabel3.Text = Request.QueryString("RequestNum")
        RequestNumLabel4.Text = Request.QueryString("RequestNum")
        RequestNumLabel5.Text = Request.QueryString("RequestNum")
        RequestNumLabel6.Text = Request.QueryString("RequestNum")

        WhoclientNumLabel.Text = UserInfo.ClientNum
        entryclientnumLabel.Text = UserInfo.ClientNum
        iTechNum = UserInfo.TechNum
        WhoTechnumLabel.Text = UserInfo.TechNum
        WhoNtloginLabel.Text = UserInfo.NtLogin
        entrytechnumLabel.Text = UserInfo.TechNum
        SecurityLevelLabel.Text = UserInfo.SecurityLevelNum
        iAudit = UserInfo.Audit

        RequestTickect = New RequestTicket(RequestNum, Session("CSCConn"))
        TicketstatusClass = New TicketStatus(RequestNum, Session("CSCConn"))

        Dim Client3Contr As New Client3Controller()
        Dim ctrlRequest As New RequestEntryController()

        FormSignOn = New FormSQL(Session("CSCConn"), "selTicketRFSByNumV7", "UpdTicketRFSObjectV8", "", Page)
        FormSignOn.AddSelectParm("@requestnum", RequestNum, SqlDbType.Int, 6)

        StatusForm = New FormSQL(Session("CSCConn"), "SelCurrentStatus", "UpdTicketRFSObjectV8", "", Page)
        StatusForm.AddSelectParm("@request_num", RequestNum, SqlDbType.Int, 6)

        Dim dbAccess As New CkhsEmployeeDbAccess(Session("EmployeeConn"))
        Dim ckhsEmployee = dbAccess.GetCkhsEmployee(iClientNum)

        If SecurityLevelLabel.Text <> "" Then
            Session("SecurityLevelNum") = CInt(SecurityLevelLabel.Text)

        End If

        If Session("LoginID").ToString.ToLower = "melej" Then
            connid.Visible = True
            connid.Text = "CSC Conn " & Session("CSCConn").ToString & "Employee Conn " & Session("EmployeeConn").ToString

            'WhoclientNumLabel.Visible = True
            'clientnumLabel.Visible = True

        End If

        'sServer = Request.ServerVariables("SERVER_NAME")

        'If sServer = ConfigurationManager.AppSettings.Get("ProdWebServers") Then
        '    ' Production Data and web site
        '    File.Delete("D:\\CSCFileUpload\Image1.jpg")
        '    savePath = ("D:\\CSCFileUpload\")
        '    saveImage = ("D:\\CSCFileUpload\Image1.jpg")


        'ElseIf sServer = ConfigurationManager.AppSettings.Get("TestWebServers") Then
        '    ' Test Site and test data
        '    File.Delete("D:\\CSCFileUpload\Image1.jpg")

        '    savePath = ("D:\\CSCFileUpload\")
        '    saveImage = ("D:\\CSCFileUpload\Image1.jpg")

        'Else
        '    ' else default to test
        '    File.Delete("D:\\CSCFileUpload\Image1.jpg")

        '    savePath = ("D:\\CSCFileUpload\")
        '    saveImage = ("D:\\CSCFileUpload\Image1.jpg")
        'End If

        savePath = Server.MapPath("~\\CSCFileUpload\\")
        saveImage = (savePath & "/image1.jpg")

        'ClientnameHeader.Text = ckhsEmployee.FullName

        'FillActivities()
        If Request.QueryString("label") <> "" Then ' this comes from the update of client calls back to this page line 2748
            lblValidation.Text = Request.QueryString("label")
        End If

        'If hdimagetext.Text <> "" Then
        '    fileimg.ImageUrl = hdimagetext.Text
        'End If

        If Not IsPostBack Then

            FormSignOn.FillForm()
            StatusForm.FillForm()

            DisplayFromDB()
            'currentgroupnumLabel

            categorycdLabel.Text = RequestTickect.categorycd
            typecdLabel.Text = RequestTickect.typecd
            itemcdLabel.Text = RequestTickect.itemcd
            clientnumLabel.Text = RequestTickect.clientnum
            currenttechnumLabel.Text = RequestTickect.currenttechnum
            currentgroupnumLabel.Text = RequestTickect.currentgroupnum
            origionalGroupNumLable.Text = RequestTickect.currentgroupnum
            completiondateTextBox.Text = RequestTickect.completiondate
            opentimelb.Text = RequestTickect.TotalAcknowledgeTime
            hdPriorityLb.Text = RequestTickect.priority
            Sendemail.Text = RequestTickect.Sendemail
            hdSurveyexists.Text = RequestTickect.Surveyexists
            RequestTypeLabel.Text = RequestTickect.requesttype

            'LoginNamelabel.Text = Client3Contr.LoginName

            If iAudit <> 16 Then
                If currentgroupnumLabel.Text = "16" Then
                    Response.Redirect("./MyQueue.aspx", True)

                End If

            End If


            Dim status As String


            status = RequestTickect.currentacktime

            If status = "" Then
                'if request is assigned to current viewer
                If iTechNum = RequestTickect.currenttechnum Then
                    btnacknoledge.Visible = True

                End If
            End If

            If hdPriorityLb.Text = "1" Then

                requestlbl.Text = requestlbl.Text & " P1 "

                requestlbl.ForeColor = Color.Red
                RequestNumLabel.ForeColor = Color.Red

                priorityrbl.ForeColor = Color.Red
                p1lbl.ForeColor = Color.Red
            End If
            Dim ddlBinder As New DropDownListBinder
            Dim ddlTypeBinder As New DropDownListBinder

            ddlBinder.BindData(entitycdDDL, "selEntityCodesV4", "entity_cd", "hosp_desc", Session("EmployeeConn"))


            Dim ddStaBinder As New DropDownListBinder(Statusddl, "SelUserStatusCodesV7", Session("CSCConn"))
            ddStaBinder.AddDDLCriteria("@request_num", RequestNum, SqlDbType.Int)

            ddStaBinder.BindData("status_cd", "status_desc")

            'Dim ddlStatusBinder As New DropDownListBinder(StatusDesddl, "SelUserStatusCodesV7", Session("CSCConn"))
            'ddlStatusBinder.AddDDLCriteria("@request_num", RequestNum, SqlDbType.Int)

            'ddlStatusBinder.BindData("status_cd", "status_desc")


            Dim ddlCurrGroup As New DropDownListBinder(changegroupddl, "SelTechGroup", Session("CSCConn"))
            ddlCurrGroup.AddDDLCriteria("@display", "queue", SqlDbType.NVarChar)
            'ddlCurrGroup.AddDDLCriteria("@securityLevel", Session("SecurityLevelNum"), SqlDbType.Int)
            ddlCurrGroup.AddDDLCriteria("@ntlogin", Session("LoginID"), SqlDbType.NVarChar)


            ddlCurrGroup.BindData("group_num", "group_name")


            'Dim ddlCurrTech As New DropDownListBinder(ChangeTechddl, "selTechsByGroupNum", Session("CSCConn"))
            ''currentgroupnumLabel.Text
            'ddlCurrTech.AddDDLCriteria("@group_num", currentgroupnumLabel.Text, SqlDbType.NVarChar)
            'ddlCurrTech.BindData("tech_num", "TechName")

            If currentgroupnumLabel.Text = "" Then
                changegroupddl.SelectedIndex = -1

                'Get all groups and techs

                Dim ddlCurrTech As New DropDownListBinder(ChangeTechddl, "selTechsByGroupNum", Session("CSCConn"))
                ddlCurrTech.BindData("group_num", "Group_name")

                ChangeTechddl.Enabled = False

            ElseIf currentgroupnumLabel.Text <> "" Then

                Try
                    Dim ddlCurrTech As New DropDownListBinder(ChangeTechddl, "selTechsByGroupNum", Session("CSCConn"))
                    'currentgroupnumLabel.Text
                    ddlCurrTech.AddDDLCriteria("@group_num", currentgroupnumLabel.Text, SqlDbType.NVarChar)
                    ddlCurrTech.AddDDLCriteria("@securityLevel", Session("SecurityLevelNum"), SqlDbType.Int)

                    ddlCurrTech.BindData("tech_num", "TechName")
                    '        currTechlb.Text = "Previous Tech:"


                    changegroupddl.SelectedValue = currentgroupnumLabel.Text
                Catch ex As Exception

                    changegroupddl.SelectedIndex = -1
                End Try




            End If

            If currenttechnumLabel.Text = "" Then

                ChangeTechddl.SelectedIndex = -1
            Else
                ChangeTechddl.SelectedValue = currenttechnumLabel.Text


            End If

            If entity_cdLabel.Text = "" Then
                entitycdDDL.SelectedIndex = -1
            Else
                Dim entitycd As String = entity_cdLabel.Text

                entitycd = entitycd.Trim
                entitycdDDL.SelectedValue = entitycd

            End If


            Dim ddbDepartmentBinder As New DropDownListBinder(departmentcdddl, "selDepartmentCodesV4", Session("EmployeeConn"))
            ddbDepartmentBinder.AddDDLCriteria("@entity_cd", entitycdDDL.SelectedValue, SqlDbType.NVarChar)
            ddbDepartmentBinder.BindData("department_cd", "department_name")

            If department_cdLabel.Text = "" Then
                departmentcdddl.SelectedIndex = -1
            ElseIf department_cdLabel.Text <> "" Then
                Try

                    departmentcdddl.SelectedValue = department_cdLabel.Text.Trim
                Catch ex As Exception

                    departmentcdddl.SelectedIndex = -1
                End Try

            End If

            ddlBinder.BindData(facilitycdddl, "SelFacilityCodes", "facility_cd", "facility_short_name", Session("EmployeeConn"))


            If facility_cdLabel.Text = "" Then
                facilitycdddl.SelectedIndex = -1
            ElseIf facility_cdLabel.Text <> "" Then
                Try
                    facilitycdddl.SelectedValue = facility_cdLabel.Text
                    FacilityCdeMailLabel.Text = facilitycdddl.SelectedItem.ToString

                Catch ex As Exception
                    facilitycdddl.SelectedIndex = -1

                End Try
            End If


            If building_cdLabel.Text = "" Then
                buildingcdddl.SelectedIndex = -1
            ElseIf building_cdLabel.Text <> "" Then

                Try

                    ddlBinder = New DropDownListBinder(buildingcdddl, "SelBuildingByFacility", Session("EmployeeConn"))
                    ddlBinder.AddDDLCriteria("@facility_cd", facility_cdLabel.Text, SqlDbType.NVarChar)
                    ddlBinder.BindData("building_cd", "building_name")

                    buildingcdddl.SelectedValue = building_cdLabel.Text.Trim
                    buildingEmailnameLabel.Text = buildingcdddl.SelectedItem.ToString

                Catch ex As Exception

                    buildingcdddl.SelectedIndex = -1

                End Try



            End If


            If building_cdLabel.Text <> "" And facility_cdLabel.Text <> "" And floorLabel.Text <> "" Then

                Try
                    Dim ddlFloorBinder As DropDownListBinder

                    ddlFloorBinder = New DropDownListBinder(Floorddl, "SelFloorsByBuilding", Session("EmployeeConn"))
                    ddlFloorBinder.AddDDLCriteria("@facility_cd", facility_cdLabel.Text, SqlDbType.NVarChar)
                    ddlFloorBinder.AddDDLCriteria("@building_cd", building_cdLabel.Text, SqlDbType.NVarChar)

                    ddlFloorBinder.BindData("floor_no", "floor_no")

                    Floorddl.SelectedValue = floorLabel.Text.TrimEnd
                    floorEmailLabel.Text = Floorddl.SelectedItem.ToString

                Catch ex As Exception
                    Floorddl.SelectedValue = -1
                End Try
            Else
                Floorddl.SelectedValue = -1

            End If

            'Dim ViewRequestTicket As RequestTicket = ctrlRequest.RequestTicketByRequestNum(RequestNum)


            'Dim frmHelperRequest As New FormHelper()
            'frmHelperRequest.FillForm(ViewRequestTicket, Page)

            'categorycd.Text = ViewRequestTicket.categorycd
            'typecd.Text = ViewRequestTicket.typecd
            'itemcd.text = ViewRequestTicket.itemcd



            ddlBinder = New DropDownListBinder(categorycdddl, "SelCategoryCodes", Session("CSCConn"))
            ddlBinder.AddDDLCriteria("@securitylevel", Session("SecurityLevelNum"), SqlDbType.SmallInt)

            If RequestTypeLabel.Text <> "" Then

                If RequestTypeLabel.Text.Trim = "service" Then
                    ddlBinder.AddDDLCriteria("@categoryType", "rfs", SqlDbType.NVarChar)

                Else
                    ddlBinder.AddDDLCriteria("@categoryType", "both", SqlDbType.NVarChar)

                End If
            Else
                ddlBinder.AddDDLCriteria("@categoryType", "both", SqlDbType.NVarChar)
            End If

            ddlBinder.BindData("category_cd", "Category_Desc")

            If categorycdLabel.Text <> "" Then
                Try
                    categorycdddl.SelectedValue = categorycdLabel.Text.Trim
                Catch ex As Exception
                    categorycdddl.SelectedIndex = -1
                End Try

            ElseIf categorycdLabel.Text = "" Then
                categorycdLabel.Text = "other"
                categorycdddl.SelectedValue = categorycdLabel.Text.Trim

            End If



            ddlTypeBinder = New DropDownListBinder(typecdddl, "SelCategoryType", Session("CSCConn"))
            ddlTypeBinder.AddDDLCriteria("@Category_cd", categorycdLabel.Text, SqlDbType.NVarChar)


            ddlTypeBinder.BindData("type_cd", "CatagoryTypeDesc")
            If typecdLabel.Text <> "" Then
                Try
                    typecdddl.SelectedValue = typecdLabel.Text.Trim.ToLower
                Catch ex As Exception
                    typecdddl.SelectedIndex = -1
                End Try
            ElseIf typecdLabel.Text = "" Then
                typecdLabel.Text = "other"
                typecdddl.SelectedValue = typecdLabel.Text.Trim
            End If



            ddlBinder = New DropDownListBinder(itemcdddl, "SelCategoryItems", Session("CSCConn"))
            'ddlBinder.BindData("item_num", "item_cd")
            ddlBinder.AddDDLCriteria("@Category_cd", categorycdLabel.Text, SqlDbType.NVarChar)
            ddlBinder.AddDDLCriteria("@type_cd", typecdLabel.Text, SqlDbType.NVarChar)

            ddlBinder.BindData("item_cd", "ItemTypeDesc")
            itemcdddl.Items.Insert(1, "Other")


            If itemcdLabel.Text <> "" Then
                Try
                    itemcdddl.SelectedValue = itemcdLabel.Text.Trim

                Catch ex As Exception
                    itemcdddl.SelectedIndex = -1
                End Try
            Else
                itemcdddl.SelectedIndex = -1

            End If



            'Number of terminals
            If numofterminalsLabel.Text = "" Then
                num_of_terminalsddl.SelectedIndex = -1
            ElseIf CInt(numofterminalsLabel.Text) > 10 Then
                numofterminalsLabel.Text = "10"
                num_of_terminalsddl.SelectedValue = "10"
            ElseIf CInt(numofterminalsLabel.Text) = "0" Then
                num_of_terminalsddl.SelectedIndex = -1
            End If
            'Resolution codes




            'SelCategoryResolutionCodesV7

            'Dim cl3Requestor As Client3 = Client3Contr.GetClientByClientNum(clientnumLabel.Text)

            'Dim frmHelperRequestor As New FormHelper()
            'frmHelperRequestor.FillForm(cl3Requestor, Page, "Requestor")


            If RequestTypeLabel.Text.Trim <> "" Then


                RequestTyperbl.SelectedValue = RequestTypeLabel.Text.Trim

                If RequestTyperbl.SelectedValue = "ticket" Then

                    ticketPnl.Visible = True
                    rfsPnl.Visible = False
                    HDAllPurposeDesc.Text = shortdescTextbox.Text

                ElseIf RequestTyperbl.SelectedValue = "rfs" Or RequestTyperbl.SelectedValue = "service" Then

                    rfsPnl.Visible = True

                    If categorycdLabel.Text = "reports" Then

                        'report panel 
                        reportPanel.Visible = True
                        TechPanl.Visible = False


                        'get Items from requestitems
                        GetRequestItems()

                        DataFieldsTextBox.Text = RequestTickect.terminalinfodesc
                        DateRangeTextBox.Text = RequestTickect.printerinfodesc
                        TotalOnfieldTextBox.Text = RequestTickect.pcinfodesc
                        ReportDescTextBox.Text = RequestTickect.netdropsinfodesc
                        reportOtherDescTextBox.Text = RequestTickect.otherinfodesc
                    Else


                        terminal_info_descTextBox.Text = RequestTickect.terminalinfodesc
                        printer_info_descTextBox.Text = RequestTickect.printerinfodesc

                        pc_info_descTextBox.Text = RequestTickect.pcinfodesc
                        net_drops_info_descTextBox.Text = RequestTickect.netdropsinfodesc
                        other_info_descTextBox.Text = RequestTickect.otherinfodesc



                        reportPanel.Visible = False
                        TechPanl.Visible = True
                    End If

                    servicedescTextbox.Text = RequestTickect.servicedesc
                    businessdesTextBox.Text = RequestTickect.businessdes
                    impactdescTextBox.Text = RequestTickect.impactdesc

                    HDAllPurposeDesc.Text = servicedescTextbox.Text


                    ticketPnl.Visible = False

                    priorityrbl.Enabled = False

                    completiondateTextBox.BackColor() = Color.Yellow
                    completiondateTextBox.BorderColor() = Color.Black
                    completiondateTextBox.BorderStyle() = BorderStyle.Solid
                    completiondateTextBox.BorderWidth = Unit.Pixel(2)

                    servicedescTextbox.BackColor() = Color.Yellow
                    servicedescTextbox.BorderColor() = Color.Black
                    servicedescTextbox.BorderStyle() = BorderStyle.Solid
                    servicedescTextbox.BorderWidth = Unit.Pixel(2)

                    businessdesTextBox.BackColor() = Color.Yellow
                    businessdesTextBox.BorderColor() = Color.Black
                    businessdesTextBox.BorderStyle() = BorderStyle.Solid
                    businessdesTextBox.BorderWidth = Unit.Pixel(2)


                End If


            End If

            'If RequestTypeLabel.Text.Trim = "project" Then

            '    If categorycdLabel.Text = "reports" Then
            '        rfsPnl.Visible = True

            '        'report panel 
            '        reportPanel.Visible = True
            '        TechPanl.Visible = False
            '    Else

            '        rfsPnl.Visible = True

            '        reportPanel.Visible = False
            '        TechPanl.Visible = True

            '    End If

            '    ticketPnl.Visible = True


            'End If

            If hdSurveyexists.Text = RequestNumLabel.Text Then
                tabsurvey.Visible = True

            End If

        End If

            If Session("SecurityLevelNum") < 70 Then

                If Session("SecurityLevelNum") <> 60 Then

                    ' if this is not your request back to search page
                    If WhoclientNumLabel.Text <> clientnumLabel.Text Then
                        Response.Redirect("./SimpleSearch.aspx", True)

                    Else
                        Dim ctrl As Control = DataCheckingAndValidation.SetChildControlsRecursiveToEnabledFalse(uplTicket, False)
                        btnSubmit.Enabled = False
                        btnoldCSC.Visible = False
                        btnacknoledge.Visible = False
                        tbAddActivity.Visible = False


                    End If

                    ' Adding or remove ticket to a project
                    ProJPan.Visible = False

                End If
            End If



            categorycdddl.BackColor() = Color.Yellow
            categorycdddl.BorderColor() = Color.Black
            categorycdddl.BorderStyle() = BorderStyle.Solid
            categorycdddl.BorderWidth = Unit.Pixel(2)


            typecdddl.BackColor() = Color.Yellow
            typecdddl.BorderColor() = Color.Black
            typecdddl.BorderStyle() = BorderStyle.Solid
            typecdddl.BorderWidth = Unit.Pixel(2)


            'itemcdddl.BackColor() = Color.Yellow
            'itemcdddl.BorderColor() = Color.Black
            'itemcdddl.BorderStyle() = BorderStyle.Solid
            'itemcdddl.BorderWidth = Unit.Pixel(2)

            'If currenttechnumLabel.Text = "" Then

            '    ChangeTechddl.SelectedIndex = 0
            'Else
            '    ChangeTechddl.SelectedValue = currenttechnumLabel.Text



            'End If

            'If currentgroupnumLabel.Text = "" Then
            '    changegroupddl.SelectedIndex = 0
            'Else
            '    changegroupddl.SelectedValue = currentgroupnumLabel.Text
            'End If


            fillActivityTable()


            'If Session("LoginID").ToString.ToLower = "melej" Then
            '    'Session("LoginID").ToString.ToLower = "geip00" Or Session("LoginID").ToString.ToLower = "ralphc" Or Then
            '    Dim ctrl As Control = DataCheckingAndValidation.SetChildControlsRecursiveToEnabledFalse(uplTicket, False)

            'End If
            '' general public can view not modify
            If Session("SecurityLevelNum") < 70 Then

                Dim ctrl As Control = DataCheckingAndValidation.SetChildControlsRecursiveToEnabledFalse(uplTicket, False)
                btnSubmit.Enabled = False
                btnoldCSC.Enabled = False
            End If

            'If Session("SecurityLevelNum") < 101 Then

            '    Dim ctrl As Control = DataCheckingAndValidation.SetChildControlsRecursiveToEnabledFalse(uplTicket, False)
            '    btnSubmit.Enabled = False
            '    btnoldCSC.Enabled = True
            'End If


            If currstatuscdLabel.Text = "resols" Then

                Dim ctrl As Control = DataCheckingAndValidation.SetChildControlsRecursiveToEnabledFalse(uplTicket, False)
                btnSubmit.Text = "ReOpen Ticket/RFS"

                btnSubmit.Enabled = True

                'btnoldCSC.Enabled = True

                tbAddActivity.Visible = False


                Fileview.Enabled = True
                btnRemove.Enabled = False
                btnOpenFile2.Enabled = True

            ElseIf CloseDateLabel.Text <> "" Then
                Dim ctrl As Control = DataCheckingAndValidation.SetChildControlsRecursiveToEnabledFalse(uplTicket, False)
                'btnoldCSC.Enabled = True


            End If
            If clientnumLabel.Text <> "" Then
                btnReturnClient.Enabled = True
            End If

            If Session("environment").ToString.ToLower = "testing" Then
                If Session("SecurityLevelNum") > 69 Then

                    btnSubmit.Enabled = True
                    btnacknoledge.Enabled = True
                    ' btnAddTechService.Enabled = False
                    btnSubmitActivityNote.Enabled = True


                Else
                    btnSubmit.Enabled = False
                    btnacknoledge.Enabled = False
                    'btnAddTechService.Enabled = False
                    btnSubmitActivityNote.Enabled = False

                End If

                'R4 security
            'If Session("SecurityLevelNum") = 70 Then
            '    changegroupddl.Enabled = False
            '    ChangeTechddl.Enabled = False
            '    tabsurvey.Visible = False
            '    ProJPan.Visible = False
            '    btnoldCSC.Visible = False
            '    priorityrbl.Enabled = False

            'End If

            Else
                'production

                If Session("SecurityLevelNum") > 69 Then


                    'If Session("LoginID").ToString.ToLower = "melej" Or Session("LoginID").ToString.ToLower = "geip00" Or Session("LoginID").ToString.ToLower = "durr05" Then
                    btnSubmit.Enabled = True
                    btnacknoledge.Enabled = True
                    ' btnAddTechService.Enabled = False
                    btnSubmitActivityNote.Enabled = True


                Else
                    btnSubmit.Enabled = False
                    btnacknoledge.Enabled = False
                    ' btnAddTechService.Enabled = False
                    btnSubmitActivityNote.Enabled = False

                End If

                'R4 security
            'If Session("SecurityLevelNum") = 70 Then
            '    changegroupddl.Enabled = False
            '    ChangeTechddl.Enabled = False
            '    tabsurvey.Visible = False
            '    ProJPan.Visible = False
            '    btnoldCSC.Visible = False
            '    priorityrbl.Enabled = False
            'End If


            End If

    End Sub
    Protected Sub GetRequestItems()


        Dim cblFacilitesBinder As New CheckBoxListBinder(cblFacilities, "SelFacilityEntity", Session("EmployeeConn"))
        cblFacilitesBinder.AddDDLCriteria("@EntityFacility", "Facility", SqlDbType.NVarChar)
        cblFacilitesBinder.BindData("Code", "EntityFacilityDesc")


        'SelDistrictCodes
        'cblDistrictCodes
        Dim cblDistrictBinder As New CheckBoxListBinder(cblDistrictCodes, "SelDistrictCodes", Session("CSCConn"))
        'cblDistrictBinder.AddDDLCriteria("@EntityFacility", "Facility", SqlDbType.NVarChar)
        cblDistrictBinder.BindData("district_cd", "DistrictDesc")

        'SelFrequencyCodes
        'ReportFreqddl
        Dim ddlFreqBinder As New DropDownListBinder(ReportFreqddl, "SelFrequencyCodes", Session("CSCConn"))
        ddlFreqBinder.BindData("frequency_cd", "frequency_desc")


        Dim RequestsItm As New RequestReportItems()

        RequestsItm = New RequestReportItems(HttpContext.Current.Session("CSCConn").ToString, RequestNum)


        'district codes

        For Each cblItem As ListItem In cblDistrictCodes.Items

            'GetRequestByItemType
            For Each ari As RPTRequestItems In RequestsItm.GetRequestByItemType("district")

                If ari.ItemDetail = cblItem.Value Then
                    If cblItem.Selected = False Then
                        cblItem.Selected = True
                    End If
                End If
            Next
        Next

        'facilities
        For Each cbFacitem As ListItem In cblFacilities.Items
            For Each fitm As RPTRequestItems In RequestsItm.GetRequestByItemType("facility")
                If fitm.ItemDetail = cbFacitem.Value Then
                    If cbFacitem.Selected = False Then
                        cbFacitem.Selected = True
                    End If
                End If

            Next
        Next

        'frequency
        'ReportFreqTextBox
        For Each feqitm As RPTRequestItems In RequestsItm.GetRequestByItemType("frequency")
            If feqitm.ItemDetail <> "" Then
                ReportFreqTextBox.Text = feqitm.ItemDetail
                ReportFreqddl.SelectedValue = feqitm.ItemDetail

            End If

        Next

        'rptType
        For Each rptyitm As RPTRequestItems In RequestsItm.GetRequestByItemType("rptType")
            If rptyitm.ItemDetail <> "" Then
                ReportTypeTextBox.Text = rptyitm.ItemDetail
                ReportTypeddl.SelectedValue = rptyitm.ItemDetail

            End If

        Next


    End Sub

    Sub DisplayFromDB()

        'Directory.SetCurrentDirectory(savePath)

        Using SqlConnection As New SqlConnection(Session("CSCConn"))
            Dim Cmd As New SqlCommand()
            Cmd.CommandText = "SelRequestImages"
            Cmd.Parameters.AddWithValue("@request_num", RequestNum)

            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.Connection = SqlConnection

            SqlConnection.Open()
            Using Cmd

                Using dr = Cmd.ExecuteReader()
                    If dr.HasRows Then

                        Do While dr.Read



                            imageInBytes = IIf(IsDBNull(dr("FileContent")), Nothing, dr("FileContent"))
                            'fileExtension
                            Dim Fileextension As String = IIf(IsDBNull(dr("FileType")), Nothing, dr("FileType"))
                            Dim filename As String = IIf(IsDBNull(dr("FileName")), Nothing, dr("FileName"))

                            Fileextensionlbl.Text = Fileextension

                            Dim memoryStream As System.IO.MemoryStream = _
                                New System.IO.MemoryStream(imageInBytes, False)

                            'Dim image As System.Drawing.Image = System.Drawing.Image.FromStream(memoryStream)



                            filenamelbl.Text = filename

                            Dim loca As String
                            loca = Server.MapPath("~\\CSCFileUpload\\")

                            File.Delete(loca & filenamelbl.Text)
                            File.Delete(loca & "/image1.jpeg")
                            File.Delete(loca & "/image1.jpg")

                            'delete it before writing to file

                            ' File.Delete("D:/CSCFileUpload/" & filenamelbl.Text)
                            'File.Delete("D:/wwwroot/CSC14/CSCFileUpload/image1.jpeg")

                            'Response.ContentType = Fileextension


                            'Dim fstif As New FileStream("D:/wwwroot/CSC14/CSCFileUpload/" & filenamelbl.Text, FileMode.CreateNew)
                            'Dim wtif As New BinaryWriter(fstif)


                            'If Fileextension = "image/jpeg" Or Fileextension = "image/png" Then
                            '    wtif.Write(imageInBytes)
                            '    wtif.Flush()
                            '    wtif.Close()

                            '    fileimg.ImageUrl = Server.MapPath("~\\CSCFileUpload\\" & filenamelbl.Text)
                            '    fileimg.DataBind()


                            '    hdimagetext.Text = Server.MapPath("~\\CSCFileUpload\\" & filenamelbl.Text)

                            'End If

                            'Dim strfn As String = Convert.ToString(DateTime.Now.ToFileTime())
                            'Dim fs As New FileStream(strfn, FileMode.CreateNew, FileAccess.Write)
                            'fs.Write(imageInBytes, 0, imageInBytes.Length)

                            'fs.Flush()
                            'fs.Close()

                            'myImg.FromHbitmap = New Uri(System.Environment.CurrentDirectory & "\" & strfn)
                            'imgDBExtract.Source = myImg



                            'image.Save("saveImage")
                            'Response.BinaryWrite(imageInBytes)

                            'fileimg.ImageUrl = saveImage

                            Fileview.Visible = True
                        Loop
                    Else

                        Fileview.Visible = True
                        btnOpenFile2.Visible = False
                        btnRemove.Visible = False

                        btnAddFile.Visible = True
                    End If

                End Using
            End Using
            SqlConnection.Close()

        End Using

    End Sub
    'Protected Sub fileimg_DataBinding(sender As Object, e As System.EventArgs) Handles fileimg.DataBinding
    '    hdimagetext.Text = Server.MapPath("~\\CSCFileUpload\\" & filenamelbl.Text)

    'End Sub

    Public Function CheckRequiredFields() As Boolean
        Dim removeCss As New PageControls(Page)
        removeCss.RemoveCssClass(Page, "textInputRequired")

        'Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
        'Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")

        'emp_type_cdLabel.Text = emp_type_cdrbl.SelectedValue

        Dim Catdesc As New RequiredField(categorycdddl, "Category Type")
        categorycdddl.BackColor() = Color.Yellow
        categorycdddl.BorderColor() = Color.Black
        categorycdddl.BorderStyle() = BorderStyle.Solid
        categorycdddl.BorderWidth = Unit.Pixel(2)

        Dim TypeDesc As New RequiredField(typecdddl, "Type ")

        typecdddl.BackColor() = Color.Yellow
        typecdddl.BorderColor() = Color.Black
        typecdddl.BorderStyle() = BorderStyle.Solid
        typecdddl.BorderWidth = Unit.Pixel(2)

        'Dim ItemDesc As New RequiredField(itemcdddl, "Item ")

        'itemcdddl.BackColor() = Color.Yellow
        'itemcdddl.BorderColor() = Color.Black
        'itemcdddl.BorderStyle() = BorderStyle.Solid
        'itemcdddl.BorderWidth = Unit.Pixel(2)

        Return bHasError


    End Function
    Protected Sub fillActivityTable()


        tblActivity.Dispose()
        tblActivity.Rows.Clear()


        Dim thisData As New CommandsSqlAndOleDb("SelRequestActivitiesV7", Session("CSCConn"))
        thisData.AddSqlProcParameter("@request_num", RequestNum, SqlDbType.NVarChar)

        Dim iRowCount = 1

        Dim dt As DataTable

        dt = thisData.GetSqlDataTable

        Dim rwHeader As New TableRow

        Dim clAccountHeader As New TableCell
        Dim clEntryDatepeHeader As New TableCell
        Dim clStatusHeader As New TableCell
        Dim clpriorityheader As New TableCell
        Dim clDescHeader As New TableCell
        Dim clTechnameheader As New TableCell




        clAccountHeader.Text = "Activity#"
        clEntryDatepeHeader.Text = "Entry Date"
        clStatusHeader.Text = "Status"
        clpriorityheader.Text = "Priority"
        clTechnameheader.Text = "Tech"
        clDescHeader.Text = "Desc"


        rwHeader.Cells.Add(clAccountHeader)
        rwHeader.Cells.Add(clEntryDatepeHeader)
        rwHeader.Cells.Add(clStatusHeader)
        rwHeader.Cells.Add(clpriorityheader)
        rwHeader.Cells.Add(clTechnameheader)
        rwHeader.Cells.Add(clDescHeader)


        rwHeader.Font.Bold = True
        rwHeader.BackColor = Color.FromName("#006666")
        rwHeader.ForeColor = Color.FromName("White")
        rwHeader.Height = Unit.Pixel(36)

        tblActivity.Rows.Add(rwHeader)

        For Each drRow As DataRow In dt.Rows


            Dim rwData As New TableRow

            Dim clAccountData As New TableCell
            Dim clEntryDateData As New TableCell
            Dim clStatusData As New TableCell
            Dim clPriorityData As New TableCell
            Dim clDescData As New TableCell
            Dim clTechname As New TableCell


            'Dim clReqPhoneData As New TableCell




            clAccountData.Text = IIf(IsDBNull(drRow("request_seq_num")), "", drRow("request_seq_num"))
            clEntryDateData.Text = IIf(IsDBNull(drRow("last_change_date")), "", drRow("last_change_date"))
            clStatusData.Text = IIf(IsDBNull(drRow("status_desc")), "", drRow("status_desc"))
            clPriorityData.Text = IIf(IsDBNull(drRow("current_priority")), "", drRow("current_priority"))
            clDescData.Text = IIf(IsDBNull(drRow("current_activity")), "", drRow("current_activity"))
            clTechname.Text = IIf(IsDBNull(drRow("tech_name")), "", drRow("tech_name"))

            clTechname.Width = Unit.Percentage(10)



            rwData.Cells.Add(clAccountData)
            rwData.Cells.Add(clEntryDateData)
            rwData.Cells.Add(clStatusData)
            rwData.Cells.Add(clPriorityData)
            rwData.Cells.Add(clTechname)

            rwData.Cells.Add(clDescData)



            If iRowCount > 0 Then

                rwData.BackColor = Color.Bisque

            Else

                rwData.BackColor = Color.LightGray

            End If
            Dim thisrowcount As Int16

            thisrowcount = tblActivity.Rows.Count()

            tblActivity.Rows.Add(rwData)

            iRowCount = iRowCount * -1

        Next
        tblActivity.CellPadding = 5
        tblActivity.Width = New Unit("100%")


    End Sub
    Protected Sub btnSubmitActivityNote_Click(sender As Object, e As System.EventArgs) Handles btnSubmitActivityNote.Click

        Dim s As String = txtActivity.Text

        Dim sComdProd As String


        If hdActtype.Text = "resols" Then
            sComdProd = "updTicketCloseV7"


        Else
            sComdProd = "insRequestActivitiesV8"

            If txtActivity.Text = "" Then
                Exit Sub
            End If

        End If


        'updTicketCloseV7
        Dim thisData As New CommandsSqlAndOleDb(sComdProd, Session("CSCConn"))

        thisData.AddSqlProcParameter("@request_num", RequestNum, SqlDbType.Int)
        thisData.AddSqlProcParameter("@current_activity ", txtActivity.Text, SqlDbType.NVarChar)

        thisData.AddSqlProcParameter("@status_cd", hdActtype.Text, SqlDbType.NVarChar)
        thisData.AddSqlProcParameter("@nt_login", UserInfo.NtLogin, SqlDbType.NVarChar)

        thisData.AddSqlProcParameter("@tech_num", UserInfo.TechNum, SqlDbType.Int)

        If hdActtype.Text = "resols" Then
            thisData.AddSqlProcParameter("@resolution_cd", hdresolutionCD.Text, SqlDbType.NVarChar)

        Else
            thisData.AddSqlProcParameter("@client_num", UserInfo.ClientNum, SqlDbType.Int)

        End If

        thisData.ExecNonQueryNoReturn()
        'FillActivities()

        txtActivity.Text = ""
        'fillActivityTable()

        If hdActtype.Text = "resols" Then
            'InsP1SendEmail

            If Sendemail.Text = "sendmail" Then
                ' send email to client
                SendCompletedEmail(RequestNum)

            End If

            If hdPriorityLb.Text = "1" Then

                SendP1Email(RequestNum, 1, "close")
                SendGroupP1Email(RequestNum, "close")
            End If

            Response.Redirect("./ViewRequestTicket.aspx?RequestNum=" & RequestNum, True)
        Else
            tabs.ActiveTabIndex = 1

        End If



        'Close Ticket




    End Sub

    Protected Sub ResolutionCodesddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ResolutionCodesddl.SelectedIndexChanged
        hdresolutionCD.Text = ResolutionCodesddl.SelectedValue
    End Sub

    Protected Sub RblActivityType_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles RblActivityType.SelectedIndexChanged
        hdActtype.Text = RblActivityType.SelectedValue

        Select Case hdActtype.Text
            Case "resols"
                btnSubmitActivityNote.Text = "Close Request"
                Dim ddlBinder As New DropDownListBinder

                ddlBinder = New DropDownListBinder(ResolutionCodesddl, "SelCategoryResolutionCodesV7", Session("CSCConn"))
                'ddlBinder.BindData("item_num", "item_cd")
                ddlBinder.AddDDLCriteria("@request_num", RequestNumLabel.Text, SqlDbType.Int)

                ddlBinder.BindData("category_cd", "resolution_cd")

                ResolutionPanel.Visible = True

            Case "note"
                btnSubmitActivityNote.Text = "Add Note"
                ResolutionPanel.Visible = False
                hdresolutionCD.Text = ""

            Case "openep"

                ResolutionPanel.Visible = False
                hdresolutionCD.Text = ""

        End Select

    End Sub
    Protected Sub tabs_DataBinding(sender As Object, e As System.EventArgs) Handles tabs.DataBinding
        If tabs.ActiveTabIndex = 4 Then
            'fileimg.ImageUrl = Server.MapPath("~\\CSCFileUpload\\" & filenamelbl.Text)
            'fileimg.DataBind()

            'hdimagetext.Text = Server.MapPath("~\\CSCFileUpload\\" & filenamelbl.Text)


            'Response.Redirect("./viewImage.aspx?document_num=" & HDdocumentnum.Text & "&seqnum=" & HDseqnum.Text & "&file=" & HDfilename.Text)
            'closewindow()
            'Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "CloseWindow", "returnToParent();", true);

        End If

    End Sub

    Protected Sub tabs_ActiveTabChanged(sender As Object, e As System.EventArgs) Handles tabs.ActiveTabChanged
        If tabs.ActiveTabIndex = 3 Then

            If Session("SecurityLevelNum") < 70 Then

                ' if this is not your request back to search page
                If WhoclientNumLabel.Text <> clientnumLabel.Text Then
                    Response.Redirect("./SimpleSearch.aspx", True)

                Else

                End If
            End If

            Response.Redirect("./CSCSurvey.aspx?RequestNum=" & RequestNum, True)
        ElseIf tabs.ActiveTabIndex = 4 Then
            'fileimg.ImageUrl = Server.MapPath("~\\CSCFileUpload\\" & filenamelbl.Text)
            'fileimg.DataBind()

            'hdimagetext.Text = Server.MapPath("~\\CSCFileUpload\\" & filenamelbl.Text)

            If Fileextensionlbl.Text = "application/vnd.ms-excel" Then

            End If
            'Response.Redirect("./viewImage.aspx?RequestNum=" & RequestNum, True)

            'Dim pageurl As String = "./viewImage.aspx?RequestNum=" & RequestNum


            'closewindow()
            'Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "CloseWindow", "returnToParent();", true);

        ElseIf tabs.ActiveTabIndex = 5 Then
            Dim ddlBindert As New DropDownListBinder
            ddlBindert.BindData(Projectsddl, "Selprojects", "ProjectNum", "ProjectName", Session("CSCConn"))

            Dim DTReturnProjectNum As DataTable


            Dim thisdataDoc As New CommandsSqlAndOleDb("SelProjectRequestStatus", Session("CSCConn"))
            thisdataDoc.AddSqlProcParameter("@RequestNum", RequestNum, SqlDbType.Int, 20)

            DTReturnProjectNum = thisdataDoc.GetSqlDataTable()

            If DTReturnProjectNum.Rows.Count = 1 Then

                ProjectNumLabel.Text = DTReturnProjectNum.Rows(0)("ProjectNum").ToString

                If ProjectNumLabel.Text <> "0" Then
                    Projectsddl.SelectedValue = ProjectNumLabel.Text

                    Projectsddl.Enabled = False
                    BtnSubproject.Text = "Remove from Project"


                End If
            End If

        End If
    End Sub

    Protected Sub BtnSubproject_Click(sender As Object, e As System.EventArgs) Handles BtnSubproject.Click

        If BtnSubproject.Text = "Remove from Project" Then
            Dim thisdataDoc As New CommandsSqlAndOleDb("DelRequestFromProject", Session("CSCConn"))
            thisdataDoc.AddSqlProcParameter("@client_num", entryclientnumLabel.Text, SqlDbType.Int, 20)
            thisdataDoc.AddSqlProcParameter("@request_num", RequestNum, SqlDbType.Int, 20)
            thisdataDoc.AddSqlProcParameter("@projectNum", ProjectNumLabel.Text, SqlDbType.Int, 20)
            thisdataDoc.AddSqlProcParameter("@tech_num", WhoTechnumLabel.Text, SqlDbType.Int, 20)
            thisdataDoc.AddSqlProcParameter("@ntlogin", WhoNtloginLabel.Text, SqlDbType.NVarChar, 50)

            thisdataDoc.ExecNonQueryNoReturn()

        Else
            Dim thisdataDoc As New CommandsSqlAndOleDb("InsRequestToProject", Session("CSCConn"))
            thisdataDoc.AddSqlProcParameter("@client_num", entryclientnumLabel.Text, SqlDbType.Int, 20)
            thisdataDoc.AddSqlProcParameter("@request_num", RequestNum, SqlDbType.Int, 20)
            thisdataDoc.AddSqlProcParameter("@projectNum", ProjectNumLabel.Text, SqlDbType.Int, 20)
            thisdataDoc.AddSqlProcParameter("@tech_num", WhoTechnumLabel.Text, SqlDbType.Int, 20)
            thisdataDoc.AddSqlProcParameter("@ntlogin", WhoNtloginLabel.Text, SqlDbType.NVarChar, 50)


            thisdataDoc.ExecNonQueryNoReturn()


        End If

        Response.Redirect("./ViewRequestTicket.aspx?RequestNum=" & RequestNum, True)


    End Sub

    Protected Sub Projectsddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles Projectsddl.SelectedIndexChanged

        If Projectsddl.SelectedIndex > -1 Then

            ProjectNumLabel.Text = Projectsddl.SelectedValue
        End If

    End Sub

    'Protected Sub ajaxFileUpload_OnUploadComplete(sender As Object, e As AjaxControlToolkit.AjaxFileUploadEventArgs)

    '    htmlEdExActivity.AjaxFileUpload.SaveAs("\\backup02\\Library\\Support\\Customer Service Center\\Screenshots\\brep02\\" + e.FileName)
    '    e.PostedUrl = Page.ResolveUrl("\\backup02\\Library\\Support\\Customer Service Center\\Screenshots\\brep02\\" + e.FileName)

    'End Sub

    Protected Sub btnacknoledge_Click(sender As Object, e As System.EventArgs) Handles btnacknoledge.Click
        Dim thisData As New CommandsSqlAndOleDb("updTicketAckTechV7", Session("CSCConn"))
        thisData.AddSqlProcParameter("@request_num", RequestNum, SqlDbType.Int)
        thisData.AddSqlProcParameter("@tech_num", UserInfo.TechNum, SqlDbType.Int)

        thisData.ExecNonQueryNoReturn()

        Response.Redirect("./ViewRequestTicket.aspx?RequestNum=" & RequestNum, True)

        'FormSignOn.FillForm()
        'StatusForm.FillForm()


        'btnacknoledge.Visible = False


    End Sub
    Protected Sub btnSubmit_Click(sender As Object, e As System.EventArgs) Handles btnSubmit.Click

        If currstatuscdLabel.Text = "resols" Then

            If btnSubmit.Text = "ReOpen Ticket/RFS" Then

                ' reopen ticket
                ' Check for P1


                'Then submit reopen
                Dim thisData As New CommandsSqlAndOleDb("updReopenRequestV7", Session("CSCConn"))

                thisData.AddSqlProcParameter("@request_num", RequestNum, SqlDbType.Int)
                thisData.AddSqlProcParameter("@tech_num", UserInfo.TechNum, SqlDbType.Int)
                thisData.AddSqlProcParameter("@nt_login", UserInfo.NtLogin, SqlDbType.NVarChar)

                thisData.ExecNonQueryNoReturn()
                txtActivity.Text = ""
                'fillActivityTable()

                If priorityrbl.SelectedValue = "1" Then
                    'Send p1 email

                    SendP1Email(RequestNum, 1, "reopen")
                    SendGroupP1Email(RequestNum, "reopen")

                End If

                Response.Redirect("./ViewRequestTicket.aspx?RequestNum=" & RequestNum, True)

            End If


        End If


        CheckRequiredFields()

        If num_of_terminalsddl.SelectedIndex = 0 Then
            num_of_terminalsddl.SelectedValue = Nothing

        End If
        If num_of_printersddl.SelectedIndex = 0 Then
            num_of_printersddl.SelectedValue = Nothing
        End If

        If num_of_pcddl.SelectedIndex = 0 Then
            num_of_pcddl.SelectedValue = Nothing
        End If

        If num_of_net_dropsddl.SelectedIndex = 0 Then
            num_of_net_dropsddl.SelectedValue = Nothing
        End If

        If num_of_otherddl.SelectedIndex = 0 Then
            num_of_otherddl.SelectedValue = Nothing

        End If

        'If alternatecontactnameTextBox.Text <> "" Then

        '    If (Regex.IsMatch(alternatecontactnameTextBox.Text, "^[\.\-A-Za-z]") = False) Then

        '        lblValidation.Text = "Alternate name can not have numbers."
        '        lblValidation.ForeColor = Color.Red

        '        alternatecontactnameTextBox.Focus()
        '        alternatecontactnameTextBox.BackColor() = Color.Red
        '        Exit Sub

        '    End If

        'End If

        'If alternatecontactphoneTextBox.Text <> "" Then

        '    If (Regex.IsMatch(alternatecontactphoneTextBox.Text, "^[0-9-.]") = False) Then

        '        lblValidation.Text = "Alternate Phone Number must only contain numbers."
        '        lblValidation.ForeColor = Color.Red

        '        alternatecontactphoneTextBox.Focus()
        '        alternatecontactphoneTextBox.BackColor() = Color.Red
        '        Exit Sub

        '    End If

        'End If

        ' City State zip and phone Fax 
        'If citytextbox.Text <> "" Then

        '    If (Regex.IsMatch(citytextbox.Text, "^[\.\-A-Za-z]") = False) Then

        '        lblValidation.Text = "City name can not have numbers."
        '        lblValidation.ForeColor = Color.Red
        '        ' citytextbox.Text = ""
        '        citytextbox.Focus()

        '        'lblValidation.Focus()
        '        citytextbox.BackColor() = Color.Red
        '        Exit Sub

        '    End If

        'End If

        'If statetextbox.Text <> "" Then

        '    If (Regex.IsMatch(statetextbox.Text, "^[\.\-A-Za-z]") = False) Then

        '        lblValidation.Text = "State name can not have numbers."
        '        lblValidation.ForeColor = Color.Red
        '        'statetextbox.Text = ""

        '        statetextbox.Focus()
        '        statetextbox.BackColor() = Color.Red
        '        Exit Sub

        '    End If

        'End If

        'If phoneTextBox.Text <> "" Then

        '    If (Regex.IsMatch(phoneTextBox.Text, "^[0-9-.]") = False) Then

        '        lblValidation.Text = "Phone Number must only contain numbers."
        '        lblValidation.ForeColor = Color.Red
        '        phoneTextBox.Text = ""

        '        phoneTextBox.Focus()
        '        phoneTextBox.BackColor() = Color.Red
        '        Exit Sub

        '    End If

        'End If


        'If ziptextbox.Text <> "" Then

        '    If (Regex.IsMatch(ziptextbox.Text, "^[0-9]") = False) Then

        '        lblValidation.Text = "Zip Code can only contain numbers."
        '        lblValidation.ForeColor = Color.Red
        '        'ziptextbox.Text = ""

        '        ziptextbox.Focus()
        '        ziptextbox.BackColor() = Color.Red
        '        Exit Sub

        '    End If

        'End If



        If bHasError = True Then
            Exit Sub
        End If

        If categorycdLabel.Text = "" Then
            lblValidation.Text = "Must supply Category"
            lblValidation.ForeColor = Color.Red
            lblValidation.Focus()

            categorycdddl.ForeColor = Color.Red
            Exit Sub
        End If

        If typecdLabel.Text = "" Then

            lblValidation.Text = "Must supply Type"
            lblValidation.ForeColor = Color.Red
            lblValidation.Focus()

            typecdddl.ForeColor = Color.Red
            Exit Sub
        End If

        If shortdescTextbox.Text = "" Then
            If typecdLabel.Text.ToLower = "ticket" Then

                lblValidation.Text = "Must enter a Short Description"
                lblValidation.ForeColor = Color.Red

                lblValidation.Focus()
                shortdescTextbox.BackColor() = Color.Red
                Return

            End If

        End If

        If categorycdLabel.Text = "reports" Then


            'FormSignOn.AddInsertParm("@HDservicedesc", DataFieldsTextBox.Text, SqlDbType.NVarChar, 1000)

            FormSignOn.AddInsertParm("@HDterminalinfodesc", DataFieldsTextBox.Text, SqlDbType.NVarChar, 500)
            FormSignOn.AddInsertParm("@HDprinterinfodesc", DateRangeTextBox.Text, SqlDbType.NVarChar, 500)
            '@HDprinterinfodesc
            FormSignOn.AddInsertParm("@HDpcinfodesc", TotalOnfieldTextBox.Text, SqlDbType.NVarChar, 500)
            '@HDpcinfodesc
            FormSignOn.AddInsertParm("@HDnetdropsinfodesc", ReportDescTextBox.Text, SqlDbType.NVarChar, 500)
            FormSignOn.AddInsertParm("@HDotherinfodesc", reportOtherDescTextBox.Text, SqlDbType.NVarChar, 500)


        Else



            FormSignOn.AddInsertParm("@HDterminalinfodesc", terminal_info_descTextBox.Text, SqlDbType.NVarChar, 500)
            FormSignOn.AddInsertParm("@HDprinterinfodesc", printer_info_descTextBox.Text, SqlDbType.NVarChar, 500)

            FormSignOn.AddInsertParm("@HDpcinfodesc", pc_info_descTextBox.Text, SqlDbType.NVarChar, 500)
            FormSignOn.AddInsertParm("@HDnetdropsinfodesc", net_drops_info_descTextBox.Text, SqlDbType.NVarChar, 500)
            FormSignOn.AddInsertParm("@HDotherinfodesc", other_info_descTextBox.Text, SqlDbType.NVarChar, 500)


        End If

        FormSignOn.AddInsertParm("@request_num", RequestNum, SqlDbType.Int, 9)
        FormSignOn.AddInsertParm("@HDActivityDesc", ActivityDescTextbox.Text, SqlDbType.NVarChar, 1000)
        FormSignOn.AddInsertParm("@HDservicedesc", servicedescTextbox.Text, SqlDbType.NVarChar, 1000)
        FormSignOn.AddInsertParm("@HDbusinessdes", businessdesTextBox.Text, SqlDbType.NVarChar, 500)
        FormSignOn.AddInsertParm("@HDimpactdesc", impactdescTextBox.Text, SqlDbType.NVarChar, 500)


        FormSignOn.UpdateForm()

        SubmitRequestItems()

        If origionalGroupNumLable.Text <> changegroupddl.SelectedValue Then

            SendEmailtoClientGroupAssig(RequestNum, changegroupddl.SelectedItem.ToString)
            SendEmailtoTechsGroupAssign(RequestNum, changegroupddl.SelectedValue, changegroupddl.SelectedItem.ToString)

            origionalGroupNumLable.Text = changegroupddl.SelectedValue.ToString

        End If


        Dim iPriority As Integer

        'P1 inquiry 
        If hdPriorityLb.Text <> priorityrbl.SelectedValue Then

            If hdPriorityLb.Text = "1" Then
                'send desclation email
                iPriority = hdPriorityLb.Text


                SendP1Email(RequestNum, iPriority, "desclation")
                SendGroupP1Email(RequestNum, "desclation")
                HDSendP1email.Text = "no"

            End If
            If priorityrbl.SelectedValue = "1" Then
                'Send p1 email
                iPriority = 1
                'SendP1Email(RequestNum, iPriority, "esclation")

                SendP1Email(RequestNum, iPriority, "new")
                HDSendP1email.Text = "no"
                SendGroupP1Email(RequestNum, "new")


            End If

            'else who give ashit

        End If

        If hdPriorityLb.Text = priorityrbl.SelectedValue Then
            If esclationlbl.Text = "escaltion" Then
                SendP1Email(RequestNum, iPriority, "esclation")
                SendGroupP1Email(RequestNum, "esclation")

                HDSendP1email.Text = "no"
            End If

            If esclationlbl.Text = "desclation" Then
                SendP1Email(RequestNum, iPriority, "desclation")
                SendGroupP1Email(RequestNum, "desclation")

                HDSendP1email.Text = "no"
            End If
        End If

        If HDSendP1email.Text = "yes" Then
            ' group has changed
            SendP1Email(RequestNum, "1", "groupassign")

        End If

        If currenttechnumLabel.Text <> "" Then

            If CInt(currenttechnumLabel.Text) > 0 Then

                SendTechEmail(RequestNum, iPriority)

            End If

        End If

        Dim sDate As DateTime = Date.Now()

        lblValidation.Text = "Information Updated at -> " & sDate

        Response.Redirect("~/" & "ViewRequestTicket.aspx?RequestNum=" & RequestNum & "&label=" & lblValidation.Text, True)


        '    Dim frmHelperRequestEntry As New FormHelper()
        '    frmHelperRequestEntry.InitializeUpdateForm("UpdRequestEntry", Page)
        'frmHelperRequestEntry.AddCmdParameter("@SubmitterClientNum", UserInfo.ClientNum)
        'frmHelperRequestEntry.AddCmdParameter("@RequestNum", RequestNum)
        'frmHelperRequestEntry.UpdateForm()

        '    Dim frmHelperRequestTicket As New FormHelper()
        '    frmHelperRequestTicket.InitializeUpdateForm("InsUpdRequestTicket", Page)
        'frmHelperRequestTicket.AddCmdParameter("@RequestNum", RequestNum)
        '    frmHelperRequestTicket.UpdateForm()

    End Sub

    Protected Sub btnRemove_Click(sender As Object, e As System.EventArgs) Handles btnRemove.Click
        'check for file to upload
        'Panel1.Visible = True
        'filediv.Visible = True

        Dim thisData As New CommandsSqlAndOleDb("delFileRequest", Session("CSCConn"))

        thisData.AddSqlProcParameter("@request_num", RequestNum, SqlDbType.Int)
        thisData.ExecNonQueryNoReturn()

        Fileview.Visible = True
        btnOpenFile2.Visible = False
        btnRemove.Visible = False
        btnAddFile.Visible = True

        ' Response.Redirect("~/" & "ViewRequestTicket.aspx?RequestNum=" & RequestNum, True)

        'ModalFilePop.Show()

    End Sub
    Protected Sub SubmitRequestItems()

        'facilities
        For Each cblFactem As ListItem In cblFacilities.Items
            'Dim sCurrFacitem As String = cblFactem.Value


            If cblFactem.Selected = True Then


                Dim SubmitData As New CommandsSqlAndOleDb("InsRequestItems", Session("CSCConn"))
                SubmitData.AddSqlProcParameter("@request_num", requestnumlabel.Text, SqlDbType.Int, 9)
                SubmitData.AddSqlProcParameter("@item_type_cd", "facility", SqlDbType.NVarChar, 12)
                SubmitData.AddSqlProcParameter("@item_detail", cblFactem.Value, SqlDbType.NVarChar, 50)
                SubmitData.AddSqlProcParameter("@item_desc", cblFactem.Text, SqlDbType.NVarChar, 150)

                SubmitData.ExecNonQueryNoReturn()


            End If

        Next

        ' district codes
        For Each cblDistitem As ListItem In cblDistrictCodes.Items
            'Dim sCurrFacitem As String = cblDistitem.Value
            'Dim sCurrFacDesc As String = cblDistitem.Selected

            If cblDistitem.Selected = True Then


                Dim SubmitData As New CommandsSqlAndOleDb("InsRequestItems", Session("CSCConn"))
                SubmitData.AddSqlProcParameter("@request_num", requestnumlabel.Text, SqlDbType.Int, 9)
                SubmitData.AddSqlProcParameter("@item_type_cd", "district", SqlDbType.NVarChar, 12)
                SubmitData.AddSqlProcParameter("@item_detail", cblDistitem.Value, SqlDbType.NVarChar, 50)
                SubmitData.AddSqlProcParameter("@item_desc", cblDistitem.Text, SqlDbType.NVarChar, 150)

                SubmitData.ExecNonQueryNoReturn()


            End If

        Next

        ' frequency 
        'ReportFreqddl

        If ReportFreqTextBox.Text <> "" Then


            Dim SubmitData As New CommandsSqlAndOleDb("InsRequestItems", Session("CSCConn"))
            SubmitData.AddSqlProcParameter("@request_num", requestnumlabel.Text, SqlDbType.Int, 9)
            SubmitData.AddSqlProcParameter("@item_type_cd", "frequency", SqlDbType.NVarChar, 12)
            SubmitData.AddSqlProcParameter("@item_detail", ReportFreqTextBox.Text, SqlDbType.NVarChar, 50)
            SubmitData.AddSqlProcParameter("@item_desc", ReportFreqDescTextBox.Text, SqlDbType.NVarChar, 150)

            SubmitData.ExecNonQueryNoReturn()


        End If


        ' report type 

        If ReportTypeTextBox.Text <> "" Then


            Dim SubmitData As New CommandsSqlAndOleDb("InsRequestItems", Session("CSCConn"))
            SubmitData.AddSqlProcParameter("@request_num", requestnumlabel.Text, SqlDbType.Int, 9)
            SubmitData.AddSqlProcParameter("@item_type_cd", "rptType", SqlDbType.NVarChar, 12)
            SubmitData.AddSqlProcParameter("@item_detail", ReportTypeTextBox.Text, SqlDbType.NVarChar, 50)
            SubmitData.AddSqlProcParameter("@item_desc", ReportTypeDescTextBox.Text, SqlDbType.NVarChar, 150)

            SubmitData.ExecNonQueryNoReturn()


        End If

    End Sub
    Protected Sub cblFacilities_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cblFacilities.SelectedIndexChanged

        'facilities
        For Each cblFactem As ListItem In cblFacilities.Items
            'Dim sCurrFacitem As String = cblFactem.Value


            If cblFactem.Selected = False Then


                Dim SubmitData As New CommandsSqlAndOleDb("DeleteRequestItem", Session("CSCConn"))
                SubmitData.AddSqlProcParameter("@request_num", RequestNumLabel.Text, SqlDbType.Int, 9)
                SubmitData.AddSqlProcParameter("@item_type_cd", "facility", SqlDbType.NVarChar, 12)
                SubmitData.AddSqlProcParameter("@item_detail", cblFacilities.SelectedValue, SqlDbType.NVarChar, 50)


                SubmitData.ExecNonQueryNoReturn()

            ElseIf cblFactem.Selected = True Then


                Dim SubmitData As New CommandsSqlAndOleDb("InsRequestItems", Session("CSCConn"))
                SubmitData.AddSqlProcParameter("@request_num", RequestNumLabel.Text, SqlDbType.Int, 9)
                SubmitData.AddSqlProcParameter("@item_type_cd", "facility", SqlDbType.NVarChar, 12)
                SubmitData.AddSqlProcParameter("@item_detail", cblFactem.Value, SqlDbType.NVarChar, 50)
                SubmitData.AddSqlProcParameter("@item_desc", cblFactem.Text, SqlDbType.NVarChar, 150)

                SubmitData.ExecNonQueryNoReturn()


            End If

        Next

        Dim sDate As DateTime = Date.Now()

        lblValidation.Text = "Information Updated at -> " & sDate


    End Sub
    Protected Sub cblDistrictCodes_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cblDistrictCodes.SelectedIndexChanged

        ' district codes
        For Each cblDistitem As ListItem In cblDistrictCodes.Items
            'Dim sCurrFacitem As String = cblDistitem.Value
            'Dim sCurrFacDesc As String = cblDistitem.Selected

            If cblDistitem.Selected = False Then


                Dim SubmitData As New CommandsSqlAndOleDb("DeleteRequestItem", Session("CSCConn"))
                SubmitData.AddSqlProcParameter("@request_num", RequestNumLabel.Text, SqlDbType.Int, 9)
                SubmitData.AddSqlProcParameter("@item_type_cd", "district", SqlDbType.NVarChar, 12)
                SubmitData.AddSqlProcParameter("@item_detail", cblDistitem.Value, SqlDbType.NVarChar, 50)

                SubmitData.ExecNonQueryNoReturn()

            ElseIf cblDistitem.Selected = True Then


                Dim SubmitData As New CommandsSqlAndOleDb("InsRequestItems", Session("CSCConn"))
                SubmitData.AddSqlProcParameter("@request_num", RequestNumLabel.Text, SqlDbType.Int, 9)
                SubmitData.AddSqlProcParameter("@item_type_cd", "district", SqlDbType.NVarChar, 12)
                SubmitData.AddSqlProcParameter("@item_detail", cblDistitem.Value, SqlDbType.NVarChar, 50)
                SubmitData.AddSqlProcParameter("@item_desc", cblDistitem.Text, SqlDbType.NVarChar, 150)

                SubmitData.ExecNonQueryNoReturn()


            End If


        Next

        Dim sDate As DateTime = Date.Now()

        lblValidation.Text = "Information Updated at -> " & sDate


    End Sub

    Protected Sub ReportTypeddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ReportTypeddl.SelectedIndexChanged

        If ReportTypeTextBox.Text <> ReportTypeddl.SelectedValue Then
            Dim DelData As New CommandsSqlAndOleDb("DeleteRequestItem", Session("CSCConn"))
            DelData.AddSqlProcParameter("@request_num", RequestNumLabel.Text, SqlDbType.Int, 9)
            DelData.AddSqlProcParameter("@item_type_cd", "rptType", SqlDbType.NVarChar, 12)
            DelData.AddSqlProcParameter("@item_detail", ReportTypeTextBox.Text, SqlDbType.NVarChar, 50)

            DelData.ExecNonQueryNoReturn()


        End If

        If ReportTypeTextBox.Text <> "" Then

            ReportTypeTextBox.Text = ReportTypeddl.SelectedValue
            ReportTypeDescTextBox.Text = ReportTypeddl.SelectedItem.ToString

        ElseIf ReportTypeddl.SelectedValue.ToLower = "select" Then
            ReportTypeTextBox.Text = Nothing
            ReportTypeDescTextBox.Text = Nothing

        Else

            ReportTypeTextBox.Text = ReportTypeddl.SelectedValue
            ReportTypeDescTextBox.Text = ReportTypeddl.SelectedItem.ToString

        End If

        If ReportTypeTextBox.Text <> "select" Then


            Dim SubmitData As New CommandsSqlAndOleDb("InsRequestItems", Session("CSCConn"))
            SubmitData.AddSqlProcParameter("@request_num", RequestNumLabel.Text, SqlDbType.Int, 9)
            SubmitData.AddSqlProcParameter("@item_type_cd", "rptType", SqlDbType.NVarChar, 12)
            SubmitData.AddSqlProcParameter("@item_detail", ReportTypeTextBox.Text, SqlDbType.NVarChar, 50)
            SubmitData.AddSqlProcParameter("@item_desc", ReportTypeDescTextBox.Text, SqlDbType.NVarChar, 150)

            SubmitData.ExecNonQueryNoReturn()

            Dim sDate As DateTime = Date.Now()

            lblValidation.Text = "Information Updated at -> " & sDate


        End If

    End Sub
    Protected Sub ReportFreqddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ReportFreqddl.SelectedIndexChanged

        If ReportFreqTextBox.Text <> ReportFreqddl.SelectedValue Then
            Dim DelData As New CommandsSqlAndOleDb("DeleteRequestItem", Session("CSCConn"))
            DelData.AddSqlProcParameter("@request_num", RequestNumLabel.Text, SqlDbType.Int, 9)
            DelData.AddSqlProcParameter("@item_type_cd", "frequency", SqlDbType.NVarChar, 12)
            DelData.AddSqlProcParameter("@item_detail", ReportFreqTextBox.Text, SqlDbType.NVarChar, 50)

            DelData.ExecNonQueryNoReturn()


        End If

        If ReportFreqTextBox.Text <> "" Then

            ReportFreqTextBox.Text = ReportFreqddl.SelectedValue
            ReportFreqDescTextBox.Text = ReportFreqddl.SelectedItem.ToString

        ElseIf ReportTypeddl.SelectedValue.ToLower = "select" Then
            ReportFreqTextBox.Text = Nothing
            ReportFreqDescTextBox.Text = Nothing

        Else

            ReportFreqTextBox.Text = ReportFreqddl.SelectedValue
            ReportFreqDescTextBox.Text = ReportFreqddl.SelectedItem.ToString

        End If

        If ReportFreqTextBox.Text.ToLower <> "select" Then


            Dim SubmitData As New CommandsSqlAndOleDb("InsRequestItems", Session("CSCConn"))
            SubmitData.AddSqlProcParameter("@request_num", RequestNumLabel.Text, SqlDbType.Int, 9)
            SubmitData.AddSqlProcParameter("@item_type_cd", "frequency", SqlDbType.NVarChar, 12)
            SubmitData.AddSqlProcParameter("@item_detail", ReportFreqTextBox.Text, SqlDbType.NVarChar, 50)
            SubmitData.AddSqlProcParameter("@item_desc", ReportFreqDescTextBox.Text, SqlDbType.NVarChar, 150)

            SubmitData.ExecNonQueryNoReturn()


        End If
        Dim sDate As DateTime = Date.Now()

        lblValidation.Text = "Information Updated at -> " & sDate



    End Sub
    Protected Sub btnAddFile_Click(sender As Object, e As System.EventArgs) Handles btnAddFile.Click
        ModalFilePop.Show()

    End Sub
    Protected Sub FileComplete()
        Dim fileOK As Boolean

        fileName = fileUpload2.FileName

        filenamelbl.Text = fileName

        filetype = fileUpload2.PostedFile.ContentType


        fileExtension = System.IO.Path. _
            GetExtension(fileUpload2.FileName).ToLower()

        Dim allowedExtensions As String() = _
            {".jpg", ".jpeg", ".png", ".gif", ".doc", ".docx", ".pdf", ".txt", ".xls", ".xlsx", ".csv"}
        For i As Integer = 0 To allowedExtensions.Length - 1
            If fileExtension = allowedExtensions(i) Then
                fileOK = True
            End If
        Next
        If fileOK Then
            Try

                savePath += fileUpload2.FileName

                fileUpload2.SaveAs(savePath)

                fileUpload2.SaveAs(saveImage)


                'Select Case fileExtension
                '    Case ".jpg", ".jpeg", ".png", ".gif"
                '        requestImg2.Visible = True
                'End Select

                Dim fs As FileStream = New FileStream(savePath, FileMode.Open, FileAccess.Read)
                Dim br As BinaryReader = New BinaryReader(fs)
                Dim bites As Byte() = br.ReadBytes(Convert.ToInt32(fs.Length))


                br.Close()
                fs.Close()

                btnFilComplete.Enabled = True

                'ModalFilePop.Hide()

                'DisplayFile()  View File

                Using SqlConnection As New SqlConnection(Session("CSCConn"))

                    Dim Cmd As New SqlCommand()
                    Cmd.CommandText = "InsRequestFiles"

                    Cmd.Parameters.Add("@request_num", SqlDbType.NVarChar).Value = RequestNumLabel.Text

                    Cmd.Parameters.Add("@FileType", SqlDbType.NVarChar).Value = filetype
                    Cmd.Parameters.Add("@FileName", SqlDbType.NVarChar).Value = filenamelbl.Text

                    Cmd.Parameters.Add("@FileContent", SqlDbType.VarBinary).Value = bites

                    Cmd.CommandType = CommandType.StoredProcedure
                    Cmd.Connection = SqlConnection

                    SqlConnection.Open()
                    Cmd.ExecuteNonQuery()

                    'requestImage.ResolveUrl = bitmap
                    SqlConnection.Close()
                    SqlConnection.Dispose()



                End Using

                File.Delete(filenamelbl.Text)


            Catch ex As Exception
                messagelbl.Text = "File can not be uploaded."
                messagelbl.Font.Size = FontUnit.Large
                messagelbl.BackColor() = Color.Red
                messagelbl.ForeColor = Color.White
                messagelbl.BorderStyle() = BorderStyle.Solid
                messagelbl.BorderWidth = Unit.Pixel(2)

                ModalFilePop.Hide()

            End Try
        Else
            messagelbl.Visible = True
            messagelbl.Text = "Cannot accept files of this type or File does not exists or File not Loaded"
            messagelbl.Font.Size = FontUnit.Large
            messagelbl.BackColor() = Color.Red
            messagelbl.ForeColor = Color.White
            messagelbl.BorderStyle() = BorderStyle.Solid
            messagelbl.BorderWidth = Unit.Pixel(2)


            ModalFilePop.Hide()

        End If

    End Sub

    Protected Sub fileUpload2_UploadedComplete(sender As Object, e As AjaxControlToolkit.AsyncFileUploadEventArgs) Handles fileUpload2.UploadedComplete
        Dim fileOK As Boolean

        fileName = fileUpload2.FileName

        filenamelbl.Text = fileName

        filetype = fileUpload2.PostedFile.ContentType


        fileExtension = System.IO.Path. _
            GetExtension(fileUpload2.FileName).ToLower()

        Dim allowedExtensions As String() = _
            {".jpg", ".jpeg", ".png", ".gif", ".doc", ".docx", ".pdf", ".txt", ".xls", ".xlsx", ".csv", ".pptx"}
        For i As Integer = 0 To allowedExtensions.Length - 1
            If fileExtension = allowedExtensions(i) Then
                fileOK = True
            End If
        Next
        If fileOK Then
            Try

                savePath += fileUpload2.FileName

                fileUpload2.SaveAs(savePath)

                fileUpload2.SaveAs(saveImage)


                'Select Case fileExtension
                '    Case ".jpg", ".jpeg", ".png", ".gif"
                '        requestImg2.Visible = True
                'End Select

                Dim fs As FileStream = New FileStream(savePath, FileMode.Open, FileAccess.Read)
                Dim br As BinaryReader = New BinaryReader(fs)
                Dim bites As Byte() = br.ReadBytes(Convert.ToInt32(fs.Length))

                Dim fsize As Integer = Convert.ToInt32(fs.Length)


                br.Close()
                fs.Close()


                btnFilComplete.Enabled = True

                'ModalFilePop.Hide()

                'DisplayFile()  View File

                'Using SqlConnection As New SqlConnection(Session("CSCConn"))

                '    Dim Cmd As New SqlCommand()
                '    Cmd.CommandText = "InsRequestFiles"

                '    Cmd.Parameters.Add("@request_num", SqlDbType.NVarChar).Value = RequestNumLabel.Text

                '    Cmd.Parameters.Add("@FileType", SqlDbType.NVarChar).Value = filetype
                '    Cmd.Parameters.Add("@FileName", SqlDbType.NVarChar).Value = filenamelbl.Text

                '    Cmd.Parameters.Add("@FileContent", SqlDbType.VarBinary).Value = bites

                '    Cmd.CommandType = CommandType.StoredProcedure
                '    Cmd.Connection = SqlConnection

                '    SqlConnection.Open()
                '    Cmd.ExecuteNonQuery()

                '    'requestImage.ResolveUrl = bitmap
                '    SqlConnection.Close()
                '    SqlConnection.Dispose()

                '    ModalFilePop.Hide()

                '    Response.Redirect("./ViewRequestTicket.aspx?RequestNum=" & RequestNum, True)


                'End Using

                File.Delete(filenamelbl.Text)


            Catch ex As Exception
                messagelbl.Text = "File can not be uploaded."
                messagelbl.Font.Size = FontUnit.Large
                messagelbl.BackColor() = Color.Red
                messagelbl.ForeColor = Color.White
                messagelbl.BorderStyle() = BorderStyle.Solid
                messagelbl.BorderWidth = Unit.Pixel(2)

                ModalFilePop.Hide()

            End Try
            'Else
            '    messagelbl.Visible = True
            '    messagelbl.Text = "Cannot accept files of this type or File does not exists or File not Loaded"
            '    messagelbl.Font.Size = FontUnit.Large
            '    messagelbl.BackColor() = Color.Red
            '    messagelbl.ForeColor = Color.White
            '    messagelbl.BorderStyle() = BorderStyle.Solid
            '    messagelbl.BorderWidth = Unit.Pixel(2)


            '    ModalFilePop.Hide()

        End If
    End Sub
    Protected Sub HDBtnFile_Click(sender As Object, e As System.EventArgs) Handles HDBtnFile.Click
        ModalFilePop.Hide()
    End Sub

    Protected Sub btnCancelfile_Click(sender As Object, e As System.EventArgs) Handles btnCancelfile.Click

        ModalFilePop.Hide()

    End Sub
    Protected Sub btnFilComplete_Click(sender As Object, e As System.EventArgs) Handles btnFilComplete.Click

        Dim fileOK As Boolean

        fileName = fileUpload2.FileName

        filenamelbl.Text = fileName

        filetype = fileUpload2.PostedFile.ContentType


        fileExtension = System.IO.Path. _
            GetExtension(fileUpload2.FileName).ToLower()

        Dim allowedExtensions As String() = _
            {".jpg", ".jpeg", ".png", ".gif", ".doc", ".docx", ".pdf", ".txt", ".xls", ".xlsx", ".csv", ".pptx"}
        For i As Integer = 0 To allowedExtensions.Length - 1
            If fileExtension = allowedExtensions(i) Then
                fileOK = True
            End If
        Next
        If fileOK Then
            Try

                savePath += fileUpload2.FileName

                fileUpload2.SaveAs(savePath)

                fileUpload2.SaveAs(saveImage)


                'Select Case fileExtension
                '    Case ".jpg", ".jpeg", ".png", ".gif"
                '        requestImg2.Visible = True
                'End Select

                Dim fs As FileStream = New FileStream(savePath, FileMode.Open, FileAccess.Read)
                Dim br As BinaryReader = New BinaryReader(fs)
                Dim bites As Byte() = br.ReadBytes(Convert.ToInt32(fs.Length))
                Dim fsize As Integer = Convert.ToInt32(fs.Length)


                br.Close()
                fs.Close()

                btnFilComplete.Enabled = True


                If fsize > 7900000 Then
                    ModalFilePop.Hide()

                    messagelbl.Text = "File is too large."
                    messagelbl.Font.Size = FontUnit.Large
                    messagelbl.BackColor() = Color.Red
                    messagelbl.ForeColor = Color.White
                    messagelbl.BorderStyle() = BorderStyle.Solid
                    messagelbl.BorderWidth = Unit.Pixel(2)

                    Exit Sub
                End If
                'ModalFilePop.Hide()

                'DisplayFile()  View File

                Using SqlConnection As New SqlConnection(Session("CSCConn"))

                    Dim Cmd As New SqlCommand()
                    Cmd.CommandText = "InsRequestFiles"

                    Cmd.Parameters.Add("@request_num", SqlDbType.NVarChar).Value = RequestNumLabel.Text

                    Cmd.Parameters.Add("@FileType", SqlDbType.NVarChar).Value = filetype
                    Cmd.Parameters.Add("@FileName", SqlDbType.NVarChar).Value = filenamelbl.Text

                    Cmd.Parameters.Add("@FileContent", SqlDbType.VarBinary).Value = bites

                    Cmd.CommandType = CommandType.StoredProcedure
                    Cmd.Connection = SqlConnection

                    SqlConnection.Open()
                    Cmd.ExecuteNonQuery()

                    'requestImage.ResolveUrl = bitmap
                    SqlConnection.Close()
                    SqlConnection.Dispose()



                End Using

                File.Delete(filenamelbl.Text)

                Response.Redirect("./ViewRequestTicket.aspx?RequestNum=" & RequestNum, True)

            Catch ex As Exception
                Label1.Text = "File can not be uploaded."
                Label1.Font.Size = FontUnit.Large
                Label1.BackColor() = Color.Red
                Label1.ForeColor = Color.White
                Label1.BorderStyle() = BorderStyle.Solid
                Label1.BorderWidth = Unit.Pixel(2)

            End Try
            'Else
            '    Label1.Visible = True
            '    Label1.Text = "Cannot accept files of this type or File does not exists or File not Loaded"
            '    Label1.Font.Size = FontUnit.Large
            '    Label1.BackColor() = Color.Red
            '    Label1.ForeColor = Color.White
            '    Label1.BorderStyle() = BorderStyle.Solid
            '    Label1.BorderWidth = Unit.Pixel(2)

        End If


        ModalFilePop.Hide()

    End Sub
    Protected Sub btnOpenfile2_Click(sender As Object, e As System.EventArgs) Handles btnOpenFile2.Click

        Response.Redirect("./viewImage.aspx?RequestNum=" & RequestNum, True)

    End Sub
    Protected Sub SendP1Email(ByVal RequestNum As Integer, ByVal sPriority As Integer, ByVal sType As String)
        '======================
        ' Get current path
        '======================
        'If Session("LoginID").ToString.ToLower = "melej" Then

        Dim path As String
        Dim directory As String = ""
        Dim EmailList2 As String = ""
        Dim sSubject As String = ""
        Dim sBody As String
        Dim fulleMailList As String
        Dim toList As String()
        Dim i As Integer


        path = HttpContext.Current.Request.Url.AbsoluteUri
        Dim pathParts() As String = Split(path, "/")


        Do While i < pathParts.Length() - 1

            directory = directory + pathParts(i) + "/"
            i = i + 1

        Loop

        Mailmsg.To.Clear()




        Select Case sType
            Case "new"
                sSubject = "New "
            Case "desclation"
                sSubject = "De-Esclated "

            Case "esclation"
                sSubject = "Esclated "

            Case "reopen"
                sSubject = "Re-Open "
            Case "close"
                sSubject = "Resolved "
            Case "groupassign"
                sSubject = "Group Assignment "

        End Select

        If sSubject = "" Then
            sSubject = "P1 Ticket# " & RequestNum.ToString & " Unknow Status ?"

        Else
            sSubject = "P1 Ticket# " & RequestNum.ToString & " has been " & sSubject

        End If



        sBody = shortdescTextbox.Text & " "

        emaillist.getP1emaillist(Session("CSCConn"), RequestNum.ToString)

        EmailList2 = emaillist.EmailListReturn()


        'EmailList2 = New EmailListOne(RequestNum, Session("EmployeeConn")).EmailListReturn

        fulleMailList = EmailList2

        If Session("environment").ToString.ToLower = "testing" Then
            EmailList2 = "Jeff.Mele@crozer.org"
            sBody = sBody & fulleMailList
            Mailmsg.To.Add(New System.Net.Mail.MailAddress(EmailList2))

        Else


            'EmailList2 = "2672944056@vtext.com;6105177353@txt.att.net"
            toList = EmailList2.Split(";")
            If toList.Length = 1 Then

                Mailmsg.To.Add(New System.Net.Mail.MailAddress(toList(0)))


            Else
            End If
            For i = 0 To toList.Length - 1 ' - 2
                If toList(i).Length > 10 Then ' validity check
                    Mailmsg.To.Add(New System.Net.Mail.MailAddress(toList(i)))
                    Console.WriteLine(toList(i))
                End If
            Next

        End If

        'Dim mailObj As New MailMessage

        Mailmsg.From = New MailAddress("CSCemail@crozer.org")
        Mailmsg.IsBodyHtml = True

        Mailmsg.Subject = sSubject
        Mailmsg.Body = sBody

        Dim smtpClient As New SmtpClient()
        smtpClient.Host = "ah1smtprelay01.altahospitals.com"

        Try

            smtpClient.Send(Mailmsg)


            'Console.WriteLine(Mailmsg)

        Catch ex As Exception
            'Console.WriteLine(ex.Message)

        End Try

        'Insert record into P1Messages then 2 minute process will not send email to accounts
        Dim SendP1Data As New CommandsSqlAndOleDb("InsP1SendEmail", Session("CSCConn"))
        SendP1Data.AddSqlProcParameter("@requestnum", RequestNum, SqlDbType.Int)
        SendP1Data.AddSqlProcParameter("@requesttype", RequestTypeLabel.Text, SqlDbType.NChar, 8)
        SendP1Data.AddSqlProcParameter("@categorycd", categorycdLabel.Text, SqlDbType.NChar, 12)
        SendP1Data.AddSqlProcParameter("@shortDesc", shortdescTextbox.Text, SqlDbType.NChar, 100)
        SendP1Data.AddSqlProcParameter("@closeRecord", "yes", SqlDbType.NChar, 3)

        SendP1Data.ExecNonQueryNoReturn()


        'Insert into P1MessagesSentTo  show who received message
        Dim thisData As New CommandsSqlAndOleDb("InsSentP1Messages", Session("CSCConn"))
        thisData.AddSqlProcParameter("@requestnum", RequestNum, SqlDbType.Int)
        thisData.AddSqlProcParameter("@ToAddress", EmailList2, SqlDbType.NVarChar, 2000)
        thisData.AddSqlProcParameter("@subject", sSubject, SqlDbType.NVarChar, 200)
        thisData.AddSqlProcParameter("@body", sBody, SqlDbType.NVarChar, 4000)

        thisData.ExecNonQueryNoReturn()

        If sType = "close" Then

            Dim closeData As New CommandsSqlAndOleDb("CloseP1", Session("CSCConn"))
            closeData.AddSqlProcParameter("@requestnum", RequestNum, SqlDbType.Int)

        End If

        If sType = "desclation" Then

            Dim descData As New CommandsSqlAndOleDb("DeEsclatePriorityP1", Session("CSCConn"))
            descData.AddSqlProcParameter("@requestnum", RequestNum, SqlDbType.Int)
        End If




    End Sub
    Protected Sub SendGroupP1Email(ByVal RequestNum As Integer, ByVal sType As String)
        '======================
        ' Get current path
        '======================
        'If Session("LoginID").ToString.ToLower = "melej" Then

        Dim path As String
        Dim directory As String = ""
        Dim EmailList2 As String = ""
        Dim sSubject As String = ""
        Dim sBody As String
        Dim Link As String
        Dim fulleMailList As String
        Dim toList As String()
        Dim i As Integer


        path = HttpContext.Current.Request.Url.AbsoluteUri
        Dim pathParts() As String = Split(path, "/")


        Do While i < pathParts.Length() - 1

            directory = directory + pathParts(i) + "/"
            i = i + 1

        Loop

        Mailmsg.To.Clear()

        If Session("environment").ToString.ToLower = "testing" Then
            'toEmail = "Jeff.Mele@crozer.org"
            Link = "http://webdev3/CSCV2/ViewRequestTicket.aspx?RequestNum=" & RequestNum.ToString
        Else
            Link = "http://Casemgmt02/CSCV2/ViewRequestTicket.aspx?RequestNum=" & RequestNum.ToString

        End If


        'sSubject = "P1 Ticket# " & RequestNum.ToString

        'sBody = shortdescTextbox.Text & " "
        Select Case sType
            Case "new", ""
                sSubject = "New P1 Ticket #" & RequestNum
                If sType = "" Then
                    sType = "new"
                End If
            Case "desclation"
                sSubject = "P1 De-Esclated  #" & RequestNum

            Case "esclation"
                sSubject = "P1 Esclated #" & RequestNum

            Case "reopen"
                sSubject = "P1 Re-Open #" & RequestNum
            Case "close"
                sSubject = "P1 Resolved #" & RequestNum

        End Select

        If sSubject = "" Then
            sSubject = "P1 Ticket# " & RequestNum.ToString & " Unknow Status ?"

        End If



        sBody = "<!DOCTYPE html>" & _
        "<html>" & _
        "<head>" & _
        "<style>" & _
        "table" & _
        "{" & _
        "border-collapse:collapse;" & _
        "font-family: 'Helvetica Neue', 'Lucida Grande', 'Segoe UI', Arial, Helvetica, Verdana, sans-serif;" & _
        "}" & _
        "table, th, td" & _
        "{" & _
        "border: 1px solid black;" & _
        "padding: 3px" & _
        "}" & _
        "</style>" & _
        "</head>" & _
        "" & _
        "<body>" & _
        "<table cellpadding='5px'>" & _
                "<tr>" & _
                    "<td colspan='4' align='center'  style='font-size:large' color='red'>" & _
        "               " & _
                    "<b> Ticket/RFS </b>" & _
        "                 " & _
                     "</td>    " & _
                "</tr>" & _
                "<tr>" & _
                    "<td style='font-weight:bold' colspan='1'>Name:</td>" & _
                    "<td colspan='3'>" & first_nameTextBox.Text & " " & last_nameTextBox.Text & "</td>" & _
                "</tr>" & _
                "<tr>" & _
                    "<td style='font-weight:bold' colspan='1'>Request#:</td>" & _
                    "<td colspan='3'>" & RequestNum & "</td>" & _
                "</tr>" & _
                "<tr>" & _
                    "<td style='font-weight:bold' colspan='1'>Submitted By:</td>" & _
                    "<td colspan='3'>" & UserInfo.UserName & "</td>" & _
                "</tr>" & _
                   "<tr>" & _
                    "<td style='font-weight:bold' colspan='1'>Requestor Email:</td>" & _
                    "<td colspan='3'>" & emailTextBox.Text & "</td>" & _
                "</tr>" & _
                "<tr>" & _
                    "<td style='font-weight:bold' colspan='1'>Date Requested:</td>" & _
                    "<td colspan='3'>" & DateTime.Now.ToString("yyyy/MM/dd-hh:mm") & "</td>" & _
                "</tr>" & _
                "<tr>" & _
                    "<td style='font-weight:bold' colspan='4'>Request Information </td>" & _
                "</tr>" & _
        "      " & _
        "     " & _
        "<tr><td colspan='4'><b>Description:</b> " & shortdescTextbox.Text & "</td></tr>" & _
        "<tr><td> Request Type: " & RequestTyperbl.SelectedValue.ToString & "</td><td> Category: " & categorycdddl.SelectedValue.ToString & "</td><td> Type:" & typecdddl.SelectedValue.ToString & "</td><td> Item: " & itemcdddl.SelectedValue.ToString & "</td></tr>" & _
        "<tr><td colspan='2'> Phone: " & phoneTextBox.Text & "</td><td colspan='2'> Alternate Phone:" & alternatecontactphoneTextBox.Text & "</td></tr>" & _
        "<tr><td colspan='2'> eMail: " & emailTextBox.Text & "</td><td colspan='2'> IP Address: " & ipaddressTextBox.Text & "</td></tr>" & _
        "<tr><td> Location: " & FacilityCdeMailLabel.Text & "</td><td> Building: " & buildingEmailnameLabel.Text & "</td><td colspan='2'> Floor: " & floorEmailLabel.Text & "</td></tr>" & _
        "<tr><td colspan='2'> Address1: " & address_1textbox.Text & "</td><td colspan='2'> Address2: " & address_2textbox.Text & "</td></tr>" & _
        "<tr><td colspan='2'> City: " & citytextbox.Text & "</td><td>State: " & statetextbox.Text & "</td><td> Office: " & OfficeTextbox.Text & "</td></tr>" & _
        "<tr><td colspan='4'><table>" & _
        "<tr>" & _
            "<td align='center'  style='font-size:large'>" & _
"               " & _
            "<b>View Details by clicking on link below</b>" & _
"                 " & _
             "</td>    " & _
        "</tr>" & _
        "<tr>" & _
            "<td align='center'>" & Link & "</td>" & _
        "</tr>" & _
        "<tr>" & _
             "<td colspan='4' align='center'  style='font-size:large'>" & _
        "               " & _
                    "<b> DO NOT REPLY to This eMail</b>" & _
        "                 " & _
                     "</td>    " & _
                "</tr></table>" & _
         "</td></tr></table>" & _
        "</body>" & _
        "</html>"


        'SelGroupP1EmailV9

        emaillist.getGroupP1emaillist(Session("CSCConn"), RequestNum.ToString, sType)

        EmailList2 = emaillist.EmailListReturn()

        If EmailList2.ToString.ToLower = "csc@crozer.org" Then
            Exit Sub
        End If


        'EmailList2 = New EmailListOne(RequestNum, Session("EmployeeConn")).EmailListReturn

        fulleMailList = EmailList2

        If Session("environment").ToString.ToLower = "testing" Then
            EmailList2 = "Jeff.Mele@crozer.org"
            sBody = sBody & fulleMailList
            Mailmsg.To.Add(New System.Net.Mail.MailAddress(EmailList2))

        Else


            'EmailList2 = "2672944056@vtext.com;6105177353@txt.att.net"
            toList = EmailList2.Split(";")
            If toList.Length = 1 Then

                Mailmsg.To.Add(New System.Net.Mail.MailAddress(toList(0)))


            Else
            End If
            For i = 0 To toList.Length - 1 ' - 2
                If toList(i).Length > 10 Then ' validity check
                    Mailmsg.To.Add(New System.Net.Mail.MailAddress(toList(i)))
                    Console.WriteLine(toList(i))
                End If
            Next

        End If

        'Dim mailObj As New MailMessage

        Mailmsg.From = New MailAddress("CSCemail@crozer.org")
        Mailmsg.IsBodyHtml = True

        Mailmsg.Subject = sSubject
        Mailmsg.Body = sBody

        Dim smtpClient As New SmtpClient()
        smtpClient.Host = "ah1smtprelay01.altahospitals.com"

        Try

            smtpClient.Send(Mailmsg)


            'Console.WriteLine(Mailmsg)

        Catch ex As Exception
            'Console.WriteLine(ex.Message)

        End Try


    End Sub
    'Protected Overrides Sub FillToolBar()

    '    Dim tlBarBtn As New AjaxControlToolkit.HTMLEditor.ToolbarButton.MethodButton()
    '    tlBarBtn.NormalSrc = "Images/Contact23.png"
    '    '  htmlEdExActivity.Toolbar.Add(tlBarBtn)


    'End Sub

    'Protected Sub CategoryTypeCdddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles CategoryTypeCdddl.SelectedIndexChanged
    '    'ddlBinder = New DropDownListBinder(CategoryTypeCdddl, "sel_category_codes", Session("EmployeeConn"))

    '    'ddlBinder.BindData("category_num", "category_cd")

    'End Sub


    Protected Sub ChangeTechddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ChangeTechddl.SelectedIndexChanged
        If ChangeTechddl.SelectedIndex = 0 Then

            currTechlb.Text = "Current Tech:"
            currenttechnamelabel.Text = "No Tech Assigned"
            Exit Sub
        End If

        currTechlb.Text = "New Tech:"

        currenttechnumLabel.Text = ChangeTechddl.SelectedValue
        currenttechnamelabel.Text = ChangeTechddl.SelectedItem.ToString

        If hdPriorityLb.Text = "1" Then
            priorityrbl.ForeColor = Color.Red
            p1lbl.ForeColor = Color.Red

            btnSendTechP1.Visible = True

        Else

            priorityrbl.ForeColor = Color.Black
            p1lbl.ForeColor = Color.Black


        End If

        lblValidation.Text = "Please Update Request to Apply changes!"
        lblValidation.Visible = True

    End Sub
    Protected Sub priorityrbl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles priorityrbl.SelectedIndexChanged

        If hdPriorityLb.Text <> priorityrbl.SelectedValue Then
            If hdPriorityLb.Text = "1" Then
                'send desclation email
                esclationlbl.Text = "desclation"
            End If
            If priorityrbl.SelectedValue = "1" Then
                esclationlbl.Text = "escaltion"

                'Send p1 email
            End If
            'else who give ashit
        End If

        hdPriorityLb.Text = priorityrbl.SelectedValue

        If hdPriorityLb.Text = "1" Then
            priorityrbl.ForeColor = Color.Red
            p1lbl.ForeColor = Color.Red
            esclationlbl.Text = "escaltion"
            ' send p1 page

            'btnSendTechP1.Visible = True

        Else

            priorityrbl.ForeColor = Color.Black
            p1lbl.ForeColor = Color.Black

        End If

        lblValidation.Text = "Please Update Request to Apply changes!"
        lblValidation.Visible = True
    End Sub
    Protected Sub SendTechEmail(ByVal RequestNum As Integer, ByVal sPriority As Integer)
        '======================
        ' Get current path
        '======================
        'If Session("LoginID").ToString.ToLower = "melej" Then

        Dim path As String
        Dim Link As String
        Dim directory As String = ""
        Dim EmailList2 As String = ""
        Dim sSubject As String = ""
        Dim sBody As String = ""
        Dim fulleMailList As String
        Dim toList As String()
        Dim i As Integer
        Dim iTechnum As Integer

        iTechnum = CInt(currenttechnumLabel.Text)



        path = HttpContext.Current.Request.Url.AbsoluteUri
        Dim pathParts() As String = Split(path, "/")


        Do While i < pathParts.Length() - 1

            directory = directory + pathParts(i) + "/"
            i = i + 1

        Loop

        Mailmsg.To.Clear()



        If Session("environment").ToString.ToLower = "testing" Then
            'toEmail = "Jeff.Mele@crozer.org"
            Link = "http://webdev3/CSCV2/ViewRequestTicket.aspx?RequestNum=" & RequestNum.ToString
        Else
            Link = "http://Casemgmt02/CSCV2/ViewRequestTicket.aspx?RequestNum=" & RequestNum.ToString

        End If


        sSubject = "New Ticket/RFS Assigned to you #" & RequestNum


        sBody = "<!DOCTYPE html>" & _
        "<html>" & _
        "<head>" & _
        "<style>" & _
        "table" & _
        "{" & _
        "border-collapse:collapse;" & _
        "font-family: 'Helvetica Neue', 'Lucida Grande', 'Segoe UI', Arial, Helvetica, Verdana, sans-serif;" & _
        "}" & _
        "table, th, td" & _
        "{" & _
        "border: 1px solid black;" & _
        "padding: 3px" & _
        "}" & _
        "</style>" & _
        "</head>" & _
        "" & _
        "<body>" & _
        "<table cellpadding='5px'>" & _
                "<tr>" & _
                    "<td colspan='4' align='center'  style='font-size:large'>" & _
        "               " & _
                    "<b>Ticket/RFS </b>" & _
        "                 " & _
                     "</td>    " & _
                "</tr>" & _
                "<tr>" & _
                    "<td style='font-weight:bold' colspan='1'>Name:</td>" & _
                    "<td colspan='3'>" & first_nameTextBox.Text & " " & last_nameTextBox.Text & "</td>" & _
                "</tr>" & _
                "<tr>" & _
                    "<td style='font-weight:bold' colspan='1'>Request#:</td>" & _
                    "<td colspan='3'>" & RequestNum & "</td>" & _
                "</tr>" & _
                "<tr>" & _
                    "<td style='font-weight:bold' colspan='1'>Submitted By:</td>" & _
                    "<td colspan='3'>" & UserInfo.UserName & "</td>" & _
                "</tr>" & _
                   "<tr>" & _
                    "<td style='font-weight:bold' colspan='1'>Requestor Email:</td>" & _
                    "<td colspan='3'>" & emailTextBox.Text & "</td>" & _
                "</tr>" & _
                "<tr>" & _
                    "<td style='font-weight:bold' colspan='1'>Date Requested:</td>" & _
                    "<td colspan='3'>" & DateTime.Now.ToString("yyyy/MM/dd-hh:mm") & "</td>" & _
                "</tr>" & _
                "<tr>" & _
                    "<td style='font-weight:bold' colspan='4'>Request Information </td>" & _
                "</tr>" & _
        "      " & _
        "     " & _
        "<tr><td colspan='4'><b>Description:</b> " & shortdescTextbox.Text & "</td></tr>" & _
        "<tr><td> Request Type: " & RequestTyperbl.SelectedValue.ToString & "</td><td> Category: " & categorycdddl.SelectedValue.ToString & "</td><td> Type:" & typecdddl.SelectedValue.ToString & "</td><td> Item: " & itemcdddl.SelectedValue.ToString & "</td></tr>" & _
        "<tr><td colspan='2'> Phone: " & phoneTextBox.Text & "</td><td colspan='2'> Alternate Phone:" & alternatecontactphoneTextBox.Text & "</td></tr>" & _
        "<tr><td colspan='2'> eMail: " & emailTextBox.Text & "</td><td colspan='2'> IP Address: " & ipaddressTextBox.Text & "</td></tr>" & _
        "<tr><td> Location: " & FacilityCdeMailLabel.Text & "</td><td> Building: " & buildingEmailnameLabel.Text & "</td><td colspan='2'> Floor: " & floorEmailLabel.Text & "</td></tr>" & _
        "<tr><td colspan='2'> Address1: " & address_1textbox.Text & "</td><td colspan='2'> Address2: " & address_2textbox.Text & "</td></tr>" & _
        "<tr><td colspan='2'> City: " & citytextbox.Text & "</td><td>State: " & statetextbox.Text & "</td><td> Office: " & OfficeTextbox.Text & "</td></tr>" & _
        "<tr><td colspan='4'><table>" & _
        "<tr>" & _
            "<td align='center'  style='font-size:large'>" & _
"               " & _
            "<b>View Details by clicking on link below</b>" & _
"                 " & _
             "</td>    " & _
        "</tr>" & _
        "<tr>" & _
            "<td align='center'>" & Link & "</td>" & _
        "</tr>" & _
        "<tr>" & _
             "<td colspan='4' align='center'  style='font-size:large'>" & _
        "               " & _
                    "<b> DO NOT REPLY to This eMail</b>" & _
        "                 " & _
                     "</td>    " & _
                "</tr></table>" & _
         "</td></tr></table>" & _
        "</body>" & _
        "</html>"



        emaillist.getTechemaillist(Session("CSCConn"), "email", iTechnum)
        EmailList2 = emaillist.EmailListReturn()


        'EmailList2 = New EmailListOne(RequestNum, Session("EmployeeConn")).EmailListReturn

        fulleMailList = EmailList2

        'Session("environment") = "production"

        If Session("environment").ToString.ToLower = "testing" Then
            EmailList2 = "Jeff.Mele@crozer.org"
            ' sBody
            Mailmsg.To.Add(New System.Net.Mail.MailAddress(EmailList2))

        Else


            'EmailList2 = "2672944056@vtext.com;6105177353@txt.att.net"
            toList = EmailList2.Split(";")
            If toList.Length = 1 Then

                Mailmsg.To.Add(New System.Net.Mail.MailAddress(toList(0)))


            Else
            End If
            For i = 0 To toList.Length - 1 ' - 2
                If toList(i).Length > 10 Then ' validity check
                    Mailmsg.To.Add(New System.Net.Mail.MailAddress(toList(i)))
                    Console.WriteLine(toList(i))
                End If
            Next

        End If


        'Dim mailObj As New MailMessage

        Mailmsg.From = New MailAddress("CSCemail@crozer.org")
        Mailmsg.IsBodyHtml = True

        Mailmsg.Subject = sSubject
        Mailmsg.Body = sBody

        Dim smtpClient As New SmtpClient()
        smtpClient.Host = "ah1smtprelay01.altahospitals.com"

        Try

            smtpClient.Send(Mailmsg)


            'Console.WriteLine(Mailmsg)

        Catch ex As Exception
            'Console.WriteLine(ex.Message)

        End Try


        'Insert into P1MessagesSentTo  show who received message
        'Dim thisData As New CommandsSqlAndOleDb("InsSentP1Messages", Session("CSCConn"))
        'thisData.AddSqlProcParameter("@requestnum", RequestNum, SqlDbType.Int)
        'thisData.AddSqlProcParameter("@ToAddress", EmailList2, SqlDbType.NVarChar, 2000)
        'thisData.AddSqlProcParameter("@subject", sSubject, SqlDbType.NVarChar, 200)
        'thisData.AddSqlProcParameter("@body", sBody, SqlDbType.NVarChar, 4000)

        'thisData.ExecNonQueryNoReturn()



    End Sub

    Protected Sub btnSendTechP1_Click(sender As Object, e As System.EventArgs) Handles btnSendTechP1.Click
        Dim TechpageList As String = ""
        Dim TechEmailList As String = ""
        Dim Link As String
        Dim path As String
        Dim pathParts() As String = Split(path, "/")
        Dim directory As String = ""
        Dim i As Integer
        Dim sSubject As String
        Dim sBody As String
        Dim Fulllist As String = ""

        iTechNum = currenttechnumLabel.Text


        emaillist.getTechemaillist(Session("CSCConn"), "pagers", iTechNum)
        TechpageList = emaillist.EmailListReturn()


        emaillist.getTechemaillist(Session("CSCConn"), "email", iTechNum)
        TechEmailList = emaillist.EmailListReturn()


        Do While i < pathParts.Length() - 1

            directory = directory + pathParts(i) + "/"
            i = i + 1

        Loop

        Mailmsg.To.Clear()

        If Session("environment").ToString.ToLower = "testing" Then
            'toEmail = "Jeff.Mele@crozer.org"
            Link = "http://webdev3/CSCV2/ViewRequestTicket.aspx?RequestNum=" & RequestNum.ToString
        Else
            Link = "http://Casemgmt02/CSCV2/ViewRequestTicket.aspx?RequestNum=" & RequestNum.ToString

        End If


        sSubject = "New P1 Ticket #" & RequestNum & " has been Submitted  for " & first_nameTextBox.Text & " " & last_nameTextBox.Text


        sBody = "<!DOCTYPE html>" & _
        "<html>" & _
        "<head>" & _
        "<style>" & _
        "table" & _
        "{" & _
        "border-collapse:collapse;" & _
        "font-family: 'Helvetica Neue', 'Lucida Grande', 'Segoe UI', Arial, Helvetica, Verdana, sans-serif;" & _
        "}" & _
        "table, th, td" & _
        "{" & _
        "border: 1px solid black;" & _
        "padding: 3px" & _
        "}" & _
        "</style>" & _
        "</head>" & _
        "" & _
        "<body>" & _
        "<table cellpadding='5px'>" & _
                "<tr>" & _
                    "<td colspan='4' align='center'  style='font-size:large' color='red'>" & _
        "               " & _
                    "<b>P1 Ticket/RFS </b>" & _
        "                 " & _
                     "</td>    " & _
                "</tr>" & _
                "<tr>" & _
                    "<td style='font-weight:bold' colspan='1'>Name:</td>" & _
                    "<td colspan='3'>" & first_nameTextBox.Text & " " & last_nameTextBox.Text & "</td>" & _
                "</tr>" & _
                "<tr>" & _
                    "<td style='font-weight:bold' colspan='1'>Request#:</td>" & _
                    "<td colspan='3'>" & RequestNum & "</td>" & _
                "</tr>" & _
                "<tr>" & _
                    "<td style='font-weight:bold' colspan='1'>Submitted By:</td>" & _
                    "<td colspan='3'>" & UserInfo.UserName & "</td>" & _
                "</tr>" & _
                   "<tr>" & _
                    "<td style='font-weight:bold' colspan='1'>Requestor Email:</td>" & _
                    "<td colspan='3'>" & emailTextBox.Text & "</td>" & _
                "</tr>" & _
                "<tr>" & _
                    "<td style='font-weight:bold' colspan='1'>Date Requested:</td>" & _
                    "<td colspan='3'>" & DateTime.Now.ToString("yyyy/MM/dd-hh:mm") & "</td>" & _
                "</tr>" & _
                "<tr>" & _
                    "<td style='font-weight:bold' colspan='4'>Request Information </td>" & _
                "</tr>" & _
        "      " & _
        "     " & _
        "<tr><td colspan='4'><b>Description:</b> " & shortdescTextbox.Text & "</td></tr>" & _
        "<tr><td> Request Type: " & RequestTyperbl.SelectedValue.ToString & "</td><td> Category: " & categorycdddl.SelectedValue.ToString & "</td><td> Type:" & typecdddl.SelectedValue.ToString & "</td><td> Item: " & itemcdddl.SelectedValue.ToString & "</td></tr>" & _
        "<tr><td colspan='2'> Phone: " & phoneTextBox.Text & "</td><td colspan='2'> Alternate Phone:" & alternatecontactphoneTextBox.Text & "</td></tr>" & _
        "<tr><td colspan='2'> eMail: " & emailTextBox.Text & "</td><td colspan='2'> IP Address: " & ipaddressTextBox.Text & "</td></tr>" & _
        "<tr><td> Location: " & FacilityCdeMailLabel.Text & "</td><td> Building: " & buildingEmailnameLabel.Text & "</td><td colspan='2'> Floor: " & floorEmailLabel.Text & "</td></tr>" & _
        "<tr><td colspan='2'> Address1: " & address_1textbox.Text & "</td><td colspan='2'> Address2: " & address_2textbox.Text & "</td></tr>" & _
        "<tr><td colspan='2'> City: " & citytextbox.Text & "</td><td>State: " & statetextbox.Text & "</td><td> Office: " & OfficeTextbox.Text & "</td></tr>" & _
        "<tr><td colspan='4'><table>" & _
        "<tr>" & _
            "<td align='center'  style='font-size:large'>" & _
"               " & _
            "<b>View Details by clicking on link below</b>" & _
"                 " & _
             "</td>    " & _
        "</tr>" & _
        "<tr>" & _
            "<td align='center'>" & Link & "</td>" & _
        "</tr></table>" & _
        "</td></tr>" & _
        "</body>" & _
        "</html>"


        Fulllist = TechEmailList

        If Session("environment").ToString.ToLower = "testing" Then
            TechEmailList = "Jeff.Mele@crozer.org"
            sBody = sBody & TechEmailList

        Else
            'EmailListp1 = MgrEmailList
            'sBody = sBody & fulleMailList

        End If

        Mailmsg.To.Add(New System.Net.Mail.MailAddress(TechEmailList))
		Mailmsg.From = New MailAddress("ckhsitcsc@crozer.org")
		Mailmsg.IsBodyHtml = True

        Mailmsg.Subject = sSubject
        Mailmsg.Body = sBody

        Dim smtpClient As New SmtpClient()
        smtpClient.Host = "ah1smtprelay01.altahospitals.com"

        smtpClient.Send(Mailmsg)

        ' ********* now send Text to phone an pagers
        Do While i < pathParts.Length() - 1

            directory = directory + pathParts(i) + "/"
            i = i + 1

        Loop

        Mailmsg.To.Clear()

        sSubject = "New P1 Ticket# " & RequestNum.ToString & " has been Opened "
        sBody = shortdescTextbox.Text

        Fulllist = TechpageList

        If Session("environment").ToString.ToLower = "testing" Then
            TechpageList = "Jeff.Mele@crozer.org"
            sBody = sBody & " " & TechpageList

        Else
            'EmailListp1 = MgrEmailList
            'sBody = sBody & fulleMailList

        End If

        Mailmsg.To.Add(New System.Net.Mail.MailAddress(TechpageList))
		Mailmsg.From = New MailAddress("ckhsitcsc@crozer.org")
		Mailmsg.IsBodyHtml = True

        Mailmsg.Subject = sSubject
        Mailmsg.Body = sBody

        Dim smtpClientpager As New SmtpClient()
        smtpClientpager.Host = "ah1smtprelay01.altahospitals.com"

        'send email immeadatly
        smtpClientpager.Send(Mailmsg)



        'TechEmailList
        'Insert record into P1Messages then 2 minute process will not send email to accounts
        Dim SendP1Data As New CommandsSqlAndOleDb("InsP1SendEmail", Session("CSCConn"))
        SendP1Data.AddSqlProcParameter("@requestnum", RequestNum, SqlDbType.Int)
        SendP1Data.AddSqlProcParameter("@requesttype", RequestTyperbl.SelectedValue.ToString, SqlDbType.NChar, 8)
        SendP1Data.AddSqlProcParameter("@categorycd", categorycdddl.SelectedValue.ToString, SqlDbType.NChar, 12)
        SendP1Data.AddSqlProcParameter("@shortDesc", shortdescTextbox.Text, SqlDbType.NChar, 100)
        SendP1Data.AddSqlProcParameter("@closeRecord", "yes", SqlDbType.NChar, 3)

        SendP1Data.ExecNonQueryNoReturn()


        'Insert into P1MessagesSentTo  show who received message
        Dim thisData As New CommandsSqlAndOleDb("InsSentP1Messages", Session("CSCConn"))
        thisData.AddSqlProcParameter("@requestnum", RequestNum, SqlDbType.Int)
        thisData.AddSqlProcParameter("@ToAddress", TechEmailList, SqlDbType.NVarChar, 4000)
        thisData.AddSqlProcParameter("@subject", sSubject, SqlDbType.NVarChar, 200)
        thisData.AddSqlProcParameter("@body", sBody, SqlDbType.NVarChar, 4000)

        thisData.ExecNonQueryNoReturn()

        btnSendTechP1.Visible = False

    End Sub
    Protected Sub SendEmailtoTechsGroupAssign(ByVal RequestNum As Integer, ByVal GroupNum As String, ByVal sGroupName As String)
        Dim path As String
        Dim directory As String = ""
        Dim toList As String()
        Dim Link As String
        Dim sSubject As String
        Dim sBody As String
        Dim fulleMailList As String
        Dim i As Integer
        Dim EmailListp1 As String
        Dim MgrEmailList As String = ""

        If sGroupName = "Select" Or sGroupName = "" Then
            Exit Sub

        End If


        path = HttpContext.Current.Request.Url.AbsoluteUri
        Dim pathParts() As String = Split(path, "/")




        Do While i < pathParts.Length() - 1

            directory = directory + pathParts(i) + "/"
            i = i + 1

        Loop

        Mailmsg.To.Clear()

        If Session("environment").ToString.ToLower = "testing" Then
            'toEmail = "Jeff.Mele@crozer.org"
            Link = "http://webdev3/CSCV2/ViewRequestTicket.aspx?RequestNum=" & RequestNum.ToString
        Else
            Link = "http://Casemgmt02/CSCV2/ViewRequestTicket.aspx?RequestNum=" & RequestNum.ToString

        End If


        sSubject = "New Ticket #" & RequestNum & " in group queue " & sGroupName


        sBody = "<!DOCTYPE html>" & _
        "<html>" & _
        "<head>" & _
        "<style>" & _
        "table" & _
        "{" & _
        "border-collapse:collapse;" & _
        "font-family: 'Helvetica Neue', 'Lucida Grande', 'Segoe UI', Arial, Helvetica, Verdana, sans-serif;" & _
        "}" & _
        "table, th, td" & _
        "{" & _
        "border: 1px solid black;" & _
        "padding: 3px" & _
        "}" & _
        "</style>" & _
        "</head>" & _
        "" & _
        "<body>" & _
        "<table cellpadding='5px'>" & _
                "<tr>" & _
                    "<td colspan='4' align='center'  style='font-size:large' color='red'>" & _
        "               " & _
                    "<b> Ticket/RFS </b>" & _
        "                 " & _
                     "</td>    " & _
                "</tr>" & _
                "<tr>" & _
                    "<td style='font-weight:bold' colspan='1'>Name:</td>" & _
                    "<td colspan='3'>" & first_nameTextBox.Text & " " & last_nameTextBox.Text & "</td>" & _
                "</tr>" & _
                "<tr>" & _
                    "<td style='font-weight:bold' colspan='1'>Request#:</td>" & _
                    "<td colspan='3'>" & RequestNum & "</td>" & _
                "</tr>" & _
                "<tr>" & _
                    "<td style='font-weight:bold' colspan='1'>Submitted By:</td>" & _
                    "<td colspan='3'>" & UserInfo.UserName & "</td>" & _
                "</tr>" & _
                   "<tr>" & _
                    "<td style='font-weight:bold' colspan='1'>Requestor Email:</td>" & _
                    "<td colspan='3'>" & emailTextBox.Text & "</td>" & _
                "</tr>" & _
                "<tr>" & _
                    "<td style='font-weight:bold' colspan='1'>Date Requested:</td>" & _
                    "<td colspan='3'>" & DateTime.Now.ToString("yyyy/MM/dd-hh:mm") & "</td>" & _
                "</tr>" & _
                "<tr>" & _
                    "<td style='font-weight:bold' colspan='4'>Request Information </td>" & _
                "</tr>" & _
        "      " & _
        "     " & _
        "<tr><td colspan='4'><b>Description:</b> " & shortdescTextbox.Text & "</td></tr>" & _
        "<tr><td> Request Type: " & RequestTyperbl.SelectedValue.ToString & "</td><td> Category: " & categorycdddl.SelectedValue.ToString & "</td><td> Type:" & typecdddl.SelectedValue.ToString & "</td><td> Item: " & itemcdddl.SelectedValue.ToString & "</td></tr>" & _
        "<tr><td colspan='2'> Phone: " & phoneTextBox.Text & "</td><td colspan='2'> Alternate Phone:" & alternatecontactphoneTextBox.Text & "</td></tr>" & _
        "<tr><td colspan='2'> eMail: " & emailTextBox.Text & "</td><td colspan='2'> IP Address: " & ipaddressTextBox.Text & "</td></tr>" & _
        "<tr><td> Location: " & FacilityCdeMailLabel.Text & "</td><td> Building: " & buildingEmailnameLabel.Text & "</td><td colspan='2'> Floor: " & floorEmailLabel.Text & "</td></tr>" & _
        "<tr><td colspan='2'> Address1: " & address_1textbox.Text & "</td><td colspan='2'> Address2: " & address_2textbox.Text & "</td></tr>" & _
        "<tr><td colspan='2'> City: " & citytextbox.Text & "</td><td>State: " & statetextbox.Text & "</td><td> Office: " & OfficeTextbox.Text & "</td></tr>" & _
        "<tr><td colspan='4'><table>" & _
        "<tr>" & _
            "<td align='center'  style='font-size:large'>" & _
"               " & _
            "<b>View Details by clicking on link below</b>" & _
"                 " & _
             "</td>    " & _
        "</tr>" & _
        "<tr>" & _
            "<td align='center'>" & Link & "</td>" & _
        "</tr>" & _
        "<tr>" & _
             "<td colspan='4' align='center'  style='font-size:large'>" & _
        "               " & _
                    "<b> DO NOT REPLY to This eMail</b>" & _
        "                 " & _
                     "</td>    " & _
                "</tr></table>" & _
         "</td></tr></table>" & _
        "</body>" & _
        "</html>"




        'EmailList = New EmailListOne(RequestNum, Session("CSCConn")).EmailListReturn

        'fulleMailList = EmailList
        emaillist.GetGroupAssignEmailList(Session("CSCConn"), GroupNum, RequestNum)
        MgrEmailList = emaillist.EmailListReturn()

        If MgrEmailList.ToString.ToLower = "csc@crozer.org" Then
            Exit Sub
        End If

        Try

            EmailListp1 = emailTextBox.Text & ";"

            fulleMailList = emailTextBox.Text & ";"

            If Session("environment").ToString.ToLower = "testing" Then
                EmailListp1 = "Jeff.Mele@crozer.org"
                sBody = sBody & MgrEmailList

            Else
                EmailListp1 = MgrEmailList

                'sBody = sBody & fulleMailList

            End If

        Catch ex As Exception
            sSubject = "Ticket/RFS Email Error " & sSubject
            EmailListp1 = "Jeff.Mele@crozer.org"
        End Try

        'Mailmsg.To.Add(New System.Net.Mail.MailAddress(EmailListp1))

        toList = MgrEmailList.Split(";")
        If toList.Length = 1 Then
            Mailmsg.To.Add(New System.Net.Mail.MailAddress(toList(0)))
        Else
        End If
        For i = 0 To toList.Length - 1 ' - 2
            If toList(i).Length > 10 Then ' validity check
                Mailmsg.To.Add(New System.Net.Mail.MailAddress(toList(i)))
            End If
        Next

		'Dim mailObj As New MailMessage
		Mailmsg.From = New MailAddress("ckhsitcsc@crozer.org")
		Mailmsg.IsBodyHtml = True

        Mailmsg.Subject = sSubject
        Mailmsg.Body = sBody

        Dim smtpClient As New SmtpClient()
        smtpClient.Host = "ah1smtprelay01.altahospitals.com"

        smtpClient.Send(Mailmsg)


    End Sub
    Protected Sub SendEmailtoClientGroupAssig(ByVal RequestNum As Integer, ByVal GroupName As String)

        If EMailTextBox.Text = "" Then
            Return

        End If

        If GroupName = "Select" Or GroupName = "" Then
            Exit Sub

        End If

        Dim path As String
        Dim directory As String = ""
        Dim toList As String()
        Dim Link As String
        Dim sSubject As String
        Dim sBody As String
        Dim fulleMailList As String
        Dim i As Integer
        Dim EmailList As String

        path = HttpContext.Current.Request.Url.AbsoluteUri
        Dim pathParts() As String = Split(path, "/")

        Do While i < pathParts.Length() - 1

            directory = directory + pathParts(i) + "/"
            i = i + 1

        Loop

        Mailmsg.To.Clear()

        If Session("environment").ToString.ToLower = "testing" Then
            'toEmail = "Jeff.Mele@crozer.org"
            Link = "http://webdev3/CSCV2/ViewRequestTicket.aspx?RequestNum=" & RequestNum.ToString
        Else
            Link = "http://Casemgmt02/CSCV2/ViewRequestTicket.aspx?RequestNum=" & RequestNum.ToString

        End If


        sSubject = "Ticket #" & RequestNum & " has been Assigned to NEW Group  " & GroupName


        sBody = "<!DOCTYPE html>" & _
        "<html>" & _
        "<head>" & _
        "<style>" & _
        "table" & _
        "{" & _
        "border-collapse:collapse;" & _
        "font-family: 'Helvetica Neue', 'Lucida Grande', 'Segoe UI', Arial, Helvetica, Verdana, sans-serif;" & _
        "}" & _
        "table, th, td" & _
        "{" & _
        "border: 1px solid black;" & _
        "padding: 3px" & _
        "}" & _
        "</style>" & _
        "</head>" & _
        "" & _
        "<body>" & _
        "<table cellpadding='5px'>" & _
                "<tr>" & _
                    "<td colspan='4' align='center'  style='font-size:large' color='red'>" & _
        "               " & _
                    "<b> Ticket/RFS </b>" & _
        "                 " & _
                     "</td>    " & _
                "</tr>" & _
                "<tr>" & _
                    "<td style='font-weight:bold' colspan='1'>Name:</td>" & _
                    "<td colspan='3'>" & first_nameTextBox.Text & " " & last_nameTextBox.Text & "</td>" & _
                "</tr>" & _
                "<tr>" & _
                    "<td style='font-weight:bold' colspan='1'>Request#:</td>" & _
                    "<td colspan='3'>" & RequestNum & "</td>" & _
                "</tr>" & _
                "<tr>" & _
                    "<td style='font-weight:bold' colspan='1'>Submitted By:</td>" & _
                    "<td colspan='3'>" & UserInfo.UserName & "</td>" & _
                "</tr>" & _
                   "<tr>" & _
                    "<td style='font-weight:bold' colspan='1'>Requestor Email:</td>" & _
                    "<td colspan='3'>" & emailTextBox.Text & "</td>" & _
                "</tr>" & _
                "<tr>" & _
                    "<td style='font-weight:bold' colspan='1'>Date Requested:</td>" & _
                    "<td colspan='3'>" & DateTime.Now.ToString("yyyy/MM/dd-hh:mm") & "</td>" & _
                "</tr>" & _
                "<tr>" & _
                    "<td style='font-weight:bold' colspan='4'>Request Information </td>" & _
                "</tr>" & _
        "      " & _
        "     " & _
        "<tr><td colspan='4'><b>Description:</b> " & shortdescTextbox.Text & "</td></tr>" & _
        "<tr><td> Request Type: " & RequestTyperbl.SelectedValue.ToString & "</td><td> Category: " & categorycdddl.SelectedValue.ToString & "</td><td> Type:" & typecdddl.SelectedValue.ToString & "</td><td> Item: " & itemcdddl.SelectedValue.ToString & "</td></tr>" & _
        "<tr><td colspan='2'> Phone: " & phoneTextBox.Text & "</td><td colspan='2'> Alternate Phone:" & alternatecontactphoneTextBox.Text & "</td></tr>" & _
        "<tr><td colspan='2'> eMail: " & emailTextBox.Text & "</td><td colspan='2'> IP Address: " & ipaddressTextBox.Text & "</td></tr>" & _
        "<tr><td> Location: " & FacilityCdeMailLabel.Text & "</td><td> Building: " & buildingEmailnameLabel.Text & "</td><td colspan='2'> Floor: " & floorEmailLabel.Text & "</td></tr>" & _
        "<tr><td colspan='2'> Address1: " & address_1textbox.Text & "</td><td colspan='2'> Address2: " & address_2textbox.Text & "</td></tr>" & _
        "<tr><td colspan='2'> City: " & citytextbox.Text & "</td><td>State: " & statetextbox.Text & "</td><td> Office: " & OfficeTextbox.Text & "</td></tr>" & _
        "<tr><td colspan='4'><table>" & _
        "<tr>" & _
            "<td align='center'  style='font-size:large'>" & _
"               " & _
            "<b>View Details by clicking on link below</b>" & _
"                 " & _
             "</td>    " & _
        "</tr>" & _
        "<tr>" & _
            "<td align='center'>" & Link & "</td>" & _
        "</tr>" & _
        "<tr>" & _
             "<td colspan='4' align='center'  style='font-size:large'>" & _
        "               " & _
                    "<b> DO NOT REPLY to This eMail</b>" & _
        "                 " & _
                     "</td>    " & _
                "</tr></table>" & _
         "</td></tr></table>" & _
        "</body>" & _
        "</html>"




        Try
            If EMailTextBox.Text = "" Then

                Exit Sub

            End If

            emaillist = EMailTextBox.Text & ";"

            fulleMailList = EMailTextBox.Text & ";"

            If Session("environment").ToString.ToLower = "testing" Then
                emaillist = "Jeff.Mele@crozer.org"
                'sBody = sBody & fulleMailList
            Else

                sBody = sBody & fulleMailList

            End If

        Catch ex As Exception
            sSubject = "Ticket/RFS Email Error " & sSubject
            emaillist = "Jeff.Mele@crozer.org"
        End Try

        toList = emaillist.Split(";")
        If toList.Length = 1 Then
            Mailmsg.To.Add(New System.Net.Mail.MailAddress(toList(0)))
        Else
        End If
        For i = 0 To toList.Length - 1 ' - 2
            If toList(i).Length > 10 Then ' validity check
                Mailmsg.To.Add(New System.Net.Mail.MailAddress(toList(i)))
            End If
        Next

		'Dim mailObj As New MailMessage
		Mailmsg.From = New MailAddress("ckhsitcsc@crozer.org")
		Mailmsg.IsBodyHtml = True

        Mailmsg.Subject = sSubject
        Mailmsg.Body = sBody

        Dim smtpClient As New SmtpClient()
        smtpClient.Host = "ah1smtprelay01.altahospitals.com"

        smtpClient.Send(Mailmsg)


    End Sub
    Protected Sub changegroupddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles changegroupddl.SelectedIndexChanged
        If changegroupddl.SelectedIndex = 0 Then

            currGroupLb.Text = "Current Group:"
            currentgroupnameLabel.Text = "No Group Assigned"

            ChangeTechddl.Enabled = False

            currTechlb.Text = "Current Tech:"
            currenttechnamelabel.Text = "No Tech Assigned"
            currenttechnumLabel.Text = ""


            Exit Sub
        End If

        currGroupLb.Text = "New Group:"

        If origionalGroupNumLable.Text <> changegroupddl.SelectedValue Then
            If hdPriorityLb.Text = "1" Then
                HDSendP1email.Text = "yes"

                'SendP1Email(RequestNum, "1", "groupassign")

            End If


        End If

        currentgroupnumLabel.Text = changegroupddl.SelectedValue.ToString
        currentgroupnameLabel.Text = changegroupddl.SelectedItem.ToString

        ''Change Tech assignment
        currenttechnumLabel.Text = ""
        currTechlb.Text = "Previous Tech:"

        Dim ddlCurrTech As New DropDownListBinder(ChangeTechddl, "selTechsByGroupNum", Session("CSCConn"))
        'currentgroupnumLabel.Text
        ddlCurrTech.AddDDLCriteria("@group_num", currentgroupnumLabel.Text, SqlDbType.NVarChar)
        ddlCurrTech.AddDDLCriteria("@securityLevel", Session("SecurityLevelNum"), SqlDbType.Int)

        ddlCurrTech.BindData("tech_num", "TechName")

        ChangeTechddl.Enabled = True

        lblValidation.Text = "Please Update Request to Apply changes!"
        lblValidation.Visible = True

    End Sub

    Protected Sub categorycdddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles categorycdddl.SelectedIndexChanged

        categorycdLabel.Text = categorycdddl.SelectedValue

        Dim ddlBinder As New DropDownListBinder(typecdddl, "SelCategoryType", Session("CSCConn"))
        ddlBinder.AddDDLCriteria("@Category_cd", categorycdLabel.Text, SqlDbType.NVarChar)

        ddlBinder.BindData("type_cd", "CatagoryTypeDesc")

        typecdddl.SelectedIndex = -1

        typecdLabel.Text = ""
        itemcdLabel.Text = ""

        itemcdddl.Enabled = False

        If categorycdLabel.Text = "reports" Then

            'report panel 
            reportPanel.Visible = True
            TechPanl.Visible = False
        Else

            reportPanel.Visible = False
            TechPanl.Visible = True
        End If


    End Sub

    Protected Sub typecdddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles typecdddl.SelectedIndexChanged

        typecdLabel.Text = typecdddl.SelectedValue
        categorycdLabel.Text = categorycdddl.SelectedValue


        Dim ddlBinder As New DropDownListBinder(itemcdddl, "SelCategoryItems", Session("CSCConn"))
        'ddlBinder.BindData("item_num", "item_cd")
        ddlBinder.AddDDLCriteria("@Category_cd", categorycdddl.SelectedValue, SqlDbType.NVarChar)
        ddlBinder.AddDDLCriteria("@type_cd", typecdLabel.Text, SqlDbType.NVarChar)


        ddlBinder.BindData("item_cd", "ItemTypeDesc")

        itemcdddl.Items.Insert(1, "Other")


        itemcdddl.SelectedIndex = -1
        itemcdLabel.Text = ""
        itemcdddl.Enabled = True

    End Sub
    Protected Sub itemcdddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles itemcdddl.SelectedIndexChanged
        itemcdLabel.Text = itemcdddl.SelectedValue
        typecdLabel.Text = typecdddl.SelectedValue
        categorycdLabel.Text = categorycdddl.SelectedValue

    End Sub

    Protected Sub facilitycdddl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles facilitycdddl.SelectedIndexChanged
        Dim ddlBinder As DropDownListBinder


        ddlBinder = New DropDownListBinder(buildingcdddl, "sel_building_by_facility", Session("EmployeeConn"))
        ddlBinder.AddDDLCriteria("@facility_cd", facilitycdddl.SelectedValue, SqlDbType.NVarChar)
        ddlBinder.BindData("building_cd", "building_name")

        Dim ddlFloorBinder As DropDownListBinder

        ddlFloorBinder = New DropDownListBinder(Floorddl, "SelFloorsByBuilding", Session("EmployeeConn"))
        ddlFloorBinder.AddDDLCriteria("@facility_cd", facilitycdddl.SelectedValue, SqlDbType.NVarChar)
        ddlFloorBinder.AddDDLCriteria("@building_cd", buildingcdddl.SelectedValue, SqlDbType.NVarChar)

        ddlFloorBinder.BindData("floor_no", "floor_no")

        'FacilityCdeMailLabel.Text = facilitycdddl.SelectedItem.ToString


        'ChangColor()
    End Sub
    Protected Sub Floorddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles Floorddl.SelectedIndexChanged
        floorLabel.Text = Floorddl.SelectedValue
        floorEmailLabel.Text = Floorddl.SelectedItem.ToString
        'ChangColor()
    End Sub

    Protected Sub buildingcdddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles buildingcdddl.SelectedIndexChanged
        building_cdLabel.Text = buildingcdddl.SelectedValue
        'buildingnameLabel.Text = building_cdddl.SelectedItem.Text

        Dim ddlFloorBinder As DropDownListBinder

        ddlFloorBinder = New DropDownListBinder(Floorddl, "SelFloorsByBuilding", Session("EmployeeConn"))
        ddlFloorBinder.AddDDLCriteria("@facility_cd", facilitycdddl.SelectedValue, SqlDbType.NVarChar)
        ddlFloorBinder.AddDDLCriteria("@building_cd", buildingcdddl.SelectedValue, SqlDbType.NVarChar)

        ddlFloorBinder.BindData("floor_no", "floor_no")

        FacilityCdeMailLabel.Text = facilitycdddl.SelectedItem.ToString
        buildingEmailnameLabel.Text = buildingcdddl.SelectedItem.ToString

    End Sub

    Protected Sub entitycdDDL_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles entitycdDDL.SelectedIndexChanged
        Dim ddbDepartmentBinder As New DropDownListBinder(departmentcdddl, "selDepartmentCodesV4", Session("EmployeeConn"))
        ddbDepartmentBinder.AddDDLCriteria("@entity_cd", entitycdDDL.SelectedValue, SqlDbType.NVarChar)
        ddbDepartmentBinder.BindData("department_cd", "department_name")
    End Sub

    Protected Sub btnReturnClient_Click(sender As Object, e As System.EventArgs) Handles btnReturnClient.Click
        Response.Redirect("~/ClientDemo.aspx?ClientNum=" & clientnumLabel.Text, True)

    End Sub
    Sub Createxcel()

        Dim filepath = Server.MapPath("~/CSCFileUpload/excel1.xls")

        Response.ContentType = "application/vnd.ms-excel"

        Response.AddHeader("content-disposition", "attachment;filename=excel1.xls")

        Response.Charset = String.Empty
        '  Response.ContentType = "application/vnd.xls"

        'Dim sw As System.IO.StringWriter = New System.IO.StringWriter()

        'Dim hw As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(sw)

        '===============================Create form to contain grid
        'Dim frm As New HtmlForm()

        'frm.RenderControl(hw)
        'Response.Write(sw.ToString())
        Response.WriteFile(filepath)
        Response.Flush()
        Response.End()

    End Sub


    Protected Sub btnoldCSC_Click(sender As Object, e As System.EventArgs) Handles btnoldCSC.Click
        If RequestTyperbl.SelectedValue = "ticket" Then
            Response.Redirect("http://intranet01/csc/edit_ticket.asp?id=" & RequestNum, True)


        ElseIf RequestTyperbl.SelectedValue = "rfs" Or RequestTyperbl.SelectedValue = "service" Then
            If currstatuscdLabel.Text = "resols" Then

                Response.Redirect("http://intranet01/csc/view_request.asp?id=" & RequestNum, True)

            ElseIf CloseDateLabel.Text <> "" Then

                Response.Redirect("http://intranet01/csc/view_request.asp?id=" & RequestNum, True)

            Else

                Response.Redirect("http://intranet01/csc/update_rfs.asp?id=" & RequestNum, True)

            End If

        End If

    End Sub

    Protected Sub SendCompletedEmail(ByVal RequestNum As Integer)
        '======================
        ' Get current path
        '======================

        Dim path As String = ""
        Dim directory As String = ""
        Dim toList As String()
        Dim Link As String
        'Dim toEmail As String = EmailAddress.Email
        Dim sSubject As String
        Dim sBody As String
        Dim fulleMailList As String = ""
        Dim pathParts() As String = Split(path, "/")
        Dim i As Integer


        path = HttpContext.Current.Request.Url.AbsoluteUri

        Dim EmailList As String


        Do While i < pathParts.Length() - 1

            directory = directory + pathParts(i) + "/"
            i = i + 1

        Loop

        Mailmsg.To.Clear()

        If Session("environment").ToString.ToLower = "testing" Then
            'toEmail = "Jeff.Mele@crozer.org"
            Link = "http://webdev3/CSCV2/CSCSurvey.aspx?RequestNum=" & RequestNum.ToString
        Else
            Link = "http://Casemgmt02/CSCV2/CSCSurvey.aspx?RequestNum=" & RequestNum.ToString

        End If



        sSubject = "Ticket/RFS #" & RequestNum & " has been completed " & first_nameTextBox.Text & " " & last_nameTextBox.Text


        sBody = "<!DOCTYPE html>" & _
        "<html>" & _
        "<head>" & _
        "<style>" & _
        "table" & _
        "{" & _
        "border-collapse:collapse;" & _
        "font-family: 'Helvetica Neue', 'Lucida Grande', 'Segoe UI', Arial, Helvetica, Verdana, sans-serif;" & _
        "}" & _
        "table, th, td" & _
        "{" & _
        "border: 1px solid black;" & _
        "padding: 3px" & _
        "}" & _
        "</style>" & _
        "</head>" & _
        "" & _
        "<body>" & _
        "<table cellpadding='5px'>" & _
                "<tr>" & _
                    "<td colspan='4' align='center'  style='font-size:large'>" & _
        "               " & _
                    "<b>Ticket/RFS </b>" & _
        "                 " & _
                     "</td>    " & _
                "</tr>" & _
                "<tr>" & _
                    "<td style='font-weight:bold' colspan='1'>Name:</td>" & _
                    "<td colspan='3'>" & first_nameTextBox.Text & " " & last_nameTextBox.Text & "</td>" & _
                "</tr>" & _
                "<tr>" & _
                    "<td style='font-weight:bold' colspan='1'>Request#:</td>" & _
                    "<td colspan='3'>" & RequestNum & "</td>" & _
                "</tr>" & _
                "<tr>" & _
                    "<td style='font-weight:bold' colspan='1'>Submitted By:</td>" & _
                    "<td colspan='3'>" & UserInfo.UserName & "</td>" & _
                "</tr>" & _
                   "<tr>" & _
                    "<td style='font-weight:bold' colspan='1'>Requestor Email:</td>" & _
                    "<td colspan='3'>" & emailTextBox.Text & "</td>" & _
                "</tr>" & _
                "<tr>" & _
                    "<td style='font-weight:bold' colspan='1'>Date Requested:</td>" & _
                    "<td colspan='3'>" & DateTime.Now.ToString("yyyy/MM/dd-hh:mm") & "</td>" & _
                "</tr>" & _
                "<tr>" & _
                    "<td style='font-weight:bold' colspan='4'>Request Information </td>" & _
                "</tr>" & _
        "      " & _
        "     " & _
        "<tr><td colspan='4'><b>Description:</b>" & shortdescTextbox.Text & "</td></tr>" & _
        "<tr><td> Request Type: " & RequestTyperbl.SelectedValue.ToString & "</td><td> Category: " & categorycdLabel.Text & "</td><td> Type:" & typecdLabel.Text & "</td><td> Item: " & itemcdLabel.Text & "</td></tr>" & _
        "<tr><td colspan='2'> Phone: " & phoneTextBox.Text & "</td><td colspan='2'> Alternate Phone:" & alternatecontactphoneTextBox.Text & "</td></tr>" & _
        "<tr><td colspan='2'> eMail: " & emailTextBox.Text & "</td><td colspan='2'> IP Address: " & ipaddressTextBox.Text & "</td></tr>" & _
        "<tr><td> Location: " & FacilityCdeMailLabel.Text & "</td><td> Building: " & buildingEmailnameLabel.Text & "</td><td colspan='2'> Floor: " & floorEmailLabel.Text & "</td></tr>" & _
        "<tr><td colspan='2'> Address1: " & address_1textbox.Text & "</td><td colspan='2'> Address2: " & address_2textbox.Text & "</td></tr>" & _
        "<tr><td colspan='2'> City: " & citytextbox.Text & "</td><td>State: " & statetextbox.Text & "</td><td> Office: " & OfficeTextbox.Text & "</td></tr>" & _
        "<tr><td colspan='4'><table>" & _
        "<tr>" & _
            "<td align='center'  style='font-size:large'>" & _
"               " & _
            "<b>Please Complete Survey by clicking on the link below</b>" & _
"                 " & _
             "</td>    " & _
        "</tr>" & _
        "<tr>" & _
            "<td align='center'>" & Link & "</td>" & _
        "</tr></table>" & _
        "</td></tr>" & _
        "</body>" & _
        "</html>"




        'EmailList = New EmailListOne(RequestNum, Session("CSCConn")).EmailListReturn

        'fulleMailList = EmailList
        Try

            If emailTextBox.Text <> "" Then

                EmailList = emailTextBox.Text & ";"

                fulleMailList = emailTextBox.Text & ";"
            Else

                EmailList = "Jeff.Mele@crozer.org;"

                fulleMailList = "Jeff.Mele@crozer.org;"
                sSubject = "Ticket/RFS Email Error " & sSubject

            End If

            If Session("environment").ToString.ToLower = "testing" Then
                EmailList = "Jeff.Mele@crozer.org"
                'sBody = sBody & fulleMailList
            Else

                sBody = sBody & fulleMailList

            End If

        Catch ex As Exception
            sSubject = "Ticket/RFS Email Error " & sSubject
            EmailList = "Jeff.Mele@crozer.org"
        End Try

        toList = EmailList.Split(";")
        If toList.Length = 1 Then
            Mailmsg.To.Add(New System.Net.Mail.MailAddress(toList(0)))
        Else
        End If
        For i = 0 To toList.Length - 1 ' - 2
            If toList(i).Length > 10 Then ' validity check
                Mailmsg.To.Add(New System.Net.Mail.MailAddress(toList(i)))
            End If
        Next

		'Dim mailObj As New MailMessage
		Mailmsg.From = New MailAddress("ckhsitcsc@crozer.org")
		Mailmsg.IsBodyHtml = True

        Mailmsg.Subject = sSubject
        Mailmsg.Body = sBody

        Dim smtpClient As New SmtpClient()
        smtpClient.Host = "ah1smtprelay01.altahospitals.com"

        smtpClient.Send(Mailmsg)

    End Sub


    Protected Sub btnPrintTicket_Click(sender As Object, e As System.EventArgs) Handles btnPrintTicket.Click

        Response.Redirect("./PrintRequestv8.aspx?RequestNum=" & RequestNum)

        'Response.Redirect("~/Reports/PrintListReport.aspx?ReportName=TicketRFS" & "&requestnum=" & RequestNum)

    End Sub


End Class
