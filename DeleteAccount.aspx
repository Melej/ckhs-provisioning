﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="false" CodeFile="DeleteAccount.aspx.vb" Inherits="DeleteAccount" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

<asp:UpdatePanel runat="server" ID="uplPage" UpdateMode="Conditional">
<ContentTemplate>

<table width="100%">
   <tr>
     <td align="left">
       <asp:Label ID = "LblMainInfo" runat = "server" ForeColor="Red"/>
    </td>
  </tr>
 <tr>
  <td align="left">
  
<ajaxToolkit:TabContainer ID="tabs" runat="server" ActiveTabIndex="0"
          Width="100%" style="margin-bottom: 1px" EnableTheming="False" 
          BackColor="#efefef">

    <ajaxToolkit:TabPanel ID="tpheader" runat="server" TabIndex="0"  BackColor="Gainsboro" Visible = "True">
        <HeaderTemplate > 
            <asp:Label ID="Label2" runat="server" Text="This Account Request" Font-Bold="true" ForeColor="Black" ></asp:Label>
        </HeaderTemplate>
         <ContentTemplate>



             <table border="3" id="tblHeader" style="empty-cells:show" cellpadding="5px" 
                 width="100%">
           <tr>
                <th colspan = "4" class="tableRowHeader">
                    <asp:Label ID="lblheader" runat="server" />
                </th>     
           </tr>
          
           <tr>
            <td colspan = "2" style="font-weight:bold">
               Client Name
            </td>
             <td colspan = "2" >
       
                <asp:Label ID = "first_namelabel" runat = "server"></asp:Label>&nbsp<asp:Label ID = "last_namelabel" runat = "server" ></asp:Label>
            </td>
            
           </tr>
           <tr>
            <td colspan = "2" style="font-weight:bold">
              Requestor Name
            </td>
           <td colspan = "2" >
                <asp:Label ID = "requestor_namelabel" runat = "server"></asp:Label>
            </td>
           </tr>
           <tr>
                 <td colspan = "2" style="font-weight:bold">
                    Request Type
                </td>     
         
                 <td colspan = "2" >
                     <asp:Label ID = "RequestTypeDesclabel" runat = "server"></asp:Label>
                </td>     
           </tr>
           <tr>
               
                  <td style="font-weight:bold" colspan="2">
                 Submit Date
                  </td>
        
               
          
                <td colspan="2">
                    <asp:Label ID = "entered_datelabel" runat = "server"></asp:Label>
                </td>
            </tr>
           
            <tr>
                <td colspan="4"  >
                  <table>
                    <tr>
                        <td>
                        </td>
                        <td>
                             <asp:Button ID = "btnCreateSubmit" runat = "server" Text ="Submit Account Deleted " 
                              Width="225" BackColor="#336666" ForeColor="White"/>
                         </td>

                        <td>
                              <asp:Button ID = "btnInvalid" runat = "server" Text ="Mark Delete Request Invalid " 
                               Width="250" BackColor="#336666" ForeColor="White"/>
                        </td>
                        <td>
                            <asp:Button ID = "btnSaveItemInfo" runat = "server" Text ="Save Comments"  Width="175"  
                            Visible="false" BackColor="#336666" ForeColor="White" />
                        </td>

                        
                    </tr>
                  
                  </table>



                </td>
            </tr>

            <tr>
                <td colspan = "4" class ="tableRowSubHeader">
                Account Information
                </td>
            </tr>
            <tr>
                <td style="font-weight:bold" >
                  Account
                </td>
                <td colspan = "3">
                    <asp:Label ID ="AccountLabel" runat = "server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="font-weight:bold" >
                   User Name
                </td>
                <td colspan="3">
                    <asp:TextBox runat = "server" ID = "LoginIDTextBox"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="font-weight:bold">
                    <asp:Label ID="lblLocationofCare" runat="server" text="CKHN Location of Care (G#)" 
                        Visible="false" />
                </td>
                <td>
                    <asp:TextBox ID="T1LocationOfCareIDTextBox" runat="server" Visible="false" Width="300px" />
                </td>
                <td style="font-weight:bold">
                    <asp:Label ID="lblCopyTo" runat="server" text="CKHN Copy To MedAssist." 
                        Visible="false" />
                </td>
                <td>
                    <asp:TextBox ID="T1CopyToTextBox" runat="server" Visible="false" Width="300px" />
                </td>
                           
            </tr>

            <tr>
                <td style="font-weight:bold">
                    <asp:Label ID="lblCommLocationofcare" runat="server" text="COMM. Location of Care (G#)" 
                        Visible="false" />
                </td>
                <td>
                    <asp:TextBox ID="T1CommLocationOfCareIDtextBox" runat="server" Visible="false" Width="300px" />
                </td>
                <td style="font-weight:bold">
                    <asp:Label ID="lblCommCopyTo" runat="server" text="COMM. Copy To MedAssist." 
                        Visible="false" />
                </td>
                <td>
                    <asp:TextBox ID="T1CommCopytoTextBox" runat="server" Visible="false" Width="300px" />
                </td>


            </tr>
            <tr>
                <td style="font-weight:bold">
                    <asp:Label ID="LblDoctormaster" runat="server" text="Doctor Master/HIS" 
                        Visible="false" />
                </td>
                <td colspan="3">
                    <asp:Label ID="lbldoctormastertxt" runat="server"  Visible="false" />
                </td>
            </tr>
            <tr>
                <td  colspan="4" align="center">
                <asp:Panel Visible="false" runat="server" id="interpnl">
                <table width="100%" border="1px">
                    <tr>
                        <td style="font-weight:bold">
                            <asp:Label ID="lbladdcopyto" runat="server" Text="Added DM/HIS to Cloverleaf" Visible="false" />
                                        
                        </td>
                        <td style="font-weight:bold">
                                <asp:Label ID="lbladdMedass" runat="server" Text="Added Med Assist Value to Cloverleaf" Visible="false" />

                        </td>
                        <td style="font-weight:bold">
                            <asp:Label ID="lblBounced" runat="server" Text="Bounced CKHN/Comm Interface" Visible="false" />
                                        
                        </td>
                                    
                    </tr>
                    <tr>
                        <td>

                            <asp:RadioButtonList ID="CopyTorbl" runat="server" RepeatDirection="Horizontal" Visible="false">
                                <asp:ListItem>Yes</asp:ListItem>
                                <asp:ListItem Selected="True">No</asp:ListItem>
                            </asp:RadioButtonList>
                                        
                        </td>
                        <td>

                            <asp:RadioButtonList ID="MedAssrbl" runat="server" RepeatDirection="Horizontal" Visible="false">
                                <asp:ListItem>Yes</asp:ListItem>
                                <asp:ListItem Selected="True">No</asp:ListItem>
                            </asp:RadioButtonList>
                                        
                        </td>
                        <td>

                            <asp:RadioButtonList ID="Bouncedrbl" runat="server" RepeatDirection="Horizontal" Visible="false">
                                <asp:ListItem>CKHN</asp:ListItem>
                                <asp:ListItem>Comm</asp:ListItem>
                                <asp:ListItem>Both</asp:ListItem>
                                <asp:ListItem>N/A</asp:ListItem>
                            </asp:RadioButtonList>
                                        
                        </td>

                    </tr>                    
                </table>
                </asp:Panel>
                </td>
            </tr>
            <tr>
                <td style="font-weight:bold" valign="top" colspan="4">
                    Comments
                </td>
            </tr>

            <tr>
                <td align="center" colspan="4">
   	            <%-- =================== Nurse Stations for mak Pyxis ===============================--%>
                <asp:Panel id="CCMCNurseStationspan" runat="server"  Visible="false">
                <table  border="3px" width="100%">
                    <tr>
                        <td>
                            CCMC
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                                <asp:CheckBoxList ID="cblCCMCAll" runat="server" RepeatColumns="0"
                                    AutoPostBack="true" Enabled="false">
                                    <asp:ListItem Value="ccmcall" Text="All CCMC" ></asp:ListItem>
                                </asp:CheckBoxList>                                    
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <asp:CheckBoxList ID="cblCCMCNursestations" runat="server" RepeatColumns="2"
                                    AutoPostBack="true" Enabled="false">
                                </asp:CheckBoxList>                                    
                        </td>


                    </tr>
                </table>
                </asp:Panel>
                <asp:Panel ID="dcmhnursepan" runat="server" Visible="false">
                <table  border="3px" width="100%">
                    <tr>
                        <td>
                            DCMH
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                                <asp:CheckBoxList ID="cblDCMHAll" runat="server" RepeatColumns="0"
                                    AutoPostBack="true"  Enabled="false">
                                    <asp:ListItem Value="dcmhall" Text="All DCMH"></asp:ListItem>
                                </asp:CheckBoxList>                                    
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <asp:CheckBoxList ID="cblDCMHNursestataions" runat="server" RepeatColumns="3"
                                    AutoPostBack="true" Enabled="false">
                                </asp:CheckBoxList>                                    
                        </td>
                    </tr>
                </table>
                          
                </asp:Panel>
                <asp:Panel ID="taylornursepan" runat="server" Visible="false">
                <table  border="3px" width="100%">
                    <tr>
                        <td valign="top">
                        Taylor
                        </td>
                    </tr>

                    <tr>
                        <td valign="top">
                                <asp:CheckBoxList ID="cblTAll" runat="server" RepeatColumns="0"
                                    AutoPostBack="true" Enabled="false">
                                    <asp:ListItem Value="taylorall" Text="All Taylor"></asp:ListItem>
                                </asp:CheckBoxList>                                    
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <asp:CheckBoxList ID="cblTaylorNursestataions" runat="server" RepeatColumns="3"
                                    AutoPostBack="true" Enabled="false">
                                </asp:CheckBoxList>                                    
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblAppSystem" runat="server" Visible="false"></asp:Label>
                        </td>
                    </tr>
                </table>
                          
                </asp:Panel>
                <asp:Panel ID="SpringNurseStationsPan" runat="server"  Visible="false">

                    <table id="Table2"  border="3px" width="100%">
                    <tr>
                                    
                        <td>
                            Springfield
                            </td>
                    </tr>
                    <tr>
                        <td>
                                <asp:CheckBoxList ID="cblSAll" runat="server" RepeatColumns="0"
                                    AutoPostBack="true" Enabled="false">
                                    <asp:ListItem Value="springall" Text="All Springfield"></asp:ListItem>
                                </asp:CheckBoxList>
                        </td>
                                    
                    </tr>
                    <tr>
                        <td valign="top">
                            <asp:CheckBoxList ID="cblSpringfieldNursestataions" runat="server" RepeatColumns="3"
                                    AutoPostBack="true" Enabled="false">
                                </asp:CheckBoxList>

                        </td>
                                
                    </tr>
                    </table>
                </asp:Panel>
                </td>
            </tr> 
            <tr>
                <td align="center" colspan="4">
   	            <%-- =================== 365 List sites ===============================--%>

                <asp:Panel ID="O365Panel" runat="server"  Visible="false">

                    <table id="Table1"  border="3px" width="100%">
                    <tr>
                        <td>
                            O365 asccounts
                                    
                        </td>
                        <td>
                                <asp:CheckBoxList ID="cblo365" runat="server" RepeatColumns="0"
                                    AutoPostBack="false" Enabled="false">
                                </asp:CheckBoxList>
                        </td>
                                    
                    </tr>
                    </table>
                </asp:Panel>
                </td>
            </tr>

            <tr>
                <td colspan = "4">
                
                 <asp:TextBox runat = "server" ID = "CommentsTextBox" TextMode="MultiLine" Width="98%" Rows="5"></asp:TextBox>
                 <br />
                 <br />
                 <span style="float:right"> </span>
                </td>
            </tr>
            <tr>
                <td colspan = "4" >
                    <asp:TextBox ID ="ItemCompleteDateTextBox" runat="server" Visible="false" />
                    <asp:Button runat="server" ID ="btnRequestDetails" Text ="Request Details" Visible="false" />

                </td>
            </tr>
                    <tr>
                    <td colspan="4" align="center">

                        <asp:Panel ID="PanInvalid" runat="server" Visible="False" >
                        <table id="tblInvalid" runat="server" cellspacing="1" cellpadding="10" 
                            bgcolor="gainsboro" bordercolor="navy"  rules="none"  border="2" 
                                width="350Px">
                            <tr>
                                <td align="center" colspan="2" style="font-weight: bold; height: 25px;">
                                    Provide Invalid Reason or Comment</td>
                            </tr>
                            <tr>
                                <td align="center" colspan="2">
                                        <asp:DropDownList ID="Invalidddl" runat="server" Width="250px">
                                        </asp:DropDownList>

                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnSubmitInvalid" runat="server" BackColor="#00C000" Font-Bold="True"
                                        ForeColor="Black" Height="25px" Text='Submit'
                                        ToolTip="This will Identify this 1 Account as Invalid "/>
                                </td>
                                <td align="center" >
                                    <asp:Button ID="btnCancelInvalid" runat="server" BackColor="DimGray" Font-Bold="True"
                                        ForeColor="White" Height="25px" Text="Cancel" Width="80px" />
                                </td>
                            </tr>
                            <tr>
                                <td align="center"  colspan="2">
                                        Comments:
                                </td>
                            </tr>
                            <tr>
                                <td  align="center"  colspan="2">
                                    <asp:TextBox ID = "InvalidDescTextBox" runat="server"  Height="100px" Width="80%" Font-Size="Medium" />

                                </td>
        
                            </tr>
                        </table>
                    </asp:Panel>
    
                        </td>
                    </tr>  
        </table>


        </ContentTemplate>
    </ajaxToolkit:TabPanel>
         <ajaxToolkit:TabPanel ID="tpDemographics" runat="server" TabIndex="0"  BackColor="Gainsboro" Visible = "True">
        <HeaderTemplate > 
            <asp:Label ID="Label1" runat="server" Text="Request" Font-Bold="true" ForeColor="Black"></asp:Label>
                    
        </HeaderTemplate>
                      <ContentTemplate>
                            <table border="3" style="width: 100%">
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID = "lblValidation" runat = "server" ForeColor="Red"/>
                                        </td>
            
                                    </tr>
                                    <tr>
                                             <td class="tableRowHeader">
                                                Account Request Information
                                             </td>
                                    </tr>
                                    <tr>
          
                                        <td class="tableRowSubHeader"  align="left">
                                       Form Submission
                                        </td>
            
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <asp:Button ID = "btnGoToRequest" runat = "server" Text ="Go To Request " Width="250px" 
                                                BackColor="#336666" ForeColor="White"  Visible="false"/>

                                        </td>
                                    </tr>
                                    <tr>
                                    <td align="left">
                                       User submitting:         
                                       <asp:Label ID = "submitter_nameLabel" runat="server" Font-Bold="True"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                        Requestor:   <asp:Label ID = "Label3" runat="server" Font-Bold="True"></asp:Label>
            
              
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tableRowSubHeader"  align="left">
                                        CKHS Affiliation:
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:RadioButtonList ID ="emp_type_cdrbl" runat = "server" 
                                                RepeatDirection="Horizontal" AutoPostBack="True" Font-Size="Smaller">
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                    <tr>
                                      <td>
                                       <asp:Panel runat="server" ID ="pnlOtherAffilliation" Visible="False">
<%--                                        <table>
                                         <tr>
                                          <td width="30%">
                                            Other Affiliation Description:
                                          </td>
                                          <td>
                                               <asp:TextBox runat="server" width="90%" ID = "OtherAffDescTextBox"></asp:TextBox>
                                          </td>
                                         </tr>
                                        </table>--%>
                                     </asp:Panel>
                                       <asp:Panel runat="server" ID ="pnlAlliedHealth" Visible="False">
                                        <table>
                                          <tr>
                                            <td width="30%">
                                                 Allied Health Degree:
                                            </td>
                                            <td>
                                               <asp:DropDownList runat="server"  width="40%" ID = "OtherAffDescddl">
                                                </asp:DropDownList> 
                                            </td>
                                          </tr>
                
                                        </table>
                                     </asp:Panel>
                                    </td>
                                  </tr>
                                   <asp:Panel runat="server" ID="pnlProvider" Visible="False">
                                      <tr>
                                        <td colspan="4"  class="tableRowSubHeader" align="left">
                                            Provider Information
                                        </td>
                                      </tr>
                                       <tr>
                                        <td colspan="4">
                                       <table id="tblprovider"  border="3" width="100%">
                                            
                                        <tr>
                                                <td align="right">
                                                <asp:Label ID="lblprov" runat="server" Text = "Provider:"  Width="150px" />
                                                </td>
                                                <td  align="left" colspan="3">
                                                <asp:RadioButtonList ID = "providerrbl" runat="server" RepeatDirection="Horizontal">
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                        <asp:ListItem >No</asp:ListItem>            
                                                    </asp:RadioButtonList>
                                                </td>

                                        </tr>
                                        <tr>
                                            <td align="right" >
                                            Credentialed Date:
                                            </td>
                                            <td align="left" colspan="3">
                                                <asp:TextBox ID = "credentialedDateTextBox" runat="server" CssClass="calenderClass"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" >
                                                CKHN-HAN:
                                            </td>
                                            <td align="left">
                                                <asp:RadioButtonList ID = "hanrbl" runat="server" RepeatDirection="Horizontal">
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                    <asp:ListItem Selected="True">No</asp:ListItem>            
                                                </asp:RadioButtonList>
                                            </td>
                                            <td align="right">
                                               Doctor Number:
                                          </td>
                                          <td align="left">
                                                <asp:TextBox ID = "doctor_master_numTextBox" runat="server" Enabled="false"></asp:TextBox>
                                          </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                NPI:
                                            </td>
                                            <td  align="left" >
                                                <asp:TextBox ID = "npitextbox" runat = "server"></asp:TextBox>
                                            </td>
                                            <td align="right" >
                                                License No.
                                            </td>
                                            <td align="left" >
                                                    <asp:TextBox ID = "LicensesNo" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                Taxonomy:
                                                </td>
                                            <td align="left" colspan="3">
                                                <asp:TextBox ID = "taxonomytextbox" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                             CKHN Location Of Care (G#):
                                             </td>
                                             <td>
                                                <asp:TextBox ID = "LocationOfCareIDtextbox" runat="server"></asp:TextBox>
                                             </td>

                                            <td align="right">
                                             CKHN Copy To MedAssist.:
                                             </td>
                                             <td>
                                                <asp:TextBox ID = "CopyToTextBox" runat="server"></asp:TextBox>
                                             </td>

                                        </tr>
                                        <tr>
                                            <td align="right">
                                                Comm. Location Of Care (G#):
                                            </td>
                                            <td>
                                                <asp:TextBox ID = "CommLocationOfCareIDTextBox" runat="server"></asp:TextBox>
                                                
                                            </td>
                                            <td align="right">
                                                Comm. Copy To MedAssist.:
                                            </td>
                                             <td>
                                                <asp:TextBox ID = "CommCopyToTextBox" runat="server"></asp:TextBox>
                                             </td>

                                        </tr>


                                        <tr>
                                        
                                              <td align="right" >
                                                    Invision Group Number:
                                                </td>
                                                <td align="left" colspan="3">
                                                    <asp:TextBox ID="group_numTextBox" runat="server"></asp:TextBox>
                                                </td>
                                        
                                        
                                        </tr>
                                        <tr>
                                                <td align="right" >
                                                 Invision Group Name:

                                                </td>
                                                <td align="left" colspan="3">
                                                    <asp:TextBox ID="group_nameTextBox" runat="server" Width="500px"></asp:TextBox>
                                                </td>
                                        </tr>
                                        <tr>
                                            <td align="right" >
                                            Practice Name:
                                            </td>       
                                            <td colspan="3" align="left">
                                            <asp:DropDownList ID ="practice_numddl"  Width="500px" runat="server">
            
                                            </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                            Title:
                                            </td>
                                            <td align="left" >
                                                <asp:dropdownlist ID = "Titleddl" runat="server"></asp:dropdownlist>
                                            </td>
                                            <td  align="right" >
                                            Specialty:
                                            </td>
                                            <td align="left" >
                                                <asp:dropdownlist ID = "Specialtyddl" runat="server"></asp:dropdownlist>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" >
                                                Non-CKHN Location:
                                            </td>
                                            <td align="left" colspan="3" >
                                                    <asp:TextBox ID = "NonHanLocationTextBox" runat="server" Width="500px"></asp:TextBox>
                                            </td>

                                        </tr>
                                       </table>
                                        </td>
                                     </tr>
                                   </asp:Panel> 
                                   <tr>
                                    <td>
                                      <table id="tbldemo" border="3px" width="100%">
                                       <tr>
                                        <td colspan="4" class="tableRowSubHeader"  align="left">
                                            Demographics
                                        </td>
                                      </tr>
                                      <tr>

                                        <td colspan="4" >
                                         <table class="style3" > 
                                            <tr>
                                                <td>
                                                 First Name:
                                                </td>
                                                <td align="left"  >
                                                    <asp:TextBox width="280px" ID = "first_nameTextBox" runat="server" ></asp:TextBox>
                                                </td>
                                                <td>
                                                    MI:
                                                </td>
                                                <td>
                                                  <asp:TextBox ID = "middle_initialTextBox" runat="server" Width="15px"></asp:TextBox>
                                                </td>
                                                <td align="left" >
                                                  Last Name:
                                                </td>

                                                <td align="left" >
                                                    <asp:TextBox ID = "last_nameTextBox" width="280px" runat="server"></asp:TextBox>
                                                </td>
                                                <td>
                                                    Suffix: 
                                                </td>
                                                <td>
                                                    <asp:dropdownlist id="suffixddl" runat="server" Height="20px" Width="52px">
				                                        <asp:ListItem Value="------" Selected="True">------</asp:ListItem>
				                                        <asp:ListItem Value="Sr.">Sr.</asp:ListItem>
				                                        <asp:ListItem Value="Jr.">Jr.</asp:ListItem>
				                                        <asp:ListItem Value="II">II</asp:ListItem>
				                                        <asp:ListItem Value="III">III</asp:ListItem>
				                                        <asp:ListItem Value="IV">IV</asp:ListItem>
                                                     </asp:dropdownlist>
                                                </td>
                                            </tr>
                                         </table>
                                         </td>
                                      </tr>
                                      <tr>
                                        <td align="right" width="20%" >
                                            <asp:Label ID="lblpos" runat="server" Text="Position Description:" />
                                        </td>
                                        <td colspan = "3" align="left" >
                                            <asp:TextBox ID="user_position_descTextBox" runat="server" Width = "500px"></asp:TextBox>
                                        </td>
                                    </tr>
                                      <tr>
                                        <td align="right" >
                                        Entity:
                                        </td>
                                        <td align="left">
                                        <asp:DropDownList ID ="entity_cdDDL" runat="server" AutoPostBack ="True" >
            
                                        </asp:DropDownList>
                                        </td>
       
                                        <td align="right" >
                                            <asp:Label ID="lbldept" runat="server" Text = "Department Name:" />
                                        </td>
                                        <td align="left"  >
                                        <asp:DropDownList ID ="department_cdddl" runat="server" Width="250px">
            
                                        </asp:DropDownList>
                                        </td>
                                    </tr>
                                      <tr>
                                        <td  align="right">
                                            Address 1:
                                          </td>
                                        <td align="left"> 
                                             <asp:TextBox ID = "address_1textbox" runat="server" Width="250px"></asp:TextBox>
                                         </td>
                                        <td align="right" >
                                            Address 2:
                                         </td>
                                        <td align="left" >
                                          <asp:TextBox ID = "address_2textbox" runat="server" Width="250px"></asp:TextBox>
                                         </td>

                                    </tr>
                                     <tr>
                                         <td align="right">
                                                <asp:Label id="lblcity" runat="server" width="50px"  text="City:" />
                                            </td>
                                            <td align="left" colspan="3">
                                              <asp:TextBox ID = "citytextbox" runat="server" width="500px"></asp:TextBox>
                                            </td>


                                       </tr>

                                      <tr>

                                           <td align="right">
                                                <asp:Label ID="lblstate" runat="server" Text="State:" Width="50px" />
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID = "statetextbox" runat="server" Width="50px"></asp:TextBox>
                                             </td>
                                            <td align="right" >

                                                <asp:Label ID="lblzip" runat="server" Text="Zip:" Width="25px" />
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID = "ziptextbox" runat="server" Width="50px"></asp:TextBox>
                                            </td>
                                    </tr>
                                      <tr>
                                        <td align="right"  >
                                        Phone Number:
                                        </td>
                                        <td align="left" >
                                        <asp:TextBox ID = "phoneTextBox" runat="server"></asp:TextBox>
                                        </td>
       
                                        <td align="right" >
                                        Pager Number:
                                        </td>
                                        <td align="left" >
                                       <asp:TextBox ID = "pagerTextBox" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>

                                      <tr>
                                       <td align="right" >
                                        Fax:
                                       </td>
                                        <td align="left" >
            
                                            <asp:TextBox ID = "faxtextbox" runat="server"></asp:TextBox>
                                         </td>
         
                                         <td align="right" >
                                        Email:
                                        </td>
                                        <td align="left" >
                                       <asp:TextBox ID = "e_mailTextBox" runat="server" Width="250px"></asp:TextBox>
                                        </td>
                                    </tr>
                                      <tr>
                                        <td align="right" >
                                        Location:
                                        </td>
                                        <td align="left" >
         
                                          <asp:DropDownList ID ="facility_cdddl" runat="server" AutoPostBack="True">
              
                                        </asp:DropDownList>
          
                                        </td>
        
                                        <td align="right" >
                                        Building:
                                        </td>
                                        <td align="left" >

                                         <asp:DropDownList ID ="building_cdddl" runat="server" CssClass="style3">
            
                                        </asp:DropDownList>
          
                                        </td>
                                    </tr>
                                      <tr>
          
                                        <td align="right" >
                                            Vendor Name:
                                         </td>
                                        <td align="left"  colspan="3">
                                            <asp:TextBox ID ="VendorNameTextBox" runat="server" Width="500px" ></asp:TextBox>
                                        </td>
                                    </tr>
                                      <tr>
                                        <td align="right" >
                                        Start Date:
                                        </td>
                                        <td align="left" >
                                           <asp:TextBox ID ="start_dateTextBox" runat="server" CssClass="calenderClass"></asp:TextBox>
                                        </td>
       
                                        <td align="right" >
                                        End Date:
                                        </td>
                                        <td align="left" >
                                            <asp:TextBox ID="end_dateTextBox" runat="server" CssClass="calenderClass"></asp:TextBox>
                                        </td>
                                      </tr>
                                      <tr>
                                       
                                        <td align="right" >
                                            Share Drive:
                                        </td>
                                        <td align="left" >
                                             <asp:TextBox ID="share_driveTextBox" runat="server" Width="250px"></asp:TextBox>
                                        </td>
                                        <td align="right" >
                                            Model After:
                                        </td>
                                        <td align="left"  > 
                                            <asp:TextBox ID="ModelAfterTextBox" runat="server" Width="250px"></asp:TextBox>
                                        </td>
                                       </tr>
                                      <tr>
                                        <td align="right" >
                                            Request Close Date:
                                        </td>
                                        <td align="left" >
                                            <asp:TextBox ID= "close_dateTextBox" runat="server"></asp:TextBox>
                                        </td>
                                        <td >
                                          <asp:Label ID ="department_cdLabel" runat="server" Visible="False"></asp:Label>
                                        </td>
                                        <td>
                                              <asp:TextBox ID = "building_cdTextBox" runat="server" Visible ="False"></asp:TextBox>
                                        </td>
                                      </tr>

                                     <tr>
                                        <td colspan="4" align="left" >
                                            Comments:
                                        </td>
                                      </tr>
                                      <tr>
                                        <td colspan = "4" align="left" >
                                            <asp:TextBox ID="request_descTextBox" runat ="server" TextMode = "MultiLine" Width="100%"></asp:TextBox>
                                         </td>
                                      </tr>
                                      <tr>
                                        <td colspan = "4">
                                            <asp:Label ID="HDemployeetype" runat="server" Visible="false"></asp:Label>
                                        </td>
                                      </tr>

                                     </table>
                                    </td>
                                  </tr>
                       </table>
                      </ContentTemplate>

      </ajaxToolkit:TabPanel>
      <ajaxToolkit:TabPanel ID="tpRequestItems" runat="server" TabIndex="0"  BackColor="Black" Visible = "True">
                      <HeaderTemplate > 
                            <asp:Label ID="Label6" runat="server" Text="All Request Items" Font-Bold="true" ForeColor="Black"></asp:Label>
                    
                      </HeaderTemplate>
                      <ContentTemplate>
                          <table border="3" width = "100%">
                            <tr>
                                <td colspan = "4">
                                      <asp:Table ID = "tblRequestItems" runat = "server">
                                     </asp:Table>
            
                                </td>
        
                            </tr>
                           </table>
                  </ContentTemplate>
         </ajaxToolkit:TabPanel>  
         
              <ajaxToolkit:TabPanel ID="TPActionItems" runat="server" TabIndex="0"  BackColor="Black" Visible = "True">
            <HeaderTemplate > 
                <asp:Label ID="Label7" runat="server" Text="Action Items" Font-Bold="true" ForeColor="Black" ></asp:Label>
                    
            </HeaderTemplate>
            <ContentTemplate>
                <table border="3" width = "100%">
                <tr>
                    <td colspan = "4">
                            <asp:Table ID = "tblActionItems" runat = "server">
                            </asp:Table>
            
                    </td>
        
                </tr>
                </table>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>                 
    </ajaxToolkit:TabContainer>
  </td>
 </tr>
</table>

</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>

