﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="false"    CodeFile="NewRequestTicket.aspx.vb" Inherits="NewRequestTicket"  ViewStateMode="Enabled"%>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">

    <script type="text/javascript" src="Scripts/FieldValidator.js"></script>
<script type="text/javascript">
   function pageLoad(sender, args) {

      fieldValidator('textInputRequired', 'btnSubmit', 'lblValidation');

  }
</script>
    <style type="text/css">
        .style34
        {
            height: 40px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server" >
  <asp:UpdatePanel runat="server" UpdateMode ="Conditional" ID= "uplTicket">
     <Triggers>

        <asp:AsyncPostBackTrigger ControlID="RequestTyperbl" EventName = "SelectedIndexChanged" />
        <asp:PostBackTrigger ControlID="fileUpload2" />
        <%--<asp:PostBackTrigger ControlID="btnOpenfile" />--%>
<%--         <asp:AsyncPostBackTrigger ControlID="btnOpenfile" EventName="click" />
--%>
        <asp:PostBackTrigger ControlID="btnCompletion" />

    </Triggers>
     <ContentTemplate>
       <table width = "100%" >
        <tr>
           <td class="tableRowHeader">

                    Client Information
           </td>
        </tr>
        <tr>
           <td class="tableRowHeader">
                       <asp:Label ID="ClientnameHeader" runat="server" ></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="center">
                <table border="3px" style="font-size: small" width="99%">
                               <tr>
                                    <td align="right">
                                        First Name:
                                    </td>
                                    <td align="left"  >
                                        <asp:TextBox width="280px" ID = "FirstNameTextBox" runat="server" ></asp:TextBox>
   
                                    </td>
                                    <td align="right" >
                                        Last Name:
                                    </td>

                                    <td align="left" >
                                        <asp:TextBox ID = "LastNameTextBox" width="280px" runat="server"></asp:TextBox>
  
                                    </td>
                                </tr>
                                <tr>
                                   <td align="right" >
                                    Entity:
                                    </td>
                                    <td align="left">
                                    <asp:DropDownList ID ="entitycdDDL" runat="server"  Enabled="false" >
            
                                    </asp:DropDownList>
                                    </td>
       
                                    <td align="right" >
                                        <asp:Label ID="lbldept" runat="server" Text = "Department Name:" />
                                    </td>
                                    <td align="left"  >
                                    <asp:DropDownList ID ="departmentcdddl" runat="server" Width="250px" Enabled="false">
            
                                    </asp:DropDownList>
                                    </td>

                                </tr>
                                <tr>
                                    <td  align="right">
                                        Address 1:
                                        </td>
                                    <td align="left"> 
                                            <asp:TextBox ID = "Address1Textbox" runat="server" Width="250px"></asp:TextBox>
                                        </td>
                                    <td align="right" >
                                        Address 2:
                                        </td>

                                    <td align="left" >
                                        <asp:TextBox ID = "Address2textbox" runat="server" Width="250px"></asp:TextBox>
                                        </td>

                                </tr>
                                <tr>
                                    <td align="right">
                                        <asp:Label id="lblcity" runat="server" width="50px"  text="City:" />
                                    </td>
                                    <td align="left" colspan="3">
                                        <asp:TextBox ID = "Citytextbox" runat="server" width="500px"></asp:TextBox>
                                    </td>


                                </tr>

                                <tr>

                                    <td align="right">
                                        <asp:Label ID="lblstate" runat="server" Text="State:" Width="50px" />
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID = "Statetextbox" runat="server" Width="50px"></asp:TextBox>
                                        </td>
                                    <td align="right" >

                                        <asp:Label ID="lblzip" runat="server" Text="Zip:" Width="25px" />
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID = "Ziptextbox" runat="server" Width="50px"></asp:TextBox>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td align="right"  >
                                    Phone Number:
                                    </td>
                                    <td align="left" >
                                    <asp:TextBox ID = "PhoneTextBox" runat="server" MaxLength="12"></asp:TextBox>
                                    </td>
                                    <td align="right" >
                                    Email:
                                    </td>
                                    <td align="left" >
                                    <asp:TextBox ID = "EMailTextBox" runat="server" Width="250px"></asp:TextBox>
                                    </td>
                               </tr>

                            <tr>
                                <td align="right">
                                    Affiliation:
                                </td>
                                <td align="left">
                                    <asp:Label ID = "EmpTypeCdLabel" runat="server" ></asp:Label>
                                </td>
                                <td align="right">
                                    Position:
                                </td>
                                <td align="left">
                                    <asp:Label ID = "UserPositionDescLabel" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" >
                                Location:
                                </td>
                                <td align="left" >
         
                                    <asp:DropDownList ID ="facilitycdddl" runat="server" AutoPostBack="True">
              
                                </asp:DropDownList>
          
                                </td>
        
                                <td align="right" >
                                Building:
                                </td>
                                <td align="left" >

                                    <asp:DropDownList ID ="buildingcdddl" runat="server" AutoPostBack="True">
            
                                    </asp:DropDownList>
            
                                </td>
                        </tr>
                       <tr>
                            <td align="right" >
                                Floor:
                            </td>

                            <td align="left">
                                <asp:DropDownList ID="Floorddl" runat="server" AutoPostBack="True">
            
                                </asp:DropDownList>
                            
                            </td>
                            <td align="right" >
                                 Office:
                            </td>
                               
                            <td align="left" > 
                                <asp:TextBox ID="OfficeTextbox" runat="server"></asp:TextBox>
                            </td>


                        </tr>


                    <tr>

                        <td  align="right">
                            Fax:
                        </td>
                        <td align="left">
                        <asp:TextBox ID = "faxtextbox" runat="server" MaxLength="12"></asp:TextBox>
                        </td>

                        <td  align="right">
                            Account Name:
                        </td>
                        <td align="left">
                        <asp:Label ID = "loginnamelabel" runat="server" ></asp:Label>
                        </td>

                    </tr>

                    <tr>
                        <td  align="right">
                        Last Known Asset Tag:
                        </td>
                        <td align="left">
                        <asp:Label id="assettagLabel" runat="server"></asp:Label>
                        </td>
                        <td  align="right">
                         Submitting Ip Address:
                        </td>
                        <td align="left">
                        <asp:Label ID="ipaddressLabel" runat="server"></asp:Label>
                        </td>

                    </tr>

                    <tr>
                        <td colspan="2">
                            <asp:Label ID="RequestLbl" runat="server" Text="Request #" ></asp:Label>
                        </td>
                        <td colspan="2">
                            <asp:Label ID = "requestnumlabel" runat="server" ></asp:Label>

                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Label id="lblValidation" runat="server" Font-Bold="true" Font-Size="Small" ></asp:Label>
            </td>
        </tr>
        <tr>
          <td align="center" class="tableRowSubHeader">
             Select Request Type :
            
          </td>
        </tr>
        <tr>
            <td align="center" class="style34">
                    <table>
                             <tr>
                                <td colspan="2" align="center">
                                    <asp:Button ID="btnSubmitTicket" runat="server"  Text="Submit Ticket" 
                                     CssClass="btnhov" BackColor="#006666"  Width="175px"/> 

                                </td>

                                <td colspan="2" align="center">
                                       <asp:Button runat="server" ID="btnSubmitRFS" Text="Submit RFS" 
                                       CssClass="btnhov" BackColor="#006666"  Visible="false" Width="175px"/> 
                                </td>
                            
                                 <td colspan="2" align="center">
                                    <asp:Button ID="btnQuickClose" runat="server" Text="Quick Close"
                                    CssClass="btnhov" BackColor="#006666"  Width="175px"/> 
                                     
                                </td>

                        
                        </tr>

                   </table>
             </td>
        </tr>

        <tr>
            <td align="center">
            
                <asp:RadioButtonList ID="RequestTyperbl" runat="server" AutoPostBack="true" RepeatDirection="Horizontal">
                        <asp:ListItem Value="ticket" Text="Ticket"></asp:ListItem>
                        <asp:ListItem Value="service" Text="Req. For Service"></asp:ListItem>
                                    
                </asp:RadioButtonList>          
            
            </td>
        </tr>
        <tr>
        
            <td align="center">
                Do you want to attach a file or Image to this request?
                           <asp:RadioButtonList ID="LoadFilerlb" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Value="yes" Text="Yes"></asp:ListItem>
                        <asp:ListItem Value="no"  Selected="True" Text="No"></asp:ListItem>
                                    
                </asp:RadioButtonList>          

            
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Panel runat="server" ID="Panel1" Enabled="false">

                    <table border="3px" style="font-size: small" width="99%">
                        <tr align="center">
                        
                            <td align="center" colspan="4">

                                    <asp:Label ID="prioritylbl" runat="server" Text="Priority" Width="150px"></asp:Label>                            
                                       <asp:RadioButtonList ID="priorityrbl" runat="server"  Enabled ="false" RepeatDirection="Horizontal" Width="50px">
                            
                                        <asp:ListItem Value="1" Text="1"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="2"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="3" Selected="True"></asp:ListItem>
                                             
                                    </asp:RadioButtonList>                              
                            
                            
                            </td>
                        
                        </tr>
                        
                        <tr align="center">

                            <td align="right" colspan="2">
                                <asp:Label ID="altContactTicket" runat="server" Text="Alternate Contact"></asp:Label>
                            </td>
                            <td align="left" colspan="2">
                                 <asp:TextBox ID="alternatecontactnameTextBox" runat="server" Width="250px"></asp:TextBox>

                            </td>


                        </tr>
                        <tr align="center">
                            <td align="right" colspan="2">
                                    <asp:Label ID="altContactPhoneTicket" runat="server" Text="Alternate Phone" Width="150px"></asp:Label>
                            </td>
                            <td align="left" colspan="2">
                                    <asp:TextBox ID="alternatecontactphoneTextBox" runat="server" Width="150px"></asp:TextBox>
                            
                            </td>

                        </tr>

                         <tr align="center">
                                <td colspan = "4" align="center">
                                    <asp:Label ID="taglbl" runat="server" Text="Correct Asset Tag and IP Address"></asp:Label>

                                      <asp:RadioButtonList ID="tagradio" runat="server" RepeatDirection="Horizontal" Width="50px" AutoPostBack="true">
                            
                                        <asp:ListItem Value="yes" Selected="True" Text="Yes"></asp:ListItem>
                                        <asp:ListItem Value="no" Text="No"></asp:ListItem>

                                             
                                    </asp:RadioButtonList>  
                                </td>

                          </tr>

                           <tr align="center">

<%--                                <td>
                                    <asp:Label ID="proxlbl" runat="server" Text="Proxy "></asp:Label>                                
                                </td>
                            
                                <td>
                                    <asp:TextBox id="proxyonpcTextBox" runat="server" ></asp:TextBox>
                                
                                </td>

--%>                            
                                <td align="right" colspan="2">
                                            <asp:Label ID="assettagLbl" runat="server" Text="Asset Tag"></asp:Label>  
                                  </td>
                                  <td align="left" colspan="2">
                                            <asp:TextBox ID="assettagTextBox" runat="server" Width="270px"></asp:TextBox>        
                                </td>
                            </tr>
                            <tr>

                                <td align="right" colspan="2">
                                                            
                                    <asp:Label ID="iplbl" runat="server" Text="Ip Address"></asp:Label>
                                </td>
                                <td align="left" colspan="2">
                                    <asp:TextBox ID="ipaddressTextBox" runat="server"></asp:TextBox>
                                
                                </td>
                                
                            
                            </tr>
                        <tr>
                            <td colspan="4" align="center">
                                <table>
                                    <tr>
                                        <td align="right">
                                           <asp:Label ID="Catlbl" runat="server" Text = "Category"></asp:Label>
                                        
                                        </td>
                                        <td align="left">
                                        
                                            <asp:DropDownList ID="categorycdddl" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>

                                        </td>
                                        <td align="right">
                                            <asp:Label ID="Typelbl" runat="server" Text = "Type" Visible="false"></asp:Label>
                                       
                                        </td>
                                        <td align="left">
                                            <asp:DropDownList ID="typecdddl" runat="server" AutoPostBack="true" Visible="false">
                                            </asp:DropDownList>

                                        
                                        </td>
                                        <td align="right">
                                            <asp:Label ID="Itemlbl" runat="server" Text = "Item" Visible="false"></asp:Label>
                                        
                                        </td>
                                        <td align="left">
                                            <asp:DropDownList ID="itemcdddl" runat="server" AutoPostBack="true" Visible="false">
                                            </asp:DropDownList>
                                        
                                        </td>
                                    
                                    </tr>
                                </table>
                            
                            </td>
                        
                        </tr>
                    
                    </table>
                
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Panel runat="server" ID="techgroups" Visible="false">
                    Assign Groups/Techs
                    <table border="3px" style="font-size: small" width="99%">
                        <tr>

                                <td align="right">
                                        Assign Group
                                </td>
                                <td align="left">
                                    <asp:DropDownList runat="server" ID="changegroupddl" AutoPostBack="True"></asp:DropDownList>
                                    <asp:Label runat="server" ID="currentgroupnumLabel"  Visible="false" ></asp:Label>
                                    <asp:Label runat="server" ID="currentgroupnameLabel"  Visible="false" ></asp:Label>
                                    
                                </td>

                                <td align="right">
                                    <asp:Label ID="changteclb" runat="server" Text = "Assign Tech:"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:DropDownList runat="server" ID="ChangeTechddl" AutoPostBack="True" ></asp:DropDownList>
                                <asp:Label runat="server" ID="currenttechnumLabel"  Visible="false" ></asp:Label>
                                </td>

                            </tr>
                    
                            <tr>
                                <td colspan="4" align="center">
                                       <asp:label ID="seclbl" runat="server" Text = "Security Code(4 digits)->" Visible="false"></asp:label>
                                      <asp:TextBox ID="LastFourTextbox" runat="server" Visible="false" ></asp:TextBox>
                                </td>

                            </tr>

                    </table>
                </asp:Panel>

                
            </td>
        </tr>

<%--        <tr>
            <td align="center" colspan="4">
                    <asp:Panel ID="TicRFSPnl" runat="server">
                        <table border="3px" style="font-size: small" width="99%">


                        </table>
                    </asp:Panel>
            </td>
        </tr>--%>

        <tr>
            <td align="center">
                  <asp:Panel Visible="false" runat="server" ID="ticketPnl">
                        Ticket 
                        <table border="3px" style="font-size: small" width="99%">

                            <tr>
                                <td colspan="4" align="center"> 
                                    <asp:Label id="shorDeslbl" runat="server" Text = "Short Description" ></asp:Label>
                                
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" align="center"> 
                                    <asp:TextBox ID="shortdescTextBox" runat="server"  Rows="2" TextMode="MultiLine" 
                                        Width="95%" MaxLength="20"></asp:TextBox>
                                
                                </td>
                            </tr>
                        <tr>
                            <td  colspan="4" align="center">
                                Full Description
                            </td>   
                            
                        </tr>
                        <tr>
                            <td  colspan="4" align="center">
                                <asp:TextBox ID="desctextbox" runat="server"  rows="6" TextMode="MultiLine"  Width="95%"></asp:TextBox>
                            
                             </td>
                        
                        </tr>

                            <tr>
                                <td colspan="4">

                                    <asp:GridView ID="GridOnCall" runat="server" AllowSorting="True" 
                                        AutoGenerateColumns="False" BackColor="White" BorderColor="#336666" 
                                        BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                                        DataKeyNames="group_num,tech_num"
                                        EmptyDataText="No On Call" 
                                        EnableTheming="False" Font-Size="Medium" GridLines="Horizontal" Width="100%">
                                        <FooterStyle BackColor="White" ForeColor="#333333" />
                                        <HeaderStyle BackColor="#336666" Font-Bold="False" Font-Size="Small" 
                                            ForeColor="White" HorizontalAlign="Left" />
                                        <PagerStyle BackColor="#336666" Font-Size="Medium" ForeColor="White" 
                                            HorizontalAlign="Left" />
                                        <RowStyle BackColor="White" ForeColor="#333333" />
                                        <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                        <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                        <SortedAscendingHeaderStyle BackColor="#487575" />
                                        <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                        <SortedDescendingHeaderStyle BackColor="#275353" />
                                         <Columns>

                                            <asp:CommandField ButtonType="Button" SelectText="Select" ShowSelectButton= "true" HeaderStyle-HorizontalAlign = "Left"  ControlStyle-BackColor="#84A3A3" ItemStyle-Height="30px" />
                                            <asp:BoundField DataField="group_name" HeaderText="On Call Group Name" />
                                            <asp:BoundField DataField="techname" HeaderText="Currently On Call" />
                                        </Columns>
                                    </asp:GridView>
                                
                                </td>
                            
                            </tr>

                        </table>
                     </asp:Panel>
            </td>
        </tr>

        <tr>
            <td>
                <asp:Panel Visible="False" runat="server" ID="rfsPnl">

                    <table border="3px" style="font-size: small" width="99%" >
                         <tr>
                            <td colspan="4" align="center"> 
                                Completion Date: 
                                <asp:TextBox ID="completion_dateTextBox" runat="server" CssClass="calenderClass"></asp:TextBox>

                            </td>
                        </tr>
                        <tr class="tableRowSubHeader">
                            <td colspan="4" align="left">
                                Description / Explanation                            
                           
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="center">
                                    <asp:TextBox ID="service_descTextbox" runat="server" TextMode="MultiLine" 
                                        Width="95%"></asp:TextBox>
                           
                            </td>

                        </tr>
                        <tr class="tableRowSubHeader">
                            <td colspan="4" align="left">
                            
                                Business Justification / Reason for Request
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="center">
                                <asp:TextBox runat="server" ID ="business_desTextBox" TextMode="MultiLine"  Width="95%" Rows ="3"></asp:TextBox>
                            
                            
                            </td>
                        </tr>

                        <tr class="tableRowSubHeader">
                            <td colspan="4" align="left">
                            
                            List Any Known Departments or Systems This Change Will Impact, or any Deadlines That Could Affect This Request

                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="center">
                                    <asp:TextBox runat="server" ID="impact_descTextBox" TextMode="MultiLine"  Width="95%" Rows="4"></asp:TextBox>
                            
                            </td>
                        </tr>

                        <tr align="center">
                            <td colspan="4">
                                <asp:Panel ID="TechPanl" runat="server" Visible="false">
                                <table border="2px" width="100%">
                                        <tr  class= "tableRowSubHeader">
                                            <td colspan="4">
                                                Technical Services
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                Service Type
                                            </td>
                                    
                                            <td>
                                                    #of Devices
                                            </td>
                                            <td>
                                                Device IDs
                                            </td>
                                            <td>
                                                    Description
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                    Terminals
                                            </td>

                                                <td>                                        
                                                    <asp:DropDownList runat="server" ID = "num_of_terminalsddl">
                                                        <asp:ListItem Value="0" Text="Select" />
                                                        <asp:ListItem Value="1" Text="1" />
                                                        <asp:ListItem Value="2" Text="2" />
                                                        <asp:ListItem Value="3" Text="3" />
                                                        <asp:ListItem Value="4" Text="4" />
                                                        <asp:ListItem Value="5" Text="5" />
                                                        <asp:ListItem Value="6" Text="6" />
                                                        <asp:ListItem Value="7" Text="7" />
                                                        <asp:ListItem Value="8" Text="8" />
                                                        <asp:ListItem Value="9" Text="9" />
                                                        <asp:ListItem Value="10" Text="10" />

                                                    </asp:DropDownList>
                                        
                                            </td>

                                            <td>
                                                <asp:TextBox ID="terminal_idsTextBox" runat="server" Width="200px"></asp:TextBox>
                                                                
                                            </td>
                                            <td>
                                                <asp:TextBox ID ="terminal_info_descTextBox" runat="server" TextMode="MultiLine"
                                                    Width="300px"></asp:TextBox>
                                            </td>
                                        </tr>

                                            <tr>
                                                <td>
                                                Printers
                                                </td>

                                                <td>
                                                                 
                                                <asp:DropDownList runat="server" ID = "num_of_printersddl">
                                                        <asp:ListItem Value="0" Text="Select" />
                                                        <asp:ListItem Value="1" Text="1" />
                                                        <asp:ListItem Value="2" Text="2" />
                                                        <asp:ListItem Value="3" Text="3" />
                                                        <asp:ListItem Value="4" Text="4" />
                                                        <asp:ListItem Value="5" Text="5" />
                                                        <asp:ListItem Value="6" Text="6" />
                                                        <asp:ListItem Value="7" Text="7" />
                                                        <asp:ListItem Value="8" Text="8" />
                                                        <asp:ListItem Value="9" Text="9" />
                                                        <asp:ListItem Value="10" Text="10" />

                                                    </asp:DropDownList>

                                                </td>
                                            <td>
                                                    <asp:TextBox runat="server" ID="printer_idsTextBox" Width="200px"></asp:TextBox>

                                        
                                            </td>
                                            <td>
                                                    <asp:TextBox runat="server" ID ="printer_info_descTextBox" TextMode="MultiLine"
                                                    Width="300px"></asp:TextBox>
                                        
                                            </td>
                                        </tr>


                                            <tr>
                                                <td>
                                                PC
                                                </td>

                                                <td>
                                                                 

                                                <asp:DropDownList runat="server" ID = "num_of_pcddl">

                                                        <asp:ListItem Value="0" Text="Select" />
                                                        <asp:ListItem Value="1" Text="1" />
                                                        <asp:ListItem Value="2" Text="2" />
                                                        <asp:ListItem Value="3" Text="3" />
                                                        <asp:ListItem Value="4" Text="4" />
                                                        <asp:ListItem Value="5" Text="5" />
                                                        <asp:ListItem Value="6" Text="6" />
                                                        <asp:ListItem Value="7" Text="7" />
                                                        <asp:ListItem Value="8" Text="8" />
                                                        <asp:ListItem Value="9" Text="9" />
                                                        <asp:ListItem Value="10" Text="10" />

                                                    </asp:DropDownList>

                                                </td>
                                            <td>
                                                    <asp:TextBox runat="server" ID="pc_idsTextBox" Width="200px"></asp:TextBox>

                                        
                                            </td>
                                            <td>
                                                    <asp:TextBox runat="server" ID ="pc_info_descTextBox" TextMode="MultiLine"
                                                    Width="300px"></asp:TextBox>
                                        
                                            </td>
                                        </tr>

                                            <tr>
                                                <td>
                                                Network Drops
                                                </td>

                                                <td>
                                                                 
                                                <asp:DropDownList runat="server" ID = "num_of_net_dropsddl">
                                                        <asp:ListItem Value="0" Text="Select" />

                                                        <asp:ListItem Value="1" Text="1" />
                                                        <asp:ListItem Value="2" Text="2" />
                                                        <asp:ListItem Value="3" Text="3" />
                                                        <asp:ListItem Value="4" Text="4" />
                                                        <asp:ListItem Value="5" Text="5" />
                                                        <asp:ListItem Value="6" Text="6" />
                                                        <asp:ListItem Value="7" Text="7" />
                                                        <asp:ListItem Value="8" Text="8" />
                                                        <asp:ListItem Value="9" Text="9" />
                                                        <asp:ListItem Value="10" Text="10" />

                                                    </asp:DropDownList>

                                                </td>
                                            <td>
                                                    <asp:TextBox runat="server" ID="net_drop_idsTextBox" Width="200px"></asp:TextBox>

                                        
                                            </td>
                                            <td>
                                                    <asp:TextBox runat="server" ID ="net_drops_info_descTextBox" TextMode="MultiLine" 
                                                    Width="300px"></asp:TextBox>
                                        
                                            </td>
                                        </tr>

                                            <tr>
                                                <td>
                                                Other
                                                </td>

                                                <td>
                                                                 
                                                <asp:DropDownList runat="server" ID = "num_of_otherddl">
                                                        <asp:ListItem Value="0" Text="Select" />

                                                        <asp:ListItem Value="1" Text="1" />
                                                        <asp:ListItem Value="2" Text="2" />
                                                        <asp:ListItem Value="3" Text="3" />
                                                        <asp:ListItem Value="4" Text="4" />
                                                        <asp:ListItem Value="5" Text="5" />
                                                        <asp:ListItem Value="6" Text="6" />
                                                        <asp:ListItem Value="7" Text="7" />
                                                        <asp:ListItem Value="8" Text="8" />
                                                        <asp:ListItem Value="9" Text="9" />
                                                        <asp:ListItem Value="10" Text="10" />

                                                    </asp:DropDownList>

                                                </td>
                                            <td>
                                                    <asp:TextBox runat="server" ID="other_idsTextBox" Width="200px"></asp:TextBox>

                                        
                                            </td>
                                            <td>
                                                    <asp:TextBox runat="server" ID ="other_info_descTextBox" TextMode="MultiLine"
                                                    Width="300px"></asp:TextBox>
                                        
                                            </td>
                                        </tr>

<%--                                                            <tr>
                                            <td align="center" colspan="4">
                                                        <asp:Button runat="server" ID ="btnAddTechService" Text="Add Tech Service"
                                                        CssClass="btnhov" BackColor="#006666"  Width="179px"/> 
                                        
                                            </td>
                                        </tr>--%>
                                    </table>
                              </asp:Panel>

                            </td>
                        </tr>

                         <tr align="center">
                              <td colspan="4" align="center">
                                    
                                <asp:Panel ID="reportPanel" runat="server" Visible="False">
                                        <table border="2px" width="100%">
                                            <tr class= "tableRowSubHeader">
                                                <td colspan="4">
                                                    Report Data Elements
                                                </td>
                                            </tr>

                                            <tr>
                                            
                                                <td>
                                                        Hospital:
                                                </td>
                                            
                                                <td >
                                                        District Code:
                                                </td>
                                            
                                                <td  class="style1">
                                                    Report Frequency:
                                                
                                                </td>
                                            
                                                <td  class="style1">
                                                    Type of Report:
                                                
                                                </td>

                                            </tr>

                                            <tr>
                                            
                                                <td  valign="top">
                                                
                                                    <asp:CheckBoxList ID="cblFacilities" runat="server" 
                                                            RepeatDirection="Vertical" >
                                                        </asp:CheckBoxList>
                                                </td>

                                                <td valign="top">
                                                    <asp:CheckBoxList ID="cblDistrictCodes" runat="server" RepeatColumns="0"
                                                        AutoPostBack="false">
                                                     </asp:CheckBoxList>
                                                
                                                </td>

                                                <td valign="top">
                                                    <asp:DropDownList runat="server" ID = "ReportFreqddl" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                    
                                                    <asp:TextBox ID="ReportFreqTextBox" runat="server" Visible="false">
                                                    </asp:TextBox>

                                                    <asp:TextBox ID="ReportFreqDescTextBox" runat="server" Visible="false">
                                                    </asp:TextBox>

                                                </td>
                                                <td valign="top">
                                                
                                                     <asp:DropDownList runat="server" ID = "ReportTypeddl" AutoPostBack="true">
                                                        <asp:ListItem Value="0" Text="Select" />
                                                        <asp:ListItem Value="statistical" Text="Statistical" />
                                                        <asp:ListItem Value="detail" Text="Detail" />
                                                        <asp:ListItem Value="summary" Text="Summary" />
                                                        <asp:ListItem Value="detailsummary" Text="Detail with Summary" />
                                                    </asp:DropDownList>
 
                                                    <asp:TextBox ID="ReportTypeTextBox" runat="server" Visible="false"></asp:TextBox>
                                                    <asp:TextBox ID="ReportTypeDescTextBox" runat="server" Visible="false"></asp:TextBox>
                                            
                                            </tr>
                                            <tr>
                                                <td colspan="2" class="style1">
                                                    Data Fields (i.e. Patient Accct# Hosp. Service)
                                                </td>
                                    
                                                <td colspan="2">
                                                        Date Range (From -> To) 
                                                </td>
                                            </tr>


                                            <tr align="center">
                                                                                                                              

                                                <td align="center" colspan="2" class="style1">
                                                    <asp:TextBox ID="DataFieldsTextBox" runat="server" 
                                                    Width="100%" TextMode="MultiLine" Height="80px"></asp:TextBox>
                                                                
                                                </td>
                                                <td align="center" colspan="2">
                                                    <asp:TextBox ID ="DateRangeTextBox" runat="server" TextMode="MultiLine" 
                                                    Width="100%" Height="80px"></asp:TextBox>
                                                </td>
                                                </tr>

                                            <tr>

                                                <td colspan="2" class="style1">
                                                    Total on Field(s)
                                                </td>
                                                <td colspan="2">
                                                        Description 
                                                </td>
                                            </tr>


                                                <tr align = "center">

                                                <td align="center" colspan="2" class="style1">
                                                                
                                                    <asp:TextBox ID="TotalOnfieldTextBox" runat="server" Width="100%" 
                                                    TextMode="MultiLine" Height="80px"></asp:TextBox>
                                                                    
                                                </td>
                                                <td align="center" colspan="2">
                                                                
                                                    <asp:TextBox ID="ReportDescTextBox" runat="server" Width="100%" 
                                                    TextMode="MultiLine" Height="80px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    Other Information:
                                                </td>
                                            </tr>
                                            <tr align="center" >
                                                <td align="center" colspan="4">
                                                                                
                                                    <asp:TextBox ID="reportOtherDescTextBox" runat="server" Width="100%" 
                                                    TextMode="MultiLine" Height="80px"></asp:TextBox>
                                                </td>
                                                                            
                                            </tr>
                                                             
                                        </table>
                                    </asp:Panel>

                                </td>
                                
                            </tr>
                            <tr>

                                <td colspan="4">

                                    <asp:GridView ID="gvTechServices" runat="server" AllowSorting="True" 
                                        AutoGenerateColumns="False" BackColor="White" BorderColor="#336666"
                                        BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                                        EmptyDataText="" EnableTheming="False" DataKeyNames="SeqNum"
                                        Font-Size="Medium" GridLines="Horizontal" Width="100%">
                                   
                                            <FooterStyle BackColor="White" ForeColor="#333333" />
                                            <HeaderStyle BackColor="#336666" Font-Bold="False" ForeColor="White" Font-Size="Small" HorizontalAlign="Left"    />                                    
                                            <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Left" Font-Size="Medium"/>
                                            <RowStyle BackColor="White" ForeColor="#333333" />
                                            <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                            <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                            <SortedAscendingHeaderStyle BackColor="#487575" />
                                            <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                            <SortedDescendingHeaderStyle BackColor="#275353" />

                                            <Columns>
                                                                                            
                                                        <asp:CommandField ButtonType="Button" SelectText="Remove" ShowSelectButton= "true" />
                                                        <asp:BoundField  DataField="TechServiceDesc"  HeaderText="" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
				                                        <asp:BoundField  DataField="NumOfDevices"  HeaderText="" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
                                                        <asp:BoundField  DataField="DeviceID"  HeaderText="" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
                                                        <asp:BoundField  DataField="DetailInformation"  HeaderText="" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
                                  
                                            </Columns>

                                        </asp:GridView>
                            
                                </td>
                        
                        </tr>

                    </table>

              </asp:Panel>
            </td>

        </tr>

        <tr>
            <td>
                    <table>
                    
                    </table>
            
            </td>
        </tr>
        <tr>
            <td>

                    <asp:Table ID="tblVars" runat="server" Visible="false">
                                
                    </asp:Table>                            

            </td>
        </tr>
        <tr>
            <td>
            

                        <asp:Label ID="client_numLabel"  runat="server" Visible="False"></asp:Label>
                        <asp:Label ID="entryclientnumLabel"  runat="server" Visible="False"></asp:Label>

                        <asp:Label ID="firstnameLabel"  runat="server" Visible="False"></asp:Label>
                        <asp:Label ID="lastnameLabel"  runat="server" Visible="False"></asp:Label>
                        <asp:Label ID="ntloginLabel"  runat="server" Visible="False"></asp:Label>
                        

                        <asp:Label ID ="DepartmentCdLabel" runat="server" Visible="False"></asp:Label>
                        <asp:Label ID ="TitleLabel" runat="server" Visible="False"></asp:Label>
                                     
                        <asp:Label ID = "EntityCdLabel" runat="server" Visible ="False"></asp:Label>
                        <asp:Label ID = "EntityNameLabel" runat="server" Visible ="False"></asp:Label>
                        <asp:Label ID = "EntityeMailNameLabel" runat="server" Visible ="False"></asp:Label>


                        <asp:Label ID= "BuildingCdLabel" runat="server" Visible="False" ></asp:Label>
                        <asp:Label ID= "buildingnameLabel" runat="server" Visible="False" ></asp:Label>
                        <asp:Label ID= "buildingEmailnameLabel" runat="server" Visible="False" ></asp:Label>


                        <asp:Label ID="FacilityCdLabel" runat="server" visible="False" />
                        <asp:Label ID="FacilityCdeMailLabel" runat="server" visible="False" />

                        
                        <asp:Label ID="floorLabel" runat="server" visible="False" />
                        <asp:Label ID="floorEmailLabel" runat="server" visible="False" />


                        <asp:Label ID="HDemployeetype" runat="server" Visible="False"></asp:Label> 
                        <asp:Label ID="suffixlabel" runat="server" Visible="False"></asp:Label>
                        
                        <asp:Label ID= "AccountRequestTypeLabel" runat="server" Visible="False"></asp:Label>
                        <asp:Label ID="typecd" runat="server" Visible="False"></asp:Label>
                        <asp:Label ID="categorycd"  runat="server" Visible="False"></asp:Label>
                        <asp:Label ID="RequestType" runat="server" Visible="False"></asp:Label>
                                     
                        <asp:Label ID="itemcd" runat="server" visible="False" />
                        <asp:Label ID="requestor_client_numLabel" runat="server" visible="False"></asp:Label>
                        <asp:Label ID ="CoverageGroupNumLabel"  runat="server" visible="False"></asp:Label>
                        <asp:Label id="NonPerferedGroupnumLabel" runat="server" Visible="False"></asp:Label>

                        <asp:Label ID="CommLocationOfCareIDLabel" runat="server" visible="False"></asp:Label>
                        <asp:Label ID="PositionRoleNumLabel" runat="server" Visible="False"></asp:Label>
                        <asp:Label id="invisionLbl" runat="server" Text="eCare Group Number:" Visible="False"></asp:Label>
                        <asp:TextBox ID="groupnumTextBox" runat="server" Enabled="False" Visible="False"></asp:TextBox>
                        <asp:TextBox ID="InvisionGroupNumTextBox" runat="server"  Enabled="False" Visible="False"></asp:TextBox>

                        <asp:TextBox ID="imagelocation" runat="server" Visible="false"></asp:TextBox>
                        
                       <asp:TextBox ID="txtFilename" runat="server" Visible="false"></asp:TextBox>
                        
            </td>
        </tr>
    
        <tr>
            <td>
            
              <%--  ======================== Ticket/RFS completion panel ========================== --%>
       	        <asp:Panel ID="pnlSubmit" runat="server" Height="350px" Width="575px"  BackColor="#FFFFCC" style="display:none" >
                <table  id="Table1" runat="server" style="border: medium groove #000000; height:100%"  align="center" width="100%" >
                    <tr>
                        <td style="font-weight: bold; font-size: larger; text-align: center;" align="center">
                            <asp:label id="CompletLbl" runat="server" text="Your request has been submitted, you can view the progress by clicking on the  link below. A new EMail has been sent to you for your records. ">
                            </asp:label>
                         </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold; font-size: larger; text-align: center;" align="center">
                
                            <asp:label ID="CompletionLbl" runat="server"></asp:label>
                        </td>
            
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:RadioButtonList ID="completionrbl" runat="server" AutoPostBack="true">
                                              <asp:ListItem Value="yes" Text="Yes" Selected="True" ></asp:ListItem>
                                                <asp:ListItem Value="no" Text="No"></asp:ListItem>
                            </asp:RadioButtonList>

                        
                        </td>
                    </tr>
                    <tr>
                        <td align="center" >
                            <asp:Button ID="btnCompletion" runat="server" BackColor="#00C000" Font-Bold="True" Font-Names="Arial"
                                Font-Size="Small" ForeColor="Black" Height="25px" Text="Continue"   Width="120px" />

                        </td>
                    </tr>
                         
                </table>
                <br />
                </asp:Panel> 

                <ajaxToolkit:ModalPopupExtender ID="ModalCompletionPop" runat="server" 
                    TargetControlID="HdBtnCompleted" PopupControlID="pnlSubmit" 
                    BackgroundCssClass="ModalBackground" >
                    </ajaxToolkit:ModalPopupExtender>

                    <asp:Button ID="HdBtnCompleted" runat="server" style="display:none" />
            
            
            </td>
        </tr>
         
           <tr>
              <td><%--  ======================== UpLoad file  ========================== --%>
                  <asp:Panel ID="Panel2" runat="server" BackColor="#FFFFCC" Height="350px" style="display:none" Width="575px">
                      <table id="Table3" runat="server" align="center" style="border: medium groove #000000; height:100%" width="100%">
                          <tr>
                              <td align="center" style="font-weight: bold; font-size: larger; text-align: center;">
                                  <asp:Label ID="Label5" runat="server" text="Upload a file with your Reqest">
                            </asp:Label>
                              </td>
                          </tr>
                          <tr class="tableRowSubHeader">
                              <td align="center" colspan="4" width="75%">
                                  <asp:Label ID="Label6" runat="server" Text="Select file to save with request#"></asp:Label>
                              </td>
                          </tr>
                          <tr>
                              <td align="center" colspan="4" width="95%">
                                  <ajaxToolkit:AsyncFileUpload ID="AsyncFileUpload1" runat="server" />
                              </td>
                          </tr>
                          <tr>
                              <td align="center">
                                  <asp:Button ID="Button1" runat="server" BackColor="#00C000" Font-Bold="True" Font-Names="Arial" Font-Size="Small" ForeColor="Black" Height="25px" Text=" Continue " Width="170px" />
                                  <asp:Button ID="Button2" runat="server" BackColor="#00C000" Font-Bold="True" Font-Names="Arial" Font-Size="Small" ForeColor="Black" Height="25px" Text=" Cancel" Width="170px" />
                                  <asp:Label ID="Label3" runat="server" Visible="false"></asp:Label>
                                  <asp:Label ID="Label4" runat="server" Visible="false"></asp:Label>
                                  <asp:Label ID="Label7" runat="server" Visible="false"></asp:Label>
                              </td>
                          </tr>
                          <%--            <tr>
                            <td  colspan="4" align="center" width="95%">
                                      <asp:Button ID="btnOpenfile" runat="server"  Text="Load file"  
                                    CssClass="btnhov" BackColor="#006666"  Width="175px"/>                            


                            </td>
            
                        </tr>--%>
                          <tr>
                              <td colspan="4">
                                  <img id="Img2" runat="server"  visible="false" src ="CSCFileUpload/Image1.jpg" alt="" />
                                  <img id="Img3" runat="server"  visible="false" src ="~/FileUpload/Image1.jpg" alt="" />
                              </td>
                          </tr>
                      </table>
                      <br />
                  </asp:Panel>
                  <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" 
                      BackgroundCssClass="ModalBackground" PopupControlID="PanFile" TargetControlID="HDBtnFile">
                  </ajaxToolkit:ModalPopupExtender>
                  <asp:Button ID="Button3" runat="server" style="display:none" />
              </td>
          </tr>
        <tr>
            <td>
                
               <%--  ======================== UpLoad file  ========================== --%>
       	        <asp:Panel ID="PanFile" runat="server" Height="350px" Width="575px"  BackColor="#FFFFCC" style="display:none" >
                <table  id="Table2" runat="server" style="border: medium groove #000000; height:100%"  align="center" width="100%" >
                    <tr>
                        <td style="font-weight: bold; font-size: larger; text-align: center;" align="center">
                            <asp:label id="Label2" runat="server" text="Upload a file with your Reqest">
                            </asp:label>
                         </td>
                    </tr>

                    <tr class="tableRowSubHeader">
                        <td colspan="4" align="center" width="75%">
                                <asp:Label id="Label1" runat="server" Text = "Select file to save with request#" ></asp:Label>

                        </td>
                    </tr>
                    
                        <tr>
                        <td colspan="4" align="center" width="95%">


                                    <ajaxToolkit:AsyncFileUpload ID="fileUpload2" runat="server" />

                        </td>
                    </tr>
                     <tr>
                        <td align="center" >
                            <asp:Button ID="btnFilComplete" runat="server" BackColor="#00C000" Font-Bold="True" Font-Names="Arial"
                                Font-Size="Small" ForeColor="Black" Height="25px" Text=" Continue "  Width="170px" />

                                <asp:Button ID="btnCancelfile" runat="server" BackColor="#00C000" Font-Bold="True" Font-Names="Arial"
                                Font-Size="Small" ForeColor="Black" Height="25px" Text=" Cancel"   Width="170px" />

                             <asp:Label ID="filenamelbl" runat="server"  Visible="false"></asp:Label>
                             <asp:Label ID="Fileextensionlbl" runat="server" Visible="false"></asp:Label>
                             
                             <asp:Label ID="filetypelbl" runat="server" Visible="false"></asp:Label>
                             

                        </td>
                    </tr>

            <%--            <tr>
                            <td  colspan="4" align="center" width="95%">
                                      <asp:Button ID="btnOpenfile" runat="server"  Text="Load file"  
                                    CssClass="btnhov" BackColor="#006666"  Width="175px"/>                            


                            </td>
            
                        </tr>--%>
                    <tr>
                        <td colspan="4">
                            <img id="Img1" runat="server"  visible="false" src ="CSCFileUpload/Image1.jpg" alt="" />
                            
                            <img id="requestImg2" runat="server"  visible="false" src ="~/FileUpload/Image1.jpg" alt="" />
                                
                        </td>
                    </tr>

                         
                </table>
                <br />
                </asp:Panel> 

                <ajaxToolkit:ModalPopupExtender ID="ModalFilePop" runat="server" 
                    TargetControlID="HDBtnFile" PopupControlID="PanFile" 
                    BackgroundCssClass="ModalBackground" >
                    </ajaxToolkit:ModalPopupExtender>

                    <asp:Button ID="HDBtnFile" runat="server" style="display:none" />
            

            </td>
        </tr>
    </table>
  </ContentTemplate>
 </asp:UpdatePanel>
</asp:Content>