﻿Imports System.IO
Imports System.Data '
Imports System.Data.SqlClient
Imports App_Code
Imports AjaxControlToolkit

Partial Class SurveyReport
    Inherits System.Web.UI.Page
    Dim iProjectNum As Integer
    Dim FormSignOn As New FormSQL()
    'Dim VendorFormSignOn As FormSQL
    'Dim UpdPhyFormSignOn As FormSQL
    Dim iClientNum As Integer
    Dim iUserNum As Integer
    Dim iUserSecurityLevel As Integer
    Dim dtSelected As DataTable

    Dim UserInfo As UserSecurity

    Dim RequestNum As String
    Dim thisdata As CommandsSqlAndOleDb

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        UserInfo = New UserSecurity(Session("LoginID"), Session("EmployeeConn"))

        iUserNum = UserInfo.ClientNum
        iUserSecurityLevel = UserInfo.SecurityLevelNum
        'userDepartmentCD.Text = UserInfo.DepartmentCd

        Select Case iUserSecurityLevel
            Case (81), (80), (90), (91), (92), (93), (94), (100), (105)

            Case Else
                Response.Redirect("~/SimpleSearch.aspx", True)

        End Select

        Dim SurveyContr As New SurveyController()
        'FormSignOn = New FormSQL(Session("CSCConn"), "SelSurveyReport", "", "", Page)
        'FormSignOn.AddInsertParm("@ProjectDesc", ProjectDesctxt.Text, SqlDbType.NVarChar, 275)

        If Not IsPostBack Then

            'If GNumber <> "" Then

            '    tabs.ActiveTabIndex = 1

            'End If

            'Dim ddlBinder As New DropDownListBinder(Technicianddl, "SelSurveyQuestions", Session("EmployeeConn"))
            'ddlBinder.AddDDLCriteria("@techType", "project", SqlDbType.NVarChar)

            'ddlBinder.BindData("client_num", "tech_name")


            bindSurveys()

        End If


    End Sub
    Protected Sub bindSurveys()
        Dim thisData As New CommandsSqlAndOleDb("SelSurveyReport", Session("CSCConn"))

        'thisData.AddSqlProcParameter("@projectType", ProjectTyperbl.SelectedValue, SqlDbType.NVarChar)

        Dim surveydt As DataTable

        surveydt = thisData.GetSqlDataTable

        GvSurveys.DataSource = surveydt
        GvSurveys.DataBind()

        Session("GvSurveystbl") = surveydt


        GvSurveys.SelectedIndex = -1


    End Sub

    Protected Sub btnExcel_Click(sender As Object, e As System.EventArgs) Handles btnExcel.Click

        Response.Redirect("./Reports/RolesReport.aspx?ReportName=SurveyReport", True)

    End Sub

    Protected Sub GvSurveys_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GvSurveys.RowCommand
        If e.CommandName = "Select" Then
            GvSurveys.SelectedIndex = -1
            RequestNum = GvSurveys.DataKeys(Convert.ToInt32(e.CommandArgument))("request_num").ToString()

            Response.Redirect("./ViewRequestTicket.aspx?RequestNum=" & RequestNum, True)

        End If
    End Sub
    Protected Sub GvSurveys_Sorting(sender As Object, e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles GvSurveys.Sorting
        Dim sortdt = TryCast(Session("GvSurveystbl"), DataTable)

        If sortdt IsNot Nothing Then

            'Sort the data.
            sortdt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression)
            GvSurveys.DataSource = sortdt
            GvSurveys.DataBind()

        End If

    End Sub
    Private Function GetSortDirection(ByVal column As String) As String

        ' By default, set the sort direction to ascending.
        Dim sortDirection = "ASC"

        ' Retrieve the last column that was sorted.
        Dim sortExpression = TryCast(ViewState("SortExpression"), String)

        If sortExpression IsNot Nothing Then
            ' Check if the same column is being sorted.
            ' Otherwise, the default value can be returned.
            If sortExpression = column Then
                Dim lastDirection = TryCast(ViewState("SortDirection"), String)
                If lastDirection IsNot Nothing _
                  AndAlso lastDirection = "ASC" Then

                    sortDirection = "DESC"

                End If
            End If
        End If

        ' Save new values in ViewState.
        ViewState("SortDirection") = sortDirection
        ViewState("SortExpression") = column

        Return sortDirection

    End Function
End Class
