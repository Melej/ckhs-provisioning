﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="false" CodeFile="ActiveManagers.aspx.vb" Inherits="ActiveManagers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <style type="text/css">
        .style2
        {
            width: 50%;
            height: 232px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="100"  >
   <ProgressTemplate>
        <div id="IMGDIV" align="center" valign="middle" runat="server" style="position:
                absolute;left: 400px;top:150px;visibility:visible;vertical-align:middle;border-style:
                inset;border-color:black;background-color:White;width:210px;height:75px; z-index: 999;">
        Loading Data ...
            <img src="Images/AjaxProgress.gif" /><br />
        </div>
   </ProgressTemplate>
</asp:UpdateProgress>

<asp:UpdatePanel ID="uplActiveMgr" runat="server"  >
   <ContentTemplate>
  
    <table>
       <tr>
        <th align="center" colspan="4" class="tableRowHeader">
       
           CKHS OnLine Provisioning Directors/VP to Departments

        </th>

      </tr>

      <tr>
        <td colspan="4">
            <table border="2px" width="100%">
            
                    <tr>
                      <td colspan="4" align="center">
                        You can search by ONLY 1 criteria
          
                      </td>
                  </tr>
                  <tr>
                     <td align="right">
                        Last Name
                    </td>
                    <td colspan="2">
                     <asp:TextBox ID="LastNameTextBox" Width="200px" runat = "server"></asp:TextBox>
                    </td>
                     <td>
                    </td>

                  </tr>
                  <tr>

                    <td align="right">
                        <asp:Label ID="lblEntity" runat="server" Text="Entity Name:" />
                    </td>

                    <td align="left">
                                    <asp:DropDownList ID="entity_cdDDL" runat="server" AutoPostBack="true">
                                     </asp:DropDownList>
                            <asp:TextBox ID="entityText" runat="server" Visible="false">
                            </asp:TextBox>
                    </td>

                    <td align="right">
                        <asp:Label ID="lbldept" runat="server" Text="Department Name:" />
                    </td>
                    <td align="left">
                        <asp:DropDownList ID="department_cdddl" runat="server" Width="250px">
                        </asp:DropDownList>
                        <asp:TextBox ID="departmentText" runat="server" Visible="false"></asp:TextBox>
                    </td>

                  </tr>
                  <tr>
                      <td  align="center" colspan="4">
                        <table>
                            <tr>
                                <td align="center">

                                        <asp:Button ID ="btnSearch" runat = "server" Text ="Search"  Width="120px" Height="30px"
                                             Font-Names="Arial" ForeColor="White" Font-Bold="True" Font-Size="Small" />
                                </td>
                                <td align="center" >

                                        <asp:Button ID = "btnReset" runat= "server" Text="Reset"  Width="120px" Height="30px"
                                             Font-Names="Arial" ForeColor="White" Font-Bold="True" Font-Size="Small" />

                                </td>
                            </tr>
                        </table>
    
                    </td>
                  </tr>          
            
            
            </table>
        
        </td>
      
      </tr>




      <tr>

          <td class="style2" colspan="4" valign="top">
              <asp:GridView ID="grActiveManagers" runat="server" AllowSorting="True" 
                  AutoGenerateColumns="False" BackColor="White" BorderColor="#336666" 
                  BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                  DataKeyNames="client_num" EmptyDataText="No Managers Found" 
                  EnableTheming="False" Font-Size="Small" GridLines="Horizontal" Width="100%">
                  <FooterStyle BackColor="White" ForeColor="#333333" />
                  <HeaderStyle BackColor="#336666" Font-Bold="False" Font-Size="Small" 
                      ForeColor="White" HorizontalAlign="Left" />
                  <PagerStyle BackColor="#336666" Font-Size="Medium" ForeColor="White" 
                      HorizontalAlign="Left" />
                  <RowStyle BackColor="White" ForeColor="#333333" />
                  <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                  <SortedAscendingCellStyle BackColor="#CCCCCC" />
                  <SortedAscendingHeaderStyle BackColor="#487575" />
                  <SortedDescendingCellStyle BackColor="#CCCCCC" />
                  <SortedDescendingHeaderStyle BackColor="#275353" />
                  <Columns>
                      <asp:CommandField ButtonType="Button" ControlStyle-BackColor="#84A3A3" 
                          HeaderStyle-HorizontalAlign="Left" ItemStyle-Height="30px" SelectText="Select" 
                          ShowSelectButton="true">
                      <ControlStyle BackColor="#84A3A3" />
                      <HeaderStyle HorizontalAlign="Left" />
                      <ItemStyle Height="30px" />
                      </asp:CommandField>
                      <asp:BoundField DataField="fullname" HeaderStyle-HorizontalAlign="Left" 
                          HeaderText="Name" ItemStyle-Height="35px" SortExpression="fullname">
                      <HeaderStyle HorizontalAlign="Left" />
                      <ItemStyle Height="35px" />
                      </asp:BoundField>
                      <asp:BoundField DataField="entity_name" HeaderStyle-HorizontalAlign="Left" 
                          HeaderText="Entity" ItemStyle-Height="35px" SortExpression="entity_name">
                      <HeaderStyle HorizontalAlign="Left" />
                      <ItemStyle Height="35px" />
                      </asp:BoundField>
                      <asp:BoundField DataField="department_name" HeaderStyle-HorizontalAlign="Left" 
                          HeaderText="Department " ItemStyle-Height="35px" 
                          SortExpression="department_name">
                      <HeaderStyle HorizontalAlign="Left" />
                      <ItemStyle Height="35px" />
                      </asp:BoundField>
                  </Columns>
              </asp:GridView>
              <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="" 
                  EnableViewState="False" SelectCommand="SelActiveManagers" 
                  SelectCommandType="StoredProcedure">
<%--                  <SelectParameters>
                      <asp:ControlParameter ControlID="ddlUnits" DefaultValue="TELE" 
                          Name="LastNameTextBox" PropertyName="SelectedValue" Type="String" />
                      <asp:SessionParameter DefaultValue="" Name="NtLogin" SessionField="loginid" 
                          Type="String" />
                  </SelectParameters>--%>
              </asp:SqlDataSource>
          </td>
    
    </table>
    </ContentTemplate>
 </asp:UpdatePanel>
</asp:Content>

