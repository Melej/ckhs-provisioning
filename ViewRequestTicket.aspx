﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="false" CodeFile="ViewRequestTicket.aspx.vb" Inherits="ViewRequestTicket"  ViewStateMode="Enabled"%>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript" src="Scripts/FieldValidator.js"></script>

<script type="text/javascript">
    function pageLoad(sender, args) {

        fieldValidator('textInputRequired', 'btnUpdate', 'lblValidation');

    }
    function MM_openBrWindow(theURL, winName, features) { //v2.0
        window.open(theURL, winName, features);
    }

</script>

<style type="text/css"> 
 .TestAfterCss:after
{ 
content:url(Images/contact24.png);
position:relative;
display:inline;
margin-left: 5px;
}
    .style1
    {
        width: 223px;
    }
</style>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:UpdatePanel runat="server" UpdateMode ="Conditional" ID="uplTicket">
             <Triggers>

        <%--<asp:AsyncPostBackTrigger ControlID="RequestTyperbl" EventName = "SelectedIndexChanged" />--%>
        <asp:PostBackTrigger ControlID="fileUpload2" />
        <asp:PostBackTrigger ControlID="btnFilComplete" />
        <%--<asp:PostBackTrigger ControlID="btnOpenfile" />
             <asp:AsyncPostBackTrigger ControlID="btnOpenfile" EventName="click" />
             
--%>
        

    </Triggers>
    <ContentTemplate>
     <table width="100%">
        <tr>
         <td>

           <ajaxToolkit:TabContainer ID="tabs" runat="server" ActiveTabIndex="0"
                Width="100%" style="margin-bottom: 0px" EnableTheming="true"  AutoPostBack="true"
                BackColor="#efefef">


                   
              <ajaxToolkit:TabPanel runat="server" ID="TabPanelDemo" BackColor="Gainsboro" EnableTheming="true" TabIndex="0">
                <HeaderTemplate > 
                   <asp:Label ID="Label1" runat="server" Text="Ticket/RFS Info" Font-Bold="True" 
                           ForeColor="Black"></asp:Label>
                </HeaderTemplate>
                 <ContentTemplate>
                  <table  border="1" cellpadding="3" width="100%">
                           <tr>
                                <td  colspan= "4"  class ="tableRowHeader">
                                          <asp:Label ID="requestlbl" runat="server" Text =" Ticket/RFS"></asp:Label>
                                            <asp:Label ID="RequestNumLabel" runat="server"></asp:Label>

                               </td>
                           </tr>
                           <tr>
                              <td  align="center" colspan = "4">
                                <table class="style3" > 
                                  <tr>
                                    <td>
                                        <asp:Button ID="btnReturnClient" runat="server"  Text = "Return to Client "
                                        CssClass="btnhov" BackColor="#006666"  Width="200px"  />
                                        
                                    </td>
                                    <td>
                                        <asp:Button ID="btnoldCSC" runat="server" Text = "Old CSC Edit Ticket CSC" 
                                            CssClass="btnhov" BackColor="#006666"  Width="200px"  Visible="False" />    

                                    
                                    </td>

                                    <td>
                           
                                        <asp:Button runat="server" ID="btnSubmit" Text="Update Ticket/RFS" 
                                        CssClass="btnhov" BackColor="#006666"  Width="200px"  />
                                        
                                    </td>


                                    <td>
                           
                                        <asp:Button runat="server" ID="btnPrintTicket" Text="Print Ticket/RFS" 
                                        CssClass="btnhov" BackColor="#006666"  Width="200px"/>
                                        
                                    </td>
                                   </tr>
                                </table>
                              </td>
                           </tr>
                         <tr align="center">
                            <td colspan="4" align="center">
                                <asp:Label ID = "lblValidation" runat = "server" ForeColor="Red" 
                                    Font-Bold="True"/>
                            </td>
            
                        </tr>

                           <tr>
                            <td colspan = "4">
                                <table class="style3" > 
                                <tr>
                                    <td align="right" >
                                        First Name:
                                    </td>
                                    <td align="left"  >
                                        <asp:TextBox width="280px" ID = "first_nameTextBox" runat="server" ></asp:TextBox>
                                    </td>
                                    <td align="right" >
                                        Last Name:
                                    </td>

                                    <td align="left" >
                                        <asp:TextBox ID = "last_nameTextBox" width="280px" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="right">
                                        Account Name:
                                    </td>
                                    <td colspan="2" align="left">
                                        <asp:Label ID="LoginNamelabel" runat="server" ForeColor="Red"></asp:Label>
                                    
                                    </td>
                                </tr>
                                <tr>
                                   <td align="right" >
                                    Entity:
                                    </td>
                                    <td align="left">
                                    <asp:DropDownList ID ="entitycdDDL" runat="server"  Enabled="False" >
            
                                    </asp:DropDownList>
                                    </td>
       
                                    <td align="right" >
                                        <asp:Label ID="lbldept" runat="server" Text = "Department Name:" />
                                    </td>
                                    <td align="left"  >
                                    <asp:DropDownList ID ="departmentcdddl" runat="server" Width="250px" 
                                            Enabled="False">
            
                                    </asp:DropDownList>
                                    </td>

                                </tr>
                                <tr>
                                    <td  align="right">
                                        Address 1:
                                        </td>
                                    <td align="left"> 
                                            <asp:TextBox ID = "address_1textbox" runat="server" Width="250px"></asp:TextBox>
                                        </td>
                                    <td align="right" >
                                        Address 2:
                                        </td>
                                    <td align="left" >
                                        <asp:TextBox ID = "address_2textbox" runat="server" Width="250px"></asp:TextBox>
                                        </td>

                                </tr>
                            <tr>
                                <td align="right">
                                    <asp:Label id="lblcity" runat="server" width="50px"  text="City:" />
                                </td>
                                <td align="left" colspan="3">
                                    <asp:TextBox ID = "citytextbox" runat="server" width="500px"></asp:TextBox>
                                </td>


                            </tr>

                            <tr>

                                <td align="right">
                                    <asp:Label ID="lblstate" runat="server" Text="State:" Width="50px" />
                                </td>
                                <td align="left">
                                    <asp:TextBox ID = "statetextbox" runat="server" Width="50px"></asp:TextBox>
                                    </td>
                                <td align="right" >

                                    <asp:Label ID="lblzip" runat="server" Text="Zip:" Width="25px" />
                                </td>
                                <td align="left">
                                    <asp:TextBox ID = "ziptextbox" runat="server" Width="50px"></asp:TextBox>
                                </td>
                              </tr>
                              <tr>
                                    <td align="right"  >
                                    Phone Number:
                                    </td>
                                    <td align="left" >
                                    <asp:TextBox ID = "phoneTextBox" runat="server" MaxLength="12"></asp:TextBox>
                                    </td>
                                    <td align="right" >
                                    Email:
                                    </td>
                                    <td align="left" >
                                    <asp:TextBox ID = "emailTextBox" runat="server" Width="250px"></asp:TextBox>
                                    </td>
                               </tr>
                           
                             <tr>
                                <td align="right" >
                                Location:
                                </td>
                                <td align="left" >
         
                                    <asp:DropDownList ID ="facilitycdddl" runat="server" AutoPostBack="True">
              
                                </asp:DropDownList>
          
                                </td>
        
                                <td align="right" >
                                Building:
                                </td>
                                <td align="left" >

                                    <asp:DropDownList ID ="buildingcdddl" runat="server" AutoPostBack="True">
            
                                    </asp:DropDownList>
            
                                </td>
                        </tr>
                        <tr>
                            <td align="right" >
                                Floor:
                            </td>

                            <td align="left">
                                <asp:DropDownList ID="Floorddl" runat="server" AutoPostBack="True">
            
                                </asp:DropDownList>
                            
                            </td>
                            <td align="right" >
                                 Office:
                            </td>
                               
                            <td align="left" > 
                                <asp:TextBox ID="OfficeTextbox" runat="server"></asp:TextBox>
                            </td>


                        </tr>
                      </table>
                    </td>
                   </tr>
                    <tr class="tableRowSubHeader">
                                <td colspan="4">
                                    Acknowledge Status: <asp:label id="acklabel"  runat="server" > Time: </asp:label>
                                                  
                                    <asp:Label ID="acknowledgetimeLabel" text = " " runat="server"></asp:Label>

                                    <asp:Button ID="btnacknoledge" runat="server" text="Acknowledge Ticket" 
                                        Visible="False" BackColor="#ffff38"  Width="200px"  ForeColor="Black" />

                                   
                                </td>
                     </tr>
                           
        <tr >
            <td align="center" colspan="2">
            
                <asp:RadioButtonList ID="RequestTyperbl" runat="server"  Enabled="False" 
                    RepeatDirection="Horizontal">
                        <asp:ListItem Value="ticket" Text="Ticket"></asp:ListItem>
                        <asp:ListItem Value="service" Text="Req. For Service"></asp:ListItem>
                                                           
                </asp:RadioButtonList>          
            
            </td>
            <td align="center" colspan="2">
                Current Status:
                    <asp:Label ID="currstatusDescLabel" runat="server"  Font-Bold="True"></asp:Label>
                   
            </td>
        </tr>                    
                           <tr>
                                <td class="tableCellDisplay" width="200px" align="right" >
                                
                                    <asp:Label ID="p1lbl" runat="server" Text = "Priority"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:RadioButtonList runat="server" ID="priorityrbl" 
                                        RepeatDirection="Horizontal" AutoPostBack="True">
                                                <asp:ListItem>1</asp:ListItem>      
                                                <asp:ListItem>2</asp:ListItem>      
                                                <asp:ListItem>3</asp:ListItem>      
                                                </asp:RadioButtonList>

                                    <asp:Button ID="btnSendTechP1" runat="server" Text= "Send Tech P1 eMail" 
                                        BackColor="#CCCCCC"  Width="200px"  ForeColor="Black" Visible="False" />

                                </td>
                                <td width="175px" align="right">
                                     <asp:Label ID="statusLabel" runat="server" text="Elapsed Time:"></asp:Label>  
                                </td>
                                <td align="left">
                                        <asp:Label ID="opentimelb" runat="server"></asp:Label>

                                        <asp:DropDownList runat="server" ID ="Statusddl" Visible="False"></asp:DropDownList>
                                        
                                  </td>

                            </tr>
                              <tr>
                                <td align="right">
                                    Entered by:
                                </td>
                                <td align="left">
                                    <asp:Label ID="entrytechnamelabel" runat="server" Font-Bold="True"></asp:Label>
                                
                                </td>
                                <td align="right">
                                    Date Entered:
                                </td>

                                <td align="left">
                                    <asp:Label ID="entrydatelabel" runat="server" Font-Bold="True"></asp:Label>
                                </td>
                              </tr>
                              <tr>
                                   <td align="right">
                                            <asp:Label ID="currGroupLb" runat="server" Text="Current Group:" ></asp:Label>
                                   </td>
                                   <td align="left">
                                         <asp:Label runat="server" ID="currentgroupnameLabel" Font-Bold="True" ></asp:Label>
                                         
                                   </td>

                                          <td align="right">
                                           
                                                <asp:Label ID="currTechlb" runat="server" Text = "Current Tech:"></asp:Label>
                                         </td>
                                         <td align="left">
                                            <asp:Label runat="server" ID="currenttechnamelabel" Font-Bold="True"></asp:Label>
                                         
                                          </td>
                                </tr>
                                
                                <tr>

                                       <td align="right">
                                             Change Group
                                        </td>
                                        <td align="left">
                                                <asp:DropDownList runat="server" ID="changegroupddl" AutoPostBack="True"></asp:DropDownList>

                                         </td>

                                          <td align="right">
                                            <asp:Label ID="changteclb" runat="server" Text = "Change Tech:"></asp:Label>
                                           </td>
                                           <td align="left">
                                                <asp:DropDownList runat="server" ID="ChangeTechddl" AutoPostBack="True" ></asp:DropDownList>
                                           </td>

                                  </tr>
                            <tr>
                                <td align="right">
                                    Alternate Contact
                                </td>
                                     <td align="left">
                                         <asp:TextBox runat="server" ID = "alternatecontactnameTextBox" Width="90%"></asp:TextBox>
                                     </td>
                                     <td align="right">
                                        Alternate Phone
                                     </td>
                                     <td align="left">
                                        <asp:TextBox runat="server" ID = "alternatecontactphoneTextBox"></asp:TextBox>
                                        </td>
                            </tr>


                                <tr>

                                    <td align="right">
                                                <asp:Label ID="assettagLbl" runat="server" Text="Asset Tag"></asp:Label>                            
                                    </td>
                                    <td align="left">
                                            <asp:TextBox ID="assettagTextBox" runat="server" Width="270px"></asp:TextBox>                            
                                    </td>                                                        
                                        <td align="right">
                                        <asp:Label ID="iplbl" runat="server" Text="Ip Address"></asp:Label>
                                
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="ipaddressTextBox" runat="server"></asp:TextBox>
                                
                                    </td>
                                </tr>



                                  <tr>
                                    <td  align="center" colspan="4">
                                        <table ID="Table4" runat="server" width="95%">
                                            <tr runat="server">
                                                <td runat="server">
                                                <asp:Label ID="Catlbl" runat="server" Text = "Category"></asp:Label>
                                                </td>    
                                                
                                                <td runat="server">
                                                <asp:DropDownList ID="categorycdddl" runat="server" AutoPostBack="True">
                                                </asp:DropDownList>
                                                </td>    

                                                <td runat="server">
                                                    <asp:Label ID="Typelbl" runat="server" Text = "Type"></asp:Label>
                                                </td>
  
                                                <td runat="server">
                                                <asp:DropDownList ID="typecdddl" runat="server" AutoPostBack="True">
                                                </asp:DropDownList>
                                                </td> 
                                               
                                                <td runat="server">
                                                    <asp:Label ID="Itemlbl" runat="server" Text = "Item"></asp:Label>
                                                </td>    
                                                 
                                                <td runat="server">
                                                <asp:DropDownList ID="itemcdddl" runat="server" AutoPostBack="True">
                                                </asp:DropDownList>
                                                </td>  
                                                                                              
                                            </tr>                                        
                                        </table>
                                    </td>
                                </tr>


                                <tr>
                                    <td align="center" colspan="4">
                                          <asp:Panel Visible="False" runat="server" ID="ticketPnl">
                                                Ticket Info
                                                <table width="100%">
                                                    <tr>
                                                        <td align="right"> 
                                                            <asp:Label id="shorDeslbl" runat="server" Text = "Brief Description" ></asp:Label>
                                
                                                        </td>
                                                         <td colspan="3" align="left">
                                                            <asp:TextBox ID = "shortdescTextbox" runat="server" width="95%"></asp:TextBox> 
                                                         </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right">
                                                            <asp:Label ID="DescLbl" runat="server" Text = "Description"></asp:Label>
                                                        </td>
                                                        <td colspan="3" align="left">
                                                            <asp:TextBox ID="ActivityDescTextbox" runat="server" width="95%"  Height="125px" TextMode="MultiLine"
                                                             MaxLength="1000"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="4">

                                                            <asp:GridView ID="GridOnCall" runat="server" AllowSorting="True" 
                                                                AutoGenerateColumns="False" BackColor="White" BorderColor="#336666" 
                                                                BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                                                                DataKeyNames="group_num,tech_num"
                                                                EmptyDataText="No On Call" 
                                                                EnableTheming="False" Font-Size="Medium" GridLines="Horizontal" Width="100%">
                                                                <FooterStyle BackColor="White" ForeColor="#333333" />
                                                                <HeaderStyle BackColor="#336666" Font-Bold="False" Font-Size="Small" 
                                                                    ForeColor="White" HorizontalAlign="Left" />
                                                                <PagerStyle BackColor="#336666" Font-Size="Medium" ForeColor="White" 
                                                                    HorizontalAlign="Left" />
                                                                <RowStyle BackColor="White" ForeColor="#333333" />
                                                                <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                                                <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                                                <SortedAscendingHeaderStyle BackColor="#487575" />
                                                                <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                                                <SortedDescendingHeaderStyle BackColor="#275353" />
                                                                 <Columns>

                                                                    <asp:CommandField ButtonType="Button" ShowSelectButton= "True" >
                                                                     <ControlStyle BackColor="#84A3A3" />
                                                                     <HeaderStyle HorizontalAlign="Left" />
                                                                     <ItemStyle Height="30px" />
                                                                     </asp:CommandField>
                                                                    <asp:BoundField DataField="group_name" HeaderText="On Call Group Name" />
                                                                    <asp:BoundField DataField="techname" HeaderText="Currently On Call" />
                                                                </Columns>
                                                            </asp:GridView>
                                
                                                        </td>
                            
                                                    </tr>
                                                </table>
                                             </asp:Panel>
                                    </td>
                                </tr>


                                <tr>
                                    <td colspan = "4">
                                        <asp:Panel Visible="False" runat="server" ID="rfsPnl">
                                            <table width="100%" >
                                                 <tr>
                                                    <td colspan="4" align="center"> 
                                                        Completion Date: 
                                                        <asp:TextBox ID="completiondateTextBox" runat="server" CssClass="calenderClass"></asp:TextBox>

                                                    </td>
                                                </tr>
                                                <tr class="tableRowSubHeader">
                                                    <td colspan="4" align="left">
                                                        Description / Explanation                            
                           
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4" align="center">
                                                            <asp:TextBox ID="servicedescTextbox" runat="server" TextMode="MultiLine" 
                                                                Width="95%" Rows ="5"></asp:TextBox>
                           
                                                    </td>

                                                </tr>
                                                <tr class="tableRowSubHeader">
                                                    <td colspan="4" align="left">
                            
                                                        Business Justification / Reason for Request
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4" align="center">
                                                        <asp:TextBox runat="server" ID ="businessdesTextBox" TextMode="MultiLine"  Width="95%" Rows ="5"></asp:TextBox>
                            
                            
                                                    </td>
                                                </tr>

                                                <tr class="tableRowSubHeader">
                                                    <td colspan="4" align="left">
                            
                                                    List Any Known Departments or Systems This Change Will Impact, or any Deadlines That Could Affect This Request

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4" align="center">
                                                            <asp:TextBox runat="server" ID="impactdescTextBox" TextMode="MultiLine"  Width="95%" Rows="4"></asp:TextBox>
                            
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td colspan="4" align="center">
                                                       <asp:Panel ID="TechPanl" runat="server" Visible="False">
                                                        <table border="2px" width="100%" >
                                                           <tr  class= "tableRowSubHeader">
                                                                <td colspan="4">
                                                                    Technical Services
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td>
                                                                    Service Type
                                                                </td>
                                    
                                                                <td>
                                                                     #of Devices
                                                                </td>
                                                                <td>
                                                                    Device IDs
                                                                </td>
                                                                <td>
                                                                     Description
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td>
                                                                     Terminals
                                                                </td>

                                                                 <td>                                        
                                                                     <asp:DropDownList runat="server" ID = "num_of_terminalsddl">
                                                                            <asp:ListItem Value="1" Text="1" />
                                                                            <asp:ListItem Value="2" Text="2" />
                                                                            <asp:ListItem Value="3" Text="3" />
                                                                            <asp:ListItem Value="4" Text="4" />
                                                                            <asp:ListItem Value="5" Text="5" />
                                                                            <asp:ListItem Value="6" Text="6" />
                                                                            <asp:ListItem Value="7" Text="7" />
                                                                            <asp:ListItem Value="8" Text="8" />
                                                                            <asp:ListItem Value="9" Text="9" />
                                                                            <asp:ListItem Value="10" Text="10" />

                                                                        </asp:DropDownList>
                                        
                                                                </td>

                                                                <td>
                                                                   <asp:TextBox ID="terminal_idsTextBox" runat="server" Width="200px"></asp:TextBox>
                                                                
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID ="terminal_info_descTextBox" runat="server" TextMode="MultiLine"
                                                                        Width="300px"></asp:TextBox>
                                                                </td>
                                                            </tr>

                                                             <tr>
                                                                 <td>
                                                                    Printers
                                                                 </td>

                                                                 <td>
                                                                 
                                                                    <asp:DropDownList runat="server" ID = "num_of_printersddl">
                                                                            <asp:ListItem Value="1" Text="1" />
                                                                            <asp:ListItem Value="2" Text="2" />
                                                                            <asp:ListItem Value="3" Text="3" />
                                                                            <asp:ListItem Value="4" Text="4" />
                                                                            <asp:ListItem Value="5" Text="5" />
                                                                            <asp:ListItem Value="6" Text="6" />
                                                                            <asp:ListItem Value="7" Text="7" />
                                                                            <asp:ListItem Value="8" Text="8" />
                                                                            <asp:ListItem Value="9" Text="9" />
                                                                            <asp:ListItem Value="10" Text="10" />

                                                                        </asp:DropDownList>

                                                                 </td>
                                                               <td>
                                                                        <asp:TextBox runat="server" ID="printer_idsTextBox" Width="200px"></asp:TextBox>

                                        
                                                                </td>
                                                                <td>
                                                                        <asp:TextBox runat="server" ID ="printer_info_descTextBox" TextMode="MultiLine"
                                                                        Width="300px"></asp:TextBox>
                                        
                                                                </td>
                                                            </tr>


                                                             <tr>
                                                                 <td>
                                                                    PC
                                                                 </td>

                                                                 <td>
                                                                 
                                                                    <asp:DropDownList runat="server" ID = "num_of_pcddl">
                                                                            <asp:ListItem Value="1" Text="1" />
                                                                            <asp:ListItem Value="2" Text="2" />
                                                                            <asp:ListItem Value="3" Text="3" />
                                                                            <asp:ListItem Value="4" Text="4" />
                                                                            <asp:ListItem Value="5" Text="5" />
                                                                            <asp:ListItem Value="6" Text="6" />
                                                                            <asp:ListItem Value="7" Text="7" />
                                                                            <asp:ListItem Value="8" Text="8" />
                                                                            <asp:ListItem Value="9" Text="9" />
                                                                            <asp:ListItem Value="10" Text="10" />

                                                                        </asp:DropDownList>

                                                                 </td>
                                                               <td>
                                                                        <asp:TextBox runat="server" ID="pc_idsTextBox" Width="200px"></asp:TextBox>

                                        
                                                                </td>
                                                                <td>
                                                                        <asp:TextBox runat="server" ID ="pc_info_descTextBox" TextMode="MultiLine"
                                                                        Width="300px"></asp:TextBox>
                                        
                                                                </td>
                                                            </tr>

                                                             <tr>
                                                                 <td>
                                                                    Network Drops
                                                                 </td>

                                                                 <td>
                                                                 
                                                                    <asp:DropDownList runat="server" ID = "num_of_net_dropsddl">
                                                                            <asp:ListItem Value="1" Text="1" />
                                                                            <asp:ListItem Value="2" Text="2" />
                                                                            <asp:ListItem Value="3" Text="3" />
                                                                            <asp:ListItem Value="4" Text="4" />
                                                                            <asp:ListItem Value="5" Text="5" />
                                                                            <asp:ListItem Value="6" Text="6" />
                                                                            <asp:ListItem Value="7" Text="7" />
                                                                            <asp:ListItem Value="8" Text="8" />
                                                                            <asp:ListItem Value="9" Text="9" />
                                                                            <asp:ListItem Value="10" Text="10" />

                                                                        </asp:DropDownList>

                                                                 </td>
                                                               <td>
                                                                        <asp:TextBox runat="server" ID="net_drop_idsTextBox" Width="200px"></asp:TextBox>

                                        
                                                                </td>
                                                                <td>
                                                                        <asp:TextBox runat="server" ID ="net_drops_info_descTextBox" TextMode="MultiLine" 
                                                                        Width="300px"></asp:TextBox>
                                        
                                                                </td>
                                                            </tr>

                                                             <tr>
                                                                 <td>
                                                                    Other
                                                                 </td>

                                                                 <td>
                                                                 
                                                                    <asp:DropDownList runat="server" ID = "num_of_otherddl">
                                                                            <asp:ListItem Value="1" Text="1" />
                                                                            <asp:ListItem Value="2" Text="2" />
                                                                            <asp:ListItem Value="3" Text="3" />
                                                                            <asp:ListItem Value="4" Text="4" />
                                                                            <asp:ListItem Value="5" Text="5" />
                                                                            <asp:ListItem Value="6" Text="6" />
                                                                            <asp:ListItem Value="7" Text="7" />
                                                                            <asp:ListItem Value="8" Text="8" />
                                                                            <asp:ListItem Value="9" Text="9" />
                                                                            <asp:ListItem Value="10" Text="10" />

                                                                        </asp:DropDownList>

                                                                 </td>
                                                               <td>
                                                                        <asp:TextBox runat="server" ID="other_idsTextBox" Width="200px"></asp:TextBox>

                                        
                                                                </td>
                                                                <td>
                                                                        <asp:TextBox runat="server" ID ="other_info_descTextBox" TextMode="MultiLine"
                                                                        Width="300px"></asp:TextBox>
                                        
                                                                </td>
                                                            </tr>

                                                        </table>
                                                      </asp:Panel>
                                                    </td>
                                                </tr>

                         <tr align="center">
                              <td colspan="4" align="center">
                                    
                                <asp:Panel ID="reportPanel" runat="server" Visible="False">
                                        <table border="2px" width="100%">
                                            <tr class= "tableRowSubHeader">
                                                <td colspan="4">
                                                    Report Data Elements
                                                </td>
                                            </tr>
                                            <tr>
                                            
                                                <td>
                                                        Hospital:
                                                </td>
                                            
                                                <td >
                                                        District Code:
                                                </td>
                                            
                                                <td  class="style1">
                                                    Report Frequency:
                                                
                                                </td>
                                            
                                                <td  class="style1">
                                                    Type of Report:
                                                
                                                </td>

                                            </tr>

                                            <tr>
                                            
                                                <td  valign="top">
                                                
                                                    <asp:CheckBoxList ID="cblFacilities" runat="server" 
                                                            RepeatDirection="Vertical" AutoPostBack="true">
                                                        </asp:CheckBoxList>
                                                </td>

                                                <td valign="top">
                                                    <asp:CheckBoxList ID="cblDistrictCodes" runat="server" RepeatColumns="0"
                                                        AutoPostBack="true">
                                                     </asp:CheckBoxList>
                                                
                                                </td>

                                                <td valign="top">
                                                    <asp:DropDownList runat="server" ID = "ReportFreqddl" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                    
                                                    <asp:TextBox ID="ReportFreqTextBox" runat="server" Visible="false">
                                                    </asp:TextBox>

                                                    <asp:TextBox ID="ReportFreqDescTextBox" runat="server" Visible="false">
                                                    </asp:TextBox>

                                                </td>
                                                <td valign="top">
                                                
                                                     <asp:DropDownList runat="server" ID = "ReportTypeddl" AutoPostBack="true">
                                                        <asp:ListItem Value="0" Text="Select" />
                                                        <asp:ListItem Value="statistical" Text="Statistical" />
                                                        <asp:ListItem Value="detail" Text="Detail" />
                                                        <asp:ListItem Value="summary" Text="Summary" />
                                                        <asp:ListItem Value="detailsummary" Text="Detail with Summary" />
                                                    </asp:DropDownList>
 
                                                    <asp:TextBox ID="ReportTypeTextBox" runat="server" Visible="false"></asp:TextBox>
                                                    <asp:TextBox ID="ReportTypeDescTextBox" runat="server" Visible="false"></asp:TextBox>
                                            
                                            </tr>
                                            <tr>
                                                <td colspan="2" class="style1">
                                                    Data Fields (i.e. Patient Accct# Hosp. Service)
                                                </td>
                                    
                                                <td colspan="2">
                                                        Date Range (From -> To) 
                                                </td>
                                            </tr>


                                            <tr align="center">
                                                                                                                              

                                                <td align="center" colspan="2" class="style1">
                                                    <asp:TextBox ID="DataFieldsTextBox" runat="server" 
                                                    Width="100%" TextMode="MultiLine" Height="80px"></asp:TextBox>
                                                                
                                                </td>
                                                <td align="center" colspan="2">
                                                    <asp:TextBox ID ="DateRangeTextBox" runat="server" TextMode="MultiLine" 
                                                    Width="100%" Height="80px"></asp:TextBox>
                                                </td>
                                                </tr>

                                            <tr>

                                                <td colspan="2" class="style1">
                                                    Total on Field(s)
                                                </td>
                                                <td colspan="2">
                                                        Description 
                                                </td>
                                            </tr>


                                                <tr align = "center">

                                                <td align="center" colspan="2" class="style1">
                                                                
                                                    <asp:TextBox ID="TotalOnfieldTextBox" runat="server" Width="100%" 
                                                    TextMode="MultiLine" Height="80px"></asp:TextBox>
                                                                    
                                                </td>
                                                <td align="center" colspan="2">
                                                                
                                                    <asp:TextBox ID="ReportDescTextBox" runat="server" Width="100%" 
                                                    TextMode="MultiLine" Height="80px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    Other Information:
                                                </td>
                                            </tr>
                                            <tr align="center" >
                                                <td align="center" colspan="4">
                                                                                
                                                    <asp:TextBox ID="reportOtherDescTextBox" runat="server" Width="100%" 
                                                    TextMode="MultiLine" Height="80px"></asp:TextBox>
                                                </td>
                                                                            
                                            </tr>
                                                             
                                        </table>
                                    </asp:Panel>

                                </td>
                                
                            </tr>

                                          </table>
                                      </asp:Panel>
                                    </td>

                                </tr>
<%--                                <tr>
                                    <td colspan = "4">
                                        <asp:Panel Visible="False" runat="server" ID="OldReportpan">
                                        
                                          <table width="100%" >
                                                 <tr>
                                                    <td colspan="4" align="center"> 
                                                        Completion Date: 
                                                        <asp:TextBox ID="TextBox1" runat="server" CssClass="calenderClass"></asp:TextBox>

                                                    </td>
                                                </tr>
                                                <tr class="tableRowSubHeader">
                                                    <td colspan="4" align="left">
                                                        Description / Explanation                            
                           
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4" align="center">
                                                            <asp:TextBox ID="TextBox2" runat="server" TextMode="MultiLine" 
                                                                Width="95%" Rows ="5"></asp:TextBox>
                           
                                                    </td>

                                                </tr>
                                                <tr class="tableRowSubHeader">
                                                    <td colspan="4" align="left">
                            
                                                        Business Justification / Reason for Request
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4" align="center">
                                                        <asp:TextBox runat="server" ID ="TextBox3" TextMode="MultiLine"  Width="95%" Rows ="5"></asp:TextBox>
                            
                            
                                                    </td>
                                                </tr>

                                                <tr class="tableRowSubHeader">
                                                    <td colspan="4" align="left">
                            
                                                    List Any Known Departments or Systems This Change Will Impact, or any Deadlines That Could Affect This Request

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4" align="center">
                                                            <asp:TextBox runat="server" ID="TextBox4" TextMode="MultiLine"  Width="95%" Rows="4"></asp:TextBox>
                            
                                                    </td>
                                                </tr>


                                          </table>
                                        
                                        
                                        </asp:Panel>
                                   
                                    </td>
                                </tr>--%>

                                <tr>
                                  <td colspan = "4">
                                        <asp:Label ID="clabel" runat="server" Visible="False"></asp:Label>
                                        <asp:Label ID="clientnumLabel" runat="server" Visible="False"></asp:Label>
                                        <asp:Label ID="entryclientnumLabel" runat="server" Visible="False"></asp:Label>

                                        <asp:Label ID="WhoclientNumLabel" runat="server" Visible="False"></asp:Label>
                        
                                        <asp:Label ID="SecurityLevelLabel" runat="server" Visible="False"></asp:Label>
                        
                                        <asp:Label ID ="department_cdLabel" runat="server" Visible="False"></asp:Label>
                                        <asp:Label ID ="TitleLabel" runat="server" Visible="False"></asp:Label>
                                     
                                        <asp:Label ID = "entity_cdLabel" runat="server" Visible ="False"></asp:Label>
                                        <asp:Label ID = "EntityeMailNameLabel" runat="server" Visible ="False"></asp:Label>


                                        <asp:Label ID = "building_cdLabel" runat="server" Visible ="False"></asp:Label>
                                        <asp:Label ID= "building_nameLabel" runat="server" Visible="False" ></asp:Label>
                                        <asp:Label ID= "buildingEmailnameLabel" runat="server" Visible="False" ></asp:Label>

                                        <asp:Label ID="HDemployeetype" runat="server" Visible="False"></asp:Label> 

                                        <asp:Label ID="suffixlabel" runat="server" Visible="False"></asp:Label>
                                        <asp:Label ID= "AccountRequestTypeLabel" runat="server" Visible="False"></asp:Label>
                                        <asp:Label ID="typecdLabel" runat="server" Visible="False"></asp:Label>
                                        <asp:Label ID="categorycdLabel"  runat="server" Visible="False"></asp:Label>
                                        <asp:Label ID="itemcdLabel" runat="server" visible="False" />
                                        
                                        <asp:Label ID="facility_cdLabel" runat="server" visible="False" />
                                        <asp:Label ID="FacilityCdeMailLabel" runat="server" visible="False" />

                                        <asp:Label ID="floorLabel" runat="server" visible="False" />
                                        <asp:Label ID="floorEmailLabel" runat="server" visible="False" />

                            
                                        <asp:Label ID = "currenttechnumLabel" runat="server" Visible ="False"></asp:Label>
                                        <asp:Label ID = "currentgroupnumLabel" runat="server" Visible ="False"></asp:Label>

                                        <asp:Label ID = "origionalGroupNumLable" runat="server" Visible ="False"></asp:Label>


                                        <asp:Label ID = "WhoTechnumLabel" runat="server" Visible ="False"></asp:Label>
                                        <asp:Label ID = "WhoNtloginLabel" runat="server" Visible ="False"></asp:Label>

                                        <asp:Label ID="RequestTypeLabel" runat="server" Visible="False"></asp:Label>
                                        <asp:Label ID="currstatuscdLabel" runat="server" Visible="False"></asp:Label>

                                        <asp:Label ID="CloseDateLabel" runat="server" Visible="False"></asp:Label>
                                        
                                        <asp:Label ID="numofterminalsLabel"  runat="server" Visible="False"></asp:Label>
                                        <asp:label id="entrytechnumLabel" runat="server" Visible="False"></asp:label>
                                        <asp:Label ID="hdPriorityLb" runat="server" Visible="False"></asp:Label>
                                        <asp:Label ID="esclationlbl" runat="server" Visible="False"></asp:Label>
                                        <asp:Label ID="Sendemail"  runat="server" Visible="False"></asp:Label>

                                        <asp:Label ID="HDSendP1email"  runat="server" Visible="False"></asp:Label>
                                        <asp:TextBox ID="HDAllPurposeDesc" runat="server" Visible="false"></asp:TextBox>


                                     </td>
                                  </tr>
                             </table>
                 </ContentTemplate>
                </ajaxToolkit:TabPanel>

               <ajaxToolkit:TabPanel runat="server" ID="tbcuuActivity" TabIndex="1">
                    <HeaderTemplate>
                        <asp:Label ID="Label4" runat="server" Text="Activity" Font-Bold="true" ForeColor="Black"></asp:Label>
                    </HeaderTemplate>
                  <ContentTemplate>
                        <table border="1" cellpadding="3" width="100%">
                           <tr align="center" >
                                <td class ="tableRowHeader" align="center">
                                         <asp:Label ID="Label8" runat="server" Text =" Ticket/RFS"></asp:Label>
                                           <asp:Label ID="RequestNumLabel2" runat="server"></asp:Label>

                               </td>
                           </tr>
                        </table>

                        <div  style="overflow:auto;" class="subContainer" >
                                        <asp:Table runat="server" ID="tblActivity" Width="100%">
        
                                        </asp:Table>

                        </div>
                   </ContentTemplate>
                   
               </ajaxToolkit:TabPanel>

               <ajaxToolkit:TabPanel runat="server" ID="tbAddActivity" TabIndex="2">
                    <HeaderTemplate>
                        <asp:Label ID="Label5" runat="server" Text="Add Note/Set Expectation/Close Request" Font-Bold="true" ForeColor="Black">
                            </asp:Label>
                    </HeaderTemplate>
                          <ContentTemplate>
                                 <table width="100%" border="1">

                                      <tr align="center" >
                                        <td class ="tableRowHeader" align="center">
                                                    <asp:Label ID="Label9" runat="server" Text =" Ticket/RFS"></asp:Label>
                                                    <asp:Label ID="RequestNumLabel3" runat="server"></asp:Label>

                                        </td>
                                    </tr>
                                    <tr class="tableRowSubHeader">
                                      <td>
                                              Note/
                                      </td>
                                    </tr>
                                    
                                    <tr>
                                      <td>
                                              Select Activity Type 
                                          <asp:RadioButtonList runat="server" ID="RblActivityType" RepeatDirection="Horizontal" AutoPostBack="true">
                                                <asp:ListItem Value="note" Selected="True">Note</asp:ListItem>      
                                                <asp:ListItem Value="openep">Set Expectation</asp:ListItem>      
                                                <asp:ListItem Value="resols">Close Request</asp:ListItem>      

                                                </asp:RadioButtonList>
                                                <asp:TextBox ID="hdActtype" runat="server" Visible="false"></asp:TextBox>
                                      </td>
                                    </tr>
                                    
                                     <tr>
                                    
                                        <td>
                                             <asp:Panel ID="ResolutionPanel" runat="server" Visible="False">
                                            <table>
                                                <tr>
                                                
                                                   <td>
                                           
                                                            Resolution: 
                                                    </td>
                                                    <td>
                                                            <asp:DropDownList runat="server" ID ="ResolutionCodesddl">
                                                            </asp:DropDownList>
                                                            <asp:Label ID="hdresolutionCD" runat="server" Visible="false"></asp:Label>
                                                      </td>

                                                
                                                
                                                </tr>
                                            </table>
                                         </asp:Panel>
                                        </td>
                                           
                                    </tr>

                                    <tr>
                                      <td>
                                         <asp:TextBox runat="server" ID="txtActivity" TextMode="MultiLine" Width="95%" Rows="15"></asp:TextBox>
                                        
                                         <asp:Button runat="server" ID="btnSubmitActivityNote" Text ="Add Note" 
                                         CssClass="btnhov" BackColor="#006666"  Width="179px"/> 
                                      </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="connid" runat="server" Visible="false"></asp:Label>
                                        </td>
                                    </tr>
                                  </table>
                             </ContentTemplate>
                   </ajaxToolkit:TabPanel>
                   
               <ajaxToolkit:TabPanel id="tabsurvey" runat="server"  TabIndex="3" Visible="false">
                    <HeaderTemplate>
                            <asp:Label ID="resid" runat="server" Text="Survey" Font-Bold="true" ForeColor="Black">
                             </asp:Label>
                    </HeaderTemplate>
                       <ContentTemplate>
                          <table width="100%" border="1">
                            <tr align="center" >
                                <td class ="tableRowHeader" align="center">
                                            <asp:Label ID="Label10" runat="server" Text =" Ticket/RFS"></asp:Label>
                                            <asp:Label ID="RequestNumLabel4" runat="server"></asp:Label>

                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="hdSurveyexists" runat="server" Visible="false"></asp:Label>

                                </td>
                              </tr>
                           </table>
                       </ContentTemplate>
                   </ajaxToolkit:TabPanel>


              <ajaxToolkit:TabPanel ID="Fileview" runat="server" TabIndex="4" Visible="false" >
                    <HeaderTemplate>
                        <asp:Label ID="fileid" runat="server" Text="File/Image"  Font-Bold="true" ForeColor="Black">
                        </asp:Label>
                    </HeaderTemplate>
                        <ContentTemplate>
                          <table width="100%" border="1">
                            <tr align="center" >
                                <td class ="tableRowHeader" align="center">
                                            <asp:Label ID="Label11" runat="server" Text =" Ticket/RFS"></asp:Label>
                                            <asp:Label ID="RequestNumLabel5" runat="server"></asp:Label>

                                </td>
                            </tr>

                             <tr>
                                <td  align="center">
                                            <asp:Label ID="messagelbl" Text = "" runat="server"></asp:Label> 
                                          <asp:Label ID="Label2" runat="server"  Visible="false"></asp:Label>
                                
                                </td>

                             </tr>
                            <tr>
                                <td align="center">
                                        <asp:Button ID="btnOpenFile2" runat="server" Text = "View File" 
                                        CssClass="btnhov" BackColor="#006666"  Width="200px" />

                                       <asp:Button ID="btnRemove" runat="server" Text = "Remove File" 
                                        CssClass="btnhov" BackColor="#006666"  Width="200px" />

                                       <asp:Button ID="btnAddFile" runat="server" Text = "Add File" 
                                        CssClass="btnhov" BackColor="#006666"  Width="200px"  Visible="false"/>

                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="filenamelbl" runat="server"  Visible="false"></asp:Label>
                                    <asp:Label ID="Fileextensionlbl" runat="server" Visible="false"></asp:Label>
                                    

<%--                                    <asp:Image ID="fileimg" runat="server"  Visible="true" Width="150px" 
                                        ImageAlign="Middle" Height="200px" ></asp:Image>

                                    <img src="CSCFileUpload/Image1.jpeg" alt="" width="180px" height="59px"/>
--%>                                    <asp:TextBox ID="hdimagetext" runat="server" Visible="false"></asp:TextBox>
                                 </td>
                              </tr>
                                  
<%--                        <tr>
                                        <td style="font-weight: bold; font-size: larger; text-align: center;" align="center">
                                            <asp:label id="Label8" runat="server" text="Upload a file with your Reqest">
                                            </asp:label>
                                         </td>
                                    </tr>

                                    <tr class="tableRowSubHeader">
                                        <td colspan="4" align="left" width="75%">
                                                <asp:Label id="Label9" runat="server" Text = "Copy and Past Contents of Documents or Screen Shots Here" ></asp:Label>

                                        </td>
                                    </tr>
                    
                                        <tr>
                                        <td colspan="4" align="center" width="95%">


                                                    <ajaxToolkit:AsyncFileUpload ID="fileUpload2" runat="server" OnClientUploadComplete="c" />

                                        </td>
                                    </tr>
                                     <tr>
                                        <td align="center" >
                                            <asp:Button ID="Button1" runat="server" BackColor="#00C000" Font-Bold="True" Font-Names="Arial"
                                                Font-Size="Small" ForeColor="Black" Height="25px" Text=" Submit Attachment"   Width="170px" />

                                            <asp:Button ID="Button2" runat="server" BackColor="#00C000" Font-Bold="True" Font-Names="Arial"
                                                Font-Size="Small" ForeColor="Black" Height="25px" Text=" Cancel"   Width="170px" />

                                        </td>
                                    </tr>

                                    <tr>
                                        <td colspan="4">
                                                        <img id="Img2" runat="server"  visible="false" src ="~/FileUpload/Image1.jpg" alt="" />
                                
                                        </td>
                                    </tr>
                                    --%>

                         
                                </table>
                       </ContentTemplate>

                   
                   </ajaxToolkit:TabPanel>


                   <ajaxToolkit:TabPanel ID="ProJPan" runat="server" TabIndex="5" >
                    <HeaderTemplate>
                        <asp:Label ID="Label3" runat="server" Text="Project Connection"  Font-Bold="true" ForeColor="Black">
                        </asp:Label>
                    </HeaderTemplate>
                        <ContentTemplate>
                          <table width="100%" border="1">
                              <tr align="center" >
                                <td class ="tableRowHeader" align="center" colspan="4">
                                            <asp:Label ID="Label12" runat="server" Text =" Ticket/RFS"></asp:Label>
                                            <asp:Label ID="RequestNumLabel6" runat="server"></asp:Label>

                                </td>
                            </tr>

                            <tr>
                                <td colspan="4">
                                    <asp:Label ID="Hdrlabe" runat="server"  Text="Add or Remove This Ticket from a Project" ></asp:Label>
                                 </td>
                              </tr>
                              <tr>
                                    <td colspan="3">
                                        <asp:DropDownList ID = "Projectsddl" runat = "server"  AutoPostBack="true" Width="200px">
                                        </asp:DropDownList>
                                        <asp:Label ID="ProjectNumLabel" runat="server" Visible="false"></asp:Label>
                           
                                  </td>

                              </tr>
                              <tr>
                                    <td colspan="4">
                                    
                                        <asp:Button ID="BtnSubproject" runat="server" Text = "Submit to Project "
                                         CssClass="btnhov" BackColor="#006666"  Width="199px" />

                                    </td>
                              </tr>
                           </table>
                       </ContentTemplate>

                   
                   </ajaxToolkit:TabPanel>


            </ajaxToolkit:TabContainer>

            </td>
          </tr>
          
          <tr>
            <td>
                
               <%--  ======================== UpLoad file  ========================== --%>
       	        <asp:Panel ID="PanFile" runat="server" Height="350px" Width="575px"  BackColor="#FFFFCC" style="display:none" >
                <table  id="Table2" runat="server" style="border: medium groove #000000; height:100%"  align="center" width="100%" >
                    <tr>
                        <td style="font-weight: bold; font-size: larger; text-align: center;" align="center">
                            <asp:label id="Label6" runat="server" text="Upload a file with your Reqest">
                            </asp:label>
                         </td>
                    </tr>

                    <tr class="tableRowSubHeader">
                        <td colspan="4" align="left" width="75%">
                                <asp:Label id="Label7" runat="server" Text = "Copy and Past Contents of Documents or Screen Shots Here" ></asp:Label>

                        </td>
                    </tr>
                    
                        <tr>
                        <td colspan="4" align="center" width="95%">


                                    <ajaxToolkit:AsyncFileUpload ID="fileUpload2" runat="server" />

                        </td>
                    </tr>
                     <tr>
                        <td align="center" >
                            <asp:Button ID="btnFilComplete" runat="server" BackColor="#00C000" Font-Bold="True" Font-Names="Arial"
                                Font-Size="Small" ForeColor="Black" Height="25px" Text=" Submit Attachment"   Width="170px" />

                            <asp:Button ID="btnCancelfile" runat="server" BackColor="#00C000" Font-Bold="True" Font-Names="Arial"
                                Font-Size="Small" ForeColor="Black" Height="25px" Text=" Cancel"   Width="170px" />

                        </td>
                    </tr>

                    <tr>
                        <td colspan="4">
                               <img id="Img1" runat="server"  visible="false" src ="CSCFileUpload/Image1.jpg" alt="" />
                                
                        </td>
                    </tr>

                         
                </table>
                <br />
                </asp:Panel> 

                <ajaxToolkit:ModalPopupExtender ID="ModalFilePop" runat="server" 
                    TargetControlID="HDBtnFile" PopupControlID="PanFile" 
                    BackgroundCssClass="ModalBackground" >
                    </ajaxToolkit:ModalPopupExtender>

                    <asp:Button ID="HDBtnFile" runat="server" style="display:none" />
            

            </td>
        </tr>
        </table>
    </ContentTemplate>

  </asp:UpdatePanel>
</asp:Content>