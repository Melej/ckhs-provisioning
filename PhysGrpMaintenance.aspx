﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="false" CodeFile="PhysGrpMaintenance.aspx.vb" Inherits="PhysGrpMaintenance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <style type="text/css">
        .style2
        {
            text-align: center;
            background-color: #FFFFCC;
            color: #336666;
            font-weight: bold;
            font-size: x-large;
            height: 43px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

<%--   <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="200"  >
  
      <ProgressTemplate>
       <div id="IMGDIV" align="center" valign="middle" runat="server" style="position:absolute;left: 400px;top:150px;visibility:visible;vertical-align:middle;border-style:inset;border-color:black;background-color:White;width:210px;height:75px;">
                     Loading Data ...
            <img src="Images/AjaxProgress.gif" />
            <br />
       </div>
      </ProgressTemplate>

  </asp:UpdateProgress>

--%>   

 <table>
    <tr align="center">
       
        <td align="center">
                                
            <asp:Button ID="btnExcel" runat="server" Text="All Group RPT." 
                CssClass="btnhov" BackColor="#006666"  Width="135px" />

        </td>
<%--        <td align="center">
                <asp:Button ID="btnGroupPhysExcel" runat="server" Text="Groups & Physicians RPT." 
                CssClass="btnhov" BackColor="#006666"  Width="175px" />
        </td>--%>
    </tr>
    <tr>
        <td  align="center">
            <asp:Label ID="lblmessage" runat="server" Visible="false"  Font-Size="Larger" ForeColor="Red"></asp:Label>
        </td>
    </tr>
</table>

   <%--<div id="maindiv" runat="server" align="left">--%>
    <table  style="border-style: groove" align="left" width="100%">
        <tr align="center">
           <td class="tableRowHeader">
                  Physician Group Maintenance
           </td>
        </tr>
        <tr>
            <td>
                <asp:Label  ID="errorlbl" runat="server" Visible="false">
                </asp:Label>
            </td>
        
        </tr>
        <tr>
             
            <td align="center">


            </td>
        </tr>
        <tr>
          <td align="left">
             <ajaxToolkit:TabContainer ID="tabs" runat="server" ActiveTabIndex="0"
                Width="100%"  EnableTheming="False"  AutoPostBack="true"
                BackColor="#efefef">

                  <ajaxToolkit:TabPanel ID="tpGrouplist" runat="server" TabIndex="0"  BackColor="Gainsboro" Visible = "True" EnableTheming="True" >
                    <HeaderTemplate > 
                        <asp:Label ID="Label4" runat="server" Text="Current Phys. Groups" Font-Bold="true" ForeColor="Black"></asp:Label>
                    </HeaderTemplate>
                      <ContentTemplate>

                       <table border="1" width="100%"  style="empty-cells:show" >
                         <tr>
                              <td colspan="4" align="center">
                                You can search by ONLY 1 criteria
          
                              </td>
                        </tr>
                         <tr>
                             <td align="right">
                                Group Name
                            </td>
                            <td>
                             <asp:TextBox ID="SearchGroupNameTextBox" Width="200px" runat = "server"></asp:TextBox>
                            </td>

                             <td align="right">
                                <asp:Label ID="Label5" runat="server" Text="Group Types:" />
                            </td>
                            <td align="left">
                                <asp:DropDownList ID="GroupTypesddl" runat="server" Width="250px" >
                                </asp:DropDownList>
                                <asp:TextBox ID="GroupTypeCdTextbox" runat="server" Visible="false"></asp:TextBox>
                            </td>
                      </tr>
                      <tr>
                        <td align="center" colspan="4">
                                <asp:Label ID="CurrentSelectLable" runat="server" text="All Groups"></asp:Label>
                        </td>
                      
                      </tr>
                      <tr>
                       <td  align="center" colspan="4">
                        <table>
                            <tr>
                                <td align="center">

                                        <asp:Button ID ="btnSearch" runat = "server" Text ="Search" 
                                        CssClass="btnhov" BackColor="#006666"  Width="120px" />
                                        
                                </td>
                                <td>
                                
                                </td>
                                <td align="center" >

                                        <asp:Button ID = "btnReset" runat= "server" Text="Reset" 
                                        CssClass="btnhov" BackColor="#006666"  Width="120px" />
                                         

                                </td>
                                <td>
                                </td>
                                <td align="center">
                                    <asp:Button ID = "btnPrintAll" runat="server" Text = "Export to Excel"  Visible="false"
                                    CssClass="btnhov" BackColor="#006666"  Width="190px" />
                                    
                                
                                
                                </td>
                            </tr>
                        </table>
                      </td>
                     </tr>          
                         <tr>
                           <td colspan="4">
                            <asp:GridView ID="GvPhysGroup" runat="server" AllowSorting="True" 
                           AutoGenerateColumns="False" BackColor="White" BorderColor="#336666" 
                           BorderStyle="Double" BorderWidth="3px" CellPadding="1" 
                           DataKeyNames="Group_num,GroupTypeCd,GroupID,Validation" 
                           EmptyDataText="No Phys Groups" 
                           EnableTheming="False" Font-Size="Medium" GridLines="Horizontal" Width="100%">
                           <FooterStyle BackColor="White" ForeColor="#333333" />
                           <HeaderStyle BackColor="#336666" Font-Bold="False" Font-Size="X-Small" 
                               ForeColor="White" HorizontalAlign="Left" VerticalAlign="Middle" />
                           <PagerStyle BackColor="#336666" Font-Size="Medium" ForeColor="White" 
                               HorizontalAlign="Left" />
                           <RowStyle BackColor="White" ForeColor="#333333" Font-Size="X-Small" />
                           <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                           <SortedAscendingCellStyle BackColor="#F7F7F7" />
                           <SortedAscendingHeaderStyle BackColor="#487575" />
                           <SortedDescendingCellStyle BackColor="#E5E5E5" />
                           <SortedDescendingHeaderStyle BackColor="#275353" />
                           <Columns>

                               <asp:CommandField ButtonType="Button" ControlStyle-BackColor="#84A3A3" 
                                      HeaderStyle-HorizontalAlign="Left" ItemStyle-Height="20px" SelectText="Select" 
                                      ShowSelectButton="true">
                                  <ControlStyle BackColor="#84A3A3" />
                                  <HeaderStyle HorizontalAlign="Left" />
                                  <ItemStyle Height="20px" />
                                  </asp:CommandField>
                            
                            	  


                               <asp:BoundField DataField="Group_name"  ItemStyle-HorizontalAlign="Left" HeaderText="Group Name" />

                               <asp:BoundField DataField="GroupTypeCd" ItemStyle-HorizontalAlign="Center" HeaderText="Group Type" />
                               
                               <asp:BoundField DataField="GroupCount" ItemStyle-HorizontalAlign="Center" HeaderText="#Phys. in Group" />

                               <asp:BoundField DataField="GroupID" ItemStyle-HorizontalAlign="Center" HeaderText="Group ID" />


                               <asp:BoundField DataField="DuplicateInvisionGroupNum"  ItemStyle-HorizontalAlign="Center" HeaderText="Secondary Invision Group# " />

                              <asp:TemplateField HeaderText = "Validation" SortExpression="Validation" HeaderStyle-HorizontalAlign = "Left" ItemStyle-HorizontalAlign="Left">
                                   <ItemTemplate>
                                          <asp:Label ID="validationlbl" runat="server"><%# If(Eval("Validation"), "")%></asp:Label>                                                
                                    </ItemTemplate>
                                </asp:TemplateField>

                           </Columns>
                       </asp:GridView>                            
                           </td>
                         </tr>
        <tr>
            <td align="center">
            
                <asp:GridView ID="GridHidden" runat="server" 
                     BackColor="#FFFFFF" BorderColor="#FFFFFF" 
                     BorderStyle="Double" BorderWidth="3px" CellPadding="2" 
                      EmptyDataText="No Data Found"  EnableTheming="False" 
                    AllowSorting="True"  EnableModelValidation="True"  Font-Size="Smaller"
                    AutoGenerateColumns="false" EditRowStyle-BorderStyle="Ridge" CellSpacing="2"
                     AlternatingRowStyle-BackColor="#CCCCCC" Font-Bold="True" HeaderStyle-CssClass="subheading" Visible="false">
                      

                    <FooterStyle BackColor="White" ForeColor="#333333" />

                    <HeaderStyle BackColor="#D7D5D5" Font-Bold="False" ForeColor="#060606" Font-Size="Smaller" HorizontalAlign="Left"    />                                    
                    <PagerStyle BackColor="#FFFFFF" ForeColor="#FFFFFF" HorizontalAlign="Left" Font-Size="Medium"/>
                    <RowStyle BackColor="#FFFFFF" ForeColor="#333333" Font-Size="Smaller" />

                         <Columns>




    						<asp:BoundField DataField="Group_num" HeaderText="Group NUmber"></asp:BoundField>
							<asp:BoundField DataField="Group_Name" HeaderText="Group Name"></asp:BoundField>
                             <asp:BoundField DataField="GroupCount" HeaderText="#Phys. in Group" />
							<asp:BoundField DataField="GrouptypeCd"  HeaderText="Group Type "></asp:BoundField>

							<asp:BoundField DataField="GroupID"  HeaderText="Group ID" ></asp:BoundField>
   							<asp:BoundField DataField="DirectEMail"  HeaderText="Direct EMail"></asp:BoundField>


		
                   </Columns> 
                </asp:GridView>
            
            
            </td>
        </tr>

                        </table>
                      </ContentTemplate>
                </ajaxToolkit:TabPanel>

                 <ajaxToolkit:TabPanel ID="tpNewSubmitter" runat="server"  BackColor="Gainsboro" Visible = "True" EnableTheming="True" TabIndex="1" >
                       <HeaderTemplate > 
                          <asp:Label ID="Label3" runat="server" Text="Add New Group" Font-Bold="true" ForeColor="Black" ></asp:Label>
                     </HeaderTemplate>
                      <ContentTemplate>
                       <table border="1" width="100%"  style="empty-cells:show" >
                         <tr>
                              <td colspan="2" align="center">
                               Add Physician Groups
          
                              </td>
                        </tr>

                         <tr>
                       <td  align="center" colspan="4">
                        <table>
                            <tr>
                                <td align="center">

                                        <asp:Button ID ="BtnAddGroup" runat = "server" Text ="Submit " 
                                        CssClass="btnhov" BackColor="#006666"  Enabled="false" Width="120px" />
                                        
                                        
                                </td>
                                <td>
                                
                                </td>
                                <td align="center" >

                                        <asp:Button ID = "BtnAddReset" runat= "server" Text="Reset" 
                                        CssClass="btnhov" BackColor="#006666"  Width="120px" />

                                        

                                </td>
                            </tr>
                        </table>
                      </td>
                     </tr>   
                        <tr>
                            <td align="center" colspan="4">
                                <asp:Label ID="lblAddgroupmsg" runat="server" ForeColor="Red" Font-Size="15px" Font-Bold="True"></asp:Label>
                            
                            </td>
                        </tr>
     
                           <tr>
                            <td>
                                <asp:Label ID="groupType" runat="server" Text="Group Type:">
                                </asp:Label>    
                            
                            </td>
                            <td colspan="3">
                                   
                                <asp:DropDownList ID="GroupTypelist2" runat="server" Width="250px"  AutoPostBack="true">
                                </asp:DropDownList>

                            </td>
                         </tr>

                         <tr>
                            <td colspan="4">
                                <asp:Panel id="PhysOfficepan" runat="server"  Enabled="false">
                                    <table  width="100%">


                                        <tr>
                                          <td>
                                              <asp:Label ID="GrpNamelbl" runat="server" Text="Group Name">
                                               </asp:Label>
                                          </td>

                                          <td colspan="3">
                                               <asp:TextBox ID="Group_nameTextBox" runat="server" Width="450PX">
                                                </asp:TextBox>
                          
                                          </td>
                                         </tr>
                                         <tr>
                                            <td>
                                                <asp:Label ID="idgrouplbl" runat="server" text="Group ID:"></asp:Label>
                                            </td>
                                            <td colspan="3">
                                                <asp:TextBox ID="GroupIDtextbox" runat="server"></asp:TextBox>
                                            </td>
                                         </tr>

                                        <tr>
                                            <td>
                                                <asp:Label ID="akagrpnamelbl" runat="server" Text="AKA Group Name"></asp:Label>
                                            </td>
                                            <td colspan="3">
                                                <asp:TextBox ID ="AKAGroupNameTextBox" runat="server"  Width="450PX" >
                                                </asp:TextBox>
                            
                                            </td>
                        
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="directlbl" runat="server" Text="Direct eMail:" Visible="false"></asp:Label>
                                            </td>
                                            <td colspan="3">
                                                <asp:TextBox ID="directeMailTextBox" runat="server" Visible="false"></asp:TextBox>
                            
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Caplbl" runat="server" Visible="false" Text="Cap Count:"></asp:Label>
                                            </td>
                                            <td colspan="3">
                                                <asp:TextBox ID="CapCount" runat="server" Visible="false"></asp:TextBox>
                                            </td>
                                         </tr>


                                    </table>
                                </asp:Panel>
                            
                            
                            
                            </td>
                         
                         </tr>



                        </table>
                       </ContentTemplate>
                   </ajaxToolkit:TabPanel>

                  <ajaxToolkit:TabPanel ID="tpCurrentAccounts" runat="server"   BorderStyle="Solid" BorderWidth="1px" Visible = "False">
                      <HeaderTemplate > 
                            <asp:Label ID="lblcurr" runat="server" Text="Current Accounts" Font-Bold="true" ForeColor="Black" ></asp:Label>
                    
                      </HeaderTemplate>
                      <ContentTemplate>
                          <table border="0" width = "100%">
                            <tr>
                                <td colspan = "4">
                                      <asp:Table ID = "tblCurrentAccounts" runat = "server">
                                     </asp:Table>
            
                                </td>
        
                            </tr>
                           </table>
                       </ContentTemplate>
                  </ajaxToolkit:TabPanel>

          </ajaxToolkit:TabContainer>
        </td>
     </tr>
  </table>

<%--  </div> --%>

</asp:Content>

