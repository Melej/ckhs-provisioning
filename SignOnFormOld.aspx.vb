﻿Imports System.IO
Imports System.Data '
Imports System.Data.SqlClient
Imports App_Code
Imports AjaxControlToolkit
Imports C1.Web.UI.Controls.C1Window
Imports System.Net.Mail


Partial Class SignOnForm
    Inherits MyBasePage '   Inherits System.Web.UI.Page


    Dim sActivitytype As String = ""

    Dim FormSignOn As FormSQL
    Dim AccountRequestNum As Integer
    Dim Cred As String
    Dim Provider As String
    Dim Han As String
    Dim dtAdjustedRoles As DataTable
    Dim pageCtrl As New PageControls()
    Dim UserInfo As UserSecurity
    Dim conn As New SetDBConnection()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        FormSignOn = New FormSQL(Session("EmployeeConn"), "SelAccountRequestV3", "InsAccountRequestV3", "", Page)

        UserInfo = New UserSecurity(Session("LoginID"), Session("EmployeeConn"))
        Dim rblAffiliateBinder As New RadioButtonListBinder(emp_type_cdrbl, "SelAffiliations", Session("EmployeeConn"))


        If Not IsPostBack Then

            userSubmittingLabel.Text = UserInfo.UserName
            SubmittingOnBehalfOfNameLabel.Text = UserInfo.UserName
            requestor_client_numLabel.Text = UserInfo.ClientNum

           

            Dim ddlBinder As New DropDownListBinder

            Dim cblBinder As New CheckBoxListBinder(cblApplications, "SelApplicationCodes", conn.EmployeeConn)
            cblBinder.AddDDLCriteria("@AppSystem", "base", SqlDbType.NVarChar)
            cblBinder.BindData("ApplicationNum", "ApplicationDesc")


            Dim cblFacilitesBinder As New CheckBoxListBinder(cblFacilities, "SelFacilityEntity", conn.EmployeeConn)
            cblFacilitesBinder.AddDDLCriteria("@EntityFacility", "Facility", SqlDbType.NVarChar)
            cblFacilitesBinder.BindData("Code", "EntityFacilityDesc")

            Dim cblEntityBinder As New CheckBoxListBinder(cblEntities, "SelFacilityEntity", conn.EmployeeConn)
            cblEntityBinder.AddDDLCriteria("@EntityFacility", "Entity", SqlDbType.NVarChar)
            cblEntityBinder.BindData("Code", "EntityFacilityDesc")




            rblAffiliateBinder.ToolTip = False
            rblAffiliateBinder.BindData("AffiliationDescLower", "AffiliationDesc")


            ddlBinder.BindData(Rolesddl, "SelEmployeeRoles", "rolenum", "role", conn.EmployeeConn)
            ddlBinder.BindData(Titleddl, "SelTitles", "TitleDesc", "TitleDesc", conn.EmployeeConn)
            ddlBinder.BindData(Specialtyddl, "SelSpecialtyCodes", "specialty", "specialty", conn.EmployeeConn)
            ddlBinder.BindData(facility_cdddl, "SelFacilityCodes", "facility_cd", "facility_short_name", conn.EmployeeConn)
            ddlBinder.BindData(practice_numddl, "sel_practices", "practice_num", "parctice_name", conn.EmployeeConn)
            ddlBinder.BindData(entity_cdDDL, "sel_entity_codes", "entity_cd", "hosp_desc", conn.EmployeeConn)
            ddlBinder.BindData(OtherAffDescddl, "SelAlliedHealthPos", "AlliedHealthPosShort", "AlliedHealthPosShort", conn.EmployeeConn)


            department_cdddl.Items.Add("Select Entity")
            building_cdddl.Items.Add("Select Facility")


        End If

        ' ColorCodeAccounts()
        '  rblAffiliateBinder.addToolTips()


    End Sub


   
    Protected Sub facility_cdddl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles facility_cdddl.SelectedIndexChanged
        Dim ddlBinder As DropDownListBinder


        ddlBinder = New DropDownListBinder(building_cdddl, "sel_building_by_facility", conn.EmployeeConn)
        ddlBinder.AddDDLCriteria("@facility_cd", facility_cdddl.SelectedValue, SqlDbType.NVarChar)
        ddlBinder.BindData("building_cd", "building_name")



    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        If emp_type_cdrbl.SelectedIndex = -1 Then

            lblValidation.Text = "Must select an Affiliation"
            lblValidation.Focus()
            Return


        End If


        If Not checkFacilities() Then

            lblValidation.Text = "Must select a Hospital"
            Return
        End If

        If Not checkEntities() Then

            lblValidation.Text = "Must select an Entity"
            Return

        End If

      



        Dim BusLog As New AccountBusinessLogic
        Dim thisdata As New CommandsSqlAndOleDb("SelApplicationCodes", Session("EmployeeConn"))
        Dim AccountCode As String


        thisdata.ExecNonQueryNoReturn()

        BusLog.Accounts = thisdata.GetSqlDataTable

        FormSignOn.AddInsertParm("@submitter_client_num", UserInfo.ClientNum, SqlDbType.NVarChar, 10)

        FormSignOn.AddInsertParm("@account_request_type", "addnew", SqlDbType.NVarChar, 9)

        FormSignOn.UpdateFormReturnReqNum()

        AccountRequestNum = FormSignOn.AccountRequestNum

        For Each cbItem As ListItem In cblApplications.Items
            If cbItem.Selected Then

                If BusLog.isHospSpecific(cbItem.Value) Then
                    For Each cbFacility As ListItem In cblFacilities.Items

                        If cbFacility.Selected Then

                            AccountCode = BusLog.GetApplicationCode(cbItem.Value, cbFacility.Value)

                            insAcctItems(AccountRequestNum, AccountCode, request_descTextBox.Text)

                        End If

                    Next

                ElseIf BusLog.isEntSpecific(cbItem.Value) Then

                    For Each cbEnt As ListItem In cblEntities.Items

                        If cbEnt.Selected Then

                            AccountCode = BusLog.GetApplicationCode(cbItem.Value, cbEnt.Value)

                            insAcctItems(AccountRequestNum, AccountCode, request_descTextBox.Text)

                        End If

                    Next


                Else

                    insAcctItems(AccountRequestNum, cbItem.Value, request_descTextBox.Text)

                End If





            End If

        Next

        If providerrbl.SelectedValue = "Yes" Then

            SendProviderEmail(AccountRequestNum)
        End If




        Response.Redirect("~/RequestSubmited.aspx?RequestNum=" & AccountRequestNum)



    End Sub

    Private Sub insAcctItems(ByVal requestNum As Integer, ByVal appNum As String, ByVal comments As String)



        Dim dt As New DataTable
        Dim thisdata As New CommandsSqlAndOleDb("InsAccountItemsV3", Session("EmployeeConn"))
        thisdata.AddSqlProcParameter("@account_request_num", requestNum, SqlDbType.Int, 20)
        thisdata.AddSqlProcParameter("@ApplicationNum", appNum, SqlDbType.Int, 8)
        thisdata.AddSqlProcParameter("@account_item_desc", comments, SqlDbType.NVarChar, 255)

        If providerrbl.SelectedValue = "Yes" Then

            thisdata.AddSqlProcParameter("@validate", "NeedsDocNum", SqlDbType.NVarChar, 20)




        Else

            thisdata.AddSqlProcParameter("@validate", "NeedsValidation", SqlDbType.NVarChar, 20)

        End If



        thisdata.ExecNonQueryNoReturn()



    End Sub
    Protected Sub Rolesddl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Rolesddl.SelectedIndexChanged
       

        Dim rwRef() As DataRow

        For Each cblItem As ListItem In cblApplications.Items
            cblItem.Selected = False


            rwRef = getApplicationsByRole.Select("ApplicationNum='" & cblItem.Value & "'")
            If rwRef.Count > 0 Then

                cblItem.Selected = True


            End If

        Next

        Dim BusLog As New AccountBusinessLogic
        'BusLog.CheckAccountDependancy(cblApplications)
       


        If BusLog.eCare Then
            pnlTCl.Visible = True
            'Dim reqField As New RequiredField(TCLTextBox, "TCL for eCare")

        Else
            pnlTCl.Visible = False

            pageCtrl.RemoveCssClass(pnlTCl, "textInputRequired")

        End If

    End Sub

    Protected Function getApplicationsByRole() As DataTable
        Dim thisdata As New CommandsSqlAndOleDb("SelApplicationsByRole", Session("EmployeeConn"))
        thisdata.AddSqlProcParameter("@roleNum", Rolesddl.SelectedValue, SqlDbType.Int, 20)

        'thisdata.ExecNonQueryNoReturn()

        Return thisdata.GetSqlDataTable

    End Function

  
    Protected Sub cblApplications_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cblApplications.SelectedIndexChanged

       
        Dim BusLog As New AccountBusinessLogic
        '   BusLog.CheckAccountDependancy(cblApplications)



        If BusLog.eCare Then
            pnlTCl.Visible = True
            'Dim reqField As New RequiredField(TCLTextBox, "TCL for eCare")

        Else
            ' pnlTCl.Visible = False

            pageCtrl.RemoveCssClass(pnlTCl, "textInputRequired")

        End If


    End Sub

    
    Protected Sub btnReturn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReturn.Click
        '  BusinessLogic()

        Response.Redirect("MyRequests.aspx")

    End Sub

   

    Protected Sub emp_type_cdrbl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles emp_type_cdrbl.SelectedIndexChanged


        Dim removeCss As New PageControls(Page)
        removeCss.RemoveCssClass(Page, "textInputRequired")
        OtherAffDescTextBox.Text = ""
        OtherAffDescddl.SelectedIndex = -1

        pnlOtherAffilliation.Visible = False
        pnlAlliedHealth.Visible = False


   

        Select Case emp_type_cdrbl.SelectedValue

            Case "employee"

                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")

                Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                'Dim faxqReq As New RequiredField(faxtextbox, "Fax")
                '  Dim locReq As New RequiredField(facility_cdddl, "Location")

                ' Dim endReq As New RequiredField(end_dateTextBox, "End Date")



             

            Case "resident", "student", "physician", "non staff clinician"

                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")
                Dim titleReq As New RequiredField(Titleddl, "Title")
                Dim specReq As New RequiredField(Specialtyddl, "Specialty")
                Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim faxqReq As New RequiredField(faxtextbox, "Fax")
                'Dim locReq As New RequiredField(facility_cdddl, "Location")

                'Dim endReq As New RequiredField(end_dateTextBox, "End Date")



            

            Case "vendor"

                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")

                Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim vendorRequired As New RequiredField(VendorNameTextBox, "Vendor")


             

            Case "contract"

                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")

                Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")

            Case "allied health"

                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")
                Dim titleReq As New RequiredField(Titleddl, "Title")
                Dim specReq As New RequiredField(Specialtyddl, "Specialty")
                Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim faxqReq As New RequiredField(faxtextbox, "Fax")

                Dim otherAff As New RequiredField(OtherAffDescTextBox, "Other Affiliation Description")

                providerrbl.SelectedIndex = 0

                pnlAlliedHealth.Visible = True

            Case "other"

                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")

                Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")

                Dim otherAff As New RequiredField(OtherAffDescTextBox, "Other Affiliation Description")

                pnlOtherAffilliation.Visible = True



            Case Else


                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")


        End Select


        Dim BusLog As New AccountBusinessLogic
        BusLog.CheckAccountDependancy(cblApplications)



        If BusLog.eCare Then
            pnlTCl.Visible = True
            'Dim reqField As New RequiredField(TCLTextBox, "TCL for eCare")

        Else
            pnlTCl.Visible = False

            'pageCtrl.RemoveCssClass(pnlTCl, "textInputRequired")

        End If



    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click

        Dim thisData As New CommandsSqlAndOleDb("SelSimpleSearch", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@FirstName", FirstNameTextBox.Text, SqlDbType.NVarChar, 20)
        thisData.AddSqlProcParameter("@LastName", LastNameTextBox.Text, SqlDbType.NVarChar, 20)


       



        gvSearch.DataSource = thisData.GetSqlDataTable
        gvSearch.DataBind()

        uplOnBehalfOf.Update()
        mpeOnBehalfOf.Show()

    End Sub

    Protected Sub gvSearch_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvSearch.RowCommand

        If e.CommandName = "Select" Then
            Dim client_num As String = gvSearch.DataKeys(Convert.ToInt32(e.CommandArgument))("client_num").ToString()
            Dim login_name As String = gvSearch.DataKeys(Convert.ToInt32(e.CommandArgument))("login_name").ToString()

            SubmittingOnBehalfOfNameLabel.Text = gvSearch.Rows(Convert.ToInt32(e.CommandArgument)).Cells(1).Text
            requestor_client_numLabel.Text = client_num



            'Dim actDir As New ActiveDir(login_name)
            'SubmittingOnBehalfOfEmailLabel.Text = actDir.UserAdEntry.Properties.Item("mail").Value.ToString()



        End If
    End Sub

    Protected Function checkFacilities() As Boolean
        Dim blnReturn = False

        For Each item As ListItem In cblFacilities.Items
            If item.Selected Then
                blnReturn = True

            End If


        Next


        Return blnReturn

    End Function

    Protected Function checkEntities() As Boolean
        Dim blnReturn As Boolean = False

        For Each item As ListItem In cblEntities.Items
            If item.Selected Then

                blnReturn = True
            End If

        Next


        Return blnReturn
    End Function
  
    Protected Sub btnClearAccounts_Click(sender As Object, e As System.EventArgs) Handles btnClearAccounts.Click
        For Each item As ListItem In cblApplications.Items
            item.Selected = False


        Next


    End Sub


    Public Sub CheckBoxValueSelector(ByRef cbl As CheckBoxList, ByVal val As String)

        Dim BusLog As New AccountBusinessLogic

        For Each item As ListItem In cbl.Items

            If item.Value = val Then

                item.Selected = True

            End If




        Next

    End Sub

    Protected Sub entity_cdDDL_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles entity_cdDDL.SelectedIndexChanged
        Dim ddbDepartmentBinder As New DropDownListBinder(department_cdddl, "sel_department_codes", Session("EmployeeConn"))
        ddbDepartmentBinder.AddDDLCriteria("@entity_cd", entity_cdDDL.SelectedValue, SqlDbType.NVarChar)
        ddbDepartmentBinder.BindData("department_cd", "department_name")


    End Sub
    Protected Sub ColorCodeAccounts()
        Dim BaseApps As New List(Of Application)
        BaseApps = New Applications().BaseApps()

        For Each item As ListItem In cblApplications.Items

            If BaseApps.Find(Function(x) x.ApplicationNum = item.Value).AppSystem = "HospSpec" Then

                item.Attributes.Add("style", "color:red")
            ElseIf BaseApps.Find(Function(x) x.ApplicationNum = item.Value).AppSystem = "EntSpec" Then

                item.Attributes.Add("style", "color:blue")
            End If
        Next

    End Sub

    Protected Sub SendProviderEmail(ByVal AccountRequestNum As Integer)



        '======================
        ' Get current path
        '======================

        Dim path As String
        Dim directory As String = ""
        path = HttpContext.Current.Request.Url.AbsoluteUri
        Dim pathParts() As String = Split(path, "/")
        Dim i As Integer
        Do While i < pathParts.Length() - 1

            directory = directory + pathParts(i) + "/"
            i = i + 1

        Loop

        '======================

        Dim EmailList As New List(Of EmailDistribution)
        EmailList = New EmailDistributionList().EmailList

        Dim sndEmailTo = From e In EmailList
                         Where e.EmailEvent = "AddNew" And e.AppBase = "provider"
                         Select e




        For Each EmailAddress In sndEmailTo



            Dim toEmail As String = EmailAddress.Email





            Dim sSubject As String
            Dim sBody As String


            sSubject = "Account Request for " & first_nameTextBox.Text & " " & last_nameTextBox.Text & " has been added to the Provider Queue."


            sBody = "<!DOCTYPE html>" & _
                                "<html>" & _
                                "<head>" & _
                                "<style>" & _
                                "table" & _
                                "{" & _
                                "border-collapse:collapse;" & _
                                "font-family: 'Helvetica Neue', 'Lucida Grande', 'Segoe UI', Arial, Helvetica, Verdana, sans-serif;" & _
                                "}" & _
                                "table, th, td" & _
                                "{" & _
                                "border: 1px solid black;" & _
                                "padding: 3px" & _
                                "}" & _
                                "</style>" & _
                                "</head>" & _
                                "" & _
                                "<body>" & _
                                "A new account request has been added to the Provider Queue.  Follow the link below to go to Queue Screen." & _
                                 "<br />" & _
                                "<a href=""" & directory & "/ProviderRequest.aspx?requestnum=" & AccountRequestNum & """ ><u><b>Provider: " & first_nameTextBox.Text & " " & last_nameTextBox.Text & "</b></u></a>" & _
                                "</body>" & _
                            "</html>"






            Dim sToAddress As String = EmailAddress.Email


            '  sToAddress = "Patrick.Brennan@crozer.org"


            Dim mailObj As New MailMessage()
            mailObj.From = New MailAddress("CSC@crozer.org")
            mailObj.To.Add(New MailAddress(sToAddress))
            mailObj.IsBodyHtml = True
            mailObj.Subject = sSubject
            mailObj.Body = sBody


            Dim smtpClient As New SmtpClient()
            smtpClient.Host = "Smtp2.Crozer.Org"



            smtpClient.Send(mailObj)




        Next




    End Sub


End Class
