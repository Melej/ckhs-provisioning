﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="false" CodeFile="SimpleSearch.aspx.vb" Inherits="Search"  ViewStateMode="Enabled"%>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">


<asp:UpdatePanel ID="uplSearch" runat ="server" >

<Triggers>

   <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />

</Triggers>

 <ContentTemplate>
<table style="border-style: groove" align="left" width="100%">
<tr >
<td>
  <asp:Panel ID="pansearch" runat="server" Width = "100%" DefaultButton="btnSearch" >
    <table width="100%" border ="1" rules="none">
      <tr>
        <th align="center" colspan="4" class="tableRowHeader">
       
           CKHS OnLine Provisioning Request System

        </th>

      </tr>
      
     <tr>
     
         <td align="right" style="font-weight: bold">
            Last Name
        </td>
        <td>
         <asp:TextBox ID = "LastNameTextBox" runat = "server"></asp:TextBox>
        </td>

          <td align="right" style="font-weight: bold">
           First Name
         
        </td>
        <td>
           <asp:TextBox ID = "FirstNameTextBox" runat = "server"></asp:TextBox>
        </td>
    </tr>
     
    <tr>

        
        <td align="right" style="font-weight: bold">
            <asp:Label ID="activitylbl" runat="server" Text="Active Directory Login"></asp:Label>
        </td>

       <td>
         <asp:TextBox ID = "NtLoginTextBox" runat = "server"></asp:TextBox>
       </td>

        <td align="right" style="font-weight: bold">
            <asp:Label ID="requestlbl" runat="server" Text = "Request Number"></asp:Label>
        </td>

        <td>
         <asp:TextBox ID = "RequestNumberTextBox" runat = "server"></asp:TextBox>
        </td>
      
      </tr>
    <tr>
        <td align="right" style="font-weight: bold">
             <asp:Label ID="emplbl" runat="server" Text="Employee Num."></asp:Label>
        </td>
        <td>
            <asp:TextBox ID = "EmpnumTextBox" runat = "server"></asp:TextBox>
        </td>
         
        <td>

        </td>
        <td>

        </td>
    </tr>
      <tr>
        <td align="right" style="font-weight: bold">
            <asp:Label id="sTypelbl" runat="server" Text = "Search Type"></asp:Label>
            
        </td>
        <td colspan="3">
            <asp:RadioButtonList runat="server" ID="rblSystem" RepeatDirection="Horizontal">
                <asp:ListItem Selected="True" Value="clients">Employees</asp:ListItem>
                <asp:ListItem  Value="requests">Requests</asp:ListItem>
                <asp:ListItem  Value="deactivated">Deactivated</asp:ListItem>
            </asp:RadioButtonList>
        </td>
      </tr>

   <%-- <tr>
        <td colspan = "2">
          Department <asp:DropDownList ID = "Departmentddl" runat = "server"></asp:DropDownList>
        </td>
    </tr>--%>

      <tr>
        <td align="left" colspan="4">
            <asp:Label runat="server" ID = "lblValidation" ForeColor="Blue" Visible = "false">Test tool tip</asp:Label>
        </td>
    </tr>
      <tr>
        <td  align="center" colspan="4">
            <table>
                <tr>
                    <td align="center">

                            <asp:Button ID ="btnSearch" runat = "server" Text ="Search"  Width="120px" 
                                  CssClass="btnhov" />
                    </td>
                    <td align="center" >

                            <asp:Button ID = "btnReset" runat= "server" Text="Reset"  Width="120px"  CssClass="btnhov" />

                    </td>
                </tr>
            </table>
    
        </td>
    </tr>
      <tr>
        <td colspan ="4">
         <asp:GridView ID="gvSearch" runat="server" AllowSorting="True"  AutoGenerateColumns="False" BackColor="White" BorderColor="#336666"
              BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
              EmptyDataText="No Items Found" EnableTheming="False" DataKeyNames="client_num"
              Font-Size="small" GridLines="Horizontal" Width="100%">
                                   
            <FooterStyle BackColor="White" ForeColor="#333333" />
            <HeaderStyle BackColor="#336666" Font-Bold="False" ForeColor="White" Font-Size="Small" HorizontalAlign="Left"    />                                    
            <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Left" Font-Size="Medium"/>
            <RowStyle BackColor="White" ForeColor="#333333" />
            <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F7F7F7" />
            <SortedAscendingHeaderStyle BackColor="#487575" />
            <SortedDescendingCellStyle BackColor="#E5E5E5" />
            <SortedDescendingHeaderStyle BackColor="#275353" />
            <Columns>
                <asp:CommandField ButtonType="Button" SelectText="Select" ShowSelectButton= "true" ControlStyle-BackColor="#84A3A3" ItemStyle-Height="30px" />
                <asp:BoundField  DataField="fullname"  SortExpression="fullname" HeaderText="Full Name" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" ItemStyle-Height="35px" ></asp:BoundField>

                <asp:BoundField  DataField="phone"  SortExpression="phone" HeaderText="Phone" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
                <asp:BoundField  DataField="department_name" SortExpression="department_name" HeaderText="Department" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
				<asp:BoundField  DataField="Emp_type_cd"  SortExpression="Emp_type_cd" HeaderText="Acct Type" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
                
                <asp:TemplateField HeaderText = "Network Login" SortExpression="login_name" HeaderStyle-HorizontalAlign = "Left" ItemStyle-HorizontalAlign="Left">
                   <ItemTemplate>
                          <asp:Label ID="lblLoginName" runat="server"><%# If(Eval("login_name"), "")%></asp:Label>                                                
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
		
        <asp:GridView ID="gvRequests" runat="server" AllowSorting="True" 
             AutoGenerateColumns="False" BackColor="White" BorderColor="#336666"
             BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
             EmptyDataText="No Items Found" EnableTheming="False" DataKeyNames="account_request_num"
             Font-Size="small" GridLines="Horizontal" Width="100%"  >
                                   
            <FooterStyle BackColor="White" ForeColor="#333333" />
            <HeaderStyle BackColor="#336666" Font-Bold="False" ForeColor="White" Font-Size="Small" HorizontalAlign="Left"    />                                    
            <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Left" Font-Size="small"/>
            <RowStyle BackColor="White" ForeColor="#333333" />
            <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F7F7F7" />
            <SortedAscendingHeaderStyle BackColor="#487575" />
            <SortedDescendingCellStyle BackColor="#E5E5E5" />
            <SortedDescendingHeaderStyle BackColor="#275353" />

            <Columns>
                    <asp:CommandField ButtonType="Button" SelectText="Select" ShowSelectButton= "true" ControlStyle-BackColor="#84A3A3" ItemStyle-Height="30px"  />                                                                                 
                    <asp:BoundField  DataField="fullname" SortExpression="fullname" HeaderText="Full Name" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" ItemStyle-Height="35px" ></asp:BoundField>
                    <asp:BoundField  DataField="entered_date"  SortExpression="entered_date" HeaderText="Entered Date" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
                    <asp:BoundField  DataField="RequestorFullName"  SortExpression="RequestorFullName" HeaderText="Requestor" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
				    <asp:BoundField  DataField="SubmitterFullName"  SortExpression="SubmitterFullName" HeaderText="Submitter" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
				    <asp:BoundField  DataField="Emp_type_cd"  SortExpression="Emp_type_cd" HeaderText="Acct Type" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
    
				    <asp:BoundField  DataField="RequestStatus"  SortExpression="RequestStatus" HeaderText="RequestStatus" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
                                  
                    
            </Columns>
         </asp:GridView>

         <asp:GridView ID="gvSecureView" runat="server" AllowSorting="True"  AutoGenerateColumns="False" BackColor="White" BorderColor="#336666"
              BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
              EmptyDataText="No Items Found" EnableTheming="False" DataKeyNames="client_num"
              Font-Size="small" GridLines="Horizontal" Width="100%">
                                   
            <FooterStyle BackColor="White" ForeColor="#333333" />
            <HeaderStyle BackColor="#336666" Font-Bold="False" ForeColor="White" Font-Size="Small" HorizontalAlign="Left"    />                                    
            <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Left" Font-Size="Medium"/>
            <RowStyle BackColor="White" ForeColor="#333333" />
            <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F7F7F7" />
            <SortedAscendingHeaderStyle BackColor="#487575" />
            <SortedDescendingCellStyle BackColor="#E5E5E5" />
            <SortedDescendingHeaderStyle BackColor="#275353" />
            <Columns>
                <asp:CommandField ButtonType="Button" SelectText="Select" ShowSelectButton= "true" ControlStyle-BackColor="#84A3A3" ItemStyle-Height="30px" />
                <asp:BoundField  DataField="fullname"  HeaderText="Full Name" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" ItemStyle-Height="35px" ></asp:BoundField>
    <%--             <asp:BoundField  DataField="login_name"  HeaderText="Login"></asp:BoundField>--%>
                <asp:BoundField  DataField="phone"  HeaderText="Phone" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
                <asp:BoundField  DataField="department_name"  HeaderText="Department" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
				<asp:BoundField  DataField="Emp_type_cd"  HeaderText="Acct Type" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
                
              <asp:TemplateField HeaderText = "Network Login" HeaderStyle-HorizontalAlign = "Left" ItemStyle-HorizontalAlign="Left">
                   <ItemTemplate>
                          <asp:Label ID="lblLoginName" runat="server"><%# If(Eval("login_name"), "")%></asp:Label>                                                
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>

        <asp:GridView ID="GVEmpNum" runat="server" AllowSorting="True"  AutoGenerateColumns="False" BackColor="White" BorderColor="#336666"
              BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
              EmptyDataText="No Items Found" EnableTheming="False" DataKeyNames="client_num"
              Font-Size="small" GridLines="Horizontal" Width="100%">
                                   
            <FooterStyle BackColor="White" ForeColor="#333333" />
            <HeaderStyle BackColor="#336666" Font-Bold="False" ForeColor="White" Font-Size="Small" HorizontalAlign="Left"    />                                    
            <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Left" Font-Size="Medium"/>
            <RowStyle BackColor="White" ForeColor="#333333" />
            <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F7F7F7" />
            <SortedAscendingHeaderStyle BackColor="#487575" />
            <SortedDescendingCellStyle BackColor="#E5E5E5" />
            <SortedDescendingHeaderStyle BackColor="#275353" />
            <Columns>
                <asp:CommandField ButtonType="Button" SelectText="Select" ShowSelectButton= "true" ControlStyle-BackColor="#84A3A3" ItemStyle-Height="30px" />
                <asp:BoundField  DataField="fullname" SortExpression="fullname" HeaderText="Full Name" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" ItemStyle-Height="35px" ></asp:BoundField>
  
                
                <asp:BoundField  DataField="empnum" SortExpression="empnum" HeaderText="empnum" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
                <asp:BoundField  DataField="DepartmentName"  SortExpression="DepartmentName" HeaderText="Department" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
				<asp:BoundField  DataField="PositionCode" SortExpression="PositionCode"  HeaderText="Position CD" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
                <asp:BoundField  DataField="StartDate"   SortExpression="StartDate"  HeaderText="Start Date" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>


            </Columns>
        </asp:GridView>

        </td>
    </tr>
   </table>
  </asp:Panel>
 </td>
</tr>

    
    


</table>

</ContentTemplate>

</asp:UpdatePanel>
           

</asp:Content>

