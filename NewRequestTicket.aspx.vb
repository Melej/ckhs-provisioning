﻿Imports System.IO
Imports System.Data
Imports System.Diagnostics
Imports System.Net
Imports System.Net.Mail
Imports System.Net.Mail.SmtpClient
Imports System.Data.SqlClient
Imports App_Code
Imports AjaxControlToolkit
Imports C1.Web.UI.Controls.C1Window

Partial Class NewRequestTicket
    Inherits MyBasePage 'System.Web.UI.Page
    'Dim c3Requestor As New Client3
    Dim UserInfo As UserSecurity
    Dim ddlBinder As New DropDownListBinder
    'Dim frmHelper As New FormHelper

    Dim FormSignOn As New FormSQL
    Dim DemoForm As New FormSQL

    Dim Mailobj As System.Net.Mail.SmtpClient = New System.Net.Mail.SmtpClient
    Dim Mailmsg As New System.Net.Mail.MailMessage
    Dim MailAddress As System.Net.Mail.MailAddress
    Dim mailAttachment As System.Net.Mail.Attachment

    'RequestTicket
    Dim RequestTickect As RequestTicket
    Dim sServer As String = ""
    Dim savePath As String
    Dim saveImage As String


    Dim bHasError As Boolean


    Dim Client3Contrl As New Client3Controller()
    Dim emaillist As New EmailListOne()
    Dim iClientNum As Integer
    Dim iSecurityLevel As Integer
    Dim irequest_num As Integer
    Dim NewTicketRequest As Integer
    Dim PCName As String = ""
    Dim pcIP As String
    Dim path As String
    Dim path1 As String

    Dim fileName As String
    Dim filetype As String
    Dim fileExtension As String

    'Dim Ticfs As FileStream
    'Dim Ticbr As BinaryReader
    'Dim Ticbites As Byte()


    Private _processInfo As String

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        ' Dim UserInfo = Session("objUserSecurity")
        UserInfo = New UserSecurity(Session("LoginID"), Session("EmployeeConn"))

        If Request.QueryString("request_num") <> "" Then
            irequest_num = Request.QueryString("request_num")
            requestnumlabel.Text = Request.QueryString("request_num")

        End If


        If Request.QueryString("ClientNum") <> "" Then
            iClientNum = Request.QueryString("ClientNum")
            client_numlabel.Text = Request.QueryString("ClientNum")

        ElseIf Request.QueryString("ClientNum") = "" Then
            iClientNum = UserInfo.ClientNum
            client_numlabel.Text = UserInfo.ClientNum

        Else
            Response.Redirect("~/" & "SimlpleSearch.aspx", True)
        End If

        entryclientnumLabel.Text = UserInfo.ClientNum
        ntloginLabel.Text = UserInfo.NtLogin
        iSecurityLevel = UserInfo.SecurityLevelNum

		If iSecurityLevel < 70 Then

			Response.Redirect("~/" & "SimlpleSearch.aspx", True)
		End If

		'sServer = Request.ServerVariables("SERVER_NAME")

		'If sServer = ConfigurationManager.AppSettings.Get("ProdWebServers") Then
		'    ' Production Data and web site
		'    File.Delete("D:\\CSCFileUpload\Image1.jpg")
		'    savePath = ("D:\\CSCFileUpload\")
		'    saveImage = ("D:\\CSCFileUpload\Image1.jpg")


		'ElseIf sServer = ConfigurationManager.AppSettings.Get("TestWebServers") Then
		'    ' Test Site and test data
		'    File.Delete("C:\\CSCFileUpload\Image1.jpg")

		'    savePath = ("C:\\CSCFileUpload\")
		'    saveImage = ("C:\\CSCFileUpload\Image1.jpg")

		'Else
		'    ' else default to test
		'    File.Delete("C:\\CSCFileUpload\Image1.jpg")

		'    savePath = ("C:\\CSCFileUpload\")
		'    saveImage = ("C:\\CSCFileUpload\Image1.jpg")
		'End If

		savePath = Server.MapPath("~\\CSCFileUpload\\")
        saveImage = (savePath & "/image1.jpg")


        Dim dbAccess As New CkhsEmployeeDbAccess(Session("EmployeeConn"))
        Dim ckhsEmployee = dbAccess.GetCkhsEmployee(iClientNum)

        ClientnameHeader.Text = ckhsEmployee.FullName
        firstnameLabel.Text = ckhsEmployee.FirstName
        lastnameLabel.Text = ckhsEmployee.LastName
        loginnamelabel.Text = ckhsEmployee.LoginName
        LastFourTextbox.Text = ckhsEmployee.LastFour

        If UserPositionDescLabel.Text <> "" Then
            TitleLabel.Text = UserPositionDescLabel.Text
        End If

        'FormSignOn = New FormSQL(Session("EmployeeConn"), "SelClientByNumberV4", "UpdClientObjectByNumberV6", "", Page)
        'FormSignOn.AddSelectParm("@client_num", iClientNum, SqlDbType.Int, 6)


        'frmHelper.FillForm(clientInfo, tpDemographics, "Update")

        'NewRequestForm = New FormSQL(Session("CSCConn"), "selTicketRFSByNum", "InsTicketRFS", "", Page)

        DemoForm = New FormSQL(Session("EmployeeConn"), "SelClientByNumberV4", "InsAccountRequestV4", "", Page) ' "SelClientByNumberCSCV5", "InsTicketRFSV8",InsAccountRequestV4
        DemoForm.AddSelectParm("@client_num", iClientNum, SqlDbType.Int, 6)

        RequestTyperbl.BackColor() = Color.Yellow
        RequestTyperbl.BorderColor() = Color.Black
        RequestTyperbl.BorderStyle() = BorderStyle.Solid
        RequestTyperbl.BorderWidth = Unit.Pixel(2)


        If Not IsPostBack Then

            getIPAddress()
            getAsstag()



            DemoForm.FillForm()

            Dim ddlBinder As New DropDownListBinder
            Dim ddlTypeBinder As New DropDownListBinder

            ddlBinder.BindData(entitycdDDL, "selEntityCodesV4", "entity_cd", "hosp_desc", Session("EmployeeConn"))

            If EntityCdLabel.Text = "" Then
                entitycdDDL.SelectedIndex = -1
            Else
                Dim entitycd As String = EntityCdLabel.Text

                entitycd = entitycd.Trim
                entitycdDDL.SelectedValue = entitycd
                EntityNameLabel.Text = entitycdDDL.SelectedValue
                EntityeMailNameLabel.Text = entitycdDDL.SelectedItem.ToString

            End If


            Dim ddbDepartmentBinder As New DropDownListBinder(departmentcdddl, "selDepartmentCodesV4", Session("EmployeeConn"))
            ddbDepartmentBinder.AddDDLCriteria("@entity_cd", entitycdDDL.SelectedValue, SqlDbType.NVarChar)
            ddbDepartmentBinder.BindData("department_cd", "department_name")

            If DepartmentCdLabel.Text = "" Then
                departmentcdddl.SelectedIndex = -1
            ElseIf DepartmentCdLabel.Text <> "" Then
                Try

                    departmentcdddl.SelectedValue = DepartmentCdLabel.Text.Trim
                Catch ex As Exception

                    departmentcdddl.SelectedIndex = -1
                End Try

            End If

            ddlBinder.BindData(facilitycdddl, "SelFacilityCodes", "facility_cd", "facility_short_name", Session("EmployeeConn"))


            If FacilityCdLabel.Text = "" Then
                facilitycdddl.SelectedIndex = -1
            ElseIf FacilityCdLabel.Text <> "" Then
                Try
                    facilitycdddl.SelectedValue = FacilityCdLabel.Text
                    FacilityCdeMailLabel.Text = facilitycdddl.SelectedItem.ToString
                Catch ex As Exception
                    facilitycdddl.SelectedIndex = -1

                End Try
            End If


            If BuildingCdLabel.Text = "" Then
                buildingcdddl.SelectedIndex = -1
            ElseIf BuildingCdLabel.Text <> "" Then

                Try

                    ddlBinder = New DropDownListBinder(buildingcdddl, "SelBuildingByFacility", Session("EmployeeConn"))
                    ddlBinder.AddDDLCriteria("@facility_cd", FacilityCdLabel.Text, SqlDbType.NVarChar)
                    ddlBinder.BindData("building_cd", "building_name")

                    buildingcdddl.SelectedValue = BuildingCdLabel.Text.Trim
                    buildingnameLabel.Text = buildingcdddl.SelectedValue
                    buildingEmailnameLabel.Text = buildingcdddl.SelectedItem.ToString
                Catch ex As Exception

                    buildingcdddl.SelectedIndex = -1

                End Try



            End If


            If BuildingCdLabel.Text <> "" And FacilityCdLabel.Text <> "" And floorLabel.Text <> "" Then

                Try
                    Dim ddlFloorBinder As DropDownListBinder

                    ddlFloorBinder = New DropDownListBinder(Floorddl, "SelFloorsByBuilding", Session("EmployeeConn"))
                    ddlFloorBinder.AddDDLCriteria("@facility_cd", FacilityCdLabel.Text, SqlDbType.NVarChar)
                    ddlFloorBinder.AddDDLCriteria("@building_cd", BuildingCdLabel.Text, SqlDbType.NVarChar)

                    ddlFloorBinder.BindData("floor_no", "floor_no")

                    Floorddl.SelectedValue = floorLabel.Text.TrimEnd
                    floorEmailLabel.Text = Floorddl.SelectedItem.ToString


                Catch ex As Exception
                    Floorddl.SelectedValue = -1
                End Try
            Else
                Floorddl.SelectedValue = -1

            End If


            Dim ddlCurrGroup As New DropDownListBinder(changegroupddl, "SelTechGroup", Session("CSCConn"))
            ddlCurrGroup.AddDDLCriteria("@display", "queue", SqlDbType.NVarChar)
            'ddlCurrGroup.AddDDLCriteria("@securityLevel", Session("SecurityLevelNum"), SqlDbType.Int)
            ddlCurrGroup.AddDDLCriteria("@ntlogin", Session("LoginID"), SqlDbType.NVarChar)


            ddlCurrGroup.BindData("group_num", "group_name")
        End If
        ' end postback
        '' Security 

        If iSecurityLevel > 69 Then
            priorityrbl.Enabled = True


        Else

            priorityrbl.Enabled = False
            btnQuickClose.Visible = False
            btnQuickClose.Enabled = False

        End If


        If Session("environment").ToString.ToLower = "testing" Then
            If iSecurityLevel > 24 Then

                'If Session("LoginID").ToString.ToLower = "melej" Or Session("LoginID").ToString.ToLower = "geip00" Or Session("LoginID").ToString.ToLower = "durr05" Then
                btnSubmitRFS.Enabled = True
                btnSubmitTicket.Enabled = True

            Else
                btnSubmitRFS.Enabled = False
                btnSubmitTicket.Enabled = False

            End If

            'R4
            'If Session("SecurityLevelNum") = 70 Then
            '    changegroupddl.Enabled = False
            '    ChangeTechddl.Enabled = False

            'End If

        Else
            ' production

            If iSecurityLevel > 24 Then
                'If Session("LoginID").ToString.ToLower = "melej" Or Session("LoginID").ToString.ToLower = "geip00" Or Session("LoginID").ToString.ToLower = "durr05" Then
                btnSubmitRFS.Enabled = True
                btnSubmitTicket.Enabled = True

            Else
                btnSubmitRFS.Enabled = False
                btnSubmitTicket.Enabled = False

            End If

            'R4
            'If Session("SecurityLevelNum") = 70 Then
            '    changegroupddl.Enabled = False
            '    ChangeTechddl.Enabled = False

            'End If

        End If

        If RequestTyperbl.SelectedValue = "ticket" Then


        ElseIf RequestTyperbl.SelectedValue = "service" Then

            priorityrbl.Enabled = False

        End If
    End Sub

    Protected Sub RequestTyperbl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles RequestTyperbl.SelectedIndexChanged
        RequestType.Text = RequestTyperbl.SelectedValue

        Panel1.Enabled = True


        categorycdddl.BackColor() = Color.Yellow
        categorycdddl.BorderColor() = Color.Black
        categorycdddl.BorderStyle() = BorderStyle.Solid
        categorycdddl.BorderWidth = Unit.Pixel(2)


        typecdddl.BackColor() = Color.Yellow
        typecdddl.BorderColor() = Color.Black
        typecdddl.BorderStyle() = BorderStyle.Solid
        typecdddl.BorderWidth = Unit.Pixel(2)


        itemcdddl.BackColor() = Color.Yellow
        itemcdddl.BorderColor() = Color.Black
        itemcdddl.BorderStyle() = BorderStyle.Solid
        itemcdddl.BorderWidth = Unit.Pixel(2)

        alternatecontactphoneTextBox.BackColor() = Color.Yellow
        alternatecontactphoneTextBox.BorderColor() = Color.Black
        alternatecontactphoneTextBox.BorderStyle() = BorderStyle.Solid
        alternatecontactphoneTextBox.BorderWidth = Unit.Pixel(2)

        Dim rtnum As String

        rtnum = RequestTyperbl.SelectedIndex

        If iSecurityLevel > 69 Then
            techgroups.Visible = True

        End If


        If RequestTyperbl.SelectedValue = "ticket" Then

            ticketPnl.Visible = True
            rfsPnl.Visible = False
            shortdescTextBox.BackColor() = Color.Yellow
            shortdescTextBox.BorderColor() = Color.Black
            shortdescTextBox.BorderStyle() = BorderStyle.Solid
            shortdescTextBox.BorderWidth = Unit.Pixel(2)

            ddlBinder = New DropDownListBinder(categorycdddl, "SelCategoryCodes", Session("CSCConn"))
            ddlBinder.AddDDLCriteria("@securitylevel", iSecurityLevel, SqlDbType.Int)
            ddlBinder.BindData("Category_cd", "category_desc")



        ElseIf RequestTyperbl.SelectedValue = "service" Then

            ddlBinder = New DropDownListBinder(categorycdddl, "SelCategoryCodes", Session("CSCConn"))
            ddlBinder.AddDDLCriteria("@securitylevel", iSecurityLevel, SqlDbType.Int)
            ddlBinder.AddDDLCriteria("@categoryType", "rfs", SqlDbType.NVarChar)

            ddlBinder.BindData("Category_cd", "category_desc")


            rfsPnl.Visible = True
            'TechPanl.Visible = True
            ticketPnl.Visible = False
            btnSubmitRFS.Visible = True
            btnSubmitTicket.Visible = False
            btnQuickClose.Visible = False
            RequestTyperbl.Enabled = False

            priorityrbl.Enabled = False



            completion_dateTextBox.BackColor() = Color.Yellow
            completion_dateTextBox.BorderColor() = Color.Black
            completion_dateTextBox.BorderStyle() = BorderStyle.Solid
            completion_dateTextBox.BorderWidth = Unit.Pixel(2)

            service_descTextbox.BackColor() = Color.Yellow
            service_descTextbox.BorderColor() = Color.Black
            service_descTextbox.BorderStyle() = BorderStyle.Solid
            service_descTextbox.BorderWidth = Unit.Pixel(2)

            business_desTextBox.BackColor() = Color.Yellow
            business_desTextBox.BorderColor() = Color.Black
            business_desTextBox.BorderStyle() = BorderStyle.Solid
            business_desTextBox.BorderWidth = Unit.Pixel(2)



        End If



    End Sub
    Protected Sub btnSubmitTicket_Click(sender As Object, e As System.EventArgs) Handles btnSubmitTicket.Click

        FormSignOn = New FormSQL(Session("CSCConn"), "selTicketRFSByNumV7", "InsTicketRFSV8", "", Page)
        FormSignOn.AddSelectParm("@request_num", irequest_num, SqlDbType.Int, 6)



        'NewRequestForm.AddInsertParm("@client_num", client_numlabel.text, SqlDbType.Int, 4)
        CheckRequiredFields()
        'getIPAddress()

        If categorycdddl.SelectedIndex = 0 Then
            lblValidation.Text = "Must select a Category"
            lblValidation.ForeColor = Color.Red

            lblValidation.Focus()
            categorycdddl.BackColor() = Color.Red

            Exit Sub


        End If

        If typecdddl.SelectedIndex = 0 Then
            lblValidation.Text = "Must select a Type"
            lblValidation.ForeColor = Color.Red

            lblValidation.Focus()
            typecdddl.BackColor() = Color.Red

            Exit Sub


        End If

        If shortdescTextBox.Text = "" Then

            lblValidation.Text = "Must enter a Short Description"
            lblValidation.ForeColor = Color.Red

            lblValidation.Focus()
            shortdescTextBox.BackColor() = Color.Red

            Exit Sub


        ElseIf shortdescTextBox.Text.Length > 200 Then

			lblValidation.Text = "LENGTH TOO LONG Short Description"
			lblValidation.ForeColor = Color.Red

            lblValidation.Focus()
            shortdescTextBox.BackColor() = Color.Red
            Exit Sub


        End If

        If alternatecontactphoneTextBox.Text = "" Then

            lblValidation.Text = "Must enter a Alternate Phone"
            lblValidation.ForeColor = Color.Red

            lblValidation.Focus()
            alternatecontactphoneTextBox.BackColor() = Color.Red

            Exit Sub

        End If

        'If alternatecontactnameTextBox.Text <> "" Then

        '    If (Regex.IsMatch(alternatecontactnameTextBox.Text, "^[\.\-A-Za-z]") = False) Then

        '        lblValidation.Text = "Alternate name can not have numbers."
        '        lblValidation.ForeColor = Color.Red

        '        alternatecontactnameTextBox.Focus()
        '        alternatecontactnameTextBox.BackColor() = Color.Red
        '        Exit Sub

        '    End If

        'End If

        'If alternatecontactphoneTextBox.Text <> "" Then

        '    If (Regex.IsMatch(alternatecontactphoneTextBox.Text, "^[0-9-.]") = False) Then

        '        lblValidation.Text = "Alternate Phone Number must only contain numbers."
        '        lblValidation.ForeColor = Color.Red

        '        alternatecontactphoneTextBox.Focus()
        '        alternatecontactphoneTextBox.BackColor() = Color.Red
        '        Exit Sub

        '    End If

        'End If

        '' City State zip and phone Fax 
        'If Citytextbox.Text <> "" Then

        '    If (Regex.IsMatch(Citytextbox.Text, "^[\.\-A-Za-z]") = False) Then

        '        lblValidation.Text = "City name can not have numbers."
        '        lblValidation.ForeColor = Color.Red
        '        'Citytextbox.Text = ""
        '        Citytextbox.Focus()

        '        'lblValidation.Focus()
        '        Citytextbox.BackColor() = Color.Red
        '        Exit Sub

        '    End If

        'End If

        'If Statetextbox.Text <> "" Then

        '    If (Regex.IsMatch(Statetextbox.Text, "^[\.\-A-Za-z]") = False) Then

        '        lblValidation.Text = "State name can not have numbers."
        '        lblValidation.ForeColor = Color.Red
        '        'Statetextbox.Text = ""

        '        Statetextbox.Focus()
        '        Statetextbox.BackColor() = Color.Red
        '        Exit Sub

        '    End If

        'End If

        'If PhoneTextBox.Text <> "" Then

        '    If (Regex.IsMatch(PhoneTextBox.Text, "^[0-9-.]") = False) Then

        '        lblValidation.Text = "Phone Number must only contain numbers."
        '        lblValidation.ForeColor = Color.Red
        '        'PhoneTextBox.Text = ""

        '        PhoneTextBox.Focus()
        '        PhoneTextBox.BackColor() = Color.Red
        '        Exit Sub

        '    End If

        'End If


        'If faxtextbox.Text <> "" Then

        '    If (Regex.IsMatch(faxtextbox.Text, "^[0-9-.]") = False) Then

        '        lblValidation.Text = "Fax Number must only contain numbers."
        '        lblValidation.ForeColor = Color.Red
        '        'faxtextbox.Text = ""

        '        faxtextbox.Focus()
        '        faxtextbox.BackColor() = Color.Red
        '        Exit Sub

        '    End If

        'End If


        'If Ziptextbox.Text <> "" Then

        '    If (Regex.IsMatch(Ziptextbox.Text, "^[0-9]") = False) Then

        '        lblValidation.Text = "Zip Code can only contain numbers."
        '        lblValidation.ForeColor = Color.Red
        '        Ziptextbox.Text = ""

        '        Ziptextbox.Focus()
        '        Ziptextbox.BackColor() = Color.Red
        '        Exit Sub

        '    End If

        'End If

        'FormSignOn.AddInsertParm("@clientnum", iClientNum, SqlDbType.Int, 6)
        FormSignOn.UpdateFormReturnReqNum()

        NewTicketRequest = FormSignOn.AccountRequestNum
        requestnumlabel.Text = FormSignOn.AccountRequestNum



        If NewTicketRequest = "-1" Then

        Else

            RequestLbl.Visible = True
            requestnumlabel.Visible = True


        End If

        Dim iPriority As Integer

        'P1 inquiry 
        If priorityrbl.SelectedValue = "1" Then
            'Send p1 email
            iPriority = 1
            SendP1Email(NewTicketRequest, iPriority, "p1")
            SendGroupP1Email(NewTicketRequest)

        End If


        If iSecurityLevel > 69 Then
            'CompletionLbl.Text = "Do you want the client to receive an emails for this request ? <a href=""ViewRequestTicket.aspx?RequestNum=" & requestnumlabel.Text & """ ><u><b> " & requestnumlabel.Text & "</b></u></a>"

            ' update lastfour
            'InsUpdClientLastFour @client_num int,
            '@lastFour nvarchar(4),
            '@siemens_emp_num nvarchar(40) =  null

        Else

            CompletionLbl.Text = "Do you want to receive an email for #request ? <a href=""ViewRequestTicket.aspx?RequestNum=" & requestnumlabel.Text & """ ><u><b> " & requestnumlabel.Text & "</b></u></a>"
        End If


        'check for file to upload
        If LoadFilerlb.SelectedValue = "yes" Then

            ModalFilePop.Show()

        End If


        If EMailTextBox.Text <> "" Then

            If iClientNum <> CInt(entryclientnumLabel.Text) Then

                CompletLbl.Text = "Request has been submitted and the client can view the progress. A new EMail has been sent to the client with a link to view the request and for there records. "


                CompletionLbl.Text = "The client will receive an emails for this request ? <a href=""ViewRequestTicket.aspx?RequestNum=" & requestnumlabel.Text & """ ><u><b> " & requestnumlabel.Text & "</b></u></a>"

                completionrbl.Visible = False


            End If


            ModalCompletionPop.Show()
        Else

            Dim thisData As New CommandsSqlAndOleDb("UpdSendEmailMsg", Session("CSCConn"))
            thisData.AddSqlProcParameter("@requestnum", requestnumlabel.Text, SqlDbType.Int)
            thisData.AddSqlProcParameter("@SendEmail", "no", SqlDbType.NVarChar, 3)
			thisData.AddSqlProcParameter("@client_num", entryclientnumLabel.Text, SqlDbType.NVarChar, 9)
			thisData.AddSqlProcParameter("@priority", iPriority, SqlDbType.NVarChar, 3)

            thisData.ExecNonQueryNoReturn()

            Response.Redirect("./ViewRequestTicket.aspx?RequestNum=" & requestnumlabel.Text, True)


        End If

        If currentgroupnumLabel.Text <> "" Then

            SendEmailtoClientGroupAssig(NewTicketRequest, currentgroupnumLabel.Text)
            SendEmailtoTechsGroupAssign(NewTicketRequest, currentgroupnumLabel.Text, currentgroupnameLabel.Text)

        End If
        If currenttechnumLabel.Text <> "" Then
            If CInt(currenttechnumLabel.Text) > 0 Then
                SendTechEmail(NewTicketRequest, iPriority)
            End If


        End If

        'Response.Redirect("./ViewRequestTicket.aspx?RequestNum=" & requestnumlabel.Text, True)


    End Sub


    Protected Sub SubmitRequestItems()

        'facilities
        For Each cblFactem As ListItem In cblFacilities.Items
            'Dim sCurrFacitem As String = cblFactem.Value


            If cblFactem.Selected = True Then


                Dim SubmitData As New CommandsSqlAndOleDb("InsRequestItems", Session("CSCConn"))
                SubmitData.AddSqlProcParameter("@request_num", requestnumlabel.Text, SqlDbType.Int, 9)
                SubmitData.AddSqlProcParameter("@item_type_cd", "facility", SqlDbType.NVarChar, 12)
                SubmitData.AddSqlProcParameter("@item_detail", cblFactem.Value, SqlDbType.NVarChar, 50)
                SubmitData.AddSqlProcParameter("@item_desc", cblFactem.Text, SqlDbType.NVarChar, 150)

                SubmitData.ExecNonQueryNoReturn()


            End If

        Next

        ' district codes
        For Each cblDistitem As ListItem In cblDistrictCodes.Items
            'Dim sCurrFacitem As String = cblDistitem.Value
            'Dim sCurrFacDesc As String = cblDistitem.Selected

            If cblDistitem.Selected = True Then


                Dim SubmitData As New CommandsSqlAndOleDb("InsRequestItems", Session("CSCConn"))
                SubmitData.AddSqlProcParameter("@request_num", requestnumlabel.Text, SqlDbType.Int, 9)
                SubmitData.AddSqlProcParameter("@item_type_cd", "district", SqlDbType.NVarChar, 12)
                SubmitData.AddSqlProcParameter("@item_detail", cblDistitem.Value, SqlDbType.NVarChar, 50)
                SubmitData.AddSqlProcParameter("@item_desc", cblDistitem.Text, SqlDbType.NVarChar, 150)

                SubmitData.ExecNonQueryNoReturn()


            End If

        Next

        ' frequency 
        'ReportFreqddl

        If ReportFreqTextBox.Text <> "" Then


            Dim SubmitData As New CommandsSqlAndOleDb("InsRequestItems", Session("CSCConn"))
            SubmitData.AddSqlProcParameter("@request_num", requestnumlabel.Text, SqlDbType.Int, 9)
            SubmitData.AddSqlProcParameter("@item_type_cd", "frequency", SqlDbType.NVarChar, 12)
            SubmitData.AddSqlProcParameter("@item_detail", ReportFreqTextBox.Text, SqlDbType.NVarChar, 50)
            SubmitData.AddSqlProcParameter("@item_desc", ReportFreqDescTextBox.Text, SqlDbType.NVarChar, 150)

            SubmitData.ExecNonQueryNoReturn()


        End If


        ' report type 

        If ReportTypeTextBox.Text <> "" Then


            Dim SubmitData As New CommandsSqlAndOleDb("InsRequestItems", Session("CSCConn"))
            SubmitData.AddSqlProcParameter("@request_num", requestnumlabel.Text, SqlDbType.Int, 9)
            SubmitData.AddSqlProcParameter("@item_type_cd", "rptType", SqlDbType.NVarChar, 12)
            SubmitData.AddSqlProcParameter("@item_detail", ReportTypeTextBox.Text, SqlDbType.NVarChar, 50)
            SubmitData.AddSqlProcParameter("@item_desc", ReportTypeDescTextBox.Text, SqlDbType.NVarChar, 150)

            SubmitData.ExecNonQueryNoReturn()


        End If

    End Sub
    Protected Sub btnSubmitRFS_Click(sender As Object, e As System.EventArgs) Handles btnSubmitRFS.Click

        FormSignOn = New FormSQL(Session("CSCConn"), "selTicketRFSByNumV7", "InsTicketRFSV8", "", Page)

        'FormSignOn.AddInsertParm("@submitter_client_num", iUserNum, SqlDbType.Int, 9)
        'FormSignOn.AddInsertParm("@HDActivityDesc", ActivityDescTextbox.Text, SqlDbType.NVarChar, 1000)


        If categorycd.Text = "reports" Then


            FormSignOn.AddInsertParm("@HDterminalinfodesc", DataFieldsTextBox.Text, SqlDbType.NVarChar, 500)
            FormSignOn.AddInsertParm("@HDprinterinfodesc", DateRangeTextBox.Text, SqlDbType.NVarChar, 500)
            FormSignOn.AddInsertParm("@HDpcinfodesc", TotalOnfieldTextBox.Text, SqlDbType.NVarChar, 500)
            FormSignOn.AddInsertParm("@HDnetdropsinfodesc", ReportDescTextBox.Text, SqlDbType.NVarChar, 500)
            FormSignOn.AddInsertParm("@HDotherinfodesc", reportOtherDescTextBox.Text, SqlDbType.NVarChar, 500)


        Else



            FormSignOn.AddInsertParm("@HDterminalinfodesc", terminal_info_descTextBox.Text, SqlDbType.NVarChar, 500)
            FormSignOn.AddInsertParm("@HDprinterinfodesc", printer_info_descTextBox.Text, SqlDbType.NVarChar, 500)

            FormSignOn.AddInsertParm("@HDpcinfodesc", pc_info_descTextBox.Text, SqlDbType.NVarChar, 500)
            FormSignOn.AddInsertParm("@HDnetdropsinfodesc", net_drops_info_descTextBox.Text, SqlDbType.NVarChar, 500)
            FormSignOn.AddInsertParm("@HDotherinfodesc", other_info_descTextBox.Text, SqlDbType.NVarChar, 500)


        End If

        FormSignOn.AddInsertParm("@Oldrequestnum", irequest_num, SqlDbType.Int, 6)
        'FormSignOn.AddInsertParm("@HDActivityDesc", Activity_DescTextbox.Text, SqlDbType.NVarChar, 1000)
        FormSignOn.AddInsertParm("@HDservicedesc", service_descTextbox.Text, SqlDbType.NVarChar, 1000)
        FormSignOn.AddInsertParm("@HDbusinessdes", business_desTextBox.Text, SqlDbType.NVarChar, 500)
        FormSignOn.AddInsertParm("@HDimpactdesc", impact_descTextBox.Text, SqlDbType.NVarChar, 500)

        CheckRequiredFields()
        'getIPAddress()

        If categorycdddl.SelectedIndex = 0 Then
            lblValidation.Text = "Must select a Category"
            lblValidation.ForeColor = Color.Red

            lblValidation.Focus()
            categorycdddl.BackColor() = Color.Red
            Return

        End If

        If typecdddl.SelectedIndex = 0 Then
            lblValidation.Text = "Must select a Type"
            lblValidation.ForeColor = Color.Red

            lblValidation.Focus()
            typecdddl.BackColor() = Color.Red
            Return

        End If

        If service_descTextbox.Text = "" Then
            lblValidation.Text = "Must enter a Service Description"
            lblValidation.ForeColor = Color.Red

            lblValidation.Focus()
            service_descTextbox.BackColor() = Color.Red
            Return

        End If

        If business_desTextBox.Text = "" Then
            lblValidation.Text = "Must enter a Business Description"
            lblValidation.ForeColor = Color.Red

            lblValidation.Focus()
            business_desTextBox.BackColor() = Color.Red
            Return
        End If

        If completion_dateTextBox.Text = "" Then

            lblValidation.Text = "Must enter a Completion Date"
            lblValidation.ForeColor = Color.Red

            lblValidation.Focus()
            completion_dateTextBox.BackColor() = Color.Red
            Return

        End If

        If alternatecontactphoneTextBox.Text = "" Then

            lblValidation.Text = "Must enter a Alternate Phone"
            lblValidation.ForeColor = Color.Red

            lblValidation.Focus()
            alternatecontactphoneTextBox.BackColor() = Color.Red

            Exit Sub

        End If


        If num_of_terminalsddl.SelectedIndex = 0 Then
            num_of_terminalsddl.SelectedValue = Nothing

        End If
        If num_of_printersddl.SelectedIndex = 0 Then
            num_of_printersddl.SelectedValue = Nothing
        End If

        If num_of_pcddl.SelectedIndex = 0 Then
            num_of_pcddl.SelectedValue = Nothing
        End If

        If num_of_net_dropsddl.SelectedIndex = 0 Then
            num_of_net_dropsddl.SelectedValue = Nothing
        End If

        If num_of_otherddl.SelectedIndex = 0 Then
            num_of_otherddl.SelectedValue = Nothing

        End If
        ' FormSignOn.AddInsertParm("@client_num", iClientNum, SqlDbType.Int, 6)


        FormSignOn.UpdateFormReturnReqNum()

        NewTicketRequest = FormSignOn.AccountRequestNum
        requestnumlabel.Text = FormSignOn.AccountRequestNum

        ' now have request num 
        SubmitRequestItems()


        If NewTicketRequest = "-1" Then

        Else
            If LoadFilerlb.SelectedValue = "yes" Then

                ModalFilePop.Show()
            End If


            RequestLbl.Visible = True
            requestnumlabel.Visible = True


        End If

        'SubmitFile()


        'CompletionLbl.Text = "<a href=""ViewRequestTicket.aspx?RequestNum=" & requestnumlabel.Text & """ ><u><b> " & requestnumlabel.Text & "</b></u></a>"



        If EMailTextBox.Text <> "" Then

            If iClientNum <> CInt(entryclientnumLabel.Text) Then

                CompletLbl.Text = "Request has been submitted and the client can view the progress by clicking on the  link below. A new EMail has been sent to the client for there records. "


                CompletionLbl.Text = "The client will receive an emails for this request ? <a href=""ViewRequestTicket.aspx?RequestNum=" & requestnumlabel.Text & """ ><u><b> " & requestnumlabel.Text & "</b></u></a>"

                completionrbl.Visible = False


            End If

            ModalCompletionPop.Show()
        Else

            Dim thisData As New CommandsSqlAndOleDb("UpdSendEmailMsg", Session("CSCConn"))
            thisData.AddSqlProcParameter("@requestnum", requestnumlabel.Text, SqlDbType.Int)
            thisData.AddSqlProcParameter("@SendEmail", "no", SqlDbType.NVarChar, 3)
			thisData.AddSqlProcParameter("@client_num", entryclientnumLabel.Text, SqlDbType.NVarChar, 9)
			thisData.AddSqlProcParameter("@priority", "3", SqlDbType.NVarChar, 3)

            thisData.ExecNonQueryNoReturn()

            Response.Redirect("./ViewRequestTicket.aspx?RequestNum=" & requestnumlabel.Text, True)


        End If

        'SendNewRequestEmail(requestnumlabel.Text)

        'Response.Redirect("./ViewRequestTicket.aspx?RequestNum=" & requestnumlabel.Text, True)


        'If client_numlabel.Text = "" Then

        '    '  txtRecommendation.Text = "Select User"
        '    '//Do something
        'Else

        '    Dim frmHelperRequestEntry As New FormHelper()

        '    frmHelperRequestEntry.InitializeUpdateForm("InsNewRequestEntry", Page)
        '    frmHelperRequestEntry.AddCmdParameter("@SubmitterClientNum", UserInfo.ClientNum)
        '    '  frmHelperRequestEntry.AddCmdParameter("@RequestorClientNum", c3Requestor.ClientNum)

        '    frmHelperRequestEntry.AddCmdParameter("@RequestType", "ticket")
        '    frmHelperRequestEntry.UpdateForm("@NextRequestNum")


        '    Dim frmHelperRequestTicket As New FormHelper()

        '    frmHelperRequestTicket.InitializeUpdateForm("InsUpdRequestTicket", Page)
        '    frmHelperRequestTicket.AddCmdParameter("@RequestNum", frmHelperRequestEntry.OutParamValue)
        '    frmHelperRequestTicket.UpdateForm()



        'End If

    End Sub

    Private Shared Function IsPhoneNumberValid(phoneNumber As String) As Boolean
        Dim result As String = ""
        Dim chars As Char() = phoneNumber.ToCharArray()
        For count = 0 To chars.GetLength(0) - 1
            Dim tempChar As Char = chars(count)
            If [Char].IsDigit(tempChar) Or "()+-., ".Contains(tempChar.ToString()) Then

                result += StripNonAlphaNumeric(tempChar)
            Else
                Return False
            End If

        Next
        Return True
        'Return result.Length = 10 'Length of US phone numbers is 10
    End Function

    Private Shared Function StripNonAlphaNumeric(value As String) As String
        Dim regex = New Regex("[^0-9a-zA-Z]", RegexOptions.None)
        Dim result As String = ""
        If regex.IsMatch(value) Then
            result = regex.Replace(value, "")
        Else
            result = value
        End If

        Return result
    End Function

    Protected Sub btnQuickClose_Click(sender As Object, e As System.EventArgs) Handles btnQuickClose.Click
        FormSignOn = New FormSQL(Session("CSCConn"), "selTicketRFSByNumV7", "InsTicketRFSV8", "", Page)
        FormSignOn.AddSelectParm("@request_num", irequest_num, SqlDbType.Int, 6)

        FormSignOn.AddInsertParm("@quick_ticket", "yes", SqlDbType.NVarChar, 3)

        'NewRequestForm.AddInsertParm("@client_num", client_numlabel.text, SqlDbType.Int, 4)
        CheckRequiredFields()
        'getIPAddress()

        If categorycdddl.SelectedIndex = 0 Then
            lblValidation.Text = "Must select a Category"
            lblValidation.ForeColor = Color.Red

            lblValidation.Focus()
            categorycdddl.BackColor() = Color.Red
            Return

        End If

        If typecdddl.SelectedIndex = 0 Then
            lblValidation.Text = "Must select a Type"
            lblValidation.ForeColor = Color.Red

            lblValidation.Focus()
            typecdddl.BackColor() = Color.Red
            Return

        End If

        If categorycd.Text <> "pwreset" Then

            If shortdescTextBox.Text = "" Then

                lblValidation.Text = "Must enter a Short Description"
                lblValidation.ForeColor = Color.Red

                lblValidation.Focus()
                shortdescTextBox.BackColor() = Color.Red
                Return


            End If
        Else
            shortdescTextBox.Text = "Password Reset"
        End If

        'FormSignOn.AddInsertParm("@clientnum", iClientNum, SqlDbType.Int, 6)

        FormSignOn.UpdateFormReturnReqNum()

        NewTicketRequest = FormSignOn.AccountRequestNum
        requestnumlabel.Text = FormSignOn.AccountRequestNum

        Response.Redirect("./ViewRequestTicket.aspx?RequestNum=" & requestnumlabel.Text, True)



    End Sub
    Protected Sub SendP1Email(ByVal RequestNum As Integer, ByVal sPriority As Integer, ByVal sType As String)
        '======================
        ' Get current path
        '======================
        'If Session("LoginID").ToString.ToLower = "melej" Then


        Dim path As String
        Dim directory As String = ""
        Dim toList As String()

        path = HttpContext.Current.Request.Url.AbsoluteUri
        Dim pathParts() As String = Split(path, "/")
        Dim i As Integer

        Dim EmailList2 As String = ""
        Dim MgrpageList As String = ""
        Dim MgrEmailList As String = ""
        Dim sSubject As String
        Dim sBody As String
        Dim fulleMailList As String

        Do While i < pathParts.Length() - 1

            directory = directory + pathParts(i) + "/"
            i = i + 1

        Loop

        Mailmsg.To.Clear()


        'Dim toEmail As String = EmailAddress.Email


        sSubject = "New P1 Ticket# " & RequestNum.ToString & " has been Opened "


        sBody = shortdescTextBox.Text

        'emaillist.getP1emaillist(Session("CSCConn"))

        emaillist.getTicketemaillist(Session("CSCConn"), RequestNum, "pagers")
        MgrpageList = emaillist.EmailListReturn()

        fulleMailList = " Pagers: " & MgrpageList

        'emaillist.getTicketemaillist(Session("CSCConn"), RequestNum, "email")
        'MgrEmailList = emaillist.EmailListReturn()

        fulleMailList = fulleMailList & " eMail: " & MgrEmailList

        'EmailList = New EmailListOne(Session("CSCConn"), "p1").EmailListReturn
        'fulleMailList = EmailList2

        If Session("environment").ToString.ToLower = "testing" Then

            sSubject = " Unformatted p1 Email " & sSubject
            EmailList2 = "Jeff.Mele@crozer.org"
            sBody = sBody & " here is the full list for this p1 " & fulleMailList
            Mailmsg.To.Add(New System.Net.Mail.MailAddress(EmailList2))

        Else

            'MgrpageList
            'Mailmsg.To.Add(New System.Net.Mail.MailAddress(MgrpageList))

            toList = MgrpageList.Split(";")

            If toList.Length = 1 Then
                Mailmsg.To.Add(New System.Net.Mail.MailAddress(toList(0)))
            Else
            End If

            For i = 0 To toList.Length - 1 ' - 2
                If toList(i).Length > 10 Then ' validity check
                    Mailmsg.To.Add(New System.Net.Mail.MailAddress(toList(i)))
                End If
            Next

            ' Now mgr email
            'toList = MgrEmailList.Split(";")

            'If toList.Length = 1 Then
            '    Mailmsg.To.Add(New System.Net.Mail.MailAddress(toList(0)))
            'Else
            'End If


            'For i = 0 To toList.Length - 1 ' - 2
            '    If toList(i).Length > 10 Then ' validity check
            '        Mailmsg.To.Add(New System.Net.Mail.MailAddress(toList(i)))
            '    End If
            'Next



        End If


        'Dim mailObj As New MailMessage
        Mailmsg.From = New MailAddress("CSCemail@crozer.org")
        Mailmsg.IsBodyHtml = True

        Mailmsg.Subject = sSubject
        Mailmsg.Body = sBody

        Dim smtpClient As New SmtpClient()
        smtpClient.Host = "ah1smtprelay01.altahospitals.com"

        'send email immeadatly
        smtpClient.Send(Mailmsg)

        'Now send an email formated message to managers
        SendMangersFormatedEmail(RequestNum)


        'Insert record into P1Messages then 2 minute process will not send email to accounts
        Dim SendP1Data As New CommandsSqlAndOleDb("InsP1SendEmail", Session("CSCConn"))
        SendP1Data.AddSqlProcParameter("@requestnum", RequestNum, SqlDbType.Int)
        SendP1Data.AddSqlProcParameter("@requesttype", RequestType.Text, SqlDbType.NChar, 8)
        SendP1Data.AddSqlProcParameter("@categorycd", categorycd.Text, SqlDbType.NChar, 12)
        SendP1Data.AddSqlProcParameter("@shortDesc", shortdescTextBox.Text, SqlDbType.NChar, 100)
        SendP1Data.ExecNonQueryNoReturn()


        'Insert into P1MessagesSentTo  show who received message
        Dim thisData As New CommandsSqlAndOleDb("InsSentP1Messages", Session("CSCConn"))
        thisData.AddSqlProcParameter("@requestnum", RequestNum, SqlDbType.Int)
        thisData.AddSqlProcParameter("@ToAddress", EmailList2, SqlDbType.NVarChar, 4000)
        thisData.AddSqlProcParameter("@subject", sSubject, SqlDbType.NVarChar, 200)
        thisData.AddSqlProcParameter("@body", sBody, SqlDbType.NVarChar, 4000)

        thisData.ExecNonQueryNoReturn()

    End Sub

    Protected Sub completionrbl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles completionrbl.SelectedIndexChanged

        Dim sSendEmail As String = ""
        sSendEmail = completionrbl.SelectedValue.ToString()

        If completionrbl.SelectedValue.ToString = "no" Then
            ModalCompletionPop.Hide()

        Else
            SendNewRequestEmail(requestnumlabel.Text)

        End If

        Dim ipriority As Integer
        ipriority = priorityrbl.SelectedValue

        Dim thisData As New CommandsSqlAndOleDb("UpdSendEmailMsg", Session("CSCConn"))
        thisData.AddSqlProcParameter("@requestnum", requestnumlabel.Text, SqlDbType.Int)
        thisData.AddSqlProcParameter("@SendEmail", sSendEmail, SqlDbType.NVarChar, 3)
		thisData.AddSqlProcParameter("@client_num", entryclientnumLabel.Text, SqlDbType.NVarChar, 9)
		thisData.AddSqlProcParameter("@priority", ipriority, SqlDbType.NVarChar, 3)

        thisData.ExecNonQueryNoReturn()

        Response.Redirect("./ViewRequestTicket.aspx?RequestNum=" & requestnumlabel.Text, True)

    End Sub
    Protected Sub btnCompletion_Click(sender As Object, e As System.EventArgs) Handles btnCompletion.Click
        Dim sSendEmail As String = ""
        sSendEmail = completionrbl.SelectedValue.ToString()

        ModalCompletionPop.Hide()

        If Session("environment") = "Production" Then

            If completionrbl.SelectedValue.ToString = "yes" Then
                SendNewRequestEmail(requestnumlabel.Text)

            End If
        End If

        Dim ipriority As Integer
        ipriority = priorityrbl.SelectedValue

        Dim thisData As New CommandsSqlAndOleDb("UpdSendEmailMsg", Session("CSCConn"))
        thisData.AddSqlProcParameter("@requestnum", requestnumlabel.Text, SqlDbType.Int)
        thisData.AddSqlProcParameter("@SendEmail", sSendEmail, SqlDbType.NVarChar, 3)
		thisData.AddSqlProcParameter("@client_num", entryclientnumLabel.Text, SqlDbType.NVarChar, 9)
		thisData.AddSqlProcParameter("@priority", ipriority, SqlDbType.NVarChar, 3)

        thisData.ExecNonQueryNoReturn()

        Response.Redirect("./ViewRequestTicket.aspx?RequestNum=" & requestnumlabel.Text, True)



    End Sub
    Protected Sub SendTechEmail(ByVal RequestNum As Integer, ByVal sPriority As Integer)
        '======================
        ' Get current path
        '======================
        'If Session("LoginID").ToString.ToLower = "melej" Then

        Dim path As String
        Dim Link As String
        Dim directory As String = ""
        Dim EmailList2 As String = ""
        Dim sSubject As String = ""
        Dim sBody As String = ""
        Dim fulleMailList As String
        Dim toList As String()
        Dim i As Integer
        Dim iTechnum As Integer

        iTechnum = CInt(currenttechnumLabel.Text)



        path = HttpContext.Current.Request.Url.AbsoluteUri
        Dim pathParts() As String = Split(path, "/")


        Do While i < pathParts.Length() - 1

            directory = directory + pathParts(i) + "/"
            i = i + 1

        Loop

        Mailmsg.To.Clear()


        
        If Session("environment").ToString.ToLower = "testing" Then
            'toEmail = "Jeff.Mele@crozer.org"
            Link = "http://webdev3/CSCV2/ViewRequestTicket.aspx?RequestNum=" & RequestNum.ToString
        Else
            Link = "http://Casemgmt02/CSCV2/ViewRequestTicket.aspx?RequestNum=" & RequestNum.ToString

        End If

        sSubject = "New Ticket/RFS Assigned to you #" & RequestNum


        sBody = "<!DOCTYPE html>" & _
        "<html>" & _
        "<head>" & _
        "<style>" & _
        "table" & _
        "{" & _
        "border-collapse:collapse;" & _
        "font-family: 'Helvetica Neue', 'Lucida Grande', 'Segoe UI', Arial, Helvetica, Verdana, sans-serif;" & _
        "}" & _
        "table, th, td" & _
        "{" & _
        "border: 1px solid black;" & _
        "padding: 3px" & _
        "}" & _
        "</style>" & _
        "</head>" & _
        "" & _
        "<body>" & _
        "<table cellpadding='5px'>" & _
                "<tr>" & _
                    "<td colspan='4' align='center'  style='font-size:large'>" & _
        "               " & _
                    "<b>Ticket/RFS </b>" & _
        "                 " & _
                     "</td>    " & _
                "</tr>" & _
                "<tr>" & _
                    "<td style='font-weight:bold' colspan='1'>Name:</td>" & _
                    "<td colspan='3'>" & FirstNameTextBox.Text & " " & LastNameTextBox.Text & "</td>" & _
                "</tr>" & _
                "<tr>" & _
                    "<td style='font-weight:bold' colspan='1'>Request#:</td>" & _
                    "<td colspan='3'>" & RequestNum & "</td>" & _
                "</tr>" & _
                "<tr>" & _
                    "<td style='font-weight:bold' colspan='1'>Submitted By:</td>" & _
                    "<td colspan='3'>" & UserInfo.UserName & "</td>" & _
                "</tr>" & _
                   "<tr>" & _
                    "<td style='font-weight:bold' colspan='1'>Requestor Email:</td>" & _
                    "<td colspan='3'>" & EMailTextBox.Text & "</td>" & _
                "</tr>" & _
                "<tr>" & _
                    "<td style='font-weight:bold' colspan='1'>Date Requested:</td>" & _
                    "<td colspan='3'>" & DateTime.Now.ToString("yyyy/MM/dd-hh:mm") & "</td>" & _
                "</tr>" & _
                "<tr>" & _
                    "<td style='font-weight:bold' colspan='4'>Request Information </td>" & _
                "</tr>" & _
        "      " & _
        "     " & _
        "<tr><td colspan='4'><b>Description:</b> " & shortdescTextBox.Text & "</td></tr>" & _
        "<tr><td> Request Type: " & RequestType.Text & "</td><td> Category: " & categorycd.Text & "</td><td> Type:" & typecd.Text & "</td><td> Item: " & itemcd.Text & "</td></tr>" & _
        "<tr><td colspan='2'> Phone: " & PhoneTextBox.Text & "</td><td colspan='2'> Alternate Phone:" & alternatecontactphoneTextBox.Text & "</td></tr>" & _
        "<tr><td colspan='2'> eMail: " & EMailTextBox.Text & "</td><td colspan='2'> IP Address: " & ipaddressTextBox.Text & "</td></tr>" & _
        "<tr><td> Location: " & FacilityCdeMailLabel.Text & "</td><td> Building: " & buildingEmailnameLabel.Text & "</td><td colspan='2'> Floor: " & floorLabel.Text & "</td></tr>" & _
        "<tr><td colspan='2'> Address1: " & Address1Textbox.Text & "</td><td colspan='2'> Address2: " & Address2textbox.Text & "</td></tr>" & _
        "<tr><td colspan='2'> City: " & Citytextbox.Text & "</td><td>State: " & Statetextbox.Text & "</td><td> Office: " & OfficeTextbox.Text & "</td></tr>" & _
        "<tr><td colspan='4'><table>" & _
        "<tr>" & _
            "<td align='center'  style='font-size:large'>" & _
"               " & _
            "<b>View Details by clicking on link below</b>" & _
"                 " & _
             "</td>    " & _
        "</tr>" & _
        "<tr>" & _
            "<td align='center'>" & Link & "</td>" & _
        "</tr>" & _
        "<tr>" & _
             "<td colspan='4' align='center'  style='font-size:large'>" & _
        "               " & _
                    "<b> DO NOT REPLY to This eMail</b>" & _
        "                 " & _
                     "</td>    " & _
                "</tr></table>" & _
         "</td></tr></table>" & _
        "</body>" & _
        "</html>"



        emaillist.getTechemaillist(Session("CSCConn"), "email", iTechnum)
        EmailList2 = emaillist.EmailListReturn()


        'EmailList2 = New EmailListOne(RequestNum, Session("EmployeeConn")).EmailListReturn

        fulleMailList = EmailList2

        'Session("environment") = "production"

        If Session("environment").ToString.ToLower = "testing" Then
            EmailList2 = "Jeff.Mele@crozer.org"
            ' sBody
            Mailmsg.To.Add(New System.Net.Mail.MailAddress(EmailList2))

        Else


            'EmailList2 = "2672944056@vtext.com;6105177353@txt.att.net"
            toList = EmailList2.Split(";")
            If toList.Length = 1 Then

                Mailmsg.To.Add(New System.Net.Mail.MailAddress(toList(0)))


            Else
            End If
            For i = 0 To toList.Length - 1 ' - 2
                If toList(i).Length > 10 Then ' validity check
                    Mailmsg.To.Add(New System.Net.Mail.MailAddress(toList(i)))
                    Console.WriteLine(toList(i))
                End If
            Next

        End If


        'Dim mailObj As New MailMessage

        Mailmsg.From = New MailAddress("CSCemail@crozer.org")
        Mailmsg.IsBodyHtml = True

        Mailmsg.Subject = sSubject
        Mailmsg.Body = sBody

        Dim smtpClient As New SmtpClient()
        smtpClient.Host = "ah1smtprelay01.altahospitals.com"

        Try

            smtpClient.Send(Mailmsg)


            'Console.WriteLine(Mailmsg)

        Catch ex As Exception
            'Console.WriteLine(ex.Message)

        End Try


        'Insert into P1MessagesSentTo  show who received message
        'Dim thisData As New CommandsSqlAndOleDb("InsSentP1Messages", Session("CSCConn"))
        'thisData.AddSqlProcParameter("@requestnum", RequestNum, SqlDbType.Int)
        'thisData.AddSqlProcParameter("@ToAddress", EmailList2, SqlDbType.NVarChar, 2000)
        'thisData.AddSqlProcParameter("@subject", sSubject, SqlDbType.NVarChar, 200)
        'thisData.AddSqlProcParameter("@body", sBody, SqlDbType.NVarChar, 4000)

        'thisData.ExecNonQueryNoReturn()



    End Sub
    
    Protected Sub SendMangersFormatedEmail(ByVal RequestNum As Integer)
        Dim path As String
        Dim directory As String = ""
        Dim toList As String()
        Dim Link As String
        Dim sSubject As String
        Dim sBody As String
        Dim fulleMailList As String
        Dim i As Integer
        Dim EmailListp1 As String
        Dim MgrEmailList As String = ""


        path = HttpContext.Current.Request.Url.AbsoluteUri
        Dim pathParts() As String = Split(path, "/")




        Do While i < pathParts.Length() - 1

            directory = directory + pathParts(i) + "/"
            i = i + 1

        Loop

        Mailmsg.To.Clear()

        If Session("environment").ToString.ToLower = "testing" Then
            'toEmail = "Jeff.Mele@crozer.org"
            Link = "http://webdev3/CSCV2/ViewRequestTicket.aspx?RequestNum=" & RequestNum.ToString
        Else
            Link = "http://Casemgmt02/CSCV2/ViewRequestTicket.aspx?RequestNum=" & RequestNum.ToString

        End If


        sSubject = "New P1 Ticket #" & RequestNum & " has been Submitted  for " & FirstNameTextBox.Text & " " & LastNameTextBox.Text


        sBody = "<!DOCTYPE html>" & _
        "<html>" & _
        "<head>" & _
        "<style>" & _
        "table" & _
        "{" & _
        "border-collapse:collapse;" & _
        "font-family: 'Helvetica Neue', 'Lucida Grande', 'Segoe UI', Arial, Helvetica, Verdana, sans-serif;" & _
        "}" & _
        "table, th, td" & _
        "{" & _
        "border: 1px solid black;" & _
        "padding: 3px" & _
        "}" & _
        "</style>" & _
        "</head>" & _
        "" & _
        "<body>" & _
        "<table cellpadding='5px'>" & _
                "<tr>" & _
                    "<td colspan='4' align='center'  style='font-size:large' color='red'>" & _
        "               " & _
                    "<b>P1 Ticket/RFS </b>" & _
        "                 " & _
                     "</td>    " & _
                "</tr>" & _
                "<tr>" & _
                    "<td style='font-weight:bold' colspan='1'>Name:</td>" & _
                    "<td colspan='3'>" & FirstNameTextBox.Text & " " & LastNameTextBox.Text & "</td>" & _
                "</tr>" & _
                "<tr>" & _
                    "<td style='font-weight:bold' colspan='1'>Request#:</td>" & _
                    "<td colspan='3'>" & RequestNum & "</td>" & _
                "</tr>" & _
                "<tr>" & _
                    "<td style='font-weight:bold' colspan='1'>Submitted By:</td>" & _
                    "<td colspan='3'>" & UserInfo.UserName & "</td>" & _
                "</tr>" & _
                   "<tr>" & _
                    "<td style='font-weight:bold' colspan='1'>Requestor Email:</td>" & _
                    "<td colspan='3'>" & EMailTextBox.Text & "</td>" & _
                "</tr>" & _
                "<tr>" & _
                    "<td style='font-weight:bold' colspan='1'>Date Requested:</td>" & _
                    "<td colspan='3'>" & DateTime.Now.ToString("yyyy/MM/dd-hh:mm") & "</td>" & _
                "</tr>" & _
                "<tr>" & _
                    "<td style='font-weight:bold' colspan='4'>Request Information </td>" & _
                "</tr>" & _
        "      " & _
        "     " & _
        "<tr><td colspan='4'><b>Description:</b> " & shortdescTextBox.Text & "</td></tr>" & _
        "<tr><td> Request Type: " & RequestType.Text & "</td><td> Category: " & categorycd.Text & "</td><td> Type:" & typecd.Text & "</td><td> Item: " & itemcd.Text & "</td></tr>" & _
        "<tr><td colspan='2'> Phone: " & PhoneTextBox.Text & "</td><td colspan='2'> Alternate Phone:" & alternatecontactphoneTextBox.Text & "</td></tr>" & _
        "<tr><td colspan='2'> eMail: " & EMailTextBox.Text & "</td><td colspan='2'> IP Address: " & ipaddressTextBox.Text & "</td></tr>" & _
        "<tr><td> Location: " & FacilityCdeMailLabel.Text & "</td><td> Building: " & buildingEmailnameLabel.Text & "</td><td colspan='2'> Floor: " & floorEmailLabel.Text & "</td></tr>" & _
        "<tr><td colspan='2'> Address1: " & Address1Textbox.Text & "</td><td colspan='2'> Address2: " & Address2textbox.Text & "</td></tr>" & _
        "<tr><td colspan='2'> City: " & Citytextbox.Text & "</td><td>State: " & Statetextbox.Text & "</td><td> Office: " & OfficeTextbox.Text & "</td></tr>" & _
        "<tr><td colspan='4'><table>" & _
        "<tr>" & _
            "<td align='center'  style='font-size:large'>" & _
"               " & _
            "<b>View Details by clicking on link below</b>" & _
"                 " & _
             "</td>    " & _
        "</tr>" & _
        "<tr>" & _
            "<td align='center'>" & Link & "</td>" & _
        "</tr>" & _
        "<tr>" & _
             "<td colspan='4' align='center'  style='font-size:large'>" & _
        "               " & _
                    "<b> DO NOT REPLY to This eMail</b>" & _
        "                 " & _
                     "</td>    " & _
                "</tr></table>" & _
         "</td></tr></table>" & _
        "</body>" & _
        "</html>"




        'EmailList = New EmailListOne(RequestNum, Session("CSCConn")).EmailListReturn

        'fulleMailList = EmailList
        emaillist.getTicketemaillist(Session("CSCConn"), RequestNum, "email")
        MgrEmailList = emaillist.EmailListReturn()

        Try

            EmailListp1 = EMailTextBox.Text & ";"

            fulleMailList = EMailTextBox.Text & ";"

            If Session("environment").ToString.ToLower = "testing" Then
                EmailListp1 = "Jeff.Mele@crozer.org"
                sBody = sBody & MgrEmailList

            Else
                EmailListp1 = MgrEmailList

                'sBody = sBody & fulleMailList

            End If

        Catch ex As Exception
            sSubject = "Ticket/RFS Email Error " & sSubject
            EmailListp1 = "Jeff.Mele@crozer.org"
        End Try

        'Mailmsg.To.Add(New System.Net.Mail.MailAddress(EmailListp1))

        toList = MgrEmailList.Split(";")
        If toList.Length = 1 Then
            Mailmsg.To.Add(New System.Net.Mail.MailAddress(toList(0)))
        Else
        End If
        For i = 0 To toList.Length - 1 ' - 2
            If toList(i).Length > 10 Then ' validity check
                Mailmsg.To.Add(New System.Net.Mail.MailAddress(toList(i)))
            End If
        Next

		'Dim mailObj As New MailMessage
		Mailmsg.From = New MailAddress("ckhsitcsc@crozer.org")

		Mailmsg.IsBodyHtml = True

        Mailmsg.Subject = sSubject
        Mailmsg.Body = sBody

        Dim smtpClient As New SmtpClient()
        smtpClient.Host = "ah1smtprelay01.altahospitals.com"

        smtpClient.Send(Mailmsg)
    End Sub
    Protected Sub SendNewRequestEmail(ByVal RequestNum As Integer)
        '======================
        ' Get current path
        '======================

        Dim path As String
        Dim directory As String = ""
        Dim toList As String()
        Dim Link As String
        'Dim toEmail As String = EmailAddress.Email
        Dim sSubject As String
        Dim sBody As String
        Dim fulleMailList As String
        Dim pathParts() As String = Split(path, "/")
        Dim i As Integer
        Dim EmailList As String


        If FacilityCdeMailLabel.Text = "" Then

            FacilityCdeMailLabel.Text = EntityeMailNameLabel.Text

        End If

        path = HttpContext.Current.Request.Url.AbsoluteUri



        Do While i < pathParts.Length() - 1

            directory = directory + pathParts(i) + "/"
            i = i + 1

        Loop

        Mailmsg.To.Clear()

        If Session("environment").ToString.ToLower = "testing" Then
            'toEmail = "Jeff.Mele@crozer.org"
            Link = "http://webdev3/CSCV2/ViewRequestTicket.aspx?RequestNum=" & RequestNum.ToString
        Else
            Link = "http://Casemgmt02/CSCV2/ViewRequestTicket.aspx?RequestNum=" & RequestNum.ToString

        End If



        sSubject = "New Ticket/RFS #" & RequestNum & " has been Submitted  for " & FirstNameTextBox.Text & " " & LastNameTextBox.Text


        sBody = "<!DOCTYPE html>" & _
        "<html>" & _
        "<head>" & _
        "<style>" & _
        "table" & _
        "{" & _
        "border-collapse:collapse;" & _
        "font-family: 'Helvetica Neue', 'Lucida Grande', 'Segoe UI', Arial, Helvetica, Verdana, sans-serif;" & _
        "}" & _
        "table, th, td" & _
        "{" & _
        "border: 1px solid black;" & _
        "padding: 3px" & _
        "}" & _
        "</style>" & _
        "</head>" & _
        "" & _
        "<body>" & _
        "<table cellpadding='5px'>" & _
                "<tr>" & _
                    "<td colspan='4' align='center'  style='font-size:large'>" & _
        "               " & _
                    "<b>Ticket/RFS </b>" & _
        "                 " & _
                     "</td>    " & _
                "</tr>" & _
                "<tr>" & _
                    "<td style='font-weight:bold' colspan='1'>Name:</td>" & _
                    "<td colspan='3'>" & FirstNameTextBox.Text & " " & LastNameTextBox.Text & "</td>" & _
                "</tr>" & _
                "<tr>" & _
                    "<td style='font-weight:bold' colspan='1'>Request#:</td>" & _
                    "<td colspan='3'>" & RequestNum & "</td>" & _
                "</tr>" & _
                "<tr>" & _
                    "<td style='font-weight:bold' colspan='1'>Submitted By:</td>" & _
                    "<td colspan='3'>" & UserInfo.UserName & "</td>" & _
                "</tr>" & _
                   "<tr>" & _
                    "<td style='font-weight:bold' colspan='1'>Requestor Email:</td>" & _
                    "<td colspan='3'>" & EMailTextBox.Text & "</td>" & _
                "</tr>" & _
                "<tr>" & _
                    "<td style='font-weight:bold' colspan='1'>Date Requested:</td>" & _
                    "<td colspan='3'>" & DateTime.Now.ToString("yyyy/MM/dd-hh:mm") & "</td>" & _
                "</tr>" & _
                "<tr>" & _
                    "<td style='font-weight:bold' colspan='4'>Request Information </td>" & _
                "</tr>" & _
        "      " & _
        "     " & _
        "<tr><td colspan='4'><b>Description:</b> " & shortdescTextBox.Text & "</td></tr>" & _
        "<tr><td> Request Type: " & RequestType.Text & "</td><td> Category: " & categorycd.Text & "</td><td> Type:" & typecd.Text & "</td><td> Item: " & itemcd.Text & "</td></tr>" & _
        "<tr><td colspan='2'> Phone: " & PhoneTextBox.Text & "</td><td colspan='2'> Alternate Phone:" & alternatecontactphoneTextBox.Text & "</td></tr>" & _
        "<tr><td colspan='2'> eMail: " & EMailTextBox.Text & "</td><td colspan='2'> IP Address: " & ipaddressTextBox.Text & "</td></tr>" & _
        "<tr><td> Location: " & FacilityCdeMailLabel.Text & "</td><td> Building: " & buildingEmailnameLabel.Text & "</td><td colspan='2'> Floor: " & floorEmailLabel.Text & "</td></tr>" & _
        "<tr><td colspan='2'> Address1: " & Address1Textbox.Text & "</td><td colspan='2'> Address2: " & Address2textbox.Text & "</td></tr>" & _
        "<tr><td colspan='2'> City: " & Citytextbox.Text & "</td><td>State: " & Statetextbox.Text & "</td><td> Office: " & OfficeTextbox.Text & "</td></tr>" & _
        "<tr><td colspan='4'><table>" & _
        "<tr>" & _
            "<td align='center'  style='font-size:large'>" & _
"               " & _
            "<b>View Details by clicking on link below</b>" & _
"                 " & _
             "</td>    " & _
        "</tr>" & _
        "<tr>" & _
            "<td align='center'>" & Link & "</td>" & _
        "</tr>" & _
        "<tr>" & _
             "<td colspan='4' align='center'  style='font-size:large'>" & _
        "               " & _
                    "<b> DO NOT REPLY to This eMail</b>" & _
        "                 " & _
                     "</td>    " & _
                "</tr></table>" & _
         "</td></tr></table>" & _
        "</body>" & _
        "</html>"




        'EmailList = New EmailListOne(RequestNum, Session("CSCConn")).EmailListReturn

        'fulleMailList = EmailList
        Try
            If EMailTextBox.Text = "" Then

                Exit Sub

            End If

            EmailList = emailTextBox.Text & ";"

            fulleMailList = emailTextBox.Text & ";"

            If Session("environment").ToString.ToLower = "testing" Then
                EmailList = "Jeff.Mele@crozer.org"
                'sBody = sBody & fulleMailList
            Else

                sBody = sBody & fulleMailList

            End If

        Catch ex As Exception
            sSubject = "Ticket/RFS Email Error " & sSubject
            EmailList = "Jeff.Mele@crozer.org"
        End Try

        toList = EmailList.Split(";")
        If toList.Length = 1 Then
            Mailmsg.To.Add(New System.Net.Mail.MailAddress(toList(0)))
        Else
        End If
        For i = 0 To toList.Length - 1 ' - 2
            If toList(i).Length > 10 Then ' validity check
                Mailmsg.To.Add(New System.Net.Mail.MailAddress(toList(i)))
            End If
        Next

		'Dim mailObj As New MailMessage
		Mailmsg.From = New MailAddress("ckhsitcsc@crozer.org")
		Mailmsg.IsBodyHtml = True

        Mailmsg.Subject = sSubject
        Mailmsg.Body = sBody

        Dim smtpClient As New SmtpClient()
        smtpClient.Host = "ah1smtprelay01.altahospitals.com"

        smtpClient.Send(Mailmsg)

    End Sub
    Protected Sub SendEmailtoTechsGroupAssign(ByVal RequestNum As Integer, ByVal GroupNum As String, ByVal sGroupName As String)
        Dim path As String
        Dim directory As String = ""
        Dim toList As String()
        Dim Link As String
        Dim sSubject As String
        Dim sBody As String
        Dim fulleMailList As String
        Dim i As Integer
        Dim EmailListp1 As String
        Dim MgrEmailList As String = ""


        path = HttpContext.Current.Request.Url.AbsoluteUri
        Dim pathParts() As String = Split(path, "/")

        If sGroupName = "Select" Or sGroupName = "" Then
            Exit Sub

        End If



        Do While i < pathParts.Length() - 1

            directory = directory + pathParts(i) + "/"
            i = i + 1

        Loop

        Mailmsg.To.Clear()

        If Session("environment").ToString.ToLower = "testing" Then
            'toEmail = "Jeff.Mele@crozer.org"
            Link = "http://webdev3/CSCV2/ViewRequestTicket.aspx?RequestNum=" & RequestNum.ToString
        Else
            Link = "http://Casemgmt02/CSCV2/ViewRequestTicket.aspx?RequestNum=" & RequestNum.ToString

        End If


        sSubject = "New Ticket #" & RequestNum & " in group queue " & sGroupName


        sBody = "<!DOCTYPE html>" & _
        "<html>" & _
        "<head>" & _
        "<style>" & _
        "table" & _
        "{" & _
        "border-collapse:collapse;" & _
        "font-family: 'Helvetica Neue', 'Lucida Grande', 'Segoe UI', Arial, Helvetica, Verdana, sans-serif;" & _
        "}" & _
        "table, th, td" & _
        "{" & _
        "border: 1px solid black;" & _
        "padding: 3px" & _
        "}" & _
        "</style>" & _
        "</head>" & _
        "" & _
        "<body>" & _
        "<table cellpadding='5px'>" & _
                "<tr>" & _
                    "<td colspan='4' align='center'  style='font-size:large' color='red'>" & _
        "               " & _
                    "<b> Ticket/RFS </b>" & _
        "                 " & _
                     "</td>    " & _
                "</tr>" & _
                "<tr>" & _
                    "<td style='font-weight:bold' colspan='1'>Name:</td>" & _
                    "<td colspan='3'>" & FirstNameTextBox.Text & " " & LastNameTextBox.Text & "</td>" & _
                "</tr>" & _
                "<tr>" & _
                    "<td style='font-weight:bold' colspan='1'>Request#:</td>" & _
                    "<td colspan='3'>" & RequestNum & "</td>" & _
                "</tr>" & _
                "<tr>" & _
                    "<td style='font-weight:bold' colspan='1'>Submitted By:</td>" & _
                    "<td colspan='3'>" & UserInfo.UserName & "</td>" & _
                "</tr>" & _
                   "<tr>" & _
                    "<td style='font-weight:bold' colspan='1'>Requestor Email:</td>" & _
                    "<td colspan='3'>" & EMailTextBox.Text & "</td>" & _
                "</tr>" & _
                "<tr>" & _
                    "<td style='font-weight:bold' colspan='1'>Date Requested:</td>" & _
                    "<td colspan='3'>" & DateTime.Now.ToString("yyyy/MM/dd-hh:mm") & "</td>" & _
                "</tr>" & _
                "<tr>" & _
                    "<td style='font-weight:bold' colspan='4'>Request Information </td>" & _
                "</tr>" & _
        "      " & _
        "     " & _
        "<tr><td colspan='4'><b>Description:</b> " & shortdescTextBox.Text & "</td></tr>" & _
        "<tr><td> Request Type: " & RequestType.Text & "</td><td> Category: " & categorycd.Text & "</td><td> Type:" & typecd.Text & "</td><td> Item: " & itemcd.Text & "</td></tr>" & _
        "<tr><td colspan='2'> Phone: " & PhoneTextBox.Text & "</td><td colspan='2'> Alternate Phone:" & alternatecontactphoneTextBox.Text & "</td></tr>" & _
        "<tr><td colspan='2'> eMail: " & EMailTextBox.Text & "</td><td colspan='2'> IP Address: " & ipaddressTextBox.Text & "</td></tr>" & _
        "<tr><td> Location: " & FacilityCdeMailLabel.Text & "</td><td> Building: " & buildingEmailnameLabel.Text & "</td><td colspan='2'> Floor: " & floorEmailLabel.Text & "</td></tr>" & _
        "<tr><td colspan='2'> Address1: " & Address1Textbox.Text & "</td><td colspan='2'> Address2: " & Address2textbox.Text & "</td></tr>" & _
        "<tr><td colspan='2'> City: " & Citytextbox.Text & "</td><td>State: " & Statetextbox.Text & "</td><td> Office: " & OfficeTextbox.Text & "</td></tr>" & _
        "<tr><td colspan='4'><table>" & _
        "<tr>" & _
            "<td align='center'  style='font-size:large'>" & _
"               " & _
            "<b>View Details by clicking on link below</b>" & _
"                 " & _
             "</td>    " & _
        "</tr>" & _
        "<tr>" & _
            "<td align='center'>" & Link & "</td>" & _
        "</tr>" & _
        "<tr>" & _
             "<td colspan='4' align='center'  style='font-size:large'>" & _
        "               " & _
                    "<b> DO NOT REPLY to This eMail</b>" & _
        "                 " & _
                     "</td>    " & _
                "</tr></table>" & _
         "</td></tr></table>" & _
        "</body>" & _
        "</html>"




        'EmailList = New EmailListOne(RequestNum, Session("CSCConn")).EmailListReturn

        'fulleMailList = EmailList
        emaillist.GetGroupAssignEmailList(Session("CSCConn"), GroupNum, RequestNum)
        MgrEmailList = emaillist.EmailListReturn()

		If MgrEmailList.ToString.ToLower = "CSC@crozer.org" Then
			Exit Sub
		End If


		Try

            EmailListp1 = EMailTextBox.Text & ";"

            fulleMailList = EMailTextBox.Text & ";"

            If Session("environment").ToString.ToLower = "testing" Then
                EmailListp1 = "Jeff.Mele@crozer.org"
                sBody = sBody & MgrEmailList

            Else
                EmailListp1 = MgrEmailList

                'sBody = sBody & fulleMailList

            End If

        Catch ex As Exception
            sSubject = "Ticket/RFS Email Error " & sSubject
            EmailListp1 = "Jeff.Mele@crozer.org"
        End Try

        'Mailmsg.To.Add(New System.Net.Mail.MailAddress(EmailListp1))

        toList = MgrEmailList.Split(";")
        If toList.Length = 1 Then
            Mailmsg.To.Add(New System.Net.Mail.MailAddress(toList(0)))
        Else
        End If
        For i = 0 To toList.Length - 1 ' - 2
            If toList(i).Length > 10 Then ' validity check
                Mailmsg.To.Add(New System.Net.Mail.MailAddress(toList(i)))
            End If
        Next

		'Dim mailObj As New MailMessage
		Mailmsg.From = New MailAddress("ckhsitcsc@crozer.org")
		Mailmsg.IsBodyHtml = True

        Mailmsg.Subject = sSubject
        Mailmsg.Body = sBody

        Dim smtpClient As New SmtpClient()
        smtpClient.Host = "ah1smtprelay01.altahospitals.com"

        smtpClient.Send(Mailmsg)


    End Sub
    Protected Sub SendEmailtoClientGroupAssig(ByVal RequestNum As Integer, ByVal GroupName As String)

        If EMailTextBox.Text = "" Then
            Return

        End If

        If GroupName = "Select" Or GroupName = "" Then
            Exit Sub

        End If


        Dim path As String
        Dim directory As String = ""
        Dim toList As String()
        Dim Link As String
        Dim sSubject As String
        Dim sBody As String
        Dim fulleMailList As String
        Dim i As Integer
        Dim EmailList As String

        path = HttpContext.Current.Request.Url.AbsoluteUri
        Dim pathParts() As String = Split(path, "/")

        Do While i < pathParts.Length() - 1

            directory = directory + pathParts(i) + "/"
            i = i + 1

        Loop

        Mailmsg.To.Clear()

        If Session("environment").ToString.ToLower = "testing" Then
            'toEmail = "Jeff.Mele@crozer.org"
            Link = "http://webdev3/CSCV2/ViewRequestTicket.aspx?RequestNum=" & RequestNum.ToString
        Else
            Link = "http://Casemgmt02/CSCV2/ViewRequestTicket.aspx?RequestNum=" & RequestNum.ToString

        End If


        sSubject = "Ticket #" & RequestNum & " has been Assigned to NEW Group  " & currentgroupnameLabel.Text


        sBody = "<!DOCTYPE html>" & _
        "<html>" & _
        "<head>" & _
        "<style>" & _
        "table" & _
        "{" & _
        "border-collapse:collapse;" & _
        "font-family: 'Helvetica Neue', 'Lucida Grande', 'Segoe UI', Arial, Helvetica, Verdana, sans-serif;" & _
        "}" & _
        "table, th, td" & _
        "{" & _
        "border: 1px solid black;" & _
        "padding: 3px" & _
        "}" & _
        "</style>" & _
        "</head>" & _
        "" & _
        "<body>" & _
        "<table cellpadding='5px'>" & _
                "<tr>" & _
                    "<td colspan='4' align='center'  style='font-size:large' color='red'>" & _
        "               " & _
                    "<b>Ticket/RFS </b>" & _
        "                 " & _
                     "</td>    " & _
                "</tr>" & _
                "<tr>" & _
                    "<td style='font-weight:bold' colspan='1'>Name:</td>" & _
                    "<td colspan='3'>" & FirstNameTextBox.Text & " " & LastNameTextBox.Text & "</td>" & _
                "</tr>" & _
                "<tr>" & _
                    "<td style='font-weight:bold' colspan='1'>Request#:</td>" & _
                    "<td colspan='3'>" & RequestNum & "</td>" & _
                "</tr>" & _
                "<tr>" & _
                    "<td style='font-weight:bold' colspan='1'>Submitted By:</td>" & _
                    "<td colspan='3'>" & UserInfo.UserName & "</td>" & _
                "</tr>" & _
                   "<tr>" & _
                    "<td style='font-weight:bold' colspan='1'>Requestor Email:</td>" & _
                    "<td colspan='3'>" & EMailTextBox.Text & "</td>" & _
                "</tr>" & _
                "<tr>" & _
                    "<td style='font-weight:bold' colspan='1'>Date Requested:</td>" & _
                    "<td colspan='3'>" & DateTime.Now.ToString("yyyy/MM/dd-hh:mm") & "</td>" & _
                "</tr>" & _
                "<tr>" & _
                    "<td style='font-weight:bold' colspan='4'>Request Information </td>" & _
                "</tr>" & _
        "      " & _
        "     " & _
        "<tr><td colspan='4'><b>Description:</b> " & shortdescTextBox.Text & "</td></tr>" & _
        "<tr><td> Request Type: " & RequestType.Text & "</td><td> Category: " & categorycd.Text & "</td><td> Type:" & typecd.Text & "</td><td> Item: " & itemcd.Text & "</td></tr>" & _
        "<tr><td colspan='2'> Phone: " & PhoneTextBox.Text & "</td><td colspan='2'> Alternate Phone:" & alternatecontactphoneTextBox.Text & "</td></tr>" & _
        "<tr><td colspan='2'> eMail: " & EMailTextBox.Text & "</td><td colspan='2'> IP Address: " & ipaddressTextBox.Text & "</td></tr>" & _
        "<tr><td> Location: " & FacilityCdeMailLabel.Text & "</td><td> Building: " & buildingEmailnameLabel.Text & "</td><td colspan='2'> Floor: " & floorEmailLabel.Text & "</td></tr>" & _
        "<tr><td colspan='2'> Address1: " & Address1Textbox.Text & "</td><td colspan='2'> Address2: " & Address2textbox.Text & "</td></tr>" & _
        "<tr><td colspan='2'> City: " & Citytextbox.Text & "</td><td>State: " & Statetextbox.Text & "</td><td> Office: " & OfficeTextbox.Text & "</td></tr>" & _
        "<tr><td colspan='4'><table>" & _
        "<tr>" & _
            "<td align='center'  style='font-size:large'>" & _
"               " & _
            "<b>View Details by clicking on link below</b>" & _
"                 " & _
             "</td>    " & _
        "</tr>" & _
        "<tr>" & _
            "<td align='center'>" & Link & "</td>" & _
        "</tr>" & _
        "<tr>" & _
             "<td colspan='4' align='center'  style='font-size:large'>" & _
        "               " & _
                    "<b> DO NOT REPLY to This eMail</b>" & _
        "                 " & _
                     "</td>    " & _
                "</tr></table>" & _
         "</td></tr></table>" & _
        "</body>" & _
        "</html>"




        Try
            If EMailTextBox.Text = "" Then

                Exit Sub

            End If

            EmailList = EMailTextBox.Text & ";"

            fulleMailList = EMailTextBox.Text & ";"

            If Session("environment").ToString.ToLower = "testing" Then
                EmailList = "Jeff.Mele@crozer.org"
                'sBody = sBody & fulleMailList
            Else

                sBody = sBody & fulleMailList

            End If

        Catch ex As Exception
            sSubject = "Ticket/RFS Email Error " & sSubject
            EmailList = "Jeff.Mele@crozer.org"
        End Try

        toList = EmailList.Split(";")
        If toList.Length = 1 Then
            Mailmsg.To.Add(New System.Net.Mail.MailAddress(toList(0)))
        Else
        End If
        For i = 0 To toList.Length - 1 ' - 2
            If toList(i).Length > 10 Then ' validity check
                Mailmsg.To.Add(New System.Net.Mail.MailAddress(toList(i)))
            End If
        Next

		'Dim mailObj As New MailMessage
		Mailmsg.From = New MailAddress("ckhsitcsc@crozer.org")
		Mailmsg.IsBodyHtml = True

        Mailmsg.Subject = sSubject
        Mailmsg.Body = sBody

        Dim smtpClient As New SmtpClient()
        smtpClient.Host = "ah1smtprelay01.altahospitals.com"

        smtpClient.Send(Mailmsg)


    End Sub
    Protected Sub SendGroupP1Email(ByVal RequestNum As Integer)
        '======================
        ' Get current path
        '======================
        'If Session("LoginID").ToString.ToLower = "melej" Then

        Dim path As String
        Dim directory As String = ""
        Dim EmailList2 As String = ""
        Dim sSubject As String = ""
        Dim sBody As String
        Dim Link As String
        Dim fulleMailList As String
        Dim toList As String()
        Dim i As Integer


        path = HttpContext.Current.Request.Url.AbsoluteUri
        Dim pathParts() As String = Split(path, "/")


        Do While i < pathParts.Length() - 1

            directory = directory + pathParts(i) + "/"
            i = i + 1

        Loop

        Mailmsg.To.Clear()

        If Session("environment").ToString.ToLower = "testing" Then
            'toEmail = "Jeff.Mele@crozer.org"
            Link = "http://webdev3/CSCV2/ViewRequestTicket.aspx?RequestNum=" & RequestNum.ToString
        Else
            Link = "http://Casemgmt02/CSCV2/ViewRequestTicket.aspx?RequestNum=" & RequestNum.ToString

        End If


        'sSubject = "P1 Ticket# " & RequestNum.ToString

        'sBody = shortdescTextbox.Text & " "

        sSubject = "New P1 Ticket #" & RequestNum


        sBody = "<!DOCTYPE html>" & _
        "<html>" & _
        "<head>" & _
        "<style>" & _
        "table" & _
        "{" & _
        "border-collapse:collapse;" & _
        "font-family: 'Helvetica Neue', 'Lucida Grande', 'Segoe UI', Arial, Helvetica, Verdana, sans-serif;" & _
        "}" & _
        "table, th, td" & _
        "{" & _
        "border: 1px solid black;" & _
        "padding: 3px" & _
        "}" & _
        "</style>" & _
        "</head>" & _
        "" & _
        "<body>" & _
        "<table cellpadding='5px'>" & _
                "<tr>" & _
                    "<td colspan='4' align='center'  style='font-size:large' color='red'>" & _
        "               " & _
                    "<b> Ticket/RFS </b>" & _
        "                 " & _
                     "</td>    " & _
                "</tr>" & _
                "<tr>" & _
                    "<td style='font-weight:bold' colspan='1'>Name:</td>" & _
                    "<td colspan='3'>" & FirstNameTextBox.Text & " " & LastNameTextBox.Text & "</td>" & _
                "</tr>" & _
                "<tr>" & _
                    "<td style='font-weight:bold' colspan='1'>Request#:</td>" & _
                    "<td colspan='3'>" & RequestNum & "</td>" & _
                "</tr>" & _
                "<tr>" & _
                    "<td style='font-weight:bold' colspan='1'>Submitted By:</td>" & _
                    "<td colspan='3'>" & UserInfo.UserName & "</td>" & _
                "</tr>" & _
                   "<tr>" & _
                    "<td style='font-weight:bold' colspan='1'>Requestor Email:</td>" & _
                    "<td colspan='3'>" & EMailTextBox.Text & "</td>" & _
                "</tr>" & _
                "<tr>" & _
                    "<td style='font-weight:bold' colspan='1'>Date Requested:</td>" & _
                    "<td colspan='3'>" & DateTime.Now.ToString("yyyy/MM/dd-hh:mm") & "</td>" & _
                "</tr>" & _
                "<tr>" & _
                    "<td style='font-weight:bold' colspan='4'>Request Information </td>" & _
                "</tr>" & _
        "      " & _
        "     " & _
        "<tr><td colspan='4'><b>Description:</b> " & shortdescTextBox.Text & "</td></tr>" & _
        "<tr><td> Request Type: " & RequestTyperbl.SelectedValue.ToString & "</td><td> Category: " & categorycdddl.SelectedValue.ToString & "</td><td> Type:" & typecdddl.SelectedValue.ToString & "</td><td> Item: " & itemcdddl.SelectedValue.ToString & "</td></tr>" & _
        "<tr><td colspan='2'> Phone: " & PhoneTextBox.Text & "</td><td colspan='2'> Alternate Phone:" & alternatecontactphoneTextBox.Text & "</td></tr>" & _
        "<tr><td colspan='2'> eMail: " & EMailTextBox.Text & "</td><td colspan='2'> IP Address: " & ipaddressTextBox.Text & "</td></tr>" & _
        "<tr><td> Location: " & FacilityCdeMailLabel.Text & "</td><td> Building: " & buildingEmailnameLabel.Text & "</td><td colspan='2'> Floor: " & floorEmailLabel.Text & "</td></tr>" & _
        "<tr><td colspan='2'> Address1: " & Address1Textbox.Text & "</td><td colspan='2'> Address2: " & Address2textbox.Text & "</td></tr>" & _
        "<tr><td colspan='2'> City: " & Citytextbox.Text & "</td><td>State: " & Statetextbox.Text & "</td><td> Office: " & OfficeTextbox.Text & "</td></tr>" & _
        "<tr><td colspan='4'><table>" & _
        "<tr>" & _
            "<td align='center'  style='font-size:large'>" & _
"               " & _
            "<b>View Details by clicking on link below</b>" & _
"                 " & _
             "</td>    " & _
        "</tr>" & _
        "<tr>" & _
            "<td align='center'>" & Link & "</td>" & _
        "</tr>" & _
        "<tr>" & _
             "<td colspan='4' align='center'  style='font-size:large'>" & _
        "               " & _
                    "<b> DO NOT REPLY to This eMail</b>" & _
        "                 " & _
                     "</td>    " & _
                "</tr></table>" & _
         "</td></tr></table>" & _
        "</body>" & _
        "</html>"


        'SelGroupP1EmailV9

        emaillist.getGroupP1emaillist(Session("CSCConn"), RequestNum.ToString, "new")

        EmailList2 = emaillist.EmailListReturn()


        'EmailList2 = New EmailListOne(RequestNum, Session("EmployeeConn")).EmailListReturn

        fulleMailList = EmailList2

        If Session("environment").ToString.ToLower = "testing" Then
            EmailList2 = "Jeff.Mele@crozer.org"
            sBody = sBody & fulleMailList
            Mailmsg.To.Add(New System.Net.Mail.MailAddress(EmailList2))

        Else


            'EmailList2 = "2672944056@vtext.com;6105177353@txt.att.net"
            toList = EmailList2.Split(";")
            If toList.Length = 1 Then

                Mailmsg.To.Add(New System.Net.Mail.MailAddress(toList(0)))


            Else
            End If
            For i = 0 To toList.Length - 1 ' - 2
                If toList(i).Length > 10 Then ' validity check
                    Mailmsg.To.Add(New System.Net.Mail.MailAddress(toList(i)))
                    Console.WriteLine(toList(i))
                End If
            Next

        End If

        'Dim mailObj As New MailMessage

        Mailmsg.From = New MailAddress("CSCemail@crozer.org")
        Mailmsg.IsBodyHtml = True

        Mailmsg.Subject = sSubject
        Mailmsg.Body = sBody

        Dim smtpClient As New SmtpClient()
        smtpClient.Host = "ah1smtprelay01.altahospitals.com"

        Try

            smtpClient.Send(Mailmsg)


            'Console.WriteLine(Mailmsg)

        Catch ex As Exception
            'Console.WriteLine(ex.Message)

        End Try

    End Sub
    'Protected Sub UpdateClientPanel()


    '    Dim c3Update As New Client3
    '    c3Update = Client3Contrl.GetClientByClientNum(lblRequestorClientNum.Text)


    '    frmHelper.FillForm(c3Update, tblUpdateClient, "Update")


    '    ddlBinder.BindData(ddlUpdateFacilityCd, "SelFacilityCodes", "facility_cd", "facility_short_name", Session("EmployeeConn"))



    'End Sub

    'Protected Sub btnUpdateClient_Click(sender As Object, e As System.EventArgs) Handles btnUpdateClient.Click
    '    Dim frmHelperRequestTicket As New FormHelper()

    '    frmHelperRequestTicket.InitializeUpdateForm("UpdClientObjectByNumberV3", tblUpdateClient, "Update")
    '    frmHelperRequestTicket.AddCmdParameter("@ClientNum", lblRequestorClientNum.Text)
    '    frmHelperRequestTicket.UpdateForm()

    '    c3Requestor = Client3Contrl.GetClientByClientNum(lblRequestorClientNum.Text)



    '    '   ViewState("objc3Requestor") = c3Requestor



    '    frmHelperRequestTicket.FillForm(c3Requestor, Page, "Requestor")


    '    mpeUpdateClient.Hide()
    'End Sub

    'Protected Sub ddlChangeGroup_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlChangeGroup.SelectedIndexChanged
    '    ddlBinder = New DropDownListBinder(ddlChangeTech, "selTechnicians", Session("EmployeeConn"))
    '    ddlBinder.AddDDLCriteria("@GroupNum", ddlChangeGroup.SelectedValue, SqlDbType.Int)
    '    ddlBinder.BindData("client_num", "TechName")

    '    uplTicket.Update()

    'End Sub


    Private Sub FillVarsTable()
        Dim sErr As String = ""
        Try

            Dim item As Object
            Dim tc As TableCell
            Dim tr As TableRow
            Dim x As Integer
            tr = New TableRow
            tc = New TableCell
            tc.Text = "Session"
            tr.Cells.Add(tc)
            tc = New TableCell
            tc.Text = " Variables"
            tr.Cells.Add(tc)
            tblVars.Rows.Add(tr)
            tr = New TableRow
            tc = New TableCell
            tc.Text = ""
            tr.Cells.Add(tc)
            tc = New TableCell
            tc.Text = ""
            tr.Cells.Add(tc)
            tblVars.Rows.Add(tr)

            '  For Each item In Session.Contents
            For y As Integer = 0 To Session.Contents.Count - 1
                tr = New TableRow
                tc = New TableCell
                tc.Text = Session.Keys(y) '.ToString 'item.ToString()
                tr.Cells.Add(tc)
                tc = New TableCell
                If Not Session.Contents(y) Is Nothing Then
                    tc.Text = Session.Contents.Item(y).ToString
                    tr.Cells.Add(tc)
                    tblVars.Rows.Add(tr)
                    x = x + 1
                Else ' emmpty session
                    tc.Text = "Nothing"
                    tr.Cells.Add(tc)
                    tblVars.Rows.Add(tr)
                End If
            Next
            tr = New TableRow
            tc = New TableCell
            tc.Text = "Application"
            tr.Cells.Add(tc)
            tc = New TableCell
            tc.Text = " Variables"
            tr.Cells.Add(tc)
            tblVars.Rows.Add(tr)
            tr = New TableRow
            tc = New TableCell
            tc.Text = ""
            tr.Cells.Add(tc)
            tc = New TableCell
            tc.Text = ""
            tr.Cells.Add(tc)
            tblVars.Rows.Add(tr)
            x = 0
            '  For Each item In Application.Contents
            For y As Integer = 0 To Application.Contents.Count - 1
                tr = New TableRow
                tc = New TableCell
                tc.Text = Application.Keys(y).ToString()
                tr.Cells.Add(tc)
                tc = New TableCell
                If Not Application.Contents(y) Is Nothing Then
                    tc.Text = Application.Contents(x).ToString
                    tr.Cells.Add(tc)
                    tblVars.Rows.Add(tr)
                    x = x + 1
                Else ' emmpty session
                    tc.Text = "Nothing"
                    tr.Cells.Add(tc)
                    tblVars.Rows.Add(tr)
                End If
            Next
            x = 0
            tr = New TableRow
            tc = New TableCell
            tc.Text = "System.IO.Path.GetFileNameWithoutExtension(Request.PhysicalPath)"
            tr.Cells.Add(tc)
            tc = New TableCell
            tc.Text = System.IO.Path.GetFileNameWithoutExtension(Request.PhysicalPath).ToString

            tr.Cells.Add(tc)
            tblVars.Rows.Add(tr)


            '  For Each item In Request.ServerVariables
            For y As Integer = 0 To Request.ServerVariables.Count - 1
                tr = New TableRow
                tc = New TableCell
                tc.Text = Request.ServerVariables.Keys(y).ToString()
                tr.Cells.Add(tc)
                tc = New TableCell
                tc.Text = Request.ServerVariables(y).ToString

                tr.Cells.Add(tc)
                tblVars.Rows.Add(tr)
                x = x + 1
            Next
        Catch ex As Exception
            sErr = ex.Message
            'Dim objCurrentInfo As ProcessInfo = ProcessModelInfo.GetCurrentProcessInfo()

            'Dim perfAppRestarts As New PerformanceCounter("ASP.NET", "Application Restarts")
            'Dim perfFreeMem As New PerformanceCounter("Memory", "Available MBytes")

            'lblRequestCount.Text = objCurrentInfo.RequestCount()
            'lblAppRestarts.Text = perfAppRestarts.NextValue()
            'lblFreeMem.Text = perfFreeMem.NextValue()

            'lblPeakMem.Text = objCurrentInfo.PeakMemoryUsed
            'lblProcID.Text = objCurrentInfo.ProcessID
        End Try


    End Sub

    Protected Sub categorycdddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles categorycdddl.SelectedIndexChanged
        'reportPanel


        Dim ddlBinder As DropDownListBinder

        categorycd.Text = categorycdddl.SelectedValue


        ddlBinder = New DropDownListBinder(typecdddl, "SelCategoryType", Session("CSCConn"))
        ddlBinder.AddDDLCriteria("@Category_cd", categorycdddl.SelectedValue, SqlDbType.NVarChar)
        ddlBinder.BindData("type_cd", "CatagoryTypeDesc")

        Typelbl.Visible = True
        typecdddl.Visible = True

        typecdddl.SelectedIndex = -1

        If categorycd.Text = "reports" Then

            'report panel 

            Dim cblFacilitesBinder As New CheckBoxListBinder(cblFacilities, "SelFacilityEntity", Session("EmployeeConn"))
            cblFacilitesBinder.AddDDLCriteria("@EntityFacility", "Facility", SqlDbType.NVarChar)
            cblFacilitesBinder.BindData("Code", "EntityFacilityDesc")


            'SelDistrictCodes
            'cblDistrictCodes
            Dim cblDistrictBinder As New CheckBoxListBinder(cblDistrictCodes, "SelDistrictCodes", Session("CSCConn"))
            'cblDistrictBinder.AddDDLCriteria("@EntityFacility", "Facility", SqlDbType.NVarChar)
            cblDistrictBinder.BindData("district_cd", "DistrictDesc")


            'SelFrequencyCodes
            'ReportFreqddl
            Dim ddlFreqBinder As New DropDownListBinder(ReportFreqddl, "SelFrequencyCodes", Session("CSCConn"))
            ddlFreqBinder.BindData("frequency_cd", "frequency_desc")


            reportPanel.Visible = True
            TechPanl.Visible = False
        Else

            reportPanel.Visible = False
            TechPanl.Visible = True
        End If

        'getIPAddress()


    End Sub
    Protected Sub ReportTypeddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ReportTypeddl.SelectedIndexChanged

        If ReportTypeTextBox.Text <> "" Then

            ReportTypeTextBox.Text = ReportTypeddl.SelectedValue
            ReportTypeDescTextBox.Text = ReportTypeddl.SelectedItem.ToString

        ElseIf ReportTypeddl.SelectedValue.ToLower = "select" Then
            ReportTypeTextBox.Text = Nothing
            ReportTypeDescTextBox.Text = Nothing

        Else

            ReportTypeTextBox.Text = ReportTypeddl.SelectedValue
            ReportTypeDescTextBox.Text = ReportTypeddl.SelectedItem.ToString

        End If

    End Sub
    Protected Sub ReportFreqddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ReportFreqddl.SelectedIndexChanged

        If ReportFreqTextBox.Text <> "" Then

            ReportFreqTextBox.Text = ReportFreqddl.SelectedValue
            ReportFreqDescTextBox.Text = ReportFreqddl.SelectedItem.ToString

        ElseIf ReportTypeddl.SelectedValue.ToLower = "select" Then
            ReportFreqTextBox.Text = Nothing
            ReportFreqDescTextBox.Text = Nothing

        Else

            ReportFreqTextBox.Text = ReportFreqddl.SelectedValue
            ReportFreqDescTextBox.Text = ReportFreqddl.SelectedItem.ToString

        End If

    End Sub

    Protected Sub typecdddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles typecdddl.SelectedIndexChanged
        Dim ddlBinder As DropDownListBinder
        typecd.Text = typecdddl.SelectedValue


        ddlBinder = New DropDownListBinder(itemcdddl, "SelCategoryItems", Session("CSCConn"))
        ddlBinder.AddDDLCriteria("@Category_cd", categorycd.Text, SqlDbType.NVarChar)


        ddlBinder.AddDDLCriteria("@type_cd", typecdddl.SelectedValue, SqlDbType.NVarChar)
        ddlBinder.BindData("item_cd", "ItemTypeDesc")

        'Always add other
        itemcdddl.Items.RemoveAt(0)

        itemcdddl.SelectedIndex = -1

        itemcdddl.Items.Insert(0, "Other")

        Itemlbl.Visible = True
        itemcdddl.Visible = True

        'getIPAddress()


    End Sub
    Protected Sub itemcdddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles itemcdddl.SelectedIndexChanged
        itemcd.Text = itemcdddl.SelectedValue

        'getIPAddress()

    End Sub

    Public Shared Function GetIP4Address() As String
        Dim IP4Address As String = String.Empty

        For Each IPA As IPAddress In Dns.GetHostAddresses(HttpContext.Current.Request.UserHostAddress)
            If IPA.AddressFamily.ToString() = "InterNetwork" Then
                IP4Address = IPA.ToString()
                Exit For
            End If
        Next

        If IP4Address <> String.Empty Then
            Return IP4Address
        End If

        For Each IPA As IPAddress In Dns.GetHostAddresses(Dns.GetHostName())
            If IPA.AddressFamily.ToString() = "InterNetwork" Then
                IP4Address = IPA.ToString()
                Exit For
            End If
        Next


        Return IP4Address
    End Function
    Private Shared Function GetMachineNameFromIPAddress(ByVal ipAdress As String) As String
        Dim machineName As String = String.Empty
        Try


            Dim hostEntry As System.Net.IPHostEntry = System.Net.Dns.GetHostEntry(ipAdress)

            machineName = hostEntry.HostName

        Catch ex As Exception
            'machineName = "N/A"
        End Try

        Return machineName
    End Function
    Protected Sub getAsstag()
        assettagTextBox.Text = GetMachineNameFromIPAddress(ipaddressLabel.Text)

        If Session("SecurityLevelNum") = 70 Then
            assettagTextBox.Text = ""

        End If

    End Sub
    Protected Sub getIPAddress()
        'Try

        '    ipaddressLabel.Text = GetIP4Address()
        '    ipaddressTextBox.Text = GetIP4Address()


        'Catch ex As Exception

        '    ipaddressLabel.Text = "::1"

        'End Try

        ipaddressLabel.Text = GetIP4Address()
        ipaddressTextBox.Text = GetIP4Address()


        If Session("SecurityLevelNum") = 70 Then
            assettagTextBox.Text = ""

            ipaddressLabel.Text = ""
            ipaddressTextBox.Text = ""

        End If

        'Dim PCName As String = ""

        ''Dim pcname3 As String
        'If ipaddressLabel.Text <> "" Then
        '    PCName = GetMachineNameFromIPAddress(ipaddressLabel.Text)

        '    If PCName.ToLower.IndexOf("casemgmt") = 0 Then
        '        PCName = Nothing

        '    End If

        'End If



        'Dim host As IPHostEntry = Dns.GetHostEntry(Dns.GetHostName())

        'PCName = host.HostName

        'pcname3 = Dns.GetHostEntry(ipaddressTextBox.Text).HostName.ToString

        'Dim pcname3 As New Process
        'pcname3 = Process.Start("cmd", "/c netstat -a" & ipaddressLabel.Text)
        'pcname3.Start()

        'PCName = Dns.GetHostEntry(Dns.GetHostName).AddressList(1).ToString()

        'PCName = Dns.EndGetHostByName(HttpContext.Current.Request.ServerVariables("REMOTE_HOST")).HostName.ToString
        ' Dns.GetHostName()

        'assettagLabel.Text = PCName

        'assettagTextBox.Text = PCName 


        'Try
        '    pcIP = System.Net.Dns.GetHostEntry(HttpContext.Current.Request.ServerVariables("REMOTE_HOST")).HostName

        '    'Dns.GetHostEntry(PCName).AddressList(1).ToString()

        'Catch ex As Exception
        '    pcIP = "::"
        'End Try

    End Sub
    Protected Sub tagradio_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles tagradio.SelectedIndexChanged

        If tagradio.SelectedValue = "no" Then
            assettagTextBox.Text = ""
            ipaddressTextBox.Text = ""

        Else

        End If

    End Sub

    Public Function CheckRequiredFields() As Boolean
        Dim removeCss As New PageControls(Page)
        removeCss.RemoveCssClass(Page, "textInputRequired")

        'Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
        'Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")

        'emp_type_cdLabel.Text = emp_type_cdrbl.SelectedValue

        Dim Catdesc As New RequiredField(categorycdddl, "Category Type")
        categorycdddl.BackColor() = Color.Yellow
        categorycdddl.BorderColor() = Color.Black
        categorycdddl.BorderStyle() = BorderStyle.Solid
        categorycdddl.BorderWidth = Unit.Pixel(2)

        Dim TypeDesc As New RequiredField(typecdddl, "Type ")

        typecdddl.BackColor() = Color.Yellow
        typecdddl.BorderColor() = Color.Black
        typecdddl.BorderStyle() = BorderStyle.Solid
        typecdddl.BorderWidth = Unit.Pixel(2)

        'Dim ItemDesc As New RequiredField(itemcdddl, "Item ")

        'itemcdddl.BackColor() = Color.Yellow
        'itemcdddl.BorderColor() = Color.Black
        'itemcdddl.BorderStyle() = BorderStyle.Solid
        'itemcdddl.BorderWidth = Unit.Pixel(2)

        Return bHasError
    End Function

    Protected Sub SubmitFile()


        'If txtFilename.Text <> "" Then

        'End If

        Using SqlConnection As New SqlConnection(Session("CSCConn"))

            Dim Cmd As New SqlCommand()
            Cmd.CommandText = "InsRequestFiles"

            Cmd.Parameters.Add("@request_num", SqlDbType.NVarChar).Value = requestnumlabel.Text

            Cmd.Parameters.Add("@FileType", SqlDbType.NVarChar).Value = filetype
            Cmd.Parameters.Add("@FileName", SqlDbType.NVarChar).Value = txtFilename.Text

            'Cmd.Parameters.Add("@FileContent", SqlDbType.VarBinary).Value = Ticbites

            'Cmd.Parameters.Add("@ClientNum", SqlDbType.Int).Value = entryclientnumLabel.Text

            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.Connection = SqlConnection

            SqlConnection.Open()
            Cmd.ExecuteNonQuery()


            ' -- now show image from DB
            'Dim Selcmd As New SqlCommand()
            'Selcmd.CommandText = "SelRequestImages"

            'Selcmd.Parameters.Add("@request_num", SqlDbType.Int).Value = 1

            'Selcmd.CommandType = CommandType.StoredProcedure
            'Selcmd.Connection = SqlConnection

            'Selcmd.ExecuteScalar()

            'Dim da As New System.Data.SqlClient.SqlDataAdapter(Selcmd)
            'Dim ds As New DataSet()
            'da.Fill(ds)
            'Dim bits As Byte() = CType(ds.Tables(0).Rows(0).Item(2), Byte())
            'Dim memoryBits As New MemoryStream(bits)
            'Dim bitmap As New Bitmap(memoryBits) 'bitmap has the image now.

            'requestImage.ResolveUrl = bitmap
            SqlConnection.Close()
            SqlConnection.Dispose()



        End Using



        'Dim fileOK As Boolean

        ''If ShowBtn.Text = "true" Then
        ''    btnOpenfile.Visible = True

        ''End If

        'If fileUpload2.HasFile Or ShowBtn.Text = "true" Then


        '    fileName = fileUpload2.FileName

        '    filetype = fileUpload2.PostedFile.ContentType


        '    fileExtension = System.IO.Path. _
        '        GetExtension(fileUpload2.FileName).ToLower()

        '    Dim allowedExtensions As String() = _
        '        {".jpg", ".jpeg", ".png", ".gif", ".doc", ".docx", ".pdf", ".txt", ".xls", ".csv"}
        '    For i As Integer = 0 To allowedExtensions.Length - 1
        '        If fileExtension = allowedExtensions(i) Then
        '            fileOK = True
        '        End If
        '    Next
        '    If fileOK Then
        '        Try

        '            savePath += fileUpload2.FileName

        '            fileUpload2.SaveAs(savePath)

        '            fileUpload2.SaveAs(saveImage)

        '            imagelocation.Text = savePath

        '            Select Case fileExtension
        '                Case ".jpg", ".jpeg", ".png", ".gif"
        '                    requestImg2.Visible = True
        '            End Select

        '            Dim fs As FileStream = New FileStream(savePath, FileMode.Open, FileAccess.Read)
        '            Dim br As BinaryReader = New BinaryReader(fs)
        '            Dim bites As Byte() = br.ReadBytes(Convert.ToInt32(fs.Length))

        '            br.Close()
        '            fs.Close()

        '            'DisplayFile()  View File

        '            Using SqlConnection As New SqlConnection(Session("CSCConn"))

        '                Dim Cmd As New SqlCommand()
        '                Cmd.CommandText = "InsRequestFiles"

        '                Cmd.Parameters.Add("@request_num", SqlDbType.NVarChar).Value = requestnumlabel.Text

        '                Cmd.Parameters.Add("@FileType", SqlDbType.NVarChar).Value = filetype
        '                Cmd.Parameters.Add("@FileName", SqlDbType.NVarChar).Value = fileName

        '                Cmd.Parameters.Add("@FileContent", SqlDbType.VarBinary).Value = bites

        '                Cmd.Parameters.Add("@ClientNum", SqlDbType.Int).Value = entryclientnumLabel.Text

        '                Cmd.CommandType = CommandType.StoredProcedure
        '                Cmd.Connection = SqlConnection

        '                SqlConnection.Open()
        '                Cmd.ExecuteNonQuery()


        '                ' -- now show image from DB
        '                'Dim Selcmd As New SqlCommand()
        '                'Selcmd.CommandText = "SelRequestImages"

        '                'Selcmd.Parameters.Add("@request_num", SqlDbType.Int).Value = 1

        '                'Selcmd.CommandType = CommandType.StoredProcedure
        '                'Selcmd.Connection = SqlConnection

        '                'Selcmd.ExecuteScalar()

        '                'Dim da As New System.Data.SqlClient.SqlDataAdapter(Selcmd)
        '                'Dim ds As New DataSet()
        '                'da.Fill(ds)
        '                'Dim bits As Byte() = CType(ds.Tables(0).Rows(0).Item(2), Byte())
        '                'Dim memoryBits As New MemoryStream(bits)
        '                'Dim bitmap As New Bitmap(memoryBits) 'bitmap has the image now.

        '                'requestImage.ResolveUrl = bitmap
        '                SqlConnection.Close()
        '                SqlConnection.Dispose()



        '            End Using


        '            'imageReq.DataImageUrlField = savePath
        '            'requestImg2.Src = savePath

        '            'requestImage.ImageUrl = savePath


        '            Label1.Text = "File uploaded!"
        '            Label1.Font.Size = FontUnit.Medium
        '            Label1.BackColor() = Color.White
        '            Label1.ForeColor = Color.Black
        '            Label1.BorderStyle() = BorderStyle.NotSet
        '            Label1.BorderWidth = Unit.Pixel(0)



        '            'File.Delete(savePath)



        '        Catch ex As Exception
        '            Label1.Text = "File could not be uploaded."
        '            Label1.Font.Size = FontUnit.Large
        '            Label1.BackColor() = Color.Red
        '            Label1.ForeColor = Color.White
        '            Label1.BorderStyle() = BorderStyle.Solid
        '            Label1.BorderWidth = Unit.Pixel(2)

        '        End Try
        '    Else
        '        Label1.Visible = True
        '        Label1.Text = "Cannot accept files of this type or File does not exists or File not Loaded"
        '        Label1.Font.Size = FontUnit.Large
        '        Label1.BackColor() = Color.Red
        '        Label1.ForeColor = Color.White
        '        Label1.BorderStyle() = BorderStyle.Solid
        '        Label1.BorderWidth = Unit.Pixel(2)

        '    End If
        'Else
        '    If filetype <> "" Then

        '        Label1.Visible = True
        '        Label1.Text = "Cannot accept files of this type or File does not exists or File not Loaded"
        '        Label1.Font.Size = FontUnit.Large
        '        Label1.BackColor() = Color.Red
        '        Label1.ForeColor = Color.White
        '        Label1.BorderStyle() = BorderStyle.Solid
        '        Label1.BorderWidth = Unit.Pixel(2)

        '    Else
        '        Exit Sub
        '    End If

        'End If


    End Sub
    Sub DisplayFile()
        Response.Buffer = True
        Response.ContentType = filetype
        Dim Myfile() As Byte
        fileExtension = System.IO.Path. _
    GetExtension(fileUpload2.FileName).ToLower()


        Dim fs As FileStream = New FileStream(savePath, FileMode.Open, FileAccess.Read)
        Dim br As BinaryReader = New BinaryReader(fs)
        Dim bites As Byte() = br.ReadBytes(Convert.ToInt32(fs.Length))


        Myfile = bites

        Select Case fileExtension
            Case ".jpg", ".jpeg", ".png", ".gif"
                requestImg2.Visible = True
            Case Else

        End Select
        Response.BinaryWrite(Myfile)


        br.Close()
        fs.Close()
        Response.End()

    End Sub

    Protected Sub changegroupddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles changegroupddl.SelectedIndexChanged
        'currGroupLb.Text = "New Group:"
        currentgroupnumLabel.Text = changegroupddl.SelectedValue
        currentgroupnameLabel.Text = changegroupddl.SelectedItem.ToString

        ' ''Change Tech assignment
        'currenttechnumLabel.Text = ""
        'currTechlb.Text = "Previous Tech:"
        If changegroupddl.SelectedItem.ToString <> "Select" Then

            Dim ddlCurrTech As New DropDownListBinder(ChangeTechddl, "selTechsByGroupNum", Session("CSCConn"))
            'currentgroupnumLabel.Text
            ddlCurrTech.AddDDLCriteria("@group_num", currentgroupnumLabel.Text, SqlDbType.NVarChar)
            ddlCurrTech.AddDDLCriteria("@securityLevel", Session("SecurityLevelNum"), SqlDbType.Int)

            ddlCurrTech.BindData("tech_num", "TechName")

            ChangeTechddl.Enabled = True
        Else
            currentgroupnumLabel.Text = ""

        End If

    End Sub

    Protected Sub ChangeTechddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ChangeTechddl.SelectedIndexChanged
        currenttechnumLabel.Text = ChangeTechddl.SelectedValue
        If currenttechnumLabel.Text.ToLower = "select" Then
            currenttechnumLabel.Text = ""
        End If
        'currenttechnamelabel.Text = ChangeTechddl.SelectedItem.ToString
    End Sub

    Protected Sub fileUpload2_UploadedComplete(sender As Object, e As AjaxControlToolkit.AsyncFileUploadEventArgs) Handles fileUpload2.UploadedComplete
        'btnOpenfile.Visible = True
        
        Dim fileOK As Boolean

        fileName = fileUpload2.FileName

        txtFilename.Text = fileName

        filetype = fileUpload2.PostedFile.ContentType


        fileExtension = System.IO.Path. _
            GetExtension(fileUpload2.FileName).ToLower()

        Fileextensionlbl.Text = fileExtension

        Dim allowedExtensions As String() = _
            {".jpg", ".jpeg", ".png", ".gif", ".doc", ".docx", ".pdf", ".txt", ".xls", ".xlsx", ".csv", ".pptx"}
        For i As Integer = 0 To allowedExtensions.Length - 1
            If fileExtension = allowedExtensions(i) Then
                fileOK = True
            End If
        Next
        If fileOK Then
            Try

                savePath += fileUpload2.FileName

                fileUpload2.SaveAs(savePath)

                fileUpload2.SaveAs(saveImage)

                'imagelocation.Text = savePath

                'Select Case fileExtension
                '    Case ".jpg", ".jpeg", ".png", ".gif"
                '        requestImg2.Visible = True
                'End Select

                Dim fs As FileStream = New FileStream(savePath, FileMode.Open, FileAccess.Read)
                Dim br As BinaryReader = New BinaryReader(fs)
                Dim bites As Byte() = br.ReadBytes(Convert.ToInt32(fs.Length))

                Dim fsize As Integer = Convert.ToInt32(fs.Length)

                'Ticfs = fs
                'Ticbr = br
                'Ticbites = bites


                br.Close()
                fs.Close()

                If fsize > 7900000 Then
                    ModalFilePop.Hide()

                    lblValidation.Text = "File is too large."
                    lblValidation.Font.Size = FontUnit.Large
                    lblValidation.BackColor() = Color.Red
                    lblValidation.ForeColor = Color.White
                    lblValidation.BorderStyle() = BorderStyle.Solid
                    lblValidation.BorderWidth = Unit.Pixel(2)

                    Exit Sub
                End If

                btnFilComplete.Enabled = True

                'ModalFilePop.Hide()

                'DisplayFile()  View File


                Using SqlConnection As New SqlConnection(Session("CSCConn"))

                    Dim Cmd As New SqlCommand()
                    Cmd.CommandText = "InsRequestFiles"

                    Cmd.Parameters.Add("@request_num", SqlDbType.NVarChar).Value = requestnumlabel.Text

                    Cmd.Parameters.Add("@FileType", SqlDbType.NVarChar).Value = filetype
                    Cmd.Parameters.Add("@FileName", SqlDbType.NVarChar).Value = fileName

                    Cmd.Parameters.Add("@FileContent", SqlDbType.VarBinary).Value = bites

                    Cmd.CommandType = CommandType.StoredProcedure
                    Cmd.Connection = SqlConnection

                    SqlConnection.Open()
                    Cmd.ExecuteNonQuery()

                    'requestImage.ResolveUrl = bitmap
                    SqlConnection.Close()
                    SqlConnection.Dispose()



                End Using


                File.Delete(txtFilename.Text)



                'imageReq.DataImageUrlField = savePath
                'requestImg2.Src = savePath

                'requestImage.ImageUrl = savePath


                'File.Delete(savePath)

                Label1.Text = "File uploaded!"
                Label1.Font.Size = FontUnit.Medium
                Label1.BackColor() = Color.White
                Label1.ForeColor = Color.Black
                Label1.BorderStyle() = BorderStyle.NotSet
                Label1.BorderWidth = Unit.Pixel(0)


            Catch ex As Exception
                Label1.Text = "File can not be uploaded."
                Label1.Font.Size = FontUnit.Large
                Label1.BackColor() = Color.Red
                Label1.ForeColor = Color.White
                Label1.BorderStyle() = BorderStyle.Solid
                Label1.BorderWidth = Unit.Pixel(2)

            End Try
        Else
            Label1.Visible = True
            Label1.Text = "Cannot accept files of this type or File does not exists or File not Loaded"
            Label1.Font.Size = FontUnit.Large
            Label1.BackColor() = Color.Red
            Label1.ForeColor = Color.White
            Label1.BorderStyle() = BorderStyle.Solid
            Label1.BorderWidth = Unit.Pixel(2)

        End If


    End Sub
    Protected Sub btnFilComplete_Click(sender As Object, e As System.EventArgs) Handles btnFilComplete.Click
        'SubmitFile()

        ModalFilePop.Hide()
        ModalCompletionPop.Show()


    End Sub

    Protected Sub btnCancelfile_Click(sender As Object, e As System.EventArgs) Handles btnCancelfile.Click
        ModalFilePop.Hide()
        ModalCompletionPop.Show()

    End Sub

End Class

