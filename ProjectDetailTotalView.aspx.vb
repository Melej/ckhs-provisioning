﻿Imports System.IO
Imports System.Data '
Imports System.Data.SqlClient
Imports App_Code
Imports AjaxControlToolkit

Partial Class ProjectDetailTotalView
	Inherits System.Web.UI.Page
	Dim iProjectNum As Integer
	'	Dim FormSignOn As New FormSQL()
	Dim iClientNum As Integer

	Dim iUserNum As Integer
	Dim iUserSecurityLevel As Integer
	Dim dtSelected As DataTable

	Dim UserInfo As UserSecurity
	Dim dst As DataSet
	Dim dt As DataTable
	Dim totaldt As DataTable


	Dim ProjectNumS As String
	Dim thisdata As CommandsSqlAndOleDb


	Private Sub ProjectDetailTotalView_Load(sender As Object, e As EventArgs) Handles Me.Load

		UserInfo = New UserSecurity(Session("LoginID"), Session("EmployeeConn"))

		iUserNum = UserInfo.ClientNum

		HDClientnum.Text = UserInfo.ClientNum
		HDRptClientnum.Text = UserInfo.ClientNum
		HDTechnum.Text = UserInfo.TechNum

		iClientNum = Request.QueryString("ClientNum")
		iProjectNum = Request.QueryString("ProjectNum")


		iUserSecurityLevel = UserInfo.SecurityLevelNum

		If iUserSecurityLevel = 66 Then
			iUserSecurityLevel = 80
		End If

		If iUserSecurityLevel < 79 Then

			Response.Redirect("~/ProjectHoursMaintenance.aspx", True)

		End If

		If Not IsPostBack Then


			LoadDataTable()


			Dim c3Controller = New Client3Controller(iClientNum, "no", Session("EmployeeConn"))

			empName.Text = c3Controller.FirstName & " " & c3Controller.LastName
			siemensempnumLabel.Text = c3Controller.SiemensEmpNum
			'ntloginTextBox.Text = c3Controller.LoginName
			'userpositiondescTextBox.Text = c3Controller.UserPositionDesc
			'phoneTextBox.Text = c3Controller.Phone
			'e_mailTextBox.Text = c3Controller.EMail
			DepartmentName.Text = c3Controller.EntityName & " - " & c3Controller.DepartmentName
			'DepartmentCd.text = c3Controller.DepartmentCd

			EntityCd.Text = c3Controller.EntityCd & " " & c3Controller.DepartmentCd

		End If

	End Sub

	Private Sub btnReturn_Click(sender As Object, e As EventArgs) Handles btnReturn.Click
		Response.Redirect("~/ProjectHoursMaintenance.aspx", True)

	End Sub
	Private Sub LoadDataTable()
		Dim thisData As New CommandsSqlAndOleDb("SelProjectTotalsByProj", Session("CSCConn"))
		thisData.AddSqlProcParameter("@ProjectNum", iProjectNum, SqlDbType.Int, 8)
		thisData.AddSqlProcParameter("@client_num", iClientNum, SqlDbType.Int, 8)
		thisData.AddSqlProcParameter("@ReportType", "projclient", SqlDbType.NVarChar, 12)

		dt = thisData.GetSqlDataTable
		dst = thisData.GetSqlDataset()

		gvprojectTotals.DataSource = thisData.GetSqlDataTable
		gvprojectTotals.DataBind()

	End Sub
	Private Sub BtnExcel_Click(sender As Object, e As EventArgs) Handles BtnExcel.Click

		LoadDataTable()

		CreateExcel2(dt)

	End Sub
	Sub CreateExcel2(ByVal ReportTable As DataTable)



		Response.AddHeader("content-disposition", "attachment;filename=FileName.xls")

		Response.Charset = String.Empty
		Response.ContentType = "application/vnd.ms-excel"
		'  Response.ContentType = "application/vnd.xls"

		Dim sw As System.IO.StringWriter = New System.IO.StringWriter()

		Dim hw As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(sw)
		Dim dg As New DataGrid

		' commented try new style of excel 2017
		'===============================Create form to contain grid
		'Dim frm As New HtmlForm()

		'GridHidden.Visible = True

		'GridHidden.Columns.RemoveAt(0)
		'GridHidden.Parent.Controls.Add(frm)
		'frm.Attributes("RunAt") = "server"
		'frm.Controls.Add(GridHidden)

		'frm.RenderControl(hw)
		'new style from open provider page

		' must be name of datasource on page
		dg.DataSource = ReportTable
		Dim i As Integer = dg.Items.Count
		dg.DataBind()
		'  dg.GridLines = GridLines.None
		' ClearControls(dg)

		dg.HeaderStyle.Font.Bold = True
		dg.HeaderStyle.BackColor = Drawing.Color.LightGray


		dg.RenderControl(hw)


		Response.Write(sw.ToString())

		Response.End()

		'If iUserSecurityLevel = 65 Then

		'	gvEmployeeProject.Visible = True
		'	gvprojectTotals.Visible = False
		'End If

		'If iUserSecurityLevel > 70 And iUserSecurityLevel < 79 Then

		'	gvEmployeeProject.Visible = True
		'	gvprojectTotals.Visible = False

		'End If

		'If iUserSecurityLevel > 79 Then
		'	If ReportTypeddl.SelectedValue = "projall" Then

		'		gvEmployeeProject.Visible = True
		'		gvprojectTotals.Visible = False


		'	End If
		'	If ReportTypeddl.SelectedValue = "all" Or ReportTypeddl.SelectedValue = "projTotal" Or ReportTypeddl.SelectedValue = "projclient" Or ReportTypeddl.SelectedValue = "Allclient" Then

		'		gvEmployeeProject.Visible = False
		'		gvprojectTotals.Visible = True

		'	End If



		'End If


	End Sub
End Class
