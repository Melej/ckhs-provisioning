﻿Imports System.IO
Imports System.Data '
Imports System.Data.SqlClient
Imports App_Code
Imports AjaxControlToolkit
Partial Class ProjectMaintenance
    Inherits System.Web.UI.Page
    Dim iProjectNum As Integer
    Dim FormSignOn As New FormSQL()
    'Dim VendorFormSignOn As FormSQL
    'Dim UpdPhyFormSignOn As FormSQL
    Dim iClientNum As Integer
    Dim iUserNum As Integer
    Dim iUserSecurityLevel As Integer
    Dim dtSelected As DataTable

    Dim UserInfo As UserSecurity

    Dim ProjectNumS As String
    Dim thisdata As CommandsSqlAndOleDb

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        UserInfo = New UserSecurity(Session("LoginID"), Session("EmployeeConn"))

        iUserNum = UserInfo.ClientNum
        iUserSecurityLevel = UserInfo.SecurityLevelNum
        'userDepartmentCD.Text = UserInfo.DepartmentCd

        Select Case iUserSecurityLevel
            Case (81), (80), (90), (91), (92), (93), (94), (100), (105)

            Case Else
                Response.Redirect("~/SimpleSearch.aspx", True)

        End Select

        Dim ProjectContr As New ProjectController()
        FormSignOn = New FormSQL(Session("CSCConn"), "SelProjectDesc", "InsUpProject", "", Page)
        FormSignOn.AddInsertParm("@ProjectDesc", ProjectDesctxt.Text, SqlDbType.NVarChar, 275)


        If Not IsPostBack Then

            'If GNumber <> "" Then

            '    tabs.ActiveTabIndex = 1

            'End If

            Dim ddlBinder As New DropDownListBinder(Technicianddl, "SelTechniciansV8", Session("EmployeeConn"))
            ddlBinder.AddDDLCriteria("@techType", "project", SqlDbType.NVarChar)

            ddlBinder.BindData("client_num", "tech_name")

            tabs.ActiveTabIndex = 0

            bindProjects()

        End If

        ProjectNameTextBox.BackColor() = Color.Yellow
        ProjectNameTextBox.BorderColor() = Color.Black
        ProjectNameTextBox.BorderStyle() = BorderStyle.Solid
        ProjectNameTextBox.BorderWidth = Unit.Pixel(2)


        Technicianddl.BackColor() = Color.Yellow
        Technicianddl.BorderColor() = Color.Black
        Technicianddl.BorderStyle() = BorderStyle.Solid
        Technicianddl.BorderWidth = Unit.Pixel(2)


        PlannedDateTextBox.BackColor() = Color.Yellow
        PlannedDateTextBox.BorderColor() = Color.Black
        PlannedDateTextBox.BorderStyle() = BorderStyle.Solid
        PlannedDateTextBox.BorderWidth = Unit.Pixel(2)

		ProjectTyperbl.BackColor() = Color.Yellow
		ProjectTyperbl.BorderColor() = Color.Black
		ProjectTyperbl.BorderStyle() = BorderStyle.Solid
		ProjectTyperbl.BorderWidth = Unit.Pixel(2)


	End Sub
    Protected Sub bindProjects()
        Dim thisData As New CommandsSqlAndOleDb("SelProjectDesc", Session("CSCConn"))

		thisData.AddSqlProcParameter("@projectType", ProjectTypeListrbl.SelectedValue, SqlDbType.NVarChar)

		Dim projectdt As DataTable

        projectdt = thisData.GetSqlDataTable

        GvProjects.DataSource = projectdt
        GvProjects.DataBind()

        Session("Projectstbl") = projectdt


        GvProjects.SelectedIndex = -1


    End Sub

    Protected Sub BtnAddProject_Click(sender As Object, e As System.EventArgs) Handles BtnAddProject.Click


        If ProjectNameTextBox.Text = "" Then
            BtnAddProject.BackColor = Color.FromName("#800000")

            lblAddprojectmsg.Text = " Must supply Group Name"
            lblAddprojectmsg.Visible = True
            ProjectNameTextBox.Focus()

            ProjectNameTextBox.BackColor() = Color.Yellow
            ProjectNameTextBox.BorderColor() = Color.Black
            ProjectNameTextBox.BorderStyle() = BorderStyle.Solid
            ProjectNameTextBox.BorderWidth = Unit.Pixel(2)

            Exit Sub
        End If


        If client_numLabel.Text = "" Then
            BtnAddProject.BackColor = Color.FromName("#800000")

            lblAddprojectmsg.Text = " Must supply Owner "
            lblAddprojectmsg.Visible = True
            Technicianddl.Focus()

            Technicianddl.BackColor() = Color.Yellow
            Technicianddl.BorderColor() = Color.Black
            Technicianddl.BorderStyle() = BorderStyle.Solid
            Technicianddl.BorderWidth = Unit.Pixel(2)

            Exit Sub
        Else
            iClientNum = client_numLabel.Text

        End If


        If PlannedDateTextBox.Text = "" Then
            BtnAddProject.BackColor = Color.FromName("#800000")

            lblAddprojectmsg.Text = " Must supply Planned Live Date "
            lblAddprojectmsg.Visible = True
            PlannedDateTextBox.Focus()

            PlannedDateTextBox.BackColor() = Color.Yellow
            PlannedDateTextBox.BorderColor() = Color.Black
            PlannedDateTextBox.BorderStyle() = BorderStyle.Solid
            PlannedDateTextBox.BorderWidth = Unit.Pixel(2)

            Exit Sub
        End If

		If ProjectTyperbl.SelectedValue = "" Then
			BtnAddProject.BackColor = Color.FromName("#800000")

			lblAddprojectmsg.Text = " Must supply Project Type "
			lblAddprojectmsg.Visible = True
			ProjectTyperbl.Focus()

			ProjectTyperbl.BackColor() = Color.Yellow
			ProjectTyperbl.BorderColor() = Color.Black
			ProjectTyperbl.BorderStyle() = BorderStyle.Solid
			ProjectTyperbl.BorderWidth = Unit.Pixel(2)

			Exit Sub
		End If
		' FormSignOn.AddInsertParm("@group_Num", GroupNum, SqlDbType.Int, 9)

		FormSignOn.UpdateForm()

        Response.Redirect("./ProjectMaintenance.aspx", True)
    End Sub

    Protected Sub Technicianddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles Technicianddl.SelectedIndexChanged
        If Technicianddl.SelectedIndex > 0 Then

            OwnerNameLabel.Text = Technicianddl.SelectedItem.ToString
            client_numLabel.Text = Technicianddl.SelectedValue
            iClientNum = Technicianddl.SelectedValue
        End If

    End Sub

    Protected Sub GvProjects_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GvProjects.RowCommand
        If e.CommandName = "Select" Then
            GvProjects.SelectedIndex = -1
            ProjectNumS = GvProjects.DataKeys(Convert.ToInt32(e.CommandArgument))("ProjectNum").ToString()
            ProjectNumTextBox.Text = ProjectNumS
            tabLabel.Text = "Project"
            Projectpan.Enabled = True

            'Response.Redirect("ProjectMaintDetail.aspx?&ProjectNum=" & ProjectNumTextBox.Text, True)


            Dim GetProJect As New ProjectController()
            GetProJect.GetProjectByNum(ProjectNumS, Session("CSCConn"))

            ProjectDesctxt.Text = GetProJect.ProjectDesc
            ProjectNameTextBox.Text = GetProJect.ProjectName
            ProjectShortNameTextBox.Text = GetProJect.ProjectShortName
            StartDateTextBox.Text = GetProJect.StartDate.ToString
            EnddateTextBox.Text = GetProJect.endDate

            PlannedDateTextBox.Text = GetProJect.plannedDate
            ActualDateTextBox.Text = GetProJect.actualDate

            DepartmentNamelabel.Text = GetProJect.DepartmentName

            DepartmentOwnerTextbox.Text = GetProJect.DepartmentOwner
            departmentClientNumLabel.Text = GetProJect.departmentClientNum

			ProjectTyperbl.SelectedValue = GetProJect.ProjectType

			Dim ddlBinderProj As New DropDownListBinder(DepartmentNameddl, "SelProjectDepartments", Session("CSCConn"))

            ddlBinderProj.BindData("DepartmentName", "DepartmentName")


            If StartDateTextBox.Text = "1/1/0001 12:00:00 AM" Then
                StartDateTextBox.Text = ""
            End If

            If EnddateTextBox.Text = "12:00:00 AM" Then
                EnddateTextBox.Text = ""
            End If

            If PlannedDateTextBox.Text = "12:00:00 AM" Then
                PlannedDateTextBox.Text = ""
            End If

            If ActualDateTextBox.Text = "12:00:00 AM" Then
                ActualDateTextBox.Text = ""
            End If

            iClientNum = GetProJect.ClientNum
            iProjectNum = GetProJect.ProjectNum
            ProjectNumTextBox.Text = iProjectNum
            client_numLabel.Text = iClientNum

            If client_numLabel.Text <> "" Then
                Technicianddl.SelectedValue = iClientNum
            End If

            If DepartmentNamelabel.Text <> "" Then
                DepartmentNameddl.SelectedValue = DepartmentNamelabel.Text
            End If


            tabs.ActiveTabIndex = 1

            fillTable()

            gvTicketDetail.Visible = True

            BtnAddProject.Text = "Update"
            BtnExcel2.Visible = True

            'Response.Redirect("./ProjectMaintDetail.aspx?ProjectNum=" & ProjectNumS, True)
        End If
    End Sub

    Protected Sub tabs_ActiveTabChanged(sender As Object, e As System.EventArgs) Handles tabs.ActiveTabChanged
        If tabs.ActiveTabIndex = 0 Then



            GvProjects.SelectedIndex = -1
            tabLabel.Text = "New Project"
            BtnAddProject.Text = "Submit"


        ElseIf tabs.ActiveTabIndex = 1 Then

            ' Response.Redirect("ProjectMaintDetail.aspx", True)

            BtnAddProject.Text = "Submit"
            tabLabel.Text = "New Project"
            BtnExcel2.Visible = False

            '[SelProjectDepartments]

            Dim ddlBinderProj As New DropDownListBinder(DepartmentNameddl, "SelProjectDepartments", Session("CSCConn"))

            ddlBinderProj.BindData("DepartmentName", "DepartmentName")



            ClearFields()

            gvTicketDetail.Visible = False

        End If
    End Sub
    Protected Sub btnDepartmentOwner_Click(sender As Object, e As System.EventArgs) Handles btnDepartmentOwner.Click

        gvSearch.DataSource = dtSelected
        gvSearch.DataBind()


        gvSearch.SelectedIndex = -1
        gvSearch.Dispose()

        LastNameTextBox.Text = ""
        FirstNameTextBox.Text = ""

        mpeProjecowner.Show()

    End Sub
    Protected Sub gvSearch_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvSearch.RowCommand
        If e.CommandName = "Select" Then
            Dim client_num As String = gvSearch.DataKeys(Convert.ToInt32(e.CommandArgument))("client_num").ToString()
            Dim login_name As String = gvSearch.DataKeys(Convert.ToInt32(e.CommandArgument))("login_name").ToString()
            Dim dbAccess As New CkhsEmployeeDbAccess(Session("EmployeeConn"))
            Dim ckhsEmployee = dbAccess.GetCkhsEmployee(client_num)


            'c3Controller = New Client3Controller(client_num)
            'c3Submitter = c3Controller.GetClientByClientNum(client_num)


            'requestor_client_numLabel.Text = client_num
            'btnUpdateSubmitter.Visible = True


            DepartmentOwnerTextbox.Text = ckhsEmployee.FullName & " (" & ckhsEmployee.EMail & ")"

            btnDepartmentOwner.Text = "Change Dept. Owner"

            DepartmentOwnerTextbox.Enabled = False

            gvSearch.SelectedIndex = -1
            gvSearch.Dispose()

            mpeProjecowner.Hide()


            'Select Case userDepartmentCD.Text
            '    Case "832600", "832700", "823100"
            '        If Session("SecurityLevelNum") < 90 And requestor_client_numLabel.Text = submitter_client_numLabel.Text Then
            '            btnSelectOnBehalfOf.Visible = True
            '            lblValidation.Text = "Must Change Requestor"
            '            lblValidation.Focus()
            '        ElseIf Session("SecurityLevelNum") < 90 And requestor_client_numLabel.Text <> submitter_client_numLabel.Text Then
            '            lblValidation.Text = ""
            '            btnSelectOnBehalfOf.BackColor() = Color.FromKnownColor("336666")
            '            btnSelectOnBehalfOf.ForeColor() = Color.White
            '            btnSelectOnBehalfOf.BorderStyle() = BorderStyle.Groove
            '            btnSelectOnBehalfOf.BorderWidth = Unit.Pixel(1)



            '        End If


            'End Select

        End If

    End Sub
    Public Sub ClearFields()

        ProjectNumTextBox.Text = ""
        ProjectNameTextBox.Text = ""
        client_numLabel.Text = ""
        OwnerNameLabel.Text = ""

        ProjectShortNameTextBox.Text = ""
        ProjectDesctxt.Text = ""
        StartDateTextBox.Text = ""

        PlannedDateTextBox.Text = ""
        ActualDateTextBox.Text = ""

        DepartmentNamelabel.Text = ""

        DepartmentOwnerTextbox.Text = ""
        departmentClientNumLabel.Text = ""


        Dim ddlBinder As New DropDownListBinder(Technicianddl, "SelTechniciansV8", Session("EmployeeConn"))
        ddlBinder.AddDDLCriteria("@techType", "project", SqlDbType.NVarChar)

        ddlBinder.BindData("client_num", "tech_name")


        Dim ddlBinderProj As New DropDownListBinder(DepartmentNameddl, "SelProjectDepartments", Session("CSCConn"))

        ddlBinderProj.BindData("DepartmentName", "DepartmentName")



    End Sub
    Protected Sub fillTable()

        Dim thisData As New CommandsSqlAndOleDb("SelRequestsByproject", Session("CSCConn"))

        thisData.AddSqlProcParameter("@projectNum", ProjectNumTextBox.Text, SqlDbType.NVarChar)

        Dim iRowCount = 1
        Dim iTotalRowcount = 0

        Dim dt As DataTable
        dt = thisData.GetSqlDataTable

        gvTicketDetail.DataSource = thisData.GetSqlDataTable

        gvTicketDetail.DataBind()

        Session("projectRequeststbl") = dt


    End Sub



    Protected Sub btnExcel_Click(sender As Object, e As System.EventArgs) Handles btnExcel.Click

        Response.Redirect("./Reports/RolesReport.aspx?ReportName=ProjectReport", True)

    End Sub
    Protected Sub BtnExcel2_Click(sender As Object, e As System.EventArgs) Handles BtnExcel2.Click
        Response.Redirect("./Reports/RolesReport.aspx?ReportName=RequestbyProjectReport" & "&ProjectNum=" & ProjectNumTextBox.Text, True)
    End Sub

    

    Protected Sub ProjectTyperbl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ProjectTyperbl.SelectedIndexChanged
		' bindProjects()

	End Sub

    Private Function GetSortDirection(ByVal column As String) As String

        ' By default, set the sort direction to ascending.
        Dim sortDirection = "ASC"

        ' Retrieve the last column that was sorted.
        Dim sortExpression = TryCast(ViewState("SortExpression"), String)

        If sortExpression IsNot Nothing Then
            ' Check if the same column is being sorted.
            ' Otherwise, the default value can be returned.
            If sortExpression = column Then
                Dim lastDirection = TryCast(ViewState("SortDirection"), String)
                If lastDirection IsNot Nothing _
                  AndAlso lastDirection = "ASC" Then

                    sortDirection = "DESC"

                End If
            End If
        End If

        ' Save new values in ViewState.
        ViewState("SortDirection") = sortDirection
        ViewState("SortExpression") = column

        Return sortDirection

    End Function

    Protected Sub GvProjects_Sorting(sender As Object, e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles GvProjects.Sorting
        Dim sortdt = TryCast(Session("Projectstbl"), DataTable)

        If sortdt IsNot Nothing Then

            'Sort the data.
            sortdt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression)
            GvProjects.DataSource = sortdt
            GvProjects.DataBind()

        End If

    End Sub

    Protected Sub gvTicketDetail_Sorting(sender As Object, e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvTicketDetail.Sorting
        Dim sortdt = TryCast(Session("projectRequeststbl"), DataTable)

        If sortdt IsNot Nothing Then

            'Sort the data.
            sortdt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression)
            gvTicketDetail.DataSource = sortdt
            gvTicketDetail.DataBind()

        End If

    End Sub
    Protected Sub btnSearch_Click(sender As Object, e As System.EventArgs) Handles btnSearch.Click
        Dim thisData As New CommandsSqlAndOleDb("SelActiveCKHSEmployees", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@FirstName", FirstNameTextBox.Text, SqlDbType.NVarChar, 20)
        thisData.AddSqlProcParameter("@LastName", LastNameTextBox.Text, SqlDbType.NVarChar, 20)

        gvSearch.DataSource = thisData.GetSqlDataTable
        gvSearch.DataBind()

        'uplDepartmentOwner.Update()
        mpeProjecowner.Show()

    End Sub
    'Public Function CheckRequiredFields() As Boolean
    '    Dim removeCss As New PageControls(Page)
    '    removeCss.RemoveCssClass(Page, "textInputRequired")

    '    Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
    '    Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")

    '    emp_type_cdLabel.Text = emp_type_cdrbl.SelectedValue


    '    Dim phoneReq As New RequiredField(phoneTextBox, "Phone")

    '    Dim Location As New RequiredField(facility_cdddl, "Location")
    '    Dim building As New RequiredField(building_cdddl, "building")

    '    facility_cdddl.BackColor() = Color.Yellow
    '    facility_cdddl.BorderColor() = Color.Black
    '    facility_cdddl.BorderStyle() = BorderStyle.Solid
    '    facility_cdddl.BorderWidth = Unit.Pixel(2)

    '    building_cdddl.BackColor() = Color.Yellow
    '    building_cdddl.BorderColor() = Color.Black
    '    building_cdddl.BorderStyle() = BorderStyle.Solid
    '    building_cdddl.BorderWidth = Unit.Pixel(2)
    'End Function

    Protected Sub DepartmentNameddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles DepartmentNameddl.SelectedIndexChanged
        DepartmentNamelabel.Text = DepartmentNameddl.SelectedValue
    End Sub

	Private Sub ProjectTypeListrbl_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ProjectTypeListrbl.SelectedIndexChanged
		bindProjects()
	End Sub
End Class
