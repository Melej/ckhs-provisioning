﻿Imports System.IO
Imports System.Data '
Imports System.Data.SqlClient
Imports App_Code
Imports AjaxControlToolkit
Imports C1.Web.UI.Controls.C1Window
Partial Class Search
    Inherits MyBasePage '   Inherits System.Web.UI.Page
    Dim conn As New SetDBConnection()
    Dim dst As DataSet
    Dim dt As DataTable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim tp As New ToolTipHelper()


        lblValidation.Attributes.Add("class", "masterTooltip")
        lblValidation.Attributes.Add("title", tp.ToolTipHtml())

        Dim ddlBinder As New DropDownListBinder

        If Not IsPostBack Then

            'ddlBinder.BindData(Departmentddl, "sel_department_codes", "department_cd", "department_name", Session("EmployeeConn"))

        End If
        If Session("SecurityLevelNum") < 25 Then
            Response.Redirect("~/SecurityV2.aspx")
        End If

        If Session("SecurityLevelNum") < 39 Then
            Response.Redirect("~/MyRequests.aspx")

        End If

        'If Session("SecurityLevelNum") < 60 Then
        '    sTypelbl.Visible = False
        '    activitylbl.Visible = False
        '    NtLoginTextBox.Visible = False
        '    requestlbl.Visible = False
        '    RequestNumberTextBox.Visible = False
        '    rblSystem.Visible = False
        '    'Dim ctrl As Control = DataCheckingAndValidation.SetChildControlsRecursiveToEnabledFalse(tpDemographics, False)

        'End If
        If Session("SecurityLevelNum") > 39 And Session("SecurityLevelNum") < 60 Then
            sTypelbl.Visible = False
            activitylbl.Visible = False
            NtLoginTextBox.Visible = False
            requestlbl.Visible = False
            RequestNumberTextBox.Visible = False
            rblSystem.Visible = False
            emplbl.Visible = False
            EmpnumTextBox.Visible = False

            'Dim ctrl As Control = DataCheckingAndValidation.SetChildControlsRecursiveToEnabledFalse(tpDemographics, False)
        ElseIf Session("SecurityLevelNum") > 59 And Session("SecurityLevelNum") < 70 Then
            rblSystem.Items.Remove(rblSystem.Items.FindByValue("deactivated"))




        End If


    End Sub
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        lblValidation.Text = "Test"
        gvSearch.Visible = True
        gvSearch.SelectedIndex = -1

        gvRequests.Visible = True
        gvRequests.SelectedIndex = -1

        gvSecureView.Visible = True
        gvSecureView.SelectedIndex = -1

        GVEmpNum.Visible = True
        GVEmpNum.SelectedIndex = -1

        lblValidation.Visible = False


        If rblSystem.SelectedValue = "clients" Or rblSystem.SelectedValue = "deactivated" Then
            If LastNameTextBox.Text.Length = 0 And FirstNameTextBox.Text.Length = 0 And NtLoginTextBox.Text.Length = 0 And EmpnumTextBox.Text.Length = 0 Then

                lblValidation.Text = "Must supply Last Name or AD Name or Emp Number"
                lblValidation.Visible = True
                Reset()

                gvSearch.Visible = False
                gvRequests.Visible = False
                gvSecureView.Visible = False
            End If
        End If


        If rblSystem.SelectedValue = "clients" Or rblSystem.SelectedValue = "deactivated" Then

            ' conn.EmployeeConn
            Dim thisData As New CommandsSqlAndOleDb("SelSimpleSearchV20", Session("EmployeeConn"))
                thisData.AddSqlProcParameter("@FirstName", FirstNameTextBox.Text, SqlDbType.NVarChar, 20)
                thisData.AddSqlProcParameter("@LastName", LastNameTextBox.Text, SqlDbType.NVarChar, 20)
                thisData.AddSqlProcParameter("@System", rblSystem.SelectedValue, SqlDbType.NVarChar, 12)
                thisData.AddSqlProcParameter("@NtLogin", NtLoginTextBox.Text, SqlDbType.NVarChar, 20)
                thisData.AddSqlProcParameter("@Empnumber", EmpnumTextBox.Text, SqlDbType.NVarChar, 16)

                ' clear out other fields
                RequestNumberTextBox.Text = Nothing

                dt = thisData.GetSqlDataTable
                dst = thisData.GetSqlDataset()

                Session("SStable") = dt


                If Session("SecurityLevelNum") > 39 And Session("SecurityLevelNum") < 60 Then
                    gvSecureView.DataSource = thisData.GetSqlDataTable
                    gvSecureView.DataBind()

                    gvSearch.Visible = False
                    gvRequests.Visible = False
                    GVEmpNum.Visible = False

                ElseIf Session("SecurityLevelNum") > 59 Then

                    If EmpnumTextBox.Text <> "" Then

                        GVEmpNum.DataSource = thisData.GetSqlDataTable
                        GVEmpNum.DataBind()

                        gvRequests.Visible = False
                        gvSecureView.Visible = False


                    Else

                        gvSearch.DataSource = thisData.GetSqlDataTable
                        gvSearch.DataBind()

                        gvRequests.Visible = False
                        gvSecureView.Visible = False
                        GVEmpNum.Visible = False

                    End If




                End If

        End If

        If rblSystem.SelectedValue = "requests" Then

            If (Regex.IsMatch(RequestNumberTextBox.Text, "^[0-9 ]") = False And RequestNumberTextBox.Text.Length > 0) Then

                lblValidation.Text = "Request Number must only contain numbers."
                lblValidation.Visible = True

                Reset()
                gvSearch.Visible = False
                gvRequests.Visible = False
                GVEmpNum.Visible = False

            Else
                Dim thisData As New CommandsSqlAndOleDb("SelSimpleSearchV3", Session("EmployeeConn"))
                thisData.AddSqlProcParameter("@FirstName", FirstNameTextBox.Text, SqlDbType.NVarChar, 20)
                thisData.AddSqlProcParameter("@LastName", LastNameTextBox.Text, SqlDbType.NVarChar, 20)
                thisData.AddSqlProcParameter("@System", rblSystem.SelectedValue, SqlDbType.NVarChar, 20)

                ' clear out other fields
                FirstNameTextBox.Text = Nothing
                LastNameTextBox.Text = Nothing
                NtLoginTextBox.Text = Nothing


                If RequestNumberTextBox.Text.Length > 0 Then

                    thisData.AddSqlProcParameter("@RequestNum", RequestNumberTextBox.Text, SqlDbType.Int, 20)

                End If

                'If Departmentddl.SelectedIndex > 0 Then
                '    thisData.AddSqlProcParameter("@department_cd", Departmentddl.SelectedValue, SqlDbType.NVarChar, 8)

                'End If

                gvRequests.DataSource = thisData.GetSqlDataTable
                gvRequests.DataBind()

                gvSearch.Visible = False
                GVEmpNum.Visible = False


            End If
        End If
    End Sub
    Protected Sub gvSearch_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvSearch.RowCommand
        If e.CommandName = "Select" Then
            Dim client_num As String = gvSearch.DataKeys(Convert.ToInt32(e.CommandArgument))("client_num").ToString()
            If rblSystem.SelectedValue = "clients" Then
                Response.Redirect("~/ClientDemo.aspx?ClientNum=" & client_num, True)

            ElseIf rblSystem.SelectedValue = "deactivated" Then
                Response.Redirect("~/DeactivatedAccount.aspx?ClientNum=" & client_num)

            End If


        End If

    End Sub

    Protected Sub gvRequests_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvRequests.RowCommand
        Dim account_request_num As String

        account_request_num = gvRequests.DataKeys(Convert.ToInt32(e.CommandArgument))("account_request_num").ToString()

        Response.Redirect("~/AccountRequestInfo2.aspx?RequestNum=" & account_request_num, True)

    End Sub
    Protected Sub gvSecureView_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvSecureView.RowCommand
        If e.CommandName = "Select" Then
            Dim client_num As String = gvSecureView.DataKeys(Convert.ToInt32(e.CommandArgument))("client_num").ToString()
            If rblSystem.SelectedValue = "clients" Then
                Response.Redirect("~/ClientDemo.aspx?ClientNum=" & client_num, True)

            ElseIf rblSystem.SelectedValue = "deactivated" Then
                Response.Redirect("~/DeactivatedAccount.aspx?ClientNum=" & client_num)

            End If


        End If

    End Sub
    Private Sub GVEmpNum_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles GVEmpNum.RowCommand
        If e.CommandName = "Select" Then
            Dim client_num As String = GVEmpNum.DataKeys(Convert.ToInt32(e.CommandArgument))("client_num").ToString()
            If rblSystem.SelectedValue = "clients" Then
                Response.Redirect("~/ClientDemo.aspx?ClientNum=" & client_num, True)

            ElseIf rblSystem.SelectedValue = "deactivated" Then
                Response.Redirect("~/DeactivatedAccount.aspx?ClientNum=" & client_num)

            End If


        End If
    End Sub

    Protected Sub gvSearch_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvSearch.RowDataBound

        If (e.Row.RowType = DataControlRowType.DataRow) Then  ' Check to make sure it isn't not getting header or footer

            '   Dim drv As DataRowView = CType(e.Row.Cells, DataRowView)

            Dim ClientNum As String = gvSearch.DataKeys(e.Row.RowIndex)("client_num")
            Dim lblUserName As Label

            lblUserName = e.Row.FindControl("lblLoginName")
            Dim accts As New ClientAccounts(ClientNum)
            If accts.AccountsByAcctNum(9).Count > 1 Then
                Dim sAccts As String = ""

                For Each acct As ClientAccountAccess In accts.AccountsByAcctNum(9)

                    sAccts = sAccts + acct.LoginName + "<br>"


                Next

                lblUserName.Attributes.Add("class", "masterTooltip")
                lblUserName.Attributes.Add("title", sAccts)
                lblUserName.ForeColor = Color.Blue
            End If

            'Dim btnAssign As Button
            'Dim btnAccept As Button

            'btnAssign = e.Row.FindControl("btnLinkView")
            'btnAccept = e.Row.FindControl("btnAcceptCase")


            'If diseaseID <> "" Then

            '    btnAssign.Visible = False

            'Else

            '    btnAssign.Visible = True

            'End If

            'If CaseNtLogin <> Session("ntLogin") And CaseNtLogin <> "" Then

            '    btnAccept.Visible = True

            'ElseIf CaseNtLogin = Session("ntLogin") Or CaseNtLogin = "" Then

            '    btnAccept.Visible = False

            'End If
        End If

    End Sub
    Public Sub Reset()
        ' clear out all fields
        Dim emptytbl As New DataTable

        FirstNameTextBox.Text = Nothing
        LastNameTextBox.Text = Nothing
        NtLoginTextBox.Text = Nothing
        RequestNumberTextBox.Text = Nothing
        EmpnumTextBox.Text = Nothing

        gvSearch.Visible = False
        gvSearch.SelectedIndex = -1

        gvRequests.Visible = False
        gvRequests.SelectedIndex = -1

        gvSecureView.Visible = False
        gvSecureView.SelectedIndex = -1

        GVEmpNum.Visible = False
        GVEmpNum.SelectedIndex = -1

        gvSearch.DataSource = emptytbl
        gvSearch.DataBind()

        gvRequests.DataSource = emptytbl
        gvRequests.DataBind()

        gvSecureView.DataSource = emptytbl
        gvSecureView.DataBind()

        GVEmpNum.DataSource = emptytbl
        GVEmpNum.DataBind()

        rblSystem.SelectedValue = "clients"

    End Sub
    Protected Sub btnReset_Click(sender As Object, e As System.EventArgs) Handles btnReset.Click
        Reset()
    End Sub
    Protected Sub gvSearch_Sorting(sender As Object, e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvSearch.Sorting
        'Retrieve the table from the session object.
        Dim sortdt = TryCast(Session("SStable"), DataTable)

        If sortdt IsNot Nothing Then

            'Sort the data.
            sortdt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression)
            gvSearch.DataSource = sortdt
            gvSearch.DataBind()

        End If
    End Sub
    Private Sub GVEmpNum_Sorting(sender As Object, e As GridViewSortEventArgs) Handles GVEmpNum.Sorting
        'Retrieve the table from the session object.
        Dim sortdt = TryCast(Session("SStable"), DataTable)

        If sortdt IsNot Nothing Then

            'Sort the data.
            sortdt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression)
            GVEmpNum.DataSource = sortdt
            GVEmpNum.DataBind()

        End If
    End Sub

    Private Function GetSortDirection(ByVal column As String) As String

        ' By default, set the sort direction to ascending.
        Dim sortDirection = "ASC"

        ' Retrieve the last column that was sorted.
        Dim sortExpression = TryCast(ViewState("SortExpression"), String)

        If sortExpression IsNot Nothing Then
            ' Check if the same column is being sorted.
            ' Otherwise, the default value can be returned.
            If sortExpression = column Then
                Dim lastDirection = TryCast(ViewState("SortDirection"), String)
                If lastDirection IsNot Nothing _
                  AndAlso lastDirection = "ASC" Then

                    sortDirection = "DESC"

                End If
            End If
        End If

        ' Save new values in ViewState.
        ViewState("SortDirection") = sortDirection
        ViewState("SortExpression") = column

        Return sortDirection

    End Function


End Class
