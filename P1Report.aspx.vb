﻿Imports System.IO
'Imports System.Web.UI.DataVisualization
'Imports System.Web.UI.DataVisualization.Charting
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.Control
Imports iTextSharp.text.pdf
Imports App_Code
Imports System.Collections.Generic
'Imports C1.Web.UI.Controls
'Imports C1.Web.Wijmo.Controls
Partial Class Reports_P1Report
    Inherits MyBasePage 'Inherits System.Web.UI.Page
    '=============Charting===================
    Dim dst As DataSet
    Dim dstOut As DataSet
    Dim dstPrintRows As DataSet

    Dim dvGrpByDate As DataView
    Dim dvGrpByType As DataView
    Dim dtGrpByDate As New DataTable
    Dim dtGrpByType As New DataTable
    Dim dtnew As New DataTable

    Dim CurrentDate As String
    Dim CurrentType As String

    Dim ReportTotalContrl As New P1ReportController()
    Dim ReportTotalList As New List(Of P1ReportViewer)
    Dim sentryyear As String
    'Dim sDisplayField As String
    'Dim PointDataField As String = ""
    'Dim PointTypeField As String = ""
    'Dim PointCountField As String = ""
    'Dim ChartSortField As String = ""
    'Dim alSeriesList As ArrayList = New ArrayList()

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            LoadDates()
            'Datesddl.SelectedValue = 0
            Datesddl.SelectedValue = 1
            CurrentDate = Datesddl.SelectedValue
            CurrentType = Typeddl.SelectedValue


            ReportTotalContrl.GetP1Totals(CurrentType, CurrentDate)

            ReportTotalList = ReportTotalContrl.getP1ByDate()
            FillDataSet()


            lblChartTitle.Text = "Total for this Month " ' & ReportTotalList.Item(6).MonthlyTotal.ToString

            grdTotal.AutoGenerateColumns = False
            grdTotal.DataSource = ReportTotalList
            grdTotal.DataBind()

            'Chart1.Series("ChartSeries").XValueMember = "TotalCount"
            'Chart1.Series("ChartSeries").YValueMembers = "DisplayField"
            HDtype.Text = Typeddl.SelectedValue.ToLower

            Select Case Typeddl.SelectedValue.ToLower
                Case "group"
                    grdgroup.Visible = True

                    grdTotal.Visible = False
                    grdMonth.Visible = False
                    grdHour.Visible = False
                    grdDay.Visible = False

                    grdgroup.AutoGenerateColumns = False
                    grdgroup.DataSource = ReportTotalList
                    grdgroup.DataBind()



                Case "year"
                    grdTotal.Visible = True

                    grdgroup.Visible = False
                    grdMonth.Visible = False
                    grdHour.Visible = False
                    grdDay.Visible = False

                    grdTotal.AutoGenerateColumns = False
                    grdTotal.DataSource = ReportTotalList
                    grdTotal.DataBind()


                Case "month"
                    grdMonth.Visible = True

                    grdTotal.Visible = False
                    grdgroup.Visible = False
                    grdHour.Visible = False
                    grdDay.Visible = False

                    grdMonth.AutoGenerateColumns = False
                    grdMonth.DataSource = ReportTotalList
                    grdMonth.DataBind()


                Case "day"
                    grdDay.Visible = True

                    grdMonth.Visible = False
                    grdTotal.Visible = False
                    grdgroup.Visible = False
                    grdHour.Visible = False

                    grdDay.AutoGenerateColumns = False
                    grdDay.DataSource = ReportTotalList
                    grdDay.DataBind()

                Case "hour"
                    grdHour.Visible = True

                    grdDay.Visible = False
                    grdMonth.Visible = False
                    grdTotal.Visible = False
                    grdgroup.Visible = False

                    grdHour.AutoGenerateColumns = False
                    grdHour.DataSource = ReportTotalList
                    grdHour.DataBind()
                Case Else
                    grdTotal.Visible = True

                    grdgroup.Visible = False
                    grdMonth.Visible = False
                    grdHour.Visible = False
                    grdDay.Visible = False

                    grdTotal.AutoGenerateColumns = False
                    grdTotal.DataSource = ReportTotalList
                    grdTotal.DataBind()


            End Select
        End If

        If HDtype.Text.ToLower = "year" Then

            'PointDataField = "entryyear" '
            'PointTypeField = "none" 'Series Field
            'PointCountField = "TotalCount" 'Totaling
            'ChartSortField = "entryyear"

            FillDataset()

            'BuildSeries()
            'SetChartType()

            'Chart1.Visible = True
            'Chart1.DataSource = ReportTotalList
            'Chart1.DataBind()
        End If

    End Sub
    Protected Sub Datesddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles Datesddl.SelectedIndexChanged

    End Sub
    Protected Sub LoadDates()
        Dim dt As DataTable
        Dim thisData As New CommandsSqlAndOleDb
        thisData = New CommandsSqlAndOleDb("SelTicketEntryDates", Session("CSCConn"))
        dt = thisData.GetSqlDataTable()
        If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
            Datesddl.DataSource = dt
            Datesddl.DataValueField = "EntryYear"
            Datesddl.DataTextField = "EntryYear"
            Datesddl.DataBind()
            thisData = Nothing
        End If

    End Sub
    Public Sub FillDataset()

        Dim thisData As New CommandsSqlAndOleDb("SelP1Totals", Session("CSCConn"))
        thisData.AddSqlProcParameter("@TotalType", HDtype.Text, SqlDbType.NVarChar, 24)

        thisData.AddSqlProcParameter("@totalyear", HDyear.Text, SqlDbType.NVarChar, 24)


        dst = thisData.GetSqlDataset()
        dtnew = thisData.GetSqlDataTable


        Session("P1table") = dtnew

    End Sub
    'Public Sub SetChartType()
    '    Dim sSeries As SeriesCollection
    '    sSeries = Chart1.Series
    '    For Each s As Series In sSeries
    '        Select Case rblChartType.SelectedValue.ToLower
    '            Case "column"
    '                s.ChartType = SeriesChartType.Column
    '                s("DrawingStyle") = "Cylinder"
    '                s.IsValueShownAsLabel = True
    '                s("PointWidth") = "0.6" ' + (i / 10)
    '                s("BarLabelStyle") = "Center"
    '                'Chart1.Legends(0).InterlacedRows = True
    '                'Chart1.Legends(0).IsTextAutoFit = True

    '            Case "bar"
    '                s.ChartType = SeriesChartType.Bar
    '                s("DrawingStyle") = "Cylinder"
    '                s.IsValueShownAsLabel = True
    '                s("PointWidth") = "0.6" ' + (i / 10)
    '                s("BarLabelStyle") = "Center"
    '            Case "line"
    '                ' s.ChartType = SeriesChartType.Line
    '                s.ChartType = SeriesChartType.Line
    '                s("DrawingStyle") = "Cylinder"
    '                s.IsValueShownAsLabel = True
    '                s("PointWidth") = "0.2" ' + (i / 10)
    '                s("BarLabelStyle") = "Center"
    '            Case "radar"
    '                ' s.ChartType = SeriesChartType.Line
    '                s.ChartType = SeriesChartType.Radar
    '                s("DrawingStyle") = "Cylinder"
    '                s.IsValueShownAsLabel = True
    '                s("PointWidth") = "0.2" ' + (i / 10)
    '                s("BarLabelStyle") = "Center"
    '            Case "pie"
    '                s.ChartType = SeriesChartType.Pie
    '                s("DrawingStyle") = "Cylinder"
    '                s.IsValueShownAsLabel = True 'False '
    '                s("PieLabelStyle") = "Outside" ' "Inside" '"Disabled" ' '
    '                ' s.Points(2).AxisLabel = s.Name
    '                ' s.Points(2).Label = s.Name

    '                s("PointWidth") = "0.2" ' + (i / 10)
    '                s("BarLabelStyle") = "Center"
    '                Chart1.Legends(0).Enabled = True
    '            Case "doughnut"
    '                s.ChartType = SeriesChartType.Doughnut
    '                s("DrawingStyle") = "Cylinder"
    '                s.IsValueShownAsLabel = True
    '                s("PieLabelStyle") = "Inside" '"Disabled" '"Outside" 


    '                s("PointWidth") = "0.2" ' + (i / 10)
    '                s("BarLabelStyle") = "Center"
    '                Chart1.Legends(0).Enabled = True
    '                Chart1.Legends(0).IsTextAutoFit = True
    '                '  Chart1.Legends(0).InterlacedRows = True
    '        End Select
    '    Next
    'End Sub
    Protected Sub btnUpdate_Click(sender As Object, e As System.EventArgs) Handles btnUpdate.Click

        ' try this
        'With Me.Chart1
        '    .Legends.Clear()
        '    .Series.Clear()
        '    .ChartAreas.Clear()
        'End With

        'Dim areas1 As ChartArea = Me.Chart1.ChartAreas.Add("Areas1")

        'With areas1
        'End With

        'Dim series1 As Series = Me.Chart1.Series.Add("ChartSeries")

        'With series1
        '    .ChartArea = areas1.Name
        '    .ChartType = SeriesChartType.Pie
        '    .Points.AddXY("Online", 60)
        '    .Points.AddXY("Offline", 40)
        'End With

        'Dim legends1 As Legend = Me.Chart1.Legends.Add("Legends1")

        Dim ReportTotalList As New List(Of P1ReportViewer)

        CurrentDate = Datesddl.SelectedValue
        CurrentType = Typeddl.SelectedValue

        HDyear.Text = CurrentDate
        HDtype.Text = CurrentType

        ReportTotalContrl.GetP1Totals(HDtype.Text, HDyear.Text)
        ReportTotalList = ReportTotalContrl.getP1ByDate()


        ' gets Total for this query
        lblChartTitle.Text = "Total for this Month " '& ReportTotalList.Item(0).MonthlyTotal.ToString


        FillDataset()


        'Chart1.Series("ChartSeries").XValueMember = "TotalCount"
        'Chart1.Series("ChartSeries").YValueMembers = "DisplayField"

        Select Case Typeddl.SelectedValue.ToLower
            Case "group"
                grdgroup.Visible = True

                grdTotal.Visible = False
                grdMonth.Visible = False
                grdHour.Visible = False
                grdDay.Visible = False

                grdgroup.AutoGenerateColumns = False
                grdgroup.DataSource = ReportTotalList
                grdgroup.DataBind()

                'Chart1.Visible = False


            Case "year"
                grdTotal.Visible = True

                grdgroup.Visible = False
                grdMonth.Visible = False
                grdHour.Visible = False
                grdDay.Visible = False

                grdTotal.AutoGenerateColumns = False
                grdTotal.DataSource = ReportTotalList
                grdTotal.DataBind()


                'Chart1.DataSource = ReportTotalList
                'Chart1.DataBind()
                'PointDataField = "entryyear" '
                'PointTypeField = "none" 'Series Field
                'PointCountField = "TotalCount" 'Totaling
                'ChartSortField = "entryyear"



                'BuildSeries()
                'SetChartType()

                'Chart1.Visible = True


            Case "month"
                grdMonth.Visible = True

                grdTotal.Visible = False
                grdgroup.Visible = False
                grdHour.Visible = False
                grdDay.Visible = False

                grdMonth.AutoGenerateColumns = False
                FillDataset()

                grdMonth.DataSource = ReportTotalList
                grdMonth.DataBind()

                'Chart1.Visible = False

            Case "day"
                grdDay.Visible = True

                grdMonth.Visible = False
                grdTotal.Visible = False
                grdgroup.Visible = False
                grdHour.Visible = False

                grdDay.AutoGenerateColumns = False
                grdDay.DataSource = ReportTotalList
                grdDay.DataBind()

                'Chart1.Visible = False

            Case "hour"
                grdHour.Visible = True

                grdDay.Visible = False
                grdMonth.Visible = False
                grdTotal.Visible = False
                grdgroup.Visible = False

                grdHour.AutoGenerateColumns = False
                grdHour.DataSource = ReportTotalList
                grdHour.DataBind()

                'Chart1.Visible = False

            Case Else
                grdTotal.Visible = True

                grdgroup.Visible = False
                grdMonth.Visible = False
                grdHour.Visible = False
                grdDay.Visible = False

                grdTotal.AutoGenerateColumns = False
                grdTotal.DataSource = ReportTotalList
                grdTotal.DataBind()

                'Chart1.Visible = False


        End Select




    End Sub
    Protected Sub Typeddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles Typeddl.SelectedIndexChanged
        CurrentType = Typeddl.SelectedValue

        HDtype.Text = CurrentType

    End Sub

    Protected Sub grdgroup_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles grdgroup.SelectedIndexChanged

        sentryyear = grdgroup.SelectedRow.Cells(1).Text.ToString
        'sDisplayField = grdgroup.SelectedRow.Cells(3).Text.ToString
        'Response.Redirect("~/P1ReportDetail.aspx?entryyear=" & sentryyear & "&DisplayField=" & sDisplayField & "&Type=" & "group", True)

    End Sub

    Protected Sub grdDay_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles grdDay.SelectedIndexChanged
        sentryyear = grdDay.SelectedRow.Cells(1).Text.ToString
        'sDisplayField = grdDay.SelectedRow.Cells(3).Text.ToString
        'Response.Redirect("~/P1ReportDetail.aspx?entryyear=" & sentryyear & "&DisplayField=" & sDisplayField & "&Type=" & "day", True)

    End Sub

    Protected Sub grdHour_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles grdHour.SelectedIndexChanged
        sentryyear = grdHour.SelectedRow.Cells(1).Text.ToString
        'sDisplayField = grdHour.SelectedRow.Cells(3).Text.ToString
        'Response.Redirect("~/P1ReportDetail.aspx?entryyear=" & sentryyear & "&DisplayField=" & sDisplayField & "&Type=" & "hour", True)

    End Sub

    Protected Sub grdMonth_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles grdMonth.SelectedIndexChanged
        sentryyear = grdMonth.SelectedRow.Cells(1).Text.ToString
        'sDisplayField = grdMonth.SelectedRow.Cells(3).Text.ToString
        'Response.Redirect("~/P1ReportDetail.aspx?entryyear=" & sentryyear & "&DisplayField=" & sDisplayField & "&Type=" & "month", True)

    End Sub
    Protected Sub grdMonth_Sorting(sender As Object, e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdMonth.Sorting
        'Retrieve the table from the session object.
        Dim sortdt = TryCast(Session("P1table"), DataTable)

        If sortdt IsNot Nothing Then

            'Sort the data.
            sortdt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression)
            grdMonth.DataSource = sortdt
            grdMonth.DataBind()

        End If

    End Sub
    Protected Sub grdgroup_Sorting(sender As Object, e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdgroup.Sorting
        'Retrieve the table from the session object.
        Dim sortdt = TryCast(Session("P1table"), DataTable)

        If sortdt IsNot Nothing Then

            'Sort the data.
            sortdt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression)
            grdgroup.DataSource = sortdt
            grdgroup.DataBind()

        End If
    End Sub

    Protected Sub grdHour_Sorting(sender As Object, e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdHour.Sorting
        'Retrieve the table from the session object.
        Dim sortdt = TryCast(Session("P1table"), DataTable)

        If sortdt IsNot Nothing Then

            'Sort the data.
            sortdt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression)
            grdHour.DataSource = sortdt
            grdHour.DataBind()

        End If
    End Sub
    Protected Sub grdDay_Sorting(sender As Object, e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdDay.Sorting
        'Retrieve the table from the session object.
        Dim sortdt = TryCast(Session("P1table"), DataTable)

        If sortdt IsNot Nothing Then

            'Sort the data.
            sortdt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression)
            grdDay.DataSource = sortdt
            grdDay.DataBind()

        End If
    End Sub

    Private Function GetSortDirection(ByVal column As String) As String

        ' By default, set the sort direction to ascending.
        Dim sortDirection = "ASC"

        ' Retrieve the last column that was sorted.
        Dim sortExpression = TryCast(ViewState("SortExpression"), String)

        If sortExpression IsNot Nothing Then
            ' Check if the same column is being sorted.
            ' Otherwise, the default value can be returned.
            If sortExpression = column Then
                Dim lastDirection = TryCast(ViewState("SortDirection"), String)
                If lastDirection IsNot Nothing _
                  AndAlso lastDirection = "ASC" Then

                    sortDirection = "DESC"

                End If
            End If
        End If

        ' Save new values in ViewState.
        ViewState("SortDirection") = sortDirection
        ViewState("SortExpression") = column

        Return sortDirection

    End Function
#Region "Chart"
    'Public Sub BuildSeries()
    '    Dim dt As New DataTable

    '    'dt = dst.Tables(0)
    '    'PointDateField
    '    'PointTypeField
    '    'CountField 

    '    Chart1.Series.Clear()
    '    Dim dtChartTypes As New DataSet()
    '    Dim thisdata As New CommandsSqlAndOleDb()
    '    If Not PointTypeField.ToLower = "none" Then
    '        dtChartTypes = CommandsSqlAndOleDb.getDistinctDataSet(PointTypeField, "", dst)
    '        alSeriesList.AddRange(dtChartTypes.Tables(0))
    '    Else
    '        alSeriesList.Add("none")
    '    End If
    '    '    
    '    Dim iPointTotals(10) As Integer
    '    Dim dv As DataView

    '    Chart1.Series.Clear()
    '    For i As Integer = 0 To alSeriesList.Count - 1 ' chkTypes.Items.Count - 1
    '        ' If chkTypes.Items(i).Selected Then
    '        dt = getRowsByType(alSeriesList(i)) ' dt = getRowsByType(chkTypes.Items(i).Value)
    '        dv = New DataView(dt)

    '        Chart1.Series.Add(alSeriesList(i))
    '        Chart1.Series(alSeriesList(i)).Points.DataBindXY(dv, PointDataField, dv, "TotalCount")

    '        'If rblCharttype.SelectedValue.ToLower = "column" Then

    '        'End If
    '    Next

    '    '=================Totaling==============
    '    Select Case rblCharttype.SelectedValue.ToLower
    '        Case "-radar", "-line" '-  disables
    '            Dim iTypes As Integer = 0 'airbourne
    '            Dim iPoints As Integer = 0
    '            For iTypes = 0 To alSeriesList.Count - 1 'Chart1.Series.Count - 1 ' chkTypes.Items.Count - 1 'outer loop
    '                If Not alSeriesList(iTypes) Is Nothing Then ' chkTypes.Items(iTypes).Selected Then
    '                    For iPoints = 0 To Chart1.Series(0).Points.Count - 1
    '                        iPointTotals(iPoints) += Integer.Parse(Chart1.Series(iTypes).Points(iPoints).YValues(0))
    '                        'Chart1.Series(iTypes).Points(iPoints).Url = "http:\\www.amazon.com"
    '                    Next

    '                End If
    '            Next
    '            For iPoints = 0 To Chart1.Series(0).Points.Count - 1
    '                Chart1.Series(0).Points(iPoints).AxisLabel &= ControlChars.Lf & "       (" & iPointTotals(iPoints).ToString() & ")"
    '                ' Chart1.Series(0).Points(iPoints).AxisLabel &= "(" & iPointTotals(iPoints).ToString() & ")"
    '            Next
    '    End Select
    '    'Totals

    '    If rblCharttype.SelectedValue.ToLower = "line" Then
    '        Chart1.Series.Add("Total")
    '        Chart1.Series("Total").Points.DataBindXY(dvGrpByDate, "PointDate", dvGrpByDate, "PointValue")
    '        Chart1.Series("Total").ChartType = SeriesChartType.Line
    '    End If

    '    ' Show as 3D
    '    Chart1.ChartAreas("ChartArea1").Area3DStyle.Enable3D = True

    'End Sub
    'Public Sub SetChartType()
    '    Dim sSeries As SeriesCollection
    '    sSeries = Chart1.Series
    '    For Each s As Series In sSeries
    '        Select Case rblChartType.SelectedValue.ToLower
    '            Case "column"
    '                s.ChartType = SeriesChartType.Column
    '                s("DrawingStyle") = "Cylinder"
    '                s.IsValueShownAsLabel = True
    '                s("PointWidth") = "0.6" ' + (i / 10)
    '                s("BarLabelStyle") = "Center"
    '                'Chart1.Legends(0).InterlacedRows = True
    '                'Chart1.Legends(0).IsTextAutoFit = True

    '            Case "bar"
    '                s.ChartType = SeriesChartType.Bar
    '                s("DrawingStyle") = "Cylinder"
    '                s.IsValueShownAsLabel = True
    '                s("PointWidth") = "0.6" ' + (i / 10)
    '                s("BarLabelStyle") = "Center"
    '            Case "line"
    '                ' s.ChartType = SeriesChartType.Line
    '                s.ChartType = SeriesChartType.Line
    '                s("DrawingStyle") = "Cylinder"
    '                s.IsValueShownAsLabel = True
    '                s("PointWidth") = "0.2" ' + (i / 10)
    '                s("BarLabelStyle") = "Center"
    '            Case "radar"
    '                ' s.ChartType = SeriesChartType.Line
    '                s.ChartType = SeriesChartType.Radar
    '                s("DrawingStyle") = "Cylinder"
    '                s.IsValueShownAsLabel = True
    '                s("PointWidth") = "0.2" ' + (i / 10)
    '                s("BarLabelStyle") = "Center"
    '            Case "pie"
    '                s.ChartType = SeriesChartType.Pie
    '                s("DrawingStyle") = "Cylinder"
    '                s.IsValueShownAsLabel = True 'False '
    '                s("PieLabelStyle") = "Outside" ' "Inside" '"Disabled" ' '
    '                ' s.Points(2).AxisLabel = s.Name
    '                ' s.Points(2).Label = s.Name

    '                s("PointWidth") = "0.2" ' + (i / 10)
    '                s("BarLabelStyle") = "Center"
    '                Chart1.Legends(0).Enabled = True
    '            Case "doughnut"
    '                s.ChartType = SeriesChartType.Doughnut
    '                s("DrawingStyle") = "Cylinder"
    '                s.IsValueShownAsLabel = True
    '                s("PieLabelStyle") = "Inside" '"Disabled" '"Outside" 


    '                s("PointWidth") = "0.2" ' + (i / 10)
    '                s("BarLabelStyle") = "Center"
    '                Chart1.Legends(0).Enabled = True
    '                Chart1.Legends(0).IsTextAutoFit = True
    '                '  Chart1.Legends(0).InterlacedRows = True
    '        End Select
    '    Next
    'End Sub
    'Public Sub FillDataset(ByVal sHospital As String)
    '    Dim sConn As String = "Data Source=hypnos03;Initial Catalog=CCMCPatientTrack01;User ID=bedtracking;Password=crozer"

    '    Dim sTypes As String = ""
    '    Dim iSelected As Integer = 0

    '    Select Case sHospital.ToLower
    '        Case "crozer"
    '            sConn = Session("CrozerDataConn") ' "Data Source=hypnos03;Initial Catalog=CCMCPatientTrack01;User ID=bedtracking;Password=crozer"

    '        Case "dcmh"
    '            sConn = Session("DcmhDataConn") ' "Data Source=hypnos03;Initial Catalog=DcmhPatientTrack01;User ID=bedtracking;Password=crozer"

    '        Case "taylor"
    '            sConn = Session("TaylorDataConn") ' "Data Source=hypnos03;Initial Catalog=TaylorPatientTrack01;User ID=bedtracking;Password=crozer"
    '        Case "springfield"
    '            sConn = Session("SpringfieldDataConn") '"Data Source=hypnos03;Initial Catalog=SpringfieldPatientTrack01;User ID=bedtracking;Password=crozer"

    '    End Select
    '    'SelectCommand="SelTransportCount" SelectCommandType="StoredProcedure">
    '    '                <SelectParameters>
    '    '                    <asp:ControlParameter ControlID="txtStartDate" DefaultValue="04/20/2010" 
    '    '                        Name="StartDate" PropertyName="Text" Type="string" />
    '    '                        <asp:ControlParameter ControlID="txtEndDate" DefaultValue="04/20/2010" 
    '    '                        Name="EndDate" PropertyName="Text" Type="string" />

    '    sConn = SqlDataSource1.ConnectionString
    '    Session("CurrDataConn") = SqlDataSource1.ConnectionString
    '    Dim thisData As New CommandsSqlAndOleDb("SelTransportCount", sConn)
    '    If IsDate(txtStartDate.Text) Then
    '        thisData.AddSqlProcParameter("@StartDate", txtStartDate.Text, SqlDbType.DateTime, 16)
    '    End If
    '    If IsDate(txtStartDate.Text) Then
    '        thisData.AddSqlProcParameter("@EndDate", txtEndDate.Text, SqlDbType.DateTime, 16)
    '    End If

    '    dst = thisData.GetSqlDataset()

    'End Sub
    'Private Sub setChartDataSource()
    '    'Example   setChartDataSource("ReportMonth","none(for multiSeries)",ReportCount)ByVal PointDateField As String, ByVal PointTypeField As String, ByVal PointCountField As String
    '    Dim iRows As Integer
    '    dstOut = New DataSet()
    '    '=======dvPie data now grouped by date

    '    Dim dCol As New DataColumn
    '    Dim i As Integer = 0
    '    Dim j As Integer = 0
    '    Dim bFoundExistingDate As Boolean
    '    Dim bFoundExistingType As Boolean

    '    dtGrpByDate.Columns.Clear()
    '    dtGrpByType.Columns.Clear()
    '    dtGrpByDate = New DataTable()
    '    dtGrpByType = New DataTable()
    '    dCol = New DataColumn("PointDate", System.Type.GetType("System.String"))
    '    dtGrpByDate.Columns.Add(dCol)
    '    dCol = New DataColumn("PointValue", System.Type.GetType("System.String"))
    '    dtGrpByDate.Columns.Add(dCol)
    '    '=========type
    '    dCol = New DataColumn("PointType", System.Type.GetType("System.String")) 'airborne etc
    '    dtGrpByType.Columns.Add(dCol)
    '    dCol = New DataColumn("PointValue", System.Type.GetType("System.String"))
    '    dtGrpByType.Columns.Add(dCol)
    '    For i = 0 To dst.Tables(0).Rows.Count - 1
    '        bFoundExistingDate = False
    '        For j = 0 To dtGrpByDate.Rows.Count - 1 'look for match
    '            'for group by date
    '            If dst.Tables(0).Rows(i).Item(PointDataField) = dtGrpByDate.Rows(j).Item("pointDate") Then
    '                dtGrpByDate.Rows(j).Item("PointValue") += dst.Tables(0).Rows(i).Item(PointCountField)
    '                bFoundExistingDate = True
    '            End If
    '            'for group by type
    '        Next
    '        For j = 0 To dtGrpByType.Rows.Count - 1 'look for match
    '            If dst.Tables(0).Rows(i).Item(PointTypeField) = dtGrpByType.Rows(j).Item("pointType") Then
    '                dtGrpByType.Rows(j).Item("pointValue") += dst.Tables(0).Rows(i).Item(PointCountField)
    '                bFoundExistingType = True
    '            End If
    '        Next

    '        '  For j = 0 To dtPie.Rows.Count - 1
    '        'Add row or add value in row with this) 
    '        If bFoundExistingDate = False Then
    '            Dim dr As DataRow
    '            dr = dtGrpByDate.NewRow()
    '            dr.Item("pointDate") = dst.Tables(0).Rows(i).Item(PointDataField)
    '            dr.Item("pointValue") = 0
    '            dr.Item("pointValue") += dst.Tables(0).Rows(i).Item(PointCountField)

    '            dtGrpByDate.Rows.Add(dr)
    '        End If
    '        If PointTypeField.ToLower() <> "none" Then 'none is only single Column series
    '            If bFoundExistingType = False Then
    '                Dim dr As DataRow
    '                dr = dtGrpByType.NewRow()
    '                dr.Item("PointType") = dst.Tables(0).Rows(i).Item(PointTypeField)
    '                dr.Item("PointValue") = 0
    '                dr.Item("PointValue") += dst.Tables(0).Rows(i).Item(PointCountField)

    '                dtGrpByType.Rows.Add(dr)
    '            End If
    '        End If
    '        'Next


    '    Next
    '    dvGrpByDate = New DataView(dtGrpByDate)
    '    dvGrpByType = New DataView(dtGrpByDate) ' New DataView(dtGrpByType)


    'End Sub
    'Public Function getRowsByType(ByVal sType As String) As DataTable
    '    Dim foundRows() As DataRow
    '    Dim dt As DataTable

    '    dstOut = New DataSet()

    '    Dim sCriteria As String = PointTypeField & " = '" & sType & " '" ' PointCountField & " > 0 "
    '    If sType = "none" Then
    '        sCriteria = PointCountField & " > 0 "
    '    End If
    '    foundRows = dst.Tables(0).Select(sCriteria, ChartSortField)

    '    Dim thisdata As New CommandsSqlAndOleDb()

    '    dt = thisdata.GetSqlDataTableWithSortAndCriteria(sCriteria, ChartSortField, dst)

    '    Dim irows As Integer = dt.Rows.Count()

    '    Return dt 'dstOut

    'End Function
#End Region
    Protected Sub btnExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExcel.Click
        CreateExcel2()
    End Sub
    Sub CreateExcel2()



        Response.AddHeader("content-disposition", "attachment;filename=FileName.xls")

        Response.Charset = String.Empty
        Response.ContentType = "application/vnd.ms-excel"
        '  Response.ContentType = "application/vnd.xls"

        Dim sw As System.IO.StringWriter = New System.IO.StringWriter()

        Dim hw As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(sw)

        '===============================Create form to contain grid
        Dim frm As New HtmlForm()

        Dim ReportTotalList As New List(Of P1ReportViewer)

        CurrentDate = Datesddl.SelectedValue
        CurrentType = Typeddl.SelectedValue

        HDyear.Text = CurrentDate
        HDtype.Text = CurrentType

        ReportTotalContrl.GetP1Totals(HDtype.Text, HDyear.Text)
        ReportTotalList = ReportTotalContrl.getP1ByDate()

        'bindVendors()
        'GvVendors.Columns.RemoveAt(0)
        'GvVendors.Parent.Controls.Add(frm)
        'frm.Attributes("RunAt") = "server"
        'frm.Controls.Add(GvVendors)

        Select Case Typeddl.SelectedValue.ToLower
            Case "group"
                grdgroup.Visible = True

                grdTotal.Visible = False
                grdMonth.Visible = False
                grdHour.Visible = False
                grdDay.Visible = False

                grdgroup.AutoGenerateColumns = False
                grdgroup.DataSource = ReportTotalList
                grdgroup.DataBind()

                grdgroup.Columns.RemoveAt(0)
                grdgroup.Parent.Controls.Add(frm)
                frm.Attributes("RunAt") = "server"
                frm.Controls.Add(grdgroup)

            Case "year"
                grdTotal.Visible = True

                grdgroup.Visible = False
                grdMonth.Visible = False
                grdHour.Visible = False
                grdDay.Visible = False

                grdTotal.AutoGenerateColumns = False
                grdTotal.DataSource = ReportTotalList
                grdTotal.DataBind()

                grdTotal.Columns.RemoveAt(0)
                grdTotal.Parent.Controls.Add(frm)
                frm.Attributes("RunAt") = "server"
                frm.Controls.Add(grdTotal)

            Case "month"
                grdMonth.Visible = True

                grdTotal.Visible = False
                grdgroup.Visible = False
                grdHour.Visible = False
                grdDay.Visible = False

                grdMonth.AutoGenerateColumns = False
                grdMonth.DataSource = ReportTotalList
                grdMonth.DataBind()

                grdMonth.Columns.RemoveAt(0)
                grdMonth.Parent.Controls.Add(frm)
                frm.Attributes("RunAt") = "server"
                frm.Controls.Add(grdMonth)

            Case "day"
                grdDay.Visible = True

                grdMonth.Visible = False
                grdTotal.Visible = False
                grdgroup.Visible = False
                grdHour.Visible = False

                grdDay.AutoGenerateColumns = False
                grdDay.DataSource = ReportTotalList
                grdDay.DataBind()

                grdDay.Columns.RemoveAt(0)
                grdDay.Parent.Controls.Add(frm)
                frm.Attributes("RunAt") = "server"
                frm.Controls.Add(grdDay)

            Case "hour"
                grdHour.Visible = True

                grdDay.Visible = False
                grdMonth.Visible = False
                grdTotal.Visible = False
                grdgroup.Visible = False

                grdHour.AutoGenerateColumns = False
                grdHour.DataSource = ReportTotalList
                grdHour.DataBind()

                grdHour.Columns.RemoveAt(0)
                grdHour.Parent.Controls.Add(frm)
                frm.Attributes("RunAt") = "server"
                frm.Controls.Add(grdHour)


            Case Else
                grdTotal.Visible = True

                grdgroup.Visible = False
                grdMonth.Visible = False
                grdHour.Visible = False
                grdDay.Visible = False

                grdTotal.AutoGenerateColumns = False
                grdTotal.DataSource = ReportTotalList
                grdTotal.DataBind()

                grdTotal.Columns.RemoveAt(0)
                grdTotal.Parent.Controls.Add(frm)
                frm.Attributes("RunAt") = "server"
                frm.Controls.Add(grdTotal)

        End Select


        frm.RenderControl(hw)
        Response.Write(sw.ToString())

        Response.End()

    End Sub
End Class
