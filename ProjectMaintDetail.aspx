﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="false" CodeFile="ProjectMaintDetail.aspx.vb" Inherits="ProjectMaintDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
<asp:UpdatePanel ID="uplPanel" runat ="server"  UpdateMode="Conditional">
   <ContentTemplate>
                       <table border="1" width="100%"  style="empty-cells:show" >
                         <tr>
                           <td  align="center" colspan="4">
                            <table>
                                <tr>
                                    <td align="center">

                                            <asp:Button ID ="BtnAddProject" runat = "server" Text ="Submit " 
                                            CssClass="btnhov" BackColor="#006666" Width="120px" />
                                        
                                        
                                    </td>
                                    <td align="center">
                                 
                                            <asp:Button ID="BtnExcel2" runat="server" Text="Excel" 
                                            CssClass="btnhov" BackColor="#006666" Width="120px" />
                                    </td>
                                    <td align="center" >

                                            <asp:Button ID = "BtnAddReset" runat= "server" Text="Reset" 
                                            CssClass="btnhov" BackColor="#006666"  Width="120px" Visible="false" />

                                    </td>
                                    <td>
                                    </td>

                                </tr>
                            </table>
                          </td>
                         </tr>   
                            <tr>
                                <td align="center" colspan="4">
                                    <asp:Label ID="lblAddprojectmsg" runat="server" ForeColor="Red" Font-Size="15px" Font-Bold="True"></asp:Label>
                            
                                </td>
                            </tr>


                             <tr>
                                <td colspan="4">

                                        <table  width="100%">


                                            <tr>
                                              <td align="right">
                                                  <asp:Label ID="ProjectNamelbl" runat="server" Text="Project Name:">
                                                   </asp:Label>
                                              </td>

                                              <td colspan="3">
                                                   <asp:TextBox ID="ProjectNameTextBox" runat="server" Width="600PX">
                                                    </asp:TextBox>
                                                    <asp:TextBox ID="ProjectNumTextBox" runat="server" Visible="false"></asp:TextBox>
                          
                                              </td>
                                             </tr>
                                             <tr>
                                                <td align="right">
                                                    <asp:Label ID="ShortNamelbl" runat="server" text="Short Name:"></asp:Label>
                                                </td>
                                                <td colspan="3">
                                                    <asp:TextBox ID="ProjectShortNameTextBox" runat="server" Width="600px"></asp:TextBox>
                                                </td>
                                             </tr>

                                            <tr>
                                                <td align="right">
                                                    <asp:Label ID="ProDesclbl" runat="server" Text="Project Description"></asp:Label>
                                                </td>
                                                <td colspan="3">
                                                    <asp:TextBox ID ="ProjectDesctxt" runat="server" TextMode="MultiLine" Width="600px">
                                                    </asp:TextBox>
                            
                                                </td>
                        
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    <asp:Label ID="AddOwnerlbl" runat="server" Text="Project Owner:" ></asp:Label>
                                                </td>
                                                <td colspan="3">
                                                        <asp:DropDownList ID = "Technicianddl" runat = "server" Width="200px">
                                                        </asp:DropDownList>
                                                        <asp:Label ID="OwnerNameLabel" runat="server" Visible="false"></asp:Label>
                                                        <asp:Label ID="client_numLabel" runat="server" Visible="false"></asp:Label>
    
                            
                                                </td>
                                            </tr>
                                            <tr>
                                            
                                                <td align="right">
                                                    <asp:Label id="deptlbl" runat="server" Text="Department:" ></asp:Label>
                                                
                                                </td>
                                                <td colspan="3">
                                                    <asp:DropDownList ID="DepartmentNameddl" runat="server" Width="200px">
                                                    </asp:DropDownList>
                                                    <asp:Label ID="DepartmentNamelabel" runat="server" Visible="false"></asp:Label>

                                                
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" align="center">
                                                
                                                <table>
                                                    <tr>
                                                        <td colspan="2">
                                                           <asp:Button ID="btnDepartmentOwner" runat="server" Text="Department Owner" 
                                                            CssClass="btnhov" BackColor="#006666"  Width="200px"  Height="30px"  />
                                                        
                                                            
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                          <td align="right">
                                                            <asp:Label ID="deptownerlbl" runat="server" Text= "Department Owner:"></asp:Label>
                                                
                                                        </td>
                                                        <td>
                                                              <asp:TextBox ID="DepartmentOwnerTextbox" runat="server" 
                                                              Width="200px" Text="test"></asp:TextBox>


                                                            <asp:Label ID="departmentClientNumLabel"  runat="server" Visible="false"></asp:Label>
                                                        </td>

                                                    
                                                    </tr>
                                                
                                                </table>

                                                </td>
                                            </tr>
                                            <tr>
                                                  <td align="right" width="180">
                                                        Start Date:</td>
                                                    <td class="style19" align="left" colspan="3">
                                                       <asp:TextBox ID="StartDateTextBox"  Class="calanderClass" runat="server" Width="200px" AutoPostBack="false" CssClass="calenderClass"></asp:TextBox>
                                                    </td>
                                             </tr>
                                            <tr>
                                                  <td align="right" width="180">
                                                        <asp:Label ID="Enddatelbl" runat="server" text="Close Date:">
                                                        </asp:Label>
                                                   </td>
                                                    <td class="style19" align="left" colspan="3">
                                                       <asp:TextBox ID="EnddateTextBox"  Class="calanderClass" runat="server" Width="200px" AutoPostBack="false" CssClass="calenderClass"></asp:TextBox>
                                                    </td>
                                             </tr>


                                            <tr>
                                                  <td align="right" width="180">
                                                        <asp:Label ID="Label1" runat="server" text="Planned Live Date:">
                                                        </asp:Label>
                                                   </td>
                                                    <td class="style19" align="left" colspan="3">
                                                       <asp:TextBox ID="PlannedDateTextBox"  Class="calanderClass" runat="server" Width="200px" AutoPostBack="false" CssClass="calenderClass"></asp:TextBox>
                                                    </td>
                                             </tr>


                                            <tr>
                                                  <td align="right" width="180">
                                                        <asp:Label ID="Label2" runat="server" text="Actual Live Date:">
                                                        </asp:Label>
                                                   </td>
                                                    <td class="style19" align="left" colspan="3">
                                                       <asp:TextBox ID="ActualDateTextBox"  Class="calanderClass" runat="server" Width="200px" AutoPostBack="false" CssClass="calenderClass"></asp:TextBox>
                                                    </td>
                                             </tr>
                                             <tr>
                                                <td colspan ="4">
                                                    <asp:GridView ID="gvTicketDetail" runat="server" AllowSorting="True"  AutoGenerateColumns="False" BackColor="LightGray" BorderColor="#336666"
                                                        BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                                                        EmptyDataText="No Tickets/RFS Found" EnableTheming="False" DataKeyNames="Request"
                                                        Font-Size="small" GridLines="Horizontal" Width="100%" AlternatingRowStyle-BackColor="Bisque"  Visible="false">
                                   
                                                    <FooterStyle BackColor="White" ForeColor="#333333" />
                                                    <HeaderStyle BackColor="#336666" Font-Bold="False" ForeColor="White" Font-Size="Small" HorizontalAlign="Left"    />                                    
                                                    <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Left" Font-Size="Medium"/>
                                                    <RowStyle BackColor="LightGray" ForeColor="#333333" />
                                                    <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                                    <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                                    <SortedAscendingHeaderStyle BackColor="#487575" />
                                                    <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                                    <SortedDescendingHeaderStyle BackColor="#275353" />
                                                    <Columns>
                                                            <asp:TemplateField HeaderText="Request"  SortExpression="Request" ItemStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>

                                                            <asp:HyperLink ID="HyperLink1" runat="server" 

                                                                NavigateUrl='<% #Eval("Request", "~/ViewRequestTicket.aspx?RequestNum={0}") %>'

                                                                Text='<% #Eval("Request") %>'>


                                                            </asp:HyperLink>
                       
                                                            </ItemTemplate>
                        

                                                        </asp:TemplateField>
                                    <%--                    <asp:CommandField ButtonType="Button" SelectText="Select" ShowSelectButton= "true" ControlStyle-BackColor="#84A3A3" ItemStyle-Height="30px" />
                                    --%>
                                                        <asp:BoundField  DataField="FullName"  HeaderText="Full Name" SortExpression="FullName" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" ItemStyle-Height="35px" ></asp:BoundField>
   
                                                        <asp:BoundField  DataField="category_cd"  HeaderText="Category" SortExpression="category_cd" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
				                                        <asp:BoundField  DataField="type_cd"  HeaderText="Type " SortExpression="type_cd" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>

                                                        <asp:BoundField  DataField="entry_date"  HeaderText="Entry Date" SortExpression="entry_date" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
				                                        <asp:BoundField  DataField="TechName"  HeaderText="TechName" SortExpression="TechName" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>

				                                        <asp:BoundField  DataField="CurrentStatus"  HeaderText="Status" SortExpression="CurrentStatus" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
                

                                                    </Columns>
                                                </asp:GridView>
                                                </td>
                                            </tr>


                                        </table>
                           
                            
                                </td>
                         
                             </tr>



                        </table>

                        <asp:Panel ID="pnlDepartmentOwner" runat="server"  Width="70%" Height="60%"  BackColor="#CCCCCC"  style="display:none">
                                    <table align="center" border="1px" width="90%">
                                       <tr valign="middle">
                                            <td colspan="4"></td>
                                        </tr>
                                        <tr>
                                            <td class="tableRowHeader" colspan="4">
                                               Search for CKHS Individual who will be Responsable for this Project<br />
                                               Listed below are CKHS Directors-Managers-VP 

                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Last Name
                                            </td>
                                            <td>
                                                <asp:TextBox ID="LastNameTextBox" runat="server"></asp:TextBox>
                                            </td>
                                            <td>
                                                First Name
                                            </td>
                                            <td>
                                                <asp:TextBox ID="FirstNameTextBox" runat="server"></asp:TextBox>
                                            </td>

                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <div class="scrollContentContainer">

<%--                                                    <asp:UpdateProgress ID="UpdateProgress1" runat="server" 
                                                        AssociatedUpdatePanelID="uplDepartmentOwner" DisplayAfter="200">
                                                        <ProgressTemplate>
                                                            <div ID="IMGDIV" runat="server" align="center" 
                                                                style="visibility:visible;vertical-align:middle;" valign="middle">
                                                                Loading Data ...
                                                            </div>
                                                        </ProgressTemplate>
                                                    </asp:UpdateProgress>
--%>

                                                    <asp:GridView ID="gvSearch" runat="server" AllowSorting="True" 
                                                        AutoGenerateColumns="False" BackColor="White" BorderColor="#336666" 
                                                        BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                                                        DataKeyNames="client_num, login_name" EmptyDataText="No Items Found" 
                                                        EnableTheming="False" Font-Size="Medium" GridLines="Horizontal" Width="100%">

                                                        <FooterStyle BackColor="White" ForeColor="#333333" />
                                                        <HeaderStyle BackColor="#336666" Font-Bold="False" Font-Size="Small" 
                                                            ForeColor="White" HorizontalAlign="Left" />
                                                        <PagerStyle BackColor="#336666" Font-Size="Medium" ForeColor="White" 
                                                            HorizontalAlign="Left" />

                                                        <RowStyle BackColor="White" ForeColor="#333333" />
                                                        <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                                        <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                                        <SortedAscendingHeaderStyle BackColor="#487575" />
                                                        <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                                        <SortedDescendingHeaderStyle BackColor="#275353" />
                                                        <Columns>

                                                            <asp:CommandField ButtonType="Button" SelectText="Select" 
                                                                ShowSelectButton="true" />
                                                            <asp:BoundField DataField="fullname" HeaderStyle-HorizontalAlign="Left" 
                                                                HeaderText="Full Name" ItemStyle-HorizontalAlign="Left" />
                                                            <asp:BoundField DataField="phone" HeaderStyle-HorizontalAlign="Left" 
                                                                HeaderText="Phone" ItemStyle-HorizontalAlign="Left" />
                                                            <asp:BoundField DataField="department_name" HeaderStyle-HorizontalAlign="Left" 
                                                                HeaderText="Department" ItemStyle-HorizontalAlign="Left" />
                                                        
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="4">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Button ID="btnSearch" runat="server" Font-Bold="True" Font-Names="Arial" 
                                                                Font-Size="Small" Height="20px" Text="Search" Width="125px" />
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="btnCloseDeptOwner" runat="server" Font-Bold="True" 
                                                                Font-Names="Arial" Font-Size="Small" Height="20px" Text="Close" Width="125px" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                    </table>

                            
                         </asp:Panel>


                           <ajaxToolkit:ModalPopupExtender ID="mpeProjecowner" runat="server" 
                           DynamicServicePath="" Enabled="True" TargetControlID="HDBtnNewpro"   
                           PopupControlID="pnlDepartmentOwner" BackgroundCssClass="ModalBackground"   
                             DropShadow="true" CancelControlID="btnCloseDeptOwner">  
                            </ajaxToolkit:ModalPopupExtender>

                           <asp:Button ID="HDBtnNewpro" runat="server" style="display:none" />

       
    </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

