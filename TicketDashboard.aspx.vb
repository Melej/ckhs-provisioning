﻿Imports App_Code
Imports System.Web.UI.DataVisualization.Charting

Partial Class TicketDashboard
    Inherits System.Web.UI.Page
    Dim sActivitytype As String = ""
    Dim cblBinder As New CheckBoxListBinder
    Dim UserInfo As UserSecurity
    Dim FormSignOn As New FormSQL()

    Dim sClientNum As String

    Dim ApplicationNumber As String
    Dim GroupNum As String
    Dim AccountCd As String
    Dim MaintType As String

    Dim sGnumber As String = ""
    Dim sCommGnumber As String = ""
    Dim sInvisionNumber As String = ""
    Dim GroupNumS As String = ""
    Dim sendate As String

    Dim securityLevel As Integer
    Dim totalcount As Integer
    Dim UnknowCount As Integer
    Dim EmployeeCount As Integer
    Dim ContractCount As Integer
    Dim StudentCount As Integer
    Dim PhysicianCount As Integer
    Dim NursingCount As Integer
    Dim EmpNoNumberCount As Integer
    Dim NonCKHNCount As Integer
    Dim ResidentCount As Integer
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        UserInfo = New UserSecurity(Session("LoginID"), Session("EmployeeConn"))

        securityLevel = UserInfo.SecurityLevelNum

        'Dim frmSec As New FormSecurity(Session("objUserSecurity"))

        If Session("SecurityLevelNum") < 25 Then
            Response.Redirect("~/Security.aspx")
        End If

        If Session("SecurityLevelNum") < 40 Then
            Response.Redirect("~/MyRequests.aspx")

        End If


        If Not IsPostBack Then

            'txtEndDate.Text = DateTime.Now.ToShortDateString
            'txtBeginDate.Text = DateAdd(DateInterval.Day, -14, DateTime.Now()).ToShortDateString

            txtBeginDate.Text = DateTime.Today.AddDays(-365)
            txtEndDate.Text = DateTime.Today

            HDStartDate.Text = txtBeginDate.Text
            HDEnddate.Text = txtEndDate.Text

            Dim ddlBinder As DropDownListBinder = New DropDownListBinder(categorycdddl, "SelCategoryCodes", Session("CSCConn"))
            ddlBinder.AddDDLCriteria("@securitylevel", securityLevel, SqlDbType.Int)

            ddlBinder.BindData("category_cd", "Category_Desc")

            HDStatus.Text = Statusrbl.SelectedValue

            fillTable()

        End If
        'PopulateTickets()


    End Sub
    Protected Sub PopulateTickets()

        Dim thisData As New CommandsSqlAndOleDb("GetCSCCategoryTypeCounts", Session("CSCConn"))
        thisData.AddSqlProcParameter("@startDate", HDStartDate.Text, SqlDbType.NVarChar)
        thisData.AddSqlProcParameter("@endDate", HDEnddate.Text, SqlDbType.NVarChar)


        'If rblPriority.SelectedValue <> "All" Then
        '    thisData.AddSqlProcParameter("@Priority", rblPriority.SelectedValue, SqlDbType.Int)

        'End If

        'If ddlGroups.SelectedIndex > 0 Then

        '    thisData.AddSqlProcParameter("@GroupNum", ddlGroups.SelectedValue, SqlDbType.Int)

        'End If




        Dim dt As DataTable = thisData.GetSqlDataTable




    End Sub
    Public Function TicketQueue(ByRef dtTable As DataTable) As Table
        Dim tblReturn As New Table

        tblReturn.Width = New Unit("100%")
        tblReturn.BackColor = Color.White

        tblReturn.CssClass = "Requests"
        If dtTable IsNot Nothing Then

            For Each dr As DataRow In dtTable.Rows

                Dim rwHeader As New TableHeaderRow

                'Dim clTicketNum As New TableCell
                'Dim clTicketDate As New TableCell

                'Dim rwUser As New TableRow
                'Dim clUserHeader As New TableCell
                'Dim clUser As New TableCell
                'Dim clUserEntityHeader As New TableCell
                'Dim clUserEntity As New TableCell
                'Dim clDepartmentHeader As New TableCell
                'Dim clDepartment As New TableCell


                'Category 
                'Type 
                'Counts 
                'CurrentStatus
                Dim rwCategory As New TableRow

                Dim clCatetoryHeader As New TableCell
                Dim clCategory As New TableCell
                Dim clTypeHeader As New TableCell
                Dim clType As New TableCell

                Dim clCountHeader As New TableCell
                Dim clCount As New TableCell

                Dim clStatusHeader As New TableCell
                Dim clStatus As New TableCell



                Dim rwSitutation As New TableRow
                Dim clSituationHeader As New TableCell
                Dim clSituation As New TableCell


                'clTicketNum.Text = "<a href='ViewRequestTicket.aspx?RequestNum=" & dr("RequestNum") & "'>" & dr("RequestNum") & " Priority " & dr("Priority") & "</a>"
                'clTicketNum.ColumnSpan = 3
                'clTicketDate.Text = dr("DateEntered")
                'clTicketDate.ColumnSpan = 3


                'clTicketDate.HorizontalAlign = HorizontalAlign.Right

                'clUserHeader.Text = "User:"
                'clUserHeader.CssClass = "displayCell"
                'clUser.Text = dr("RequestorName")

                'clUserEntityHeader.Text = "Location:"
                'clUserEntityHeader.CssClass = "displayCell"
                'clUserEntity.Text = dr("facility_name")

                'clDepartmentHeader.Text = "Department:"
                'clDepartmentHeader.CssClass = "displayCell"
                'clDepartment.Text = "department place holder'"


                'rwUser.Cells.Add(clUserHeader)
                'rwUser.Cells.Add(clUser)
                'rwUser.Cells.Add(clUserEntityHeader)
                'rwUser.Cells.Add(clUserEntity)
                'rwUser.Cells.Add(clDepartmentHeader)
                'rwUser.Cells.Add(clDepartment)


                clCatetoryHeader.Text = "Category:"
                clCatetoryHeader.CssClass = "displayCell"
                clCategory.Text = IIf(IsDBNull(dr("Category")), "", dr("Category"))

                clTypeHeader.Text = "Type:"
                clTypeHeader.CssClass = "displayCell"
                clType.Text = IIf(IsDBNull(dr("Type")), "", dr("Type"))

                clCountHeader.Text = "Count:"
                clCountHeader.CssClass = "displayCell"
                clCount.Text = IIf(IsDBNull(dr("Counts")), "", dr("Counts"))


                clStatusHeader.Text = "Status:"
                clStatusHeader.CssClass = "displayCell"
                clStatus.Text = IIf(IsDBNull(dr("CurrentStatus")), "", dr("CurrentStatus"))

                rwCategory.Cells.Add(clCatetoryHeader)
                rwCategory.Cells.Add(clCategory)
                rwCategory.Cells.Add(clTypeHeader)
                rwCategory.Cells.Add(clType)

                rwCategory.Cells.Add(clCountHeader)
                rwCategory.Cells.Add(clCount)

                rwCategory.Cells.Add(clStatusHeader)
                rwCategory.Cells.Add(clStatus)

                'clSituationHeader.Text = "Situation:"
                'clSituationHeader.CssClass = "displayCell"

                'clSituation.Text = dr("ShortDesc")
                'clSituation.ColumnSpan = 5

                'rwSitutation.Cells.Add(clSituationHeader)
                'rwSitutation.Cells.Add(clSituation)

                rwHeader.CssClass = "header"
                'rwHeader.Font.Size = "16"

                'rwHeader.Cells.Add(clTicketNum)
                'rwHeader.Cells.Add(clTicketDate)




                tblReturn.Rows.Add(rwHeader)
                'tblReturn.Rows.Add(rwUser)
                tblReturn.Rows.Add(rwCategory)
                'tblReturn.Rows.Add(rwSitutation)






            Next





        End If






        Return tblReturn
    End Function

    Protected Sub fillTable()

        FillTotals()

        'GvTotals

        Dim thisData As New CommandsSqlAndOleDb("GetCSCCategoryTypeCounts", Session("CSCConn"))

        thisData.AddSqlProcParameter("@startDate", HDStartDate.Text, SqlDbType.NVarChar)
        thisData.AddSqlProcParameter("@endDate", HDEnddate.Text, SqlDbType.NVarChar)

        thisData.AddSqlProcParameter("@Category", HDCategory.Text, SqlDbType.NVarChar)
        thisData.AddSqlProcParameter("@Status", HDStatus.Text, SqlDbType.NVarChar)

        'If displyrbl.SelectedValue.ToLower = "yes" Then
        '    thisData.AddSqlProcParameter("@enddate", "yes", SqlDbType.NVarChar)

        'End If
        'totalpan.Visible = False
        'Category
        'Type
        'Counts
        'CurrentStatus

        Dim iRowCount = 1
        Dim iTotalRowcount = 0

        Dim dt As DataTable
        dt = thisData.GetSqlDataTable

        Dim rwHeader As New TableRow

        Dim clSelectHeader As New TableCell
        Dim clCategoryHeader As New TableCell
        Dim clTypeHeader As New TableCell
        Dim clCountheader As New TableCell

        Dim clStatusHeader As New TableCell
        'Dim clStartDateHeader As New TableCell


        clSelectHeader.Text = " Details "
        clCategoryHeader.Text = "  Category  "
        clTypeHeader.Text = "  Type "
        clCountheader.Text = "  Count  "
        'clemailHeader.Text = "  eMail "

        clStatusHeader.Text = " Status "
        'clStartDateHeader.Text = "  Start Date  "


        rwHeader.Cells.Add(clSelectHeader)
        rwHeader.Cells.Add(clCategoryHeader)
        rwHeader.Cells.Add(clTypeHeader)
        rwHeader.Cells.Add(clCountheader)
        'rwHeader.Cells.Add(clemailHeader)

        rwHeader.Cells.Add(clStatusHeader)
        'rwHeader.Cells.Add(clStartDateHeader)


        rwHeader.Font.Bold = True
        rwHeader.BackColor = Color.FromName("#006666")
        rwHeader.ForeColor = Color.FromName("White")
        rwHeader.Height = Unit.Pixel(56)

        tblAccounts.Rows.Add(rwHeader)

        For Each drRow As DataRow In dt.Rows

            'If iTotalRowcount = 0 Then
            '    totalcountlbl.Text = drRow("TotalCount")
            '    UnknowCountlbl.Text = drRow("UnknowCount")
            '    EmployeeCountlbl.Text = drRow("EmployeeCount")
            '    ContractCountlbl.Text = drRow("ContractCount")
            '    StudentCountlbl.Text = drRow("StudentCount")
            '    PhysicianCountlbl.Text = drRow("PhysicianCount")
            '    NursingCountlbl.Text = drRow("NursingCount")
            '    EmpNoNumberCountlbl.Text = drRow("EmpNoNumberCount")
            '    NonCKHNCountlbl.Text = drRow("NonCKHNCount")
            '    ResidentCountlbl.Text = drRow("ResidentCount")

            'End If

            'Dim btnSelect As New Button
            Dim rwData As New TableRow

            Dim btnSelect As New TableCell
            Dim clCategory As New TableCell
            Dim clType As New TableCell

            Dim clCount As New TableCell
            'Dim clAppNameData As New TableCell
            'Dim clEmailData As New TableCell


            Dim clStatus As New TableCell
            'Dim clStartData As New TableCell



            'btnSelect.EnableTheming = False


            clCategory.Text = IIf(IsDBNull(drRow("category")), "", drRow("category"))
            clType.Text = IIf(IsDBNull(drRow("Type")), "", drRow("Type"))

            'clAppNameData.Text = IIf(IsDBNull(drRow("AppLoginName")), "", drRow("AppLoginName"))
            clCount.Text = (IIf(IsDBNull(drRow("Counts")), "", drRow("Counts")))

            clStatus.Text = (IIf(IsDBNull(drRow("CurrentStatus")), "", drRow("CurrentStatus")))
            'clStartData.Text = IIf(IsDBNull(drRow("StartDate")), "", drRow("StartDate"))

            'rwData.Cells.Add(clSelectData)

            btnSelect.Text = "<a href=""TicketDashBoardDetail.aspx?StarDate=" & HDStartDate.Text & "&EndDate=" & HDEnddate.Text & "&Category=" & clCategory.Text & "&Status=" & clStatus.Text & "&Type=" & clType.Text & """ ><u><b>" & " Detail " & "</b></u></a>"


            rwData.Cells.Add(btnSelect)

            rwData.Cells.Add(clCategory)
            rwData.Cells.Add(clType)
            rwData.Cells.Add(clCount)
            rwData.Cells.Add(clStatus)

            'rwData.Cells.Add(clAppNameData)
            'rwData.Cells.Add(clEmailData)

            'rwData.Cells.Add(clStartData)


            If iRowCount > 0 Then

                rwData.BackColor = Color.Bisque

            Else

                rwData.BackColor = Color.LightGray

            End If

            tblAccounts.Rows.Add(rwData)

            iRowCount = iRowCount * -1
            iTotalRowcount = iTotalRowcount + 1

        Next
        tblAccounts.Width = New Unit("100%")

        'If totalcountlbl.Text <> "0" Then
        '    totalpan.Visible = True
        'End If
    End Sub
    Protected Sub FillTotals()

        Dim thisData As New CommandsSqlAndOleDb("GetCSCCategoryTypeCounts", Session("CSCConn"))

        thisData.AddSqlProcParameter("@startDate", HDStartDate.Text, SqlDbType.NVarChar)
        thisData.AddSqlProcParameter("@endDate", HDEnddate.Text, SqlDbType.NVarChar)

        thisData.AddSqlProcParameter("@Category", HDCategory.Text, SqlDbType.NVarChar)
        thisData.AddSqlProcParameter("@Status", HDStatus.Text, SqlDbType.NVarChar)

        thisData.AddSqlProcParameter("@showTotals", "yes", SqlDbType.NVarChar)

        Dim dt As DataTable
        dt = thisData.GetSqlDataTable

        GvTotals.DataSource = thisData.GetSqlDataTable

        GvTotals.DataBind()
    End Sub
    'Public Sub bindAccount()


    '    Dim thisData As New CommandsSqlAndOleDb("SelAccountsByApplicationV3", Session("EmployeeConn"))
    '    thisData.AddSqlProcParameter("@Applicationnum", Appnumlbl.Text, SqlDbType.NVarChar)
    '    thisData.AddSqlProcParameter("@AffiliationID", empType.Text, SqlDbType.NVarChar)

    '    If displyrbl.SelectedValue.ToLower = "yes" Then
    '        thisData.AddSqlProcParameter("@enddate", "yes", SqlDbType.NVarChar)

    '    End If

    '    'If SearchVendorNameTextBox.Text = "" Then
    '    '    thisData.AddSqlProcParameter("@VendorName", "all", SqlDbType.NVarChar)
    '    'Else
    '    '    thisData.AddSqlProcParameter("@VendorName", SearchVendorNameTextBox.Text, SqlDbType.NVarChar)
    '    'End If

    '    Dim dt As DataTable

    '    dt = thisData.GetSqlDataTable

    '    GridAccounts.DataSource = dt
    '    GridAccounts.DataBind()


    '    GridAccounts.SelectedIndex = -1
    'End Sub
    'Protected Sub btnExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExcel.Click

    '    If displyrbl.SelectedValue.ToLower = "yes" Then
    '        sendate = displyrbl.SelectedValue

    '    End If

    '    Response.Redirect("./Reports/RolesReport.aspx?ReportName=AccountToUserReport" & "&Applicationnum=" & Appnumlbl.Text & "&EmpType=" & empType.Text & "&enddate=" & sendate, True)

    '    CreateExcel2()
    'End Sub
    Sub CreateExcel2()



        Response.AddHeader("content-disposition", "attachment;filename=FileName.xls")

        Response.Charset = String.Empty
        Response.ContentType = "application/vnd.ms-excel"
        '  Response.ContentType = "application/vnd.xls"

        Dim sw As System.IO.StringWriter = New System.IO.StringWriter()

        Dim hw As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(sw)

        '===============================Create form to contain grid
        Dim thisData As New CommandsSqlAndOleDb("SelAccountsByApplicationV3", Session("EmployeeConn"))
        'thisData.AddSqlProcParameter("@Applicationnum", Appnumlbl.Text, SqlDbType.NVarChar)
        'thisData.AddSqlProcParameter("@AffiliationID", empType.Text, SqlDbType.NVarChar)

        'If displyrbl.SelectedValue.ToLower = "yes" Then
        '    thisData.AddSqlProcParameter("@enddate", "yes", SqlDbType.NVarChar)

        'End If

        'If SearchVendorNameTextBox.Text = "" Then
        '    thisData.AddSqlProcParameter("@VendorName", "all", SqlDbType.NVarChar)
        'Else
        '    thisData.AddSqlProcParameter("@VendorName", SearchVendorNameTextBox.Text, SqlDbType.NVarChar)
        'End If

        Dim dt As DataTable

        dt = thisData.GetSqlDataTable

        GridAccounts.DataSource = dt
        GridAccounts.DataBind()

        'bindAccount()
        'GridAccounts.Visible = True

        Dim frm As New HtmlForm()
        'GridAccounts.Columns.RemoveAt(0)
        GridAccounts.Parent.Controls.Add(frm)
        frm.Attributes("RunAt") = "server"
        frm.Controls.Add(GridAccounts)

        frm.RenderControl(hw)
        Response.Write(sw.ToString())

        Response.End()

        'GridAccounts.Visible = False

    End Sub

    Protected Sub GridAccounts_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridAccounts.RowCommand
        If e.CommandName = "Select" Then

            sClientNum = GridAccounts.DataKeys(Convert.ToInt32(e.CommandArgument))("client_num").ToString()

            GridAccounts.SelectedIndex = -1

            Response.Redirect("./ClientDemo.aspx?ClientNum=" & sClientNum, True)


        End If

    End Sub

    Protected Sub btnReset_Click(sender As Object, e As System.EventArgs) Handles btnReset.Click
        'Appnumlbl.Text = ""
        Response.Redirect("./TicketDashboard.aspx", True)

    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As System.EventArgs) Handles btnSubmit.Click

        If categorycdddl.SelectedValue.ToLower <> "select" Then
            HDCategory.Text = categorycdddl.SelectedValue

        End If
        fillTable()

        
    End Sub

    Protected Sub categorycdddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles categorycdddl.SelectedIndexChanged
        If categorycdddl.SelectedValue.ToLower <> "select" Then
            HDCategory.Text = categorycdddl.SelectedValue

        End If

    End Sub

    Protected Sub Statusrbl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles Statusrbl.SelectedIndexChanged
        HDStatus.Text = Statusrbl.SelectedValue

    End Sub

    Protected Sub txtBeginDate_TextChanged(sender As Object, e As System.EventArgs) Handles txtBeginDate.TextChanged
        HDStartDate.Text = txtBeginDate.Text

    End Sub

    Protected Sub txtEndDate_TextChanged(sender As Object, e As System.EventArgs) Handles txtEndDate.TextChanged
        HDEnddate.Text = txtEndDate.Text
    End Sub

    Protected Sub btnTicketDetail_Click(sender As Object, e As System.EventArgs) Handles btnTicketDetail.Click
        Response.Redirect("~/TicketRFSReportDetails.aspx", True)
    End Sub
End Class
