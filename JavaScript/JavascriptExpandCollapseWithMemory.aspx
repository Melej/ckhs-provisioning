<%@ Page Language="VB" AutoEventWireup="false" CodeFile="JavascriptExpandCollapseWithMemory.aspx.vb" Inherits="JavaScript_JavascriptExpandCollapseWithMemory" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
    <script type="text/javascript">

// The Cookie Jar
////// Standard functions for Setting, Getting, and Deleting Cookies
////// john@xcentrixlc.com

// To set a cookie pass in the name, value, and persist type
// SetCookie("name", "value", 1) set the persist type to 1 to store your cookie
// or to 0 to last for this browser session only.  By default the storage time is for one year.
// You can add another argument to hold the length of time you want to store the cookie:
// SetCookie("name", "value",1,15768000000) would store the cookie for 6 months.
// the time is calcutaled in miliseconds so for one month of milliseconds you calculate:
// 31 * 24 * 60 * 60 * 1000=2678400000
// To get or delete a cookie simply pass in the name of the cookie you want to effect
// GetCookie("name")  DeleteCookie("name")
// if you ask for a cookie that does not exist it will return null.

function getCookieVal(offset) {
  endstr = document.cookie.indexOf (";", offset)
  if(endstr == -1) endstr = document.cookie.length
  return unescape(document.cookie.substring(offset, endstr))
}

function GetCookie(name) {
  arg = name + "="
  alen = arg.length
  clen = document.cookie.length
  var i = 0
  while (i < clen) {
    j = i + alen
    if(document.cookie.substring(i, j) == arg) return getCookieVal(j)
    i = document.cookie.indexOf(" ", i) + 1
    if(i == 0) break
  }
  return null
}

function SetCookie (name, value, per, exp) {
  cstr = name + "=" + escape(value) + ";"
  if(per){
    addtime=(exp>0) ? exp : 31536000000
    expdate = new Date()
    expdate.setTime(expdate.getTime() + addtime)
    expdate = expdate.toGMTString()
   cstr+=" expires=" + expdate
  }
  document.cookie = cstr
}

function DeleteCookie(name) {
  exp = new Date()
  exp.setTime (exp.getTime() - 1)
  cval = GetCookie(name)
  if(cval != null)
  document.cookie = name + "=" + cval + "; expires=" + exp.toGMTString()
}

function expandCollapse(id){
  var div = document.getElementById(id);
  if (div.style.display=='none'){
    div.style.display='block';
    SetCookie(id,'block');
  }
  else {
    div.style.display='none';
    SetCookie(id,'none');
  }
}

function setDisplay(div,disp){
  if (disp!=null) div.style.display=disp;
}

function setState(){
  var div;
  var i=1;
  while (div=document.getElementById('Div'+(i++))) {
     setDisplay(div,GetCookie(div.id));
  }
}
</script>

</head>
<body onload="setState()">

    <form id="form1" runat="server">
 
<p>
<a href="#" onclick="expandCollapse('Div1')">Expand/Collapse</a>
<div id ="Div1" style="display:block;">Div1 content</div>
</p>
<p>
<a href="#" onclick="expandCollapse('Div2')">Expand/Collapse</a>
<div id ="Div2" style="display:block;">Div2 content</div>
</p>
<p>
<a href="#" onclick="expandCollapse('Div3')">Expand/Collapse</a>
<div id ="Div3" style="display:block;">Div3 content</div>
</p>
</form>
</body>
</html>
