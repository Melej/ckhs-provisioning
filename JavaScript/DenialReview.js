﻿function canShowbtnViewEdits() {
    var theCount = 0;
    // var gridClientID = '<%= GridView1.ClientID %>';
    var gridClientID = "ctl00_ContentPlaceHolder1_GridView1";
    jQuery.each($("#" + gridClientID + " input[type='checkbox']"), function() {
        // if (this.checked == true);
        if ($(this).is(':checked')) {
            theCount++;
            // $(this).attr('checked', 'checked'); set checkbox to checked
            // $(this).attr('checked', ''); clear checkbox

        }
    });
    if (theCount > 0)
        $("#btnViewEdits").fadeIn(1000);
    else
        $("#btnViewEdits").fadeOut(1000);
    //     $('.checkbox').each(function() {
    //       
    //        //only toggle if not set
    //        if ($(this).hasClass('checked')) {

    //            if ($(this).is(':checked')) {
    //                theCount++;
    //            }

    //        }
    //        if (theCount > 0) { //make button visible
    //            $("#btnViewEdits").show();
    //        }
    //        else {// hide button
    //            $("#btnViewEdits").hide();
    //        }
    //    });

}
////function showDenialControls() {
////    if ($("#ctl00_ContentPlaceHolder1_chkShowOpen").is(':checked')) {
////        //multi mode show controls
////        $("#multiControls").show(2000);
////        $("#singleControls").hide();
////        $("#spanCaseInfo").hide();
////        //        if ($('#ctl00_ContentPlaceHolder1_GridView1 tr').filter(':not(:checked)') > 0) {
////        //            $("#btnViewEdits").show();
////        //        }
////    }
////    else {
////        location.href = "listcreator.aspx";
////        $("#multiControls").hide();
////        $("#singleControls").show(2000);
////        var rowCount = $('#ctl00_ContentPlaceHolder1_GridView1 tr').length;
////        if (rowCount < 1)
////            showThisForm("divInitial");
////    }

////}

function closeDenialDetail() {

    $("#divDenialDetails").hide(1000);
}
function showDenialForm() {
    var theForm = "divInitial";
    //Multimode only
    if ($("#ctl00_ContentPlaceHolder1_rblMode_0").is(':checked')) {
        theForm = "divDoctorReview"
        //        $("#divDoctorReview").hide();
        $("#divAppeals").hide();
        $("#divFinalOutcome").hide();
        $("#divInitial").hide();
    }
    if ($("#ctl00_ContentPlaceHolder1_rblMode_1").is(':checked')) {
        theForm = "divAppeals"
        $("#divDoctorReview").hide();
        //        $("#divAppeals").hide();
        $("#divFinalOutcome").hide();
        $("#divInitial").hide();
    }
    if ($("#ctl00_ContentPlaceHolder1_rblMode_2").is(':checked')) {
        theForm = "divFinalOutcome"
        $("#divDoctorReview").hide();
        $("#divAppeals").hide();
        //        $("#divFinalOutcome").hide();
        $("#divInitial").hide();
    }
    if ($("#ctl00_ContentPlaceHolder1_rblMode_3").is(':checked')) {
        theForm = "divInitial"
        $("#divDoctorReview").hide();
        $("#divAppeals").hide();
        $("#divFinalOutcome").hide();
    }

    //$("#ctl00$ContentPlaceHolder1$DateReviewedTextBox").datepick({ showOn: 'both', buttonImage: 'Images/jQueryCalendar.gif' });

    $("#" + theForm).draggable();
    $("#" + theForm).css("left", "170px");
    $("#" + theForm).css("top", "100px");

    //$("#" + theForm).css("height", "350px");
    $("#" + theForm).css("width", "50%");


    $("#" + theForm).css("overflow", "auto");
    $("#" + theForm).css("padding", "10px");
    $("#" + theForm).css("background-color", "silver");
    $("#" + theForm).css("border-width", 15);
    $("#" + theForm).css("border-style", "solid");
    $("#" + theForm).css("border-color", "#006666");
    $("#" + theForm).css("cursor", "hand");
    $("#" + theForm).css("opacity", "0.95");
    // $("#" + theForm).css("z-index", "999");
    $("#" + theForm).css("position", "absolute");
    $("#" + theForm).corner();
    $("#" + theForm).show(1000);
    //   $("btnViewEdits").corner();
    //   $("ctl00_ContentPlaceHolder1_btnClear").corner(); 
    $(".calenderClass").datepick({ showOn: 'both', buttonImage: 'Images/jQueryCalendar.gif' });
}
function closeTheForm(theForm) {

    $("#" + theForm).hide(1000);

}
//======show specific form
function showThisDenialForm(sForm) {
    var theForm = sForm;  //document.getElementById(sForm);
   
    $("#" + theForm).draggable();
    $("#" + theForm).css("left", "300px");
    $("#" + theForm).css("top", "150px");

    //$("#" + theForm).css("height", "350px");
    $("#" + theForm).css("width", "60%");


    $("#" + theForm).css("overflow", "auto");
    $("#" + theForm).css("padding", "10px");
    $("#" + theForm).css("background-color", "silver");
    $("#" + theForm).css("border-width", 15);
    $("#" + theForm).css("border-style", "solid");
    $("#" + theForm).css("border-color", "#006666");
    $("#" + theForm).css("cursor", "hand");
    $("#" + theForm).css("opacity", "0.95");
    // $("#" + theForm).css("z-index", "999");
    $("#" + theForm).css("position", "absolute");
    $("#" + theForm).corner();
    $("#" + theForm).fadeIn(1000);
    $(".calenderClass").datepick({ showOn: 'both', buttonImage: 'Images/jQueryCalendar.gif' });
}
// if ($("#ctl00_ContentPlaceHolder1_rblCaseDenialMode_0").is(':checked')) {
function showDenialControls() {
    // alert("showdenialcontrols");
    if ($("#ctl00_ContentPlaceHolder1_chkShowOpen").is(':checked')) {
        //multi mode show controls
        $("#multiControls").show(2000);
        $("#spanCaseInfo").fadeOut(1000);
        $("#singleControls").hide();
        //        if ($('#ctl00_ContentPlaceHolder1_GridView1 tr').filter(':not(:checked)') > 0) {
        //            $("#btnViewEdits").show();
        //        }
    }
    else {
        $("spanCaseInfo").show();
        location.href = "listcreator.aspx";
        $("#multiControls").hide();
        $("#singleControls").show(2000);
        var rowCount = $('#ctl00_ContentPlaceHolder1_GridView1 tr').length;
        if (rowCount < 1)
            setMenuDisplayDivForm('newdenial');
        //   showThisForm("divInitial");

    }

}
function setMenuDisplayDivForm(sStatus) {// show form set menu display by statusshowThisDenialForm
    var iCellsToShow;
    iCellsToShow = 1;
    $("#divDoctorReview").hide();
    $("#divAppeals").hide();
    $("#divFinalOutcome").hide();
   // if( sStatus !='pageload')
    $("#divInitial").hide();
    $("#divInsurance").hide();
    if (sStatus == 'newdenial') {
        showThisDenialForm('divInitial');
        iCellsToShow = 1;
        $('#ctl00_ContentPlaceHolder1_DenialDateTextBox').val('');
       }
   if (sStatus == 'initialstart') { showThisDenialForm('divDoctorReview'); iCellsToShow = 2; }
   if (sStatus == 'physicok') { showThisDenialForm('divAppeals'); iCellsToShow = 5; }
   if (sStatus == 'physicdec') { showThisDenialForm('divAppeals'); iCellsToShow = 5; }
   if (sStatus == 'denialappeal') { showThisDenialForm('divFinalOutcome'); iCellsToShow = 5; }
   if (sStatus == 'denialclosed') { showThisDenialForm('divFinalOutcome'); iCellsToShow = 5; }






   var tTable = document.getElementById("tblEditMenu"); // ("#tblEditMenu")
   var cell = tTable.rows[0].cells[3];
    for (var cellindex = 0; cellindex < 5; cellindex++) {
       // $('#' + tTable ).rows[0].cells[5]).css('display', 'none');
        // $($('#tblEditMenu').rows[0].cells[5]).css('display', 'none');
        cell = tTable.rows[0].cells[cellindex];
        if (cellindex > iCellsToShow) cell.style.display = 'none';
        else cell.style.display = 'block';
       }
   //cell.style.display = 'none';

}
function canShowbtnViewEdits() {
    var theCount = 0;
    // var gridClientID = '<%= GridView1.ClientID %>';
    var gridClientID = "ctl00_ContentPlaceHolder1_GridView1";
    jQuery.each($("#" + gridClientID + " input[type='checkbox']"), function() {
        // if (this.checked == true);
        if ($(this).is(':checked')) {
            theCount++;
            // $(this).attr('checked', 'checked'); set checkbox to checked
            // $(this).attr('checked', ''); clear checkbox

        }
    });
    if (theCount > 0)
        $("#btnViewEdits").fadeIn(1500);
    else
        $("#btnViewEdits").fadeOut(1500);
    //     $('.checkbox').each(function() {
    //       
    //        //only toggle if not set
    //        if ($(this).hasClass('checked')) {

    //            if ($(this).is(':checked')) {
    //                theCount++;
    //            }

    //        }
    //        if (theCount > 0) { //make button visible
    //            $("#btnViewEdits").show();
    //        }
    //        else {// hide button
    //            $("#btnViewEdits").hide();
    //        }
    //    });

}
//===================Grid Filter by selection================

var DenialReview_sPreviousFilterData = "";
function filterTheRows(iRow, iCell, sFilterValue) {


    var list1 = '';

    var cell1;

   var table1 = document.getElementById("ctl00_ContentPlaceHolder1_GridView1");
    //   var table1 = document.getElementById("tblTest");

    for (var loopindex = 1; loopindex < table1.rows.length; loopindex++) {
        row1 = table1.rows[loopindex];
        if (DenialReview_sPreviousFilterData == sFilterValue)
        { iRow = -1; }//-1 is indicates header row in gridview
        if (iRow < 0)//showall
        {
            row1.style.display = 'block';
            $(row1).find("td").eq(0).find(':checkbox').attr('checked', false);
        }
        else {


            if (row1.cells[iCell].innerHTML == sFilterValue) {
                row1.style.display = 'block';
                //check test

                //  $("#tbl1 input[type='checkbox']").attr('checked', true);
                $(row1).find("td").eq(0).find(':checkbox').attr('checked', true);
            }
            else {
               // if (iRow > 0) {
                    $(row1).find("td").eq(0).find(':checkbox').attr('checked', false);
                    row1.style.display = 'none';
               // }
            }

        }
    }
    if (DenialReview_sPreviousFilterData == sFilterValue)
        DenialReview_sPreviousFilterData = "";
    else
        DenialReview_sPreviousFilterData = sFilterValue;
    canShowbtnViewEdits()
}
//===================next================
function btnClearChecks_onclick() {
    //function filterTheRows(iRow, iCell, sFilterValue) {

    var iRow = 1;
    var list1 = '';
    var iCell = 0;

    var cell1;

    var table1 = document.getElementById("ctl00_ContentPlaceHolder1_GridView1");
    //   var table1 = document.getElementById("tblTest");

    for (var loopindex = 1; loopindex < table1.rows.length; loopindex++) {
        row1 = table1.rows[loopindex];
        //  if (DenialReview_sPreviousFilterData == sFilterValue)
        //  { iRow = -1; }//-1 is indicates header row in gridview
        if (loopindex > 0)//showall
        {
//            if ($(row1).find("td").eq(0).find(':checkbox').attr('checked') == true) {
//                //row1.style.display = 'block';
//                $(row1).find("td").eq(0).find(':checkbox').attr('checked', false);
//            }
//            else {
//                $(row1).find("td").eq(0).find(':checkbox').attr('checked', true);
            //            }
            $(row1).find("td").eq(0).find(':checkbox').attr('checked', false);
        }
    }
    canShowbtnViewEdits();
}
////$(function() { // this line makes sure this code runs on page load
////    $('#checkall').click(function() {
////        $(this).parents('fieldset:eq(0)').find(':checkbox').attr('checked', this.checked);
////    });
////});
