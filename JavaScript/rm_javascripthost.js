var rootURL = "";
var t;
var username;
var password;
var loginPageLoaded = false;
var failedLoginMessage = "";
var admin1 = false;


var saveAfterOpen = false;

//=============================================================================
function login() {
//=============================================================================
	t = setTimeout('loginCheck()',30000);
	//rootURL = _rootURL;
	rootURL = document.getElementById("inputROOTURL").value;
    admin1 = false;
	var cermeFrame = document.getElementById('cerme');
	username = document.getElementById("username").getAttribute('value');
	password = document.getElementById("password").getAttribute('value');
  	try {
		if (!loginPageLoaded) {
			loadLoginPage();
			return;
		}
		submitLogin();
	} catch (ex) {
		clearTimeout(t);
		alert('Unable to access login page - User already Logged In');
	}

}

//=============================================================================
function adminlogin() {
//=============================================================================
	t = setTimeout('loginCheck()',30000);
	//rootURL = _rootURL;
	admin1 = true;
	rootURL = document.getElementById("inputROOTURL").value;

	var cermeFrame = document.getElementById('cerme');
	username = document.getElementById("username").getAttribute('value');
	password = document.getElementById("password").getAttribute('value');
  	try {
		if (!loginPageLoaded) {
			loadAdminLoginPage();
			return;
		}
		submitAdminLogin();
	} catch (ex) {
		clearTimeout(t);
		alert('Unable to access login page - User already Logged In');
	}

}

//=============================================================================
function loginSingleSignOn() {
//=============================================================================
	t = setTimeout('loginCheck()',30000);
	//rootURL = _rootURL;
	rootURL = document.getElementById("inputROOTURL").value;

	var cermeFrame = document.getElementById('cerme');
	 cermeFrame.src = rootURL + '/iqm/html/gateway?method=sectionmap&SectionType=Integration&responseFormat=HTML&authenticate=Y&integration=JavaScript'
	//cermeFrame.src = rootURL + '/iqm/html/gateway?method=sectionmap&SectionType=Integration&responseFormat=HTML&authenticate=Y&integration=JavaScript&unified=kJhBn%2BLjGX29ChmbV3CBJ6PhpiCUFicPmNMARtwhLEBOlPNzGaEDUmJkBpE42C7W9UdMuqdq1Q7oLgOTVK6ORKBypW1nzsyqiUD5UABIqXNljgxGfIcLNjeIsv02E5jkiYLeKMr51f96xnb9tH7Smg%3D%3D'
	return;

	try {
		if (!loginPageLoaded) {
			loadLoginPage();
			return;
		}
		submitLogin();
	} catch (ex) {
		clearTimeout(t);
		alert('Unable to access login page - server may not be available');
	}

}
//=============================================================================
function loginCheck() {
//=============================================================================
	//alert('There has been no response from the server for 30 seconds - unable to continue');
}

//=============================================================================
function loadLoginPage() {
//=============================================================================
	if (!loginPageLoaded) {
		var cermeFrame = document.getElementById('cerme');
		//cermeFrame.src = rootURL + "/login?&integration=javascript";
		cermeFrame.src = rootURL + "/login?integration=javascript";
	}
}

//=============================================================================
function loadAdminLoginPage() {
//=============================================================================
	if (!loginPageLoaded) {
		var cermeFrame = document.getElementById('cerme');
		//cermeFrame.src = rootURL + "/login?&integration=javascript";
		cermeFrame.src = rootURL + "/admin?integration=javascript";
	}
}

//=============================================================================
// called on the onload event of the index.htm page - form is loaded
function submitLogin() {
//=============================================================================
	loginPageLoaded = true;
	username = document.getElementById("username").getAttribute('value');
	password = document.getElementById("password").getAttribute('value');
	cerme.jsLogin(username,password);
}

//=============================================================================
// called on the onload event of the index.htm page - form is loaded
function submitAdminLogin() {
//=============================================================================
	loginPageLoaded = true;
	username = document.getElementById("username").getAttribute('value');
	password = document.getElementById("password").getAttribute('value');
	
	cerme.jsAdminLogin(username,password);
	
	
}

//=============================================================================
function saveCompleted() {
//=============================================================================
	try {
		cerme.hostSaveCompleted();
	} catch (ex) {
		noObject('saveCompleted');
	}
}
// This Method opens and saves  Review XML with custom changes , both open and 
// save operations are done in background...
// NOTE : saveAfterOpen must be true BEFORE jsOpenReview is called to achieve
// this else it just opens the review
// 1.Opens a review
// 2.On Review open  CERM_ReviewOpened is called 
// 3. CERM_ReviewOpened saves the Review XML if saveAfterOpen =  true
// 
//=============================================================================
function openReviewAndSave() {
//=============================================================================
	saveAfterOpen = true;
	openReview();
	
}

//=============================================================================
function CERM_ReviewOpened(CERMFrame) {
//=============================================================================
	if (saveAfterOpen) {
		saveReview();
		saveAfterOpen = false;
	}
	else {
		alert('CERM_ReviewOpened for frame ' + CERMFrame.getAttribute('name'));
	}
}

//=============================================================================
function newReview() {
//=============================================================================
	
	try {
		var ele = document.getElementById('newReview_ParamsXML');
		var paramsXML = ele.innerText;
		
		ele = document.getElementById('newReview_ReviewXML');
		var reviewXML = ele.innerText;
		
		cerme.jsNewReview(paramsXML,reviewXML);
		
	} catch (ex) {
		noObject('newReview');
	}
}

//=============================================================================
function openReview() {
//=============================================================================

	try {
		var ele = document.getElementById('openReview_ReviewCID');
		var reviewCID = ele.getAttribute('value');
		if (reviewCID == '') {
			reviewCID = null
		}

		ele = document.getElementById('openReview_ParamsXML');
		var paramsXML = ele.innerText;

		ele = document.getElementById('openReview_ReviewXML');
		var reviewXML = ele.innerText;
		
		cerme.jsOpenReview(reviewCID,reviewXML,paramsXML);
		
	} catch (ex) {
		noObject('openReview');
	}
}

//=============================================================================
function Reports() {
//=============================================================================
	
	try {
		cerme.jsReports();
		
	} catch (ex) {
		noObject('Reports');
	}
}

//=============================================================================
function Reports2(reportType) {
//=============================================================================
	
	try {
		cerme.jsReports2(reportType);
		
	} catch (ex) {
		noObject('Reports2');
	}
}

//=============================================================================
function openABReview() {
//=============================================================================
	try {
		var ele = document.getElementById('abusername');
		var abUserName = ele.getAttribute('value');
		
		var ele = document.getElementById('abreviewid');
		var abReviewID = ele.getAttribute('value');
		
		var ele = document.getElementById('abreviewxml');
		var abReviewXML = ele.getAttribute('value');
		
		cerme.jsOpenABReview(abUserName,abReviewID,abReviewXML);
		
	} catch (ex) {
		noObject('openABReview');
	}
}
//=============================================================================
function logout(_rootURL) {
//=============================================================================
	try {
	if(admin1)
		cerme.adminlogout(_rootURL);
		else
		cerme.logout(rootURL);
		loginPageLoaded = false;
	} catch (ex) {
		noObject('logout');
	}
}

//=============================================================================
function okToCloseReview() {
//=============================================================================
	try {
		cerme.jsOkToCloseReview();
	} catch(ex) {
		noObject('okToCloseReview');
	}
}

//=============================================================================
function okToSaveReview() {
//=============================================================================
	try {
		cerme.jsOKToSave();
	} catch (ex) {
		noObject('okToSaveReview');
	}
}

//=============================================================================
function saveReview() {
//=============================================================================
	try {
	cerme.saveReview();
	} catch(ex) {
		noObject('saveReview');
	}
}

//=============================================================================
function completeReview() {
//=============================================================================
	try {
	
	var ele = document.getElementById('newReview_ParamsXML');
	var paramsXML = ele.innerText;

	cerme.jsCompleteReview(paramsXML);
	} catch(ex) {
		noObject('completeReview');
	}
}


//=============================================================================
function closeReview() {
//=============================================================================
	try {
		cerme.closeReview(true);
	} catch (ex) {
		noObject('closeReview');
	}
}

//=============================================================================
function bookView() {
//=============================================================================
	try {
		var ele = document.getElementById('bookView_ParamsXML');
		var paramsXML = ele.innerText;
		cerme.jsBookView(paramsXML);
	} catch (ex) {
		noObject('bookView');
	}
}

//=============================================================================
function getCurrentOutcome() {
//=============================================================================
	try {
		cerme.getCurrentOutcome();
	} catch (ex) {
		noObject('getCurrentOutcome');
	}
	
	
}

//=============================================================================
function printReviewSummary() {
//=============================================================================
	try {
		cerme.printReviewSummary();
	} catch (ex) {
		noObject('printReviewSummary');		
	}
}

//	the following function correspond to the events in the activeX
//=============================================================================
function CERM_LoginResult(CERMFrame, success) {
//=============================================================================
   // if success == false , then the actual message text is 
   // populated in  failedLoginMessage
   	clearTimeout(t);
   	if (success == 'false')
	alert('CERM_LoginResult for frame ' + CERMFrame.getAttribute('name') + ' -- ' + success + '\n Description: '+ failedLoginMessage);
	else
	alert('CERM_LoginResult for frame ' + CERMFrame.getAttribute('name') + ' -- ' + success );
}

//	the following function correspond to the events in the activeX
//=============================================================================
function CERM_AdminLoginResult(CERMFrame, success) {
//=============================================================================
   // if success == false , then the actual message text is 
   // populated in  failedLoginMessage
   	clearTimeout(t);
   	if (success == 'false')
	alert('CERM_AdminLoginResult for frame ' + CERMFrame.getAttribute('name') + ' -- ' + success + '\n Description: '+ failedLoginMessage);
	else
	alert('CERM_AdminLoginResult for frame ' + CERMFrame.getAttribute('name') + ' -- ' + success );
}

//=============================================================================
function CERM_Logout(CERMFrame) {
//=============================================================================
	alert('CERM_Logout for frame ' + CERMFrame.getAttribute('name'));
}

//=============================================================================
function CERM_SaveCompleted(CERMFrame, reviewCID, reviewXML) {
//=============================================================================
	alert('CERM save completed for frame ' + CERMFrame.getAttribute('name'));
	var reviewCIDEle = document.getElementById('openReview_ReviewCID');
	reviewCIDEle.setAttribute('value',reviewCID);
	
	var reviewXMLEle = document.getElementById('openReview_ReviewXML');
	reviewXMLEle.innerText = reviewXML;
}

//=============================================================================
function CERM_Error(CERMFrame, errCode, errText, errDataXML) {
//=============================================================================
	alert('error in frame ' +  CERMFrame.getAttribute('name') + ' -- ' + errText);
}

//=============================================================================
function CERM_ReviewOKToClose(CERMFrame, okToClose) {
//=============================================================================
	alert('CERM_ReviewOKToClose() for frame ' +  CERMFrame.getAttribute('name') + ' -- ' + okToClose);
}

//=============================================================================
function CERM_ReviewOKToSave(CERMFRAME, okToSave) {
//=============================================================================
	
	alert('CERM_ReviewOKToSave() for frame ' + CERMFRAME.getAttribute('name') + ' -- ' + okToSave);
}

//=============================================================================
function CERM_ReviewClosed(CERMFrame) {
//=============================================================================
	alert('CERM_ReviewClosed for frame ' + CERMFrame.getAttribute('name'));
}

//=============================================================================
function CERM_CurrentOutcome(CERMFrame, outcomeType, additionalReview, outcomeCID, outcomeDesc, reviewDesc) {
//=============================================================================
	alert('CurrentOutcome\n\noutcomeType = ' + outcomeType + '\nadditionalReview = ' + additionalReview + '\noutcomeCID = ' + outcomeCID + '\noutcomeDesc = ' + outcomeDesc + '\nreviewDesc = ' + reviewDesc);
}


function noObject (ob) {
	var CERMFrame = document.getElementById('cerme');
	var errText = 'Unable to complete request - object ' + ob + ' is not available';
	errText += '\nPossible cause - Login failure, server not available';
	errText += '\nYou may need to have a review open to complete this action';
	CERM_Error(CERMFrame, "", errText, "");
}