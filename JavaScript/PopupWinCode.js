﻿// JScript File

// This is the javascript library of the CSP site

var xPos = 605;
var yPos = 117;
var POPUP_WIDTH = 800;
var POPUP_HEIGHT = 600;
 
/**
 * The Function opens new popup window with default size 800x600 without menubar,toolbar,location and status.
 * New window ist displayed in the center of screen.
 */
function openPopupWindow(url, wndName, width, height, param) {
  var params;

  if(wndName == null) wndName = "eHealtPopupWnd";

  if(width == null) width = POPUP_WIDTH;	//default
  if(height == null) height = POPUP_HEIGHT;	//default

  if(param == null) {
    params = "dependent=1,scrollbars=1,location=0,directories=0,status=0,menubar=0,toolbar=0,resizable=1";
  } else {
    params = param;
  }
  params = params + ",width="+width+",height="+height+",left="+(screen.availWidth-width)/2+",top="+(screen.availHeight-height)/2;

  if (window.popupWnd && !window.popupWnd.closed) {
  	//window.popupWnd.close();
  } else {
	window.popupWnd = window.open(url, wndName, params, true);
  }
  window.popupWnd.focus();
}


function openPopUp(purl, pname, pheight, pwidth, scrollb) { 
  newWindow = window.open (purl,pname,'"alwaysLowered=0,alwaysRaised=yes,dependent=no,directories=0,fullscreen=0,hotkeys=1,location=0,menubar=0,resizable=1,scrollbars='+scrollb+'status=0,titlebar=1,toolbar=0,z-lock=0,top='+yPos+',left='+xPos+',height='+pheight+',width='+pwidth+'"');
}
			
function openPopUpHelp(purl) {
  openPopUp(purl, '_help', 234, 378, 1);
}

function ticketCreateSelected(disabled) {
  if (disabled == true) {
		document.getElementById("userPoNumber").value = "";
		document.getElementById("userPoNumber").className="formTextDisabled";	
		document.getElementById("userPoNumber").disabled = true;
		document.getElementById("poDesc1").className="formItemDescriptorDisabled";
		document.getElementById("poDesc2").className="formItemDescriptorDisabled";
	} else {
		document.getElementById("userPoNumber").className="formTextInput";	
		document.getElementById("userPoNumber").disabled = false;
		document.getElementById("poDesc1").className="formItemDescriptor";
		document.getElementById("poDesc2").className="formItemDescriptor";		
	}
}			

function submitFormWithAction(formName, actionName) {
  form = document.getElementById(formName);
  form.elements.action.value = actionName;
  form.submit();
}

function submitKBForm(theForm, materialNumber, logUID, logCID) {
  windowName = 'report_window';
  windowURL  = 'CSPKnowledgeBaseDisplay?Search=' + theForm.Search.value + '&MaterialNumber=' + materialNumber + '&logUID=' + logUID + '&logCID=' + logCID;
  windowOpt  = 'width=640,height=480,location=no,toolbar=no,directories=no,scrollbars=yes';
  myWindow = window.open(windowURL, windowName, windowOpt);
  myWindow.focus();
}

function openSTRWindow(windowURL) {
  windowName = 'str_report_window';
  windowOpt  = 'width=800,height=600,location=no,toolbar=no,directories=no,scrollbars=yes,resizable=yes';
  myWindow = window.open(windowURL, windowName, windowOpt);
  myWindow.focus();
}

function submitFormOnEnter(e, theForm) {
	var characterCode
	
	if(e && e.which) { //if which property of event object is supported (NN4)
		e = e
		characterCode = e.which //character code is contained in NN4's which property
	} else {
		e = event
		characterCode = e.keyCode //if generated character code is equal to ascii 13 (if enter key)
	}

	if (characterCode == 13) { //if generated character code is equal to ascii 13 (if enter key)
	  theForm.submit();
		return false
	} else {
		return true
	}
}

/** Checks whether the content of the field exceeds the
 * defined maxlimit and truncates the field accordingly.
 * The function is especially useful for textarea elements. 
 * Call the function as handler for "onkeyup" or "onkeydown" actions
 */
function limitFieldLength(field, maxlimit) {
	if (field == null) return;
	var text = replaceSubstring(field.value, "\r\n", "\n");
  if (text.length > maxlimit) {
    // if too long...trim it!
    field.value = text.substr(0, maxlimit);
  }
}

function replaceSubstring(inputString, fromString, toString) {
	// Goes through the inputString and replaces every occurrence of fromString with toString
	var temp = inputString;
	if (fromString == "") {
		return inputString;
	}
  if (toString.indexOf(fromString) == -1) { // If the string being replaced is not a part of the replacement string (normal situation)
    while (temp.indexOf(fromString) != -1) {
      var toTheLeft = temp.substring(0, temp.indexOf(fromString));
      var toTheRight = temp.substring(temp.indexOf(fromString)+fromString.length, temp.length);
      temp = toTheLeft + toString + toTheRight;
    }
  } else { // String being replaced is part of replacement string (like "+" being replaced with "++") - prevent an infinite loop
    var midStrings = new Array("~", "`", "_", "^", "#");
    var midStringLen = 1;
    var midString = "";
    // Find a string that doesn't exist in the inputString to be used
    // as an "inbetween" string
    while (midString == "") {
      for (var i=0; i < midStrings.length; i++) {
        var tempMidString = "";
        for (var j=0; j < midStringLen; j++) { tempMidString += midStrings[i]; }
        if (fromString.indexOf(tempMidString) == -1) {
          midString = tempMidString;
          i = midStrings.length + 1;
        }
      }
    } // Keep on going until we build an "inbetween" string that doesn't exist
    // Now go through and do two replaces - first, replace the "fromString" with the "inbetween" string
    while (temp.indexOf(fromString) != -1) {
      var toTheLeft = temp.substring(0, temp.indexOf(fromString));
      var toTheRight = temp.substring(temp.indexOf(fromString)+fromString.length, temp.length);
      temp = toTheLeft + midString + toTheRight;
    }
    // Next, replace the "inbetween" string with the "toString"
    while (temp.indexOf(midString) != -1) {
      var toTheLeft = temp.substring(0, temp.indexOf(midString));
      var toTheRight = temp.substring(temp.indexOf(midString)+midString.length, temp.length);
      temp = toTheLeft + toString + toTheRight;
    }
  } // Ends the check to see if the string being replaced is part of the replacement string or not
  return temp; // Send the updated string back to the user
} // Ends the "replaceSubstring" function



/** Handles selection of a title in the selection box for academic titles.
 * If the option "Other..." is selected then the textfield for user value
 * is made visible. Renames the selection and textfield components accordingly
 * in order to prevent two values for ProfileConstants.TITLE being sent 
 * in the request.
 */
function academicTitleChanged() {
  var textField = document.getElementById("personTitleText");	
  var selectField = document.getElementById("personTitleSelect")
  var paramName = "personTitle"; // name of the parameter expected by a command
  if (selectField != null && selectField != null) {                 
	  //alert("Selectedindex: " + personTitleSelect.selectedIndex);
	  if (selectField.selectedIndex == selectField.length - 1) {                                     
	    textField.style.visibility = "visible";
	    selectField.name = paramName + "_select";
	    textField.name = paramName;
	  } else {
	    textField.style.visibility = "hidden";
	    selectField.name = paramName;
	    textField.name = paramName + "_text";					                    
	    if (selectField.selectedIndex == 0) {
	    	textField.value = "";
	    }
	  }                  
	}
}



