﻿Imports System.IO
Imports System.Data '
Imports System.Data.SqlClient
Imports App_Code
Imports AjaxControlToolkit
Imports C1.Web.UI.Controls.C1Window
Imports System.Net
Imports System.Reflection
Partial Class CSCSurvey
    Inherits System.Web.UI.Page
    Dim FormSignOn As New FormSQL
    Dim StatusForm As New FormSQL
    Dim DemoForm As New FormSQL()

    Dim UserInfo As UserSecurity
    Dim bHasError As Boolean

    Dim Mailobj As System.Net.Mail.SmtpClient = New System.Net.Mail.SmtpClient
    Dim Mailmsg As New System.Net.Mail.MailMessage
    Dim MailAddress As System.Net.Mail.MailAddress
    Dim mailAttachment As System.Net.Mail.Attachment

    Dim RequestNum As Integer
    Dim RequestTickect As RequestTicket
    Dim TicketstatusClass As TicketStatus

    Dim iClientNum As Integer
    Dim iSecurityLevel As Integer
    Dim iTechNum As Integer

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        UserInfo = Session("objUserSecurity")
        RequestNum = Request.QueryString("RequestNum")

        RequestNumLabel.Text = Request.QueryString("RequestNum")
        nt_login.Text = Session("LoginID")

        WhoclientNumLabel.Text = UserInfo.ClientNum
        entryclientnumLabel.Text = UserInfo.ClientNum
        iTechNum = UserInfo.TechNum
        entrytechnumLabel.Text = UserInfo.TechNum
        SecurityLevelLabel.Text = UserInfo.SecurityLevelNum


        RequestTickect = New RequestTicket(RequestNum, Session("CSCConn"))
        TicketstatusClass = New TicketStatus(RequestNum, Session("CSCConn"))

        Dim Client3Contr As New Client3Controller()
        Dim ctrlRequest As New RequestEntryController()

        FormSignOn = New FormSQL(Session("CSCConn"), "selTicketRFSByNumV7", "InsClientSurveyV8", "", Page)
        FormSignOn.AddSelectParm("@requestnum", RequestNum, SqlDbType.Int, 6)

        StatusForm = New FormSQL(Session("CSCConn"), "SelClientSurvey", "UpdTicketRFSObjectV8", "", Page)
        StatusForm.AddSelectParm("@requestnum", RequestNum, SqlDbType.Int, 6)

        'SelClientSurvey

        Dim dbAccess As New CkhsEmployeeDbAccess(Session("EmployeeConn"))
        Dim ckhsEmployee = dbAccess.GetCkhsEmployee(iClientNum)

        'If Session("LoginID").ToString.ToLower = "melej" Then
        '    connid.Visible = True
        '    connid.Text = "CSC Conn " & Session("CSCConn").ToString & "Employee Conn " & Session("EmployeeConn").ToString

        'End If
        If Request.QueryString("label") <> "" Then ' this comes from the update of client calls back to this page line 2748
            lblValidation.Text = Request.QueryString("label")
        End If
        If Not IsPostBack Then

            FormSignOn.FillForm()
            StatusForm.FillForm()

            categorycdLabel.Text = RequestTickect.categorycd
            typecdLabel.Text = RequestTickect.typecd
            itemcdLabel.Text = RequestTickect.itemcd
            clientnumLabel.Text = RequestTickect.clientnum
            currenttechnumLabel.Text = RequestTickect.currenttechnum
            currentgroupnumLabel.Text = RequestTickect.currentgroupnum
            'completiondateTextBox.Text = RequestTickect.completiondate
            'opentimelb.Text = RequestTickect.TotalAcknowledgeTime
            hdPriorityLb.Text = RequestTickect.priority
            hdexists.Text = RequestTickect.Surveyexists

            Dim status As String

            status = RequestTickect.currentacktime


            Dim ddlBinder As New DropDownListBinder
            Dim ddlTypeBinder As New DropDownListBinder

            ddlBinder.BindData(entitycdDDL, "selEntityCodesV4", "entity_cd", "hosp_desc", Session("EmployeeConn"))




            If entity_cdLabel.Text = "" Then
                entitycdDDL.SelectedIndex = -1
            Else
                Dim entitycd As String = entity_cdLabel.Text

                entitycd = entitycd.Trim
                entitycdDDL.SelectedValue = entitycd

            End If


            Dim ddbDepartmentBinder As New DropDownListBinder(departmentcdddl, "selDepartmentCodesV4", Session("EmployeeConn"))
            ddbDepartmentBinder.AddDDLCriteria("@entity_cd", entitycdDDL.SelectedValue, SqlDbType.NVarChar)
            ddbDepartmentBinder.BindData("department_cd", "department_name")

            If department_cdLabel.Text = "" Then
                departmentcdddl.SelectedIndex = -1
            ElseIf department_cdLabel.Text <> "" Then
                Try

                    departmentcdddl.SelectedValue = department_cdLabel.Text.Trim
                Catch ex As Exception

                    departmentcdddl.SelectedIndex = -1
                End Try

            End If

            ddlBinder.BindData(facilitycdddl, "SelFacilityCodes", "facility_cd", "facility_short_name", Session("EmployeeConn"))


            If facility_cdLabel.Text = "" Then
                facilitycdddl.SelectedIndex = -1
            ElseIf facility_cdLabel.Text <> "" Then
                Try
                    facilitycdddl.SelectedValue = facility_cdLabel.Text

                Catch ex As Exception
                    facilitycdddl.SelectedIndex = -1

                End Try
            End If


            If building_cdLabel.Text = "" Then
                buildingcdddl.SelectedIndex = -1
            ElseIf building_cdLabel.Text <> "" Then

                Try

                    ddlBinder = New DropDownListBinder(buildingcdddl, "SelBuildingByFacility", Session("EmployeeConn"))
                    ddlBinder.AddDDLCriteria("@facility_cd", facility_cdLabel.Text, SqlDbType.NVarChar)
                    ddlBinder.BindData("building_cd", "building_name")

                    buildingcdddl.SelectedValue = building_cdLabel.Text.Trim

                Catch ex As Exception

                    buildingcdddl.SelectedIndex = -1

                End Try



            End If


            If building_cdLabel.Text <> "" And facility_cdLabel.Text <> "" And floorLabel.Text <> "" Then

                Try
                    Dim ddlFloorBinder As DropDownListBinder

                    ddlFloorBinder = New DropDownListBinder(Floorddl, "SelFloorsByBuilding", Session("EmployeeConn"))
                    ddlFloorBinder.AddDDLCriteria("@facility_cd", facility_cdLabel.Text, SqlDbType.NVarChar)
                    ddlFloorBinder.AddDDLCriteria("@building_cd", building_cdLabel.Text, SqlDbType.NVarChar)

                    ddlFloorBinder.BindData("floor_no", "floor_no")

                    Floorddl.SelectedValue = floorLabel.Text.TrimEnd


                Catch ex As Exception
                    Floorddl.SelectedValue = -1
                End Try
            Else
                Floorddl.SelectedValue = -1

            End If

            ddlBinder = New DropDownListBinder(categorycdddl, "SelCategoryCodes", Session("CSCConn"))
            ddlBinder.AddDDLCriteria("@securitylevel", SecurityLevelLabel.Text, SqlDbType.SmallInt)
            ddlBinder.BindData("category_cd", "Category_Desc")

            If categorycdLabel.Text <> "" Then
                Try
                    categorycdddl.SelectedValue = categorycdLabel.Text.Trim
                Catch ex As Exception
                    categorycdddl.SelectedIndex = -1
                End Try

            ElseIf categorycdLabel.Text = "" Then
                categorycdLabel.Text = "other"
                categorycdddl.SelectedValue = categorycdLabel.Text.Trim

            End If



            ddlTypeBinder = New DropDownListBinder(typecdddl, "SelCategoryType", Session("CSCConn"))
            ddlTypeBinder.AddDDLCriteria("@Category_cd", categorycdLabel.Text, SqlDbType.NVarChar)


            ddlTypeBinder.BindData("type_cd", "CatagoryTypeDesc")
            If typecdLabel.Text <> "" Then
                Try
                    typecdddl.SelectedValue = typecdLabel.Text.Trim.ToLower
                Catch ex As Exception
                    typecdddl.SelectedIndex = -1
                End Try
            ElseIf typecdLabel.Text = "" Then
                typecdLabel.Text = "other"
                typecdddl.SelectedValue = typecdLabel.Text.Trim
            End If



        End If

        If hdexists.Text = RequestNumLabel.Text Then
            btnSubmit.Visible = False
            Dim ctrl As Control = DataCheckingAndValidation.SetChildControlsRecursiveToEnabledFalse(maintbl, False)

        End If


        'If currstatuscdLabel.Text = "resols" Then

        '    Dim ctrl As Control = DataCheckingAndValidation.SetChildControlsRecursiveToEnabledFalse(uplTicket, False)
        '    btnSubmit.Text = "ReOpen Ticket/RFS"

        '    btnSubmit.Enabled = True


        'ElseIf CloseDateLabel.Text <> "" Then
        '    Dim ctrl As Control = DataCheckingAndValidation.SetChildControlsRecursiveToEnabledFalse(uplTicket, False)


        'End If



    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As System.EventArgs) Handles btnSubmit.Click
        Dim thisData As New CommandsSqlAndOleDb("InsClientSurveyV8", Session("CSCConn"))

        thisData.AddSqlProcParameter("@request_num", RequestNum, SqlDbType.Int)

        thisData.AddSqlProcParameter("@courteous", courteousrbl.SelectedValue, SqlDbType.TinyInt)
        thisData.AddSqlProcParameter("@knowledgeable", knowledgeablerbl.SelectedValue, SqlDbType.TinyInt)
        thisData.AddSqlProcParameter("@expectations", expectationsrbl.SelectedValue, SqlDbType.TinyInt)
        thisData.AddSqlProcParameter("@timely", timelyrbl.SelectedValue, SqlDbType.TinyInt)

		thisData.AddSqlProcParameter("@comments", commenttextbox.Text, SqlDbType.NVarChar, 500)

		thisData.AddSqlProcParameter("@nt_login", nt_login.Text, SqlDbType.NVarChar, 50)

        thisData.ExecNonQueryNoReturn()

        Response.Redirect("./ViewRequestTicket.aspx?RequestNum=" & RequestNum, True)

    End Sub

    Protected Sub btnReturn_Click(sender As Object, e As System.EventArgs) Handles btnReturn.Click
        Response.Redirect("~/ViewRequestTicket.aspx?RequestNum=" & RequestNum, True)

    End Sub
End Class
