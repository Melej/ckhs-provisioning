﻿Imports App_Code

Partial Class KeywordSearch
    Inherits System.Web.UI.Page





    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load




    End Sub

    Protected Sub btnSearch_Click(sender As Object, e As System.EventArgs) Handles btnSearch.Click

        Dim thisData As New CommandsSqlAndOleDb("SelRequestTicket", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@SelectType", "Open", SqlDbType.NVarChar, 12)



        'gvUnassTickets.DataSource = thisData.GetSqlDataTable
        'gvUnassTickets.DataBind()

        pnlSearch.Controls.Add(TicketQueue(thisData.GetSqlDataTable))


    End Sub
    Public Function TicketQueue(ByRef dtTable As DataTable) As Table
        Dim tblReturn As New Table

        tblReturn.Width = New Unit("100%")
        tblReturn.BackColor = Color.White

        tblReturn.CssClass = "Requests"
        If dtTable IsNot Nothing Then

            For Each dr As DataRow In dtTable.Rows

                Dim rwHeader As New TableHeaderRow
                Dim clTicketNum As New TableCell
                Dim clTicketDate As New TableCell

                Dim rwUser As New TableRow
                Dim clUserHeader As New TableCell
                Dim clUser As New TableCell
                Dim clUserEntityHeader As New TableCell
                Dim clUserEntity As New TableCell
                Dim clDepartmentHeader As New TableCell
                Dim clDepartment As New TableCell



                Dim rwCategory As New TableRow

                Dim clCatetoryHeader As New TableCell
                Dim clCategory As New TableCell
                Dim clTypeHeader As New TableCell
                Dim clType As New TableCell
                Dim clItemHeader As New TableCell
                Dim clItem As New TableCell




                Dim rwSitutation As New TableRow
                Dim clSituationHeader As New TableCell
                Dim clSituation As New TableCell


                clTicketNum.Text = "<a href='ViewRequestTicket.aspx?RequestNum=" & dr("RequestNum") & "'>" & dr("RequestNum") & " Priority " & dr("Priority") & "</a>"
                clTicketNum.ColumnSpan = 3
                clTicketDate.Text = dr("DateEntered")
                clTicketDate.ColumnSpan = 3


                clTicketDate.HorizontalAlign = HorizontalAlign.Right

                clUserHeader.Text = "User:"
                clUserHeader.CssClass = "displayCell"
                clUser.Text = dr("RequestorName")

                clUserEntityHeader.Text = "Location:"
                clUserEntityHeader.CssClass = "displayCell"
                clUserEntity.Text = dr("facility_name")

                clDepartmentHeader.Text = "Department:"
                clDepartmentHeader.CssClass = "displayCell"
                clDepartment.Text = "department place holder'"


                rwUser.Cells.Add(clUserHeader)
                rwUser.Cells.Add(clUser)
                rwUser.Cells.Add(clUserEntityHeader)
                rwUser.Cells.Add(clUserEntity)
                rwUser.Cells.Add(clDepartmentHeader)
                rwUser.Cells.Add(clDepartment)


                clCatetoryHeader.Text = "Category:"
                clCatetoryHeader.CssClass = "displayCell"
                clCategory.Text = IIf(IsDBNull(dr("CategoryTypeCd")), "", dr("CategoryTypeCd"))

                clTypeHeader.Text = "Type:"
                clTypeHeader.CssClass = "displayCell"
                clType.Text = IIf(IsDBNull(dr("TypeCd")), "", dr("TypeCd"))

                clItemHeader.Text = "Item:"
                clTypeHeader.CssClass = "displayCell"
                clItem.Text = IIf(IsDBNull(dr("ItemCd")), "", dr("ItemCd"))


                rwCategory.Cells.Add(clCatetoryHeader)
                rwCategory.Cells.Add(clCategory)
                rwCategory.Cells.Add(clTypeHeader)
                rwCategory.Cells.Add(clType)
                rwCategory.Cells.Add(clItemHeader)
                rwCategory.Cells.Add(clItem)


                clSituationHeader.Text = "Situation:"
                clSituationHeader.CssClass = "displayCell"

                clSituation.Text = dr("ShortDesc")
                clSituation.ColumnSpan = 5

                rwSitutation.Cells.Add(clSituationHeader)
                rwSitutation.Cells.Add(clSituation)

                rwHeader.CssClass = "header"
                'rwHeader.Font.Size = "16"

                rwHeader.Cells.Add(clTicketNum)
                rwHeader.Cells.Add(clTicketDate)




                tblReturn.Rows.Add(rwHeader)
                tblReturn.Rows.Add(rwUser)
                tblReturn.Rows.Add(rwCategory)
                tblReturn.Rows.Add(rwSitutation)






            Next





        End If






        Return tblReturn
    End Function
End Class
