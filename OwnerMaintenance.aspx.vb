﻿Imports System.IO
Imports System.Data '
Imports System.Data.SqlClient
Imports App_Code
Imports AjaxControlToolkit
Imports C1.Web.UI.Controls.C1Window

Partial Class OwnerMaintenance
    Inherits MyBasePage '   Inherits System.Web.UI.Page




    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        If Not IsPostBack Then

            Dim ddlAppBinder As New DropDownListBinder

            ddlAppBinder = New DropDownListBinder(Applicationsddl, "SelApplicationCodes", Session("EmployeeConn"))

            ddlAppBinder.AddDDLCriteria("@AppSystem", "base", SqlDbType.NVarChar)
            ddlAppBinder.AddDDLCriteria("@ApplicationGroup", "all", SqlDbType.NVarChar)

            ddlAppBinder.BindData("AppBase", "ApplicationDesc")



            'Dim AppBases As List(Of Application) = New Applications().BaseApps()
            'Applicationsddl.DataSource = AppBases
            'Applicationsddl.DataValueField = "AppBase"
            'Applicationsddl.DataTextField = "ApplicationDesc"
            'Applicationsddl.DataBind()


        End If

    End Sub



    Protected Sub bindAppOwners()


        Dim thisData As New CommandsSqlAndOleDb("SelApplicationOwner", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@AppBase", Applicationsddl.SelectedValue, SqlDbType.Int)
        Dim dt As DataTable

        dt = thisData.GetSqlDataTable

        gvAppOwners.DataSource = dt
        gvAppOwners.DataBind()


        gvAppOwners.SelectedIndex = -1

        Dim ddlBinder As New DropDownListBinder
        ddlBinder.BindData(Technicianddl, "SelTechniciansV3", "tech_num", "tech_name", Session("EmployeeConn"))

        Technicianddl.Visible = True
        btnAddOwner.Visible = True


    End Sub

    Protected Sub Applicationsddl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Applicationsddl.SelectedIndexChanged
        bindAppOwners()

        'If Applicationsddl.SelectedIndex > 0 Then

        '    bindAppOwners()

        'End If


    End Sub

    Protected Sub btnAddOwner_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddOwner.Click
        If Technicianddl.SelectedIndex > 0 Then


            Dim thisData As New CommandsSqlAndOleDb("InsUpdApplicationOwner", Session("EmployeeConn"))
            thisData.AddSqlProcParameter("@AppBase", Applicationsddl.SelectedValue, SqlDbType.Int)
            thisData.AddSqlProcParameter("@tech_num", Technicianddl.SelectedValue, SqlDbType.Int)

            thisData.ExecNonQueryNoReturn()

            bindAppOwners()

        End If

    End Sub

   
    Protected Sub gvAppOwners_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvAppOwners.RowCommand
        If e.CommandName = "Select" Then
            Dim tech_num As String = gvAppOwners.DataKeys(Convert.ToInt32(e.CommandArgument))("tech_num").ToString()



            Dim thisData As New CommandsSqlAndOleDb("InsUpdApplicationOwner", Session("EmployeeConn"))
            thisData.AddSqlProcParameter("@AppBase", Applicationsddl.SelectedValue, SqlDbType.Int)
            thisData.AddSqlProcParameter("@tech_num", tech_num, SqlDbType.Int)
            thisData.AddSqlProcParameter("@OwnerType", "Remove", SqlDbType.NVarChar)
            thisData.ExecNonQueryNoReturn()

            Dim updEmail As New CommandsSqlAndOleDb("InsUpdEmailDistribution", Session("EmployeeConn"))
            updEmail.AddSqlProcParameter("@AppBase", Applicationsddl.SelectedValue, SqlDbType.Int)
            updEmail.AddSqlProcParameter("@tech_num", tech_num, SqlDbType.Int)
            updEmail.AddSqlProcParameter("@EventID", "AddAcct", SqlDbType.NVarChar)
            updEmail.AddSqlProcParameter("@Action", "Remove", SqlDbType.NVarChar)


            updEmail.ExecNonQueryNoReturn()

            '  Response.Redirect("~/OwnerMaintenance.aspx", True)

            bindAppOwners()

            gvAppOwners.SelectedIndex = -1

        End If
    End Sub
    Protected Sub ckbAcctEmail_OnCheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim ckbEmail As New CheckBox
        ckbEmail = CType(sender, CheckBox)
        Dim gvOwnersRow As GridViewRow = CType(ckbEmail.NamingContainer, GridViewRow)
        Dim tech_num As String = gvAppOwners.DataKeys(gvOwnersRow.RowIndex)("tech_num")
        Dim ApplicationNum As String = gvAppOwners.DataKeys(gvOwnersRow.RowIndex)("ApplicationNum")


        Dim thisData As New CommandsSqlAndOleDb("InsUpdApplicationOwner", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@AppBase", Applicationsddl.SelectedValue, SqlDbType.Int)
        thisData.AddSqlProcParameter("@tech_num", tech_num, SqlDbType.Int)
        thisData.AddSqlProcParameter("@OwnerType", "Update", SqlDbType.NVarChar)
        thisData.AddSqlProcParameter("@ReceiveNewAcctEmail", ckbEmail.Checked.ToString(), SqlDbType.NVarChar)
        thisData.ExecNonQueryNoReturn()

        Dim updEmail As New CommandsSqlAndOleDb("InsUpdEmailDistribution", Session("EmployeeConn"))
        updEmail.AddSqlProcParameter("@AppBase", Applicationsddl.SelectedValue, SqlDbType.Int)
        updEmail.AddSqlProcParameter("@tech_num", tech_num, SqlDbType.Int)
        updEmail.AddSqlProcParameter("@EventID", "AddAcct", SqlDbType.NVarChar)

        If ckbEmail.Checked = True Then
            updEmail.AddSqlProcParameter("@Action", "Add", SqlDbType.NVarChar)
        Else
            updEmail.AddSqlProcParameter("@Action", "Remove", SqlDbType.NVarChar)
        End If


        updEmail.ExecNonQueryNoReturn()



        bindAppOwners()

        gvAppOwners.SelectedIndex = -1

    End Sub

    Protected Sub btnReturn_Click(sender As Object, e As System.EventArgs) Handles btnReturn.Click
        Response.Redirect("./UserMaintenance.aspx", True)

    End Sub
End Class
