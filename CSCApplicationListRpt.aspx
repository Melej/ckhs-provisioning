﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="false" CodeFile="CSCApplicationListRpt.aspx.vb" Inherits="CSCApplicationListRpt" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <style type="text/css">
        .style12
        {
            height: 30px;
            width: 518px;
        }        
        .ui-datepicker-calendar { 
        display: none; 
        } 
        .style14
        {
            font-size: medium;
        }
        .style17
        {
            width: 418px;
        }
        .style18
        {
            width: 92px;
        }
        .style19
        {
            width: 164px;
        }
        .style20
        {
            width: 145px;
        }
</style> 


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <table>

        <tr>
            <td  align="center">
               
               <table id="tblTypeAndReset">
                <tr align="center">
                        <td>
                            <asp:Button ID="btnCSCOwer" runat="server" Text="CSC App. Maint." Width="150px" CssClass="btnhov" />

                        </td>
                      <td>
                            <asp:Button ID="btnExcel" runat="server" Text="Excel" Width="150px" CssClass="btnhov" />
                       </td> 

                </tr>
              </table>
           </td>
        </tr>
         <tr>
            <td align="center">
                <asp:GridView ID="GridEmployees"  runat="server" 
                     BackColor="#FFFFFF" BorderColor="#FFFFFF" 
                     BorderStyle="Double" BorderWidth="3px" CellPadding="2" 
                      EmptyDataText="No Data Found"  EnableTheming="False" 
                    AllowSorting="True"  EnableModelValidation="True"  Font-Size="Smaller"
                    AutoGenerateColumns="false" EditRowStyle-BorderStyle="Ridge" CellSpacing="2"
                     AlternatingRowStyle-BackColor="#CCCCCC" Font-Bold="True" HeaderStyle-CssClass="subheading"  Visible="True">
         

                    <FooterStyle BackColor="White" ForeColor="#333333" />

                    <HeaderStyle BackColor="#D7D5D5" Font-Bold="False" ForeColor="#060606" Font-Size="Small" HorizontalAlign="Left"    />                                    
                    <PagerStyle BackColor="#FFFFFF" ForeColor="#FFFFFF" HorizontalAlign="Left" Font-Size="Medium"/>
                    <RowStyle BackColor="#FFFFFF" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#D7D5D5" Font-Bold="True" ForeColor="#060606" />


                         <Columns>

    						<asp:BoundField DataField="ApplicationName" SortExpression="ApplicationName" HeaderText="Name" ItemStyle-Width="20%"></asp:BoundField>
							<asp:BoundField DataField="ClientSupportNames" SortExpression="ClientSupportNames" HeaderText="Client Support " ItemStyle-Width="20%"></asp:BoundField>
							<asp:BoundField DataField="TechSupportNames" SortExpression="TechSupportNames" HeaderText="Tech Support " ItemStyle-Width="20%"></asp:BoundField>

							<asp:BoundField DataField="VendorNames" SortExpression="VendorNames" HeaderText="OutSide Support" ></asp:BoundField>
							<asp:BoundField DataField="Comments" SortExpression="Comments" HeaderText="Comments" ItemStyle-Width="20%"></asp:BoundField>

                            
                   </Columns> 
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td align="center">
            
                <asp:GridView ID="GridHidden" runat="server" 
                     BackColor="#FFFFFF" BorderColor="#FFFFFF" 
                     BorderStyle="Double" BorderWidth="3px" CellPadding="2" 
                      EmptyDataText="No Data Found"  EnableTheming="False" 
                    AllowSorting="True"  EnableModelValidation="True"  Font-Size="Smaller"
                    AutoGenerateColumns="false" EditRowStyle-BorderStyle="Ridge" CellSpacing="2"
                     AlternatingRowStyle-BackColor="#CCCCCC" Font-Bold="True" HeaderStyle-CssClass="subheading" Visible="false">
                      

                    <FooterStyle BackColor="White" ForeColor="#333333" />

                    <HeaderStyle BackColor="#D7D5D5" Font-Bold="False" ForeColor="#060606" Font-Size="Smaller" HorizontalAlign="Left"    />                                    
                    <PagerStyle BackColor="#FFFFFF" ForeColor="#FFFFFF" HorizontalAlign="Left" Font-Size="Medium"/>
                    <RowStyle BackColor="#FFFFFF" ForeColor="#333333" Font-Size="Smaller" />

                         <Columns>

                             						<asp:BoundField DataField="ApplicationName" SortExpression="ApplicationName" HeaderText="Name"></asp:BoundField>
							<asp:BoundField DataField="ClientSupportNames" SortExpression="ClientSupportNames" HeaderText="Client Support "></asp:BoundField>
							<asp:BoundField DataField="TechSupportNames" SortExpression="TechSupportNames" HeaderText="Tech Support "></asp:BoundField>

							<asp:BoundField DataField="VendorNames" SortExpression="VendorNames" HeaderText="OutSide Support" ></asp:BoundField>
							<asp:BoundField DataField="Comments" SortExpression="Comments" HeaderText="Comments" ></asp:BoundField>

                             
							<asp:BoundField DataField="CSCAppdisplay" SortExpression="CSCAppdisplay" HeaderText="App in CSC" ></asp:BoundField>

    						<asp:BoundField DataField="AccountQueueDisplay" SortExpression="AccountQueueDisplay" HeaderText="Provisioning Account Queue" ></asp:BoundField>


                   </Columns> 
                </asp:GridView>
            
            
            </td>
        </tr>
       <tr>
          <td >
            <asp:Label ID="HDStartDate" runat="server" Visible="false"></asp:Label>
            <asp:Label ID="HDEnddate" runat="server" Visible="false"></asp:Label>



        </td>
        </tr>
  </table>
</asp:Content>

