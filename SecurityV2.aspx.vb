﻿Imports App_Code

Partial Class SecurityV2
    Inherits System.Web.UI.Page
    'Dim UserInfo As UserSecurity
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Session("SecurityLevelNum") = ""

        testid.Text = Session("LoginID")
    End Sub


    Protected Sub BtnSubmit_Click(sender As Object, e As System.EventArgs) Handles BtnSubmit.Click

        If Accountnameid.Text <> "" Then
            'Dim Security As New UserSecurity(Session("LoginID"), Session("EmployeeConn"))

            Dim UserInfo = New UserSecurity(Accountnameid.Text, Session("EmployeeConn"))

            Session("objUserSecurity") = UserInfo

            Session("ClientNum") = UserInfo.ClientNum
            Session("TechClientNum") = UserInfo.TechNum
            Session("UserName") = UserInfo.UserName
            Session("SecurityLevelNum") = UserInfo.SecurityLevelNum
            Session("Usertype") = UserInfo.UserType
            Session("loginid") = UserInfo.NtLogin

            If Session("SecurityLevelNum") < 25 Then
                Response.Redirect("~/Security.aspx")
            Else
                Response.Redirect("~/SimpleSearch.aspx")
            End If

            If Session("SecurityLevelNum") < 41 Then
                Response.Redirect("~/MyRequests.aspx")

            End If


        Else
            Response.Redirect("~/Security.aspx")

        End If
    End Sub

End Class
