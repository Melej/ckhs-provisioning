﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="false" CodeFile="UnassignedRequests.aspx.vb" Inherits="UnassignedRequests"  ViewStateMode="Enabled"%>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

<asp:UpdatePanel runat="server" ID="uplMain" UpdateMode="Conditional">
<ContentTemplate>
 <div id ="divQueueOptions" style="text-align:center" >
         <table width="100%">
            <tr>

                <td>
               Request:
                </td>
                <td>
                <asp:RadioButtonList runat="server" ID="rblRequestType" AutoPostBack="true" >

                    <asp:ListItem Value="ticket" Selected="True" Text="Tickets"></asp:ListItem>
                    <asp:ListItem Value="rfs" Text="RFS"></asp:ListItem>

                </asp:RadioButtonList>
                </td>
             
            
            </tr>
         </table>
       
       
       <hr />
       

    </div>


  
     <div style="overflow:auto;" class="subContainer" >

     <table width="100%">
      


        <tr>
            <td>
                <asp:Table runat="server" ID="tblUnassigned" Width="100%" CssClass="Requests">
                
                
                </asp:Table>
             </td>
        </tr>

     </table>

     </div>

      <asp:Button ID="btnAssignDummy" Style="display: none" runat="server" Text="Button" />

     <ajaxToolkit:ModalPopupExtender ID="mpeAssign" runat="server"   
                                 DynamicServicePath="" Enabled="True" TargetControlID="btnAssignDummy"   
                                 PopupControlID="pnlAssign" BackgroundCssClass="ModalBackground"   
                                 DropShadow="true" >  
     </ajaxToolkit:ModalPopupExtender>  

<asp:Panel ID="pnlAssign" runat="server" style="display:none; background-color:#efefef;  font-size:large;  width:600px;  border: 2px solid #FFFFCC;">

                                            
                                                            <table  width="100%" border="1px" >
                                                                <tr>
                                                                    <th class="tableRowHeader" colspan = "4">
                                                                  <asp:Label runat="server" ID ="lblRequest"></asp:Label>
                                                                    </th>
                                                                </tr>

                                                             <tr>
                                                                
                                                                
                                                                <td colspan="4">
                                                                   <div class="scrollContentContainer" >
                                                                   <asp:UpdatePanel runat="server" ID="uplAssign" UpdateMode="Conditional">
                                                                    <ContentTemplate>
                                                                  
                                                                        <table>
                                                                            <tr>
                                                                                <td>
                                                                                    Assign Group
                                                                                </td>
                                                                                <td>
                                                                                      <asp:DropDownList runat="server" ID="ddlAssignGroup"></asp:DropDownList>
                                                                                </td>
                                                                            
                                                                            </tr>
                                                                         <tr>
                                                                                <td>
                                                                                    Assign Tech
                                                                                </td>
                                                                                <td>
                                                                                      <asp:DropDownList runat="server" ID="ddlAssignTech">
                                                                                        <asp:ListItem>Select Group</asp:ListItem>
                                                                                      </asp:DropDownList>
                                                                                </td>
                                                                            
                                                                            </tr>
                                                                        </table>
                                                                        </ContentTemplate> 
                                                                 </asp:UpdatePanel>
                                                                   </div>
                                                                
                                                                
                                                                </td>
                                                            
       
                                                            </tr>
                                                           
                                                                                                                     
                                                               
                                                                <tr>
                                                                    <td colspan = "4" align = "center" >
                                                                     <asp:Button ID = "btnAssign" runat = "server" Text="Assign" />
                                                                    <asp:Button ID = "btnClose" runat = "server" Text="Cancel" />
                                                                    
                                                                  
                                                                   
                                                                    </td>
                                                                </tr>
                                
                                                            </table>

                          
                                 
                                </asp:Panel>




</ContentTemplate>
</asp:UpdatePanel>

</asp:Content>

  
