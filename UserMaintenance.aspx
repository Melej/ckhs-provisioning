﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="false" CodeFile="UserMaintenance.aspx.vb" Inherits="UserMaintenance" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <style type="text/css">
        .style2
        {
            text-align: center;
            background-color: #FFFFCC;
            color: #336666;
            font-weight: bold;
            font-size: x-large;
            height: 43px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:UpdatePanel ID="uplPanel" runat ="server"  UpdateMode="Conditional">

   <ContentTemplate>
   <div id="maindiv" runat="server" align="left">
    <table  style="border-style: groove" align="left" width="100%">
        <tr align="center">
           <td class="tableRowHeader">
                  Provisioning and CSC Submitters/Technicians Maintenance
           </td>
        </tr>
        <tr>
          <td align="left">
             <ajaxToolkit:TabContainer ID="Maintab" runat="server" ActiveTabIndex="0"
                Width="100%"  EnableTheming="False"  AutoPostBack="true"
                BackColor="#efefef">

                  <ajaxToolkit:TabPanel ID="tptechnicians" runat="server"  BackColor="Gainsboro" Visible = "True" EnableTheming="True" TabIndex="0">
                    <HeaderTemplate > 
                        <asp:Label ID="Label4" runat="server" Text="Current Techs & Submitters " Font-Bold="true" ForeColor="Black"></asp:Label>
                    </HeaderTemplate>
                      <ContentTemplate>

                       <table border="1" width="100%"  style="empty-cells:show" >
                         <tr>
                              <td colspan="4" align="center">
                                You can search by ONLY 1 criteria
          
                              </td>
                        </tr>
                         <tr>
                             <td align="right">
                                Last Name
                            </td>
                            <td>
                             <asp:TextBox ID="SearchLastNameTextBox" Width="200px" runat = "server"></asp:TextBox>
                            </td>

                             <td align="right">
                                <asp:Label ID="Label5" runat="server" Text="Account Types:" />
                            </td>
                            <td align="left">
                                <asp:DropDownList ID="UserTypesddl" runat="server" Width="250px">
                                </asp:DropDownList>
                                <asp:TextBox ID="SecurityLevelText" runat="server" Visible="false"></asp:TextBox>
                            </td>
                      </tr>
                      
                         <tr>
                       <td  align="center" colspan="4">
                        <table>
                            <tr>
                                <td align="center">

                                        <asp:Button ID ="btnSearch" runat = "server" Text ="Search" 
                                        CssClass="btnhov" BackColor="#006666"  Width="120px" />
                                </td>
                                <td>
                                
                                </td>
                                <td align="center" >

                                        <asp:Button ID = "btnReset" runat= "server" Text="Reset"  
                                            CssClass="btnhov" BackColor="#006666"  Width="120px" />

                                </td>
                            </tr>
                        </table>
                      </td>
                     </tr>          
                         <tr>
                           <td colspan="4">
                                <asp:GridView ID="gvAccounts" runat="server" AllowSorting="True" 
                               AutoGenerateColumns="False" BackColor="White" BorderColor="#336666" 
                               BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                               DataKeyNames="client_num,TechStatus" EmptyDataText="No Techs" 
                               EnableTheming="False" Font-Size="Medium" GridLines="Horizontal" Width="100%">
                               <FooterStyle BackColor="White" ForeColor="#333333" />
                               <HeaderStyle BackColor="#336666" Font-Bold="False" Font-Size="Small" 
                                   ForeColor="White" HorizontalAlign="Left" />
                               <PagerStyle BackColor="#336666" Font-Size="Medium" ForeColor="White" 
                                   HorizontalAlign="Left" />
                               <RowStyle BackColor="White" ForeColor="#333333" />
                               <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                               <SortedAscendingCellStyle BackColor="#F7F7F7" />
                               <SortedAscendingHeaderStyle BackColor="#487575" />
                               <SortedDescendingCellStyle BackColor="#E5E5E5" />
                               <SortedDescendingHeaderStyle BackColor="#275353" />
                               <Columns>

                                   <asp:CommandField ButtonType="Button" ControlStyle-BackColor="#84A3A3" 
                                          HeaderStyle-HorizontalAlign="Left" ItemStyle-Height="30px" SelectText="Select" 
                                          ShowSelectButton="true">
                                      <ControlStyle BackColor="#84A3A3" />
                                      <HeaderStyle HorizontalAlign="Left" />
                                      <ItemStyle Height="30px" />
                                      </asp:CommandField>
                            

                                   <asp:BoundField DataField="tech_name" HeaderText="Name" />

                                   <asp:BoundField DataField="UserType" HeaderText="Security Level" />

                                   <asp:BoundField DataField="ntlogin" HeaderText="Account " />

                                   <asp:BoundField DataField="TechStatus" HeaderText="Tech Status " />

                               </Columns>
                           </asp:GridView>
                           </td>
                         </tr>
                        </table>
                      </ContentTemplate>
                </ajaxToolkit:TabPanel>

                 <ajaxToolkit:TabPanel ID="tpNewSubmitter" runat="server" BackColor="Gainsboro" Visible = "True" EnableTheming="True" TabIndex="1">
                       <HeaderTemplate > 
                          <asp:Label ID="Label3" runat="server" Text="Add New Tech/Submitter" Font-Bold="true" ForeColor="Black" ></asp:Label>
                     </HeaderTemplate>
                      <ContentTemplate>

                       <table border="1" width="100%"  style="empty-cells:show" >
                         <tr>
                              <td colspan="2" align="center">
                               Search for CKHS or Prospect Employees to Add
          
                              </td>
                        </tr>
                         <tr>
                            <td colspan="2" align="center">
                                <table>
                                    <tr>
                                         <td align="right" style="font-weight: bold">
                                            Last Name
                                        </td>
                                    
                                        <td>
                                         <asp:TextBox ID="ADDLastNameTextBox" Width="200px" runat = "server"></asp:TextBox>
                                        </td>
                                    
                                    
							          <td align="right" style="font-weight: bold">
										   First Name
         
										</td>
										<td>
										   <asp:TextBox ID = "AddFirstNameTextBox" runat = "server"></asp:TextBox>
										</td>

                                    </tr>
                                </table>
                            </td>

                      </tr>
                         <tr>
                       <td  align="center" colspan="4">
                        <table>
                            <tr>
                                <td align="center">

                                        <asp:Button ID ="BtnAddSearch" runat = "server" Text ="Search" 
                                        CssClass="btnhov" BackColor="#006666"  Width="120px" />
                                        
                                </td>
                                <td>
                                
                                </td>
                                <td align="center" >

                                        <asp:Button ID = "BtnAddReset" runat= "server" Text="Reset" 
                                        CssClass="btnhov" BackColor="#006666"  Width="120px" />
                                </td>
                            </tr>
                        </table>
                      </td>
                     </tr>          
                         <tr>
                           <td colspan="4">
                            <asp:GridView ID="GvADDClient" runat="server" AllowSorting="True" 
                           AutoGenerateColumns="False" BackColor="White" BorderColor="#336666" 
                           BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                           DataKeyNames="client_num" EmptyDataText="No Techs" 
                           EnableTheming="False" Font-Size="Medium" GridLines="Horizontal" Width="100%">
                           <FooterStyle BackColor="White" ForeColor="#333333" />
                           <HeaderStyle BackColor="#336666" Font-Bold="False" Font-Size="Small" 
                               ForeColor="White" HorizontalAlign="Left" />
                           <PagerStyle BackColor="#336666" Font-Size="Medium" ForeColor="White" 
                               HorizontalAlign="Left" />
                           <RowStyle BackColor="White" ForeColor="#333333" />
                           <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                           <SortedAscendingCellStyle BackColor="#F7F7F7" />
                           <SortedAscendingHeaderStyle BackColor="#487575" />
                           <SortedDescendingCellStyle BackColor="#E5E5E5" />
                           <SortedDescendingHeaderStyle BackColor="#275353" />
                           <Columns>

                               <asp:CommandField ButtonType="Button" ControlStyle-BackColor="#84A3A3" 
                                      HeaderStyle-HorizontalAlign="Left" ItemStyle-Height="30px" SelectText="Select" 
                                      ShowSelectButton="true">
                                  <ControlStyle BackColor="#84A3A3" />
                                  <HeaderStyle HorizontalAlign="Left" />
                                  <ItemStyle Height="30px" />
                                  </asp:CommandField>
                            

                               <asp:BoundField DataField="fullname" HeaderText="Name" />

                               <asp:BoundField DataField="siemens_emp_num" HeaderText="Emp#" />

                               <asp:BoundField DataField="department_name" HeaderText="Department " />

                               <asp:BoundField DataField="login_name" HeaderText="Account " />

                           </Columns>
                       </asp:GridView>


                           </td>
                         </tr>
                        </table>
                       </ContentTemplate>
                   </ajaxToolkit:TabPanel>


                 <ajaxToolkit:TabPanel ID="tpOnCall" runat="server" BackColor="Gainsboro" Visible = "False" EnableTheming="True" TabIndex="2">
                       <HeaderTemplate > 
                          <asp:Label ID="Label1" runat="server" Text="On Call maintenance" Font-Bold="true" ForeColor="Black" ></asp:Label>
                     </HeaderTemplate>
                      <ContentTemplate>
                       <table border="1" width="100%"  style="empty-cells:show" >
                           <tr>
                              <td colspan="2" align="center">
                                Modify On-Call Queues
          
                              </td>
                        </tr>
                           <tr>
                            <td colspan="2" align="center">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Label ID="grouplbl" runat="server" Text = "Select Group:"  Visible="false"></asp:Label>
                                            <asp:DropDownList ID = "TechGroupDddl" runat="server" AutoPostBack="true" Visible="false">
                                             </asp:DropDownList>

                                        </td>
                                        <td>
                                            <asp:Label ID="SelTechlbl" runat="server" Text="Select Tech:" Visible="false"></asp:Label>

                                            <asp:DropDownList ID = "Techddl" runat="server" AutoPostBack="true" Visible="false">
                                             </asp:DropDownList>


                                        </td>
                                    
                                    
                                    </tr>
                                </table>
                            </td>

                      </tr>
                      
                           <tr>
                       <td  align="center" colspan="4">
                        <table>
                            <tr>
                                <td align="center">

                                        <asp:Button ID ="btnUpdOnCallTech" runat = "server" Text ="Submit On Call Change"  Visible="false"
                                        CssClass="btnhov" BackColor="#006666"  Width="160px" />
                                </td>
                                <td>
                                
                                </td>
                                <td align="center" >

                                        <asp:Button ID = "btnResetOncall" runat= "server" Text="Reset"
                                        CssClass="btnhov" BackColor="#006666"  Width="160px" />

                                </td>
                            </tr>
                        </table>
                      </td>
                     </tr>
                           <tr>

                        <td>
                         <asp:GridView ID="GridOnCall" runat="server" AllowSorting="True" 
                           AutoGenerateColumns="False" BackColor="White" BorderColor="#336666" 
                           BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                            DataKeyNames="group_num,tech_num"
                           EmptyDataText="No On Call" 
                           EnableTheming="False" Font-Size="Medium" GridLines="Horizontal" Width="100%">
                           <FooterStyle BackColor="White" ForeColor="#333333" />
                           <HeaderStyle BackColor="#336666" Font-Bold="False" Font-Size="Small" 
                               ForeColor="White" HorizontalAlign="Left" />
                           <PagerStyle BackColor="#336666" Font-Size="Medium" ForeColor="White" 
                               HorizontalAlign="Left" />
                           <RowStyle BackColor="White" ForeColor="#333333" />
                           <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                           <SortedAscendingCellStyle BackColor="#F7F7F7" />
                           <SortedAscendingHeaderStyle BackColor="#487575" />
                           <SortedDescendingCellStyle BackColor="#E5E5E5" />
                           <SortedDescendingHeaderStyle BackColor="#275353" />
                           <Columns>

                                <asp:CommandField ButtonType="Button" SelectText="Select" ShowSelectButton= "true" HeaderStyle-HorizontalAlign = "Left"  ControlStyle-BackColor="#84A3A3" ItemStyle-Height="30px" />

                               <asp:BoundField DataField="group_name" HeaderText="On Call Group Name" />

                               <asp:BoundField DataField="techname" HeaderText="Currently On Call" />

							   <asp:BoundField DataField="Phone" HeaderText="Phone" />


                           </Columns>
                       </asp:GridView>


                           </td>                     
                     
                     </tr>          
                           <tr>
                        <td>
                            <asp:Label ID="hdGroup" runat="server" Visible="false"></asp:Label>
                            <asp:Label ID="hdTech" runat="server" Visible="false"></asp:Label>
                        </td>
                     </tr>
                       </table>
                   </ContentTemplate>
                 </ajaxToolkit:TabPanel>

                  <ajaxToolkit:TabPanel ID="TabPaneGroupView" runat="server" BackColor="Gainsboro"   Visible="False" TabIndex="3">
                      <HeaderTemplate > 
                            <asp:Label ID="Label6" runat="server" Text="Group To Tech " Font-Bold="true" ForeColor="Black" ></asp:Label>
                    
                      </HeaderTemplate>
                      <ContentTemplate>
                          <table border="1" width = "100%" style="empty-cells:show" >
                            <tr>
                                      <td colspan = "4">
                                            <asp:Label ID="lblGroups" runat="server" Text="Groups Queues:"></asp:Label>
                                            <asp:DropDownList ID = "TechGroup2Dddl" runat="server" AutoPostBack="true" >
                                             </asp:DropDownList>

                                             <asp:Label ID="hdnewgroup" runat="server" Visible="false"></asp:Label>
                                      </td>
                            
                                
                            </tr>

                            <tr>
                                <td colspan = "4">
                                         <asp:GridView ID="GridGroupView" runat="server" AllowSorting="True" 
                                           AutoGenerateColumns="False" BackColor="White" BorderColor="#336666" 
                                           BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                                            DataKeyNames="client_num"
                                           EmptyDataText="No On Call" 
                                           EnableTheming="False" Font-Size="Medium" GridLines="Horizontal" Width="100%">
                                           <FooterStyle BackColor="White" ForeColor="#333333" />
                                           <HeaderStyle BackColor="#336666" Font-Bold="False" Font-Size="Small" 
                                               ForeColor="White" HorizontalAlign="Left" />
                                           <PagerStyle BackColor="#336666" Font-Size="Medium" ForeColor="White" 
                                               HorizontalAlign="Left" />
                                           <RowStyle BackColor="White" ForeColor="#333333" />
                                           <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                           <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                           <SortedAscendingHeaderStyle BackColor="#487575" />
                                           <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                           <SortedDescendingHeaderStyle BackColor="#275353" />
                                           <Columns>

                                                <asp:CommandField ButtonType="Button" SelectText="Select" ShowSelectButton= "true" HeaderStyle-HorizontalAlign = "Left"  ControlStyle-BackColor="#84A3A3" ItemStyle-Height="30px" />

                                               <asp:BoundField DataField="TechName" HeaderText="Tech Name " />

                                               <asp:BoundField DataField="beeper" HeaderText="Pager " />


                                           </Columns>
                                       </asp:GridView>
          
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </ajaxToolkit:TabPanel>


                 <ajaxToolkit:TabPanel ID="tabProv" runat="server"  BackColor="Gainsboro"  EnableTheming="False" TabIndex="4">
                     <HeaderTemplate > 
                            <asp:Label ID="Label7" runat="server" Text="Provisioning Ownership" Font-Bold="True" 
                                ForeColor="Black"></asp:Label>
                     </HeaderTemplate>
                     <ContentTemplate>

                    </ContentTemplate>
                </ajaxToolkit:TabPanel>

               <ajaxToolkit:TabPanel ID="tabCscGrp" runat="server"  BackColor="Gainsboro"  EnableTheming="False" Visible="false" TabIndex="5">
                     <HeaderTemplate > 
                            <asp:Label ID="Label8" runat="server" Text="CSC Group Maintenance" Font-Bold="True" 
                                ForeColor="Black"></asp:Label>
                     </HeaderTemplate>
                     <ContentTemplate>
                      <table border="0" style="width: 100%">
                        <tr>
                            <td></td>
                        </tr>
                    </table>
                    </ContentTemplate>
                </ajaxToolkit:TabPanel>

               <ajaxToolkit:TabPanel ID="tabClientMaint" runat="server"  BackColor="Gainsboro"  EnableTheming="False" TabIndex="6">
                     <HeaderTemplate > 
                            <asp:Label ID="Label9" runat="server" Text="Client Acct.Maintenance" Font-Bold="True" 
                                ForeColor="Black"></asp:Label>
                     </HeaderTemplate>
                     <ContentTemplate>
                      <table border="0" style="width: 100%">
                        <tr>
                            <td></td>
                        </tr>
                    </table>
                    </ContentTemplate>
                </ajaxToolkit:TabPanel>


          </ajaxToolkit:TabContainer>
        </td>
     </tr>
  </table>
  </div>
   </ContentTemplate>
  </asp:UpdatePanel>
</asp:Content>

