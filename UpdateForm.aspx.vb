﻿Imports System.IO
Imports System.Data '
Imports System.Data.SqlClient
Imports App_Code
Imports AjaxControlToolkit
Imports C1.Web.UI.Controls.C1Window

Partial Class UpdateForm
    Inherits MyBasePage '   Inherits System.Web.UI.Page


    Dim sActivitytype As String = ""
    Dim cblBinder As New CheckBoxListBinder
    Dim FormSignOn As New FormSQL()
    Dim iClientNum As Integer
    Dim dtAccounts As DataTable
    Dim dtPendingRequests As DataTable
    Dim AddAccountRequestNum As Integer = 0
    Dim DelAccountRequestNum As Integer = 0



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        FormSignOn = New FormSQL(Session("EmployeeConn"), "SelClientByNumberV20", "InsAccountRequestV20", "", Page)
        iClientNum = Request.QueryString("ClientNum")
        dtUserApplications()
        If Not IsPostBack Then



            Dim ddlBinder As New DropDownListBinder

            cblBinder.BindData(cblApplications, "SelApplications", "account_type_cd", "ApplicationDesc", Session("EmployeeConn"))





            FormSignOn.AddSelectParm("@client_num", iClientNum, SqlDbType.Int, 6)
            FormSignOn.FillForm()


            ' DisablePendingQueues()

            fillApplications()
            GetPendingQueues()

            Dim DisableControls As New DisableControls(Page)
            DisableQueues()


            displayPendingQueues()
            '  displayPendingQueues()

        End If

        

    End Sub


  
    

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        CompareAccounts()

    End Sub

    Private Sub insAcctItems(ByVal requestNum As Integer, ByVal acctTypeCode As String, ByVal comments As String)




        Dim thisdata As New CommandsSqlAndOleDb("InsAccountItemsV3", Session("EmployeeConn"))
        thisdata.AddSqlProcParameter("@account_request_num", requestNum, SqlDbType.Int, 20)
        thisdata.AddSqlProcParameter("@account_type_cd", acctTypeCode, SqlDbType.NVarChar, 8)
        thisdata.AddSqlProcParameter("@account_item_desc", comments, SqlDbType.NVarChar, 255)
        thisdata.AddSqlProcParameter("@validate", "NeedsValidation", SqlDbType.NVarChar, 20)


        thisdata.ExecNonQueryNoReturn()



    End Sub


    Private Sub DisableTextBoxes(ByRef ctrl As Control)

        For Each c As Control In ctrl.Controls

            If TypeOf c Is TextBox Then

                Dim txt As TextBox = DirectCast(c, TextBox)

                txt.Enabled = False
                txt.Attributes.Remove("class")


            End If

            If TypeOf c Is DropDownList Then

                Dim ddl As DropDownList = DirectCast(c, DropDownList)

                If ddl.ID <> "Rolesddl" Then

                    ddl.Enabled = False

                End If




            End If


            If TypeOf c Is RadioButtonList Then

                Dim rbl As RadioButtonList = DirectCast(c, RadioButtonList)

                rbl.Enabled = False

            End If


            DisableTextBoxes(c)

        Next



    End Sub

    Protected Sub dtUserApplications()
        Dim thisdata As New CommandsSqlAndOleDb("sel_all_client_accounts_by_number", Session("EmployeeConn"))
        thisdata.AddSqlProcParameter("@client_num", iClientNum, SqlDbType.Int, 20)

        thisdata.ExecNonQueryNoReturn()

        dtAccounts = thisdata.GetSqlDataTable

    End Sub

    Protected Sub fillApplications()
        Dim rwRef() As DataRow

        For Each cblItem As ListItem In cblApplications.Items
            cblItem.Selected = False


            rwRef = dtAccounts.Select("account_type_cd='" & cblItem.Value & "'")
            If rwRef.Count > 0 Then

                cblItem.Selected = True

            End If

        Next


    End Sub
    Private Function AccountRequestNum(ByVal sRequestType As String) As Integer
        FormSignOn = New FormSQL(Session("EmployeeConn"), "sel_client_by_number", "InsAccountRequestV20", "", Page)
        FormSignOn.AddInsertParm("@requestor_client_num", Session("ClientNum"), SqlDbType.NVarChar, 10)
        FormSignOn.AddInsertParm("@receiver_client_num", iClientNum, SqlDbType.Int, 10)
        FormSignOn.AddInsertParm("@account_request_type", sRequestType, SqlDbType.NVarChar, 9)
        FormSignOn.UpdateFormReturnReqNum()

        AccountRequestNum = FormSignOn.AccountRequestNum

    End Function



    Private Sub CompareAccounts()

        Dim rwAcct As DataRow()


        For Each i As ListItem In cblApplications.Items

            rwAcct = dtAccounts.Select("account_type_cd='" & i.Value & "'")


            If rwAcct.Count > 0 And i.Selected = False And i.Enabled = True Then

                If DelAccountRequestNum = 0 Then

                    DelAccountRequestNum = AccountRequestNum("delacct")

                End If

                insAcctItems(DelAccountRequestNum, i.Value, "")

            End If

            If rwAcct.Count = 0 And i.Selected = True And i.Enabled = True Then

                If AddAccountRequestNum = 0 Then

                    AddAccountRequestNum = AccountRequestNum("addacct")

                End If

                insAcctItems(AddAccountRequestNum, i.Value, "")

            End If


        Next



    End Sub

    Private Sub GetPendingQueues()
        Dim thisdata As New CommandsSqlAndOleDb("SelPendingQueuesByRecieverNum", Session("EmployeeConn"))
        thisdata.AddSqlProcParameter("@reciever_client_num", iClientNum, SqlDbType.Int, 20)

        dtPendingRequests = thisdata.GetSqlDataTable

    End Sub


    Private Sub DisableQueues()

        If dtPendingRequests.Rows.Count > 0 Then
            For Each cblItem As ListItem In cblApplications.Items

                cblItem.Enabled = False


            Next

            btnSubmit.Visible = False

        End If
       

    End Sub


    Private Sub DisablePendingQueues()

        GetPendingQueues()

        Dim rwPending() As DataRow

        For Each cblItem As ListItem In cblApplications.Items
            cblItem.Selected = False


            rwPending = dtPendingRequests.Select("account_type_cd='" & cblItem.Value & "'")
            If rwPending.Count > 0 Then
                Select Case rwPending(0).Item("account_request_type")
                    Case "addacct", "addnew"
                        cblItem.Attributes.Add("style", "background-color:green; color:black")

                        cblItem.Enabled = False
                    Case "delacct"
                        cblItem.Attributes.Add("style", "background-color:red; color:black")

                        cblItem.Enabled = False

                End Select

               
            End If
        Next

    End Sub

    Private Sub displayPendingQueues()

        If dtPendingRequests.Rows.Count > 0 Then
            Dim iRowCount As Integer = 1
            Dim rwHeader As New TableRow
            Dim clAccountHdr As New TableCell
            Dim clRequestTypeHdr As New TableCell
            Dim clValidCdHdr As New TableCell
            Dim clRequestorHdr As New TableCell
            Dim clDateHdr As New TableCell

            clAccountHdr.Text = "Account"
            clRequestTypeHdr.Text = "Request"
            clValidCdHdr.Text = "Status"
            clRequestorHdr.Text = "Requester"
            clDateHdr.Text = "Request Date"

            rwHeader.Cells.Add(clAccountHdr)
            rwHeader.Cells.Add(clRequestTypeHdr)
            rwHeader.Cells.Add(clValidCdHdr)
            rwHeader.Cells.Add(clRequestorHdr)
            rwHeader.Cells.Add(clDateHdr)

            rwHeader.Font.Bold = True
            rwHeader.BackColor = Color.FromName("#006666")
            rwHeader.ForeColor = Color.FromName("White")
            rwHeader.Height = Unit.Pixel(36)

            tblPendingQueues.Rows.Add(rwHeader)

            tblPendingQueues.Width = New Unit("100%")



            For Each rwPending As DataRow In dtPendingRequests.Rows

                Dim rwData As New TableRow
                Dim clAccountData As New TableCell
                Dim clRequestTypeData As New TableCell
                Dim clValidCdData As New TableCell
                Dim clRequestorData As New TableCell
                Dim clDateData As New TableCell


                clAccountData.Text = rwPending("ApplicationDesc")
                clRequestTypeData.Text = rwPending("account_request_type")
                clValidCdData.Text = rwPending("valid_cd")
                clRequestorData.Text = rwPending("requestor_name")
                clDateData.Text = IIf(IsDBNull(rwPending("entered_date")), "", rwPending("entered_date"))

                rwData.Cells.Add(clAccountData)
                rwData.Cells.Add(clRequestTypeData)
                rwData.Cells.Add(clValidCdData)
                rwData.Cells.Add(clRequestorData)
                rwData.Cells.Add(clDateData)


                If iRowCount > 0 Then

                    rwData.BackColor = Color.Bisque

                Else

                    rwData.BackColor = Color.LightGray

                End If



                iRowCount = iRowCount * -1


                tblPendingQueues.Rows.Add(rwData)


            Next



        End If

    End Sub
End Class
