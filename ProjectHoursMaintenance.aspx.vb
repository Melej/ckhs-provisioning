﻿Imports System.IO
Imports System.Data '
Imports System.Data.SqlClient
Imports App_Code
Imports AjaxControlToolkit

Partial Class ProjectHoursMaintenance
	Inherits System.Web.UI.Page
	Dim iProjectNum As Integer
	Dim FormSignOn As New FormSQL()
	'Dim VendorFormSignOn As FormSQL
	'Dim UpdPhyFormSignOn As FormSQL
	Dim iClientNum As Integer
	Dim iUserNum As Integer
	Dim iUserSecurityLevel As Integer
	Dim dtSelected As DataTable

	Dim UserInfo As UserSecurity
	Dim dst As DataSet
	Dim dt As DataTable
	Dim totaldt As DataTable


	Dim ProjectNumS As String
	Dim thisdata As CommandsSqlAndOleDb

	Private Sub ProjectHoursMaintenance_Load(sender As Object, e As EventArgs) Handles Me.Load

		UserInfo = New UserSecurity(Session("LoginID"), Session("EmployeeConn"))

		iUserNum = UserInfo.ClientNum
		HDClientnum.Text = UserInfo.ClientNum
		HDRptClientnum.Text = UserInfo.ClientNum

		HDTechnum.Text = UserInfo.TechNum


		iUserSecurityLevel = UserInfo.SecurityLevelNum
        'userDepartmentCD.Text = UserInfo.DepartmentCd

        If iUserSecurityLevel > 71 Or iUserSecurityLevel = 65 Or iUserSecurityLevel = 66 Or iUserSecurityLevel = 60 Then

        Else
            Response.Redirect("~/SimpleSearch.aspx", True)

		End If

		'Select Case iUserSecurityLevel
		'	Case (65), (81), (80), (90), (91), (92), (93), (94), (100), (105)

		'	Case Else
		'		If iUserSecurityLevel > 71 Then

		'		End If



		'End Select


		If Not IsPostBack Then
			'iUserSecurityLevel = 65

			Dim ProjectContr As New ProjectController()
			FormSignOn = New FormSQL(Session("CSCConn"), "SelProjectdesc", "InsUpdProjectEmpWeek", "", Page)
			'FormSignOn.AddInsertParm("@ProjectDesc", ProjectDesctxt.Text, SqlDbType.NVarChar, 275)

			Dim ddlBindert As New DropDownListBinder(Projectsddl, "Selprojects", Session("CSCConn"))
			ddlBindert.AddDDLCriteria("@ProjectType", "capital", SqlDbType.NVarChar)
			ddlBindert.BindData("ProjectNum", "ProjectName")

			Dim ddlBindMgr As New DropDownListBinder(Projectlist2ddl, "Selprojects", Session("CSCConn"))
			ddlBindMgr.AddDDLCriteria("@ProjectType", "capital", SqlDbType.NVarChar)
			ddlBindMgr.BindData("ProjectNum", "ProjectName")

			Dim ddlRptBinder As New DropDownListBinder(ReportTypeddl, "SelProjectReportTypes", Session("CSCConn"))
			ddlRptBinder.AddDDLCriteria("@securitylevel", iUserSecurityLevel, SqlDbType.Int)
			ddlRptBinder.BindData("ReportTypecd", "ReportDesc")

			'Dim ddlBindCleint As New DropDownListBinder
			'ddlBindCleint.BindData(Employeelistddl, "SelProjectClients", "client_num", "fullName", Session("CSCConn"))

			If iUserSecurityLevel = 65 Then
				ReportTypeddl.SelectedValue = "Allclient"
			End If

			If iUserSecurityLevel > 70 And iUserSecurityLevel < 79 Then
				ReportTypeddl.SelectedValue = "Allclient"
			End If

			If iUserSecurityLevel > 79 Or iUserSecurityLevel = 66 Then
				ReportTypeddl.SelectedValue = "all"
			End If

			Dim ddlBindMonth As New DropDownListBinder
			ddlBindMonth.BindData(MonthNameddl, "SelMonthNameRolling", "Workdatenum", "WorkDates", Session("CSCConn"))

			If Request.QueryString("ProjectNum") <> "" Then
				HDProjectNum.Text = Request.QueryString("Projectnum")
				LoadGrid()

				gvrojectHrs.Visible = True

				Projectsddl.SelectedValue = HDProjectNum.Text


			End If

			Dim c3Controller = New Client3Controller(HDClientnum.Text, "no", Session("EmployeeConn"))

			empName.Text = c3Controller.FirstName & " " & c3Controller.LastName
			siemensempnumLabel.Text = c3Controller.SiemensEmpNum
			'ntloginTextBox.Text = c3Controller.LoginName
			'userpositiondescTextBox.Text = c3Controller.UserPositionDesc
			'phoneTextBox.Text = c3Controller.Phone
			'e_mailTextBox.Text = c3Controller.EMail
			DepartmentName.Text = c3Controller.EntityName & " - " & c3Controller.DepartmentName
			'DepartmentCd.text = c3Controller.DepartmentCd

			EntityCd.Text = c3Controller.EntityCd & " " & c3Controller.DepartmentCd

		End If
		'If iUserSecurityLevel > 90 Then
		'	MgrTab.Visible = True
		'End If

		If tabs.ActiveTab.TabIndex = 2 Then

			'If iUserSecurityLevel > 79 Then

			'	Employeelistddl.Visible = True
			'End If



			LoadTotalsReport()
		End If

		'SelMonthNameRolling

	End Sub

	Private Sub LoadGrid()

		Dim thisData As New CommandsSqlAndOleDb("SelProjectHrsByuser", Session("CSCConn"))
		thisData.AddSqlProcParameter("@ProjectNum", HDProjectNum.Text, SqlDbType.Int, 8)
		thisData.AddSqlProcParameter("@client_num", HDClientnum.Text, SqlDbType.Int, 8)

		dt = thisData.GetSqlDataTable
		dst = thisData.GetSqlDataset()

		gvrojectHrs.DataSource = thisData.GetSqlDataTable
		gvrojectHrs.DataBind()

	End Sub
	Private Sub btnSubmitRpt_Click(sender As Object, e As EventArgs) Handles btnSubmitRpt.Click

		If HDRptprojectnum.Text = "" Then
			lblValidation.Text = "Need to select a Project"
			lblValidation.Visible = True
			Exit Sub
		End If

		'If iUserSecurityLevel > 79 Then
		'	If HDTypeReport.Text = "projclient" Or HDTypeReport.Text = "Allclient" Then
		'		If HDRptClientnum.Text = "" Then
		'			lblValidation.Text = "Need to select a Employee"
		'			lblValidation.Visible = True
		'			Exit Sub

		'		End If

		'	End If
		'End If



		LoadTotalsReport()

	End Sub

	Private Sub LoadTotalsReport()
		lblValidation.Visible = False
		lblValidation.Text = ""


		If ReportTypeddl.SelectedValue = "Allclient" Then
			HDRptprojectnum.Text = ""
			Projectlist2ddl.SelectedValue = "Select"
			Projectlist2ddl.SelectedIndex = -1

		End If

		If ReportTypeddl.SelectedValue = "all" Then
			HDRptprojectnum.Text = ""

			Projectlist2ddl.SelectedValue = "Select"

		End If

		Dim thisData As New CommandsSqlAndOleDb("SelProjectTotalsByProj", Session("CSCConn"))

		thisData.AddSqlProcParameter("@ProjectNum", HDRptprojectnum.Text, SqlDbType.NVarChar, 12)
		thisData.AddSqlProcParameter("@client_num", HDRptClientnum.Text, SqlDbType.NVarChar, 12)
		thisData.AddSqlProcParameter("@ReportType", ReportTypeddl.SelectedValue, SqlDbType.NVarChar, 12)

		totaldt = thisData.GetSqlDataTable
		dst = thisData.GetSqlDataset()

		Session("ProjTotaltbl") = totaldt


		If iUserSecurityLevel = 65 Then
			gvEmployeeProject.DataSource = thisData.GetSqlDataTable
			gvEmployeeProject.DataBind()

			gvEmployeeProject.Visible = True
			gvprojectTotals.Visible = False
		End If

		If iUserSecurityLevel > 70 And iUserSecurityLevel < 79 Then
			gvEmployeeProject.DataSource = thisData.GetSqlDataTable
			gvEmployeeProject.DataBind()

			gvEmployeeProject.Visible = True
			gvprojectTotals.Visible = False

		End If

		If iUserSecurityLevel > 79 Or iUserSecurityLevel = 66 Then
			If ReportTypeddl.SelectedValue = "projall" Then
				gvEmployeeProject.DataSource = thisData.GetSqlDataTable
				gvEmployeeProject.DataBind()

				gvEmployeeProject.Visible = True
				gvprojectTotals.Visible = False


			End If
			If ReportTypeddl.SelectedValue = "all" Or ReportTypeddl.SelectedValue = "projTotal" Or ReportTypeddl.SelectedValue = "projclient" Or ReportTypeddl.SelectedValue = "Allclient" Then

				gvprojectTotals.DataSource = thisData.GetSqlDataTable
				gvprojectTotals.DataBind()

				gvEmployeeProject.Visible = False
				gvprojectTotals.Visible = True

			End If



		End If

	End Sub
	Private Sub Projectsddl_SelectedIndexChanged(sender As Object, e As EventArgs) Handles Projectsddl.SelectedIndexChanged
		HDProjectNum.Text = Projectsddl.SelectedValue.ToString
		Currprojectnamelbl.Text = Projectsddl.SelectedItem.ToString

		LoadGrid()

		gvrojectHrs.Visible = True


	End Sub
	Private Sub btnSubmithours_Click(sender As Object, e As EventArgs) Handles btnSubmithours.Click

		Dim thisData As New CommandsSqlAndOleDb("InsUpdProjectEmpWeek", Session("CSCConn"))
		thisData.AddSqlProcParameter("@ProjectNum", HDProjectNum.Text, SqlDbType.Int, 8)
		thisData.AddSqlProcParameter("@client_num", HDClientnum.Text, SqlDbType.Int, 8)
		thisData.AddSqlProcParameter("@tech_num", HDTechnum.Text, SqlDbType.Int, 8)
		'			@WorkDate datetime = null,
		thisData.AddSqlProcParameter("@WorkDateNum", HDWorkdatenum.Text, SqlDbType.Int, 8)
		thisData.AddSqlProcParameter("@Week1HoursWorked", Week1HoursWorkedddl.SelectedValue, SqlDbType.Float, 8)
		thisData.AddSqlProcParameter("@Week2HoursWorked", Week2HoursWorkedddl.SelectedValue, SqlDbType.Float, 8)
		thisData.AddSqlProcParameter("@Week3HoursWorked", Week3HoursWorkedddl.SelectedValue, SqlDbType.Float, 8)
		thisData.AddSqlProcParameter("@Week4HoursWorked", Week4HoursWorkedddl.SelectedValue, SqlDbType.Float, 8)
		thisData.AddSqlProcParameter("@Week5HoursWorked", Week5HoursWorkedddl.SelectedValue, SqlDbType.Float, 8)
		thisData.ExecNonQueryNoReturn()

		Response.Redirect("./ProjectHoursMaintenance.aspx?ProjectNum=" & HDProjectNum.Text, True)



	End Sub
	Private Sub gvrojectHrs_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvrojectHrs.RowCommand
		If e.CommandName = "Select" Then
			Dim projectnum As String = gvrojectHrs.DataKeys(Convert.ToInt32(e.CommandArgument))("ProjectNum").ToString()
			Dim weekdatenum As String = gvrojectHrs.DataKeys(Convert.ToInt32(e.CommandArgument))("WorkDateNum").ToString()

			HDWorkdatenum.Text = weekdatenum
			HDProjectNum.Text = projectnum

			Dim week1 As String = gvrojectHrs.DataKeys(Convert.ToInt32(e.CommandArgument))("Week1HoursWorked").ToString()
			Dim week2 As String = gvrojectHrs.DataKeys(Convert.ToInt32(e.CommandArgument))("Week2HoursWorked").ToString()
			Dim week3 As String = gvrojectHrs.DataKeys(Convert.ToInt32(e.CommandArgument))("Week3HoursWorked").ToString()
			Dim week4 As String = gvrojectHrs.DataKeys(Convert.ToInt32(e.CommandArgument))("Week4HoursWorked").ToString()
			Dim week5 As String = gvrojectHrs.DataKeys(Convert.ToInt32(e.CommandArgument))("Week5HoursWorked").ToString()

			Dim currDate As String = gvrojectHrs.DataKeys(Convert.ToInt32(e.CommandArgument))("WorkDate").ToString()

			CurrDatelbl.Text = currDate

			HDWeek1HoursWorked.Text = week1
			HDWeek2HoursWorked.Text = week2
			HDWeek3HoursWorked.Text = week3
			HDWeek4HoursWorked.Text = week4
			HDWeek5HoursWorked.Text = week5

			Dim ddlBinder As New DropDownListBinder
			ddlBinder.BindData(Week1HoursWorkedddl, "selhrsforWork", "hrs", "hrs", Session("CSCConn"))
			ddlBinder.BindData(Week2HoursWorkedddl, "selhrsforWork", "hrs", "hrs", Session("CSCConn"))
			ddlBinder.BindData(Week3HoursWorkedddl, "selhrsforWork", "hrs", "hrs", Session("CSCConn"))
			ddlBinder.BindData(Week4HoursWorkedddl, "selhrsforWork", "hrs", "hrs", Session("CSCConn"))
			ddlBinder.BindData(Week5HoursWorkedddl, "selhrsforWork", "hrs", "hrs", Session("CSCConn"))

			Week1HoursWorkedddl.SelectedValue = HDWeek1HoursWorked.Text
			Week2HoursWorkedddl.SelectedValue = HDWeek2HoursWorked.Text
			Week3HoursWorkedddl.SelectedValue = HDWeek3HoursWorked.Text
			Week4HoursWorkedddl.SelectedValue = HDWeek4HoursWorked.Text
			Week5HoursWorkedddl.SelectedValue = HDWeek5HoursWorked.Text


            tpProjectlist.Visible = False


            HrsEntry.Visible = True

		End If
	End Sub

	Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click


		clearstuff()

	End Sub
	Private Sub clearstuff()
		tpProjectlist.Visible = True

		gvrojectHrs.Visible = False
		HrsEntry.Visible = False

		HDWeek1HoursWorked.Text = ""
		HDWeek2HoursWorked.Text = ""
		HDWeek3HoursWorked.Text = ""
		HDWeek4HoursWorked.Text = ""
		HDWeek5HoursWorked.Text = ""

		CurrDatelbl.Text = ""
		Currprojectnamelbl.Text = ""

		HDWorkdatenum.Text = ""
		HDProjectNum.Text = ""

		Projectsddl.SelectedIndex = -1
		gvrojectHrs.SelectedIndex = -1

	End Sub
	Private Sub gvprojectTotals_Sorting(sender As Object, e As GridViewSortEventArgs) Handles gvprojectTotals.Sorting

		Dim sortdt = TryCast(Session("ProjTotaltbl"), DataTable)

		If sortdt IsNot Nothing Then

			'Sort the data.
			sortdt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression)
			gvprojectTotals.DataSource = sortdt
			gvprojectTotals.DataBind()

		End If

	End Sub
	Private Sub gvEmployeeProject_Sorting(sender As Object, e As GridViewSortEventArgs) Handles gvEmployeeProject.Sorting

		Dim sortdt = TryCast(Session("ProjTotaltbl"), DataTable)

		If sortdt IsNot Nothing Then

			'Sort the data.
			sortdt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression)
			gvEmployeeProject.DataSource = sortdt
			gvEmployeeProject.DataBind()

		End If
	End Sub

	Private Function GetSortDirection(ByVal column As String) As String

		' By default, set the sort direction to ascending.
		Dim sortDirection = "ASC"

		' Retrieve the last column that was sorted.
		Dim sortExpression = TryCast(ViewState("SortExpression"), String)

		If sortExpression IsNot Nothing Then
			' Check if the same column is being sorted.
			' Otherwise, the default value can be returned.
			If sortExpression = column Then
				Dim lastDirection = TryCast(ViewState("SortDirection"), String)
				If lastDirection IsNot Nothing _
				  AndAlso lastDirection = "ASC" Then

					sortDirection = "DESC"

				End If
			End If
		End If

		' Save new values in ViewState.
		ViewState("SortDirection") = sortDirection
		ViewState("SortExpression") = column

		Return sortDirection

	End Function
	Private Sub Projectlist2ddl_SelectedIndexChanged(sender As Object, e As EventArgs) Handles Projectlist2ddl.SelectedIndexChanged
		HDRptprojectnum.Text = Projectlist2ddl.SelectedValue
	End Sub

	'Private Sub Employeelistddl_SelectedIndexChanged(sender As Object, e As EventArgs) Handles Employeelistddl.SelectedIndexChanged
	'	HDRptClientnum.Text = Employeelistddl.SelectedValue
	'End Sub

	Private Sub ReportTypeddl_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ReportTypeddl.SelectedIndexChanged
		HDRptClientnum.Text = ""
		'If iUserSecurityLevel > 79 Then
		'	If ReportTypeddl.SelectedValue = "projclient" Or ReportTypeddl.SelectedValue = "Allclient" Then
		'		Employeelistddl.Visible = True
		'		selemplbl.Visible = True


		'		HDTypeReport.Text = ReportTypeddl.SelectedValue

		'		Projectlist2ddl.SelectedValue = "Select"
		'		Employeelistddl.SelectedValue = "Select"

		'	End If
		'End If


	End Sub

	Private Sub BtnExcel_Click(sender As Object, e As EventArgs) Handles BtnExcel.Click

		LoadTotalsReport()

		CreateExcel2(totaldt)

	End Sub
	Sub CreateExcel2(ByVal ReportTable As DataTable)



		Response.AddHeader("content-disposition", "attachment;filename=FileName.xls")

		Response.Charset = String.Empty
		Response.ContentType = "application/vnd.ms-excel"
		'  Response.ContentType = "application/vnd.xls"

		Dim sw As System.IO.StringWriter = New System.IO.StringWriter()

		Dim hw As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(sw)
		Dim dg As New DataGrid

		' commented try new style of excel 2017
		'===============================Create form to contain grid
		'Dim frm As New HtmlForm()

		'GridHidden.Visible = True

		'GridHidden.Columns.RemoveAt(0)
		'GridHidden.Parent.Controls.Add(frm)
		'frm.Attributes("RunAt") = "server"
		'frm.Controls.Add(GridHidden)

		'frm.RenderControl(hw)
		'new style from open provider page

		' must be name of datasource on page
		dg.DataSource = ReportTable
		Dim i As Integer = dg.Items.Count
		dg.DataBind()
		'  dg.GridLines = GridLines.None
		' ClearControls(dg)

		dg.HeaderStyle.Font.Bold = True
		dg.HeaderStyle.BackColor = Drawing.Color.LightGray


		dg.RenderControl(hw)


		Response.Write(sw.ToString())

		Response.End()

		'If iUserSecurityLevel = 65 Then

		'	gvEmployeeProject.Visible = True
		'	gvprojectTotals.Visible = False
		'End If

		'If iUserSecurityLevel > 70 And iUserSecurityLevel < 79 Then

		'	gvEmployeeProject.Visible = True
		'	gvprojectTotals.Visible = False

		'End If

		'If iUserSecurityLevel > 79 Then
		'	If ReportTypeddl.SelectedValue = "projall" Then

		'		gvEmployeeProject.Visible = True
		'		gvprojectTotals.Visible = False


		'	End If
		'	If ReportTypeddl.SelectedValue = "all" Or ReportTypeddl.SelectedValue = "projTotal" Or ReportTypeddl.SelectedValue = "projclient" Or ReportTypeddl.SelectedValue = "Allclient" Then

		'		gvEmployeeProject.Visible = False
		'		gvprojectTotals.Visible = True

		'	End If



		'End If


	End Sub

	Private Sub gvEmployeeProject_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvEmployeeProject.RowCommand
		If e.CommandName = "Select" Then
			Dim sClientNum As String = gvEmployeeProject.DataKeys(Convert.ToInt32(e.CommandArgument))("ClientNum").ToString()
			Dim sProjectNum As String = gvEmployeeProject.DataKeys(Convert.ToInt32(e.CommandArgument))("ProjectNum").ToString()


			Response.Redirect("~/ProjectDetailTotalView.aspx?ClientNum=" & sClientNum & "&ProjectNum=" & sProjectNum, True)

		End If

	End Sub
End Class
