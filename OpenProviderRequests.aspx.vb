﻿Imports System.IO
Imports System.Data '
Imports System.Data.SqlClient
Imports App_Code
Imports AjaxControlToolkit
Imports C1.Web.UI.Controls.C1Window

Partial Class OpenProviderRequests
    Inherits MyBasePage '   Inherits System.Web.UI.Page


    Dim sActivitytype As String = ""
    Dim cblBinder As New CheckBoxListBinder
    Dim FormSignOn As New FormSQL()
    Dim AccountRequestNum As Integer
    Dim dt As DataTable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load



        If Not IsPostBack Then


            '    bindValidateQueue()



        End If
        fillProviderTable("all")
    End Sub
    Protected Sub btnOldOpenPrvd_Click(sender As Object, e As System.EventArgs) Handles btnOldOpenPrvd.Click
        If btnOldOpenPrvd.Text = "View 30 Day Old Request" Then
            tblProviderQueue.Rows.Clear()

            fillProviderTable("old")
            btnOldOpenPrvd.Text = "View Current Open Provider Req."
        Else
            tblProviderQueue.Rows.Clear()

            fillProviderTable("all")
            btnOldOpenPrvd.Text = "View 30 Day Old Request"
        End If


    End Sub

    Protected Sub fillProviderTable(ByRef sPrvType As String)
        Dim Procedure As String

        If sPrvType = "all" Then
            Procedure = "SelOpenProviderRequestV4"
        Else
            Procedure = "Sel60DaysOpenProviderRequestV4"
        End If

        Dim iRowCount = 1

        Dim thisData As New CommandsSqlAndOleDb(Procedure, Session("EmployeeConn"))
        dt = thisData.GetSqlDataTable
        rowsreturnlbl.Text = dt.Rows.Count.ToString

        Dim rwHeader As New TableRow
        Dim clSelectHeader As New TableCell
        Dim clNameHeader As New TableCell
        Dim clSubmitDateHeader As New TableCell
        Dim clRequestorHeader As New TableCell
        Dim clReqPhoneHeader As New TableCell
        Dim clAffilheader As New TableCell
        Dim clValidDateheader As New TableCell
        Dim clRequestDescheader As New TableCell


        'account_request_num,
        'receiver_client_num,
        'receiver_full_name,
        'account_request_type,
        'requestor_client_num,
        'requestor_name , 
        'requestor_phone,
        'entered_date,
        'Affiliation,
        'close_date,
        'valid_date,
        'valid_client_num,
        'validator_name,
        'request_desc


        clNameHeader.Text = "Req For"
        clAffilheader.Text = "Affiliation"
        clRequestorHeader.Text = "Requestor"
        clReqPhoneHeader.Text = "Req Phone"
        clSubmitDateHeader.Text = "Submit Date"
        clValidDateheader.Text = "Valid Date"
        clRequestDescheader.Text = "Request Desc"

        rwHeader.Cells.Add(clSelectHeader)
        rwHeader.Cells.Add(clNameHeader)
        rwHeader.Cells.Add(clAffilheader)
        rwHeader.Cells.Add(clRequestorHeader)
        rwHeader.Cells.Add(clReqPhoneHeader)
        rwHeader.Cells.Add(clSubmitDateHeader)
        rwHeader.Cells.Add(clValidDateheader)
        rwHeader.Cells.Add(clRequestDescheader)

        rwHeader.Font.Bold = True
        rwHeader.BackColor = Color.FromName("#006666")
        rwHeader.ForeColor = Color.FromName("White")
        rwHeader.Height = Unit.Pixel(36)

        tblProviderQueue.Rows.Add(rwHeader)

        For Each drRow As DataRow In dt.Rows

            'Dim btnSelect As New Button
            Dim rwData As New TableRow
            Dim clSelectData As New TableCell
            Dim btnSelect As New TableCell
            Dim clNameData As New TableCell
            Dim clAffilData As New TableCell
            Dim clSubmitDateData As New TableCell
            Dim clRequestorData As New TableCell
            Dim clReqPhoneData As New TableCell
            Dim clValidDate As New TableCell
            Dim clReqDesc As New TableCell


            'Dim sFirstName As String = IIf(IsDBNull(drRow("first_name")), "", drRow("first_name"))
            'Dim sLastName As String = IIf(IsDBNull(drRow("last_name")), "", drRow("last_name"))
            'drRow("account_request_num")
            btnSelect.Text = "<a href=""AccountRequestInfo2.aspx?RequestNum=" & drRow("account_request_num") & """ ><u><b> " & drRow("account_request_num") & "</b></u></a>"
            'btnSelect.Text = "Select"
            'btnSelect.CommandName = "Select"
            'btnSelect.CommandArgument = drRow("account_request_num")
            ' btnSelect.Width = 50
            btnSelect.EnableTheming = False

            'AddHandler btnSelect.Click, AddressOf btnSelect_Click

            'clSelectData.Controls.Add(btnSelect)
            clNameData.Text = IIf(IsDBNull(drRow("receiver_full_name")), "", drRow("receiver_full_name"))
            clAffilData.Text = IIf(IsDBNull(drRow("Affiliation")), "", drRow("Affiliation"))
            clRequestorData.Text = IIf(IsDBNull(drRow("requestor_name")), "", drRow("requestor_name"))
            clReqPhoneData.Text = IIf(IsDBNull(drRow("requestor_phone")), "", drRow("requestor_phone"))
            clSubmitDateData.Text = IIf(IsDBNull(drRow("entered_date")), "", drRow("entered_date"))
            clValidDate.Text = IIf(IsDBNull(drRow("valid_date")), "", drRow("valid_date"))
            clReqDesc.Text = IIf(IsDBNull(drRow("request_desc")), "", drRow("request_desc"))

            'rwData.Cells.Add(clSelectData)
            rwData.Cells.Add(btnSelect)
            rwData.Cells.Add(clNameData)
            rwData.Cells.Add(clAffilData)
            rwData.Cells.Add(clRequestorData)
            rwData.Cells.Add(clReqPhoneData)
            rwData.Cells.Add(clSubmitDateData)
            rwData.Cells.Add(clValidDate)
            rwData.Cells.Add(clReqDesc)

            If iRowCount > 0 Then
                rwData.BackColor = Color.Bisque
            Else
                rwData.BackColor = Color.LightGray
            End If
            tblProviderQueue.CellSpacing = 5

            tblProviderQueue.Rows.Add(rwData)

            iRowCount = iRowCount * -1

        Next
        tblProviderQueue.Width = New Unit("100%")


    End Sub
    Sub btnSelect_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim btnLinkView As Button = sender
        If btnLinkView.CommandName = "Select" Then

            Dim sArguments As String = btnLinkView.CommandArgument

            Response.Redirect("./AccountRequestInfo2.aspx?RequestNum=" & sArguments)

            '            Response.Redirect("./OpenProviderRequestItems.aspx?RequestNum=" & sArguments(0))

        End If
    End Sub
    Protected Sub CreateExcel(ByVal ReportTable As DataTable)
        Dim gvData As GridView


        Session("UCCurrentMethod") = "CreateDocOrExcel()"
        ' GridView1.Visible = True
        'doesnt work with gridviews that have controls in them
        Response.ClearContent()
        Dim attachment As String = "attachment; filename=ExcelDownload.xls"

        HttpContext.Current.Response.ContentType = "application/excel"
        HttpContext.Current.Response.AddHeader("Content-disposition", attachment)


        Dim strStyle As String = "<style>.text {mso-number-format:\@; } </style>"   ' to help retain leading zeroes
        Response.Write(strStyle)
        Response.Write("<H4 align='center'>")  ' throw in a pageheader just for the heck of it to tell people what the data is and when it was run
        '  Response.Write("Parts Listing as of " & Date.Now.ToString("d"))
        Response.Write(Session("Hospital") & " Room List")
        Response.Write("</H4>")
        Response.Write("<BR>")
        'GridView1.HeaderStyle.Font.Bold = True
        'GridView1.HeaderStyle.BackColor = Drawing.Color.LightGray

        Dim sw As StringWriter = New StringWriter()
        Dim hw As HtmlTextWriter = New HtmlTextWriter(sw)
        Dim dg As New DataGrid


        ' must be name of datasource on page
        dg.DataSource = ReportTable
        Dim i As Integer = dg.Items.Count
        dg.DataBind()
        '  dg.GridLines = GridLines.None
        ' ClearControls(dg)

        dg.HeaderStyle.Font.Bold = True
        dg.HeaderStyle.BackColor = Drawing.Color.LightGray


        dg.RenderControl(hw)
        '   Dim dt As New DataTable
        ' Dim dv As New DataView

        'dv.Table.ToString()
        Response.Write(sw.ToString())
        Response.End()

        ' GridView1.AllowSorting = True
        sw = Nothing
        hw = Nothing
        dg.Dispose()
    End Sub

    Protected Sub BtnExcel_Click(sender As Object, e As System.EventArgs) Handles BtnExcel.Click
        CreateExcel(dt)

    End Sub
End Class
