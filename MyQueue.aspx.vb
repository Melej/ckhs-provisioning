﻿Imports System.IO
Imports System.Data '
Imports System.Data.SqlClient
Imports System.Web.UI.Control
Imports iTextSharp.text.pdf


Imports App_Code
Imports AjaxControlToolkit
Partial Class MyQueue
    Inherits MyBasePage 'Inherits System.Web.UI.Page '
    Dim dsQueues As New DataSet("Queues")
    Dim cblBinder As New CheckBoxListBinder

    'Dim Appinfo As New Applications
    'Dim Apps As New Applications

    Dim BaseApps As New List(Of Application)
    Dim NewBaseApps As New List(Of AllMyApplications)

    Dim AllGroupQueuesInfo As New GroupQueues
    Dim AllMyGroupInfo As New GroupQueues

    'GetAllGroupQueues

    Dim changeGroup As String
    Dim Groups As String = ""


    Dim allgrouplist As New List(Of AllGroupQueues)
    Dim MyQueuelist As New List(Of CurrentTickectRFSReportItems)
    Dim MyGroupList As New List(Of CurrentTickectRFSReportItems)
    Dim NoTechGroupList As New List(Of CurrentTickectRFSReportItems)

    Dim Unassignedlist As New List(Of CurrentTickectRFSReportItems)
    Dim Openp1List As New List(Of CurrentTickectRFSReportItems)
    'Dim GroupOwnership As New List(Of String)

    Dim Mydt As DataTable

    Dim TicketRFSList As New TicketsRFS
    Dim conn As New SetDBConnection()
    Dim sAccountType As String
    Dim technum As Integer
    Dim Tablerows As Integer

    Dim sUnassigned As String = ""
    Dim sNoTechGroup As String = ""
    Dim scscqueue As String = ""
    Dim sP1 As String = ""

    Dim GroupNumber As Integer = 0
    Dim GroupOwnership As List(Of Integer)

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        'Session("LoginID") = "durr05"

        Dim Security As New UserSecurity(Session("LoginID"), Session("EmployeeConn"))




        'session("ShowAll") = "ShowAll" is only for Show all groups else OFF

        technum = Security.TechNum
        hdtechnumLabel.Text = Security.TechNum.ToString
        GroupOwnership = Security.MySecGroups

        PanView.Visible = False

        If Session("LoginID").ToString.ToLower = "melej" Then
            connid.Visible = True
            connid.Text = "CSC Conn " & Session("CSCConn").ToString & "Employee Conn " & Session("EmployeeConn").ToString

        End If



        '' use to here 
        'sUnassigned = Request.QueryString("Unassigned")
        sNoTechGroup = Request.QueryString("NoTech")
        sP1 = Request.QueryString("P1Queue")

        sUnassigned = Session("Myqueue")
        sNoTechGroup = Session("NoTech")
        sP1 = Session("P1Queue")

        If sUnassigned = "yes" Then
            rblShowGroup.SelectedValue = "unassigned"
            HDRadioBt.Text = "unassigned"

            Session("Myqueue") = ""
            Session("cscqueue") = ""

        ElseIf sNoTechGroup = "yes" Then
            rblShowGroup.SelectedValue = "notech"
            HDRadioBt.Text = "notech"

            Session("NoTech") = ""
            Session("cscqueue") = ""
        ElseIf sP1 = "yes" Then
            rblShowGroup.SelectedValue = "openp1"
            HDRadioBt.Text = "openp1"

            Session("P1Queue") = ""
            Session("cscqueue") = ""
        End If




        If rblShowGroup.SelectedValue <> "" Then

            Session("cscqueue") = rblShowGroup.SelectedValue
            HDRadioBt.Text = rblShowGroup.SelectedValue

        ElseIf Session("cscqueue") <> "" Then
            rblShowGroup.SelectedValue = Session("cscqueue")
            HDRadioBt.Text = Session("cscqueue")
        Else
            rblShowGroup.SelectedValue = "myqueue"
            Session("cscqueue") = "myqueue"
            HDRadioBt.Text = "myqueue"
        End If

        'If HDRadioBt.Text <> "" Then
        '    rblShowGroup.SelectedValue = HDRadioBt.Text
        'End If

        If Not IsPostBack Then
            ' Now its here


            ' returns a list of applications User checked to view
            ' this returns all applications only base numbers not individual sites 31

            txtStartDate.Enabled = False
            'txtStartDate.Text = DateAdd(DateInterval.Day, -31, Date.Today)
            HDStartDate.Text = DateAdd(DateInterval.Day, -1095, Date.Today)
            HDEnddate.Text = Date.Today

            If LblLastRefresh.Text = "" Then

                LblLastRefresh.Text = "Last Update: " & DateTime.Now.ToString()
            End If

            txtEndDate.Enabled = False
            'txtEndDate.Text = Date.Today

            pnlDate.Visible = False

            AllGroupQueuesInfo.GetAllGroupQueues(HttpContext.Current.Session("CSCConn").ToString, "queue", Session("LoginID"))

            'allgrouplist.Clear()

            allgrouplist = AllGroupQueuesInfo.AllGroups

            cblGroupAccountQueues.Dispose()

            cblGroupAccountQueues.DataSource = allgrouplist

            cblGroupAccountQueues.DataValueField = "GroupNum"
            cblGroupAccountQueues.DataTextField = "GroupName"
            cblGroupAccountQueues.DataBind()


            If Session("cscqueue") <> "" Then

                scscqueue = Session("cscqueue")


                Select Case scscqueue
                    Case "myqueue"
                        rblShowGroup.SelectedValue = "myqueue"

                    Case "groupqueue"
                        rblShowGroup.SelectedValue = "groupqueue"

                    Case "unassigned"
                        rblShowGroup.SelectedValue = "unassigned"

                    Case "notech"
                        rblShowGroup.SelectedValue = "notech"
                    Case "openp1"
                        rblShowGroup.SelectedValue = "openp1"

                End Select

            End If


            Select Case rblShowGroup.SelectedValue
                Case "myqueue"
                    rblStatus.Enabled = False
                    rblStatus.SelectedValue = "Open"
                    hdStatus.Text = "open"

                    rbltype.Enabled = False
                    rbltype.SelectedValue = "both"
                    hdType.Text = "both"

                    rblPriority.Enabled = False
                    rblPriority.SelectedValue = "all"
                    hdPriority.Text = "all"



                    HDRadioBt.Text = "myqueue"
                    Session("ShowAll") = ""
                    Session("cscqueue") = "myqueue"

                    'btnShowAll.Text = "My Queue"
                    lblCurrentView.Text = ""
                    lblCurrentView.Text = "Now Showing My Queue"
                    TicketRFSList.GetMyQueue(Session("LoginID"), HttpContext.Current.Session("CSCConn").ToString)


                    MyQueuelist = TicketRFSList.AllMyQueue()

                    GetMyQueue()

                Case "openp1"
                    rblStatus.Enabled = False
                    rblStatus.SelectedValue = "Open"
                    hdStatus.Text = "open"

                    rbltype.Enabled = False
                    rbltype.SelectedValue = "both"
                    hdType.Text = "both"

                    rblPriority.Enabled = False
                    rblPriority.SelectedValue = "1"
                    hdPriority.Text = "1"

                    HDRadioBt.Text = "openp1"


                    Session("ShowAll") = ""

                    pnlDate.Visible = False
                    Session("ShowAll") = ""

                    'btnShowAll.Text = "My Queue"
                    lblCurrentView.Text = "Now Showing P1 Tickets"

                    TicketRFSList.GetP1Queue("open", HttpContext.Current.Session("CSCConn").ToString)

                    Openp1List = TicketRFSList.AllMyQueue

                    GetUnassignedQueue()


                Case "groupqueue"

                    rblStatus.Enabled = False
                    rblStatus.SelectedValue = "Open"
                    hdStatus.Text = "open"

                    rbltype.Enabled = False
                    rbltype.SelectedValue = "both"
                    hdType.Text = "both"

                    rblPriority.Enabled = False
                    rblPriority.SelectedValue = "all"
                    hdPriority.Text = "all"


                    HDRadioBt.Text = "groupqueue"
                    Session("ShowAll") = ""
                    Session("cscqueue") = "groupqueue"


                    lblCurrentView.Text = "Now Showing My Groups"

                    TicketRFSList.GetAllMyGroupQueue(Session("LoginID"), HttpContext.Current.Session("CSCConn").ToString)

                    MyQueuelist = TicketRFSList.AllMyQueue

                    Dim checkBoxes = From it As ListItem In cblGroupAccountQueues.Items
                        Join own As Integer In GroupOwnership On it.Value Equals own.ToString
                        Select it
                    For Each itm In checkBoxes
                        itm.Selected = True
                    Next

                    GetMyQueue()

                    'For Each cbGroupQueue As ListItem In cblGroupAccountQueues.Items
                    '    If cbGroupQueue.Selected = True Then
                    '        Groups = Groups & "," & CType(cbGroupQueue.Value, String)
                    '        hdGroups.Text = hdGroups.Text & "," & CType(cbGroupQueue.Value, String)
                    '    End If
                    'Next


                Case "unassigned"
                    Session("ShowAll") = ""
                    rblStatus.Enabled = False
                    rblStatus.SelectedValue = "Open"
                    hdStatus.Text = "open"

                    rbltype.Enabled = False
                    rbltype.SelectedValue = "both"
                    hdType.Text = "both"

                    rblPriority.Enabled = False
                    rblPriority.SelectedValue = "all"
                    hdPriority.Text = "all"



                    HDRadioBt.Text = "unassigned"
                    Session("ShowAll") = ""
                    Session("cscqueue") = "unassigned"


                    'btnShowAll.Text = "My Queue"
                    lblCurrentView.Text = "Now Showing Unassigned Queue"

                    TicketRFSList.GetUnAssignQueue(Session("LoginID"), HttpContext.Current.Session("CSCConn").ToString)

                    Unassignedlist = TicketRFSList.AllMyQueue

                    GetUnassignedQueue()

                Case "notech"
                    Session("ShowAll") = ""
                    Session("cscqueue") = "notech"

                    'For Each cbGroupQueue As ListItem In cblGroupAccountQueues.Items
                    '    If cbGroupQueue.Selected = False Then
                    '        cbGroupQueue.Selected = True
                    '        Groups = Groups & "," & CType(cbGroupQueue.Value, String)
                    '        hdGroups.Text = hdGroups.Text & "," & CType(cbGroupQueue.Value, String)
                    '    End If
                    'Next

                    rblStatus.Enabled = False
                    rblStatus.SelectedValue = "Open"
                    hdStatus.Text = "open"

                    rbltype.Enabled = False
                    rbltype.SelectedValue = "both"
                    hdType.Text = "both"

                    rblPriority.Enabled = False
                    rblPriority.SelectedValue = "all"
                    hdPriority.Text = "all"

                    HDRadioBt.Text = "notechgroupqueue"

                    lblCurrentView.Text = "Now Showing Assigned Groups NO Techs"

                    TicketRFSList.GetNoTechQueue(Session("LoginID"), HttpContext.Current.Session("CSCConn").ToString)

                    NoTechGroupList = TicketRFSList.NoTechGroup


                    'Dim checkBoxes = From it As ListItem In cblGroupAccountQueues.Items
                    '    Join own As Integer In GroupOwnership On it.Value Equals own.ToString
                    '    Select it
                    'For Each itm In checkBoxes
                    '    itm.Selected = True
                    'Next
                    btnShowAll.Enabled = False
                    cblGroupAccountQueues.Enabled = False

                    GetNoTechGroupData()

                Case Else
                    ' if a Doctor master number exist

            End Select

            sNoTechGroup = ""
            sUnassigned = ""
            HDRadioBt.Text = ""
        End If

        If rblShowGroup.SelectedValue = "" Then
            rblShowGroup.SelectedValue = "myqueue"

        End If

        tblGroupAccountQueues.Width = New Unit("100%")
        tblGroupAccountQueues.Height = New Unit("100%")
        tblGroupAccountQueues.Visible = True

        btnShowAll.Enabled = True
        cblGroupAccountQueues.Enabled = True

        sNoTechGroup = ""
        sUnassigned = ""


    End Sub
    Public Sub GetMyQueue()

        Dim appHeaderCreated As Boolean = False
        Dim iOrderBy As Integer
        Dim currReqNum As Integer

        Groups = ""
        hdGroups.Text = ""

        If rblStatus.SelectedValue = "both" Then
            lblCurrentView.Text = lblCurrentView.Text & " Open and Closed "
        Else
            lblCurrentView.Text = lblCurrentView.Text & " Only " & rblStatus.SelectedValue

        End If

        If rbltype.SelectedValue = "both" Then
            lblCurrentView.Text = lblCurrentView.Text & " Tickets and RFS "

        Else
            lblCurrentView.Text = lblCurrentView.Text & " " & rbltype.SelectedValue

        End If


        'For Each cblItem As ListItem In cblGroupAccountQueues.Items
        '    If cblItem.Selected = True Then
        '        Groups = Groups & "," & CType(cblItem.Value, String)
        '        hdGroups.Text = hdGroups.Text & "," & CType(cblItem.Value, String)
        '    End If

        'Next


        appHeaderCreated = False

        Dim iRowCount As Integer = 1

        MyQueuelist = TicketRFSList.AllMyQueue


        For Each ari As CurrentTickectRFSReportItems In MyQueuelist
            currReqNum = 0

            iOrderBy = ari.OrderbyNum
            currReqNum = ari.request_num

            If currReqNum = 0 Then

                'tblAccountQueues.Rows.Add(rowjeff)
                Dim rwSuperHeader As New TableRow
                Dim rwSuperColTitle As New TableRow

                Dim clSuperheader As New TableCell

                rwSuperHeader.Font.Bold = True
                rwSuperHeader.BackColor = Color.FromName("#FFFFCC")
                rwSuperHeader.ForeColor = Color.FromName("Black")
                rwSuperHeader.Font.Size = 12


                clSuperheader.ColumnSpan = 9

                clSuperheader.Text = ari.status_desc



                rwSuperHeader.Cells.Add(clSuperheader)

                tblGroupAccountQueues.Rows.Add(rwSuperHeader)


                Dim rwHeader As New TableRow
                Dim rwColTitle As New TableRow

                Dim clHeader As New TableCell
                Dim clReqNumTitle As New TableCell
                Dim clclientNameTitle As New TableCell
                Dim clRequestTypeTitle As New TableCell

                Dim clentrydateTitle As New TableCell
                Dim clTimeInQueueTitle As New TableCell
                Dim clcategoryTitle As New TableCell
                Dim clFacilityTitle As New TableCell
                Dim clpriorityTitle As New TableCell
                'Dim clGroupNameTitle As New TableCell
                Dim clTechNameTitle As New TableCell
                Dim clBDescTitle As New TableCell
                'Dim cltypeTitle As New TableCell

                rwHeader.Font.Bold = True
                rwHeader.BackColor = Color.FromName("#FFFFCC")
                rwHeader.ForeColor = Color.FromName("Black")
                rwHeader.Font.Size = 10

                clHeader.ColumnSpan = 9

                'If Session("ViewRequestType") = "open" Then
                '    clHeader.Text = ari.GroupName & " Group "

                'ElseIf Session("ViewRequestType") = "closed" Then
                '    clHeader.Text = ari.GroupName & " Closed Cases "
                'Else
                '    clHeader.Text = ari.RequestTypeDesc & " PAST 30 Days Invalid Requests"
                'End If

                clHeader.Text = ari.GroupName

                rwHeader.Cells.Add(clHeader)

                tblGroupAccountQueues.Rows.Add(rwHeader)


                rwColTitle.Font.Bold = True
                rwColTitle.BackColor = Color.FromName("#006666")
                rwColTitle.ForeColor = Color.FromName("White")
                rwColTitle.Height = Unit.Pixel(36)

                clReqNumTitle.Text = "Ticket/RFS #"
                clclientNameTitle.Text = "Client Name"
                clRequestTypeTitle.Text = "Req. Type"
                clentrydateTitle.Text = "Entry Date"
                clentrydateTitle.Width = Unit.Pixel(55)
                clTimeInQueueTitle.Text = "Time in Queue"
                clTimeInQueueTitle.Width = Unit.Pixel(55)
                clcategoryTitle.Text = "Category"
                clBDescTitle.Text = "Brief Description"
                'cltypeTitle.Text = "Cat. Type"
                clpriorityTitle.Text = "Priority"
                clFacilityTitle.Text = "Facility"
                'clGroupNameTitle.Text = "Group Name"
                clTechNameTitle.Text = "Tech Name"


                rwColTitle.Cells.Add(clReqNumTitle)
                rwColTitle.Cells.Add(clclientNameTitle)
                rwColTitle.Cells.Add(clentrydateTitle)
                rwColTitle.Cells.Add(clTimeInQueueTitle)

                rwColTitle.Cells.Add(clRequestTypeTitle)
                rwColTitle.Cells.Add(clFacilityTitle)

                'rwColTitle.Cells.Add(cltypeTitle)


                rwColTitle.Cells.Add(clpriorityTitle)
                rwColTitle.Cells.Add(clBDescTitle)
                'rwColTitle.Cells.Add(clGroupNameTitle)
                'rwColTitle.Cells.Add(clTechNameTitle)

                rwColTitle.Font.Size = 10
                tblGroupAccountQueues.Rows.Add(rwColTitle)

                appHeaderCreated = True
            End If
            '----------------------------------------
            ' End Header
            '----------------------------------------

            'requestnum
            'clientName
            'entrydate
            'closedate
            'request_type
            'category_cd
            'type_cd
            'priority
            'GroupNum
            'GroupName
            'TechName
            'status_desc
            'bDesc

            If currReqNum > 0 Then


                Dim rwData As New TableRow
                Dim clReqNum As New TableCell
                Dim clClientName As New TableCell
                Dim clEntryDate As New TableCell
                Dim clCloseDate As New TableCell
                Dim clReqType As New TableCell
                Dim clCategory As New TableCell
                Dim clbDesc As New TableCell
                Dim clTimeInQueue As New TableCell
                Dim clpriority As New TableCell
                Dim clFacility As New TableCell
                Dim clTechName As New TableCell


                If Session("ViewRequestType") = "" Then
                    Session("ViewRequestType") = "open"
                End If

                'If Session("ViewRequestType") = "open" Then
                '    clReqNum.Text = "<a href=""AccountRequestInfo2.aspx?requestnum=" & ari.HRRequestNum & """ ><u><b> " & ari.HRRequestNum & "</b></u></a>"
                'ElseIf Session("ViewRequestType") = "closed" Then
                '    clReqNum.Text = "<a href=""AccountRequestInfo2.aspx?requestnum=" & ari.HRRequestNum & """ ><u><b> " & ari.HRRequestNum & "</b></u></a>"
                'End If

                'clReqNum.Text = ari.request_num

                'clReqNum.Text = "<a href=""AccountRequestInfo2.aspx?requestnum=" & ari.request_num & """ ><u><b> " & ari.request_num & "</b></u></a>"
                clReqNum.Text = "<a href=""ViewRequestTicket.aspx?RequestNum=" & ari.request_num & """ ><u><b> " & ari.request_num & "</b></u></a>"


                clTechName.Text = ari.TechName
                clClientName.Text = ari.clientName
                clEntryDate.Text = ari.entry_date

                clCloseDate.Text = ari.close_date

                clbDesc.Text = ari.bDesc

                clReqType.Text = ari.request_type
                clCategory.Text = ari.category_cd
                'clCatType.Text = ari.type_cd
                clpriority.Text = ari.priority
                clFacility.Text = ari.facility_name
                clTechName.Text = ari.TechName
                clTimeInQueue.Text = ari.TimeInQueue


                rwData.Cells.Add(clReqNum)
                rwData.Cells.Add(clClientName)
                rwData.Cells.Add(clEntryDate)
                rwData.Cells.Add(clTimeInQueue)
                rwData.Cells.Add(clReqType)
                rwData.Cells.Add(clFacility)
                rwData.Cells.Add(clpriority)

                rwData.Cells.Add(clbDesc)


                'rwData.Cells.Add(clCatType)
                'rwData.Cells.Add(clGroupName)
                'rwData.Cells.Add(clTechName)
                rwData.Font.Size = 10

                'P1 backgroud
                If clpriority.Text = "1" Then
                    clpriority.ForeColor = Color.Red
                    clTechName.ForeColor = Color.Red
                    clClientName.ForeColor = Color.Red
                    clEntryDate.ForeColor = Color.Red

                    clbDesc.ForeColor = Color.Red

                    clReqType.ForeColor = Color.Red

                End If
                If iRowCount > 0 Then

                    ' rwData.BackColor = Color.Bisque

                    rwData.BackColor = Color.WhiteSmoke

                Else

                    rwData.BackColor = Color.LightGray

                End If

                tblGroupAccountQueues.Rows.Add(rwData)


                iRowCount = iRowCount * -1

            End If
        Next

        tblGroupAccountQueues.Width = New Unit("100%")
        tblGroupAccountQueues.Height = New Unit("100%")

        tblGroupAccountQueues.Visible = True

        btnShowAll.Enabled = True
        cblGroupAccountQueues.Enabled = True


    End Sub

    Public Sub GetUnassignedQueue()

        Dim appHeaderCreated As Boolean = False
        Dim iOrderBy As Integer
        Dim currReqNum As Integer

        'Groups = ""
        'hdGroups.Text = ""

        'If rblStatus.SelectedValue = "both" Then
        '    lblCurrentView.Text = lblCurrentView.Text & " Open and Closed "
        'Else
        '    lblCurrentView.Text = lblCurrentView.Text & " Only " & rblStatus.SelectedValue

        'End If

        'If rbltype.SelectedValue = "both" Then
        '    lblCurrentView.Text = lblCurrentView.Text & " Tickets and RFS "

        'Else
        '    lblCurrentView.Text = lblCurrentView.Text & " " & rbltype.SelectedValue

        'End If


        'For Each cblItem As ListItem In cblGroupAccountQueues.Items
        '    If cblItem.Selected = True Then
        '        Groups = Groups & "," & CType(cblItem.Value, String)
        '        hdGroups.Text = hdGroups.Text & "," & CType(cblItem.Value, String)
        '    End If

        'Next

        'TicketRFSList = New TicketsRFS(HDStartDate.Text, HDEnddate.Text, Groups, hdStatus.Text, hdType.Text, HttpContext.Current.Session("CSCConn").ToString, hdPriority.Text)

        appHeaderCreated = False

        Dim iRowCount As Integer = 1

        MyQueuelist = TicketRFSList.AllMyQueue
        For Each ari As CurrentTickectRFSReportItems In MyQueuelist
            currReqNum = 0

            iOrderBy = ari.OrderbyNum
            currReqNum = ari.request_num

            If appHeaderCreated = False Then


                'tblAccountQueues.Rows.Add(rowjeff)
                Dim rwSuperHeader As New TableRow
                Dim rwSuperColTitle As New TableRow

                Dim clSuperheader As New TableCell

                rwSuperHeader.Font.Bold = True
                rwSuperHeader.BackColor = Color.FromName("#FFFFCC")
                rwSuperHeader.ForeColor = Color.FromName("Black")
                rwSuperHeader.Font.Size = 12


                clSuperheader.ColumnSpan = 9

                clSuperheader.Text = ari.status_desc



                rwSuperHeader.Cells.Add(clSuperheader)

                tblGroupAccountQueues.Rows.Add(rwSuperHeader)


                Dim rwHeader As New TableRow
                Dim rwColTitle As New TableRow

                Dim clHeader As New TableCell
                Dim clReqNumTitle As New TableCell
                Dim clclientNameTitle As New TableCell
                Dim clRequestTypeTitle As New TableCell

                Dim clentrydateTitle As New TableCell
                Dim clTimeInQueueTitle As New TableCell
                Dim clcategoryTitle As New TableCell
                Dim clFacilityTitle As New TableCell

                Dim clpriorityTitle As New TableCell
                'Dim clGroupNameTitle As New TableCell
                Dim clTechNameTitle As New TableCell
                Dim clBDescTitle As New TableCell
                'Dim cltypeTitle As New TableCell

                rwHeader.Font.Bold = True
                rwHeader.BackColor = Color.FromName("#FFFFCC")
                rwHeader.ForeColor = Color.FromName("Black")
                rwHeader.Font.Size = 10

                clHeader.ColumnSpan = 9

                'If Session("ViewRequestType") = "open" Then
                '    clHeader.Text = ari.GroupName & " Group "

                'ElseIf Session("ViewRequestType") = "closed" Then
                '    clHeader.Text = ari.GroupName & " Closed Cases "
                'Else
                '    clHeader.Text = ari.RequestTypeDesc & " PAST 30 Days Invalid Requests"
                'End If

                clHeader.Text = ari.GroupName

                rwHeader.Cells.Add(clHeader)

                tblGroupAccountQueues.Rows.Add(rwHeader)


                rwColTitle.Font.Bold = True
                rwColTitle.BackColor = Color.FromName("#006666")
                rwColTitle.ForeColor = Color.FromName("White")
                rwColTitle.Height = Unit.Pixel(36)

                clReqNumTitle.Text = "Ticket/RFS #"
                clclientNameTitle.Text = "Client Name"
                clRequestTypeTitle.Text = "Req. Type"
                clentrydateTitle.Text = "Entry Date"
                clentrydateTitle.Width = Unit.Pixel(55)
                clTimeInQueueTitle.Text = "Time in Queue"
                clTimeInQueueTitle.Width = Unit.Pixel(55)
                clcategoryTitle.Text = "Category"
                clBDescTitle.Text = "Brief Description"
                'cltypeTitle.Text = "Cat. Type"
                clpriorityTitle.Text = "Priority"
                clFacilityTitle.Text = "Facility"

                'clGroupNameTitle.Text = "Group Name"
                clTechNameTitle.Text = "Tech Name"


                rwColTitle.Cells.Add(clReqNumTitle)
                rwColTitle.Cells.Add(clclientNameTitle)
                rwColTitle.Cells.Add(clentrydateTitle)
                rwColTitle.Cells.Add(clTimeInQueueTitle)

                rwColTitle.Cells.Add(clRequestTypeTitle)
                'rwColTitle.Cells.Add(clcategoryTitle)
                'rwColTitle.Cells.Add(cltypeTitle)


                rwColTitle.Cells.Add(clpriorityTitle)
                rwColTitle.Cells.Add(clFacilityTitle)

                rwColTitle.Cells.Add(clBDescTitle)
                'rwColTitle.Cells.Add(clGroupNameTitle)
                'rwColTitle.Cells.Add(clTechNameTitle)

                rwColTitle.Font.Size = 10
                tblGroupAccountQueues.Rows.Add(rwColTitle)

                appHeaderCreated = True
            End If
            '----------------------------------------
            ' End Header
            '----------------------------------------

            'requestnum
            'clientName
            'entrydate
            'closedate
            'request_type
            'category_cd
            'type_cd
            'priority
            'GroupNum
            'GroupName
            'TechName
            'status_desc
            'bDesc

            If currReqNum > 0 Then


                Dim rwData As New TableRow
                Dim clReqNum As New TableCell
                Dim clClientName As New TableCell
                Dim clEntryDate As New TableCell
                Dim clCloseDate As New TableCell
                Dim clReqType As New TableCell
                Dim clCategory As New TableCell
                Dim clbDesc As New TableCell
                Dim clTimeInQueue As New TableCell
                Dim clpriority As New TableCell

                Dim clFacility As New TableCell
                Dim clTechName As New TableCell


                If Session("ViewRequestType") = "" Then
                    Session("ViewRequestType") = "open"
                End If

                'If Session("ViewRequestType") = "open" Then
                '    clReqNum.Text = "<a href=""AccountRequestInfo2.aspx?requestnum=" & ari.HRRequestNum & """ ><u><b> " & ari.HRRequestNum & "</b></u></a>"
                'ElseIf Session("ViewRequestType") = "closed" Then
                '    clReqNum.Text = "<a href=""AccountRequestInfo2.aspx?requestnum=" & ari.HRRequestNum & """ ><u><b> " & ari.HRRequestNum & "</b></u></a>"
                'End If

                'clReqNum.Text = ari.request_num

                'clReqNum.Text = "<a href=""AccountRequestInfo2.aspx?requestnum=" & ari.request_num & """ ><u><b> " & ari.request_num & "</b></u></a>"
                clReqNum.Text = "<a href=""ViewRequestTicket.aspx?RequestNum=" & ari.request_num & """ ><u><b> " & ari.request_num & "</b></u></a>"


                clTechName.Text = ari.TechName
                clClientName.Text = ari.clientName
                clEntryDate.Text = ari.entry_date

                clCloseDate.Text = ari.close_date

                clbDesc.Text = ari.bDesc

                clReqType.Text = ari.request_type
                clCategory.Text = ari.category_cd
                clFacility.Text = ari.facility_name
                clpriority.Text = ari.priority
                'clGroupName.Text = ari.GroupName
                clTechName.Text = ari.TechName
                clTimeInQueue.Text = ari.TimeInQueue


                rwData.Cells.Add(clReqNum)
                rwData.Cells.Add(clClientName)
                rwData.Cells.Add(clEntryDate)
                rwData.Cells.Add(clTimeInQueue)
                rwData.Cells.Add(clReqType)
                'rwData.Cells.Add(clCategory)
                rwData.Cells.Add(clpriority)
                rwData.Cells.Add(clFacility)

                'clFacility
                rwData.Cells.Add(clbDesc)


                'rwData.Cells.Add(clGroupName)
                'rwData.Cells.Add(clTechName)
                rwData.Font.Size = 10

                'P1 backgroud
                If clpriority.Text = "1" Then
                    clpriority.ForeColor = Color.Red
                    clTechName.ForeColor = Color.Red
                    clClientName.ForeColor = Color.Red
                    clEntryDate.ForeColor = Color.Red

                    clbDesc.ForeColor = Color.Red

                    clReqType.ForeColor = Color.Red


                End If


                If iRowCount > 0 Then

                    ' rwData.BackColor = Color.Bisque

                    rwData.BackColor = Color.WhiteSmoke

                Else

                    rwData.BackColor = Color.LightGray

                End If

                tblGroupAccountQueues.Rows.Add(rwData)


                iRowCount = iRowCount * -1

            End If
        Next

        tblGroupAccountQueues.Width = New Unit("100%")
        tblGroupAccountQueues.Height = New Unit("100%")

        tblGroupAccountQueues.Visible = True

        btnShowAll.Enabled = True
        cblGroupAccountQueues.Enabled = True


    End Sub

    Public Sub GetQueueDataSet()

        If rblStatus.SelectedValue = "both" Then
            lblCurrentView.Text = lblCurrentView.Text & " Open and Closed "
        Else
            lblCurrentView.Text = lblCurrentView.Text & " Only " & rblStatus.SelectedValue

        End If

        Select Case rbltype.SelectedValue
            Case "both"
                lblCurrentView.Text = lblCurrentView.Text & " Tickets and RFS "
            Case "Tickets"
                lblCurrentView.Text = lblCurrentView.Text & " " & rbltype.SelectedValue
            Case "RFS"
                lblCurrentView.Text = lblCurrentView.Text & " " & rbltype.SelectedValue
            Case "Projects"
                lblCurrentView.Text = lblCurrentView.Text & " " & rbltype.SelectedValue
        End Select

        hdGroups.Text = ""

        'If rbltype.SelectedValue = "both" Then
        '    lblCurrentView.Text = lblCurrentView.Text & " Tickets and RFS "

        'Else
        '    lblCurrentView.Text = lblCurrentView.Text & " " & rbltype.SelectedValue

        'End If

        If Session("ShowAll") = "ShowAll" Then

            For Each cbGroupQueue As ListItem In cblGroupAccountQueues.Items
                If cbGroupQueue.Selected = False Then
                    cbGroupQueue.Selected = True
                    Groups = Groups & "," & CType(cbGroupQueue.Value, String)
                    hdGroups.Text = hdGroups.Text & "," & CType(cbGroupQueue.Value, String)
                End If
            Next


            btnShowAll.Text = "Uncheck All"
            rblShowGroup.SelectedValue = "groupqueue"
        Else
            Session("ShowAll") = "Showgroups"

            lblCurrentView.Text = "Now Showing Selected Groups"

            For Each cbGroupQueue As ListItem In cblGroupAccountQueues.Items
                If cbGroupQueue.Selected = True Then
                    Groups = Groups & "," & CType(cbGroupQueue.Value, String)
                    hdGroups.Text = hdGroups.Text & "," & CType(cbGroupQueue.Value, String)
                End If
            Next

        End If

        'GetAllMyQueue
        'AllGroupQueuesInfo.GetAllMyQueue(HDStartDate.Text, HDEnddate.Text, Groups, hdStatus.Text, hdType.Text, HttpContext.Current.Session("CSCConn").ToString, hdPriority.Text)

        'allgrouplist.Clear()

        'allgrouplist = AllGroupQueuesInfo.AllGroups

        If HDRadioBt.Text = "groupqueue" Then
            ' show my groups 
            If hdType.Text = "Projects" Then
                TicketRFSList = New TicketsRFS(HDStartDate.Text, HDEnddate.Text, Groups, hdStatus.Text, hdType.Text, HttpContext.Current.Session("CSCConn").ToString, hdPriority.Text, hdtechnumLabel.Text)
            Else

                TicketRFSList = New TicketsRFS(HDStartDate.Text, HDEnddate.Text, Groups, hdStatus.Text, hdType.Text, HttpContext.Current.Session("CSCConn").ToString, hdPriority.Text, hdtechnumLabel.Text)
            End If
        Else

            If hdType.Text = "Projects" Then
                TicketRFSList = New TicketsRFS(HDStartDate.Text, HDEnddate.Text, Groups, hdStatus.Text, hdType.Text, HttpContext.Current.Session("CSCConn").ToString, hdPriority.Text, hdtechnumLabel.Text)
            Else

                TicketRFSList = New TicketsRFS(HDStartDate.Text, HDEnddate.Text, Groups, hdStatus.Text, hdType.Text, HttpContext.Current.Session("CSCConn").ToString, hdPriority.Text, Nothing)


            End If

        End If

        Dim RequestType As String
        Dim appHeaderCreated As Boolean = False
        Dim Checkitem As Integer
        Dim CurrLevel As String
        Dim PrevLevel As String = "start"
        Dim CurrType As String
        Dim PrevType As String = "start"

        Dim CurrGroup As Integer
        Dim PrevGroup As Integer = 0



        Dim rwHeader As New TableRow


        ' for each of the 31 base applications 
        For Each cblItem As ListItem In cblGroupAccountQueues.Items
            ' if the app is checked then 
            If cblItem.Selected = True Then


                Dim iRowCount As Integer = 1

                'now for each Category that are checked
                'For Each app As AllMyGroups In AllMyGroupInfo.GroupByGroupNumber(cblItem.Value)
                'For Each app As AllGroupQueues In AllGroupQueuesInfo.AllGroupByGroupNumber(cblItem.Value)
                '.GroupByGroupNumber(cblItem.Value)
                'AllGroupQueuesInfo.AllGroups AllGroupQueues

                appHeaderCreated = False
                Checkitem = cblItem.Value

                'If Session("GroupNum") <> "" Then
                '    Checkitem = Session("GroupNum")
                'End If

                For Each ari As CurrentTickectRFSReportItems In TicketRFSList.GetAllRequestByGroup(cblItem.Value)
                    '.OrderByDescending(Function(x) x.request_type)
                    CurrLevel = ari.LevelStatus
                    CurrType = ari.request_type.TrimEnd
                    CurrGroup = ari.GroupNum


                    If CurrGroup <> PrevGroup Then

                        Dim rwSuperGroupHeader As New TableRow
                        Dim clSuperGroupheader As New TableCell


                        clSuperGroupheader.Font.Bold = True
                        clSuperGroupheader.BackColor = Color.FromName("#FFFFCC")
                        clSuperGroupheader.ForeColor = Color.FromName("Black")
                        clSuperGroupheader.Font.Size = 10

                        clSuperGroupheader.ColumnSpan = 9

                        clSuperGroupheader.Text = ari.GroupName

                        rwSuperGroupHeader.Cells.Add(clSuperGroupheader)

                        tblGroupAccountQueues.Rows.Add(rwSuperGroupHeader)

                        PrevGroup = CurrGroup
                        PrevType = "start"



                    End If

                    If CurrLevel = "Unacknowledged" Then


                        If appHeaderCreated = False Then

                            Dim rwUnackHeader As New TableRow
                            Dim rwUnackColTitle As New TableRow

                            Dim clUnackheader As New TableCell

                            Dim clUnackReqNumTitle As New TableCell
                            Dim clUnackclientNameTitle As New TableCell
                            Dim clUnackRequestTypeTitle As New TableCell

                            Dim clUnackentrydateTitle As New TableCell
                            Dim clUnackpriorityTitle As New TableCell
                            Dim clUnackTechNameTitle As New TableCell
                            Dim clUnackBDescTitle As New TableCell
                            Dim clUnaFacilityTitle As New TableCell




                            clUnackheader.Font.Bold = True
                            clUnackheader.BackColor = Color.FromName("#FFFFCC")
                            clUnackheader.ForeColor = Color.FromName("Black")
                            clUnackheader.Font.Size = 10

                            clUnackheader.ColumnSpan = 9

                            clUnackheader.Text = "Unacknowledged Requests for " & ari.GroupName

                            rwUnackHeader.Cells.Add(clUnackheader)

                            tblGroupAccountQueues.Rows.Add(rwUnackHeader)

                            rwUnackColTitle.Font.Bold = True
                            rwUnackColTitle.BackColor = Color.FromName("#006666")
                            rwUnackColTitle.ForeColor = Color.FromName("White")
                            rwUnackColTitle.Height = Unit.Pixel(36)

                            clUnackReqNumTitle.Text = "Ticket/RFS #"
                            clUnackclientNameTitle.Text = "Client Name"
                            clUnackRequestTypeTitle.Text = "Req. Type"
                            clUnackentrydateTitle.Text = "Entry Date"
                            clUnackentrydateTitle.Width = Unit.Pixel(55)

                            clUnackpriorityTitle.Text = "Priority"
                            clUnaFacilityTitle.Text = "Facility"
                            clUnackTechNameTitle.Text = "Tech Name"
                            clUnackBDescTitle.Text = "Brief Description"


                            rwUnackColTitle.Cells.Add(clUnackReqNumTitle)
                            rwUnackColTitle.Cells.Add(clUnackclientNameTitle)
                            rwUnackColTitle.Cells.Add(clUnackentrydateTitle)

                            rwUnackColTitle.Cells.Add(clUnackRequestTypeTitle)


                            rwUnackColTitle.Cells.Add(clUnackpriorityTitle)
                            rwUnackColTitle.Cells.Add(clUnaFacilityTitle)

                            rwUnackColTitle.Cells.Add(clUnackTechNameTitle)
                            rwUnackColTitle.Cells.Add(clUnackBDescTitle)

                            rwUnackColTitle.Font.Size = 10
                            tblGroupAccountQueues.Rows.Add(rwUnackColTitle)


                            appHeaderCreated = True

                        End If


                    Else

                        If CurrType <> PrevType Then ' type are RFS and Tickets
                            If PrevType = "start" Then
                                Select Case CurrType.TrimEnd
                                    Case "service"
                                        RequestType = "RFS "
                                    Case "ticket"
                                        RequestType = "Tickets "
                                    Case "project"
                                        RequestType = "Projects "
                                End Select

                                Dim rwStartSuperGroupHeader As New TableRow
                                Dim rwStartColTitle As New TableRow

                                Dim clStartSuperheader As New TableCell

                                Dim clStartHeader As New TableCell
                                Dim clStartReqNumTitle As New TableCell
                                Dim clStartclientNameTitle As New TableCell
                                Dim clStartRequestTypeTitle As New TableCell

                                Dim clStartentrydateTitle As New TableCell
                                Dim clStartpriorityTitle As New TableCell
                                Dim clFacilityTitle As New TableCell
                                Dim clStartTechNameTitle As New TableCell
                                Dim clStartBDescTitle As New TableCell

                                ' ************* new header *************

                                rwStartSuperGroupHeader.Font.Bold = True
                                rwStartSuperGroupHeader.BackColor = Color.FromName("#FFFFCC")
                                rwStartSuperGroupHeader.ForeColor = Color.FromName("Black")
                                rwStartSuperGroupHeader.Font.Size = 10

                                clStartSuperheader.ColumnSpan = 9

                                clStartSuperheader.Text = RequestType & " " & ari.LevelStatus

                                rwStartSuperGroupHeader.Cells.Add(clStartSuperheader)

                                tblGroupAccountQueues.Rows.Add(rwStartSuperGroupHeader)



                                rwStartColTitle.Font.Bold = True
                                rwStartColTitle.BackColor = Color.FromName("#006666")
                                rwStartColTitle.ForeColor = Color.FromName("White")
                                rwStartColTitle.Height = Unit.Pixel(36)

                                clStartReqNumTitle.Text = "Ticket/RFS #"
                                clStartclientNameTitle.Text = "Client Name"
                                clStartRequestTypeTitle.Text = "Req. Type"
                                clStartentrydateTitle.Text = "Entry Date"
                                clStartentrydateTitle.Width = Unit.Pixel(55)

                                clStartpriorityTitle.Text = "Priority"
                                clFacilityTitle.Text = "Facility"
                                clStartTechNameTitle.Text = "Tech Name"
                                clStartBDescTitle.Text = "Brief Description"


                                rwStartColTitle.Cells.Add(clStartReqNumTitle)
                                rwStartColTitle.Cells.Add(clStartclientNameTitle)
                                rwStartColTitle.Cells.Add(clStartentrydateTitle)

                                rwStartColTitle.Cells.Add(clStartRequestTypeTitle)


                                rwStartColTitle.Cells.Add(clStartpriorityTitle)
                                rwStartColTitle.Cells.Add(clFacilityTitle)
                                rwStartColTitle.Cells.Add(clStartTechNameTitle)
                                rwStartColTitle.Cells.Add(clStartBDescTitle)

                                rwStartColTitle.Font.Size = 10
                                tblGroupAccountQueues.Rows.Add(rwStartColTitle)



                                ' ************* end new header ****************
                            Else
                                Dim rwSuperHeader As New TableRow
                                'Dim rwSuperColTitle As New TableRow
                                Dim rwColTitle As New TableRow

                                Dim clSuperheader As New TableCell
                                Dim clHeader As New TableCell
                                Dim clReqNumTitle As New TableCell
                                Dim clclientNameTitle As New TableCell
                                Dim clRequestTypeTitle As New TableCell

                                Dim clentrydateTitle As New TableCell
                                Dim clpriorityTitle As New TableCell
                                Dim clFacilityTitle As New TableCell
                                Dim clTechNameTitle As New TableCell
                                Dim clBDescTitle As New TableCell

                                Select Case CurrType.TrimEnd
                                    Case "service"
                                        RequestType = "RFS "
                                    Case "ticket"
                                        RequestType = "Tickets "
                                    Case "project"
                                        RequestType = "Projects "
                                End Select
                                'If CurrType = "service " Then
                                '    RequestType = "RFS "
                                'Else
                                '    RequestType = "Tickets "
                                'End If

                                rwSuperHeader.Font.Bold = True
                                rwSuperHeader.BackColor = Color.FromName("#FFFFCC")
                                rwSuperHeader.ForeColor = Color.FromName("Black")
                                rwSuperHeader.Font.Size = 10

                                clSuperheader.ColumnSpan = 9

                                clSuperheader.Text = RequestType & " " & ari.LevelStatus

                                rwSuperHeader.Cells.Add(clSuperheader)

                                tblGroupAccountQueues.Rows.Add(rwSuperHeader)

                                '  Just put title header here start of each Group

                                rwColTitle.Font.Bold = True
                                rwColTitle.BackColor = Color.FromName("#006666")
                                rwColTitle.ForeColor = Color.FromName("White")
                                rwColTitle.Height = Unit.Pixel(36)

                                clReqNumTitle.Text = "Ticket/RFS #"
                                clclientNameTitle.Text = "Client Name"
                                clRequestTypeTitle.Text = "Req. Type"
                                clentrydateTitle.Text = "Entry Date"
                                clentrydateTitle.Width = Unit.Pixel(55)
                                clpriorityTitle.Text = "Priority"
                                clFacilityTitle.Text = "Faciity"
                                clTechNameTitle.Text = "Tech Name"
                                clBDescTitle.Text = "Brief Description"


                                rwColTitle.Cells.Add(clReqNumTitle)
                                rwColTitle.Cells.Add(clclientNameTitle)
                                rwColTitle.Cells.Add(clentrydateTitle)

                                rwColTitle.Cells.Add(clRequestTypeTitle)

                                rwColTitle.Cells.Add(clpriorityTitle)
                                rwColTitle.Cells.Add(clFacilityTitle)
                                rwColTitle.Cells.Add(clTechNameTitle)
                                rwColTitle.Cells.Add(clBDescTitle)

                                rwColTitle.Font.Size = 10
                                tblGroupAccountQueues.Rows.Add(rwColTitle)





                            End If


                        End If


                        'If CurrLevel <> PrevLevel Then ' "Unacknowledged" Group assigned and Tech assigned

                        '    clSuperheader.Text = RequestType & " Levvel " & ari.LevelStatus

                        '    rwSuperHeader.Cells.Add(clSuperheader)

                        '    tblGroupAccountQueues.Rows.Add(rwSuperHeader)


                        '    'Dim cltypeTitle As New TableCell

                        '    rwHeader.Font.Bold = True
                        '    rwHeader.BackColor = Color.FromName("#FFFFCC")
                        '    rwHeader.ForeColor = Color.FromName("Black")
                        '    rwHeader.Font.Size = 8

                        '    clHeader.ColumnSpan = 9

                        '    PrevLevel = CurrLevel



                        'End If

                        PrevType = CurrType


                    End If

                    'If CurrLevel <> "Unacknowledged" Then

                    '    clSuperheader.Text = CurrType & " " & ari.LevelStatus

                    '    rwSuperHeader.Cells.Add(clSuperheader)

                    '    tblGroupAccountQueues.Rows.Add(rwSuperHeader)


                    '    'Dim cltypeTitle As New TableCell

                    '    rwHeader.Font.Bold = True
                    '    rwHeader.BackColor = Color.FromName("#FFFFCC")
                    '    rwHeader.ForeColor = Color.FromName("Black")
                    '    rwHeader.Font.Size = 8

                    '    clHeader.ColumnSpan = 9

                    '    'If Session("ViewRequestType") = "open" Then
                    '    '    clHeader.Text = ari.GroupName & " Group "

                    '    'ElseIf Session("ViewRequestType") = "closed" Then
                    '    '    clHeader.Text = ari.GroupName & " Closed Cases "
                    '    'Else
                    '    '    clHeader.Text = ari.RequestTypeDesc & " PAST 30 Days Invalid Requests"
                    '    'End If

                    '    clHeader.Text = ari.GroupName

                    '    rwHeader.Cells.Add(clHeader)

                    '    tblGroupAccountQueues.Rows.Add(rwHeader)


                    '    rwColTitle.Font.Bold = True
                    '    rwColTitle.BackColor = Color.FromName("#006666")
                    '    rwColTitle.ForeColor = Color.FromName("White")
                    '    rwColTitle.Height = Unit.Pixel(36)

                    '    clReqNumTitle.Text = "Ticket/RFS #"
                    '    clclientNameTitle.Text = "Client Name"
                    '    clRequestTypeTitle.Text = "Req. Type"
                    '    clentrydateTitle.Text = "Entry Date"
                    '    clentrydateTitle.Width = Unit.Pixel(55)
                    '    'clclosedateTitle.Text = "Close Date"
                    '    'clclosedateTitle.Width = Unit.Pixel(55)
                    '    'clcategoryTitle.Text = "Category"
                    '    'cltypeTitle.Text = "Cat. Type"
                    '    clpriorityTitle.Text = "Priority"
                    '    'clGroupNameTitle.Text = "Group Name"
                    '    clTechNameTitle.Text = "Tech Name"
                    '    clBDescTitle.Text = "Brief Description"


                    '    rwColTitle.Cells.Add(clReqNumTitle)
                    '    rwColTitle.Cells.Add(clclientNameTitle)
                    '    rwColTitle.Cells.Add(clentrydateTitle)
                    '    'rwColTitle.Cells.Add(clclosedateTitle)

                    '    rwColTitle.Cells.Add(clRequestTypeTitle)
                    '    'rwColTitle.Cells.Add(clcategoryTitle)
                    '    'rwColTitle.Cells.Add(cltypeTitle)


                    '    rwColTitle.Cells.Add(clpriorityTitle)
                    '    'rwColTitle.Cells.Add(clGroupNameTitle)
                    '    rwColTitle.Cells.Add(clTechNameTitle)
                    '    rwColTitle.Cells.Add(clBDescTitle)

                    '    rwColTitle.Font.Size = 8
                    '    tblGroupAccountQueues.Rows.Add(rwColTitle)


                    '    PrevType = CurrType


                    'End If



                    'If appHeaderCreated = False Then
                    '    appHeaderCreated = True
                    'End If
                    '----------------------------------------
                    ' End Header
                    '----------------------------------------

                    'requestnum
                    'clientName
                    'entrydate
                    'closedate
                    'request_type
                    'category_cd
                    'type_cd
                    'priority
                    'GroupNum
                    'GroupName
                    'TechName
                    'status_desc
                    'bDesc


                    Dim rwData As New TableRow
                    Dim clReqNum As New TableCell
                    Dim clClientName As New TableCell
                    Dim clEntryDate As New TableCell
                    'Dim clCloseDate As New TableCell
                    Dim clReqType As New TableCell
                    'Dim clCategory As New TableCell
                    ' Dim clCatType As New TableCell

                    Dim clpriority As New TableCell
                    Dim clFacility As New TableCell
                    'Dim clGroupName As New TableCell
                    Dim clTechName As New TableCell
                    Dim clbDesc As New TableCell



                    If Session("ViewRequestType") = "" Then
                        Session("ViewRequestType") = "open"
                    End If

                    clReqNum.Text = "<a href=""ViewRequestTicket.aspx?RequestNum=" & ari.request_num & """ ><u><b> " & ari.request_num & "</b></u></a>"


                    clTechName.Text = ari.TechName
                    clClientName.Text = ari.clientName
                    clEntryDate.Text = ari.entry_date

                    'clCloseDate.Text = ari.close_date

                    clReqType.Text = ari.request_type
                    'clCategory.Text = ari.category_cd
                    'clCatType.Text = ari.type_cd
                    clpriority.Text = ari.priority
                    clFacility.Text = ari.facility_name
                    clTechName.Text = ari.TechName
                    clbDesc.Text = ari.bDesc

                    rwData.Cells.Add(clReqNum)
                    rwData.Cells.Add(clClientName)
                    rwData.Cells.Add(clEntryDate)
                    'rwData.Cells.Add(clCloseDate)
                    rwData.Cells.Add(clReqType)
                    'rwData.Cells.Add(clCategory)

                    'rwData.Cells.Add(clCatType)
                    rwData.Cells.Add(clpriority)
                    rwData.Cells.Add(clFacility)
                    rwData.Cells.Add(clTechName)
                    rwData.Cells.Add(clbDesc)
                    rwData.Font.Size = 10

                    'P1 backgroud
                    If clpriority.Text = "1" Then
                        clpriority.ForeColor = Color.Red
                        clTechName.ForeColor = Color.Red
                        clClientName.ForeColor = Color.Red
                        clEntryDate.ForeColor = Color.Red

                        clbDesc.ForeColor = Color.Red

                        clReqType.ForeColor = Color.Red


                    End If

                    If iRowCount > 0 Then

                        ' rwData.BackColor = Color.Bisque

                        rwData.BackColor = Color.WhiteSmoke

                    Else

                        rwData.BackColor = Color.LightGray

                    End If

                    tblGroupAccountQueues.Rows.Add(rwData)


                    iRowCount = iRowCount * -1

                Next
                'Next
            End If
        Next
        tblGroupAccountQueues.Width = New Unit("100%")
        tblGroupAccountQueues.Height = New Unit("100%")

        tblGroupAccountQueues.Visible = True

        btnShowAll.Enabled = True
        cblGroupAccountQueues.Enabled = True


    End Sub

    Public Sub GetNoTechGroupData()

        If rblStatus.SelectedValue = "both" Then
            lblCurrentView.Text = lblCurrentView.Text & " Open and Closed "
        Else
            lblCurrentView.Text = lblCurrentView.Text & " Only " & rblStatus.SelectedValue

        End If

        Select Case rbltype.SelectedValue
            Case "both"
                lblCurrentView.Text = lblCurrentView.Text & " Tickets and RFS "
            Case "Tickets"
                lblCurrentView.Text = lblCurrentView.Text & " " & rbltype.SelectedValue
            Case "RFS"
                lblCurrentView.Text = lblCurrentView.Text & " " & rbltype.SelectedValue
            Case "Projects"
                lblCurrentView.Text = lblCurrentView.Text & " " & rbltype.SelectedValue
        End Select

        hdGroups.Text = ""


        Dim RequestType As String = ""
        Dim appHeaderCreated As Boolean = False
        Dim CurrLevel As String
        Dim PrevLevel As String = "start"
        Dim CurrType As String
        Dim PrevType As String = "start"

        Dim CurrGroup As Integer
        Dim PrevGroup As Integer = 0



        Dim rwHeader As New TableRow


        ' for each of the 31 base applications 
        For Each cblItem As ListItem In cblGroupAccountQueues.Items
            cblItem.Selected = False
        Next

        Dim iRowCount As Integer = 1

        appHeaderCreated = False
        'Checkitem = cblItem.Value

        NoTechGroupList = TicketRFSList.NoTechGroup
        For Each ari As CurrentTickectRFSReportItems In NoTechGroupList

            '.OrderByDescending(Function(x) x.request_type)
            CurrLevel = ari.LevelStatus
            CurrType = ari.request_type.TrimEnd
            CurrGroup = ari.GroupNum


            If CurrGroup > 0 Then

                For Each cblItem As ListItem In cblGroupAccountQueues.Items
                    If cblItem.Value = CurrGroup Then
                        cblItem.Selected = True
                        hdGroups.Text = hdGroups.Text & "," & CType(cblItem.Value, String)
                    End If
                Next

            End If

            If CurrGroup <> PrevGroup Then

                Dim rwSuperGroupHeader As New TableRow
                Dim clSuperGroupheader As New TableCell


                clSuperGroupheader.Font.Bold = True
                clSuperGroupheader.BackColor = Color.FromName("#FFFFCC")
                clSuperGroupheader.ForeColor = Color.FromName("Black")
                clSuperGroupheader.Font.Size = 12

                clSuperGroupheader.ColumnSpan = 9

                clSuperGroupheader.Text = ari.GroupName

                rwSuperGroupHeader.Cells.Add(clSuperGroupheader)

                tblGroupAccountQueues.Rows.Add(rwSuperGroupHeader)

                PrevGroup = CurrGroup
                PrevType = "start"



            End If

            If CurrLevel = "Unacknowledged" Then


                If appHeaderCreated = False Then

                    Dim rwUnackHeader As New TableRow
                    Dim rwUnackColTitle As New TableRow

                    Dim clUnackheader As New TableCell

                    Dim clUnackReqNumTitle As New TableCell
                    Dim clUnackclientNameTitle As New TableCell
                    Dim clUnackRequestTypeTitle As New TableCell

                    Dim clUnackentrydateTitle As New TableCell
                    Dim clUnackpriorityTitle As New TableCell
                    Dim clUnackTechNameTitle As New TableCell
                    Dim clUnackBDescTitle As New TableCell
                    Dim clUnaFacilityTitle As New TableCell


                    clUnackheader.Font.Bold = True
                    clUnackheader.BackColor = Color.FromName("#FFFFCC")
                    clUnackheader.ForeColor = Color.FromName("Black")
                    clUnackheader.Font.Size = 10

                    clUnackheader.ColumnSpan = 9

                    clUnackheader.Text = "Unacknowledged Requests for " & ari.GroupName

                    rwUnackHeader.Cells.Add(clUnackheader)

                    tblGroupAccountQueues.Rows.Add(rwUnackHeader)

                    rwUnackColTitle.Font.Bold = True
                    rwUnackColTitle.BackColor = Color.FromName("#006666")
                    rwUnackColTitle.ForeColor = Color.FromName("White")
                    rwUnackColTitle.Height = Unit.Pixel(36)

                    clUnackReqNumTitle.Text = "Ticket/RFS #"
                    clUnackclientNameTitle.Text = "Client Name"
                    clUnackRequestTypeTitle.Text = "Req. Type"
                    clUnackentrydateTitle.Text = "Entry Date"
                    clUnackentrydateTitle.Width = Unit.Pixel(55)

                    clUnackpriorityTitle.Text = "Priority"
                    clUnaFacilityTitle.Text = "Facility"

                    clUnackTechNameTitle.Text = "Tech Name"
                    clUnackBDescTitle.Text = "Brief Description"


                    rwUnackColTitle.Cells.Add(clUnackReqNumTitle)
                    rwUnackColTitle.Cells.Add(clUnackclientNameTitle)
                    rwUnackColTitle.Cells.Add(clUnackentrydateTitle)

                    rwUnackColTitle.Cells.Add(clUnackRequestTypeTitle)


                    rwUnackColTitle.Cells.Add(clUnackpriorityTitle)
                    rwUnackColTitle.Cells.Add(clUnaFacilityTitle)

                    rwUnackColTitle.Cells.Add(clUnackTechNameTitle)
                    rwUnackColTitle.Cells.Add(clUnackBDescTitle)

                    rwUnackColTitle.Font.Size = 10
                    tblGroupAccountQueues.Rows.Add(rwUnackColTitle)


                    appHeaderCreated = True

                End If


            Else

                If CurrType <> PrevType Then ' type are RFS and Tickets
                    If PrevType = "start" Then
                        Select Case CurrType.TrimEnd
                            Case "service"
                                RequestType = "RFS "
                            Case "ticket"
                                RequestType = "Tickets "
                            Case "project"
                                RequestType = "Projects "
                        End Select

                        Dim rwStartSuperGroupHeader As New TableRow
                        Dim rwStartColTitle As New TableRow

                        Dim clStartSuperheader As New TableCell

                        Dim clStartHeader As New TableCell
                        Dim clStartReqNumTitle As New TableCell
                        Dim clStartclientNameTitle As New TableCell
                        Dim clStartRequestTypeTitle As New TableCell

                        Dim clStartentrydateTitle As New TableCell
                        Dim clStartpriorityTitle As New TableCell
                        Dim clStartFacilityTitle As New TableCell

                        Dim clStartTechNameTitle As New TableCell
                        Dim clStartBDescTitle As New TableCell

                        ' ************* new header *************

                        rwStartSuperGroupHeader.Font.Bold = True
                        rwStartSuperGroupHeader.BackColor = Color.FromName("#FFFFCC")
                        rwStartSuperGroupHeader.ForeColor = Color.FromName("Black")
                        rwStartSuperGroupHeader.Font.Size = 10

                        clStartSuperheader.ColumnSpan = 9

                        clStartSuperheader.Text = RequestType & " " & ari.LevelStatus

                        rwStartSuperGroupHeader.Cells.Add(clStartSuperheader)

                        tblGroupAccountQueues.Rows.Add(rwStartSuperGroupHeader)



                        rwStartColTitle.Font.Bold = True
                        rwStartColTitle.BackColor = Color.FromName("#006666")
                        rwStartColTitle.ForeColor = Color.FromName("White")
                        rwStartColTitle.Height = Unit.Pixel(36)

                        clStartReqNumTitle.Text = "Ticket/RFS #"
                        clStartclientNameTitle.Text = "Client Name"
                        clStartRequestTypeTitle.Text = "Req. Type"
                        clStartentrydateTitle.Text = "Entry Date"
                        clStartentrydateTitle.Width = Unit.Pixel(55)

                        clStartpriorityTitle.Text = "Priority"
                        clStartFacilityTitle.Text = "Facility"

                        clStartTechNameTitle.Text = "Tech Name"
                        clStartBDescTitle.Text = "Brief Description"


                        rwStartColTitle.Cells.Add(clStartReqNumTitle)
                        rwStartColTitle.Cells.Add(clStartclientNameTitle)
                        rwStartColTitle.Cells.Add(clStartentrydateTitle)

                        rwStartColTitle.Cells.Add(clStartRequestTypeTitle)


                        rwStartColTitle.Cells.Add(clStartpriorityTitle)
                        rwStartColTitle.Cells.Add(clStartFacilityTitle)

                        rwStartColTitle.Cells.Add(clStartTechNameTitle)
                        rwStartColTitle.Cells.Add(clStartBDescTitle)

                        rwStartColTitle.Font.Size = 10
                        tblGroupAccountQueues.Rows.Add(rwStartColTitle)



                        ' ************* end new header ****************
                    Else
                        Dim rwSuperHeader As New TableRow
                        'Dim rwSuperColTitle As New TableRow
                        Dim rwColTitle As New TableRow

                        Dim clSuperheader As New TableCell
                        Dim clHeader As New TableCell
                        Dim clReqNumTitle As New TableCell
                        Dim clclientNameTitle As New TableCell
                        Dim clRequestTypeTitle As New TableCell

                        Dim clentrydateTitle As New TableCell
                        Dim clpriorityTitle As New TableCell
                        Dim clFacilityTitle As New TableCell

                        Dim clTechNameTitle As New TableCell
                        Dim clBDescTitle As New TableCell

                        Select Case CurrType.TrimEnd
                            Case "service"
                                RequestType = "RFS "
                            Case "ticket"
                                RequestType = "Tickets "
                            Case "project"
                                RequestType = "Projects "
                        End Select
                        'If CurrType = "service " Then
                        '    RequestType = "RFS "
                        'Else
                        '    RequestType = "Tickets "
                        'End If

                        rwSuperHeader.Font.Bold = True
                        rwSuperHeader.BackColor = Color.FromName("#FFFFCC")
                        rwSuperHeader.ForeColor = Color.FromName("Black")
                        rwSuperHeader.Font.Size = 10

                        clSuperheader.ColumnSpan = 9

                        clSuperheader.Text = RequestType & " " & ari.LevelStatus

                        rwSuperHeader.Cells.Add(clSuperheader)

                        tblGroupAccountQueues.Rows.Add(rwSuperHeader)

                        '  Just put title header here start of each Group

                        rwColTitle.Font.Bold = True
                        rwColTitle.BackColor = Color.FromName("#006666")
                        rwColTitle.ForeColor = Color.FromName("White")
                        rwColTitle.Height = Unit.Pixel(36)

                        clReqNumTitle.Text = "Ticket/RFS #"
                        clclientNameTitle.Text = "Client Name"
                        clRequestTypeTitle.Text = "Req. Type"
                        clentrydateTitle.Text = "Entry Date"
                        clentrydateTitle.Width = Unit.Pixel(55)
                        clpriorityTitle.Text = "Priority"
                        clFacilityTitle.Text = "Facility"

                        clTechNameTitle.Text = "Tech Name"
                        clBDescTitle.Text = "Brief Description"


                        rwColTitle.Cells.Add(clReqNumTitle)
                        rwColTitle.Cells.Add(clclientNameTitle)
                        rwColTitle.Cells.Add(clentrydateTitle)

                        rwColTitle.Cells.Add(clRequestTypeTitle)

                        rwColTitle.Cells.Add(clpriorityTitle)
                        rwColTitle.Cells.Add(clFacilityTitle)

                        rwColTitle.Cells.Add(clTechNameTitle)
                        rwColTitle.Cells.Add(clBDescTitle)

                        rwColTitle.Font.Size = 10
                        tblGroupAccountQueues.Rows.Add(rwColTitle)





                    End If


                End If

                PrevType = CurrType

            End If
            '----------------------------------------
            ' End Header
            '----------------------------------------

            Dim rwData As New TableRow
            Dim clReqNum As New TableCell
            Dim clClientName As New TableCell
            Dim clEntryDate As New TableCell
            'Dim clCloseDate As New TableCell
            Dim clReqType As New TableCell
            'Dim clCategory As New TableCell
            ' Dim clCatType As New TableCell

            Dim clpriority As New TableCell
            Dim clFacility As New TableCell

            Dim clTechName As New TableCell
            Dim clbDesc As New TableCell


            If Session("ViewRequestType") = "" Then
                Session("ViewRequestType") = "open"
            End If

            clReqNum.Text = "<a href=""ViewRequestTicket.aspx?RequestNum=" & ari.request_num & """ ><u><b> " & ari.request_num & "</b></u></a>"


            clTechName.Text = ari.TechName
            clClientName.Text = ari.clientName
            clEntryDate.Text = ari.entry_date

            'clCloseDate.Text = ari.close_date

            clReqType.Text = ari.request_type
            'clCategory.Text = ari.category_cd
            'clCatType.Text = ari.type_cd
            clpriority.Text = ari.priority
            clFacility.Text = ari.facility_name
            clTechName.Text = ari.TechName
            clbDesc.Text = ari.bDesc

            rwData.Cells.Add(clReqNum)
            rwData.Cells.Add(clClientName)
            rwData.Cells.Add(clEntryDate)
            'rwData.Cells.Add(clCloseDate)
            rwData.Cells.Add(clReqType)
            'rwData.Cells.Add(clCatType)

            rwData.Cells.Add(clpriority)
            rwData.Cells.Add(clFacility)


            'rwData.Cells.Add(clGroupName)
            rwData.Cells.Add(clTechName)
            rwData.Cells.Add(clbDesc)
            rwData.Font.Size = 10



            'P1 backgroud
            If clpriority.Text = "1" Then
                clpriority.ForeColor = Color.Red
                clTechName.ForeColor = Color.Red
                clClientName.ForeColor = Color.Red
                clEntryDate.ForeColor = Color.Red

                clbDesc.ForeColor = Color.Red

                clReqType.ForeColor = Color.Red


            End If

            If iRowCount > 0 Then

                ' rwData.BackColor = Color.Bisque

                rwData.BackColor = Color.WhiteSmoke

            Else

                rwData.BackColor = Color.LightGray

            End If

            tblGroupAccountQueues.Rows.Add(rwData)


            iRowCount = iRowCount * -1

        Next

        tblGroupAccountQueues.Width = New Unit("100%")
        tblGroupAccountQueues.Height = New Unit("100%")

        tblGroupAccountQueues.Visible = True

        btnShowAll.Enabled = False
        cblGroupAccountQueues.Enabled = False


    End Sub

    Protected Sub btnShowAll_Click(sender As Object, e As System.EventArgs) Handles btnShowAll.Click
        lblCurrentView.Text = ""
        rblShowGroup.Enabled = True

        If btnShowAll.Text = "Check All" Then

            For Each cbGroupQueue As ListItem In cblGroupAccountQueues.Items
                If cbGroupQueue.Selected = False Then
                    cbGroupQueue.Selected = True
                    Groups = Groups & "," & CType(cbGroupQueue.Value, String)
                    hdGroups.Text = hdGroups.Text & "," & CType(cbGroupQueue.Value, String)
                End If
            Next
            pnlDate.Visible = False

            'txtStartDate.Enabled = False
            'txtStartDate.Text = DateAdd(DateInterval.Day, -31, Date.Today)
            'HDStartDate.Text = DateAdd(DateInterval.Day, -31, Date.Today)

            'txtEndDate.Enabled = False
            'txtEndDate.Text = Date.Today
            'HDEnddate.Text = Date.Today

            rblShowGroup.Enabled = True
            rblShowGroup.SelectedIndex = -1

            rblStatus.Enabled = True
            rblStatus.SelectedValue = "Open"
            hdStatus.Text = "open"

            rbltype.Enabled = True
            rbltype.SelectedValue = "both"
            hdType.Text = "both"

            rblPriority.Enabled = True
            rblPriority.SelectedValue = "all"
            hdPriority.Text = "all"


            Session("ShowAll") = "ShowAll"
            btnShowAll.Text = "Uncheck All"
            rblShowGroup.SelectedValue = "groupqueue"
            GetQueueDataSet()
        Else

            Session("ShowAll") = ""

            btnShowAll.Text = "Check All"
            lblCurrentView.Text = "Now Showing My Queue"

            For Each cbGroupQueue As ListItem In cblGroupAccountQueues.Items
                If cbGroupQueue.Selected = True Then
                    cbGroupQueue.Selected = False
                End If
            Next

            Groups = ""
            hdGroups.Text = ""
            rblShowGroup.SelectedValue = "myqueue"

            TicketRFSList.GetMyQueue(Session("LoginID"), HttpContext.Current.Session("CSCConn").ToString)

            MyQueuelist = TicketRFSList.AllMyQueue


            GetMyQueue()

        End If


    End Sub
    Protected Sub cblGroupAccountQueues_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cblGroupAccountQueues.SelectedIndexChanged

        lblCurrentView.Text = ""

        rblShowGroup.Enabled = True

        lblCurrentView.Text = ""

        'lblCurrentView.Text = "Now Showing All Groups"

        AllGroupQueuesInfo.GetAllGroupQueues(HttpContext.Current.Session("CSCConn").ToString, Session("LoginID").ToString)


        allgrouplist.Clear()

        Session("ShowAll") = "Showgroups"

        allgrouplist = AllGroupQueuesInfo.AllGroups
        pnlDate.Visible = False

        Groups = ""
        hdGroups.Text = ""

        lblCurrentView.Text = "Now Showing Selected Groups"

        For Each cbGroupQueue As ListItem In cblGroupAccountQueues.Items
            If cbGroupQueue.Selected = True Then
                Groups = Groups & "," & CType(cbGroupQueue.Value, String)
                hdGroups.Text = hdGroups.Text & "," & CType(cbGroupQueue.Value, String)
            End If
        Next


        'If Session("ShowAll") <> "" Then ' "ShowAll" Then

        '    ' this returns all applications only base numbers not individual sites 31
        '    lblCurrentView.Text = ""

        '    lblCurrentView.Text = "Now Showing All Groups"

        '    AllGroupQueuesInfo.GetAllGroupQueues(HttpContext.Current.Session("CSCConn").ToString)

        '    allgrouplist.Clear()

        '    allgrouplist = AllGroupQueuesInfo.AllGroups
        '    Groups = ""
        '    hdGroups.Text = ""

        '    lblCurrentView.Text = "Now Showing Selected Groups"

        '    For Each cbGroupQueue As ListItem In cblGroupAccountQueues.Items
        '        If cbGroupQueue.Selected = True Then
        '            Groups = Groups & "," & CType(cbGroupQueue.Value, String)
        '            hdGroups.Text = hdGroups.Text & "," & CType(cbGroupQueue.Value, String)
        '        End If
        '    Next



        'Else

        '    Session("ShowAll") = "Showgroups"

        '    Groups = ""
        '    hdGroups.Text = ""

        '    lblCurrentView.Text = "Now Showing Selected Groups"

        '    For Each cbGroupQueue As ListItem In cblGroupAccountQueues.Items
        '        If cbGroupQueue.Selected = True Then
        '            Groups = Groups & "," & CType(cbGroupQueue.Value, String)
        '            hdGroups.Text = hdGroups.Text & "," & CType(cbGroupQueue.Value, String)
        '        End If
        '    Next

        'End If

        'pnlDate.Visible = True


        'txtStartDate.Enabled = True
        'txtEndDate.Enabled = True

        'If HDStartDate.Text <> "" Then
        '    txtStartDate.Text = HDStartDate.Text
        'Else
        '    txtStartDate.Text = DateAdd(DateInterval.Day, -31, Date.Today)
        '    HDStartDate.Text = DateAdd(DateInterval.Day, -31, Date.Today)

        'End If

        'If HDEnddate.Text <> "" Then
        '    txtEndDate.Text = HDEnddate.Text
        'Else
        '    txtEndDate.Text = Date.Today
        '    HDEnddate.Text = Date.Today

        'End If

        'rblShowGroup.Enabled = False
        rblShowGroup.SelectedIndex = -1

        rblStatus.Enabled = True
        rblStatus.SelectedValue = "Open"


        rbltype.Enabled = True
        rbltype.SelectedValue = "both"

        rblPriority.Enabled = True
        rblPriority.SelectedValue = "all"

        'TicketRFSList = New TicketsRFS(HDStartDate.Text, HDEnddate.Text, Groups, hdStatus.Text, hdType.Text, HttpContext.Current.Session("CSCConn").ToString, hdPriority.Text)

        GetQueueDataSet()

    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As System.EventArgs) Handles btnSubmit.Click

        lbTotal.Text = ""

        HDRadioBt.Text = rblShowGroup.SelectedValue
        rblShowGroup.Enabled = True

        'txtStartDate.Enabled = True
        'txtEndDate.Enabled = True

        'If HDStartDate.Text <> "" Then
        '    txtStartDate.Text = HDStartDate.Text
        'Else
        '    txtStartDate.Text = DateAdd(DateInterval.Day, -31, Date.Today)
        '    HDStartDate.Text = DateAdd(DateInterval.Day, -31, Date.Today)

        'End If

        'If HDEnddate.Text <> "" Then
        '    txtEndDate.Text = HDEnddate.Text
        'Else
        '    txtEndDate.Text = Date.Today
        '    HDEnddate.Text = Date.Today

        'End If

        QueueLoadData()

    End Sub
    Protected Sub btnReset_Click(sender As Object, e As System.EventArgs) Handles btnReset.Click
        lblCurrentView.Text = ""

        Session("ShowAll") = ""
        Session("cscqueue") = ""


        Response.Redirect("~/" & "MyQueue.aspx")

    End Sub
    Protected Sub btnExcel_Click(sender As Object, e As System.EventArgs) Handles btnExcel.Click
        Dim sReportname As String


        If rblShowGroup.SelectedValue = "myqueue" Then

            sReportname = "myqueue"
        Else

            sReportname = "TicketRFSReport"

        End If


        Groups = hdGroups.Text

        Response.Redirect("./Reports/RolesReport.aspx?ReportName=" & sReportname & "&StartDate=" & HDStartDate.Text & "&enddate=" & HDEnddate.Text & "&GroupNum=" & Groups & "&Status=" & hdStatus.Text & "&RequestType=" & hdType.Text & "&Priority=" & hdPriority.Text & "&LoginName=" & Session("LoginID"), True)




    End Sub

    Protected Sub rblShowGroup_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles rblShowGroup.SelectedIndexChanged
        sNoTechGroup = ""
        sUnassigned = ""


        If HDRadioBt.Text <> "" Then
            rblShowGroup.SelectedValue = HDRadioBt.Text
        Else

            HDRadioBt.Text = rblShowGroup.SelectedValue

        End If


        lblCurrentView.Text = ""

        Groups = ""
        hdGroups.Text = ""

        For Each cbGroupQueue As ListItem In cblGroupAccountQueues.Items
            If cbGroupQueue.Selected = True Then
                cbGroupQueue.Selected = False
            End If
        Next

        rblStatus.Enabled = False
        rblStatus.SelectedValue = "Open"
        hdStatus.Text = "open"

        pnlDate.Visible = False

        'txtStartDate.Text = DateAdd(DateInterval.Day, -31, Date.Today)
        'HDStartDate.Text = DateAdd(DateInterval.Day, -31, Date.Today)

        'pnlDate.Visible = False


        'txtEndDate.Text = Date.Today
        'HDEnddate.Text = Date.Today

        rbltype.Enabled = False
        rbltype.SelectedValue = "both"
        hdType.Text = "both"

        rblPriority.Enabled = False
        rblPriority.SelectedValue = "all"
        hdPriority.Text = "all"

        Session("cscqueue") = rblShowGroup.SelectedValue

        Select Case rblShowGroup.SelectedValue
            Case "myqueue"

                HDRadioBt.Text = "myqueue"
                Session("ShowAll") = ""

                pnlDate.Visible = False

                'btnShowAll.Text = "My Queue"
                lblCurrentView.Text = "Now Showing My Queue"
                TicketRFSList.GetMyQueue(Session("LoginID"), HttpContext.Current.Session("CSCConn").ToString)


                MyQueuelist = TicketRFSList.AllMyQueue

                GetMyQueue()

            Case "groupqueue"

                pnlDate.Visible = False

                txtStartDate.Enabled = True
                txtEndDate.Enabled = True


                HDRadioBt.Text = "groupqueue"
                Session("ShowAll") = ""

                lblCurrentView.Text = "Now Showing My Groups"

                TicketRFSList.GetAllMyGroupQueue(Session("LoginID"), HttpContext.Current.Session("CSCConn").ToString)

                MyQueuelist = TicketRFSList.AllMyQueue

                Dim checkBoxes = From it As ListItem In cblGroupAccountQueues.Items
                    Join own As Integer In GroupOwnership On it.Value Equals own.ToString
                    Select it
                For Each itm In checkBoxes
                    itm.Selected = True
                Next

                GetQueueDataSet()


            Case "unassigned"
                Session("ShowAll") = ""

                pnlDate.Visible = False


                HDRadioBt.Text = "unassigned"
                Session("ShowAll") = ""

                'btnShowAll.Text = "My Queue"
                lblCurrentView.Text = "Now Showing Unassigned Queue"

                TicketRFSList.GetUnAssignQueue(Session("LoginID"), HttpContext.Current.Session("CSCConn").ToString)

                Unassignedlist = TicketRFSList.AllMyQueue

                GetUnassignedQueue()
            Case "openp1"
                Session("ShowAll") = ""

                pnlDate.Visible = False


                HDRadioBt.Text = "unassigned"
                Session("ShowAll") = ""

                'btnShowAll.Text = "My Queue"
                lblCurrentView.Text = "Now Showing P1 Tickets"

                TicketRFSList.GetP1Queue("open", HttpContext.Current.Session("CSCConn").ToString)

                Openp1List = TicketRFSList.AllMyQueue

                GetUnassignedQueue()

            Case "notech"
                Session("ShowAll") = ""

                pnlDate.Visible = False

                txtStartDate.Enabled = True
                txtEndDate.Enabled = True


                HDRadioBt.Text = "notech"

                lblCurrentView.Text = "Now Showing Assigned Groups NO Techs"

                TicketRFSList.GetNoTechQueue(Session("LoginID"), HttpContext.Current.Session("CSCConn").ToString)

                NoTechGroupList = TicketRFSList.NoTechGroup


                For Each cbGroupQueue As ListItem In cblGroupAccountQueues.Items
                    If cbGroupQueue.Selected = False Then
                        cbGroupQueue.Selected = True
                        Groups = Groups & "," & CType(cbGroupQueue.Value, String)
                        hdGroups.Text = hdGroups.Text & "," & CType(cbGroupQueue.Value, String)
                    End If
                Next

                Dim checkBoxes = From it As ListItem In cblGroupAccountQueues.Items
                    Join own As Integer In GroupOwnership On it.Value Equals own.ToString
                    Select it
                For Each itm In checkBoxes
                    itm.Selected = True
                Next


                GetNoTechGroupData()


            Case Else
                ' if a Doctor master number exist

        End Select


        'Response.Redirect("~/" & "MyQueue.aspx")
    End Sub
    Protected Sub QueueLoadData()
        If Session("cscqueue") <> "" Then

            scscqueue = Session("cscqueue")


            Select Case scscqueue
                Case "myqueue"
                    rblShowGroup.SelectedValue = "myqueue"

                Case "groupqueue"
                    rblShowGroup.SelectedValue = "groupqueue"

                Case "unassigned"
                    rblShowGroup.SelectedValue = "unassigned"

                Case "notech"
                    rblShowGroup.SelectedValue = "notech"
            End Select
        Else
            Session("cscqueue") = "myqueue"
            rblShowGroup.SelectedValue = "myqueue"


        End If

        Tablerows = tblGroupAccountQueues.Rows.Count()

        If Tablerows < 1 Then
            lblCurrentView.Text = ""
        End If
        If Session("ShowAll") <> "" Then

            rblShowGroup.SelectedIndex = -1

            rblStatus.Enabled = True
            rblStatus.SelectedValue = "Open"

            rbltype.Enabled = True
            'rbltype.SelectedValue = "both"

            rblPriority.Enabled = True
            'rblPriority.SelectedValue = "all"


            GetQueueDataSet()


        Else
            rblShowGroup.Enabled = True
            rblShowGroup.SelectedValue = "myqueue"

            For Each cbGroupQueue As ListItem In cblGroupAccountQueues.Items

                If cbGroupQueue.Selected = True Then
                    cbGroupQueue.Selected = False

                End If
            Next
            pnlDate.Visible = False

            txtStartDate.Enabled = False
            'txtStartDate.Text = DateAdd(DateInterval.Day, -31, Date.Today)
            'HDStartDate.Text = DateAdd(DateInterval.Day, -31, Date.Today)

            txtEndDate.Enabled = False
            'txtEndDate.Text = Date.Today
            'HDEnddate.Text = Date.Today

            rblStatus.Enabled = False
            rblStatus.SelectedValue = "Open"
            hdStatus.Text = "open"

            rbltype.Enabled = False
            rbltype.SelectedValue = "both"
            hdType.Text = "both"

            rblPriority.Enabled = False
            rblPriority.SelectedValue = "all"
            hdPriority.Text = "all"

            Select Case HDRadioBt.Text ' or rblShowGroup.SelectedValue ' or HDRadioBt.Text

                Case "myqueue"


                    HDRadioBt.Text = "myqueue"
                    Session("ShowAll") = ""

                    'btnShowAll.Text = "My Queue"
                    lblCurrentView.Text = "Now Showing My Queue"
                    TicketRFSList.GetMyQueue(Session("LoginID"), HttpContext.Current.Session("CSCConn").ToString)


                    MyQueuelist = TicketRFSList.AllMyQueue

                    GetMyQueue()

                Case "groupqueue"

                    pnlDate.Visible = True

                    txtStartDate.Enabled = False
                    txtEndDate.Enabled = False

                    HDRadioBt.Text = "groupqueue"
                    Session("ShowAll") = ""

                    lblCurrentView.Text = "Now Showing My Groups"

                    TicketRFSList.GetAllMyGroupQueue(Session("LoginID"), HttpContext.Current.Session("CSCConn").ToString)

                    MyQueuelist = TicketRFSList.AllMyQueue

                    Dim checkBoxes = From it As ListItem In cblGroupAccountQueues.Items
                        Join own As Integer In GroupOwnership On it.Value Equals own.ToString
                        Select it
                    For Each itm In checkBoxes
                        itm.Selected = True
                    Next

                    GetMyQueue()

                    'For Each cbGroupQueue As ListItem In cblGroupAccountQueues.Items
                    '    If cbGroupQueue.Selected = True Then
                    '        Groups = Groups & "," & CType(cbGroupQueue.Value, String)
                    '        hdGroups.Text = hdGroups.Text & "," & CType(cbGroupQueue.Value, String)
                    '    End If
                    'Next


                Case "unassigned"
                    Session("ShowAll") = ""

                    HDRadioBt.Text = "unassigned"
                    Session("ShowAll") = ""

                    'btnShowAll.Text = "My Queue"
                    lblCurrentView.Text = "Now Showing Unassigned Queue"

                    TicketRFSList.GetUnAssignQueue(Session("LoginID"), HttpContext.Current.Session("CSCConn").ToString)

                    Unassignedlist = TicketRFSList.AllMyQueue

                    GetUnassignedQueue()

                Case "notech"
                    Session("ShowAll") = ""

                    'For Each cbGroupQueue As ListItem In cblGroupAccountQueues.Items
                    '    If cbGroupQueue.Selected = False Then
                    '        cbGroupQueue.Selected = True
                    '        Groups = Groups & "," & CType(cbGroupQueue.Value, String)
                    '        hdGroups.Text = hdGroups.Text & "," & CType(cbGroupQueue.Value, String)
                    '    End If
                    'Next

                    rblStatus.Enabled = False
                    rblStatus.SelectedValue = "Open"
                    hdStatus.Text = "open"

                    rbltype.Enabled = False
                    rbltype.SelectedValue = "both"
                    hdType.Text = "both"

                    rblPriority.Enabled = False
                    rblPriority.SelectedValue = "all"
                    hdPriority.Text = "all"

                    HDRadioBt.Text = "notech"

                    lblCurrentView.Text = "Now Showing Assigned Groups NO Techs"

                    TicketRFSList.GetNoTechQueue(Session("LoginID"), HttpContext.Current.Session("CSCConn").ToString)

                    NoTechGroupList = TicketRFSList.NoTechGroup


                    'Dim checkBoxes = From it As ListItem In cblGroupAccountQueues.Items
                    '    Join own As Integer In GroupOwnership On it.Value Equals own.ToString
                    '    Select it
                    'For Each itm In checkBoxes
                    '    itm.Selected = True
                    'Next
                    btnShowAll.Enabled = False
                    cblGroupAccountQueues.Enabled = False

                    GetNoTechGroupData()


                Case Else


                    HDRadioBt.Text = "myqueue"
                    Session("ShowAll") = ""

                    'btnShowAll.Text = "My Queue"
                    lblCurrentView.Text = "Now Showing My Queue"
                    TicketRFSList.GetMyQueue(Session("LoginID"), HttpContext.Current.Session("CSCConn").ToString)


                    MyQueuelist = TicketRFSList.AllMyQueue

                    GetMyQueue()
            End Select

        End If
    End Sub
    Protected Sub TimerControl2_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles TimerControl2.Tick
        QueueLoadData()

        rblShowGroup.SelectedValue = HDRadioBt.Text

        LblLastRefresh.Text = "Last Update: " & DateTime.Now.ToString()

        'Always set it back to MY Queue
        'Session("ShowAll") = "MyQueue"
        'rblShowGroup.SelectedValue = "myqueue"

        'Dim checkBoxes = From it As ListItem In cblGroupAccountQueues.Items
        'For Each itm In checkBoxes
        '    itm.Selected = False

        'Next

        'lblCurrentView.Text = ""

        'btnShowAll.Text = "Check All"
        'lblCurrentView.Text = "Now Showing My Queue"


        'TicketRFSList.GetMyQueue(Session("LoginID"), HttpContext.Current.Session("CSCConn").ToString)

        'MyQueuelist = TicketRFSList.AllMyQueue

        'GetMyQueue()



    End Sub

    Protected Sub rblStatus_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles rblStatus.SelectedIndexChanged


        'hdStatus.Text = rblStatus.SelectedValue
        'Select Case hdStatus.Text
        '    Case "both"
        '        lbTotal.Text = "Click Submit"
        '        pnlDate.Visible = False


        '    Case "Open"
        '        pnlDate.Visible = True

        '        txtStartDate.Enabled = True
        '        txtEndDate.Enabled = True

        '        lbTotal.Text = "Select a Date Range and Submit"

        '    Case "Closed"
        '        pnlDate.Visible = True

        '        txtStartDate.Enabled = True
        '        txtEndDate.Enabled = True

        '        lbTotal.Text = "Select a Date Range and Submit"



        'End Select

    End Sub
    Sub CreateExcel2(ByVal ReportTable As DataTable)

        Response.AddHeader("content-disposition", "attachment;filename=FileName.xls")

        Response.Charset = String.Empty
        Response.ContentType = "application/vnd.ms-excel"

        Dim sw As System.IO.StringWriter = New System.IO.StringWriter()

        Dim hw As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(sw)
        Dim dg As New DataGrid



        ' must be name of datasource on page
        dg.DataSource = ReportTable
        Dim i As Integer = dg.Items.Count
        dg.DataBind()

        dg.HeaderStyle.Font.Bold = True
        dg.HeaderStyle.BackColor = Drawing.Color.LightGray


        dg.RenderControl(hw)

        Response.Write(sw.ToString())
        Response.End()

        'sw = Nothing
        'hw = Nothing
        'dg.Dispose()


        'GridAccounts.Visible = False

    End Sub

    Protected Sub rbltype_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles rbltype.SelectedIndexChanged
        hdType.Text = rbltype.SelectedValue
    End Sub

    Protected Sub rblPriority_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles rblPriority.SelectedIndexChanged
        hdPriority.Text = rblPriority.SelectedValue
    End Sub

    Protected Sub txtStartDate_TextChanged(sender As Object, e As System.EventArgs) Handles txtStartDate.TextChanged
        HDStartDate.Text = txtStartDate.Text
    End Sub

    Protected Sub txtEndDate_TextChanged(sender As Object, e As System.EventArgs) Handles txtEndDate.TextChanged
        HDEnddate.Text = txtEndDate.Text
    End Sub

    Protected Sub btnTicketDash_Click(sender As Object, e As System.EventArgs) Handles btnTicketDash.Click
        Response.Redirect("./TicketDashboard.aspx", True)
    End Sub
End Class
