﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="false" CodeFile="ProjectHoursMaintenance.aspx.vb" Inherits="ProjectHoursMaintenance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
	<script type="text/javascript" src="Scripts/FieldValidator.js"></script>
	<script type="text/javascript">
		function pageLoad(sender, args) {

			fieldValidator('textInputRequired', 'btnSubmit', 'lblValidation');

		}
	</script>

	<style type="text/css">
		.style2 {
			text-align: center;
			background-color: #FFFFCC;
			color: #336666;
			font-weight: bold;
			font-size: x-large;
			height: 43px;
		}

		.auto-style1 {
			height: 29px;
		}
	</style>
	<script type="text/javascript">


		$(function () {

			$(':text').bind('keydown', function (e) { //on keydown for all textboxes  

				if (e.keyCode == 13) //if this is enter key  

					e.preventDefault();

			});

		});



	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="100"  >
       <ProgressTemplate>
            <div id="IMGDIV" align="center" valign="middle" runat="server" style="position:
                    absolute;left: 200px;top:150px;visibility:visible;vertical-align:middle;border-style:
                    inset;border-color:black;background-color:White;width:210px;height:75px; z-index: 999;">
            Loading Data ...
                <img src="Images/AjaxProgress.gif" /><br />
            </div>
       </ProgressTemplate>
    </asp:UpdateProgress>

	<table width="100%">
		<tr>
			<td align="center">
				<asp:Button ID="BtnExcel" runat="server" Text="Excel"
					CssClass="btnhov" BackColor="#006666" Width="120px" />
				<asp:UpdatePanel ID="uplPanel" runat="server" UpdateMode="Conditional">
					<ContentTemplate>
						<div id="maindiv" runat="server" align="left">
							<table style="border-style: groove" align="left" width="100%">
								<tr>

									<td align="center">ProJect Hours
									</td>
								</tr>
								<tr>
									<td align="center">Employee Info
									</td>


								</tr>
								<tr>
									<td align="center">
										<table width="80%" cellpadding="5px">
											<tr>
												<td align="right">Name:
												</td>
												<td align="left">
													<asp:Label ID="empName" runat="server" Font-Bold="true" ForeColor="Black"></asp:Label>
												</td>
												<td align="right">Employee Number:
												</td>
												<td align="left">
													<asp:Label ID="siemensempnumLabel" runat="server" Font-Bold="true" ForeColor="Black"></asp:Label>
												</td>

											</tr>

											<tr>
												<td align="right">Entity - Department:

												</td>
												<td align="left">
													<asp:Label ID="DepartmentName" runat="server" Font-Bold="true" ForeColor="Black"></asp:Label>
												</td>
												<td align="right">Entity Department CD:
												</td>
												<td align="left">
													<asp:Label ID="EntityCd" runat="server" Font-Bold="true" ForeColor="Black"></asp:Label>
												</td>

											</tr>

										</table>


									</td>

								</tr>
								<tr>
									<td>

										<ajaxToolkit:TabContainer ID="tabs" runat="server" ActiveTabIndex="0"
											Width="100%" EnableTheming="False" AutoPostBack="true"
											BackColor="#efefef">

											<ajaxToolkit:TabPanel ID="tpProjectlist" runat="server" TabIndex="0" BackColor="Gainsboro" Visible="True" EnableTheming="True">
												<HeaderTemplate>
													<asp:Label ID="Label4" runat="server" Text="Capital Projects" Font-Bold="true" ForeColor="Black"></asp:Label>
												</HeaderTemplate>
												<ContentTemplate>

													<table>

														<tr align="center">
															<td align="right" class="auto-style1">Select Project </td>

															<td align="left" class="auto-style1">
																<asp:DropDownList ID="Projectsddl" runat="server" AutoPostBack="true" Width="200px">
																</asp:DropDownList>
																<asp:Label ID="ProjectNumLabel" runat="server" Visible="false"></asp:Label>

															</td>

															<td align="right" class="auto-style1"></td>
															<td align="left" class="auto-style1">
																<asp:DropDownList ID="MonthNameddl" runat="server" Width="200px" Visible="false">
																</asp:DropDownList>
																<asp:Label ID="MonthNumlabel" runat="server" Visible="false"></asp:Label>
															</td>

														</tr>
														<tr>
															<td></td>

														</tr>

														<tr>
															<td align="center" colspan="4">

																<asp:GridView ID="gvrojectHrs" runat="server" AllowSorting="True" AutoGenerateColumns="False"
																	BackColor="LightGray" BorderColor="#336666"
																	BorderStyle="Double" BorderWidth="3px" CellPadding="5"
																	EmptyDataText="No Tickets/RFS Found" EnableTheming="False" 
                                                                    DataKeyNames="ProjectNum,WorkDateNum,Week1HoursWorked,Week2HoursWorked,
														Week3HoursWorked,Week4HoursWorked,Week5HoursWorked,WorkDate"
																	Font-Size="small" GridLines="Horizontal" Width="100%" AlternatingRowStyle-BackColor="Bisque" Visible="false">

																	<FooterStyle BackColor="White" ForeColor="#333333" />
																	<HeaderStyle BackColor="#336666" Font-Bold="False" ForeColor="White" Font-Size="Small" HorizontalAlign="Left" />
																	<PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Left" Font-Size="Medium" />
																	<RowStyle BackColor="LightGray" ForeColor="#333333" />
																	<SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
																	<SortedAscendingCellStyle BackColor="#F7F7F7" />
																	<SortedAscendingHeaderStyle BackColor="#487575" />
																	<SortedDescendingCellStyle BackColor="#E5E5E5" />
																	<SortedDescendingHeaderStyle BackColor="#275353" />
																	<Columns>
																				<asp:TemplateField HeaderText="Request" SortExpression="Request" ItemStyle-HorizontalAlign="Center">
<%--										<ItemTemplate>

											<asp:HyperLink ID="HyperLink1" runat="server"
												NavigateUrl='<% #Eval("Request", "~/ViewRequestTicket.aspx?RequestNum={0}") %>'
												Text='<% #Eval("Request") %>'>


											</asp:HyperLink>

										</ItemTemplate>--%>


									</asp:TemplateField>

																		<asp:CommandField ButtonType="Button" SelectText="Select" ShowSelectButton="true" ControlStyle-BackColor="#84A3A3" ItemStyle-Height="30px" />


																		<asp:BoundField DataField="WorkDate" HeaderText="Date" SortExpression="WorkDate" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" ItemStyle-Height="35px"></asp:BoundField>

																		<asp:BoundField DataField="Week1HoursWorked" HeaderText="Week 1" SortExpression="Week1HoursWorked" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"></asp:BoundField>
																		<asp:BoundField DataField="Week2HoursWorked" HeaderText="Week 2" SortExpression="Week2HoursWorked" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"></asp:BoundField>

																		<asp:BoundField DataField="Week3HoursWorked" HeaderText="Week 3" SortExpression="Week3HoursWorked" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"></asp:BoundField>
																		<asp:BoundField DataField="Week4HoursWorked" HeaderText="Week 4" SortExpression="Week4HoursWorked" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"></asp:BoundField>

																		<asp:BoundField DataField="Week5HoursWorked" HeaderText="Week 5" SortExpression="Week5HoursWorked" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"></asp:BoundField>
																		<asp:BoundField DataField="ProjectNum" HeaderText="" Visible="false"></asp:BoundField>
																		<asp:BoundField DataField="WorkDateNum" HeaderText="" Visible="false"></asp:BoundField>
																	</Columns>
																</asp:GridView>



															</td>

														</tr>

													</table>
												</ContentTemplate>
											</ajaxToolkit:TabPanel>


											<ajaxToolkit:TabPanel ID="HrsEntry" runat="server" TabIndex="1" BackColor="Gainsboro" Visible="False" EnableTheming="True">
												<HeaderTemplate>
													<asp:Label ID="Label1" runat="server" Text="Enter Project Hours" Font-Bold="true" ForeColor="Black">

													</asp:Label>
												</HeaderTemplate>

												<ContentTemplate>

													<table width="60%">
														<tr align="center">
															<td align="center">ProJect Name
													<asp:Label ID="Currprojectnamelbl" runat="server" Font-Bold="true" ForeColor="Black"></asp:Label>
															</td>
															<td align="center">Date for Entry
													<asp:Label ID="CurrDatelbl" runat="server" Font-Bold="true" ForeColor="Black"></asp:Label>
															</td>
														</tr>
														<tr align="center">
															<td align="center" colspan="2">
																<table>

																	<tr align="center">
																		<td align="center">Week 1
																		</td>
																		<td align="center">Week 2
																		</td>
																		<td align="center">Week 3
																		</td>
																		<td align="center">Week 4
																		</td>
																		<td align="center">Week 5
																		</td>

																	</tr>

																	<tr align="center">
																		<td align="center">
																			<asp:DropDownList ID="Week1HoursWorkedddl" runat="server" Width="75px"></asp:DropDownList>
																		</td>
																		<td align="center">
																			<asp:DropDownList ID="Week2HoursWorkedddl" runat="server" Width="75px"></asp:DropDownList>
																		</td>
																		<td align="center">
																			<asp:DropDownList ID="Week3HoursWorkedddl" runat="server" Width="75px"></asp:DropDownList>
																		</td>
																		<td align="center">
																			<asp:DropDownList ID="Week4HoursWorkedddl" runat="server" Width="75px"></asp:DropDownList>
																		</td>
																		<td align="center">
																			<asp:DropDownList ID="Week5HoursWorkedddl" runat="server" Width="75px"></asp:DropDownList>
																		</td>

																	</tr>
																</table>

															</td>
														</tr>
														<tr align="center">
															<td align="center">
																<asp:Button ID="btnSubmithours" runat="server" Text="Submit"
																	CssClass="btnhov" BackColor="#006666" Width="200px" />

															</td>
															<td align="center">

																<asp:Button ID="btnCancel" runat="server" Text="Cancel no Entry"
																	CssClass="btnhov" BackColor="#006666" Width="200px" />

															</td>

														</tr>

													</table>

												</ContentTemplate>


											</ajaxToolkit:TabPanel>

											<ajaxToolkit:TabPanel ID="MgrTab" runat="server" TabIndex="2" BackColor="Gainsboro" EnableTheming="True">
												<HeaderTemplate>
													<asp:Label ID="Label2" runat="server" Text=" Project Hours For Users " Font-Bold="true" ForeColor="Black">

													</asp:Label>
												</HeaderTemplate>

												<ContentTemplate>

													<table width="90%" align="center">
														<tr align="center">
															<td colspan="4" align="center">
																<table>

																	<tr align="center">
																		<td align="left">Select View By </td>

																		<td align="left">Select Project </td>


																		<%--																		<td align="left">
																			<asp:Label ID="selemplbl" runat="server"
																				Text="Select Employee" visible="false" />

																		</td>--%>
																	</tr>

																	<tr>
																		<td align="left">
																			<asp:DropDownList ID="ReportTypeddl" runat="server">
																			</asp:DropDownList>

																		</td>

																		<td align="left">
																			<asp:DropDownList ID="Projectlist2ddl" runat="server" Width="200px">
																			</asp:DropDownList>

																		</td>

																		<%--
																		<td>
																			<asp:DropDownList ID="Employeelistddl" runat="server" Visible="false">
																			</asp:DropDownList>

																		</td>--%>
																	</tr>
																</table>

															</td>
														</tr>



														<tr align="center">
															<td colspan="2">
																<asp:Button ID="btnSubmitRpt" runat="server" Text="Submit"
																	CssClass="btnhov" BackColor="#006666" Width="120px" />

															</td>
															<td colspan="2"></td>

														</tr>
														<tr align="center">
															<td align="center" colspan="4">
																<asp:Label ID="lblValidation" runat="server" ForeColor="Red" Font-Bold="true" />
															</td>
														</tr>

														<tr align="center">
															<td colspan="4" align="center">

																<asp:GridView ID="gvprojectTotals" runat="server" AllowSorting="True"
																	AutoGenerateColumns="False" BackColor="White" BorderColor="#336666"
																	BorderStyle="Double" BorderWidth="3px" CellPadding="0"
																	EmptyDataText="No Items Found"
																	EnableTheming="False" Font-Size="Medium" GridLines="Horizontal" Width="100%">

																	<FooterStyle BackColor="White" ForeColor="#333333" />
																	<HeaderStyle BackColor="#336666" Font-Bold="False" Font-Size="Small"
																		ForeColor="White" HorizontalAlign="Left" />
																	<PagerStyle BackColor="#336666" Font-Size="Medium" ForeColor="White"
																		HorizontalAlign="Left" />

																	<RowStyle BackColor="White" ForeColor="#333333" />
																	<SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
																	<SortedAscendingCellStyle BackColor="#F7F7F7" />
																	<SortedAscendingHeaderStyle BackColor="#487575" />
																	<SortedDescendingCellStyle BackColor="#E5E5E5" />
																	<SortedDescendingHeaderStyle BackColor="#275353" />
																	<Columns>

																		<asp:BoundField DataField="ProjectName" HeaderStyle-HorizontalAlign="Left"
																			SortExpression="ProjectName" HeaderText="Project Name" ItemStyle-HorizontalAlign="Left" />
																		<asp:BoundField DataField="WorkDate" HeaderStyle-HorizontalAlign="Left"
																			SortExpression="WorkDate" HeaderText="Date" ItemStyle-HorizontalAlign="Left" />
																		<asp:BoundField DataField="TotalHours" HeaderStyle-HorizontalAlign="Left"
																			SortExpression="TotalHours" HeaderText="Total Hours" ItemStyle-HorizontalAlign="Left" />

																	</Columns>
																</asp:GridView>


																<asp:GridView ID="gvEmployeeProject" runat="server" AllowSorting="True" Visible="false"
																	AutoGenerateColumns="False" BackColor="White" BorderColor="#336666"
																	BorderStyle="Double" BorderWidth="3px" CellPadding="0"
																	EmptyDataText="No Items Found" DataKeyNames="ClientNum,ProjectNum"
																	EnableTheming="False" Font-Size="small" GridLines="Horizontal" Width="100%">

																	<FooterStyle BackColor="White" ForeColor="#333333" />
																	<HeaderStyle BackColor="#336666" Font-Bold="False" Font-Size="Small"
																		ForeColor="White" HorizontalAlign="Left" />
																	<PagerStyle BackColor="#336666" Font-Size="Medium" ForeColor="White"
																		HorizontalAlign="Left" />

																	<RowStyle BackColor="White" ForeColor="#333333" />
																	<SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
																	<SortedAscendingCellStyle BackColor="#F7F7F7" />
																	<SortedAscendingHeaderStyle BackColor="#487575" />
																	<SortedDescendingCellStyle BackColor="#E5E5E5" />
																	<SortedDescendingHeaderStyle BackColor="#275353" />
																	<Columns>


<%--																		<asp:TemplateField HeaderText="Emp Detail" SortExpression="FullName" ItemStyle-HorizontalAlign="Center">
																			<ItemTemplate>

																				<asp:HyperLink ID="HyperLink1" runat="server"
																					NavigateUrl='<% #Eval("ClientNum", "~/ProjectDetailTotalView.aspx?ClientNum={0}") %>'
																					Text="Select">

																				</asp:HyperLink>

																			</ItemTemplate>


																		</asp:TemplateField>--%>


																		<asp:CommandField ButtonType="Button" SelectText="Select" ShowSelectButton= "true" 
																			ControlStyle-BackColor="#84A3A3"  ItemStyle-Height="30px"
																			HeaderText="Emp Detail" ItemStyle-Width="80px" />
                                    

																		<asp:BoundField DataField="ProjectName" HeaderStyle-HorizontalAlign="Left"
																			SortExpression="ProjectName" HeaderText="Project Name" ItemStyle-HorizontalAlign="Left" />
																		<asp:BoundField DataField="WorkDate" HeaderStyle-HorizontalAlign="Left"
																			SortExpression="WorkDate" HeaderText="Date" ItemStyle-HorizontalAlign="Left" />
																		<asp:BoundField DataField="TotalHours" HeaderStyle-HorizontalAlign="Left"
																			SortExpression="TotalHours" HeaderText="Total Hours" ItemStyle-HorizontalAlign="Left" />

																		<asp:BoundField DataField="FullName" HeaderStyle-HorizontalAlign="Left"
																			SortExpression="FullName" HeaderText="Emp Name" ItemStyle-HorizontalAlign="Left" />


																		<asp:BoundField DataField="DepartmentName" HeaderStyle-HorizontalAlign="Left"
																			SortExpression="DepartmentName" HeaderText="Dept. Name" ItemStyle-HorizontalAlign="Left" />


																		<asp:BoundField DataField="CostCenter" HeaderStyle-HorizontalAlign="Left"
																			SortExpression="CostCenter" HeaderText="Cost Center" ItemStyle-HorizontalAlign="Left" />

																	</Columns>
																</asp:GridView>
															</td>


														</tr>


													</table>
												</ContentTemplate>
											</ajaxToolkit:TabPanel>

										</ajaxToolkit:TabContainer>
									</td>

								</tr>

								<tr>
									<td colspan="4">

										<asp:Label ID="HDClientnum" runat="server" Visible="false"></asp:Label>
										<asp:Label ID="HDTechnum" runat="server" Visible="false"></asp:Label>
										<asp:Label ID="HDProjectNum" runat="server" Visible="false"></asp:Label>
										<asp:Label ID="HDWorkdatenum" runat="server" Visible="false"></asp:Label>
										<asp:Label ID="HDWeek1HoursWorked" runat="server" Visible="false"></asp:Label>
										<asp:Label ID="HDWeek2HoursWorked" runat="server" Visible="false"></asp:Label>
										<asp:Label ID="HDWeek3HoursWorked" runat="server" Visible="false"></asp:Label>
										<asp:Label ID="HDWeek4HoursWorked" runat="server" Visible="false"></asp:Label>
										<asp:Label ID="HDWeek5HoursWorked" runat="server" Visible="false"></asp:Label>

										<asp:Label ID="HDTypeReport" runat="server" Visible="false"></asp:Label>
										<asp:Label ID="HDRptClientnum" runat="server" Visible="false"></asp:Label>
										<asp:Label ID="HDRptprojectnum" runat="server" Visible="false"></asp:Label>

									</td>

								</tr>
							</table>

						</div>
					</ContentTemplate>
				</asp:UpdatePanel>

			</td>

		</tr>

	</table>

</asp:Content>

