﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="false" CodeFile="CSCOwnerMaintenance.aspx.vb" Inherits="CSCOwnerMaintenance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

<asp:UpdatePanel ID="uplAppMaintenance" runat ="server" >

  <Triggers>
      <%-- <asp:AsyncPostBackTrigger ControlID="CSCApplicationsddl" EventName ="SelectedIndexChanged" />--%>
      <%--class="wrapper" style="border-style: groove" align="left"--%>
  </Triggers>

    <ContentTemplate>
    <table  width="100%">
     <tr>
      <td>
        <table width="100%" border="3" id="Maintbl">
            <tr>
                <th colspan="4" align ="center" class="tableRowHeader">
        
                     Add/Remove Application Contacts 
                </th>

            </tr>
            <tr>
                <td>
                        Select Application
                </td>
                <td colspan="3">
                    <asp:DropDownList ID = "CSCApplicationsddl" runat = "server" AutoPostBack="true">
                    </asp:DropDownList>
                    
                </td>
        
           </tr>
           <tr>
                <td colspan="4" valign="middle">
                
                Comments: 
                    <asp:TextBox ID="AppCommentsTextBox" runat="server" TextMode="MultiLine" Width="80%" Text=""></asp:TextBox>
                </td>
           
           </tr>
            <tr>
                <td>
                    <asp:Button id="BtnCSCRpt" runat="server" Text="CSC List Rpt." Width="120px" CssClass="btnhov" />
                </td>
                <td  colspan="3" align="center" style="font-size: medium; font-weight: bold">
                Current Application Contact List for: <asp:Label ID="ApplicationCdlbl" runat="server" ></asp:Label>
                </td>
            </tr>
            <tr>
                 <td colspan = "4">
            
                         <asp:GridView ID="gvAppOwners" runat="server" AllowSorting="True" 
                                    AutoGenerateColumns="False" BackColor="White" BorderColor="#336666"
                                    BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                                    EmptyDataText="No Owners" EnableTheming="False" DataKeyNames="clientnum,ApplicationCd,groupNum,vendornum,OwnerTypecd"
                                    Font-Size="Medium" GridLines="Horizontal" Width="100%" Visible="true">
                                   
                                    <FooterStyle BackColor="White" ForeColor="#333333" />
                                    <HeaderStyle BackColor="#336666" Font-Bold="False" ForeColor="White" Font-Size="Small" HorizontalAlign="Left"    />                                    
                                    <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Left" Font-Size="Medium"/>
                                    <RowStyle BackColor="White" ForeColor="#333333" />
                                    <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                    <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                    <SortedAscendingHeaderStyle BackColor="#487575" />
                                    <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                    <SortedDescendingHeaderStyle BackColor="#275353" />

                                    <Columns>
                                    
                                          <asp:CommandField ButtonType="Button" SelectText="Remove" ShowSelectButton= "true" />

                                          <asp:BoundField  DataField="Name"  HeaderText="Viewer/Owner" HeaderStyle-HorizontalAlign ="Left"></asp:BoundField>
                                          
                                          <asp:BoundField DataField="OwnerTypeDesc" HeaderText="Type" />

                                          <asp:BoundField DataField="Phone" HeaderText="Phone" />
                                          <asp:BoundField DataField="email" HeaderText="eMail" />

<%--				                          <asp:TemplateField HeaderText="Receive Email Alert/Add " ItemStyle-HorizontalAlign="Center">
                                            
                                            <ItemTemplate>
                                                 <asp:CheckBox ID="ckbAcctEmail" runat="server" AutoPostBack="true" 
                                                               OnCheckedChanged="ckbAcctEmail_OnCheckedChanged"
                                                               Checked= '<%# Convert.ToBoolean(Eval("ReceiveNewAcctEmail")) %>'
                                                                 />
                                            </ItemTemplate>
                                          
                                          </asp:TemplateField>--%>
                                          




                                  
                                  </Columns>

                                </asp:GridView>
		
                </td>
            </tr>
           <tr>
            <td colspan="4">
                <asp:Panel ID="pancontactNames"  runat="server" Visible="false">
                
                    <table width="100%">
                        <tr>
                            <td align="center">
                                <asp:Label id="lblClientContact" runat="server" Text = "Client Contact"></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lbltechContact" runat="server" Text = "Tech Contact"></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblVendor" runat="server" Text="Vendor Contact"></asp:Label>
                            </td>
                        
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Label ID="Clientnamelbl" runat="server" ></asp:Label>
                                <asp:Label ID="ClientNumlbl" runat="server" Visible="false" ></asp:Label>

                            </td>
                            
                            <td align="center">
                                <asp:Label ID="TechNamelbl" runat="server" ></asp:Label>
                                <asp:Label ID="TechNumlbl" runat="server" Visible="false" ></asp:Label>
                                <asp:Label ID="ClientTechNum" runat="server" Visible="false" ></asp:Label>

                            
                            </td>
                            <td align="center">
                                <asp:Label ID="VendorNamelbl" runat="server" ></asp:Label>
                                <asp:Label ID="VendorNumlbl" runat="server" Visible="false" ></asp:Label>
                            
                            </td>
                        
                        </tr>
                        <tr>
                            <td colspan="3" align="center">
                                <asp:Button ID="btnSubmitContact" runat="server" text ="Submit/Update" Width="150px" CssClass="btnhov" />
                                <asp:Button ID="btnReset" runat="server" Text="Reset" Width="150px" CssClass="btnhov" />
                            </td>
                        </tr>
                    </table>
                
                </asp:Panel>
             </td>
            </tr>
           <tr>
             <td align="left" colspan="4">
               <ajaxToolkit:TabContainer ID="tabs" runat="server" ActiveTabIndex="0"
                Width="100%"  EnableTheming="False"  Visible="false" AutoPostBack="true"  BackColor="#efefef">

                    <ajaxToolkit:TabPanel ID="tpNewSubmitter" runat="server" BackColor="Gainsboro"  TabIndex="0" Visible = "True" EnableTheming="True" >
                       <HeaderTemplate > 
                          <asp:Label ID="Label3" runat="server" Text="Add New Client Contact" Font-Bold="true" ForeColor="Black" ></asp:Label>
                     </HeaderTemplate>
                      <ContentTemplate>
                       <table border="1" width="100%"  style="empty-cells:show" >
                         <tr>
                              <td colspan="2" align="center">
                               Search for CKHS Employees to Add
          
                              </td>
                        </tr>
                         <tr>
                            <td colspan="2" align="center">
                                <table>
                                    <tr>
                                         <td align="right">
                                            Last Name
                                        </td>
                                    
                                        <td>
                                         <asp:TextBox ID="ADDLastNameTextBox" Width="200px" runat = "server"></asp:TextBox>
                                        </td>
                                    
                                    
                                    </tr>
                                </table>
                            </td>

                      </tr>
                         <tr>
                          <td  align="center" colspan="4">

                                 <asp:Button ID ="BtnAddSearch" runat = "server" Text ="Search"  Width="120px" CssClass="btnhov" />
                          </td>
                        </tr>          
                         <tr>
                           <td colspan="4">
                            <asp:GridView ID="GvADDClient" runat="server" AllowSorting="True" 
                                   AutoGenerateColumns="False" BackColor="White" BorderColor="#336666" 
                                   BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                                   DataKeyNames="client_num,fullname" EmptyDataText="No Techs" 
                                   EnableTheming="False" Font-Size="Medium" GridLines="Horizontal" Width="100%">
                                   <FooterStyle BackColor="White" ForeColor="#333333" />
                                   <HeaderStyle BackColor="#336666" Font-Bold="False" Font-Size="Small" 
                                       ForeColor="White" HorizontalAlign="Left" />
                                   <PagerStyle BackColor="#336666" Font-Size="Medium" ForeColor="White" 
                                       HorizontalAlign="Left" />
                                   <RowStyle BackColor="White" ForeColor="#333333" />
                                   <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                   <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                   <SortedAscendingHeaderStyle BackColor="#487575" />
                                   <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                   <SortedDescendingHeaderStyle BackColor="#275353" />
                                   <Columns>

                                       <asp:CommandField ButtonType="Button" ControlStyle-BackColor="#84A3A3" 
                                              HeaderStyle-HorizontalAlign="Left" ItemStyle-Height="30px" SelectText="Select" 
                                              ShowSelectButton="true">
                                          <ControlStyle BackColor="#84A3A3" />
                                          <HeaderStyle HorizontalAlign="Left" />
                                          <ItemStyle Height="30px" />
                                          </asp:CommandField>
                            

                                       <asp:BoundField DataField="fullname" HeaderText="Name" />
                                       <asp:BoundField DataField="phone" HeaderText="Phone" />

                                       <asp:BoundField DataField="siemens_emp_num" HeaderText="Emp#" />
                                       

                                       <asp:BoundField DataField="department_name" HeaderText="Department " />

                                       <asp:BoundField DataField="login_name" HeaderText="Account " />

                                   </Columns>
                               </asp:GridView>


                           </td>
                         </tr>
                        </table>
                       </ContentTemplate>
                   </ajaxToolkit:TabPanel>

                    <ajaxToolkit:TabPanel ID="tpITContact" runat="server" BackColor="Gainsboro"  TabIndex="1" Visible = "True" EnableTheming="True" >
                       <HeaderTemplate > 
                          <asp:Label ID="Label1" runat="server" Text="Add New IT Contacts " Font-Bold="true" ForeColor="Black" ></asp:Label>
                     </HeaderTemplate>
                      <ContentTemplate>

                       <table border="1" width="100%"  style="empty-cells:show" >
                         <tr>
                              <td colspan="4" align="center">
                                IT Contact
          
                              </td>
                        </tr>
         
                         <tr>
                           <td colspan="4">
                                <asp:GridView ID="gvAccounts" runat="server" AllowSorting="True" 
                               AutoGenerateColumns="False" BackColor="White" BorderColor="#336666" 
                               BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                               DataKeyNames="client_num,tech_num,tech_name" EmptyDataText="No Techs" 
                               EnableTheming="False" Font-Size="Medium" GridLines="Horizontal" Width="100%">
                               <FooterStyle BackColor="White" ForeColor="#333333" />
                               <HeaderStyle BackColor="#336666" Font-Bold="False" Font-Size="Small" 
                                   ForeColor="White" HorizontalAlign="Left" />
                               <PagerStyle BackColor="#336666" Font-Size="Medium" ForeColor="White" 
                                   HorizontalAlign="Left" />
                               <RowStyle BackColor="White" ForeColor="#333333" />
                               <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                               <SortedAscendingCellStyle BackColor="#F7F7F7" />
                               <SortedAscendingHeaderStyle BackColor="#487575" />
                               <SortedDescendingCellStyle BackColor="#E5E5E5" />
                               <SortedDescendingHeaderStyle BackColor="#275353" />
                               <Columns>

                                   <asp:CommandField ButtonType="Button" ControlStyle-BackColor="#84A3A3" 
                                          HeaderStyle-HorizontalAlign="Left" ItemStyle-Height="30px" SelectText="Select" 
                                          ShowSelectButton="true">
                                      <ControlStyle BackColor="#84A3A3" />
                                      <HeaderStyle HorizontalAlign="Left" />
                                      <ItemStyle Height="30px" />
                                      </asp:CommandField>
                            

                                   <asp:BoundField DataField="tech_name" HeaderText="Name" />

                                   <asp:BoundField DataField="e_mail" HeaderText="e_mail" />

                                   <asp:BoundField DataField="ntlogin" HeaderText="Account " />

                                   <asp:BoundField DataField="phone" HeaderText="Phone " />

                               </Columns>
                           </asp:GridView>
                           </td>
                         </tr>
                        </table>

                   </ContentTemplate>
                 </ajaxToolkit:TabPanel>

                    <ajaxToolkit:TabPanel ID="tpVendor" runat="server" BackColor="Gainsboro" TabIndex="2" Visible = "True" EnableTheming="True" >
                       <HeaderTemplate > 
                          <asp:Label ID="Label2" runat="server" Text="Add New Vendor Contacts " Font-Bold="true" ForeColor="Black" ></asp:Label>
                     </HeaderTemplate>
                      <ContentTemplate>
                       <table border="1" width="100%"  style="empty-cells:show" >
                          <tr>
                              <td  align="center">
                                Select Vendor
          
                              </td>
                          </tr>

                         <tr>
                           <td >
                            <asp:GridView ID="GvVendors" runat="server" AllowSorting="True" 
                               AutoGenerateColumns="False" BackColor="White" BorderColor="#336666" 
                               BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                               DataKeyNames="VendorNum,VendorName" EmptyDataText="No Vendors" 
                               EnableTheming="False" Font-Size="Medium" GridLines="Horizontal" Width="100%">
                               <FooterStyle BackColor="White" ForeColor="#333333" />
                               <HeaderStyle BackColor="#336666" Font-Bold="False" Font-Size="X-Small" 
                                   ForeColor="White" HorizontalAlign="Left" />
                               <PagerStyle BackColor="#336666" Font-Size="Medium" ForeColor="White" 
                                   HorizontalAlign="Left" />
                               <RowStyle BackColor="White" ForeColor="#333333" Font-Size="X-Small" />
                               <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                               <SortedAscendingCellStyle BackColor="#F7F7F7" />
                               <SortedAscendingHeaderStyle BackColor="#487575" />
                               <SortedDescendingCellStyle BackColor="#E5E5E5" />
                               <SortedDescendingHeaderStyle BackColor="#275353" />
                                   <Columns>

                                       <asp:CommandField ButtonType="Button" ControlStyle-BackColor="#84A3A3" 
                                              HeaderStyle-HorizontalAlign="Left" ItemStyle-Height="20px" SelectText="Select" 
                                              ShowSelectButton="true">
                                          <ControlStyle BackColor="#84A3A3" />
                                          <HeaderStyle HorizontalAlign="Left" />
                                          <ItemStyle Height="20px" />
                                          </asp:CommandField>
                            
                            	  


                                       <asp:BoundField DataField="VendorName" HeaderText="Name" />

                                       <asp:BoundField DataField="VendorType" HeaderText="Type" />

                                       <asp:BoundField DataField="City" HeaderText="City" />

                                       <asp:BoundField DataField="State" HeaderText="State " />

                                       <asp:BoundField DataField="Phone" HeaderText="Phone"/>

                                       <asp:BoundField DataField="zip" HeaderText="Zip"/>

                                   </Columns>
                               </asp:GridView>                            
                           </td>
                         </tr>

                       </table>
                      </ContentTemplate>
                    </ajaxToolkit:TabPanel>

               </ajaxToolkit:TabContainer>
             </td>
          </tr>


            <tr>
                <td colspan = "1">
                </td>
                <td colspan = "2">
                  <asp:Button ID = "btnAddContact" runat="server" Text= "Add Owner" Visible="false" Width="120px" CssClass="btnhov" />

                </td>
                <td>
                
                </td>
            </tr>

  
           </table>

      </td>
     </tr>
    </table>
    </ContentTemplate>
  </asp:UpdatePanel>
</asp:Content>

