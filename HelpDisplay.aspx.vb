Imports System.IO
Imports System.Object
Imports System.Diagnostics
Imports System.ComponentModel
Imports App_Code

Partial Class HelpFiles_HelpDisplay
    Inherits System.Web.UI.Page
    Dim sUrl As String = ""
    Dim sHelpPage As String = ""
    Dim sSubPath As String
    Dim FielName As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Request.QueryString("FileName") <> Nothing Then
            FielName = Request.QueryString("FileName")
            tblHelpTopics.Visible = False
            SaveCopy(FielName)
            UpdatePanel1.Visible = True
        Else
            fillHelpItemsTable()
            tblHelpTopics.Visible = True
            UpdatePanel1.Visible = False


        End If

    End Sub
    'Protected Sub btndisplay_Click(sender As Object, e As System.EventArgs) Handles btndisplay.Click
    '    Dim sPhysicalPagePath As String = "" 'sUrl
    '    ' Dim sActualPage As String = getActualPage() 'is it htm, aspx, mht, etc?
    '    Dim sActualPage As String = "CSCTraining.MHT"
    '    sSubPath = "\helpDocs"
    '    sPhysicalPagePath = Me.MapPath("") & sSubPath & "\" & sActualPage
    '    sUrl = "helpDocs" & "/" & sActualPage
    '    Dim fInfo As New FileInfo(sPhysicalPagePath)

    '    If Not IsNothing(sUrl) Then

    '        If sUrl.ToLower.IndexOf(".aspx") > -1 Or sUrl.ToLower.IndexOf(".htm") > -1 _
    '        Or sUrl.ToLower.IndexOf(".mht") > -1 Then 'asp page

    '        ElseIf sUrl.ToLower.IndexOf(".pdf") > -1 Then 'pdf or powerpoint
    '            Response.ContentType = "application/pdf"

    '        ElseIf sUrl.ToLower.IndexOf(".doc") > -1 Or sUrl.ToLower.IndexOf(".dot") > -1 Or _
    '        sUrl.ToLower.IndexOf(".txt") > -1 Then 'word
    '            Response.ContentType = "application/ms-word"

    '        ElseIf sUrl.ToLower.IndexOf(".xls") > -1 Or sUrl.ToLower.IndexOf(".xlt") > -1 Then 'Excel
    '            Response.ContentType = "application/vnd.microsoft-excel" '"application/ms-excel"

    '        End If
    '    End If

    'End Sub
    Protected Sub fillHelpItemsTable()

        Dim thisData As New CommandsSqlAndOleDb("SelHelpTopics", Session("EmployeeConn"))
        Dim iRowCount = 1

        Dim dt As DataTable
        dt = thisData.GetSqlDataTable

        Dim rwHeader As New TableRow

        Dim clHelpIDHeader As New TableCell
        Dim clHelpDescHeader As New TableCell


        clHelpIDHeader.Text = " "
        clHelpDescHeader.Text = "Help Topics"


        rwHeader.Cells.Add(clHelpIDHeader)
        rwHeader.Cells.Add(clHelpDescHeader)



        rwHeader.Font.Bold = True
        rwHeader.BackColor = Color.FromName("#006666")
        rwHeader.ForeColor = Color.FromName("White")
        rwHeader.Height = Unit.Pixel(36)


        tblHelpTopics.Rows.Add(rwHeader)

        For Each drRow As DataRow In dt.Rows


            Dim rwData As New TableRow
            Dim btnSelect As New Button
            Dim clSelectData As New TableCell

            Dim clHelpDesc As New TableCell



            btnSelect.Text = "Select"
            btnSelect.CommandName = "Select"
            btnSelect.CommandArgument = drRow("FileName")
            btnSelect.CssClass = "btnhov"
            btnSelect.Width = "129"

            AddHandler btnSelect.Click, AddressOf btnSelect_Click

            clHelpDesc.Text = IIf(IsDBNull(drRow("HelpDesc")), "", drRow("HelpDesc"))
            clSelectData.Controls.Add(btnSelect)

            rwData.Cells.Add(clSelectData)
            rwData.Cells.Add(clHelpDesc)


            If iRowCount > 0 Then

                rwData.BackColor = Color.Bisque

            Else

                rwData.BackColor = Color.LightGray

            End If

            tblHelpTopics.Rows.Add(rwData)

            iRowCount = iRowCount * -1

        Next
        tblHelpTopics.Font.Name = "Arial"
        tblHelpTopics.Font.Size = 12

        tblHelpTopics.CellPadding = 10
        tblHelpTopics.Width = New Unit("100%")


    End Sub
    Sub btnSelect_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim btnLinkView As Button = sender

        If btnLinkView.CommandName = "Select" Then
            Dim sArguments As String() = Split(btnLinkView.CommandArgument, ";")

            Response.Redirect("./HelpDisplay.aspx?FileName=" & sArguments(0))

            '            Response.Redirect("./OpenProviderRequestItems.aspx?RequestNum=" & sArguments(0))

        End If
    End Sub

    Protected Sub SaveCopy(ByVal bFillIframe As String)
        Dim sPhysicalPagePath As String = "" 'sUrl

        Dim sActualPage As String = bFillIframe
        sSubPath = "\helpDocs"
        sPhysicalPagePath = Me.MapPath("") & sSubPath & "\" & sActualPage
        sUrl = "helpDocs" & "/" & sActualPage
        Dim fInfo As New FileInfo(sPhysicalPagePath)

        pdfFrame.Attributes("src") = sUrl ' Session("HelpUrl")
        pdfFrame.Attributes("height") = "700"
        pdfFrame.Attributes("width") = "780"

    End Sub

    Protected Sub btnReturn_Click(sender As Object, e As System.EventArgs) Handles btnReturn.Click

        Response.Redirect("./HelpDisplay.aspx")
    End Sub
End Class

