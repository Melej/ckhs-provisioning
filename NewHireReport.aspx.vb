﻿Imports App_Code
Partial Class NewHireReport
    Inherits System.Web.UI.Page
    Dim DTnewHires As DataTable
    Dim UserInfo As UserSecurity
    Dim sSecurityLevel As Integer

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        UserInfo = New UserSecurity(Session("LoginID"), Session("EmployeeConn"))

        sSecurityLevel = UserInfo.SecurityLevelNum

        Select Case Session("SecurityLevelNum")
            Case 72 To 79
                Response.Redirect("~/SimpleSearch.aspx", True)

            Case < 70
                Response.Redirect("~/SimpleSearch.aspx", True)

        End Select



        If Not IsPostBack Then
            txtStartDate.Text = DateTime.Today.AddDays(-5)
            txtEndDate.Text = DateTime.Today

            HDStartDate.Text = txtStartDate.Text
            HDEnddate.Text = txtEndDate.Text

            HDReportType.Text = AllRequestrbl.SelectedValue
            'start_dateTextBox.Text = DateAdd(DateInterval.Day, -14, Date.Today)
            'end_dateTextBox.Text = Date.Today

            fillTable()
        Else
            HDStartDate.Text = txtStartDate.Text
            HDEnddate.Text = txtEndDate.Text

        End If

        HDReportType.Text = AllRequestrbl.SelectedValue



    End Sub
    Protected Sub btnSubmit_Click(sender As Object, e As System.EventArgs) Handles btnSubmit.Click
        fillTable()


    End Sub

    Protected Sub fillTable()

        Dim thisData As New CommandsSqlAndOleDb("SelNewHiresCompleted", Session("EmployeeConn"))

        If txtStartDate.Text <> "" Then
            thisData.AddSqlProcParameter("@startDate", HDStartDate.Text, SqlDbType.NVarChar, 12)
        End If

        If txtEndDate.Text <> "" Then
            thisData.AddSqlProcParameter("@endDate", HDEnddate.Text, SqlDbType.NVarChar, 12)
        End If
        thisData.AddSqlProcParameter("@reportType", HDReportType.Text, SqlDbType.NVarChar, 12)

        Dim iRowCount = 1



        DTnewHires = thisData.GetSqlDataTable


        'AttachedClients = DTnewHires.Rows.Count()

        GvnewHires.DataSource = DTnewHires
        GvnewHires.DataBind()

        HRNewHiresGrid.DataSource = DTnewHires
        HRNewHiresGrid.DataBind()

        HDGrid.DataSource = DTnewHires
        HDGrid.DataBind()

        If sSecurityLevel = 67 Then
            HRNewHiresGrid.Visible = True
            GvnewHires.Visible = False
        End If

        GvnewHires.SelectedIndex = -1
        HRNewHiresGrid.SelectedIndex = -1

    End Sub
    Protected Sub btnExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExcel.Click
        CreateExcel2()
    End Sub
    Sub CreateExcel2()



        Response.AddHeader("content-disposition", "attachment;filename=FileName.xls")

        Response.Charset = String.Empty
        Response.ContentType = "application/vnd.ms-excel"
        '  Response.ContentType = "application/vnd.xls"

        Dim sw As System.IO.StringWriter = New System.IO.StringWriter()

        Dim hw As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(sw)

        '===============================Create form to contain grid
        Dim frm As New HtmlForm()

        HDGrid.Visible = True

        HDGrid.Columns.RemoveAt(0)
        HDGrid.Parent.Controls.Add(frm)
        frm.Attributes("RunAt") = "server"
        frm.Controls.Add(HDGrid)

        frm.RenderControl(hw)
        Response.Write(sw.ToString())


        Response.End()

        HDGrid.Visible = False

    End Sub


    Public Sub LoadTable()

        Dim thisData As New CommandsSqlAndOleDb("SelOpenProviderReportV4", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@startDate", HDStartDate.Text, SqlDbType.NVarChar, 12)
        thisData.AddSqlProcParameter("@endDate", HDEnddate.Text, SqlDbType.NVarChar, 12)
        thisData.AddSqlProcParameter("@requestTypes", HDReportType.Text, SqlDbType.NVarChar, 4)

        Dim iRowCount = 1

        Dim dt As DataTable
        dt = thisData.GetSqlDataTable


        Dim rwHeader As New TableRow
        Dim clSelectHeader As New TableCell
        Dim clLastNameHeader As New TableCell
        Dim clFirstNameHeader As New TableCell
        Dim clStartDateHeader As New TableCell
        Dim clEndDateHeader As New TableCell
        Dim clEmpTypeHeader As New TableCell
        Dim clAccountCountHeader As New TableCell



        clLastNameHeader.Text = "  Last Name  "
        clFirstNameHeader.Text = "  First Name "
        clStartDateHeader.Text = "  Start Date  "
        clEndDateHeader.Text = "  End Date  "

        clEmpTypeHeader.Text = " Affiliation "
        clAccountCountHeader.Text = "  #Accounts  "


        rwHeader.Cells.Add(clSelectHeader)
        rwHeader.Cells.Add(clLastNameHeader)
        rwHeader.Cells.Add(clFirstNameHeader)
        rwHeader.Cells.Add(clStartDateHeader)
        rwHeader.Cells.Add(clEndDateHeader)
        rwHeader.Cells.Add(clEmpTypeHeader)
        rwHeader.Cells.Add(clAccountCountHeader)


        rwHeader.Font.Bold = True
        rwHeader.BackColor = Color.FromName("#006666")
        rwHeader.ForeColor = Color.FromName("White")
        rwHeader.Height = Unit.Pixel(36)

        tblEndDate.Rows.Add(rwHeader)

        For Each drRow As DataRow In dt.Rows

            'Dim btnSelect As New Button
            Dim rwData As New TableRow
            Dim btnSelect As New TableCell
            Dim clLNameData As New TableCell
            Dim clFNameData As New TableCell
            Dim clStartData As New TableCell

            Dim clEndData As New TableCell

            Dim clEmpTypeData As New TableCell
            Dim clAccountCountData As New TableCell


            btnSelect.Text = "<a href=""ClientDemo.aspx?ClientNum=" & drRow("client_num") & """ ><u><b> " & drRow("client_num") & "</b></u></a>"

            btnSelect.EnableTheming = False


            clLNameData.Text = IIf(IsDBNull(drRow("last_name")), "", drRow("last_name"))
            clFNameData.Text = IIf(IsDBNull(drRow("first_name")), "", drRow("first_name"))
            clStartData.Text = IIf(IsDBNull(drRow("start_date")), "", drRow("start_date"))
            clEndData.Text = IIf(IsDBNull(drRow("end_date")), "", drRow("end_date"))
            clEmpTypeData.Text = (IIf(IsDBNull(drRow("emp_type_cd")), "", drRow("emp_type_cd")))
            clAccountCountData.Text = (IIf(IsDBNull(drRow("AccountCount")), "", drRow("AccountCount")))


            'rwData.Cells.Add(clSelectData)
            rwData.Cells.Add(btnSelect)
            rwData.Cells.Add(clLNameData)
            rwData.Cells.Add(clFNameData)
            rwData.Cells.Add(clStartData)
            rwData.Cells.Add(clEndData)
            rwData.Cells.Add(clEmpTypeData)
            rwData.Cells.Add(clAccountCountData)



            If iRowCount > 0 Then

                rwData.BackColor = Color.Bisque

            Else

                rwData.BackColor = Color.LightGray

            End If

            tblEndDate.Rows.Add(rwData)

            iRowCount = iRowCount * -1

        Next
        tblEndDate.Width = New Unit("100%")

    End Sub

    Private Sub AllRequestrbl_SelectedIndexChanged(sender As Object, e As EventArgs) Handles AllRequestrbl.SelectedIndexChanged
        HDReportType.Text = AllRequestrbl.SelectedValue
    End Sub

    Private Sub GvnewHires_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles GvnewHires.RowCommand
        If e.CommandName = "Select" Then

            Dim sAccountRequest As String
            Dim sActive As String

            sAccountRequest = GvnewHires.DataKeys(Convert.ToInt32(e.CommandArgument))("account_request_num").ToString()

            GvnewHires.SelectedIndex = -1

            Response.Redirect("./AccountRequestInfo2.aspx?requestnum=" & sAccountRequest, True)


        End If
    End Sub
End Class
