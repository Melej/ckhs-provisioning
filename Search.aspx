﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="false" CodeFile="Search.aspx.vb" Inherits="Search" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">


<asp:UpdatePanel ID="uplSearch" runat ="server" >

<Triggers>

   <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />

</Triggers>

<ContentTemplate>
<table style="border-style: groove" align="left">
<tr >
<td>

         <table width="100%" border ="3">
    <tr>
        <th align="center" colspan="2" class="tableRowHeader">
        
           CKHS Employee OnLine Account Request System

        </th>

    </tr>
    <tr>
        <td>
            Last Name <asp:TextBox ID = "LastNameTextBox" runat = "server"></asp:TextBox>
        </td>
        <td>
        
        <asp:RadioButtonList ID = "rblLastNameType" runat = "server">
            <asp:ListItem Value = "s" Text = "Starts With" Selected="True"></asp:ListItem>
            <asp:ListItem Value = "e" Text = "Ends With"></asp:ListItem>
            <asp:ListItem Value = "q" Text = "Equal To"></asp:ListItem>
        </asp:RadioButtonList>
        </td>
    </tr>
     <tr>
        <td>
           First Name <asp:TextBox ID = "FirstNameTextBox" runat = "server"></asp:TextBox>
        </td>
        <td>
        
        <asp:RadioButtonList ID = "rblFirstNameType" runat = "server">
            <asp:ListItem Value = "s" Text = "Starts With" Selected="True"></asp:ListItem>
            <asp:ListItem Value = "e" Text = "Ends With"></asp:ListItem>
            <asp:ListItem Value = "q" Text = "Equal To"></asp:ListItem>
        </asp:RadioButtonList>
        </td>
    </tr>
    <tr>
        <td colspan = "2">
                <asp:DropDownList ID = "Departmentddl" runat = "server"></asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td colspan ="2" align ="center">
        
            <asp:Button ID ="btnSearch" runat = "server" Text ="Search" />

        </td>
    </tr>
    <tr>
        <td colspan ="2">
      
            
         <asp:GridView ID="gvSearch" runat="server" AllowSorting="True" 
                                    AutoGenerateColumns="False" BackColor="White" BorderColor="#336666"
                                    BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                                    EmptyDataText="No Items Found" EnableTheming="False" DataKeyNames="client_num"
                                    Font-Size="Medium" GridLines="Horizontal" Width="100%">
                                   
                                    <FooterStyle BackColor="White" ForeColor="#333333" />
                                    <HeaderStyle BackColor="#336666" Font-Bold="False" ForeColor="White" Font-Size="Small" HorizontalAlign="Left"    />                                    
                                    <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Left" Font-Size="Medium"/>
                                    <RowStyle BackColor="White" ForeColor="#333333" />
                                    <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                    <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                    <SortedAscendingHeaderStyle BackColor="#487575" />
                                    <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                    <SortedDescendingHeaderStyle BackColor="#275353" />

                                    <Columns>
                                    
                                          <asp:BoundField  DataField="first_name"  HeaderText="First Name"></asp:BoundField>
                                          <asp:BoundField  DataField="last_name"  HeaderText="Last Name"></asp:BoundField>
                          <%--                <asp:BoundField  DataField="login_name"  HeaderText="Login"></asp:BoundField>--%>
                                          <asp:BoundField  DataField="phone"  HeaderText="Phone"></asp:BoundField>
                                          <asp:BoundField  DataField="department_name"  HeaderText="Department"></asp:BoundField>
				                          <asp:CommandField ButtonType="Button" SelectText="Update" ShowSelectButton= "true" />




                                  
                                  </Columns>

                                </asp:GridView>
		

	
        </td>
    </tr>
   </table>

        
    </td>
</tr>

    
    


</table>

</ContentTemplate>


        </asp:UpdatePanel>


</asp:Content>

