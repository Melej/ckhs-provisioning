﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="false" CodeFile="VendorMaintenance.aspx.vb" Inherits="VendorMaintenance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <script type="text/javascript" src="Scripts/FieldValidator.js"></script>
<script type="text/javascript">
    function pageLoad(sender, args) {

        fieldValidator('textInputRequired', 'BtnAddVendor', 'lblValidation');

    }
</script>

    <style type="text/css">
        .style2
        {
            text-align: center;
            background-color: #FFFFCC;
            color: #336666;
            font-weight: bold;
            font-size: x-large;
            height: 43px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:Button ID="btnExcel" runat="server" Text="Excel" height="25px" Width="100px" />
<asp:UpdatePanel ID="uplPanel" runat ="server"  UpdateMode="Conditional">
    <Triggers>
    </Triggers>
   <ContentTemplate>
   <div id="maindiv" runat="server" align="left">
    <table  style="border-style: groove" align="left" width="100%">
        <tr align="center">
           <td class="tableRowHeader">
                  Vendor / Contractor Maintenance
           </td>
        </tr>
        <tr>
          <td align="left">
             <ajaxToolkit:TabContainer ID="tabs" runat="server" ActiveTabIndex="0"
                Width="100%"  EnableTheming="False" 
                BackColor="#efefef">

                  <ajaxToolkit:TabPanel ID="tpGrouplist" runat="server" TabIndex="0"  BackColor="Gainsboro" Visible = "True" EnableTheming="True" >
                    <HeaderTemplate > 
                        <asp:Label ID="Label4" runat="server" Text="Current Vendors/Contractors" Font-Bold="true" ForeColor="Black"></asp:Label>
                    </HeaderTemplate>
                      <ContentTemplate>

                       <table border="1" width="100%"  style="empty-cells:show" >
                         <tr>
                             <td align="right">
                                Vendor/Contractor Name
                            </td>
                            <td colspan="3">
                             <asp:TextBox ID="SearchVendorNameTextBox" Width="200px" runat = "server"></asp:TextBox>
                            </td>

                         </tr>
                         <tr>
                           <td  align="center" colspan="4">
                            <table>
                              <tr>
                                <td align="center">

                                        <asp:Button ID ="btnSearch" runat = "server" Text ="Search" 
                                        CssClass="btnhov" BackColor="#006666"  Width="135px" />

                                        
                                </td>
                                <td>
                                
                                </td>
                                <td align="center" >

                                        <asp:Button ID = "btnReset" runat= "server" Text="Reset"
                                        CssClass="btnhov" BackColor="#006666"  Width="135px" />

                                        

                                </td>

                            </tr>
                        </table>
                      </td>
                     </tr>          
                         <tr>
                           <td colspan="4">
                            <asp:GridView ID="GvVendors" runat="server" AllowSorting="True" 
                           AutoGenerateColumns="False" BackColor="White" BorderColor="#336666" 
                           BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                           DataKeyNames="VendorNum" EmptyDataText="No Vendors" 
                           EnableTheming="False" Font-Size="Medium" GridLines="Horizontal" Width="100%">
                           <FooterStyle BackColor="White" ForeColor="#333333" />
                           <HeaderStyle BackColor="#336666" Font-Bold="False" Font-Size="X-Small" 
                               ForeColor="White" HorizontalAlign="Left" />
                           <PagerStyle BackColor="#336666" Font-Size="Medium" ForeColor="White" 
                               HorizontalAlign="Left" />
                           <RowStyle BackColor="White" ForeColor="#333333" Font-Size="X-Small" />
                           <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                           <SortedAscendingCellStyle BackColor="#F7F7F7" />
                           <SortedAscendingHeaderStyle BackColor="#487575" />
                           <SortedDescendingCellStyle BackColor="#E5E5E5" />
                           <SortedDescendingHeaderStyle BackColor="#275353" />
                           <Columns>

                               <asp:CommandField ButtonType="Button" ControlStyle-BackColor="#84A3A3" 
                                      HeaderStyle-HorizontalAlign="Left" ItemStyle-Height="20px" SelectText="Select" 
                                      ShowSelectButton="true">
                                  <ControlStyle BackColor="#84A3A3" />
                                  <HeaderStyle HorizontalAlign="Left" />
                                  <ItemStyle Height="20px" />
                                  </asp:CommandField>
                            
                            	  


                               <asp:BoundField DataField="VendorName" HeaderText="Name" />

                               <asp:BoundField DataField="VendorType" HeaderText="Type" />

                               <asp:BoundField DataField="City" HeaderText="City" />

                               <asp:BoundField DataField="State" HeaderText="State " />

                               <asp:BoundField DataField="Phone" HeaderText="Phone"/>

                               <asp:BoundField DataField="zip" HeaderText="Zip"/>

                           </Columns>
                       </asp:GridView>                            
                           </td>
                         </tr>
                        </table>
                      </ContentTemplate>
                </ajaxToolkit:TabPanel>

                 <ajaxToolkit:TabPanel ID="tpNewSubmitter" runat="server" BackColor="Gainsboro" Visible = "True" EnableTheming="True" TabIndex="1">
                       <HeaderTemplate > 
                          <asp:Label ID="Label3" runat="server" Text="Add New Vendor/Contractor" Font-Bold="true" ForeColor="Black" ></asp:Label>
                     </HeaderTemplate>
                    <ContentTemplate>
                     <table border="1" width="100%"  style="empty-cells:show" >

                       <tr>
                               <td>
                               </td>
                              <td colspan="2" align="center">
                                    <asp:Label ID = "lblValidation" runat = "server" ForeColor="Red" Font-Bold="true"/>          
                              </td>
                              <td>
                               </td>

                      
                      </tr>

                      <tr>
                        <td  align="center" colspan="4">
                         <table>
                            <tr>
                                <td align="center">

                                        <asp:Button ID ="BtnAddVendor" runat = "server" Text ="Submit " 
                                        CssClass="btnhov" BackColor="#006666"  Width="135px" />

                                        
                                </td>
                                <td>
                                
                                </td>
                                <td align="center" >

                                        <asp:Button ID = "BtnAddReset" runat= "server" Text="Reset"  
                                        CssClass="btnhov" BackColor="#006666"  Width="135px" />

                                        

                                </td>
                            </tr>
                        </table>
                      </td>
                     </tr>
                      <tr>
                            <td>
                                <asp:Label ID="VendorTypelb" runat="server" Text="Type" ></asp:Label>
                            </td>
                            <td colspan="3">
                                <asp:RadioButtonList ID="VendorTyperbl" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Selected="True" Value="Vendor" Text="Vendor"></asp:ListItem>
                                    <asp:ListItem Value="Contractor" Text="Contractor"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                      </tr>   
                      <tr>
                          <td>
                              <asp:Label ID="VendorNamelbl" runat="server" Text="Vendor/Contractor Name">
                               </asp:Label>
                          </td>

                          <td colspan="3">
                               <asp:TextBox ID="VendorNameTextBox" runat="server" Width="450PX">
                                </asp:TextBox>
                          
                          </td>
                       </tr>
                       <tr>
                          <td>
                               <asp:Label ID="Addresslbl" runat="server" Text="Address">
                               </asp:Label>
                          
                          </td>
                          <td colspan="3">
                            <asp:TextBox ID="Address1TextBox" runat="server" Width="450PX" >
                            </asp:TextBox>
                          
                          </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Citylbl" runat="server" Text="City:"></asp:Label>
                            </td>
                            <td colspan="3">
                                <asp:TextBox ID="CityTextBox" runat="server" Width="450PX" >
                                </asp:TextBox>
                            
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Statelbl" runat="server" Text="State:"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID ="StateTextBox" runat="server" >
                                </asp:TextBox>
                            
                            </td>

                            <td>
                                <asp:Label ID="ziplbl" runat="server" Text="Zip:"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID ="ZipTextBox" runat="server" >
                                </asp:TextBox>
                            
                            </td>
                        
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="PhoneLbl" runat="server" Text="Phone:">
                                </asp:Label>    
                            
                            </td>
                            <td colspan="3">
                                <asp:TextBox ID ="PhoneTextBox" runat="server" >
                                </asp:TextBox>
                            

                            </td>
                         </tr>

                        </table>
                       </ContentTemplate>
                   </ajaxToolkit:TabPanel>

                  <ajaxToolkit:TabPanel ID="tpCurrentAccounts" runat="server"   BorderStyle="Solid" BorderWidth="1px" Visible = "False">
                      <HeaderTemplate > 
                            <asp:Label ID="lblcurr" runat="server" Text="Current Accounts" Font-Bold="true" ForeColor="Black" ></asp:Label>
                    
                      </HeaderTemplate>
                      <ContentTemplate>
                          <table border="0" width = "100%">
                            <tr>
                                <td colspan = "4">
                                      <asp:Table ID = "tblCurrentAccounts" runat = "server">
                                     </asp:Table>
            
                                </td>
        
                            </tr>
                           </table>
                       </ContentTemplate>
                  </ajaxToolkit:TabPanel>

          </ajaxToolkit:TabContainer>
        </td>
     </tr>
  </table>

  </div>
   </ContentTemplate>
  </asp:UpdatePanel>
   <script type="text/javascript">
       $(document).ready(function () {
           setJQCalander();
           // just commented $(".calanderClass").datepick({ showOn: 'both', buttonImage: 'Images/jQueryCalendar.gif' });
           //  alert("test select"); //only works first time,dateFormat: 'MM yyyy'
       });

       Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
       Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);
       function setJQCalander() {
           // User must hit submit
           $(".calanderClass").datepick({ showOn: 'both', buttonImage: 'Images/jQueryCalendar.gif' });

       }
       function EndRequestHandler(sender, args) {
           //end of async postback
           // alert("end Async" );
           setJQCalander();


       }

       function BeginRequestHandler(sender, args) {

       }
    </script>
</asp:Content>

