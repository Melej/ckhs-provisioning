﻿Imports System.IO
Imports System.Object
Imports System.Diagnostics
Imports System.ComponentModel
Imports App_Code

Partial Class ReportDisplay
    Inherits System.Web.UI.Page
    Dim Securitylevel As Integer

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        fillReportsTable()
        tblReports.Visible = True
        UpdatePanel1.Visible = False

    End Sub
    Protected Sub fillReportsTable()

        Dim Security As New UserSecurity(Session("LoginID"), Session("EmployeeConn"))

        Securitylevel = Security.SecurityLevelNum

        Select Case Securitylevel

            Case 72 To 79
                Response.Redirect("~/SimpleSearch.aspx", True)

            Case < 70
                Response.Redirect("~/SimpleSearch.aspx", True)

        End Select



        Dim thisData As New CommandsSqlAndOleDb("SelReports", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@securityLevel", Securitylevel, SqlDbType.Int, 8)

        Dim iRowCount = 1

        Dim dt As DataTable
        dt = thisData.GetSqlDataTable

        Dim rwHeader As New TableRow

        Dim clHelpIDHeader As New TableCell
        Dim clHelpDescHeader As New TableCell




        clHelpIDHeader.Text = " "
        clHelpDescHeader.Text = "Reports"


        rwHeader.Cells.Add(clHelpIDHeader)
        rwHeader.Cells.Add(clHelpDescHeader)



        rwHeader.Font.Bold = True
        rwHeader.BackColor = Color.FromName("#006666")
        rwHeader.ForeColor = Color.FromName("White")
        rwHeader.Height = Unit.Pixel(36)

        tblReports.Rows.Add(rwHeader)

        For Each drRow As DataRow In dt.Rows


            Dim rwData As New TableRow
            Dim btnSelect As New Button
            Dim clSelectData As New TableCell

            Dim clHelpDesc As New TableCell



            btnSelect.Text = "Select"
            btnSelect.CommandName = "Select"
            btnSelect.CommandArgument = drRow("ReportShortDesc")
            btnSelect.CssClass = "btnhov"
            btnSelect.Width = "129"


            AddHandler btnSelect.Click, AddressOf btnSelect_Click

            clHelpDesc.Text = IIf(IsDBNull(drRow("ReportDes")), "", drRow("ReportDes"))
            clSelectData.Controls.Add(btnSelect)


            rwData.Cells.Add(clSelectData)
            rwData.Cells.Add(clHelpDesc)


            If iRowCount > 0 Then

                rwData.BackColor = Color.Bisque

            Else

                rwData.BackColor = Color.LightGray

            End If

            tblReports.Rows.Add(rwData)

            iRowCount = iRowCount * -1

        Next
        tblReports.Font.Name = "Arial"
        tblReports.Font.Size = 12

        tblReports.CellPadding = 10
        tblReports.Width = New Unit("100%")


    End Sub
    Sub btnSelect_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim btnLinkView As Button = sender

        If btnLinkView.CommandName = "Select" Then
            Dim sArguments As String() = Split(btnLinkView.CommandArgument, ";")
            Select Case sArguments(0)

                Case "P1Report"
                    Response.Redirect("./P1Report.aspx", True)
                Case "Requests"
                    Response.Redirect("./RequestReport.aspx", True)
                Case "Accounts"
                    Response.Redirect("./AccountReport.aspx", True)
                Case "EndDate"
                    Response.Redirect("./EndDateReport.aspx", True)
                Case "Termination"
                    Response.Redirect("./TerminationReport.aspx", True)

                Case "Provider"
                    Response.Redirect("./ProviderReport.aspx", True)
                Case "TicketRFS"
                    Response.Redirect("./TicketsRFSReport.aspx", True)

                Case "TicketRFS8"
                    Response.Redirect("./TicketRFSReportDetails.aspx", True)

                Case "HRReport"
                    Response.Redirect("./NewHireReport.aspx", True)

                Case Else

            End Select

        End If
    End Sub

End Class
