﻿Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports App_Code

Partial Class VendorMaintenance
    Inherits System.Web.UI.Page
    Dim sActivitytype As String = ""
    Dim cblBinder As New CheckBoxListBinder
    Dim UserInfo As UserSecurity
    Dim FormSignOn As New FormSQL()
    Dim bHasError As Boolean

    Dim iUserNum As Integer
    Dim iUserSecurityLevel As Integer

    Dim VendorNum As String
    Dim AccountCd As String
    Dim MaintType As String

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        VendorNum = Request.QueryString("VendorNum")

        FormSignOn = New FormSQL(Session("EmployeeConn"), "SelVendors", "InsUpdVendors", "", Page)

        UserInfo = New UserSecurity(Session("LoginID"), Session("EmployeeConn"))

        iUserNum = UserInfo.ClientNum
        iUserSecurityLevel = UserInfo.SecurityLevelNum

        If iUserSecurityLevel < 70 Then
            Response.Redirect("~/SimpleSearch.aspx", True)
        ElseIf iUserSecurityLevel < 90 Then
            tpNewSubmitter.Visible = False
        End If

        If VendorNum = Nothing Then
            FormSignOn.AddSelectParm("@VendorName", "all", SqlDbType.NVarChar, 25)
        Else
            FormSignOn.AddSelectParm("@VendorNum", VendorNum, SqlDbType.Int, 6)

        End If

        'GNumber = "1"


        If Not IsPostBack Then
            'If GNumber <> "" Then

            '    tabs.ActiveTabIndex = 1

            'End If

            tabs.ActiveTabIndex = 0

            bindVendors()

            ' LoadGroupTypes("list")
            GvVendors.SelectedIndex = -1
        End If

        CheckRequiredFields()


    End Sub
    Public Sub bindVendors()


        Dim thisData As New CommandsSqlAndOleDb("SelVendors", Session("EmployeeConn"))

        If SearchVendorNameTextBox.Text = "" Then
            thisData.AddSqlProcParameter("@VendorName", "all", SqlDbType.NVarChar)
        Else
            thisData.AddSqlProcParameter("@VendorName", SearchVendorNameTextBox.Text, SqlDbType.NVarChar)
        End If

        Dim dt As DataTable

        dt = thisData.GetSqlDataTable

        GvVendors.DataSource = dt
        GvVendors.DataBind()


        GvVendors.SelectedIndex = -1
    End Sub

    Protected Sub GvVendors_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GvVendors.RowCommand
        If e.CommandName = "Select" Then

            Dim VendorNumS As String = GvVendors.DataKeys(Convert.ToInt32(e.CommandArgument))("VendorNum").ToString()

            GvVendors.SelectedIndex = -1

            Response.Redirect("./VendorMaintDetail.aspx?VendorNum=" & VendorNumS, True)


        End If
    End Sub

    Protected Sub BtnAddVendor_Click(sender As Object, e As System.EventArgs) Handles BtnAddVendor.Click
        'If VendorNameTextBox.Text = "" Then
        '    Exit Sub
        'End If


        CheckRequiredFields()

        ' FormSignOn.AddInsertParm("@group_Num", GroupNum, SqlDbType.Int, 9)
        FormSignOn.UpdateForm()

        Response.Redirect("./VendorMaintenance.aspx", True)

    End Sub
    Public Function CheckRequiredFields() As Boolean
        Dim removeCss As New PageControls(Page)
        removeCss.RemoveCssClass(Page, "textInputRequired")

        Dim firstNameReq As New RequiredField(VendorNameTextBox, "Vendor Name")
        Dim add1Req As New RequiredField(Address1TextBox, "Address 1")
        Dim cityReq As New RequiredField(CityTextBox, "City")
        Dim stateReq As New RequiredField(StateTextBox, "State")
        Dim zipReq As New RequiredField(ZipTextBox, "ZIP")
        Dim phoneReq As New RequiredField(PhoneTextBox, "Phone")

        Return bHasError

    End Function

    Protected Sub btnReset_Click(sender As Object, e As System.EventArgs) Handles btnReset.Click
        Response.Redirect("./VendorMaintenance.aspx", True)
    End Sub

    Protected Sub btnSearch_Click(sender As Object, e As System.EventArgs) Handles btnSearch.Click
        bindVendors()
    End Sub
    Protected Sub btnExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExcel.Click
        CreateExcel2()
    End Sub
    Sub CreateExcel2()



        Response.AddHeader("content-disposition", "attachment;filename=FileName.xls")

        Response.Charset = String.Empty
        Response.ContentType = "application/vnd.ms-excel"
        '  Response.ContentType = "application/vnd.xls"

        Dim sw As System.IO.StringWriter = New System.IO.StringWriter()

        Dim hw As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(sw)

        '===============================Create form to contain grid
        Dim frm As New HtmlForm()

        bindVendors()
        GvVendors.Columns.RemoveAt(0)
        GvVendors.Parent.Controls.Add(frm)
        frm.Attributes("RunAt") = "server"
        frm.Controls.Add(GvVendors)

        frm.RenderControl(hw)
        Response.Write(sw.ToString())

        Response.End()

    End Sub
End Class
