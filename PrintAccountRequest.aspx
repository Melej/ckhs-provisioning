﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PrintAccountRequest.aspx.vb" Inherits="PrintAccountRequest" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>

			<table>
                 <tr>
                      <td colspan = "4">
                              <table class="style3" > 
                                            <tr class="tableRowSubHeader">
                                                <td colspan="4" align="center" >
                                                    Account Request# <asp:Label ID="requestnumlabel" runat="server" ></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right" >
                                                    First Name:
                                                </td>
                                                <td align="left"  >
                                                    <asp:Label width="280px" ID = "first_namelabel" runat="server"  Font-Bold="True" ></asp:Label>
                                                </td>
                                                <td align="right" >
                                                    Last Name:
                                                </td>

                                                <td align="left" >
                                                    <asp:Label ID = "last_namelabel" width="280px" runat="server" Font-Bold="True"></asp:Label>
                                                </td>
                                            </tr>
								            <tr>

                                                    <td align="right">
                                                            <asp:Label ID="currSubmitter" runat="server" Text="Submitted By:" ></asp:Label>
                                                    </td>
                                                    <td align="left">
                                                            <asp:Label runat="server" ID="submittedameLabel" Font-Bold="True" ></asp:Label>
                                         
                                                    </td>

                                                    <td align="right">
                                                          <asp:Label ID="currPositionlb" runat="server" Text = "Position:"></asp:Label>
                                                      </td>

                                                    <td align="left" >
                                                                <asp:Label runat="server" ID="positionlabel" Font-Bold="True"></asp:Label>
                                                        </td>

                                                </tr>


                                            <tr>
                                                 <td align="right">
                                                    Date Entered:
                                                </td>

                                                <td align="left">
                                                    <asp:Label ID="entrydatelabel" runat="server" Font-Bold="True"></asp:Label>
                                                </td>

                                                <td align="right" >
                                                    <asp:Label ID="lbldept" runat="server" Text = "Department Name:" />
                                                </td>
                                                <td runat="server" align="left"  >
                                                <asp:DropDownList ID ="departmentcdddl" runat="server" Width="250px" 
                                                        Enabled="False">
            
                                                </asp:DropDownList>
                                                </td>

                                            </tr>
                                                <tr>
                                                        <td id="Td1" runat="server" align="right">
                                                            <asp:Label ID="requestlabel" runat="server" Text = "Request Type:"></asp:Label>
                                                        </td>    
                                                
<%--                                                        <td id="Td2" runat="server" align="left">
                                                            <asp:DropDownList ID="categorycdddl" runat="server" Enabled="False" Font-Bold="True" >
                                                            </asp:DropDownList>
                                                        </td> --%>   

                                                        <td id="Td3" runat="server" align="left">
                                                            <asp:Label ID="Requesttypebl" runat="server"  Font-Bold="True"></asp:Label>
                                                        </td>
  
<%--                                                        <td id="Td4" runat="server" align="left">
                                                            <asp:DropDownList ID="typecdddl" runat="server" Enabled="False" Font-Bold="True">
                                                            </asp:DropDownList>
                                                        </td> --%>
                                               
                                                         <td colspan="2">

                                                         </td>                              

                                                </tr>

                           
                                                <tr>
                                                <td align="right" >
                                                Location:
                                                </td>
                                                <td align="left" >
         
                                                    <asp:DropDownList ID ="facilitycdddl" runat="server" Enabled="False" Font-Bold="True" >
              
                                                </asp:DropDownList>
          
                                                </td>
        
                                                <td align="right" >
                                                Building:
                                                </td>
                                                <td align="left" >

                                                    <asp:DropDownList ID ="buildingcdddl" runat="server" Enabled="False" Font-Bold="True" >
            
                                                    </asp:DropDownList>
            
                                                </td>
                                        </tr>
<%--                                         <tr>
                                            <td align="right" >
                                                Floor:
                                            </td>

                                            <td align="left">
                                                <asp:DropDownList ID="Floorddl" runat="server" Enabled="False" Font-Bold="True">
            
                                                </asp:DropDownList>
                            
                                            </td>
                                            <td align="right" >
                                                    Office:
                                            </td>
                               
                                            <td align="left" > 
                                                <asp:Label ID="OfficeLabel" runat="server"></asp:Label>
                                            </td>


                                        </tr>

                                        <tr>
                                            <td align="right">
                                                Entered by:
                                            </td>
                                            <td align="left">
                                                <asp:Label ID="entrytechnamelabel" runat="server" Font-Bold="True"></asp:Label>
                                
                                            </td>
                                            <td align="right">
                                                
                                            </td>

                                            <td align="left">
                                                
                                            </td>
                                      </tr>--%>

                               
<%--                                        <tr>
                                            <td align="right">
                                                Alternate Contact:
                                            </td>
                                                 <td align="left">
                                                     <asp:Label runat="server" ID = "alternatecontactnameLabel" Width="90%" Font-Bold="True"></asp:Label>
                                                 </td>
                                                 <td align="right">
                                                    Alternate Phone:
                                                 </td>
                                                 <td align="left">
                                                    <asp:Label runat="server" ID = "alternatecontactphoneLabel" Font-Bold="True"></asp:Label>
                                                 </td>
                                        </tr>--%>


<%--                                <tr>

                                    <td align="right">
                                                <asp:Label ID="assettagLbl" runat="server" Text="Asset Tag:"></asp:Label>                            
                                    </td>
                                    <td align="left">
                                            <asp:Label ID="assettagLabel" runat="server" Width="270px" Font-Bold="True"></asp:Label>                            
                                    </td>                                                        
                                        <td align="right">
                                        <asp:Label ID="iplbl" runat="server" Text="Ip Address:"></asp:Label>
                                
                                    </td>
                                    <td align="left">
                                        <asp:Label ID="ipaddressLabel" runat="server" Font-Bold="True"></asp:Label>
                                
                                    </td>
                                </tr>--%>



                              </table>

                         </td>
                    </tr>
<%--        <tr>
            <td align="left" colspan="2">
            
                <asp:RadioButtonList ID="RequestTyperbl" runat="server"  Enabled="False" 
                    RepeatDirection="Horizontal">
                        <asp:ListItem Value="ticket" Text="Ticket"></asp:ListItem>
                        <asp:ListItem Value="service" Text="Req. For Service"></asp:ListItem>
                                                           
                </asp:RadioButtonList>          
            
            </td>
            <td align="left" colspan="2">
                Current Status:
                    <asp:Label ID="currstatusDescLabel" runat="server"  Font-Bold="True"></asp:Label>
                   
            </td>
        </tr> --%>
<%--        <tr>
            <td colspan="4">
            
               <asp:Panel Visible="False" runat="server" ID="ticketPnl">

                    <asp:TextBox ID="shortdescTextBox" runat="server" TextMode="MultiLine" Width="95%">
                    </asp:TextBox>
                </asp:Panel>

            <asp:Panel Visible="False" runat="server" ID="rfsPnl">

                <table>
                    <tr>
                        <td>
                                Service Desc:
                                <asp:TextBox ID="servicedescTextbox" runat="server" Enabled="false" TextMode="MultiLine" Width="95%">
                                </asp:TextBox>
                        
                        </td>

                    </tr>                

                    <tr>
                        <td>
                                Business Needs:
                                <asp:TextBox ID="businessdesTextBox" runat="server" Enabled="false" TextMode="MultiLine" Width="95%">
                                </asp:TextBox>
                        
                        </td>

                    </tr>                
                    <tr>
                        <td>
                        
                                Completion Date:                        
                                <asp:TextBox ID="completiondateTextBox" runat="server" Enabled="false">
                                </asp:TextBox>
                        </td>

                    </tr>                

                
                </table>

            </asp:Panel>

            </td>
        </tr>--%>
        <tr>
        
            <td colspan = "4">
            
                       <div  style="overflow:auto;" class="subContainer" >
                                        <asp:Table runat="server" ID="TPActionItems" Width="100%">
        
                                        </asp:Table>

                        </div>
            
            
            </td>
        
        </tr>
        <tr>
            <td colspan = "4">
                <asp:DropDownList ID ="entitycdDDL" runat="server"  Visible="false">
            
                </asp:DropDownList>
                <asp:Label ID="nt_login" runat="server" Visible="False"></asp:Label>
                <asp:Label ID="clientnumLabel" runat="server" Visible="False"></asp:Label>
                <asp:Label ID="entryclientnumLabel" runat="server" Visible="False"></asp:Label>

                <asp:Label ID="WhoclientNumLabel" runat="server" Visible="False"></asp:Label>
                        
                <asp:Label ID="SecurityLevelLabel" runat="server" Visible="False"></asp:Label>
                        
                <asp:Label ID ="department_cdLabel" runat="server" Visible="False"></asp:Label>
                <asp:Label ID ="TitleLabel" runat="server" Visible="False"></asp:Label>
                                     
                <asp:Label ID = "entity_cdLabel" runat="server" Visible ="False"></asp:Label>
                <asp:Label ID = "building_cdLabel" runat="server" Visible ="False"></asp:Label>

                <asp:Label ID= "building_nameLabel" runat="server" Visible="False" ></asp:Label>
                <asp:Label ID="HDemployeetype" runat="server" Visible="False"></asp:Label> 

                <asp:Label ID="suffixlabel" runat="server" Visible="False"></asp:Label>
                <asp:Label ID= "AccountRequestTypeLabel" runat="server" Visible="False"></asp:Label>
                <asp:Label ID="typecdLabel" runat="server" Visible="False"></asp:Label>
                <asp:Label ID="categorycdLabel"  runat="server" Visible="False"></asp:Label>
                <asp:Label ID="itemcdLabel" runat="server" visible="False" />
                                        
                <asp:Label ID="facility_cdLabel" runat="server" visible="False" />
                <asp:Label ID="floorLabel" runat="server" visible="False" />
                            
                <asp:Label ID = "currenttechnumLabel" runat="server" Visible ="False"></asp:Label>
                <asp:Label ID = "currentgroupnumLabel" runat="server" Visible ="False"></asp:Label>

                <asp:Label ID="RequestTypeLabel" runat="server" Visible="False"></asp:Label>
                <asp:Label ID="currstatuscdLabel" runat="server" Visible="False"></asp:Label>

                <asp:Label ID="CloseDateLabel" runat="server" Visible="False"></asp:Label>
                                        
                <asp:Label ID="numofterminalsLabel"  runat="server" Visible="False"></asp:Label>
                <asp:label id="entrytechnumLabel" runat="server" Visible="False"></asp:label>
                <asp:Label ID="hdPriorityLb" runat="server" Visible="false"></asp:Label>

                <asp:Label ID="hdexists" runat="server" Visible="false"></asp:Label>
                </td>
            </tr>

    
    </table>

        </div>
    </form>
</body>
</html>
