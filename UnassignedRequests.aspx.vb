﻿Imports App_Code

Partial Class UnassignedRequests
    Inherits System.Web.UI.Page
    Private dtGroups As DataTable
    Dim UserInfo As UserSecurity
    Dim ddlBinder As DropDownListBinder
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        UserInfo = Session("objUserSecurity")


        Dim thisData As New CommandsSqlAndOleDb("sel_group_names", Session("EmployeeConn"))

        dtGroups = thisData.GetSqlDataTable


        'If Not IsPostBack Then



        'End If


        If rblRequestType.SelectedValue = "ticket" Then

            FillUnassignedTable()

        ElseIf rblRequestType.SelectedValue = "rfs" Then

            FillUnassignedRequestsTable()


        End If


    End Sub
    Protected Sub FillUnassignedTable()
        tblUnassigned.Rows.Clear()


        Dim thisData As New CommandsSqlAndOleDb("SelRequestTicket", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@SelectType", "Unassigned", SqlDbType.NVarChar, 12)

        Dim dt As DataTable

        dt = thisData.GetSqlDataTable



        For Each dr In thisData.GetSqlDataTable.Rows
            Dim rwHeader As New TableRow
            Dim clHeader As New TableCell

            Dim rwShortDesc As New TableRow
            Dim clShortDesc As New TableCell
            Dim clShortDescTitle As New TableCell

            Dim rwAssignGroup As New TableRow
            Dim clAssignGroupTitle As New TableCell
            Dim clAssignGroup As New TableCell

            Dim ddlAssignGroup As New DropDownList
            Dim ddlAssignIndividual As New DropDownList
            Dim uplAssign As New UpdatePanel


            Dim lblSelectGroup As New Label
            Dim lblSelectIndividual As New Label

            Dim btnAssign As New Button

            lblSelectGroup.Text = "Select Group: "
            lblSelectIndividual.Text = "Select Individual: "

            btnAssign.Text = "Assign ticket"
            btnAssign.ID = "btnAssignGroup" & dr("RequestNum")

            rwHeader.CssClass = "tableRowSubHeader"

            clHeader.ColumnSpan = "2"
            clHeader.Text = "<span style='float:left'>Requestor: " & dr("RequestorName") & "</span><span style='float:right'>" & dr("DateEntered") & "</span>"

            rwHeader.Cells.Add(clHeader)


            clShortDescTitle.Text = "Situation: "
            clShortDesc.Text = dr("ShortDesc")

            rwShortDesc.Cells.Add(clShortDescTitle)
            rwShortDesc.Cells.Add(clShortDesc)

            clAssignGroupTitle.Text = "Assign Ticket: "

            ddlAssignGroup.DataSource = dtGroups
            ddlAssignGroup.DataTextField = "group_name"
            ddlAssignGroup.DataValueField = "group_num"
            ddlAssignGroup.DataBind()

            ddlAssignGroup.AutoPostBack = True
            ScriptManager.GetCurrent(Me).RegisterAsyncPostBackControl(ddlAssignGroup)
            ddlAssignGroup.ID = "ddlAssignGroup" & dr("RequestNum")




            ddlAssignIndividual.ID = "ddlAssignIndividual" & dr("RequestNum")


            AddHandler ddlAssignGroup.SelectedIndexChanged, AddressOf AssignGroupIndexChanged
            AddHandler btnAssign.Click, AddressOf btnAssignGroup_click




            With uplAssign

                .ID = "uplAssign" & dr("RequestNum")
                .ChildrenAsTriggers = False
                .UpdateMode = UpdatePanelUpdateMode.Conditional

                .ContentTemplateContainer.Controls.Add(lblSelectGroup)
                .ContentTemplateContainer.Controls.Add(ddlAssignGroup)
                .ContentTemplateContainer.Controls.Add(lblSelectIndividual)
                .ContentTemplateContainer.Controls.Add(ddlAssignIndividual)
                .ContentTemplateContainer.Controls.Add(btnAssign)

            End With


            clAssignGroup.Controls.Add(uplAssign)


            rwAssignGroup.Cells.Add(clAssignGroupTitle)
            rwAssignGroup.Cells.Add(clAssignGroup)



            tblUnassigned.Rows.Add(rwHeader)
            tblUnassigned.Rows.Add(rwShortDesc)
            tblUnassigned.Rows.Add(rwAssignGroup)



        Next

    End Sub
    Protected Sub FillUnassignedRequestsTable()
        tblUnassigned.Rows.Clear()

        Dim thisData As New CommandsSqlAndOleDb("SelRequestService", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@SelectType", "Unassigned", SqlDbType.NVarChar, 12)

        thisData.ExecNonQueryNoReturn()

        For Each dr In thisData.GetSqlDataTable.Rows
            Dim rwHeader As New TableRow
            Dim clHeader As New TableCell

            Dim rwShortDesc As New TableRow
            Dim clShortDesc As New TableCell
            Dim clShortDescTitle As New TableCell

            Dim rwAssignGroup As New TableRow
            Dim clAssignGroupTitle As New TableCell
            Dim clAssignGroup As New TableCell

            Dim ddlAssignGroup As New DropDownList
            Dim ddlAssignIndividual As New DropDownList
            Dim uplAssign As New UpdatePanel



            Dim lblSelectGroup As New Label
            Dim lblSelectIndividual As New Label

            Dim btnAssign As New Button

            lblSelectGroup.Text = "Select Group: "
            lblSelectIndividual.Text = "Select Individual: "

            btnAssign.Text = "Assign ticket"
            btnAssign.ID = "btnAssignGroup" & dr("RequestNum")

            rwHeader.CssClass = "tableRowSubHeader"

            clHeader.ColumnSpan = "2"
            clHeader.Text = "<span style='float:left'>Requestor: " & dr("RequestorName") & "</span><span style='float:right'>" & dr("DateEntered") & "</span>"

            rwHeader.Cells.Add(clHeader)


            clShortDescTitle.Text = "Service Description: "
            clShortDesc.Text = dr("ServiceDesc")

            rwShortDesc.Cells.Add(clShortDescTitle)
            rwShortDesc.Cells.Add(clShortDesc)

            clAssignGroupTitle.Text = "Assign Ticket: "




            ddlAssignGroup.DataSource = dtGroups
            ddlAssignGroup.DataTextField = "group_name"
            ddlAssignGroup.DataValueField = "group_num"
            ddlAssignGroup.DataBind()

            ddlAssignGroup.AutoPostBack = True
            ScriptManager.GetCurrent(Me).RegisterAsyncPostBackControl(ddlAssignGroup)

            ddlAssignGroup.ID = "ddlAssignGroup" & dr("RequestNum")




            ddlAssignIndividual.ID = "ddlAssignIndividual" & dr("RequestNum")


            AddHandler ddlAssignGroup.SelectedIndexChanged, AddressOf AssignGroupIndexChanged
            AddHandler btnAssign.Click, AddressOf btnAssignGroup_click




            With uplAssign

                .ID = "uplAssign" & dr("RequestNum")
                .ChildrenAsTriggers = False
                .UpdateMode = UpdatePanelUpdateMode.Conditional

                .ContentTemplateContainer.Controls.Add(lblSelectGroup)
                .ContentTemplateContainer.Controls.Add(ddlAssignGroup)
                .ContentTemplateContainer.Controls.Add(lblSelectIndividual)
                .ContentTemplateContainer.Controls.Add(ddlAssignIndividual)
                .ContentTemplateContainer.Controls.Add(btnAssign)

            End With


            clAssignGroup.Controls.Add(uplAssign)
          


            rwAssignGroup.Cells.Add(clAssignGroupTitle)
            rwAssignGroup.Cells.Add(clAssignGroup)



            tblUnassigned.Rows.Add(rwHeader)
            tblUnassigned.Rows.Add(rwShortDesc)
            tblUnassigned.Rows.Add(rwAssignGroup)



        Next


    End Sub
    Protected Sub AssignGroupIndexChanged(sender As Object, e As System.EventArgs)

        Dim reqNum As String
        Dim ddlSender As DropDownList
        Dim ddlIndividual As New DropDownList
        Dim uplAssign As New UpdatePanel



        ddlSender = DirectCast(sender, DropDownList)

        reqNum = DirectCast(sender, DropDownList).ID

        reqNum = reqNum.Replace("ddlAssignGroup", "")

        ddlIndividual = tblUnassigned.FindControl("ddlAssignIndividual" & reqNum)
        uplAssign = tblUnassigned.FindControl("uplAssign" & reqNum)

        ddlBinder = New DropDownListBinder(ddlIndividual, "selTechnicians", Session("EmployeeConn"))
        ddlBinder.AddDDLCriteria("@GroupNum", ddlSender.SelectedValue, SqlDbType.Int)
        ddlBinder.BindData("client_num", "TechName")

        uplAssign.Update()


    End Sub

    Protected Sub btnAssignGroup_click(sender As Object, e As System.EventArgs)


        Dim reqNum As String
        ' Get ID from sending button

        reqNum = DirectCast(sender, Button).ID

        reqNum = Replace(reqNum, "btnAssignGroup", "")

        Dim ddlGroup As DropDownList
        Dim ddlIndividual As DropDownList

        ddlGroup = tblUnassigned.FindControl("ddlAssignGroup" & reqNum)
        ddlIndividual = tblUnassigned.FindControl("ddlAssignIndividual" & reqNum)

        If ddlGroup.SelectedIndex < 0 Then

            Return

        End If

        Dim iGroupNum As Integer = ddlGroup.SelectedValue

        Dim thisData As New CommandsSqlAndOleDb("UpdRequestEntry", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@RequestNum", reqNum, SqlDbType.Int, 12)
        thisData.AddSqlProcParameter("@AssignedGroupNum", iGroupNum, SqlDbType.Int)
        thisData.AddSqlProcParameter("@DateTimeGroupAssigned", Date.Now(), SqlDbType.DateTime)

        If ddlIndividual.SelectedIndex > 0 Then

            thisData.AddSqlProcParameter("AssignedTechClientNum", ddlIndividual.SelectedValue, SqlDbType.Int)
            thisData.AddSqlProcParameter("@DateTimeTechAssigned", Date.Now(), SqlDbType.DateTime)


        End If


        thisData.ExecNonQueryNoReturn()


        Dim ReqActivity As New RequestActivity(reqNum, UserInfo.ClientNum, "Open Group Assignment", "sys", "openga")



    End Sub
    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles Me.PreInit

    End Sub

    'Protected Sub btnRFS_Click(sender As Object, e As System.EventArgs) Handles btnRFS.Click
    '    FillUnassignedRequestsTable()
    'End Sub

    'Protected Sub btnTickets_Click(sender As Object, e As System.EventArgs) Handles btnTickets.Click
    '    FillUnassignedTable()
    'End Sub

    Protected Sub btnClose_Click(sender As Object, e As System.EventArgs) Handles btnClose.Click
        mpeAssign.Hide()
    End Sub

    Protected Sub rblRequestType_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles rblRequestType.SelectedIndexChanged
        If rblRequestType.SelectedValue = "ticket" Then

            FillUnassignedTable()

        ElseIf rblRequestType.SelectedValue = "rfs" Then

            FillUnassignedRequestsTable()


        End If
    End Sub
End Class
