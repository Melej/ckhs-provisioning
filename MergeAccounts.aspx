﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="false" CodeFile="MergeAccounts.aspx.vb" Inherits="MergeAccounts"  ViewStateMode="Enabled"%>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
<style type="text/css"> 

</style>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

  
<asp:UpdatePanel runat="server" ID = "uplAccounts" UpdateMode ="Conditional">
<ContentTemplate>
<asp:Table runat="server" ID ="tblMerge" Width="100%" BorderWidth="3">
<asp:TableRow CssClass="tableRowHeader">
    <asp:TableCell ColumnSpan="2">
         Merge Accounts
    </asp:TableCell>
</asp:TableRow>

<asp:TableRow CssClass="tableRowSubHeader">
         <asp:TableCell Width="50%">
            Select First Account
          <asp:Button Text="Search" runat="server" ID="btnSearchFirstAccount" />
        </asp:TableCell>
       <asp:TableCell Width="50%">
            Select Second Account
        <asp:Button Text="Search" runat="server" ID="btnSearchSecondAccount" />
    </asp:TableCell>
    
    
</asp:TableRow>
<asp:TableRow>
    <asp:TableCell>
            <asp:Table ID="tbl1stInfo" runat="server">
           
                <asp:TableRow>
                  <asp:TableCell>
                  
                  First Name:
                   </asp:TableCell>  
                 
                   <asp:TableCell>
                   <asp:TextBox runat="server" ID="txt1stFirstName"></asp:TextBox>
                    </asp:TableCell>  
                </asp:TableRow>
                 <asp:TableRow>
                  <asp:TableCell>
                    Last Name:
                     </asp:TableCell>    
                     <asp:TableCell>
                     <asp:TextBox runat="server" ID="txt1stLastName"></asp:TextBox>
                      </asp:TableCell>  
                </asp:TableRow>
                 <asp:TableRow>
                  <asp:TableCell>
                    Network Login:
                     </asp:TableCell>    
                     <asp:TableCell>
                     <asp:TextBox runat="server" ID="txt1stLoginName"></asp:TextBox><asp:Label runat="server" ID="lbl1stLoginInfo"></asp:Label>
                      </asp:TableCell>  
                </asp:TableRow>
                 <asp:TableRow>
                  <asp:TableCell>
                    Department:  
                     </asp:TableCell>  
                     <asp:TableCell><asp:TextBox runat="server" ID="txt1stDepartmentCd"></asp:TextBox>
                      </asp:TableCell>  
                </asp:TableRow>
                 <asp:TableRow>
                  <asp:TableCell>
                    Entity:  
                     </asp:TableCell>  
                     <asp:TableCell>
                     <asp:TextBox runat="server" ID="txt1stEntityCd"></asp:TextBox>
                      </asp:TableCell>  
                </asp:TableRow>
                 <asp:TableRow>
                  <asp:TableCell>
                   Employee Number: 
                    </asp:TableCell>  
                    <asp:TableCell>
                     <asp:TextBox runat="server" ID="txt1stSiemensEmpNum"></asp:TextBox>
                      </asp:TableCell>  
                </asp:TableRow>
            </asp:Table>
    
    
    </asp:TableCell>

      <asp:TableCell>
            <asp:Table ID="tbl2ndInfo" runat="server">
           
                <asp:TableRow>
                  <asp:TableCell>
                  
                  First Name:
                   </asp:TableCell>  
                 
                   <asp:TableCell>
                   <asp:TextBox runat="server" ID="txt2ndFirstName"></asp:TextBox>
                    </asp:TableCell>  
                </asp:TableRow>
                 <asp:TableRow>
                  <asp:TableCell>
                    Last Name:
                     </asp:TableCell>    
                     <asp:TableCell>
                     <asp:TextBox runat="server" ID="txt2ndLastName"></asp:TextBox>
                      </asp:TableCell>  
                </asp:TableRow>
                 <asp:TableRow>
                  <asp:TableCell>
                    Network Login:
                     </asp:TableCell>    
                     <asp:TableCell>
                     <asp:TextBox runat="server" ID="txt2ndLoginName"></asp:TextBox><asp:Label runat="server" ID="lbl2ndLoginInfo"></asp:Label>
                      </asp:TableCell>  
                </asp:TableRow>
                 <asp:TableRow>
                  <asp:TableCell>
                    Department:  
                     </asp:TableCell>  
                     <asp:TableCell><asp:TextBox runat="server" ID="txt2ndDepartmentCd"></asp:TextBox>
                      </asp:TableCell>  
                </asp:TableRow>
                 <asp:TableRow>
                  <asp:TableCell>
                    Entity:  
                     </asp:TableCell>  
                     <asp:TableCell>
                     <asp:TextBox runat="server" ID="txt2ndEntityCd"></asp:TextBox>
                      </asp:TableCell>  
                </asp:TableRow>
                 <asp:TableRow>
                  <asp:TableCell>
                   Employee Number: 
                    </asp:TableCell>  
                    <asp:TableCell>
                     <asp:TextBox runat="server" ID="txt2ndSiemensEmpNum"></asp:TextBox>
                      </asp:TableCell>  
                </asp:TableRow>
            </asp:Table>
    
    
    </asp:TableCell>

</asp:TableRow>
<asp:TableRow runat="server" ID = "rwCurrentAccountsHeader" BorderWidth="1"  Visible = "false">
    <asp:TableCell>
     Current Accounts
    </asp:TableCell>
        <asp:TableCell>
     Current Accounts
    </asp:TableCell>

</asp:TableRow>
<asp:TableRow runat="server" ID ="rwCurrentAccounts" BorderWidth="1" Visible = "false">
 <asp:TableCell VerticalAlign="Top">
                        <asp:GridView ID="gvFirstAccounts" runat="server" AllowSorting="True" 
                                                                                AutoGenerateColumns="False" BackColor="White" BorderColor="#336666"
                                                                                BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                                                                                EmptyDataText="Select applications to be removed" EnableTheming="False" DataKeyNames="ApplicationNum, ApplicationDesc, LoginName"
                                                                                Font-Size="Medium" GridLines="Horizontal" Width="100%" >
                                   
                                                                                <FooterStyle BackColor="White" ForeColor="#333333" />
                                                                                <HeaderStyle BackColor="#336666" Font-Bold="False" ForeColor="White" Font-Size="Small" HorizontalAlign="Left"    />                                    
                                                                                <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Left" Font-Size="Medium"/>
                                                                                <RowStyle BackColor="White" ForeColor="#333333" />
                                                                                <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                                                                <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                                                                <SortedAscendingHeaderStyle BackColor="#487575" />
                                                                                <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                                                                <SortedDescendingHeaderStyle BackColor="#275353" />

                                                                                <Columns>
                                              
                                              
                                                                                        <asp:BoundField  DataField="ApplicationDesc"  HeaderText="Application" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
				                                                                            <asp:BoundField  DataField="LoginName" HeaderText="User Name" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
                                                                                            <asp:BoundField  DataField="CreateDate" HeaderText="Create Date" HeaderStyle-HorizontalAlign="Left" dataformatstring="{0:d}"></asp:BoundField>
                                                                                                                  





                                  
                                                                                </Columns>

                                                                            </asp:GridView>
                      </asp:TableCell>

                   <asp:TableCell VerticalAlign="Top">
                            <asp:GridView ID="gvSecondAccounts" runat="server" AllowSorting="True" 
                                                                                AutoGenerateColumns="False" BackColor="White" BorderColor="#336666"
                                                                                BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                                                                                EmptyDataText="Select applications to be removed" EnableTheming="False" DataKeyNames="ApplicationNum, ApplicationDesc, LoginName"
                                                                                Font-Size="Medium" GridLines="Horizontal" Width="100%" >
                                   
                                                                                <FooterStyle BackColor="White" ForeColor="#333333" />
                                                                                <HeaderStyle BackColor="#336666" Font-Bold="False" ForeColor="White" Font-Size="Small" HorizontalAlign="Left"    />                                    
                                                                                <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Left" Font-Size="Medium"/>
                                                                                <RowStyle BackColor="White" ForeColor="#333333" />
                                                                                <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                                                                <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                                                                <SortedAscendingHeaderStyle BackColor="#487575" />
                                                                                <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                                                                <SortedDescendingHeaderStyle BackColor="#275353" />

                                                                                <Columns>
                                              
                                              
                                                                                         
                                                                                            <asp:BoundField  DataField="ApplicationDesc"  HeaderText="Application" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
				                                                                            <asp:BoundField  DataField="LoginName" HeaderText="User Name" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
                                                                                            <asp:BoundField  DataField="CreateDate" HeaderText="Create Date" HeaderStyle-HorizontalAlign="Left" dataformatstring="{0:d}"></asp:BoundField>
                                                               
                                                                                                                  




                                  
                                                                                </Columns>

                                                                            </asp:GridView>
                     </asp:TableCell>

</asp:TableRow>
<asp:TableRow runat="server" ID="rwMerge" BorderWidth="1"  Visible = "false">
                    <asp:TableCell VerticalAlign="Top">
                        Merge Account Access from the Left Account to the Right Account
                        <asp:Button runat="server" ID="btnMergeToTheRight" Text = "Merge Right" />
                    </asp:TableCell>
                     <asp:TableCell VerticalAlign="Top">
                        Merge Account Access from the Right Account to the Left Account
                        <asp:Button runat="server" ID="btnMergeToTheLeft" Text = "Merge Left" />
                    </asp:TableCell>
</asp:TableRow>

</asp:Table>




   </ContentTemplate>

</asp:UpdatePanel>

      <asp:Button ID="btnRequestorDummy" Style="display: none" runat="server" Text="Button" />

                                                    <ajaxToolkit:ModalPopupExtender ID="mpeSearchClient" runat="server"   
                                                                        DynamicServicePath="" Enabled="True" TargetControlID="btnRequestorDummy"   
                                                                        PopupControlID="pnlSearchClient" BackgroundCssClass="ModalBackground"   
                                                                        DropShadow="true" >  
                                                            </ajaxToolkit:ModalPopupExtender>  
            
            
<asp:Panel ID="pnlSearchClient" runat="server" style="display:none; background-color:#efefef;  font-size:large;  width:600px;  border: 2px solid #FFFFCC;">

    <asp:UpdatePanel ID = "uplSearchClient" runat="server" UpdateMode="Conditional" >
   
    
   <ContentTemplate>
      
                                            
                                                            <table  width="100%" border="1px" >
                                                                <tr>
                                                                    <th class="tableRowHeader" colspan = "4">
                                                                  Search for User
                                                                    </th>
                                                                </tr>
                                                             <tr>
                                                                <td>
                                                                   First Name <asp:TextBox ID = "FirstNameTextBox" runat = "server"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    Last Name <asp:TextBox ID = "LastNameTextBox" runat = "server"></asp:TextBox>
                                                                      <asp:Button ID ="btnSearch" runat = "server" Text ="Search"  />
                                                                </td>
       
  
                                                                
       
                                                            </tr>
                                                             
                                                            <tr>
                                                                <td colspan = "4">
                                                                <div class="scrollContentContainer" >
                                                                
                                                                 <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="200" AssociatedUpdatePanelID="uplSearchClient"  >

                                                                    <ProgressTemplate>

                                                                    <div id="IMGDIV" align="center" valign="middle" runat="server" style="visibility:visible;vertical-align:middle;">
                                                                     Loading Data ...
                   
                                                                   
                   
                                                                    </div>

                                                                    </ProgressTemplate>

                                                        </asp:UpdateProgress>
                                                        

                                                                
                                                                     <asp:GridView ID="gvSearch" runat="server" AllowSorting="True" 
                                                                                                AutoGenerateColumns="False" BackColor="White" BorderColor="#336666"
                                                                                                BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                                                                                                EmptyDataText="No Items Found" EnableTheming="False" DataKeyNames="client_num"
                                                                                                Font-Size="Medium" GridLines="Horizontal" Width="100%">
                                   
                                                                                                <FooterStyle BackColor="White" ForeColor="#333333" />
                                                                                                <HeaderStyle BackColor="#336666" Font-Bold="False" ForeColor="White" Font-Size="Small" HorizontalAlign="Left"    />                                    
                                                                                                <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Left" Font-Size="Medium"/>
                                                                                                <RowStyle BackColor="White" ForeColor="#333333" />
                                                                                                <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                                                                                <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                                                                                <SortedAscendingHeaderStyle BackColor="#487575" />
                                                                                                <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                                                                                <SortedDescendingHeaderStyle BackColor="#275353" />

                                                                                                <Columns>
                                                                                                    <asp:CommandField ButtonType="Button" SelectText="Select" ShowSelectButton= "true" />

                                         
                                                                                                      <asp:BoundField  DataField="fullname"  HeaderText="Full Name" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
                                                                                      <%--                <asp:BoundField  DataField="login_name"  HeaderText="Login"></asp:BoundField>--%>
                                                                                                      <asp:BoundField  DataField="phone"  HeaderText="Phone" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
                                                                                                      <asp:BoundField  DataField="department_name"  HeaderText="Department" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
				                          



                                  
                                                                                              </Columns>

                                                                                            </asp:GridView>
                                                                
                                                                        </div>
                                                                
                                                                </td>
                                                            
                                                            
                                                            </tr>
                                                          
                                                           
                                                               
                                                                <tr>
                                                                    <td colspan = "4" align = "center" >
                                                                    <asp:Button ID = "btnCloseSearchClient" runat = "server" Text="Close" />
                                                                    
                                                                  
                                                                   
                                                                    </td>
                                                                </tr>
                                
                                                            </table>

                                         </ContentTemplate>               
                              </asp:UpdatePanel>
                                 
                                </asp:Panel>

</asp:Content>

  
