﻿Imports System.IO
Imports System.Data '
Imports System.Data.SqlClient
Imports App_Code
Imports AjaxControlToolkit
Imports C1.Web.UI.Controls.C1Window

Partial Class SearchByEmpNumber
    Inherits MyBasePage '   Inherits System.Web.UI.Page
    Delegate Function getgridcellNoE(ByVal sFieldName As String, ByRef gridview1 As GridView) As Integer

    Dim getcellNoE As New getgridcellNoE(AddressOf GridViewFunctionsCls.GetGridCellIndexNoE)

    Dim conn As New SetDBConnection()
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim tp As New ToolTipHelper()
        EmployeeNumberTextBox.Focus()

        lblValidation.Attributes.Add("class", "masterTooltip")
        lblValidation.Attributes.Add("title", tp.ToolTipHtml())

        Dim ddlBinder As New DropDownListBinder

        If Not IsPostBack Then

            'ddlBinder.BindData(Departmentddl, "sel_department_codes", "department_cd", "department_name", Session("EmployeeConn"))

        End If
        If Session("SecurityLevelNum") < 25 Then
            Response.Redirect("~/Security.aspx")
        End If

        If Session("SecurityLevelNum") < 40 Then
            Response.Redirect("~/MyRequests.aspx")

        End If




    End Sub
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        lblValidation.Text = "Test"
        gvSearch.Visible = True
        gvSearch.SelectedIndex = -1
        gvRequests.Visible = True
        gvRequests.SelectedIndex = -1
        lblValidation.Visible = False


        ' conn.EmployeeConn
        Dim thisData As New CommandsSqlAndOleDb("SelClientWithVaccines", Session("VaccineConn"))


        If EmployeeNumberTextBox.Text <> "" Then
            thisData.AddSqlProcParameter("@employeenumber", EmployeeNumberTextBox.Text, SqlDbType.NVarChar, 20)

        ElseIf LastNameTextBox.Text <> "" Then
            thisData.AddSqlProcParameter("@FirstName", FirstNameTextBox.Text, SqlDbType.NVarChar, 20)
            thisData.AddSqlProcParameter("@LastName", LastNameTextBox.Text, SqlDbType.NVarChar, 20)
            'thisData.AddSqlProcParameter("@System", rblSystem.SelectedValue, SqlDbType.NVarChar, 20)
            'thisData.AddSqlProcParameter("@NtLogin", NtLoginTextBox.Text, SqlDbType.NVarChar, 20)

        End If

        gvSearch.DataSource = thisData.GetSqlDataTable
        gvSearch.DataBind()

        Dim rowsfound As Integer
        Dim client_num As String

        rowsfound = gvSearch.Rows.Count

        gvRequests.Visible = False

        If EmployeeNumberTextBox.Text <> "" Then

            If rowsfound = 1 Then
                For i = 0 To gvSearch.Rows.Count - 1
                    If gvSearch.Rows(i).Cells(2).Text <> "" Then
                        gvSearch.SelectedIndex = i
                        client_num = gvSearch.SelectedRow.Cells(2).Text
                        Exit For
                    End If
                Next

                If client_num <> "" Then
                    Response.Redirect("~/EmployeeVaccine.aspx?ClientNum=" & client_num, True)

                End If

                'client_num = gvSearch.SelectedRow.Cells(0).Text
                'client_num = gvSearch.SelectedRow.Cells("client_num").ToString
            End If

        End If



    End Sub
    Protected Sub gvSearch_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles gvSearch.SelectedIndexChanged
        Dim client_num As String

        gvSearch.SelectedIndex = 1

        client_num = gvSearch.SelectedRow.Cells(0).Text
        client_num = gvSearch.SelectedRow.Cells("client_num").ToString

        Response.Redirect("~/EmployeeVaccine.aspx?ClientNum=" & client_num, True)

    End Sub

    Protected Sub gvSearch_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvSearch.RowCommand
        If e.CommandName = "Select" Then

            Dim client_num As String = gvSearch.DataKeys(Convert.ToInt32(e.CommandArgument))("client_num").ToString()
            Dim VaccineNum As String = gvSearch.DataKeys(Convert.ToInt32(e.CommandArgument))("VaccineNum").ToString()

            Response.Redirect("~/EmployeeVaccine.aspx?ClientNum=" & client_num & "&VaccineNum=" & VaccineNum, True)

        End If

    End Sub

    Protected Sub gvRequests_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvRequests.RowCommand
        Dim account_request_num As String

        account_request_num = gvRequests.DataKeys(Convert.ToInt32(e.CommandArgument))("account_request_num").ToString()

        Response.Redirect("~/AccountRequestInfo2.aspx?RequestNum=" & account_request_num, True)

    End Sub
    Protected Sub gvSearch_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvSearch.RowDataBound

        If (e.Row.RowType = DataControlRowType.DataRow) Then  ' Check to make sure it isn't not getting header or footer

            '   Dim drv As DataRowView = CType(e.Row.Cells, DataRowView)

            Dim ClientNum As String = gvSearch.DataKeys(e.Row.RowIndex)("client_num")
            Dim lblUserName As Label

            lblUserName = e.Row.FindControl("lblLoginName")
            Dim accts As New ClientAccounts(ClientNum)
            If accts.AccountsByAcctNum(9).Count > 1 Then
                Dim sAccts As String = ""

                For Each acct As ClientAccountAccess In accts.AccountsByAcctNum(9)

                    sAccts = sAccts + acct.LoginName + "<br>"


                Next

                lblUserName.Attributes.Add("class", "masterTooltip")
                lblUserName.Attributes.Add("title", sAccts)
                lblUserName.ForeColor = Color.Blue
            End If

        End If

    End Sub
    Public Sub Reset()
        ' clear out all fields
        EmployeeNumberTextBox.Text = ""
        EmployeeNumberTextBox.Focus()

        Dim emptytbl As New DataTable

        gvSearch.Visible = False
        gvSearch.SelectedIndex = -1
        gvRequests.Visible = False
        gvRequests.SelectedIndex = -1

        gvSearch.DataSource = emptytbl
        gvSearch.DataBind()

        gvRequests.DataSource = emptytbl
        gvRequests.DataBind()


    End Sub
    Protected Sub btnReset_Click(sender As Object, e As System.EventArgs) Handles btnReset.Click
        Reset()
    End Sub

    Protected Sub btnNotFound_Click(sender As Object, e As System.EventArgs) Handles btnNotFound.Click
        Response.Redirect("~/EmployeeVaccine.aspx?NotFound=" & "no", True)

    End Sub
End Class
