﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="false" CodeFile="TechGroupMaintenance.aspx.vb" Inherits="TechGroupMaintenance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <style type="text/css">
        .style2
        {
            text-align: center;
            background-color: #FFFFCC;
            color: #336666;
            font-weight: bold;
            font-size: x-large;
            height: 43px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:UpdatePanel ID="uplPanel" runat ="server"  UpdateMode="Conditional">
    <Triggers>
    </Triggers>
   <ContentTemplate>
   <div id="maindiv" runat="server" align="left">
    <table  style="border-style: groove" align="left" width="100%">
        <tr align="center">
           <td class="tableRowHeader">
                  CSC Technicians Group Maintenance
           </td>
        </tr>
        <tr>
          <td align="left">
             <ajaxToolkit:TabContainer ID="tabs" runat="server" ActiveTabIndex="0"
                Width="100%"  EnableTheming="False" 
                BackColor="#efefef">

                  <ajaxToolkit:TabPanel ID="tpAllGroups" runat="server"  BackColor="Gainsboro" Visible = "True" EnableTheming="True">
                    <HeaderTemplate > 
                        <asp:Label ID="Label4" runat="server" Text="Current Tech Groups" Font-Bold="true" ForeColor="Black"></asp:Label>
                    </HeaderTemplate>
                      <ContentTemplate>

                       <table border="1" width="100%"  style="empty-cells:show" >
                      
                         <tr>
                           <td>
                                <asp:GridView ID="gvTechGroups" runat="server" AllowSorting="True" 
                               AutoGenerateColumns="False" BackColor="White" BorderColor="#336666" 
                               BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                               DataKeyNames="group_num,displayDesc,group_name,displaystatus,defaultCategoryCD"
                               EmptyDataText="No Tech Groups" 
                               EnableTheming="False" Font-Size="Medium" GridLines="Horizontal" Width="100%">
                               <FooterStyle BackColor="White" ForeColor="#333333" />
                               <HeaderStyle BackColor="#336666" Font-Bold="False" Font-Size="Small" 
                                   ForeColor="White" HorizontalAlign="Left" />
                               <PagerStyle BackColor="#336666" Font-Size="Medium" ForeColor="White" 
                                   HorizontalAlign="Left" />
                               <RowStyle BackColor="White" ForeColor="#333333" />
                               <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                               <SortedAscendingCellStyle BackColor="#F7F7F7" />
                               <SortedAscendingHeaderStyle BackColor="#487575" />
                               <SortedDescendingCellStyle BackColor="#E5E5E5" />
                               <SortedDescendingHeaderStyle BackColor="#275353" />
                               <Columns>

                                   <asp:CommandField ButtonType="Button" ControlStyle-BackColor="#84A3A3" 
                                          HeaderStyle-HorizontalAlign="Left" ItemStyle-Height="30px" SelectText="Select" 
                                          ShowSelectButton="true">
                                      <ControlStyle BackColor="#84A3A3" />
                                      <HeaderStyle HorizontalAlign="Left" />
                                      <ItemStyle Height="30px" />
                                      </asp:CommandField>
                            

                                   <asp:BoundField DataField="group_name" HeaderText="Name" />

                                   <asp:BoundField DataField="displayDesc" HeaderText="Display Status " />
                                   <asp:BoundField DataField="displaystatus" Visible="false" />

                                   


                               </Columns>
                           </asp:GridView>
                           </td>
                         </tr>
                        </table>
                      </ContentTemplate>
                </ajaxToolkit:TabPanel>

                 <ajaxToolkit:TabPanel ID="tpNewGroup" runat="server" BackColor="Gainsboro" Visible = "True" EnableTheming="True" TabIndex="1">
                       <HeaderTemplate > 
                          <asp:Label ID="Label3" runat="server" Text="Add New Tech Group" Font-Bold="true" ForeColor="Black" ></asp:Label>
                     </HeaderTemplate>
                      <ContentTemplate>
                       <table border="1" width="100%"  style="empty-cells:show" >
                         <tr>
                              <td colspan="2" align="center">
                               Fill in Group Name
          
                              </td>
                        </tr>
                         <tr>
                            <td colspan="2" align="center">
                                <asp:TextBox ID="group_nameTextBox" Width="200px" runat = "server"></asp:TextBox>
                            </td>

                         </tr>
                         <tr>
                            <td colspan="2" align="center">
                                    Default Category:
                                    <asp:DropDownList ID="Newcategorycdddl" runat="server" AutoPostBack="true">
                                    </asp:DropDownList>
                                    <asp:Label ID="NewDefaultCategoryCD" runat="server" Visible="false"></asp:Label>

                            </td>
                         </tr>
                         <tr>
                            <td colspan="2" align="center">
                            
                                On Call Queue/CSC Group Queue or Both
                                <asp:CheckBoxList id="DisplayStatuschb" runat="server"  RepeatDirection="Horizontal">
                                    <asp:ListItem value= "oncallonly" Text="On Call Queue"></asp:ListItem>
                                    <asp:ListItem value="queueonly"  Text="Display for Tickets/RFS in CSC "></asp:ListItem>
                                </asp:CheckBoxList>
                                 <asp:Label ID="HDGroupStatus" runat="server" Visible="false"></asp:Label>

                            </td>
                            
                         </tr>

                          <tr>
                                <td align="center">

                                        <asp:Button ID ="BtnAddGroup" runat = "server" Text ="Submit"  Width="120px" Height="30px"
                                             Font-Names="Arial" ForeColor="White" Font-Bold="True" Font-Size="Small" />
                                </td>
                                <td align="center" >

                                        <asp:Button ID = "BtnAddReset" runat= "server" Text="Reset"  Width="120px" Height="30px"
                                             Font-Names="Arial" ForeColor="White" Font-Bold="True" Font-Size="Small" />

                                </td>
                          </tr>

                        </table>
                       </ContentTemplate>
                 </ajaxToolkit:TabPanel>

                 <ajaxToolkit:TabPanel ID="tpGroup" runat="server" BackColor="Gainsboro" Visible = "false" EnableTheming="True" TabIndex="2">
                       <HeaderTemplate > 
                          <asp:Label ID="Label1" runat="server" Text="Tech Group" Font-Bold="true" ForeColor="Black" ></asp:Label>
                     </HeaderTemplate>
                      <ContentTemplate>
                       <table border="1" width="100%"  style="empty-cells:show" >
                         <tr>
                              <td colspan="2" align="center">
                                Group Name
          
                              </td>
                        </tr>
                         <tr>
                            <td  align="center">
                                <asp:TextBox ID="GroupNameTextBox2" Width="200px" runat = "server"></asp:TextBox>
                                <asp:TextBox ID="groupnumtextbox" Visible="false" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                        <asp:Label ID="defcatlbl" runat="server" Text = "Default Category:"></asp:Label>

                                    <asp:DropDownList ID="categorycdddl" runat="server" AutoPostBack="true">
                                    </asp:DropDownList>
                                    <asp:Label ID="defaultcategorycd" runat="server" Visible="false"></asp:Label>
                            </td>

                         </tr>

                         <tr>
                            <td colspan="2" align="center">
                            
                                On Call Queue/CSC Group Queue or Both
                                <asp:CheckBoxList id="GroupCheckBox" runat="server" RepeatDirection="Horizontal" >

                                    <asp:ListItem value= "oncallonly" Text="On Call Queue"></asp:ListItem>
                                    <asp:ListItem value="queueonly"  Text="CSC Group Queue"></asp:ListItem>

                                </asp:CheckBoxList>
                            </td>
                            
                         </tr>

                         <tr>
                            <td colspan="2" align="center" >
                                Deactivate Group
                                    <asp:RadioButtonList ID="deactivaterbl" runat="server" RepeatDirection="Horizontal">
                                        <asp:ListItem Value="no" Selected="True"> No </asp:ListItem>
                                        <asp:ListItem Value="yes"> Yes </asp:ListItem>
                                    </asp:RadioButtonList>

                            </td>
                         </tr>
                          <tr>
                                <td align="center">

                                        <asp:Button ID ="btnUpdateGroup" runat = "server" Text ="Update"  Width="120px" Height="30px"
                                             Font-Names="Arial" ForeColor="White" Font-Bold="True" Font-Size="Small" />
                                </td>
                                <td align="center" >

                                        <asp:Button ID = "btnReturn" runat= "server" Text="Return"  Width="120px" Height="30px"
                                             Font-Names="Arial" ForeColor="White" Font-Bold="True" Font-Size="Small" />

                                </td>
                          </tr>

                        </table>
                       </ContentTemplate>
                 </ajaxToolkit:TabPanel>

         </ajaxToolkit:TabContainer>
        </td>
     </tr>
  </table>
  </div>
   </ContentTemplate>
  </asp:UpdatePanel>
</asp:Content>

