﻿Imports System.IO
Imports System.Data '
Imports System.Data.SqlClient
Imports App_Code

Partial Class RequestReport
    Inherits System.Web.UI.Page
    Dim ReportTotalList As New DataTable


    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        Chart1.Series("ChartSeries").XValueMember = "ThisYear"
        Chart1.Series("ChartSeries").YValueMembers = "GrandTotalPerYear"

        Dim thisdata As New CommandsSqlAndOleDb("SelGrandTotalAccountRequestV2", Session("EmployeeConn"))

        thisdata.ExecNonQueryNoReturn()
        ReportTotalList = thisdata.GetSqlDataTable

        ViewState("dtTotalRequest") = thisdata.GetSqlDataTable

        CurrentTotals()

        Chart1.DataSource = ReportTotalList
        Chart1.DataBind()

    End Sub
    Protected Sub CurrentTotals()

        grdTotal.DataSource = ViewState("dtTotalRequest")
        grdTotal.DataBind()

        'gvRemoveAccounts.DataSource = ViewState("dtRemoveAccounts")
        'gvRemoveAccounts.DataBind()

    End Sub

    Protected Sub grdTotal_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdTotal.RowCommand
        If e.CommandName = "Select" Then


            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim iThisyear As Integer = grdTotal.DataKeys(e.CommandArgument)("ThisYear")

            Response.Redirect("~/" & "RequestDetail.aspx?iYear=" & iThisyear, True)

        End If

    End Sub
End Class
