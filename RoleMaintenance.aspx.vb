﻿Imports System.IO
Imports System.Data '
Imports System.Data.SqlClient
Imports App_Code
Imports AjaxControlToolkit
Imports C1.Web.UI.Controls.C1Window

Partial Class RoleMaintenance
    Inherits MyBasePage '   Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        If Not IsPostBack Then

            Dim ddlBinder As New DropDownListBinder

            'cblAddRole

            Dim cblBinder As New CheckBoxListBinder(cblAddRole, "SelApplicationCodes", Session("EmployeeConn"))
            cblBinder.AddDDLCriteria("@AppSystem", "base", SqlDbType.NVarChar)
            cblBinder.BindData("ApplicationNum", "ApplicationDesc")

            Dim cblBinder2 As New CheckBoxListBinder(cblUpdateRoles, "SelApplicationCodes", Session("EmployeeConn"))
            cblBinder2.AddDDLCriteria("@AppSystem", "base", SqlDbType.NVarChar)
            cblBinder2.BindData("ApplicationNum", "ApplicationDesc")

            ddlBinder.BindData(EmployeeTitlesddl, "SelEmployeeRolesForAdding", "RoleNum", "Role", Session("EmployeeConn"))
            ddlBinder.BindData(Rolesddl, "SelEmployeeRoles", "Rolenum", "Role", Session("EmployeeConn"))

        End If

        If lblRolenum.Text <> "" Then
            EmployeeTitlesddl.SelectedValue = lblRolenum.Text
        End If

    End Sub


    Protected Sub btnAddRole_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddRole.Click
        For Each cblItem As ListItem In cblAddRole.Items

            If cblItem.Selected = True Then



                Dim thisdata As New CommandsSqlAndOleDb("InsAppRole", Session("EmployeeConn"))
                thisdata.AddSqlProcParameter("@roleNum", EmployeeTitlesddl.SelectedValue, SqlDbType.Int, 20)
                thisdata.AddSqlProcParameter("@ApplicationNum", cblItem.Value, SqlDbType.Int, 8)
                thisdata.ExecNonQueryNoReturn()


            End If

        Next
        lblAddRole.Text = "Role Created"


        Dim ddlBinder As New DropDownListBinder
        ddlBinder.BindData(Rolesddl, "SelEmployeeRolesV2", "Rolenum", "Role", Session("EmployeeConn"))
        ddlBinder.BindData(EmployeeTitlesddl, "SelEmployeeRolesForAdding", "RoleNum", "Role", Session("EmployeeConn"))

        Dim cblBinder As New CheckBoxListBinder(cblAddRole, "SelApplicationCodes", Session("EmployeeConn"))
        cblBinder.AddDDLCriteria("@AppSystem", "base", SqlDbType.NVarChar)
        cblBinder.BindData("ApplicationNum", "ApplicationDesc")

        Dim cblBinder2 As New CheckBoxListBinder(cblUpdateRoles, "SelApplicationCodes", Session("EmployeeConn"))
        cblBinder2.AddDDLCriteria("@AppSystem", "base", SqlDbType.NVarChar)
        cblBinder2.BindData("ApplicationNum", "ApplicationDesc")


    End Sub

    Protected Function getApplicationsByRole() As DataTable
        Dim thisdata As New CommandsSqlAndOleDb("SelApplicationsByRole", Session("EmployeeConn"))
        thisdata.AddSqlProcParameter("@roleNum", Rolesddl.SelectedValue, SqlDbType.Int, 20)

        thisdata.ExecNonQueryNoReturn()

        Return thisdata.GetSqlDataTable

    End Function



    Protected Sub Rolesddl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Rolesddl.SelectedIndexChanged


        Dim rwRef() As DataRow

        For Each cblItem As ListItem In cblUpdateRoles.Items
            cblItem.Selected = False


            rwRef = getApplicationsByRole.Select("ApplicationNum='" & cblItem.Value & "'")
            If rwRef.Count > 0 Then

                cblItem.Selected = True

            End If

        Next

        btnUpdateRole.Visible = True
        btnAddRole.Visible = False

        lblUpdateRole.Text = ""

    End Sub
    Protected Sub EmployeeTitlesddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles EmployeeTitlesddl.SelectedIndexChanged
        'lblRolenum.Text = EmployeeTitlesddl.SelectedValue

        For Each cblItem As ListItem In cblAddRole.Items
            cblItem.Selected = False

        Next

        lblAddRole.Text = ""
        btnUpdateRole.Visible = False
        btnAddRole.Visible = True

    End Sub


    Protected Sub btnUpdateRole_Click(sender As Object, e As System.EventArgs) Handles btnUpdateRole.Click
        Dim rwRef() As DataRow

        For Each cblItem As ListItem In cblUpdateRoles.Items
            rwRef = getApplicationsByRole.Select("ApplicationNum='" & cblItem.Value & "'")
            If rwRef.Count > 0 And cblItem.Selected = False Then

                Dim thisdata As New CommandsSqlAndOleDb("UpdAppRole", Session("EmployeeConn"))
                thisdata.AddSqlProcParameter("@roleNum", Rolesddl.SelectedValue, SqlDbType.Int, 20)
                thisdata.AddSqlProcParameter("@Deacivate", "yes", SqlDbType.NVarChar, 6)
                thisdata.AddSqlProcParameter("@ApplicationNum", cblItem.Value, SqlDbType.Int, 8)
                thisdata.ExecNonQueryNoReturn()

            End If
            If rwRef.Count = 0 And cblItem.Selected = True Then


                Dim thisdata As New CommandsSqlAndOleDb("InsAppRole", Session("EmployeeConn"))
                thisdata.AddSqlProcParameter("@roleNum", Rolesddl.SelectedValue, SqlDbType.Int, 20)
                thisdata.AddSqlProcParameter("@ApplicationNum", cblItem.Value, SqlDbType.Int, 8)
                thisdata.ExecNonQueryNoReturn()



            End If

        Next

        lblUpdateRole.Text = "Role Updated"

        Dim ddlBinder As New DropDownListBinder
        ddlBinder.BindData(Rolesddl, "SelEmployeeRoles", "Rolenum", "Role", Session("EmployeeConn"))
        ddlBinder.BindData(EmployeeTitlesddl, "SelEmployeeRolesForAdding", "RoleNum", "Role", Session("EmployeeConn"))

        Dim cblBinder As New CheckBoxListBinder(cblAddRole, "SelApplicationCodes", Session("EmployeeConn"))
        cblBinder.AddDDLCriteria("@AppSystem", "base", SqlDbType.NVarChar)
        cblBinder.BindData("ApplicationNum", "ApplicationDesc")

        Dim cblBinder2 As New CheckBoxListBinder(cblUpdateRoles, "SelApplicationCodes", Session("EmployeeConn"))
        cblBinder2.AddDDLCriteria("@AppSystem", "base", SqlDbType.NVarChar)
        cblBinder2.BindData("ApplicationNum", "ApplicationDesc")

    End Sub

End Class
