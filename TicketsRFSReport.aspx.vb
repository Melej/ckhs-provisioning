﻿Imports System.IO
Imports System.Data '
Imports System.Data.SqlClient

Imports App_Code
Imports AjaxControlToolkit

'Imports iTextSharp.text
'Imports iTextSharp.text.pdf

Partial Class TicketsRFSReport
    Inherits MyBasePage 'Inherits System.Web.UI.Page '
    Dim dsQueues As New DataSet("Queues")
    Dim cblBinder As New CheckBoxListBinder

    'Dim Appinfo As New Applications
    'Dim Apps As New Applications

    Dim BaseApps As New List(Of Application)
    Dim NewBaseApps As New List(Of AllMyApplications)

    Dim AllGroupQueuesInfo As New GroupQueues
    Dim AllMyGroupInfo As New GroupQueues

    'GetAllGroupQueues

    Dim changeGroup As String

    Dim allgrouplist As New List(Of AllGroupQueues)
    'Dim MyGrouplist As New List(Of AllMyGroups)
    Dim GroupOwnership As New List(Of String)


    Dim TicketRFSReportlistC As New TicketsRFSReportClass()
    Dim conn As New SetDBConnection()
    Dim sAccountType As String
    Dim technum As Integer
    Dim GroupNumber As Integer = 0
    Dim Groups As String = ""




    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        Dim Security As New UserSecurity(Session("LoginID"), Session("EmployeeConn"))
        Dim GroupOwnership As List(Of Integer)
        Dim SGroupOwnership As List(Of String)

        technum = Security.TechNum
        hdtechnumLabel.Text = Security.TechNum.ToString

        GroupOwnership = Security.MySecGroups
        SGroupOwnership = Security.MySecGroupString


        If Not IsPostBack Then

            ' returns a list of applications User checked to view



            ' this returns all applications only base numbers not individual sites 31

            AllGroupQueuesInfo.GetAllGroupQueues(HttpContext.Current.Session("CSCConn").ToString, "queue")

            'allgrouplist.Clear()

            allgrouplist = AllGroupQueuesInfo.AllGroups

            cblGroupAccountQueues.Dispose()

            cblGroupAccountQueues.DataSource = allgrouplist

            cblGroupAccountQueues.DataValueField = "GroupNum"
            cblGroupAccountQueues.DataTextField = "GroupName"
            cblGroupAccountQueues.DataBind()


            AllMyGroupInfo.GetMyQueues(HttpContext.Current.Session("CSCConn").ToString, technum)


            Dim checkBoxes = From it As ListItem In cblGroupAccountQueues.Items
                 Join own As String In GroupOwnership On it.Value Equals own.ToString
                 Select it
            For Each itm In checkBoxes
                itm.Selected = True
            Next


            If txtStartDate.Text = "" Then
                txtStartDate.Text = DateAdd(DateInterval.Day, -30, Date.Today)
                HDStartDate.Text = DateAdd(DateInterval.Day, -30, Date.Today)
            Else
                HDStartDate.Text = txtStartDate.Text
            End If

            If txtEndDate.Text = "" Then
                txtEndDate.Text = Date.Today
                HDEnddate.Text = Date.Today
            Else
                HDEnddate.Text = txtEndDate.Text
            End If

            If rblStatus.SelectedValue = "" Then
                rblStatus.SelectedValue = "both"
                hdStatus.Text = "both"

            Else
                hdStatus.Text = rblStatus.SelectedValue

            End If

            If rbltype.SelectedValue = "" Then
                rbltype.SelectedValue = "both"
                hdType.Text = "both"


            Else
                hdType.Text = rbltype.SelectedValue

            End If


            If Session("ShowAll") = "Show" Then
                For Each ListItem As ListItem In cblGroupAccountQueues.Items
                    ListItem.Selected = True
                Next

                btnShowAll.Text = "UnCheck All"
                lblCurrentView.Text = "Now Showing All Groups from " & txtStartDate.Text & " to " & txtEndDate.Text

                'If Session("ViewRequestType") = "closed" Then
                '    lblCurrentView.Text = "Now Showing All Groups"
                'Else
                '    lblCurrentView.Text = "Now Showing All Your Groups"
                'End If
            Else
                lblCurrentView.Text = "Now Showing Selected Groups from " & txtStartDate.Text & " to " & txtEndDate.Text

                lblCurrentView.Text = " Select criteria and then Submit "


                'If Session("ViewRequestType") = "closed" Then
                '    lblCurrentView.Text = "Now Showing Your Queues with closed Cases"
                'Else
                '    lblCurrentView.Text = "Now Showing Your Queues with Open Cases"
                'End If

            End If

            'TicketRFSReportlist = New TicketsRFS(txtStartDate.Text, txtEndDate.Text, Groups, rblStatus.SelectedValue, rbltype.SelectedValue, HttpContext.Current.Session("CSCConn").ToString)
            'GetQueueDataSet()
        Else
            lblCurrentView.Text = ""

            If Session("ShowAll") = "Show" Then
                lblCurrentView.Text = "Now Showing All Groups  from " & txtStartDate.Text & " to " & txtEndDate.Text

            Else
                lblCurrentView.Text = "Now Showing Selected Groups  from " & txtStartDate.Text & " to " & txtEndDate.Text

            End If

        End If

        tblGroupAccountQueues.Width = New Unit("100%")
        tblGroupAccountQueues.Visible = True

    End Sub

    Public Sub GetQueueDataSet()

        If rblStatus.SelectedValue = "both" Then
            lblCurrentView.Text = lblCurrentView.Text & " Open and Closed "
        Else
            lblCurrentView.Text = lblCurrentView.Text & " Only " & rblStatus.SelectedValue

        End If

        If rbltype.SelectedValue = "both" Then
            lblCurrentView.Text = lblCurrentView.Text & " Tickets and RFS "

        Else
            lblCurrentView.Text = lblCurrentView.Text & " " & rbltype.SelectedValue

        End If

        'AllGroupQueuesInfo.GetAllGroupQueues(HttpContext.Current.Session("CSCConn").ToString)

        'allgrouplist.Clear()

        'allgrouplist = AllGroupQueuesInfo.AllGroups


        TicketRFSReportlistC = New TicketsRFSReportClass(HDStartDate.Text, HDEnddate.Text, Groups, hdStatus.Text, hdType.Text, HttpContext.Current.Session("CSCConn").ToString, hdPriority.Text)

        Dim appHeaderCreated As Boolean = False
        Dim Checkitem As Integer

        ' for each of the 31 base applications 
        For Each cblItem As ListItem In cblGroupAccountQueues.Items
            ' if the app is checked then 
            If cblItem.Selected = True Then


                Dim iRowCount As Integer = 1

                'now for each Category that are checked
                'For Each app As AllMyGroups In AllMyGroupInfo.GroupByGroupNumber(cblItem.Value)
                'For Each groupn As AllGroupQueues In AllGroupQueuesInfo.AllGroupByGroupNumber(cblItem.Value)
                '.GroupByGroupNumber(cblItem.Value)
                'AllGroupQueuesInfo.AllGroups AllGroupQueues

                appHeaderCreated = False
                Checkitem = cblItem.Value

                'If Session("GroupNum") <> "" Then
                '    Checkitem = Session("GroupNum")
                'End If

                For Each ari As TickectRFSReportItems In TicketRFSReportlistC.GetAllRequestByGroup(cblItem.Value) 'app.GroupNum.OrderByDescending(Function(x) x.AccountRequestNum)

                    If appHeaderCreated = False Then

                        'tblAccountQueues.Rows.Add(rowjeff)

                        Dim rwHeader As New TableRow
                        Dim rwColTitle As New TableRow

                        Dim clHeader As New TableCell
                        Dim clReqNumTitle As New TableCell
                        Dim clclientNameTitle As New TableCell
                        Dim clRequestTypeTitle As New TableCell

                        Dim clentrydateTitle As New TableCell
                        Dim clclosedateTitle As New TableCell
                        Dim clcategoryTitle As New TableCell
                        'Dim cltypeTitle As New TableCell
                        Dim clpriorityTitle As New TableCell
                        'Dim clGroupNameTitle As New TableCell
                        Dim clTechNameTitle As New TableCell
                        'Dim cltypeTitle As New TableCell

                        rwHeader.Font.Bold = True
                        rwHeader.BackColor = Color.FromName("#FFFFCC")
                        rwHeader.ForeColor = Color.FromName("Black")
                        rwHeader.Font.Size = 8

                        clHeader.ColumnSpan = 9

                        'If Session("ViewRequestType") = "open" Then
                        '    clHeader.Text = ari.GroupName & " Group "

                        'ElseIf Session("ViewRequestType") = "closed" Then
                        '    clHeader.Text = ari.GroupName & " Closed Cases "
                        'Else
                        '    clHeader.Text = ari.RequestTypeDesc & " PAST 30 Days Invalid Requests"
                        'End If

                        clHeader.Text = ari.GroupName

                        rwHeader.Cells.Add(clHeader)

                        tblGroupAccountQueues.Rows.Add(rwHeader)


                        rwColTitle.Font.Bold = True
                        rwColTitle.BackColor = Color.FromName("#006666")
                        rwColTitle.ForeColor = Color.FromName("White")
                        rwColTitle.Height = Unit.Pixel(36)

                        clReqNumTitle.Text = "Ticket/RFS #"
                        clclientNameTitle.Text = "Client Name"
                        clRequestTypeTitle.Text = "Req. Type"
                        clentrydateTitle.Text = "Entry Date"
                        clentrydateTitle.Width = Unit.Pixel(55)
                        clclosedateTitle.Text = "Close Date"
                        clclosedateTitle.Width = Unit.Pixel(55)
                        clcategoryTitle.Text = "Category"
                        'cltypeTitle.Text = "Cat. Type"
                        clpriorityTitle.Text = "Priority"
                        'clGroupNameTitle.Text = "Group Name"
                        clTechNameTitle.Text = "Tech Name"


                        rwColTitle.Cells.Add(clReqNumTitle)
                        rwColTitle.Cells.Add(clclientNameTitle)
                        rwColTitle.Cells.Add(clentrydateTitle)
                        rwColTitle.Cells.Add(clclosedateTitle)

                        rwColTitle.Cells.Add(clRequestTypeTitle)
                        rwColTitle.Cells.Add(clcategoryTitle)
                        'rwColTitle.Cells.Add(cltypeTitle)


                        rwColTitle.Cells.Add(clpriorityTitle)
                        'rwColTitle.Cells.Add(clGroupNameTitle)
                        rwColTitle.Cells.Add(clTechNameTitle)

                        rwColTitle.Font.Size = 8
                        tblGroupAccountQueues.Rows.Add(rwColTitle)

                        appHeaderCreated = True
                    End If
                    '----------------------------------------
                    ' End Header
                    '----------------------------------------

                    'requestnum
                    'clientName
                    'entrydate
                    'closedate
                    'request_type
                    'category_cd
                    'type_cd
                    'priority
                    'GroupNum
                    'GroupName
                    'TechName
                    'status_desc
                    'bDesc


                    Dim rwData As New TableRow
                    Dim clReqNum As New TableCell
                    Dim clClientName As New TableCell
                    Dim clEntryDate As New TableCell
                    Dim clCloseDate As New TableCell
                    Dim clReqType As New TableCell
                    Dim clCategory As New TableCell
                    ' Dim clCatType As New TableCell

                    Dim clpriority As New TableCell
                    'Dim clGroupName As New TableCell
                    Dim clTechName As New TableCell


                    If Session("ViewRequestType") = "" Then
                        Session("ViewRequestType") = "open"
                    End If

                    'If Session("ViewRequestType") = "open" Then
                    '    clReqNum.Text = "<a href=""AccountRequestInfo2.aspx?requestnum=" & ari.HRRequestNum & """ ><u><b> " & ari.HRRequestNum & "</b></u></a>"
                    'ElseIf Session("ViewRequestType") = "closed" Then
                    '    clReqNum.Text = "<a href=""AccountRequestInfo2.aspx?requestnum=" & ari.HRRequestNum & """ ><u><b> " & ari.HRRequestNum & "</b></u></a>"
                    'End If

                    clReqNum.Text = "<a href=""ViewRequestTicket.aspx?RequestNum=" & ari.request_num & """ ><u><b> " & ari.request_num & "</b></u></"
                    'clReqNum.Text = ari.request_num

                    clTechName.Text = ari.TechName
                    clClientName.Text = ari.clientName
                    clEntryDate.Text = ari.entry_date

                    clCloseDate.Text = ari.close_date

                    clReqType.Text = ari.request_type
                    clCategory.Text = ari.category_cd
                    'clCatType.Text = ari.type_cd
                    clpriority.Text = ari.priority
                    'clGroupName.Text = ari.GroupName
                    clTechName.Text = ari.TechName


                    rwData.Cells.Add(clReqNum)
                    rwData.Cells.Add(clClientName)
                    rwData.Cells.Add(clEntryDate)

                    rwData.Cells.Add(clCloseDate)
                    rwData.Cells.Add(clReqType)
                    rwData.Cells.Add(clCategory)

                    'rwData.Cells.Add(clCatType)
                    rwData.Cells.Add(clpriority)
                    'rwData.Cells.Add(clGroupName)
                    rwData.Cells.Add(clTechName)
                    rwData.Font.Size = 7


                    If iRowCount > 0 Then

                        ' rwData.BackColor = Color.Bisque

                        rwData.BackColor = Color.WhiteSmoke

                    Else

                        rwData.BackColor = Color.LightGray

                    End If

                    tblGroupAccountQueues.Rows.Add(rwData)


                    iRowCount = iRowCount * -1

                Next
                ' Next
            End If
        Next
        tblGroupAccountQueues.Width = New Unit("100%")
        tblGroupAccountQueues.Visible = True



    End Sub

    Protected Sub btnShowAll_Click(sender As Object, e As System.EventArgs) Handles btnShowAll.Click
        If Session("ShowAll") = "Show" Then
            Session("ShowAll") = ""
        Else
            Session("ShowAll") = "Show"
        End If
        Response.Redirect("~/" & "TicketsRFSReport.aspx")
    End Sub
    Protected Sub cblGroupAccountQueues_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cblGroupAccountQueues.SelectedIndexChanged

        ' this returns all applications only base numbers not individual sites 31

        'AllGroupQueuesInfo.GetAllGroupQueues(HttpContext.Current.Session("CSCConn").ToString)

        'allgrouplist.Clear()

        'allgrouplist = AllGroupQueuesInfo.AllGroups


        'TicketRFSReportlist = New TicketsRFS(HDStartDate.Text, HDEnddate.Text, GroupNumber, hdStatus.Text, hdType.Text, HttpContext.Current.Session("CSCConn").ToString)


        'GetQueueDataSet()

    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As System.EventArgs) Handles btnSubmit.Click

        Groups = ""
        hdGroups.Text = ""


        For Each cblItem As ListItem In cblGroupAccountQueues.Items
            If cblItem.Selected = True Then
                Groups = Groups & "," & CType(cblItem.Value, String)
                hdGroups.Text = hdGroups.Text & "," & CType(cblItem.Value, String)
            End If

        Next


        lblCurrentView.Text = ""

        If Session("ShowAll") = "Show" Then

            lblCurrentView.Text = "Now Showing All Groups from " & txtStartDate.Text & " to " & txtEndDate.Text
        Else
            lblCurrentView.Text = "Now Showing Selected Groups from " & txtStartDate.Text & " to " & txtEndDate.Text

        End If


        AllGroupQueuesInfo.GetAllGroupQueues(HttpContext.Current.Session("CSCConn").ToString, "queue")

        allgrouplist.Clear()

        allgrouplist = AllGroupQueuesInfo.AllGroups

        'TicketRFSReportlist = New TicketsRFS(HDStartDate.Text, HDEnddate.Text, Groups, hdStatus.Text, hdType.Text, HttpContext.Current.Session("CSCConn").ToString)



        GetQueueDataSet()


    End Sub

    Protected Sub rbltype_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles rbltype.SelectedIndexChanged
        hdType.Text = rbltype.SelectedValue
        '' this returns all applications only base numbers not individual sites 31

        'AllGroupQueuesInfo.GetAllGroupQueues(HttpContext.Current.Session("CSCConn").ToString)

        'allgrouplist.Clear()

        'allgrouplist = AllGroupQueuesInfo.AllGroups


        'TicketRFSReportlist = New TicketsRFS(HDStartDate.Text, HDEnddate.Text, GroupNumber, hdStatus.Text, hdType.Text, HttpContext.Current.Session("CSCConn").ToString)


        ''rbltype.SelectedIndex = -1
        'GetQueueDataSet()

    End Sub

    Protected Sub rblPriority_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles rblPriority.SelectedIndexChanged
        hdPriority.Text = rblPriority.SelectedValue

    End Sub

    Protected Sub rblStatus_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles rblStatus.SelectedIndexChanged

        hdStatus.Text = rblStatus.SelectedValue

        '' this returns all applications only base numbers not individual sites 31

        'AllGroupQueuesInfo.GetAllGroupQueues(HttpContext.Current.Session("CSCConn").ToString)

        'allgrouplist.Clear()

        'allgrouplist = AllGroupQueuesInfo.AllGroups


        'TicketRFSReportlist = New TicketsRFS(HDStartDate.Text, HDEnddate.Text, Groups, hdStatus.Text, hdType.Text, HttpContext.Current.Session("CSCConn").ToString)

        ''rblStatus.SelectedIndex = -1

        'GetQueueDataSet()


    End Sub

    Protected Sub btnReset_Click(sender As Object, e As System.EventArgs) Handles btnReset.Click
        lblCurrentView.Text = ""

        Session("ShowAll") = ""

        Response.Redirect("~/" & "TicketsRFSReport.aspx")

    End Sub

    Protected Sub txtStartDate_TextChanged(sender As Object, e As System.EventArgs) Handles txtStartDate.TextChanged

        HDStartDate.Text = txtStartDate.Text

        'AllGroupQueuesInfo.GetAllGroupQueues(HttpContext.Current.Session("CSCConn").ToString)

        'allgrouplist.Clear()

        'allgrouplist = AllGroupQueuesInfo.AllGroups


        'TicketRFSReportlist = New TicketsRFS(HDStartDate.Text, HDEnddate.Text, Groups, hdStatus.Text, hdType.Text, HttpContext.Current.Session("CSCConn").ToString)

        'GetQueueDataSet()

    End Sub

    Protected Sub txtEndDate_TextChanged(sender As Object, e As System.EventArgs) Handles txtEndDate.TextChanged

        HDEnddate.Text = txtEndDate.Text
        'AllGroupQueuesInfo.GetAllGroupQueues(HttpContext.Current.Session("CSCConn").ToString)

        'allgrouplist.Clear()

        'allgrouplist = AllGroupQueuesInfo.AllGroups


        'TicketRFSReportlist = New TicketsRFS(HDStartDate.Text, HDEnddate.Text, Groups, hdStatus.Text, hdType.Text, HttpContext.Current.Session("CSCConn").ToString)
        'GetQueueDataSet()

    End Sub
    Protected Sub btnDashboard_Click(sender As Object, e As System.EventArgs) Handles btnDashboard.Click
        Response.Redirect("~/TicketDashboard.aspx", True)

    End Sub

    Protected Sub btnExcel_Click(sender As Object, e As System.EventArgs) Handles btnExcel.Click

        Groups = hdGroups.Text

        Response.Redirect("./Reports/RolesReport.aspx?ReportName=TicketRFSReport" & "&StartDate=" & HDStartDate.Text & "&enddate=" & HDEnddate.Text & "&GroupNum=" & Groups & "&Status=" & hdStatus.Text & "&RequestType=" & hdType.Text & "&Priority=" & hdPriority.Text, True)


    End Sub


End Class
