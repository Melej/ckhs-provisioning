﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="false" CodeFile="SignOnFormFinanical.aspx.vb" Inherits="SignOnFormFinanical" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <script type="text/javascript" src="Scripts/FieldValidator.js"></script>
<script type="text/javascript">
    function pageLoad(sender, args) {

        fieldValidator('textInputRequired', 'btnSubmit', 'lblValidation');

    }
</script>
    <style type="text/css">
        .style2
        {
            text-align: center;

        }
    </style>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
<asp:UpdatePanel ID="uplSignOnForm" runat="server"  >

    <Triggers>

        <asp:AsyncPostBackTrigger ControlID="entity_cdDDL" EventName = "SelectedIndexChanged" />
        <asp:AsyncPostBackTrigger ControlID="facility_cdddl" EventName = "SelectedIndexChanged" />
        <asp:AsyncPostBackTrigger ControlID="cblApplications" EventName = "SelectedIndexChanged" />
        <asp:AsyncPostBackTrigger ControlID="emp_type_cdrbl" EventName = "SelectedIndexChanged" />
        <asp:AsyncPostBackTrigger ControlID="btnClearAccounts" EventName="Click" />
        <asp:AsyncPostBackTrigger ControlID = "btnSubmit" EventName ="Click" />
    </Triggers>
  <ContentTemplate>
  
   <table width = "100%" >
    <tr>
     <td>
      <table>
        <tr>
            <td align ="center">
            <asp:Label ID = "lblValidation" runat = "server" ForeColor="Red" Font-Size="15px" Font-Bold="True" />
            </td>
            
        </tr>
        <tr class="tableRowHeader">
                 <td>
                     Provisioning Form
                 </td>
        </tr>
        <tr class="tableRowSubHeader">
                <td align="left">
                    Form Submission
                </td>
        </tr>
        <tr>
            <td align="center">
              <table>
                <tr>
                    <td colspan = "4" align="center">
                        <asp:Label ID="thisenviro" runat="server" Visible="false" />
                    
                    </td>
                </tr>
                <tr>
                   <td align="right">
                      <asp:Label ID="lblusersubmit" runat="server" Text="User Submitting:" Font-Size="Small" Width="125px" />
                   </td>

                   <td align="left">
                        <asp:Label ID="userSubmittingLabel" Width="150px" runat="server" Font-Size="Small" Font-Bold="true"></asp:Label>
                        <asp:Label ID="submitter_client_numLabel" runat="server" visible="false" />
                    </td>
                   <td align="right">
                        <asp:Label ID="lblcurrRequesto" runat="server" Width="200px" Font-Size="Small" Text="Current Auth.Mgr./Delegate:" />
                  </td>
                  <td align="left">
                        <asp:Label ID="SubmittingOnBehalfOfNameLabel" runat="server" Font-Size="Small" Font-Bold="true"></asp:Label>
                        <asp:Label ID="requestor_client_numLabel" runat="server" visible="false"></asp:Label>
                  </td>
                </tr>
              </table>

              <table>
                <tr align="center">
                    <td>
                        <asp:Button ID="btnSubmit" runat="server" Text="Submit"
                        CssClass="btnhov" BackColor="#006666"  Width="140px" Height="20px" />

                    </td>
                    <td>
                        <asp:Button ID="btnSelectOnBehalfOf" runat="server" Text="Change Auth.Mgr./Delegate:" 
                             CssClass="btnhov" BackColor="#006666"  Width="200px" Height="20px" />

                    </td>
                    <td>
                        <asp:Button ID="btnReturn" runat="server" Text="Return" 
                          CssClass="btnhov" BackColor="#006666"  Width="140px" Height="20px" />

                    </td>

                    <td>
                        <asp:Button ID="btnUpdateSubmitter" runat="server" Font-Bold="True" 
                            Font-Names="Arial" Font-Size="Small" Height="20px" Text="Update Demographics" 
                            Visible="false" Width="140px"/>
                    </td>
                </tr>
                 <tr>
                      <td colspan="4" align="center">
                              <asp:Button ID="btnCloseRequest" runat="server" Text="Close Request" Visible="false"
                        CssClass="btnhov" BackColor="#006666"  Width="149px"  Height="20px"  />

                      </td>
                  </tr>
              </table>
           </td>
        </tr>
        <tr>
            <td align="left" class="tableRowSubHeader">
                  CKHS Affiliation
              </td>
        </tr>
        <tr>
            <td align="center">
                  <asp:RadioButtonList ID="emp_type_cdrbl" runat="server" AutoPostBack="true" 
                      RepeatDirection="Horizontal" Font-Size="Smaller" >
                  </asp:RadioButtonList>
             </td>
        </tr>
        <tr>
           <td colspan="4">
              <asp:Panel ID="pnlProvider" runat="server" >
                      <table id="tblprovider" border="3" width="100%">

                          <tr>
                              <td align="left" class="tableRowSubHeader" colspan="4">
                                  Provider Information
                              </td>
                          </tr>

                          <tr>
                              <td align="right">
                                    <asp:Label ID="priviledlbl" runat="server" Text="Privileged Date:" 
                                    ToolTip="Only Credentialing Office will enter this data, Indicates the date the Provider was credentialied on." 
                                     Width="150px" />

                                  
                              </td>
                              <td align="left">
                                  <asp:TextBox ID="credentialedDateTextBox" runat="server"  ToolTip="Only Credentialing Office will enter this data"
                                       Enabled="false"></asp:TextBox>
                              </td>

<%--                              <td align="right">
                                  DCMH Privileged Date:
                              </td>
                              <td align="left">
                                  <asp:TextBox ID="CredentialedDateDCMHTextBox" runat="server" 
                                       Enabled="false"></asp:TextBox>
                              </td>--%>


                                <td align="right">
                                    <asp:Label ID="ccmcadmitlbl" runat="server" Text ="Privileged Rights:" Width="150px" />                                   
                                </td>
                                <td align="left" >
                                        <asp:RadioButtonList ID = "CCMCadmitRightsrbl" runat="server" RepeatDirection="Horizontal"
                                         AutoPostBack="true">
                                        <asp:ListItem>Yes</asp:ListItem>
                                        <asp:listItem>No</asp:listItem>
                                    </asp:RadioButtonList>
                                   
                                </td>
<%--                                <td align="right">
                                    <asp:Label ID="dcmhlbl" runat="server" Text ="DCMH Privileged Rights:" Width="150px" />
                                    </td>

                                <td align="left" >
                                        <asp:RadioButtonList ID = "DCMHadmitRightsrbl" runat="server" RepeatDirection="Horizontal"
                                        AutoPostBack="true">
                                        <asp:ListItem>Yes</asp:ListItem>
                                        <asp:listItem>No</asp:listItem>
                                    </asp:RadioButtonList>

                                </td>--%>

                            </tr>

                          <tr>
                              <td align="right">
                                  CHMG:
                              </td>
                              <td align="left">
                                  <asp:RadioButtonList ID="hanrbl" runat="server" RepeatDirection="Horizontal" AutoPostBack="true">
                                      <asp:ListItem>Yes</asp:ListItem>
                                      <asp:ListItem>No</asp:ListItem>
                                  </asp:RadioButtonList>
                              </td>
                              <td align="right">
                                       Doctor Number:
                              </td>
                              <td align="left">
                                    <asp:TextBox ID = "doctor_master_numTextBox" runat="server" Enabled="false" MaxLength="6"></asp:TextBox>
                              </td>
                         </tr>

                          <tr>
                              <td align="right">
                                  NPI:
                              </td>
                              <td align="left">
                                  <asp:TextBox ID="npitextbox" runat="server" MaxLength="10"></asp:TextBox>
                              </td>
                              <td align="right">
                                  License No.
                              </td>
                              <td align="left">
                                  <asp:TextBox ID="LicensesNotextbox" runat="server"></asp:TextBox>
                              </td>
                          </tr>
                          <tr>
                              <td align="right">
                                  Taxonomy:
                              </td>
                              <td align="left">
                                  <asp:TextBox ID="taxonomytextbox" runat="server"></asp:TextBox>
                              </td>
                              <td align="right">
                                 <%-- Group Number:--%>
                              </td>

                              <td align="left">
                                  <%--<asp:TextBox ID="group_numTextBox" runat="server" Enabled="false" ></asp:TextBox>--%>
                              </td>
                          </tr>

<%--                          <tr>
                            <td align="right">
                                Medicaid ID:
                            </td>
                            <td align="left">
                                <asp:TextBox ID="MedicareIDTextBox" runat="server" Width="250px"></asp:TextBox>
                            </td>

                            <td align="right">
                                BlueCross ID:
                            </td>
                            <td align="left">
                                <asp:TextBox ID="BlueCrossIDTextBox" runat="server" Width="250px"></asp:TextBox>
                            </td>

                        </tr>--%>

                        <tr>
                            <td align="right">
                                DEA ID:
                            </td>
                            <td align="left">
                                <asp:TextBox ID="DEAIDTextBox" runat="server" Width="250px"></asp:TextBox>
                            </td>

                            <td align="right">
                               <%-- DEA Extension:--%>
                            </td>
                            <td align="left">
                                <%--<asp:TextBox ID="DEAExtensionIDTextBox" runat="server" Width="250px"></asp:TextBox>--%>
                            </td>

                        </tr>
<%--                          <tr>
                               <td align="right">
                                  <asp:Label ID="lblprov" runat="server" Text="HAN PCP:" Width="150px" Visible="false" />
                              </td>
                              <td align="left">
                                  <asp:RadioButtonList ID="providerrbl" runat="server" Visible="false" 
                                      RepeatDirection="Horizontal">
                                      <asp:ListItem>Yes</asp:ListItem>
                                      <asp:ListItem>No</asp:ListItem>
                                  </asp:RadioButtonList>
                              </td>
                                <td colspan="2">
                                </td>

                            </tr>--%>

                          <tr>
                                <td colspan="4">
                                <table border="3px" width="100%">

                                    <tr>
                                    <td colspan="4"  class="tableRowSubHeader" align="center">
                                        Physician Groups 
                                    </td>
                                    </tr>

                                    <tr>
                                            <td align="right" style="width: 220px">
                                                <asp:Label ID ="ckhnlbl" runat="server" Text="CHMG (G#):" Visible="false" >
                                                </asp:Label>

                                            </td>
                                            <td colspan="3" align="left">
                                                <asp:DropDownList ID = "LocationOfCareIDddl" runat="server" Visible="false" Width="90%">
    
                                                </asp:DropDownList>
                                            </td>
                                
                                    </tr>

<%--                                    <tr>

                                            <td align="right">
                                                <asp:Label ID="commlbl" runat="server" Text = "Comm. Location Of Care:"></asp:Label>
                                            </td>
                                            <td colspan="3" align="left">
                                                <asp:DropDownList ID = "CommLocationOfCareIDddl" runat="server" Width="90%">
                                            
                                                </asp:DropDownList>
                                                
                                            </td>
                                
                                    </tr>--%>

                          
                                  <tr>
                                        <td align="right" >
                                            <asp:Label ID="ecarelbl" runat="server" Text = "Provider Group Names:"></asp:Label>
                                        </td>       
                                        <td colspan="3" align="left">
                                        <asp:DropDownList ID ="group_nameddl"  runat="server" AutoPostBack="true" Width="90%">
            
                                        </asp:DropDownList>
                                        </td>
                                    </tr>

<%--                                    <tr>
                                        <td  align="right">
                                            <asp:Label ID="coverlbl" runat="server" Text = "Coverage Group ID:"></asp:Label>
                                        </td>
                                        <td  colspan="3" align="left">
                                                <asp:DropDownList ID = "CoverageGroupddl" runat="server" Width="90%">
                                            
                                                </asp:DropDownList>

                                        </td>
                                    </tr>

                                    <tr>
                                
                                        <td  align="right">
                                            <asp:Label ID="nighlbl" runat="server" Text = "Neighbor Phys Groups:" >
                                            </asp:Label>
                                        </td>
                                        <td  colspan="3" align="left">
                                                <asp:DropDownList ID = "Nonperferdddl" runat="server" Width="90%">
                                            
                                                </asp:DropDownList>
                                        </td>
                                
                                    </tr>--%>
                                </table>
                            </td>
                            </tr>

                          <tr>
                              <td align="right">
                                  Title:
                              </td>
                              <td align="left">
                                  <asp:DropDownList ID="Titleddl" runat="server"  AutoPostBack="true">
                                  </asp:DropDownList>
                                  <asp:TextBox ID="Title2TextBox" runat="server" Visible="false">
                                  </asp:TextBox>
                              </td>
                              <td align="right">
                                  Specialty:
                              </td>
                              <td align="left">
                                  <asp:DropDownList ID="Specialtyddl" runat="server"  AutoPostBack="true">
                                  </asp:DropDownList>
                                  <asp:Label ID ="SpecialtLabel" runat="server" Visible="False"></asp:Label>

                              </td>
                          </tr>

<%--                          <tr>
                              <td align="right">
                                  Non-CKHN Location:
                              </td>
                              <td align="left" colspan="3">
                                  <asp:TextBox ID="NonHanLocationTextBox" runat="server" Width="500px"></asp:TextBox>
                              </td>
                          </tr>--%>

<%--                          <tr>
                                <td align="right">
                                    CCMC Consults:
                                </td>
                                <td align="left" >
                                        <asp:RadioButtonList ID = "CCMCConsultsrbl" runat="server" RepeatDirection="Horizontal">
                                        <asp:ListItem>Yes</asp:ListItem>
                                        <asp:listItem>No</asp:listItem>
                                    </asp:RadioButtonList>

                                </td>
                                <td align="right">
                                    Springfield Consults:
                                </td>
                                <td align="left" >
                                        <asp:RadioButtonList ID = "SpringfieldConsultsrbl" runat="server" RepeatDirection="Horizontal">
                                        <asp:ListItem>Yes</asp:ListItem>
                                        <asp:listItem>No</asp:listItem>
                                    </asp:RadioButtonList>

                                </td>
                            </tr>
                          <tr>
                                <td align="right">
                                    Taylor Consults:
                                </td>
                                <td align="left" >
                                        <asp:RadioButtonList ID = "TaylorConsultsrbl" runat="server" RepeatDirection="Horizontal">
                                        <asp:ListItem>Yes</asp:ListItem>
                                        <asp:listItem>No</asp:listItem>
                                    </asp:RadioButtonList>

                                </td>
                                <td align="right">
                                    DCMH Consults:
                                </td>
                                <td align="left" >
                                        <asp:RadioButtonList ID = "DCMHConsultsrbl" runat="server" RepeatDirection="Horizontal">
                                        <asp:ListItem>Yes</asp:ListItem>
                                        <asp:listItem>No</asp:listItem>
                                    </asp:RadioButtonList>

                                </td>


                            </tr>
                          <tr>
                                <td align="right">
                                    Write Orders for C.T.S. Bedded Patients:
                                </td>
                                    <td align="left" >
                                        <asp:RadioButtonList ID = "Writeordersrbl" runat="server" RepeatDirection="Horizontal">
                                        <asp:ListItem>Yes</asp:ListItem>
                                        <asp:listItem>No</asp:listItem>
                                    </asp:RadioButtonList>

                                </td>

                                <td align="right">
                                    Write Orders for DCMH Bedded Patients:
                                </td>
                                    <td align="left" >
                                        <asp:RadioButtonList ID = "WriteordersDCMHrbl" runat="server" RepeatDirection="Horizontal">
                                        <asp:ListItem>Yes</asp:ListItem>
                                        <asp:listItem>No</asp:listItem>
                                    </asp:RadioButtonList>

                                </td>
                            </tr>--%>

                      </table>
                 </asp:Panel>
           </td>
         </tr>
           <tr>
                <td>
                    <asp:Panel ID="pnlCererUserPositions" runat="server" Visible="true">
                        <table border="3" width="100%">
                            <tr>
                                <td align="left" class="tableRowSubHeader" colspan="2">
                                    Cerner User Positions

                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <asp:Label ID="lblCerberpos" runat="server" Text ="Cerner Position" />
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="CernerPositionDescTextBox" runat="server" Width="300px" Enabled="false"></asp:TextBox>
                                    <asp:TextBox ID="UserCategoryCdTextBox" runat="server" Visible="false"></asp:TextBox>
                                </td>
                            </tr>


                        <tr>
                            <td align="right">
                                Cerner Position:

                            </td>
                        <td align="left"  >
                                    <asp:DropDownList ID = "CernerPosddl" runat="server" Visible="true" Width="80%" AutoPostBack="true"> 
                                    </asp:DropDownList>
                            </td>
                    
                    </tr>

                    </table>  
                    </asp:Panel>
                </td>

            </tr>
          <tr>
            <td>

              <asp:Panel ID="pnlTCl" runat="server" Visible="false">

                
                <table  id="tbltcl" border="3" width="100%">
                  <tr>
                     <td align="left" class="tableRowSubHeader" colspan="3">
                           TCL / User Type
                      </td>
                    </tr>
                  <tr>
                    <td align="center">
                            <asp:Label ID="LblTCL" runat="server" Text="TCL"/>
                    </td>
                    <td align="center">
                            <asp:Label ID="LblUser" runat="server" Text="Invision User Type" />
                    
                    </td>
                    <td align="center">
                            <asp:Label ID="lblUserdesc" runat="server" Text="Description"/>
                    
                    </td>

                </tr>
                  <tr>
                    <td align="center">
                            <asp:TextBox ID="TCLTextBox" runat="server" Visible="false"  Enabled="false" Width="110px"></asp:TextBox>
                    </td>

                    <td align="center">
                            <asp:TextBox ID="UserTypeCdTextBox" runat="server" Visible="false" Enabled="false" Width="110px"></asp:TextBox>

                    </td>

                    <td align="center">
                                <asp:TextBox ID="UserTypeDescTextBox" runat="server" Visible="false" Enabled="false" Width="300px"></asp:TextBox>
        
                            <asp:TextBox ID="TCLCCMC" runat="server" Visible="false"  Enabled="false" Width="110px"></asp:TextBox>
                            <asp:TextBox ID="TCLDCMH" runat="server" Visible="false"  Enabled="false" Width="110px"></asp:TextBox>
                            <asp:TextBox ID="TCLTaylor" runat="server" Visible="false"  Enabled="false" Width="110px"></asp:TextBox>
                            <asp:TextBox ID="TCLSpring" runat="server" Visible="false"  Enabled="false" Width="110px"></asp:TextBox>
                            <asp:TextBox ID="UnionTCL" runat="server" Visible="false"  Enabled="false" Width="110px"></asp:TextBox>



                    </td>
            


                </tr>
                <tr>
                <td align="center" colspan="3">
                    <asp:Button ID="btnChangeTCL" runat="server" Text="Change TCL" Width="200px" Visible="false"  />
                </td>
                </tr>

                  <tr>
                    <td align="center" colspan="3">


                        <div>
                        <asp:GridView ID="gvTCL" runat="server" AllowSorting="False"
                                AutoGenerateColumns="False" BackColor="White" BorderColor="#336666"
                                BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                                EmptyDataText="No Docmaster # Found" EnableTheming="False" 
                                DataKeyNames="TCL,UserTypeCd,UserTypeDesc" Visible="false"
                                Font-Size="Small" GridLines="Horizontal" Width="100%">
                                   
                                <FooterStyle BackColor="White" ForeColor="#333333" />
                                <HeaderStyle BackColor="#336666" Font-Bold="False" ForeColor="White" Font-Size="Small" HorizontalAlign="Left"    />                                    
                                <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Left" Font-Size="Medium"/>
                                <RowStyle BackColor="White" ForeColor="#333333" />


                                <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                <SortedAscendingHeaderStyle BackColor="#487575" />
                                <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                <SortedDescendingHeaderStyle BackColor="#275353" />

                                <Columns>
                                        <asp:CommandField ButtonType="Button" SelectText="Select" ShowSelectButton= "true" HeaderStyle-HorizontalAlign = "Left"  ControlStyle-BackColor="#84A3A3" ItemStyle-Height="30px" />
                                        <asp:BoundField  DataField="TCL"  HeaderText="TCL"  HeaderStyle-HorizontalAlign = "Left" ItemStyle-Height="35px"></asp:BoundField>
                                        <asp:BoundField  DataField="UserTypeCd"  HeaderText="User Type"  HeaderStyle-HorizontalAlign = "Left" ItemStyle-Height="35px"></asp:BoundField>
                                        <asp:BoundField  DataField="UserTypeDesc"  HeaderText="User Type Desc."  HeaderStyle-HorizontalAlign = "Left" ItemStyle-Height="35px"></asp:BoundField>
				                                                                  
                                  
                                </Columns>
                                </asp:GridView>
                        </div>


                    </td>

                      
                </tr>

                </table>  

              </asp:Panel>
                
                         
           </td>
         </tr>


        <tr>
           <td>

              <asp:Panel ID="pnlDemo" runat="server">

                  <table id="tbldemo" border="3px" width="100%">
                      <tr>
                          <td align="left" class="tableRowSubHeader" colspan="4">
                              Demographics
                          </td>
                      </tr>
                      <tr>
                          <td colspan="4">
                              <table class="style3">
                                  <tr>
                                      <td>
                                          First Name:
                                      </td>
                                      <td align="left">
                                          <asp:TextBox ID="first_nameTextBox" runat="server" width="280px"></asp:TextBox>
                                      </td>
                                      <td>
                                          MI:
                                      </td>
                                      <td>
                                          <asp:TextBox ID="miTextBox" runat="server" Width="15px"></asp:TextBox>
                                      </td>
                                      <td align="left">
                                          Last Name:
                                      </td>
                                      <td align="left">
                                          <asp:TextBox ID="last_nameTextBox" runat="server" width="280px"></asp:TextBox>
                                      </td>
                                      <td>
                                          Suffix:
                                      </td>
                                      <td>
                                          <asp:DropDownList ID="suffixddl" runat="server" Height="20px" Width="52px">
                                          </asp:DropDownList>
                                      </td>
                                  </tr>
                              </table>
                          </td>
                      </tr>
					  <tr>
                          <td colspan="4">

							  <asp:Panel ID="pnluserPosition" runat="server" Visible="false">
								  <table class="style3">
									  <tr>
										  <td>
											  Position From HR:
										  </td>
										  <td>
											 <asp:TextBox ID="UserPositionDescTextBox" runat="server" width="280px" 
													 Enabled="false"></asp:TextBox>
										  </td>
										  
										  <td>
											  Dept From HR:
										  </td>
										  <td>
											  <asp:TextBox ID="DepartmentNameTextBox" runat="server" width="280px" 
													 Enabled="false"></asp:TextBox>
										  </td>
									  </tr>

								  </table>

							  </asp:Panel>


						  </td>

					  </tr>
                      <tr>
                          <td align="right" width="20%">
                              <asp:Label ID="lblpos" runat="server" Text="Position Description:" />
                          </td>
                           <td align="left">
                               <asp:DropDownList ID="Rolesddl" runat="server"  AutoPostBack="true" >
                                    
                              </asp:DropDownList>

                              <asp:TextBox ID="user_position_descTextBox" runat="server" Visible="false"  Width="250px"></asp:TextBox>
                          </td>
                          <td align="right">
                                <asp:Label ID="empLbl" runat="server" Text ="Employee Number:" ></asp:Label>
                          </td>
                          <td align="left">
                              <asp:TextBox ID="AuthorizationEmpNumTextBox" runat="server" Enabled="false" 
                                  Width="250px"></asp:TextBox>
                          </td>
                      </tr>
                      <tr>
                        <td align="right">
                            <asp:Label ID="SuppSuppLbl" runat="server" Text="Supplemental Support Staff:" Visible="true">
                            </asp:Label>

                            
                        </td>
                        <td align="left">
                                <asp:RadioButtonList ID = "SuppSupportrbl" runat="server" RepeatDirection="Horizontal" AutoPostBack="true">
                                    <asp:ListItem>Yes</asp:ListItem>
                                    <asp:listItem>No</asp:listItem>
                                </asp:RadioButtonList>

                        </td>
                        <td>
                            <asp:Label ID="DoesNeedprovlbl" runat="server" Text="Behavioral Health ID# Needed" Visible="true">
                            </asp:Label>
                        
                        </td>
                        <td>
                            <asp:RadioButtonList ID = "NeedsProviderrbl" runat="server" RepeatDirection="Horizontal" Visible="true">
                                    <asp:ListItem>Yes</asp:ListItem>
                                    <asp:listItem>No</asp:listItem>

                            </asp:RadioButtonList>
                        
                        </td>
                      
                      </tr>
                      <tr>
                          <td align="right">
                              Entity:
                          </td>
                          <td align="left">
                              <asp:DropDownList ID="entity_cdDDL" runat="server" AutoPostBack="True">
                              </asp:DropDownList>
                          </td>
                          <td align="right">
                              <asp:Label ID="lbldept" runat="server" Text="Department Name:" />
                          </td>
                          <td align="left">
                              <asp:DropDownList ID="department_cdddl" runat="server" Width="250px">
                              </asp:DropDownList>
                          </td>
                      </tr>
                      <tr>
                          <td align="right">
                              Address 1:
                          </td>
                          <td align="left">
                              <asp:TextBox ID="address_1textbox" runat="server" Width="250px"></asp:TextBox>
                              <asp:TextBox ID="address1TextBox" runat="server" Width="250px" Visible="false"></asp:TextBox>

                          </td>
                          <td align="right">
                              Address 2:
                          </td>
                          <td align="left">
                              <asp:TextBox ID="address_2textbox" runat="server" Width="250px"></asp:TextBox>
                          </td>
                      </tr>
                      <tr>
                          <td align="right">
                              <asp:Label ID="lblcity" runat="server" text="City:" width="50px" />
                          </td>
                          <td align="left" colspan="3">
                              <asp:TextBox ID="citytextbox" runat="server" width="500px"></asp:TextBox>
                          </td>
                      </tr>
                      <tr>
                          <td align="right">
                              <asp:Label ID="lblstate" runat="server" Text="State:" Width="50px" />
                          </td>
                          <td align="left">
                              <asp:TextBox ID="statetextbox" runat="server" Width="50px"></asp:TextBox>
                          </td>
                          <td align="right">
                              <asp:Label ID="lblzip" runat="server" Text="Zip:" Width="25px" />
                          </td>
                          <td align="left">
                              <asp:TextBox ID="ziptextbox" runat="server" Width="50px"></asp:TextBox>
                          </td>
                      </tr>
                      <tr>
                          <td align="right">
                              Phone Number:
                          </td>
                          <td align="left">
                              <asp:TextBox ID="phoneTextBox" runat="server" MaxLength="12"></asp:TextBox>
                          </td>
                          <td align="right">
                              Pager Number:
                          </td>
                          <td align="left">
                              <asp:TextBox ID="pagerTextBox" runat="server" MaxLength="12"></asp:TextBox>
                          </td>
                      </tr>
                      <tr>
                          <td align="right">
                              Fax:
                          </td>
                          <td align="left">
                              <asp:TextBox ID="faxtextbox" runat="server" MaxLength="12"></asp:TextBox>
                          </td>
                          <td align="right">
                              Email:
                          </td>
                          <td align="left">
                              <asp:TextBox ID="e_mailTextBox" runat="server"  Width="250px"></asp:TextBox>
                          </td>
                      </tr>
                      <tr>
                          <td align="right">
                              Location:
                          </td>
                          <td align="left">
                              <asp:DropDownList ID="facility_cdddl" runat="server" AutoPostBack="True">
                              </asp:DropDownList>
                          </td>
                          <td align="right">
                              Building:
                          </td>
                          <td align="left">
                              <asp:DropDownList ID="building_cdddl" runat="server" AutoPostBack="True" CssClass="style3">
                              </asp:DropDownList>
                          </td>
                      </tr>
                       <tr>
                            <td align="right" >
                                Floor:
                            </td>

                            <td align="left">
                                <asp:DropDownList ID="Floorddl" runat="server" AutoPostBack="True">
            
                                </asp:DropDownList>
                            
                            </td>
                            <td>
                            
                            </td>

                            <td>
                            
                            </td>

                        </tr>

                      <tr>
                          <td align="right">
                              <asp:Label ID="lblVendor" runat="server" Text="Vendor/Contracter Name:"></asp:Label>
                          </td>
                          <td align="left" colspan="3">
                              <asp:TextBox ID="VendorNameTextBox" runat="server" Width="500px"></asp:TextBox>
                              <asp:Label ID="VendorNumLabel" runat="server" Visible="False"></asp:Label>
                                <asp:DropDownList ID="Vendorddl" runat="server" Width="500px" Visible="false" AutoPostBack="true">
                                
                                </asp:DropDownList>
                          </td>
                      </tr>
                      <tr>
                          <td align="right">
                              Start Date:
                          </td>
                          <td align="left">
                              <asp:TextBox ID="start_dateTextBox" runat="server" CssClass="calenderClass"></asp:TextBox>
                          </td>
                          <td align="right">
                              End Date:
                          </td>
                          <td align="left">
                              <asp:TextBox ID="end_dateTextBox" runat="server" CssClass="calenderClass"></asp:TextBox>
                          </td>
                      </tr>
                      <tr>
                          <td align="right">
                              Share Drive:
                          </td>
                          <td align="left">
                              <asp:TextBox ID="share_driveTextBox" runat="server" Width="250px"></asp:TextBox>
                          </td>
                          <td align="right">
                              Model After:
                          </td>
                          <td align="left">
                              <asp:TextBox ID="ModelAfterTextBox" runat="server"  Enabled="false" Width="250px"></asp:TextBox>
                          </td>
                      </tr>
                      <tr>
                          <td align="right">
                              <asp:Label ID="lblreqclosedate" runat="server" Text="Request Close Date:" />
                          </td>
                          <td align="left">
                              <asp:TextBox ID="close_dateTextBox" runat="server" Enabled="false"></asp:TextBox>
                          </td>
                          <td>
                              <asp:Label ID="department_cdLabel" runat="server" Visible="False"></asp:Label>
                              <asp:Label ID = "emp_type_cdLabel" runat="server" Visible="false"></asp:Label>
                              <asp:Label ID ="TitleLabel" runat="server" Visible="False"></asp:Label>
                              <asp:Label ID="RoleNumLabel" runat="server" Visible="false"></asp:Label>
                              <asp:Label ID="TestednameLabel" runat="server" Visible="false"></asp:Label>
                              <asp:Label ID="userDepartmentCD"  runat="server" Visible="False"></asp:Label>
                                <asp:TextBox ID="FloorTextBox" runat="server" Visible="false"></asp:TextBox>

                                 <asp:Label ID="LocationOfCareIDLabel"  runat="server" visible="false"></asp:Label>
                                  <asp:Label ID="GNumberGroupNumLabel"  runat="server" visible="false"></asp:Label>

                                 <asp:Label ID ="CoverageGroupNumLabel"  runat="server" visible="false"></asp:Label>
                                 <asp:Label id="NonPerferedGroupnumLabel" runat="server" Visible="false"></asp:Label>

<%--                                   <asp:Label ID="LocationOfCareLabel" runat="server" Visible="false"></asp:Label>
--%>
                                  <asp:Label ID="CommNumberGroupNumLabel" runat="server" visible="false"></asp:Label>
                                  <asp:Label ID="CommLocationOfCareIDLabel" runat="server" visible="false"></asp:Label>

                                  <asp:TextBox ID="group_nameTextBox" runat="server" Width="500px" Visible="false"></asp:TextBox>

                                  <asp:Label ID="TokenTest" runat="server" Visible="false"></asp:Label>
                                  <asp:TextBox ID="masterRole" runat="server"  Visible="false"></asp:TextBox>

                                  <asp:TextBox ID="RehireRequestNumTextBox" runat="server"  Enabled="false" Visible="false"></asp:TextBox>
                                   <asp:TextBox ID="ReactivateClientNumTextBox" runat="server"  Enabled="false" Visible="false"></asp:TextBox>

                                  <asp:Label ID="ClaimMgtID" runat="server" Visible="false"></asp:Label>
                                  <asp:Label ID="ClaimMgtGroupNum" runat="server" Visible="false"></asp:Label>

                                  <asp:Label ID="CarePayID" runat="server" Visible="false"></asp:Label>
                                  <asp:Label ID="CarePayGroupNum" runat="server" Visible="false"></asp:Label>

                                  <asp:Label ID="PatientAcctID" runat="server" Visible="false"></asp:Label>
                                  <asp:Label ID="PatientAcctGroupNum" runat="server" Visible="false"></asp:Label>

                                  <asp:Label ID="SCIid" runat="server" Visible="false"></asp:Label>
                                  <asp:Label ID="SCIGroupNum" runat="server" Visible="false"></asp:Label>

                              <asp:TextBox ID="facility_cdTextBox" runat="server" Visible="false"></asp:TextBox>

                              <asp:TextBox ID="PMHRequestNumTextBox" runat="server" Visible="false"></asp:TextBox>
								<asp:TextBox ID="RequesttypeTextBox" runat="server"  Enabled="false" Visible="false"></asp:TextBox>							  						


                          </td>
                          <td>
                              <asp:TextBox ID="building_cdTextBox" runat="server" Visible="False"></asp:TextBox>
                          </td>
                      </tr>
                      <tr>
                          <td align="left" colspan="4">
                              Comments:
                          </td>
                      </tr>
                      <tr>
                          <td align="left" colspan="4">
                              <asp:TextBox ID="request_descTextBox" runat="server" TextMode="MultiLine" 
                                  Width="100%"></asp:TextBox>
                            <asp:TextBox id="OrigReqDesc" runat="server" Visible="false"></asp:TextBox>
                          </td>
                      </tr>
                       <tr>
                         <td align="left" colspan="4">
         	                      <%-- =================== Claim management ===============================--%>

                            <asp:Panel ID="pnlClaimMgt" runat="server"  Visible="false">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblClaimsMgt" runat="server" Width="175" text = "Claims Mgt. Level"></asp:Label>
                                        </td>

                                       <td>
                                             <asp:RadioButtonList ID="ClaimMgtrbl" runat="server" Width="550" RepeatDirection="Horizontal" >
                                              </asp:RadioButtonList>

                                            
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                         </td>                        
                       </tr> 
                       <tr>
                         <td align="left" colspan="4">
         	                      <%-- =================== Care Payment ===============================--%>

                            <asp:Panel ID="pnlCarePay" runat="server"  Visible="false">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblCarePAy" runat="server" Width="175" text = "Care Payment Level"></asp:Label>
                                        </td>

                                       <td>
                                             <asp:RadioButtonList ID="CarePaymentrbl" runat="server" Width="550" RepeatDirection="Horizontal">
                                              </asp:RadioButtonList>

                                            
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                         </td>                        
                       </tr> 

                       <tr>
                         <td align="left" colspan="4">
         	                      <%-- =================== Paient Accounting ===============================--%>

                            <asp:Panel ID="panPatientAcct" runat="server"  Visible="false">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblPaientAcct" runat="server" Width="175" text = "Patient Accounting Functions"></asp:Label>
                                        </td>

                                       <td>
                                             <asp:RadioButtonList ID="PAtientAcctrbl" runat="server" Width="550" RepeatDirection="Horizontal">
                                              </asp:RadioButtonList>

                                        </td>
                                  </tr>
                                  <tr>
                                        <td align="right">
                                            <asp:Label ID="ptamountlbl" runat="server" Text = "Select Level" Visible="false" />
                                        
                                        </td>
                                        <td align="left">
                                                 <asp:DropDownList ID = "PatientAcctddl" runat="server" Visible="false" Width="300">
                                                        <asp:ListItem Value="" Text="Select Level" Selected="True"></asp:ListItem>
                                                        <asp:ListItem Value="Staff" Text="Staff to 499.99"></asp:ListItem>
                                                        <asp:ListItem Value="Lead" Text="Lead to 999.99"></asp:ListItem>
                                                        <asp:ListItem Value="Specialists" Text="Specialists to 4,999.99"></asp:ListItem>
                                                        <asp:ListItem Value="Supervisor" Text="Supervisor to 9,999.99"></asp:ListItem>
                                                        <asp:ListItem Value="Manager" Text="Manager to 25,000.00"></asp:ListItem>

                                                </asp:DropDownList>

                                            <asp:TextBox ID="patamount" runat="server" Visible="false" Width="175" /> </td>
                                  </tr>
                                </table>
                            </asp:Panel>
                         </td>                        
                       </tr> 

                       <tr>
                         <td align="left" colspan="4">
         	                      <%-- =================== SCI Scheduling===============================--%>

                            <asp:Panel ID="panSCISchedule" runat="server"  Visible="false">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblSCISCh" runat="server" text = "SCI Scheduler Level" Width="175" ></asp:Label>
                                        </td>
                                       <td>
                                             <asp:RadioButtonList ID="ScISchedulerbl" runat="server" Width="550" RepeatDirection="Horizontal">
                                              </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                         </td>                        
                       </tr> 


                      <tr>
                         <td align="center" colspan="4">
   	                      <%-- =================== RSA Token Address ===============================--%>

                          <asp:Panel ID="pnlTokenAddress" runat="server"  Visible="false">

                             <table id="Tokentbl"  border="3px" width="100%">
                              <tr>
                                 <td align="left" class="tableRowSubHeader" colspan="4">
                                       Cell Phone Information for Remote Access
                                  </td>
                                </tr>

                                <tr>
                                    <td colspan="4" align="center">
                                        <asp:Label id="lblTokenError" runat="server" BackColor="Red" Visible="false" Font-Size="Large" Font-Bold="True"></asp:Label>
                                    </td>
                                </tr>
                                
                                <tr>

                                    <td align="right">
                                        Provider:
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="CellPhoneddl" runat="server" Width="250px" AutoPostBack="true">
                                
                                        </asp:DropDownList>
                                        <asp:TextBox ID="otherCellprov" runat="server" Visible="false"  MaxLength="25" ></asp:TextBox>
                                    </td>
                                    <td align="right">
                                        Number: ##########
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID ="TokenPhoneTextBox" runat="server" MaxLength="10" 
                                            ToolTip="Only enter Numbers no other Characters" ></asp:TextBox>
                                    </td>

                                </tr>

<%--                                <tr>
                                    <td align="right">
                                        Comments:
                                    </td>
                                    <td align="left" colspan="3">
                                        </asp:TextBox>

                                    </td>

                
                                </tr>--%>

                                <tr align="center">
                                    <td align="left" colspan="4" >

                                        <asp:TextBox ID="CellProvidertxt" runat="server" Visible="false"></asp:TextBox>
                                        <asp:TextBox ID ="TokenNameTextBox" runat="server"  Width="80%" Visible="false"></asp:TextBox>
                                        <asp:TextBox ID ="TokenAddress1TextBox" Width="80%" runat="server" Visible="false"></asp:TextBox>
                                        <asp:TextBox ID ="TokenAddress2TextBox" runat="server" Width="80%" Visible="false"></asp:TextBox>
                                          <asp:TextBox ID ="TokencommentsTextBox" runat="server"  Width="90%" TextMode="MultiLine" Visible="false"></asp:TextBox>

                                        <asp:TextBox ID ="TokenStTextBox" runat="server" Visible="false"></asp:TextBox>
                                      <asp:TextBox ID ="TokenZipTextBox" runat="server" Visible="false"></asp:TextBox>
                                        <asp:TextBox ID ="TokenCityTextBox" runat="server" Width="80%" Visible="false"></asp:TextBox>
                                        <asp:Label ID ="ecareappSelectedlbl" runat="server"  Visible="false"></asp:Label>
                                         <asp:TextBox ID="PositionRoleNumTextBox" runat="server" Visible="false"></asp:TextBox>

                                    </td>
                                </tr>
            
                              </table>
            			

                             </asp:Panel>


<%--                                
                                <tr>
                                    <td align="right">
                                        Name:
                                    </td>

                                    <td align="right">
                                        Address Line 1:
                                    </td>
                                    <td align="left">
                                    </td>

                                    <td align="right">
                                        Address Line 2:
                                    </td>
                                    <td align="left">
                                    </td>

                                </tr>
                                <tr>
                                    <td align="right">
                                        City:
                                    </td>
                                    <td align="left">
                                    </td>

                                    <td align="right">
                                        State:
                                    </td>
                                    <td >
                                    </td>
                                    <td align="right">
                                        Zip:
                                    </td>
                                    <td align="left">

                                    </td>
                        
                        
                                </tr>
--%>

                       </td>
                     </tr>

                        
                        

                      <tr>
                          <td>
                              Entities
                          </td>
                          <td colspan="3">
                              <asp:CheckBoxList ID="cblEntities" runat="server" RepeatDirection="Horizontal" AutoPostBack="true">
                              </asp:CheckBoxList>
                          </td>
                      </tr>
                      <tr>
                          <td>
                              Hospitals
                          </td>
                          <td colspan="3">
                              <asp:CheckBoxList ID="cblFacilities" runat="server" 
                                  RepeatDirection="Horizontal"  AutoPostBack="true">
                              </asp:CheckBoxList>
                          </td>
                      </tr>
                      <tr>
                        <td>
                            <asp:Label ID="Regionlbl" runat="server" text="Regions" Visible="false"></asp:Label>
                        </td>
                        <td colspan="3">
                              <asp:CheckBoxList ID="cblRegions" runat="server" 
                                  RepeatDirection="Horizontal"  AutoPostBack="true" Visible="false">
                              </asp:CheckBoxList>

                        </td>
                      </tr>
                      <tr>
                          <td>
<%--                              Role Template
--%>                          </td>
                          <td>

                          </td>
                          <td colspan="2">
                              <asp:Button ID="btnClearAccounts" runat="server" 
                                  Text="Clear Selected Accounts" Font-Bold="True" Font-Names="Arial" 
                                  Font-Size="Small" Height="20px" Width="170px" Visible="false" />
                          </td>
                      </tr>
                      <tr>
                          <td colspan="4" align="left">
                              <asp:CheckBoxList ID="cblApplications" runat="server" RepeatColumns="3" AutoPostBack="false">
                              </asp:CheckBoxList>


                          </td>
                      </tr>
                      <tr>
                            <td colspan="4">
                                <asp:TextBox ID="HdMainrole" runat="server" Visible="false"></asp:TextBox>
                                <asp:TextBox ID="HDEmpRoleNum" runat="server" Visible="false"></asp:TextBox>

                            </td>
                      </tr>
                  </table>

              </asp:Panel>

            </td>
        </tr>
     </table>
     </td>
    </tr>
   </table>

    <asp:Panel ID="pnlOnBehalfOf" runat="server"  Width="70%" Height="60%"  BackColor="#CCCCCC"  style="display:none">
        <asp:UpdatePanel ID="uplOnBehalfOf" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table align="center" border="1px" width="90%">
                   <tr valign="middle">
                        <td colspan="4"></td>
                    </tr>
                    <tr>
                        <td class="tableRowHeader" colspan="4">
                           Search for CKHS Individual who will be Responsable for these Account(s)<br />
                           Listed below are CKHS Directors-Managers-VP 

                        </td>
                    </tr>
                    <tr>
                        <td>
                            Last Name
                        </td>
                        <td>
                            <asp:TextBox ID="LastNameTextBox" runat="server"></asp:TextBox>
                        </td>
                        <td>
                            First Name
                        </td>
                        <td>
                            <asp:TextBox ID="FirstNameTextBox" runat="server"></asp:TextBox>
                        </td>

                    </tr>
                    <tr>
                        <td colspan="4">
                            <div class="scrollContentContainer">
                                <asp:UpdateProgress ID="UpdateProgress1" runat="server" 
                                    AssociatedUpdatePanelID="uplOnBehalfOf" DisplayAfter="200">
                                    <ProgressTemplate>
                                        <div ID="IMGDIV" runat="server" align="center" 
                                            style="visibility:visible;vertical-align:middle;" valign="middle">
                                            Loading Data ...
                                        </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                                <asp:GridView ID="gvSearch" runat="server" AllowSorting="True" 
                                    AutoGenerateColumns="False" BackColor="White" BorderColor="#336666" 
                                    BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                                    DataKeyNames="client_num, login_name" EmptyDataText="No Items Found" 
                                    EnableTheming="False" Font-Size="Medium" GridLines="Horizontal" Width="100%">
                                    <FooterStyle BackColor="White" ForeColor="#333333" />
                                    <HeaderStyle BackColor="#336666" Font-Bold="False" Font-Size="Small" 
                                        ForeColor="White" HorizontalAlign="Left" />
                                    <PagerStyle BackColor="#336666" Font-Size="Medium" ForeColor="White" 
                                        HorizontalAlign="Left" />
                                    <RowStyle BackColor="White" ForeColor="#333333" />
                                    <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                    <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                    <SortedAscendingHeaderStyle BackColor="#487575" />
                                    <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                    <SortedDescendingHeaderStyle BackColor="#275353" />
                                    <Columns>
                                        <asp:CommandField ButtonType="Button" SelectText="Select" 
                                            ShowSelectButton="true" />
                                        <asp:BoundField DataField="fullname" HeaderStyle-HorizontalAlign="Left" 
                                            HeaderText="Full Name" ItemStyle-HorizontalAlign="Left" />
                                        <asp:BoundField DataField="phone" HeaderStyle-HorizontalAlign="Left" 
                                            HeaderText="Phone" ItemStyle-HorizontalAlign="Left" />
                                        <asp:BoundField DataField="department_name" HeaderStyle-HorizontalAlign="Left" 
                                            HeaderText="Department" ItemStyle-HorizontalAlign="Left" />
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <table>
                                <tr>
                                    <td>
                                        <asp:Button ID="btnSearch" runat="server" Font-Bold="True" Font-Names="Arial" 
                                            Font-Size="Small" Height="20px" Text="Search" Width="125px" />
                                    </td>
                                    <td>
                                        <asp:Button ID="btnCloseOnBelafOf" runat="server" Font-Bold="True" 
                                            Font-Names="Arial" Font-Size="Small" Height="20px" Text="Close" Width="125px" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>

    <ajaxToolkit:UpdatePanelAnimationExtender ID="UpdatePanelAnimationExtender1" runat="server" TargetControlID="uplOnBehalfOf">
        <Animations>
            <OnUpdating>
                <StyleAction animationtarget="progress" Attribute="display" value="block" />
            </OnUpdating>
            <OnUpdated>
                <StyleAction animationtarget="progress" Attribute="display" value="none" />
            </OnUpdated>
        </Animations>
    </ajaxToolkit:UpdatePanelAnimationExtender>

    <asp:Button ID="btnUpdateDummy" runat="server" Style="display: none" Text="Button" />

                 
    <ajaxToolkit:ModalPopupExtender ID="mpeOnBehalfOf" runat="server" 
       DynamicServicePath="" Enabled="True" TargetControlID="btnSelectOnBehalfOf"   
       PopupControlID="pnlOnBehalfOf" BackgroundCssClass="ModalBackground"   
         DropShadow="true" CancelControlID="btnCloseOnBelafOf">  
        </ajaxToolkit:ModalPopupExtender>

    <ajaxToolkit:ModalPopupExtender ID="mpeUpdateClient" runat="server" 
        BackgroundCssClass="ModalBackground" DropShadow="true" DynamicServicePath="" 
        Enabled="True" PopupControlID="pnlUpdateClient" CancelControlID="btnCloseUpdate"
        TargetControlID="btnUpdateDummy">
    </ajaxToolkit:ModalPopupExtender>

    <asp:Panel ID="pnlUpdateClient" runat="server" 
                    style="display:none; background-color:#efefef;  font-size:large;  width:600px;  border: 2px solid #FFFFCC;">
                    <table border="1px" width="100%">
                        <tr>
                            <th class="tableRowHeader">
                                <asp:Label ID="lblUpdateClientFullName" runat="server"></asp:Label>
                            </th>
                        </tr>
                        <tr>
                            <td>
                                <asp:Table ID="tblUpdateClient" runat="server" Width="100%">
                                    <asp:TableRow>
                                        <asp:TableCell>
                Phone Number
            </asp:TableCell>
                                        <asp:TableCell>
                <asp:TextBox runat="server" ID="txtUpdatePhone"></asp:TextBox>
            </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                    Email
            </asp:TableCell>
                                        <asp:TableCell>
                <asp:TextBox runat="server" ID="txtUpdateEmail" Width="100%"></asp:TextBox>
            </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button ID="btnUpdateClient" runat="server" Text="Update" />
                                <asp:Button ID="btnCloseUpdate" runat="server" Text="Cancel" />
                            </td>
                        </tr>
                    </table>
        </asp:Panel>

  <%-- =================== Have you Searched ============================================================================--%>
	<asp:Panel ID="pnlSearchFirst" runat="server" Height="450px" Width="575px"  BackColor="#FFFFCC" style="display:none" >
        <table  id="tblSearchFirst" runat="server" style="border: medium groove #000000; height:100%"  align="center" width="100%" >

            <tr>

                <td  colspan="3" align="center" style="font-size: x-large; font-weight: bold; color: #FF0000">
                        Have you Searched for the person you are about to add? 
                </td>

            </tr>
            <tr>
                <td style="width: 3px">
                </td>

                <td style="font-weight: bold; font-size: larger; text-align: center;" align="center">
                   This form is to be filled out for New individuals who have NEVER been at Crozer Keystone Health System before!
                </td>
                <td style="width: 3px">
                </td>

            </tr>
            <tr>
                <td style="width: 3px">
                </td>

                <td style="font-weight: bold; font-size: larger; text-align: center;" align="center">
                   If you want to add New Accounts or Delete current Accounts for your personnel, Search and click on Add Account Tab or Delete Account tab.
                </td>
                <td style="width: 3px">
                </td>

            </tr>

            <tr>
                <td style="width: 3px">
                    <asp:Label ID="lblSearched" runat="server" Visible="false"></asp:Label>
                </td>
                <td align="center" >
                    <asp:Button ID="btnSearchFirst" runat="server" BackColor="#00C000" Font-Bold="True" Font-Names="Arial"
                        Font-Size="Small" ForeColor="Black" Height="35px" Text="Yes I Searched" Width="140px" />

                    <asp:Button ID="btnNoSearch" runat="server" BackColor="DimGray" Font-Bold="True"
                        Font-Names="Arial" Font-Size="Small" ForeColor="White" Height="35px" Text="No I want to Search"
                        Visible="true" Width="140px" ToolTip="Return " />

                </td>
                <td style="width: 3px">
                </td>
            </tr>
        </table>
        <br />
        </asp:Panel> 

        <ajaxToolkit:ModalPopupExtender ID="SearchPopup" runat="server" 
             TargetControlID="HdBtnSearch" PopupControlID="pnlSearchFirst" 
            BackgroundCssClass="ModalBackground" >
        </ajaxToolkit:ModalPopupExtender>

        <%--CancelControlID="btnNoSearch"--%>

       <asp:Button ID="HdBtnSearch" runat="server" style="display:none" />

 	<%-- =================== Clients exists ============================================================================--%>
         <asp:Panel ID="pnlClientExists" runat="server"  style="display:none" Width="70%" BorderColor="Black" BackColor="#999999" Height="80%">
                <table id="tblClientEx" runat="server" style="border: medium groove #000000; height:80%"  align="center" width="60%" >
                    <tr>
                        <td class="tableRowHeader" colspan="4">
                           Clients That may already exists
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4" style="background-color: #CCCCCC; font-size: x-large; font-weight: bold">
                            Use Perfered Clients that have Valid Employee Numbers, Not "0"
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <div class="scrollContentContainer">
                                <asp:GridView ID="grvClientsExists" runat="server" AllowSorting="True" 
                                    AutoGenerateColumns="False" BackColor="White" BorderColor="#336666" 
                                    BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                                    DataKeyNames="AccountRequestNum, client_num" EmptyDataText="No Items Found" 
                                    EnableTheming="False" Font-Size="Medium" GridLines="Horizontal" Width="100%">
                                    <FooterStyle BackColor="White" ForeColor="#333333" />
                                    <HeaderStyle BackColor="#336666" Font-Bold="False" Font-Size="Small" 
                                        ForeColor="White" HorizontalAlign="Left" />
                                    <PagerStyle BackColor="#336666" Font-Size="Medium" ForeColor="White" 
                                        HorizontalAlign="Left" />
                                    <RowStyle BackColor="White" ForeColor="#333333" />
                                    <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                    <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                    <SortedAscendingHeaderStyle BackColor="#487575" />
                                    <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                    <SortedDescendingHeaderStyle BackColor="#275353" />
                                    <Columns>
                                        <asp:CommandField ButtonType="Button" SelectText="Select" 
                                            ShowSelectButton="true" />
                                        <asp:BoundField DataField="first_name" HeaderStyle-HorizontalAlign="Left" 
                                            HeaderText="First Name" ItemStyle-HorizontalAlign="Left" />
                                        <asp:BoundField DataField="last_name" HeaderStyle-HorizontalAlign="Left" 
                                            HeaderText="Last Name" ItemStyle-HorizontalAlign="Left" />
                                        <asp:BoundField DataField="siemens_emp_num" HeaderStyle-HorizontalAlign="Left" 
                                            HeaderText="Employee#" ItemStyle-HorizontalAlign="Left" />
                                    </Columns>
                                </asp:GridView>       
                            </div> 
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <table>
                                <tr>
                                    <td align="center" colspan="4" style="background-color: #CCCCCC; font-size: large; font-weight: bold">
                                        <asp:Label ID="lblCloseExists" runat="server" Text="Continue with SignOn Form Submission or Select a Current Client.">
                                        </asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <asp:Label runat="server" ID="lblblank" Height="30px"></asp:Label>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="center">
                                        <asp:Button ID="btnCloseExists" runat="server" Font-Bold="True" 
                                            Font-Names="Arial" Font-Size="Small" Height="30px" Text="Continue Submit Request" Width="225px" />
                                    </td>
                                    <td>
                                        <asp:Button id="btnReturnExists" runat="server" Font-Bold="True" 
                                            Font-Names="Arial" Font-Size="Small" Height="30px" Text="Return To Form " Width="225px" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
        </asp:Panel>
                        
     <ajaxToolkit:ModalPopupExtender ID="ClientExists" runat="server" 
       TargetControlID="hdnBtnExists" PopupControlID="pnlClientExists" 
       BackgroundCssClass="ModalBackground">  
        </ajaxToolkit:ModalPopupExtender>
        

      <asp:Button ID="hdnBtnExists" runat="server" Style="display: none" />

 </ContentTemplate>
</asp:UpdatePanel>
</asp:Content>

