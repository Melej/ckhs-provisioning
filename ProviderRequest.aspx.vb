﻿Imports System.IO
Imports System.Data '
Imports System.Data.SqlClient
Imports App_Code
Imports AjaxControlToolkit
Imports C1.Web.UI.Controls.C1Window
Imports System.Net
Imports System.Net.Mail
Imports System.Net.Mail.SmtpClient
Partial Class ValidateRequest
    Inherits MyBasePage '   Inherits System.Web.UI.Page


    Dim sActivitytype As String = ""
    Dim cblBinder As New CheckBoxListBinder
    ' Dim AccountRequest As AccountRequest

    Dim FormViewRequest As New FormSQL()
    Dim AccountRequestNum As Integer
    Dim AccountRequestType As String
    Dim Mailobj As System.Net.Mail.SmtpClient = New System.Net.Mail.SmtpClient
    Dim Mailmsg As New System.Net.Mail.MailMessage
    Dim MailAddress As System.Net.Mail.MailAddress



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        FormViewRequest = New FormSQL(Session("EmployeeConn"), "SelAccountRequestV20", "InsAccountRequestV20", "", Page)
        AccountRequestNum = Request.QueryString("RequestNum")

        If Session("SecurityLevelNum") < 90 Then
            btnValidate.Enabled = False
            btnInValid.Enabled = False
        End If


        If Not IsPostBack Then

            FormViewRequest.AddSelectParm("@account_request_num", AccountRequestNum, SqlDbType.Int, 4)
            FormViewRequest.FillForm()


            AccountRequestType = FormViewRequest.getFieldValue("account_request_type")

            first_namelabel.Text = FormViewRequest.getFieldValue("first_name")

            If AccountRequestType = "rehire" Then
                lblRehire.Visible = True
                lblRehire.Text = "THIS IS A REHIRE EMPLOYEE IF POSSIBLE USE SAME Doctor Number"

            End If


        End If

        ' btnRequestDetails.Attributes.Add("onclick", "newPopup('RequestDetail.aspx?RequestNum=" & AccountRequestNum & " ')")

    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("./providerQueue.aspx")

    End Sub
    Protected Sub btnValidate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnValidate.Click
        If doctor_master_numTextBox.Text = "" Then
            lblValidation.Text = "Must supplpy and Doctor Master number"
            Exit Sub
        End If


        Dim cmd As New CommandsSqlAndOleDb("UpdProviderDocMaster", Session("EmployeeConn"))
        cmd.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int, 8)
        cmd.AddSqlProcParameter("@nt_login", Session("LoginID"), SqlDbType.NVarChar, 50)
        cmd.AddSqlProcParameter("@doctor_master_num", doctor_master_numTextBox.Text, SqlDbType.NVarChar, 20)
        cmd.AddSqlProcParameter("@validate", "validated", SqlDbType.NVarChar, 10)


        cmd.ExecNonQueryNoReturn()

        sendAncillaryEmail()



        Response.Redirect("./ProviderQueue.aspx")

    End Sub
    Protected Sub btnInValid_Click(sender As Object, e As System.EventArgs) Handles btnInValid.Click
        'ButLoginCancel.Text = "Cancel" '& vbCrLf & "Basic View"
        With pnlInvalidRequest
            .Visible = True
            With .Style
                .Add("LEFT", "200px")
                .Add("TOP", "100px")
                .Add("POSITION", "absolute")
                .Add("Z-INDEX", "999")
            End With
        End With
        pnlInvalidRequest.Visible = True

        'btnCancelRequest_ConfirmButtonExtender.ConfirmText = "Are You Sure!!!"


    End Sub

    Protected Sub btnInvalidSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnInvalidSubmit.Click

        Dim cmd As New CommandsSqlAndOleDb("UpdProviderDocMaster", Session("EmployeeConn"))
        cmd.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int, 8)
        cmd.AddSqlProcParameter("@nt_login", Session("LoginID"), SqlDbType.NVarChar, 50)
        cmd.AddSqlProcParameter("@doctor_master_num", doctor_master_numTextBox.Text, SqlDbType.NVarChar, 20)
        cmd.AddSqlProcParameter("@comments", txtInvalidComments.Text, SqlDbType.NVarChar, 200)
        cmd.AddSqlProcParameter("@validate", "invalid", SqlDbType.NVarChar, 10)


        cmd.ExecNonQueryNoReturn()


        Response.Redirect("./ProviderQueue.aspx")


    End Sub

    Protected Sub btnRequestDetails_Click(sender As Object, e As System.EventArgs) Handles btnRequestDetails.Click
        Response.Redirect("./AccountRequestInfo2.aspx?RequestNum=" & AccountRequestNum, True)
    End Sub
    Protected Sub btnCloseNav_Click(sender As Object, e As System.EventArgs) Handles btnCloseNav.Click
        pnlInvalidRequest.Visible = False

    End Sub
    Private Sub sendAncillaryEmail()
        Dim Request As New AccountRequest(AccountRequestNum, Session("EmployeeConn"))
        Dim RequestItems As New RequestItems(AccountRequestNum)
        Dim EmailBody As New EmailBody

        Dim sSubject As String
        Dim sBody As String

        Dim EmailList As String
        Dim sAppbase As String = ""
        Dim Link As String

        Dim sAccounts As String = ""

        If Session("environment").ToString.ToLower = "testing" Then
            'toEmail = "Jeff.Mele@crozer.org"
            Link = "http://webdev3/CSCV2/AccountRequestInfo2.aspx?RequestNum=" & AccountRequestNum.ToString
        Else
            Link = "http://Casemgmt02/CSCV2/AccountRequestInfo2.aspx?RequestNum=" & AccountRequestNum.ToString

        End If

        sSubject = "New Doctor Master Request for " & Request.FirstName & " " & Request.LastName & " Update Ancillary Systems ."


        sBody = "<!DOCTYPE html>" & _
"<html>" & _
"<head>" & _
"<style>" & _
"table" & _
"{" & _
"border-collapse:collapse;" & _
"font-family: 'Helvetica Neue', 'Lucida Grande', 'Segoe UI', Arial, Helvetica, Verdana, sans-serif;" & _
"}" & _
"table, th, td" & _
"{" & _
"border: 1px solid black;" & _
"padding: 3px" & _
"}" & _
"</style>" & _
"</head>" & _
"" & _
"<body>" & _
"<table cellpadding='5px'>" & _
"<tr>" & _
    "<td colspan='3' align='center'  style='font-size:large'>" & _
"               " & _
    "<b>NEW Provider/Physician Doctor Master</b>" & _
"                 " & _
     "</td>    " & _
"</tr>" & _
"<tr>" & _
    "<td style='font-weight:bold' colspan='1'>Name:</td>" & _
    "<td colspan='3'>" & Request.FirstName & " " & Request.LastName & "</td>" & _
"</tr>" & _
"<tr>" & _
    "<td style='font-weight:bold' colspan='1'>Requested By:</td>" & _
    "<td colspan='3'>" & Request.RequestorName & "</td>" & _
"</tr>" & _
   "<tr>" & _
    "<td style='font-weight:bold' colspan='1'>Requestor Email:</td>" & _
    "<td colspan='3'>" & Request.RequestorEmail & "</td>" & _
"</tr>" & _
"<tr>" & _
    "<td style='font-weight:bold' colspan='1'>Date Requested:</td>" & _
    "<td colspan='3'>" & Request.EnteredDate & "</td>" & _
"</tr>" & _
"      " & _
"     " & _
"<tr style='font-weight:bold'><td>Information </td><td> Data </td></tr>" & _
"<tr><td> First Name </td><td>" & Request.FirstName & "</td></tr>" & _
"<tr><td> Last Name </td><td>" & Request.LastName & "</td></tr>" & _
"<tr><td> Credential Date</td><td>" & Request.CredentialedDate & "</td></tr>" & _
"<tr><td> Address 1 </td><td>" & Request.Address1 & "</td></tr>" & _
"<tr><td> Address 2 </td><td>" & Request.Address2 & "</td></tr>" & _
"<tr><td> City </td><td>" & Request.City & "</td></tr>" & _
"<tr><td> State </td><td>" & Request.State & "</td></tr>" & _
"<tr><td> Zip </td><td>" & Request.Zip & "</td></tr>" & _
"<tr><td> Phone </td><td>" & Request.Phone & "</td></tr>" & _
"<tr><td> Fax </td><td>" & Request.Fax & "</td></tr>" & _
"<tr><td> Doctor Master#</td><td>" & doctor_master_numTextBox.Text & "</td></tr>" & _
"<tr><td> NPI</td><td>" & Request.Npi & "</td></tr>" & _
"<tr><td> LicensesNo</td><td>" & Request.LicensesNo & "</td></tr>" & _
"<tr><td> Specialty</td><td>" & Request.Specialty & "</td></tr>" & _
"<tr><td> Title</td><td>" & Request.Title & "</td></tr>" & _
"</table>" & _
       "</body>" & _
        "</html>"

        sBody = sBody & "<table cellpadding='5px'>" & _
        "<tr>" & _
            "<td align='center'  style='font-size:large'>" & _
"               " & _
            "<b>View Details by clicking on link below</b>" & _
"                 " & _
             "</td>    " & _
        "</tr>" & _
        "<tr>" & _
            "<td align='center'>" & Link & "</td>" & _
        "</tr>"


        'ActionControl.GetAllActions(AccountRequestNum, Session("EmployeeConn"))

        EmailList = New EmailListOne(1000, AccountRequestNum, Session("EmployeeConn")).EmailListReturn


        Dim sToAddress As String = Request.SubmitterEmail

        Dim toList As String()

        toList = EmailList.Split(";")
        If toList.Length = 1 Then
            Mailmsg.To.Add(New System.Net.Mail.MailAddress(toList(0)))
        Else
        End If
        For i = 0 To toList.Length - 1 ' - 2
            If toList(i).Length > 10 Then ' validity check
                Mailmsg.To.Add(New System.Net.Mail.MailAddress(toList(i)))
            End If
        Next

        Mailmsg.From = New MailAddress("CSC@crozer.org")
        Mailmsg.IsBodyHtml = True

        Mailmsg.Subject = sSubject
        Mailmsg.Body = sBody

        Dim smtpClient As New SmtpClient()
        smtpClient.Host = "ah1smtprelay01.altahospitals.com"

        If Session("environment") = "Production" Then
            smtpClient.Send(Mailmsg)
        End If



        Dim thisData As New CommandsSqlAndOleDb("InsAccounteMailTo", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int)
        thisData.AddSqlProcParameter("@ToAddress", EmailList, SqlDbType.NVarChar, 4000)
        thisData.AddSqlProcParameter("@subject", sSubject, SqlDbType.NVarChar, 200)
        thisData.AddSqlProcParameter("@body", sBody, SqlDbType.NVarChar, 4000)

        thisData.ExecNonQueryNoReturn()




    End Sub
End Class
