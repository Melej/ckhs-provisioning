﻿Imports System.IO
Imports System.Data '
Imports System.Data.SqlClient
Imports App_Code
Imports AjaxControlToolkit
Imports C1.Web.UI.Controls.C1Window

Partial Class ApplicationMaintenance
    Inherits MyBasePage '   Inherits System.Web.UI.Page




    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Redirect("~/" & "CaseMaster.aspx")
        Dim ddlBinder As New DropDownListBinder

        If Not IsPostBack Then

            bindApps()

        End If

    End Sub


    Protected Sub btnAddApp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddApp.Click
        Dim dt As New DataTable
        Dim thisdata As New CommandsSqlAndOleDb("InsUpdApplication", Session("EmployeeConn"))
        thisdata.AddSqlProcParameter("@ApplicationDesc", ApplicationDescTextBox.Text, SqlDbType.NVarChar, 50)
        thisdata.AddSqlProcParameter("@AppSys", rblEntityHospital.SelectedValue, SqlDbType.NVarChar, 25)

        thisdata.ExecNonQueryNoReturn()

        AddAppConfirmlabel.Text = "Application Added"

        bindApps()
    End Sub

    Protected Sub bindApps()


        Dim thisData As New CommandsSqlAndOleDb("SelApplicationCodes", Session("EmployeeConn"))

        gvApps.DataSource = thisData.GetSqlDataTable
        gvApps.DataBind()





    End Sub

   

   

    Protected Sub gvApps_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvApps.RowCommand
        If e.CommandName = "Select" Then
            Dim ApplicationNum As String = gvApps.DataKeys(Convert.ToInt32(e.CommandArgument))("ApplicationNum").ToString()
            Dim ApplicationDesc As String = gvApps.DataKeys(Convert.ToInt32(e.CommandArgument))("ApplicationDesc").ToString()


            Dim thisData As New CommandsSqlAndOleDb("InsUpdApplication", Session("EmployeeConn"))
            thisData.AddSqlProcParameter("@ApplicationNum", ApplicationNum, SqlDbType.Int)
            thisData.AddSqlProcParameter("@deactivate", "Remove", SqlDbType.NVarChar)
            thisData.ExecNonQueryNoReturn()

            bindApps()

        End If
    End Sub

  
End Class
