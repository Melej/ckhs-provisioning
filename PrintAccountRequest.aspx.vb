﻿Imports App_Code

Partial Class PrintAccountRequest
	Inherits System.Web.UI.Page
	Dim FormSignOn As New FormSQL
	Dim StatusForm As New FormSQL

	Dim UserInfo As UserSecurity

	Dim AccountRequest As AccountRequest
	Dim ClientInfo As Client3Controller

	Dim RequestNum As Integer
	Dim clientnum As Integer

	Dim ActionList As New List(Of Actions)
	Dim ActionControl As New ActionController()
	Dim c3Controller As New Client3Controller()


	Dim iClientNum As Integer
	Dim iSecurityLevel As Integer
	Dim iTechNum As Integer

	Private Sub PrintAccountRequest_Load(sender As Object, e As EventArgs) Handles Me.Load
		UserInfo = Session("objUserSecurity")
		RequestNum = Request.QueryString("RequestNum")
		clientnum = Request.QueryString("ClientNum")

		nt_login.Text = Session("LoginID")


		WhoclientNumLabel.Text = UserInfo.ClientNum
		entryclientnumLabel.Text = UserInfo.ClientNum
		iTechNum = UserInfo.TechNum
		entrytechnumLabel.Text = UserInfo.TechNum
		SecurityLevelLabel.Text = UserInfo.SecurityLevelNum



		'RequestNum = 220478
		AccountRequest = New AccountRequest(RequestNum, Session("EmployeeConn"))


		ClientInfo = New Client3Controller(clientnum, "no", Session("EmployeeConn"))


		requestnumlabel.Text = RequestNum


        FormSignOn = New FormSQL(Session("EmployeeConn"), "SelAccountRequestV20", "UpdFullAccountRequestV20", "", Page)
        FormSignOn.AddSelectParm("@account_request_num", RequestNum, SqlDbType.Int, 6)


		StatusForm = New FormSQL(Session("EmployeeConn"), "SelClientDemoRequestByNumberV4", "UpdClientDemoObjectByNumberV4", "", Page)
		StatusForm.AddSelectParm("@account_request_num", RequestNum, SqlDbType.Int, 6)

		If Not IsPostBack Then
			FormSignOn.FillForm()
			StatusForm.FillForm()


			first_namelabel.Text = ClientInfo.FirstName
			last_namelabel.Text = ClientInfo.LastName
			'siemensempnumLabel.Text = ClientInfo.SiemensEmpNum
			'ntloginTextBox.Text = ClientInfo.EmpTypeCd
			'userpositiondescTextBox.Text = c3Controller.UserPositionDesc
			entrydatelabel.Text = AccountRequest.EnteredDate

			'e_mailTextBox.Text = c3Controller.EMail
			'DepartmentName.Text = ClientInfo.EntityName & " - " & ClientInfo.DepartmentName
			'DepartmentCd.text = c3Controller.DepartmentCd

			'EntityCd.Text = ClientInfo.EntityCd & " " & ClientInfo.DepartmentCd

			submittedameLabel.Text = AccountRequest.SubmitterName
			positionlabel.Text = AccountRequest.UserPositionDesc
			Requesttypebl.Text = AccountRequest.AccountRequestType
			'clientnumLabel.Text = AccountRequest.clientnum
			'currenttechnumLabel.Text = AccountRequest.currenttechnum
			'currentgroupnumLabel.Text = AccountRequest.currentgroupnum
			'completiondateTextBox.Text = AccountRequest.completiondate
			'opentimelb.Text = AccountRequest.TotalAcknowledgeTime
			'hdPriorityLb.Text = AccountRequest.priority
			'hdexists.Text = AccountRequest.Surveyexists
			'RequestTypeLabel.Text = AccountRequest.requesttype

			Dim status As String

			'status = AccountRequest.currentacktime

			Dim ddlBinder As New DropDownListBinder
			Dim ddlTypeBinder As New DropDownListBinder

			ddlBinder.BindData(entitycdDDL, "selEntityCodesV4", "entity_cd", "hosp_desc", Session("EmployeeConn"))




			If entity_cdLabel.Text = "" Then
				entitycdDDL.SelectedIndex = -1
			Else
				Dim entitycd As String = entity_cdLabel.Text

				entitycd = entitycd.Trim
				entitycdDDL.SelectedValue = entitycd

			End If


			Dim ddbDepartmentBinder As New DropDownListBinder(departmentcdddl, "selDepartmentCodesV4", Session("EmployeeConn"))
			ddbDepartmentBinder.AddDDLCriteria("@entity_cd", entitycdDDL.SelectedValue, SqlDbType.NVarChar)
			ddbDepartmentBinder.BindData("department_cd", "department_name")

			If department_cdLabel.Text = "" Then
				departmentcdddl.SelectedIndex = -1
			ElseIf department_cdLabel.Text <> "" Then
				Try

					departmentcdddl.SelectedValue = department_cdLabel.Text.Trim
				Catch ex As Exception

					departmentcdddl.SelectedIndex = -1
				End Try

			End If

			ddlBinder.BindData(facilitycdddl, "SelFacilityCodes", "facility_cd", "facility_short_name", Session("EmployeeConn"))


			If facility_cdLabel.Text = "" Then
				facilitycdddl.SelectedIndex = -1
			ElseIf facility_cdLabel.Text <> "" Then
				Try
					facilitycdddl.SelectedValue = facility_cdLabel.Text

				Catch ex As Exception
					facilitycdddl.SelectedIndex = -1

				End Try
			End If


			If building_cdLabel.Text = "" Then
				buildingcdddl.SelectedIndex = -1
			ElseIf building_cdLabel.Text <> "" Then

				Try

					ddlBinder = New DropDownListBinder(buildingcdddl, "SelBuildingByFacility", Session("EmployeeConn"))
					ddlBinder.AddDDLCriteria("@facility_cd", facility_cdLabel.Text, SqlDbType.NVarChar)
					ddlBinder.BindData("building_cd", "building_name")

					buildingcdddl.SelectedValue = building_cdLabel.Text.Trim

				Catch ex As Exception

					buildingcdddl.SelectedIndex = -1

				End Try



			End If

			'If building_cdLabel.Text <> "" And facility_cdLabel.Text <> "" And floorLabel.Text <> "" Then

			'	Try
			'		Dim ddlFloorBinder As DropDownListBinder

			'		ddlFloorBinder = New DropDownListBinder(Floorddl, "SelFloorsByBuilding", Session("EmployeeConn"))
			'		ddlFloorBinder.AddDDLCriteria("@facility_cd", facility_cdLabel.Text, SqlDbType.NVarChar)
			'		ddlFloorBinder.AddDDLCriteria("@building_cd", building_cdLabel.Text, SqlDbType.NVarChar)

			'		ddlFloorBinder.BindData("floor_no", "floor_no")

			'		Floorddl.SelectedValue = floorLabel.Text.TrimEnd


			'	Catch ex As Exception
			'		Floorddl.SelectedValue = -1
			'	End Try
			'Else
			'	Floorddl.SelectedValue = -1

			'End If


			fillActionItemsTable()

		End If



		'ActionControl.GetAllActions(RequestNum, Session("EmployeeConn"))
		'ActionList = ActionControl.AllActions()
		'TPActionItems.Controls.Add(fillActionItemsTable(ActionControl.AllActions()))


	End Sub
	Public Sub fillActionItemsTable()


		Dim dt As New DataTable
		Dim rwHeader As New TableRow

		Dim clActiondateHeader As New TableCell
		Dim clActionCodeDescheader As New TableCell
		Dim clTechHeader As New TableCell
		Dim clDescHeader As New TableCell

		TPActionItems.Dispose()
		TPActionItems.Rows.Clear()

		RequestNum = requestnumlabel.Text

		Dim thisData As New CommandsSqlAndOleDb("SelAccountActionItemsByRequest", Session("EmployeeConn"))
		thisData.AddSqlProcParameter("@account_request_num", RequestNum, SqlDbType.NVarChar)

		Dim iRowCount = 1

		dt = thisData.GetSqlDataTable

		clActionCodeDescheader.Text = "Action Type"
		clActiondateHeader.Text = "Action Date"
		clTechHeader.Text = "Name"
		clDescHeader.Text = "Description"


		rwHeader.Cells.Add(clActionCodeDescheader)
		rwHeader.Cells.Add(clActiondateHeader)
		rwHeader.Cells.Add(clTechHeader)
		rwHeader.Cells.Add(clDescHeader)



		rwHeader.Font.Bold = True
		rwHeader.BackColor = Color.FromName("#006666")
		rwHeader.ForeColor = Color.FromName("White")
		rwHeader.Height = Unit.Pixel(36)

		TPActionItems.Rows.Add(rwHeader)

		For Each drRow As DataRow In dt.Rows


			Dim rwData As New TableRow

			Dim clActionDate As New TableCell
			Dim clActionCodeDesc As New TableCell
			Dim clTechName As New TableCell
			Dim clDescription As New TableCell


			clActionCodeDesc.Text = IIf(IsDBNull(drRow("ActionCodeDesc")), "", drRow("ActionCodeDesc"))
			clActionDate.Text = IIf(IsDBNull(drRow("action_date")), "", drRow("action_date"))
			clTechName.Text = IIf(IsDBNull(drRow("TechName")), "", drRow("TechName"))

			clDescription.Text = IIf(IsDBNull(drRow("action_desc")), "", drRow("action_desc"))



			rwData.Cells.Add(clActionCodeDesc)
			rwData.Cells.Add(clActionDate)
			rwData.Cells.Add(clTechName)
			rwData.Cells.Add(clDescription)


			If iRowCount > 0 Then

				rwData.BackColor = Color.Bisque

			Else

				rwData.BackColor = Color.LightGray

			End If

			TPActionItems.Rows.Add(rwData)

			iRowCount = iRowCount * -1

		Next
		TPActionItems.Font.Name = "Arial"
		TPActionItems.Font.Size = 8

		TPActionItems.CellPadding = 10
		TPActionItems.Width = New Unit("100%")


	End Sub
End Class
