﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="false" CodeFile="DeactivatedAccount.aspx.vb" Inherits="DeactivatedAccount" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">

<style type="text/css">
    .style2
    {
        text-align: center;

    }
    .style13
    {
        height: 30px;
    }
    .style15
    {
        font-size: larger;
    }
    
    .style23
    {
        width: 200px;
    }
    .style25
    {
        width: 100%;
    }
    .textHasFocus
    {
         border-color:Red;
        border-width:medium;
        border-style:outset;
    }
   
    .style26
    {
        width: 91px;
    }
    .style27
    {
        width: 110px;
    }
   
    .style28
    {
        width: 80px;
    }
    .style29
    {
        width: 94px;
    }
   
    .style30
    {
        width: 85px;
    }
   
    .style31
    {
        width: 192px;
    }
    .style32
    {
        width: 206px;
    }
   
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
<asp:UpdatePanel ID="uplDeactivateForm" runat="server" >
  <ContentTemplate>
  <table width="100%">
  <tr align="center" valign="middle">
   <td align="center" valign="middle">

   </td>
   </tr>
       <tr>
        <td align="center">
            <table>
                <tr align="center" >
                    <td align="right">
                        <asp:Label ID="lblusersubmit" runat="server" Text="Current User:" 
                        Width="125px" />
                    </td>

                    <td align="left">
                        <asp:Label ID="userSubmittingLabel" Width="150px" runat="server" Font-Bold="true"></asp:Label>
                    </td>

                    <td align="right">
                        <asp:Label ID="lblcurrRequestor" runat="server" Text="Current Auth.Mgr./Delegate:" Width="200px" Visible="false"></asp:Label>
                    </td>

                    <td align="left">
                        <asp:Label ID="SubmittingOnBehalfOfNameLabel" runat="server" Font-Bold="true" Visible="false" ></asp:Label>
                    </td>
                </tr>

<%--                <tr align="center" >
                    <td>
                    </td>
                    <td align="center" >
                       <asp:Button ID="btnPrintClient" runat="server" Font-Bold="True" 
                            Font-Names="Arial" Font-Size="Small" Height="20px" Text="Print Client" 
                            Width="140px"  />
                    </td>
                    <td align="center">
                        <asp:Button ID="btnSelectOnBehalfOf" runat="server" Font-Bold="True" 
                            Font-Names="Arial" Font-Size="Small" Height="20px" Text="Change  Auth.Mgr./Delegate:" 
                            Width="200px" />
                    </td>
                    <td>
                    </td>
                </tr>--%>

                <tr align="center">
                    <td align="center" colspan="4" >
                        <asp:Label ID = "lblValidation" runat = "server" ForeColor="Red" Font-Bold="true"/>
                    </td>
                </tr>


           </table>

        </td>
    </tr>
    <tr>
      <td>

        <ajaxToolkit:TabContainer ID="tabs" runat="server" Width="100%" style="margin-bottom: 1px" EnableTheming="False" 
                  BackColor="#efefef">

           <ajaxToolkit:TabPanel ID="tpDemographics" runat="server"  BackColor="Gainsboro"  TabIndex="0"
                Visible = "True" EnableTheming="False">
                         <HeaderTemplate > 
                                <asp:Label ID="Label1" runat="server" Text="Demographics" Font-Bold="True" 
                                    ForeColor="Black"></asp:Label>
                         </HeaderTemplate>
                         <ContentTemplate>
                            <table border="0" style="width: 100%">

                            <tr>
                                        <td class="tableRowHeader">
                                        Client Account(s) Information
                                        </td>
                            </tr>
                            <tr>
                                        <td class="tableRowHeader">
                                            <asp:Label ID="ClientnameHeader" runat="server" >
                                            </asp:Label>
                                 </td>
                            </tr>
                            <tr>
          
                                <td class="tableRowSubHeader"  align="left">
                                Form Submission
                                </td>
            
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Label ID="TerminateLabel" runat="server" 
                                     Text="This Account currently has an Open Terminate Request"  Visible="false" Font-Bold="True" ForeColor="#FF3300" />
                                </td>
                            <tr>
                                <td align="center">
                                    <asp:Button ID = "btnUpdate" runat = "server" Text ="Submit Reactivate Request for This Client" Width="280px" 
                                        BackColor="#336666" ForeColor="White" />
              
                                    <asp:Button ID = "btnReactivate" runat = "server" Text ="Reactivate Account(no Request) " Width="175px" 
                                        BackColor="#336666" ForeColor="White" Visible="false"/>
	<%--
                                    <asp:Button ID="btnNonPrivileged" runat="server" Text="No Longer Privileged"  Width="175px" 
                                        BackColor="#336666" ForeColor="White" Visible="false"/>

                                    <asp:Button ID="btnDeactivateClient" runat="server" Text = "De-Activate Client"  Width="175px" 
                                        BackColor="#336666" ForeColor="White" Visible="false" ToolTip="Client has no Accounts and Does not have a Crozer Emp#" />
--%>
                                </td>
                            </tr>

                            <tr>
                                <td>
    <%--                            <asp:Label ID = "submitter_nameLabel" runat="server" Font-Bold="True"></asp:Label>
                                    <asp:Label ID = "requestor_nameLabel" runat="server" Font-Bold="True"></asp:Label>--%>
                                </td>
                            </tr>
                            <tr>
                                <td class="tableRowSubHeader"  align="left">
                                CKHS Affiliation:
                                </td>
                            </tr>
                            <tr>
                                <td  align="center">
                                    <asp:RadioButtonList ID ="emptypecdrbl" runat = "server" 
                                        RepeatDirection="Horizontal" AutoPostBack="True" Font-Size="Smaller" >
                                    </asp:RadioButtonList>
                                </td>
                            </tr>

                            <asp:Panel runat="server" ID="pnlProvider" Visible="False">
                                <tr>
                                <td colspan="4"  class="tableRowSubHeader" align="left">
                                    Provider Information
                                </td>
                                </tr>
                                <tr>
                                <td colspan="4">
                                    <table id="tblprovider"  border="1" width="100%">
                                            

                                    <tr>
                                        <td align="right" >
                                            <asp:Label ID="priviledlbl" runat="server" Text="Privileged Date:" 
                                            ToolTip="Only Credentialing Office will enter this data, Indicates the date the Provider was credentialied on."  Width="150px" />

                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID = "credentialedDateTextBox" runat="server" ToolTip="Only Credentialing Office will enter this data"
                                                Enabled="false"></asp:TextBox>
                                        </td>
<%--                                      <td align="right">
                                          Privileged DCMH Date:
                                      </td>
                                      <td align="left">
                                          <asp:TextBox ID="CredentialedDateDCMHTextBox" runat="server" 
                                               Enabled="false"></asp:TextBox>
                                      </td>--%>

                                        <td align="right">
                                             <asp:Label ID="ccmcadmitlbl" runat="server" Text ="Admitting Rights:" Width="150px" />                                   
                                        </td>
                                        <td align="left" >
                                               <asp:RadioButtonList ID = "CCMCadmitRightsrbl" runat="server" 
                                               AutoPostBack="true" RepeatDirection="Horizontal">
                                                <asp:ListItem>Yes</asp:ListItem>
                                                <asp:listItem>No</asp:listItem>
                                            </asp:RadioButtonList>

                                        </td>
<%--                                        <td align="right">
                                            <asp:Label ID="dcmhlbl" runat="server" Text ="DCMH Admitting Rights:" Width="150px" />
                                         </td>

                                        <td align="left" >
                                               <asp:RadioButtonList ID = "DCMHadmitRightsrbl" runat="server" AutoPostBack="true"
                                               RepeatDirection="Horizontal">
                                                <asp:ListItem>Yes</asp:ListItem>
                                                <asp:listItem>No</asp:listItem>
                                            </asp:RadioButtonList>

                                        </td>--%>

                                    </tr>

                                    <tr>
                                        <td align="right" >
                                            CHMG:
                                        </td>
                                        <td align="left" colspan="3" >
                                            <asp:RadioButtonList ID = "hanrbl" runat="server" RepeatDirection="Horizontal" AutoPostBack="true">
                                                <asp:ListItem>Yes</asp:ListItem>
                                                <asp:ListItem>No</asp:ListItem>            
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
<%--                                    <tr>
                                          <td align="right">
                                                <asp:Label ID="lblReffer" runat="server" Text="Referring Clinician:" Width="150px" />
                              
                                          </td>
                                          <td align="left">
                                              <asp:RadioButtonList ID="ReferringClinicianrbl" runat="server" 
                                                  RepeatDirection="Horizontal">
                                                  <asp:ListItem>Yes</asp:ListItem>
                                                  <asp:ListItem>No</asp:ListItem>
                                              </asp:RadioButtonList>                              
                                          </td>
                                        <td colspan="2">
                                        </td>

                                    </tr>--%>

                                    <tr>
                                        <td align="right">
                                                Doctor Number:
                                        </td>
                                        <td align="left" colspan="3">
                                            <asp:TextBox ID = "DoctorMasterNumTextBox" runat="server" Enabled="false" MaxLength="6"></asp:TextBox>
                                        </td>

                                    </tr>
<%--                                    <tr>
                                        <td>
                                        </td>
                                        <td colspan="3">
                                         <asp:Panel Visible="false" runat="server" ID="docMstrPnl">
                                             <table id="docmst">
                                                <tr>
                                                    <td colspan="2" align="center">
                                                        <asp:Button ID="btnAddDuplicateDcoMaster" runat="server" width="230px" Text="Submit Dup. Doctor Master#" />

                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td align="center">
                                                        <asp:Label ID="AddDupmstlbl" runat="server" Text="Add Another Doctor Master Number:"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="DupDoctorMstrtxt" runat="server"></asp:TextBox>
                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td colspan="2" align="center">
                                                      <div>
                                                        <asp:GridView ID="gvDocmster" runat="server" AllowSorting="True" 
                                                                AutoGenerateColumns="False" BackColor="White" BorderColor="#336666"
                                                                BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                                                                EmptyDataText="No Docmaster # Found" EnableTheming="False" 
                                                                DataKeyNames="client_num"
                                                                Font-Size="Small" GridLines="Horizontal" Width="100%">
                                   
                                                                <FooterStyle BackColor="White" ForeColor="#333333" />
                                                                <HeaderStyle BackColor="#336666" Font-Bold="False" ForeColor="White" Font-Size="Small" HorizontalAlign="Left"    />                                    
                                                                <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Left" Font-Size="Medium"/>
                                                                <RowStyle BackColor="White" ForeColor="#333333" />


                                                                <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                                                <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                                                <SortedAscendingHeaderStyle BackColor="#487575" />
                                                                <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                                                <SortedDescendingHeaderStyle BackColor="#275353" />

                                                                <Columns>
       
				                                                                  
                                  
                                                               </Columns>
                                                             </asp:GridView>
                                                         </div>
                                                
                                                    </td>
                                            
                                                </tr>
                                             </table>
                                     
                                         </asp:Panel>
                                     </td>
                                            
                                    </tr>--%>
                                    <tr>
                                        <td align="right">
                                            NPI:
                                        </td>
                                        <td  align="left" >
                                            <asp:TextBox ID = "npitextbox" runat = "server" MaxLength="10"></asp:TextBox>
                                        </td>
                                        <td align="right" >
                                            License No.
                                        </td>
                                        <td align="left" >
                                                <asp:TextBox ID = "LicensesNoTextBox" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                          Taxonomy:
                                        </td>
                                        <td align="left" colspan="3" >
                                           <asp:TextBox ID = "taxonomytextbox" runat="server"></asp:TextBox>
                                        </td>

                                    </tr>
                                     <tr>
                                        <td align="right">
                                        <asp:Label ID="lblprov" runat="server" Text = "CHMG:"  Width="150px" Visible="false" />
                                        </td>
                                        <td  align="left">
                                        <asp:RadioButtonList ID = "providerrbl" runat="server" RepeatDirection="Horizontal" Visible="false" >
                                            <asp:ListItem>Yes</asp:ListItem>
                                                <asp:ListItem >No</asp:ListItem>            
                                            </asp:RadioButtonList>
                                        </td>
                                        <td colspan="2">
                                        </td>
                                    </tr>
                                    <tr>
                                      <td colspan="4">
                                       <table border="3px" width="100%">

                                            <tr>
                                            <td colspan="4"  class="tableRowSubHeader" align="center">
                                                Physician Groups 
                                            </td>
                                            </tr>

<%--                                            <tr>
                                                <td align="right">
                                                    Location of Care Email:
                                                </td>
                                                <td colspan="3" align="left">
                                                    <asp:TextBox ID="DirectEmailTextBox" runat="server" Width="275px" Enabled="false"></asp:TextBox>
                                                </td>
                                            </tr>--%>


                                            <tr>
                                                 <td align="right">
                                                   <asp:Label ID ="ckhnlbl" runat="server" Text="CHMG (G#):" Visible="false" >
                                                    </asp:Label>
                                       
                                                 </td>
                                                 <td colspan="3" align="left">
                                                    <asp:DropDownList ID = "LocationOfCareIDddl" runat="server"  Visible="false" AutoPostBack="true" Width="90%">
                                                    </asp:DropDownList>
                                        
                                                 </td>
                                
                                
                                            </tr>

<%--                                            <tr>

                                                    <td align="right">
                                                        Comm. Location Of Care:
                                                    </td>
                                                    <td colspan="3" align="left">
                                                        <asp:DropDownList ID = "CommLocationOfCareIDddl" runat="server" Width="90%">
                                            
                                                        </asp:DropDownList>
                                                
                                                    </td>
                                
                                            </tr>--%>

                                            <tr>
                                                <td align="right" >
                                                   Provider Group Names:
                                                </td>       
                                                <td colspan="3" align="left">
                                                    <asp:DropDownList ID ="group_nameddl"  Width="90%" runat="server">
            
                                                    </asp:DropDownList>
                                                 </td>
                                            </tr>



<%--                                            <tr>
                                                <td  align="right">
                                                    Coverage Group ID:
                                                </td>
                                                <td  colspan="3" align="left">
                                                        <asp:DropDownList ID = "CoverageGroupddl" runat="server" Width="90%">
                                            
                                                        </asp:DropDownList>

                                                </td>
                                            </tr>

                                            <tr>
                                
                                                <td  align="right">
                                                    Neighbor Phys Groups:
                                                </td>
                                                <td  colspan="3" align="left">
                                                        <asp:DropDownList ID = "Nonperferdddl" runat="server" Width="90%">
                                            
                                                        </asp:DropDownList>
                                                </td>
                                
                                            </tr>

                                            <tr>

                                                 <td align="right">
                                                      CKHN Copy To MedAssist.:
                                                 </td>
                                                 <td>
                                                    <asp:TextBox ID = "CopyToTextBox" runat="server"></asp:TextBox>
                                                 </td>

                                                 <td align="right">
                                                        Comm. Copy To MedAssist.:
                                                 </td>
                                                 <td>
                                                        <asp:TextBox ID = "CommCopyToTextBox" runat="server"></asp:TextBox>
                                                 </td>
                                            </tr>--%>

                                        </table>
                                    </td>
                                    </tr>


                                 <tr>
                                    <td align="right">
                                    Title:
                                    </td>
                                    <td align="left" >
                                        <asp:dropdownlist ID = "Titleddl" runat="server" AutoPostBack="True"></asp:dropdownlist>
                                         <asp:Label ID="Title2Label" runat="server" Width="250px" Visible="false"> </asp:Label>

                                    </td>
                                    <td  align="right" >
                                    Specialty:
                                    </td>
                                    <td align="left" >
                                        <asp:dropdownlist ID = "Specialtyddl" runat="server" AutoPostBack="true" ></asp:dropdownlist>
                                         <asp:Label ID = "SpecialtyLabel" runat="server" Visible ="False"></asp:Label>

                                    </td>
                                </tr>
<%--                                <tr>
                                    <td align="right" >
                                        Non-CKHN Location:
                                    </td>
                                    <td align="left" colspan="3">
                                            <asp:TextBox ID = "NonHanLocationTextBox" runat="server" Width="500px"></asp:TextBox>
                                    </td>

                                </tr>--%>

    <%--                              <tr>
                                      <td align="right">
                                          Courtesy Staff C.T.S.:
                                      </td>
                                      <td align="left">
                                        <asp:RadioButtonList ID = "CourtesyStaffrbl" runat="server" RepeatDirection="Horizontal">
                                                <asp:ListItem>Yes</asp:ListItem>
                                                <asp:listItem>No</asp:listItem>
                                            </asp:RadioButtonList>
                                      </td>
                                      <td align="right">
                                          Courtesy Staff DCMH:
                                      </td>
                                      <td align="left">
                                        <asp:RadioButtonList ID = "CourtesyStaffDCMHrbl" runat="server" RepeatDirection="Horizontal">
                                                <asp:ListItem>Yes</asp:ListItem>
                                                <asp:listItem>No</asp:listItem>
                                            </asp:RadioButtonList>
                                      </td>

                                  </tr>
    --%>
   
                                        <%--<tr>
                                    <td align="right">
                                        CCMC Consults:
                                    </td>
                                    <td align="left" >
                                           <asp:RadioButtonList ID = "CCMCConsultsrbl" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Yes</asp:ListItem>
                                            <asp:listItem>No</asp:listItem>
                                        </asp:RadioButtonList>

                                    </td>

                                    <td align="right">
                                        Springfield Consults:
                                    </td>
                                    <td align="left" >
                                           <asp:RadioButtonList ID = "SpringfieldConsultsrbl" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Yes</asp:ListItem>
                                            <asp:listItem>No</asp:listItem>
                                        </asp:RadioButtonList>

                                   </td>

                                </tr>
                                <tr>
                                    <td align="right">
                                        Taylor Consults:
                                    </td>
                                    <td align="left" >
                                           <asp:RadioButtonList ID = "TaylorConsultsrbl" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Yes</asp:ListItem>
                                            <asp:listItem>No</asp:listItem>
                                        </asp:RadioButtonList>

                                    </td>
                                    <td align="right">
                                        DCMH Consults:
                                    </td>
                                    <td align="left" >
                                           <asp:RadioButtonList ID = "DCMHConsultsrbl" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Yes</asp:ListItem>
                                            <asp:listItem>No</asp:listItem>
                                        </asp:RadioButtonList>

                                   </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        Write Orders for C.T.S. Bedded Patients:
                                    </td>
                                        <td align="left" >
                                            <asp:RadioButtonList ID = "Writeordersrbl" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Yes</asp:ListItem>
                                            <asp:listItem>No</asp:listItem>
                                        </asp:RadioButtonList>

                                    </td>

                                    <td align="right">
                                        Write Orders for DCMH Bedded Patients:
                                    </td>
                                        <td align="left" >
                                            <asp:RadioButtonList ID = "WriteordersDCMHrbl" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Yes</asp:ListItem>
                                            <asp:listItem>No</asp:listItem>
                                        </asp:RadioButtonList>

                                    </td>
                                </tr>

                                <tr>
                                   <td align="right">
                                        Medicaid ID:
                                   </td>
                                   <td align="left">
                                      <asp:TextBox ID="MedicareIDTextBox" runat="server" Width="250px"></asp:TextBox>
                                   </td>

                                   <td align="right">
                                        BlueCross ID:
                                   </td>
                                   <td align="left">
                                      <asp:TextBox ID="BlueCrossIDTextBox" runat="server" Width="250px"></asp:TextBox>
                                   </td>

                                </tr>

                                <tr>
                                   <td align="right">
                                        DEA ID:
                                   </td>
                                   <td align="left">
                                      <asp:TextBox ID="DEAIDTextBox" runat="server" Width="250px"></asp:TextBox>
                                   </td>

                                   <td align="right">
                                        DEA Extension:
                                   </td>
                                   <td align="left">
                                      <asp:TextBox ID="DEAExtensionIDTextBox" runat="server" Width="250px"></asp:TextBox>
                                   </td>

                                </tr>
                                <tr>
                                   <td align="right">
                                        Medic Group ID:
                                   </td>
                                   <td align="left">
                                      <asp:TextBox ID="MedicGroupIDTextBox" runat="server" Width="250px"></asp:TextBox>
                                   </td>

                                   <td align="right">
                                        Medic Group Phys ID:
                                   </td>
                                   <td align="left">
                                      <asp:TextBox ID="MedicGroupPhysIDTextBox" runat="server" Width="250px"></asp:TextBox>
                                   </td>

                                </tr>
                                <tr>
                                    <td align="right">
                                        Uniform Physician ID:
                                    </td>
                                     <td align="left">
                                     <asp:TextBox ID="UniformPhysIDTextBox" runat="server" Width="250px"></asp:TextBox>
                                   </td>
                                    <td align="right">  
                                        Sure Script ID:
                                   </td>
                                   <td align="left">
                                      <asp:TextBox ID="SureScriptIDTextBox" runat="server" Width="250px"></asp:TextBox>
                                   </td>

                                </tr>
--%>

                            </table>
                                </td>
                                </tr>
                            </asp:Panel> 
 
                                <tr>
                                <td>
                                    <asp:Panel ID="pnlTCl" runat="server" Visible="false">
                                     <tr>
                                       <td>
                                        <table  id="tbltcl" border="3px" width="100%">
                                          <tr>
                                                <td align="left" class="tableRowSubHeader" colspan="4">
                                                    TCL / User Type
                                                </td>
                                            </tr>
                                          <tr>
                                            <td align="right">
                                                    <asp:Label ID="LblTCL" runat="server" Text="TCL/ Invision User Type:" />
                                            </td>
                                            <td colspan="3">
                
                                                    <asp:TextBox ID="TCLTextBox" runat="server" Visible="false"  Enabled="false" Width="110px"></asp:TextBox>
                                                    <asp:TextBox ID="UserTypeCdTextBox" runat="server" Visible="false" Enabled="false" Width="110px"></asp:TextBox>

                                                    <asp:TextBox ID="UserTypeDescTextBox" runat="server" Visible="false" Enabled="false" Width="300px"></asp:TextBox>
        
                                            </td>
                                         </tr>
                                         <tr>
                                            <td align="center" colspan="4">
                                                <asp:Button ID="btnChangeTCL" runat="server" Text="Change TCL" Visible="false"  />
                                            </td>
                                         </tr>
                                          <tr>
                                            <td align="center" colspan="4">


                                                <div>
                                                <asp:GridView ID="gvTCL" runat="server" AllowSorting="False"
                                                        AutoGenerateColumns="False" BackColor="White" BorderColor="#336666"
                                                        BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                                                        EmptyDataText="No Docmaster # Found" EnableTheming="False" 
                                                        DataKeyNames="TCL,UserTypeCd,UserTypeDesc" Visible="false"
                                                        Font-Size="Small" GridLines="Horizontal" Width="100%">
                                   
                                                        <FooterStyle BackColor="White" ForeColor="#333333" />
                                                        <HeaderStyle BackColor="#336666" Font-Bold="False" ForeColor="White" Font-Size="Small" HorizontalAlign="Left"    />                                    
                                                        <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Left" Font-Size="Medium"/>
                                                        <RowStyle BackColor="White" ForeColor="#333333" />


                                                        <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                                        <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                                        <SortedAscendingHeaderStyle BackColor="#487575" />
                                                        <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                                        <SortedDescendingHeaderStyle BackColor="#275353" />

                                                        <Columns>
                                                                <asp:CommandField ButtonType="Button" SelectText="Select" ShowSelectButton= "true" HeaderStyle-HorizontalAlign = "Left"  ControlStyle-BackColor="#84A3A3" ItemStyle-Height="30px" />
                                                                <asp:BoundField  DataField="TCL"  HeaderText="TCL"  HeaderStyle-HorizontalAlign = "Left" ItemStyle-Height="35px"></asp:BoundField>
                                                                <asp:BoundField  DataField="UserTypeCd"  HeaderText="User Type"  HeaderStyle-HorizontalAlign = "Left" ItemStyle-Height="35px"></asp:BoundField>
                                                                <asp:BoundField  DataField="UserTypeDesc"  HeaderText="User Type Desc."  HeaderStyle-HorizontalAlign = "Left" ItemStyle-Height="35px"></asp:BoundField>
				                                                                  
                                  
                                                        </Columns>
                                                        </asp:GridView>
                                                    </div>


                                            </td>

                      
                                        </tr>
                                        <tr>
                                               <td>
                                                    <asp:Label ID="EmpDoclbl" runat="server" Text="This Account Doc Master#" Visible="false">
                                                    </asp:Label>
                                                </td>
                                               <td align="left" colspan="3">
                                                    <asp:TextBox ID = "EmpDoctorTextBox" runat="server" MaxLength="6" Visible="false" ></asp:TextBox>
                                              </td>
                                            </tr>


                                        </table>             
                                      </td>
                                     </tr>
                                   </asp:Panel>                            
                            
                                </td>
                            </tr>
                            <tr>
                            <td>
                                <table id="tbldemo" border="1px" width="100%">
                                <tr>
                                <td colspan="4" class="tableRowSubHeader"  align="left">
                                    Demographics
                                </td>
                                </tr>
                                <tr>

                                <td colspan="4" >
                                    <table class="style3" > 
                                    <tr>
                                        <td>
                                            First Name:
                                        </td>
                                        <td align="left"  >
                                            <asp:TextBox width="280px" ID = "firstnameTextBox" runat="server" ></asp:TextBox>
                                        </td>
                                        <td>
                                            MI:
                                        </td>
                                        <td>
                                            <asp:TextBox ID = "miTextBox" runat="server" Width="15px"></asp:TextBox>
                                        </td>
                                        <td align="left" >
                                            Last Name:
                                        </td>

                                        <td align="left" >
                                            <asp:TextBox ID = "lastnameTextBox" width="280px" runat="server"></asp:TextBox>
                                        </td>
                                        <td>
                                            Suffix: 
                                        </td>
                                        <td>
                                            <asp:dropdownlist id="suffixddl" runat="server" Height="20px" Width="52px">
                                                </asp:dropdownlist>
                                        </td>
                                    </tr>
                                    </table>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="right">
                                        <asp:Label ID="Emplbl" runat="server" Text="Employee #" ></asp:Label>
                                    </td>

                                    <td>
                                        <asp:TextBox ID ="SiemensEmpNumTextBox" runat="server" Enabled="false"></asp:TextBox>
                                    </td>
                                    <td align="right">
                                        <asp:Label ID="siemdesclbl" runat="server" Text="Position:"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label id="userpositiondescLabel" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="netlbl" runat="server" Text="Network Logon:" >
                                        </asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID = "loginnameTextBox" runat="server"></asp:TextBox>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                <td align="right" width="20%" >
                                    <asp:Label ID="lblpos" runat="server" Text="Position Description:" />
                                </td>
                                <td colspan = "3" align="left" >
                                        <asp:DropDownList ID="Rolesddl" runat="server"  AutoPostBack="true"  >
                                    
                                        </asp:DropDownList>

                                        <asp:TextBox ID="PositionRoleNumTextBox"  runat="server" Width="50px" Visible="false"></asp:TextBox>
                                        <asp:TextBox ID="PositionNumTextBox"  runat="server" Width="50px" Visible="false"></asp:TextBox>
                                        <asp:TextBox ID="userpositiondescTextBox" runat="server" Width = "500px" Visible="false"></asp:TextBox>

                                </td>
                                </tr>
                                <tr>
                                <td align="right" >
                                Entity:
                                </td>
                                <td align="left">
                                <asp:DropDownList ID ="entitycdDDL" runat="server" AutoPostBack ="True" >
            
                                </asp:DropDownList>
                                </td>
       
                                <td align="right" >
                                    <asp:Label ID="lbldept" runat="server" Text = "Department Name:" />
                                </td>
                                <td align="left"  >
                                <asp:DropDownList ID ="departmentcdddl" runat="server" Width="250px" AutoPostBack="True">
            
                                </asp:DropDownList>
                                </td>
                            </tr>
                                <tr>
                                <td  align="right">
                                    Address 1:
                                    </td>
                                <td align="left"> 
                                        <asp:TextBox ID = "address1textbox" runat="server" Width="250px"></asp:TextBox>
                                    </td>
                                <td align="right" >
                                    Address 2:
                                    </td>
                                <td align="left" >
                                    <asp:TextBox ID = "address2textbox" runat="server" Width="250px"></asp:TextBox>
                                    </td>

                            </tr>
                                <tr>
                                    <td align="right">
                                        <asp:Label id="lblcity" runat="server" width="50px"  text="City:" />
                                    </td>
                                    <td align="left" colspan="3">
                                        <asp:TextBox ID = "citytextbox" runat="server" width="500px"></asp:TextBox>
                                    </td>


                                </tr>

                                <tr>

                                    <td align="right">
                                        <asp:Label ID="lblstate" runat="server" Text="State:" Width="50px" />
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID = "statetextbox" runat="server" Width="50px"></asp:TextBox>
                                        </td>
                                    <td align="right" >

                                        <asp:Label ID="lblzip" runat="server" Text="Zip:" Width="25px" />
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID = "ziptextbox" runat="server" Width="50px"></asp:TextBox>
                                    </td>
                            </tr>
                                <tr>
                                <td align="right"  >
                                Phone Number:
                                </td>
                                <td align="left" >
                                <asp:TextBox ID = "phoneTextBox" runat="server" MaxLength="12"></asp:TextBox>
                                </td>
       
                                <td align="right" >
                                Pager Number:
                                </td>
                                <td align="left" >
                                <asp:TextBox ID = "pagerTextBox" runat="server" MaxLength="12"></asp:TextBox>
                                </td>
                            </tr>

                                <tr>
                                <td align="right" >
                                Fax:
                                </td>
                                <td align="left" >
            
                                    <asp:TextBox ID = "faxtextbox" runat="server" MaxLength="12"></asp:TextBox>
                                    </td>
         
                                    <td align="right" >
                                Email:
                                </td>
                                <td align="left" >
                                <asp:TextBox ID = "emailTextBox" runat="server" Width="250px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" >
                                Location:
                                </td>
                                <td align="left" >
         
                                    <asp:DropDownList ID ="facilitycdddl" runat="server" AutoPostBack="True">
              
                                </asp:DropDownList>
          
                                </td>
        
                                <td align="right" >
                                Building:
                                </td>
                                <td align="left" >

                                    <asp:DropDownList ID ="buildingcdddl" runat="server" AutoPostBack="True">
            
                                </asp:DropDownList>
          
                                </td>
                            </tr>
                            <tr>
                                <td align="right" >
                                    Floor:
                                </td>

                                <td align="left">
                                    <asp:DropDownList ID="Floorddl" runat="server" AutoPostBack="True">
            
                                    </asp:DropDownList>
                            
                                </td>
                                <td>
                            
                                </td>

                                <td>
                            
                                </td>

                            </tr>
                         
                             <tr>
                                <td colspan="4">
                                   <table>
                                        <tr>
                                            <td>
                                                <asp:Label ID="otherVendorlbl" runat="server" Visible="false" Text="Unknow Vendor/Contractor:"></asp:Label>
                                            </td>
                                            <td align="left" colspan="3">
                                                <asp:Label ID="VendorUnknowlbl" runat="server" Visible="False"></asp:Label>
                                            </td>
                                    
                                        </tr>

                                      <tr>
                                          <td align="right" >
                                              <asp:Label ID="lblVendor" runat="server" Text="Contractor/Vendor Name:"></asp:Label>
                                            </td>
                                          <td align="left"  colspan="3">
                                                <asp:TextBox ID ="VendorNameTextBox" runat="server" Enabled="false" Width="500px" ></asp:TextBox>
                                                  <asp:DropDownList ID="Vendorddl" runat="server" Width="500px" Visible="false" AutoPostBack="true">
                                
                                                </asp:DropDownList>
                                                <asp:Label ID="VendorNumLabel" runat="server" Visible="False"></asp:Label>

                                          </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                             <tr>
                                <td align="right" >
                                Start Date:
                                </td>
                                <td align="left" >
                                    <asp:TextBox ID ="startdateTextBox" runat="server" CssClass="calenderClass"></asp:TextBox>
                                </td>
       
                                <td align="right" >
                                End Date:
                                </td>
                                <td align="left" >
                                    <asp:TextBox ID="enddateTextBox" runat="server" CssClass="calenderClass"></asp:TextBox>
                                </td>
                                </tr>
                               <tr>
                                    <td colspan = "4">
                                        <asp:Label ID ="departmentcdLabel" runat="server" Visible="False"></asp:Label>
                                         <asp:Label ID ="TitleLabel" runat="server" Visible="False"></asp:Label>
                                         <asp:TextBox ID = "buildingcdTextBox" runat="server" Visible ="False"></asp:TextBox>
                                        <asp:Label ID = "emptypecdLabel" runat="server" Visible="false"></asp:Label>
                                         <asp:Label ID = "facilitycdLabel" runat="server" Visible ="False"></asp:Label>
                                         <asp:Label ID = "entitycdLabel" runat="server" Visible ="False"></asp:Label>
                                         <asp:Label ID= "buildingnameLabel" runat="server" Visible="false" ></asp:Label>
                                         <asp:Label ID="HDemployeetype" runat="server" Visible="false"></asp:Label> 
                                         <asp:Label ID="suffixlabel" runat="server" Visible="false"></asp:Label>
                                         <asp:Label ID= "AccountRequestTypeLabel" runat="server" Visible="false"></asp:Label>
                                         <asp:Label ID="RoleNumLabel" runat="server" Visible="false"></asp:Label>
                                         <asp:Label ID="userDepartmentCD"  runat="server" Visible="False"></asp:Label>
                                         <asp:Label ID="SelectedChgRequestor" runat="server" Visible="false"></asp:Label>
                                         <asp:TextBox ID="FloorTextBox" runat="server" Visible="false"></asp:TextBox>
                                        <asp:Label ID="submitter_client_numLabel" runat="server" visible="false" />
                                        <asp:Label ID="requestor_client_numLabel" runat="server" visible="false"></asp:Label>
                                        <asp:Label ID="LocationOfCareIDLabel"  runat="server" visible="false"></asp:Label>
                                        <asp:Label ID ="CoverageGroupNumLabel"  runat="server" visible="false"></asp:Label>
                                        <asp:Label id="NonPerferedGroupnumLabel" runat="server" Visible="false"></asp:Label>

                                        <asp:Label ID="CommLocationOfCareIDLabel" runat="server" visible="false"></asp:Label>
                                         <asp:TextBox ID="groupnameTextBox" runat="server" Width="500px" Visible="false"></asp:TextBox>
                                         <asp:Label ID="PositionRoleNumLabel" runat="server" Visible="false"></asp:Label>
                                         <asp:Label id="invisionLbl" runat="server" Text="eCare Group Number:"   Visible="false"></asp:Label>
                                         <asp:TextBox ID="groupnumTextBox" runat="server" Enabled="false" Visible="false"></asp:TextBox>
                                         <asp:TextBox ID="InvisionGroupNumTextBox" runat="server"  Enabled="false" Visible="false"></asp:TextBox>
                                         <asp:TextBox ID="RehireRequestNumTextBox" runat="server"  Enabled="false" Visible="false"></asp:TextBox>
                                         <asp:TextBox ID="RehireAccountReqnumTextBox" runat="server"  Enabled="false" Visible="false"></asp:TextBox>

                                     </td>
                                </tr>
                                </table>
                            </td>
                            </tr>
                           </table>
                         </ContentTemplate>
                      </ajaxToolkit:TabPanel>


           <ajaxToolkit:TabPanel ID="tpAccounts" runat="server" TabIndex="1" BorderStyle="Solid" BorderWidth="1px">
                <HeaderTemplate > 
                    <asp:Label ID="Label2" runat="server" Text="Accounts" Font-Bold="true" ForeColor="Black" ></asp:Label>
                </HeaderTemplate>
             <ContentTemplate>
              <table width="100%" border="1"  bgcolor="#CCCCCC">
                                                 
                            <tr style="background-color:#FFFFCC; font-weight:bold">
                                <td align="center" >
                                Accounts
                                </td>
                                                        
                            </tr>
                            <tr>
                            <td  style="width: 100%">
                                <asp:GridView ID="gvCurrentUserAccounts" runat="server" AllowSorting="True" 
                                    AutoGenerateColumns="False" BackColor="White" BorderColor="#336666"
                                    BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                                    EmptyDataText="No Applications Found" EnableTheming="False" DataKeyNames="ApplicationNum, ApplicationDesc, login_name, create_date"
                                    Font-Size="Medium" GridLines="Horizontal" Width="100%">
                                   
                                        <FooterStyle BackColor="White" ForeColor="#333333" />
                                        <HeaderStyle BackColor="#336666" Font-Bold="False" ForeColor="White" Font-Size="Small" HorizontalAlign="Left"    />                                    
                                        <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Left" Font-Size="small"/>
                                        <RowStyle BackColor="White" ForeColor="#333333" />
                                        <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                        <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                        <SortedAscendingHeaderStyle BackColor="#487575" />
                                        <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                        <SortedDescendingHeaderStyle BackColor="#275353" />

                                        <Columns>
                                                                                                                                                                                                             
                                                    <asp:BoundField  DataField="ApplicationDesc"  HeaderText="Application" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
				                                    <asp:BoundField  DataField="login_name" HeaderText="User Name" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
                                                    <asp:BoundField  DataField="create_date" HeaderText="Date Created" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
                                                    <asp:BoundField  DataField="CreatedBy" HeaderText="Created By" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
                                                    <asp:BoundField  DataField="acct_term_date" HeaderText="Term Date " HeaderStyle-HorizontalAlign="Left"></asp:BoundField>

                                                    
                                        </Columns>

                                    </asp:GridView>
		
                                </td>
                             </tr>
                         </table>
             </ContentTemplate>
           </ajaxToolkit:TabPanel>

                 <ajaxToolkit:TabPanel ID="tpOpenRequest" runat="server" TabIndex="2" BorderStyle="Solid" BorderWidth="1px">
                      <HeaderTemplate > 
                          <asp:Label ID="Label4" runat="server" Text="Open Requests" Font-Bold="true" ForeColor="Black" ></asp:Label>
                     </HeaderTemplate>
                      <ContentTemplate>
                          <table border="0" width = "100%">
                            <tr>
                                <td colspan = "4">
                                      <asp:Table ID = "tblOpenRequests" runat = "server" Font-Size="Small">
                                     </asp:Table>
            
                                </td>
        
                            </tr>
                           </table>
                       </ContentTemplate>
                  </ajaxToolkit:TabPanel>

                   <ajaxToolkit:TabPanel ID="tpClosedRequest" runat="server" TabIndex="3" BorderStyle="Solid" BorderWidth="1px">
                       <HeaderTemplate > 
                          <asp:Label ID="Label3" runat="server" Text="Closed Requests" Font-Bold="true" ForeColor="Black" ></asp:Label>
                     </HeaderTemplate>
                      <ContentTemplate>
                          <table border="0" width = "100%">
                            <tr>
                                <td colspan = "4">
                                      <asp:Table ID = "tblClosedRequest" runat = "server" Font-Size="Small">
                                     </asp:Table>
            
                                </td>
        
                            </tr>
                           </table>
                       </ContentTemplate>
                   </ajaxToolkit:TabPanel>
                        
           <ajaxToolkit:TabPanel ID="tpRequestItems" runat="server"  Visible="True" TabIndex="4" BorderStyle="Solid" BorderWidth="1px">
             <HeaderTemplate>
              <asp:Label ID="Label6" runat="server" Font-Bold="true" ForeColor="Black" 
                Text="RFS/Tickets"></asp:Label>
             </HeaderTemplate>
            <ContentTemplate>
              <table border="0" width="100%">
                <tr>
                    <td colspan="4">
                        <asp:Table ID="tblRFSTickets" runat="server">
                        </asp:Table>
                    </td>
                </tr>
            </table>
             </ContentTemplate>
          </ajaxToolkit:TabPanel>

        </ajaxToolkit:TabContainer>
     </td>
   </tr>
 </table>

  	<%-- =================== Clients exists ============================================================================--%>
         <asp:Panel ID="pnlReactivateClient" runat="server"  style="display:none" Width="70%" BorderColor="Black" BackColor="#999999" Height="80%">
                <table id="tblClientEx" runat="server" style="border: medium groove #000000; height:80%"  align="center" width="60%" >
                    <tr>
                        <td class="tableRowHeader" colspan="4">
                           This will submit a NEW request to Activate this Client.
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <table>
                                <tr>
                                    <td align="center" colspan="4" style="background-color: #CCCCCC; font-size: large; font-weight: bold">
                                        <asp:Label ID="lblCloseExists" runat="server" Text="All of this clients information will be used to activate this account. ">
                                        </asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <asp:Label runat="server" ID="lblblank" Height="30px"></asp:Label>
                                    </td>

                                </tr>

                                <tr>
                                    <td align="center">
                                        <asp:Button ID="btnSubmitRequest" runat="server" Font-Bold="True" 
                                            Font-Names="Arial" Font-Size="Small" Height="30px" Text="Submit Request" Width="225px" />
                                    </td>
                                    <td>
                                        <asp:Button id="btnReturnExists" runat="server" Font-Bold="True" 
                                            Font-Names="Arial" Font-Size="Small" Height="30px" Text="Cancel " Width="225px" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
        </asp:Panel>
                        
     <ajaxToolkit:ModalPopupExtender ID="ReactivateClient" runat="server" 
       TargetControlID="hdnBtnExists" PopupControlID="pnlReactivateClient" 
       BackgroundCssClass="ModalBackground">  
        </ajaxToolkit:ModalPopupExtender>
        

      <asp:Button ID="hdnBtnExists" runat="server" Style="display: none" />

 </ContentTemplate>
 </asp:UpdatePanel>

</asp:Content>

