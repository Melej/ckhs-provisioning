﻿Imports System.IO
Imports System.Data '
Imports System.Data.SqlClient
Imports App_Code
Imports AjaxControlToolkit
Imports C1.Web.UI.Controls.C1Window
Imports System.Net
Imports System
Imports System.Net.Mail

Partial Class ValidateAddAcctRequest
    Inherits MyBasePage '   Inherits System.Web.UI.Page

    Dim AccountSeqNum As Integer
    Dim sActivitytype As String = ""
    Dim HiddenSeqNum As Integer
    Dim HiddenLogin As String = ""
    Dim HiddenAppnum As Integer

    Dim cblBinder As New CheckBoxListBinder
    Dim FormViewRequest As New FormSQL()
    Dim AccountRequestNum As Integer
    Dim AccountRequestType As String
    Dim dtRequestedAccounts As DataTable


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        FormViewRequest = New FormSQL(Session("EmployeeConn"), "SelAccountRequestV3", "ins_account_request", "", Page)
        AccountRequestNum = Request.QueryString("RequestNum")
        Dim req As New AccountRequest(AccountRequestNum, Session("EmployeeConn"))


        For Each reqitm As RequestItem In req.RequestItems



        Next




        If Not IsPostBack Then

            FormViewRequest.AddSelectParm("@account_request_num", AccountRequestNum, SqlDbType.Int, 4)
            FormViewRequest.FillForm()

            bindAppRequests()
            AccountRequestType = FormViewRequest.getFieldValue("account_request_type")
            UserNameLabel.Text = SuggestedUserName(FormViewRequest.getFieldValue("first_name"), FormViewRequest.getFieldValue("last_name"))

        End If

    End Sub

    Private Function NTRequested() As Boolean
        Dim blnReturn As Boolean = False



        For Each rw As GridViewRow In gvAppRequests.Rows
            If rw.RowType = DataControlRowType.DataRow Then

                Dim ApplicationNum As String = gvAppRequests.DataKeys(rw.RowIndex)("ApplicationNum")
                If ApplicationNum = 9 Then
                    blnReturn = True

                End If

            End If

        Next





        Return blnReturn
    End Function
    Protected Sub bindAppRequests()


        Dim thisData As New CommandsSqlAndOleDb("SelItemsByNumValidCd", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int)
        thisData.AddSqlProcParameter("@valid_cd", "NeedsValidation", SqlDbType.NVarChar, 20)


        dtRequestedAccounts = thisData.GetSqlDataTable

        gvAppRequests.DataSource = dtRequestedAccounts
        gvAppRequests.DataBind()
        If gvAppRequests.Rows.Count < 1 Then
            Response.Redirect("./ValidateQueue.aspx")

        End If


    End Sub

    Protected Sub gvAppRequests_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvAppRequests.RowCommand

        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        HiddenSeqNum = gvAppRequests.DataKeys(e.CommandArgument)("account_request_seq_num")
        HiddenLogin = IIf(IsDBNull(gvAppRequests.DataKeys(e.CommandArgument).Values("login_name")), "", gvAppRequests.DataKeys(e.CommandArgument).Values("login_name"))
        HiddenAppnum = gvAppRequests.DataKeys(e.CommandArgument)("ApplicationNum")

        lblHiddenSeqNum.Text = HiddenSeqNum
        lblHiddenLogin.Text = HiddenLogin

        FillInvalidCodes()


        With PanInvalid
            .Visible = True
            With .Style
                .Add("LEFT", "260px")
                .Add("TOP", "190px")
                .Add("POSITION", "absolute")
                .Add("Z-INDEX", "999")
            End With
        End With


    End Sub
    Private Sub FillInvalidCodes()
        'SelInvalidCodes
        Dim dt As DataTable
        Dim thisData As New CommandsSqlAndOleDb
        thisData = New CommandsSqlAndOleDb("SelInvalidCodes", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@invalidType", "create", SqlDbType.NVarChar, 12)


        dt = thisData.GetSqlDataTable()
        Invalidddl.DataSource = dt
        Invalidddl.DataValueField = "Invalidcd"
        Invalidddl.DataTextField = "InvalidDesc"
        Invalidddl.DataBind()
        thisData = Nothing

    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("./MyAccountQueue.aspx")

    End Sub

    Protected Sub btnValidate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnValidate.Click




        Dim cmd As New CommandsSqlAndOleDb("UpdValidateRequestV20", Session("EmployeeConn"))
        cmd.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int, 6)
        cmd.AddSqlProcParameter("@nt_login", Session("LoginID"), SqlDbType.NVarChar, 20)
        cmd.AddSqlProcParameter("@validate", "Validated", SqlDbType.NVarChar, 20)





        cmd.ExecNonQueryNoReturn()





        If AccountRequestType <> "delacct" And AccountRequestType <> "terminate" Then


            Dim cmdUpdateAccount As New CommandsSqlAndOleDb("UpdAccountRequestV3", Session("EmployeeConn"))
            cmdUpdateAccount.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int, 8)

            cmdUpdateAccount.AddSqlProcParameter("@login_name", UserNameLabel.Text, SqlDbType.NVarChar, 20)



            cmdUpdateAccount.ExecNonQueryNoReturn()

            If Session("environment").ToString.ToLower <> "testing" Then
                sendAlertEmail()
            End If



        End If

        Response.Redirect("./ValidateQueue.aspx")




    End Sub


    'Protected Sub btnInvalid_Click(ByVal sender As Object, ByVal e As EventArgs)
    '    'Dim btnInvalid As Button = sender
    '    'Dim gvrCase As GridViewRow = CType(btnInvalid.NamingContainer, GridViewRow)
    '    'Dim HiddenSeqNum As String = gvAppRequests.DataKeys(gvrCase.RowIndex).Values("account_request_seq_num")
    '    'Dim HiddenLogin As String = IIf(IsDBNull(gvAppRequests.DataKeys(gvrCase.RowIndex).Values("login_name")), "", gvAppRequests.DataKeys(gvrCase.RowIndex).Values("login_name"))
    '    'Dim InvalidRequestItem As String = gvAppRequests.Rows(gvrCase.RowIndex).Cells(1).Text



    '    'lblInvalidRequestItem.Text = InvalidRequestItem
    '    'lblHiddenSeqNum.Text = HiddenSeqNum
    '    'lblHiddenLogin.Text = HiddenLogin


    '    'Response.Redirect("./ValidateQueue.aspx")

    '    'mpeInvalidItem.Show()


    'End Sub
    Protected Sub btnSubmitInvalid_Click(sender As Object, e As System.EventArgs) Handles btnSubmitInvalid.Click

        ' AccountSeqNum = lblHiddenSeqNum.Text
        '[UpdValidateRequestV4]()
        'HiddenSeqNum 
        'HiddenLogin
        'HiddenAppnum 
        Dim account_request_seq_num As String = lblHiddenSeqNum.Text
        Dim login_name As String = lblHiddenLogin.Text


        Dim thisData As New CommandsSqlAndOleDb("UpdValidateRequestItemsV4", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int)
        thisData.AddSqlProcParameter("@account_request_seq_num", account_request_seq_num, SqlDbType.Int)
        thisData.AddSqlProcParameter("@nt_login", Session("LoginID"), SqlDbType.NVarChar, 50)
        thisData.AddSqlProcParameter("@validate", "invalid", SqlDbType.NVarChar, 10)
        If Invalidddl.SelectedIndex > 0 Then
            thisData.AddSqlProcParameter("@Invalidcd", Invalidddl.SelectedValue, SqlDbType.NVarChar, 75)
        End If

        thisData.AddSqlProcParameter("@action_desc", InvalidDescTextBox.Text, SqlDbType.NVarChar, 255)


        thisData.ExecNonQueryNoReturn()
        bindAppRequests()


    End Sub

    'Protected Sub btnInvalidSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnInvalidSubmit.Click
    '    ' this is the old submit invalid
    '    Dim account_request_seq_num As String = lblHiddenSeqNum.Text
    '    Dim login_name As String = lblHiddenLogin.Text


    '    Dim thisData As New CommandsSqlAndOleDb("UpdValidateRequestItems", Session("EmployeeConn"))
    '    thisData.AddSqlProcParameter("@account_request_seq_num", account_request_seq_num, SqlDbType.Int)
    '    thisData.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int)
    '    thisData.AddSqlProcParameter("@requestor_client_num", Session("ClientNum"), SqlDbType.NVarChar, 30)
    '    thisData.AddSqlProcParameter("@validate", "invalid", SqlDbType.NVarChar, 10)
    '    thisData.AddSqlProcParameter("@action_desc", txtInvalidComments.Text, SqlDbType.NVarChar, 255)


    '    thisData.ExecNonQueryNoReturn()

    '    bindAppRequests()

    'End Sub

    Private Function SuggestedUserName(ByVal FirstName As String, ByVal LastName As String) As String
        Dim sReturn As String = ""


        Dim cmdSql As New SqlCommand
        Dim connSql As New SqlConnection(Session("EmployeeConn"))
        Dim drSql As SqlClient.SqlDataReader



        '   connSql.ConnectionString = _DataConn

        cmdSql.CommandText = "SelSuggestedUserNameLDAP"
        cmdSql.CommandType = CommandType.StoredProcedure
        cmdSql.Connection = connSql
        cmdSql.Parameters.Add("@FirstName", SqlDbType.NVarChar).Value = FirstName
        cmdSql.Parameters.Add("@LastName", SqlDbType.NVarChar).Value = LastName


        connSql.Open()

        Try




            drSql = cmdSql.ExecuteReader


            While drSql.Read()


                sReturn = IIf(IsDBNull(drSql("SuggestedName")), "", drSql("SuggestedName"))



            End While

        Catch ex As Exception
            Dim er As String = ex.ToString()

        End Try

        connSql.Close()
        Return sReturn
    End Function

  
    Private Function TestCustomUserName(ByVal UserName As String) As String
        Dim sReturn As String = ""


        Dim cmdSql As New SqlCommand
        Dim connSql As New SqlConnection(Session("EmployeeConn"))
        Dim drSql As SqlClient.SqlDataReader



        '   connSql.ConnectionString = _DataConn

        cmdSql.CommandText = "SelTestCustomUserName"
        cmdSql.CommandType = CommandType.StoredProcedure
        cmdSql.Connection = connSql
        cmdSql.Parameters.Add("@Login", SqlDbType.NVarChar).Value = UserName


        connSql.Open()

        If UserName.Length > 0 Then

            Try




                drSql = cmdSql.ExecuteReader


                While drSql.Read()


                    sReturn = IIf(IsDBNull(drSql("ReturnString")), "", drSql("ReturnString"))



                End While

            Catch ex As Exception
                Dim er As String = ex.ToString()

            End Try
        Else

            sReturn = "Must Provide Custom Username to check."

        End If


        connSql.Close()
        Return sReturn
    End Function

  
    Private Sub sendAlertEmail()
        '547131
        Dim Request As New AccountRequest(AccountRequestNum, Session("EmployeeConn"))
        Dim RequestItems As New RequestItems(AccountRequestNum)
        Dim EmailBody As New EmailBody
        '    Dim sCss As String = File.ReadAllText(Server.MapPath("/Styles/Tables.css"))



        '======================
        ' Get current path
        '======================

        Dim path As String
        Dim directory As String = ""
        path = HttpContext.Current.Request.Url.AbsoluteUri
        Dim pathParts() As String = Split(path, "/")
        Dim i As Integer
        Do While i < pathParts.Length() - 1

            directory = directory + pathParts(i) + "/"
            i = i + 1

        Loop

        '======================

        Dim EmailList As New List(Of EmailDistribution)
        EmailList = New EmailDistributionList().EmailList

        Dim sndEmailTo = From r In Request.RequestItems()
                         Join e In EmailList
                         On r.AppBase Equals e.AppBase
                         Where e.EmailEvent = "AddAcct"
                         Select e, r




        For Each EmailAddress In sndEmailTo



            Dim toEmail As String = EmailAddress.e.Email





            Dim sSubject As String
            Dim sBody As String


            sSubject = "Account Request for " & Request.FirstName & " " & Request.LastName & " has been added to the " & EmailAddress.r.ApplicationDesc.ToUpper() & " Queue."


            sBody = "<!DOCTYPE html>" & _
                                "<html>" & _
                                "<head>" & _
                                "<style>" & _
                                "table" & _
                                "{" & _
                                "border-collapse:collapse;" & _
                                "font-family: 'Helvetica Neue', 'Lucida Grande', 'Segoe UI', Arial, Helvetica, Verdana, sans-serif;" & _
                                "}" & _
                                "table, th, td" & _
                                "{" & _
                                "border: 1px solid black;" & _
                                "padding: 3px" & _
                                "}" & _
                                "</style>" & _
                                "</head>" & _
                                "" & _
                                "<body>" & _
                                "A new account request has been added to the " & EmailAddress.r.ApplicationDesc & " Queue.  Follow the link below to go to Queue Screen." & _
                                 "<br />" & _
                                "<a href=""" & directory & "/CreateAccount.aspx?requestnum=" & EmailAddress.r.AccountRequestNum & "&account_seq_num=" & EmailAddress.r.AccountRequestSeqNum & """ ><u><b> " & EmailAddress.r.ApplicationDesc.ToUpper() & ": " & Request.FirstName & " " & Request.LastName & "</b></u></a>" & _
                                "</body>" & _
                            "</html>"






            Dim sToAddress As String = EmailAddress.e.Email


            '  sToAddress = "Patrick.Brennan@crozer.org"


            Dim mailObj As New MailMessage()
            mailObj.From = New MailAddress("CSC@crozer.org")
            mailObj.To.Add(New MailAddress(sToAddress))
            mailObj.IsBodyHtml = True
            mailObj.Subject = sSubject
            mailObj.Body = sBody


            Dim smtpClient As New SmtpClient()
            smtpClient.Host = "ah1smtprelay01.altahospitals.com"



            smtpClient.Send(mailObj)




        Next




    End Sub

    Protected Sub btnCancelInvalid_Click(sender As Object, e As System.EventArgs) Handles btnCancelInvalid.Click
        PanInvalid.Visible = False
    End Sub
End Class
