﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="false" CodeFile="AddRemoveAcctFormOld.aspx.vb" Inherits="AddRemoveAcctForm" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">


   

<table width="100%">

<tr>

<td>

              <ajaxToolkit:TabContainer ID="tabs" runat="server" ActiveTabIndex="0"
                Width="100%" style="margin-bottom: 1px" EnableTheming="False" 
                BackColor="#efefef">



                  <ajaxToolkit:TabPanel ID="tpDemographics" runat="server" TabIndex="0"  BackColor="Gainsboro" Visible = "True">


                      <HeaderTemplate > 
                     
                            <asp:Label ID="Label1" runat="server" Text="Demographics" Font-Bold="true"></asp:Label>
            
                     
                      </HeaderTemplate>


                      <ContentTemplate>

                              <asp:UpdatePanel ID = "uplDemographics" runat="server">
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID = "entity_cdDDL" EventName ="SelectedIndexChanged" />
                                <asp:AsyncPostBackTrigger ControlID = "department_cdddl" EventName ="SelectedIndexChanged" />
                            </Triggers>
                              <ContentTemplate>
                      
                      

                                 <table border="3" style="background-color: gainsboro">
                        
                        
                 
       

                                    <tr>
                                             <td colspan = "4" class="tableRowHeader">
                                                Employee Information
                                             </td>
                                    </tr>
                                                  
                                    <tr>
                                        <td colspan = "4" align ="center">
                                            <asp:Label ID = "lblValidation" runat = "server" ForeColor="Red" Font-Size="Large"/>
                                        </td>
            
                                    </tr>
                                            <tr>
          
                                        <td colspan="4" class="tableRowSubHeader"  align="left">
                                        CKHS Affiliation:
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                         <asp:RadioButtonList ID ="emp_type_cdrbl" runat = "server" 
                                                RepeatDirection="Horizontal" AutoPostBack="True">
                                         
                                         </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                     <tr>
                                        <td colspan="4" class="tableRowSubHeader" align="left">Provider Information</td>
                                     </tr>
                                     <tr>
          
                                                  <td>
                                                                 Provider:
                                                                </td>
                                                                <td colspan="3">
                                                                <asp:RadioButtonList ID = "providerrbl" runat="server" RepeatDirection="Horizontal" >
                                                                    <asp:ListItem>Yes</asp:ListItem>
                                                                     <asp:ListItem Selected= "True">No</asp:ListItem>            
                                                                    </asp:RadioButtonList>
                                                                </td>
                                   

                                     </tr>
                                     <tr>
                                                     <td>
                                                    Han:
                                                    </td>
                                                    <td colspan="3">
                                                    <asp:RadioButtonList ID = "hanrbl" runat="server" RepeatDirection="Horizontal">
                                                         <asp:ListItem>Yes</asp:ListItem>
                                                          <asp:ListItem Selected="True">No</asp:ListItem>            
                                                        </asp:RadioButtonList>
                                                    </td>
                    
                                     </tr>
                                      <tr>
                                                    <td>
                                                    Credentialed Date:
                                                    </td>
                                                    <td colspan="3">
                                                     <asp:TextBox ID = "TextBox1" runat="server" CssClass="calenderClass"></asp:TextBox>
                                                    </td>
                       
                                     </tr>
                                    <tr>
                                        <td>
                                        Doctor Number:
                                        </td>
                                     <td colspan="3">
                                                     <asp:TextBox ID = "doctor_master_numTextBox" runat="server" Enabled="false"></asp:TextBox>
                                                    </td>
                                    </tr>
  
                              <tr>
                                <td colspan="4" class="tableRowSubHeader"  align="left">
                                Demographics
                                </td>
                              </tr>

                                    <tr>
                                        <td>
                                        First Name:           
            
                                        </td>
                                         <td>
                                        <asp:TextBox ID = "first_nameTextBox" runat="server" CssClass="textInput"></asp:TextBox>
                                          M:
                                             <asp:TextBox ID = "miTextBox" runat="server" Width="15px"></asp:TextBox>
                                        </td>
        
                                      <td>
                                        Last Name:
                                        </td>
                                         <td>
                                        <asp:TextBox ID = "last_nameTextBox" runat="server"></asp:TextBox>
                                        Suffix: 
                                        <asp:dropdownlist id="suffixddl"
				                            tabIndex="3" runat="server" Height="20px" Width="52px">
				                            <asp:ListItem Value="------" Selected="True">------</asp:ListItem>
				                            <asp:ListItem Value="Sr.">Sr.</asp:ListItem>
				                            <asp:ListItem Value="Jr.">Jr.</asp:ListItem>
				                            <asp:ListItem Value="II">II</asp:ListItem>
				                            <asp:ListItem Value="III">III</asp:ListItem>
				                            <asp:ListItem Value="IV">IV</asp:ListItem>
                                            </asp:dropdownlist>


                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        Employee Number:
                                        </td>
                                        <td>
                                        <asp:Label ID ="siemens_emp_numLabel" runat="server"></asp:Label>
                                        </td>
                                        <td>Network Logon</td>
                                        <td>
                                            <asp:TextBox ID = "login_nameTextBox" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
     
                                    <tr>
                                       <td>Title:</td>
                                        <td>
                                           <asp:dropdownlist ID = "Titleddl" runat="server"></asp:dropdownlist>
                                        </td>
                                        <td >
                                        Specialty:
                                        </td>
                                        <td>
                                           <asp:dropdownlist ID = "Specialtyddl" runat="server"></asp:dropdownlist>
                                        </td>
                                    </tr>
                                     <tr>
                                            <td>
                                                Position Description:
                                            </td>
                                            <td colspan = "3">
                                                <asp:TextBox ID="user_position_descTextBox" runat="server" Width = "200px"></asp:TextBox>
                                            </td>
                                        </tr>
                                            <tr>
                                                <td>
                                                Entity:
                                                </td>
                                                <td>
                                                <asp:DropDownList ID ="entity_cdDDL" runat="server" AutoPostBack ="true">
            
                                                </asp:DropDownList>
                                                </td>
       
                                                <td>
                                                Department Name:
                                                </td>
                                                <td>
                                                <asp:Label ID ="department_cdLabel" runat="server" Visible="false"></asp:Label>
                                                <asp:DropDownList ID ="department_cdddl" runat="server" AutoPostBack = "true">
            
                                                </asp:DropDownList>
                                                </td>
                                            </tr>
                                          
                                     <tr>
                                        <td>
                                        Practice Name:
                                        </td>       
                                        <td colspan="3">
                                        <asp:DropDownList ID ="practice_numddl" runat="server">
            
                                        </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Non-Han Location:
                                        </td>
                                        <td colspan="3">
                                              <asp:TextBox ID = "NonHanLocationTextBox" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Address 1:</td>
                                        <td>  <asp:TextBox ID = "address_1textbox" runat="server"></asp:TextBox></td>
                                        <td>City:</td>
                                        <td>  <asp:TextBox ID = "citytextbox" runat="server"></asp:TextBox></td>
                                    </tr>
                                     <tr>
                                        <td>Address 2:</td>
                                        <td>  <asp:TextBox ID = "address_2textbox" runat="server"></asp:TextBox></td>
                                        <td>State:<asp:TextBox ID = "statetextbox" runat="server" Width="25px"></asp:TextBox></td>
                                        <td>Zip:  <asp:TextBox ID = "ziptextbox" runat="server" Width="50px"></asp:TextBox></td>
                                    </tr>
       
                                        <tr>
                                        <td>
                                        Phone Number:
                                        </td>
                                        <td>
                                        <asp:TextBox ID = "phoneTextBox" runat="server"></asp:TextBox>
                                        </td>
       
                                        <td>
                                        Pager Number:
                                        </td>
                                        <td>
                                       <asp:TextBox ID = "pagerTextBox" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
   
 
                                    <tr>
                                       <td>Fax:</td>
                                        <td>
            
                                            <asp:TextBox ID = "faxtextbox" runat="server"></asp:TextBox></td>
           
           
                                         <td>
                                        Email:
                                        </td>
                                        <td>
                                       <asp:TextBox ID = "e_mailTextBox" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
      
                                     <tr>
                                        <td>
                                        Location:
                                        </td>
                                        <td>
         
                                          <asp:DropDownList ID ="facility_cdddl" runat="server" AutoPostBack="True">
              
                                        </asp:DropDownList>
          
                                        </td>
        
                                        <td>
                                        Building:
                                        </td>
                                        <td>
                                          <asp:TextBox ID = "building_cdTextBox" runat="server" Visible ="false"></asp:TextBox>
                                         <asp:DropDownList ID ="building_cdddl" runat="server">
            
                                        </asp:DropDownList>
          
                                        </td>
                                    </tr>
                                  <tr>
                                                <td>Floor:</td>
                                                <td> <asp:TextBox ID = "floorTextBox" runat="server"/></td>
                                                <td>Office:</td>
                                                <td> <asp:TextBox ID = "officeTextBox" runat="server"></asp:TextBox></td>
                                    </tr>
       

  
                            <tr>
          
                                        <td>Vendor Name:</td>
                                        <td colspan="3"><asp:TextBox ID ="VendorNameTextBox" runat="server" ></asp:TextBox></td>
                                    </tr>
                            <tr>
                                        <td>
                                        Start Date:
                                        </td>
                                        <td>
                                           <asp:TextBox ID ="start_dateTextBox" runat="server" CssClass="calenderClass"></asp:TextBox>
                                        </td>
       
                                        <td>
                                        End Date:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="end_dateTextBox" runat="server" CssClass="calenderClass"></asp:TextBox>
                                        </td>
                                      </tr>
        
 
                            <tr>
                                        <td>
                                        Group Name:

                                        </td>
                                        <td>
                                            <asp:TextBox ID="group_numTextBox" runat="server"></asp:TextBox>
                                        </td>
                                        <td>
                                            Share Drive:
                                        </td>
                                        <td >
                                             <asp:TextBox ID="share_driveTextBox" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                            <tr>
                                        <td>Model After:</td>
                                        <td colspan="3"> <asp:TextBox ID="ModelAfterTextBox" runat="server"></asp:TextBox></td>
                                    </tr>
                            <tr>
                                <td align="center" colspan="4">
                                    <asp:Button ID="btnUpdate" runat="server" Text="Update Demographics" />
                                   
                                </td>
                        
                        </table>

                                </ContentTemplate>

                              </asp:UpdatePanel>


                      </ContentTemplate>

                      </ajaxToolkit:TabPanel>

                  <ajaxToolkit:TabPanel ID="tpAccounts" runat="server" TabIndex="0"  BackColor="Gainsboro" Visible = "True">


                      <HeaderTemplate > 
                     
                            <asp:Label ID="Label2" runat="server" Text="Accounts" Font-Bold="true"></asp:Label>
            
                     
                      </HeaderTemplate>


                      <ContentTemplate>

                        <asp:UpdatePanel runat="server"  ID="uplAccounts" UpdateMode= "Conditional" >
        
                               <ContentTemplate>
                                 <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0"
                                         Width="100%" style="margin-bottom: 1px" EnableTheming="False"
                                            BackColor="#efefef">


                                      <ajaxToolkit:TabPanel ID="TabPanel3" runat="server" TabIndex="0"  BackColor="Gainsboro" Visible = "True">


                                          <HeaderTemplate > 
                     
                                                <asp:Label ID="Label6" runat="server" Text="Current Accounts" Font-Bold="true"></asp:Label>
            
                     
                                          </HeaderTemplate>


                                          <ContentTemplate>
                                            <asp:Panel runat="server" ID="pnlEditLogin" Visible = "false" Width="100%">
                                                
                                                <table >
                                                    <tr>
                                                        <td >
                                                           <h3> Edit Account Login</h3>
                                                        </td>
                                                    
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                        Account:
                                                            <asp:Label runat="server" ID="lblEditAccountName" Font-Bold="true"></asp:Label>
                                                        </td>
                                                        <td>
                                                         <asp:TextBox runat="server" ID ="txtEditAccountNum" Visible = "false"></asp:TextBox>
                                                            <asp:TextBox runat="server" ID ="txtEditAccountName"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:Button runat="server" ID = "btnEditAccountName" Text="Submit" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">
                                                            <asp:Panel runat="server" ID="pnlDuplicatAccounts" Visible = "false">
                                                            
                                                                <asp:Table runat="server" ID="tblDubNames">
                                                                    <asp:TableRow>
                                                                            <asp:TableCell>
                                                                             The following accounts have the same account name:
                                                                            </asp:TableCell>
                                                                                                                                                    
                                                                    </asp:TableRow>
                                                                   
                                                                   
                                                                
                                                                
                                                                </asp:Table>
                                                           
                                                                Do you wish to continue?
                                                                <br />
                                                                <asp:Button runat="server" ID ="btnEditLoginYes" Text = "Yes" />
                                                                <asp:Button runat="server" ID ="btnEditLoginNo" Text = "No" />

                                                            
                                                            </asp:Panel>
                                                        </td>
                                                    </tr>
                                                
                                                </table>
                                            
                                            </asp:Panel>

                                                <table width="100%" border="1"  style="background-color:Gainsboro">
                                                 
                                                    <tr style="background-color:#FFFFCC; font-weight:bold">
                                                        <td align="center" >
                                                        Accounts
                                                        </td>
                                                        
                                                    </tr>
                                                    <tr>
                                                                <td  style="width: 100%">
                                                                    <asp:GridView ID="gvCurrentUserAccounts" runat="server" AllowSorting="True" 
                                                                        AutoGenerateColumns="False" BackColor="White" BorderColor="#336666"
                                                                        BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                                                                        EmptyDataText="No Applications Found" EnableTheming="False" DataKeyNames="ApplicationNum, ApplicationDesc, login_name, create_date"
                                                                        Font-Size="Medium" GridLines="Horizontal" Width="100%">
                                   
                                                                                                        <FooterStyle BackColor="White" ForeColor="#333333" />
                                                                                                        <HeaderStyle BackColor="#336666" Font-Bold="False" ForeColor="White" Font-Size="Small" HorizontalAlign="Left"    />                                    
                                                                                                        <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Left" Font-Size="Medium"/>
                                                                                                        <RowStyle BackColor="White" ForeColor="#333333" />
                                                                                                        <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                                                                                        <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                                                                                        <SortedAscendingHeaderStyle BackColor="#487575" />
                                                                                                        <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                                                                                        <SortedDescendingHeaderStyle BackColor="#275353" />

                                                                                                        <Columns>
                                                                                            
                                                                                                                   <asp:CommandField ButtonType="Button" SelectText="Edit Login" ShowSelectButton= "true" />
                                                                                                                  <asp:BoundField  DataField="ApplicationDesc"  HeaderText="Application" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
				                                                                                                  <asp:BoundField  DataField="login_name" HeaderText="User Name" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
                                                                                                                  <asp:BoundField  DataField="create_date" HeaderText="Date Created" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
                                                                                                                   <asp:BoundField  DataField="CreatedBy" HeaderText="Created By" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
                                                                                                      </Columns>

                                                                                                    </asp:GridView>
		
                                                                                </td>
                                                                
                                
		
                                                                        
                         </tr>
                                                
                                                </table>

                                          </ContentTemplate>

                                    </ajaxToolkit:TabPanel>
                                      <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" TabIndex="0"  BackColor="Gainsboro" Visible = "True">


                                          <HeaderTemplate > 
                     
                                                <asp:Label ID="Label4" runat="server" Text="Remove Accounts" Font-Bold="true"></asp:Label>
            
                     
                                          </HeaderTemplate>


                                          <ContentTemplate>
                                                <table width="100%" border="1"  style="background-color:Gainsboro">
                                               
                                                 <td colspan = "4" align="left">
                                                        <asp:Button runat="server" ID ="btnRemoveAccts" Text = "Submit Remove Request" /> 
                                                    </td>

                                                     <tr>
                                                        <td colspan="4">
                                                           User submitting:         
                                                           <asp:Label ID = "Remove_userSubmittingLabel" runat="server" Font-Bold="true">Test</asp:Label>
                                                            </td>
                                                        </tr>
                                                        
                                                            <tr>
                                                            <td colspan="4">
                                                            Requestor:   <asp:Label ID = "Remove_SubmittingOnBehalfOfNameLabel" runat="server" Font-Bold="true"></asp:Label>
                                                             <asp:Label ID = "Remove_requestor_client_numLabel" runat="server" visible="false"></asp:Label>
                                                             
                                                            </td>
                                                        </tr>
                                                          <tr>
                                                            <td colspan = "4">

                                                            <asp:Button ID="btnRemoveOnBehalfOf" runat="server" Text="Select Requestor" />  
                                                            
                                                            </td>
                                                        
                                                        </tr>
                                                    <tr style="background-color:#FFFFCC; font-weight:bold">
                                                        <td colspan="2" >
                                                        Accounts
                                                        </td>
                                                        <td colspan="2">
                                                        Accounts to remove
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                                <td colspan="2"  valign="top"  style="width: 50%">
                                                                    <asp:GridView ID="gvCurrentApps" runat="server" AllowSorting="True" 
                                                                        AutoGenerateColumns="False" BackColor="White" BorderColor="#336666"
                                                                        BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                                                                        EmptyDataText="No Applications Found" EnableTheming="False" DataKeyNames="ApplicationNum, ApplicationDesc, create_date, login_name"
                                                                        Font-Size="Medium" GridLines="Horizontal" Width="100%">
                                   
                                                                                                        <FooterStyle BackColor="White" ForeColor="#333333" />
                                                                                                        <HeaderStyle BackColor="#336666" Font-Bold="False" ForeColor="White" Font-Size="Small" HorizontalAlign="Left"    />                                    
                                                                                                        <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Left" Font-Size="Medium"/>
                                                                                                        <RowStyle BackColor="White" ForeColor="#333333" />
                                                                                                        <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                                                                                        <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                                                                                        <SortedAscendingHeaderStyle BackColor="#487575" />
                                                                                                        <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                                                                                        <SortedDescendingHeaderStyle BackColor="#275353" />

                                                                                                        <Columns>
                                                                                            
                                                                                                                  <asp:CommandField ButtonType="Button" SelectText="Select" ShowSelectButton= "true" />
                                                                                                                  <asp:BoundField  DataField="ApplicationDesc"  HeaderText="Application" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
                                                                                                                   <asp:BoundField  DataField="login_name" HeaderText="User Name" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
                                                                                                                  
                                  
                                                                                                      </Columns>

                                                                                                    </asp:GridView>
		
                                                                                </td>
                                                                 <td colspan="2" valign="top"  style="width: 50%">
                                                                <asp:GridView ID="gvRemoveAccounts" runat="server" AllowSorting="True" 
                                                                                AutoGenerateColumns="False" BackColor="White" BorderColor="#336666"
                                                                                BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                                                                                EmptyDataText="Select applications to be removed" EnableTheming="False" DataKeyNames="ApplicationNum, ApplicationDesc, login_name"
                                                                                Font-Size="Medium" GridLines="Horizontal" Width="100%" >
                                   
                                                                                <FooterStyle BackColor="White" ForeColor="#333333" />
                                                                                <HeaderStyle BackColor="#336666" Font-Bold="False" ForeColor="White" Font-Size="Small" HorizontalAlign="Left"    />                                    
                                                                                <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Left" Font-Size="Medium"/>
                                                                                <RowStyle BackColor="White" ForeColor="#333333" />
                                                                                <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                                                                <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                                                                <SortedAscendingHeaderStyle BackColor="#487575" />
                                                                                <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                                                                <SortedDescendingHeaderStyle BackColor="#275353" />

                                                                                <Columns>
                                              
                                              
                                                                                            <asp:CommandField ButtonType="Button" SelectText="<-- Remove" ShowSelectButton= "true" />
                                                                                            <asp:BoundField  DataField="ApplicationDesc"  HeaderText="Application" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
				                                                                             <asp:BoundField  DataField="login_name" HeaderText="User Name" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
                                                                                                                  




                                  
                                                                                </Columns>

                                                                            </asp:GridView>
		
                                                                        </td>
                         </tr>
                                                
                                                </table>

                                          </ContentTemplate>

                                    </ajaxToolkit:TabPanel>
                                     <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" TabIndex="0"  BackColor="Gainsboro" Visible = "True">


                                          <HeaderTemplate > 
                     
                                                <asp:Label ID="Label5" runat="server" Text="Request Accounts" Font-Bold="true"></asp:Label>
            
                     
                                          </HeaderTemplate>


                                          <ContentTemplate>
                                         
                                         
                                                        

                                                                      <table border="1" style="background-color:Gainsboro">
                                                
                                                   <tr>

                                                    <td colspan="4" style="background-color:#FFFFCC; font-weight:bold" >
                                                    Select Account for Request
                                                    
                                                    </td>
                                                   
           
                                                   </tr>
                                                         
                                                     <tr>
                                                        <td colspan="4">
                                                           User submitting:         
                                                           <asp:Label ID = "Add_userSubmittingLabel" runat="server" Font-Bold="true">Test</asp:Label>
                                                            </td>
                                                        </tr>
                                                        
                                                            <tr>
                                                            <td colspan="4">
                                                            Requestor:   <asp:Label ID = "Add_SubmittingOnBehalfOfNameLabel" runat="server" Font-Bold="true"></asp:Label>
                                                             <asp:Label ID = "Add_requestor_client_numLabel" runat="server" visible="false"></asp:Label>
                                                             
                                                            </td>
                                                        </tr>
                                                             
                                                        <tr>
                                                            <td colspan = "4">

                                                            <asp:Button ID="btnSelectOnBehalfOf" runat="server" Text="Select Requestor" />  
                                                            
                                                            </td>
                                                        
                                                        </tr>
                                                       
                                                     
                                                          
                                                           
                                                   <tr>                                                                                                                           
                                                                <td colspan="4">
                                                                <span style="Font-weight:bold"> Entities</span>
                                                                    <asp:CheckBoxList ID = "cblEntities" runat="server" RepeatDirection = "Horizontal">
                                                                    </asp:CheckBoxList>
                                                                </td>
            
                                                            </tr>
                                                    <tr>
                                                                <td colspan="4">
                                                                    <span style="Font-weight:bold">Hospitals</span>
                                                                
                                                                    <asp:CheckBoxList ID = "cblFacilities" runat="server" RepeatDirection = "Horizontal">
                                                                    </asp:CheckBoxList>
                                                                </td>
      
        
                                                            </tr>
                                                             <tr>
                                                            <td colspan = "4" align="left">
                                                             <asp:Label Text="" runat="server" ID="lblValidateAccountRequest" ForeColor="Blue" Font-Bold="true" />
                                                            </td>
                                                        </tr>
                                                        <td colspan = "4" align="right">
                                                        <asp:Button runat="server" ID ="btnSubmitAddRequest" Text = "Submit Add Request" /> 
                                                    </td>
                                                       
                                                       <tr style="background-color:#FFFFCC; font-weight:bold">


                                                                 <td colspan="2">
                                                                    Accounts available
                                                                    </td>
                                                                 <td colspan="2">
                                                                    Accounts to be added
                                                                    </td>
                                                              </tr>
                                                                         <tr>
                                                                          <td colspan="2"  valign="top" style="width: 50%">
                                                                                 <asp:GridView ID="grAvailableAccounts" runat="server" AllowSorting="True" 
                                                                                AutoGenerateColumns="False" BackColor="White" BorderColor="#336666"
                                                                                BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                                                                                EmptyDataText="No Applications Found" EnableTheming="False" DataKeyNames="AppBase, ApplicationDesc"
                                                                                Font-Size="Medium" GridLines="Horizontal" Width="100%">
                                   
                                                                                <FooterStyle BackColor="White" ForeColor="#333333" />
                                                                                <HeaderStyle BackColor="#336666" Font-Bold="False" ForeColor="White" Font-Size="Small" HorizontalAlign="Left"    />                                    
                                                                                <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Left" Font-Size="Medium"/>
                                                                                <RowStyle BackColor="White" ForeColor="#333333" />
                                                                                <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                                                                <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                                                                <SortedAscendingHeaderStyle BackColor="#487575" />
                                                                                <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                                                                <SortedDescendingHeaderStyle BackColor="#275353" />

                                                                                <Columns>
                                                                                            
                                                                                          <asp:CommandField ButtonType="Button" SelectText="Select" ShowSelectButton= "true" HeaderStyle-HorizontalAlign = "Left" />
                                                                                          <asp:BoundField  DataField="ApplicationDesc"  HeaderText="Application"  HeaderStyle-HorizontalAlign = "Left"></asp:BoundField>
				                                                                  
                                  
                                                                              </Columns>

                                                                            </asp:GridView>
		
                                                                  </td>
                                                                  <td colspan="2"  valign="top" style="width: 50%">
                                                                                 <asp:GridView ID="grAccountsToAdd" runat="server" AllowSorting="True" 
                                                                                AutoGenerateColumns="False" BackColor="White" BorderColor="#336666"
                                                                                BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                                                                                EmptyDataText="Select accounts to add" EnableTheming="False" DataKeyNames="AppBase, ApplicationDesc"
                                                                                Font-Size="Medium" GridLines="Horizontal" Width="100%">
                                   
                                                                                <FooterStyle BackColor="White" ForeColor="#333333" />
                                                                                <HeaderStyle BackColor="#336666" Font-Bold="False" ForeColor="White" Font-Size="Small" HorizontalAlign="Left"    />                                    
                                                                                <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Left" Font-Size="Medium"/>
                                                                                <RowStyle BackColor="White" ForeColor="#333333" />
                                                                                <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                                                                <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                                                                <SortedAscendingHeaderStyle BackColor="#487575" />
                                                                                <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                                                                <SortedDescendingHeaderStyle BackColor="#275353" />

                                                                                <Columns>
                                                                                            
                                                                                          <asp:CommandField ButtonType="Button" SelectText="<-- Remove" ShowSelectButton= "true" HeaderStyle-HorizontalAlign = "Left" />
                                                                                          <asp:BoundField  DataField="ApplicationDesc"  HeaderText="Application"  HeaderStyle-HorizontalAlign = "Left"></asp:BoundField>
				                                                                  
                                  
                                                                              </Columns>

                                                                            </asp:GridView>
		
                                                                  </td>
                                                        </tr>
                                                
                                                </table>
                                                         
                                          </ContentTemplate>

                                    </ajaxToolkit:TabPanel>
                                </ajaxToolkit:TabContainer>
                            </ContentTemplate>
                       
                       </asp:UpdatePanel>

                       
                       </ContentTemplate>                                

                  </ajaxToolkit:TabPanel>

                  <ajaxToolkit:TabPanel ID="tpPendingRequests" runat="server" TabIndex="0"  BackColor="red" Visible = "True">


                      <HeaderTemplate > 
                     
                            <asp:Label ID="Label3" runat="server" Text="Pending Requests" Font-Bold="true"></asp:Label>
            
                     
                      </HeaderTemplate>


                      <ContentTemplate>

                      <table border="3" width = "100%">
                        <tr>
                            <td colspan = "4">
                                  <asp:Table ID = "tblPendingQueues" runat = "server">
                                 </asp:Table>
            
                            </td>
        
                        </tr>
                      </table>
                      
                         </ContentTemplate>
       

                      </ajaxToolkit:TabPanel>

           </ajaxToolkit:TabContainer>





</td>

</tr>

</table>

   <asp:Button ID="btnRequestorDummy" Style="display: none" runat="server" Text="Button" />

                                                    <ajaxToolkit:ModalPopupExtender ID="mpeOnBehalfOf" runat="server"   
                                                                        DynamicServicePath="" Enabled="True" TargetControlID="btnRequestorDummy"   
                                                                        PopupControlID="pnlOnBehalfOf" BackgroundCssClass="ModalBackground"   
                                                                        DropShadow="true" >  
                                                            </ajaxToolkit:ModalPopupExtender>  
            
            
<asp:Panel ID="pnlOnBehalfOf" runat="server" style="display:none; background-color:#efefef;  font-size:large;  width:600px;  border: 2px solid #FFFFCC;">

    <asp:UpdatePanel ID = "uplOnBehalfOf" runat="server" UpdateMode="Conditional" >
   
    
   <ContentTemplate>
      
                                            
                                                            <table  width="100%" border="1px" >
                                                                <tr>
                                                                    <th class="tableRowHeader" colspan = "4">
                                                                  Search for Requestor
                                                                    </th>
                                                                </tr>
                                                             <tr>
                                                                <td>
                                                                   First Name <asp:TextBox ID = "FirstNameTextBox" runat = "server"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    Last Name <asp:TextBox ID = "LastNameTextBox" runat = "server"></asp:TextBox>
                                                                      <asp:Button ID ="btnSearch" runat = "server" Text ="Search"  />
                                                                </td>
       
  
                                                                
       
                                                            </tr>
                                                             
                                                            <tr>
                                                                <td colspan = "4">
                                                                <div class="scrollContentContainer" >
                                                                
                                                                 <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="200" AssociatedUpdatePanelID="uplOnBehalfOf"  >

                                                                    <ProgressTemplate>

                                                                    <div id="IMGDIV" align="center" valign="middle" runat="server" style="visibility:visible;vertical-align:middle;">
                                                                     Loading Data ...
                   
                                                                   
                   
                                                                    </div>

                                                                    </ProgressTemplate>

                                                        </asp:UpdateProgress>
                                                        

                                                                
                                                                     <asp:GridView ID="gvSearch" runat="server" AllowSorting="True" 
                                                                                                AutoGenerateColumns="False" BackColor="White" BorderColor="#336666"
                                                                                                BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                                                                                                EmptyDataText="No Items Found" EnableTheming="False" DataKeyNames="client_num"
                                                                                                Font-Size="Medium" GridLines="Horizontal" Width="100%">
                                   
                                                                                                <FooterStyle BackColor="White" ForeColor="#333333" />
                                                                                                <HeaderStyle BackColor="#336666" Font-Bold="False" ForeColor="White" Font-Size="Small" HorizontalAlign="Left"    />                                    
                                                                                                <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Left" Font-Size="Medium"/>
                                                                                                <RowStyle BackColor="White" ForeColor="#333333" />
                                                                                                <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                                                                                <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                                                                                <SortedAscendingHeaderStyle BackColor="#487575" />
                                                                                                <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                                                                                <SortedDescendingHeaderStyle BackColor="#275353" />

                                                                                                <Columns>
                                                                                                    <asp:CommandField ButtonType="Button" SelectText="Select" ShowSelectButton= "true" />

                                         
                                                                                                      <asp:BoundField  DataField="fullname"  HeaderText="Full Name" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
                                                                                      <%--                <asp:BoundField  DataField="login_name"  HeaderText="Login"></asp:BoundField>--%>
                                                                                                      <asp:BoundField  DataField="phone"  HeaderText="Phone" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
                                                                                                      <asp:BoundField  DataField="department_name"  HeaderText="Department" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
				                          



                                  
                                                                                              </Columns>

                                                                                            </asp:GridView>
                                                                
                                                                        </div>
                                                                
                                                                </td>
                                                            
                                                            
                                                            </tr>
                                                          
                                                           
                                                               
                                                                <tr>
                                                                    <td colspan = "4" align = "center" >
                                                                    <asp:Button ID = "btnCloseOnBelafOf" runat = "server" Text="Close" />
                                                                    
                                                                  
                                                                   
                                                                    </td>
                                                                </tr>
                                
                                                            </table>

                                         </ContentTemplate>               
                              </asp:UpdatePanel>
                                 
                                </asp:Panel>

                              <%--  <ajaxToolkit:UpdatePanelAnimationExtender ID="UpdatePanelAnimationExtender1" runat="server" TargetControlID="uplOnBehalfOf">
                                                                      <Animations>
                                                                          <OnUpdating>
                                                                           <StyleAction animationtarget="progress" Attribute="display" value="block" />
                                                                        </OnUpdating>
                                                                        <OnUpdated>
                                                                             <StyleAction animationtarget="progress" Attribute="display" value="none" />

                                                                        </OnUpdated>
                                                                    </Animations>
                                                        </ajaxToolkit:UpdatePanelAnimationExtender>--%>
                                 


</asp:Content>

