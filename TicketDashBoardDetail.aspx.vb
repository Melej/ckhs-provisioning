﻿Imports App_Code
Partial Class TicketDashBoardDetail
    Inherits System.Web.UI.Page
    Dim sActivitytype As String = ""
    Dim cblBinder As New CheckBoxListBinder
    Dim UserInfo As UserSecurity
    Dim FormSignOn As New FormSQL()

    Dim sClientNum As String

    Dim ApplicationNumber As String
    Dim GroupNum As String
    Dim AccountCd As String
    Dim MaintType As String

    Dim sGnumber As String = ""
    Dim sCommGnumber As String = ""
    Dim sInvisionNumber As String = ""
    Dim GroupNumS As String = ""
    Dim sendate As String

    Dim securityLevel As Integer
    Dim totalcount As Integer
    Dim UnknowCount As Integer
    Dim EmployeeCount As Integer
    Dim ContractCount As Integer
    Dim StudentCount As Integer
    Dim PhysicianCount As Integer
    Dim NursingCount As Integer
    Dim EmpNoNumberCount As Integer
    Dim NonCKHNCount As Integer
    Dim ResidentCount As Integer

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        UserInfo = New UserSecurity(Session("LoginID"), Session("EmployeeConn"))

        securityLevel = UserInfo.SecurityLevelNum

        'Dim frmSec As New FormSecurity(Session("objUserSecurity"))

        If Session("SecurityLevelNum") < 25 Then
            Response.Redirect("~/Security.aspx")
        End If

        If Session("SecurityLevelNum") < 40 Then
            Response.Redirect("~/MyRequests.aspx")

        End If


        If Not IsPostBack Then

            HDStartDate.Text = Request.QueryString("StarDate")
            HDEnddate.Text = Request.QueryString("EndDate")
            HDCategory.Text = Request.QueryString("Category") ' application
            HDStatus.Text = Request.QueryString("Status").ToLower 'open closed
            HDType.Text = Request.QueryString("Type") ' item type

            'txtEndDate.Text = DateTime.Now.ToShortDateString
            'txtBeginDate.Text = DateAdd(DateInterval.Day, -14, DateTime.Now()).ToShortDateString

            txtBeginDate.Text = DateTime.Today.AddDays(-91)
            txtEndDate.Text = DateTime.Today
            If HDStartDate.Text = "" Then
                HDStartDate.Text = txtBeginDate.Text
                HDEnddate.Text = txtEndDate.Text

            End If

            Dim ddlBinder As DropDownListBinder = New DropDownListBinder(categorycdddl, "SelCategoryCodes", Session("CSCConn"))
            ddlBinder.AddDDLCriteria("@securitylevel", securityLevel, SqlDbType.Int)

            ddlBinder.BindData("category_cd", "Category_Desc")

            If HDCategory.Text <> "" Then
                HDCategory.Text = RTrim(HDCategory.Text.ToLower)
                categorycdddl.SelectedValue = RTrim(HDCategory.Text.ToLower)

                If HDType.Text <> "" Then
                    HDType.Text = RTrim(HDType.Text.ToLower)
                    Dim ddlTypeBinder As New DropDownListBinder(typecdddl, "SelCategoryType", Session("CSCConn"))
                    ddlTypeBinder.AddDDLCriteria("@Category_cd", RTrim(HDCategory.Text.ToLower), SqlDbType.NVarChar)

                    ddlTypeBinder.BindData("type_cd", "CatagoryTypeDesc")



                    typecdddl.SelectedValue = RTrim(HDType.Text.ToLower)
                End If

            End If

            If HDStatus.Text <> "" Then
                Statusrbl.SelectedValue = HDStatus.Text
            End If


            fillTable()

        End If


    End Sub

    Protected Sub fillTable()

        Dim thisData As New CommandsSqlAndOleDb("SelCategoryDetailTickets", Session("CSCConn"))

        thisData.AddSqlProcParameter("@startDate", HDStartDate.Text, SqlDbType.DateTime)
        thisData.AddSqlProcParameter("@endDate", HDEnddate.Text, SqlDbType.DateTime)

        thisData.AddSqlProcParameter("@Category", HDCategory.Text, SqlDbType.NVarChar)
        thisData.AddSqlProcParameter("@Status", HDStatus.Text, SqlDbType.NVarChar)
        If HDType.Text <> "" Then
            thisData.AddSqlProcParameter("@Type", HDType.Text, SqlDbType.NVarChar)

        End If
        'If displyrbl.SelectedValue.ToLower = "yes" Then
        '    thisData.AddSqlProcParameter("@enddate", "yes", SqlDbType.NVarChar)

        'End If
        'totalpan.Visible = False
        'Category
        'Type
        'Counts
        'CurrentStatus

        Dim iRowCount = 1
        Dim iTotalRowcount = 0

        Dim dt As DataTable
        dt = thisData.GetSqlDataTable

        gvTicketDetail.DataSource = thisData.GetSqlDataTable

        gvTicketDetail.DataBind()


    End Sub


    Protected Sub gvTicketDetail_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvTicketDetail.RowCommand

        If e.CommandName = "Select" Then
            Dim request_num As String = gvTicketDetail.DataKeys(Convert.ToInt32(e.CommandArgument))("Request").ToString()

            Response.Redirect("~/ViewRequestTicket.aspx?RequestNum=" & request_num, True)



        End If

    End Sub
    Protected Sub btnReset_Click(sender As Object, e As System.EventArgs) Handles btnReset.Click
        'Appnumlbl.Text = ""
        Response.Redirect("./TicketDashboard.aspx", True)

    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As System.EventArgs) Handles btnSubmit.Click

        If categorycdddl.SelectedValue.ToLower <> "select" Then
            HDCategory.Text = categorycdddl.SelectedValue

        End If
        fillTable()


    End Sub

    Protected Sub categorycdddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles categorycdddl.SelectedIndexChanged
        If categorycdddl.SelectedValue.ToLower <> "select" Then
            HDCategory.Text = categorycdddl.SelectedValue

            Dim ddlBinder As New DropDownListBinder(typecdddl, "SelCategoryType", Session("CSCConn"))
            ddlBinder.AddDDLCriteria("@Category_cd", HDCategory.Text, SqlDbType.NVarChar)

            ddlBinder.BindData("type_cd", "CatagoryTypeDesc")

            typecdddl.SelectedIndex = -1
        End If

    End Sub

    Protected Sub Statusrbl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles Statusrbl.SelectedIndexChanged
        HDStatus.Text = Statusrbl.SelectedValue

    End Sub

    Protected Sub txtBeginDate_TextChanged(sender As Object, e As System.EventArgs) Handles txtBeginDate.TextChanged
        HDStartDate.Text = txtBeginDate.Text

    End Sub

    Protected Sub txtEndDate_TextChanged(sender As Object, e As System.EventArgs) Handles txtEndDate.TextChanged
        HDEnddate.TextMode = txtEndDate.Text
    End Sub

    Protected Sub typecdddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles typecdddl.SelectedIndexChanged
        HDType.Text = typecdddl.SelectedValue

    End Sub

    Protected Sub gvTicketDetail_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvTicketDetail.RowDataBound

    End Sub

    Protected Sub btnReport_Click(sender As Object, e As System.EventArgs) Handles btnReport.Click
        Response.Redirect("~/TicketRFSReportDetails.aspx", True)

    End Sub
End Class
