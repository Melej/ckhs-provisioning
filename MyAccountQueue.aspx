﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="false" CodeFile="MyAccountQueue.aspx.vb" Inherits="MyAccountQueue" %>



<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <style type="text/css">
        .style13
    {
        height: 30px;
    }
    </style>

   
</asp:Content>



<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

 <div>
  <asp:UpdatePanel ID = "uplPanel" runat="server">
      <Triggers>
            <%--<asp:AsyncPostBackTrigger ControlID="cblAccountQueues" EventName="SelectedIndexChanged" />--%>
            <%--<asp:AsyncPostBackTrigger ControlID="cblMyQueues" EventName="SelectedIndexChanged" />--%>
            <asp:AsyncPostBackTrigger controlId="rblAppGgroup"  EventName="SelectedIndexChanged" />
      </Triggers>
     <ContentTemplate>
<table style="border-style: groove;" width="100%" align="left">
<tr>
   <td colspan="2" >
    <div id ="divQueueOptions" style="text-align:center" >
         <table width="100%">
            <tr>

                <td>
                 <asp:Button runat="server" ID="btnAdd" Text = "Add Accounts"  
                  CssClass="btnhov" BackColor="#006666"  Width="120px" />
                </td>
                <td>
                 <asp:Button runat="server" ID="btnDelete" Text = "Delete Accounts" 
                 CssClass="btnhov" BackColor="#006666"  Width="120px" />

                 
                </td>
                <td>
                 <asp:Button runat="server" ID="btnInvalid" Text = "Invalid Requests"   
                 CssClass="btnhov" BackColor="#006666"  Width="120px" />

                 
                </td>
            
            </tr>
            <tr>
                <td  colspan="3">
                    <asp:Label ID="LblViewtype" runat="server"  Font-Bold="True" 
                        ForeColor="#990000"></asp:Label>
                
                </td>
            </tr>
            <tr>
                <td colspan="3">
                
                    <asp:Label ID="LblLastRefresh" runat="server" Font-Bold="True" ></asp:Label>
                </td>
            </tr>
         </table>
       <hr/>
    </div>
  </td>
 </tr>
 <tr>
    <td valign = "top" align="left">
        <div style="overflow:auto;" class="subContainer" >
          <asp:CheckBoxList ID = "cblAccountQueues" runat = "server" 
            AutoPostBack="True" OnSelectedIndexChanged="cblAccountQueues_SelectedIndexChanged" />
            <br />
             <br />
         </div>

    </td>
    <td valign = "top" align="left">

           <div style="overflow:auto;" class="subContainer" >

           <%--class="subContainer"--%>
           <table>
           <tr>
                <td>
                    <asp:Label ID="lblerror" runat="server" Width="550" />

                </td>
           </tr>
           <tr>
                <td align="left">
                    <asp:RadioButtonList ID="rblAppGgroup" runat="server" RepeatDirection="Horizontal" AutoPostBack="True" >
   <%--                         <asp:ListItem Value="clinical">Clinical Apps</asp:ListItem>
                            <asp:listItem Value="financial">Financial</asp:listItem>--%>

                    </asp:RadioButtonList>
                </td>
            </tr>

           
           </table>
            <asp:Table ID = "tblAccountQueues" runat = "server" Width="90%" CellPadding="0" CellSpacing="0" EnableTheming="false" > 
            
            </asp:Table>


               <br></br>
               <br></br>
               <br></br>

               <br></br>

          </div>
    </td>
 </tr>
 <tr>
    <td colspan="2" >
        <%-- Clinical or Finanical--%>
        <asp:Label ID="HDGroupType" runat="server" Visible="false"></asp:Label>

        <%-- Add Delete or Invalid--%>
        <asp:Label ID="HDViewType" runat="server" Visible="false"></asp:Label>
    </td>
 </tr>
</table>
 </ContentTemplate>
</asp:UpdatePanel>

</div>
 <asp:Timer ID="Timer1" runat="server"  Enabled="True" Interval="240000">
    <%--240000--%>
 </asp:Timer>
</asp:Content>

