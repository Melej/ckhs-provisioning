﻿Imports System.IO
Imports System.Data '
Imports System.Data.SqlClient
Imports App_Code
Partial Class RequestDetail
    Inherits System.Web.UI.Page
    Dim ReportTotalList As New DataTable
    Dim iYear As Integer = 0


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        If Not IsPostBack Then

            iYear = Request.QueryString("iYear")
            Curryeartxt.Text = Request.QueryString("iYear").ToString

            lblnotice.Text = "Year Selected " & iYear.ToString

            fillTable()

            CurrentTotals()

        End If



    End Sub
    Protected Sub fillTable()

        Dim thisdata As New CommandsSqlAndOleDb("SelAccountRequestReportV2", Session("EmployeeConn"))
        thisdata.AddSqlProcParameter("@RequestType", accounttypelbl.Text, SqlDbType.NVarChar, 12)
        If iYear > 0 Then
            thisdata.AddSqlProcParameter("@year", iYear, SqlDbType.Int, 7)

        End If


        Dim iRowCount = 1
        Dim iTotalRowcount = 0
        Dim total1 As Integer = 0

        ReportTotalList = thisdata.GetSqlDataTable


        grdAccounts.DataSource = thisdata.GetSqlDataTable

        grdAccounts.DataBind()

        'ViewState("dtOpenRequests") = ReportTotalList

        'total1 = ReportTotalList.Rows.

        Session("accountRequeststbl") = ReportTotalList

        grdAccounts.Visible = True
        grdDetails.Visible = False


    End Sub

    Protected Sub CurrentTotals()

        'grdAccounts.DataSource = ViewState("dtOpenRequests")
        'grdAccounts.DataBind()

        'gvRemoveAccounts.DataSource = ViewState("dtRemoveAccounts")
        'gvRemoveAccounts.DataBind()

    End Sub

    Protected Sub AccountTyperbl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles AccountTyperbl.SelectedIndexChanged
        accounttypelbl.text = AccountTyperbl.SelectedValue.ToString

        Dim thisdata As New CommandsSqlAndOleDb("SelOpenAccountRequestReport", Session("EmployeeConn"))
        thisdata.AddSqlProcParameter("@RequestType", accounttypelbl.Text, SqlDbType.NVarChar, 12)


        thisdata.ExecNonQueryNoReturn()
        ReportTotalList = thisdata.GetSqlDataTable

        ViewState("dtOpenRequests") = thisdata.GetSqlDataTable

        CurrentTotals()


    End Sub
    Private Function GetSortDirection(ByVal column As String) As String

        ' By default, set the sort direction to ascending.
        Dim sortDirection = "ASC"

        ' Retrieve the last column that was sorted.
        Dim sortExpression = TryCast(ViewState("SortExpression"), String)

        If sortExpression IsNot Nothing Then
            ' Check if the same column is being sorted.
            ' Otherwise, the default value can be returned.
            If sortExpression = column Then
                Dim lastDirection = TryCast(ViewState("SortDirection"), String)
                If lastDirection IsNot Nothing _
                  AndAlso lastDirection = "ASC" Then

                    sortDirection = "DESC"

                End If
            End If
        End If

        ' Save new values in ViewState.
        ViewState("SortDirection") = sortDirection
        ViewState("SortExpression") = column

        Return sortDirection

    End Function

    Protected Sub grdAccounts_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdAccounts.RowCommand
        If e.CommandName = "Select" Then


            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim iAppBase As Integer = grdAccounts.DataKeys(e.CommandArgument)("AppBase")

            ' load from rbl and year and app display detail grid hide current grid

            'Response.Redirect("~/" & "RequestDetail.aspx?iYear=" & iThisyear, True)

            Dim thisdata As New CommandsSqlAndOleDb("SelAccountRequestReportV2", Session("EmployeeConn"))
            thisdata.AddSqlProcParameter("@RequestType", accounttypelbl.Text, SqlDbType.NVarChar, 12)

            thisdata.AddSqlProcParameter("@Appbase", iAppBase, SqlDbType.Int, 7)

            thisdata.AddSqlProcParameter("@year", CInt(Curryeartxt.Text), SqlDbType.Int, 7)
            thisdata.AddSqlProcParameter("@detail", "yes", SqlDbType.NVarChar, 6)

            'ReportTotalList = thisdata.GetSqlDataTable


            grdDetails.DataSource = thisdata.GetSqlDataTable

            grdDetails.DataBind()


            grdAccounts.Visible = False
            grdDetails.Visible = True

        End If

    End Sub

    Protected Sub grdDetails_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdDetails.RowCommand
        If e.CommandName = "Select" Then


            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim iAccountreq As Integer = grdDetails.DataKeys(e.CommandArgument)("account_request_num")

            ' load from rbl and year and app display detail grid hide current grid

            Response.Redirect("~/" & "AccountRequestInfo2.aspx?RequestNum=" & iAccountreq, True)

        End If
    End Sub
End Class
