﻿Imports App_Code
Imports System.Web.UI.DataVisualization.Charting

Partial Class TicketStats
    Inherits System.Web.UI.Page





    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        Dim frmSec As New FormSecurity(Session("objUserSecurity"))


        If Not IsPostBack Then

            txtEndDate.Text = DateTime.Now.ToShortDateString
            txtBeginDate.Text = DateAdd(DateInterval.Day, -14, DateTime.Now()).ToShortDateString


        End If
        PopulateCategories()


    End Sub

    Protected Function ListChartTable(ByRef dt As DataTable, ByRef Stat As String) As Table

        Dim tblTable As New Table
        Dim rwRow As New TableRow
        Dim clList As New TableCell
        Dim clChart As New TableCell

        clChart.Controls.Add(fillStatChart(dt))
        clChart.VerticalAlign = VerticalAlign.Top
        clList.Controls.Add(fillStatList(dt, Stat))
        clList.VerticalAlign = VerticalAlign.Top

        rwRow.Cells.Add(clList)
        rwRow.Cells.Add(clChart)

        tblTable.Rows.Add(rwRow)

        Return tblTable

    End Function



    Protected Sub PopulateCategories()

        Dim thisData As New CommandsSqlAndOleDb("SelTicketStats", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@beginDate", txtBeginDate.Text, SqlDbType.DateTime)
        thisData.AddSqlProcParameter("@endDate", txtEndDate.Text, SqlDbType.DateTime)

        If rblPriority.SelectedValue <> "All" Then
            thisData.AddSqlProcParameter("@Priority", rblPriority.SelectedValue, SqlDbType.Int)

        End If





        Dim dt As DataTable = thisData.GetSqlDataTable

        pnlCats.Controls.Add(ListChartTable(dt, "Category"))







      

    End Sub

    Protected Function fillStatList(ByRef dt As DataTable, ByRef Stat As String) As Table

        Dim tblCatList As New Table

        Dim rws = From rs In dt
             Order By rs("StatCount") Descending
             Select rs


        For Each dr As DataRow In rws
            Dim trCatList As New TableRow
            Dim tcCatName As New TableCell
            Dim tcCatCount As New TableCell
            Dim tcCatPerc As New TableCell

            Dim lbtnCat As New LinkButton



            lbtnCat.Text = IIf(IsDBNull(dr("StatCd")), "", dr("StatCd"))
            lbtnCat.CommandName = "Select" & Stat
            lbtnCat.CommandArgument = IIf(IsDBNull(dr("StatCd")), "", dr("StatCd"))


            ScriptManager.GetCurrent(Me).RegisterAsyncPostBackControl(lbtnCat)


            AddHandler lbtnCat.Click, AddressOf lbtnCat_Click

            tcCatName.Controls.Add(lbtnCat)
            tcCatCount.Text = dr("StatCount")
            tcCatPerc.Text = dr("TotalTicketsAvg")

            trCatList.Cells.Add(tcCatName)
            trCatList.Cells.Add(tcCatCount)
            trCatList.Cells.Add(tcCatPerc)

            tblCatList.Rows.Add(trCatList)
        Next

        Return tblCatList

    End Function


    Protected Function fillAjaxBarChart(ByRef dt As DataTable) As AjaxControlToolkit.BarChart
        Dim brchrStats As New AjaxControlToolkit.BarChart

        Dim br As New AjaxControlToolkit.BarChartSeries


        Return brchrStats
    End Function



    Protected Function fillStatChart(ByRef dt As DataTable) As Chart

        Dim chrtStats As New Chart

        Dim rws = From rs In dt
                  Order By rs("StatCount") Descending
                  Take (5)
                  Select rs


        chrtStats.DataSource = rws
        chrtStats.Width = 500

        Dim ttlCat As New Title

        chrtStats.Titles.Add(ttlCat)



        Dim srsCategores As New Series()
        srsCategores.YValueMembers = "StatCount"
        srsCategores.XValueMember = "StatCd"
        srsCategores.CustomProperties = "DrawingStyle=Cylinder, MaxPixelPointWidth=50, ShadowOffset=2, IsValueShownAsLabel=True"

        chrtStats.Series.Add(srsCategores)


        Dim crarCategories As New ChartArea()
        crarCategories.BackGradientStyle = GradientStyle.TopBottom
        crarCategories.BackSecondaryColor = Color.Aqua  '"#B6D6EC"
        crarCategories.BorderDashStyle = ChartDashStyle.Solid
        crarCategories.BorderWidth = 2
        crarCategories.AxisX.MajorGrid.Enabled = False

        chrtStats.ChartAreas.Add(crarCategories)
        chrtStats.DataBind()

        Return chrtStats

    End Function

    Public Sub lbtnCat_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim btnLinkView As LinkButton = sender

        If btnLinkView.CommandName = "SelectCategory" Then

            Dim ParamValue As String = btnLinkView.CommandArgument


            Dim thisData As New CommandsSqlAndOleDb("SelTicketStats", Session("EmployeeConn"))
            thisData.AddSqlProcParameter("@CategoryTypeCd", ParamValue, SqlDbType.NVarChar)
            thisData.AddSqlProcParameter("@beginDate", txtBeginDate.Text, SqlDbType.DateTime)
            thisData.AddSqlProcParameter("@endDate", txtEndDate.Text, SqlDbType.DateTime)

            If rblPriority.SelectedValue <> "All" Then
                thisData.AddSqlProcParameter("@Priority", rblPriority.SelectedValue, SqlDbType.Int)

            End If


            Dim dt As DataTable = thisData.GetSqlDataTable


            pnlCats.Controls.Add(ListChartTable(dt, "Type"))
            ' Do something
            uplStats.Update()



        End If





    End Sub

    Protected Sub rblPriority_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles rblPriority.SelectedIndexChanged
        PopulateCategories()
        uplStats.Update()

    End Sub
End Class
