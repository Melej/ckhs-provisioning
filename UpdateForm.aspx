﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="false" CodeFile="UpdateForm.aspx.vb" Inherits="UpdateForm" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

<asp:UpdatePanel ID="uplSignOnForm" runat ="server" >

<Triggers>

</Triggers>

<ContentTemplate>
<table class="wrapper" style="border-style: groove" align="left">
<tr>
<td>


        <table border="3">
      <%--  <tr>
            <td>

            First Name:           
            
            </td>
             <td>
            <asp:Label ID = "first_nameLabel" runat="server" CssClass="labelsForm"></asp:Label>
              M:
                 <asp:Label ID = "middle_initialLabel" runat="server" CssClass="labelsForm" Width="50px"></asp:Label>
            </td>
        
          <td>
            Last Name:
            </td>
             <td>
            <asp:Label ID = "last_nameLabel" runat="server"></asp:Label>
            </td>
        </tr>
        
      <tr>
            <td colspan="4">
            Suffix: 
            <asp:Label ID ="suffixlabel" runat = "server"></asp:Label>
         
           
            Title:
            <asp:Label ID ="titlelabel" runat = "server"></asp:Label>
            </td>
        
        </tr>
        <tr>
            <td>
            Position:
            </td>
            <td>
             <asp:Label ID = "user_position_descLabel" runat="server"></asp:Label>
            </td>
          <td>
            CKHS Affiliation:
            </td>
            <td>
             <asp:Label ID ="emp_type_cdlabel" runat = "server">  </asp:Label>
            </td>
        </tr>
        <tr>
            <td>
            Start Date:
            </td>
            <td>
               <asp:Label ID ="start_dateLabel" runat="server" ></asp:Label>
            </td>
       
            <td>
            End Date:
            </td>
            <td>
                <asp:Label ID="end_dateLabel" runat="server" ></asp:Label>
            </td>
          </tr>
        
         <tr>
            <td>
            Entity:
            </td>
            <td>
            <asp:Label ID ="entity_nameLabel" runat="server" >
            
            </asp:Label>
            </td>
       
            <td>
            Department Name:
            </td>
            <td>
            <asp:Label ID ="department_namelabel" runat="server"> </asp:Label>
            </td>
        </tr>
         <tr>
            <td>
            Practice Name:
            </td>       
            <td colspan="3">
            <asp:Label ID ="practice_namelabel" runat="server"> </asp:Label>
            </td>
        </tr>
            <tr>
            <td>
            Phone Number:
            </td>
            <td>
            <asp:Label ID = "phoneLabel" runat="server"></asp:Label>
            </td>
       
            <td>
            Pager Number:
            </td>
            <td>
           <asp:Label ID = "pagerLabel" runat="server"></asp:Label>
            </td>
        </tr>
   
 
        <tr>
            <td>
            Email:
            </td>
            <td>
           <asp:Label ID = "emailLabel" runat="server"></asp:Label>
            </td>
            <td>
          Office Number
            </td>
            <td>
           <asp:Label ID = "office_numberLabel" runat="server"></asp:Label>
            </td>
        </tr>
         <tr>
            <td>
            Facility
            </td>
            <td>
         
              <asp:Label ID ="facility_nameLabel" runat="server">
             
            </asp:Label>
          
            </td>
        
            <td>
            Building
            </td>
            <td>
           
             <asp:Label ID ="building_nameLabel" runat="server">    
            </asp:Label>
          
            </td>
        </tr>--%>
    <tr>
            <td>
            *First Name:           
            
            </td>
             <td>
            <asp:TextBox ID = "first_nameTextBox" runat="server"></asp:TextBox>
              M:
                 <asp:TextBox ID = "middle_initialTextBox" runat="server" Width="50px"></asp:TextBox>
            </td>
        
          <td>
            *Last Name:
            </td>
             <td>
            <asp:TextBox ID = "last_nameTextBox" runat="server"></asp:TextBox>
            Suffix: 
            <asp:dropdownlist id="suffixddl"
				tabIndex="3" runat="server" Height="20px" Width="52px">
				<asp:ListItem Value="------" Selected="True">------</asp:ListItem>
				<asp:ListItem Value="Sr.">Sr.</asp:ListItem>
				<asp:ListItem Value="Jr.">Jr.</asp:ListItem>
				<asp:ListItem Value="II">II</asp:ListItem>
				<asp:ListItem Value="III">III</asp:ListItem>
				<asp:ListItem Value="IV">IV</asp:ListItem>
                </asp:dropdownlist>


            </td>
        </tr>
        
     
        <tr>
            <td>
            Position:
            </td>
            <td>
             <asp:TextBox ID = "user_position_descTextBox" runat="server"></asp:TextBox>
            </td>
            <td>
                 Title:
            <asp:dropdownlist id="ddlTitle"  tabIndex="4" runat="server" Height="20px" Width="52px">
				<asp:ListItem Value="------" Selected="True">------</asp:ListItem>
				<asp:ListItem Value="MD">MD</asp:ListItem>
				<asp:ListItem Value="RN">RN</asp:ListItem>
			</asp:dropdownlist>
            </td>
        
         <tr>
            <td>
            Entity:
            </td>
            <td>
            <asp:DropDownList ID ="entity_cdDDL" runat="server" AutoPostBack ="true">
            
            </asp:DropDownList>
            </td>
       
            <td>
            Department Name:
            </td>
            <td>
            <asp:DropDownList ID ="department_cdddl" runat="server">
            
            </asp:DropDownList>
            </td>
        </tr>
         <tr>
            <td>
            Practice Name:
            </td>       
            <td colspan="3">
            <asp:DropDownList ID ="practice_numddl" runat="server">
            
            </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>Address 1:</td>
            <td>  <asp:TextBox ID = "address_1textbox" runat="server"></asp:TextBox></td>
            <td>City:</td>
            <td>  <asp:TextBox ID = "citytextbox" runat="server"></asp:TextBox></td>
        </tr>
         <tr>
            <td>Address 2:</td>
            <td>  <asp:TextBox ID = "address_2textbox" runat="server"></asp:TextBox></td>
            <td>State:<asp:TextBox ID = "statetextbox" runat="server" Width="75px"></asp:TextBox></td>
            <td>Zip:  <asp:TextBox ID = "ziptextbox" runat="server"></asp:TextBox></td>
        </tr>
       
            <tr>
            <td>
            Phone Number:
            </td>
            <td>
            <asp:TextBox ID = "phoneTextBox" runat="server"></asp:TextBox>
            </td>
       
            <td>
            Pager Number:
            </td>
            <td>
           <asp:TextBox ID = "pagerTextBox" runat="server"></asp:TextBox>
            </td>
        </tr>
   
 
        <tr>
           <td>Fax:</td>
            <td><asp:TextBox ID = "faxtextbox" runat="server"></asp:TextBox></td>
           
           
            <td>
            Office Number:           
            </td>
            <td>
           <asp:TextBox ID = "office_numberTextBox" runat="server"></asp:TextBox>
            </td>
        </tr>
         <tr>
          <td>
            Email:
            </td>
            <td>
           <asp:TextBox ID = "emailTextBox" runat="server"></asp:TextBox>
            </td>
             <td colspan = "2"></td>
        </tr>
         <tr>
            <td>
            Facility
            </td>
            <td>
         
              <asp:DropDownList ID ="facility_cdddl" runat="server" AutoPostBack="true">
                <asp:ListItem Value="0" Selected="True">--Select--</asp:ListItem>
				<asp:ListItem Value="15">CCMC</asp:ListItem>
				<asp:ListItem Value="12">DCMH</asp:ListItem>
				<asp:ListItem Value="19">Taylor</asp:ListItem>
				<asp:ListItem Value="14">Community</asp:ListItem>
				<asp:ListItem Value="16">Springfield</asp:ListItem>
				<asp:ListItem Value="16">Corporate</asp:ListItem>
				<asp:ListItem Value="99">Other - Remote Doctors Office</asp:ListItem>
            </asp:DropDownList>
          
            </td>
        
            <td>
            Building
            </td>
            <td>
           
             <asp:DropDownList ID ="building_cdddl" runat="server">
            
            </asp:DropDownList>
          
            </td>
        </tr>
         
       
        <tr>
          
            <td colspan="2">
            CKHS Affiliation:
            </td>
            <td colspan="2">
             <asp:RadioButtonList ID ="emp_type_cdrbl" runat = "server" RepeatDirection="Horizontal">
                <asp:ListItem Selected ="True">Employee</asp:ListItem>
                <asp:ListItem>Contract</asp:ListItem>
                <asp:ListItem>Resident</asp:ListItem>
                <asp:ListItem>Other</asp:ListItem>
             </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td>
            *Start Date:
            </td>
            <td>
               <asp:TextBox ID ="start_dateTextBox" runat="server" CssClass="calenderClass"></asp:TextBox>
            </td>
       
            <td>
            End Date:
            </td>
            <td>
                <asp:TextBox ID="end_dateTextBox" runat="server" CssClass="calenderClass"></asp:TextBox>
            </td>
          </tr>
        
        </tr>
        <tr>
            <td>
            Group Number

            </td>
            <td>
                <asp:TextBox ID="group_numTextBox" runat="server"></asp:TextBox>
            </td>
            <td>
                Share Drive
            </td>
            <td >
                 <asp:TextBox ID="share_driveTextBox" runat="server"></asp:TextBox>
            </td>
        </tr>

        <tr>
            <th colspan = "4">

            Applications

            </th>
        </tr>
        
        <tr>
            <td colspan="4">            
            <asp:CheckBoxList ID = "cblApplications" runat = "server" RepeatColumns="3" >
            
            
            
            </asp:CheckBoxList>
                        
            </td>
        
        
        
        </tr>
        <tr>
            <td colspan = "4" align = "center">
            Additional Comments:            
            </td>
        
        </tr>
        <tr>
            <td colspan = "4">

                <asp:Label ID="requests_descLabel" runat ="server" TextMode = "MultiLine" Width="100%"></asp:Label>
            
            </td>
        
        </tr>
        <tr>
            <td colspan = "4">
                  <asp:Table ID = "tblPendingQueues" runat = "server">
                 </asp:Table>
            
            </td>
        
        </tr>
        
        <tr>
            <td colspan = "4" align="center">
                <asp:Button ID = "btnSubmit" runat = "server" Text ="Update Applications" />
            </td>
            
        </tr>

        </table>

</td>

</tr>

</table>

</ContentTemplate>


        </asp:UpdatePanel>


</asp:Content>

