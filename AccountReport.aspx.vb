﻿Imports System.IO
Imports System.Web

Imports System.Data
Imports System.Data.SqlClient
Imports App_Code

Partial Class AccountReport
    Inherits System.Web.UI.Page
    Dim sActivitytype As String = ""
    Dim cblBinder As New CheckBoxListBinder
    Dim UserInfo As UserSecurity
    Dim FormSignOn As New FormSQL()

    Dim sClientNum As String

    Dim ApplicationNumber As String
    Dim GroupNum As String
    Dim AccountCd As String
    Dim MaintType As String

    Dim sGnumber As String = ""
    Dim sCommGnumber As String = ""
    Dim sInvisionNumber As String = ""
    Dim GroupNumS As String = ""
    Dim sendate As String


    Dim totalcount As Integer
    Dim UnknowCount As Integer
    Dim EmployeeCount As Integer
    Dim ContractCount As Integer
    Dim StudentCount As Integer
    Dim PhysicianCount As Integer
    Dim NursingCount As Integer
    Dim EmpNoNumberCount As Integer
    Dim NonCKHNCount As Integer
    Dim ResidentCount As Integer


    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load


        If Not IsPostBack Then
            'If GNumber <> "" Then

            '    tabs.ActiveTabIndex = 1

            'End If
            Dim ddlBinder As New DropDownListBinder

            ddlBinder = New DropDownListBinder(Applicationddl, "SelApplicationCodes", Session("EmployeeConn"))
            ddlBinder.AddDDLCriteria("@AppSystem", "all", SqlDbType.NVarChar)
            'ddlBinder.AddDDLCriteria("@ApplicationGroup", "all", SqlDbType.NVarChar)

            ddlBinder.BindData("ApplicationNum", "ApplicationDesc")

            Dim ddbAffiliationBinder As New DropDownListBinder(Affiliationddl, "SelAffiliationForReport", Session("EmployeeConn"))

            ddbAffiliationBinder.BindData("AffiliationID", "AffiliationDisplay")

        End If


    End Sub
    Protected Sub btnSubmit_Click(sender As Object, e As System.EventArgs) Handles btnSubmit.Click

        'Loadwait.Show()

        If Appnumlbl.Text <> "" Then
            ' no counts
            fillTable()
        Else
            ' show counts not applogin name
            fillTableCounts()
        End If

        'UpdateProgress1.EnableViewState = "false"
        'Loadwait.Hide()

    End Sub
    Protected Sub Applicationddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles Applicationddl.SelectedIndexChanged
        Appnumlbl.Text = Applicationddl.SelectedValue.ToString
    End Sub
    Protected Sub fillTable()

        Dim thisData As New CommandsSqlAndOleDb("SelAccountsByApplicationV3", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@Applicationnum", Appnumlbl.Text, SqlDbType.Int)
        thisData.AddSqlProcParameter("@AffiliationID", empType.Text, SqlDbType.NVarChar)

        If displyrbl.SelectedValue.ToLower = "yes" Then
            thisData.AddSqlProcParameter("@enddate", "yes", SqlDbType.NVarChar)

        End If
        totalpan.Visible = False

        Dim iRowCount = 1
        Dim iTotalRowcount = 0

        Dim dt As DataTable
        dt = thisData.GetSqlDataTable

        Dim rwHeader As New TableRow

        Dim clSelectHeader As New TableCell
        Dim clNameHeader As New TableCell
        Dim clLoginNameHeader As New TableCell
        Dim clApploginCount As New TableCell

        Dim clEmpTypeHeader As New TableCell
        Dim clStartDateHeader As New TableCell


        clSelectHeader.Text = " "
        clNameHeader.Text = "  Name  "
        clLoginNameHeader.Text = "  Account Name "
        clApploginCount.Text = "  App Account "
        'clemailHeader.Text = "  eMail "

        clEmpTypeHeader.Text = " Affiliation "
        clStartDateHeader.Text = "  Start Date  "


        rwHeader.Cells.Add(clSelectHeader)
        rwHeader.Cells.Add(clNameHeader)
        rwHeader.Cells.Add(clLoginNameHeader)
        rwHeader.Cells.Add(clApploginCount)
        'rwHeader.Cells.Add(clemailHeader)

        rwHeader.Cells.Add(clEmpTypeHeader)
        rwHeader.Cells.Add(clStartDateHeader)


        rwHeader.Font.Bold = True
        rwHeader.BackColor = Color.FromName("#006666")
        rwHeader.ForeColor = Color.FromName("White")
        rwHeader.Height = Unit.Pixel(36)

        tblAccounts.Rows.Add(rwHeader)

        For Each drRow As DataRow In dt.Rows

            If iTotalRowcount = 0 Then
                totalcountlbl.Text = drRow("TotalCount")
                UnknowCountlbl.Text = drRow("UnknowCount")
                EmployeeCountlbl.Text = drRow("EmployeeCount")
                ContractCountlbl.Text = drRow("ContractCount")
                StudentCountlbl.Text = drRow("StudentCount")
                PhysicianCountlbl.Text = drRow("PhysicianCount")
                NursingCountlbl.Text = drRow("NursingCount")
                EmpNoNumberCountlbl.Text = drRow("EmpNoNumberCount")
                NonCKHNCountlbl.Text = drRow("NonCKHNCount")
                ResidentCountlbl.Text = drRow("ResidentCount")

            End If

            'Dim btnSelect As New Button
            Dim rwData As New TableRow

            Dim btnSelect As New TableCell
            Dim clNameData As New TableCell
            Dim clLoginNameData As New TableCell

            Dim clAppcount As New TableCell
            'Dim clAppNameData As New TableCell
            'Dim clEmailData As New TableCell


            Dim clEmpTypeData As New TableCell
            Dim clStartData As New TableCell



            btnSelect.Text = "<a href=""ClientDemo.aspx?ClientNum=" & drRow("client_num") & """ ><u><b> " & drRow("client_num") & "</b></u></a>"

            btnSelect.EnableTheming = False


            clNameData.Text = IIf(IsDBNull(drRow("FullName")), "", drRow("FullName"))
            clLoginNameData.Text = IIf(IsDBNull(drRow("LoginName")), "", drRow("LoginName"))

            'clAppNameData.Text = IIf(IsDBNull(drRow("AppLoginName")), "", drRow("AppLoginName"))
            clAppcount.Text = (IIf(IsDBNull(drRow("AccountCount")), "", drRow("AccountCount")))

            clEmpTypeData.Text = (IIf(IsDBNull(drRow("AffiliationDesc")), "", drRow("AffiliationDesc")))
            clStartData.Text = IIf(IsDBNull(drRow("StartDate")), "", drRow("StartDate"))

            'rwData.Cells.Add(clSelectData)
            rwData.Cells.Add(btnSelect)
            rwData.Cells.Add(clNameData)
            rwData.Cells.Add(clLoginNameData)
            rwData.Cells.Add(clAppcount)

            'rwData.Cells.Add(clAppNameData)
            'rwData.Cells.Add(clEmailData)

            rwData.Cells.Add(clEmpTypeData)
            rwData.Cells.Add(clStartData)


            If iRowCount > 0 Then

                rwData.BackColor = Color.Bisque

            Else

                rwData.BackColor = Color.LightGray

            End If

            tblAccounts.Rows.Add(rwData)

            iRowCount = iRowCount * -1
            iTotalRowcount = iTotalRowcount + 1

        Next
        tblAccounts.Width = New Unit("100%")

        If totalcountlbl.Text <> "0" Then
            totalpan.Visible = True
        End If
    End Sub
    Protected Sub fillTableCounts()
        Dim thisData As New CommandsSqlAndOleDb("SelAccountsByApplicationV3", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@Applicationnum", Appnumlbl.Text, SqlDbType.NVarChar)
        thisData.AddSqlProcParameter("@AffiliationID", empType.Text, SqlDbType.NVarChar)

        If displyrbl.SelectedValue.ToLower = "yes" Then
            thisData.AddSqlProcParameter("@enddate", "yes", SqlDbType.NVarChar)

        End If
        totalpan.Visible = False

        Dim iRowCount = 1
        Dim iTotalRowcount = 0

        Dim dt As DataTable
        dt = thisData.GetSqlDataTable



        ' IIf(IsDBNull(dt("TotalCount")), "", dt("TotalCount"))


        Dim rwHeader As New TableRow
        Dim clSelectHeader As New TableCell
        Dim clNameHeader As New TableCell
        Dim clLoginNameHeader As New TableCell
        Dim clApploginCount As New TableCell
        'Dim clemailHeader As New TableCell
        Dim clEmpTypeHeader As New TableCell
        Dim clStartDateHeader As New TableCell


        clSelectHeader.Text = ""
        clNameHeader.Text = "  Name  "
        clLoginNameHeader.Text = "  Account Name "
        clApploginCount.Text = "  App Account "
        'clemailHeader.Text = "  eMail "

        clEmpTypeHeader.Text = " Affiliation "
        clStartDateHeader.Text = "  Start Date  "


        rwHeader.Cells.Add(clSelectHeader)
        rwHeader.Cells.Add(clNameHeader)
        rwHeader.Cells.Add(clLoginNameHeader)
        rwHeader.Cells.Add(clApploginCount)
        'rwHeader.Cells.Add(clemailHeader)

        rwHeader.Cells.Add(clEmpTypeHeader)
        rwHeader.Cells.Add(clStartDateHeader)


        rwHeader.Font.Bold = True
        rwHeader.BackColor = Color.FromName("#006666")
        rwHeader.ForeColor = Color.FromName("White")
        rwHeader.Height = Unit.Pixel(36)

        tblAccounts.Rows.Add(rwHeader)

        For Each drRow As DataRow In dt.Rows

            If iTotalRowcount = 0 Then
                totalcountlbl.Text = drRow("TotalCount")
                UnknowCountlbl.Text = drRow("UnknowCount")
                EmployeeCountlbl.Text = drRow("EmployeeCount")
                ContractCountlbl.Text = drRow("ContractCount")
                StudentCountlbl.Text = drRow("StudentCount")
                PhysicianCountlbl.Text = drRow("PhysicianCount")
                NursingCountlbl.Text = drRow("NursingCount")
                EmpNoNumberCountlbl.Text = drRow("EmpNoNumberCount")
                NonCKHNCountlbl.Text = drRow("NonCKHNCount")
                ResidentCountlbl.Text = drRow("ResidentCount")


            End If


            'Dim btnSelect As New Button
            Dim rwData As New TableRow

            Dim btnSelect As New TableCell
            Dim clNameData As New TableCell
            Dim clLoginNameData As New TableCell
            Dim clAppcount As New TableCell


            'Dim clAppNameData As New TableCell
            'Dim clEmailData As New TableCell


            Dim clEmpTypeData As New TableCell
            Dim clStartData As New TableCell



            btnSelect.Text = "<a href=""ClientDemo.aspx?ClientNum=" & drRow("client_num") & """ ><u><b> " & drRow("client_num") & "</b></u></a>"

            btnSelect.EnableTheming = False


            clNameData.Text = IIf(IsDBNull(drRow("fullname")), "", drRow("fullname"))
            clLoginNameData.Text = IIf(IsDBNull(drRow("LoginName")), "", drRow("LoginName"))

            'clAppNameData.Text = IIf(IsDBNull(drRow("AppLoginName")), "", drRow("AppLoginName"))
            clAppcount.Text = (IIf(IsDBNull(drRow("AccountCount")), "", drRow("AccountCount")))

            clEmpTypeData.Text = (IIf(IsDBNull(drRow("AffiliationDesc")), "", drRow("AffiliationDesc")))
            clStartData.Text = IIf(IsDBNull(drRow("StartDate")), "", drRow("StartDate"))


            'rwData.Cells.Add(clSelectData)
            rwData.Cells.Add(btnSelect)
            rwData.Cells.Add(clNameData)
            rwData.Cells.Add(clLoginNameData)
            rwData.Cells.Add(clAppcount)

            'rwData.Cells.Add(clAppNameData)
            'rwData.Cells.Add(clEmailData)

            rwData.Cells.Add(clEmpTypeData)
            rwData.Cells.Add(clStartData)


            If iRowCount > 0 Then

                rwData.BackColor = Color.Bisque

            Else

                rwData.BackColor = Color.LightGray

            End If

            tblAccounts.Rows.Add(rwData)

            iRowCount = iRowCount * -1

            iTotalRowcount = iTotalRowcount + 1

        Next
        tblAccounts.Width = New Unit("100%")
        If totalcountlbl.Text <> "0" Then
            totalpan.Visible = True
        End If

    End Sub
    Public Sub bindAccount()


        Dim thisData As New CommandsSqlAndOleDb("SelAccountsByApplicationV3", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@Applicationnum", Appnumlbl.Text, SqlDbType.NVarChar)
        thisData.AddSqlProcParameter("@AffiliationID", empType.Text, SqlDbType.NVarChar)

        If displyrbl.SelectedValue.ToLower = "yes" Then
            thisData.AddSqlProcParameter("@enddate", "yes", SqlDbType.NVarChar)

        End If

        'If SearchVendorNameTextBox.Text = "" Then
        '    thisData.AddSqlProcParameter("@VendorName", "all", SqlDbType.NVarChar)
        'Else
        '    thisData.AddSqlProcParameter("@VendorName", SearchVendorNameTextBox.Text, SqlDbType.NVarChar)
        'End If

        Dim dt As DataTable

        dt = thisData.GetSqlDataTable

        GridAccounts.DataSource = dt
        GridAccounts.DataBind()


        GridAccounts.SelectedIndex = -1
    End Sub
    Protected Sub btnExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExcel.Click

        If displyrbl.SelectedValue.ToLower = "yes" Then
            sendate = displyrbl.SelectedValue

        End If

        Response.Redirect("./Reports/RolesReport.aspx?ReportName=AccountToUserReport" & "&Applicationnum=" & Appnumlbl.Text & "&EmpType=" & empType.Text & "&enddate=" & sendate, True)

        'CreateExcel2()
    End Sub
    Sub CreateExcel2()



        Response.AddHeader("content-disposition", "attachment;filename=FileName.xls")

        Response.Charset = String.Empty
        Response.ContentType = "application/vnd.ms-excel"
        '  Response.ContentType = "application/vnd.xls"

        Dim sw As System.IO.StringWriter = New System.IO.StringWriter()

        Dim hw As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(sw)

        '===============================Create form to contain grid
        Dim thisData As New CommandsSqlAndOleDb("SelAccountsByApplicationV3", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@Applicationnum", Appnumlbl.Text, SqlDbType.NVarChar)
        thisData.AddSqlProcParameter("@AffiliationID", empType.Text, SqlDbType.NVarChar)

        If displyrbl.SelectedValue.ToLower = "yes" Then
            thisData.AddSqlProcParameter("@enddate", "yes", SqlDbType.NVarChar)

        End If

        'If SearchVendorNameTextBox.Text = "" Then
        '    thisData.AddSqlProcParameter("@VendorName", "all", SqlDbType.NVarChar)
        'Else
        '    thisData.AddSqlProcParameter("@VendorName", SearchVendorNameTextBox.Text, SqlDbType.NVarChar)
        'End If

        Dim dt As DataTable

        dt = thisData.GetSqlDataTable

        GridAccounts.DataSource = dt
        GridAccounts.DataBind()

        'bindAccount()
        'GridAccounts.Visible = True

        Dim frm As New HtmlForm()
        'GridAccounts.Columns.RemoveAt(0)
        GridAccounts.Parent.Controls.Add(frm)
        frm.Attributes("RunAt") = "server"
        frm.Controls.Add(GridAccounts)

        frm.RenderControl(hw)
        Response.Write(sw.ToString())

        Response.End()

        'GridAccounts.Visible = False

    End Sub

    Protected Sub GridAccounts_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridAccounts.RowCommand
        If e.CommandName = "Select" Then

            sClientNum = GridAccounts.DataKeys(Convert.ToInt32(e.CommandArgument))("client_num").ToString()

            GridAccounts.SelectedIndex = -1

            Response.Redirect("./ClientDemo.aspx?ClientNum=" & sClientNum, True)


        End If

    End Sub

    Protected Sub btnReset_Click(sender As Object, e As System.EventArgs) Handles btnReset.Click
        Appnumlbl.Text = ""
        Response.Redirect("./AccountReport.aspx", True)

    End Sub

    Protected Sub Affiliationddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles Affiliationddl.SelectedIndexChanged
        empType.Text = Affiliationddl.SelectedValue

    End Sub


End Class
