﻿Imports System.IO
Imports System.Data '
Imports System.Data.SqlClient
Imports App_Code
Imports AjaxControlToolkit
Imports C1.Web.UI.Controls.C1Window

Partial Class AccountRequestItems
    Inherits MyBasePage '   Inherits System.Web.UI.Page


    Dim sActivitytype As String = ""
    Dim cblBinder As New CheckBoxListBinder
    Dim FormViewRequest As New FormSQL()
    Dim AccountRequestNum As Integer
    Dim AccountRequestType As String


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        FormViewRequest = New FormSQL(Session("EmployeeConn"), "SelAccountRequestV3", "ins_account_request", "", Page)
        AccountRequestNum = Request.QueryString("RequestNum")

        If Not IsPostBack Then

            FormViewRequest.AddSelectParm("@account_request_num", AccountRequestNum, SqlDbType.Int, 4)
            FormViewRequest.FillForm()
            fillOpenProviderItemsTable()


        End If

    End Sub


    Protected Sub fillOpenProviderItemsTable()
        Dim thisData As New CommandsSqlAndOleDb("SelRequestItems", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.NVarChar)

        Dim iRowCount = 1

        Dim dt As DataTable
        dt = thisData.GetSqlDataTable

        Dim rwHeader As New TableRow

        Dim clAccountHeader As New TableCell
        Dim clRequestTypeHeader As New TableCell
        Dim clStatusHeader As New TableCell



        clAccountHeader.Text = "Account"
        clRequestTypeHeader.Text = "Request Type"
        clStatusHeader.Text = "Status"



        rwHeader.Cells.Add(clAccountHeader)
        rwHeader.Cells.Add(clRequestTypeHeader)
        rwHeader.Cells.Add(clStatusHeader)


        rwHeader.Font.Bold = True
        rwHeader.BackColor = Color.FromName("#006666")
        rwHeader.ForeColor = Color.FromName("White")
        rwHeader.Height = Unit.Pixel(36)

        tblOpenProviderRequestItems.Rows.Add(rwHeader)

        For Each drRow As DataRow In dt.Rows


            Dim rwData As New TableRow

            Dim clAccountData As New TableCell
            Dim clRequestTypeData As New TableCell
            Dim clStatusData As New TableCell
            Dim clRequestorData As New TableCell
            Dim clReqPhoneData As New TableCell




            clAccountData.Text = IIf(IsDBNull(drRow("ApplicationDesc")), "", drRow("ApplicationDesc"))
            clRequestTypeData.Text = IIf(IsDBNull(drRow("account_request_type")), "", drRow("account_request_type"))
            clStatusData.Text = IIf(IsDBNull(drRow("valid_cd_desc")), "", drRow("valid_cd_desc"))

            rwData.Cells.Add(clAccountData)
            rwData.Cells.Add(clRequestTypeData)
            rwData.Cells.Add(clStatusData)


            If iRowCount > 0 Then

                rwData.BackColor = Color.Bisque

            Else

                rwData.BackColor = Color.LightGray

            End If

            tblOpenProviderRequestItems.Rows.Add(rwData)

            iRowCount = iRowCount * -1

        Next
        tblOpenProviderRequestItems.CellPadding = 10
        tblOpenProviderRequestItems.Width = New Unit("100%")


    End Sub






    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("./MyAccountQueue.aspx")

    End Sub



End Class
