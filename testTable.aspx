﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="false" CodeFile="testTable.aspx.vb" Inherits="testTable" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
         <table  width="90%" border="1px" >
         <tr>
                    <td class="tableRowHeader" colspan = "4">
                    Search for Requestor
                    </td>
         </tr>
         <tr>
                <td>
                    First Name
                </td>
                <td>
                     <asp:TextBox ID = "FirstNameTextBox" runat = "server"></asp:TextBox>
                </td>
                <td>
                    Last Name
                </td>
                <td>
                     <asp:TextBox ID = "LastNameTextBox" runat = "server"></asp:TextBox>

                </td>
       
         </tr>
         <tr>
            <td colspan="4">
            <table>
                <tr>
                    <td>
                              <asp:Button ID ="btnSearch" runat = "server" Text ="Search"  />
                    </td>
                    <td>
                             <asp:Button ID = "btnCloseOnBelafOf" runat = "server" Text="Close" />

                    </td>
                
                </tr>
            </table>
            </td>
         </tr>
          <tr>
           <td colspan = "4">
              <div class="scrollContentContainer" >
                 <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="200" AssociatedUpdatePanelID="uplOnBehalfOf"  >
                    <ProgressTemplate>
                        <div id="IMGDIV" align="center" valign="middle" runat="server" style="visibility:visible;vertical-align:middle;">
                            Loading Data ...
                         </div>
                    </ProgressTemplate>
                 </asp:UpdateProgress>
                    <asp:GridView ID="gvSearch" runat="server" AllowSorting="True" 
                        AutoGenerateColumns="False" BackColor="White" BorderColor="#336666"
                        BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                        EmptyDataText="No Items Found" EnableTheming="False" DataKeyNames="client_num, login_name"
                        Font-Size="Medium" GridLines="Horizontal" Width="100%">
                                   
                            <FooterStyle BackColor="White" ForeColor="#333333" />
                            <HeaderStyle BackColor="#336666" Font-Bold="False" ForeColor="White" Font-Size="Small" HorizontalAlign="Left"    />                                    
                            <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Left" Font-Size="Medium"/>
                            <RowStyle BackColor="White" ForeColor="#333333" />
                            <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#F7F7F7" />
                            <SortedAscendingHeaderStyle BackColor="#487575" />
                            <SortedDescendingCellStyle BackColor="#E5E5E5" />
                            <SortedDescendingHeaderStyle BackColor="#275353" />

                            <Columns>
                                <asp:CommandField ButtonType="Button" SelectText="Select" ShowSelectButton= "true" />
                                    <asp:BoundField  DataField="fullname"  HeaderText="Full Name" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
                    <%--            <asp:BoundField  DataField="login_name"  HeaderText="Login"></asp:BoundField>--%>
                                    <asp:BoundField  DataField="phone"  HeaderText="Phone" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
                                    <asp:BoundField  DataField="department_name"  HeaderText="Department" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
                                  
                            </Columns>
                     </asp:GridView>
                </div>
             </td>
           </tr>

        </table>
</asp:Content>

