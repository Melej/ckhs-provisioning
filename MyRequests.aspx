﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="false" CodeFile="MyRequests.aspx.vb" Inherits="MyRequests" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">


<asp:UpdatePanel ID="upnlMyrequest" runat ="server" UpdateMode="Conditional" >

<Triggers>

 <asp:AsyncPostBackTrigger ControlID = "btnChange" EventName ="Click" />

</Triggers>

  <ContentTemplate>
    <table>
        
        
            <tr>
                    <td  class="tableRowHeader">
                        <asp:Label ID="Requestheadlabel" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Button id="btnChange" runat="server" width="275px"/>

                    </td>
                </tr>
                <tr>
                    <td align="center">
        
                        <asp:Label ID="showRequests" runat ="server" Text = " Show " Font-Bold="True" />
                        <asp:RadioButtonList ID="allrequestsrbl" runat="server" AutoPostBack="true" RepeatDirection="Horizontal" Font-Size="Small" >
                            <asp:ListItem  Selected="True" Value="no" Text="Last 6 Months" />

                            <asp:ListItem Value="yes" Text="All My Requests" />

                                </asp:RadioButtonList>

                    </td>
                </tr>
                <tr>
                <td>
                    <asp:Label ID="alltimelabel" runat="server" Visible="false" />
                </td>
            </tr>


        <tr>
            <td>
            
              <ajaxToolkit:TabContainer ID="tabs" runat="server" ActiveTabIndex="0"
                Width="100%"  EnableTheming="False" 
                BackColor="#efefef">

                  <ajaxToolkit:TabPanel ID="tpDemographics" runat="server"  BackColor="Gainsboro" Visible = "True" EnableTheming="False">

                       <HeaderTemplate > 
                            <asp:Label ID="Label1" runat="server" Text="Account Requests" Font-Bold="True" 
                                ForeColor="Black"></asp:Label>
                     </HeaderTemplate>
                       <ContentTemplate>

                            <table style="border-style: groove" align="center" width="100%" >
                                <tr >
                                    <td>

                                        <asp:Table  Width="100%" ID = "tblProviderQueue" runat = "server">
                                        </asp:Table>
                                    </td>
                                   </tr>
                             </table>
                        </ContentTemplate>
                   </ajaxToolkit:TabPanel>

                    <ajaxToolkit:TabPanel ID="tpRequestItems" runat="server" TabIndex="0" BorderStyle="Solid" BorderWidth="1px" Visible="false">
                        <HeaderTemplate>
                            <asp:Label ID="Label6" runat="server" Font-Bold="true" ForeColor="Black" 
                                Text="RFS/Tickets"></asp:Label>
                        </HeaderTemplate>
                            <ContentTemplate>
                                <table border="0" width="100%">
                                    <tr>
                                        <td colspan="4">
                                            <asp:Table ID="tblRFSTickets" runat="server">
                                            </asp:Table>
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                    </ajaxToolkit:TabPanel>
            </ajaxToolkit:TabContainer>

         </td>
        </tr>
        <tr>
            <td>
                        <asp:Label ID="WhoclientNumLabel" runat="server" Visible="False"></asp:Label>

            </td>
        </tr>
    </table>
  </ContentTemplate>
</asp:UpdatePanel>
</asp:Content>

