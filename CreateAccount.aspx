﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="false" CodeFile="CreateAccount.aspx.vb" Inherits="CreateAccount" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
<asp:UpdatePanel runat="server" ID="uplPage" UpdateMode="Conditional">


<ContentTemplate>
 <table  width= "100%">
   <tr>
     <td align="left">
       <asp:Label ID = "LblMainInfo" runat = "server" ForeColor="Red"/>
    </td>
  </tr>
  <tr>
      <td align="left">
          <ajaxToolkit:TabContainer ID="tabs" runat="server" ActiveTabIndex="0" 
              BackColor="#efefef" EnableTheming="true" style="margin-bottom: 0px" 
              Width="100%">
              <ajaxToolkit:TabPanel ID="tpheader" runat="server" BackColor="Gainsboro" 
                  TabIndex="0" Visible="True">
                  <HeaderTemplate>
                      <asp:Label ID="Label2" runat="server" Font-Bold="true" ForeColor="Black" 
                          Text="This Account Request"></asp:Label>
                  </HeaderTemplate>

                  <ContentTemplate>
                      <table id="tblHeader" border="1px" cellpadding="5px" style="empty-cells:show" 
                          width="100%">
                          <tr>
                              <td class="tableRowHeader" colspan="4">
                                  Create Account Request
                              </td>
                          </tr>
                           <tr align="center">
                                <td colspan="4" align="center">
                                    <asp:Label ID = "lblRehire" runat = "server" ForeColor="Red" Font-Bold="true" Font-Size="Large" />
                                </td>
            
                            </tr>
                          <tr>
                              <td colspan="2" style="font-weight:bold">
                                  Client Name
                              </td>
                              <td colspan="2">
                                  <asp:Label ID="first_namelabel" runat="server"></asp:Label>
                                  &nbsp;<asp:Label ID="last_namelabel" runat="server"></asp:Label>
                              </td>
                          </tr>
                          <tr>
                              <td colspan="2" style="font-weight:bold">
                                  Requestor Name
                              </td>
                              <td colspan="2">
                                  <asp:Label ID="requestor_namelabel" runat="server"></asp:Label>
                              </td>
                          </tr>
                          <tr>
                              <td colspan="2" style="font-weight:bold">
                                  Request Type
                              </td>
                              <td colspan="2">
                                  <asp:Label ID="RequestCommentlabel" runat="server"></asp:Label>
                              </td>
                          </tr>
                          <tr>
                              <td colspan="2" style="font-weight:bold">
                                  Submit Date
                              </td>
                              <td colspan="2">
                                  <asp:Label ID="entered_datelabel" runat="server"></asp:Label>
                              </td>
                          </tr>
                          <tr>
                              <td colspan="2" style="font-weight:bold">
                                  Validation Date:
                              </td>
                              <td colspan="2">
                                  <asp:Label ID="valid_dateLabel" runat="server"></asp:Label>
                              </td>
                          </tr>
                          <tr>
                              <td colspan="4">
                                  <table>
                                      <tr>
                                          <td>
                                          </td>
                                          <td>
                                              <asp:Button ID="btnCreateSubmit" runat="server" Text="Submit Account Creation "
                                              CssClass="btnhov" BackColor="#006666"  Width="225px" />
                                          </td>
                                          <td>
                                              <asp:Button ID="btnInvalid" runat="server" Text="Mark Account Invalid "
                                                CssClass="btnhov" BackColor="#006666"  Width="225px" />
                                          </td>
                                          <td>
                                              <asp:Button ID="btnCheckLDAP" runat="server" Text="Check LDAP" Visible="false"
                                               CssClass="btnhov" BackColor="#006666"  Width="175px" />
                                          </td>

                                          <td>
                                              <asp:Button ID="btnSaveItemInfo" runat="server" Text="Save Comments" Visible="false"
                                               CssClass="btnhov" BackColor="#006666"  Width="175px" />
                                          </td>
										  <td>
                                              <asp:Button ID="btnGotoPMHRequest" runat="server" Text="Go To Request" Visible="false"
                                               CssClass="btnhov" BackColor="#006666"  Width="175px" />
                                          </td>
                                          <td>
                                              <asp:Button ID="btnSubmitApprovBy" runat="server" Text="Submit Approve By" Visible="false"
                                               CssClass="btnhov" BackColor="#006666"  Width="175px" />
                                          </td>
                                      </tr>
                                  </table>
                              </td>
                          </tr>
                          <tr>
                              <td class="tableRowSubHeader" colspan="4">
                                  Account Information
                              </td>
                          </tr>
                          <tr>
                              <td style="font-weight:bold">
                                  Account
                              </td>
                              <td colspan="3">
                                  <asp:Label ID="AccountLabel" runat="server"></asp:Label>
                              </td>
                          </tr>
                          <tr>
                              <td style="font-weight:bold">
                                  User Name
                              </td>
                              <td colspan="3">
                                  <asp:TextBox ID="LoginIDTextBox" runat="server" Width="450px"></asp:TextBox>
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  <asp:Label ID="lblAppname" runat="server" Font-Bold="True" ForeColor="Red" 
                                      Visible="false" />
                              </td>
                              <td colspan="3">
                                  <asp:TextBox ID="AppnameTextBox" runat="server" Visible="false" Width="300px" />
                                  <asp:TextBox ID="CernerPositionDescTextBox" runat="server" Visible="false" Width="300px" />
                              </td>
                          </tr>

<%--                            <tr>
                              <td style="font-weight:bold">
                                  <asp:Label ID="lblLocationofCare" runat="server" text="CKHN Location of Care (G#)" 
                                      Visible="false" />
                              </td>
                              <td>
                                  <asp:Label ID="T1LocationOfCareIDLabel" runat="server" Visible="false" Width="300px" />
                              </td>
                              <td style="font-weight:bold">
                                  <asp:Label ID="lblCopyTo" runat="server" text="CKHN Copy To MedAssist." 
                                      Visible="false" />
                              </td>
                              <td>
                                  <asp:Label ID="T1CopyToLabel" runat="server" Visible="false" Width="300px" />
                              </td>
                            
                            </tr>

                            <tr>
                              <td style="font-weight:bold">
                                  <asp:Label ID="lblCommLocationofcare" runat="server" text="COMM. Location of Care (G#)" 
                                      Visible="false" />
                              </td>
                              <td>
                                  <asp:Label ID="T1CommLocationOfCareIDLabel" runat="server" Visible="false" Width="300px" />
                              </td>
                              <td style="font-weight:bold">
                                  <asp:Label ID="lblCommCopyTo" runat="server" text="COMM. Copy To MedAssist." 
                                      Visible="false" />
                              </td>
                              <td>
                                  <asp:Label ID="T1CommCopytoLabel" runat="server" Visible="false" Width="300px" />
                              </td>

                            </tr>--%>

                            <tr>
                              <td style="font-weight:bold">
                                  <asp:Label ID="LblDoctormaster" runat="server" text="Doctor Master/HIS" 
                                      Visible="false" />
                              </td>
                              <td colspan="3">
                                  <asp:Label ID="lbldoctormastertxt" runat="server"  Visible="false" />
                              </td>
                            </tr>
                            <tr>
                             <td  colspan="4" align="center">
                              <asp:Panel Visible="false" runat="server" id="epcspnl">
                                <div>
                                 <table width="100%" border="1px">
                                    <tr>

                                        <td style="font-weight:bold">
                                             <asp:Label ID="lblneedapproval" runat="server" Text="Needs EPCS Approval" Visible="false" />

                                        </td>
                                        
                                        
                                        <td style="font-weight:bold">
                                            <asp:Label ID="lblneednomination" runat="server" Text="Needs EPCS Nomination" Visible="false" />
                                            <asp:TextBox ID="EPCSNominationTextbox" runat="server"  Visible="false" />
                                        
                                        </td>
                                    </tr>
                                     <tr>
                                         <td>

                                            <asp:RadioButtonList ID="Approvalrbl" runat="server" RepeatDirection="Horizontal" Visible="false"  AutoPostBack="True">
                                             <asp:ListItem Value="yes">Yes</asp:ListItem>
                                             <asp:ListItem value="no" Selected="True">No</asp:ListItem>
                                            </asp:RadioButtonList>
                                        
                                            <asp:TextBox ID="HDApprovalTextBox" runat="server"  Visible="false" />
                                        </td>

                                        <td>

                                            <asp:RadioButtonList ID="Nominaationrbl" runat="server" RepeatDirection="Horizontal" Visible="false" AutoPostBack="True">
                                             <asp:ListItem Value="yes">Yes</asp:ListItem>
                                             <asp:ListItem value="no" Selected="True">No</asp:ListItem>
                                            </asp:RadioButtonList>
                                            <asp:TextBox ID="HDNominationTextBox" runat="server"  Visible="false" />

                                        
                                        </td>
                                    
                                    </tr>
                               
                                </table>
                                </div>
                              </asp:Panel>
                             </td>
                            </tr>

                            <tr>
                                <td colspan="4" align="center">
                                    <asp:Panel ID="financialPnl" Visible="false" runat="server">
                                     <div>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblApproveBy" runat="server" text="Approved By ->" ></asp:Label>
                                                </td>
                                                <td>    
                                                    <asp:Label ID="lblLastApprvedBy" runat="server" ></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label id="lblLevel" runat="server" Visible="false" Text="Level Needed ->"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblLevelSubmited" runat="server" Visible="false"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lbldollar" runat="server" Text="Dollar Amount ->"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblDollarSubmitted" runat="server" Visible="false"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>  
                                       </div>                                  
                                    </asp:Panel>                            
                                </td>
                            </tr>

                         <tr>
                             <td align="center" colspan="4">
   	                          <%-- =================== Nurse Stations for mak Pyxis ===============================--%>
                              <asp:Panel id="CCMCNurseStationspan" runat="server"  Visible="false">
                                <table  border="3px" width="100%">
                                    <tr>
                                        <td>
                                          CCMC
                                        </td>
                                    </tr>
    <%--                                <tr>
                                        <td valign="top">
                                             <asp:CheckBoxList ID="cblCCMCAll" runat="server" RepeatColumns="0"
                                                 AutoPostBack="true">
                                                 <asp:ListItem Value="ccmcall" Text="All CCMC" ></asp:ListItem>
                                             </asp:CheckBoxList>                                    
                                        </td>
                                    </tr>--%>

                                    <tr>
                                        <td valign="top">
                                           <asp:CheckBoxList ID="cblCCMCNursestations" runat="server" RepeatColumns="3"
                                                 AutoPostBack="true" >
                                             </asp:CheckBoxList>                                    
                                        </td>


                                    </tr>
                                </table>
                              </asp:Panel>
                              <asp:Panel ID="dcmhnursepan" runat="server" Visible="false">
                                <table  border="3px" width="100%">
                                    <tr>
                                        <td>
                                            DCMH
                                        </td>
                                    </tr>
    <%--                                <tr>
                                        <td valign="top">
                                             <asp:CheckBoxList ID="cblDCMHAll" runat="server" RepeatColumns="0"
                                                 AutoPostBack="true" >
                                                 <asp:ListItem Value="dcmhall" Text="All DCMH"></asp:ListItem>
                                             </asp:CheckBoxList>                                    
                                        </td>
                                    </tr>--%>
                                    <tr>
                                        <td valign="top">
                                           <asp:CheckBoxList ID="cblDCMHNursestataions" runat="server" RepeatColumns="3"
                                                 AutoPostBack="true">
                                             </asp:CheckBoxList>                                    
                                        </td>
                                    </tr>
                                </table>
                          
                              </asp:Panel>
                              <asp:Panel ID="taylornursepan" runat="server" Visible="false">
                                <table  border="3px" width="100%">
                                    <tr>
                                        <td valign="top">
                                        Taylor
                                        </td>
                                    </tr>

    <%--                                <tr>
                                        <td valign="top">
                                             <asp:CheckBoxList ID="cblTAll" runat="server" RepeatColumns="0"
                                                 AutoPostBack="true" >
                                                 <asp:ListItem Value="taylorall" Text="All Taylor"></asp:ListItem>
                                             </asp:CheckBoxList>                                    
                                        </td>
                                    </tr>--%>
                                    <tr>
                                        <td valign="top">
                                           <asp:CheckBoxList ID="cblTaylorNursestataions" runat="server" RepeatColumns="3"
                                                 AutoPostBack="true">
                                             </asp:CheckBoxList>                                    
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblAppSystem" runat="server" Visible="false"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                          
                              </asp:Panel>
                              <asp:Panel ID="SpringNurseStationsPan" runat="server"  Visible="false">

                                 <table id="Table1"  border="3px" width="100%">
                                    <tr>
                                    
                                        <td>
                                          Springfield
                                         </td>
                                    </tr>
    <%--                                <tr>
                                        <td>
                                             <asp:CheckBoxList ID="cblSAll" runat="server" RepeatColumns="0"
                                                 AutoPostBack="true">
                                                 <asp:ListItem Value="springall" Text="All Springfield"></asp:ListItem>
                                             </asp:CheckBoxList>
                                        </td>
                                    
                                    </tr>--%>
                                    <tr>
                                        <td valign="top">
                                           <asp:CheckBoxList ID="cblSpringfieldNursestataions" runat="server" RepeatColumns="3"
                                                 AutoPostBack="true" >
                                             </asp:CheckBoxList>

                                        </td>
                                
                                    </tr>
                                 </table>
                                </asp:Panel>
                             </td>
                         </tr> 
                          <tr>
                             <td align="center" colspan="4">
   	                          <%-- =================== 365 List sites ===============================--%>

                              <asp:Panel ID="O365Panel" runat="server"  Visible="false">

                                 <table id="Table2"  border="3px" width="100%">
                                    <tr>
                                        <td>
                                            O365 asccounts
                                    
                                        </td>
                                        <td>
                                             <asp:CheckBoxList ID="cblo365" runat="server" RepeatColumns="0"
                                                 AutoPostBack="false">
                                             </asp:CheckBoxList>
                                        </td>
                                    
                                    </tr>
                                 </table>
                                </asp:Panel>
                             </td>
                         </tr>

                          <tr>
                              <td colspan="4" style="font-weight:bold" valign="top">
                                  Comments
                              </td>
                          </tr>
                          <tr>
                              <td colspan="4">
                                  <asp:TextBox ID="CommentsTextBox" runat="server" Rows="5" TextMode="MultiLine" 
                                      Width="98%"></asp:TextBox>
                                  <br />
                                  <br />
                                  <span style="float:right"></span>
                              </td>
                          </tr>
                          <tr>
                              <td colspan="4">
                                  <asp:TextBox ID="ItemCompleteDateTextBox" runat="server" Visible="false" />
<%--                                  <asp:Button ID="btnRequestDetails" runat="server" Text="Request Details" 
                                      Visible="false" />
--%>                              </td>
                          </tr>
                          <tr>
                              <td align="center" colspan="4">
                                  <asp:Panel ID="PanInvalid" runat="server" Visible="False">
                                      <table ID="tblInvalid" runat="server" bgcolor="gainsboro" border="2" 
                                          bordercolor="navy" cellpadding="10" cellspacing="1" rules="none" width="350px">
                                          <tr>
                                              <td align="center" colspan="2" style="font-weight: bold; height: 25px;">
                                                  Provide Invalid Reason or Comment</td>
                                          </tr>
                                          <tr>
                                              <td align="center" colspan="2">
                                                  <asp:DropDownList ID="Invalidddl" runat="server" Width="250px">
                                                  </asp:DropDownList>
                                              </td>
                                          </tr>
                                          <tr>
                                              <td align="center">
                                                  <asp:Button ID="btnSubmitInvalid" runat="server" BackColor="#00C000" 
                                                      Font-Bold="True" ForeColor="Black" Height="25px" Text="Submit"  Width="120px" 
                                                      ToolTip="This will Identify this 1 Account as Invalid " />
                                              </td>
                                              <td align="center">
                                                  <asp:Button ID="btnCancelInvalid" runat="server" BackColor="DimGray" 
                                                      Font-Bold="True" ForeColor="White" Height="25px" Text="Cancel" Width="120px" />
                                              </td>
                                          </tr>
                                          <tr>
                                              <td align="center" colspan="2">
                                                  Comments:
                                              </td>
                                          </tr>
                                          <tr>
                                              <td align="center" colspan="2">
                                                  <asp:TextBox ID="InvalidDescTextBox" runat="server" Font-Size="Medium" 
                                                      Height="100px" Width="80%" />
                                              </td>
                                          </tr>
                                      </table>
                                  </asp:Panel>
                              </td>
                          </tr>
                          <tr>
                            <td align="center" colspan="4">
                          <asp:Panel  id="PanDemo" runat="server" >
                          <table width="100%" id="demoTbl">
                           <tr>
                              <td align="left">
                                  <asp:Label ID="lblValidation" runat="server" ForeColor="Red" />
                              </td>
                          </tr>
                          <tr>
                              <td class="tableRowHeader">
                                  Account Request Information
                              </td>
                          </tr>
                          <tr>
                              <td align="left" class="tableRowSubHeader">
                                  Form Submission
                              </td>
                          </tr>
                          <tr>
                              <td align="center">
                                  <asp:Button ID="btnGoToRequest" runat="server" Text="Go To Request "
                                  CssClass="btnhov" BackColor="#006666"  Width="250px" />
                              </td>
                          </tr>
                          <tr>
                             <td align="center">
                             
                                <table>
                                 <tr align="center">
                                
                                
                                  <td align="left">
                                      User submitting:
                                      <asp:Label ID="submitter_nameLabel" runat="server" Font-Bold="True"></asp:Label>
                                  </td>
                                  <td align="left">
                                      Requestor:
                                      <asp:Label ID="requestor_nameLabel2" runat="server" Font-Bold="True"></asp:Label>
                                  </td>

                                
                                </tr>
                             </table>
                             </td>

                          </tr>
                          <tr>
                              <td align="left" class="tableRowSubHeader">
                                  CKHS Affiliation:
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  <asp:RadioButtonList ID="emp_type_cdrbl" runat="server" AutoPostBack="True" 
                                      Font-Size="Smaller" RepeatDirection="Horizontal">
                                  </asp:RadioButtonList>
                              </td>
                          </tr>

                           <tr>
                               <td align="left" class="tableRowSubHeader" colspan="4">
                                     Provider Information
                                </td>
                           </tr>
                           <tr>
                             <td>                           
                                <asp:Panel ID="pnlProvider" runat="server" Visible="False">
                                    <table id="tblprovider" border="1" width="100%">
                                          <tr>
                                              <td align="right">
                                                  Privileged Date:
                                              </td>
                                              <td align="left">
                                                  <asp:TextBox ID="credentialedDateTextBox" runat="server" 
                                                      CssClass="calenderClass"></asp:TextBox>
                                              </td>
                                             <%-- <td align="right">
                                                  Privileged DCMH Date:
                                              </td>
                                              <td align="left">
                                                  <asp:TextBox ID="CredentialedDateDCMHTextBox" runat="server" 
                                                       Enabled="false"></asp:TextBox>
                                              </td>--%>


                                            <td align="right">
                                                <asp:Label ID="ccmcadmitlbl" runat="server" Text ="Admitting Rights:" Width="150px" />   
                                            </td>
                                            <td align="left" >
                                                   <asp:RadioButtonList ID = "CCMCadmitRightsrbl" runat="server" AutoPostBack="true"
                                                   RepeatDirection="Horizontal">
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                    <asp:listItem>No</asp:listItem>
                                                </asp:RadioButtonList>

                                            </td>
<%--                                            <td align="right">
                                                <asp:Label ID="Label3" runat="server" Text ="DCMH Admitting Rights:" Width="150px" />
                                             </td>

                                            <td align="left" >
                                                   <asp:RadioButtonList ID = "DCMHadmitRightsrbl" runat="server" AutoPostBack="true"
                                                   RepeatDirection="Horizontal">
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                    <asp:listItem>No</asp:listItem>
                                                </asp:RadioButtonList>

                                            </td>--%>

                                        </tr>
                                          <tr>
                                              <td align="right">
                                                  CHMG:
                                              </td>
                                              <td align="left">
                                                  <asp:RadioButtonList ID="hanrbl" runat="server" RepeatDirection="Horizontal">
                                                      <asp:ListItem>Yes</asp:ListItem>
                                                      <asp:ListItem Selected="True">No</asp:ListItem>
                                                  </asp:RadioButtonList>
                                              </td>
                                              <td align="right">
                                                  Doctor Number:
                                              </td>
                                              <td align="left">
                                                  <asp:TextBox ID="doctor_master_numTextBox" runat="server"></asp:TextBox>
                                              </td>
                                          </tr>

<%--                                        <tr>
                                              <td align="right">
                                                    <asp:Label ID="lblReffer" runat="server" Text="Referring Clinician:" Width="150px" />
                              
                                              </td>
                                              <td align="left">
                                                  <asp:RadioButtonList ID="ReferringClinicianrbl" runat="server" 
                                                      RepeatDirection="Horizontal">
                                                      <asp:ListItem>Yes</asp:ListItem>
                                                      <asp:ListItem>No</asp:ListItem>
                                                  </asp:RadioButtonList>                              
                                              </td>
                                            <td colspan="2">
                                            </td>


                                        </tr>--%>

                                          <tr>
                                              <td align="right">
                                                  NPI:
                                              </td>
                                              <td align="left">
                                                  <asp:TextBox ID="npitextbox" runat="server"></asp:TextBox>
                                              </td>
                                              <td align="right">
                                                  License No.
                                              </td>
                                              <td align="left">
                                                  <asp:TextBox ID="LicensesNoTextBox" runat="server"></asp:TextBox>
                                              </td>
                                          </tr>
                                          <tr>
                                              <td align="right">
                                                  Taxonomy:
                                              </td>
                                              <td align="left" colspan="3">
                                                  <asp:TextBox ID="taxonomyTextBox" runat="server"></asp:TextBox>
                                              </td>
                                            </tr>
<%--                                          <tr>
                                              <td align="right">
                                                  <asp:Label ID="lblprov" runat="server" Text="CHMG :" Width="150px" />
                                              </td>
                                              <td align="left" colspan="3">
                                                  <asp:RadioButtonList ID="providerrbl" runat="server" 
                                                      RepeatDirection="Horizontal">
                                                      <asp:ListItem>Yes</asp:ListItem>
                                                      <asp:ListItem>No</asp:ListItem>
                                                  </asp:RadioButtonList>
                                              </td>
                                          </tr>--%>

                                            <tr>
                                              <td colspan="4">
                                               <table border="3px" width="100%">

                                                    <tr>
                                                    <td colspan="4"  class="tableRowSubHeader" align="center">
                                                        Physician Groups 
                                                    </td>
                                                    </tr>


                                                    <tr>
                                                         <td align="right">
                                                                <asp:Label ID ="ckhnlbl" runat="server" Text="CHMG:" Visible="false" >
                                                                </asp:Label>

                                                            </td>
                                                            <td colspan="3" align="left">
                                                            <asp:DropDownList ID = "LocationOfCareIDddl" runat="server" Visible="false" Width="90%" >
                                                            </asp:DropDownList>

                                                            </td>
                                
                                
                                                    </tr>
<%--                                                    <tr>

                                                            <td align="right">
                                                                Comm. Location Of Care:
                                                            </td>
                                                            <td colspan="3" align="left">
                                                                <asp:DropDownList ID = "CommLocationOfCareIDddl" runat="server" Width="90%">
                                            
                                                                </asp:DropDownList>
                                                
                                                            </td>
                                
                                                    </tr>--%>

                                                     <tr>
                                                        <td align="right" >
                                                        Provider Group Names:
                                                        </td>       
                                                        <td colspan="3" align="left">
                                                        <asp:DropDownList ID ="group_nameddl" runat="server" Width="90%">
            
                                                        </asp:DropDownList>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td align="right" >
                                                           <asp:Label id="invisionLbl" runat="server" Text="eCare Group Number:" Visible="false"></asp:Label>
                                                        </td>
                                                        <td align="left"  colspan="2">
                                                                <asp:TextBox ID="group_numTextBox" runat="server" Enabled="false" Visible="false"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                                <asp:TextBox ID="InvisionGroupNumTextBox" runat="server"  Enabled="false" Visible="false"></asp:TextBox>

                                                        </td>
                                                    </tr>

<%--                                                    <tr>
                                                        <td  align="right">
                                                            Coverage Group ID:
                                                        </td>
                                                        <td  colspan="3" align="left">
                                                                <asp:DropDownList ID = "CoverageGroupddl" runat="server" Width="90%">
                                            
                                                                </asp:DropDownList>

                                                        </td>
                                                    </tr>

                                                    <tr>
                                
                                                        <td  align="right">
                                                            Non Perfered Phys Groups:
                                                        </td>
                                                        <td  colspan="3" align="left">
                                                                <asp:DropDownList ID = "Nonperferdddl" runat="server" Width="90%">
                                            
                                                                </asp:DropDownList>
                                                        </td>
                                
                                                    </tr>--%>

<%--                                                    <tr>

                                                        <td align="right">
                                                            CKHN Copy To MedAssist.:
                                                        </td>
                                                         <td>
                                                            <asp:TextBox ID = "CopyToTextBox" runat="server"></asp:TextBox>
                                                        </td>
                                                        <td align="right">
                                                            Comm. Copy To MedAssist.:
                                                        </td>
                                                         <td>
                                                            <asp:TextBox ID = "CommCopyToTextBox" runat="server"></asp:TextBox>
                                                         </td>

                                                    </tr>--%>

                                                </table>
                                            </td>
                                            </tr>
                                          <tr>
                                              <td align="right">
                                                  Title:
                                              </td>
                                              <td align="left">
                                                  <asp:DropDownList ID="Titleddl" runat="server" Visible="false">
                                                  </asp:DropDownList>
                                                 <asp:TextBox ID="TitleTextBox" runat="server" Visible="true"> </asp:TextBox>

                                              </td>
                                              <td align="right">
                                                  Specialty:
                                              </td>
                                              <td align="left">
                                                  <asp:DropDownList ID="Specialtyddl" runat="server" Visible="false">
                                                  </asp:DropDownList>
                                                  <asp:Label ID ="SpecialtyLabel" runat="server" Visible="true"></asp:Label>
                                              </td>
                                          </tr>

                                          
                                        <tr>
                                              <td align="right">
                                                  Medicaid ID:
                                              </td>
                                              <td align="left">
                                                  <asp:TextBox ID="MedicareIDTextBox" runat="server" Width="250px"></asp:TextBox>
                                              </td>
                                              <td align="right">
                                                  BlueCross ID:
                                              </td>
                                              <td align="left">
                                                  <asp:TextBox ID="BlueCrossIDTextBox" runat="server" Width="250px"></asp:TextBox>
                                              </td>
                                          </tr>
                                          <tr>
                                              <td align="right">
                                                  DEA ID:
                                              </td>
                                              <td align="left">
                                                  <asp:TextBox ID="DEAIDTextBox" runat="server" Width="250px"></asp:TextBox>
                                              </td>
                                              <td align="right">
                                                 
                                              </td>
                                              <td align="left">
                                                  <%--<asp:TextBox ID="DEAExtensionIDTextBox" runat="server" Width="250px"></asp:TextBox>--%>
                                              </td>
                                          </tr>
                                         
<%--                                        <tr>
                                              <td align="right">
                                                  Medic Group ID:
                                              </td>
                                              <td align="left">
                                                  <asp:TextBox ID="MedicGroupIDTextBox" runat="server" Width="250px"></asp:TextBox>
                                              </td>
                                              <td align="right">
                                                  Medic Group Phys ID:
                                              </td>
                                              <td align="left">
                                                  <asp:TextBox ID="MedicGroupPhysIDTextBox" runat="server" Width="250px"></asp:TextBox>
                                              </td>
                                          </tr>
                                          <tr>
                                              <td align="right">
                                                  Uniform Physician ID:
                                              </td>
                                              <td align="left">
                                                  <asp:TextBox ID="UniformPhysIDTextBox" runat="server" Width="250px"></asp:TextBox>
                                              </td>
                                              <td align="right">
                                                  Sure Script ID:
                                              </td>
                                              <td align="left">
                                                  <asp:TextBox ID="SureScriptIDTextBox" runat="server" Width="250px"></asp:TextBox>
                                              </td>
                                          </tr>--%>
                                      </table>
                                  </asp:Panel>
                          </td>
                         </tr>

                        <tr>
                            <td>
                                <asp:Panel ID="pnlTCl" runat="server" Visible="true">
                                 <tr>
                                   <td>
                                    <table  id="tbltcl" border="3px" width="100%">
                                      <tr>
                                            <td align="left" class="tableRowSubHeader" colspan="4">
                                                TCL / User Type
                                            </td>
                                        </tr>
                                      <tr>
                                        <td align="right">
                                                <asp:Label ID="LblTCL" runat="server" Text="TCL/ Invision User Type:" />
                                        </td>
                                        <td colspan="3" align="left">
                
                                                <asp:TextBox ID="TCLTextBox" runat="server" Visible="false"  Enabled="false" Width="110px"></asp:TextBox>
                                                <asp:TextBox ID="UserTypeCdTextBox" runat="server" Visible="false" Enabled="false" Width="110px"></asp:TextBox>

                                                <asp:TextBox ID="UserTypeDescTextBox" runat="server" Visible="false" Enabled="false" Width="150px"></asp:TextBox>
        
                                        </td>
                                    </tr>
                                      <tr>
                                        <td align="center" colspan="4">


                                            <div>
                                            <asp:GridView ID="gvTCL" runat="server" AllowSorting="False"
                                                    AutoGenerateColumns="False" BackColor="White" BorderColor="#336666"
                                                    BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                                                    EmptyDataText="No Docmaster # Found" EnableTheming="False" 
                                                    DataKeyNames="TCL,UserTypeCd,UserTypeDesc" Visible="false"
                                                    Font-Size="Small" GridLines="Horizontal" Width="100%">
                                   
                                                    <FooterStyle BackColor="White" ForeColor="#333333" />
                                                    <HeaderStyle BackColor="#336666" Font-Bold="False" ForeColor="White" Font-Size="Small" HorizontalAlign="Left"    />                                    
                                                    <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Left" Font-Size="Medium"/>
                                                    <RowStyle BackColor="White" ForeColor="#333333" />


                                                    <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                                    <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                                    <SortedAscendingHeaderStyle BackColor="#487575" />
                                                    <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                                    <SortedDescendingHeaderStyle BackColor="#275353" />

                                                    <Columns>
                                                            <asp:CommandField ButtonType="Button" SelectText="Select" ShowSelectButton= "true" HeaderStyle-HorizontalAlign = "Left"  ControlStyle-BackColor="#84A3A3" ItemStyle-Height="30px" />
                                                            <asp:BoundField  DataField="TCL"  HeaderText="TCL"  HeaderStyle-HorizontalAlign = "Left" ItemStyle-Height="35px"></asp:BoundField>
                                                            <asp:BoundField  DataField="UserTypeCd"  HeaderText="User Type"  HeaderStyle-HorizontalAlign = "Left" ItemStyle-Height="35px"></asp:BoundField>
                                                            <asp:BoundField  DataField="UserTypeDesc"  HeaderText="User Type Desc."  HeaderStyle-HorizontalAlign = "Left" ItemStyle-Height="35px"></asp:BoundField>
				                                                                  
                                  
                                                    </Columns>
                                                    </asp:GridView>
                                                </div>


                                        </td>

                      
                                    </tr>

                                    </table>             
                                  </td>
                                 </tr>
                               </asp:Panel>                            
                            
                            </td>
                        </tr
                          <tr>
                              <td>
                                  <table id="tbldemo" border="1px" width="100%">
                                      <tr>
                                          <td align="left" class="tableRowSubHeader" colspan="4">
                                              Demographics
                                          </td>
                                      </tr>
                                      <tr>
                                          <td colspan="4">
                                              <table class="style3">
                                                  <tr>
                                                      <td>
                                                          First Name:
                                                      </td>
                                                      <td align="left">
                                                          <asp:TextBox ID="first_nameTextBox" runat="server" width="280px"></asp:TextBox>
                                                      </td>
                                                      <td>
                                                          MI:
                                                      </td>
                                                      <td>
                                                          <asp:TextBox ID="middle_initialTextBox" runat="server" Width="15px"></asp:TextBox>
                                                      </td>
                                                      <td align="left">
                                                          Last Name:
                                                      </td>
                                                      <td align="left">
                                                          <asp:TextBox ID="last_nameTextBox" runat="server" width="280px"></asp:TextBox>
                                                      </td>
                                                      <td>
                                                          Suffix:
                                                      </td>
                                                      <td>
                                                          <asp:DropDownList ID="suffixddl" runat="server" Height="20px" Width="52px">
                                                              <asp:ListItem Value="Sr.">Sr.</asp:ListItem>
                                                              <asp:ListItem Value="Jr.">Jr.</asp:ListItem>
                                                              <asp:ListItem Value="II">II</asp:ListItem>
                                                              <asp:ListItem Value="III">III</asp:ListItem>
                                                              <asp:ListItem Value="IV">IV</asp:ListItem>
                                                          </asp:DropDownList>
                                                      </td>
                                                  </tr>
                                              </table>
                                          </td>
                                      </tr>
                                      <tr>
                                          <td align="right" width="20%">
                                              <asp:Label ID="lblpos" runat="server" Text="Position Description:" />
                                          </td>
                                          <td align="left" colspan="3">
                                              <asp:TextBox ID="user_position_descTextBox" runat="server" Width="500px"></asp:TextBox>
                                          </td>
                                      </tr>
                                      <tr>
                                        <td>
                                            <asp:Label ID="netlbl" runat="server" Text="Network Logon:" >
                                            </asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID = "loginnameTextBox" runat="server"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:Label id="behaviorallbl" runat="server" Text="BH Master#" Visible="false">
                                            </asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label id="DoctorMasterNumLabel" runat="server" Visible="false">
                                            </asp:Label>

                                        </td>
                                    </tr>

                                      <tr>
                                          <td align="right">
                                              Entity:
                                          </td>
                                          <td align="left">
                                              <asp:DropDownList ID="entity_cdDDL" runat="server" AutoPostBack="True">
                                              </asp:DropDownList>
                                          </td>
                                          <td align="right">
                                              <asp:Label ID="lbldept" runat="server" Text="Department Name:" />
                                          </td>
                                          <td align="left">
                                              <asp:DropDownList ID="department_cdddl" runat="server" Width="250px" AutoPostBack="True">
                                              </asp:DropDownList>
                                          </td>
                                      </tr>
                                      <tr>
                                          <td align="right">
                                              Address 1:
                                          </td>
                                          <td align="left">
                                              <asp:TextBox ID="address_1textbox" runat="server" Width="250px"></asp:TextBox>
                                          </td>
                                          <td align="right">
                                              Address 2:
                                          </td>
                                          <td align="left">
                                              <asp:TextBox ID="address_2textbox" runat="server" Width="250px"></asp:TextBox>
                                          </td>
                                      </tr>
                                      <tr>
                                          <td align="right">
                                              <asp:Label ID="lblcity" runat="server" text="City:" width="50px" />
                                          </td>
                                          <td align="left" colspan="3">
                                              <asp:TextBox ID="citytextbox" runat="server" width="500px"></asp:TextBox>
                                          </td>
                                      </tr>
                                      <tr>
                                          <td align="right">
                                              <asp:Label ID="lblstate" runat="server" Text="State:" Width="50px" />
                                          </td>
                                          <td align="left">
                                              <asp:TextBox ID="statetextbox" runat="server" Width="50px"></asp:TextBox>
                                          </td>
                                          <td align="right">
                                              <asp:Label ID="lblzip" runat="server" Text="Zip:" Width="25px" />
                                          </td>
                                          <td align="left">
                                              <asp:TextBox ID="ziptextbox" runat="server" Width="50px"></asp:TextBox>
                                          </td>
                                      </tr>
                                      <tr>
                                          <td align="right">
                                              Phone Number:
                                          </td>
                                          <td align="left">
                                              <asp:TextBox ID="phoneTextBox" runat="server"></asp:TextBox>
                                          </td>
                                          <td align="right">
                                              Pager Number:
                                          </td>
                                          <td align="left">
                                              <asp:TextBox ID="pagerTextBox" runat="server"></asp:TextBox>
                                          </td>
                                      </tr>
                                      <tr>
                                          <td align="right">
                                              Fax:
                                          </td>
                                          <td align="left">
                                              <asp:TextBox ID="faxtextbox" runat="server"></asp:TextBox>
                                          </td>
                                          <td align="right">
                                              Email:
                                          </td>
                                          <td align="left">
                                              <asp:TextBox ID="e_mailTextBox" runat="server" Width="250px"></asp:TextBox>
                                          </td>
                                      </tr>
                                      <tr>
                                          <td align="right">
                                              Location:
                                          </td>
                                          <td align="left">
                                              <asp:DropDownList ID="facility_cdddl" runat="server" AutoPostBack="True">
                                              </asp:DropDownList>
                                          </td>
                                          <td align="right">
                                              Building:
                                          </td>
                                          <td align="left">
                                              <asp:DropDownList ID="building_cdddl" runat="server" CssClass="style3">
                                              </asp:DropDownList>
                                          </td>
                                      </tr>
                                      <tr>
                                             <asp:Panel ID="PhysOfficepanel" runat="server" Visible="false">
                                              <div>
                                                    <td align="right" style="width: 220px">
                                                        <asp:Label ID ="Label1" runat="server" Text="Physicians Office:"  >
                                                        </asp:Label>

                                                    </td>
                                                    <td colspan="3" align="left">
                                                        <asp:TextBox ID = "group_nameTextBox" runat="server"  Width="90%">
                                                            </asp:TextBox>
                                                    </td>
                                                </div>
                                              </asp:Panel>
                                        </tr>
                                      <tr>
                                          <td align="right">
                                              Vendor Name:
                                          </td>
                                          <td align="left" colspan="3">
                                              <asp:TextBox ID="VendorNameTextBox" runat="server" Width="500px"></asp:TextBox>
                                          </td>
                                      </tr>
                                      <tr>
                                          <td align="right">
                                              Start Date:
                                          </td>
                                          <td align="left">
                                              <asp:TextBox ID="start_dateTextBox" runat="server" CssClass="calenderClass"></asp:TextBox>
                                          </td>
                                          <td align="right">
                                              End Date:
                                          </td>
                                          <td align="left">
                                              <asp:TextBox ID="end_dateTextBox" runat="server" CssClass="calenderClass"></asp:TextBox>
                                          </td>
                                      </tr>
<%--                                      <tr>
                                          <td align="right">
                                              Share Drive:
                                          </td>
                                          <td align="left">
                                              <asp:TextBox ID="share_driveTextBox" runat="server" Width="250px"></asp:TextBox>
                                          </td>
                                          <td align="right">
                                              Model After:
                                          </td>
                                          <td align="left">
                                              <asp:TextBox ID="ModelAfterTextBox" runat="server" Width="250px"></asp:TextBox>
                                          </td>
                                      </tr>--%>

                                      <tr>
                                          <td align="right">
                                              Request Close Date:
                                          </td>
                                          <td align="left">
                                              <asp:TextBox ID="close_dateTextBox" runat="server"></asp:TextBox>
                                          </td>
                                          <td>
                                              <asp:Label ID="department_cdLabel" runat="server" Visible="False"></asp:Label>
                                          </td>
                                          <td>
                                              <asp:TextBox ID="building_cdTextBox" runat="server" Visible="False"></asp:TextBox>
                                          </td>
                                      </tr>
                                      <tr>
                                          <td align="left" colspan="4">
                                              Comments:
                                          </td>
                                      </tr>
                                      <tr>
                                          <td align="left" colspan="4">
                                              <asp:TextBox ID="request_descTextBox" runat="server" TextMode="MultiLine" 
                                                  Width="100%"></asp:TextBox>
                                          </td>
                                      </tr>
                                       <tr>
                                         <td align="center" colspan="4">


   	                                      <%-- =================== RSA Token Address ===============================--%>

                                          <asp:Panel ID="pnlTokenAddress" runat="server"  Visible="false">

                                             <table id="Tokentbl"  border="3px" width="100%">
                                              <tr>
                                                 <td align="left" class="tableRowSubHeader" colspan="4">
                                                       Remote Access
                                                  </td>
                                                </tr>

                                                <tr>
                                                    <td colspan="4" align="center">
                                                        <asp:Label id="lblTokenError" runat="server" BackColor="Red" Visible="false" Font-Size="Large" Font-Bold="True"></asp:Label>
                                                    </td>
                                                </tr>
                                
                                                <tr>

<%--                                                    <td align="right">
                                                        Provider:
                                                    </td>
                                                    <td align="left">
                                                        <asp:DropDownList ID="CellPhoneddl" runat="server" Width="250px" AutoPostBack="true">
                                
                                                        </asp:DropDownList>
                                                        <asp:TextBox ID="otherCellprov" runat="server" Visible="false"  MaxLength="25" ></asp:TextBox>
                                                    </td>--%>
                                                    <td align="right">
                                                        Number:
                                                    </td>
                                                    <td align="left">
                                                        <asp:TextBox ID ="TokenPhoneTextBox" runat="server"  Enabled="false" Width="250px"
                                                             ></asp:TextBox>
                                                    </td>
                                                    <td colspan="2">

                                                    </td>

                                                </tr>

                                            <%--
                                                   <tr>
                                                    <td align="right">
                                                        Comments:
                                                    </td>
                                                    <td align="left" colspan="3">
                                                        </asp:TextBox>

                                                    </td>

                
                                                </tr>--%>

                                                <tr align="center">
                                                    <td align="left" colspan="4" >

                                                        <asp:TextBox ID="CellProvidertxt" runat="server" Visible="false"></asp:TextBox>
                                                        <asp:TextBox ID ="TokenNameTextBox" runat="server"  Width="80%" Visible="false"></asp:TextBox>
                                                        <asp:TextBox ID ="TokenAddress1TextBox" Width="80%" runat="server" Visible="false"></asp:TextBox>
                                                        <asp:TextBox ID ="TokenAddress2TextBox" runat="server" Width="80%" Visible="false"></asp:TextBox>
                                                          <asp:TextBox ID ="TokencommentsTextBox" runat="server"  Width="90%" TextMode="MultiLine" Visible="false"></asp:TextBox>

                                                        <asp:TextBox ID ="TokenStTextBox" runat="server" Visible="false"></asp:TextBox>
                                                      <asp:TextBox ID ="TokenZipTextBox" runat="server" Visible="false"></asp:TextBox>
                                                        <asp:TextBox ID ="TokenCityTextBox" runat="server" Width="80%" Visible="false"></asp:TextBox>

                                                    </td>
                                                </tr>
            
                                              </table>
            			

                                             </asp:Panel>


                                       </td>
                                     </tr
                                      <tr>
                                          <td colspan="4">
                                              <asp:Label ID ="siemens_emp_numLabel" runat="server" Visible="False"></asp:Label>
                                              <asp:Label ID="HDemployeetype" runat="server" Visible="false"></asp:Label>
                                              <asp:Label ID="emp_type_cdLabel" runat="server" Visible="false"></asp:Label>
                                              <asp:Label ID="TitleLabel" runat="server" Visible="False"></asp:Label>
                                              <asp:Label ID="VendorNumLabel" runat="server" Visible="False"></asp:Label>
                                              <asp:Label ID="suffixlabel" runat="server" Visible="False"></asp:Label>
                                              <asp:Label ID = "facility_cdLabel" runat="server" Visible ="False"></asp:Label>
                                               <asp:Label ID = "entity_cdLabel" runat="server" Visible ="False"></asp:Label>
                                              <asp:Label ID = "building_cdLabel" runat="server" Visible ="False"></asp:Label>
                                              <asp:Label ID= "building_nameLabel" runat="server" Visible="False" ></asp:Label>

                                              <asp:Label ID="GNumberGroupNumLabel"  runat="server" visible="false"></asp:Label>
                                               <asp:Label ID="LocationOfCareIDLabel"  runat="server" visible="false"></asp:Label>

                                               <asp:Label ID="Receiver_eMailLabel" runat="server" Visible="false"></asp:Label>
                                           <asp:Label ID="CommLocationOfCareIDLabel" runat="server" visible="false"></asp:Label>
                                            <asp:Label ID="CommNumberGroupNumLabel" runat="server" visible="false"></asp:Label>

                                             <asp:Label ID ="CoverageGroupNumLabel"  runat="server" visible="false"></asp:Label>
                                               <asp:Label id="NonPerferedGroupnumLabel" runat="server" Visible="false"></asp:Label>
                                               <asp:Label ID="CPMModelLabel" runat="server" Visible="false"></asp:Label>
                                               <asp:Label ID="CPMTrainCdlabel" runat="server" Visible="false"></asp:Label>

                                          </td>
                                      </tr>
                                  </table>
                              </td>
                          </tr>
                      </table>
                            
                        </asp:Panel>                       
                           
                            </td>
                          
                          </tr>
                      </table>
                  </ContentTemplate>
              </ajaxToolkit:TabPanel>
<%--              <ajaxToolkit:TabPanel ID="tpDemographics" runat="server" BackColor="Gainsboro" 
                  TabIndex="0" Visible="True">
                  <HeaderTemplate>
                      <asp:Label ID="Label1" runat="server" Font-Bold="true" ForeColor="Black" 
                          Text="Request"></asp:Label>
                  </HeaderTemplate>
                  <ContentTemplate>
                     
                  </ContentTemplate>
              </ajaxToolkit:TabPanel>--%>

              <ajaxToolkit:TabPanel ID="tpRequestItems" runat="server" TabIndex="0" Visible="True" BorderStyle="Solid" BorderWidth="1px">
                  <HeaderTemplate>
                      <asp:Label ID="Label6" runat="server" Font-Bold="true" ForeColor="Black" 
                          Text="All Request Items"></asp:Label>
                  </HeaderTemplate>
                  <ContentTemplate>
                      <table border="0" width="100%">
                            <tr>
                                <td colspan="4" align="center">
                                    <asp:Button ID="btnRequest2" runat="server" Text="Go To Request" 
                                    CssClass="btnhov" BackColor="#006666"  Width="250px" />
                                </td>
                            </tr>
                          <tr>
                              <td colspan="4">
                                  <asp:Table ID="tblRequestItems" runat="server">
                                  </asp:Table>
                              </td>
                          </tr>
                      </table>
                  </ContentTemplate>
              </ajaxToolkit:TabPanel>

              <ajaxToolkit:TabPanel ID="TPActionItems" runat="server" TabIndex="0" Visible="True" BorderStyle="Solid" BorderWidth="1px">
                  <HeaderTemplate>
                      <asp:Label ID="Label7" runat="server" Font-Bold="true" ForeColor="Black" 
                          Text="Action Items"></asp:Label>
                  </HeaderTemplate>
                  <ContentTemplate>
                      <table border="0" width="100%">
                          <tr>
                              <td colspan="4">
                                  <asp:Table ID="tblActionItems" runat="server">
                                  </asp:Table>
                              </td>
                          </tr>
                      </table>
                  </ContentTemplate>
              </ajaxToolkit:TabPanel>

                <ajaxToolkit:TabPanel ID="tpCurrentAccounts" runat="server" TabIndex="0" BorderStyle="Solid" BorderWidth="1px" >
                <HeaderTemplate > 
                    <asp:Label ID="lblcurr" runat="server" Text="Current Accounts" Font-Bold="true" ForeColor="Black" ></asp:Label>
                    
                </HeaderTemplate>
                <ContentTemplate>
                    <table border="0" width = "100%">
                    <tr>
                        <td colspan = "4">
                                <asp:Table ID = "tblCurrentAccounts" runat = "server">
                                </asp:Table>
            
                        </td>
        
                    </tr>
                    </table>
                </ContentTemplate>
            </ajaxToolkit:TabPanel>

          </ajaxToolkit:TabContainer>
      </td>
 </table>


</ContentTemplate>
</asp:UpdatePanel>


</asp:Content>

