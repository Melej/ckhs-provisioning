﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="false" CodeFile="KeywordSearch.aspx.vb" Inherits="KeywordSearch"  ViewStateMode="Enabled"%>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

  <asp:UpdatePanel runat="server" ID ="uplSearch">
  <Triggers>
    
    <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />

  </Triggers>
  <ContentTemplate>
  <div>
  
  Search for Keywords: <asp:TextBox runat="server" id="txtSearch"></asp:TextBox> <asp:Button runat="server" ID="btnSearch" Text="Search" />
  </div>

  <div class="scrollContentContainer">
    <asp:Panel runat="server" ID="pnlSearch">
    
    </asp:Panel>
  
  </div>



  </ContentTemplate>
  
  </asp:UpdatePanel>
</asp:Content>

  
