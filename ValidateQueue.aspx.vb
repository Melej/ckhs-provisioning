﻿Imports System.IO
Imports System.Data '
Imports System.Data.SqlClient
Imports App_Code
Imports AjaxControlToolkit
Imports C1.Web.UI.Controls.C1Window
Partial Class ValidateQueue
    Inherits MyBasePage '   Inherits System.Web.UI.Page
    Dim sActivitytype As String = ""
    Dim cblBinder As New CheckBoxListBinder
    Dim FormSignOn As New FormSQL()
    Dim AccountRequestNum As Integer
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load



        If Not IsPostBack Then


            'bindValidateQueue()


        End If

        fillValidateTable()

    End Sub

    Protected Sub fillValidateTable()
		Dim thisData As New CommandsSqlAndOleDb("SelValidateQueueV9", Session("EmployeeConn"))
		Dim iRowCount = 1

        Dim dt As DataTable
        dt = thisData.GetSqlDataTable

        Dim rwHeader As New TableRow
        Dim clSelectHeader As New TableCell
        Dim clNameHeader As New TableCell
        Dim clRequestTypeHeader As New TableCell
        Dim clRequestorHeader As New TableCell
        Dim clReqPhoneHeader As New TableCell
        Dim clAffiliationheader As New TableCell
        Dim clSubmitDateHeader As New TableCell



        clNameHeader.Text = "Name"
        clRequestTypeHeader.Text = "Request Type"
        clRequestorHeader.Text = "Requestor"
        clReqPhoneHeader.Text = "Req Phone"
        clAffiliationheader.Text = "Aff."
        clSubmitDateHeader.Text = "Submit Date"

        rwHeader.Cells.Add(clSelectHeader)
        rwHeader.Cells.Add(clNameHeader)
        rwHeader.Cells.Add(clRequestTypeHeader)
        rwHeader.Cells.Add(clRequestorHeader)
        rwHeader.Cells.Add(clAffiliationheader)
        rwHeader.Cells.Add(clReqPhoneHeader)
        rwHeader.Cells.Add(clSubmitDateHeader)

        rwHeader.Font.Bold = True
        rwHeader.BackColor = Color.FromName("#006666")
        rwHeader.ForeColor = Color.FromName("White")
        rwHeader.Height = Unit.Pixel(36)

        tblValidateQueue.Rows.Add(rwHeader)

        For Each drRow As DataRow In dt.Rows

            Dim btnSelect As New Button
            Dim rwData As New TableRow
            Dim clSelectData As New TableCell
            Dim clNameData As New TableCell
            Dim clRequestTypeData As New TableCell
            Dim clRequestorData As New TableCell
            Dim clAffilData As New TableCell
            Dim clReqPhoneData As New TableCell
            Dim clSubmitDateData As New TableCell

            Dim sFirstName As String = IIf(IsDBNull(drRow("first_name")), "", drRow("first_name"))
            Dim sLastName As String = IIf(IsDBNull(drRow("last_name")), "", drRow("last_name"))
            Dim sAccountType As String = IIf(IsDBNull(drRow("request_desc")), "", drRow("request_desc"))
            Dim SubmitDate As String = IIf(IsDBNull(drRow("entered_date")), "", drRow("entered_date"))
            Dim Affiliation As String = IIf(IsDBNull(drRow("emp_type_cd")), "", drRow("emp_type_cd"))

            btnSelect.Text = "Select"
            btnSelect.CommandName = "Select"
            btnSelect.CommandArgument = drRow("account_request_num") & ";" & sFirstName & ";" & sLastName & ";" & drRow("account_request_type")
            ' btnSelect.Width = 50
            btnSelect.EnableTheming = False

            AddHandler btnSelect.Click, AddressOf btnSelect_Click

            clSelectData.Controls.Add(btnSelect)
            clNameData.Text = IIf(IsDBNull(drRow("receiver_full_name")), "", drRow("receiver_full_name"))
            clRequestTypeData.Text = sAccountType

            clRequestorData.Text = IIf(IsDBNull(drRow("requestor_name")), "", drRow("requestor_name"))
            clAffilData.Text = Affiliation
            clReqPhoneData.Text = IIf(IsDBNull(drRow("requestor_phone")), "", drRow("requestor_phone"))
            clSubmitDateData.Text = SubmitDate

            rwData.Cells.Add(clSelectData)
            rwData.Cells.Add(clNameData)
            rwData.Cells.Add(clRequestTypeData)
            rwData.Cells.Add(clRequestorData)
            rwData.Cells.Add(clAffilData)
            rwData.Cells.Add(clReqPhoneData)
            rwData.Cells.Add(clSubmitDateData)

            If iRowCount > 0 Then

                rwData.BackColor = Color.Bisque

            Else

                rwData.BackColor = Color.LightGray

            End If

            tblValidateQueue.Rows.Add(rwData)

            iRowCount = iRowCount * -1

        Next



    End Sub

    Protected Sub fillSimilarAccountsTable(ByVal dtSimilarAccounts As DataTable)


        Dim iRowCount = 1



        Dim rwHeader As New TableRow
        Dim clNameHeader As New TableCell
        Dim clAccountNameHeader As New TableCell
        Dim clReqPhoneHeader As New TableCell
        Dim clDepartmentHeader As New TableCell


        clNameHeader.Text = "Name"
        clAccountNameHeader.Text = "Account Name"
        clReqPhoneHeader.Text = "Req Phone"
        clDepartmentHeader.Text = "Department"


        rwHeader.Cells.Add(clNameHeader)
        rwHeader.Cells.Add(clAccountNameHeader)
        rwHeader.Cells.Add(clReqPhoneHeader)
        rwHeader.Cells.Add(clDepartmentHeader)

        rwHeader.Font.Bold = True
        rwHeader.BackColor = Color.FromName("#006666")
        rwHeader.ForeColor = Color.FromName("White")
        rwHeader.Height = Unit.Pixel(36)

        tblSimilarAccounts.Rows.Add(rwHeader)

        For Each drRow As DataRow In dtSimilarAccounts.Rows


            Dim rwData As New TableRow
            Dim clNameData As New TableCell
            Dim clAccountNameData As New TableCell
            Dim clReqPhoneData As New TableCell
            Dim clDepartmentData As New TableCell


            Dim sFirstName As String = IIf(IsDBNull(drRow("first_name")), "", drRow("first_name"))
            Dim sLastName As String = IIf(IsDBNull(drRow("last_name")), "", drRow("last_name"))


            clNameData.Text = IIf(IsDBNull(drRow("full_name")), "", drRow("full_name"))
            clAccountNameData.Text = IIf(IsDBNull(drRow("login_name")), "", drRow("login_name"))
            clReqPhoneData.Text = IIf(IsDBNull(drRow("phone")), "", drRow("phone"))
            clDepartmentData.Text = IIf(IsDBNull(drRow("department_name")), "", drRow("department_name"))


            rwData.Cells.Add(clNameData)
            rwData.Cells.Add(clAccountNameData)
            rwData.Cells.Add(clReqPhoneData)
            rwData.Cells.Add(clDepartmentData)


            If iRowCount > 0 Then

                rwData.BackColor = Color.Bisque

            Else

                rwData.BackColor = Color.LightGray

            End If

            tblSimilarAccounts.Rows.Add(rwData)

            iRowCount = iRowCount * -1

        Next

        tblSimilarAccounts.Width = New Unit("100%")


    End Sub

    Sub btnSelect_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim btnLinkView As Button = sender

        If btnLinkView.CommandName = "Select" Then

            Dim sArguments As String() = Split(btnLinkView.CommandArgument, ";")
            Session("AccountRequestNum") = sArguments(0)
            Dim FirstName As String = sArguments(1)
            Dim LastName As String = sArguments(2)
            Dim AccountType As String = sArguments(3)

			If AccountType = "addacct" Or AccountType = "addnew" Or AccountType = "rehire" Or AccountType = "addpmhact" Then
				Response.Redirect("./ValidateRequest.aspx?RequestNum=" & Session("AccountRequestNum"))

			ElseIf AccountType = "delacct" Or AccountType = "terminate" Then
				Response.Redirect("./ValidateDeleteRequest.aspx?RequestNum=" & Session("AccountRequestNum"))

			Else

				Dim thisData As New CommandsSqlAndOleDb("sel_advanced_search", Session("EmployeeConn"))

                thisData.AddSqlProcParameter("@first_name", FirstName, SqlDbType.NVarChar, 20)
                thisData.AddSqlProcParameter("@last_name", LastName, SqlDbType.NVarChar, 20)

                Dim dt As DataTable

                dt = thisData.GetSqlDataTable

                fillSimilarAccountsTable(dt)

                lblAccountExist.Text = "Does This Client Already Exist? " & FirstName & " " & LastName

                btnContinue.Visible = True
                btnInvalid.Visible = True


                lblHeader.Text = "Current Clients With Similar Names"


            End If

        End If



    End Sub


    'Protected Sub bindValidateQueue()


    '    Dim thisData As New CommandsSqlAndOleDb("SelValidateQueue", EmployeeConn)

    '    Dim dt As DataTable

    '    dt = thisData.GetSqlDataTable

    '    dgValidateQueue.DataSource = dt
    '    dgValidateQueue.DataBind()


    'End Sub

    'Private Sub dgValidateQueue_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgValidateQueue.RowCommand


    '    If e.CommandName = "Select" Then
    '        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
    '        Dim ReceiverClientNum As String = dgValidateQueue.DataKeys(Convert.ToInt32(e.CommandArgument))("receiver_client_num").ToString()
    '        Dim FirstName As String = dgValidateQueue.DataKeys(Convert.ToInt32(e.CommandArgument))("first_name").ToString()
    '        Dim LastName As String = dgValidateQueue.DataKeys(Convert.ToInt32(e.CommandArgument))("last_name").ToString()

    '        Session("AccountRequestNum") = dgValidateQueue.DataKeys(Convert.ToInt32(e.CommandArgument))("account_request_num").ToString()
    '        AccountRequestNum = dgValidateQueue.DataKeys(Convert.ToInt32(e.CommandArgument))("account_request_num").ToString()


    '        Select Case dgValidateQueue.Rows(index).Cells(5).Text
    '            Case "addnew"
    '                Dim thisData As New CommandsSqlAndOleDb("sel_advanced_search", EmployeeConn)
    '                thisData.AddSqlProcParameter("@first_name", FirstName, SqlDbType.NVarChar, 20)
    '                thisData.AddSqlProcParameter("@last_name", LastName, SqlDbType.NVarChar, 20)

    '                Dim dt As DataTable

    '                dt = thisData.GetSqlDataTable

    '                dgSimilarAccounts.DataSource = dt
    '                dgSimilarAccounts.DataBind()
    '                dgSimilarAccounts.Visible = True
    '                dgExistingAccounts.Visible = False

    '                'DataGrid3.Visible = False
    '                lblAccountExist.Text = "Does This Client Already Exist? " & FirstName & " " & LastName

    '                btnContinue.Visible = True
    '                btnInvalid.Visible = True


    '                lblHeader.Text = "Current Clients With Similar Names"
    '                'Case "addacct"


    '                '    Dim thisData As New CommandsSqlAndOleDb("sel_all_client_acct_by_number_type", EmployeeConn)
    '                '    thisData.AddSqlProcParameter("@client_num", ReceiverClientNum, SqlDbType.NVarChar, 20)
    '                '    thisData.AddSqlProcParameter("@account_type_cd", AccountTypeCd, SqlDbType.NVarChar, 20)

    '                '    Dim dt As DataTable

    '                '    dt = thisData.GetSqlDataTable

    '                '    dgExistingAccounts.DataSource = dt
    '                '    dgExistingAccounts.DataBind()
    '                '    dgExistingAccounts.Visible = True
    '                '    dgSimilarAccounts.Visible = False



    '                '    lblAccountExist.Text = "Does this client already have an account on this system for this type  " & AccountTypeCd & " ?"
    '                ' 

    '                '    btnContinue.Visible = True
    '                '    btnInvalid.Visible = True

    '                '    lblHeader.Text = "Current account(s) for this client."
    '                'Case "delacct"
    '                '    Response.Redirect("./ViewRequest.aspx?RequestNum=" & AccountTypeCd)
    '                'Case "terminate"
    '                '    Response.Redirect("./ViewRequest.aspx?RequestNum=" & AccountTypeCd)

    '            Case Else

    '                dgSimilarAccounts.Visible = False
    '                btnContinue.Visible = True
    '                btnInvalid.Visible = True

    '        End Select

    '    End If


    'End Sub

    'Public Sub dgValidateQueue_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
    '    dgValidateQueue.PageIndex = e.NewPageIndex
    '    bindValidateQueue()

    'End Sub

    Protected Sub btnContinue_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnContinue.Click
        Response.Redirect("./ValidateRequest.aspx?RequestNum=" & Session("AccountRequestNum"))
    End Sub

    Protected Sub btnInvalid_Click(sender As Object, e As System.EventArgs) Handles btnInvalid.Click
        With PanInvalid
            .Visible = True
            With .Style
                .Add("LEFT", "260px")
                .Add("TOP", "190px")
                .Add("POSITION", "absolute")
                .Add("Z-INDEX", "999")
            End With
        End With


        FillInvalidCodes()


    End Sub
    Private Sub FillInvalidCodes()
        'SelInvalidCodes
        Dim dt As DataTable
        Dim thisData As New CommandsSqlAndOleDb
        thisData = New CommandsSqlAndOleDb("SelInvalidCodes", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@invalidType", "create", SqlDbType.NVarChar, 12)


        dt = thisData.GetSqlDataTable()
        Invalidddl.DataSource = dt
        Invalidddl.DataValueField = "Invalidcd"
        Invalidddl.DataTextField = "InvalidDesc"
        Invalidddl.DataBind()
        thisData = Nothing

    End Sub
    Protected Sub btnSubmitInvalid_Click(sender As Object, e As System.EventArgs) Handles btnSubmitInvalid.Click
        Dim thisData As New CommandsSqlAndOleDb("UpdValidateRequestV4", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int)
        thisData.AddSqlProcParameter("@nt_login", Session("LoginID"), SqlDbType.NVarChar, 50)
        thisData.AddSqlProcParameter("@validate", "invalid", SqlDbType.NVarChar, 10)
        thisData.AddSqlProcParameter("@InvalidDesc", Invalidddl.DataTextField, SqlDbType.NVarChar, 75)

        thisData.AddSqlProcParameter("@action_desc", InvalidDescTextBox.Text, SqlDbType.NVarChar, 255)


        thisData.ExecNonQueryNoReturn()

        Response.Redirect("~\MyAccountQueue.aspx")

    End Sub

    Protected Sub btnCancelInvalid_Click(sender As Object, e As System.EventArgs) Handles btnCancelInvalid.Click
        PanInvalid.Visible = False
        Invalidddl.SelectedIndex = -1
        InvalidDescTextBox.Text = Nothing

    End Sub
End Class
