﻿Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports App_Code

Partial Class ChangeLogon
    Inherits System.Web.UI.Page
    'Dim UserInfo As UserSecurity
    Dim conn As New SetDBConnection()

    Protected Sub btnSubmit_Click(sender As Object, e As System.EventArgs) Handles btnSubmit.Click
        If ChangeLogonTextBox.Text = "" Then
            messagelbl.Text = "Must supply a logon"
            Exit Sub
        End If
        Dim UserInfo As New UserSecurity(ChangeLogonTextBox.Text, Session("EmployeeConn"))

        Session("loginid") = ""
        Session("technum") = ""
        Session("UserName") = ""
        Session("UserType") = ""
        Session("SecurityLevelNum") = ""
        Session("ClientNum") = ""
        Session("TechClientNum") = ""

        Session("loginid") = UserInfo.NtLogin
        Session("ClientNum") = UserInfo.ClientNum
        Session("TechClientNum") = UserInfo.TechNum
        Session("technum") = UserInfo.TechNum
        Session("UserName") = UserInfo.UserName
        Session("SecurityLevelNum") = UserInfo.SecurityLevelNum
        Session("Usertype") = UserInfo.UserType

        GlobalUserAccount = Session("loginid")
        newidlbl.Text = "Name: " & Session("UserName") & "Login: " & Session("loginid") & " UserType " & Session("UserType") & " Level " & Session("SecurityLevelNum") _
            & " userNum: " & Session("ClientNum") & " Environment: " & Session("environment") & " Other Tech#" & Session("TechClientNum") & " Connection: " & Session("EmployeeConn")

        Session("objUserSecurity") = UserInfo

    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        Dim accounts() As String = {"melej", "geip00", "mizem"}
        Dim FindThisString As String = Session("loginid").ToString.ToLower


        Dim result As String() = Array.FindAll(accounts, Function(s) s.Contains(FindThisString))

        If result.Length = 0 Then
            Response.Redirect("~/SimpleSearch.aspx")
        End If

        'For Each Str As String In accounts
        '    If Str.Contains(FindThisString) Then
        '    Else
        '        Response.Redirect("~/SimpleSearch.aspx")
        '    End If
        'Next

    End Sub
End Class
