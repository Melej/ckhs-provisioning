﻿Imports System.Diagnostics

Imports App_Code

'Namespace Hypnos

Partial Class Variables
    Inherits MyBasePage 'System.Web.UI.Page


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents TextBox1 As System.Web.UI.WebControls.TextBox
    Protected WithEvents TextBox2 As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblfId As System.Web.UI.WebControls.Label
    Protected WithEvents lblNum As System.Web.UI.WebControls.Label
    Protected WithEvents btnDelete As System.Web.UI.WebControls.Button
    Protected WithEvents ddlPagers As System.Web.UI.WebControls.DropDownList
    Protected WithEvents btnSelect As System.Web.UI.WebControls.Button
    Protected WithEvents btnAdd As System.Web.UI.WebControls.Button
    Protected WithEvents btnUpdate As System.Web.UI.WebControls.Button
    Protected WithEvents btnCancel As System.Web.UI.WebControls.Button
    Protected WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection
    Protected WithEvents SqlDataAdapter1 As System.Data.SqlClient.SqlDataAdapter
    Protected WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Dim imSecurityLevel As Integer
    Dim smDefaultUnit, smCurrentUnit As String
    Dim smSecurityType As String
    Private Sub btnSelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelect.Click
    End Sub
    Private Sub FillVarsTable()
        Dim sErr As String = ""
        Try

            Dim item As Object
            Dim tc As TableCell
            Dim tr As TableRow
            Dim x As Integer
            tr = New TableRow
            tc = New TableCell
            tc.Text = "Session"
            tr.Cells.Add(tc)
            tc = New TableCell
            tc.Text = " Variables"
            tr.Cells.Add(tc)
            tblVars.Rows.Add(tr)
            tr = New TableRow
            tc = New TableCell
            tc.Text = ""
            tr.Cells.Add(tc)
            tc = New TableCell
            tc.Text = ""
            tr.Cells.Add(tc)
            tblVars.Rows.Add(tr)

            '  For Each item In Session.Contents
            For y As Integer = 0 To Session.Contents.Count - 1
                tr = New TableRow
                tc = New TableCell
                tc.Text = Session.Keys(y) '.ToString 'item.ToString()
                tr.Cells.Add(tc)
                tc = New TableCell
                If Not Session.Contents(y) Is Nothing Then
                    tc.Text = Session.Contents.Item(y).ToString
                    tr.Cells.Add(tc)
                    tblVars.Rows.Add(tr)
                    x = x + 1
                Else ' emmpty session
                    tc.Text = "Nothing"
                    tr.Cells.Add(tc)
                    tblVars.Rows.Add(tr)
                End If
            Next
            tr = New TableRow
            tc = New TableCell
            tc.Text = "Application"
            tr.Cells.Add(tc)
            tc = New TableCell
            tc.Text = " Variables"
            tr.Cells.Add(tc)
            tblVars.Rows.Add(tr)
            tr = New TableRow
            tc = New TableCell
            tc.Text = ""
            tr.Cells.Add(tc)
            tc = New TableCell
            tc.Text = ""
            tr.Cells.Add(tc)
            tblVars.Rows.Add(tr)
            x = 0
            '  For Each item In Application.Contents
            For y As Integer = 0 To Application.Contents.Count - 1
                tr = New TableRow
                tc = New TableCell
                tc.Text = Application.Keys(y).ToString()
                tr.Cells.Add(tc)
                tc = New TableCell
                If Not Application.Contents(y) Is Nothing Then
                    tc.Text = Application.Contents(x).ToString
                    tr.Cells.Add(tc)
                    tblVars.Rows.Add(tr)
                    x = x + 1
                Else ' emmpty session
                    tc.Text = "Nothing"
                    tr.Cells.Add(tc)
                    tblVars.Rows.Add(tr)
                End If
            Next
            x = 0
            tr = New TableRow
            tc = New TableCell
            tc.Text = "System.IO.Path.GetFileNameWithoutExtension(Request.PhysicalPath)"
            tr.Cells.Add(tc)
            tc = New TableCell
            tc.Text = System.IO.Path.GetFileNameWithoutExtension(Request.PhysicalPath).ToString

            tr.Cells.Add(tc)
            tblVars.Rows.Add(tr)


            '  For Each item In Request.ServerVariables
            For y As Integer = 0 To Request.ServerVariables.Count - 1
                tr = New TableRow
                tc = New TableCell
                tc.Text = Request.ServerVariables.Keys(y).ToString()
                tr.Cells.Add(tc)
                tc = New TableCell
                tc.Text = Request.ServerVariables(y).ToString

                tr.Cells.Add(tc)
                tblVars.Rows.Add(tr)
                x = x + 1
            Next
        Catch ex As Exception
            sErr = ex.Message
            'Dim objCurrentInfo As ProcessInfo = ProcessModelInfo.GetCurrentProcessInfo()

            'Dim perfAppRestarts As New PerformanceCounter("ASP.NET", "Application Restarts")
            'Dim perfFreeMem As New PerformanceCounter("Memory", "Available MBytes")

            'lblRequestCount.Text = objCurrentInfo.RequestCount()
            'lblAppRestarts.Text = perfAppRestarts.NextValue()
            'lblFreeMem.Text = perfFreeMem.NextValue()

            'lblPeakMem.Text = objCurrentInfo.PeakMemoryUsed
            'lblProcID.Text = objCurrentInfo.ProcessID
        End Try


    End Sub
    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click

    End Sub
    Protected pageTitle As System.Web.UI.HtmlControls.HtmlGenericControl
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'pageTitle.InnerText = getTitle(pageTitle.InnerText)
        '============================================================================= 
        ''If Session("SecurityLevel") < 101 Then
        ''    Response.Redirect("~/Unitview.aspx", True)
        ''End If
        '  Dim sErr As String
        Dim sPage As String
        '++++++++++++++get Page name+++++++++++++++++++++++++++++++
        Dim sAbsPath As String = Request.Url.AbsolutePath()
        Dim sSplitPath As String()
        sSplitPath = Split(sAbsPath, "/")
        Dim u As Integer = UBound(sSplitPath)
        sPage = sSplitPath(u)
        '++++++++++++++End get Page name+++++++++++++++++++++++++++++++
        'Try
        '  Server.Execute("sitefunctions.aspx?FuncName=chkreconn")
        ' SqlConnection1.ConnectionString = gAppDataConn
        '==============Security cut and Paste===================================
        'Dim imSecurityLevel As Integer
        ' Dim smDefaultUnit, smCurrentUnit As String
        ' Dim smSecurityType As String

        'If Session("SecurityType") <> "" Then
        '    imSecurityLevel = Session("SecurityLevel")
        '    smDefaultUnit = Session("DefaultUnit")
        '    smSecurityType = Session("SecurityType")
        'Else ' get secuityinfo from class
        '    Dim Sec As New SecurityFunctions(Session("loginid"))
        '    imSecurityLevel = Sec.securitylevel()
        '    smSecurityType = Sec.UserType()
        '    smDefaultUnit = Sec.DefaultUnit()
        '    Session("SecurityLevel") = imSecurityLevel
        '    Session("SecurityType") = smSecurityType
        '    Session("DefaultUnit") = smDefaultUnit
        'End If
        FillVarsTable()
        'usertype           SecurityLevel
        '-------------------- ------------- 
        'Interface            0
        'General              10
        'Housekeeping         10
        'Transport            10
        'TransportSup         30
        'HouseSup             30
        'NurseStation         45
        'NurseAdmin           50 
        'HouseAdmin           50
        'TransportAdmin       50
        'PFC                  75
        'HospitalAdmin        75
        'SysAdmin             100
        'If imSecurityLevel < 1 Then
        '    Response.Redirect("~/" _
        '    & "Security.aspx?mess= Security Level to low to view this Page." & Request.Url.LocalPath())
        'End If
        '==============End Security cut and Paste===================================
        '==================Error code========================
        ' Dim sErr As String
        ' dim sPage as string
        '   Try
        'Dim sAbsPath As String = Request.Url.AbsolutePath()
        'Dim sSplitPath As String()

        'sSplitPath = Split(sAbsPath, "/")
        'Dim u As Integer = UBound(sSplitPath)
        'sPage = sSplitPath(u)
        'Catch ex As Exception
        '    Session("PageError") = "Error from Page " & sPage & " Procedure Load  Message:" & ex.Message.ToString
        '    '  Response.Redirect("~/" & "ErrorDisplay.aspx?Page=" & sAbsPath)
        'End Try
    End Sub


End Class

'End Namespace
