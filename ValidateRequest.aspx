﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="false" CodeFile="ValidateRequest.aspx.vb" Inherits="ValidateRequest" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">


<asp:UpdatePanel ID="uplViewRequest" runat ="server" >
<Triggers>
    <asp:AsyncPostBackTrigger ControlID = "btnTestCustomUserName" EventName="Click" />
</Triggers>
<ContentTemplate>
<table class="wrapper" style="border-style: groove" align="left">
  
  <tr>
   <td>
        <table border="3" style="empty-cells:show"  cellpadding ="5px">
           <tr>
                <th colspan = "4" class="tableRowHeader">
                    Validate Request
                </th>     
           </tr>
             <tr align="center">
                <td colspan="4" align="center">
                    <asp:Label ID = "lblRehire" runat = "server" ForeColor="Red" Font-Bold="true" Font-Size="Large" />
                </td>
            
            </tr>

            <tr>
                  <td colspan = "4">
                  
                        <asp:Button ID = "btnValidate" runat = "server"  Enabled= "false"  text = "Validate Request" 
                        CssClass="btnhov" BackColor="#006666"  Width="135px" />

                        <asp:Button ID = "btnCancel" runat = "server" Text = "Cancel" 
                         CssClass="btnhov" BackColor="#006666"  Width="135px" />

                          <asp:Button ID="btnGoToRequest" runat="server" Text="Go To Request " 
                          CssClass="btnhov" BackColor="#006666"  Width="135px" />
                                    
                    </td>
            
            </tr>
           <tr>
            <td style="font-weight:bold">
               Client Name
            </td>
             <td>
                <asp:Label ID = "first_namelabel" runat = "server"></asp:Label>&nbsp<asp:Label ID = "last_namelabel" runat = "server" ></asp:Label>
            </td>

          
           
            <td style="font-weight:bold" >
            Requestor Name
           
            </td><td >
                <asp:Label ID = "requestor_namelabel" runat = "server"></asp:Label>
            </td>
             


           </tr>
           <tr>

                 <td  style="font-weight:bold">
                    Request Type
                </td>     
        
                 <td colspan="3" >
                     <asp:Label ID = "RequestTypeDescLabel" runat = "server"></asp:Label>
                </td>    
                 
                 
            </tr>
            <tr>

                 <td style="font-weight:bold">
                 Start Date
                  </td>
                  <td>
                    <asp:Label ID = "start_datelabel" runat = "server"></asp:Label>
                </td>
                  

           
                <td style="font-weight:bold">
                    Submit Date
                </td>
               
                 <td>
                    <asp:Label ID = "entered_datelabel" runat = "server"></asp:Label>
                </td>
                 
            </tr>
      
            <tr>
                <td colspan = "4" class="tableRowSubHeader">
                 Information Needed
                </td>
            </tr>

            <tr>
             <td style="font-weight:bold">
               Share Drive
                </td>
                <td>
                    <asp:TextBox ID = "share_driveTextBox" runat = "server"></asp:TextBox>
                </td> 
                <td>
                Model After
                </td>
               <td>
                    <asp:TextBox ID = "ModelAfterTextBox" runat="server"></asp:TextBox>
               </td>

            </tr>
            <tr>
                <td style="font-weight:bold" colspan="2"  >
                    <asp:Label id="currLoginName" runat="server" Text="" ForeColor="Red"></asp:Label>
                </td>
                <td  align="left" style="font-weight:bold" colspan="2" >
                    <asp:Label ID="CurrentAccountLabel" runat="server"  ForeColor="Red" ></asp:Label>
					<asp:Label ID="HDCurrentADAccount" runat="server" Visible="false"></asp:Label>
					<asp:Label ID="HDEcareAccount" runat="server" Visible="false"></asp:Label>
					<asp:Label ID="HDADNotFounf" runat="server" Visible="false"></asp:Label>

                </td>
            
            </tr>
            <tr>
               <td style="font-weight:bold">
                    <asp:Label Text="Username:" runat="server" ID="lblSuggestedUserName"></asp:Label>
                </td>
   

                <td colspan="3">

                    <asp:TextBox ID ="UserNameTextBox" runat = "server" />
              
                </td>

 
            </tr>
           
           <tr>
           
            <td style="font-weight:bold">
                <asp:Label Text="Test Username" runat="server" ID="lblCustomUserName"></asp:Label>
            </td>
             <td colspan="3">

<%--                <asp:TextBox ID ="CustomUserNameTextBox" runat = "server" AutoPostBack="true" />
--%>                
                <asp:Button ID ="btnTestCustomUserName" runat="server" Text ="Test" 
                CssClass="btnhov" BackColor="#006666"  Width="145px" />

                <asp:Label ID="lblCustomNameTest" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>

<%--                <br />
               
                    <asp:CheckBox ID ="cbUseCustomName" runat="server" Text="Use Custom Name" Checked="false"  Enabled= "false" />
--%>              

                <asp:Label ID="PositionRoleNumLabel" runat="server" Visible="false"></asp:Label>
                <asp:Label ID ="login_nameLabel" runat="server" Visible="false"></asp:Label>
            </td>
           </tr>            
           
          <tr>
            <td colspan = "4">
                  <asp:GridView ID="gvAppRequests" runat="server" AllowSorting="True" 
                        AutoGenerateColumns="False" BackColor="White" BorderColor="#336666"
                        BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                        EmptyDataText="No Accounts" EnableTheming="False" DataKeyNames = "account_request_seq_num, login_name, ApplicationNum"
                        Font-Size="Medium" GridLines="Horizontal" Width="100%">
                                   
                        <FooterStyle BackColor="White" ForeColor="#333333" />
                        <HeaderStyle BackColor="#336666" Font-Bold="False" ForeColor="White" Font-Size="Small" HorizontalAlign="Left"    />                                    
                        <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Left" Font-Size="Medium"/>
                        <RowStyle BackColor="White" ForeColor="#333333" />
                        <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                        <SortedAscendingCellStyle BackColor="#F7F7F7" />
                        <SortedAscendingHeaderStyle BackColor="#487575" />
                        <SortedDescendingCellStyle BackColor="#E5E5E5" />
                        <SortedDescendingHeaderStyle BackColor="#275353" />

                        <Columns>
                            <asp:TemplateField>  

                                        <ItemTemplate>  

                                                <asp:Button ID="btnLinkView" runat="server" onclick="btnInvalid_Click" Text="Invalid"  BackColor="#336666" ForeColor="White"></asp:Button> 
                                                      
                                        </ItemTemplate>

                            </asp:TemplateField>
                                        
                            <asp:BoundField  DataField="ApplicationDesc"  HeaderText="Application" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
                                  
                        </Columns>

                    </asp:GridView>
            </td>
          
          </tr>
        </table>

         <asp:Panel ID="pnlInvalidItem" runat="server" CssClass="ModalPopUpPanel" style="display:none">
             <table id="tblInvalid" runat="server" cellspacing="1" cellpadding="10" 
                bgcolor="gainsboro" bordercolor="navy"  rules="none"  border="2" 
                    width="350Px">
                <tr>
                    <td align="center" colspan="2" style="font-weight: bold; height: 25px;">
                        Provide Invalid Reason or Comment</td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                            <asp:DropDownList ID="Invalidddl" runat="server" Width="250px">
                            </asp:DropDownList>

                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Button ID="btnSubmitInvalid" runat="server" CssClass="btnhov" 
                        BackColor="#006666"  Width="135px" Text="Submit"
                            ToolTip="This will Identify this request as Invalid "/>
                    </td>
                    <td align="center" >
                        <asp:Button ID="btnCancelInvalid" runat="server" BackColor="DimGray" 
                        CssClass="btnhov"  Width="135px" 
                            ForeColor="White"  Text="Cancel"  />
                    </td>
                </tr>
                <tr>
                    <td align="center"  colspan="2">
                            Comments:
                    </td>
                </tr>
                <tr>
                    <td  align="center"  colspan="2">
                        <asp:TextBox ID = "InvalidDescTextBox" runat="server"  Height="100px" Width="80%" />

                    </td>
        
                </tr>
                 <tr>
                    <td >
                        <asp:Label ID = "lblInvalidRequestItem" runat = "server"  Visible="false"></asp:Label>
                        <asp:Label ID = "lblHiddenSeqNum" runat = "server" visible = "false"></asp:Label>
                        <asp:Label ID = "lblHiddenLogin" runat = "server" visible = "false"></asp:Label>
                        <asp:Label ID = "receiver_client_numLabel" runat = "server" visible = "false"></asp:Label>

                    </td>
                </tr>


            </table>

        </asp:Panel>
          <asp:Button ID="btnControlHidden" runat="server" style="display:none" />  
            
             <ajaxToolkit:ModalPopupExtender ID="mpeInvalidItem" runat="server"   
                                DynamicServicePath="" Enabled="True" TargetControlID="btnControlHidden"   
                                PopupControlID="pnlInvalidItem" BackgroundCssClass="ModalBackground"   
                                DropShadow="true" CancelControlID="btnCancelInvalid">  
             </ajaxToolkit:ModalPopupExtender>
   </td>
  </tr>
 </table>
</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>

