﻿Imports System.IO
Imports System.Data '
Imports System.Data.SqlClient
Imports App_Code
Imports AjaxControlToolkit
Imports C1.Web.UI.Controls.C1Window
Imports System.Net.Mail

Partial Class AddRemoveAcctForm
    Inherits MyBasePage '   Inherits System.Web.UI.Page

    Dim sWebReferrer As String = ""
    Dim sActivitytype As String = ""
    Dim FormSignOn As New FormSQL()
    Dim iClientNum As String
    Private dtAccounts As New DataTable
    Dim UserInfo As UserSecurity
    Private AddAccountRequestNum As Integer = 0
    Private DelAccountRequestNum As Integer = 0
    Private dtPendingRequests As DataTable
    Private dtRemoveAccounts As New DataTable
    Private dtAddAccounts As New DataTable
    Dim thisdata As CommandsSqlAndOleDb
    Dim c3Controller As New Client3Controller
    Dim c3Submitter As New Client3
    Dim EmployeeType As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        iClientNum = Request.QueryString("ClientNum")

        thisdata = New CommandsSqlAndOleDb("SelApplicationCodes", Session("EmployeeConn"))
        thisdata.ExecNonQueryNoReturn()

        'BusLog = New AccountBusinessLogic
        'BusLog.Accounts = thisdata.GetSqlDataTable

        UserInfo = New UserSecurity(Session("LoginID"), Session("EmployeeConn"))

        'Dim frmSec As New FormSecurity(Session("objUserSecurity"))

        'frmSec.DisableWebControlByRole(login_nameTextBox, "System Admin,Provider Master")



        FormSignOn = New FormSQL(Session("EmployeeConn"), "SelClientByNumberV20", "UpdClientObjectByNumberV4", "", Page)
        FormSignOn.AddSelectParm("@client_num", iClientNum, SqlDbType.Int, 6)

        ' CheckIfCameFromTicket()

        If Not IsPostBack Then

            userSubmittingLabel.Text = UserInfo.UserName
            SubmittingOnBehalfOfNameLabel.Text = UserInfo.UserName
            requestor_client_numLabel.Text = UserInfo.ClientNum


            'Add_userSubmittingLabel.Text = UserInfo.UserName
            'Add_SubmittingOnBehalfOfNameLabel.Text = UserInfo.UserName
            'Add_requestor_client_numLabel.Text = UserInfo.ClientNum


            'Remove_userSubmittingLabel.Text = UserInfo.UserName
            'Remove_SubmittingOnBehalfOfNameLabel.Text = UserInfo.UserName
            'Remove_requestor_client_numLabel.Text = UserInfo.ClientNum


            Dim rblAffiliateBinder As New RadioButtonListBinder(emp_type_cdrbl, "SelAffiliationsV4", Session("EmployeeConn"))
            rblAffiliateBinder.BindData("AffiliationDescLower", "AffiliationDisplay")

            ' No longer need procedure SelFacilityEntity
            ' no longer need table [EntityAndFacilites]
            'Dim cblFacilitesBinder As New CheckBoxListBinder(cblFacilities, "SelFacilityEntity", Session("EmployeeConn"))
            'cblFacilitesBinder.AddDDLCriteria("@EntityFacility", "Facility", SqlDbType.NVarChar)
            'cblFacilitesBinder.BindData("Code", "EntityFacilityDesc")

            'Dim cblEntityBinder As New CheckBoxListBinder(cblEntities, "SelFacilityEntity", Session("EmployeeConn"))
            'cblEntityBinder.AddDDLCriteria("@EntityFacility", "Entity", SqlDbType.NVarChar)
            'cblEntityBinder.BindData("Code", "EntityFacilityDesc")

            dtRemoveAccounts.Columns.Add("ApplicationNum", GetType(Integer))
            dtRemoveAccounts.Columns.Add("ApplicationDesc", GetType(String))
            dtRemoveAccounts.Columns.Add("login_name", GetType(String))


            dtAddAccounts.Columns.Add("Applicationnum", GetType(Integer))
            dtAddAccounts.Columns.Add("ApplicationDesc", GetType(String))



            ViewState("dtRemoveAccounts") = dtRemoveAccounts
            ViewState("dtAddAccounts") = dtAddAccounts

            Dim thisdata As New CommandsSqlAndOleDb("SelAllClientAccountsByNumber", Session("EmployeeConn"))
            thisdata.AddSqlProcParameter("@client_num", iClientNum, SqlDbType.Int, 20)

            thisdata.ExecNonQueryNoReturn()

            'gvCurrentUserAccounts.DataSource = thisdata.GetSqlDataTable
            'gvCurrentUserAccounts.DataBind()


            ViewState("dtAccounts") = thisdata.GetSqlDataTable


            thisdata = New CommandsSqlAndOleDb("SelAvailableAccountsByClientNumber", Session("EmployeeConn"))
            thisdata.AddSqlProcParameter("@client_num", iClientNum, SqlDbType.Int, 20)

            thisdata.ExecNonQueryNoReturn()

            ViewState("dtAvailableAccounts") = thisdata.GetSqlDataTable


            Dim ddlBinder As New DropDownListBinder

            ddlBinder.BindData(facility_cdddl, "SelFacilityCodes", "facility_cd", "facility_short_name", Session("EmployeeConn"))
            ddlBinder.BindData(Titleddl, "SelTitles", "TitleDesc", "TitleDesc", Session("EmployeeConn"))
            ddlBinder.BindData(Specialtyddl, "SelSpecialtyCodesV2", "specialty", "specialty", Session("EmployeeConn"))
            ddlBinder.BindData(practice_numddl, "sel_practices", "practice_num", "parctice_name", Session("EmployeeConn"))
            ddlBinder.BindData(entity_cdDDL, "sel_entity_codes", "entity_cd", "hosp_desc", Session("EmployeeConn"))



            FormSignOn.FillForm()

            Dim employeetype As String = emp_type_cdLabel.Text
            emp_type_cdrbl.SelectedValue = employeetype


            CheckAffiliation()
            'fillRequestItemsTable()



            '---------------------------------------------------
            ' Logic after the form has been filled
            '====================================================

            'ddlBinder.BindData(entity_cdDDL, "sel_entity_codes", "entity_cd", "hosp_desc", EmployeeConn)
            'department_cdddl.Items.Add("Select Entity")


            ddlBinder = New DropDownListBinder(building_cdddl, "sel_building_by_facility", Session("EmployeeConn"))
            ddlBinder.AddDDLCriteria("@facility_cd", facility_cdddl.SelectedValue, SqlDbType.NVarChar)
            ddlBinder.BindData("building_cd", "building_name")


            Dim ddbDepartmentBinder As New DropDownListBinder(department_cdddl, "sel_department_codes", Session("EmployeeConn"))
            ddbDepartmentBinder.AddDDLCriteria("@entity_cd", entity_cdDDL.SelectedValue, SqlDbType.NVarChar)
            ddbDepartmentBinder.BindData("department_cd", "department_name")

            department_cdddl.SelectedValue = department_cdLabel.Text

            building_cdddl.SelectedValue = building_cdTextBox.Text


            first_nameTextBox.Focus()

            If doctor_master_numTextBox.Text <> Nothing Then
                emp_type_cdrbl.Enabled = False
            End If

            'If siemens_emp_numLabel.Text <> Nothing Then
            '    emp_type_cdrbl.Enabled = False
            'End If

            If providerrbl.Text = "Yes" Then
                ' only 90 security level and above edit these fields
                If Session("SecurityLevelNum") < 90 Then
                    Dim ctrl As Control = DataCheckingAndValidation.SetChildControlsRecursiveToEnabledFalse(tpDemographics, False)

                End If
            End If

            ' building_cdddl.SelectedValue = sBuilding
            CurrentAccounts()
            AvailableAccounts()

        End If

        building_cdTextBox.ID = "building_cdTextBoxHidden"
        fillOpenRequestTable()
        'displayPendingQueues()

    End Sub
    Protected Sub AvailableAccounts()

        grAvailableAccounts.DataSource = ViewState("dtAvailableAccounts")
        grAvailableAccounts.DataBind()


    End Sub
    Protected Sub CurrentAccounts()

        gvCurrentApps.DataSource = ViewState("dtAccounts")
        gvCurrentApps.DataBind()

        'gvRemoveAccounts.DataSource = ViewState("dtRemoveAccounts")
        'gvRemoveAccounts.DataBind()

    End Sub
    Protected Sub facility_cdddl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles facility_cdddl.SelectedIndexChanged
        Dim ddlBinder As DropDownListBinder


        ddlBinder = New DropDownListBinder(building_cdddl, "sel_building_by_facility", Session("EmployeeConn"))
        ddlBinder.AddDDLCriteria("@facility_cd", facility_cdddl.SelectedValue, SqlDbType.NVarChar)
        ddlBinder.BindData("building_cd", "building_name")



    End Sub
    Private Sub insAcctItems(ByVal requestNum As Integer, ByVal appNum As Integer, ByVal comments As String, ByVal LoginName As String)


        Dim thisdata As New CommandsSqlAndOleDb("InsAccountItemsV3", Session("EmployeeConn"))
        thisdata.AddSqlProcParameter("@account_request_num", requestNum, SqlDbType.Int, 20)
        thisdata.AddSqlProcParameter("@ApplicationNum", appNum, SqlDbType.Int, 8)
        thisdata.AddSqlProcParameter("@account_item_desc", comments, SqlDbType.NVarChar, 255)
        thisdata.AddSqlProcParameter("@validate", "NeedsValidation", SqlDbType.NVarChar, 20)
        thisdata.AddSqlProcParameter("@login_name", LoginName, SqlDbType.NVarChar, 100)

        thisdata.ExecNonQueryNoReturn()

    End Sub
    'Protected Sub btnRemoveAccts_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemoveAccts.Click
    '    Try

    '        Dim DelReqNum As Integer = AccountRequestNum("delacct")

    '        For Each rw As GridViewRow In gvRemoveAccounts.Rows
    '            If rw.RowType = DataControlRowType.DataRow Then

    '                Dim appNum As String = gvRemoveAccounts.DataKeys(rw.RowIndex)("ApplicationNum")
    '                Dim LoginName As String = gvRemoveAccounts.DataKeys(rw.RowIndex)("login_name")

    '                insAcctItems(DelReqNum, appNum, "", LoginName)




    '            End If



    '        Next

    '        Response.Redirect("~/RequestSubmited.aspx?RequestNum=" & DelReqNum)

    '    Catch ex As Exception

    '    End Try


    'End Sub
    '  ************** This is from AccountRequestInfo Same Sub *********
    'Protected Sub btnRemoveAccts_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnInvalidAccts.Click
    '    Try



    '        For Each rw As GridViewRow In gvRemoveAccounts.Rows
    '            If rw.RowType = DataControlRowType.DataRow Then

    '                Dim account_request_seq_num As String = gvRemoveAccounts.DataKeys(rw.RowIndex)("account_request_seq_num")





    '                Dim thisData As New CommandsSqlAndOleDb("UpdValidateRequestItems", Session("EmployeeConn"))
    '                thisData.AddSqlProcParameter("@account_request_seq_num", account_request_seq_num, SqlDbType.Int)
    '                thisData.AddSqlProcParameter("@account_request_num", iRequestNum, SqlDbType.Int)
    '                thisData.AddSqlProcParameter("@requestor_client_num", Session("ClientNum"), SqlDbType.NVarChar, 30)
    '                thisData.AddSqlProcParameter("@validate", "invalid", SqlDbType.NVarChar, 10)



    '                thisData.ExecNonQueryNoReturn()


    '            End If



    '        Next

    '    Catch ex As Exception

    '    End Try

    '    Page.Response.Redirect(Page.Request.Url.ToString(), False)
    'End Sub

  
    Protected Sub btnSubmitAddRequest_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmitAddRequest.Click
        Try


            'Dim AccountCode As String
            ' Dim AddReqNum As Integer = AccountRequestNum("addacct")
            'Dim blnHost As Boolean = False
            'Dim blnEnt As Boolean = False


            'For Each rw As GridViewRow In grAccountsToAdd.Rows
            '    If rw.RowType = DataControlRowType.DataRow Then

            '        Dim appNum As String = grAccountsToAdd.DataKeys(rw.RowIndex)("Applicationnum")
            '        Dim appDesc As String = grAccountsToAdd.DataKeys(rw.RowIndex)("ApplicationDesc")


            '        insAcctItems(AddReqNum, appNum, "", "")

            '    End If

            'Next


            'Response.Redirect("~/RequestSubmited.aspx?RequestNum=" & AddReqNum)

            '=========================================================
            ' Checks if Hosp / Ent needed
            '=========================================================



            'For Each rw As GridViewRow In grAccountsToAdd.Rows
            '    If rw.RowType = DataControlRowType.DataRow Then

            '        Dim appNum As String = grAccountsToAdd.DataKeys(rw.RowIndex)("AppBase")
            '        Dim appDesc As String = grAccountsToAdd.DataKeys(rw.RowIndex)("ApplicationDesc")




            '        If BusLog.isHospSpecific(appNum) Then
            '            For Each cbFacility As ListItem In cblFacilities.Items

            '                If cbFacility.Selected Then

            '                    blnHost = True

            '                End If

            '            Next

            '            If blnHost = False Then

            '                lblValidateAccountRequest.Text = "Must select Hospital for " & appDesc

            '                Return
            '            End If

            '        ElseIf BusLog.isEntSpecific(appNum) Then

            '            For Each cbEnt As ListItem In cblEntities.Items

            '                If cbEnt.Selected Then

            '                    blnEnt = True

            '                End If

            '            Next

            '            If blnEnt = False Then

            '                lblValidateAccountRequest.Text = "Must select Entity for " & appDesc
            '                Return
            '            End If



            '        End If


            '    End If

            'Next
            
            '=========================================================
            ' Inserts values if Entity / Hospital check has passed
            '=========================================================



            '        If BusLog.isHospSpecific(appNum) Then
            '            For Each cbFacility As ListItem In cblFacilities.Items

            '                If cbFacility.Selected Then

            '                    AccountCode = BusLog.GetApplicationCode(appNum, cbFacility.Value)

            '                    insAcctItems(AddReqNum, AccountCode, "", "")

            '                    blnHost = True

            '                End If

            '            Next

            '            If blnHost = False Then

            '                lblValidateAccountRequest.Text = "Must select Hospital for " & appDesc

            '            End If

            '        ElseIf BusLog.isEntSpecific(appNum) Then

            '            For Each cbEnt As ListItem In cblEntities.Items

            '                If cbEnt.Selected Then

            '                    AccountCode = BusLog.GetApplicationCode(appNum, cbEnt.Value)

            '                    insAcctItems(AddReqNum, AccountCode, "", "")

            '                    blnEnt = True
            '                End If

            '            Next

            '            If blnEnt = False Then

            '                lblValidateAccountRequest.Text = "Must select Entity for " & appDesc

            '            End If

            '        Else

            '            insAcctItems(AddReqNum, appNum, "", "")

            '        End If


            '    End If

            'Next


            'Response.Redirect("~/RequestSubmited.aspx?RequestNum=" & AddReqNum)





        Catch ex As Exception

        End Try


    End Sub

    'Private Function AccountRequestNum(ByVal sRequestType As String) As Integer
    '    Dim iRequestClientNum As Integer

    '    If sRequestType = "addacct" Then
    '        iRequestClientNum = Add_requestor_client_numLabel.Text

    '    End If

    '    If sRequestType = "delacct" Then
    '        iRequestClientNum = Remove_requestor_client_numLabel.Text

    '    End If

    '    FormSignOn = New FormSQL(Session("EmployeeConn"), "sel_client_by_number", "InsAccountRequestV3", "", Page)
    '    FormSignOn.AddInsertParm("@requestor_client_num", iRequestClientNum, SqlDbType.NVarChar, 10)
    '    FormSignOn.AddInsertParm("@submitter_client_num", Session("ClientNum"), SqlDbType.NVarChar, 10)
    '    FormSignOn.AddInsertParm("@receiver_client_num", iClientNum, SqlDbType.Int, 10)
    '    FormSignOn.AddInsertParm("@account_request_type", sRequestType, SqlDbType.NVarChar, 9)
    '    FormSignOn.UpdateFormReturnReqNum()

    '    AccountRequestNum = FormSignOn.AccountRequestNum

    'End Function

    'Private Sub displayPendingQueues()
    '    GetPendingQueues()


    '    If dtPendingRequests.Rows.Count > 0 Then

    '        '  cblApplications.Enabled = False


    '        Dim rwTitle As New TableRow
    '        Dim clTitle As New TableCell


    '        Dim iRowCount As Integer = 1
    '        Dim rwHeader As New TableRow
    '        Dim clAccountHdr As New TableCell
    '        Dim clRequestTypeHdr As New TableCell
    '        Dim clValidCdHdr As New TableCell
    '        Dim clRequestorHdr As New TableCell
    '        Dim clDateHdr As New TableCell


    '        clTitle.Text = "Pending Requests"
    '        clTitle.Font.Bold = True
    '        clTitle.ColumnSpan = 5
    '        clTitle.BackColor = Color.White
    '        clTitle.HorizontalAlign = HorizontalAlign.Center
    '        clTitle.Font.Size = 20



    '        rwTitle.Cells.Add(clTitle)

    '        tblPendingQueues.Rows.Add(rwTitle)

    '        clAccountHdr.Text = "Account"
    '        clRequestTypeHdr.Text = "Request"
    '        clValidCdHdr.Text = "Status"
    '        clRequestorHdr.Text = "Requester"
    '        clDateHdr.Text = "Request Date"

    '        rwHeader.Cells.Add(clAccountHdr)
    '        rwHeader.Cells.Add(clRequestTypeHdr)
    '        rwHeader.Cells.Add(clValidCdHdr)
    '        rwHeader.Cells.Add(clRequestorHdr)
    '        rwHeader.Cells.Add(clDateHdr)

    '        rwHeader.Font.Bold = True
    '        rwHeader.BackColor = Color.FromName("#006666")
    '        rwHeader.ForeColor = Color.FromName("White")
    '        rwHeader.Height = Unit.Pixel(36)

    '        tblPendingQueues.Rows.Add(rwHeader)

    '        tblPendingQueues.Width = New Unit("100%")



    '        For Each rwPending As DataRow In dtPendingRequests.Rows

    '            Dim rwData As New TableRow
    '            Dim clAccountData As New TableCell
    '            Dim clRequestTypeData As New TableCell
    '            Dim clValidCdData As New TableCell
    '            Dim clRequestorData As New TableCell
    '            Dim clDateData As New TableCell


    '            clAccountData.Text = IIf(IsDBNull(rwPending("ApplicationDesc")), "", rwPending("ApplicationDesc"))
    '            clRequestTypeData.Text = IIf(IsDBNull(rwPending("account_request_type")), "", rwPending("account_request_type"))
    '            clValidCdData.Text = IIf(IsDBNull(rwPending("valid_cd")), "", rwPending("valid_cd"))
    '            clRequestorData.Text = IIf(IsDBNull(rwPending("requestor_name")), "", rwPending("requestor_name"))
    '            clDateData.Text = IIf(IsDBNull(rwPending("entered_date")), "", rwPending("entered_date"))

    '            rwData.Cells.Add(clAccountData)
    '            rwData.Cells.Add(clRequestTypeData)
    '            rwData.Cells.Add(clValidCdData)
    '            rwData.Cells.Add(clRequestorData)
    '            rwData.Cells.Add(clDateData)


    '            If iRowCount > 0 Then

    '                rwData.BackColor = Color.Bisque

    '            Else

    '                rwData.BackColor = Color.LightGray

    '            End If



    '            iRowCount = iRowCount * -1


    '            tblPendingQueues.Rows.Add(rwData)


    '        Next
    '        tpPendingRequests.Visible = True
    '    Else

    '        tpPendingRequests.Visible = False
    '    End If

    'End Sub

    'Private Sub GetPendingQueues()
    '    Dim thisdata As New CommandsSqlAndOleDb("SelPendingQueuesByRecieverNum", Session("EmployeeConn"))
    '    thisdata.AddSqlProcParameter("@reciever_client_num", iClientNum, SqlDbType.Int, 20)

    '    dtPendingRequests = thisdata.GetSqlDataTable

    'End Sub

    Protected Sub gvCurrentApps_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvCurrentApps.RowCommand

        If e.CommandName = "Select" Then


            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim ApplicationNum As String = gvCurrentApps.DataKeys(Convert.ToInt32(e.CommandArgument))("ApplicationNum").ToString()
            Dim ApplicationDesc As String = gvCurrentApps.DataKeys(Convert.ToInt32(e.CommandArgument))("ApplicationDesc").ToString()
            Dim CreateDate As String = gvCurrentApps.DataKeys(Convert.ToInt32(e.CommandArgument))("create_date").ToString()
            Dim LoginName As String = gvCurrentApps.DataKeys(Convert.ToInt32(e.CommandArgument))("login_name").ToString()

            dtRemoveAccounts = DirectCast(ViewState("dtRemoveAccounts"), DataTable)
            dtRemoveAccounts.Rows.Add(ApplicationNum, ApplicationDesc, LoginName)

            gvRemoveAccounts.DataSource = dtRemoveAccounts
            gvRemoveAccounts.DataBind()

            Dim dtAccounts As New DataTable
            dtAccounts = DirectCast(ViewState("dtAccounts"), DataTable)
            dtAccounts.Rows(index).Delete()
            dtAccounts.AcceptChanges()


            ViewState("dtAccounts") = dtAccounts

            gvCurrentApps.DataSource = dtAccounts
            gvCurrentApps.DataBind()



            'uplAccounts.Update()

        End If
    End Sub
    Protected Sub gvRemoveAccounts_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvRemoveAccounts.RowCommand

        If e.CommandName = "Select" Then


            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim ApplicationNum As String = gvRemoveAccounts.DataKeys(Convert.ToInt32(e.CommandArgument))("ApplicationNum").ToString()
            Dim ApplicationDesc As String = gvRemoveAccounts.DataKeys(Convert.ToInt32(e.CommandArgument))("ApplicationDesc").ToString()

            Dim dtAccounts As New DataTable
            dtAccounts = DirectCast(ViewState("dtAccounts"), DataTable)
            dtAccounts.Rows.Add(ApplicationNum, ApplicationDesc)

            ViewState("dtAccounts") = dtAccounts

            gvCurrentApps.DataSource = dtAccounts
            gvCurrentApps.DataBind()




            dtRemoveAccounts = DirectCast(ViewState("dtRemoveAccounts"), DataTable)
            dtRemoveAccounts.Rows(index).Delete()
            dtRemoveAccounts.AcceptChanges()


            ViewState("dtRemoveAccounts") = dtRemoveAccounts

            gvRemoveAccounts.DataSource = dtRemoveAccounts
            gvRemoveAccounts.DataBind()



            'uplAccounts.Update()

        End If
    End Sub
    Protected Sub grAccountsToAdd_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grAccountsToAdd.RowCommand

        If e.CommandName = "Select" Then


            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim ApplicationNum As String = grAccountsToAdd.DataKeys(Convert.ToInt32(e.CommandArgument))("Applicationnum").ToString()
            Dim ApplicationDesc As String = grAccountsToAdd.DataKeys(Convert.ToInt32(e.CommandArgument))("ApplicationDesc").ToString()

            Dim dtAvailableAccounts As New DataTable
            dtAvailableAccounts = DirectCast(ViewState("dtAvailableAccounts"), DataTable)
            dtAvailableAccounts.Rows.Add(ApplicationNum, ApplicationDesc)

            ViewState("dtAvailableAccounts") = dtAvailableAccounts

            grAvailableAccounts.DataSource = dtAvailableAccounts
            grAvailableAccounts.DataBind()


            dtAddAccounts = DirectCast(ViewState("dtAddAccounts"), DataTable)
            dtAddAccounts.Rows(index).Delete()
            dtAddAccounts.AcceptChanges()


            ViewState("dtAddAccounts") = dtAddAccounts

            grAccountsToAdd.DataSource = dtAddAccounts
            grAccountsToAdd.DataBind()


            'uplAccounts.Update()

        End If
    End Sub
    Protected Sub grAvailableAccounts_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grAvailableAccounts.RowCommand

        If e.CommandName = "Select" Then


            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim ApplicationNum As String = grAvailableAccounts.DataKeys(Convert.ToInt32(e.CommandArgument))("Applicationnum").ToString()
            Dim ApplicationDesc As String = grAvailableAccounts.DataKeys(Convert.ToInt32(e.CommandArgument))("ApplicationDesc").ToString()


            dtAddAccounts = DirectCast(ViewState("dtAddAccounts"), DataTable)
            dtAddAccounts.Rows.Add(ApplicationNum, ApplicationDesc)

            grAccountsToAdd.DataSource = dtAddAccounts
            grAccountsToAdd.DataBind()

            Dim dtAvailableAccounts As New DataTable
            dtAvailableAccounts = DirectCast(ViewState("dtAvailableAccounts"), DataTable)
            dtAvailableAccounts.Rows(index).Delete()
            dtAvailableAccounts.AcceptChanges()

            grAvailableAccounts.DataSource = dtAvailableAccounts
            grAvailableAccounts.DataBind()

            ViewState("dtAvailableAccounts") = dtAvailableAccounts
            ViewState("dtAddAccounts") = dtAddAccounts

            grAccountsToAdd.DataSource = dtAddAccounts
            grAccountsToAdd.DataBind()

            'uplAccounts.Update()

        End If
    End Sub

    'Protected Function checkFacilities() As Boolean
    '    Dim blnReturn = False

    '    For Each item As ListItem In cblFacilities.Items
    '        If item.Selected Then
    '            blnReturn = True

    '        End If


    '    Next


    '    Return blnReturn

    'End Function

    'Protected Function checkEntities() As Boolean
    '    Dim blnReturn As Boolean = False

    '    For Each item As ListItem In cblEntities.Items
    '        If item.Selected Then

    '            blnReturn = True
    '        End If

    '    Next


    '    Return blnReturn
    'End Function

    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        If HDemployeetype.Text.ToLower <> emp_type_cdrbl.SelectedValue.ToLower Then
            If emp_type_cdrbl.SelectedValue = "physician" Or emp_type_cdrbl.SelectedValue = "allied health" Then

                lblValidation.Text = "Can Not Change Affiliation type to Physician type of any kind. You must get a PROVIDER Modifier to make this change."
                emp_type_cdrbl.SelectedValue = HDemployeetype.Text

                Exit Sub
            End If
        End If

        If emp_type_cdrbl.SelectedIndex < 0 Then

            lblValidation.Text = "Must Select an Affiliation"

        Else

            ' emp_type_cdrbl.SelectedValue = "" 
            FormSignOn.AddInsertParm("@clientnum", iClientNum, SqlDbType.Int, 9)
            FormSignOn.UpdateForm()
            Dim sDate As DateTime = Date.Now()

            lblValidation.Text = "Information Updated at -> " & sDate


            Dim stProvider As String = FormSignOn.getFieldValue("provider").Trim()
            Dim isProvider As String = providerrbl.SelectedValue





            If (stProvider = "" Or stProvider = "No") And isProvider = "Yes" Then


                Dim FormProvider = New FormSQL(Session("EmployeeConn"), "SelAccountRequestV20", "InsAccountRequestV20", "", Page)

                FormProvider.AddInsertParm("@submitter_client_num", UserInfo.ClientNum, SqlDbType.NVarChar, 10)
                FormProvider.AddInsertParm("@requestor_client_num", UserInfo.ClientNum, SqlDbType.NVarChar, 10)
                FormProvider.AddInsertParm("@receiver_client_num", iClientNum, SqlDbType.Int, 30)
                FormProvider.AddInsertParm("@account_request_type", "provider", SqlDbType.NVarChar, 9)


                FormProvider.UpdateFormReturnReqNum()

                Dim AccountRequestNum As Integer = FormProvider.AccountRequestNum

                If Session("environment").ToString.ToLower <> "testing" Then
                    SendProviderEmail(AccountRequestNum)

                End If



            End If


        End If

    End Sub

    Protected Sub btnCloseOnBelafOf_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCloseOnBelafOf.Click

        mpeOnBehalfOf.Hide()

    End Sub
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click

        Dim thisData As New CommandsSqlAndOleDb("SelSimpleSearchV2", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@FirstName", FirstNameTextBox.Text, SqlDbType.NVarChar, 20)
        thisData.AddSqlProcParameter("@LastName", LastNameTextBox.Text, SqlDbType.NVarChar, 20)






        gvSearch.DataSource = thisData.GetSqlDataTable
        gvSearch.DataBind()

        uplOnBehalfOf.Update()
        mpeOnBehalfOf.Show()

    End Sub

    'Protected Sub btnRemoveOnBehalfOf_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemoveOnBehalfOf.Click


    '    ViewState("RequestorSearch") = "Remove"
    '    mpeOnBehalfOf.Show()

    'End Sub

    'Protected Sub btnSelectOnBehalfOf_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSelectOnBehalfOf.Click

    '    ViewState("RequestorSearch") = "Add"

    '    mpeOnBehalfOf.Show()

    'End Sub

    'Protected Sub gvSearch_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvSearch.RowCommand

    '    If e.CommandName = "Select" Then
    '        Dim client_num As String = gvSearch.DataKeys(Convert.ToInt32(e.CommandArgument))("client_num").ToString()


    '        If ViewState("RequestorSearch") = "Add" Then

    '            Add_SubmittingOnBehalfOfNameLabel.Text = gvSearch.Rows(Convert.ToInt32(e.CommandArgument)).Cells(1).Text
    '            Add_requestor_client_numLabel.Text = client_num


    '            ViewState("RequestorSearch") = ""
    '        End If
    '        If ViewState("RequestorSearch") = "Remove" Then

    '            Remove_SubmittingOnBehalfOfNameLabel.Text = gvSearch.Rows(Convert.ToInt32(e.CommandArgument)).Cells(1).Text
    '            Remove_requestor_client_numLabel.Text = client_num

    '            ViewState("RequestorSearch") = ""
    '        End If

    '        uplAccounts.Update()

    '        mpeOnBehalfOf.Hide()



    '    End If
    'End Sub
    'Protected Sub gvCurrentUserAccounts_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvCurrentUserAccounts.RowCommand
    '    If e.CommandName = "Select" Then
    '        Dim ApplicationNum As String = gvCurrentUserAccounts.DataKeys(Convert.ToInt32(e.CommandArgument))("ApplicationNum").ToString()
    '        Dim ApplicationDesc As String = gvCurrentUserAccounts.DataKeys(Convert.ToInt32(e.CommandArgument))("ApplicationDesc").ToString()
    '        Dim LoginName As String = gvCurrentUserAccounts.DataKeys(Convert.ToInt32(e.CommandArgument))("login_name").ToString()
    '        ViewState("LoginEditCreateDate") = gvCurrentUserAccounts.DataKeys(Convert.ToInt32(e.CommandArgument))("create_date").ToString()


    '        pnlEditLogin.Visible = True
    '        pnlDuplicatAccounts.Visible = False


    '        lblEditAccountName.Text = ApplicationDesc
    '        txtEditAccountName.Text = LoginName
    '        txtEditAccountNum.Text = ApplicationNum

    '    End If
    'End Sub

    'Protected Sub btnEditAccountName_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditAccountName.Click

    '    Dim thisData As New CommandsSqlAndOleDb("SelDuplicateAccountLogins", Session("EmployeeConn"))
    '    thisData.AddSqlProcParameter("@ApplicationNum", txtEditAccountNum.Text, SqlDbType.NVarChar, 20)
    '    thisData.AddSqlProcParameter("@login_name", txtEditAccountName.Text, SqlDbType.NVarChar, 20)


    '    Dim dtDuplicateAccounts As DataTable
    '    dtDuplicateAccounts = thisData.GetSqlDataTable


    '    If dtDuplicateAccounts.Rows.Count = 0 Then

    '        UpdateLoginAccount()

    '    Else
    '        For Each dr As DataRow In dtDuplicateAccounts.Rows

    '            Try



    '            Dim rwDup As New TableRow
    '            Dim clDup As New TableCell
    '                clDup.Text = "<a target='_blank' href=""AddRemoveAcctForm.aspx?ClientNum=" & dr("client_num") & """ ><u><b> " & dr("DupName") & "</b></u></a>"
    '                '  clDup. = "<asp:HyperLink  NavigateUrl='AddRemoveAcctForm.aspx?ClientNum=" & dr("client_num") & " runat='server' Text='" & dr("DupName") & "' Target='_blank'>" & dr("DupName") & "</asp:HyperLink>"
    '            rwDup.Cells.Add(clDup)

    '            tblDubNames.Rows.Add(rwDup)

    '            Catch ex As Exception

    '            End Try
    '        Next

    '        pnlDuplicatAccounts.Visible = True
    '    End If



    'End Sub


    'Protected Sub btnEditLoginNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditLoginNo.Click

    '    pnlEditLogin.Visible = False


    'End Sub


    'Protected Sub btnEditLoginYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditLoginYes.Click

    '    pnlEditLogin.Visible = False
    '    UpdateLoginAccount()

    'End Sub


    'Public Sub UpdateLoginAccount()

    '    Dim dataUpdateAcct As New CommandsSqlAndOleDb("UpdAccountLoginByNumberV3", Session("EmployeeConn"))
    '    dataUpdateAcct.AddSqlProcParameter("@client_num", iClientNum, SqlDbType.Int, 20)
    '    dataUpdateAcct.AddSqlProcParameter("@ApplicationNum", txtEditAccountNum.Text, SqlDbType.Int, 8)
    '    dataUpdateAcct.AddSqlProcParameter("@create_date", ViewState("LoginEditCreateDate"), SqlDbType.DateTime, 255)
    '    dataUpdateAcct.AddSqlProcParameter("@login_name", txtEditAccountName.Text, SqlDbType.NVarChar, 255)
    '    dataUpdateAcct.AddSqlProcParameter("@tech_client_num", Session("ClientNum"), SqlDbType.NVarChar, 255)

    '    dataUpdateAcct.ExecNonQueryNoReturn()

    '    BindCurrentApps()

    'End Sub

    'Public Sub BindCurrentApps()


    '    Dim thisdata As New CommandsSqlAndOleDb("SelAllClientAccountsByNumber", Session("EmployeeConn"))
    '    thisdata.AddSqlProcParameter("@client_num", iClientNum, SqlDbType.Int, 20)

    '    thisdata.ExecNonQueryNoReturn()

    '    gvCurrentUserAccounts.DataSource = thisdata.GetSqlDataTable
    '    gvCurrentUserAccounts.DataBind()


    'End Sub

    Protected Sub gvSearch_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvSearch.RowCommand

        If e.CommandName = "Select" Then
            Dim client_num As String = gvSearch.DataKeys(Convert.ToInt32(e.CommandArgument))("client_num").ToString()
            Dim login_name As String = gvSearch.DataKeys(Convert.ToInt32(e.CommandArgument))("login_name").ToString()
            Dim c3Submitter As New Client3

            c3Submitter = c3Controller.GetClientByClientNum(client_num)


            SubmittingOnBehalfOfNameLabel.Text = c3Submitter.FullName & " (" & c3Submitter.EMail & ")"
            requestor_client_numLabel.Text = client_num
            'btnUpdateSubmitter.Visible = True



            'Dim actDir As New ActiveDir(login_name)
            'SubmittingOnBehalfOfEmailLabel.Text = actDir.UserAdEntry.Properties.Item("mail").Value.ToString()



        End If
    End Sub
    Protected Sub entity_cdDDL_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles entity_cdDDL.SelectedIndexChanged
        Dim ddbDepartmentBinder As New DropDownListBinder(department_cdddl, "sel_department_codes", Session("EmployeeConn"))
        ddbDepartmentBinder.AddDDLCriteria("@entity_cd", entity_cdDDL.SelectedValue, SqlDbType.NVarChar)
        ddbDepartmentBinder.BindData("department_cd", "department_name")
        department_cdLabel.Text = ""

    End Sub
    Protected Sub department_cdddl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles department_cdddl.SelectedIndexChanged
       department_cdLabel.Text = department_cdddl.SelectedValue

    End Sub
    Protected Sub SendProviderEmail(ByVal AccountRequestNum As Integer)
        '======================
        ' Get current path
        '======================

        Dim path As String
        Dim directory As String = ""
        path = HttpContext.Current.Request.Url.AbsoluteUri
        Dim pathParts() As String = Split(path, "/")
        Dim i As Integer
        Do While i < pathParts.Length() - 1

            directory = directory + pathParts(i) + "/"
            i = i + 1

        Loop

        '======================

        Dim EmailList As New List(Of EmailDistribution)
        EmailList = New EmailDistributionList().EmailList

        Dim sndEmailTo = From e In EmailList
                         Where e.EmailEvent = "AddNew" And e.AppBase = "provider"
                         Select e


        For Each EmailAddress In sndEmailTo



            Dim toEmail As String = EmailAddress.Email


            Dim sSubject As String
            Dim sBody As String


            sSubject = "Account Request for " & first_nameTextBox.Text & " " & last_nameTextBox.Text & " has been added to the Provider Queue."


            sBody = "<!DOCTYPE html>" & _
                                "<html>" & _
                                "<head>" & _
                                "<style>" & _
                                "table" & _
                                "{" & _
                                "border-collapse:collapse;" & _
                                "font-family: 'Helvetica Neue', 'Lucida Grande', 'Segoe UI', Arial, Helvetica, Verdana, sans-serif;" & _
                                "}" & _
                                "table, th, td" & _
                                "{" & _
                                "border: 1px solid black;" & _
                                "padding: 3px" & _
                                "}" & _
                                "</style>" & _
                                "</head>" & _
                                "" & _
                                "<body>" & _
                                "A new account request has been added to the Provider Queue.  Follow the link below to go to Queue Screen." & _
                                 "<br />" & _
                                "<a href=""" & directory & "/ProviderRequest.aspx?requestnum=" & AccountRequestNum & """ ><u><b>Provider: " & first_nameTextBox.Text & " " & last_nameTextBox.Text & "</b></u></a>" & _
                                "</body>" & _
                            "</html>"






            Dim sToAddress As String = EmailAddress.Email


            '  sToAddress = "Patrick.Brennan@crozer.org"


            Dim mailObj As New MailMessage()
            mailObj.From = New MailAddress("CSC@crozer.org")
            mailObj.To.Add(New MailAddress(sToAddress))
            mailObj.IsBodyHtml = True
            mailObj.Subject = sSubject
            mailObj.Body = sBody


            Dim smtpClient As New SmtpClient()
            smtpClient.Host = "ah1smtprelay01.altahospitals.com"



            smtpClient.Send(mailObj)

        Next

    End Sub
    Protected Sub fillOpenRequestTable()
        Dim thisData As New CommandsSqlAndOleDb("SelOpenRequestByClient", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@client_num", iClientNum, SqlDbType.NVarChar)

        Dim iRowCount = 1

        Dim dt As DataTable
        dt = thisData.GetSqlDataTable

        Dim rwHeader As New TableRow

        Dim clRequestHeader As New TableCell
        Dim clReceiverNameHeader As New TableCell
        Dim clRequestorNameHeader As New TableCell
        Dim clSubmitterNameHeader As New TableCell
        Dim clReceiverHeader As New TableCell
        Dim cldateHeader As New TableCell

        'account_request_num int null,
        'ReceiverName nvarchar(150) null,
        'RequestorName nvarchar(150) null,
        'SubmitterName  nvarchar(150) null,
        'Receiver nvarchar(25) null,
        'entered_date datetime null)


        clRequestHeader.Text = "Req#"
        clReceiverNameHeader.Text = "Receiver"
        clRequestorNameHeader.Text = "Requestor"
        clSubmitterNameHeader.Text = "Submitter"
        clReceiverHeader.Text = "Who was"
        cldateHeader.Text = "Date"


        rwHeader.Cells.Add(clRequestHeader)
        rwHeader.Cells.Add(clReceiverNameHeader)
        rwHeader.Cells.Add(clRequestorNameHeader)

        rwHeader.Cells.Add(clSubmitterNameHeader)
        rwHeader.Cells.Add(clReceiverHeader)
        rwHeader.Cells.Add(cldateHeader)


        rwHeader.Font.Bold = True
        rwHeader.BackColor = Color.FromName("#006666")
        rwHeader.ForeColor = Color.FromName("White")
        rwHeader.Height = Unit.Pixel(36)

        tblOpenRequests.Rows.Add(rwHeader)

        For Each drRow As DataRow In dt.Rows


            Dim rwData As New TableRow
            Dim btnSelect As New Button

            'Dim clRequestData As New TableCell
            Dim clSelectData As New TableCell
            Dim clReceiverNameData As New TableCell
            Dim clRequestorNameData As New TableCell
            Dim clSubmitterNameData As New TableCell
            Dim clReceiverData As New TableCell
            Dim cldateData As New TableCell

            btnSelect.Text = "Select"
            btnSelect.CommandName = "Select"
            btnSelect.CommandArgument = drRow("account_request_num")
            ' btnSelect.Width = 50
            btnSelect.EnableTheming = False

            AddHandler btnSelect.Click, AddressOf btnSelect_Click

            clReceiverNameData.Text = IIf(IsDBNull(drRow("ReceiverName")), "", drRow("ReceiverName"))
            clRequestorNameData.Text = IIf(IsDBNull(drRow("RequestorName")), "", drRow("RequestorName"))
            clSubmitterNameData.Text = IIf(IsDBNull(drRow("SubmitterName")), "", drRow("SubmitterName"))

            clReceiverData.Text = IIf(IsDBNull(drRow("Receiver")), "", drRow("Receiver"))
            cldateData.Text = IIf(IsDBNull(drRow("entered_date")), "", drRow("entered_date"))

            'add button to grid
            clSelectData.Controls.Add(btnSelect)

            rwData.Cells.Add(clSelectData)
            rwData.Cells.Add(clReceiverNameData)
            rwData.Cells.Add(clRequestorNameData)
            rwData.Cells.Add(clSubmitterNameData)

            rwData.Cells.Add(clReceiverData)
            rwData.Cells.Add(cldateData)


            If iRowCount > 0 Then

                rwData.BackColor = Color.Bisque

            Else

                rwData.BackColor = Color.LightGray

            End If

            tblOpenRequests.Rows.Add(rwData)

            iRowCount = iRowCount * -1

        Next
        tblOpenRequests.CellPadding = 10
        tblOpenRequests.Width = New Unit("100%")


    End Sub
    Sub btnSelect_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim btnLinkView As Button = sender

        If btnLinkView.CommandName = "Select" Then
            Dim sArguments As String() = Split(btnLinkView.CommandArgument, ";")

            Response.Redirect("./AccountRequestInfo2.aspx?RequestNum=" & sArguments(0))

            '            Response.Redirect("./OpenProviderRequestItems.aspx?RequestNum=" & sArguments(0))

        End If
    End Sub
    Private Sub CheckAffiliation()

        pnlAlliedHealth.Visible = False


        HDemployeetype.Text = emp_type_cdrbl.SelectedValue


        Select Case emp_type_cdrbl.SelectedValue

            Case "employee"

                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")



                pnlAlliedHealth.Visible = False

                pnlProvider.Visible = False
                providerrbl.SelectedIndex = -1



            Case "student"

                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")
                Dim titleReq As New RequiredField(Titleddl, "Title")
                Dim specReq As New RequiredField(Specialtyddl, "Specialty")
                Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim faxqReq As New RequiredField(faxtextbox, "Fax")


                pnlAlliedHealth.Visible = False

                pnlProvider.Visible = False
                providerrbl.SelectedIndex = -1


            Case "physician"
                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")
                Dim titleReq As New RequiredField(Titleddl, "Title")
                Dim specReq As New RequiredField(Specialtyddl, "Specialty")
                Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim faxqReq As New RequiredField(faxtextbox, "Fax")


                pnlAlliedHealth.Visible = False

                pnlProvider.Visible = True
                providerrbl.SelectedValue = "Yes"

            Case "non staff clinician"
                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")
                Dim titleReq As New RequiredField(Titleddl, "Title")
                Dim specReq As New RequiredField(Specialtyddl, "Specialty")
                Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim faxqReq As New RequiredField(faxtextbox, "Fax")


                pnlAlliedHealth.Visible = False

                providerrbl.SelectedIndex = -1
                pnlProvider.Visible = True

            Case "resident"
                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")
                Dim titleReq As New RequiredField(Titleddl, "Title")
                Dim specReq As New RequiredField(Specialtyddl, "Specialty")
                Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim faxqReq As New RequiredField(faxtextbox, "Fax")


                pnlAlliedHealth.Visible = False

                providerrbl.SelectedValue = "Yes"
                pnlProvider.Visible = True


            Case "vendor"

                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")
                Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim vendorRequired As New RequiredField(VendorNameTextBox, "Vendor")



                pnlAlliedHealth.Visible = False
                pnlProvider.Visible = False
                providerrbl.SelectedIndex = -1


            Case "contract"

                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")

                Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")


                pnlAlliedHealth.Visible = False

                pnlProvider.Visible = False
                providerrbl.SelectedIndex = -1

            Case "allied health"

                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")
                Dim titleReq As New RequiredField(Titleddl, "Title")
                Dim specReq As New RequiredField(Specialtyddl, "Specialty")
                Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim faxqReq As New RequiredField(faxtextbox, "Fax")



                providerrbl.SelectedIndex = 0
                pnlProvider.Visible = True
                providerrbl.SelectedValue = "Yes"

                pnlAlliedHealth.Visible = True





            Case "other"

                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")

                Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")


                pnlAlliedHealth.Visible = False

                pnlProvider.Visible = False
                providerrbl.SelectedIndex = -1



            Case Else


                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")


                pnlAlliedHealth.Visible = False

                pnlProvider.Visible = False
                providerrbl.SelectedIndex = -1



        End Select


    End Sub
    Protected Sub emp_type_cdrbl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles emp_type_cdrbl.SelectedIndexChanged


        Dim removeCss As New PageControls(Page)
        removeCss.RemoveCssClass(Page, "textInputRequired")

        OtherAffDescddl.SelectedIndex = -1

        pnlAlliedHealth.Visible = False


        emp_type_cdLabel.Text = emp_type_cdrbl.SelectedValue
        Select Case emp_type_cdrbl.SelectedValue

            Case "employee"

                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")

                'Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                'Dim cityReq As New RequiredField(citytextbox, "City")
                'Dim stateReq As New RequiredField(statetextbox, "State")
                'Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                'Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                'Dim faxqReq As New RequiredField(faxtextbox, "Fax")
                '  Dim locReq As New RequiredField(facility_cdddl, "Location")

                ' Dim endReq As New RequiredField(end_dateTextBox, "End Date")


                pnlAlliedHealth.Visible = False
                pnlProvider.Visible = False
                providerrbl.SelectedIndex = -1




            Case "student"

                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")
                Dim titleReq As New RequiredField(Titleddl, "Title")
                Dim specReq As New RequiredField(Specialtyddl, "Specialty")
                Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim faxqReq As New RequiredField(faxtextbox, "Fax")

                pnlAlliedHealth.Visible = False
                pnlProvider.Visible = False
                providerrbl.SelectedIndex = -1

                'Dim locReq As New RequiredField(facility_cdddl, "Location")

                'Dim endReq As New RequiredField(end_dateTextBox, "End Date")

            Case "physician"
                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")
                Dim titleReq As New RequiredField(Titleddl, "Title")
                Dim specReq As New RequiredField(Specialtyddl, "Specialty")
                Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim faxqReq As New RequiredField(faxtextbox, "Fax")

                pnlProvider.Visible = True

                pnlAlliedHealth.Visible = False
                providerrbl.SelectedValue = "Yes"

                ' pnlTCl.Visible = True

            Case "non staff clinician"
                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")
                Dim titleReq As New RequiredField(Titleddl, "Title")
                Dim specReq As New RequiredField(Specialtyddl, "Specialty")
                Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim faxqReq As New RequiredField(faxtextbox, "Fax")

                pnlProvider.Visible = True

                pnlAlliedHealth.Visible = False
                providerrbl.SelectedIndex = -1

            Case "resident"
                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")
                Dim titleReq As New RequiredField(Titleddl, "Title")
                Dim specReq As New RequiredField(Specialtyddl, "Specialty")
                Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim faxqReq As New RequiredField(faxtextbox, "Fax")

                pnlProvider.Visible = True

                pnlAlliedHealth.Visible = False
                providerrbl.SelectedValue = "Yes"



            Case "vendor"

                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")

                Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim vendorRequired As New RequiredField(VendorNameTextBox, "Vendor")
                Dim startReq As New RequiredField(start_dateTextBox, "Start Date")


                pnlAlliedHealth.Visible = False
                pnlProvider.Visible = False
                providerrbl.SelectedIndex = -1


            Case "contract"

                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")

                Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim startReq As New RequiredField(start_dateTextBox, "Start Date")

                pnlAlliedHealth.Visible = False
                pnlProvider.Visible = False
                providerrbl.SelectedIndex = -1

            Case "allied health"

                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")
                Dim titleReq As New RequiredField(Titleddl, "Title")
                Dim specReq As New RequiredField(Specialtyddl, "Specialty")
                Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim faxqReq As New RequiredField(faxtextbox, "Fax")



                providerrbl.SelectedIndex = 0

                pnlAlliedHealth.Visible = True
                pnlProvider.Visible = True

                pnlProvider.Visible = False
                providerrbl.SelectedValue = "Yes"



            Case "other"

                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")

                Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")



                pnlAlliedHealth.Visible = False
                pnlProvider.Visible = False
                providerrbl.SelectedIndex = -1



            Case Else


                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")


                pnlAlliedHealth.Visible = False
                pnlProvider.Visible = False
                providerrbl.SelectedIndex = -1



        End Select


        'Dim BusLog As New AccountBusinessLogic
        'BusLog.CheckAccountDependancy(cblApplications)

        'If BusLog.eCare Then
        '    pnlTCl.Visible = True
        '    'Dim reqField As New RequiredField(TCLTextBox, "TCL for eCare")

        'Else
        '    pnlTCl.Visible = False

        '    'pageCtrl.RemoveCssClass(pnlTCl, "textInputRequired")

        'End If
    End Sub
End Class
