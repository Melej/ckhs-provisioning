﻿
<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="false" CodeFile="TicketStats.aspx.vb" Inherits="TicketStats"  ViewStateMode="Enabled"%>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>


<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

<div >
 <asp:UpdatePanel runat="server" ID="uplStats" ChildrenAsTriggers = "true" UpdateMode="Conditional">
                    <ContentTemplate>

<table width="100%">
    <tr>
        <td >
        View Tickets Between <asp:TextBox runat="server" ID = "txtBeginDate" CssClass="calenderClass"></asp:TextBox> And <asp:TextBox runat="server" ID = "txtEndDate" CssClass="calenderClass"></asp:TextBox>
       
        </td>
    </tr>
    <tr>
        <td>
            Priorty <asp:RadioButtonList runat="server" ID="rblPriority" RepeatDirection="Horizontal" AutoPostBack="true">
                        <asp:ListItem Selected="True">All</asp:ListItem>
                        <asp:ListItem>1</asp:ListItem>
                        <asp:ListItem>2</asp:ListItem>
                        <asp:ListItem>3</asp:ListItem>
                        </asp:RadioButtonList>
        </td>
    </tr>
      <tr>
        <td>
            Group
        </td>
      </tr>

    <tr>
        <td valign ="top"> 

           

                <asp:Panel runat="server" ID ="pnlCats">
                
                </asp:Panel>

               
        </td>
    
    </tr>
    <tr>
        <td valign ="top">

            


                <asp:Panel runat="server" ID ="pnlTypes">
                
                </asp:Panel>
        </td>
    
    </tr>

</table>



 
                    </ContentTemplate>
                    </asp:UpdatePanel>

</div>
</asp:Content>