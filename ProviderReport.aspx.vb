﻿Imports System.IO
Imports System.Web


Imports System.Data
Imports System.Data.SqlClient
Imports App_Code

Partial Class ProviderReport
    Inherits System.Web.UI.Page
    Dim AttachedClients As Integer

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            start_dateTextBox.Text = DateAdd(DateInterval.Day, -14, Date.Today)
            end_dateTextBox.Text = Date.Today

            fillTable()

        End If





    End Sub
    Protected Sub btnSubmit_Click(sender As Object, e As System.EventArgs) Handles btnSubmit.Click
        fillTable()


    End Sub

    Protected Sub fillTable()

        Dim thisData As New CommandsSqlAndOleDb("SelOpenProviderReportV4", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@startDate", start_dateTextBox.Text, SqlDbType.NVarChar, 12)
        thisData.AddSqlProcParameter("@endDate", end_dateTextBox.Text, SqlDbType.NVarChar, 12)
        thisData.AddSqlProcParameter("@requestTypes", AllRequestrbl.SelectedValue, SqlDbType.NVarChar, 4)

        Dim iRowCount = 1

        Dim dt As DataTable
        dt = thisData.GetSqlDataTable


        AttachedClients = dt.Rows.Count()

        GvPhysicians.DataSource = dt
        GvPhysicians.DataBind()


        GvPhysicians.SelectedIndex = -1

    End Sub
    Protected Sub btnExcelReport_Click(sender As Object, e As System.EventArgs) Handles btnExcelReport.Click
        Response.Redirect("./Reports/RolesReport.aspx?ReportName=ProviderReport" & "&StartDate=" & start_dateTextBox.Text & "&enddate=" & end_dateTextBox.Text & "&RequestType=" & AllRequestrbl.SelectedValue, True)


    End Sub


    Public Sub LoadTable()

        Dim thisData As New CommandsSqlAndOleDb("SelOpenProviderReportV4", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@startDate", start_dateTextBox.Text, SqlDbType.NVarChar, 12)
        thisData.AddSqlProcParameter("@endDate", end_dateTextBox.Text, SqlDbType.NVarChar, 12)
        thisData.AddSqlProcParameter("@requestTypes", AllRequestrbl.SelectedValue, SqlDbType.NVarChar, 4)

        Dim iRowCount = 1

        Dim dt As DataTable
        dt = thisData.GetSqlDataTable


        Dim rwHeader As New TableRow
        Dim clSelectHeader As New TableCell
        Dim clLastNameHeader As New TableCell
        Dim clFirstNameHeader As New TableCell
        Dim clStartDateHeader As New TableCell
        Dim clEndDateHeader As New TableCell
        Dim clEmpTypeHeader As New TableCell
        Dim clAccountCountHeader As New TableCell



        clLastNameHeader.Text = "  Last Name  "
        clFirstNameHeader.Text = "  First Name "
        clStartDateHeader.Text = "  Start Date  "
        clEndDateHeader.Text = "  End Date  "

        clEmpTypeHeader.Text = " Affiliation "
        clAccountCountHeader.Text = "  #Accounts  "


        rwHeader.Cells.Add(clSelectHeader)
        rwHeader.Cells.Add(clLastNameHeader)
        rwHeader.Cells.Add(clFirstNameHeader)
        rwHeader.Cells.Add(clStartDateHeader)
        rwHeader.Cells.Add(clEndDateHeader)
        rwHeader.Cells.Add(clEmpTypeHeader)
        rwHeader.Cells.Add(clAccountCountHeader)


        rwHeader.Font.Bold = True
        rwHeader.BackColor = Color.FromName("#006666")
        rwHeader.ForeColor = Color.FromName("White")
        rwHeader.Height = Unit.Pixel(36)

        tblEndDate.Rows.Add(rwHeader)

        For Each drRow As DataRow In dt.Rows

            'Dim btnSelect As New Button
            Dim rwData As New TableRow
            Dim btnSelect As New TableCell
            Dim clLNameData As New TableCell
            Dim clFNameData As New TableCell
            Dim clStartData As New TableCell

            Dim clEndData As New TableCell

            Dim clEmpTypeData As New TableCell
            Dim clAccountCountData As New TableCell


            btnSelect.Text = "<a href=""ClientDemo.aspx?ClientNum=" & drRow("client_num") & """ ><u><b> " & drRow("client_num") & "</b></u></a>"

            btnSelect.EnableTheming = False


            clLNameData.Text = IIf(IsDBNull(drRow("last_name")), "", drRow("last_name"))
            clFNameData.Text = IIf(IsDBNull(drRow("first_name")), "", drRow("first_name"))
            clStartData.Text = IIf(IsDBNull(drRow("start_date")), "", drRow("start_date"))
            clEndData.Text = IIf(IsDBNull(drRow("end_date")), "", drRow("end_date"))
            clEmpTypeData.Text = (IIf(IsDBNull(drRow("emp_type_cd")), "", drRow("emp_type_cd")))
            clAccountCountData.Text = (IIf(IsDBNull(drRow("AccountCount")), "", drRow("AccountCount")))


            'rwData.Cells.Add(clSelectData)
            rwData.Cells.Add(btnSelect)
            rwData.Cells.Add(clLNameData)
            rwData.Cells.Add(clFNameData)
            rwData.Cells.Add(clStartData)
            rwData.Cells.Add(clEndData)
            rwData.Cells.Add(clEmpTypeData)
            rwData.Cells.Add(clAccountCountData)



            If iRowCount > 0 Then

                rwData.BackColor = Color.Bisque

            Else

                rwData.BackColor = Color.LightGray

            End If

            tblEndDate.Rows.Add(rwData)

            iRowCount = iRowCount * -1

        Next
        tblEndDate.Width = New Unit("100%")

    End Sub

End Class
