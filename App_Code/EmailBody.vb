﻿Imports Microsoft.VisualBasic

Public Class EmailBody
    Public Sub New()

    End Sub

    Public Function EmailCompletedRequestTable(ByVal Request As AccountRequest, ByVal RequestItems As RequestItems) As String
        Dim sReturn As String

        sReturn = "<table cellpadding='5px'>" & _
        "<tr>" & _
            "<td colspan='3' align='center'  style='font-size:large'>" & _
"               " & _
            "<b>                                Account Request Complete                              </b>" & _
"                 " & _
             "</td>    " & _
        "</tr>" & _
        "<tr>" & _
            "<td style='font-weight:bold' colspan='1'>User Name:</td>" & _
            "<td colspan='2'>" & Request.FirstName & " " & Request.LastName & "</td>" & _
        "</tr>" & _
        "<tr>" & _
            "<td style='font-weight:bold' colspan='1'>Requested By:</td>" & _
            "<td colspan='2'>" & Request.RequestorName & "</td>" & _
        "</tr>" & _
           "<tr>" & _
            "<td style='font-weight:bold' colspan='1'>Requestor Email:</td>" & _
            "<td colspan='2'>" & Request.RequestorEmail & "</td>" & _
        "</tr>" & _
        "<tr>" & _
            "<td style='font-weight:bold' colspan='1'>Date Requested:</td>" & _
            "<td colspan='2'>" & Request.EnteredDate & "</td>" & _
        "</tr>" & _
        "<tr>" & _
            "<td style='font-weight:bold' colspan='1'>User Doctor Master#:</td>" & _
            "<td colspan='2'>" & Request.DoctorNum & "</td>" & _
        "</tr>" & _
        "<tr>" & _
            "<td style='font-weight:bold' colspan='1'>User Phone#:</td>" & _
            "<td colspan='2'>" & Request.Phone & "</td>" & _
        "</tr>" & _
"      " & _
"     " & _
"<tr style='font-weight:bold'><td>Account</td><td>Login</td><td>Comment</td></tr>"

        Dim sAccounts As String = ""

        For Each req As RequestItem In RequestItems.RequestItems
            Select Case req.ApplicationDesc.ToLower
                Case "rsa token for remote access"
                    If req.ValidCd = "validated" Then
                        sAccounts = sAccounts & "<tr><td>" & req.ApplicationDesc & "</td><td>" & req.LoginName & "</td><td>Wating for Completion</td></tr>"
                    Else

                        sAccounts = sAccounts & "<tr><td>" & req.ApplicationDesc & "</td><td>" & req.LoginName & "</td><td>" & req.RequestComment & "</td></tr>"
                    End If
                Case "email"

                    sAccounts = sAccounts & "<tr><td>" & req.ApplicationDesc & "</td><td>" & req.LoginName & "</td><td>" & req.eMail & "</td></tr>"
                Case Else
                    If req.ValidCd = "created" Then

                        sAccounts = sAccounts & "<tr><td>" & req.ApplicationDesc & "</td><td>" & req.LoginName & "</td><td>" & req.RequestComment & "</td></tr>"

                    Else
                        sAccounts = sAccounts & "<tr><td>" & req.ApplicationDesc & "</td><td>" & req.LoginName & "</td><td>" & req.ValidCdDesc & "</td></tr>"

                    End If
            End Select

            'If req.ApplicationDesc.ToLower = "email" Then
            '    sAccounts = sAccounts & "<tr><td>" & req.ApplicationDesc & "</td><td>" & req.LoginName & "</td><td>" & req.eMail & "</td></tr>"

            'Else
            '    sAccounts = sAccounts & "<tr><td>" & req.ApplicationDesc & "</td><td>" & req.LoginName & "</td><td>" & req.RequestComment & "</td></tr>"


            'End If


        Next
        sReturn = sReturn & sAccounts & "</table>"

        Return sReturn
    End Function

    Public Function EmailRequestInfoTable(ByVal Request As AccountRequest) As String
        Dim sReturn As String
        sReturn = "        <table border='3' style='align: center;' width='650px'>" &
"        <tr>" &
"                 <td colspan = '4' class='tableRowHeader'>" &
"                     Request Detail" &
"                 </td>" &
"         </tr>" &
"       <tr>" &
"        <td colspan='4'>" &
"           <b>User submitting</b>:         " &
        Request.SubmitterName &
"            </td>" &
"        </tr>" &
"        <tr>" &
"            <td colspan='4'>" &
"            <b>Requestor:</b>" & Request.RequestorName &
            "" &
"              " &
"            </td>" &
"        </tr>" &
       "" &
        "" &
"                <tr>" &
          "" &
"            <td colspan='4' class='tableRowSubHeader'  align='left'>" &
"            <b>CKHS Affiliation:</b> " & Request.EmpTypeCd &
"            </td>" &
"        </tr>" &
"        <tr>" &
"            <td colspan='4'>" &
                      "" &
"                <b>Other Affiliation Description:</b> " & Request.CernerPositionDesc &
"          </td>" &
"        </tr>" &
        " <tr> " &
"                      <td>" &
"                                     <b>Provider:</b>" &
"                                    </td>" &
"                                    <td colspan='3'>" &
                                    Request.Provider &
"                                    </td>" &
                                   "" &
"" &
"         </tr>" &
"         <tr>" &
"                         <td>" &
"                        <b>CKHN-HAN:</b>" &
"                        </td>" &
"                        <td colspan='3'>" &
                                Request.Han &
"                        </td>" &
                    "" &
"         </tr>" &
"           <tr>" &
"                    <td>" &
"                   <b> NPI</b>" &
"                    </td>" &
"                    <td >" &
                            Request.Npi &
"                    </td>" &
"                    <td>" &
"                    <b>License No.</b>" &
"                    </td>" &
"                    <td>" &
                            Request.LicensesNo &
"                    </td>" &
                    "" &
"" &
"               " &
"                 </tr>" &
"                 <tr>" &
"                    <td><b>Taxonomy</b></td>" &
"                    <td colspan ='3'>" &
                            Request.Taxonomy &
"                    </td>" &
                 "" &
"                 </tr>" &
 "" &
  "" &
"  <tr>" &
"    <td colspan='4' class='tableRowSubHeader'  align='left'>" &
"    Demographics" &
"    </td>" &
"  </tr>" &
"" &
"        <tr>" &
"            <td>" &
"            <b>First Name:</b> " & Request.FirstName &
            "" &
"            </td>" &
"             <td>" &
"              M:" &
Request.Mi &
"            </td>" &
        "" &
"          <td>" &
"            <b>Last Name:</b>" & Request.LastName &
"            </td>" &
"             <td>" &
"            <b>Suffix:</b> " & Request.Suffix &
"            </td>" &
"        </tr>" &
"" &
"     " &
"        <tr>" &
"           <td><b>Title:</b></td>" &
"            <td>" &
Request.Title &
"            </td>" &
"            <td >" &
"           <b>Specialty</b>" &
"            </td>" &
"            <td>" &
Request.Specialty &
"            </td>" &
"        </tr>" &
"" &
"       <tr>" &
            "<td>" &
"                <b>Position Description:</b>" &
"            </td>" &
"            <td colspan = '3'>" &
    Request.UserPositionDesc &
"            </td>" &
"        </tr>" &
"" &
"         <tr>" &
"            <td>" &
"            <b>Entity:</b>" &
"            </td>" &
"            <td>" &
Request.EntityName &
"            </asp:TextBox>" &
"            </td>" &
       "" &
"            <td>" &
"           <b> Department Name:</b>" &
"            </td>" &
"            <td>" &
Request.DepartmentName &
"            </td>" &
"        </tr>" &
"         <tr>" &
"            <td>" &
"           <b> Practice Name:</b>" &
"            </td>       " &
"            <td colspan='3'>" &
"" &
"            </td>" &
"        </tr>" &
"        <tr>" &
"            <td>" &
"              <b>  Non-CKHN Location</b>" &
"            </td>" &
"            <td colspan='3'>" &
 Request.NonHanLocation &
"            </td>" &
"        </tr>" &
"        <tr>" &
"            <td><b>Address 1:</b></td>" &
Request.Address1 &
"            <td><b>City:</b></td>" &
"            <td> " &
Request.City &
"           </td>" &
"        </tr>" &
"         <tr>" &
"            <td><b>Address 2:</b></td>" &
"            <td>" &
Request.Address2 &
"</td>" &
"            <td><b>State:</b> " & Request.State & "</td>" &
"            <td><b>Zip:</b> " & Request.Zip & "</td>" &
"        </tr>" &
       "" &
"            <tr>" &
"            <td>" &
"            <b>Phone Number:</b>" &
"            </td>" &
"            <td>" &
Request.Phone &
"            </td>" &
       "" &
"            <td>" &
"            <b>Pager Number:</b>" &
"            </td>" &
"            <td>" &
Request.Pager &
"            </td>" &
"        </tr>" &
   "" &
 "" &
"        <tr>" &
"           <td><b>Fax:</b></td>" &
"            <td>" &
            "" &
Request.Fax &
"</td>" &
           "" &
           "" &
"             <td>" &
"            <b>Email:</b>" &
"            </td>" &
"            <td>" &
"" &
"            </td>" &
"        </tr>" &
      "" &
"         <tr>" &
"            <td>" &
"            <b>Location</b>" &
"            </td>" &
"            <td>" &
Request.Location &
"            </td>" &
"            <td>" &
"            <b>Building</b>" &
"            </td>" &
"            <td>" &
Request.BuildingName &
"            </td>" &
"        </tr>" &
"           <tr>" &
"                                                <td><b>Floor:</b></td>" &
"                                                <td> " & Request.Floor & "</td>" &
"                                                <td><b>Office:</b></td>" &
"                                                <td>" & Request.OfficeNumber & "</td>" &
"                                    </tr>" &
"<tr>" &
"          " &
"            <td><b>Vendor Name</b></td>" &
"            <td colspan='3'>" & Request.VendorName & "</td>" &
"        </tr>" &
"<tr>" &
"            <td>" &
"            <b>Start Date:</b>" &
"            </td>" &
"            <td>" &
Request.StartDate &
"            </td>" &
       "" &
"            <td>" &
"            <b>End Date</b>:" &
"            </td>" &
"            <td>" &
Request.EndDate &
"            </td>" &
"          </tr>" &
"<tr>" &
"            <td>" &
"            <b>Group Name</b>" &
"" &
"            </td>" &
"            <td>" &
Request.GroupName &
"            </td>" &
"            <td>" &
"               <b> Share Drive</b>" &
"            </td>" &
"            <td >" &
Request.ShareDrive &
"            </td>" &
"        </tr>" &
"<tr>" &
"            <td><b>TCL for eCare </b></td>" &
"            <td>" & Request.TCL & "</td>" &
"            <td><b>Model After </b>:</td>" &
"            <td>" & Request.ModelAfter & "</td>" &
"        </tr>" &
"" &
"<tr>" &
"            <td colspan = '4' align = 'center'>" &
"            <b>Additional Comments: </b> " &
"            </td>" &
        "" &
"        </tr>" &
"<tr>" &
"            <td colspan = '4'>" &
Request.RequestDesc &
            "" &
"            </td>" &
        "" &
"        </tr>" &
"        </table>"


        Return sReturn
    End Function
End Class