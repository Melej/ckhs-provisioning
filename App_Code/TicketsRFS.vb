﻿Imports Microsoft.VisualBasic
Imports App_Code

Public Class TicketsRFS
    Private _Groups As New List(Of CurrentTickectRFSReportItems)
    Private _JustReport As New List(Of CurrentTickectRFSReportItems)
    Private _NoTechGroup As New List(Of CurrentTickectRFSReportItems)

    Private _dtTickets As New DataTable
    Private _myqueue As List(Of CurrentTickectRFSReportItems)

    Public Sub New()


    End Sub

    Public Sub New(ByVal sStartDate As String, ByVal sEndDate As String, sGroup As String, ByVal sTicketByVal As String, ByVal sClose As String, ByVal sConn As String, ByVal sPriority As String, ByVal sTechnum As String)
        Dim _DataConn As String
        _DataConn = sConn

        Dim sprocedures As String = ""

        If sClose = "Projects" Then


            sprocedures = "SelCurrentprojects"



        Else
            sprocedures = "SelCurrentTicketsAndRFS"


        End If

        Dim thisData As New CommandsSqlAndOleDb(sprocedures, _DataConn)
        thisData.AddSqlProcParameter("@startDate", sStartDate, SqlDbType.NVarChar)
        thisData.AddSqlProcParameter("@endDate", sEndDate, SqlDbType.NVarChar)
        thisData.AddSqlProcParameter("@group_num", sGroup, SqlDbType.NVarChar)
        thisData.AddSqlProcParameter("@ticket_rfs", sClose, SqlDbType.NVarChar)
        thisData.AddSqlProcParameter("@close", sTicketByVal, SqlDbType.NVarChar)
        thisData.AddSqlProcParameter("@P1Only", sPriority, SqlDbType.NVarChar)
        thisData.AddSqlProcParameter("@technum", sTechnum, SqlDbType.NVarChar)





        _dtTickets = thisData.GetSqlDataTable()


        If _dtTickets IsNot Nothing Then

            For Each drSql As DataRow In _dtTickets.Rows

                Dim TicketRfs As New CurrentTickectRFSReportItems()


                TicketRfs.request_num = IIf(IsDBNull(drSql("request_num")), Nothing, drSql("request_num"))

                TicketRfs.clientName = IIf(IsDBNull(drSql("clientName")), Nothing, drSql("clientName"))
                TicketRfs.entry_date = IIf(IsDBNull(drSql("entry_date")), Nothing, drSql("entry_date"))

                TicketRfs.close_date = IIf(IsDBNull(drSql("close_date")), Nothing, drSql("close_date"))

                TicketRfs.request_type = IIf(IsDBNull(drSql("request_type")), Nothing, drSql("request_type"))

                TicketRfs.category_cd = IIf(IsDBNull(drSql("category_cd")), Nothing, drSql("category_cd"))
                TicketRfs.type_cd = IIf(IsDBNull(drSql("type_cd")), Nothing, drSql("type_cd"))

                TicketRfs.priority = IIf(IsDBNull(drSql("priority")), Nothing, drSql("priority"))
                TicketRfs.GroupNum = IIf(IsDBNull(drSql("GroupNum")), Nothing, drSql("GroupNum"))


                TicketRfs.GroupName = IIf(IsDBNull(drSql("GroupName")), Nothing, drSql("GroupName"))
                'GroupNum
                TicketRfs.facility_name = IIf(IsDBNull(drSql("facility_name")), Nothing, drSql("facility_name"))

                TicketRfs.TechName = IIf(IsDBNull(drSql("TechName")), Nothing, drSql("TechName"))
                TicketRfs.status_desc = IIf(IsDBNull(drSql("status_desc")), Nothing, drSql("status_desc"))

                TicketRfs.bDesc = IIf(IsDBNull(drSql("bDesc")), Nothing, drSql("bDesc"))

                TicketRfs.LevelStatus = IIf(IsDBNull(drSql("LevelStatus")), Nothing, drSql("LevelStatus"))

                _JustReport.Add(TicketRfs)


            Next


        End If
    End Sub

    Public Sub GetNoTechQueue(ByRef sLogin As String, ByRef sConn As String)


        Dim _DataConn As String
        _DataConn = sConn



        Dim thisData As New CommandsSqlAndOleDb("SelGroupNoTechTicketsAndRFS", _DataConn)

        _dtTickets = thisData.GetSqlDataTable()


        If _dtTickets IsNot Nothing Then

            For Each drSql As DataRow In _dtTickets.Rows

                Dim TicketRfs As New CurrentTickectRFSReportItems()


                TicketRfs.request_num = IIf(IsDBNull(drSql("request_num")), Nothing, drSql("request_num"))

                TicketRfs.clientName = IIf(IsDBNull(drSql("clientName")), Nothing, drSql("clientName"))
                TicketRfs.entry_date = IIf(IsDBNull(drSql("entry_date")), Nothing, drSql("entry_date"))

                TicketRfs.request_type = IIf(IsDBNull(drSql("request_type")), Nothing, drSql("request_type"))

                TicketRfs.category_cd = IIf(IsDBNull(drSql("category_cd")), Nothing, drSql("category_cd"))
                'TicketRfs.type_cd = IIf(IsDBNull(drSql("type_cd")), Nothing, drSql("type_cd"))

                TicketRfs.priority = IIf(IsDBNull(drSql("priority")), Nothing, drSql("priority"))
                TicketRfs.GroupNum = IIf(IsDBNull(drSql("GroupNum")), Nothing, drSql("GroupNum"))


                TicketRfs.GroupName = IIf(IsDBNull(drSql("GroupName")), Nothing, drSql("GroupName"))

                TicketRfs.TimeInQueue = IIf(IsDBNull(drSql("TimeInQueue")), Nothing, drSql("TimeInQueue"))


                TicketRfs.facility_name = IIf(IsDBNull(drSql("facility_name")), Nothing, drSql("facility_name"))
                TicketRfs.status_desc = IIf(IsDBNull(drSql("status_desc")), Nothing, drSql("status_desc"))

                TicketRfs.bDesc = IIf(IsDBNull(drSql("bDesc")), Nothing, drSql("bDesc"))

                TicketRfs.OrderbyNum = IIf(IsDBNull(drSql("OrderbyNum")), Nothing, drSql("OrderbyNum"))


                _NoTechGroup.Add(TicketRfs)


            Next


        End If

    End Sub
    Public Sub GetMyQueue(ByRef sLogin As String, ByRef sConn As String)


        Dim _DataConn As String
        _DataConn = sConn



        Dim thisData As New CommandsSqlAndOleDb("selMyQueue", _DataConn)
        thisData.AddSqlProcParameter("@nt_login", sLogin, SqlDbType.NVarChar)

        _dtTickets = thisData.GetSqlDataTable()


        If _dtTickets IsNot Nothing Then

            For Each drSql As DataRow In _dtTickets.Rows

                Dim TicketRfs As New CurrentTickectRFSReportItems()


                TicketRfs.request_num = IIf(IsDBNull(drSql("request_num")), Nothing, drSql("request_num"))

                TicketRfs.clientName = IIf(IsDBNull(drSql("clientName")), Nothing, drSql("clientName"))
                TicketRfs.entry_date = IIf(IsDBNull(drSql("entry_date")), Nothing, drSql("entry_date"))

                'TicketRfs.close_date = IIf(IsDBNull(drSql("close_date")), Nothing, drSql("close_date"))

                TicketRfs.request_type = IIf(IsDBNull(drSql("request_type")), Nothing, drSql("request_type"))

                TicketRfs.category_cd = IIf(IsDBNull(drSql("category_cd")), Nothing, drSql("category_cd"))
                'TicketRfs.type_cd = IIf(IsDBNull(drSql("type_cd")), Nothing, drSql("type_cd"))

                TicketRfs.priority = IIf(IsDBNull(drSql("priority")), Nothing, drSql("priority"))
                'TicketRfs.GroupNum = IIf(IsDBNull(drSql("GroupNum")), Nothing, drSql("GroupNum"))


                TicketRfs.TimeInQueue = IIf(IsDBNull(drSql("TimeInQueue")), Nothing, drSql("TimeInQueue"))


                TicketRfs.facility_name = IIf(IsDBNull(drSql("facility_name")), Nothing, drSql("facility_name"))
                TicketRfs.status_desc = IIf(IsDBNull(drSql("status_desc")), Nothing, drSql("status_desc"))

                TicketRfs.bDesc = IIf(IsDBNull(drSql("bDesc")), Nothing, drSql("bDesc"))

                TicketRfs.OrderbyNum = IIf(IsDBNull(drSql("OrderbyNum")), Nothing, drSql("OrderbyNum"))


                _Groups.Add(TicketRfs)


            Next


        End If

    End Sub

    Public Sub GetUnAssignQueue(ByRef sLogin As String, ByRef sConn As String)


        Dim _DataConn As String
        _DataConn = sConn



        Dim thisData As New CommandsSqlAndOleDb("SelUnassignedGroupTicketsRfsV7", _DataConn)
        ' thisData.AddSqlProcParameter("@nt_login", sLogin, SqlDbType.NVarChar)

        _dtTickets = thisData.GetSqlDataTable()


        If _dtTickets IsNot Nothing Then

            For Each drSql As DataRow In _dtTickets.Rows

                Dim TicketRfs As New CurrentTickectRFSReportItems()


                TicketRfs.request_num = IIf(IsDBNull(drSql("request_num")), Nothing, drSql("request_num"))

                TicketRfs.clientName = IIf(IsDBNull(drSql("clientName")), Nothing, drSql("clientName"))
                TicketRfs.entry_date = IIf(IsDBNull(drSql("entry_date")), Nothing, drSql("entry_date"))

                'TicketRfs.close_date = IIf(IsDBNull(drSql("close_date")), Nothing, drSql("close_date"))

                TicketRfs.request_type = IIf(IsDBNull(drSql("request_type")), Nothing, drSql("request_type"))

                TicketRfs.category_cd = IIf(IsDBNull(drSql("category_cd")), Nothing, drSql("category_cd"))
                'TicketRfs.type_cd = IIf(IsDBNull(drSql("type_cd")), Nothing, drSql("type_cd"))

                TicketRfs.priority = IIf(IsDBNull(drSql("priority")), Nothing, drSql("priority"))
                'TicketRfs.GroupNum = IIf(IsDBNull(drSql("GroupNum")), Nothing, drSql("GroupNum"))


                TicketRfs.TimeInQueue = IIf(IsDBNull(drSql("TimeInQueue")), Nothing, drSql("TimeInQueue"))


                TicketRfs.facility_name = IIf(IsDBNull(drSql("facility_name")), Nothing, drSql("facility_name"))
                TicketRfs.status_desc = IIf(IsDBNull(drSql("status_desc")), Nothing, drSql("status_desc"))

                TicketRfs.bDesc = IIf(IsDBNull(drSql("bDesc")), Nothing, drSql("bDesc"))

                TicketRfs.OrderbyNum = IIf(IsDBNull(drSql("OrderbyNum")), Nothing, drSql("OrderbyNum"))



                _Groups.Add(TicketRfs)


            Next


        End If

    End Sub

    Public Sub GetAllMyGroupQueue(ByRef sLogin As String, ByRef sConn As String)


        Dim _DataConn As String
        _DataConn = sConn



        Dim thisData As New CommandsSqlAndOleDb("selAllMyGroups", _DataConn)
        thisData.AddSqlProcParameter("@nt_login", sLogin, SqlDbType.NVarChar)

        _dtTickets = thisData.GetSqlDataTable()


        If _dtTickets IsNot Nothing Then

            For Each drSql As DataRow In _dtTickets.Rows

                Dim TicketRfs As New CurrentTickectRFSReportItems()


                TicketRfs.request_num = IIf(IsDBNull(drSql("request_num")), Nothing, drSql("request_num"))

                TicketRfs.clientName = IIf(IsDBNull(drSql("clientName")), Nothing, drSql("clientName"))
                TicketRfs.entry_date = IIf(IsDBNull(drSql("entry_date")), Nothing, drSql("entry_date"))

                'TicketRfs.close_date = IIf(IsDBNull(drSql("close_date")), Nothing, drSql("close_date"))

                TicketRfs.request_type = IIf(IsDBNull(drSql("request_type")), Nothing, drSql("request_type"))

                TicketRfs.category_cd = IIf(IsDBNull(drSql("category_cd")), Nothing, drSql("category_cd"))
                'TicketRfs.type_cd = IIf(IsDBNull(drSql("type_cd")), Nothing, drSql("type_cd"))

                TicketRfs.priority = IIf(IsDBNull(drSql("priority")), Nothing, drSql("priority"))
                'TicketRfs.GroupNum = IIf(IsDBNull(drSql("GroupNum")), Nothing, drSql("GroupNum"))


                TicketRfs.TimeInQueue = IIf(IsDBNull(drSql("TimeInQueue")), Nothing, drSql("TimeInQueue"))


                TicketRfs.facility_name = IIf(IsDBNull(drSql("facility_name")), Nothing, drSql("facility_name"))
                TicketRfs.status_desc = IIf(IsDBNull(drSql("status_desc")), Nothing, drSql("status_desc"))

                TicketRfs.bDesc = IIf(IsDBNull(drSql("bDesc")), Nothing, drSql("bDesc"))

                TicketRfs.OrderbyNum = IIf(IsDBNull(drSql("OrderbyNum")), Nothing, drSql("OrderbyNum"))


                _Groups.Add(TicketRfs)


            Next


        End If

    End Sub
    Public Sub GetP1Queue(ByRef sSelecttype As String, ByRef sConn As String)


        Dim _DataConn As String
        _DataConn = sConn



        Dim thisData As New CommandsSqlAndOleDb("SelPriorityTicketsRfsV8", _DataConn)
        thisData.AddSqlProcParameter("@SelectType", sSelecttype, SqlDbType.NVarChar)

        _dtTickets = thisData.GetSqlDataTable()


        If _dtTickets IsNot Nothing Then

            For Each drSql As DataRow In _dtTickets.Rows

                Dim TicketRfs As New CurrentTickectRFSReportItems()


                TicketRfs.request_num = IIf(IsDBNull(drSql("request_num")), Nothing, drSql("request_num"))

                TicketRfs.clientName = IIf(IsDBNull(drSql("clientName")), Nothing, drSql("clientName"))
                TicketRfs.entry_date = IIf(IsDBNull(drSql("entry_date")), Nothing, drSql("entry_date"))

                'TicketRfs.close_date = IIf(IsDBNull(drSql("close_date")), Nothing, drSql("close_date"))

                TicketRfs.request_type = IIf(IsDBNull(drSql("request_type")), Nothing, drSql("request_type"))

                TicketRfs.category_cd = IIf(IsDBNull(drSql("category_cd")), Nothing, drSql("category_cd"))
                'TicketRfs.type_cd = IIf(IsDBNull(drSql("type_cd")), Nothing, drSql("type_cd"))

                TicketRfs.priority = IIf(IsDBNull(drSql("priority")), Nothing, drSql("priority"))
                'TicketRfs.GroupNum = IIf(IsDBNull(drSql("GroupNum")), Nothing, drSql("GroupNum"))

                TicketRfs.facility_name = IIf(IsDBNull(drSql("facility_name")), Nothing, drSql("facility_name"))

                TicketRfs.TimeInQueue = IIf(IsDBNull(drSql("TimeInQueue")), Nothing, drSql("TimeInQueue"))


                TicketRfs.facility_name = IIf(IsDBNull(drSql("facility_name")), Nothing, drSql("facility_name"))
                TicketRfs.status_desc = IIf(IsDBNull(drSql("status_desc")), Nothing, drSql("status_desc"))

                TicketRfs.bDesc = IIf(IsDBNull(drSql("bDesc")), Nothing, drSql("bDesc"))

                TicketRfs.OrderbyNum = IIf(IsDBNull(drSql("Orderby")), Nothing, drSql("Orderby"))


                _Groups.Add(TicketRfs)


            Next


        End If

    End Sub

    Public Sub GetAllMyQueue(ByVal sStartDate As String, ByVal sEndDate As String, sGroup As String, ByVal sTicketByVal As String, ByVal sClose As String, ByVal sConn As String, ByVal sPriority As String)


        Dim _DataConn As String
        _DataConn = sConn



        Dim thisData As New CommandsSqlAndOleDb("SelCurrentTicketsAndRFS", _DataConn)
        thisData.AddSqlProcParameter("@startDate", sStartDate, SqlDbType.NVarChar)
        thisData.AddSqlProcParameter("@endDate", sEndDate, SqlDbType.NVarChar)
        thisData.AddSqlProcParameter("@group_num", sGroup, SqlDbType.NVarChar)
        thisData.AddSqlProcParameter("@ticket_rfs", sClose, SqlDbType.NVarChar)
        thisData.AddSqlProcParameter("@close", sTicketByVal, SqlDbType.NVarChar)
        thisData.AddSqlProcParameter("@P1Only", sPriority, SqlDbType.NVarChar)

        _dtTickets = thisData.GetSqlDataTable()


        If _dtTickets IsNot Nothing Then

            For Each drSql As DataRow In _dtTickets.Rows

                Dim TicketRfs As New CurrentTickectRFSReportItems()


                TicketRfs.request_num = IIf(IsDBNull(drSql("request_num")), Nothing, drSql("request_num"))

                TicketRfs.clientName = IIf(IsDBNull(drSql("clientName")), Nothing, drSql("clientName"))
                TicketRfs.entry_date = IIf(IsDBNull(drSql("entry_date")), Nothing, drSql("entry_date"))

                'TicketRfs.close_date = IIf(IsDBNull(drSql("close_date")), Nothing, drSql("close_date"))

                TicketRfs.request_type = IIf(IsDBNull(drSql("request_type")), Nothing, drSql("request_type"))

                TicketRfs.category_cd = IIf(IsDBNull(drSql("category_cd")), Nothing, drSql("category_cd"))
                'TicketRfs.type_cd = IIf(IsDBNull(drSql("type_cd")), Nothing, drSql("type_cd"))

                TicketRfs.priority = IIf(IsDBNull(drSql("priority")), Nothing, drSql("priority"))
                'TicketRfs.GroupNum = IIf(IsDBNull(drSql("GroupNum")), Nothing, drSql("GroupNum"))


                TicketRfs.TimeInQueue = IIf(IsDBNull(drSql("TimeInQueue")), Nothing, drSql("TimeInQueue"))


                TicketRfs.facility_name = IIf(IsDBNull(drSql("facility_name")), Nothing, drSql("facility_name"))
                TicketRfs.status_desc = IIf(IsDBNull(drSql("status_desc")), Nothing, drSql("status_desc"))

                TicketRfs.bDesc = IIf(IsDBNull(drSql("bDesc")), Nothing, drSql("bDesc"))

                'TicketRfs.OrderbyNum = IIf(IsDBNull(drSql("OrderbyNum")), Nothing, drSql("OrderbyNum"))


                _Groups.Add(TicketRfs)


            Next


        End If

    End Sub

    Public Function GetAllRequestByGroup(ByVal iGroup As Integer) As List(Of CurrentTickectRFSReportItems)

        Dim requestsByGroup = (From req As CurrentTickectRFSReportItems In _JustReport _
                              Where req.GroupNum = iGroup
                       Select req).ToList

        Return requestsByGroup

    End Function
    Public Function GetNoTechRequestByGroup(ByVal iGroup As Integer) As List(Of CurrentTickectRFSReportItems)

        Dim requestsByGroup = (From req As CurrentTickectRFSReportItems In _NoTechGroup _
                              Where req.GroupNum = iGroup
                       Select req).ToList

        Return requestsByGroup

    End Function

    Public ReadOnly Property NoTechGroup As List(Of CurrentTickectRFSReportItems)
        Get
            Return _NoTechGroup

        End Get
    End Property


    Public ReadOnly Property AllMyQueue As List(Of CurrentTickectRFSReportItems)
        Get
            Return _Groups

        End Get
    End Property
End Class


Public Class CurrentTickectRFSReportItems

    Public Property request_num As Integer
    Public Property clientName As String
    Public Property entry_date As String
    Public Property close_date As String
    Public Property request_type As String
    Public Property category_cd As String
    Public Property type_cd As String
    Public Property priority As Integer
    Public Property GroupNum As Integer
    Public Property GroupName As String
    Public Property TechName As String
    Public Property status_desc As String
    Public Property bDesc As String
    Public Property LevelStatus As String

    Public Property OrderbyNum As Integer
    Public Property TimeInQueue As String
    Public Property facility_name As String


    'client_num,
    'clientName,
    'entry_date,
    'close_date,
    'request_type,
    'category_cd,
    'type_cd,
    '[priority],
    'current_group_num,
    'GroupName,
    'CurrentTechName,

    'item_cd,
    'group_assign_time,
    'assigned_group_num,
    'tech_assign_time,
    'assigned_tech_num,
    'current_tech_num,
    'acknowledge_time,
    'current_ack_time,
    'asset_tag,
    '[platform],
    'resolution_cd,
    'entry_tech_num,
    'department_num,
    'facility_cd,
    'building_cd,
    'office,
    '[floor],
    'address_1,
    'city,
    '[state],
    'zip,
    'alternate_contact_name,
    'alternate_contact_phone,
    'short_desc,
    'proxy_on_pc,
    'ip_address,
    'facility_name,
    'AssignedTechName 	

End Class

