Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Collections.Generic
Imports App_Code
Imports System.Web.SessionState

'<WebService(Namespace:="http://tempuri.org/")> _
'<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
'<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
'<System.Web.Script.Services.ScriptService()> _
' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
<System.Web.Script.Services.ScriptService()> _
    <WebService(Namespace:="http://tempuri.org/")> _
    <WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Public Class SimpleService
    Inherits System.Web.Services.WebService
    Structure FieldValue
        Public FName As String
        Public FValue As String
    End Structure
    <WebMethod()> _
    Public Function SayHello(ByVal Name As String) As String
        Return "Hello : " + Name
    End Function
    '<WebMethod()> _
    'Public Function GetThisAssociateInfo(ByVal CaseNumKey As String, ByVal AssSeqNumKey As String, ByVal sLoginID As String, ByVal sConn As String) As String
    '    '  Dim ans As Integer = 0
    '    '  Dim divider As Integer = 0
    '    Try
    '        'make error   ans = contextKey / divider
    '        If CaseNumKey = "" Or CaseNumKey = "0" Then
    '            Return "<table bgcolor='azure'><tr><th colspan='1'>No  Information Available</th></tr></table>"
    '            Exit Function
    '        End If
    '        Dim dt As New DataTable
    '        Dim i As Integer = 0 ' bgcolor='azure'
    '        Dim iTblColumnsPerRow As Integer = 6
    '        Dim sReturn As String = "<table bgcolor='azure' style='font-size:xxsmall'><tr><th colspan='1'>No  Information Available</th></tr></table>"
    '        Dim StartRow As String = "<tr style='font-size: x-small; color: Navy; background-color: azure'><td>"
    '        Dim StartCell As String = "<td>"
    '        Dim StopCell As String = "</td>"
    '        Dim StopRow As String = "</td></tr>"
    '        Dim sImage As String = " <asp:ImageButton ID='imgBtnTrans' runat='server' Height='20px' ImageUrl='/~Images/Transporting.GIF'  Width='20px' />"
    '        Dim thisdata As New CommandsSqlAndOleDb("SelCaseAssociatesByCaseNumAndAssSeqNum", sConn)
    '        thisdata.AddSqlProcParameter("@CaseNum", CaseNumKey, SqlDbType.Int, 4)
    '        thisdata.AddSqlProcParameter("@AssocSeqNum", AssSeqNumKey, SqlDbType.Int, 4)
    '        'thisdata.AddSqlProcParameter("@NtLogin", sLoginID, SqlDbType.NVarChar, 25)
    '        dt = thisdata.GetSqlDataTable
    '        If dt.Rows.Count > 0 Then 'Main table text-decoration: underline;
    '            sReturn = "<table bgcolor='azure' cellpadding='5' >" & _
    '                      "<tr style='color:green; background-color: lightyellow'><th colspan='" & iTblColumnsPerRow & "' ><center>Associate Information" _
    '                      & "</center></th></tr>"
    '            '  & "<tr style='color:navy ;font-size: small; background-color: azure'><th colspan='4'>Associate Information</th></tr>"
    '            '''''works sReturn &= MakehtmlTableRows(dt, iTblColumnsPerRow, 2)
    '            sReturn &= MakehtmlTableRowsNamesOverValues(dt, iTblColumnsPerRow, 2, 20)
    '            ''For i = 2 To dt.Columns.Count - 1 Step 2
    '            ''    If Not IsDBNull(dt.Rows(0).Item(i)) Then
    '            ''        sReturn &= StartRow & dt.Columns(i).ColumnName & ": " & dt.Rows(0).Item(i) & StopCell
    '            ''    Else
    '            ''        sReturn &= StartRow & StopCell & StartCell & StopCell
    '            ''    End If
    '            ''    If i + 1 >= dt.Columns.Count Then ' dont use 1+
    '            ''        sReturn &= StartCell & StopCell & StartCell & StopRow
    '            ''    Else 'ok to use +1
    '            ''        If Not IsDBNull(dt.Rows(0).Item(i + 1)) Then
    '            ''            sReturn &= StartRow & dt.Columns(i + 1).ColumnName & ": " & dt.Rows(0).Item(i + 1) & StopRow
    '            ''        End If
    '            ''    End If
    '            ''    'sReturn &= StartCell & dt.Rows(i).Item("PatientFirstName") & StopCell
    '            ''    'sReturn &= StartCell & dt.Rows(i).Item("PatientLastName") & StopCell
    '            ''    'sReturn &= StartCell & dt.Rows(i).Item("EnterDate") & StopCell
    '            ''    'sReturn &= StartCell & " Hours Waiting " & dt.Rows(i).Item("HoursWaiting") & StopRow
    '            ''    'sReturn &= StartCell & " Hours Waiting " & dt.Rows(i).Item("AttendingPhysName") & StopRow

    '            ''Next
    '            '' sReturn &= "</tr></table>"
    '            'sReturn &= "</tr>"
    '            'For i = 0 To dt.Rows.Count - 1
    '            '    sReturn &= "<tr><td colspan='5'>" & dt.Rows(i).Item("Comments") & StopRow

    '            'Next
    '            sReturn &= "<tr><td colspan='" & iTblColumnsPerRow & "'>" & "Empty Fields are not shown" & StopRow

    '            sReturn &= "</table>"
    '        End If
    '        If sReturn = "" Then

    '        Else
    '            Return sReturn
    '        End If
    '    Catch e As Exception
    '        'Dim uex As New ASPUnhandledException2005.Handler()
    '        'uex.HandleException(e, True, True, False)
    '        'Return "Error No Data PatientBedNum= " & contextKey & " Conn " & sConn

    '    End Try

    'End Function
    Public Function MakehtmlTableRowsNamesOverValues(ByRef dt As DataTable, ByVal iColumns As Integer, ByVal iFirstColumnsToProcess As Integer, ByVal iLastColToProcessZeroAll As Integer) As String
        Dim sReturn As String = ""
        Dim StartRow As String = "<tr style='font-size: xx-small; color: Navy; background-color: azure'><td align='left'>" '"<tr></td>" '
        Dim StartRowOnly As String = "<tr style='font-size: xx-small; color: Navy; background-color: azure'>" '"<tr></td>" '
        Dim StartAltRowOnly As String = "<tr style='font-size: xx-small; color: Navy; background-color:ghostWhite'>" '"<tr></td>" '

        Dim StartCellLeft As String = "<td align='left' cellpadding='5'>" '"<td>" '
        Dim StartCellRight As String = "<td align='left' cellpadding='5'>" '"<td>" '
        Dim StopCell As String = "</td>"
        Dim StopRow As String = "</td></tr>"
        Dim EmptyCell As String = "<td><td/>"
        Dim sImage As String = ""
        Dim i As Integer = 0
        Dim iRemainder As Integer = 0
        Dim dtFiltered As New DataTable
        Dim iRowsToMake As Double = 0.0
        '  Dim FilteredDic As New Dictionary(Of String, String)
        Dim FilteredDic(100, 100)
        '  Dim FieldValueList As FieldValue()
        Dim sKey As String = ""
        Dim sValue As String = ""
        Dim j As Integer = 0
        Dim icol As Integer = 0
        Dim irow As Integer = 0
        Dim iFieldsPerRow As Integer = iColumns / 2
        Dim sTestString As String = ""
        Dim sTemp As String = ""
        ' Dim iLastColToProcess As Integer
        If iLastColToProcessZeroAll = 0 Then iLastColToProcessZeroAll = dt.Columns.Count - 1

        For i = iFirstColumnsToProcess To iLastColToProcessZeroAll

            If Not IsDBNull(dt.Rows(0).Item(i)) And dt.Rows(0).Item(i).ToString() <> "" Then

                'FilteredDic.Add(dt.Columns(i).ColumnName, dt.Rows(0).Item(i).ToString())

                FilteredDic(j, 0) = dt.Columns(i).ColumnName
                sTestString &= FilteredDic(j, 0)
                sTemp = dt.Rows(0).Item(i).ToString()

                FilteredDic(j, 1) = sTemp ' dt.Rows(0).Item(i).ToString() ' & ","

                sTestString &= ":" & FilteredDic(j, 1)
                j += 1 'num fields  2 td'd for each name and vlaue
            End If
        Next
        j = j - 1
        iRowsToMake = ((j + 1) - iFirstColumnsToProcess) / iColumns
        'iRowsToMake = FormatNumber(iRowsToMake, 1)
        If iRowsToMake < 1 Then
            iRowsToMake = 0.0
        Else
            Dim RowResult As Double
            RowResult = ((j + 1) - iFirstColumnsToProcess) Mod iColumns
            If RowResult > 0.0 Then
                iRowsToMake = iRowsToMake + 1
            End If
            iRowsToMake = FormatNumber(iRowsToMake, 0)
        End If

        ' iRemainder = ((j) - iFirstColumnsToHide) - iRowsToMake
        iRemainder = ((j - iFirstColumnsToProcess)) - ((iRowsToMake) * iColumns)
        Dim iPos As Integer = 0
        If (iRowsToMake) >= 1 Then 'Main table text-decoration: underline;
            For irow = 0 To iRowsToMake - 1 ' dt.Columns.Count - 1 Step 2
                '=============================Header Loop
                sReturn &= "<tr bgcolor='azure' style='font-weight:bold'>"
                For icol = 0 To iColumns - 1 'iFieldsPerRow - 1 'two td 'd per field
                    ' If irow Mod 2 = 0 Then 'fieldname 
                    iPos = (irow * iColumns) + icol 'right for name  left for value
                    sReturn &= StartCellRight & FilteredDic(iPos, 0) & ": " & StopCell '& StartCellLeft & FilteredDic(iPos + 1, 0) & ": " & StopCell
                    ' IfReplace(FilteredDic(iPos, 0), "'", "\'")
                Next icol
                sReturn &= "</tr>" 'StopRow
                '=======================Field Value Loop
                sReturn &= "<tr  bgcolor='ghostwhite'>"
                For icol = 0 To iColumns - 1 'iFieldsPerRow - 1 'two td 'd per field
                    ' If irow Mod 2 = 0 Then 'fieldname 
                    iPos = (irow * iColumns) + icol 'right for name  left for value
                    sReturn &= StartCellRight & FilteredDic(iPos, 1) & StopCell '& StartCellLeft & FilteredDic(iPos + 1, 0) & ": " & StopCell
                    ' If
                Next icol
                sReturn &= "</tr>" 'StopRow
            Next irow
        End If
        '======Remainder=========================
        Dim sRemainder As String = ""
        If iRemainder > 0 Then
            sRemainder &= "<tr  bgcolor='azure' style='font-weight:bold'>" 'StartRow Header
            Dim iRemainderStart = iRowsToMake * iColumns ' iColumns
            'Header loop
            For i = iRemainderStart To (iRemainderStart + iRemainder)
                sRemainder &= StartCellRight & FilteredDic(i, 0) & ": " & StopCell ' & StartCellLeft & FilteredDic(i, 1)


            Next
            sRemainder &= "<td colspan='" & (iColumns - iRemainder) & "'></td>"
            sRemainder &= "</tr>"

            'DataField loop
            sRemainder &= "<tr  bgcolor='ghostwhite'>" 'StartRow data
            For i = iRemainderStart To (iRemainderStart + iRemainder)
                sRemainder &= StartCellRight & FilteredDic(i, 1) & ": " & StopCell '& StartCellLeft & FilteredDic(i, 1)


            Next
            sRemainder &= "<td colspan='" & (iColumns - iRemainder) & "'></td>"
            sRemainder &= "</tr>"
        End If
        sReturn &= sRemainder

        FilteredDic = Nothing
        MakehtmlTableRowsNamesOverValues = sReturn
    End Function
    Public Function MakehtmlTableRows(ByRef dt As DataTable, ByVal iColumns As Integer, ByVal iFirstColumnsToHide As Integer) As String
        Dim sReturn As String = ""
        Dim StartRow As String = "<tr style='font-size: x-small; color: Navy; background-color: azure'><td align='left'>" '"<tr></td>" '
        Dim StartCellLeft As String = "<td align='left' cellpadding='5'>" '"<td>" '
        Dim StartCellRight As String = "<td align='Right' cellpadding='5'>" '"<td>" '
        Dim StopCell As String = "</td>"
        Dim StopRow As String = "</td></tr>"
        Dim EmptyCell As String = "<td><td/>"
        Dim sImage As String = ""
        Dim i As Integer = 0
        Dim iRemainder As Integer = 0
        Dim dtFiltered As New DataTable
        Dim iRowsToMake As Integer = 0
        '  Dim FilteredDic As New Dictionary(Of String, String)
        Dim FilteredDic(100, 100)
        Dim sKey As String = ""
        Dim sValue As String = ""
        Dim j As Integer = 0
        Dim icol As Integer = 0
        Dim irow As Integer = 0
        Dim iFieldsPerRow As Integer = iColumns / 2
        For i = (iFirstColumnsToHide) To dt.Columns.Count - 1

            If Not IsDBNull(dt.Rows(0).Item(i)) And dt.Rows(0).Item(i).ToString() <> "" Then

                'FilteredDic.Add(dt.Columns(i).ColumnName, dt.Rows(0).Item(i).ToString())
                FilteredDic(j, 0) = dt.Columns(i).ColumnName
                FilteredDic(j, 1) = dt.Rows(0).Item(i).ToString()
                j += 1 'num fields  2 td'd for each name and vlaue
            End If
        Next
        iRowsToMake = (((j) - iFirstColumnsToHide) * 2) / iColumns
        ' iRemainder = ((j) - iFirstColumnsToHide) - iRowsToMake
        iRemainder = ((j - iFirstColumnsToHide) * 2) - (iRowsToMake * iColumns)
        If (iRowsToMake) > 0 Then 'Main table text-decoration: underline;
            For irow = 0 To iRowsToMake - 1 ' dt.Columns.Count - 1 Step 2
                sReturn &= "<tr>"
                For icol = 0 To iFieldsPerRow - 1 'two td 'd per field
                    'If col Mod 2 = 0 Then 'fieldname              'right for name  left for value
                    sReturn &= StartCellRight & FilteredDic((irow * iFieldsPerRow) + icol, 0) & ": " & StopCell & StartCellLeft & FilteredDic((irow * iFieldsPerRow) + icol, 1) '& StopCell
                    ' Else 'fieldvalue
                    'end if
                Next icol
                sReturn &= StopRow
            Next irow

            If iRemainder > 0 Then
                sReturn &= "<tr>" 'StartRow
                Dim iRemainderStart = iRowsToMake * iFieldsPerRow ' iColumns
                For i = iRemainderStart To (iRemainderStart + iRemainder)
                    sReturn &= StartCellRight & FilteredDic(i, 0) & ": " & StopCell & StartCellLeft & FilteredDic(i, 1)


                Next
                sReturn &= "<td colspan='" & (iFieldsPerRow - iRemainder) & "'></td>"
            End If
            sReturn &= StopRow
        End If
        FilteredDic = Nothing
        MakehtmlTableRows = sReturn
    End Function
    <WebMethod()> _
    Public Function GetPatientTransports(ByVal contextKey As String) As String
        If contextKey = "" Or contextKey = "0" Then
            Return "<table bgcolor='azure'><tr><th colspan='1'>No Transport Information Available</th></tr></table>"
            ' Exit Function
        End If
        Dim dt As New DataTable
        Dim i As Integer = 0 ' bgcolor='azure'
        Dim sReturn As String = "<table><tr><th colspan='1'>No Transport Information Available</th></tr></table>"
        Dim StartRow As String = "<tr><td>"
        Dim StartCell As String = "<td>"
        Dim StopCell As String = "</td>"
        Dim StopRow As String = "</td></tr>"
        Dim thisdata As New CommandsSqlAndOleDb("selSchedulebyPatientNum")
        thisdata.AddSqlProcParameter("@PatientBedNum", contextKey, SqlDbType.NVarChar, 16)
        ' thisdata.AddSqlProcParameter("@schedulenum", e.Row.Cells(9).Text, SqlDbType.Int, 4)
        dt = thisdata.GetSqlDataTable
        If dt.Rows.Count > 0 Then 'Main tablesTemp = sTemp.Replace("'", "\'")
            sReturn = "<table bgcolor='azure'><tr><th colspan='1'>Sch</th><th colspan='1'>From</th><th colspan='1'>To</th><th colspan='1'>When</th><th colspan='1'>Issued by</th></tr>"
            For i = 0 To dt.Rows.Count - 1
                sReturn &= StartRow & Replace(dt.Rows(i).Item("schedulenum"), "'", "\'") & StopCell
                sReturn &= StartCell & Replace(dt.Rows(i).Item("FromLocationDesc"), "'", "\'") & StopCell
                sReturn &= StartCell & Replace(dt.Rows(i).Item("ToLocationDesc"), "'", "\'") & StopCell
                sReturn &= StartCell & Replace(dt.Rows(i).Item("DateToTransport"), "'", "\'") & StopCell
                sReturn &= StartCell & Replace(dt.Rows(i).Item("UserWhoIssusedTransport"), "'", "\'") & StopRow

            Next
            sReturn &= "</tr>"
            'For i = 0 To dt.Rows.Count - 1
            '    sReturn &= "<tr><td colspan='5'>" & dt.Rows(i).Item("Comments") & StopRow

            'Next
            sReturn &= "</table>"
        End If

        Return sReturn
    End Function

    <WebMethod()> _
    Public Function GetThisCaseInfo(ByVal contextKey As String, ByVal sHospital As String, ByVal sLoginID As String, ByVal sConn As String) As String
        '  Dim ans As Integer = 0
        '  Dim divider As Integer = 0
        Dim sErr As String = ""
        Try
            'make error   ans = contextKey / divider
            If contextKey = "" Or contextKey = "0" Then
                Return "<table bgcolor='azure'><tr><th colspan='1'>No Patient Information Available</th></tr></table>"
                Exit Function
            End If
            Dim iTblColumnsPerRow As Integer = 4
            Dim dt As New DataTable
            Dim i As Integer = 0 ' bgcolor='azure'
            Dim sReturn As String = "<table bgcolor='azure' style='font-size:xxsmall'><tr><th colspan='1'>No Transport Information Available</th></tr></table>"
            Dim StartRow As String = "<tr style='font-size: x-small; color: Navy; background-color: azure'><td>"
            Dim StartCell As String = "<td>"
            Dim StopCell As String = "</td>"
            Dim StopRow As String = "</td></tr>"
            Dim sImage As String = " <asp:ImageButton ID='imgBtnTrans' runat='server' Height='20px' ImageUrl='/~Images/Transporting.GIF'  Width='20px' />"
            Dim sSplit() As String = contextKey.Split(":")
            Dim sCaseNum As String = sSplit(0)
            Dim sMode As String = sSplit(1)

            Dim thisdata As New CommandsSqlAndOleDb("selCaseByCaseNum", sConn)
            'If sMode.ToLower = "tc" Then
            '    thisdata.setCommandText("selTCCaseByCaseNum")
            'End If
            thisdata.AddSqlProcParameter("@CaseNum", sCaseNum, SqlDbType.Int, 4)
            thisdata.AddSqlProcParameter("@NtLogin", sLoginID, SqlDbType.NVarChar, 25)
            thisdata.AddSqlProcParameter("@Hospitalnum", sHospital, SqlDbType.Int, 4)


            dt = thisdata.GetSqlDataTable
            If dt.Rows.Count > 0 Then 'Main table
                sReturn = "<table bgcolor='azure' cellpadding='5' >" & _
                          "<tr style='color:green; text-decoration: underline; background-color: lightyellow'><th colspan='" & iTblColumnsPerRow & "'><center>Case Information" _
                          & sImage & "</center></th></tr>" ' _
                'Create custom columns popup display
                Dim dtnew As New DataTable
                Dim dCol As New DataColumn
                '==0
                dCol = New DataColumn("Patient Name", System.Type.GetType("System.String"))
                dtnew.Columns.Add(dCol)
                '==1
                dCol = New DataColumn("Gender", System.Type.GetType("System.String"))
                dCol.Caption = "Sex"
                dtnew.Columns.Add(dCol)
                '==2
                dCol = New DataColumn("Birth Date", System.Type.GetType("System.String"))
                dCol.Caption = "Born"
                dtnew.Columns.Add(dCol)
                '==3
                dCol = New DataColumn("Room Bed", System.Type.GetType("System.String"))
                dCol.Caption = "Room Bed #"
                dtnew.Columns.Add(dCol)
                '==4
                dCol = New DataColumn("Financial Class", System.Type.GetType("System.String"))
                dtnew.Columns.Add(dCol)
                '==5
                dCol = New DataColumn("Insurance", System.Type.GetType("System.String"))
                dtnew.Columns.Add(dCol)
                '==6
                dCol = New DataColumn("LOS", System.Type.GetType("System.String"))
                ' dCol.Caption = "Room Bed #"
                dtnew.Columns.Add(dCol)
                '==7
                dCol = New DataColumn("Attending Dr.Name", System.Type.GetType("System.String"))
                dCol.Caption = "Attending Dr.Name"
                dtnew.Columns.Add(dCol)
                '==8
                dCol = New DataColumn("Service", System.Type.GetType("System.String"))
                dCol.Caption = "Service"
                dtnew.Columns.Add(dCol)
                '==9
                dCol = New DataColumn("Case Manager", System.Type.GetType("System.String"))
                dCol.Caption = "Case Manager"
                dtnew.Columns.Add(dCol)
                '==10
                dCol = New DataColumn("Social Worker", System.Type.GetType("System.String"))
                dCol.Caption = "Social Worker"
                dtnew.Columns.Add(dCol)
                '==11
                dCol = New DataColumn("Admit Date", System.Type.GetType("System.String"))
                dCol.Caption = "Admit Date"
                dtnew.Columns.Add(dCol)
                '==112
                dCol = New DataColumn("Discharge Date", System.Type.GetType("System.String"))
                dCol.Caption = "Discharge"
                dtnew.Columns.Add(dCol)
                '==13
                dCol = New DataColumn("Isolation", System.Type.GetType("System.String"))
                dCol.Caption = "Isolation"
                dtnew.Columns.Add(dCol)
                '==14
                dCol = New DataColumn("Resistant Org", System.Type.GetType("System.String"))
                dCol.Caption = "ResistantOrg"
                dtnew.Columns.Add(dCol)
                '==15
                dCol = New DataColumn("Privacy", System.Type.GetType("System.String"))
                dCol.Caption = ""
                dtnew.Columns.Add(dCol)
                '==16
                dCol = New DataColumn("PatAcctNumber", System.Type.GetType("System.String"))
                dCol.Caption = ""
                dtnew.Columns.Add(dCol)
                '==17
                dCol = New DataColumn("MedicalRecNo", System.Type.GetType("System.String"))
                dCol.Caption = ""
                dtnew.Columns.Add(dCol)
                '==18 LastDayCovered
                'dCol = New DataColumn("LastDayCovered", System.Type.GetType("System.String"))
                dCol = New DataColumn("Next Review Date", System.Type.GetType("System.String"))
                dCol.Caption = ""
                dtnew.Columns.Add(dCol)
                '==19 LastDayCovered
                dCol = New DataColumn("PCP Physican", System.Type.GetType("System.String"))
                dCol.Caption = ""
                dtnew.Columns.Add(dCol)

                '==20 Entitlement Program
                dCol = New DataColumn("Entitlement Prg.", System.Type.GetType("System.String"))
                dCol.Caption = ""
                dtnew.Columns.Add(dCol)

                '==21 Place holder
                dCol = New DataColumn("1", System.Type.GetType("System.String"))
                dCol.Caption = ""
                dtnew.Columns.Add(dCol)

                '==22 Place holder
                dCol = New DataColumn("2", System.Type.GetType("System.String"))
                dCol.Caption = ""
                dtnew.Columns.Add(dCol)

                '==23 Place holder
                dCol = New DataColumn("3", System.Type.GetType("System.String"))
                dCol.Caption = ""
                dtnew.Columns.Add(dCol)

                Dim ictr As Integer = dtnew.Columns.Count
                For ictr = 0 To dt.Rows.Count - 1
                    Dim dtRow As DataRow
                    dtRow = dtnew.NewRow
                    dtRow.Item(0) = dt.Rows(i).Item("PatientLastName") & ", " & dt.Rows(i).Item("PatientFirstName")
                    dtRow.Item(1) = IIf(IsDBNull(dt.Rows(i).Item("Gender")), "None", dt.Rows(i).Item("Gender"))
                    dtRow.Item(3) = dt.Rows(i).Item("NurseStationCd") & ":" & dt.Rows(i).Item("Roomno") & "-" & dt.Rows(i).Item("Bedno")
                    If Not dt.Rows(i).IsNull("BirthDate") Then
                        dtRow.Item(2) = FormatDateTime(dt.Rows(i).Item("BirthDate"), DateFormat.ShortDate) & "  Age: " & DateDiff(DateInterval.Year, dt.Rows(i).Item("BirthDate"), Now)

                    Else
                        dtRow.Item(2) = "none"
                    End If
                    dtRow.Item(4) = IIf(IsDBNull(dt.Rows(i).Item("FinancialClass")), "None", dt.Rows(i).Item("FinancialClass"))
                    dtRow.Item(5) = IIf(IsDBNull(dt.Rows(i).Item("InsuranceInfo")), "None", dt.Rows(i).Item("InsuranceInfo"))
                    dtRow.Item(6) = IIf(IsDBNull(dt.Rows(i).Item("LOS")), "None", dt.Rows(i).Item("LOS"))
                    dtRow.Item(7) = IIf(IsDBNull(dt.Rows(i).Item("AttendingPhysName")), "None", dt.Rows(i).Item("AttendingPhysName"))
                    dtRow.Item(8) = IIf(IsDBNull(dt.Rows(i).Item("HospitalService")), "None", dt.Rows(i).Item("HospitalService"))
                    dtRow.Item(9) = IIf(IsDBNull(dt.Rows(i).Item("CaseMgrFullName")), "None", dt.Rows(i).Item("CaseMgrFullName"))
                    dtRow.Item(10) = IIf(IsDBNull(dt.Rows(i).Item("SocialWName")), "None", dt.Rows(i).Item("SocialWName"))
                    If Not dt.Rows(i).IsNull("InvisionAdmitDate") Then
                        dtRow.Item(11) = FormatDateTime(dt.Rows(i).Item("InvisionAdmitDate"), DateFormat.ShortDate)
                    Else
                        dtRow.Item(11) = "None"
                    End If
                    If Not dt.Rows(i).IsNull("InvisionDischargeDate") Then
                        dtRow.Item(12) = FormatDateTime(dt.Rows(i).Item("InvisionDischargeDate"), DateFormat.ShortDate)
                    Else
                        dtRow.Item(12) = "None"
                    End If
                    dtRow.Item(13) = IIf(IsDBNull(dt.Rows(i).Item("IsolationCD")), "None", dt.Rows(i).Item("IsolationCD"))

                    If IsDBNull(dt.Rows(i).Item("ResistantOrg")) Then
                        dtRow.Item(14) = "None"
                    Else 'has value
                        If dt.Rows(i).Item("ResistantOrg").ToString.Length > 20 Then
                            dtRow.Item(14) = dt.Rows(i).Item("ResistantOrg").ToString().Substring(0, 20) & " (Abbr)"
                        Else
                            dtRow.Item(14) = dt.Rows(i).Item("ResistantOrg")
                        End If

                    End If
                    dtRow.Item(15) = IIf(IsDBNull(dt.Rows(i).Item("OptInOut")), "None", dt.Rows(i).Item("OptInOut"))
                    dtRow.Item(16) = IIf(IsDBNull(dt.Rows(i).Item("PatientAcctNumber")), "None", dt.Rows(i).Item("PatientAcctNumber"))
                    dtRow.Item(17) = IIf(IsDBNull(dt.Rows(i).Item("MedicalRecNo")), "None", dt.Rows(i).Item("MedicalRecNo"))
                    dtRow.Item(18) = IIf(IsDBNull(dt.Rows(i).Item("LastDayCovered")), "None", dt.Rows(i).Item("LastDayCovered"))
                    dtRow.Item(19) = IIf(IsDBNull(dt.Rows(i).Item("PCPPhysName")), "None", dt.Rows(i).Item("PCPPhysName"))
                    dtRow.Item(20) = IIf(IsDBNull(dt.Rows(i).Item("EntitlementPrg")), "None", dt.Rows(i).Item("EntitlementPrg"))

                    'LastDayCovered()
                    'If Not dt.Rows(i).IsNull("InvisionDischargeDate") Then
                    '    dtRow.Item(12) = FormatDateTime(dt.Rows(i).Item("InvisionDischargeDate"), DateFormat.ShortDate)
                    'Else
                    '    dtRow.Item(12) = "None"
                    'End If
                    '  dtRow.Item(11) = IIf(dt.Rows(i).IsNull("InvisionAdmitDate"), "None", FormatDateTime(dt.Rows(i).Item("InvisionAdmitDate"), DateFormat.ShortDate))
                    ' dtRow.Item(12) = IIf(dt.Rows(i).IsNull("InvisionDischargeDate"), "None", FormatDateTime(dt.Rows(i).Item("InvisionDischargeDate"), DateFormat.ShortDate))
                    dtnew.Rows.Add(dtRow)
                Next
                ''   & "<tr style='color:navy ;font-size: small; background-color: azure'><th colspan='1'>Sch</th><th colspan='1'>From</th><th colspan='1'>To</th><th colspan='1'>When</th><th colspan='1'>Issued by</th></tr>"
                ''For i = 0 To dt.Rows.Count - 1
                ''    sReturn &= StartRow & dt.Rows(i).Item("CaseNum") & StopCell
                ''    sReturn &= StartCell & dt.Rows(i).Item("PatientFirstName") & StopCell
                ''    sReturn &= StartCell & dt.Rows(i).Item("PatientLastName") & StopCell
                ''    sReturn &= StartCell & dt.Rows(i).Item("EnterDate") & StopCell
                ''    sReturn &= StartCell & " Hours Waiting " & dt.Rows(i).Item("HoursWaiting") & StopRow
                ''    sReturn &= StartCell & " Hours Waiting " & dt.Rows(i).Item("AttendingPhysName") & StopRow
                ''Next
                '' '' sReturn &= "</tr></table>"
                ''sReturn &= "</tr>"
                ''For i = 0 To dt.Rows.Count - 1
                ''    sReturn &= "<tr><td colspan='5'>" & dt.Rows(i).Item("Comments") & StopRow

                ''Next
                sReturn &= MakehtmlTableRowsNamesOverValues(dtnew, iTblColumnsPerRow, 0, 0)
                ' sReturn &= "<tr><td colspan='" & iTblColumnsPerRow & "'>" & "Test Data Content to be determined" & StopRow

                sReturn &= "</table>"
                '  sReturn = sReturn.Replace("'", "\'")
            End If
            If sReturn = "" Then

            Else
                Return sReturn
            End If
        Catch e As Exception
            'Dim uex As New ASPUnhandledException2005.Handler()
            'uex.HandleException(e, True, True, False)
            'Return "Error No Data PatientBedNum= " & contextKey & " Conn " & sConn
            sErr = e.Message
            Return sErr
        End Try

    End Function
    <WebMethod()> _
    Public Function GetThisPatientTransports(ByVal contextKey As String, ByVal sConn As String) As String
        '  Dim ans As Integer = 0
        '  Dim divider As Integer = 0
        Try
            'make error   ans = contextKey / divider
            If contextKey = "" Or contextKey = "0" Then
                Return "<table bgcolor='azure'><tr><th colspan='1'>No Transport Information Available</th></tr></table>"
                Exit Function
            End If
            Dim dt As New DataTable
            Dim i As Integer = 0 ' bgcolor='azure'
            Dim sReturn As String = "<table bgcolor='azure' style='font-size:xxsmall'><tr><th colspan='1'>No Transport Information Available</th></tr></table>"
            Dim StartRow As String = "<tr style='font-size: x-small; color: Navy; background-color: azure'><td>"
            Dim StartCell As String = "<td>"
            Dim StopCell As String = "</td>"
            Dim StopRow As String = "</td></tr>"
            Dim sImage As String = " <asp:ImageButton ID='imgBtnTrans' runat='server' Height='20px' ImageUrl='/~Images/Transporting.GIF'  Width='20px' />"
            Dim thisdata As New CommandsSqlAndOleDb("selSchedulebyPatientNum", sConn)
            thisdata.AddSqlProcParameter("@PatientBedNum", contextKey, SqlDbType.NVarChar, 16)
            ' thisdata.AddSqlProcParameter("@schedulenum", e.Row.Cells(9).Text, SqlDbType.Int, 4)
            dt = thisdata.GetSqlDataTable
            If dt.Rows.Count > 0 Then 'Main table
                sReturn = "<table bgcolor='azure' cellpadding='5' >" & _
                          "<tr style='color:green; text-decoration: underline; background-color: lightyellow'><th colspan=5 ><center>Transports" _
                          & sImage & "</center></th></tr>" _
                          & "<tr style='color:navy ;font-size: small; background-color: azure'><th colspan='1'>Sch</th><th colspan='1'>From</th><th colspan='1'>To</th><th colspan='1'>When</th><th colspan='1'>Issued by</th></tr>"
                For i = 0 To dt.Rows.Count - 1
                    sReturn &= StartRow & dt.Rows(i).Item("schedulenum") & StopCell
                    sReturn &= StartCell & dt.Rows(i).Item("FromLocationDesc") & StopCell
                    sReturn &= StartCell & dt.Rows(i).Item("ToLocationDesc") & StopCell
                    sReturn &= StartCell & dt.Rows(i).Item("DateToTransport") & StopCell
                    sReturn &= StartCell & dt.Rows(i).Item("UserWhoIssusedTransport") & StopRow

                Next
                '' sReturn &= "</tr></table>"
                sReturn &= "</tr>"
                For i = 0 To dt.Rows.Count - 1
                    sReturn &= "<tr><td colspan='5'>" & dt.Rows(i).Item("Comments") & StopRow

                Next
                sReturn &= "</table>"
            End If
            If sReturn = "" Then

            Else
                Return sReturn
            End If
        Catch e As Exception
            'Dim uex As New ASPUnhandledException2005.Handler()
            'uex.HandleException(e, True, True, False)
            'Return "Error No Data PatientBedNum= " & contextKey & " Conn " & sConn

        End Try

    End Function
    <WebMethod()> _
    Public Function GetThisCaseInfo2Arg(ByVal contextKey As String, ByVal sLoginID As String, ByVal sHospital As String) As String
        'used by showit
        ' sReturnToJScript= Y or N
        '  Dim ans As Integer = 0
        '  Dim divider As Integer = 0
        ', ByVal sLoginID As String, ByVal sConn As String, ByVal sReturnToJScript As String
        'Dim sLoginID As String = "chal01", ByVal sConn As String)
        Dim sConn As String = Session("DataConn") '"data source=WEBDEV10\SQL2005;initial catalog=SpringfieldCaseMgt01;workstation id=CORPIS26900;packet size=4096;user id=bedtracking;persist security info=True;password=crozer;Connect Timeout=99"
        Dim sReturnToJScript As String = "n"
        Dim sErr As String = ""
        Try
            'make error   ans = contextKey / divider
            If contextKey = "" Or contextKey = "0" Then
                '  Return "<table bgcolor='azure'><tr><th colspan='1'>No Transport Information Available</th></tr></table>"
                Exit Function
            End If
            Dim iTblColumnsPerRow As Integer = 4
            Dim dt As New DataTable
            Dim i As Integer = 0 ' bgcolor='azure'
            Dim sReturn As String = "<table bgcolor='azure' style='font-size:xxsmall'><tr><th colspan='1'>No Transport Information Available</th></tr></table>"
            Dim StartRow As String = "<tr style='font-size: x-small; color: Navy; background-color: azure'><td>"
            Dim StartCell As String = "<td>"
            Dim StopCell As String = "</td>"
            Dim StopRow As String = "</td></tr>"
            Dim sImage As String = " <asp:ImageButton ID='imgBtnTrans' runat='server' Height='20px' ImageUrl='/~Images/Transporting.GIF'  Width='20px' />"
            Dim sSplit() As String = contextKey.Split(":")
            Dim sCaseNum As String = sSplit(0)
            Dim sMode As String = sSplit(1)

            Dim thisdata As New CommandsSqlAndOleDb("selCaseByCaseNum", sConn)
            If sMode.ToLower = "tc" Then
                thisdata.setCommandText("selTCCaseByCaseNum")
            End If
            thisdata.AddSqlProcParameter("@CaseNum", sCaseNum, SqlDbType.Int, 4)
            thisdata.AddSqlProcParameter("@NtLogin", sLoginID, SqlDbType.NVarChar, 25)
            thisdata.AddSqlProcParameter("@Hospitalnum", sHospital, SqlDbType.Int, 4)

            'Dim thisdata As New CommandsSqlAndOleDb("selCaseByCaseNum", sConn)
            ''thisdata.AddSqlProcParameter("@CaseNum", contextKey, SqlDbType.NVarChar, 16)
            'thisdata.AddSqlProcParameter("@CaseNum", contextKey, SqlDbType.Int, 4)
            'thisdata.AddSqlProcParameter("@NtLogin", sLoginID, SqlDbType.NVarChar, 25)
            dt = thisdata.GetSqlDataTable
            If dt.Rows.Count > 0 Then 'Main table
                sReturn = "<table bgcolor='azure' cellpadding='5' >" & _
                          "<tr style='color:green; text-decoration: underline; background-color: lightyellow'><th colspan='" & iTblColumnsPerRow & "'><center>Case Information" _
                          & "</center></th></tr>" ' _
                'Create custom columns popup display
                Dim dtnew As New DataTable
                Dim dCol As New DataColumn
                '==0
                dCol = New DataColumn("Patient Name", System.Type.GetType("System.String"))
                dtnew.Columns.Add(dCol)
                '==1
                dCol = New DataColumn("Gender", System.Type.GetType("System.String"))
                dCol.Caption = "Sex"
                dtnew.Columns.Add(dCol)
                '==2
                dCol = New DataColumn("Birth Date", System.Type.GetType("System.String"))
                dCol.Caption = "Born"
                dtnew.Columns.Add(dCol)
                '==3
                dCol = New DataColumn("Room Bed", System.Type.GetType("System.String"))
                dCol.Caption = "Room Bed #"
                dtnew.Columns.Add(dCol)
                '==4
                dCol = New DataColumn("Financial Class", System.Type.GetType("System.String"))
                dtnew.Columns.Add(dCol)
                '==5
                dCol = New DataColumn("Insurance", System.Type.GetType("System.String"))
                dtnew.Columns.Add(dCol)
                '==6
                dCol = New DataColumn("LOS", System.Type.GetType("System.String"))
                ' dCol.Caption = "Room Bed #"
                dtnew.Columns.Add(dCol)
                '==7
                dCol = New DataColumn("Attending Dr.Name", System.Type.GetType("System.String"))
                dCol.Caption = "Attending Dr.Name"
                dtnew.Columns.Add(dCol)
                '==8
                dCol = New DataColumn("Service", System.Type.GetType("System.String"))
                dCol.Caption = "Service"
                dtnew.Columns.Add(dCol)
                '==9
                dCol = New DataColumn("Case Manager", System.Type.GetType("System.String"))
                dCol.Caption = "Case Manager"
                dtnew.Columns.Add(dCol)
                '==10
                dCol = New DataColumn("Social Worker", System.Type.GetType("System.String"))
                dCol.Caption = "Social Worker"
                dtnew.Columns.Add(dCol)
                '==11
                dCol = New DataColumn("Admit Date", System.Type.GetType("System.String"))
                dCol.Caption = "Admit Date"
                dtnew.Columns.Add(dCol)
                '==112
                dCol = New DataColumn("Discharge Date", System.Type.GetType("System.String"))
                dCol.Caption = "Discharge"
                dtnew.Columns.Add(dCol)
                '==13
                dCol = New DataColumn("Isolation", System.Type.GetType("System.String"))
                dCol.Caption = "Isolation"
                dtnew.Columns.Add(dCol)
                '==14
                dCol = New DataColumn("Resistant Org", System.Type.GetType("System.String"))
                dCol.Caption = "ResistantOrg"
                dtnew.Columns.Add(dCol)
                '==15
                dCol = New DataColumn("Privacy", System.Type.GetType("System.String"))
                dCol.Caption = ""
                dtnew.Columns.Add(dCol)
                '==16
                dCol = New DataColumn("PatAcctNumber", System.Type.GetType("System.String"))
                dCol.Caption = ""
                dtnew.Columns.Add(dCol)
                '==17
                dCol = New DataColumn("MedicalRecNo", System.Type.GetType("System.String"))
                dCol.Caption = ""
                dtnew.Columns.Add(dCol)
                '==18 LastDayCovered
                dCol = New DataColumn("LastDayCovered", System.Type.GetType("System.String"))
                dCol.Caption = ""
                dtnew.Columns.Add(dCol)
                '==19 LastDayCovered
                dCol = New DataColumn("PCP Physican", System.Type.GetType("System.String"))
                dCol.Caption = ""
                dtnew.Columns.Add(dCol)

                Dim ictr As Integer = dtnew.Columns.Count
                For ictr = 0 To dt.Rows.Count - 1
                    Dim dtRow As DataRow
                    dtRow = dtnew.NewRow
                    dtRow.Item(0) = dt.Rows(i).Item("PatientLastName") & ", " & dt.Rows(i).Item("PatientFirstName")
                    dtRow.Item(1) = IIf(IsDBNull(dt.Rows(i).Item("Gender")), "None", dt.Rows(i).Item("Gender"))
                    dtRow.Item(3) = dt.Rows(i).Item("NurseStationCd") & ":" & dt.Rows(i).Item("Roomno") & "-" & dt.Rows(i).Item("Bedno")
                    If Not dt.Rows(i).IsNull("BirthDate") Then
                        dtRow.Item(2) = FormatDateTime(dt.Rows(i).Item("BirthDate"), DateFormat.ShortDate) & "  Age: " & DateDiff(DateInterval.Year, dt.Rows(i).Item("BirthDate"), Now)

                    Else
                        dtRow.Item(2) = "none"
                    End If
                    dtRow.Item(4) = IIf(IsDBNull(dt.Rows(i).Item("FinancialClass")), "None", dt.Rows(i).Item("FinancialClass"))
                    dtRow.Item(5) = IIf(IsDBNull(dt.Rows(i).Item("InsuranceInfo")), "None", dt.Rows(i).Item("InsuranceInfo"))
                    dtRow.Item(6) = IIf(IsDBNull(dt.Rows(i).Item("LOS")), "None", dt.Rows(i).Item("LOS"))
                    dtRow.Item(7) = IIf(IsDBNull(dt.Rows(i).Item("AttendingPhysName")), "None", dt.Rows(i).Item("AttendingPhysName"))
                    dtRow.Item(8) = IIf(IsDBNull(dt.Rows(i).Item("HospitalService")), "None", dt.Rows(i).Item("HospitalService"))
                    dtRow.Item(9) = IIf(IsDBNull(dt.Rows(i).Item("CaseMgrFullName")), "None", dt.Rows(i).Item("CaseMgrFullName"))
                    dtRow.Item(10) = IIf(IsDBNull(dt.Rows(i).Item("SocialWName")), "None", dt.Rows(i).Item("SocialWName"))
                    If Not dt.Rows(i).IsNull("InvisionAdmitDate") Then
                        dtRow.Item(11) = FormatDateTime(dt.Rows(i).Item("InvisionAdmitDate"), DateFormat.ShortDate)
                    Else
                        dtRow.Item(11) = "None"
                    End If
                    If Not dt.Rows(i).IsNull("InvisionDischargeDate") Then
                        dtRow.Item(12) = FormatDateTime(dt.Rows(i).Item("InvisionDischargeDate"), DateFormat.ShortDate)
                    Else
                        dtRow.Item(12) = "None"
                    End If
                    dtRow.Item(13) = IIf(IsDBNull(dt.Rows(i).Item("IsolationCD")), "None", dt.Rows(i).Item("IsolationCD"))

                    If IsDBNull(dt.Rows(i).Item("ResistantOrg")) Then
                        dtRow.Item(14) = "None"
                    Else 'has value
                        If dt.Rows(i).Item("ResistantOrg").ToString.Length > 20 Then
                            dtRow.Item(14) = dt.Rows(i).Item("ResistantOrg").ToString().Substring(0, 20) & " (Abbr)"
                        Else
                            dtRow.Item(14) = dt.Rows(i).Item("ResistantOrg")
                        End If

                    End If
                    dtRow.Item(15) = IIf(IsDBNull(dt.Rows(i).Item("OptInOut")), "None", dt.Rows(i).Item("OptInOut"))
                    dtRow.Item(16) = IIf(IsDBNull(dt.Rows(i).Item("PatientAcctNumber")), "None", dt.Rows(i).Item("PatientAcctNumber"))
                    dtRow.Item(17) = IIf(IsDBNull(dt.Rows(i).Item("MedicalRecNo")), "None", dt.Rows(i).Item("MedicalRecNo"))
                    dtRow.Item(18) = IIf(IsDBNull(dt.Rows(i).Item("LastDayCovered")), "None", dt.Rows(i).Item("LastDayCovered"))
                    dtRow.Item(19) = IIf(IsDBNull(dt.Rows(i).Item("PCPPhysName")), "None", dt.Rows(i).Item("PCPPhysName"))

                    'LastDayCovered()
                    'If Not dt.Rows(i).IsNull("InvisionDischargeDate") Then
                    '    dtRow.Item(12) = FormatDateTime(dt.Rows(i).Item("InvisionDischargeDate"), DateFormat.ShortDate)
                    'Else
                    '    dtRow.Item(12) = "None"
                    'End If
                    '  dtRow.Item(11) = IIf(dt.Rows(i).IsNull("InvisionAdmitDate"), "None", FormatDateTime(dt.Rows(i).Item("InvisionAdmitDate"), DateFormat.ShortDate))
                    ' dtRow.Item(12) = IIf(dt.Rows(i).IsNull("InvisionDischargeDate"), "None", FormatDateTime(dt.Rows(i).Item("InvisionDischargeDate"), DateFormat.ShortDate))
                    dtnew.Rows.Add(dtRow)
                Next
                Dim sReturnString As String = GetJson(dtnew)
                ' Return sReturnString
                'End If
                ''   & "<tr style='color:navy ;font-size: small; background-color: azure'><th colspan='1'>Sch</th><th colspan='1'>From</th><th colspan='1'>To</th><th colspan='1'>When</th><th colspan='1'>Issued by</th></tr>"
                ''For i = 0 To dt.Rows.Count - 1
                ''    sReturn &= StartRow & dt.Rows(i).Item("CaseNum") & StopCell
                ''    sReturn &= StartCell & dt.Rows(i).Item("PatientFirstName") & StopCell
                ''    sReturn &= StartCell & dt.Rows(i).Item("PatientLastName") & StopCell
                ''    sReturn &= StartCell & dt.Rows(i).Item("EnterDate") & StopCell
                ''    sReturn &= StartCell & " Hours Waiting " & dt.Rows(i).Item("HoursWaiting") & StopRow
                ''    sReturn &= StartCell & " Hours Waiting " & dt.Rows(i).Item("AttendingPhysName") & StopRow
                ''Next
                '' '' sReturn &= "</tr></table>"
                ''sReturn &= "</tr>"
                ''For i = 0 To dt.Rows.Count - 1
                ''    sReturn &= "<tr><td colspan='5'>" & dt.Rows(i).Item("Comments") & StopRow

                ''Next
                sReturn &= MakehtmlTableRowsNamesOverValues(dtnew, iTblColumnsPerRow - 2, 0, 0)
                ' sReturn &= "<tr><td colspan='" & iTblColumnsPerRow & "'>" & "Test Data Content to be determined" & StopRow

                sReturn &= "</table>"
                '  sReturn = sReturn.Replace("'", "\'")
            End If
            If sReturnToJScript.ToLower = "y" Then
                sReturn = sReturn.Replace("'", "\'")
            End If
            If sReturn = "" Then

            Else
                ' sReturn = <table><tr><td>Test</td></tr></table>
                Return sReturn
            End If
        Catch e As Exception
            'Dim uex As New ASPUnhandledException2005.Handler()
            'uex.HandleException(e, True, True, False)
            'Return "Error No Data PatientBedNum= " & contextKey & " Conn " & sConn
            sErr = e.Message
            ' Return sErr
        End Try

    End Function
    Public Function GetJson(ByVal dt As DataTable) As String

        Dim serializer As System.Web.Script.Serialization.JavaScriptSerializer = New System.Web.Script.Serialization.JavaScriptSerializer()
        Dim rows As New List(Of Dictionary(Of String, Object))
        Dim row As Dictionary(Of String, Object)

        For Each dr As DataRow In dt.Rows
            row = New Dictionary(Of String, Object)
            For Each col As DataColumn In dt.Columns
                row.Add(col.ColumnName, dr(col))
            Next
            rows.Add(row)
        Next
        Return serializer.Serialize(rows)
    End Function

End Class
'End Namespace