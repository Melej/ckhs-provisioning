﻿Imports Microsoft.VisualBasic
Imports System.ComponentModel


Public Class Client3
    <DisplayName("Client#")> Property ClientNum As Integer
    <DisplayName("First Name")> Property FirstName As String
    <DisplayName("Full Name")> Property FullName As String
    <DisplayName("Last Name")> Property LastName As String
    <DisplayName("MI")> Property Mi As String
    <DisplayName("Last Updated")> Property LastUpdated As DateTime
    <DisplayName("Login Name")> Property LoginName As String
    <DisplayName("Phone")> Property Phone As String
    <DisplayName("Fax")> Property Fax As String
    <DisplayName("EMail")> Property EMail As String
    <DisplayName("Alternate Phone")> Property AlternatePhone As String
    <DisplayName("Facility Code")> Property FacilityCd As String
    <DisplayName("Building Code")> Property BuildingCd As String
    <DisplayName("Office")> Property Office As String
    <DisplayName("Floor")> Property Floor As String
    <DisplayName("Title")> Property Title As String
    <DisplayName("Suffix")> Property Suffix As String
    <DisplayName("Address1")> Property Address1 As String
    <DisplayName("Address2")> Property Address2 As String
    <DisplayName("City")> Property City As String
    <DisplayName("State")> Property State As String
    <DisplayName("Zip")> Property Zip As String
    <DisplayName("Entity Code")> Property EntityCd As String
    <DisplayName("Entity Name")> Property EntityName As String
    <DisplayName("Department Code")> Property DepartmentCd As String
    <DisplayName("Department Name")> Property DepartmentName As String

    <DisplayName("Facility Name")> Property FacilityName As String
    <DisplayName("Date Deactivated")> Property DateDeactivated As DateTime
    <DisplayName("Pager")> Property Pager As String
    <DisplayName("PagerType")> Property PagerType As String
    <DisplayName("Employee Type Code")> Property EmpTypeCd As String
    <DisplayName("Start Date")> Property StartDate As DateTime
    <DisplayName("End Date")> Property EndDate As DateTime
    <DisplayName("Security Type Code")> Property SecurityTypeCd As String
    <DisplayName("Security Description")> Property SecurityDesc As String
    <DisplayName("Group Number")> Property GroupNum As String
    <DisplayName("Group Name")> Property GroupName As String
    <DisplayName("User Position Description")> Property UserPositionDesc As String
    <DisplayName("Doctor Master Number")> Property DoctorMasterNum As Integer
    <DisplayName("Share Drive")> Property ShareDrive As String
    <DisplayName("Npi")> Property Npi As Integer
    <DisplayName("Taxonomy")> Property Taxonomy As String
    <DisplayName("Provider")> Property Provider As String
    <DisplayName("Han")> Property Han As String
    <DisplayName("Licenses No")> Property LicensesNo As String
    <DisplayName("Credentialed Date")> Property CredentialedDate As DateTime
    <DisplayName("Specialty")> Property Specialty As String
    <DisplayName("CCMC Admit Rights")> Property CCMCadmitRights As String
    <DisplayName("DCMH Admit Rights")> Property DCMHadmitRights As String
    <DisplayName("Employee Number")> Property SiemensEmpNum As String
    <DisplayName("NonHan Location")> Property NonHanLocation As String
    <DisplayName("Vendor Name")> Property VendorName As String
    <DisplayName("Date Created")> Property DateCreated As DateTime

    <DisplayName("AKA LastName")> Property AKALastName As String
    <DisplayName("Given Employee Number")> Property AuthorizationEmpNum As String

    <DisplayName("Cerner Position")> Property CernerPositionDesc As String

    <DisplayName("G Group Number")> Property GNumberGroupNum As String
    <DisplayName("Comm Grouop Number")> Property CommNumberGroupNum As String

	<DisplayName("Last Four")> Property LastFour As String

	<DisplayName("PMH Employee ID")> Property PMHEmployeeID As String

	<DisplayName("PMH Department")> Property PMHDepartment As String

End Class
