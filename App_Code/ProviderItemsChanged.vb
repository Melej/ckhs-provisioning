﻿Imports Microsoft.VisualBasic

Public Class ProviderItemsChanged
    Private _AccountRequestNum As String
    Private _DataConn As String
    Private _info As String
    Private _FirstName As String
    Private _NewFirstName As String
    Private _CurrentFirstName As String

    Private _LastName As String
    Private _NewlastName As String
    Private _CurrentlastName As String

    Private _MI As String
    Private _NweMI As String
    Private _CurrentMI As String

    Private _Address1 As String
    Private _NewAddress1 As String
    Private _CurrentAddress1 As String

    Private _Address2 As String
    Private _NewAddress2 As String
    Private _CurrentAddress2 As String

    Private _City As String
    Private _NewCity As String
    Private _CurrentCity As String

    Private _State As String
    Private _NewState As String
    Private _CurrentState As String

    Private _Zip As String
    Private _NewZip As String
    Private _CurrentZip As String

    Private _Phone As String
    Private _NewPhone As String
    Private _CurrentPhone As String

    Private _Fax As String
    Private _NewFax As String
    Private _CurrentFax As String
    Public Sub New()

    End Sub
    Public Sub New(ByVal AccountRequestNum As String, ByVal DataConn As String)
        _AccountRequestNum = AccountRequestNum
        _DataConn = DataConn

        If _DataConn = "" Then
            _DataConn = HttpContext.Current.Session("EmployeeConn")
        End If

        getInfo()

    End Sub
    Public Sub getInfo()
        Dim cmdSql As New SqlCommand
        Dim connSql As New SqlConnection(_DataConn)
        Dim drSql As SqlClient.SqlDataReader

        cmdSql.CommandText = "SelEmailProviderChangesV2"
        cmdSql.CommandType = CommandType.StoredProcedure
        cmdSql.Connection = connSql
        cmdSql.Parameters.Add("@AccountRequestNum", SqlDbType.Int).Value = _AccountRequestNum

        connSql.Open()

        Try
            drSql = cmdSql.ExecuteReader

            While drSql.Read()
                _FirstName = IIf(IsDBNull(drSql("FirstName")), Nothing, drSql("FirstName"))
                _NewFirstName = IIf(IsDBNull(drSql("NewFirstName")), Nothing, drSql("NewFirstName"))
                _CurrentFirstName = IIf(IsDBNull(drSql("CurrentFirstName")), Nothing, drSql("CurrentFirstName"))

                _LastName = IIf(IsDBNull(drSql("LastName")), Nothing, drSql("LastName"))
                _NewlastName = IIf(IsDBNull(drSql("NewlastName")), Nothing, drSql("NewlastName"))
                _CurrentlastName = IIf(IsDBNull(drSql("CurrentlastName")), Nothing, drSql("CurrentlastName"))

                _MI = IIf(IsDBNull(drSql("MI")), Nothing, drSql("MI"))
                _NweMI = IIf(IsDBNull(drSql("NweMI")), Nothing, drSql("NweMI"))
                _CurrentMI = IIf(IsDBNull(drSql("CurrentMI")), Nothing, drSql("CurrentMI"))

                _Address1 = IIf(IsDBNull(drSql("Address1")), Nothing, drSql("Address1"))
                _NewAddress1 = IIf(IsDBNull(drSql("NewAddress1")), Nothing, drSql("NewAddress1"))
                _CurrentAddress1 = IIf(IsDBNull(drSql("CurrentAddress1")), Nothing, drSql("CurrentAddress1"))

                _Address2 = IIf(IsDBNull(drSql("Address2")), Nothing, drSql("Address2"))
                _NewAddress2 = IIf(IsDBNull(drSql("NewAddress2")), Nothing, drSql("NewAddress2"))
                _CurrentAddress2 = IIf(IsDBNull(drSql("CurrentAddress2")), Nothing, drSql("CurrentAddress2"))

                _City = IIf(IsDBNull(drSql("City")), Nothing, drSql("City"))
                _NewCity = IIf(IsDBNull(drSql("NewCity")), Nothing, drSql("NewCity"))
                _CurrentCity = IIf(IsDBNull(drSql("CurrentCity")), Nothing, drSql("CurrentCity"))

                _State = IIf(IsDBNull(drSql("State")), Nothing, drSql("State"))
                _NewState = IIf(IsDBNull(drSql("NewState")), Nothing, drSql("NewState"))
                _CurrentState = IIf(IsDBNull(drSql("CurrentState")), Nothing, drSql("CurrentState"))

                _Zip = IIf(IsDBNull(drSql("Zip")), Nothing, drSql("Zip"))
                _NewZip = IIf(IsDBNull(drSql("NewZip")), Nothing, drSql("NewZip"))
                _CurrentZip = IIf(IsDBNull(drSql("CurrentZip")), Nothing, drSql("CurrentZip"))

                _Phone = IIf(IsDBNull(drSql("Phone")), Nothing, drSql("Phone"))
                _NewPhone = IIf(IsDBNull(drSql("NewPhone")), Nothing, drSql("NewPhone"))
                _CurrentPhone = IIf(IsDBNull(drSql("CurrentPhone")), Nothing, drSql("CurrentPhone"))

                _Fax = IIf(IsDBNull(drSql("Fax")), Nothing, drSql("Fax"))
                _NewFax = IIf(IsDBNull(drSql("NewFax")), Nothing, drSql("NewFax"))
                _CurrentFax = IIf(IsDBNull(drSql("CurrentFax")), Nothing, drSql("CurrentFax"))

            End While
        Catch ex As Exception
            Throw ex
        End Try


    End Sub
    Public ReadOnly Property AccountRequestNum As String
        Get
            Return _AccountRequestNum
        End Get
    End Property

    Public ReadOnly Property FirstName As String
        Get
            Return _FirstName
        End Get
    End Property
    Public ReadOnly Property NewFirstName As String
        Get
            Return _NewFirstName
        End Get
    End Property
    Public ReadOnly Property CurrentFirstName As String
        Get
            Return _CurrentFirstName
        End Get
    End Property

    Public ReadOnly Property LastName As String
        Get
            Return _LastName
        End Get
    End Property
    Public ReadOnly Property NewlastName As String
        Get
            Return _NewlastName
        End Get
    End Property
    Public ReadOnly Property CurrentlastName As String
        Get
            Return _CurrentlastName
        End Get
    End Property

    Public ReadOnly Property MI As String
        Get
            Return _MI
        End Get
    End Property
    Public ReadOnly Property NweMI As String
        Get
            Return _NweMI
        End Get
    End Property
    Public ReadOnly Property CurrentMI As String
        Get
            Return _CurrentMI
        End Get
    End Property

    Public ReadOnly Property Address1 As String
        Get
            Return _Address1
        End Get
    End Property
    Public ReadOnly Property CurrentAddress1 As String
        Get
            Return _CurrentAddress1
        End Get
    End Property
    Public ReadOnly Property NewAddress1 As String
        Get
            Return _NewAddress1
        End Get
    End Property

    Public ReadOnly Property Address2 As String
        Get
            Return _Address2
        End Get
    End Property

    Public ReadOnly Property CurrentAddress2 As String
        Get
            Return _CurrentAddress2
        End Get
    End Property
    Public ReadOnly Property NewAddress2 As String
        Get
            Return _NewAddress2
        End Get
    End Property

    Public ReadOnly Property City As String
        Get
            Return _City
        End Get
    End Property
    Public ReadOnly Property CurrentCity As String
        Get
            Return _CurrentCity
        End Get
    End Property
    Public ReadOnly Property NewCity As String
        Get
            Return _NewCity
        End Get
    End Property

    Public ReadOnly Property State As String
        Get
            Return _State
        End Get
    End Property
    Public ReadOnly Property CurrentState As String
        Get
            Return _CurrentState
        End Get
    End Property
    Public ReadOnly Property NewState As String
        Get
            Return _NewState
        End Get
    End Property

    Public ReadOnly Property Zip As String
        Get
            Return _Zip
        End Get
    End Property
    Public ReadOnly Property CurrentZip As String
        Get
            Return _CurrentZip
        End Get
    End Property
    Public ReadOnly Property NewZip As String
        Get
            Return _NewZip
        End Get
    End Property

    Public ReadOnly Property Phone As String
        Get
            Return _Phone
        End Get
    End Property
    Public ReadOnly Property CurrentPhone As String
        Get
            Return _CurrentPhone
        End Get
    End Property
    Public ReadOnly Property NewPhone As String
        Get
            Return _NewPhone
        End Get
    End Property

    Public ReadOnly Property Fax As String
        Get
            Return _Fax
        End Get
    End Property
    Public ReadOnly Property CurrentFax As String
        Get
            Return _CurrentFax
        End Get
    End Property
    Public ReadOnly Property NewFax As String
        Get
            Return _NewFax
        End Get
    End Property

End Class
Public Class ProviderItem
    Public AccountRequestNum As String
    Public FirstName As String
    Public NewFirstName As String
    Public CurrentFirstName As String

    Public LastName As String
    Public NewlastName As String
    Public CurrentlastName As String

    Public MI As String
    Public NweMI As String
    Public CurrentMI As String

    Public Address1 As String
    Public NewAddress1 As String
    Public CurrentAddress1 As String

    Public Address2 As String
    Public NewAddress2 As String
    Public CurrentAddress2 As String

    Public City As String
    Public NewCity As String
    Public CurrentCity As String

    Public State As String
    Public NewState As String
    Public CurrentState As String

    Public Zip As String
    Public NewZip As String
    Public CurrentZip As String

    Public Phone As String
    Public NewPhone As String
    Public CurrentPhone As String

    Public Fax As String
    Public NewFax As String
    Public CurrentFax As String


End Class
