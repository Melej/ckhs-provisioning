﻿Imports Microsoft.VisualBasic
Imports App_Code

Public Class DropDownListBinder
    Dim _DDLCommand As CommandsSqlAndOleDb
    Dim _ddlControl As DropDownList

    Public Sub New()

      

    End Sub
    Public Sub New(ByRef ddlControl As DropDownList)

        _ddlControl = ddlControl

    End Sub

    Public Function SetDDLCommand(ByVal sProc As String) As CommandsSqlAndOleDb
        _DDLCommand = New CommandsSqlAndOleDb(sProc)

        Return _DDLCommand

    End Function

    Public Sub AddSQlParam(ByVal sParam As String, ByVal sParamValue As String)

        _DDLCommand.AddSqlProcParameter(sParam, sParamValue, SqlDbType.NVarChar)


    End Sub

    Public Sub New(ByRef ddlControl As DropDownList, ByVal sProc As String, ByVal sDataConn As String)

        _ddlControl = ddlControl
        _DDLCommand = New CommandsSqlAndOleDb(sProc, sDataConn)

    End Sub

    Public Sub BindData(ByRef ddlControl As DropDownList, ByVal sProc As String, ByVal sValue As String, ByVal sText As String)

        Dim dt As New DataTable
        Dim thisdata As New CommandsSqlAndOleDb(sProc)
        dt = thisdata.GetSqlDataTable

        ddlControl.Items.Clear()

        ddlControl.DataSource = dt

        ddlControl.DataSource = dt
        ddlControl.DataValueField = sValue
        ddlControl.DataTextField = sText

        ddlControl.DataBind()

        ddlControl.Items.Insert(0, "Select")

    End Sub
    Public Sub BindData(ByRef ddlControl As DropDownList, ByVal sProc As String, ByVal sValue As String, ByVal sText As String, ByVal sDataConn As String)

        Dim dt As New DataTable
        Dim thisdata As New CommandsSqlAndOleDb(sProc, sDataConn)
        dt = thisdata.GetSqlDataTable

        ddlControl.Items.Clear()

        ddlControl.DataSource = dt

        ddlControl.DataValueField = sValue
        ddlControl.DataTextField = sText

        ddlControl.DataBind()

        ddlControl.Items.Insert(0, "Select")

    End Sub
    Public Sub BindData(ByVal ddlValue As String, ByVal ddlText As String)

        Dim dt As New DataTable

        dt = _DDLCommand.GetSqlDataTable

        _ddlControl.Items.Clear()

        _ddlControl.DataSource = dt

        _ddlControl.DataValueField = ddlValue
        _ddlControl.DataTextField = ddlText

        _ddlControl.DataBind()

        _ddlControl.Items.Insert(0, "Select")

    End Sub

    Public Sub AddDDLCriteria(ByVal sParameter As String, ByVal sValue As String, ByVal sqlDataType As SqlDbType)
        _DDLCommand.AddSqlProcParameter(sParameter, sValue, sqlDataType)

    End Sub
End Class
