﻿Imports Microsoft.VisualBasic
Imports App_Code
Imports System.ComponentModel
Imports System.Web.HttpContext

Public Class ProjectController
    Private _Client_Num As Integer
    Private _ProjectName As String
    Private _ProjectNum As Integer
    Private _StartDate As DateTime
    Private _endDate As DateTime
    Private _plannedDate As DateTime
    Private _actualDate As DateTime
    Private _LastUpdated As DateTime
    Private _ProjectDesc As String
    Private _ProjectShortName As String
    Private _OwnerName As String
    Private _DepartmentName As String
    Private _DepartmentOwner As String
	Private _departmentClientNum As Integer
	Private _ProjectType As String


	Private _dataConn As String

    Private _Projects As New List(Of ProjectsLis)
    Public Sub New()
        'If dataConn Is Nothing Then
        '    dataConn = HttpContext.Current.Session("CSCConn")
        'End If

        '_dataConn = dataConn

        'GetProjectInfo()

    End Sub
    Public Sub GetProjectByNum(ByVal Projectnum As Integer, ByRef sConn As String)
        _ProjectNum = Projectnum


        If sConn Is Nothing Then
            _dataConn = HttpContext.Current.Session("CSCConn")
        Else
            _dataConn = sConn
        End If

        GetProjectInfo()
    End Sub
    Public Sub GetProjectInfo()

        If _dataConn Is Nothing Then
            _dataConn = HttpContext.Current.Session("CSCConn")
        End If

        Using sqlConn As New SqlConnection(_dataConn)

            Dim Cmd As New SqlCommand()
            Cmd.CommandText = "SelProjectDesc"
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.Parameters.AddWithValue("@projectnum", _ProjectNum)


            Cmd.Connection = sqlConn



            sqlConn.Open()

            Using Cmd

                Using dr = Cmd.ExecuteReader()

                    If dr.HasRows Then

                        Do While dr.Read

                            Try
                                _ProjectNum = IIf(IsDBNull(dr("ProjectNum")), Nothing, dr("ProjectNum"))

                                _ProjectName = IIf(IsDBNull(dr("ProjectName")), Nothing, dr("ProjectName"))
                                _Client_Num = IIf(IsDBNull(dr("client_num")), Nothing, dr("client_num"))

                                _OwnerName = IIf(IsDBNull(dr("OwnerName")), Nothing, dr("OwnerName"))
                                _StartDate = IIf(IsDBNull(dr("StartDate")), Nothing, dr("StartDate"))
                                _endDate = IIf(IsDBNull(dr("endDate")), Nothing, dr("endDate"))

                                _actualDate = IIf(IsDBNull(dr("actualDate")), Nothing, dr("actualDate"))
                                _plannedDate = IIf(IsDBNull(dr("plannedDate")), Nothing, dr("plannedDate"))


                                _ProjectDesc = IIf(IsDBNull(dr("ProjectDesc")), Nothing, dr("ProjectDesc"))
                                _ProjectShortName = IIf(IsDBNull(dr("ProjectShortName")), Nothing, dr("ProjectShortName"))


                                _DepartmentName = IIf(IsDBNull(dr("DepartmentName")), Nothing, dr("DepartmentName"))

                                _DepartmentOwner = IIf(IsDBNull(dr("DepartmentOwner")), Nothing, dr("DepartmentOwner"))

                                _departmentClientNum = IIf(IsDBNull(dr("departmentClientNum")), Nothing, dr("departmentClientNum"))
								_ProjectType = IIf(IsDBNull(dr("ProjectType")), Nothing, dr("ProjectType"))

							Catch ex As Exception

                            End Try

                        Loop

                    End If

                End Using

            End Using

            sqlConn.Close()

        End Using

    End Sub
    Property ClientNum As Integer
        Get
            Return _Client_Num
        End Get
        Set(value As Integer)
            _Client_Num = value
        End Set
    End Property


    Property ProjectName As String
        Get
            Return _ProjectName
        End Get
        Set(value As String)
            _ProjectName = value
        End Set
    End Property

    Property ProjectNum As Integer
        Get
            Return _ProjectNum
        End Get
        Set(value As Integer)
            _ProjectNum = value
        End Set
    End Property


    Property ProjectDesc As String
        Get
            Return _ProjectDesc
        End Get
        Set(value As String)
            _ProjectDesc = value
        End Set
    End Property

	Property ProjectType As String
		Get
			Return _ProjectType
		End Get
		Set(value As String)
			_ProjectType = value
		End Set
	End Property
	Property ProjectShortName As String
        Get
            Return _ProjectShortName
        End Get
        Set(value As String)
            _ProjectShortName = value
        End Set
    End Property

    Property OwnerName As String
        Get
            Return _OwnerName
        End Get
        Set(value As String)
            _OwnerName = value
        End Set
    End Property

    Property StartDate As DateTime
        Get
            Return _StartDate
        End Get
        Set(value As DateTime)
            _StartDate = value
        End Set
    End Property

    Property endDate As DateTime
        Get
            Return _endDate
        End Get
        Set(value As DateTime)
            _endDate = value
        End Set
    End Property

    Property plannedDate As DateTime
        Get
            Return _plannedDate

        End Get
        Set(value As DateTime)
            _plannedDate = value
        End Set
    End Property

    Property actualDate As DateTime
        Get
            Return _actualDate

        End Get
        Set(value As DateTime)
            _actualDate = value
        End Set
    End Property

    Property DepartmentName As String
        Get
            Return _DepartmentName
        End Get

        Set(value As String)
            _DepartmentName = value
        End Set

    End Property

    Property DepartmentOwner As String
        Get
            Return _DepartmentOwner
        End Get

        Set(value As String)
            _DepartmentOwner = value
        End Set
    End Property

    Property departmentClientNum As Integer
        Get
            Return _departmentClientNum
        End Get
        Set(value As Integer)
            _departmentClientNum = value
        End Set
    End Property


End Class
Public Class ProjectsLis

    Property ClientNum As Integer
    Property ProjectName As String
    Property ProjectNum As Integer
    Property StartDate As DateTime
    Property endDate As DateTime
    Property actualDate As DateTime
    Property plannedDate As DateTime

    Property ProjectDesc As String
    Property ProjectShortName As String
    Property OwnerName As String
    Property DepartmentName As String
	Property Departmentowner As String
	Property ProjectType As String

	Property DepartmentOwnerNum As Integer
End Class
