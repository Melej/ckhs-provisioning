﻿Imports Microsoft.VisualBasic
Imports App_Code

Public Class TicketsRFSDetailClass
    Private _GroupsRPT As New List(Of TickectRFSReportDetailItems)
    Private _JustReport As New List(Of TickectRFSReportDetailItems)

    Private _dtTickets As New DataTable

    Public Sub New()


    End Sub
    Public Sub New(ByVal sStartDate As String, ByVal sEndDate As String, sGroup As String, ByVal sTicketByVal As String, ByVal sClose As String, ByVal sConn As String, ByVal sPriority As String, ByVal sCategory As String, ByVal sLevelType As String, ByVal sFirstTouch As String, ByVal sCloseType As String, ByVal sSortBy As String)


        Dim _DataConn As String
        _DataConn = sConn



        Dim thisData As New CommandsSqlAndOleDb("SelTicketsAndRFSReportV3", _DataConn)
        thisData.AddSqlProcParameter("@startDate", sStartDate, SqlDbType.NVarChar)
        thisData.AddSqlProcParameter("@endDate", sEndDate, SqlDbType.NVarChar)
        thisData.AddSqlProcParameter("@group_num", sGroup, SqlDbType.NVarChar)
        thisData.AddSqlProcParameter("@ticket_rfs", sClose, SqlDbType.NVarChar)
        thisData.AddSqlProcParameter("@close", sTicketByVal, SqlDbType.NVarChar)
        thisData.AddSqlProcParameter("@P1Only", sPriority, SqlDbType.NVarChar)

        thisData.AddSqlProcParameter("@category_cd", sCategory, SqlDbType.NVarChar)
        thisData.AddSqlProcParameter("@ByLevelType", sLevelType, SqlDbType.NVarChar)
        thisData.AddSqlProcParameter("@ByFirstTouch", sFirstTouch, SqlDbType.NVarChar)
        thisData.AddSqlProcParameter("@CloseDatetype", sCloseType, SqlDbType.NVarChar)

        thisData.AddSqlProcParameter("@SortBy", sSortBy, SqlDbType.NVarChar)



        _dtTickets = thisData.GetSqlDataTable()


        If _dtTickets IsNot Nothing Then

            For Each drSql As DataRow In _dtTickets.Rows

                Dim TicketRfs As New TickectRFSReportDetailItems()


                TicketRfs.request_num = IIf(IsDBNull(drSql("request_num")), Nothing, drSql("request_num"))

                TicketRfs.clientName = IIf(IsDBNull(drSql("clientName")), Nothing, drSql("clientName"))
                TicketRfs.entry_date = IIf(IsDBNull(drSql("entry_date")), Nothing, drSql("entry_date"))

                TicketRfs.close_date = IIf(IsDBNull(drSql("close_date")), Nothing, drSql("close_date"))

                TicketRfs.request_type = IIf(IsDBNull(drSql("request_type")), Nothing, drSql("request_type"))

                TicketRfs.category_cd = IIf(IsDBNull(drSql("category_cd")), Nothing, drSql("category_cd"))
                TicketRfs.type_cd = IIf(IsDBNull(drSql("type_cd")), Nothing, drSql("type_cd"))

                TicketRfs.priority = IIf(IsDBNull(drSql("priority")), Nothing, drSql("priority"))
                TicketRfs.GroupNum = IIf(IsDBNull(drSql("GroupNum")), Nothing, drSql("GroupNum"))


                TicketRfs.GroupName = IIf(IsDBNull(drSql("GroupName")), Nothing, drSql("GroupName"))
                'GroupNum

                TicketRfs.TechName = IIf(IsDBNull(drSql("TechName")), Nothing, drSql("TechName"))
                TicketRfs.status_desc = IIf(IsDBNull(drSql("status_desc")), Nothing, drSql("status_desc"))

                TicketRfs.bDesc = IIf(IsDBNull(drSql("bDesc")), Nothing, drSql("bDesc"))

                TicketRfs.LevelNum = IIf(IsDBNull(drSql("LevelNum")), Nothing, drSql("LevelNum"))

                TicketRfs.LevelStatus = IIf(IsDBNull(drSql("LevelStatus")), Nothing, drSql("LevelStatus"))
                TicketRfs.CSCFirstTouch = IIf(IsDBNull(drSql("CSCFirstTouch")), Nothing, drSql("CSCFirstTouch"))
                TicketRfs.TotalAcknowledgeTime = IIf(IsDBNull(drSql("TotalAcknowledgeTime")), Nothing, drSql("TotalAcknowledgeTime"))

                _JustReport.Add(TicketRfs)


            Next


        End If
    End Sub
    Public Function GetAllRequests() As List(Of TickectRFSReportDetailItems)

        Dim requestsByGroup = (From req As TickectRFSReportDetailItems In _JustReport _
               Select req).ToList

        Return requestsByGroup

    End Function

    Public Function GetAllRequestByGroupDetails(ByVal iGroup As Integer) As List(Of TickectRFSReportDetailItems)

        Dim requestsByGroup = (From req As TickectRFSReportDetailItems In _JustReport _
                              Where req.GroupNum = iGroup
                       Select req).ToList

        Return requestsByGroup

    End Function
    Public Class TickectRFSReportDetailItems

        Public Property request_num As Integer
        Public Property clientName As String
        Public Property entry_date As String
        Public Property close_date As String
        Public Property request_type As String
        Public Property category_cd As String

        Public Property type_cd As String
        Public Property priority As Integer
        Public Property GroupNum As Integer
        Public Property GroupName As String
        Public Property TechName As String
        Public Property status_desc As String
        Public Property bDesc As String

        Public Property LevelNum As String
        Public Property LevelStatus As String
        Public Property CSCFirstTouch As String


        Public Property OrderbyNum As Integer
        Public Property TimeInQueue As String
        Public Property facility_name As String
        Public Property TotalAcknowledgeTime As String

        'client_num,
        'clientName,
        'entry_date,
        'close_date,
        'request_type,
        'category_cd,
        'type_cd,
        '[priority],
        'current_group_num,
        'GroupName,
        'CurrentTechName,

        'item_cd,
        'group_assign_time,
        'assigned_group_num,
        'tech_assign_time,
        'assigned_tech_num,
        'current_tech_num,
        'acknowledge_time,
        'current_ack_time,
        'asset_tag,
        '[platform],
        'resolution_cd,
        'entry_tech_num,
        'department_num,
        'facility_cd,
        'building_cd,
        'office,
        '[floor],
        'address_1,
        'city,
        '[state],
        'zip,
        'alternate_contact_name,
        'alternate_contact_phone,
        'short_desc,
        'proxy_on_pc,
        'ip_address,
        'facility_name,
        'AssignedTechName 	

    End Class

End Class
