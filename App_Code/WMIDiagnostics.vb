﻿Imports Microsoft.VisualBasic
Imports System.Management


Public Class WMIDiagnostics
    Private _computerName As String
    Private _itemsList As New List(Of String)



    Public Sub New(ByVal computerName As String)

        _computerName = computerName



        GetItemsForComputer()

    End Sub

    Private Function CreateNewManagementScope(ByVal computerName As String) As ManagementScope
        Dim computerString = "\\" + computerName + "\root\cimv2"

        Dim scopeReturn As New ManagementScope(computerString)
        Return scopeReturn

    End Function

    Public Sub GetItemsForComputer()

        Dim scope As ManagementScope = CreateNewManagementScope(_computerName)

        '  Dim query As SelectQuery = New SelectQuery("select * FROM Win32_Product")
        Dim query As SelectQuery = New SelectQuery("SELECT * FROM Win32_Printer")
        Try

            Using searcher As New ManagementObjectSearcher(scope, query)

                Dim items As ManagementObjectCollection = searcher.Get()


                Dim queryObj = From item As ManagementObject In items
                               Select item("Name")



                For Each itm In queryObj
                    'itm.Item

                    '_itemsList.Add((itm("Name")))


                Next





            End Using


        Catch ex As Exception

        End Try


    End Sub


    Public ReadOnly Property ItemsInList As List(Of String)
        Get
            Return _itemsList
        End Get
    End Property


End Class
