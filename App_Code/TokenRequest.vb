﻿Imports Microsoft.VisualBasic
Imports System.ComponentModel


Public Class TokenRequest
    Private _DataConn As String
    Private _AccountRequestNum As String

    Private _TokenName As String
    Private _TokenAddress1 As String
    Private _TokenAddress2 As String
    Private _TokenCity As String
    Private _TokenSt As String
    Private _TokenZip As String
    Private _TokenPhone As String
    Private _Tokencomments As String
    Public Sub New()

    End Sub


    Public Sub New(ByVal AccountRequestNum As String, ByVal DataConn As String)
        _AccountRequestNum = AccountRequestNum
        _DataConn = DataConn
        GetValues()
    End Sub

    Private Sub GetValues()



        Dim cmdSql As New SqlCommand
        Dim connSql As New SqlConnection(_DataConn)
        Dim drSql As SqlClient.SqlDataReader



        '   connSql.ConnectionString = _DataConn

        cmdSql.CommandText = "SelTokenAddress"
        cmdSql.CommandType = CommandType.StoredProcedure
        cmdSql.Connection = connSql
        cmdSql.Parameters.Add("@account_request_num", SqlDbType.Int).Value = _AccountRequestNum


        connSql.Open()

        Try




            drSql = cmdSql.ExecuteReader


            While drSql.Read()



                _AccountRequestNum = IIf(IsDBNull(drSql("account_request_num")), Nothing, drSql("account_request_num"))

                _TokenName = IIf(IsDBNull(drSql("TokenName")), Nothing, drSql("TokenName"))
                _TokenAddress1 = IIf(IsDBNull(drSql("TokenAddress1")), Nothing, drSql("TokenAddress1"))
                _TokenAddress2 = IIf(IsDBNull(drSql("TokenAddress2")), Nothing, drSql("TokenAddress2"))


                _TokenCity = IIf(IsDBNull(drSql("TokenCity")), Nothing, drSql("TokenCity"))
                _TokenSt = IIf(IsDBNull(drSql("TokenSt")), Nothing, drSql("TokenSt"))
                _TokenZip = IIf(IsDBNull(drSql("TokenZip")), Nothing, drSql("TokenZip"))

                _TokenPhone = IIf(IsDBNull(drSql("TokenPhone")), Nothing, drSql("TokenPhone"))
                _Tokencomments = IIf(IsDBNull(drSql("Tokencomments")), Nothing, drSql("Tokencomments"))




            End While

        Catch ex As Exception

            Throw ex
        End Try
    End Sub
    Public ReadOnly Property AccountRequestNum As String
        Get
            Return _AccountRequestNum
        End Get
    End Property
    Public ReadOnly Property TokenName As String
        Get
            Return _TokenName
        End Get
    End Property
    Public ReadOnly Property TokenAddress1 As String
        Get
            Return _TokenAddress1
        End Get
    End Property
    Public ReadOnly Property TokenAddress2 As String
        Get
            Return _TokenAddress2
        End Get
    End Property
    Public ReadOnly Property TokenCity As String
        Get
            Return _TokenCity
        End Get
    End Property
    Public ReadOnly Property TokenSt As String
        Get
            Return _TokenSt
        End Get
    End Property
    Public ReadOnly Property TokenZip As String
        Get
            Return _TokenZip
        End Get
    End Property
    Public ReadOnly Property TokenPhone As String
        Get
            Return _TokenPhone
        End Get
    End Property
    Public ReadOnly Property Tokencomments As String
        Get
            Return _Tokencomments
        End Get
    End Property


End Class
