﻿Imports Microsoft.VisualBasic
Imports App_Code
Imports System.Web.HttpContext

Public Class ActionController
    Private _client_num As Integer
    Private _dataConn As String
    Private _AllActions As New List(Of Actions)
    Public Sub New()

    End Sub
    Public ReadOnly Property AllActions As List(Of Actions)
        Get
            Return _AllActions

        End Get
    End Property

    Public Sub GetAllActions(ByRef sRequestnum As Integer, ByRef sConn As String)
        If sConn Is Nothing Then
            sConn = HttpContext.Current.Session("EmployeeConn")
        End If

        Using SqlConnection As New SqlConnection(sConn)
            Dim Cmd As New SqlCommand()
            Cmd.CommandText = "SelAccountActionItemsByRequest"
            Cmd.Parameters.AddWithValue("@account_request_num", sRequestnum)

            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.Connection = SqlConnection

            SqlConnection.Open()
            Using Cmd

                Using dr = Cmd.ExecuteReader()

                    If dr.HasRows Then

                        Do While dr.Read
                            Dim action As New Actions

                            action.AccountRequestNum = IIf(IsDBNull(dr("account_request_num")), Nothing, dr("account_request_num"))
                            action.AcctSeqNum = IIf(IsDBNull(dr("account_request_seq_num")), Nothing, dr("account_request_seq_num"))
                            action.ActionNum = IIf(IsDBNull(dr("action_num")), Nothing, dr("action_num"))
                            action.ActionCd = IIf(IsDBNull(dr("action_cd")), Nothing, dr("action_cd"))

                            action.ActionCodeDesc = IIf(IsDBNull(dr("ActionCodeDesc")), Nothing, dr("ActionCodeDesc"))
                            action.ActionDate = IIf(IsDBNull(dr("action_date")), Nothing, dr("action_date"))

                            action.TechClientNum = IIf(IsDBNull(dr("tech_client_num")), Nothing, dr("tech_client_num"))
                            action.TechName = IIf(IsDBNull(dr("TechName")), Nothing, dr("TechName"))
                            action.ActionDesc = IIf(IsDBNull(dr("action_desc")), Nothing, dr("action_desc"))

                            _AllActions.Add(action)

                        Loop
                    End If

                End Using
            End Using
        End Using
    End Sub


End Class
