﻿Imports Microsoft.VisualBasic

Public Class DisableControls
    Property ctrl As Control

    Public Sub New(ByRef ctrl As Control)

        _ctrl = ctrl

        DisableControls(_ctrl)

    End Sub

    Private Sub DisableControls(ByRef ctrl As Control)

        For Each c As Control In ctrl.Controls

            If TypeOf c Is TextBox Then

                Dim txt As TextBox = DirectCast(c, TextBox)

                txt.Enabled = False
                txt.Attributes.Remove("CssClass")


            End If

            If TypeOf c Is DropDownList Then

                Dim ddl As DropDownList = DirectCast(c, DropDownList)

                ddl.Enabled = False

            End If


            If TypeOf c Is RadioButtonList Then

                Dim rbl As RadioButtonList = DirectCast(c, RadioButtonList)

                rbl.Enabled = False

            End If


            DisableControls(c)

        Next



    End Sub

End Class
