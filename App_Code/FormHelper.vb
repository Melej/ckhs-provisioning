﻿Imports Microsoft.VisualBasic
Imports System.Reflection
Imports App_Code


Public Class FormHelper

    Dim _DataConn As String
    Dim Cmd As New SqlCommand()
    Dim _ReturnOutParamValue As String




    Public Sub New()

        Dim conn As New SetDBConnection()
        _DataConn = conn.EmployeeConn

    End Sub


    Public ReadOnly Property OutParamValue As String
        Get
            Return _ReturnOutParamValue
        End Get
    End Property





    Public Sub FillForm(ByRef obj As Object, ByRef ParentControl As Control, Optional ByVal Identifier As String = "")

        For Each prop As PropertyInfo In obj.GetType().GetProperties()

            Dim c As New Client3


            Dim sCurrentControlName As String

            'Loop Throught properties and search fields based on naming convention

            '--------------------------
            ' Search TextBoxes 
            '--------------------------


            sCurrentControlName = "txt" & Identifier & prop.Name
            Dim txt As New TextBox

            txt = DataCheckingAndValidation.FindWebControlRecursive(ParentControl, sCurrentControlName)


            If txt IsNot Nothing And prop.GetValue(obj, Nothing) IsNot Nothing Then

                txt.Text = prop.GetValue(obj, Nothing)


            End If


            '--------------------------
            ' Search Labels 
            '--------------------------


            sCurrentControlName = "lbl" & Identifier & prop.Name
            Dim lbl As New Label

            lbl = DataCheckingAndValidation.FindWebControlRecursive(ParentControl, sCurrentControlName)


            If lbl IsNot Nothing And prop.GetValue(obj, Nothing) IsNot Nothing Then

                lbl.Text = prop.GetValue(obj, Nothing)


            End If


            '--------------------------
            ' Search DropDownLists 
            '--------------------------


            sCurrentControlName = "ddl" & Identifier & prop.Name
            Dim ddl As New DropDownList

            ddl = DataCheckingAndValidation.FindWebControlRecursive(ParentControl, sCurrentControlName)


            If ddl IsNot Nothing And prop.GetValue(obj, Nothing) IsNot Nothing Then

                ddl.SelectedValue = prop.GetValue(obj, Nothing)


            End If


            '--------------------------
            ' Search Radiobutton List 
            '--------------------------


            sCurrentControlName = "rbl" & Identifier & prop.Name
            Dim rbl As New RadioButtonList

            rbl = DataCheckingAndValidation.FindWebControlRecursive(ParentControl, sCurrentControlName)


            If rbl IsNot Nothing And prop.GetValue(obj, Nothing) IsNot Nothing Then

                rbl.SelectedValue = prop.GetValue(obj, Nothing)


            End If

        Next

    End Sub


    Public Sub InitializeUpdateForm(ByRef sInsertUpdateProc As String, ByRef ParentControl As Control, Optional ByVal Identifier As String = "")

        Dim sParamName As String
        Dim sCurrentControlName As String
        Dim blnFound As Boolean = False

        Cmd.CommandText = sInsertUpdateProc
        Cmd.CommandType = CommandType.StoredProcedure

        '--------------------------------------------------------
        'Table to hold input parameters for stored procedure
        '--------------------------------------------------------

        For Each drParam In GetInputParameters(sInsertUpdateProc).Rows

            sParamName = drParam("ParameterName")
            sParamName = sParamName.Replace("@", "")

            blnFound = False

            sCurrentControlName = "txt" & Identifier & sParamName
            Dim txt As New TextBox

            txt = DataCheckingAndValidation.FindWebControlRecursive(ParentControl, sCurrentControlName)


            If txt IsNot Nothing Then
                If txt.Text <> "" Then

                    Cmd.Parameters.AddWithValue("@" & sParamName, txt.Text)
                    blnFound = True

                End If

            End If


            '--------------------------
            ' Search Labels 
            '--------------------------


            sCurrentControlName = "lbl" & Identifier & sParamName
            Dim lbl As New Label

            lbl = DataCheckingAndValidation.FindWebControlRecursive(ParentControl, sCurrentControlName)


            If lbl IsNot Nothing And blnFound = False Then
                If lbl.Text <> "" Then

                    Cmd.Parameters.AddWithValue("@" & sParamName, lbl.Text)
                    blnFound = True

                End If
               

            End If


            '--------------------------
            ' Search DropDownLists 
            '--------------------------


            sCurrentControlName = "ddl" & Identifier & sParamName
            Dim ddl As New DropDownList

            ddl = DataCheckingAndValidation.FindWebControlRecursive(ParentControl, sCurrentControlName)


            If ddl IsNot Nothing And blnFound = False Then
                If ddl.SelectedIndex > -1 Then
                    Cmd.Parameters.AddWithValue("@" & sParamName, ddl.SelectedValue)
                    blnFound = True
                End If



            End If


            '--------------------------
            ' Search Radiobutton List 
            '--------------------------


            sCurrentControlName = "rbl" & Identifier & sParamName
            Dim rbl As New RadioButtonList

            rbl = DataCheckingAndValidation.FindWebControlRecursive(ParentControl, sCurrentControlName)


            If rbl IsNot Nothing And blnFound = False Then

                If rbl.SelectedIndex > -1 Then

                    Cmd.Parameters.AddWithValue("@" & sParamName, rbl.SelectedValue)
                    blnFound = True

                End If

               


            End If



        Next














    End Sub

    Public Sub AddCmdParameter(ByVal Parameter As String, ByVal Value As String)

        Cmd.Parameters.AddWithValue(Parameter, Value)

    End Sub


    Public Sub UpdateForm()

        If Cmd IsNot Nothing Then

            Using sqlConn As New SqlConnection(_DataConn)

                Cmd.Connection = sqlConn

                sqlConn.Open()

                Cmd.ExecuteNonQuery()

                sqlConn.Close()


            End Using

        End If

      

    End Sub


    Public Sub UpdateForm(ByVal OutParameter As String)

        If Cmd IsNot Nothing Then

            Using sqlConn As New SqlConnection(_DataConn)

                Cmd.Connection = sqlConn

                Dim outParam As New SqlParameter(OutParameter, DbType.String)
                outParam.Direction = ParameterDirection.Output

                Cmd.Parameters.Add(outParam)




                sqlConn.Open()

                Cmd.ExecuteNonQuery()

                _ReturnOutParamValue = Cmd.Parameters(OutParameter).Value.ToString()

                sqlConn.Close()


            End Using

        End If

    End Sub


    Private Function GetInputParameters(ByVal sProc As String) As DataTable

        Dim _inputParams As New DataTable
        Dim myCommand As New SqlCommand
        Dim myConnection As New SqlConnection

        _inputParams.Columns.Add("ParameterName", GetType(String))
        _inputParams.Columns.Add("ParameterType", GetType(String))
        _inputParams.Columns.Add("ParameterSize", GetType(String))


        myConnection.ConnectionString = _DataConn


        myCommand.Connection = myConnection
        myCommand.CommandText = sProc
        myCommand.CommandType = Data.CommandType.StoredProcedure

        myConnection.Open()

        SqlCommandBuilder.DeriveParameters(myCommand)

        myConnection.Close()

        For Each param As SqlParameter In myCommand.Parameters
            If param.Direction = Data.ParameterDirection.Input OrElse param.Direction = Data.ParameterDirection.InputOutput Then

                _inputParams.Rows.Add(param.ParameterName.ToString, param.SqlDbType.ToString(), param.Size.ToString)



            End If
        Next

        Return _inputParams
    End Function

    Public Function BuildListTable(ByRef obj As Object) As Table
        Dim tblReturn As New Table





        Return tblReturn
    End Function

End Class
