﻿Imports Microsoft.VisualBasic
Imports System.Web
Imports System.Web.HttpContext
Imports System.Web.HttpRequest


Public Class Environment
    Dim sWebServer As String = ""

    Public Sub New()

        sWebServer = getServer()

    End Sub

    Public Function getEnvironment() As String
        Dim sReturn As String

        Dim sSwitch As String


        sSwitch = ConfigurationManager.AppSettings.Get("PointToServer")


        If sSwitch = "on" Then


            sWebServer = ConfigurationManager.AppSettings.Get("PointToServerName")

        End If

        If sWebServer = ConfigurationManager.AppSettings.Get("ProdWebServers") Then

            sReturn = "Production"

        ElseIf sWebServer = ConfigurationManager.AppSettings.Get("TestWebServers") Then

            sReturn = "Testing"

        Else

            sReturn = "Testing"

        End If


        Return sReturn

    End Function

    Public Function getServer() As String
        Dim strReturn As String





        strReturn = HttpContext.Current.Request.ServerVariables("SERVER_NAME")

        Return strReturn
    End Function

End Class
