﻿Imports Microsoft.VisualBasic

Public Class Globals

    Public Shared Function FindControlRecursive(
    ByVal rootControl As Control, ByVal controlID As String) As Control

        If rootControl.ID = controlID Then
            Return rootControl
        End If

        For Each controlToSearch As Control In rootControl.Controls
            Dim controlToReturn As Control =
                FindControlRecursive(controlToSearch, controlID)
            If controlToReturn IsNot Nothing Then
                Return controlToReturn
            End If
        Next
        Return Nothing
    End Function


End Class
