﻿Imports Microsoft.VisualBasic
Imports App_Code
Imports System.ComponentModel

Public Class AllMyApplications
    Private _AllMyApps As New List(Of MyApplication)
    Private _AllMyApplications As New List(Of MyApplication)


    Private _dataConn As String
    Private _sLogin As String
    Private _sDomainAndLogin As String
    Private _arrDomainAndLogin() As String


    Public Sub New(ByVal AppGroup As String)
        Dim conn As New SetDBConnection()
        _dataConn = conn.EmployeeConn

        If GlobalUserAccount.ToString = "" Or GlobalUserAccount.ToString Is Nothing Then
            _sDomainAndLogin = HttpContext.Current.Request.ServerVariables("REMOTE_USER") 'Request.Url.AbsolutePath()
            If _sDomainAndLogin.IndexOf("\") > -1 Then
                _arrDomainAndLogin = Split(_sDomainAndLogin, "\")
                _sLogin = _arrDomainAndLogin(1)
            Else
                _sLogin = _sDomainAndLogin

            End If

            '_arrDomainAndLogin = Split(_sDomainAndLogin, "\")

            '_sLogin = _arrDomainAndLogin(1)
        Else
            _sLogin = GlobalUserAccount.ToString

        End If


        Using SqlConnection As New SqlConnection(_dataConn)
            Dim Cmd As New SqlCommand()
            Cmd.CommandText = "SelApplicationCodesBySecurity"
            Cmd.Parameters.AddWithValue("@nt_login", _sLogin)
            Cmd.Parameters.AddWithValue("@ApplicationGroup", AppGroup)

            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.Connection = SqlConnection


            SqlConnection.Open()
            Using Cmd

                Using dr = Cmd.ExecuteReader()

                    If dr.HasRows Then

                        Do While dr.Read
                            Dim app As New MyApplication

                            app.ApplicationNum = IIf(IsDBNull(dr("ApplicationNum")), Nothing, dr("ApplicationNum"))
                            app.ApplicationDesc = IIf(IsDBNull(dr("ApplicationDesc")), Nothing, dr("ApplicationDesc"))
                            app.AppBase = IIf(IsDBNull(dr("AppBase")), Nothing, dr("AppBase"))
                            app.AppSystem = IIf(IsDBNull(dr("AppSystem")), Nothing, dr("AppSystem"))
                            app.DateDeactivated = IIf(IsDBNull(dr("DateDeactivated")), Nothing, dr("DateDeactivated"))
                            app.AccountTypeCd = IIf(IsDBNull(dr("account_type_cd")), Nothing, dr("account_type_cd"))
                            app.ApplicationGroup = IIf(IsDBNull(dr("ApplicationGroup")), Nothing, dr("ApplicationGroup"))

                            _AllMyApplications.Add(app)

                        Loop
                    End If

                End Using
            End Using
        End Using

    End Sub

    Public Sub GetAllMyApps(ByRef sConn As String, ByVal AppGroup As String)
        If sConn Is Nothing Then
            sConn = HttpContext.Current.Session("EmployeeConn")
        End If

        If HttpContext.Current.Session("LoginID") = "" Or HttpContext.Current.Session("LoginID") Is Nothing Then
            Dim sDomainAndLogin As String = HttpContext.Current.Request.ServerVariables("REMOTE_USER") 'Request.Url.AbsolutePath()
            Dim arrDomainAndLogin As String() = Split(sDomainAndLogin, "\")

            _sLogin = arrDomainAndLogin(1)
        Else
            _sLogin = HttpContext.Current.Session("LoginID")

        End If

        Using SqlConnection As New SqlConnection(sConn)
            Dim Cmd As New SqlCommand()
            Cmd.CommandText = "SelApplicationCodesBySecurity"
            Cmd.Parameters.AddWithValue("@nt_login", _sLogin)
            Cmd.Parameters.AddWithValue("@ApplicationGroup", AppGroup)

            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.Connection = SqlConnection


            SqlConnection.Open()
            Using Cmd

                Using dr = Cmd.ExecuteReader()

                    If dr.HasRows Then

                        Do While dr.Read
                            Dim Myapp As New MyApplication

                            Myapp.ApplicationNum = IIf(IsDBNull(dr("ApplicationNum")), Nothing, dr("ApplicationNum"))
                            Myapp.ApplicationDesc = IIf(IsDBNull(dr("ApplicationDesc")), Nothing, dr("ApplicationDesc"))
                            Myapp.AppBase = IIf(IsDBNull(dr("AppBase")), Nothing, dr("AppBase"))
                            Myapp.AppSystem = IIf(IsDBNull(dr("AppSystem")), Nothing, dr("AppSystem"))
                            Myapp.DateDeactivated = IIf(IsDBNull(dr("DateDeactivated")), Nothing, dr("DateDeactivated"))
                            Myapp.AccountTypeCd = IIf(IsDBNull(dr("account_type_cd")), Nothing, dr("account_type_cd"))
                            Myapp.ApplicationGroup = IIf(IsDBNull(dr("ApplicationGroup")), Nothing, dr("ApplicationGroup"))

                            _AllMyApps.Add(Myapp)

                        Loop
                    End If

                End Using
            End Using
        End Using
    End Sub
    Public ReadOnly Property AllMyApps As List(Of MyApplication)
        Get
            Return _AllMyApps

        End Get
    End Property
    Public Function AppBySecurityGroup(ByVal sGroup As String) As List(Of MyApplication)

        Dim MyApps = (From Myapp As MyApplication In _AllMyApplications
                    Where Myapp.AppBase = Myapp.ApplicationNum And
                    Myapp.ApplicationGroup = sGroup
                    Select Myapp).ToList

        ' appReturn = query

        Return MyApps
    End Function

    Public Function ApplicationsNewByBase(ByVal iBaseNum As Integer) As List(Of MyApplication)
        'Dim appReturn As Application

        Dim MyApps = (From Myapp As MyApplication In _AllMyApplications
                    Where Myapp.AppBase = iBaseNum
                    Select Myapp).ToList

        ' appReturn = query

        Return MyApps
    End Function
    'Public Function ApplicationsNewByBase(ByVal iBaseNum As Integer, ByVal sGroup As String) As List(Of MyApplication)
    '    'Dim appReturn As Application

    '    Dim MyApps = (From Myapp As MyApplication In _AllMyApplications
    '                Where Myapp.AppBase = iBaseNum And
    '                   Myapp.ApplicationGroup = sGroup
    '                Select Myapp).ToList

    '    ' appReturn = query

    '    Return MyApps
    'End Function
End Class
Public Class MyApplication
    Public Property ApplicationNum As String
    Public Property ApplicationDesc As String
    Public Property AppBase As String
    Public Property AppSystem As String
    Public Property DateDeactivated As DateTime
    Public Property AccountTypeCd As String
    Public Property ApplicationGroup As String

    'Private _ApplicationNum As Integer
    'Private _ApplicationDesc As String
    'Private _AppBase As String
    'Private _AppSystem As String
    'Private _DateDeactivated As String
    'Private _AccountTypeCd As String
    'Private _ApplicationGroup As String

    '<DisplayName("ApplicationNum")> Property ApplicationNum As Integer
    '    Get
    '        Return _ApplicationNum
    '    End Get
    '    Set(value As Integer)
    '        _ApplicationNum = value
    '    End Set
    'End Property
    '<DisplayName("ApplicationDesc")> Property ApplicationDesc As String
    '    Get
    '        Return _ApplicationDesc
    '    End Get
    '    Set(value As String)
    '        _ApplicationDesc = value
    '    End Set
    'End Property
    '<DisplayName("AppBase")> Property AppBase As String
    '    Get
    '        Return _AppBase
    '    End Get
    '    Set(value As String)
    '        _AppBase = value
    '    End Set
    'End Property
    '<DisplayName("AppSystem")> Property AppSystem As String
    '    Get
    '        Return _AppSystem
    '    End Get
    '    Set(value As String)
    '        _AppSystem = value
    '    End Set
    'End Property
    '<DisplayName("DateDeactivated")> Property DateDeactivated As String
    '    Get
    '        Return _DateDeactivated
    '    End Get
    '    Set(value As String)
    '        _DateDeactivated = value
    '    End Set
    'End Property
    '<DisplayName("AccountTypeCd")> Property AccountTypeCd As String
    '    Get
    '        Return _AccountTypeCd
    '    End Get
    '    Set(value As String)
    '        _AccountTypeCd = value
    '    End Set
    'End Property
    '<DisplayName("ApplicationGroup")> Property ApplicationGroup As String
    '    Get
    '        Return _ApplicationGroup
    '    End Get
    '    Set(value As String)
    '        _ApplicationGroup = value
    '    End Set
    'End Property
End Class
