﻿Imports Microsoft.VisualBasic

Public Class RequiredField
    Private _ctrl As WebControl
    Private _ctrlDesc As String
    Private _cssClasses As String
    Public Sub New(ByRef ctrl As WebControl, ByVal ctrlDesc As String)

        _ctrl = ctrl
        _ctrlDesc = ctrlDesc
        _cssClasses = "textInputRequired"
        _ctrl.Attributes.Add("alt", ctrlDesc)
        ctrl.CssClass = ctrl.CssClass & " " & _cssClasses

    End Sub

    Public Sub RemoveCssClasses()

        _cssClasses = _cssClasses.Replace("textInputRequired", "")
        _ctrl.CssClass = _cssClasses

    End Sub
    ReadOnly Property FieldDesc As String
        Get

            Return _ctrlDesc
        End Get
    End Property

    ReadOnly Property Field As WebControl
        Get

            Return _ctrl

        End Get
    End Property

    Public Function isValid() As Boolean
        Dim blnReturn As Boolean = False


        If TypeOf _ctrl Is TextBox Then

            Dim txt As TextBox = DirectCast(_ctrl, TextBox)

            If txt.Text.Length > 0 Then

                blnReturn = True

            End If


        End If

        If TypeOf _ctrl Is DropDownList Then

            Dim ddl As DropDownList = DirectCast(_ctrl, DropDownList)


            If ddl.SelectedIndex > 0 Then

                blnReturn = True

            End If

        End If


        If TypeOf _ctrl Is RadioButtonList Then

            Dim rbl As RadioButtonList = DirectCast(_ctrl, RadioButtonList)

            If rbl.SelectedIndex > 0 Then

                blnReturn = True

            End If


        End If


        'If TypeOf  _ctrl Is CheckBoxList Then

        '    Dim cbl As CheckBoxList = DirectCast( _ctrl, CheckBoxList)

        '    For Each li As ListItem In cbl.Items

        '        li.Enabled = False
        '    Next

        'End If


        Return blnReturn
    End Function

End Class
