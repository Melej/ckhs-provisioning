Imports System.Web.UI
Imports System.Configuration
Imports System.Web.Configuration

Namespace App_Code

    Public Module GlobalValues
        'P'''ublic SessionSecurityType As SecurityTypes = SecurityTypes.General no good would be over written with multiple sessions
        Public gAppDataConn As String = ""
        Public GlobalDataConn As String = ""
        Public GlobalUserAccount As String = ""
        Public gHelpDataConn As String = ""
        Public gSecurityDataConn As String = ""
        Public gCurrentSecurityType As SecurityTypes
        Public gEnvironment As String = "test" 'or production
        Public gAppName As String = "casemgr"
        Public gHelpWebSite As String = ""
        Public gWebRoot As String = ""
        Public gHospital As String = ""
        Public Enum WavePaths
            InPatient
            OutPatient
            SavedOutPatient
        End Enum
        Public Enum Hospitals
            DcmhProduction
            DcmhDevelopment
            CrozerProduction
            CrozerDevelopment
            TaylorDevelopment
            TaylorProduction
        End Enum
        Public Enum ReportServer
            DcmhWebdev8
            CrozerWebdev8
            TaylorWebdev8
            DevelopmentTest
            DefaultServer
        End Enum
        Public Enum PageNav
            FirstPage
            PreviousPage
            NextPage
            LastPage
            GotoPage
        End Enum
        Public Enum SecurityTypes 'Levels
            NonInterface    '       0       
            General          '     10
            Housekeeping      '    10
            Transport         '   10
            TransportSup       '  30
            HouseSup            ' 30
            NurseStation         '45 & 47
            NurseAdmin     '      50  
            HouseAdmin      '     50
            TransportAdmin   '    50
            PFC               '   75
            HospitalAdmin      '  75
            SysAdmin            ' 100
            Admissions 'not in use at this time
            CaseManager
            CaseAdmin
            CaseViewer
            Physician
            SocialAdmin
            SocialW
            CasePsychManager
        End Enum
        Public Function setGlobalConn(ByVal sEnvironment As String) As String
            ' Dim sEnvironment As String = "test" 'or Production
            If sEnvironment.ToLower = "production" Then
                GlobalDataConn = ConfigurationManager.AppSettings.Get("ProductionEmployee.ConnectionString").ToString()
                ' gHelpDataConn = ConfigurationManager.AppSettings.Get("HelpServerDBProduction")
            Else 'test
                'new way
                GlobalDataConn = ConfigurationManager.AppSettings.Get("TestEmployee.ConnectionString").ToString()
            End If

            Return gHelpWebSite
        End Function
        Public Function setHelpWebSite(ByVal sEnvironment As String) As String
            ' Dim sEnvironment As String = "test" 'or Production
            If sEnvironment.ToLower = "production" Then
                gHelpWebSite = ConfigurationManager.AppSettings.Get("eWebHelpProduction").ToString()
                ' gHelpDataConn = ConfigurationManager.AppSettings.Get("HelpServerDBProduction")
            Else 'test
                'new way
                gHelpWebSite = ConfigurationManager.AppSettings.Get("eWebHelpTest").ToString()
            End If

            Return gHelpWebSite
        End Function

        Public Function setHelpConnString(ByVal sEnvironment As String) As String
            'Old used for internal help before website created
            ' Dim sEnvironment As String = "test" 'or Production
            If sEnvironment.ToLower = "production" Then
                gHelpDataConn = ConfigurationManager.ConnectionStrings("HelpDbServerProduction.ConnectionString").ToString()
                ' gHelpDataConn = ConfigurationManager.AppSettings.Get("HelpServerDBProduction")
            Else 'test
                'new way
                gHelpDataConn = ConfigurationManager.ConnectionStrings("HelpDbServerTest.ConnectionString").ToString()
            End If

            Return gHelpDataConn
        End Function
        Public Function setSecurityConnString() As String
            If gWebRoot.ToLower.IndexOf("webdev3") > -1 Or gWebRoot.ToLower.IndexOf("localhost") > -1 Then ' test site
                HttpContext.Current.Session("SecurityDataConn") = ConfigurationManager.ConnectionStrings("testSecurity.ConnectionString").ToString()
            Else 'Production site
                If HttpContext.Current.Session("DataConn").ToLower.IndexOf("dcmh") > -1 Then

                    'Session("SecurityDataConn") = ConfigurationManager.AppSettings.Get("DcmhSecurity.ConnectionString")
                    HttpContext.Current.Session("SecurityDataConn") = ConfigurationManager.ConnectionStrings("DcmhSecurity.ConnectionString").ToString()
                ElseIf HttpContext.Current.Session("DataConn").ToLower.IndexOf("taylor") > -1 Then
                    HttpContext.Current.Session("SecurityDataConn") = ConfigurationManager.ConnectionStrings("TaylorSecurity.ConnectionString").ToString()
                ElseIf HttpContext.Current.Session("DataConn").ToLower.IndexOf("springfield") > -1 Then
                    HttpContext.Current.Session("SecurityDataConn") = ConfigurationManager.ConnectionStrings("SpringfieldSecurity.ConnectionString").ToString()

                ElseIf HttpContext.Current.Session("DataConn").ToLower.IndexOf("ccmc") > -1 Or HttpContext.Current.Session("DataConn").ToLower.IndexOf("crozer") > -1 Then
                    HttpContext.Current.Session("SecurityDataConn") = ConfigurationManager.ConnectionStrings("CrozerSecurity.ConnectionString").ToString()
                Else 'test
                    HttpContext.Current.Session("SecurityDataConn") = ConfigurationManager.ConnectionStrings("testSecurity.ConnectionString").ToString()
                End If
            End If
            Return HttpContext.Current.Session("SecurityDataConn")
        End Function
    End Module

    'End Namespace
End Namespace