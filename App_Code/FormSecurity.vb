﻿Imports Microsoft.VisualBasic

Public Class FormSecurity
    Private _user As UserSecurity

    Public Sub New(ByVal userSecurity As UserSecurity)
        _user = userSecurity

    End Sub

    Public Sub DisableWebControlByRole(ByRef webctrl As WebControl, ByVal sAuthRolesByName As String)
        Dim sRoles As String()
        Dim blnAuthorized As Boolean = False

        sRoles = Split(sAuthRolesByName, ",")

        For i As Integer = 0 To sRoles.Length - 1

            'If _user.UserRoles.Find(Function(x) x.RoleName = sRoles(i)) IsNot Nothing Then

            '    blnAuthorized = True

            '    Exit For

            'End If

            i = i + 1

        Next


        If blnAuthorized Then

            webctrl.Enabled = True

        Else
            webctrl.Enabled = False
            webctrl.Attributes.Add("class", "masterTooltip")
            webctrl.Attributes.Add("title", "You Do No Have Access To This Function")

        End If

    End Sub
    'Public Sub HideWebControlByRole(ByRef webctrl As WebControl, ByVal sAuthRolesByName As String)

    '    Dim sRoles As String()
    '    Dim blnAuthorized As Boolean = False

    '    sRoles = Split(sAuthRolesByName, ",")

    '    For i As Integer = 0 To sRoles.Length - 1

    '        If _user.UserRoles.Find(Function(x) x.RoleName = sRoles(i)) IsNot Nothing Then

    '            blnAuthorized = True

    '            Exit For

    '        End If

    '        i = i + 1

    '    Next


    '    If blnAuthorized Then

    '        webctrl.Visible = True

    '    Else
    '        webctrl.Visible = False


    '    End If

    'End Sub
    'Public Sub HideAjaxAccMenuByRole(ByRef accPane As AjaxControlToolkit.AccordionPane, ByVal sAuthRolesByName As String)

    '    Dim sRoles As String()
    '    Dim blnAuthorized As Boolean = False

    '    sRoles = Split(sAuthRolesByName, ",")

    '    For i As Integer = 0 To sRoles.Length - 1

    '        If _user.UserRoles.Find(Function(x) x.RoleName = sRoles(i)) IsNot Nothing Then

    '            blnAuthorized = True

    '            Exit For

    '        End If

    '        i = i + 1

    '    Next


    '    If blnAuthorized Then

    '        accPane.Visible = True

    '    Else
    '        accPane.Visible = False


    '    End If

    'End Sub
End Class
