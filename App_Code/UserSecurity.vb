﻿
Imports Microsoft.VisualBasic
Imports App_Code
Imports System.Data

Public Class UserSecurity
    Private _SecurityType As String
    Private _SecurityLevel As String
    Private _SecurityTable As DataTable
    Private _NtLogin As String
    Private _DataConn As String
    Private _CSCConn As String
    Private _location As String
    Private _TechNum As Integer
    Private _SecurityLevelNum As String
    Private _ClientNum As Integer
    Private _UserName As String
    Private _EntityCd As String
    Private _DepartmentCd As String
    Private _DepartmentName As String
    Private _UserType As String
    Private _email As String
    Private _validateCount As Integer
    Private _unassignedTickets As Integer
    Private _notechcount As Integer
    Private _P1count As Integer
    Private _Audit As Integer
    Private _DefaultApplicationGroup As String
    Private _AppOwnership As New List(Of Integer)
    Private _AllMyGroups As New List(Of Integer)
    Private _AllgroupsStr As New List(Of String)

    'Private _UserRoles As New List(Of UserRole)

    Public Sub New(ByVal NtLogin As String)
        '   Dim dataConn As New SetDBConnection()
        '   _DataConn = dataConn.setConnection()

        _NtLogin = NtLogin

        GetSecurityTable()

        GetSecurityValues()

        'getUserRoles()
    End Sub
    Public Sub New(ByVal NtLogin As String, ByVal sDataConn As String)
        '   Dim dataConn As New SetDBConnection()
        '   
        _DataConn = sDataConn

        _NtLogin = NtLogin

        GetSecurityTable()

        GetSecurityValues()

        'getUserRoles()

    End Sub
    Public Sub New(ByVal NtLogin As String, ByVal sDataConn As String, ByVal Location As String)
        '   Dim dataConn As New SetDBConnection()
        '   
        _DataConn = sDataConn

        _NtLogin = NtLogin

        _location = Location

        GetSecurityTable()

        GetSecurityValues()

        'getUserRoles()

    End Sub
	Private Sub GetSecurityTable()

        If _NtLogin <> "helpdesk" Then
            Dim SecData As New CommandsSqlAndOleDb("SeluserSecurityV8", _DataConn)

            SecData.AddSqlProcParameter("@nt_Login", _NtLogin, SqlDbType.NVarChar, 20)

            SecData.AddSqlProcParameter("@location", _location, SqlDbType.NVarChar, 25)

            _SecurityTable = SecData.GetSqlDataTable
        End If





    End Sub

	Private Sub GetSecurityValues()
        Dim rowSecurity As DataRow

        If _SecurityTable Is Nothing Then

            HttpContext.Current.Response.Redirect("~/Security.aspx", True)

        Else



            For Each rowSecurity In _SecurityTable.Rows


                _ClientNum = rowSecurity("client_num")
                _TechNum = IIf(IsDBNull(rowSecurity("tech_num")), "", rowSecurity("tech_num"))
                _SecurityLevelNum = IIf(IsDBNull(rowSecurity("security_level_num")), "", (rowSecurity("security_level_num")))
                _UserName = rowSecurity("first_name") & " " & rowSecurity("last_name")
                _EntityCd = IIf(IsDBNull(rowSecurity("entity_cd")), "", rowSecurity("entity_cd"))
                _DepartmentCd = IIf(IsDBNull(rowSecurity("department_cd")), "", rowSecurity("department_cd"))
                _DepartmentName = IIf(IsDBNull(rowSecurity("department_name")), "", rowSecurity("department_name"))
                _UserType = IIf(IsDBNull(rowSecurity("UserType")), "", rowSecurity("UserType"))
                _email = IIf(IsDBNull(rowSecurity("e_mail")), "", rowSecurity("e_mail"))
                _NtLogin = IIf(IsDBNull(rowSecurity("nt_login")), "", rowSecurity("nt_login"))
                _validateCount = IIf(IsDBNull(rowSecurity("validateCount")), "", rowSecurity("validateCount"))
                _unassignedTickets = IIf(IsDBNull(rowSecurity("unassignedTickets")), "", rowSecurity("unassignedTickets"))
                _notechcount = IIf(IsDBNull(rowSecurity("notechcount")), "", rowSecurity("notechcount"))
                _DefaultApplicationGroup = IIf(IsDBNull(rowSecurity("ApplicationGroup")), Nothing, rowSecurity("ApplicationGroup"))
                _P1count = IIf(IsDBNull(rowSecurity("P1Count")), Nothing, rowSecurity("P1Count"))
                _Audit = IIf(IsDBNull(rowSecurity("Audit")), Nothing, rowSecurity("Audit"))
            Next
            getAppOwnership()
            'GetMySecQueues()
        End If

    End Sub

    Private Sub getAppOwnership()
        Dim thisdata As New CommandsSqlAndOleDb("SelOwnerApplicationsByTechNum", _DataConn)

        thisdata.AddSqlProcParameter("@tech_num", _TechNum, SqlDbType.NVarChar, 20)
        Dim dt As DataTable = thisdata.GetSqlDataTable


        For Each dr In dt.Rows

            Dim appBase As Integer

            appBase = dr("AppBase")
            _AppOwnership.Add(appBase)

        Next


    End Sub
    'Private Sub getUserRoles()



    '    Using sqlConn As New SqlConnection(_DataConn)



    '        Dim Cmd As New SqlCommand()
    '        Cmd.CommandText = "SelUserRolesByClientNum"
    '        Cmd.CommandType = CommandType.StoredProcedure
    '        Cmd.Parameters.AddWithValue("@client_num", _ClientNum)
    '        Cmd.Connection = sqlConn



    '        sqlConn.Open()

    '        Using Cmd

    '            Using RDR = Cmd.ExecuteReader()

    '                If RDR.HasRows Then

    '                    Do While RDR.Read

    '                        Dim UserRole As New UserRole()


    '                        UserRole.RoleID = IIf(IsDBNull(RDR.Item("RoleID")), Nothing, RDR.Item("RoleID"))
    '                        UserRole.RoleName = IIf(IsDBNull(RDR.Item("RoleName")), Nothing, RDR.Item("RoleName"))
    '                        UserRole.RoleDesc = IIf(IsDBNull(RDR.Item("RoleDesc")), Nothing, RDR.Item("RoleDesc"))
    '                        UserRole.DateDeactivated = IIf(IsDBNull(RDR.Item("DateDeactivated")), Nothing, RDR.Item("DateDeactivated"))


    '                        _UserRoles.Add(UserRole)
    '                    Loop

    '                End If

    '            End Using

    '        End Using

    '        sqlConn.Close()

    '    End Using



    'End Sub

    'Public ReadOnly Property UserRoles As List(Of UserRole)
    '    Get

    '        Return _UserRoles

    '    End Get
    'End Property
    Public Sub GetMySecQueues()

        'If sConn Is Nothing Then
        '    _CSCConn = HttpContext.Current.Session("CSCConn")
        'End If

        _CSCConn = HttpContext.Current.Session("CSCConn")

        Dim thisdata As New CommandsSqlAndOleDb("SelGroupsByTechNum", _CSCConn)

        thisdata.AddSqlProcParameter("@technum", _TechNum, SqlDbType.NVarChar, 20)
        Dim Gdt As DataTable = thisdata.GetSqlDataTable


        For Each dr In Gdt.Rows

            Dim Groupnum As Integer
            Dim groupCD As String

            Groupnum = dr("group_num")
            _AllMyGroups.Add(Groupnum)

            groupCD = dr("group_cd")
            _AllgroupsStr.Add(groupCD)

        Next

    End Sub
    Public ReadOnly Property MySecGroups As IList(Of Integer)
        Get
            Return _AllMyGroups

        End Get
    End Property
    Public ReadOnly Property MySecGroupString As IList(Of String)
        Get
            Return _AllgroupsStr
        
        End Get
    End Property
    Public ReadOnly Property AppOwnership As List(Of Integer)
        Get
            Return _AppOwnership
        End Get
    End Property

    Public Function UserType() As String
        Return _UserType

    End Function

    Public Function ClientNum() As Integer
        Return _ClientNum

    End Function

    Public Function TechNum() As Integer
        Return _TechNum

    End Function

    Public Function SecurityLevelNum() As Integer
        Return _SecurityLevelNum

    End Function

    Public Function NtLogin() As String
        Return _NtLogin

    End Function
    Public Function email() As String
        Return _email

    End Function
    Public Function DepartmentCd() As String
        Return _DepartmentCd

    End Function
    Public Function EntityCd() As String
        Return _EntityCd

    End Function

    Public Function UserName() As String
        Return _UserName

    End Function

    Public Function ValidateCount() As String
        Return _validateCount

    End Function
    Public Function NoTechCount() As String
        Return _notechcount
    End Function

    Public Function unassignedTickets() As String
        Return _unassignedTickets
    End Function
    Public Function ApplicationGroup() As String
        Return _DefaultApplicationGroup

    End Function
    Public Function P1Count() As String
        Return _P1count

    End Function
    Public Function Audit() As String
        Return _Audit

    End Function


    Public Function getPageSecurityDataTable() As DataTable
        Dim thisdata As New CommandsSqlAndOleDb("SelFormSecurityByUserType", _DataConn)
        thisdata.AddSqlProcParameter("@security_level", _SecurityLevelNum, SqlDbType.NVarChar, 50)
        thisdata.AddSqlProcParameter("@ntLogin", _NtLogin, SqlDbType.NVarChar, 50)

        Dim dt As DataTable
        dt = thisdata.GetSqlDataTable()

        thisdata = Nothing


        Return dt
    End Function
    Public Function getPageSecurityDataTableV2() As DataTable
        Dim thisdata As New CommandsSqlAndOleDb("SelFormSecurityByUserTypeV2", _DataConn)
        ' thisdata.AddSqlProcParameter("@security_level", _SecurityLevelNum, SqlDbType.NVarChar, 50)
        thisdata.AddSqlProcParameter("@ntLogin", _NtLogin, SqlDbType.NVarChar, 50)

        Dim dt As DataTable
        dt = thisdata.GetSqlDataTable()

        thisdata = Nothing


        Return dt
    End Function
End Class
