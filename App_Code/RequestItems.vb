﻿Imports Microsoft.VisualBasic
Imports App_Code



Public Class RequestItems
    Private _AccountRequestItems As New List(Of RequestItem)
    Private _MissingDepItems As New List(Of Application)
    Private _dataConn As String
    Private _dtAccountRequestItems As New DataTable
    'Private _Applications As New Applications()
    Private _ClientAccess As New List(Of ClientAccountAccess)
    Public Sub New()


    End Sub

    Public Sub New(ByVal reqType As String)
        GetTheConn()
        'Dim conn As New SetDBConnection()
        Dim _dataConn As String
        '_dataConn = conn.EmployeeConn
        _dataConn = HttpContext.Current.Session("EmployeeConn")

        Dim thisData As New CommandsSqlAndOleDb("SelOpenRequestItemsByTypeV3", _dataConn)
        thisData.AddSqlProcParameter("@requestType", reqType, SqlDbType.NVarChar)

        _dtAccountRequestItems = thisData.GetSqlDataTable()


        If _dtAccountRequestItems IsNot Nothing Then



            For Each dr As DataRow In _dtAccountRequestItems.Rows
                Dim RequestItem As New RequestItem()

                RequestItem.AccountRequestNum = IIf(IsDBNull(dr("account_request_num")), Nothing, dr("account_request_num"))
                RequestItem.AccountRequestSeqNum = IIf(IsDBNull(dr("account_request_seq_num")), Nothing, dr("account_request_seq_num"))

                RequestItem.ReceiverClientNum = IIf(IsDBNull(dr("receiver_client_num")), Nothing, dr("receiver_client_num"))
                RequestItem.ReceiverFullName = IIf(IsDBNull(dr("receiver_full_name")), Nothing, dr("receiver_full_name"))
                RequestItem.FirstName = IIf(IsDBNull(dr("first_name")), Nothing, dr("first_name"))
                RequestItem.LastName = IIf(IsDBNull(dr("last_name")), Nothing, dr("last_name"))

                'RequestItem.AccountRequestType = IIf(IsDBNull(dr("request_code_desc")), Nothing, dr("request_code_desc"))
                RequestItem.RequestCodeDesc = IIf(IsDBNull(dr("request_code_desc")), Nothing, dr("request_code_desc"))

                RequestItem.RequestorClientNum = IIf(IsDBNull(dr("requestor_client_num")), Nothing, dr("requestor_client_num"))
                RequestItem.RequestorName = IIf(IsDBNull(dr("requestor_name")), Nothing, dr("requestor_name"))
                RequestItem.RequestorPhone = IIf(IsDBNull(dr("requestor_phone")), Nothing, dr("requestor_phone"))

                RequestItem.EnteredDate = IIf(IsDBNull(dr("entered_date")), Nothing, dr("entered_date"))
                RequestItem.ItemCompleteDate = IIf(IsDBNull(dr("item_complete_date")), Nothing, dr("item_complete_date"))

                RequestItem.ValidCdDesc = IIf(IsDBNull(dr("action_desc")), Nothing, dr("action_desc"))
                RequestItem.ValidCd = IIf(IsDBNull(dr("valid_cd")), Nothing, dr("valid_cd"))

                RequestItem.ApplicationNum = IIf(IsDBNull(dr("ApplicationNum")), Nothing, dr("ApplicationNum"))
                RequestItem.ApplicationDesc = IIf(IsDBNull(dr("ApplicationDesc")), Nothing, dr("ApplicationDesc"))

                RequestItem.LoginName = IIf(IsDBNull(dr("login_name")), Nothing, dr("login_name"))

                RequestItem.AccountItemDesc = IIf(IsDBNull(dr("account_item_desc")), Nothing, dr("account_item_desc"))
                RequestItem.AppBase = IIf(IsDBNull(dr("AppBase")), Nothing, dr("AppBase"))
                'RequestItem.AppSystem = IIf(IsDBNull(dr("AppSystem")), Nothing, dr("AppSystem"))

                RequestItem.RequestComment = IIf(IsDBNull(dr("request_desc")), Nothing, dr("request_desc"))
                RequestItem.AccountRequestCode = IIf(IsDBNull(dr("account_request_type")), Nothing, dr("account_request_type"))

                RequestItem.Affiliation = IIf(IsDBNull(dr("Affiliation")), Nothing, dr("Affiliation"))
                'RequestItem.DollarAmount = IIf(IsDBNull(dr("DollarAmount")), Nothing, dr("DollarAmount"))


                'RequestItem.CernerPositionDesc = IIf(IsDBNull(dr("CernerPositionDesc")), Nothing, dr("CernerPositionDesc"))




                _AccountRequestItems.Add(RequestItem)
            Next
        End If
        '  setDependencyStatus()

    End Sub


    Public Sub New(ByVal iRequestNum As Integer)

        _dataConn = HttpContext.Current.Session("EmployeeConn")


        Dim thisData As New CommandsSqlAndOleDb("SelRequestItemsV20", _dataConn)
        thisData.AddSqlProcParameter("@account_request_num", iRequestNum, SqlDbType.NVarChar)
        _dtAccountRequestItems = thisData.GetSqlDataTable()

        For Each dr As DataRow In _dtAccountRequestItems.Rows

            Dim RequestItem As New RequestItem()

            RequestItem.AccountRequestNum = IIf(IsDBNull(dr("account_request_num")), Nothing, dr("account_request_num"))
            RequestItem.AccountRequestSeqNum = IIf(IsDBNull(dr("account_request_seq_num")), Nothing, dr("account_request_seq_num"))

            RequestItem.ReceiverClientNum = IIf(IsDBNull(dr("receiver_client_num")), Nothing, dr("receiver_client_num"))
            RequestItem.ReceiverFullName = IIf(IsDBNull(dr("receiver_full_name")), Nothing, dr("receiver_full_name"))
            RequestItem.FirstName = IIf(IsDBNull(dr("first_name")), Nothing, dr("first_name"))
            RequestItem.LastName = IIf(IsDBNull(dr("last_name")), Nothing, dr("last_name"))

            RequestItem.RequestCodeDesc = IIf(IsDBNull(dr("request_code_desc")), Nothing, dr("request_code_desc"))
            RequestItem.RequestorClientNum = IIf(IsDBNull(dr("requestor_client_num")), Nothing, dr("requestor_client_num"))
            RequestItem.RequestorName = IIf(IsDBNull(dr("requestor_name")), Nothing, dr("requestor_name"))
            RequestItem.RequestorPhone = IIf(IsDBNull(dr("requestor_phone")), Nothing, dr("requestor_phone"))

            RequestItem.EnteredDate = IIf(IsDBNull(dr("entered_date")), Nothing, dr("entered_date"))
            RequestItem.ItemCompleteDate = IIf(IsDBNull(dr("item_complete_date")), Nothing, dr("item_complete_date"))

            RequestItem.ValidCdDesc = IIf(IsDBNull(dr("action_desc")), Nothing, dr("action_desc"))
            RequestItem.ValidCd = IIf(IsDBNull(dr("valid_cd")), Nothing, dr("valid_cd"))

            RequestItem.ApplicationNum = IIf(IsDBNull(dr("ApplicationNum")), Nothing, dr("ApplicationNum"))
            RequestItem.ApplicationDesc = IIf(IsDBNull(dr("ApplicationDesc")), Nothing, dr("ApplicationDesc"))

            RequestItem.LoginName = IIf(IsDBNull(dr("login_name")), Nothing, dr("login_name"))

            RequestItem.AccountItemDesc = IIf(IsDBNull(dr("account_item_desc")), Nothing, dr("account_item_desc"))
            RequestItem.AppBase = IIf(IsDBNull(dr("AppBase")), Nothing, dr("AppBase"))

            RequestItem.AppSystem = IIf(IsDBNull(dr("AppSystem")), Nothing, dr("AppSystem"))
            RequestItem.RequestComment = IIf(IsDBNull(dr("request_desc")), Nothing, dr("request_desc"))
            RequestItem.AccountRequestCode = IIf(IsDBNull(dr("account_request_type")), Nothing, dr("account_request_type"))

            RequestItem.Affiliation = IIf(IsDBNull(dr("Affiliation")), Nothing, dr("Affiliation"))
            'GroupDesc
            RequestItem.GroupDesc = IIf(IsDBNull(dr("GroupDesc")), Nothing, dr("GroupDesc"))
            RequestItem.DollarAmount = IIf(IsDBNull(dr("DollarAmount")), Nothing, dr("DollarAmount"))

            RequestItem.ApprovedbyClientName = IIf(IsDBNull(dr("ApprovedbyClientName")), Nothing, dr("ApprovedbyClientName"))
            RequestItem.eMail = IIf(IsDBNull(dr("eMail")), Nothing, dr("eMail"))


            RequestItem.LocationOfCareID = IIf(IsDBNull(dr("LocationOfCareID")), Nothing, dr("LocationOfCareID"))
            RequestItem.CopyTo = IIf(IsDBNull(dr("CopyTo")), Nothing, dr("CopyTo"))
            RequestItem.CommLocationOfCareID = IIf(IsDBNull(dr("CommLocationOfCareID")), Nothing, dr("CommLocationOfCareID"))
            RequestItem.CommCopyTo = IIf(IsDBNull(dr("CommCopyTo")), Nothing, dr("CommCopyTo"))

            RequestItem.CernerPositionDesc = IIf(IsDBNull(dr("CernerPositionDesc")), Nothing, dr("CernerPositionDesc"))

            RequestItem.EPCSNomination = IIf(IsDBNull(dr("EPCSNomination")), Nothing, dr("EPCSNomination"))

            _AccountRequestItems.Add(RequestItem)



        Next
        'GetMissingDependency()


    End Sub
    Public ReadOnly Property RequestItems As List(Of RequestItem)
        Get
            Return _AccountRequestItems

        End Get
    End Property
    Public Function GetRequestByAppNum(ByVal iAppNum As Integer) As List(Of RequestItem)

        Dim requestsByApp = (From reqItem As RequestItem In _AccountRequestItems _
                       Where reqItem.ApplicationNum = iAppNum _
                       Select reqItem).ToList

        Return requestsByApp

    End Function
    Public ReadOnly Property MissingDependencies As List(Of Application)
        Get
            Return _MissingDepItems
        End Get
    End Property
    Public Sub FilterByRequestCode(ByRef sCodes As String)

        Dim sQueryCodes As String()
        sQueryCodes = Split(sCodes, ",")
        Dim requests = (From reqItem As RequestItem In _AccountRequestItems
                       Where sQueryCodes.Contains(reqItem.AccountRequestCode)
                       Select reqItem).ToList

        _AccountRequestItems = requests


    End Sub
    'Public Function GroupByAppNum(ByVal iAppNum As Integer()) As List(Of AccountRequestItem)

    '    'Dim appGroups = (From reqItem As AccountRequestItem In _AccountRequestItems
    '    '               Where sQueryCodes.Contains(reqItem.AccountRequestCode)
    '    '               Select reqItem).ToList


    'End Function

    Public Function GetRequestItemBySeqNum(ByVal iSeqNum As Integer) As RequestItem
        Dim requestBySeqNum = (From reqItem As RequestItem In _AccountRequestItems _
                     Where reqItem.AccountRequestSeqNum = iSeqNum _
                     Select reqItem).Single()


        Return requestBySeqNum

    End Function



    Public Function GetRequestByBaseNum(ByVal iBaseNum As Integer) As List(Of RequestItem)

        Dim requestsByBase = (From reqItem As RequestItem In _AccountRequestItems _
                       Where reqItem.AppBase = iBaseNum _
                       Select reqItem).ToList

        Return requestsByBase

    End Function
    Public Sub GetTheConn()
        Dim sServer = HttpContext.Current.Session("Server")
        Dim sPath = HttpContext.Current.Session("path")
		If HttpContext.Current.Session("environment") = "Production" Then
			HttpContext.Current.Session("EmployeeConn") = ConfigurationManager.AppSettings.Get("ProductionEmployee.ConnectionString")
			HttpContext.Current.Session("CSCConn") = ConfigurationManager.AppSettings.Get("ProductionCSC.ConnectionString")
			HttpContext.Current.Session("CSCIN1Conn") = ConfigurationManager.AppSettings.Get("ProductionCSCIN1.ConnectionString")

			Exit Sub

		End If
		If sServer = ConfigurationManager.AppSettings.Get("ProdWebServers") Then
			' Production Data and web site
			If Not sPath.ToString.ToLower.IndexOf("test") = -1 Then
                ' Test directory on CASEMGMT02
                HttpContext.Current.Session("EmployeeConn") = ConfigurationManager.AppSettings.Get("ProductionEmployee.ConnectionString")
                HttpContext.Current.Session("CSCConn") = ConfigurationManager.AppSettings.Get("ProductionCSC.ConnectionString")

                '            HttpContext.Current.Session("EmployeeConn") = ConfigurationManager.AppSettings.Get("TestProdServerEmployee.ConnectionString")
                'HttpContext.Current.Session("CSCConn") = ConfigurationManager.AppSettings.Get("TestProdServerCSC.ConnectionString")
                'HttpContext.Current.Session("CSCIN1Conn") = ConfigurationManager.AppSettings.Get("TestCSC.ConnectionString")

            Else
				' Production
				HttpContext.Current.Session("EmployeeConn") = ConfigurationManager.AppSettings.Get("ProductionEmployee.ConnectionString")
				HttpContext.Current.Session("CSCConn") = ConfigurationManager.AppSettings.Get("ProductionCSC.ConnectionString")
                'HttpContext.Current.Session("CSCIN1Conn") = ConfigurationManager.AppSettings.Get("ProductionCSCIN1.ConnectionString")

            End If

		ElseIf sServer = ConfigurationManager.AppSettings.Get("TestWebServers") Then
			' Test Site and test data
			HttpContext.Current.Session("EmployeeConn") = ConfigurationManager.AppSettings.Get("TestEmployee.ConnectionString")
			HttpContext.Current.Session("CSCConn") = ConfigurationManager.AppSettings.Get("TestCSC.ConnectionString")
            'HttpContext.Current.Session("CSCIN1Conn") = ConfigurationManager.AppSettings.Get("TestCSC.ConnectionString")

        ElseIf sServer = ConfigurationManager.AppSettings.Get("LocalhostWebServers") Then
			' Test Site and test data
			HttpContext.Current.Session("EmployeeConn") = ConfigurationManager.AppSettings.Get("TestEmployee.ConnectionString")
			HttpContext.Current.Session("CSCConn") = ConfigurationManager.AppSettings.Get("TestCSC.ConnectionString")
            'HttpContext.Current.Session("CSCIN1Conn") = ConfigurationManager.AppSettings.Get("TestCSC.ConnectionString")
            'Session("path") = "test"

        Else
			' else default to test
			'HttpContext.Current.Session("EmployeeConn") = ConfigurationManager.AppSettings.Get("TestEmployee.ConnectionString")
			'HttpContext.Current.Session("CSCConn") = ConfigurationManager.AppSettings.Get("TestCSC.ConnectionString")
			'HttpContext.Current.Session("CSCIN1Conn") = ConfigurationManager.AppSettings.Get("TestCSC.ConnectionString")
			'Session("path") = "test"


			' Production
			HttpContext.Current.Session("EmployeeConn") = ConfigurationManager.AppSettings.Get("ProductionEmployee.ConnectionString")
			HttpContext.Current.Session("CSCConn") = ConfigurationManager.AppSettings.Get("ProductionCSC.ConnectionString")
            'HttpContext.Current.Session("CSCIN1Conn") = ConfigurationManager.AppSettings.Get("ProductionCSCIN1.ConnectionString")

            'Session("path") = "prod"

        End If


	End Sub

    'Private Sub GetMissingDependency()
    '    Dim iReqNum As Integer = 0

    '    Dim appDepends As New List(Of AccountDependency)
    '    appDepends = New AccountDependencies().Dependencies


    '    Dim query2 = From reqItem As RequestItem In _AccountRequestItems
    '                 Join dep As AccountDependency In appDepends On dep.AppBase Equals reqItem.AppBase
    '                 Group Join reqDepItem As RequestItem In _AccountRequestItems On reqDepItem.AppBase Equals dep.AppCreateDepBase And reqDepItem.AccountRequestNum Equals reqItem.AccountRequestNum Into Group
    '                 From reqDepItem In Group.DefaultIfEmpty()
    '                 Select reqItem, dep, reqDepItem



    '    For Each var In query2
    '        Dim depStatus As String = ""

    '        '
    '        ' Sets first value to the first reqnum in the list


    '        If iReqNum = 0 Then
    '            iReqNum = var.reqItem.AccountRequestNum
    '        Else
    '            iReqNum = iReqNum
    '        End If


    '        If IsNothing(var.reqDepItem) Then

    '            var.reqItem.DependancyStatus = "Dependency Missing"

    '            depStatus = "Dependency Missing"
    '            Dim missingapps As New List(Of Application)
    '            missingapps = _Applications.ApplicationsByBase(var.dep.AppCreateDepBase).Where(Function(x) x.AppSystem <> "HospSpec").ToList()


    '            For Each MissingApp As Application In missingapps

    '                If _MissingDepItems.FindAll(Function(x) x.ApplicationNum = MissingApp.ApplicationNum).ToList().Count() = 0 Then

    '                    _MissingDepItems.Add(MissingApp)

    '                End If


    '            Next



    '        Else
    '            If iReqNum = var.reqItem.AccountRequestNum And (var.reqItem.DependancyStatus <> "Dependency Missing" Or var.reqItem.DependancyStatus Is Nothing) Then

    '                var.reqItem.DependancyStatus = "Dependency Created"

    '            End If


    '            If iReqNum = var.reqItem.AccountRequestNum And var.reqDepItem.ValidCd <> "created" And var.reqItem.DependancyStatus <> "Dependency Missing" Then

    '                var.reqItem.DependancyStatus = "Dependency Needed"

    '            End If


    '        End If


    '        iReqNum = var.reqItem.AccountRequestNum

    '    Next


    'End Sub


    'Private Sub setDependencyStatus(ByRef ReqItem As RequestItem)
    '    Dim sReturn As String
    '    Dim appDepends As New List(Of AccountDependency)
    '    appDepends = New AccountDependencies().Dependencies


    '    If ReqItem.ReceiverClientNum Is Nothing Then


    '        Dim OtherRequests As New List(Of RequestItem)
    '        OtherRequests = New RequestItems(ReqItem.AccountRequestNum, False).RequestItems.Where(Function(x) x.ValidCd <> "created" Or x.ValidCd <> "invalid")

    '        Dim appsWithDeps = From OtherItem As RequestItem In OtherRequests
    '                           From dep As AccountDependency In appDepends.Where(Function(x) x.AppBase = OtherItem.AppBase).DefaultIfEmpty()
    '                           Select OtherItem, dep


    '        For Each thing In appsWithDeps


    '            Dim asdfas As String = thing.OtherItem.ApplicationDesc
    '            Dim asdf As String = thing.dep.AppBase


    '        Next

    '    End If

    '    If Not ReqItem.ReceiverClientNum Is Nothing Then


    '        Dim OtherRequests As New List(Of RequestItem)
    '        OtherRequests = New RequestItems(ReqItem.AccountRequestNum, False).RequestItems.Where(Function(x) x.ValidCd <> "created" And x.ValidCd <> "invalid").ToList()

    '        Dim appsWithDeps = From OtherItem As RequestItem In OtherRequests
    '                           From dep As AccountDependency In appDepends.Where(Function(x) x.AppBase = OtherItem.AppBase).DefaultIfEmpty()
    '                           Select OtherItem, dep


    '        For Each thing In appsWithDeps


    '            Dim asdfas As String = thing.OtherItem.ApplicationDesc
    '            '  Dim asdf As String = thing.dep.AppBase


    '        Next

    '    End If


    '    Dim iReqNum As Integer = 0


    '    'Dim appsWithDeps = From reqItem As RequestItem In _AccountRequestItems.Where(Function(y) y.AccountRequestCode <> "terminate")
    '    '                   From dep As AccountDependency In AppDepends.Dependencies.Where(Function(x) x.AppBase = reqItem.AppBase).DefaultIfEmpty()
    '    '                   Select reqItem, dep



    '    'For Each item In appsWithDeps



    '    'Next
    '    '    If IsNothing(item.reqItem.ReceiverClientNum) = False Then

    '    '        Dim clientAccess As New List(Of ClientAccountAccess)

    '    '        clientAccess = New ClientAccounts(item.reqItem.ReceiverClientNum).ClientAccounts

    '    '        '  Dim findClientAccss = From reqItem As RequestItem In _AccountRequestItems


    '    '    End If




    '    'Dim ReqAndAccess = From ReqWithDep As RequestItem In _AccountRequestItems
    '    '                   Join DepApps As AccountDependency In AppDepends.Dependencies On DepApps.AppBase Equals ReqWithDep.AppBase

    '    'Dim st As String = item.reqItem.ApplicationDesc
    '    'Dim sd As String = item.dep.AppBase
    '    '  From access As ClientAccountAccess In New ClientAccounts(reqItem.ReceiverClientNum).ClientAccounts.Where(Function(y) y.ClientNum = reqItem.ReceiverClientNum And y.AppBase = dep.AppBase).DefaultIfEmpty()


    '    'Next



    '    'Dim query2 = From reqItem As RequestItem In RequestItems
    '    '             Join dep As AccountDependency In AppDepends.Dependencies On dep.AppBase Equals reqItem.AppBase
    '    '             Group Join reqDepItem As RequestItem In _AccountRequestItems On reqDepItem.AppBase Equals dep.AppCreateDepBase And reqDepItem.AccountRequestNum Equals reqItem.AccountRequestNum Into Group
    '    '             From reqDepItem In Group.DefaultIfEmpty()
    '    '             Select reqItem, dep, reqDepItem



    '    'For Each var In query2
    '    '    Dim depStatus As String = ""

    '    '    '
    '    '    ' Sets first value to the first reqnum in the list


    '    '    If iReqNum = 0 Then
    '    '        iReqNum = var.reqItem.AccountRequestNum
    '    '    Else
    '    '        iReqNum = iReqNum
    '    '    End If


    '    '    If IsNothing(var.reqDepItem) Then

    '    '        var.reqItem.DependancyStatus = "Dependency Missing"

    '    '        depStatus = "Dependency Missing"

    '    '        For Each MissingApp As Application In _Applications.ApplicationsByBase(var.dep.AppCreateDepBase)

    '    '            _MissingDepItems.Add(MissingApp)

    '    '        Next



    '    '    Else
    '    '        If iReqNum = var.reqItem.AccountRequestNum And (var.reqItem.DependancyStatus <> "Dependency Missing" Or var.reqItem.DependancyStatus Is Nothing) Then

    '    '            var.reqItem.DependancyStatus = "Dependency Created"

    '    '        End If


    '    '        If iReqNum = var.reqItem.AccountRequestNum And var.reqDepItem.ValidCd <> "created" And var.reqItem.DependancyStatus <> "Dependency Missing" Then

    '    '            var.reqItem.DependancyStatus = "Dependency Needed"

    '    '        End If


    '    '    End If


    '    '    iReqNum = var.reqItem.AccountRequestNum

    '    'Next
    'End Sub

End Class

Public Class RequestItem
    Public Property AccountRequestNum As Integer
    Public Property AccountRequestSeqNum As String

    Public Property ReceiverClientNum As String
    Public Property ReceiverFullName As String
    Public Property FirstName As String
    Public Property LastName As String

    Public Property RequestCodeDesc As String

    Public Property RequestorClientNum As String
    Public Property RequestorName As String
    Public Property RequestorPhone As String

    Public Property EnteredDate As String
    Public Property ItemCompleteDate As String

    Public Property Affiliation As String

    Public Property DollarAmount As String
    Public Property GroupDesc As String

    Public Property ValidCdDesc As String
    Public Property ValidCd As String
    Public Property ApplicationNum As String
    Public Property ApplicationDesc As String
    Public Property LoginName As String
    Public Property AccountItemDesc As String
    Public Property DependancyStatus As String
    Public Property AppBase As String
    Public Property AppSystem As String

    Public Property RequestComment As String
    Public Property AccountRequestCode As String

    Public Property HasDependency As String

    Public Property TechName As String
    Public Property ApprovedbyClientName As String
    Public Property eMail As String

    Public Property LocationOfCareID As String
    Public Property CopyTo As String
    Public Property CommLocationOfCareID As String
    Public Property CommCopyTo As String

    Public Property CernerPositionDesc As String

    Public Property EPCSNomination As String




End Class