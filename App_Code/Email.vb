﻿Imports Microsoft.VisualBasic
Imports System.Net.Mail

Public Class Email
    Private _sEmailServer As String = ""
    Private _smtpClient As SmtpClient
    Private _mailMessage As New MailMessage

    Public Sub New()

        getEmailServer()

        _smtpClient = New SmtpClient(_sEmailServer)


    End Sub


    Protected Sub getEmailServer()

        _sEmailServer = ConfigurationManager.AppSettings.Get("SmtpServer")

    End Sub

    Public Sub addToAddress(ByVal sAddress As String)

        _mailMessage.To.Add(sAddress)


    End Sub

    Public Sub fromAddress(ByVal sFrom As String)

        _mailMessage.From = New MailAddress(sFrom)


    End Sub

    Public Sub mailSubject(ByVal sSubject As String)

        _mailMessage.Subject = sSubject


    End Sub

    Public Sub mailBody(ByVal sBody As String)

        _mailMessage.Body = sBody


    End Sub

    Public Sub Send()

        Try

            _smtpClient.Send(_mailMessage)


        Catch e As Exception

            Dim errorMessage As String = "Application Page: email.vb " & " Message: E-mail was not sent to anyone. Request was completed "
        End Try

    End Sub
End Class
