﻿Imports Microsoft.VisualBasic
Imports App_Code

Public Class EmailDistributionList
    Private _emailList As New List(Of EmailDistribution)


    Public Sub New()

        Dim conn As New SetDBConnection()


        Dim thisdata As New CommandsSqlAndOleDb("SelEmailDistribution", conn.EmployeeConn)


        Dim dt As DataTable = thisdata.GetSqlDataTable

        For Each dr As DataRow In dt.Rows
            Dim EmailDistribution As New EmailDistribution()

            EmailDistribution.TechNum = IIf(IsDBNull(dr("tech_num")), Nothing, dr("tech_num"))
            EmailDistribution.EmailEvent = IIf(IsDBNull(dr("event")), Nothing, dr("event"))
            EmailDistribution.AppBase = IIf(IsDBNull(dr("appBase")), Nothing, dr("appBase"))
            EmailDistribution.Email = IIf(IsDBNull(dr("email")), Nothing, dr("email"))



            _emailList.Add(EmailDistribution)

        Next


    End Sub
    Public ReadOnly Property EmailList As List(Of EmailDistribution)
        Get
            Return _emailList
        End Get

    End Property
End Class


Public Class EmailDistribution
    Property TechNum As Integer
    Property EmailEvent As String
    Property AppBase As String
    Property Email As String


End Class
