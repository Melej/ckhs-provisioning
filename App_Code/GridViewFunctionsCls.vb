Imports Microsoft.VisualBasic
'Imports System.Web.HttpApplication
'Imports System.Web.HttpResponse
'Imports System.Web.HttpApplicationState
Imports System.Web.HttpContext
Imports System.Web
Imports System.IO
'Imports C1.Web.C1WebGrid
Public Class GridViewFunctionsCls
    'Public Enum PageNav    See globalValues in app_code
    '    FirstPage
    '    PreviousPage
    '    NextPage
    '    LastPage
    ' GoToPage
    'End Enum

    Protected Shared GvIn As New GridView
    Public Shared Function GetGridCellIndex(ByVal sFieldName As String, ByRef gridview1 As GridView, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) As Integer
        ' If e.Row.RowIndex = 1 Then 'first datarow
        'GridViewRowEventArgs
        Dim i As Integer = gridview1.Columns.Count - 1
        Dim c As Integer = 0
        Dim iReturn As Integer = 0
        Dim oheaderRow As GridViewRow
        Dim dDataRow As GridViewRow
        Dim df As New GridView
        'Dim hc As New TableCell
        oheaderRow = gridview1.HeaderRow
        Dim hCell As New TableCell

        Dim sHeaderSortExpressionFields(i) As String
        Dim sHeaderFields(i) As String
        Dim sDataFields(i) As String
        If e.Row.RowType = DataControlRowType.DataRow Then

            For c = 0 To i
                sHeaderFields(c) = oheaderRow.Cells(c).Text
                If TypeOf oheaderRow.Cells(c) Is DataControlFieldCell Then
                    Dim field As DataControlField = (CType(oheaderRow.Cells(c), DataControlFieldCell)).ContainingField
                    sHeaderSortExpressionFields(c) = field.SortExpression
                    sDataFields(c) = field.HeaderText
                    If sDataFields(c).ToLower = sFieldName.ToLower Then
                        iReturn = c
                        Exit For
                    End If
                End If

                If oheaderRow.Cells(c).Text.ToLower = sFieldName.ToLower Then

                    iReturn = c
                    Exit For
                    ' Return c
                End If
            Next
        End If
        Return iReturn
    End Function
    Public Shared Function GetGridCellIndexNoE(ByVal sFieldName As String, ByRef gridview1 As GridView) As Integer

        ' If e.Row.RowIndex = 1 Then 'first datarow
        'GridViewRowEventArgs
        Dim i As Integer = gridview1.Columns.Count - 1
        Dim c As Integer = 0
        Dim iReturn As Integer = 0

        Dim dDataRow As GridViewRow
        Dim df As New GridView
        'Dim hc As New TableCell
        Dim oheaderRow As GridViewRow
        oheaderRow = gridview1.HeaderRow
        Dim hCell As New TableCell

        Dim sHeaderSortExpressionFields(i) As String
        Dim sHeaderFields(i) As String
        Dim sDataFields(i) As String
        ' If e.Row.RowType = DataControlRowType.DataRow Then

        For c = 0 To i 'Skip Header row
            sHeaderFields(c) = oheaderRow.Cells(c).Text
            If TypeOf oheaderRow.Cells(c) Is DataControlFieldCell Then
                Dim field As DataControlField = (CType(oheaderRow.Cells(c), DataControlFieldCell)).ContainingField
                sHeaderSortExpressionFields(c) = field.SortExpression
                sDataFields(c) = field.HeaderText.ToLower
                If sDataFields(c).ToLower = sFieldName.ToLower Then
                    iReturn = c
                    Exit For
                End If
            End If

            If oheaderRow.Cells(c).Text.ToLower = sFieldName.ToLower Then

                iReturn = c
                Exit For
                ' Return c
            End If
        Next
        ' End If
        Return iReturn
    End Function
    Protected Shared Sub AddSingleToGridView(ByVal dtin As DataTable, ByVal IColPosition As Integer, ByVal GvIn As GridView)
        Dim mycol As BoundField
        mycol = New BoundField
        mycol.DataField = dtin.Columns(IColPosition).ColumnName
        mycol.HeaderText = dtin.Columns(IColPosition).ColumnName
        GvIn.Columns.Add(mycol)
        ' GvIn.Columns.Insert(
        mycol = Nothing
    End Sub
    'Public Function PageGridView(ByVal dtin As DataTable, ByVal iPageSize As Integer, ByVal iCurrPage As Integer, ByVal PageFunction As PageNav, ByVal GvIn As GridView, ByVal iPageToGoTo As Integer) As Integer 'As DataTable
    '    Dim i As Integer
    '    '  Dim mycol As BoundField
    '    Dim iStartRow As Integer = iPageSize * iCurrPage
    '    Dim dtOut As New DataTable
    '    Dim iRoundUp As Integer = 0 'deal with remainder after row devision
    '    'Allow for partial row
    '    If (dtin.Rows.Count / (iPageSize)) Mod iPageSize Then iRoundUp = 1
    '    Dim iMaxPage As Integer = (dtin.Rows.Count / (iPageSize)) + iRoundUp
    '    dtOut = dtin.Clone
    '    'page management
    '    Select Case PageFunction 'make pages 1 based rows are zero
    '        Case PageNav.FirstPage
    '            iStartRow = 0
    '            iCurrPage = 1
    '        Case PageNav.LastPage
    '            iStartRow = dtin.Rows.Count - (iPageSize)
    '            iCurrPage = iMaxPage ' (dtin.Rows.Count / (iPageSize)) + 1
    '        Case PageNav.NextPage

    '            If iCurrPage + 1 <= iMaxPage Then
    '                iStartRow = iPageSize * (iCurrPage)
    '                iCurrPage = iCurrPage + 1
    '            Else 'already at last page
    '                iCurrPage = iMaxPage
    '                iStartRow = iMaxPage - 1 * iPageSize
    '            End If
    '        Case PageNav.PreviousPage
    '            If iCurrPage - 1 <= 0 Then 'already at first page
    '                'iPageSize * (iCurrPage - 1)
    '                iCurrPage = 1
    '            Else
    '                iCurrPage = iCurrPage - 1
    '            End If
    '            iStartRow = 0
    '        Case PageNav.GotoPage
    '            iCurrPage = iPageToGoTo
    '            iStartRow = iPageToGoTo * iPageSize - 1 ' iMaxPage - 1 * iPageSize
    '    End Select
    '    For i = iStartRow To iStartRow + iPageSize - 1 'dtin.Columns.Count - 1
    '        If i > iStartRow + iPageSize Then
    '            'If IsDBNull(dtin.Rows(i)) Then 'do nothing
    '        Else
    '            dtOut.ImportRow(dtin.Rows(i))
    '        End If

    '        'mycol = New BoundField
    '        'mycol.DataField = dtin.Columns(i).ColumnName
    '        'mycol.HeaderText = dtin.Columns(i).ColumnName
    '        'GvIn.Columns.Add(mycol)
    '    Next
    '    GvIn.DataSource = dtOut
    '    Return iCurrPage
    '    ' Return dtOut
    'End Function
    Public Shared Sub setHeaderRowCount(ByRef gridview1 As GridView)
        '''Use in row databound
        Dim headerRow As GridViewRow = gridview1.HeaderRow
        If gridview1.Rows.Count > 0 Then
            headerRow.Cells(0).Text = " Count:" & gridview1.Rows.Count & " <br>" & headerRow.Cells(0).Text
            '   headerRow.Cells(0).BackColor = Color.LightBlue
        End If ' Exit Sub
    End Sub
    Public Shared Sub setHeaderRowItemEmptyCount(ByRef gridview1 As GridView, ByVal iFieldIndex As Integer, ByVal sDisplayTitle As String, ByVal filter As ArrayList)
        '''Use in row databound
        Dim headerRow As GridViewRow = gridview1.HeaderRow
        Dim iRowCount As Integer = 0
        If gridview1.Rows.Count > 0 Then
            For irow = 0 To gridview1.Rows.Count - 1
                If gridview1.Rows(irow).Cells(iFieldIndex).Text <> "&nbsp;" Then
                    iRowCount += 1
                End If
            Next
            headerRow.Cells(0).Text = sDisplayTitle & " : " & iRowCount & " <br>" & headerRow.Cells(0).Text
            '   headerRow.Cells(0).BackColor = Color.LightBlue
        End If ' Exit Sub
    End Sub
    Public Shared Sub setHeaderRowItemCountNoIsoBed(ByRef gridview1 As GridView, ByVal iFieldIndex As Integer, ByVal sDisplayTitle As String, ByVal alValuesToCount As ArrayList)
        '''Use in row databound
        Dim headerRow As GridViewRow = gridview1.HeaderRow
        Dim iRowCount As Integer = 0
        Dim isoCount As Integer = 0
        Dim iItem As Integer = 0
        If gridview1.Rows.Count > 0 Then
            For irow = 0 To gridview1.Rows.Count - 1
                For iItem = 0 To alValuesToCount.Count - 1
                    If gridview1.Rows(irow).Cells(iFieldIndex).Text.ToLower.IndexOf(alValuesToCount(iItem).ToString.ToLower) > -1 Then
                        If gridview1.Rows(irow).Cells(18).Text.ToLower.IndexOf("isolation") < 0 Then 'not iso bed
                            iRowCount += 1
                        Else
                            isoCount += 1
                        End If
                    End If
                Next
            Next
            headerRow.Cells(0).Text = sDisplayTitle & " : " & iRowCount & " <br>" & headerRow.Cells(0).Text
            If isoCount > 0 Then
                headerRow.Cells(0).Text &= isoCount & " Iso Beds"
            End If
            '   headerRow.Cells(0).BackColor = Color.LightBlue
        End If ' Exit Sub
    End Sub
    Public Shared Sub setHeaderRowItemCountAColumnValue(ByRef gridview1 As GridView, ByVal iFieldIndex As Integer, ByVal sDisplayTitle As String, ByVal alValuesToCount As ArrayList)
        '''Use in row databound
        Dim headerRow As GridViewRow = gridview1.HeaderRow
        Dim iRowCount As Integer = 0
        Dim iItem As Integer = 0
        If gridview1.Rows.Count > 0 Then
            For irow = 0 To gridview1.Rows.Count - 1
                For iItem = 0 To alValuesToCount.Count - 1
                    If gridview1.Rows(irow).Cells(iFieldIndex).Text.ToLower.IndexOf(alValuesToCount(iItem).ToString.ToLower) > -1 Then
                        iRowCount += 1
                    End If
                Next
            Next
            headerRow.Cells(0).Text = sDisplayTitle & " : " & iRowCount & " <br>" & headerRow.Cells(0).Text
            '   headerRow.Cells(0).BackColor = Color.LightBlue
        End If ' Exit Sub
    End Sub
    Public Shared Function HideUnwantedColumns(ByRef GvIn As GridView, ByVal iLastVisibleCoumnIndex As Integer) As Boolean
        Dim icol As Integer = 0
        Dim irow As Integer = 0
        If GvIn.Rows.Count > 0 Then ' if it has rows


            Dim headerRow As GridViewRow = GvIn.HeaderRow
            For irow = 0 To GvIn.Rows.Count - 1
                For icol = iLastVisibleCoumnIndex To GvIn.Rows(irow).Cells.Count - 1

                    GvIn.Rows(irow).Cells(icol).Visible = False
                    'If irow = 0 Then
                    '    headerRow.Cells(icol).Visible = False
                    'End If
                Next
            Next
            If Not IsNothing(headerRow) Then
                For icol = iLastVisibleCoumnIndex To headerRow.Cells.Count - 1
                    headerRow.Cells(icol).Visible = False
                Next
            End If
        End If
    End Function

    Public Shared Sub AddColumnsToGridView(ByVal dtin As DataTable, ByRef GvIn As GridView)
        Dim i As Integer
        Dim mycol As BoundField
        For i = 0 To dtin.Columns.Count - 1
            mycol = New BoundField

            mycol.DataField = dtin.Columns(i).ColumnName
            mycol.HeaderText = dtin.Columns(i).ColumnName
            GvIn.Columns.Add(mycol)
        Next
    End Sub
    Public Shared Function GetColumnIndexByDBName(ByVal aGridView As GridView, ByVal ColumnText As String) As Integer
        Dim DataColumn As System.Web.UI.WebControls.BoundField
        For Index As Integer = 0 To aGridView.Columns.Count - 1


            DataColumn = TryCast(aGridView.Columns(Index), System.Web.UI.WebControls.BoundField)
            If Not DataColumn Is Nothing Then


                If DataColumn.DataField = ColumnText Then


                    Return Index
                End If
            End If
        Next Index
        Return -1
    End Function
    Public Shared Function GetColumnIndexByHeaderText(ByVal aGridView As GridView, ByVal ColumnText As String) As Integer
        Dim Cell As TableCell
        For Index As Integer = 0 To aGridView.HeaderRow.Cells.Count - 1
            Cell = aGridView.HeaderRow.Cells(Index)
            If Cell.Text.ToString() = ColumnText Then
                Return Index
            End If
        Next Index
        Return -1
    End Function
    Public Shared Function ReturnHeaderTextFromIndex(ByRef gridviewIn As GridView, ByVal iDex As Integer) As String
        'test
        'Create the array/collection of rows containing your data
        Dim data As GridViewRowCollection = gridviewIn.Rows()
        Dim icol As Integer = 0
        'Iterate through each row to...
        For Each row As GridViewRow In data
            icol = 0
            'Iterate through each cell in each row...
            For Each cellResult As DataControlFieldCell In row.Cells

                'Create a variable to store the data control field required
                'to reference the header text
                Dim cellDCF As DataControlField = cellResult.ContainingField

                'Create a variable to store the header text that identifies your cell
                Dim cellID As String = cellDCF.HeaderText
                If icol = iDex Then
                    If cellID <> "" Then
                        Return cellID
                    Else
                        Return "empty"
                    End If
                End If
                'Check to see if your header text is the one you want
                If cellID.ToString = "your column name" Then

                    'If it is, then grab its value (data in the cell) and store it in
                    'a variable to perform actions against
                    Dim cellValue As Integer = cellResult.Text

                    'Perform whatever actions you need to here...
                    'If cellValue = yourSessionVariable Then
                    '    rowResult.Visible = True
                    'Else
                    '    rowResult.Visible = False
                    'End If
                End If
                icol += 1
            Next
        Next
        Return "empty"
    End Function
    Public Shared Function GetGridCellHeaderTextNoE(ByVal fIndex As Integer, ByRef gridview1 As GridView) As String

        ' If e.Row.RowIndex = 1 Then 'first datarow
        'GridViewRowEventArgs
        Dim i As Integer = gridview1.Columns.Count - 1
        Dim c As Integer = 0
        Dim iReturn As Integer = 0

        Dim dDataRow As GridViewRow
        Dim df As New GridView
        'Dim hc As New TableCell
        Dim oheaderRow As GridViewRow
        oheaderRow = gridview1.HeaderRow
        Dim hCell As New TableCell

        Dim sHeaderSortExpressionFields(i) As String
        Dim sHeaderFields(i) As String
        Dim sDataFields(i) As String
        ' If e.Row.RowType = DataControlRowType.DataRow Then

        For c = 0 To i 'Skip Header row
            sHeaderFields(c) = oheaderRow.Cells(c).Text
            If TypeOf oheaderRow.Cells(c) Is DataControlFieldCell Then
                Dim field As DataControlField = (CType(oheaderRow.Cells(c), DataControlFieldCell)).ContainingField
                sHeaderSortExpressionFields(c) = field.SortExpression
                sDataFields(c) = field.HeaderText.ToLower
                If c = fIndex Then


                    'If sDataFields(c).ToLower = sFieldName.ToLower Then
                    Return sDataFields(c)
                    Exit For
                End If
            End If

            'If oheaderRow.Cells(c).Text.ToLower = sFieldName.ToLower Then

            '    iReturn = oheaderRow.Cells(c).Text
            '    Exit For
            '    ' Return c
            'End If
        Next
        ' End If
        Return "None"
    End Function
    'end test
    'demo code=============================================
    '' ''If Not IsPostBack Then

    '' ''    ' Dynamically create columns to display the desired
    '' ''    ' fields from the data source. Columns that are 
    '' ''    ' dynamically added to the GridView control are not persisted 
    '' ''    ' across posts and must be recreated each time the page is 
    '' ''    ' loaded.

    '' ''    ' Create a BoundField object to display an author's last name.
    '' ''    Dim lastNameBoundField As BoundField = New BoundField
    '' ''    lastNameBoundField.DataField = "au_lname"
    '' ''    lastNameBoundField.HeaderText = "Last Name"

    '' ''    ' Create a CheckBoxField object to indicate whether the author
    '' ''    ' is on contract.
    '' ''    Dim contractCheckBoxField As CheckBoxField = New CheckBoxField
    '' ''    contractCheckBoxField.DataField = "contract"
    '' ''    contractCheckBoxField.HeaderText = "Contract"

    '' ''    ' Add the columns to the Columns collection of the
    '' ''    ' GridView control.
    '' ''    AuthorsGridView.Columns.Add(lastNameBoundField)
    '' ''    AuthorsGridView.Columns.Add(contractCheckBoxField)

    '' ''End If
    '===============================end ==============================
    ''' End Sub
    'Protected Shared Sub AddColumnsToC1Grid(ByVal dtin As DataTable, ByVal GvIn As C1WebGrid)
    '    Dim i As Integer
    '    For i = 0 To dtin.Columns.Count - 1
    '        Dim mycol As C1.Web.C1WebGrid.C1BoundColumn

    '        mycol = New C1.Web.C1WebGrid.C1BoundColumn
    '        mycol.DataField = dtin.Columns(i).ColumnName
    '        mycol.HeaderText = dtin.Columns(i).ColumnName
    '        GvIn.Columns.Add(mycol)
    '    Next
    'End Sub
    Protected Sub CreateDocOrExcel(ByRef gridview1 As GridView, ByRef SqlDataSource1 As SqlDataSource, ByVal sWordOrExcel As String)
        'gridview1.Visible = True
        ''doesnt work with gridviews that have controls in them
        ''  Dim ct As System.Web.HttpContext.Current.
        'Response.ClearContent()
        'Dim attachment As String = "attachment; filename=ExcelDownload.xls"

        'If sWordOrExcel.ToLower = "word" Then
        '    Response.ContentType = "application/ms-word"
        '    Response.AddHeader("Content-Disposition", "inline;filename=test.doc")
        'Else
        '    Response.AddHeader("Content-disposition", attachment)
        '    Response.ContentType = "application/excel"
        'End If

        'Dim strStyle As String = "<style>.text {mso-number-format:\@; } </style>"   ' to help retain leading zeroes
        'Response.Write(strStyle)
        'Response.Write("<H4>")  ' throw in a pageheader just for the heck of it to tell people what the data is and when it was run
        'Response.Write("Parts Listing as of " & Date.Now.ToString("d"))
        'Response.Write("</H4>")
        'Response.Write("<BR>")
        'gridview1.HeaderStyle.Font.Bold = True
        'gridview1.HeaderStyle.BackColor = Drawing.Color.LightGray

        'Dim sw As StringWriter = New StringWriter()
        'Dim hw As HtmlTextWriter = New HtmlTextWriter(sw)
        'Dim dg As New DataGrid
        'dg.DataSource = SqlDataSource1
        'Dim i As Integer = dg.Items.Count
        'dg.DataBind()
        '' ClearControls(dg)
        'dg.RenderControl(hw)
        ''   Dim dt As New DataTable
        '' Dim dv As New DataView

        ''dv.Table.ToString()
        'Response.Write(sw.ToString())
        'Response.End()

        'gridview1.AllowSorting = True
    End Sub
    ''' <param name="sortDir">Sort direction - either ascending (<c>SortDirection.Ascending</c>) or descending (<c>SortDirection.Descending</c>).</param>
    ''' <param name="sortExpr">Sort expression which is a database field.</param>
    ''' <param name="grid">ASP.NET GridView control.</param>
    ''' <remarks><para>Called from a grid's "Sorting" event, e.g. <code>AppendSortOrderImageToGridHeader(e.SortDirection, e.SortExpression, [gridname])</code>.</para></remarks>
    ''Public Shared Sub AppendSortOrderImageToGridHeader(ByVal sortDir As System.Web.UI.WebControls.SortDirection, _
    ''        ByVal sortExpr As String, _
    ''        ByRef grid As System.Web.UI.WebControls.GridView)

    ''    ' looping variable 
    ''    Dim i As Integer
    ''    ' did we find the column header that's being sorted?
    ''    Dim foundColumnIndex As Integer = -1

    ''    ' constants for sort orders
    ''    Const SORT_ASC As String = "<span style='font-family: Webdings; '> 5</span>"
    ''    Const SORT_DESC As String = "<span style='font-family: Webdings; '> 6</span>"

    ''    ' get which column we're sorting on
    ''    For i = 0 To grid.Columns.Count - 1
    ''        ' remove the current sort
    ''        grid.Columns(i).HeaderText = grid.Columns(i).HeaderText.Replace(SORT_ASC, String.Empty)
    ''        grid.Columns(i).HeaderText = grid.Columns(i).HeaderText.Replace(SORT_DESC, String.Empty)
    ''        ' if the sort expression of this column matches the passed sort expression, 
    ''        ' keep the column number and mark that we've found a match for further processing
    ''        If sortExpr = grid.Columns(i).SortExpression Then
    ''            ' store the column number, but we need to keep going through the loop
    ''            ' to remove all the previous sorts
    ''            foundColumnIndex = i
    ''        End If
    ''    Next

    ''    ' if we found the sort column, append the sort direction 
    ''    If foundColumnIndex > -1 Then
    ''        ' append either ascending or descending string
    ''        If sortdir = SortDirection.Ascending Then
    ''            grid.Columns(foundColumnIndex).HeaderText &= SORT_ASC
    ''        Else
    ''            grid.Columns(foundColumnIndex).HeaderText &= SORT_DESC
    ''        End If

    ''    End If

    ''End Sub
    Public Shared Function UseSortedImage(ByRef GviewIn As GridView, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs, ByVal iLastVisibleCoumnIndex As Integer) As Boolean
        'Call from RowCreated event of gridview
        If e.Row.RowType = DataControlRowType.Header Then
            GvIn = GviewIn
            ' Call the GetSortColumnIndex helper method to determine
            ' the index of the column being sorted.
            Dim sortColumnIndex As Integer = GetSortColumnIndex(GvIn)

            If sortColumnIndex <> -1 Then

                ' Call the AddSortImage helper method to add
                ' a sort direction image to the appropriate
                ' column header. 
                AddSortImage(sortColumnIndex, e.Row)

            End If

        End If
    End Function
    Protected Shared Sub AddSortImage(ByVal columnIndex As Integer, ByVal row As GridViewRow)
        ' This is a helper method used to add a sort direction
        ' image to the header of the column being sorted.
        ' Create the sorting image based on the sort direction.
        Dim sortImage As New System.Web.UI.WebControls.Image
        If GvIn.SortDirection = SortDirection.Ascending Then

            sortImage.ImageUrl = "~/Images/sortascending.gif"
            sortImage.AlternateText = "Ascending Order"

        Else

            sortImage.ImageUrl = "~/Images/sortdescending.gif"
            sortImage.AlternateText = "Descending Order"

        End If
        ' Add the image to the appropriate header cell.
        row.Cells(columnIndex).Controls.Add(sortImage)
        sortImage.Dispose()
    End Sub

    Protected Shared Function GetSortColumnIndex(ByRef gv As GridView) As Integer

        ' Iterate through the Columns collection to determine the index
        ' of the column being sorted.
        Dim field As DataControlField
        For Each field In gv.Columns
            If field.SortExpression <> "" Then
                If field.SortExpression = gv.SortExpression Then

                    Return gv.Columns.IndexOf(field)

                End If
            End If

        Next

        Return -1

    End Function
    Public Shared Sub setCallToClientSideFilterRows(ByRef gv As GridView, ByVal ivisiblecolumns As Integer)
        Dim bisheader As Boolean
        Dim sFilterValue As String = ""
        Dim sJscriptCall As String = ""
        ' used in databound
        Dim irow As Integer = 0
        'row loop
        Dim gvHeaderRow As GridViewRow
        If gv.HeaderRow.Cells.Count > 0 Then
            gvHeaderRow = gv.HeaderRow
            sJscriptCall = "filterTheRows( -1 , 0,'Header');"
            gvHeaderRow.Cells(0).Attributes.Add("onClick", sJscriptCall)
        End If


        For iRowCtr As Integer = 0 To gv.Rows.Count - 1
            'If gv.Rows(irow).RowType = DataControlRowType.Header Then
            If gv.Rows(iRowCtr).RowType = DataControlRowType.Header Then 'header row

                ' gv.Rows.Count
                sJscriptCall = "filterTheRows( -1 , 0,'Header');"
                gv.Rows(0).Cells(0).Attributes.Add("onClick", sJscriptCall)
                bisheader = True
            Else 'datarow
                If gv.Rows.Count > 0 Then

                    irow = iRowCtr
                    ' If sFormStatus.ToLower.IndexOf("m") > -1 Then
                    For iCell = 1 To ivisiblecolumns - 1
                        If IsDate(gv.Rows(irow).Cells(iCell).Text) Then
                            sFilterValue = gv.Rows(irow).Cells(iCell).Text
                            sFilterValue = sFilterValue.Replace(" ", "-")
                            Dim ipos As Integer = sFilterValue.LastIndexOf("-")
                            '  sFilterValue = sFilterValue.Remove(ipos)
                            ipos = sFilterValue.IndexOf("-")
                            If ipos > -1 Then
                                sFilterValue = sFilterValue.Remove(ipos, 1)
                                sFilterValue = sFilterValue.Replace("-", " ")
                            End If
                            gv.Rows(irow).Cells(iCell).Text = sFilterValue

                        Else
                            sFilterValue = gv.Rows(irow).Cells(iCell).Text
                        End If
                        'sFilterValue = e.Row.Cells(iCell).Text
                        sJscriptCall = "filterTheRows(" & irow & "," & iCell & ",'" & sFilterValue & "');"
                        gv.Rows(irow).Cells(iCell).Attributes.Add("onClick", sJscriptCall)

                    Next
                End If
            End If
        Next
    End Sub
    Public Shared Sub setCallToClientSideFilterRows(ByRef gv As GridView, ByVal ivisiblecolumns As Integer, ByRef gvRow As GridViewRow)
        Dim bisheader As Boolean
        Dim sFilterValue As String = ""
        Dim sJscriptCall As String = ""
        ' used in row databound
        Dim irow As Integer = gvRow.RowIndex
        'If gv.Rows(irow).RowType = DataControlRowType.Header Then
        If gvRow.RowType = DataControlRowType.Header Then 'header row

            ' gv.Rows.Count
            '  sJscriptCall = "filterTheRows( -1 , 0,'Header');"
            '  gv.Rows(0).Cells(0).Attributes.Add("onClick", sJscriptCall)
            bisheader = True
        Else 'datarow
            If gv.Rows.Count > 0 Then


                ' If sFormStatus.ToLower.IndexOf("m") > -1 Then
                For iCell = 1 To ivisiblecolumns - 1
                    If IsDate(gv.Rows(irow).Cells(iCell).Text) Then
                        sFilterValue = gv.Rows(irow).Cells(iCell).Text
                        sFilterValue = sFilterValue.Replace(" ", "-")
                        Dim ipos As Integer = sFilterValue.LastIndexOf("-")
                        sFilterValue = sFilterValue.Remove(ipos)
                        ipos = sFilterValue.IndexOf("-")
                        If ipos > -1 Then
                            sFilterValue = sFilterValue.Remove(ipos, 1)
                            sFilterValue = sFilterValue.Replace("-", " ")
                        End If
                        gv.Rows(irow).Cells(iCell).Text = sFilterValue

                    Else
                        sFilterValue = gv.Rows(irow).Cells(iCell).Text
                    End If
                    'sFilterValue = e.Row.Cells(iCell).Text
                    sJscriptCall = "filterTheRows(" & irow & "," & iCell & ",'" & sFilterValue & "');"
                    gv.Rows(irow).Cells(iCell).Attributes.Add("onClick", sJscriptCall)

                Next
            End If
        End If

    End Sub
    Public Shared Sub HideUnwantedColumnsClientSide(ByRef gv As GridView, ByVal ivisiblecolumns As Integer)
        'used in databound to hide rows on view but available on client code
        'needs testing
        Dim sErr As String = ""
        Try
            If gv.ShowHeader = True Then
                Dim hr As GridViewRow
                hr = gv.HeaderRow
                ' hr.Cells.Count
                '  Dim icells As Integer = gv.Rows(0).Cells.Count
                'For i As Integer = ivisiblecolumns To gv.Columns.Count - 1
                For i As Integer = ivisiblecolumns To gv.Rows(0).Cells.Count - 1
                    hr.Cells(i).Attributes.CssStyle.Add("display", "none")
                Next
            End If
            Dim igvPagesRowIndex As Integer = 0
            For irow As Integer = 0 To gv.Rows.Count - 1

                igvPagesRowIndex += 1
                For i As Integer = ivisiblecolumns To gv.Rows(0).Cells.Count - 1
                    gv.Rows(irow).Cells(i).Attributes.CssStyle.Add("display", "none")
                Next
            Next
        Catch ex As Exception

            ' LogTextFileWriter.DoSomeFileWritingStuff(Session("UCCurrentMethod") + ex.Message)
            sErr = "GridviewfunctionsCls.HideUnwantedColumnsClientSide" & ex.Message

            '          Throw ex
        End Try
    End Sub
    Public Shared Sub HideUnwantedColumnsClientSide(ByRef gv As GridView, ByVal sfirstHeaderTextToHide As String)
        'used in databound to hide rows on view but available on client code
        'needs testing
        Dim sErr As String = ""
        Dim ivisiblecolumns As Integer = 0
        Try
            If gv.ShowHeader = True Then
                Dim hr As GridViewRow
                hr = gv.HeaderRow
                ' hr.Cells.Count
                '  Dim icells As Integer = gv.Rows(0).Cells.Count
                For i As Integer = ivisiblecolumns To gv.Columns.Count - 1
                    If gv.Columns(i).HeaderText.ToLower = sfirstHeaderTextToHide.ToLower Then
                        ivisiblecolumns = i
                    End If
                Next

                For i As Integer = ivisiblecolumns To gv.Rows(0).Cells.Count - 1
                    hr.Cells(i).Attributes.CssStyle.Add("display", "none")
                Next
            End If
            Dim igvPagesRowIndex As Integer = 0
            For irow As Integer = 0 To gv.Rows.Count - 1

                igvPagesRowIndex += 1
                For i As Integer = ivisiblecolumns To gv.Rows(0).Cells.Count - 1
                    gv.Rows(irow).Cells(i).Attributes.CssStyle.Add("display", "none")
                Next
            Next
        Catch ex As Exception

            ' LogTextFileWriter.DoSomeFileWritingStuff(Session("UCCurrentMethod") + ex.Message)
            sErr = "GridviewfunctionsCls.HideUnwantedColumnsClientSide" & ex.Message

            '          Throw ex
        End Try
    End Sub
End Class
