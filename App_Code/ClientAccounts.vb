﻿Imports Microsoft.VisualBasic
Imports App_Code

Public Class ClientAccounts
    Private _ClientAccountAccess As New List(Of ClientAccountAccess)
    Private _dataConn As String


    Public Sub New(ByVal iClientNum As Integer)
        Dim conn As New SetDBConnection()

        _dataConn = conn.EmployeeConn
        Dim thisdata As New CommandsSqlAndOleDb("SelAllClientAccountsByNumberV4", _dataConn)
        thisdata.AddSqlProcParameter("@client_num", iClientNum, SqlDbType.Int, 20)

        Dim dtAccounts As DataTable = thisdata.GetSqlDataTable

        For Each dr As DataRow In dtAccounts.Rows
            Dim AccountAccess As New ClientAccountAccess()

            AccountAccess.ApplicationNum = IIf(IsDBNull(dr("ApplicationNum")), Nothing, dr("ApplicationNum"))
            AccountAccess.ApplicationDesc = IIf(IsDBNull(dr("ApplicationDesc")), Nothing, dr("ApplicationDesc"))
            AccountAccess.LoginName = IIf(IsDBNull(dr("login_name")), Nothing, dr("login_name"))
            AccountAccess.AcctTermDate = IIf(IsDBNull(dr("acct_term_date")), Nothing, dr("acct_term_date"))
            AccountAccess.CreateDate = IIf(IsDBNull(dr("create_date")), Nothing, dr("create_date"))
            AccountAccess.CreatedBy = IIf(IsDBNull(dr("CreatedBy")), Nothing, dr("CreatedBy"))
            AccountAccess.AppBase = IIf(IsDBNull(dr("AppBase")), Nothing, dr("AppBase"))


            _ClientAccountAccess.Add(AccountAccess)

        Next


    End Sub


    Public Function AccountsByAcctNum(ByVal iAcctNum As Integer) As List(Of ClientAccountAccess)
        Dim lReturn As New List(Of ClientAccountAccess)


        Dim query = From acct As ClientAccountAccess In _ClientAccountAccess
                    Where acct.ApplicationNum = iAcctNum
                    Select acct

        For Each acct As ClientAccountAccess In query
            lReturn.Add(acct)

        Next

        Return lReturn
    End Function

    Public ReadOnly Property ClientAccounts As List(Of ClientAccountAccess)
        Get
            Return _ClientAccountAccess

        End Get
    End Property
End Class
