﻿Imports App_Code
Imports System.Web.HttpContext

Public Class GroupQueues

    Private _AllGroupQueues As New List(Of AllGroupQueues)
    Private _AllMyGroups As New List(Of AllMyGroups)
    Private _myGroups As New List(Of String)
    Private _dataConn As String
    Private _sLogin As String
    Private _sDomainAndLogin As String
    Private _arrDomainAndLogin() As String

    Public Sub New()

        'Dim conn As New SetDBConnection()
        '_dataConn = conn.EmployeeConn

        'If GlobalUserAccount.ToString = "" Or GlobalUserAccount.ToString Is Nothing Then
        '    _sDomainAndLogin = HttpContext.Current.Request.ServerVariables("REMOTE_USER") 'Request.Url.AbsolutePath()
        '    _arrDomainAndLogin = Split(_sDomainAndLogin, "\")

        '    _sLogin = _arrDomainAndLogin(1)
        'Else
        '    _sLogin = GlobalUserAccount.ToString

        'End If

        'Using SqlConnection As New SqlConnection(_dataConn)
        '    Dim Cmd As New SqlCommand()

        '    Cmd.CommandText = "SelTechGroup"

        '    Cmd.CommandType = CommandType.StoredProcedure
        '    Cmd.Connection = SqlConnection


        '    SqlConnection.Open()
        '    Using Cmd

        '        Using dr = Cmd.ExecuteReader()

        '            If dr.HasRows Then

        '                Do While dr.Read

        '                    Dim GroupQueue As New AllGroupQueues

        '                    GroupQueue.GroupNum = IIf(IsDBNull(dr("group_num")), Nothing, dr("group_num"))
        '                    GroupQueue.GroupName = IIf(IsDBNull(dr("group_name")), Nothing, dr("group_name"))
        '                    GroupQueue.DisplayStatus = IIf(IsDBNull(dr("DisplayStatus")), Nothing, dr("DisplayStatus"))




        '                    _AllGroupQueues.Add(GroupQueue)
        '                Loop
        '            End If

        '        End Using
        '    End Using
        'End Using


    End Sub
    Public Sub GetAllGroupQueues(ByRef sConn As String)
        If sConn Is Nothing Then
            sConn = HttpContext.Current.Session("CSCConn")
        End If

        Using SqlConnection As New SqlConnection(sConn)
            Dim Cmd As New SqlCommand()

            Cmd.CommandText = "SelTechGroup"
            ' Cmd.Parameters.AddWithValue("@display", "queue")
            ' Cmd.Parameters.AddWithValue("@ntlogin", sNtlogin)
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.Connection = SqlConnection


            SqlConnection.Open()
            Using Cmd

                Using dr = Cmd.ExecuteReader()

                    If dr.HasRows Then

                        Do While dr.Read

                            Dim GroupQueue As New AllGroupQueues

                            GroupQueue.GroupNum = IIf(IsDBNull(dr("group_num")), Nothing, dr("group_num"))
                            GroupQueue.GroupName = IIf(IsDBNull(dr("group_name")), Nothing, dr("group_name"))
                            GroupQueue.DisplayStatus = IIf(IsDBNull(dr("displayDesc")), Nothing, dr("displayDesc"))




                            _AllGroupQueues.Add(GroupQueue)
                        Loop
                    End If

                End Using
            End Using
        End Using

    End Sub

    Public Sub GetAllGroupQueues(ByRef sConn As String, ByVal sDiaplay As String)
        If sConn Is Nothing Then
            sConn = HttpContext.Current.Session("CSCConn")
        End If

        If sDiaplay = "" Then
            sDiaplay = "queue"
        End If

        Using SqlConnection As New SqlConnection(sConn)
            Dim Cmd As New SqlCommand()

            Cmd.CommandText = "SelTechGroup"
            Cmd.Parameters.AddWithValue("@display", sDiaplay)
            'Cmd.Parameters.AddWithValue("@ntlogin", sNtlogin)

            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.Connection = SqlConnection


            SqlConnection.Open()
            Using Cmd

                Using dr = Cmd.ExecuteReader()

                    If dr.HasRows Then

                        Do While dr.Read

                            Dim GroupQueue As New AllGroupQueues

                            GroupQueue.GroupNum = IIf(IsDBNull(dr("group_num")), Nothing, dr("group_num"))
                            GroupQueue.GroupName = IIf(IsDBNull(dr("group_name")), Nothing, dr("group_name"))
                            GroupQueue.DisplayStatus = IIf(IsDBNull(dr("displayDesc")), Nothing, dr("displayDesc"))




                            _AllGroupQueues.Add(GroupQueue)
                        Loop
                    End If

                End Using
            End Using
        End Using

    End Sub

    Public Sub GetAllGroupQueues(ByRef sConn As String, ByVal sDiaplay As String, ByVal sNtlogin As String)
        If sConn Is Nothing Then
            sConn = HttpContext.Current.Session("CSCConn")
        End If

        If sDiaplay = "" Then
            sDiaplay = "queue"
        End If

        Using SqlConnection As New SqlConnection(sConn)
            Dim Cmd As New SqlCommand()

            Cmd.CommandText = "SelTechGroup"
            Cmd.Parameters.AddWithValue("@ntlogin", sNtlogin)
            Cmd.Parameters.AddWithValue("@display", sDiaplay)

            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.Connection = SqlConnection


            SqlConnection.Open()
            Using Cmd

                Using dr = Cmd.ExecuteReader()

                    If dr.HasRows Then

                        Do While dr.Read

                            Dim GroupQueue As New AllGroupQueues

                            GroupQueue.GroupNum = IIf(IsDBNull(dr("group_num")), Nothing, dr("group_num"))
                            GroupQueue.GroupName = IIf(IsDBNull(dr("group_name")), Nothing, dr("group_name"))
                            GroupQueue.DisplayStatus = IIf(IsDBNull(dr("displayDesc")), Nothing, dr("displayDesc"))




                            _AllGroupQueues.Add(GroupQueue)
                        Loop
                    End If

                End Using
            End Using
        End Using

    End Sub

    Public Sub GetMyQueues(ByRef sConn As String, ByRef stechnum As Integer)

        If sConn Is Nothing Then
            sConn = HttpContext.Current.Session("CSCConn")
        End If

        Using SqlConnection As New SqlConnection(sConn)
            Dim Cmd As New SqlCommand()

            'SelGroupsByTechNum @techNum = 4
            Cmd.CommandText = "SelGroupsByTechNumV8"

            Cmd.Parameters.AddWithValue("@techNum", stechnum)
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.Connection = SqlConnection


            SqlConnection.Open()
            Using Cmd

                Using dr = Cmd.ExecuteReader()

                    If dr.HasRows Then

                        Do While dr.Read

                            Dim GroupQueue As New AllMyGroups

                            GroupQueue.GroupNum = IIf(IsDBNull(dr("group_num")), Nothing, dr("group_num"))
                            GroupQueue.GroupName = IIf(IsDBNull(dr("group_name")), Nothing, dr("group_name"))
                            'displaystatus
                            GroupQueue.displaystatus = IIf(IsDBNull(dr("displaystatus")), Nothing, dr("displaystatus"))
                            GroupQueue.DefaultGroup = IIf(IsDBNull(dr("default_group")), Nothing, dr("default_group"))
                            GroupQueue.EmailGroup = IIf(IsDBNull(dr("emailapp")), Nothing, dr("emailapp"))

                            GroupQueue.SecurityLevel = IIf(IsDBNull(dr("securityLevel")), Nothing, dr("securityLevel"))

                            _AllMyGroups.Add(GroupQueue)
                            _myGroups.Add(GroupQueue.GroupNum.ToString)
                        Loop
                    End If

                End Using
            End Using
        End Using

    End Sub
    Public ReadOnly Property AllGroups As List(Of AllGroupQueues)
        Get
            Return _AllGroupQueues

        End Get
    End Property

    Public Function MyGroupByGroupNumber(ByVal iGroupNum As Integer) As List(Of AllMyGroups)

        Dim Groups = (From group As AllMyGroups In _AllMyGroups
                    Where group.GroupNum = iGroupNum
                    Select group).ToList


        Return Groups
    End Function
    Public Function MydefaultGroup() As List(Of AllMyGroups)

        Dim MyDefGroup = (From group As AllMyGroups In _AllMyGroups
                     Where (group.DefaultGroup.ToLower = "yes")
                    Select group).ToList

        Return MyDefGroup

    End Function

    Public Function MySelectedGroup(ByVal iGroupNum As Integer) As List(Of AllMyGroups)

        Dim MySelGroup = (From group As AllMyGroups In _AllMyGroups
                               Where (group.GroupNum = iGroupNum)
                    Select group).ToList

        Return MySelGroup

    End Function

    Public Function AllGroupByGroupNumber(ByVal iGroupNum As Integer) As List(Of AllGroupQueues)

        Dim Groups = (From group As AllGroupQueues In _AllGroupQueues
                    Where group.GroupNum = iGroupNum
                    Select group).ToList


        Return Groups
    End Function
    Public Function AllMyGroups() As List(Of AllMyGroups)

        Dim groups = (From group As AllMyGroups In _AllMyGroups
                        Select group).ToList

        Return groups


    End Function


    Public ReadOnly Property GroupOwnership As List(Of String)
        Get
            Return _myGroups
        End Get
    End Property

End Class
Public Class AllMyGroups
    Public Property GroupNum As Integer
    Public Property GroupName As String
    Public Property DefaultGroup As String
    Public Property displaystatus As String
    Public Property EmailGroup As String
    Public Property SecurityLevel As Integer
End Class
Public Class AllGroupQueues
    Public Property GroupNum As Integer
    Public Property GroupName As String
    Public Property DisplayStatus As String

    Public Function ToDataTable(Of T)(ByVal items As List(Of t)) As DataTable

        Dim dataTable As DataTable = New DataTable(GetType(T).Name)
        Dim Props As Reflection.PropertyInfo() = GetType(T).GetProperties(Reflection.BindingFlags.[Public] Or Reflection.BindingFlags.Instance)

        For Each prop As Reflection.PropertyInfo In Props
            Dim type = (If(prop.PropertyType.IsGenericType AndAlso prop.PropertyType.GetGenericTypeDefinition() = GetType(Nullable(Of )), Nullable.GetUnderlyingType(prop.PropertyType), prop.PropertyType))
            dataTable.Columns.Add(prop.Name, type)
        Next

        For Each item As T In items
            Dim values = New Object(Props.Length - 1) {}

            For i As Integer = 0 To Props.Length - 1
                values(i) = Props(i).GetValue(item, Nothing)
            Next

            dataTable.Rows.Add(values)
        Next

        Return dataTable


    End Function

End Class
