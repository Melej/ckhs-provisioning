﻿Imports Microsoft.VisualBasic
Imports App_Code
Imports System.ComponentModel
Imports System.Web.HttpContext


Public Class SurveyController
    Private _request_num As Integer
    Private _EntryDate As DateTime
    Private _CloseDate As DateTime

    Private _Response1Desc As String
    Private _Response2Desc As String
    Private _Response3Desc As String
    Private _Response4Desc As String
    Private _CloseTechName As String

    Private _TotalTime As String
    Private _CommentsYes As String
    Private _ShortDesc As String
    Private _SurveyComments As String

    Private _dataConn As String



    Private _Surveys As New List(Of SurveyList)
    Public Sub New()


    End Sub
    Public Sub GetSurveyByNum(ByVal requestNum As Integer, ByRef sConn As String)
        _request_num = requestNum


        If sConn Is Nothing Then
            _dataConn = HttpContext.Current.Session("CSCConn")
        Else
            _dataConn = sConn
        End If

        GetSurveyInfo()
    End Sub
    Public Sub GetSurveyInfo()

        If _dataConn Is Nothing Then
            _dataConn = HttpContext.Current.Session("CSCConn")
        End If

        Using sqlConn As New SqlConnection(_dataConn)

            Dim Cmd As New SqlCommand()
            Cmd.CommandText = "SelSurveyReport"
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.Parameters.AddWithValue("@request_num", _request_num)


            Cmd.Connection = sqlConn



            sqlConn.Open()

            Using Cmd

                Using dr = Cmd.ExecuteReader()

                    If dr.HasRows Then

                        Do While dr.Read

                            Try
                                _request_num = IIf(IsDBNull(dr("request_num")), Nothing, dr("request_num"))

                                _EntryDate = IIf(IsDBNull(dr("EntryDate")), Nothing, dr("EntryDate"))
                                _CloseDate = IIf(IsDBNull(dr("CloseDate")), Nothing, dr("CloseDate"))

                                _Response1Desc = IIf(IsDBNull(dr("Response1Desc")), Nothing, dr("Response1Desc"))
                                _Response2Desc = IIf(IsDBNull(dr("Response2Desc")), Nothing, dr("Response2Desc"))
                                _Response3Desc = IIf(IsDBNull(dr("Response3Desc")), Nothing, dr("Response3Desc"))
                                _Response4Desc = IIf(IsDBNull(dr("Response4Desc")), Nothing, dr("Response4Desc"))


                                _CloseTechName = IIf(IsDBNull(dr("CloseTechName")), Nothing, dr("CloseTechName"))
                                _TotalTime = IIf(IsDBNull(dr("TotalTime")), Nothing, dr("TotalTime"))

                                _CommentsYes = IIf(IsDBNull(dr("CommentsYes")), Nothing, dr("CommentsYes"))
                                _ShortDesc = IIf(IsDBNull(dr("ShortDesc")), Nothing, dr("ShortDesc"))
                                _SurveyComments = IIf(IsDBNull(dr("SurveyComments")), Nothing, dr("SurveyComments"))

                            Catch ex As Exception

                            End Try

                        Loop

                    End If

                End Using

            End Using

            sqlConn.Close()

        End Using

    End Sub
    Property request_num As Integer
        Get
            Return _request_num
        End Get
        Set(value As Integer)
            _request_num = value
        End Set
    End Property

    Property EntryDate As DateTime
        Get
            Return _EntryDate
        End Get
        Set(value As DateTime)
            _EntryDate = value
        End Set
    End Property

    Property CloseDate As DateTime
        Get
            Return _CloseDate
        End Get
        Set(value As DateTime)
            _CloseDate = value
        End Set
    End Property

    Property Response1Desc As String
        Get
            Return _Response1Desc
        End Get
        Set(value As String)
            _Response1Desc = value
        End Set
    End Property

    Property Response2Desc As String
        Get
            Return _Response2Desc
        End Get
        Set(value As String)
            _Response2Desc = value
        End Set
    End Property

    Property Response4Desc As String
        Get
            Return _Response4Desc
        End Get
        Set(value As String)
            _Response4Desc = value
        End Set
    End Property

    Property CloseTechName As String
        Get
            Return _CloseTechName
        End Get
        Set(value As String)
            _CloseTechName = value
        End Set
    End Property

    Property ShortDesc As String
        Get
            Return _ShortDesc
        End Get
        Set(value As String)
            _ShortDesc = value
        End Set
    End Property
    Property SurveyComments As String
        Get
            Return _SurveyComments
        End Get
        Set(value As String)
            _SurveyComments = value
        End Set
    End Property


    Property Response3Desc As String
        Get
            Return _Response3Desc
        End Get
        Set(value As String)
            _Response3Desc = value
        End Set
    End Property

    Property TotalTime As String
        Get
            Return _TotalTime
        End Get
        Set(value As String)
            _TotalTime = value
        End Set
    End Property


    Property CommentsYes As String
        Get
            Return _CommentsYes
        End Get
        Set(value As String)
            _CommentsYes = value
        End Set
    End Property


End Class
Public Class SurveyList

    Property request_num As Integer
    Property EntryDate As DateTime
    Property CloseDate As DateTime

    Property Response1Desc As String
    Property Response2Desc As String
    Property Response3Desc As String
    Property Response4Desc As String
    Property CloseTechName As String
    Property TotalTime As String
    Property CommentsYes As String

    Property ShortDesc As String
    Property SurveyComments As String

End Class
