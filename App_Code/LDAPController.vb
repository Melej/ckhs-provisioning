﻿Imports Microsoft.VisualBasic
Imports System.ComponentModel
Public Class LDAPController
    'Private _AllLDAPItems As New List(Of LDAPItems)
    Private _dataConn As String
    Public Sub New()

    End Sub
    Public Sub New(ByVal iClientNum As Integer, ByRef sConn As String)
        client_num = iClientNum


        If sConn Is Nothing Then
            _dataConn = HttpContext.Current.Session("EmployeeConn")
        Else
            _dataConn = sConn
        End If

        GetAllLDAPItems()

    End Sub
    Public Sub New(ByVal iClientNum As Integer, ByVal sLoginname As String, ByRef sConn As String)
        client_num = iClientNum
        LoginName = sLoginname

        If sConn Is Nothing Then
            _dataConn = HttpContext.Current.Session("EmployeeConn")
        Else
            _dataConn = sConn
        End If

        GetAllLDAPItems()

    End Sub
    Public Sub GetAllLDAPItems()

        Using sqlConn As New SqlConnection(_dataConn)



            Using SqlConnection As New SqlConnection(_dataConn)

                Dim Cmd As New SqlCommand()
                Cmd.CommandText = "SelLDAPbyClientNum"
                Cmd.Parameters.AddWithValue("@client_num", client_num)
                Cmd.Parameters.AddWithValue("@ntlogin", LoginName)

                Cmd.CommandType = CommandType.StoredProcedure
                Cmd.Connection = SqlConnection

                SqlConnection.Open()
                Using Cmd

                    Using dr = Cmd.ExecuteReader()

                        If dr.HasRows Then

                            Do While dr.Read

                                Try

                                    client_num = IIf(IsDBNull(dr("client_num")), Nothing, dr("client_num"))
                                    LDAPLoginName = IIf(IsDBNull(dr("LDAPLoginName")), Nothing, dr("LDAPLoginName"))
                                    Fname = IIf(IsDBNull(dr("Fname")), Nothing, dr("Fname"))
                                    LName = IIf(IsDBNull(dr("LName")), Nothing, dr("LName"))

                                    LastVerificationDate = IIf(IsDBNull(dr("LastVerificationDate")), Nothing, dr("LastVerificationDate"))
                                    LDAPEmpnum = IIf(IsDBNull(dr("LDAPEmpnum")), Nothing, dr("LDAPEmpnum"))

                                    userAccountControl = IIf(IsDBNull(dr("userAccountControl")), Nothing, dr("userAccountControl"))
                                    LockOutDate = IIf(IsDBNull(dr("LockOutDate")), Nothing, dr("LockOutDate"))
                                    modifytimestamp = IIf(IsDBNull(dr("modifytimestamp")), Nothing, dr("modifytimestamp"))

                                    createTimestamp = IIf(IsDBNull(dr("createTimestamp")), Nothing, dr("createTimestamp"))
                                    LastDomainLogon = IIf(IsDBNull(dr("LastDomainLogon")), Nothing, dr("LastDomainLogon"))

                                    LastDeviceLogon = IIf(IsDBNull(dr("LastDeviceLogon")), Nothing, dr("LastDeviceLogon"))

                                    LDAPEmail = IIf(IsDBNull(dr("LDAPEmail")), Nothing, dr("LDAPEmail"))
                                    LDAPtelephonenumber = IIf(IsDBNull(dr("LDAPtelephonenumber")), Nothing, dr("LDAPtelephonenumber"))

                                    LDAPdepartment = IIf(IsDBNull(dr("LDAPdepartment")), Nothing, dr("LDAPdepartment"))


                                    '_AllLDAPItems.Add(ldap)
                                Catch ex As Exception

                                End Try

                            Loop


                        End If

                    End Using
                End Using
            End Using

            sqlConn.Close()

        End Using
    End Sub


    <DisplayName("Client#")> Property client_num As Integer
    Property LoginName As String

    <DisplayName("First Name")> Property Fname As String
    <DisplayName("Full Name")> Property FullName As String
    <DisplayName("Last Name")> Property LName As String

    <DisplayName("Last Verify Updated")> Property LastVerificationDate As String
    <DisplayName("Login Name")> Property LDAPLoginName As String

    <DisplayName("EMail")> Property LDAPEmail As String
    <DisplayName("LDAP Emp#")> Property LDAPEmpnum As String
    <DisplayName("Phone")> Property LDAPtelephonenumber As String

    <DisplayName("Account Satus")> Property userAccountControl As String
    <DisplayName("LockOutDate")> Property LockOutDate As String
    <DisplayName("Last LDAP Update Date")> Property modifytimestamp As String
    <DisplayName("Create Date")> Property createTimestamp As String

    <DisplayName("Device Logon")> Property LastDeviceLogon As String
    <DisplayName("Domain Logon")> Property LastDomainLogon As String

    <DisplayName("LDAPdepartment")> Property LDAPdepartment As String




End Class
