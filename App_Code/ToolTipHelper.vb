﻿Imports Microsoft.VisualBasic
Imports App_Code



Public Class ToolTipHelper

    Public Sub New()

    End Sub

    Public Function ToolTipHtml() As String

        Dim sReturn As String
        Dim sb As New StringBuilder

        sb.Append("<table style='background-color:blue'>")
        sb.Append("<tr style='background-color:red'>")
        sb.Append("<td>")
        sb.Append("<b> <b>")
        sb.Append("</td>")
        sb.Append("</tr>")


        Dim i As Integer = 0


        Do While i < 8

            sb.Append("<tr>")
            sb.Append("<td>")
            sb.Append(String.Format("<li>{0}</li>", i * 3))
            sb.Append("</td>")
            sb.Append("</tr>")

            i = i + 1
        Loop

        sb.Append("</table>")


        sReturn = sb.ToString()


        Return sReturn
    End Function

    Public Function ActiveDirToolTipHtml(ByRef actDir As ActiveDir) As String

        Dim sReturn As String
        Dim sb As New StringBuilder



        sb.Append("<table style='background-color:black'>")
        sb.Append("<tr style='background-color:#009999'>")
        sb.Append("<td>")
        sb.Append("<h2>" & actDir.UserAdEntry.Properties.Item("givenName").Value.ToString() & " " & _
                          actDir.UserAdEntry.Properties.Item("sn").Value.ToString() & _
                  "</h2>")
        sb.Append("</td>")
        sb.Append("</tr>")
        sb.Append("<tr>")
        sb.Append("<td>")
        sb.Append("<ul>")

        sb.Append("<li>")
        sb.Append("<h3>Email:</h3> ")
        sb.Append(actDir.UserAdEntry.Properties.Item("mail").Value.ToString())
        sb.Append("</li>")

        sb.Append("<li>")
        sb.Append("<h3>Phone:</h3> ")
        sb.Append(actDir.UserAdEntry.Properties.Item("telephoneNumber").Value.ToString())
        sb.Append("</li>")

        sb.Append("<li>")
        sb.Append("<h3>Department:</h3> ")
        sb.Append(actDir.UserAdEntry.Properties.Item("department").Value.ToString())
        sb.Append("</li>")

        sb.Append("<li>")
        sb.Append("<h3>Last Logon:</h3> ")
        sb.Append(actDir.UserAdEntry.Properties.Item("lastLogonTimeStamp").Value.ToString())
        sb.Append("</li>")


        sb.Append("<li>")
        sb.Append("<h3>Description:</h3> ")
        sb.Append(actDir.UserAdEntry.Properties.Item("description").Value.ToString())
        sb.Append("</li>")


      


        sb.Append("</ul>")
        sb.Append("</td>")
        sb.Append("</tr>")
        sb.Append("</table>")


        sReturn = sb.ToString()


        Return sReturn
    End Function
    Public Function Client3ToolTipHtml(ByRef c3 As Client3) As String

        Dim sReturn As String
        Dim sb As New StringBuilder



        sb.Append("<table style='background-color:black'>")
        sb.Append("<tr style='background-color:#3366ff'>")
        sb.Append("<td>")
        sb.Append("<h2>" & c3.FirstName & " " & _
                          c3.LastName & _
                  "</h2>")
        sb.Append("</td>")
        sb.Append("</tr>")
        sb.Append("<tr>")
        sb.Append("<td>")
        sb.Append("<ul>")

        sb.Append("<li>")
        sb.Append("<h3>Email:</h3> ")
        sb.Append(c3.EMail)
        sb.Append("</li>")

        sb.Append("<li>")
        sb.Append("<h3>Phone:</h3> ")
        sb.Append(c3.Phone)
        sb.Append("</li>")

        sb.Append("<li>")
        sb.Append("<h3>Department:</h3> ")
        sb.Append(c3.DepartmentName)
        sb.Append("</li>")


        sb.Append("<li>")
        sb.Append("<h3>User Position:</h3> ")
        sb.Append(c3.UserPositionDesc)
        sb.Append("</li>")





        sb.Append("</ul>")
        sb.Append("</td>")
        sb.Append("</tr>")
        sb.Append("</table>")


        sReturn = sb.ToString()


        Return sReturn
    End Function
End Class
