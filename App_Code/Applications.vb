﻿Imports Microsoft.VisualBasic
Imports App_Code
Imports System.Web.HttpContext
Public Class Applications
    Private _Applications As New List(Of Application)
    Private _AllApps As New List(Of Application)
    Private _dataConn As String
    Private _sLogin As String
    Private _sDomainAndLogin As String
    Private _arrDomainAndLogin() As String
    Public Sub New()



        Dim conn As New SetDBConnection()
        _dataConn = conn.EmployeeConn





        If GlobalUserAccount.ToString = "" Or GlobalUserAccount.ToString Is Nothing Then
            _sDomainAndLogin = HttpContext.Current.Request.ServerVariables("REMOTE_USER") 'Request.Url.AbsolutePath()
            If _sDomainAndLogin.IndexOf("\") > -1 Then
                _arrDomainAndLogin = Split(_sDomainAndLogin, "\")
                _sLogin = _arrDomainAndLogin(1)
            Else
                _sLogin = _sDomainAndLogin

            End If
            '_arrDomainAndLogin = Split(_sDomainAndLogin, "\")

            '_sLogin = _arrDomainAndLogin(1)
        Else
            _sLogin = GlobalUserAccount.ToString

        End If

        ' Saving the data sset into SQL data table
        ' Dim _dataConn As String = "data source=webdev11\sql2008;initial catalog=employee01;workstation id=CORPIS26900;packet size=4096;user id=helpdesk;persist security info=True;password=crozer;Connect Timeout=99"
        '_dataConn

        Using SqlConnection As New SqlConnection(_dataConn)
            Dim Cmd As New SqlCommand()
            Cmd.CommandText = "SelApplicationCodesBySecurity"
            Cmd.Parameters.AddWithValue("@nt_login", _sLogin)

            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.Connection = SqlConnection


            SqlConnection.Open()
            Using Cmd

                Using dr = Cmd.ExecuteReader()

                    If dr.HasRows Then

                        Do While dr.Read

                            Dim app As New Application

                            app.ApplicationNum = IIf(IsDBNull(dr("ApplicationNum")), Nothing, dr("ApplicationNum"))
                            app.ApplicationDesc = IIf(IsDBNull(dr("ApplicationDesc")), Nothing, dr("ApplicationDesc"))
                            app.AppBase = IIf(IsDBNull(dr("AppBase")), Nothing, dr("AppBase"))
                            app.AppSystem = IIf(IsDBNull(dr("AppSystem")), Nothing, dr("AppSystem"))
                            app.DateDeactivated = IIf(IsDBNull(dr("DateDeactivated")), Nothing, dr("DateDeactivated"))
                            app.AccountTypeCd = IIf(IsDBNull(dr("account_type_cd")), Nothing, dr("account_type_cd"))
                            app.ApplicationGroup = IIf(IsDBNull(dr("ApplicationGroup")), Nothing, dr("ApplicationGroup"))
                            _Applications.Add(app)
                        Loop
                    End If

                End Using
            End Using
        End Using
    End Sub
    Public Sub GetAllApps(ByRef sConn As String)
        If sConn Is Nothing Then
            sConn = HttpContext.Current.Session("EmployeeConn")
        End If

        If HttpContext.Current.Session("LoginID") = "" Or HttpContext.Current.Session("LoginID") Is Nothing Then
            Dim sDomainAndLogin As String = HttpContext.Current.Request.ServerVariables("REMOTE_USER") 'Request.Url.AbsolutePath()
            Dim arrDomainAndLogin As String() = Split(sDomainAndLogin, "\")

            _sLogin = arrDomainAndLogin(1)
        Else
            _sLogin = HttpContext.Current.Session("LoginID")

        End If

        Using SqlConnection As New SqlConnection(sConn)
            Dim Cmd As New SqlCommand()
            Cmd.CommandText = "SelApplicationCodesBySecurity"
            Cmd.Parameters.AddWithValue("@nt_login", _sLogin)
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.Connection = SqlConnection


            SqlConnection.Open()
            Using Cmd

                Using dr = Cmd.ExecuteReader()

                    If dr.HasRows Then

                        Do While dr.Read
                            Dim app As New Application

                            app.ApplicationNum = IIf(IsDBNull(dr("ApplicationNum")), Nothing, dr("ApplicationNum"))
                            app.ApplicationDesc = IIf(IsDBNull(dr("ApplicationDesc")), Nothing, dr("ApplicationDesc"))
                            app.AppBase = IIf(IsDBNull(dr("AppBase")), Nothing, dr("AppBase"))
                            app.AppSystem = IIf(IsDBNull(dr("AppSystem")), Nothing, dr("AppSystem"))
                            app.DateDeactivated = IIf(IsDBNull(dr("DateDeactivated")), Nothing, dr("DateDeactivated"))
                            app.AccountTypeCd = IIf(IsDBNull(dr("account_type_cd")), Nothing, dr("account_type_cd"))
                            app.ApplicationGroup = IIf(IsDBNull(dr("ApplicationGroup")), Nothing, dr("ApplicationGroup"))

                            _AllApps.Add(app)

                        Loop
                    End If

                End Using
            End Using
        End Using
    End Sub
    Public ReadOnly Property AllApps As List(Of Application)
        Get
            Return _AllApps

        End Get
    End Property
    Public Function ApplicationByNum(ByVal iAppNum As Integer) As Application
        Dim appReturn As Application

        Dim query = (From app As Application In _Applications
                    Where app.ApplicationNum = iAppNum
                    Select app).Single()

        appReturn = query

        Return appReturn
    End Function


    Public Function ApplicationsByBase(ByVal iBaseNum As Integer) As List(Of Application)
        'Dim appReturn As Application

        Dim Apps = (From app As Application In _Applications
                    Where app.AppBase = iBaseNum
                    Select app).ToList

        ' appReturn = query

        Return Apps
    End Function

    Public Function BaseApps() As List(Of Application)
        'Dim appReturn As Application

        Dim Apps = (From app As Application In _Applications
                    Where app.AppBase = app.ApplicationNum
                    Select app).ToList

        ' appReturn = query

        Return Apps
    End Function
    Public Function AppBySecurity() As List(Of Application)

        Dim Apps = (From app As Application In _Applications
                    Where app.AppBase = app.ApplicationNum
                    Select app).ToList

        ' appReturn = query

        Return Apps
    End Function

End Class
Public Class Application
    Public Property ApplicationNum As String
    Public Property ApplicationDesc As String
    Public Property AppBase As String
    Public Property AppSystem As String
    Public Property DateDeactivated As DateTime
    Public Property AccountTypeCd As String
    Public Property ApplicationGroup As String

End Class
