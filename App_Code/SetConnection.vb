﻿Imports Microsoft.VisualBasic
Imports System.Configuration
'Imports System.Web.Configuration
Imports System.Linq
Imports System.Collections
Imports System.Collections.Generic
'Imports System.SerializableAttribute
Imports System.Web.HttpContext
Imports System.Web.HttpRequest
Namespace App_Code

    Public Class SetConnection
        Dim sSite As String = ""
        Dim sHospital As String = ""
        Dim sConnectionDataType As String = "" 'casedata ,report ,security,help
        Dim sEnvironment As String = "" '(where is the website ) Environment is either Prod(includes Backup), Test or development 
        Dim sSqlServerType As String = "" 'Needed for DataConn(CaseData) only values can be Prod, backup, or test or dev
        'If a user is on a prod site they can only go to other hospitals connected to a prod (sqlserverType) database
        Dim sWebServer As String = ""
        Dim arHospitals As ArrayList = New ArrayList()
        Dim arTestWebServers As ArrayList = New ArrayList()
        Dim arProdAndBackupWebServers As ArrayList = New ArrayList()
        Dim arSqlServers As ArrayList = New ArrayList()
        Dim arConnectionList As ArrayList = New ArrayList()
        Dim ConnList As New List(Of ConnInfo)


        Public Sub New()
            ' ConfigurationManager.AppSettings.Get("dcmhTestWebServer")
            fillLists()
            ConnList = fillConnections()
        End Sub
        Public Sub fillLists()
            arProdAndBackupWebServers.Clear()
            Dim w As String = ConfigurationManager.AppSettings.Get("ProdWebServers")
            Dim wl() As String = w.Split(";")
            For i As Integer = 0 To wl.Length - 1
                arProdAndBackupWebServers.Add(wl(i))

            Next
            arTestWebServers.Clear()
            Dim s As String = ConfigurationManager.AppSettings.Get("TestWebServers")
            Dim sl() As String = s.Split(";")
            For i As Integer = 0 To sl.Length - 1
                arTestWebServers.Add(sl(i))
            Next

        End Sub
        Public Sub setEnvironment(ByVal sEnvironmentPresetProdTestDevelopmentOrNone As String)
            Dim sWebServer As String = LCase(HttpContext.Current.Session("WebServer"))
            Dim iServerType As Integer = 0 '1 'ProdorBackup,2 Test 3 Developer
            If sEnvironmentPresetProdTestDevelopmentOrNone.ToLower = "none" Then 'Figure ite Out
                For a As Integer = 0 To arProdAndBackupWebServers.Count - 1
                    If sWebServer.IndexOf(arProdAndBackupWebServers(a).ToString.ToLower()) > -1 Then
                        iServerType = 1 'ProdorBackup
                        Current.Session("Environment") = "prod"
                    End If
                Next
                For a As Integer = 0 To arTestWebServers.Count - 1
                    If sWebServer.IndexOf(arTestWebServers(a).ToString.ToLower()) > -1 Then
                        iServerType = 2 'test
                        Current.Session("Environment") = "test"
                    End If
                Next
                If iServerType = 0 Then 'not test or prod
                    iServerType = 3 'Developer
                    Current.Session("Environment") = "development"
                End If
            Else 'use preset for impersonation
                Current.Session("Environment") = sEnvironmentPresetProdTestDevelopmentOrNone.ToLower
            End If


        End Sub
        Public Function getCurrentWebRootFromRequestPath() As String
            Dim sWebRoot As String = ""
            Dim ipos As Integer
            Dim sSplit As String()
            Dim sServer As String = ""
            sServer = Current.Request.ServerVariables("SERVER_NAME")
            ' Current.Session("WebServer") = sServer
            ipos = Current.Request.Path.LastIndexOf("/")
            sWebRoot = Current.Request.Path.Substring(0, ipos + 1)
            sSplit = Current.Request.Path.Split("/")
            ' Current.Session("SiteRoot") =  'Application
            sWebRoot = sServer & "/" & sSplit(1) 'Session("SiteRoot")
            sWebRoot = "/" & sSplit(1) & "/"
            getCurrentWebRootFromRequestPath = "//" & sServer & sWebRoot
        End Function
        Public Function setHospitalFromAddressInfo(ByVal sWebRootin As String) As String
            '  Dim sWebRoot As String = LCase(HttpContext.Current.Session("Webroot"))
            Dim sReturn As String = arHospitals(0)
            For i As Integer = 0 To arHospitals.Count - 1
                If sWebRootin.ToLower.IndexOf(arHospitals(i).ToLower) > -1 Then
                    Current.Session("Hospital") = arHospitals(i).ToString()
                    sReturn = arHospitals(i).ToString()

                End If
            Next
            ' Current.Session("Hospital") = sReturn
            Return sReturn
        End Function
        Public Function fillConnections() As List(Of ConnInfo)
            Dim iID As Integer = 0
            'Dim appSettings As AppSettingsSection = _
            '    DirectCast(ConfigurationManager.GetSection("appSettings"), AppSettingsSection)
            ' Dim ConnList As New List(Of ConnInfo)
            arConnectionList.Clear()
            ConnList.Clear()
            Dim LocalConnList As New List(Of ConnInfo)
            Dim c As ConnInfo
            Dim sType As String = "unknown"
            Dim sConnectionDataTypeInternal As String = ""
            For Each key As String In ConfigurationManager.AppSettings.AllKeys
                Dim value As String = ConfigurationManager.AppSettings.Get(key) 'appSettings.Settings(key).Value

                sType = ""
                If key.ToString.ToLower.IndexOf(".connectionstring") > -1 Then
                    arConnectionList.Add(value)
                    'perhaps separate if and stype could   
                    'If key.ToString.ToLower.IndexOf("prod") > -1 Then
                    '    sType = "prod"
                    'ElseIf key.ToString.ToLower.IndexOf("backup") > -1 Then
                    '    sType = "backup"
                    'ElseIf key.ToString.ToLower.IndexOf("test") > -1 Then
                    '    sType = "test"
                    'ElseIf key.ToString.ToLower.IndexOf("develop") > -1 Then
                    '    sType = "developer"
                    '    'ElseIf key.ToString.ToLower.IndexOf("security") > -1 Then
                    '    '    sType = "security"
                    'End If
                    'Connection DataType
                    If key.ToString.ToLower.IndexOf("casedata") > -1 Then
                        sConnectionDataTypeInternal &= "casedata"
                    End If
                    If key.ToString.ToLower.IndexOf("security") > -1 Then
                        sConnectionDataTypeInternal &= "security"
                    End If
                    If key.ToString.ToLower.IndexOf("help") > -1 Then
                        sConnectionDataTypeInternal &= "help"
                    End If
                    If key.ToString.ToLower.IndexOf("report") > -1 Then
                        sConnectionDataTypeInternal &= "report"
                        'ElseIf key.ToString.ToLower.IndexOf("security") > -1 Then
                        '    sType = "security"
                    End If
                    'SqlserverType
                    If key.ToString.ToLower.IndexOf("prod") > -1 Then
                        sType &= "prod"
                    End If


                    If key.ToString.ToLower.IndexOf("backup") > -1 Then
                        sType &= "backup"
                    End If
                    If key.ToString.ToLower.IndexOf("test") > -1 Then
                        sType &= "test"
                    End If
                    If key.ToString.ToLower.IndexOf("develop") > -1 Then
                        sType &= "developer"
                        'ElseIf key.ToString.ToLower.IndexOf("security") > -1 Then
                        '    sType = "security"
                    End If
                    Dim ConfigParts() As String = Split(value, ":")
                    Dim SettingParts() As String = Split(ConfigParts(0), ";")

                    Dim ConnStringParts() As String = Split(ConfigParts(1), "=")
                    If ConfigParts.Length > 1 Then 'appsetting has both parts
                        ConnStringParts = Split(ConfigParts(1), "=")
                    End If
                    If sType <> "unknown" Then
                        If ConfigParts.Length > 1 And SettingParts.Length > 1 Then 'greater than 1 when webserver atttribute is present ex: WebServer=CaseMgmt02 :


                            c = New ConnInfo()
                            iID += 1
                            c.ConnInfoId = iID
                            c.Name = key.ToString()
                            c.TypeSqlServer = sType
                            c.SqlServer = ReturnSingleConnStringPart(value, "data source") ' ReturnConnStringPart(value, "data source")
                            c.Catalog = ReturnSingleConnStringPart(value, "catalog") 'ReturnConnStringPart(value, "catalog")
                            Dim sWSValue() = Split(SettingParts(0), "=")
                            c.WebServer = sWSValue(1).Trim 'SettingParts(1).Trim ' Current.Session("WebServer")
                            c.ConnString = ConfigParts(1).Trim
                            ' c.Hospital = getHospitalFromCatalog(c.Catalog)
                            c.Hospital = getHospitalFromCatalog(c.Name)
                            c.ConnectionDataType = sConnectionDataTypeInternal  '
                            LocalConnList.Add(c)
                        End If
                    End If
                End If


            Next
            Return LocalConnList
        End Function

        'Public Sub setInitialConnString() 'first time
        '    Dim sHospital As String = ""
        '    'setEnvironment()
        '    sEnvironment = HttpContext.Current.Session("environment")
        '    sConnectionDataType = "CaseData"
        '    sHospital = setHospitalFromAddressInfo(LCase(HttpContext.Current.Session("Webroot")))
        '    Current.Session("Hospital") = sHospital
        '    sWebServer = LCase(HttpContext.Current.Session("WebServer"))
        '    '  Dim sEnviron As String

        '    sSqlServerType = getSqlServerType(sWebServer, sHospital, sConnectionDataType)
        '    'only plac
        '    HttpContext.Current.Session("SqlServerType") = sSqlServerType
        '    If sEnvironment.ToLower.IndexOf("dev") > -1 Then
        '        HttpContext.Current.Session("DataConn") = GetConnBySiteAndHospitalAndDataType(sHospital, HttpContext.Current.Session("Environment"), "") ' ConfigurationManager.AppSettings.Get("TestCrozerCaseData.ConnectionString"
        '        Current.Session("SecurityDataConn") = GetConnBySiteAndHospitalAndDataType(sHospital, HttpContext.Current.Session("Environment"), "security") 'setAndGetSecurityConnBySiteAndHospital()
        '    Else
        '        HttpContext.Current.Session("DataConn") = setAndGetDataConnBySiteAndHospital(sHospital) ', sEnvironment, "casedata")
        '        Current.Session("SecurityDataConn") = GetConnBySiteAndHospitalAndDataType(sHospital, HttpContext.Current.Session("Environment"), "security") 'setAndGetSecurityConnBySiteAndHospital()
        '        'if dataconn goes to test ensure security is in test
        '    End If

        '    ' Complet Data report connection
        '    ' It only exists in production no DeV
        '    'Session("CompleteReportConn")  CompleteReportServerCrozerProd
        '    ' Now insurance report connection
        '    If sHospital.ToLower = "crozer" Then
        '        HttpContext.Current.Session("CompleteReportConn") = ConfigurationManager.AppSettings.Get("CCMCCompleteReport.InsurConnectionString")
        '        '"data source=Hypnos04;initial catalog=CCMCCaseMgt01;workstation id=CORPIS26900;packet size=4096;user id=bedtracking;persist security info=True;password=crozer;Connect Timeout=99"
        '    ElseIf sHospital.ToLower = "dcmh" Then
        '        HttpContext.Current.Session("CompleteReportConn") = ConfigurationManager.AppSettings.Get("DCMHCompleteReport.InsurConnectionString")
        '        ' "data source=Hypnos04;initial catalog=CCMCCaseMgt01;workstation id=CORPIS26900;packet size=4096;user id=bedtracking;persist security info=True;password=crozer;Connect Timeout=99"
        '    ElseIf sHospital.ToLower = "taylor" Then
        '        HttpContext.Current.Session("CompleteReportConn") = ConfigurationManager.AppSettings.Get("TaylorCompleteReport.InsurConnectionString")
        '    ElseIf sHospital.ToLower = "springfield" Then
        '        HttpContext.Current.Session("CompleteReportConn") = ConfigurationManager.AppSettings.Get("SpringfieldCompleteReport.InsurConnectionString")
        '    Else
        '        HttpContext.Current.Session("CompleteReportConn") = ConfigurationManager.AppSettings.Get("CCMCCompleteReport.InsurConnectionString")
        '    End If
        'End Sub

        'Public Function setAndGetDataConnBySiteAndHospital(ByVal sHospital As String) As String
        '    '========================= FirstDayOfWeek time only=========================
        '    'Dim arProdWebServers() As String = Split(ConfigurationManager.AppSettings.Get("ProdAndBackupWebServers"), ":")
        '    '  Dim sWebServer As String = HttpContext.Current.Session("WebServer")
        '    Dim sCurrHospital As String = HttpContext.Current.Session("Hospital")
        '    Dim x() As String = Split(ConfigurationManager.AppSettings.Get("DefaultDeveloperCaseData.ConnectionString"), ":")
        '    'HttpContext.Current.Session("DataConn") = x(1)
        '    ' setAndGetDataConnBySiteAndHospital = Current.Session("DataConn")
        '    ' Dim bIsProductionServer As Boolean
        '    'SqlServerType is only used in CaseData Main dataconn
        '    ''sSqlServerType = getSqlServerType(sWebServer, sHospital, sConnectionDataType)
        '    ' ''only plac
        '    ''HttpContext.Current.Session("SqlServerType") = sSqlServerType
        '    Dim i As Integer = 0
        '    If sSqlServerType.ToLower.IndexOf("develop") > -1 Then 'dev uses test connections
        '        sSqlServerType = "Test"
        '        ' sWebServer="" Test web server
        '    End If
        '    ' Dim s As Object
        '    Select Case sSqlServerType.ToLower
        '        Case "prod", "backup", "test" ' AndAlso conns.WebServer.ToLower = sWebServer.ToLower _


        '            Dim c = From conns In ConnList _
        '                    Where conns.Hospital.ToLower = sHospital.ToLower _
        '                    AndAlso conns.Name.ToLower.IndexOf("casedata") > -1 _
        '                          AndAlso conns.Name.ToLower.IndexOf(sSqlServerType.ToLower) > -1 _
        '                    Select conns
        '            If c Is Nothing Then 'no return
        '                setAndGetDataConnBySiteAndHospital = x(1) 'Current.Session("DataConn")
        '            Else
        '                Dim ictr As Integer = c.Count
        '                'If ictr > 0 Then
        '                setAndGetDataConnBySiteAndHospital = c.FirstOrDefault.ConnString.ToString()

        '                'End If

        '            End If
        '        Case "developer" 'AndAlso conns.WebServer.ToLower = sWebServer.ToLower _
        '            'should not happen
        '    End Select

        'End Function
        Public Function GetConnBySiteAndHospitalAndDataType(ByVal sHospital As String, ByVal sEnvironmentType As String, ByVal sDatatypeIn As String) As String
            'EnvironmentType=Prod Backup Test
            'sConnectionDataType is in Config Key can be Help ,Security ,CaseData or any value you store in the key(Name), ByVal sConnectionDataType As String
            GetConnBySiteAndHospitalAndDataType = "none" '
            If sEnvironmentType.ToLower.IndexOf("dev") > -1 Then
                sEnvironmentType = "test"
            End If
            Dim c = From conns In ConnList _
                    Where conns.Hospital.ToLower = sHospital.ToLower _
                          AndAlso conns.Name.ToLower.IndexOf(sEnvironmentType.ToLower) > -1 _
                          AndAlso conns.Name.ToLower.IndexOf(sDatatypeIn.ToLower) > -1 _
                    Select conns
            Dim ictr As Integer = c.Count
            If ictr > 0 Then
                GetConnBySiteAndHospitalAndDataType = c.FirstOrDefault.ConnString.ToString()

            Else 'fall back to test
                If sDatatypeIn.ToLower.IndexOf("securit") > -1 Then
                    Dim d = From conns In ConnList _
                    Where conns.Hospital.ToLower = sHospital.ToLower _
                          AndAlso conns.Name.ToLower.IndexOf(sEnvironmentType.ToLower) > -1 _
                          AndAlso conns.Name.ToLower.IndexOf(sDatatypeIn.ToLower) > -1 _
                    Select conns
                    GetConnBySiteAndHospitalAndDataType = d.FirstOrDefault.ConnString.ToString()
                End If
            End If
            '    Case "developer" 'AndAlso conns.WebServer.ToLower = sWebServer.ToLower _

            'End Select

        End Function
        Public Function GetDataConnBySiteAndHospitalAndDataType(ByVal sHospital As String, ByVal sSqlServerType As String, ByVal sDatatypeIn As String) As String
            'EnvironmentType=Prod Backup Test
            'sConnectionDataType is in Config Key can be Help ,Security ,CaseData or any value you store in the key(Name), ByVal sConnectionDataType As String
            GetDataConnBySiteAndHospitalAndDataType = "none" '
            Dim c = From conns In ConnList _
                    Where conns.Hospital.ToLower = sHospital.ToLower _
                          AndAlso conns.Name.ToLower.IndexOf(sSqlServerType.ToLower) > -1 _
                          AndAlso conns.Name.ToLower.IndexOf(sDatatypeIn.ToLower) > -1 _
                    Select conns
            Dim ictr As Integer = c.Count
            If ictr > 0 Then
                GetDataConnBySiteAndHospitalAndDataType = c.FirstOrDefault.ConnString.ToString()
            End If
            '    Case "developer" 'AndAlso conns.WebServer.ToLower = sWebServer.ToLower _

            'End Select

        End Function

        Public Function getSqlServerType(ByVal sWEbServerIn As String, ByVal sHospitalIn As String, ByVal sDataType As String) As String
            'test,development,production= LCase(HttpContext.Current.Session("WebServer"))
            If sWEbServerIn.ToLower.IndexOf("local") > -1 Then
                Return "Test"
            Else
                Dim c = From conns In ConnList _
                        Where conns.Hospital.ToLower = sHospitalIn.ToLower _
                        AndAlso conns.Name.ToLower.IndexOf(sDataType.ToLower) > -1 _
                              AndAlso conns.WebServer.ToLower = sWEbServerIn.ToLower _
                        Select conns
                If c.Count > 0 Then

                    Return c.FirstOrDefault.TypeSqlServer.ToString()


                Else
                    'revert to test to ensure securityalso gets set to test
                    HttpContext.Current.Session("environment") = "Test"
                    Return "Test"

                End If
            End If
        End Function
        Public Function ReturnSingleConnStringPart(ByVal sConnIn As String, ByVal sPartToReturn As String) As String
            Dim sConnPartsCollection As String()
            Dim sPartNameValue As String()
            'Dim sSplitLine As String
            Dim sSplitValue() As String
            sSplitValue = sConnIn.Split(":")
            If sSplitValue.Length > 1 Then 'has webserver attr
                sConnPartsCollection = Split(sSplitValue(1), ";")
            Else
                sConnPartsCollection = Split(sConnIn, ";")
            End If

            For i As Integer = 0 To sConnPartsCollection.Length - 1
                If sConnPartsCollection(i).ToLower.IndexOf(sPartToReturn.ToLower()) > -1 Then
                    sPartNameValue = Split(sConnPartsCollection(i), "=")
                    Return sPartNameValue(1)

                End If
            Next
            Return "not found"
        End Function
        Public Function getCurrentConnString() As String
            Return HttpContext.Current.Session("DataConn")
        End Function



        Public Function ChkReconn(ByVal sConnString As String) As Boolean
            Dim sSplit As String()
            Dim sSplitLine As String
            Dim msDataConn As String
            Dim iCtr As Integer
            '  Dim sDup As String()
            Dim bReconnect As Boolean
            'Test connection string
            msDataConn = sConnString

            sSplit = Split(msDataConn, ";")
            Dim inner, outer As Integer
            ''check for value in twice like initial Catalog=dcmhpatienttrack01;dcmhpatienttrack01;"
            If sSplit.Length < 7 Then
                bReconnect = True
                'reset split running reconn
                msDataConn = HttpContext.Current.Session("DataConn") 'Current.Current.Session("DataConn") 'ConnectionMgr.getConnString
                sSplit = Split(msDataConn, ";")
            Else
                For outer = 1 To sSplit.Length - 1
                    For inner = 1 To sSplit.Length - 1
                        If Not inner = outer Then
                            If sSplit(inner).IndexOf(sSplit(outer)) > -1 Then
                                If sSplit(inner).Length < 1 Or sSplit(outer).Length < 1 Then
                                Else ' : skip if ""
                                    bReconnect = True
                                End If

                            ElseIf sSplit(outer).IndexOf(sSplit(inner)) > -1 Then
                                If sSplit(inner).Length < 1 Or sSplit(outer).Length < 1 Then
                                Else ' : skip if ""
                                    bReconnect = True
                                End If
                            End If
                        End If
                    Next
                Next
            End If
            For iCtr = 0 To sSplit.Length - 1
                sSplitLine = sSplit(iCtr).ToLower
                If sSplitLine.IndexOf("cat") > -1 Then
                    If sSplitLine.Length < 20 Then 'empty catalog
                        bReconnect = True
                    End If
                End If
                If sSplitLine.IndexOf("data") > -1 Then
                    If sSplitLine.Length < 12 Then 'empty data source
                        bReconnect = True
                    End If
                End If
                If bReconnect = True Then Exit For
            Next
            'Test Catalog

            ' End If
            Return bReconnect

        End Function
        Public Function getHospitalFromCatalog(ByVal sCatalog As String) As String
            Dim arHospitals() As String = Split(ConfigurationManager.AppSettings.Get("Hospitals"), ";")
            Dim i As Integer = 0
            For i = 0 To arHospitals.Count - 1
                If sCatalog.ToLower.IndexOf(arHospitals(i).ToLower) > -1 Then
                    Return arHospitals(i).ToString()
                    'Exit For
                ElseIf sCatalog.ToLower.IndexOf("ccmc") > -1 Then
                    Return "Crozer"
                    ' Exit For
                End If
            Next
            Return "None"
        End Function
        Public Sub Reconn()

            ''Dim sGenericConn, msCatalogInfo, msSqlServerInfo As String ', sServer, sRedirect, msDataConn sHost, 
            ''msSqlServerInfo = "data source=" & msSqlServer & ";"
            ''msCatalogInfo = "initial catalog=" & msCatalog & ";"


            ''sGenericConn = "workstation id=CORPIS26895;packet size=4096;user id=bedtracking;persist security info=True;password=crozer;Connect Timeout=99"
            ''Current.Session("DataConn") = msSqlServerInfo & msCatalogInfo & sGenericConn

        End Sub
    End Class
    Public Class ConnInfo
        Public ConnInfoId As Integer = 0
        Public Name As String = "" ' CrozerBackup
        Public WebServer As String = ""
        Public TypeSqlServer As String = "" 'Prod Backup Test may be combined also needed for first use with casedata single value'Prod Backup Test
        Public Catalog As String = "" 'DCMHCaseMgt01
        Public Hospital As String = ""
        Public SqlServer As String = ""
        Public ConnString As String = ""
        Public ConnectionDataType As String = "" 'casedata, security,report,help
        'Public SqlServerCatalogType As String = "" ' needed for first use single value'Prod Backup Test

        Public Sub New()

        End Sub
    End Class
End Namespace