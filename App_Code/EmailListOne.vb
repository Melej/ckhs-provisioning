﻿Imports Microsoft.VisualBasic

Public Class EmailListOne
    Private _emailListOne As String
    Public Sub New()

    End Sub
    Public Sub New(ByRef AppBase As String, ByRef AccountReqNum As String, ByRef sConn As String)

        If sConn Is Nothing Then
            sConn = HttpContext.Current.Session("EmployeeConn")
        End If

        Using SqlConnection As New SqlConnection(sConn)

            Dim Cmd As New SqlCommand()

            Cmd.CommandText = "SelEmailDistribution"
            Cmd.Parameters.AddWithValue("@appbase", AppBase)
            Cmd.Parameters.AddWithValue("@AccountRequestNum", AccountReqNum)
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.Connection = SqlConnection


            SqlConnection.Open()
            Using Cmd

                Using dr = Cmd.ExecuteReader()

                    If dr.HasRows Then

                        Do While dr.Read

                            'EmailDistribution.TechNum = IIf(IsDBNull(dr("tech_num")), Nothing, dr("tech_num"))
                            'EmailDistribution.EmailEvent = IIf(IsDBNull(dr("event")), Nothing, dr("event"))
                            'EmailDistribution.AppBase = IIf(IsDBNull(dr("appBase")), Nothing, dr("appBase"))
                            _emailListOne = IIf(IsDBNull(dr("email")), Nothing, dr("email"))
                        Loop
                    End If

                End Using
            End Using

        End Using


    End Sub
    Public Sub New(ByRef AccountReqNum As String, ByRef sConn As String)

        If sConn Is Nothing Then
            sConn = HttpContext.Current.Session("EmployeeConn")
        End If

        Using SqlConnection As New SqlConnection(sConn)

            Dim Cmd As New SqlCommand()

            Cmd.CommandText = "SelEmailDistribution"
            Cmd.Parameters.AddWithValue("@AccountRequestNum", AccountReqNum)
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.Connection = SqlConnection


            SqlConnection.Open()
            Using Cmd

                Using dr = Cmd.ExecuteReader()

                    If dr.HasRows Then

                        Do While dr.Read

                            'EmailDistribution.TechNum = IIf(IsDBNull(dr("tech_num")), Nothing, dr("tech_num"))
                            'EmailDistribution.EmailEvent = IIf(IsDBNull(dr("event")), Nothing, dr("event"))
                            'EmailDistribution.AppBase = IIf(IsDBNull(dr("appBase")), Nothing, dr("appBase"))
                            _emailListOne = IIf(IsDBNull(dr("email")), Nothing, dr("email"))
                        Loop
                    End If

                End Using
            End Using

        End Using


    End Sub
    Public Sub getP1emaillist(ByRef sConn As String)

        If sConn Is Nothing Then
            sConn = HttpContext.Current.Session("CSCConn")
        End If

        Using SqlConnection As New SqlConnection(sConn)

            Dim Cmd As New SqlCommand()

            Cmd.CommandText = "SelP1PagersV8"
            'Cmd.Parameters.AddWithValue("@AccountRequestNum", AccountReqNum)
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.Connection = SqlConnection


            SqlConnection.Open()
            Using Cmd

                Using dr = Cmd.ExecuteReader()

                    If dr.HasRows Then

                        Do While dr.Read

                            'EmailDistribution.TechNum = IIf(IsDBNull(dr("tech_num")), Nothing, dr("tech_num"))
                            'EmailDistribution.EmailEvent = IIf(IsDBNull(dr("event")), Nothing, dr("event"))
                            'EmailDistribution.AppBase = IIf(IsDBNull(dr("appBase")), Nothing, dr("appBase"))
                            _emailListOne = IIf(IsDBNull(dr("email")), Nothing, dr("email"))
                        Loop
                    End If

                End Using
            End Using

        End Using

    End Sub
    Public Sub getP1emaillist(ByRef sConn As String, ByRef TicketNum As String)

        If sConn Is Nothing Then
            sConn = HttpContext.Current.Session("CSCConn")
        End If

        Using SqlConnection As New SqlConnection(sConn)

            Dim Cmd As New SqlCommand()

            Cmd.CommandText = "SelP1PagersV8"
            Cmd.Parameters.AddWithValue("@requestnum", TicketNum)
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.Connection = SqlConnection


            SqlConnection.Open()
            Using Cmd

                Using dr = Cmd.ExecuteReader()

                    If dr.HasRows Then

                        Do While dr.Read

                            'EmailDistribution.TechNum = IIf(IsDBNull(dr("tech_num")), Nothing, dr("tech_num"))
                            'EmailDistribution.EmailEvent = IIf(IsDBNull(dr("event")), Nothing, dr("event"))
                            'EmailDistribution.AppBase = IIf(IsDBNull(dr("appBase")), Nothing, dr("appBase"))
                            _emailListOne = IIf(IsDBNull(dr("email")), Nothing, dr("email"))
                        Loop
                    End If

                End Using
            End Using

        End Using

    End Sub
    Public Sub getGroupP1emaillist(ByRef sConn As String, ByRef TicketNum As String, ByVal sStatus As String)

        If sConn Is Nothing Then
            sConn = HttpContext.Current.Session("CSCConn")
        End If

        Using SqlConnection As New SqlConnection(sConn)

            Dim Cmd As New SqlCommand()

            Cmd.CommandText = "SelGroupP1EmailV9"
            Cmd.Parameters.AddWithValue("@requestnum", TicketNum)
            Cmd.Parameters.AddWithValue("@status", sStatus)
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.Connection = SqlConnection


            SqlConnection.Open()
            Using Cmd

                Using dr = Cmd.ExecuteReader()

                    If dr.HasRows Then

                        Do While dr.Read
                            _emailListOne = IIf(IsDBNull(dr("email")), Nothing, dr("email"))
                        Loop
                    End If

                End Using
            End Using

        End Using

    End Sub

    Public Sub GetGroupAssignEmailList(ByRef sConn As String, ByRef sGroupNum As String, ByRef sRequestnum As String)

        If sConn Is Nothing Then
            sConn = HttpContext.Current.Session("CSCConn")
        End If

        Using SqlConnection As New SqlConnection(sConn)

            Dim Cmd As New SqlCommand()

            Cmd.CommandText = "SelGroupAssignEmailV9"
            Cmd.Parameters.AddWithValue("@groupNum", sGroupNum)
            Cmd.Parameters.AddWithValue("@requestnum", sRequestnum)
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.Connection = SqlConnection


            SqlConnection.Open()
            Using Cmd

                Using dr = Cmd.ExecuteReader()

                    If dr.HasRows Then

                        Do While dr.Read

                            _emailListOne = IIf(IsDBNull(dr("email")), Nothing, dr("email"))
                        Loop
                    End If

                End Using
            End Using

        End Using

    End Sub
    Public Sub getTicketemaillist(ByRef sConn As String, ByRef TicketNum As String, ByRef sFormated As String)

        If sConn Is Nothing Then
            sConn = HttpContext.Current.Session("CSCConn")
        End If

        Using SqlConnection As New SqlConnection(sConn)

            Dim Cmd As New SqlCommand()

            Cmd.CommandText = "SelP1PagersV8"
            Cmd.Parameters.AddWithValue("@requestnum", TicketNum)
            Cmd.Parameters.AddWithValue("@formated", sFormated)
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.Connection = SqlConnection


            SqlConnection.Open()
            Using Cmd

                Using dr = Cmd.ExecuteReader()

                    If dr.HasRows Then

                        Do While dr.Read

                            'EmailDistribution.TechNum = IIf(IsDBNull(dr("tech_num")), Nothing, dr("tech_num"))
                            'EmailDistribution.EmailEvent = IIf(IsDBNull(dr("event")), Nothing, dr("event"))
                            'EmailDistribution.AppBase = IIf(IsDBNull(dr("appBase")), Nothing, dr("appBase"))
                            _emailListOne = IIf(IsDBNull(dr("email")), Nothing, dr("email"))
                        Loop
                    End If

                End Using
            End Using

        End Using

    End Sub
    Public Sub getTechemaillist(ByRef sConn As String, ByRef sFormated As String, ByVal iTechNum As String)

        If sConn Is Nothing Then
            sConn = HttpContext.Current.Session("CSCConn")
        End If

        Using SqlConnection As New SqlConnection(sConn)

            Dim Cmd As New SqlCommand()

            Cmd.CommandText = "SelTechPagersV8"
            Cmd.Parameters.AddWithValue("@technum", iTechNum)
            Cmd.Parameters.AddWithValue("@formated", sFormated)
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.Connection = SqlConnection


            SqlConnection.Open()
            Using Cmd

                Using dr = Cmd.ExecuteReader()

                    If dr.HasRows Then

                        Do While dr.Read

                            'EmailDistribution.TechNum = IIf(IsDBNull(dr("tech_num")), Nothing, dr("tech_num"))
                            'EmailDistribution.EmailEvent = IIf(IsDBNull(dr("event")), Nothing, dr("event"))
                            'EmailDistribution.AppBase = IIf(IsDBNull(dr("appBase")), Nothing, dr("appBase"))
                            _emailListOne = IIf(IsDBNull(dr("email")), Nothing, dr("email"))
                        Loop
                    End If

                End Using
            End Using

        End Using

    End Sub

    Public Property EmailListReturn As String

        Get
            Return _emailListOne
        End Get

        Set
        End Set


    End Property
End Class

