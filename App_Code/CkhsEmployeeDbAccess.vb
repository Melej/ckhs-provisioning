﻿Imports Microsoft.VisualBasic
Imports App_Code
Imports System.Web.HttpContext
Public Class CkhsEmployeeDbAccess
    Private _dataConn As String

    Public Sub New(ByVal dataConn As String)
        If dataConn Is Nothing Then
            dataConn = HttpContext.Current.Session("EmployeeConn")
        End If

        _dataConn = dataConn
    End Sub


    Public Function GetCkhsEmployee(ByVal ClientNum As Integer) As CkhsEmployee
        Dim employee As New CkhsEmployee
        Using sqlConn As New SqlConnection(_dataConn)


			Dim Cmd As New SqlCommand()
            Cmd.CommandText = "SelClientByNumberV20"
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.Parameters.AddWithValue("@client_num", ClientNum)
            Cmd.Connection = sqlConn



            sqlConn.Open()

            Using Cmd

                Using dr = Cmd.ExecuteReader()

                    If dr.HasRows Then

                        Do While dr.Read


                            Try

                                employee.ClientNum = IIf(IsDBNull(dr("clientnum")), Nothing, dr("clientnum"))
                                employee.FirstName = IIf(IsDBNull(dr("firstname")), Nothing, dr("firstname"))
                                employee.FullName = IIf(IsDBNull(dr("full_name")), Nothing, dr("full_name"))
                                employee.LastName = IIf(IsDBNull(dr("lastname")), Nothing, dr("lastname"))
                                employee.Mi = IIf(IsDBNull(dr("mi")), Nothing, dr("mi"))
                                employee.LastUpdated = IIf(IsDBNull(dr("LastUpdated")), Nothing, dr("LastUpdated"))
                                employee.LoginName = IIf(IsDBNull(dr("loginname")), Nothing, dr("loginname"))
                                employee.Phone = IIf(IsDBNull(dr("phone")), Nothing, dr("phone"))
                                employee.Fax = IIf(IsDBNull(dr("fax")), Nothing, dr("fax"))
                                employee.EMail = IIf(IsDBNull(dr("email")), Nothing, dr("email"))
                                employee.AlternatePhone = IIf(IsDBNull(dr("AlternatePhone")), Nothing, dr("AlternatePhone"))
                                employee.FacilityCd = IIf(IsDBNull(dr("facilitycd")), Nothing, dr("facilitycd"))
                                employee.BuildingCd = IIf(IsDBNull(dr("buildingcd")), Nothing, dr("buildingcd"))
                                employee.Office = IIf(IsDBNull(dr("office")), Nothing, dr("office"))
                                employee.Floor = IIf(IsDBNull(dr("floor")), Nothing, dr("floor"))
                                employee.Title = IIf(IsDBNull(dr("title")), Nothing, dr("title"))
                                employee.Suffix = IIf(IsDBNull(dr("suffix")), Nothing, dr("suffix"))
                                employee.Address1 = IIf(IsDBNull(dr("address1")), Nothing, dr("address1"))
                                employee.Address2 = IIf(IsDBNull(dr("address2")), Nothing, dr("address2"))
                                employee.City = IIf(IsDBNull(dr("city")), Nothing, dr("city"))
                                employee.State = IIf(IsDBNull(dr("state")), Nothing, dr("state"))
                                employee.Zip = IIf(IsDBNull(dr("zip")), Nothing, dr("zip"))
                                employee.EntityCd = IIf(IsDBNull(dr("entitycd")), Nothing, dr("entitycd"))
                                employee.EntityName = IIf(IsDBNull(dr("entity_name")), Nothing, dr("entity_name"))
                                employee.DepartmentCd = IIf(IsDBNull(dr("departmentcd")), Nothing, dr("departmentcd"))
                                employee.DepartmentName = IIf(IsDBNull(dr("department_name")), Nothing, dr("department_name"))
                                employee.FacilityName = IIf(IsDBNull(dr("facilityname")), Nothing, dr("facilityname"))
                                employee.DateDeactivated = IIf(IsDBNull(dr("datedeactivated")), Nothing, dr("datedeactivated"))
                                employee.Pager = IIf(IsDBNull(dr("pager")), Nothing, dr("pager"))
                                employee.PagerType = IIf(IsDBNull(dr("pagertype")), Nothing, dr("pagertype"))
                                employee.EmpTypeCd = IIf(IsDBNull(dr("emptypecd")), Nothing, dr("emptypecd"))
                                employee.StartDate = IIf(IsDBNull(dr("startdate")), Nothing, dr("startdate"))
                                employee.EndDate = IIf(IsDBNull(dr("enddate")), Nothing, dr("enddate"))
                                employee.SecurityTypeCd = IIf(IsDBNull(dr("securitytypecd")), Nothing, dr("securitytypecd"))
                                employee.SecurityDesc = IIf(IsDBNull(dr("securitydesc")), Nothing, dr("securitydesc"))
                                employee.GroupNum = IIf(IsDBNull(dr("groupnum")), Nothing, dr("groupnum"))
                                employee.GroupName = IIf(IsDBNull(dr("groupname")), Nothing, dr("groupname"))
                                employee.UserPositionDesc = IIf(IsDBNull(dr("userpositiondesc")), Nothing, dr("userpositiondesc"))
                                employee.DoctorMasterNum = IIf(IsDBNull(dr("doctormasternum")), Nothing, dr("doctormasternum"))
                                employee.ShareDrive = IIf(IsDBNull(dr("sharedrive")), Nothing, dr("sharedrive"))
                                employee.Npi = IIf(IsDBNull(dr("npi")), Nothing, dr("npi"))
                                employee.Taxonomy = IIf(IsDBNull(dr("taxonomy")), Nothing, dr("taxonomy"))
                                'employee.Provider = IIf(IsDBNull(dr("provider")), Nothing, dr("provider"))
                                employee.Han = IIf(IsDBNull(dr("han")), Nothing, dr("han"))
                                employee.LicensesNo = IIf(IsDBNull(dr("LicensesNo")), Nothing, dr("LicensesNo"))
                                employee.CredentialedDate = IIf(IsDBNull(dr("credentialedDate")), Nothing, dr("credentialedDate"))
                                employee.Specialty = IIf(IsDBNull(dr("specialty")), Nothing, dr("specialty"))
                                employee.CCMCadmitRights = IIf(IsDBNull(dr("CCMCadmitRights")), Nothing, dr("CCMCadmitRights"))
                                employee.DCMHadmitRights = IIf(IsDBNull(dr("DCMHadmitRights")), Nothing, dr("DCMHadmitRights"))
                                employee.SiemensEmpNum = IIf(IsDBNull(dr("siemensempnum")), Nothing, dr("siemensempnum"))
                                'employee.NonHanLocation = IIf(IsDBNull(dr("NonHanLocation")), Nothing, dr("NonHanLocation"))
                                employee.VendorName = IIf(IsDBNull(dr("VendorName")), Nothing, dr("VendorName"))
                                employee.DateCreated = IIf(IsDBNull(dr("DateCreated")), Nothing, dr("DateCreated"))

                                employee.AKALastName = IIf(IsDBNull(dr("AKALastName")), Nothing, dr("AKALastName"))
                                employee.AuthorizationEmpNum = IIf(IsDBNull(dr("AuthorizationEmpNum")), Nothing, dr("AuthorizationEmpNum"))
                                employee.Writeorders = IIf(IsDBNull(dr("Writeorders")), Nothing, dr("Writeorders"))
                                employee.CCMCConsults = IIf(IsDBNull(dr("CCMCConsults")), Nothing, dr("CCMCConsults"))
                                employee.DCMHConsults = IIf(IsDBNull(dr("DCMHConsults")), Nothing, dr("DCMHConsults"))
                                employee.TaylorConsults = IIf(IsDBNull(dr("TaylorConsults")), Nothing, dr("TaylorConsults"))
                                employee.SpringfieldConsults = IIf(IsDBNull(dr("SpringfieldConsults")), Nothing, dr("SpringfieldConsults"))
                                employee.MedicareID = IIf(IsDBNull(dr("MedicareID")), Nothing, dr("MedicareID"))
                                employee.BlueCrossID = IIf(IsDBNull(dr("BlueCrossID")), Nothing, dr("BlueCrossID"))
                                employee.SureScriptID = IIf(IsDBNull(dr("SureScriptID")), Nothing, dr("SureScriptID"))
                                employee.DEAID = IIf(IsDBNull(dr("DEAID")), Nothing, dr("DEAID"))
                                employee.DEAExtensionID = IIf(IsDBNull(dr("DEAExtensionID")), Nothing, dr("DEAExtensionID"))
                                employee.UniformPhysID = IIf(IsDBNull(dr("UniformPhysID")), Nothing, dr("UniformPhysID"))
                                employee.MedicGroupID = IIf(IsDBNull(dr("MedicGroupID")), Nothing, dr("MedicGroupID"))
                                employee.MedicGroupPhysID = IIf(IsDBNull(dr("MedicGroupPhysID")), Nothing, dr("MedicGroupPhysID"))
                                employee.RoleNum = IIf(IsDBNull(dr("RoleNum")), Nothing, dr("RoleNum"))
                                employee.PositionNum = IIf(IsDBNull(dr("PositionNum")), Nothing, dr("PositionNum"))
                                employee.PositionRoleNum = IIf(IsDBNull(dr("PositionRoleNum")), Nothing, dr("PositionRoleNum"))
                                employee.LastFour = IIf(IsDBNull(dr("lastfour")), Nothing, dr("lastfour"))

                                employee.PMHTitlePosition = IIf(IsDBNull(dr("PMHTitlePosition")), Nothing, dr("PMHTitlePosition"))
                                employee.EmploymentCategoryName = IIf(IsDBNull(dr("EmploymentCategoryName")), Nothing, dr("EmploymentCategoryName"))

                            Catch ex As Exception

                            End Try

                        Loop

                    End If

                End Using

            End Using

            sqlConn.Close()

        End Using
        Return employee

    End Function

    Public Function GetDeactivatedEmployee(ByVal ClientNum As Integer) As CkhsEmployee
        Dim employee As New CkhsEmployee
        Using sqlConn As New SqlConnection(_dataConn)

            Dim Cmd As New SqlCommand()
            Cmd.CommandText = "SelDeactivatedClientByNumberV20"
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.Parameters.AddWithValue("@client_num", ClientNum)
            Cmd.Connection = sqlConn



            sqlConn.Open()

            Using Cmd

                Using dr = Cmd.ExecuteReader()

                    If dr.HasRows Then

                        Do While dr.Read


                            Try

                                employee.ClientNum = IIf(IsDBNull(dr("clientnum")), Nothing, dr("clientnum"))
                                employee.FirstName = IIf(IsDBNull(dr("firstname")), Nothing, dr("firstname"))
                                employee.FullName = IIf(IsDBNull(dr("full_name")), Nothing, dr("full_name"))
                                employee.LastName = IIf(IsDBNull(dr("lastname")), Nothing, dr("lastname"))
                                employee.Mi = IIf(IsDBNull(dr("mi")), Nothing, dr("mi"))
                                employee.LastUpdated = IIf(IsDBNull(dr("LastUpdated")), Nothing, dr("LastUpdated"))
                                employee.LoginName = IIf(IsDBNull(dr("loginname")), Nothing, dr("loginname"))
                                employee.Phone = IIf(IsDBNull(dr("phone")), Nothing, dr("phone"))
                                employee.Fax = IIf(IsDBNull(dr("fax")), Nothing, dr("fax"))
                                employee.EMail = IIf(IsDBNull(dr("email")), Nothing, dr("email"))
                                employee.AlternatePhone = IIf(IsDBNull(dr("AlternatePhone")), Nothing, dr("AlternatePhone"))
                                employee.FacilityCd = IIf(IsDBNull(dr("facilitycd")), Nothing, dr("facilitycd"))
                                employee.BuildingCd = IIf(IsDBNull(dr("buildingcd")), Nothing, dr("buildingcd"))
                                employee.Office = IIf(IsDBNull(dr("office")), Nothing, dr("office"))
                                employee.Floor = IIf(IsDBNull(dr("floor")), Nothing, dr("floor"))
                                employee.Title = IIf(IsDBNull(dr("title")), Nothing, dr("title"))
                                employee.Suffix = IIf(IsDBNull(dr("suffix")), Nothing, dr("suffix"))
                                employee.Address1 = IIf(IsDBNull(dr("address1")), Nothing, dr("address1"))
                                employee.Address2 = IIf(IsDBNull(dr("address2")), Nothing, dr("address2"))
                                employee.City = IIf(IsDBNull(dr("city")), Nothing, dr("city"))
                                employee.State = IIf(IsDBNull(dr("state")), Nothing, dr("state"))
                                employee.Zip = IIf(IsDBNull(dr("zip")), Nothing, dr("zip"))
                                employee.EntityCd = IIf(IsDBNull(dr("entitycd")), Nothing, dr("entitycd"))
                                employee.EntityName = IIf(IsDBNull(dr("entity_name")), Nothing, dr("entity_name"))
                                employee.DepartmentCd = IIf(IsDBNull(dr("departmentcd")), Nothing, dr("departmentcd"))
                                employee.DepartmentName = IIf(IsDBNull(dr("department_name")), Nothing, dr("department_name"))
                                employee.FacilityName = IIf(IsDBNull(dr("facilityname")), Nothing, dr("facilityname"))
                                employee.DateDeactivated = IIf(IsDBNull(dr("datedeactivated")), Nothing, dr("datedeactivated"))
                                employee.Pager = IIf(IsDBNull(dr("pager")), Nothing, dr("pager"))
                                employee.PagerType = IIf(IsDBNull(dr("pagertype")), Nothing, dr("pagertype"))
                                employee.EmpTypeCd = IIf(IsDBNull(dr("emptypecd")), Nothing, dr("emptypecd"))
                                employee.StartDate = IIf(IsDBNull(dr("startdate")), Nothing, dr("startdate"))
                                employee.EndDate = IIf(IsDBNull(dr("enddate")), Nothing, dr("enddate"))
                                employee.SecurityTypeCd = IIf(IsDBNull(dr("securitytypecd")), Nothing, dr("securitytypecd"))
                                employee.SecurityDesc = IIf(IsDBNull(dr("securitydesc")), Nothing, dr("securitydesc"))
                                employee.GroupNum = IIf(IsDBNull(dr("groupnum")), Nothing, dr("groupnum"))
                                employee.GroupName = IIf(IsDBNull(dr("groupname")), Nothing, dr("groupname"))
                                employee.UserPositionDesc = IIf(IsDBNull(dr("userpositiondesc")), Nothing, dr("userpositiondesc"))
                                employee.CernerPositionDesc = IIf(IsDBNull(dr("CernerPositionDesc")), Nothing, dr("CernerPositionDesc"))

                                employee.DoctorMasterNum = IIf(IsDBNull(dr("doctormasternum")), Nothing, dr("doctormasternum"))
                                employee.ShareDrive = IIf(IsDBNull(dr("sharedrive")), Nothing, dr("sharedrive"))
                                employee.Npi = IIf(IsDBNull(dr("npi")), Nothing, dr("npi"))
                                employee.Taxonomy = IIf(IsDBNull(dr("taxonomy")), Nothing, dr("taxonomy"))
                                employee.Provider = IIf(IsDBNull(dr("provider")), Nothing, dr("provider"))
                                employee.Han = IIf(IsDBNull(dr("han")), Nothing, dr("han"))
                                employee.LicensesNo = IIf(IsDBNull(dr("LicensesNo")), Nothing, dr("LicensesNo"))
                                employee.CredentialedDate = IIf(IsDBNull(dr("credentialedDate")), Nothing, dr("credentialedDate"))
                                employee.Specialty = IIf(IsDBNull(dr("specialty")), Nothing, dr("specialty"))
                                employee.CCMCadmitRights = IIf(IsDBNull(dr("CCMCadmitRights")), Nothing, dr("CCMCadmitRights"))
                                employee.DCMHadmitRights = IIf(IsDBNull(dr("DCMHadmitRights")), Nothing, dr("DCMHadmitRights"))
                                employee.SiemensEmpNum = IIf(IsDBNull(dr("siemensempnum")), Nothing, dr("siemensempnum"))
                                employee.NonHanLocation = IIf(IsDBNull(dr("NonHanLocation")), Nothing, dr("NonHanLocation"))
                                employee.VendorName = IIf(IsDBNull(dr("VendorName")), Nothing, dr("VendorName"))
                                employee.DateCreated = IIf(IsDBNull(dr("DateCreated")), Nothing, dr("DateCreated"))

                                employee.AKALastName = IIf(IsDBNull(dr("AKALastName")), Nothing, dr("AKALastName"))
                                employee.AuthorizationEmpNum = IIf(IsDBNull(dr("AuthorizationEmpNum")), Nothing, dr("AuthorizationEmpNum"))
                                employee.Writeorders = IIf(IsDBNull(dr("Writeorders")), Nothing, dr("Writeorders"))
                                employee.CCMCConsults = IIf(IsDBNull(dr("CCMCConsults")), Nothing, dr("CCMCConsults"))
                                employee.DCMHConsults = IIf(IsDBNull(dr("DCMHConsults")), Nothing, dr("DCMHConsults"))
                                employee.TaylorConsults = IIf(IsDBNull(dr("TaylorConsults")), Nothing, dr("TaylorConsults"))
                                employee.SpringfieldConsults = IIf(IsDBNull(dr("SpringfieldConsults")), Nothing, dr("SpringfieldConsults"))
                                employee.MedicareID = IIf(IsDBNull(dr("MedicareID")), Nothing, dr("MedicareID"))
                                employee.BlueCrossID = IIf(IsDBNull(dr("BlueCrossID")), Nothing, dr("BlueCrossID"))
                                employee.SureScriptID = IIf(IsDBNull(dr("SureScriptID")), Nothing, dr("SureScriptID"))
                                employee.DEAID = IIf(IsDBNull(dr("DEAID")), Nothing, dr("DEAID"))
                                employee.DEAExtensionID = IIf(IsDBNull(dr("DEAExtensionID")), Nothing, dr("DEAExtensionID"))
                                employee.UniformPhysID = IIf(IsDBNull(dr("UniformPhysID")), Nothing, dr("UniformPhysID"))
                                employee.MedicGroupID = IIf(IsDBNull(dr("MedicGroupID")), Nothing, dr("MedicGroupID"))
                                employee.MedicGroupPhysID = IIf(IsDBNull(dr("MedicGroupPhysID")), Nothing, dr("MedicGroupPhysID"))
                                employee.RoleNum = IIf(IsDBNull(dr("RoleNum")), Nothing, dr("RoleNum"))
                                employee.PositionNum = IIf(IsDBNull(dr("PositionNum")), Nothing, dr("PositionNum"))
                                employee.PositionRoleNum = IIf(IsDBNull(dr("PositionRoleNum")), Nothing, dr("PositionRoleNum"))
                                employee.LastFour = IIf(IsDBNull(dr("lastfour")), Nothing, dr("lastfour"))


                                employee.PMHTitlePosition = IIf(IsDBNull(dr("PMHTitlePosition")), Nothing, dr("PMHTitlePosition"))
                                employee.EmploymentCategoryName = IIf(IsDBNull(dr("EmploymentCategoryName")), Nothing, dr("EmploymentCategoryName"))
                            Catch ex As Exception

                            End Try

                        Loop

                    End If

                End Using

            End Using

            sqlConn.Close()

        End Using
        Return employee

    End Function
    Public Sub CheckEmployeeType(ByRef EmptypeCd As String)

        Dim crt As New Control
        crt.ID = "Titleddl"
        If EmptypeCd.ToLower = "allied health" Then
            Dim ctrl As Control = DataCheckingAndValidation.SetChildControlsRecursiveToEnabledFalse(crt, False)

        End If

    End Sub
End Class
