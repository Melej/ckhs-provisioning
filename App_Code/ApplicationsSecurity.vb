﻿Imports Microsoft.VisualBasic
Imports App_Code

Public Class ApplicationsSecurity
    Private _ApplicatioBysecurity As New List(Of ApplicationItems)
    Private _dataConn As String
    Private _dtApplications As DataTable
    Public Sub New()

    End Sub
    Public Sub New(ByRef sConn As String, ByRef sLogin As String)
        If sConn Is Nothing Then
            sConn = HttpContext.Current.Session("EmployeeConn")
        End If

        Using SqlConnection As New SqlConnection(sConn)
            Dim Cmd As New SqlCommand()

            Cmd.CommandText = "SelApplicationCodesBySecurity"
            Cmd.Parameters.AddWithValue("@nt_login", sLogin)

            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.Connection = SqlConnection


            SqlConnection.Open()
            Using Cmd

                Using dr = Cmd.ExecuteReader()

                    If dr.HasRows Then

                        Do While dr.Read
                            Dim app As New ApplicationItems

                            app.ApplicationNum = IIf(IsDBNull(dr("ApplicationNum")), Nothing, dr("ApplicationNum"))
                            app.ApplicationDesc = IIf(IsDBNull(dr("ApplicationDesc")), Nothing, dr("ApplicationDesc"))
                            app.AppBase = IIf(IsDBNull(dr("AppBase")), Nothing, dr("AppBase"))
                            app.AppSystem = IIf(IsDBNull(dr("AppSystem")), Nothing, dr("AppSystem"))
                            'app.DateDeactivated = IIf(IsDBNull(dr("DateDeactivated")), Nothing, dr("DateDeactivated"))
                            app.AccountTypeCd = IIf(IsDBNull(dr("account_type_cd")), Nothing, dr("account_type_cd"))

                            _ApplicatioBysecurity.Add(app)

                        Loop
                    End If

                End Using
            End Using
        End Using
    End Sub
    Public Function ApplicationsByBase(ByVal iBaseNum As Integer) As List(Of ApplicationItems)
        'Dim appReturn As Application

        Dim Apps = (From app As ApplicationItems In _ApplicatioBysecurity
                    Where app.AppBase = iBaseNum
                    Select app).ToList

        ' appReturn = query

        Return Apps
    End Function
    Public Function AppBySecurity() As List(Of ApplicationItems)

        Dim Apps = (From app As ApplicationItems In _ApplicatioBysecurity
                    Where app.AppBase = app.ApplicationNum
                    Select app).ToList

        ' appReturn = query

        Return Apps
    End Function

    Public Class ApplicationItems
        Public Property ApplicationNum As String
        Public Property ApplicationDesc As String
        Public Property AppBase As String
        Public Property AppSystem As String
        Public Property DateDeactivated As DateTime
        Public Property AccountTypeCd As String

    End Class

End Class
