﻿Imports Microsoft.VisualBasic
Imports App_Code

Public Class FormSQL
  
    Private _DataConn As String
    Private _SQLSelect As String
    Private _SQLUpdate As String
    Private _SQLTable As String
    Private _AuditChanges As Boolean = False
    Private _AccountRequestNum As Integer
    Private _VaccineNum As Integer
    Private _Technum As Integer
    Private _ReturnedInteger As Integer
    Private _ValidationStatus As String
    Private _ParentControl As Control
    Private _dtTableValues As New DataTable
    Private _dtUpdatedValues As New DataTable
    Private _inputParams As New DataTable
    Private _insertData As SqlHelper
    Private _selectData As SqlHelper


    Public Sub New()


    End Sub




    Public Sub New(ByVal sDataConn As String, ByVal sSQLSelect As String, ByVal sSQLUpdate As String, ByVal sSQLTable As String, ByRef cParentTable As Control)

     
        _DataConn = sDataConn
        _SQLSelect = sSQLSelect
        _SQLUpdate = sSQLUpdate
        _SQLTable = sSQLTable
        _ParentControl = cParentTable

        _inputParams.Columns.Add("ParameterName", GetType(String))
        _inputParams.Columns.Add("ParameterType", GetType(String))
        _inputParams.Columns.Add("ParameterSize", GetType(String))

        _insertData = New SqlHelper(_SQLUpdate, _DataConn)
        _selectData = New SqlHelper(_SQLSelect, _DataConn)


        GetParameters()


    End Sub

    Protected Sub getTableValues()


        _dtTableValues = _selectData.GetSqlDataTable() ' GetTreeViewData(SqlPath)




    End Sub


    Public Sub FillForm(Optional ByVal accountRequestNum As Integer = 0)

        getTableValues()

        Dim dt As New DataTable


        dt = SqlHelper.FillFields(_dtTableValues, _ParentControl)


    End Sub
    Private Sub GetParameters()
        Dim myCommand As New SqlCommand
        Dim myConnection As New SqlConnection


        myConnection.ConnectionString = _DataConn


        myCommand.Connection = myConnection
        myCommand.CommandText = _SQLUpdate
        myCommand.CommandType = Data.CommandType.StoredProcedure

        myConnection.Open()

        SqlCommandBuilder.DeriveParameters(myCommand)

        myConnection.Close()

        For Each param As SqlParameter In myCommand.Parameters
            If param.Direction = Data.ParameterDirection.Input OrElse param.Direction = Data.ParameterDirection.InputOutput Then

                _inputParams.Rows.Add(param.ParameterName.ToString, param.SqlDbType.ToString(), param.Size.ToString)



            End If
        Next



    End Sub

    Public Sub UpdateForm(ByVal DateTimeSaved As DateTime)

        Dim thisData As New SqlHelper(_SQLUpdate, _DataConn)

        thisData.UpdateFields(_inputParams, _ParentControl)


    End Sub

    Public Sub InsertData()

        _insertData.UpdateFields(_inputParams, _ParentControl)

    End Sub
    Public Sub UpdateForm()
        _insertData.setTimeount(150)
        getTableValues()

        _insertData.UpdateFields(_inputParams, _dtTableValues, _ParentControl)
        ' _insertData.UpdateFields(_inputParams, _ParentControl)

    End Sub
    Public Sub AddInsertParm(ByVal sParam As String, ByVal sValue As String, ByRef sqlDatType As SqlDbType, ByRef iSize As Integer)

        _insertData.AddSqlProcParameter(sParam, sValue, sqlDatType, iSize)

    End Sub
    Public Sub AddSelectParm(ByVal sParam As String, ByVal sValue As String, ByRef sqlDatType As SqlDbType, ByRef iSize As Integer)

        _selectData.AddSqlProcParameter(sParam, sValue, sqlDatType, iSize)

    End Sub

    Public Sub UpdateFormReturnReqNum()

        _insertData.setTimeount(120)


        _AccountRequestNum = _insertData.UpdateFields(_inputParams, _ParentControl)



    End Sub
    Public Sub UpdateFormReturnVaccine()
        
        _insertData.setTimeount(120)


        _VaccineNum = _insertData.UpdateFields(_inputParams, _ParentControl)



    End Sub
    Public Sub UpdateFormReturnTechNum()

        _insertData.setTimeount(120)


        _Technum = _insertData.UpdateFields(_inputParams, _ParentControl)



    End Sub

    Public Sub UpdateFormReturnInt()

        _insertData.setTimeount(120)
        _ReturnedInteger = _insertData.UpdateFields(_inputParams, _ParentControl)


    End Sub

    Public Function AccountRequestNum() As String
        Return _AccountRequestNum

    End Function
    Public Function Technum() As String
        Return _Technum

    End Function
    Public Property ReturnedInteger As Integer

        Get
            Return _ReturnedInteger
        End Get

        Set(value As Integer)
        End Set


    End Property
    Public Property VaccineNum As Integer

        Get
            Return _VaccineNum
        End Get
        Set(value As Integer)

        End Set
    End Property



    Public Function getFieldValue(ByRef sFieldName As String) As String
        Dim sReturn As String = ""

        If _dtTableValues.Rows.Count = 0 Then

            getTableValues()

        End If


        Try
            If Not IsDBNull(_dtTableValues.Rows(0).Item(sFieldName)) Then

                sReturn = _dtTableValues.Rows(0).Item(sFieldName)

            Else

                sReturn = ""

            End If


        Catch ex As Exception

            Dim s As String = ex.ToString()


        End Try


        Return sReturn

    End Function



    Public Sub DisplayAuditFields(ByVal dtAudits As DataTable, ByRef ctlParent As WebControl)
        Dim drAudit As DataRow
        Dim bFoundControl As Boolean = False

        For Each drAudit In dtAudits.Rows



        Next




    End Sub

End Class
