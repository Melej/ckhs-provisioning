﻿Imports Microsoft.VisualBasic
Imports App_Code


Public Class RequestEntryController
    Private _RequestNum As Integer
    Private _dataConn As String


    Public Sub New()


        Dim conn As New SetDBConnection()

        _dataConn = conn.CSCConn
    End Sub


    Public Function RequestTicketByRequestNum(ByRef RequestNum As String) As RequestTicket

        Dim _RequestTicket As New RequestTicket



        Using sqlConn As New SqlConnection(_dataConn)

            Dim Cmd As New SqlCommand()
            Cmd.CommandText = "selTicketRFSByNumV7"
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.Parameters.AddWithValue("@Request_Num", RequestNum)
            Cmd.Connection = sqlConn



            sqlConn.Open()

            Using Cmd

                Using dr = Cmd.ExecuteReader()

                    If dr.HasRows Then

                        Do While dr.Read




                            _RequestTicket.requestnum = IIf(IsDBNull(dr("request_num")), Nothing, dr("request_num"))

                            '_RequestTicket.SubmitterClientNum = IIf(IsDBNull(dr("SubmitterClientNum")), Nothing, dr("SubmitterClientNum"))
                            _RequestTicket.clientnum = IIf(IsDBNull(dr("client_num")), Nothing, dr("client_num"))

                            _RequestTicket.entrydate = IIf(IsDBNull(dr("entry_date")), Nothing, dr("entry_date"))

                            _RequestTicket.closedate = IIf(IsDBNull(dr("close_date")), Nothing, dr("close_date"))
                            _RequestTicket.requesttype = IIf(IsDBNull(dr("request_type")), Nothing, dr("request_type"))

                            _RequestTicket.categorycd = IIf(IsDBNull(dr("category_cd")), Nothing, dr("category_cd"))
                            _RequestTicket.typecd = IIf(IsDBNull(dr("type_cd")), Nothing, dr("type_cd"))
                            _RequestTicket.itemcd = IIf(IsDBNull(dr("item_cd")), Nothing, dr("item_cd"))
                            _RequestTicket.priority = IIf(IsDBNull(dr("priority")), Nothing, dr("priority"))

                            _RequestTicket.groupassigntime = IIf(IsDBNull(dr("group_assign_time")), Nothing, dr("group_assign_time"))
                            _RequestTicket.assignedgroupnum = IIf(IsDBNull(dr("assigned_group_num")), Nothing, dr("assigned_group_num"))
                            _RequestTicket.currentgroupnum = IIf(IsDBNull(dr("current_group_num")), Nothing, dr("current_group_num"))
                            _RequestTicket.currentgroupname = IIf(IsDBNull(dr("current_group_name")), Nothing, dr("current_group_name"))

                            _RequestTicket.techassigntime = IIf(IsDBNull(dr("tech_assign_time")), Nothing, dr("tech_assign_time"))

                            _RequestTicket.assignedtechnum = IIf(IsDBNull(dr("assigned_tech_num")), Nothing, dr("assigned_tech_num"))
                            _RequestTicket.assignedtechname = IIf(IsDBNull(dr("assigned_tech_name")), Nothing, dr("assigned_tech_name"))

                            _RequestTicket.currenttechnum = IIf(IsDBNull(dr("current_tech_num")), Nothing, dr("current_tech_num"))
                            _RequestTicket.currenttechname = IIf(IsDBNull(dr("current_tech_name")), Nothing, dr("current_tech_num"))

                            _RequestTicket.acknowledgetime = IIf(IsDBNull(dr("acknowledge_time")), Nothing, dr("acknowledge_time"))
                            _RequestTicket.currentacktime = IIf(IsDBNull(dr("current_ack_time")), Nothing, dr("current_ack_time"))

                            _RequestTicket.assettag = IIf(IsDBNull(dr("asset_tag")), Nothing, dr("asset_tag"))
                            _RequestTicket.ipaddress = IIf(IsDBNull(dr("ip_address")), Nothing, dr("ip_address"))

                            _RequestTicket.platform = IIf(IsDBNull(dr("platform")), Nothing, dr("platform"))
                            _RequestTicket.resolutioncd = IIf(IsDBNull(dr("resolution_cd")), Nothing, dr("resolution_cd"))

                            _RequestTicket.entrytechnum = IIf(IsDBNull(dr("entry_tech_num")), Nothing, dr("entry_tech_num"))
                            _RequestTicket.entrytechname = IIf(IsDBNull(dr("entry_tech_name")), Nothing, dr("entry_tech_name"))

                            _RequestTicket.EMail = IIf(IsDBNull(dr("e_mail")), Nothing, dr("e_mail"))
                            _RequestTicket.Title = IIf(IsDBNull(dr("title")), Nothing, dr("title"))
                            _RequestTicket.Phone = IIf(IsDBNull(dr("phone")), Nothing, dr("phone"))

                            _RequestTicket.DepartmentCd = IIf(IsDBNull(dr("department_cd")), Nothing, dr("department_cd"))
                            _RequestTicket.DepartmentName = IIf(IsDBNull(dr("department_name")), Nothing, dr("department_name"))

                            _RequestTicket.FacilityCd = IIf(IsDBNull(dr("facility_cd")), Nothing, dr("facility_cd"))
                            _RequestTicket.FacilityName = IIf(IsDBNull(dr("facility_name")), Nothing, dr("facility_name"))

                            _RequestTicket.BuildingCd = IIf(IsDBNull(dr("building_cd")), Nothing, dr("building_cd"))
                            _RequestTicket.BuildingName = IIf(IsDBNull(dr("building_name")), Nothing, dr("building_name"))

                            _RequestTicket.Office = IIf(IsDBNull(dr("office")), Nothing, dr("office"))
                            _RequestTicket.Floor = IIf(IsDBNull(dr("floor")), Nothing, dr("floor"))

                            _RequestTicket.Address1 = IIf(IsDBNull(dr("address_1")), Nothing, dr("address_1"))
                            _RequestTicket.Address2 = IIf(IsDBNull(dr("address_2")), Nothing, dr("address_2"))
                            _RequestTicket.City = IIf(IsDBNull(dr("city")), Nothing, dr("city"))
                            _RequestTicket.State = IIf(IsDBNull(dr("state")), Nothing, dr("state"))
                            _RequestTicket.Zip = IIf(IsDBNull(dr("zip")), Nothing, dr("zip"))



                            _RequestTicket.alternatecontactname = IIf(IsDBNull(dr("alternate_contact_name")), Nothing, dr("alternate_contact_name"))
                            _RequestTicket.alternatecontactphone = IIf(IsDBNull(dr("alternate_contact_phone")), Nothing, dr("alternate_contact_phone"))
                            _RequestTicket.shortdesc = IIf(IsDBNull(dr("short_desc")), Nothing, dr("short_desc"))
                            _RequestTicket.proxyonpc = IIf(IsDBNull(dr("proxy_on_pc")), Nothing, dr("proxy_on_pc"))

                            'RFS Fields
                            _RequestTicket.completiondate = IIf(IsDBNull(dr("completion_date")), Nothing, dr("completion_date"))
                            _RequestTicket.servicedesc = IIf(IsDBNull(dr("service_desc")), Nothing, dr("service_desc"))
                            _RequestTicket.businessdes = IIf(IsDBNull(dr("business_des")), Nothing, dr("business_des"))
                            _RequestTicket.impactdesc = IIf(IsDBNull(dr("impact_desc")), Nothing, dr("impact_desc"))
                            _RequestTicket.numofterminals = IIf(IsDBNull(dr("num_of_terminals")), Nothing, dr("num_of_terminals"))
                            _RequestTicket.terminalids = IIf(IsDBNull(dr("terminal_ids")), Nothing, dr("terminal_ids"))
                            _RequestTicket.terminalinfodesc = IIf(IsDBNull(dr("terminal_info_desc")), Nothing, dr("terminal_info_desc"))
                            _RequestTicket.numofprinters = IIf(IsDBNull(dr("num_of_printers")), Nothing, dr("num_of_printers"))
                            _RequestTicket.printerids = IIf(IsDBNull(dr("printer_ids")), Nothing, dr("printer_ids"))
                            _RequestTicket.printerinfodesc = IIf(IsDBNull(dr("printer_info_desc")), Nothing, dr("printer_info_desc"))
                            _RequestTicket.numofpc = IIf(IsDBNull(dr("num_of_pc")), Nothing, dr("num_of_pc"))
                            _RequestTicket.pcids = IIf(IsDBNull(dr("pc_ids")), Nothing, dr("pc_ids"))
                            _RequestTicket.pcinfodesc = IIf(IsDBNull(dr("pc_info_desc")), Nothing, dr("pc_info_desc"))
                            _RequestTicket.numofnetdrops = IIf(IsDBNull(dr("num_of_net_drops")), Nothing, dr("num_of_net_drops"))
                            _RequestTicket.netdropids = IIf(IsDBNull(dr("net_drop_ids")), Nothing, dr("net_drop_ids"))
                            _RequestTicket.netdropsinfodesc = IIf(IsDBNull(dr("net_drops_info_desc")), Nothing, dr("net_drops_info_desc"))
                            _RequestTicket.numofother = IIf(IsDBNull(dr("num_of_other")), Nothing, dr("num_of_other"))

                            _RequestTicket.otherids = IIf(IsDBNull(dr("other_ids")), Nothing, dr("other_ids"))
                            _RequestTicket.otherinfodesc = IIf(IsDBNull(dr("other_info_desc")), Nothing, dr("other_info_desc"))


                        Loop

                    End If

                End Using

            End Using

            sqlConn.Close()

        End Using


        Return _RequestTicket


    End Function

End Class
