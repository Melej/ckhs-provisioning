﻿Imports Microsoft.VisualBasic
Imports App_Code
Imports System.ComponentModel
Imports System.Web.HttpContext

Public Class Client3Controller
	Private _LastName As String
	Private _Mi As String
	Private _LastUpdated As DateTime
	Private _LoginName As String
	Private _Phone As String
	Private _Fax As String
	Private _EMail As String
	Private _AlternatePhone As String
	Private _FacilityCd As String
	Private _BuildingCd As String
	Private _Office As String
	Private _Floor As String
	Private _Title As String
	Private _Suffix As String
	Private _Address1 As String
	Private _Address2 As String
	Private _City As String
	Private _State As String
	Private _Zip As String
	Private _EntityCd As String
	Private _EntityName As String
	Private _DepartmentCd As String

	Private _FacilityName As String
	Private _DateDeactivated As DateTime
	Private _Pager As String
	Private _PagerType As String
	Private _EmpTypeCd As String
	Private _StartDate As DateTime
	Private _EndDate As DateTime
	Private _SecurityTypeCd As String
	Private _SecurityDesc As String
	Private _GroupNum As String
	Private _GroupName As String
	Private _UserPositionDesc As String
	Private _DoctorMasterNum As Integer
	Private _ShareDrive As String
	Private _Npi As Integer
	Private _Taxonomy As String
	Private _Provider As String
	Private _Han As String
	Private _LicensesNo As String
	Private _CredentialedDate As DateTime
	Private _Specialty As String
	Private _CCMCadmitRights As String
	Private _DCMHadmitRights As String
	Private _SiemensEmpNum As String
	Private _NonHanLocation As String
	Private _VendorName As String
	Private _DateCreated As DateTime

    Private _AKALastName As String
	Private _AuthorizationEmpNum As String
	Private _Writeorders As String
	Private _CCMCConsults As String
	Private _DCMHConsults As String
	Private _TaylorConsults As String
	Private _SpringfieldConsults As String
	Private _MedicareID As String
	Private _BlueCrossID As String
	Private _SureScriptID As String
	Private _DEAID As String
	Private _DEAExtensionID As String
	Private _UniformPhysID As String
	Private _MedicGroupID As String
	Private _MedicGroupPhysID As String

	Private _LocationOfCareID As String
	Private _CopyTo As String
	Private _CommLocationOfCareID As String
	Private _CommCopyTo As String

	Private _InvisionGroupNum As String
	Private _DirectEMail As String
	Private _CoverageGroupnum As String
	Private _NonPerferedGroupnum As String
	Private _RoleNum As String
	Private _PositionNum As String
	Private _PositionRoleNum As String
    Private _CernerPositionDesc As String
    Private _VendorNum As String
	Private _ReferringClinician As String
	Private _CredentialedDateDCMH As String
	Private _CourtesyStaff As String
	Private _CourtesyStaffDCMH As String
	Private _WriteordersDCMH As String
	Private _UserTypeCd As String
	Private _TCL As String
	Private _UserTypeDesc As String

	Private _GNumberGroupNum As String
	Private _CommNumberGroupNum As String
	Private _DepartmentName As String

	Private _AccountRequestType As String


	Private _CPMModel As String
	Private _provmodel As String
	Private _paypathmodel As String
	Private _Schedulable As String
	Private _Televox As String
	Private _LastFour As String

	Private _dataConn As String
	Private _reactivate As String
	Private _client3 As New List(Of Client3)
	Public Sub New()

	End Sub
    Public Sub New(ByVal iClientNum As String, ByVal sReactivate As String, ByRef sConn As String)
        ClientNum = iClientNum
        _reactivate = sReactivate

        If sConn Is Nothing Then
            _dataConn = HttpContext.Current.Session("EmployeeConn")
        Else
            _dataConn = sConn
        End If

        GetClientInfo()

    End Sub
    Public Sub GetClientInfo()

		Using sqlConn As New SqlConnection(_dataConn)


			Dim Cmd As New SqlCommand()
            Cmd.CommandText = "SelClientByNumberV20"
            Cmd.CommandType = CommandType.StoredProcedure
			Cmd.Parameters.AddWithValue("@client_num", ClientNum)
			Cmd.Parameters.AddWithValue("@reactivated", _reactivate)

			Cmd.Connection = sqlConn



			sqlConn.Open()

			Using Cmd

				Using dr = Cmd.ExecuteReader()

					If dr.HasRows Then

						Do While dr.Read

							Try

								ClientNum = IIf(IsDBNull(dr("clientnum")), Nothing, dr("clientnum"))
								FirstName = IIf(IsDBNull(dr("firstname")), Nothing, dr("firstname"))
								FullName = IIf(IsDBNull(dr("full_name")), Nothing, dr("full_name"))
								_LastName = IIf(IsDBNull(dr("lastname")), Nothing, dr("lastname"))
								_Mi = IIf(IsDBNull(dr("mi")), Nothing, dr("mi"))
								_LastUpdated = IIf(IsDBNull(dr("LastUpdated")), Nothing, dr("LastUpdated"))
								_LoginName = IIf(IsDBNull(dr("loginname")), Nothing, dr("loginname"))
								_Phone = IIf(IsDBNull(dr("phone")), Nothing, dr("phone"))
								_Fax = IIf(IsDBNull(dr("fax")), Nothing, dr("fax"))
								_EMail = IIf(IsDBNull(dr("email")), Nothing, dr("email"))
								_AlternatePhone = IIf(IsDBNull(dr("AlternatePhone")), Nothing, dr("AlternatePhone"))
								_FacilityCd = IIf(IsDBNull(dr("facilitycd")), Nothing, dr("facilitycd"))
								_BuildingCd = IIf(IsDBNull(dr("buildingcd")), Nothing, dr("buildingcd"))
								_Office = IIf(IsDBNull(dr("office")), Nothing, dr("office"))
								_Floor = IIf(IsDBNull(dr("floor")), Nothing, dr("floor"))
								_Title = IIf(IsDBNull(dr("title")), Nothing, dr("title"))
								_Suffix = IIf(IsDBNull(dr("suffix")), Nothing, dr("suffix"))
								_Address1 = IIf(IsDBNull(dr("address1")), Nothing, dr("address1"))
								_Address2 = IIf(IsDBNull(dr("address2")), Nothing, dr("address2"))
								_City = IIf(IsDBNull(dr("city")), Nothing, dr("city"))
								_State = IIf(IsDBNull(dr("state")), Nothing, dr("state"))
								_Zip = IIf(IsDBNull(dr("zip")), Nothing, dr("zip"))
								_EntityCd = IIf(IsDBNull(dr("entitycd")), Nothing, dr("entitycd"))
								_EntityName = IIf(IsDBNull(dr("entity_name")), Nothing, dr("entity_name"))
								_DepartmentCd = IIf(IsDBNull(dr("departmentcd")), Nothing, dr("departmentcd"))
								_DepartmentName = IIf(IsDBNull(dr("department_name")), Nothing, dr("department_name"))

								_FacilityName = IIf(IsDBNull(dr("facilityname")), Nothing, dr("facilityname"))
								_DateDeactivated = IIf(IsDBNull(dr("datedeactivated")), Nothing, dr("datedeactivated"))
								_Pager = IIf(IsDBNull(dr("pager")), Nothing, dr("pager"))
								_PagerType = IIf(IsDBNull(dr("pagertype")), Nothing, dr("pagertype"))
								_EmpTypeCd = IIf(IsDBNull(dr("emptypecd")), Nothing, dr("emptypecd"))
								_StartDate = IIf(IsDBNull(dr("startdate")), Nothing, dr("startdate"))
								_EndDate = IIf(IsDBNull(dr("enddate")), Nothing, dr("enddate"))
								_SecurityTypeCd = IIf(IsDBNull(dr("securitytypecd")), Nothing, dr("securitytypecd"))
								_SecurityDesc = IIf(IsDBNull(dr("securitydesc")), Nothing, dr("securitydesc"))
								_GroupNum = IIf(IsDBNull(dr("groupnum")), Nothing, dr("groupnum"))
								_GroupName = IIf(IsDBNull(dr("groupname")), Nothing, dr("groupname"))
								_UserPositionDesc = IIf(IsDBNull(dr("userpositiondesc")), Nothing, dr("userpositiondesc"))
								_DoctorMasterNum = IIf(IsDBNull(dr("doctormasternum")), Nothing, dr("doctormasternum"))
								_ShareDrive = IIf(IsDBNull(dr("sharedrive")), Nothing, dr("sharedrive"))
								_Npi = IIf(IsDBNull(dr("npi")), Nothing, dr("npi"))
								_Taxonomy = IIf(IsDBNull(dr("taxonomy")), Nothing, dr("taxonomy"))
								' _Provider = IIf(IsDBNull(dr("provider")), Nothing, dr("provider"))
								_Han = IIf(IsDBNull(dr("han")), Nothing, dr("han"))
								_LicensesNo = IIf(IsDBNull(dr("LicensesNo")), Nothing, dr("LicensesNo"))
								_CredentialedDate = IIf(IsDBNull(dr("credentialedDate")), Nothing, dr("credentialedDate"))
								_Specialty = IIf(IsDBNull(dr("specialty")), Nothing, dr("specialty"))
								_CCMCadmitRights = IIf(IsDBNull(dr("CCMCadmitRights")), Nothing, dr("CCMCadmitRights"))
								_DCMHadmitRights = IIf(IsDBNull(dr("DCMHadmitRights")), Nothing, dr("DCMHadmitRights"))
								_SiemensEmpNum = IIf(IsDBNull(dr("siemensempnum")), Nothing, dr("siemensempnum"))
								' _NonHanLocation = IIf(IsDBNull(dr("NonHanLocation")), Nothing, dr("NonHanLocation"))
								_VendorName = IIf(IsDBNull(dr("VendorName")), Nothing, dr("VendorName"))
								_DateCreated = IIf(IsDBNull(dr("DateCreated")), Nothing, dr("DateCreated"))

                                _AKALastName = IIf(IsDBNull(dr("AKALastName")), Nothing, dr("AKALastName"))
								_AuthorizationEmpNum = IIf(IsDBNull(dr("AuthorizationEmpNum")), Nothing, dr("AuthorizationEmpNum"))
								_Writeorders = IIf(IsDBNull(dr("Writeorders")), Nothing, dr("Writeorders"))
								_CCMCConsults = IIf(IsDBNull(dr("CCMCConsults")), Nothing, dr("CCMCConsults"))
								_DCMHConsults = IIf(IsDBNull(dr("DCMHConsults")), Nothing, dr("DCMHConsults"))
								_TaylorConsults = IIf(IsDBNull(dr("TaylorConsults")), Nothing, dr("TaylorConsults"))
								_SpringfieldConsults = IIf(IsDBNull(dr("SpringfieldConsults")), Nothing, dr("SpringfieldConsults"))
								_MedicareID = IIf(IsDBNull(dr("MedicareID")), Nothing, dr("MedicareID"))
								_BlueCrossID = IIf(IsDBNull(dr("BlueCrossID")), Nothing, dr("BlueCrossID"))
								_SureScriptID = IIf(IsDBNull(dr("SureScriptID")), Nothing, dr("SureScriptID"))
								_DEAID = IIf(IsDBNull(dr("DEAID")), Nothing, dr("DEAID"))
								_DEAExtensionID = IIf(IsDBNull(dr("DEAExtensionID")), Nothing, dr("DEAExtensionID"))
								_UniformPhysID = IIf(IsDBNull(dr("UniformPhysID")), Nothing, dr("UniformPhysID"))
								_MedicGroupID = IIf(IsDBNull(dr("MedicGroupID")), Nothing, dr("MedicGroupID"))
								_MedicGroupPhysID = IIf(IsDBNull(dr("MedicGroupPhysID")), Nothing, dr("MedicGroupPhysID"))

								_LocationOfCareID = IIf(IsDBNull(dr("LocationOfCareID")), Nothing, dr("LocationOfCareID"))
								_CopyTo = IIf(IsDBNull(dr("CopyTo")), Nothing, dr("CopyTo"))
								_CommLocationOfCareID = IIf(IsDBNull(dr("CommLocationOfCareID")), Nothing, dr("CommLocationOfCareID"))
								_CommCopyTo = IIf(IsDBNull(dr("CommCopyTo")), Nothing, dr("CommCopyTo"))

								_AccountRequestType = IIf(IsDBNull(dr("AccountRequestType")), Nothing, dr("AccountRequestType"))
								_InvisionGroupNum = IIf(IsDBNull(dr("InvisionGroupNum")), Nothing, dr("InvisionGroupNum"))
								_DirectEMail = IIf(IsDBNull(dr("DirectEMail")), Nothing, dr("DirectEMail"))
								_CoverageGroupnum = IIf(IsDBNull(dr("CoverageGroupnum")), Nothing, dr("CoverageGroupnum"))
								_NonPerferedGroupnum = IIf(IsDBNull(dr("NonPerferedGroupnum")), Nothing, dr("NonPerferedGroupnum"))
								_RoleNum = IIf(IsDBNull(dr("RoleNum")), Nothing, dr("RoleNum"))
								_PositionNum = IIf(IsDBNull(dr("PositionNum")), Nothing, dr("PositionNum"))
								_PositionRoleNum = IIf(IsDBNull(dr("PositionRoleNum")), Nothing, dr("PositionRoleNum"))

                                _CernerPositionDesc = IIf(IsDBNull(dr("CernerPositionDesc")), Nothing, dr("CernerPositionDesc"))
                                _VendorNum = IIf(IsDBNull(dr("VendorNum")), Nothing, dr("VendorNum"))
								_ReferringClinician = IIf(IsDBNull(dr("ReferringClinician")), Nothing, dr("ReferringClinician"))
								_CredentialedDateDCMH = IIf(IsDBNull(dr("CredentialedDateDCMH")), Nothing, dr("CredentialedDateDCMH"))
								_CourtesyStaff = IIf(IsDBNull(dr("CourtesyStaff")), Nothing, dr("CourtesyStaff"))
								_CourtesyStaffDCMH = IIf(IsDBNull(dr("CourtesyStaffDCMH")), Nothing, dr("CourtesyStaffDCMH"))
								_WriteordersDCMH = IIf(IsDBNull(dr("WriteordersDCMH")), Nothing, dr("WriteordersDCMH"))
								_UserTypeCd = IIf(IsDBNull(dr("UserTypeCd")), Nothing, dr("UserTypeCd"))
								_TCL = IIf(IsDBNull(dr("TCL")), Nothing, dr("TCL"))
								_UserTypeDesc = IIf(IsDBNull(dr("UserTypeDesc")), Nothing, dr("UserTypeDesc"))
								_GNumberGroupNum = IIf(IsDBNull(dr("GNumberGroupNum")), Nothing, dr("GNumberGroupNum"))
								_CommNumberGroupNum = IIf(IsDBNull(dr("CommNumberGroupNum")), Nothing, dr("CommNumberGroupNum"))

								_CPMModel = IIf(IsDBNull(dr("CPMModel")), Nothing, dr("CPMModel"))
								_provmodel = IIf(IsDBNull(dr("provmodel")), Nothing, dr("provmodel"))
								_paypathmodel = IIf(IsDBNull(dr("paypathmodel")), Nothing, dr("paypathmodel"))
								_Schedulable = IIf(IsDBNull(dr("Schedulable")), Nothing, dr("Schedulable"))
								_Televox = IIf(IsDBNull(dr("Televox")), Nothing, dr("Televox"))
								_LastFour = IIf(IsDBNull(dr("lastfour")), Nothing, dr("lastfour"))
								PMHEmployeeID = IIf(IsDBNull(dr("PMHEmployeeID")), Nothing, dr("PMHEmployeeID"))
								PMHDepartment = IIf(IsDBNull(dr("PMHDepartment")), Nothing, dr("PMHDepartment"))
							Catch ex As Exception

							End Try

						Loop

					End If

				End Using

			End Using

			sqlConn.Close()

		End Using

	End Sub

	Public Function GetClientByClientNum(ByRef iClientNum As String) As Client3

		Dim _client3 As New Client3

		_dataConn = HttpContext.Current.Session("EmployeeConn")


		Using sqlConn As New SqlConnection(_dataConn)

			Dim Cmd As New SqlCommand()
            Cmd.CommandText = "SelClientByNumberV20"
            Cmd.CommandType = CommandType.StoredProcedure
			Cmd.Parameters.AddWithValue("@client_num", iClientNum)
			Cmd.Connection = sqlConn



			sqlConn.Open()

			Using Cmd

				Using dr = Cmd.ExecuteReader()

					If dr.HasRows Then

						Do While dr.Read

							Try

								_client3.ClientNum = IIf(IsDBNull(dr("client_num")), Nothing, dr("client_num"))
								_client3.FirstName = IIf(IsDBNull(dr("first_name")), Nothing, dr("first_name"))
								_client3.FullName = IIf(IsDBNull(dr("full_name")), Nothing, dr("full_name"))
								_client3.LastName = IIf(IsDBNull(dr("last_name")), Nothing, dr("last_name"))
								_client3.Mi = IIf(IsDBNull(dr("mi")), Nothing, dr("mi"))
								_client3.LastUpdated = IIf(IsDBNull(dr("LastUpdated")), Nothing, dr("LastUpdated"))
								_client3.LoginName = IIf(IsDBNull(dr("login_name")), Nothing, dr("login_name"))
								_client3.Phone = IIf(IsDBNull(dr("phone")), Nothing, dr("phone"))
								_client3.Fax = IIf(IsDBNull(dr("fax")), Nothing, dr("fax"))
								_client3.EMail = IIf(IsDBNull(dr("e_mail")), Nothing, dr("e_mail"))
								_client3.AlternatePhone = IIf(IsDBNull(dr("AlternatePhone")), Nothing, dr("AlternatePhone"))
								_client3.FacilityCd = IIf(IsDBNull(dr("facility_cd")), Nothing, dr("facility_cd"))
								_client3.BuildingCd = IIf(IsDBNull(dr("building_cd")), Nothing, dr("building_cd"))
								_client3.Office = IIf(IsDBNull(dr("office")), Nothing, dr("office"))
								_client3.Floor = IIf(IsDBNull(dr("floor")), Nothing, dr("floor"))
								_client3.Title = IIf(IsDBNull(dr("title")), Nothing, dr("title"))
								_client3.Suffix = IIf(IsDBNull(dr("suffix")), Nothing, dr("suffix"))
								_client3.Address1 = IIf(IsDBNull(dr("address_1")), Nothing, dr("address_1"))
								_client3.Address2 = IIf(IsDBNull(dr("address_2")), Nothing, dr("address_2"))
								_client3.City = IIf(IsDBNull(dr("city")), Nothing, dr("city"))
								_client3.State = IIf(IsDBNull(dr("state")), Nothing, dr("state"))
								_client3.Zip = IIf(IsDBNull(dr("zip")), Nothing, dr("zip"))
								_client3.EntityCd = IIf(IsDBNull(dr("entity_cd")), Nothing, dr("entity_cd"))
								_client3.EntityName = IIf(IsDBNull(dr("entity_name")), Nothing, dr("entity_name"))
								_client3.DepartmentCd = IIf(IsDBNull(dr("department_cd")), Nothing, dr("department_cd"))
								_client3.DepartmentName = IIf(IsDBNull(dr("department_name")), Nothing, dr("department_name"))

								_client3.FacilityName = IIf(IsDBNull(dr("facility_name")), Nothing, dr("facility_name"))
								_client3.DateDeactivated = IIf(IsDBNull(dr("date_deactivated")), Nothing, dr("date_deactivated"))
								_client3.Pager = IIf(IsDBNull(dr("pager")), Nothing, dr("pager"))
								_client3.PagerType = IIf(IsDBNull(dr("pager_type")), Nothing, dr("pager_type"))
								_client3.EmpTypeCd = IIf(IsDBNull(dr("emp_type_cd")), Nothing, dr("emp_type_cd"))
								_client3.StartDate = IIf(IsDBNull(dr("start_date")), Nothing, dr("start_date"))
								_client3.EndDate = IIf(IsDBNull(dr("end_date")), Nothing, dr("end_date"))
								_client3.SecurityTypeCd = IIf(IsDBNull(dr("security_type_cd")), Nothing, dr("security_type_cd"))
								_client3.SecurityDesc = IIf(IsDBNull(dr("security_desc")), Nothing, dr("security_desc"))
								_client3.GroupNum = IIf(IsDBNull(dr("group_num")), Nothing, dr("group_num"))
								_client3.GroupName = IIf(IsDBNull(dr("group_name")), Nothing, dr("group_name"))
								_client3.UserPositionDesc = IIf(IsDBNull(dr("user_position_desc")), Nothing, dr("user_position_desc"))
								_client3.DoctorMasterNum = IIf(IsDBNull(dr("doctor_master_num")), Nothing, dr("doctor_master_num"))
								_client3.ShareDrive = IIf(IsDBNull(dr("share_drive")), Nothing, dr("share_drive"))
								_client3.Npi = IIf(IsDBNull(dr("npi")), Nothing, dr("npi"))
								_client3.Taxonomy = IIf(IsDBNull(dr("taxonomy")), Nothing, dr("taxonomy"))
								'_client3.Provider = IIf(IsDBNull(dr("provider")), Nothing, dr("provider"))
								_client3.Han = IIf(IsDBNull(dr("han")), Nothing, dr("han"))
								_client3.LicensesNo = IIf(IsDBNull(dr("LicensesNo")), Nothing, dr("LicensesNo"))
								_client3.CredentialedDate = IIf(IsDBNull(dr("credentialedDate")), Nothing, dr("credentialedDate"))
								_client3.Specialty = IIf(IsDBNull(dr("specialty")), Nothing, dr("specialty"))
								_client3.CCMCadmitRights = IIf(IsDBNull(dr("CCMCadmitRights")), Nothing, dr("CCMCadmitRights"))
								_client3.DCMHadmitRights = IIf(IsDBNull(dr("DCMHadmitRights")), Nothing, dr("DCMHadmitRights"))
								_client3.SiemensEmpNum = IIf(IsDBNull(dr("siemens_emp_num")), Nothing, dr("siemens_emp_num"))
								'_client3.NonHanLocation = IIf(IsDBNull(dr("NonHanLocation")), Nothing, dr("NonHanLocation"))
								_client3.VendorName = IIf(IsDBNull(dr("VendorName")), Nothing, dr("VendorName"))
								_client3.DateCreated = IIf(IsDBNull(dr("DateCreated")), Nothing, dr("DateCreated"))

                                _client3.AKALastName = IIf(IsDBNull(dr("AKALastName")), Nothing, dr("AKALastName"))
								_client3.AuthorizationEmpNum = IIf(IsDBNull(dr("AuthorizationEmpNum")), Nothing, dr("AuthorizationEmpNum"))
								_client3.GNumberGroupNum = IIf(IsDBNull(dr("GNumberGroupNum")), Nothing, dr("GNumberGroupNum"))
								_client3.CommNumberGroupNum = IIf(IsDBNull(dr("CommNumberGroupNum")), Nothing, dr("CommNumberGroupNum"))
								_client3.LastFour = IIf(IsDBNull(dr("lastfour")), Nothing, dr("lastfour"))
                                _client3.CernerPositionDesc = IIf(IsDBNull(dr("CernerPositionDesc")), Nothing, dr("CernerPositionDesc"))
                            Catch ex As Exception

							End Try

						Loop

					End If

				End Using

			End Using

			sqlConn.Close()

		End Using


		Return _client3


	End Function
	Public Sub CheckEmployeeType(ByRef EmptypeCd As String)

		Dim crt As New Control
		crt.ID = "Titleddl"
		If EmptypeCd.ToLower = "allied health" Then
			Dim ctrl As Control = DataCheckingAndValidation.SetChildControlsRecursiveToEnabledFalse(crt, False)

		End If

	End Sub

	Public Function GetClientsFromTable(ByRef dtClientNums As DataTable) As List(Of Client3)
		Dim _Return As New List(Of Client3)

		For Each dr In dtClientNums.Rows
			Dim _client3 As New Client3

			Try

				_client3.ClientNum = IIf(IsDBNull(dr("client_num")), Nothing, dr("client_num"))
				_client3.FirstName = IIf(IsDBNull(dr("first_name")), Nothing, dr("first_name"))
				_client3.FullName = IIf(IsDBNull(dr("full_name")), Nothing, dr("full_name"))
				_client3.LastName = IIf(IsDBNull(dr("last_name")), Nothing, dr("last_name"))
				_client3.Mi = IIf(IsDBNull(dr("mi")), Nothing, dr("mi"))
				_client3.LastUpdated = IIf(IsDBNull(dr("LastUpdated")), Nothing, dr("LastUpdated"))
				_client3.LoginName = IIf(IsDBNull(dr("login_name")), Nothing, dr("login_name"))
				_client3.Phone = IIf(IsDBNull(dr("phone")), Nothing, dr("phone"))
				_client3.Fax = IIf(IsDBNull(dr("fax")), Nothing, dr("fax"))
				_client3.EMail = IIf(IsDBNull(dr("e_mail")), Nothing, dr("e_mail"))
				_client3.AlternatePhone = IIf(IsDBNull(dr("AlternatePhone")), Nothing, dr("AlternatePhone"))
				_client3.FacilityCd = IIf(IsDBNull(dr("facility_cd")), Nothing, dr("facility_cd"))
				_client3.BuildingCd = IIf(IsDBNull(dr("building_cd")), Nothing, dr("building_cd"))
				_client3.Office = IIf(IsDBNull(dr("office")), Nothing, dr("office"))
				_client3.Floor = IIf(IsDBNull(dr("floor")), Nothing, dr("floor"))
				_client3.Title = IIf(IsDBNull(dr("title")), Nothing, dr("title"))
				_client3.Suffix = IIf(IsDBNull(dr("suffix")), Nothing, dr("suffix"))
				_client3.Address1 = IIf(IsDBNull(dr("address_1")), Nothing, dr("address_1"))
				_client3.Address2 = IIf(IsDBNull(dr("address_2")), Nothing, dr("address_2"))
				_client3.City = IIf(IsDBNull(dr("city")), Nothing, dr("city"))
				_client3.State = IIf(IsDBNull(dr("state")), Nothing, dr("state"))
				_client3.Zip = IIf(IsDBNull(dr("zip")), Nothing, dr("zip"))
				_client3.EntityCd = IIf(IsDBNull(dr("entity_cd")), Nothing, dr("entity_cd"))
				_client3.EntityName = IIf(IsDBNull(dr("entity_name")), Nothing, dr("entity_name"))
				_client3.DepartmentCd = IIf(IsDBNull(dr("department_cd")), Nothing, dr("department_cd"))
				_client3.DepartmentName = IIf(IsDBNull(dr("department_name")), Nothing, dr("department_name"))
				_client3.FacilityName = IIf(IsDBNull(dr("facility_name")), Nothing, dr("facility_name"))
				_client3.DateDeactivated = IIf(IsDBNull(dr("date_deactivated")), Nothing, dr("date_deactivated"))
				_client3.Pager = IIf(IsDBNull(dr("pager")), Nothing, dr("pager"))
				_client3.PagerType = IIf(IsDBNull(dr("pager_type")), Nothing, dr("pager_type"))
				_client3.EmpTypeCd = IIf(IsDBNull(dr("emp_type_cd")), Nothing, dr("emp_type_cd"))
				_client3.StartDate = IIf(IsDBNull(dr("start_date")), Nothing, dr("start_date"))
				_client3.EndDate = IIf(IsDBNull(dr("end_date")), Nothing, dr("end_date"))
				_client3.SecurityTypeCd = IIf(IsDBNull(dr("security_type_cd")), Nothing, dr("security_type_cd"))
				_client3.SecurityDesc = IIf(IsDBNull(dr("security_desc")), Nothing, dr("security_desc"))
				_client3.GroupNum = IIf(IsDBNull(dr("group_num")), Nothing, dr("group_num"))
				_client3.GroupName = IIf(IsDBNull(dr("group_name")), Nothing, dr("group_name"))
				_client3.UserPositionDesc = IIf(IsDBNull(dr("user_position_desc")), Nothing, dr("user_position_desc"))
				_client3.DoctorMasterNum = IIf(IsDBNull(dr("doctor_master_num")), Nothing, dr("doctor_master_num"))
				_client3.ShareDrive = IIf(IsDBNull(dr("share_drive")), Nothing, dr("share_drive"))
				_client3.Npi = IIf(IsDBNull(dr("npi")), Nothing, dr("npi"))
				_client3.Taxonomy = IIf(IsDBNull(dr("taxonomy")), Nothing, dr("taxonomy"))
				'_client3.Provider = IIf(IsDBNull(dr("provider")), Nothing, dr("provider"))
				_client3.Han = IIf(IsDBNull(dr("han")), Nothing, dr("han"))
				_client3.LicensesNo = IIf(IsDBNull(dr("LicensesNo")), Nothing, dr("LicensesNo"))
				_client3.CredentialedDate = IIf(IsDBNull(dr("credentialedDate")), Nothing, dr("credentialedDate"))
				_client3.Specialty = IIf(IsDBNull(dr("specialty")), Nothing, dr("specialty"))
				_client3.CCMCadmitRights = IIf(IsDBNull(dr("CCMCadmitRights")), Nothing, dr("CCMCadmitRights"))
				_client3.DCMHadmitRights = IIf(IsDBNull(dr("DCMHadmitRights")), Nothing, dr("DCMHadmitRights"))
				_client3.SiemensEmpNum = IIf(IsDBNull(dr("siemens_emp_num")), Nothing, dr("siemens_emp_num"))
				'_client3.NonHanLocation = IIf(IsDBNull(dr("NonHanLocation")), Nothing, dr("NonHanLocation"))
				_client3.VendorName = IIf(IsDBNull(dr("VendorName")), Nothing, dr("VendorName"))
				_client3.DateCreated = IIf(IsDBNull(dr("DateCreated")), Nothing, dr("DateCreated"))

                _client3.AKALastName = IIf(IsDBNull(dr("AKALastName")), Nothing, dr("AKALastName"))
				_client3.AuthorizationEmpNum = IIf(IsDBNull(dr("AuthorizationEmpNum")), Nothing, dr("AuthorizationEmpNum"))
				_client3.GNumberGroupNum = IIf(IsDBNull(dr("GNumberGroupNum")), Nothing, dr("GNumberGroupNum"))
				_client3.CommNumberGroupNum = IIf(IsDBNull(dr("CommNumberGroupNum")), Nothing, dr("CommNumberGroupNum"))
				_client3.LastFour = IIf(IsDBNull(dr("lastfour")), Nothing, dr("lastfour"))


			Catch ex As Exception

			End Try


			_Return.Add(_client3)

		Next



		Return _Return
	End Function
	<DisplayName("Client Num#")> Property ClientNum As Integer

	<DisplayName("First Name")> Property FirstName As String

	<DisplayName("Full Name")> Property FullName As String

	Property PMHDepartment As String

	Property PMHEmployeeID As String

	<DisplayName("Last Name")> Property LastName As String
		Get
			Return _LastName
		End Get
		Set(value As String)
			_LastName = value
		End Set
	End Property
	<DisplayName("MI")> Property MI As String
		Get
			Return _Mi
		End Get
		Set(value As String)
			_Mi = value
		End Set
	End Property
	<DisplayName("Last Updated")> Property LastUpdated As DateTime
		Get
			Return _LastUpdated
		End Get
		Set(value As DateTime)
			_LastUpdated = value
		End Set
	End Property

	<DisplayName("Login Name")> Property LoginName As String
		Get
			Return _LoginName
		End Get
		Set(value As String)
			_LoginName = value
		End Set
	End Property
	<DisplayName("Phone")> Property Phone As String
		Get
			Return _Phone
		End Get
		Set(value As String)
			_Phone = value
		End Set
	End Property

	<DisplayName("Fax")> Property Fax As String
		Get
			Return _Fax
		End Get
		Set(value As String)
			_Fax = value
		End Set
	End Property
	<DisplayName("EMail")> Property EMail As String
		Get
			Return _EMail
		End Get
		Set(value As String)
			_EMail = value
		End Set
	End Property
	<DisplayName("Alternate Phone")> Property AlternatePhone As String
		Get
			Return _AlternatePhone
		End Get
		Set(value As String)
			_AlternatePhone = value
		End Set
	End Property
	<DisplayName("Facility Code")> Property FacilityCd As String
		Get
			Return _FacilityCd
		End Get
		Set(value As String)
			_FacilityCd = value
		End Set
	End Property

	<DisplayName("Building Code")> Property BuildingCd As String
		Get
			Return _BuildingCd
		End Get
		Set(value As String)
			_BuildingCd = value
		End Set
	End Property

	<DisplayName("Office")> Property Office As String
		Get
			Return _Office
		End Get
		Set(value As String)
			_Office = value
		End Set
	End Property
	<DisplayName("Floor")> Property Floor As String
		Get
			Return _Floor
		End Get
		Set(value As String)
			_Floor = value
		End Set
	End Property
	<DisplayName("Title")> Property Title As String
		Get
			Return _Title
		End Get
		Set(value As String)
			_Title = value
		End Set
	End Property
	<DisplayName("Suffix")> Property Suffix As String
		Get
			Return _Suffix
		End Get
		Set(value As String)
			_Suffix = value
		End Set
	End Property

	<DisplayName("Address Line 1")> Property Address1 As String
		Get
			Return _Address1
		End Get
		Set(value As String)
			_Address1 = value
		End Set
	End Property
	<DisplayName("Address Line 2")> Property Address2 As String
		Get
			Return _Address2
		End Get
		Set(value As String)
			_Address2 = value
		End Set
	End Property
	<DisplayName("City")> Property City As String
		Get
			Return _City
		End Get
		Set(value As String)
			_City = value
		End Set
	End Property
	<DisplayName("State")> Property State As String
		Get
			Return _State
		End Get
		Set(value As String)
			_State = value
		End Set
	End Property
	<DisplayName("Zip")> Property Zip As String
		Get
			Return _Zip
		End Get
		Set(value As String)
			_Zip = value
		End Set
	End Property

	<DisplayName("Entity Code")> Property EntityCd As String
		Get
			Return _EntityCd
		End Get
		Set(value As String)
			_EntityCd = value
		End Set
	End Property
	<DisplayName("Entity Name")> Property EntityName As String
		Get
			Return _EntityName
		End Get
		Set(value As String)
			_EntityName = value
		End Set
	End Property
	<DisplayName("Department Code")> Property DepartmentCd As String
		Get
			Return _DepartmentCd
		End Get
		Set(value As String)
			_DepartmentCd = value
		End Set
	End Property
	<DisplayName("Department Name")> Property DepartmentName As String
		Get
			Return _DepartmentName
		End Get
		Set(value As String)
			_DepartmentName = value
		End Set

	End Property

	<DisplayName("Department Name")> Property Department_Name As String


	<DisplayName("Facility Name")> Property FacilityName As String
        Get
            Return _FacilityName
        End Get
        Set(value As String)
            _FacilityName = value
        End Set
    End Property

    <DisplayName("Date Deactivated")> Property DateDeactivated As DateTime
        Get
            Return _DateDeactivated
        End Get
        Set(value As DateTime)
            _DateDeactivated = value
        End Set
    End Property

    <DisplayName("Pager")> Property Pager As String
        Get
            Return _Pager
        End Get
        Set(value As String)
            _Pager = value
        End Set
    End Property

    <DisplayName("Pager Type")> Property PagerType As String
        Get
            Return _PagerType
        End Get
        Set(value As String)
            _PagerType = value
        End Set
    End Property

    <DisplayName("Employee Type")> Property EmpTypeCd As String
        Get
            Return _EmpTypeCd
        End Get
        Set(value As String)
            _EmpTypeCd = value
        End Set
    End Property

    <DisplayName("Start Date")> Property StartDate As DateTime
        Get
            Return _StartDate
        End Get
        Set(value As DateTime)
            _StartDate = value
        End Set
    End Property

    <DisplayName("End Date")> Property EndDate As DateTime
        Get
            Return _EndDate
        End Get
        Set(value As DateTime)
            _EndDate = value
        End Set
    End Property

    <DisplayName("Security Type Cd")> Property SecurityTypeCd As String
        Get
            Return _SecurityTypeCd
        End Get
        Set(value As String)
            _SecurityTypeCd = value
        End Set
    End Property
    <DisplayName("Security Type Desc.")> Property SecurityDesc As String
        Get
            Return _SecurityDesc
        End Get
        Set(value As String)
            _SecurityDesc = value
        End Set
    End Property

    <DisplayName("Group Num")> Property GroupNum As String
        Get
            Return _GroupNum
        End Get
        Set(value As String)
            _GroupNum = value
        End Set
    End Property

    <DisplayName("Group Name")> Property GroupName As String
        Get
            Return _GroupName
        End Get
        Set(value As String)
            _GroupName = value
        End Set
    End Property

    <DisplayName("User Position Desc")> Property UserPositionDesc As String
        Get
            Return _UserPositionDesc
        End Get
        Set(value As String)
            _UserPositionDesc = value
        End Set
    End Property
    <DisplayName("Doctor Master Num")> Property DoctorMasterNum As Integer
        Get
            Return _DoctorMasterNum
        End Get
        Set(value As Integer)
            _DoctorMasterNum = value
        End Set
    End Property
    <DisplayName("Share Drive")> Property ShareDrive As String
        Get
            Return _ShareDrive
        End Get
        Set(value As String)
            _ShareDrive = value
        End Set
    End Property
    <DisplayName("NPI")> Property Npi As Integer
        Get
            Return _Npi
        End Get
        Set(value As Integer)
            _Npi = value
        End Set
    End Property
    <DisplayName("Taxonomy")> Property Taxonomy As String
        Get
            Return _Taxonomy
        End Get
        Set(value As String)
            _Taxonomy = value
        End Set
    End Property
    <DisplayName("Provider")> Property Provider As String
        Get
            Return _Provider
        End Get
        Set(value As String)
            _Provider = value
        End Set
    End Property
    <DisplayName("Han")> Property Han As String
        Get
            Return _Han
        End Get
        Set(value As String)
            _Han = value
        End Set
    End Property
    <DisplayName("LicensesNo")> Property LicensesNo As String
        Get
            Return _LicensesNo
        End Get
        Set(value As String)
            _LicensesNo = value
        End Set
    End Property
    <DisplayName("Credentialed Date")> Property CredentialedDate As DateTime
        Get
            Return _CredentialedDate
        End Get
        Set(value As DateTime)
            _CredentialedDate = value
        End Set
    End Property
    <DisplayName("Specialty")> Property Specialty As String
        Get
            Return _Specialty
        End Get
        Set(value As String)
            _Specialty = value
        End Set
    End Property
    <DisplayName("CCMC Admit Rights")> Property CCMCadmitRights As String
        Get
            Return _CCMCadmitRights
        End Get
        Set(value As String)
            _CCMCadmitRights = value
        End Set
    End Property
    <DisplayName("DCMH Admit Rights")> Property DCMHadmitRights As String
        Get
            Return _DCMHadmitRights
        End Get
        Set(value As String)
            _DCMHadmitRights = value
        End Set
    End Property
    <DisplayName("Employee #")> Property SiemensEmpNum As String
        Get
            Return _SiemensEmpNum
        End Get
        Set(value As String)
            _SiemensEmpNum = value
        End Set
    End Property

    <DisplayName("Non Han Location")> Property NonHanLocation As String
        Get
            Return _NonHanLocation
        End Get
        Set(value As String)
            _NonHanLocation = value
        End Set
    End Property
    <DisplayName("Vendor Name")> Property VendorName As String
        Get
            Return _VendorName
        End Get
        Set(value As String)
            _VendorName = value
        End Set
    End Property
    <DisplayName("Date Created")> Property DateCreated As DateTime
        Get
            Return _DateCreated
        End Get
        Set(value As DateTime)
            _DateCreated = value
        End Set
    End Property


    <DisplayName("AKA Last Name")> Property AKALastName As String
        Get
            Return _AKALastName
        End Get
        Set(value As String)
            _AKALastName = value
        End Set
    End Property
    <DisplayName("Authorization Emp Num")> Property AuthorizationEmpNum As String
        Get
            Return _AuthorizationEmpNum
        End Get
        Set(value As String)
            _AuthorizationEmpNum = value
        End Set
    End Property
    <DisplayName("Write orders")> Property Writeorders As String
        Get
            Return _Writeorders
        End Get
        Set(value As String)
            _Writeorders = value
        End Set
    End Property
    <DisplayName("CCMC Consults")> Property CCMCConsults As String
        Get
            Return _CCMCConsults
        End Get
        Set(value As String)
            _CCMCConsults = value
        End Set
    End Property
    <DisplayName("DCMH Consults")> Property DCMHConsults As String
        Get
            Return _DCMHConsults
        End Get
        Set(value As String)
            _DCMHConsults = value
        End Set
    End Property
    <DisplayName("Taylor Consults")> Property TaylorConsults As String
        Get
            Return _TaylorConsults
        End Get
        Set(value As String)
            _TaylorConsults = value
        End Set
    End Property
    <DisplayName("Springfield Consults")> Property SpringfieldConsults As String
        Get
            Return _SpringfieldConsults
        End Get
        Set(value As String)
            _SpringfieldConsults = value
        End Set
    End Property
    <DisplayName("Medicare ID")> Property MedicareID As String
        Get
            Return _MedicareID
        End Get
        Set(value As String)
            _MedicareID = value
        End Set
    End Property
    <DisplayName("BlueCross ID")> Property BlueCrossID As String
        Get
            Return _BlueCrossID
        End Get
        Set(value As String)
            _BlueCrossID = value
        End Set
    End Property
    <DisplayName("SureScript ID")> Property SureScriptID As String
        Get
            Return _SureScriptID
        End Get
        Set(value As String)
            _SureScriptID = value
        End Set
    End Property
    <DisplayName("DEA ID")> Property DEAID As String
        Get
            Return _DEAID
        End Get
        Set(value As String)
            _DEAID = value
        End Set
    End Property
    <DisplayName("DEA Extension ID")> Property DEAExtensionID As String
        Get
            Return _DEAExtensionID
        End Get
        Set(value As String)
            _DEAExtensionID = value
        End Set
    End Property
    <DisplayName("Uniform Phys ID")> Property UniformPhysID As String
        Get
            Return _UniformPhysID
        End Get
        Set(value As String)
            _UniformPhysID = value
        End Set
    End Property
    <DisplayName("Medic Group ID")> Property MedicGroupID As String
        Get
            Return _MedicGroupID
        End Get
        Set(value As String)
            _MedicGroupID = value
        End Set
    End Property
    <DisplayName("Medic Group Phys ID")> Property MedicGroupPhysID As String
        Get
            Return _MedicGroupPhysID
        End Get
        Set(value As String)
            _MedicGroupPhysID = value
        End Set
    End Property
    <DisplayName("LocationOfCare ID")> Property LocationOfCareID As String
        Get
            Return _LocationOfCareID
        End Get
        Set(value As String)
            _LocationOfCareID = value
        End Set
    End Property
    Property CopyTo As String
        Get
            Return _CopyTo
        End Get
        Set(value As String)
            _CopyTo = value
        End Set
    End Property
    Property CommLocationOfCareID As String
        Get
            Return _CommLocationOfCareID
        End Get
        Set(value As String)
            _CommLocationOfCareID = value
        End Set
    End Property


	Property AccountRequestType As String
		Get
			Return _AccountRequestType
		End Get
		Set(value As String)
			_AccountRequestType = value
		End Set
	End Property

	Property CommCopyTo As String
        Get
            Return _CommCopyTo
        End Get
        Set(value As String)
            _CommCopyTo = value
        End Set
    End Property
    Property InvisionGroupNum As String
        Get
            Return _InvisionGroupNum
        End Get
        Set(value As String)
            _InvisionGroupNum = value
        End Set
    End Property


    Property DirectEMail As String
        Get
            Return _DirectEMail
        End Get
        Set(value As String)
            _DirectEMail = value
        End Set
    End Property

    Property CoverageGroupnum As String
        Get
            Return _CoverageGroupnum
        End Get
        Set(value As String)
            _CoverageGroupnum = value
        End Set
    End Property

    Property NonPerferedGroupnum As String
        Get
            Return _NonPerferedGroupnum
        End Get
        Set(value As String)
            _NonPerferedGroupnum = value
        End Set
    End Property

    Property RoleNum As String
        Get
            Return _RoleNum
        End Get
        Set(value As String)
            _RoleNum = value
        End Set
    End Property

    Property PositionNum As String
        Get
            Return _PositionNum
        End Get
        Set(value As String)
            _PositionNum = value
        End Set
    End Property

    Property PositionRoleNum As String
        Get
            Return _PositionRoleNum
        End Get
        Set(value As String)
            _PositionRoleNum = value
        End Set
    End Property

    Property CernerPositionDesc As String
        Get
            Return _CernerPositionDesc
        End Get
        Set(value As String)
            _CernerPositionDesc = value
        End Set
    End Property

    Property VendorNum As String
        Get
            Return _VendorNum
        End Get
        Set(value As String)
            _VendorNum = value
        End Set
    End Property

    Property ReferringClinician As String
        Get
            Return _ReferringClinician
        End Get
        Set(value As String)
            _ReferringClinician = value
        End Set
    End Property

    Property CredentialedDateDCMH As String
        Get
            Return _CredentialedDateDCMH
        End Get
        Set(value As String)
            _CredentialedDateDCMH = value
        End Set
    End Property

    Property CourtesyStaff As String
        Get
            Return _CourtesyStaff
        End Get
        Set(value As String)
            _CourtesyStaff = value
        End Set
    End Property

    Property CourtesyStaffDCMH As String
        Get
            Return _CourtesyStaffDCMH
        End Get
        Set(value As String)
            _CourtesyStaffDCMH = value
        End Set
    End Property

    Property WriteordersDCMH As String
        Get
            Return _WriteordersDCMH
        End Get
        Set(value As String)
            _WriteordersDCMH = value
        End Set
    End Property

    Property UserTypeCd As String
        Get
            Return _UserTypeCd
        End Get
        Set(value As String)
            _UserTypeCd = value
        End Set
    End Property

    Property TCL As String
        Get
            Return _TCL
        End Get
        Set(value As String)
            _TCL = value
        End Set
    End Property

    Property UserTypeDesc As String
        Get
            Return _UserTypeDesc
        End Get
        Set(value As String)
            _UserTypeDesc = value
        End Set
    End Property

    Property GNumberGroupNum As String
        Get
            Return _GNumberGroupNum
        End Get
        Set(value As String)
            _GNumberGroupNum = value
        End Set
    End Property
    Property CommNumberGroupNum As String
        Get
            Return _CommNumberGroupNum
        End Get
        Set(value As String)
            _CommNumberGroupNum = value
        End Set
    End Property
    Public ReadOnly Property CPMModel As String
        Get
            Return (_CPMModel)
        End Get
    End Property
    Public ReadOnly Property provmodel As String
        Get
            Return (_provmodel)
        End Get
    End Property
    Public ReadOnly Property paypathmodel As String
        Get
            Return (_paypathmodel)
        End Get
    End Property
    Public ReadOnly Property Schedulable As String
        Get
            Return (_Schedulable)
        End Get
    End Property
    Public ReadOnly Property Televox As String
        Get
            Return (_Televox)
        End Get
    End Property
    Property LastFour As String
        Get
            Return (_LastFour)

        End Get
        Set(value As String)
            _LastFour = value
        End Set
    End Property

End Class
