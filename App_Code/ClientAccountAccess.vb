﻿Imports Microsoft.VisualBasic

Public Class ClientAccountAccess
    Public Property ClientNum As Integer
    Public Property ApplicationNum As Integer
    Public Property LoginName As String
    Public Property CreatedByClientNum As Integer
    Public Property CreateDate As Date
    Public Property AcctTermDate As Date
    Public Property AcctTermClientNum As Integer
    Public Property ApplicationDesc As String
    Public Property CreatedBy As String
    Public Property AppBase As String

End Class
