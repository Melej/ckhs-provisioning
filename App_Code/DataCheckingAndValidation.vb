Imports Microsoft.VisualBasic
Imports System.Collections

Namespace App_Code
    Public Class DataCheckingAndValidation
        Implements SessionState.IRequiresSessionState
        Shared sErr As String = ""
        Shared arButtonExcludeText As ArrayList = New ArrayList
        Public Shared Function GetSplitStringAsArrayList(ByVal sParams As String, ByVal sChar As String) As ArrayList
            Dim ssplit() As String = Split(sParams, sChar)
            Dim ar As New ArrayList
            ar.AddRange(ssplit)
            'If ssplit.Length < 2 Then 'no ; try space
            '    ssplit = Split(dr("ConsultSocialWorker"), " ")
            'End If
            'For i As Integer = 0 To ssplit.GetUpperBound(0)
            '    lstPatientConsultSocialWorker.Items.Add(ssplit(i))
            '    ConsultSocialWorkerTextBox.Text = dr("ConsultSocialWorker")
            'Next
            Return ar
        End Function
        Public Shared Function EscapeQuote(ByVal strIn As String, ByVal cEscapeChar As String) As String
            Dim s() As String = strIn.Split(cEscapeChar)
            Dim sReturn As String = strIn
            If s.Length > 2 Then 'alreasy has more than one char
            Else
                sReturn = sReturn.Replace(cEscapeChar, cEscapeChar & cEscapeChar)
            End If
            Return sReturn
        End Function
        Public Shared Function EscapeQuoteV2(ByVal strIn As String, ByVal arCharsToEscape As ArrayList) As String
            'Dim s() As String = strIn.Split(cEscapeChar)
            Dim c() As Char = strIn.ToCharArray
            Dim sReturn As String = "" 'Right(strIn, 1) ' ""
            Dim iEscape As Integer
            Dim strToProcess As String = strIn
            Dim iChar As Integer = 0
            Dim bAdded As Boolean
            For iChar = 0 To c.Length - 1
                For iEscape = 0 To arCharsToEscape.Count - 1 'loop thru chars to be escaped
                    If c(iChar) = arCharsToEscape(iEscape) Then 'char to be escaped(doubled)

                        If iChar < c.Length - 1 And c(iChar) = c(iChar + 1) Then 'don't check(ichar+1) if last loop d already doubled
                            '    sReturn &= c(iChar) 
                            sReturn &= c(iChar)
                            bAdded = True
                        ElseIf iChar > 1 And c(iChar) = c(iChar - 1) Then 'don't check(ichar-1) if fisrt loop already doubledThen
                            sReturn &= c(iChar)
                            bAdded = True

                        Else
                            sReturn &= c(iChar) & c(iChar)
                            bAdded = True
                        End If
                    End If


                Next
                If bAdded = False Then
                    sReturn &= c(iChar)
                End If
                bAdded = False
                'strToProcess = sReturn & strToProcess.Substring(strToProcess.Length - 1, 1) 'keep changes from current escape for next escape in outer loop
            Next

            Return sReturn
        End Function
        Public Shared Function RemoveChar(ByVal strIn As String, ByVal cEscapeChar As String) As String
            Dim s() As String = strIn.Split(cEscapeChar)
            Dim sReturn As String = strIn
            For i As Integer = 0 To s.Length - 1
                sReturn = ""
                If s(i) <> "" Then
                    sReturn &= s(i).ToString()
                End If
            Next

            Return sReturn
        End Function
        Public Shared Function CheckForIsValidQueryString(ByVal ObjIn As Object) As Boolean
            ' Dim bHasValue As Boolean
            If IsDBNull(ObjIn) Then
                Return False
            ElseIf ObjIn = Nothing Then
                Return False
            ElseIf ObjIn.ToString = String.Empty Then
                Return False
            ElseIf String.IsNullOrEmpty(ObjIn.ToString) Then

                Return False
            ElseIf ObjIn = "" Then
                Return False
            Else
                Return True
            End If
        End Function
        Public Shared Function IsValidSessionField(ByVal sField As String) As Boolean
            Dim bReturn As Boolean
            If String.IsNullOrEmpty(sField) Then

            ElseIf sField <> "" And sField.ToLower <> "&nbsp;" Then
                bReturn = True
            End If
            Return bReturn
        End Function
        Public Shared Function IsValidAndExistsSessionField(ByVal sSessionName As String) As Boolean
            Dim bReturn As Boolean
            Dim sFieldValue As String = ""
            '  If String.IsNullOrEmpty(sField) Then
            If IsNothing(HttpContext.Current.Session(sSessionName)) Then

            ElseIf HttpContext.Current.Session(sSessionName).ToString() <> "" And HttpContext.Current.Session(sSessionName).ToString.ToLower() <> "&nbsp;" Then
                bReturn = True
            End If
            Return bReturn
        End Function
        Public Shared Function IsValidStringField(ByVal sField As String) As Boolean
            Dim bReturn As Boolean
            If String.IsNullOrEmpty(sField) Then

            ElseIf sField <> "" And sField.ToLower <> "&nbsp;" Then
                bReturn = True
            End If
            Return bReturn
        End Function
        Public Shared Function CheckBirthDate(ByVal sDateIn As String) As String
            Dim mydate As DateTime
            Dim smissingValue As String = ""
            Try
                If Not IsDate(sDateIn) Then
                    smissingValue &= " Birthday format must be mm/dd/yyyy" ' 'below would check to see if a trans was already scheduled between to to and from location 
                    Return smissingValue
                Else 'valid date
                    mydate = sDateIn
                    sDateIn = mydate.ToString("MM/dd/yyyy")
                    '        sDateIn = mydate.ToString("MM/dd/yyyy")
                End If

                If DateDiff(DateInterval.Year, mydate, Date.Today) > 120 Then smissingValue &= "Patient must be less than 120 years old"
                If DateDiff(DateInterval.Day, Date.Today, mydate) > 0 Then smissingValue &= " Birthday must be in the Past"
                ' End If
                Return smissingValue
            Catch ex As Exception
                Err.Clear()
                Return " Birthday format must be mm/dd/yyyy"

            Finally
            End Try
        End Function
        Public Shared Function CheckBirthDate(ByRef sDateIn As TextBox) As String
            Dim mydate As DateTime
            Dim smissingValue As String = ""
            Try
                If Not IsDate(sDateIn.Text) Then
                    smissingValue &= " Birthday format must be mm/dd/yyyy" ' 'below would check to see if a trans was already scheduled between to to and from location 
                    Return smissingValue
                Else 'valid date
                    mydate = sDateIn.Text
                    sDateIn.Text = mydate.ToString("MM/dd/yyyy")
                    '        sDateIn = mydate.ToString("MM/dd/yyyy")
                End If


                If DateDiff(DateInterval.Year, mydate, Date.Today) > 120 Then smissingValue &= "Patient must be less than 120 years old"
                If DateDiff(DateInterval.Day, Date.Today, mydate) > 0 Then smissingValue &= " Birthday must be in the Past"
                ' End If
                Return smissingValue
            Catch ex As Exception
                Err.Clear()
                Return " Birthday format must be mm/dd/yyyy"
            Finally

            End Try

        End Function
        Public Shared Function CheckBirthDate(ByRef sDateIn As TextBox, ByVal bFormat As Boolean) As String
            Dim mydate As DateTime
            Dim smissingValue As String = ""
            Try
                If Not IsDate(sDateIn.Text) Then
                    smissingValue &= " Birthday format must be mm/dd/yyyy" ' 'below would check to see if a trans was already scheduled between to to and from location 
                    Return smissingValue
                Else 'valid date
                    If bFormat Then
                        mydate = sDateIn.Text
                        sDateIn.Text = mydate.ToString("MM/dd/yyyy")
                    End If
                    '        sDateIn = mydate.ToString("MM/dd/yyyy")
                End If


                If DateDiff(DateInterval.Year, mydate, Date.Today) > 120 Then smissingValue &= "Patient must be less than 120 years old"
                If DateDiff(DateInterval.Day, Date.Today, mydate) > 0 Then smissingValue &= " Birthday must be in the Past"
                ' End If
                Return smissingValue
            Catch ex As Exception
                Err.Clear()
                Return " Birthday format must be mm/dd/yyyy"
            Finally

            End Try

        End Function
        Public Shared Function CheckDate(ByRef sDateIn As TextBox, ByVal bFormat As Boolean) As String
            Dim mydate As DateTime
            Dim smissingValue As String = ""
            Try
                If Not IsDate(sDateIn.Text) Then
                    smissingValue &= " Date format must be mm/dd/yyyy" ' 'below would check to see if a trans was already scheduled between to to and from location 
                    Return smissingValue
                Else 'valid date
                    If bFormat Then
                        mydate = sDateIn.Text
                        sDateIn.Text = mydate.ToString("MM/dd/yyyy")
                    End If
                    '        sDateIn = mydate.ToString("MM/dd/yyyy")
                End If



                Return smissingValue
            Catch ex As Exception
                Err.Clear()
                Return " Date format must be mm/dd/yyyy"
            Finally

            End Try

        End Function
        Public Shared Function setMaxLength(ByVal stringin As String, ByVal iMaxLength As Integer) As String
            If stringin.Length > iMaxLength Then
                Return stringin.Substring(0, iMaxLength)
            Else
                Return stringin
            End If
        End Function
        Public Shared Function RemoveNonCharacters(ByVal OldStr As String, ByVal values As Char()) As String
            Dim i As Integer = 0 'values.Length
            'Dim c As Char
            'For Each c In values
            '    i = sin.IndexOfAny(values)
            '    sin.Remove(i)
            '    'sin.Replace(c, "")
            'Next
            Dim NewStr As String = ""
            Dim l As Long
            For l = 1 To Len(OldStr)
                If Asc(Mid(OldStr, l, 1)) > 31 Then
                    NewStr = NewStr & Mid(OldStr, l, 1)
                Else
                    NewStr &= " "
                End If
                '  if asc
            Next
            RemoveNonCharacters = NewStr
            'Return sin
        End Function
        Public Shared Function RemoveNonCharacters(ByVal OldStr As String, ByVal values As Char(), ByVal bRemoveAllUnder32 As Boolean, ByVal bRemoveCRLF As Boolean) As String
            Try
                Dim i As Integer = 0 'values.Length


                Dim NewStr As String
                Dim l As Long
                If bRemoveAllUnder32 Then

                    For l = 1 To 31
                        If Not i = 27 Then ' is a semi leave in";"
                            OldStr = OldStr.Replace(Chr(l), "")
                        Else
                            Dim test As String = ";"
                        End If
                        'If Asc(Mid(OldStr, l, 1)) > 31 Then
                        '    NewStr = NewStr & Mid(OldStr, l, 1)
                        'Else
                        '    NewStr &= " "
                        'End If
                        ''  if asc
                    Next
                End If
                For i = 0 To values.Length - 1
                    OldStr = OldStr.Replace(values(i), "")
                Next
                NewStr = OldStr

                If bRemoveCRLF Then
                    Dim crlf() As String = {vbCrLf}
                    Dim line As String() = NewStr.Split(crlf, StringSplitOptions.None)
                    NewStr = ""
                    For k As Integer = 0 To line.Length - 1
                        NewStr &= line(k) & " " ' could use a special char to remember where enter key was hit
                    Next
                End If
                RemoveNonCharacters = NewStr.Trim()
                'Return sin
            Catch ex As Exception
                sErr = ex.Message
                Throw ex
                'notify someonw
            End Try
        End Function
        Public Shared Function RemoveNonCharacters(ByVal OldStr As String, ByVal values As Char(), ByVal bRemoveAllUnder32 As Boolean) As String
            Try
                Dim i As Integer = 0 'values.Length

                Dim NewStr As String
                Dim l As Long
                If bRemoveAllUnder32 Then


                    For l = 1 To 31 '45 '
                        Dim aChar As Char = Chr(l)
                        ' Dim b As String = Asc(l)
                        OldStr = OldStr.Replace(aChar, String.Empty)
                        'OldStr = OldStr.Replace(Asc(l), "")
                        'If Asc(Mid(OldStr, l, 1)) > 31 Then
                        '    NewStr = NewStr & Mid(OldStr, l, 1)

                        'Else
                        '    NewStr &= " "
                        'End If
                        ''  if asc
                    Next
                End If
                For i = 0 To values.Length - 1
                    OldStr = OldStr.Replace(values(i), "")
                Next
                NewStr = OldStr
                'For l = 1 To Len(OldStr)
                '    If Asc(Mid(OldStr, l, 1)) > 31 Then
                '        NewStr = NewStr & Mid(OldStr, l, 1)
                '    Else
                '        NewStr &= " "
                '    End If
                '    '  if asc
                'Next
                'Dim crlf() As String = {vbCrLf}
                'Dim line As String() = NewStr.Split(crlf, StringSplitOptions.None)
                'NewStr = ""
                'For k As Integer = 0 To line.Length - 1
                '    NewStr &= line(k)
                'Next
                RemoveNonCharacters = NewStr
            Catch ex As Exception
                sErr = ex.Message
                Throw ex
                'notify someonw
            End Try
            'Return sin
        End Function
        Public Shared Function RemoveNonCharactersPrepForjavaScript(ByVal OldStr As String) As String
            Dim i As Integer = 0 'values.Length
            'For Each c In values
            '    i = sin.IndexOfAny(values)
            '    sin.Remove(i)
            '    'sin.Replace(c, "")
            'Next
            Dim NewStr As String = ""
            Dim l As Long
            For l = 1 To Len(OldStr)
                If Asc(Mid(OldStr, l, 1)) > 31 Then
                    NewStr = NewStr & Mid(OldStr, l, 1)

                Else
                    NewStr &= " "
                End If
                '  if asc
            Next
            'If Not i = 27 Then ' is a semi leave in";"
            '    OldStr = OldStr.Replace(Chr(l), "")
            'Else
            '    Dim test As String = ";"
            'End If
            NewStr = NewStr.Replace("\", "\\")
            NewStr = NewStr.Replace("'", "\'")

            Return NewStr
            'Return sin
        End Function

        Public Shared Function FindWebControlRecursive(ByVal ctrlParent As Control, ByVal controlIDtoFind As String) As Control
            Try 'newborn fillclinicalinfo
                'If IsNothing(ctrlParent) Then
                '    Return Nothing
                'End If
                If ctrlParent.GetType.ToString.ToLower.IndexOf("literal") > -1 Then
                    '   Return Nothing
                ElseIf ctrlParent.GetType.ToString.ToLower.IndexOf("grid") > -1 Then
                    Return Nothing
                    'ElseIf ctrlParent.GetType.ToString.ToLower.IndexOf("view") > -1 Then
                    '    Dim s As String = ctrlParent.GetType.ToString.ToLower
                    '    Return Nothing
                End If
                'If ctrlParent.ID.ToLower.IndexOf("grid") > -1 Then
                '    Return Nothing
                'End If
                If String.Compare(ctrlParent.ID, controlIDtoFind, True) = 0 Then
                    ' We found the control!
                    Return ctrlParent
                Else
                    ' Recurse through ctrl's Controls collections
                    For Each child As Control In ctrlParent.Controls
                        Dim lookFor As Control = FindWebControlRecursive(child, controlIDtoFind)

                        If lookFor IsNot Nothing Then
                            Return lookFor     ' We found the control
                        End If
                    Next

                    ' If we reach here, control was not found
                    Return Nothing
                End If
            Catch ex As Exception
                sErr = ctrlParent.ID
                sErr &= ex.Message
                Return Nothing
                ' Throw ex

            End Try
        End Function
        Public Shared Function FindWebControlRecursiveByRef(ByRef ctrlParent As Control, ByVal controlIDtoFind As String) As Control
            Try
                If String.Compare(ctrlParent.ID, controlIDtoFind, True) = 0 Then
                    ' We found the control!
                    Return ctrlParent
                Else
                    ' Recurse through ctrl's Controls collections
                    For Each child As Control In ctrlParent.Controls
                        Dim lookFor As Control = FindWebControlRecursive(child, controlIDtoFind)

                        If lookFor IsNot Nothing Then
                            Return lookFor     ' We found the control
                        End If
                    Next

                    ' If we reach here, control was not found
                    Return Nothing
                End If
            Catch ex As Exception
                sErr = ex.Message
                Throw ex
                'notify someonw
            End Try
        End Function
        Public Shared Function FindControlRecursiveAndClear(ByRef ctrl As Control) As Control
            Dim sID As String = ""
            Dim lookFor As Control
            Try
                If ctrl.NamingContainer.ToString.ToLower.IndexOf("gridview") > -1 Then
                    Return Nothing
                    Exit Function
                End If
                For Each child As Control In ctrl.Controls
                    If child.HasControls Then 'has childern
                        lookFor = New Control
                        lookFor = FindControlRecursiveAndClear(child)
                    Else 'no  childern
                        lookFor = New Control
                        lookFor = child
                        If lookFor IsNot Nothing Then
                            If lookFor.GetType.ToString.ToLower.IndexOf("webcontrols") > -1 Then
                                '  sID = lookFor.ID.ToLower.ToString()
                                sID = lookFor.GetType.ToString.ToLower
                                'If lookFor.ID.ToLower.ToString.IndexOf("ddl") > -1 Then
                                If sID.IndexOf("ddl") > -1 Or sID.IndexOf("dropdownlist") > -1 Then
                                    Dim ddl As New DropDownList
                                    ddl = lookFor
                                    If ddl.Items.Count > 0 Then
                                        ddl.SelectedIndex = 0
                                    End If
                                    ddl = Nothing
                                End If
                                If lookFor.ID.ToLower.IndexOf("rbl") > -1 Or sID.IndexOf("radiobuttonlist") > -1 Then
                                    Dim rbl As New RadioButtonList
                                    rbl = lookFor
                                    If rbl.Items.Count > 2 Then 'three options n/a yes no
                                        rbl.SelectedIndex = 0
                                    Else
                                        rbl.SelectedIndex = -1
                                    End If
                                    rbl = Nothing
                                End If
                                ' listbox
                                If lookFor.ID.ToLower.IndexOf("Listbox") > -1 Or sID.IndexOf("listbox") > -1 Then
                                    Dim listbox As New RadioButtonList
                                    listbox = lookFor
                                    If listbox.Items.Count > 2 Then 'three options n/a yes no
                                        listbox.SelectedIndex = 0
                                    Else
                                        listbox.SelectedIndex = -1
                                    End If
                                    listbox = Nothing
                                End If

                                ' If lookFor.ID.ToLower.IndexOf("textbox") > -1 Or sID.IndexOf("textbox") > -1 Then
                                If sID.IndexOf("textbox") > -1 Then
                                    Dim txt As New TextBox
                                    txt = lookFor
                                    txt.Text = ""
                                    txt = Nothing
                                End If

                            End If
                        End If

                    End If


                Next

                ' If we reach here, control was not found
                Return lookFor
            Catch ex As Exception
                sErr = ex.Message
                Throw ex
                'notify someonw
            End Try
            'End If
        End Function
        Public Shared Function SetChildControlsRecursiveToEnabledFalse(ByRef ctrl As Control, ByVal bEnabled As Boolean) As Control
            Dim sID As String = ""
            Dim lookFor As Control
            Dim sControl As String = ""
            arButtonExcludeText.Clear()
            arButtonExcludeText.Add("pdf") '
            arButtonExcludeText.Add("print")
            arButtonExcludeText.Add("report")
            arButtonExcludeText.Add("word")
            arButtonExcludeText.Add("excel")
            arButtonExcludeText.Add("exit")
            arButtonExcludeText.Add("rpt")
            ' sControl = ctrl.ID.ToString & " top " & ctrl.GetType.ToString.ToLower
            Try

                'If ctrl.GetType.ToString.ToLower = "system.web.ui.webcontrols.gridview" Then
                '    Dim btn As New GridView
                '    btn = ctrl
                '    btn.Enabled = bEnabled
                '    btn = Nothing
                'End If
                For Each child As Control In ctrl.Controls
                    '    sControl = child.ID.ToString & " Child for" & child.GetType.ToString.ToLower
                    If child.HasControls Then 'has childern
                        If child.GetType.ToString.ToLower = "system.web.ui.webcontrols.gridview" Then
                            Dim btn As New GridView
                            btn = child
                            btn.Enabled = bEnabled
                            btn = Nothing
                        End If
                        lookFor = New Control
                        ' 6/17/09lookFor = FindControlRecursiveAndClear(child)
                        lookFor = SetChildControlsRecursiveToEnabledFalse(child, bEnabled)
                    Else 'no  childern
                        'sControl = child.ID.ToString & " No Child for" & child.GetType.ToString.ToLower
                        lookFor = child
                        '=======================TODO .ID new at 20100216
                        If lookFor.ID IsNot Nothing Then '===================================================
                            If lookFor.GetType.ToString.ToLower.IndexOf("webcontrols") > -1 Then
                                '  sID = lookFor.ID.ToLower.ToString()
                                sID = lookFor.GetType.ToString.ToLower
                                'If lookFor.ID.ToLower.ToString.IndexOf("ddl") > -1 Then
                                If sID.IndexOf("ddl") > -1 Or sID.IndexOf("dropdownlist") > -1 Then
                                    Dim ddl As New DropDownList
                                    ddl = lookFor
                                    'If ddl.Items.Count > 0 Then 'ReviewReasonsDDL
                                    ddl.Enabled = bEnabled
                                    'End If
                                    ddl = Nothing
                                    'ElseIf lookFor.ID Is Nothing Then

                                    '    sErr = lookFor.ID.ToString()
                                ElseIf lookFor.ID.ToLower.IndexOf("rbl") > -1 Or sID.IndexOf("radiobuttonlist") > -1 Then
                                    '  sErr = lookFor.ID.ToString()
                                    Dim rbl As New RadioButtonList
                                    rbl = lookFor
                                    If rbl.Items.Count > 0 Then
                                        rbl.Enabled = bEnabled
                                    End If
                                    rbl = Nothing

                                ElseIf lookFor.ID.ToLower.IndexOf("textbox") > -1 Or sID.IndexOf("textbox") > -1 Then
                                    If lookFor.ID.ToLower.IndexOf("extender") > -1 Or sID.IndexOf("extender") > -1 Then
                                        sErr = lookFor.ID.ToString()
                                    Else
                                        Dim txt As New TextBox
                                        txt = lookFor

                                        txt.Enabled = bEnabled
                                        txt = Nothing
                                    End If
                                ElseIf lookFor.ID.ToLower.IndexOf("cbl") > -1 Or sID.IndexOf("checkbox") > -1 Then
                                    Dim txt As New CheckBox
                                    txt = lookFor
                                    txt.Enabled = bEnabled
                                    txt = Nothing
                                ElseIf sID.ToLower = "system.web.ui.webcontrols.checkboxlist" Then ' lookFor.ID.ToLower.IndexOf("cbl") > -1 Or sID.IndexOf("checkbox") > -1 Then
                                    Dim txt As New CheckBoxList
                                    txt = lookFor
                                    txt.Enabled = bEnabled
                                    txt = Nothing


                                ElseIf sID.ToLower = "system.web.ui.webcontrols.button" Then 'lookFor.ID.ToString.ToLower.IndexOf("button") > -1 Or sID.ToLower.IndexOf("button") > -1 Then
                                    Dim btn As New Button
                                    Dim bExclude As Boolean


                                    btn = lookFor
                                    If Not btn Is Nothing Then
                                        For ia As Integer = 0 To arButtonExcludeText.Count - 1

                                            If btn.Text.ToLower.IndexOf(arButtonExcludeText(ia)) > -1 Then
                                                bExclude = True
                                            End If
                                        Next
                                        If Not bExclude Then ' not a print or report type ok to disable
                                            btn.Enabled = bEnabled
                                        End If

                                    End If

                                    bExclude = False
                                    btn = Nothing

                                    'Dim btn As New Button
                                    'btn = lookFor
                                    'btn.Enabled = bEnabled
                                    'btn = Nothing
                                ElseIf sID.ToLower = LCase("System.Web.UI.HtmlControls.HtmlButton") Then
                                    Dim btn As New System.Web.UI.HtmlControls.HtmlButton
                                    Dim bExclude As Boolean


                                    btn = lookFor
                                    If Not btn Is Nothing Then
                                        For ia As Integer = 0 To arButtonExcludeText.Count - 1

                                            If btn.InnerText.ToLower.IndexOf(arButtonExcludeText(ia)) > -1 Then
                                                bExclude = True
                                            End If
                                        Next
                                        If Not bExclude Then ' not a print or report type ok to disable

                                            btn.Disabled = Not bEnabled
                                        End If

                                    End If

                                    bExclude = False
                                    btn = Nothing

                                    'old
                                    'If btn.InnerText.ToLower.IndexOf("print") <> -1 Then ' don't disable print button
                                    '    If btn.InnerText.ToLower.IndexOf("pdf") <> -1 Then ' don't disable print button
                                    '        btn = lookFor
                                    '        btn.Disabled = bEnabled

                                    '    End If

                                    'End If

                                    'btn = Nothing


                                ElseIf sID.ToLower = "system.web.ui.webcontrols.gridview" Then
                                    Dim btn As New GridView
                                    btn = lookFor
                                    btn.Enabled = bEnabled
                                    btn = Nothing


                                ElseIf sID.ToLower = "system.web.ui.webcontrols.listbox" Then
                                    Dim btn As New ListBox
                                    btn = lookFor
                                    btn.Enabled = bEnabled
                                    btn = Nothing

                                End If
                            End If
                        End If

                    End If


                Next
                Return lookFor
            Catch ex As Exception
                sErr = ex.Message
                Throw ex
                'notify someonw
            End Try
            ' If we reach here, control was not found

            'End If
        End Function
        Public Shared Function SetChildControlsRecursiveEnabledProperty(ByRef ctrl As Control, ByVal bEnabled As Boolean) As Control
            Dim sID As String = ""
            Dim lookFor As Control
            Dim sControl As String = ""

            ' sControl = ctrl.ID.ToString & " top " & ctrl.GetType.ToString.ToLower
            Try

                'If ctrl.GetType.ToString.ToLower = "system.web.ui.webcontrols.gridview" Then
                '    Dim btn As New GridView
                '    btn = ctrl
                '    btn.Enabled = bEnabled
                '    btn = Nothing
                'End If
                For Each child As Control In ctrl.Controls
                    '    sControl = child.ID.ToString & " Child for" & child.GetType.ToString.ToLower
                    If child.HasControls Then 'has childern
                        If child.GetType.ToString.ToLower = "system.web.ui.webcontrols.gridview" Then
                            Dim btn As New GridView
                            btn = child
                            btn.Enabled = bEnabled
                            btn = Nothing
                        End If
                        lookFor = New Control
                        ' 6/17/09lookFor = FindControlRecursiveAndClear(child)
                        lookFor = SetChildControlsRecursiveEnabledProperty(child, bEnabled)
                    Else 'no  childern
                        'sControl = child.ID.ToString & " No Child for" & child.GetType.ToString.ToLower
                        lookFor = child
                        If lookFor IsNot Nothing Then
                            If lookFor.GetType.ToString.ToLower.IndexOf("webcontrols") > -1 Then
                                '  sID = lookFor.ID.ToLower.ToString()
                                sID = lookFor.GetType.ToString.ToLower
                                'If lookFor.ID.ToLower.ToString.IndexOf("ddl") > -1 Then
                                If sID.IndexOf("ddl") > -1 Or sID.IndexOf("dropdownlist") > -1 Then
                                    Dim ddl As New DropDownList
                                    ddl = lookFor
                                    'If ddl.Items.Count > 0 Then 'ReviewReasonsDDL
                                    ddl.Enabled = bEnabled
                                    'End If
                                    ddl = Nothing

                                ElseIf lookFor.ID.ToLower.IndexOf("rbl") > -1 Or sID.IndexOf("radiobuttonlist") > -1 Then
                                    Dim rbl As New RadioButtonList
                                    rbl = lookFor
                                    If rbl.Items.Count > 0 Then
                                        rbl.Enabled = bEnabled
                                    End If
                                    rbl = Nothing

                                ElseIf lookFor.ID.ToLower.IndexOf("textbox") > -1 Or sID.IndexOf("textbox") > -1 Then
                                    Dim txt As New TextBox
                                    txt = lookFor
                                    txt.Enabled = bEnabled
                                    txt = Nothing

                                ElseIf lookFor.ID.ToLower.IndexOf("cbl") > -1 Or sID.IndexOf("checkbox") > -1 Then
                                    Dim txt As New CheckBox
                                    txt = lookFor
                                    txt.Enabled = bEnabled
                                    txt = Nothing
                                ElseIf sID.ToLower = "system.web.ui.webcontrols.checkboxlist" Then ' lookFor.ID.ToLower.IndexOf("cbl") > -1 Or sID.IndexOf("checkbox") > -1 Then
                                    Dim txt As New CheckBoxList
                                    txt = lookFor
                                    txt.Enabled = bEnabled
                                    txt = Nothing


                                ElseIf sID.ToLower = "system.web.ui.webcontrols.button" Then 'lookFor.ID.ToString.ToLower.IndexOf("button") > -1 Or sID.ToLower.IndexOf("button") > -1 Then
                                    Dim btn As New Button
                                    Dim bExclude As Boolean
                                    arButtonExcludeText.Clear()
                                    arButtonExcludeText.Add("pdf") '
                                    arButtonExcludeText.Add("print")
                                    arButtonExcludeText.Add("report")
                                    arButtonExcludeText.Add("word")
                                    arButtonExcludeText.Add("excel")

                                    btn = lookFor
                                    If Not btn Is Nothing Then
                                        For ia As Integer = 0 To arButtonExcludeText.Count - 1

                                            If btn.Text.ToLower.IndexOf(arButtonExcludeText(ia)) > 1 Then
                                                bExclude = True
                                            End If
                                        Next
                                        If Not bExclude Then ' not a print or report type ok to disable
                                            btn.Enabled = bEnabled
                                        End If

                                    End If


                                    btn = Nothing


                                ElseIf sID.ToLower = "system.web.ui.webcontrols.gridview" Then
                                    Dim btn As New GridView
                                    btn = lookFor
                                    btn.Enabled = bEnabled
                                    btn = Nothing


                                ElseIf sID.ToLower = "system.web.ui.webcontrols.listbox" Then
                                    Dim btn As New ListBox
                                    btn = lookFor
                                    btn.Enabled = bEnabled
                                    btn = Nothing

                                End If
                            End If
                        End If

                    End If


                Next
                Return (lookFor)
            Catch ex As Exception
                sErr = ex.Message
                Throw ex
            End Try
            ' If we reach here, control was not found

            'End If
        End Function
        Public Shared Function SetPageChildControlsRecursiveToEnabledFalse(ByRef ctrl As Page, ByVal bEnabled As Boolean) As Control
            Dim sID As String = ""
            Dim lookFor As Control
            Dim sControl As String = ""

            ' sControl = ctrl.ID.ToString & " top " & ctrl.GetType.ToString.ToLower
            Try

                'If ctrl.GetType.ToString.ToLower = "system.web.ui.webcontrols.gridview" Then
                '    Dim btn As New GridView
                '    btn = ctrl
                '    btn.Enabled = bEnabled
                '    btn = Nothing
                'End If
                For Each child As Control In ctrl.Controls
                    '    sControl = child.ID.ToString & " Child for" & child.GetType.ToString.ToLower
                    If child.HasControls Then 'has childern
                        If child.GetType.ToString.ToLower = "system.web.ui.webcontrols.gridview" Then
                            Dim btn As New GridView
                            btn = child
                            btn.Enabled = bEnabled
                            btn = Nothing
                        End If
                        lookFor = New Control
                        ' 6/17/09lookFor = FindControlRecursiveAndClear(child)
                        lookFor = SetChildControlsRecursiveToEnabledFalse(child, bEnabled)
                    Else 'no  childern
                        'sControl = child.ID.ToString & " No Child for" & child.GetType.ToString.ToLower
                        lookFor = child
                        If lookFor IsNot Nothing Then
                            If lookFor.GetType.ToString.ToLower.IndexOf("webcontrols") > -1 Then
                                '  sID = lookFor.ID.ToLower.ToString()
                                sID = lookFor.GetType.ToString.ToLower
                                'If lookFor.ID.ToLower.ToString.IndexOf("ddl") > -1 Then
                                If sID.IndexOf("ddl") > -1 Or sID.IndexOf("dropdownlist") > -1 Then
                                    Dim ddl As New DropDownList
                                    ddl = lookFor
                                    'If ddl.Items.Count > 0 Then 'ReviewReasonsDDL
                                    ddl.Enabled = bEnabled
                                    'End If
                                    ddl = Nothing

                                ElseIf lookFor.ID.ToLower.IndexOf("rbl") > -1 Or sID.IndexOf("radiobuttonlist") > -1 Then
                                    Dim rbl As New RadioButtonList
                                    rbl = lookFor
                                    If rbl.Items.Count > 0 Then
                                        rbl.Enabled = bEnabled
                                    End If
                                    rbl = Nothing

                                ElseIf lookFor.ID.ToLower.IndexOf("textbox") > -1 Or sID.IndexOf("textbox") > -1 Then
                                    Dim txt As New TextBox
                                    txt = lookFor
                                    txt.Enabled = bEnabled
                                    txt = Nothing

                                ElseIf lookFor.ID.ToLower.IndexOf("cbl") > -1 Or sID.IndexOf("checkbox") > -1 Then
                                    Dim txt As New CheckBox
                                    txt = lookFor
                                    txt.Enabled = bEnabled
                                    txt = Nothing
                                ElseIf sID.ToLower = "system.web.ui.webcontrols.checkboxlist" Then ' lookFor.ID.ToLower.IndexOf("cbl") > -1 Or sID.IndexOf("checkbox") > -1 Then
                                    Dim txt As New CheckBoxList
                                    txt = lookFor
                                    txt.Enabled = bEnabled
                                    txt = Nothing


                                ElseIf sID.ToLower = "system.web.ui.webcontrols.button" Then 'lookFor.ID.ToString.ToLower.IndexOf("button") > -1 Or sID.ToLower.IndexOf("button") > -1 Then
                                    Dim btn As New Button
                                    With btn.Text.ToLower
                                        Select Case btn.Text.ToLower
                                            Case .IndexOf("print") <> -1, .IndexOf("pdf") <> -1

                                        End Select
                                    End With
                                    If btn.Text.ToLower.IndexOf("print") <> -1 Then ' don't disable print button
                                        If btn.Text.ToLower.IndexOf("pdf") <> -1 Then ' don't disable print button
                                            btn = lookFor
                                            btn.Enabled = bEnabled

                                        End If

                                    End If

                                    btn = Nothing

                                ElseIf sID.ToLower = LCase("System.Web.UI.HtmlControls.HtmlButton") Then
                                    Dim btn As New System.Web.UI.HtmlControls.HtmlButton
                                    If btn.InnerText.ToLower.IndexOf("print") <> -1 Then ' don't disable print button
                                        If btn.InnerText.ToLower.IndexOf("pdf") <> -1 Then ' don't disable print button
                                            btn = lookFor
                                            btn.Disabled = bEnabled

                                        End If

                                    End If

                                    btn = Nothing


                                ElseIf sID.ToLower = "system.web.ui.webcontrols.gridview" Then
                                    Dim btn As New GridView
                                    btn = lookFor
                                    btn.Enabled = bEnabled
                                    btn = Nothing


                                ElseIf sID.ToLower = "system.web.ui.webcontrols.listbox" Then
                                    Dim btn As New ListBox
                                    btn = lookFor
                                    btn.Enabled = bEnabled
                                    btn = Nothing

                                End If
                            End If
                        End If

                    End If


                Next
                Return (lookFor)
            Catch ex As Exception
                sErr = ex.Message
                Throw ex
            End Try
            ' If we reach here, control was not found

            'End If
        End Function
        Public Shared Function SetChildControlsRecursiveToEnabledFalseOrig(ByRef ctrl As Control, ByVal bEnabled As Boolean) As Control
            Dim sID As String = ""
            Dim lookFor As Control
            For Each child As Control In ctrl.Controls
                If child.HasControls Then 'has childern
                    lookFor = New Control
                    lookFor = FindControlRecursiveAndClear(child)
                Else 'no  childern
                    lookFor = New Control
                    lookFor = child
                    If lookFor IsNot Nothing Then
                        If lookFor.GetType.ToString.ToLower.IndexOf("webcontrols") > -1 Then
                            '  sID = lookFor.ID.ToLower.ToString()
                            sID = lookFor.GetType.ToString.ToLower
                            'If lookFor.ID.ToLower.ToString.IndexOf("ddl") > -1 Then
                            If sID.IndexOf("ddl") > -1 Or sID.IndexOf("dropdownlist") > -1 Then
                                Dim ddl As New DropDownList
                                ddl = lookFor
                                If ddl.Items.Count > 0 Then
                                    ddl.Enabled = bEnabled
                                End If
                                ddl = Nothing
                            End If
                            If lookFor.ID.ToLower.IndexOf("rbl") > -1 Or sID.IndexOf("radiobuttonlist") > -1 Then
                                Dim rbl As New RadioButtonList
                                rbl = lookFor
                                If rbl.Items.Count > 0 Then
                                    rbl.Enabled = bEnabled
                                End If
                                rbl = Nothing
                            End If
                            If lookFor.ID.ToLower.IndexOf("textbox") > -1 Or sID.IndexOf("textbox") > -1 Then
                                Dim txt As New TextBox
                                txt = lookFor
                                txt.Enabled = bEnabled
                                txt = Nothing
                            End If
                            If lookFor.ID.ToLower.IndexOf("checkbox") > -1 Or sID.IndexOf("checkbox") > -1 Then
                                Dim txt As New TextBox
                                txt = lookFor
                                txt.Enabled = bEnabled
                                txt = Nothing
                            End If
                        End If
                    End If

                End If


            Next

            ' If we reach here, control was not found
            Return lookFor
            'End If
        End Function
    End Class
End Namespace