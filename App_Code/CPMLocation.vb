﻿Imports Microsoft.VisualBasic
Imports System.ComponentModel

Public Class CPMLocation
    Property Group_Num As String
    Property Group_name As String
    Property GroupTypeCd As String
    Property GroupID As String
    Property LocationTypeCd As String
    Property SpecialtyType As String
    Property ShortName As String
    Property CostCenter As String
    Property EffectiveDate As String
    Property OfficeNPI As String
    Property GroupNPI As String
    Property taxonomy As String
    Property Address1 As String
    Property Address2 As String
    Property City As String
    Property State As String
    Property Zip As String
    Property Phone As String
    Property Fax As String
    Property CLIA As String
    Property ValidationDate As String
    Property DateDeactivated As String
    Property CredentialedName As String
    Property Comments As String

End Class
