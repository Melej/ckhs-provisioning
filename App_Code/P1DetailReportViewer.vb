﻿Imports Microsoft.VisualBasic
Imports System.ComponentModel

Public Class P1DetailReportViewer
    Private _RequestNum As Integer
    Private _entryyear As String
    Private _EntryDate As String
    Private _group_name As String
    Private _EntryMonthName As String
    Private _HourofDay As String
    Private _EntryDayOfWeek As String
    Private _CategoryCd As String
    Private _TypeCd As String

    Public Sub New()

    End Sub
    <DisplayName("Request#")> Property RequestNum As Integer
        Get
            Return _RequestNum
        End Get
        Set(value As Integer)
            _RequestNum = value
        End Set
    End Property
    <DisplayName("Entry Year")> Property EntryDate As String
        Get
            Return _EntryDate
        End Get
        Set(value As String)
            _EntryDate = value
        End Set
    End Property

    <DisplayName("Entry Year")> Property entryyear As String
        Get
            Return _entryyear
        End Get
        Set(value As String)
            _entryyear = value
        End Set
    End Property
    <DisplayName("Tech Group Name")> Property group_name As String
        Get
            Return _group_name
        End Get
        Set(value As String)
            _group_name = value
        End Set
    End Property
    <DisplayName("Entry Month")> Property EntryMonthName As String
        Get
            Return _EntryMonthName
        End Get
        Set(value As String)
            _EntryMonthName = value
        End Set
    End Property
    <DisplayName("Hour of Day")> Property HourofDay As String
        Get
            Return _HourofDay
        End Get
        Set(value As String)
            _HourofDay = value
        End Set
    End Property
    <DisplayName("Day Of Week")> Property EntryDayOfWeek As String
        Get
            Return _EntryDayOfWeek
        End Get
        Set(value As String)
            _EntryDayOfWeek = value
        End Set
    End Property
    <DisplayName("CategoryCd")> Property CategoryCd As String
        Get
            Return _CategoryCd
        End Get
        Set(value As String)
            _CategoryCd = value
        End Set
    End Property
    <DisplayName("TypeCd")> Property TypeCd As String
        Get
            Return _TypeCd
        End Get
        Set(value As String)
            _TypeCd = value
        End Set
    End Property

End Class
