﻿Imports Microsoft.VisualBasic
Namespace App_Code
    Public Class SetDBConnection
        Dim sWebServer As String = ""

        Public Sub New()
            Dim sysEnvironment As New Environment()
            sWebServer = sysEnvironment.getEnvironment()

        End Sub
      

      


        Public Function EmployeeConn() As String


            If sWebServer = "Testing" Then

                EmployeeConn = ConfigurationManager.AppSettings.Get("TestEmployee.ConnectionString")

            Else

                EmployeeConn = ConfigurationManager.AppSettings.Get("ProductionEmployee.ConnectionString")

            End If

        End Function
        Public Function VaccineConn() As String
            If sWebServer = "Testing" Then

                VaccineConn = ConfigurationManager.AppSettings.Get("TestVaccine.ConnectionString")

            Else

                VaccineConn = ConfigurationManager.AppSettings.Get("ProductionVaccine.ConnectionString")

            End If


        End Function

        Public Function CSCConn() As String


            If sWebServer = "Testing" Then

                CSCConn = ConfigurationManager.AppSettings.Get("TestCSC.ConnectionString")

            Else

                CSCConn = ConfigurationManager.AppSettings.Get("ProductionCSC.ConnectionString")

            End If


        End Function

    End Class
End Namespace

