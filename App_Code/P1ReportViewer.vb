﻿Imports Microsoft.VisualBasic
Imports System.ComponentModel

Public Class P1ReportViewer
    'TotalCount  entryyear   group_name EntryMonthName HourofDay EntryDayOfWeek
    Private _TotalCount As String
    Private _entryyear As String
    Private _group_name As String
    Private _EntryMonthName As String
    Private _HourofDay As String
    Private _EntryDayOfWeek As String
    Private _DisplayField As String

    Public Sub New()

    End Sub
    <DisplayName("Total Count")> Property TotalCount As String
        Get
            Return _TotalCount
        End Get
        Set(value As String)
            _TotalCount = value
        End Set
    End Property
    <DisplayName("Entry Year")> Property entryyear As String
        Get
            Return _entryyear
        End Get
        Set(value As String)
            _entryyear = value
        End Set
    End Property
    <DisplayName("Tech Group Name")> Property group_name As String
        Get
            Return _group_name
        End Get
        Set(value As String)
            _group_name = value
        End Set
    End Property
    <DisplayName("Entry Month")> Property EntryMonthName As String
        Get
            Return _EntryMonthName
        End Get
        Set(value As String)
            _EntryMonthName = value
        End Set
    End Property
    <DisplayName("Hour of Day")> Property HourofDay As String
        Get
            Return _HourofDay
        End Get
        Set(value As String)
            _HourofDay = value
        End Set
    End Property
    <DisplayName("Day Of Week")> Property EntryDayOfWeek As String
        Get
            Return _EntryDayOfWeek
        End Get
        Set(value As String)
            _EntryDayOfWeek = value
        End Set
    End Property
    <DisplayName("Data")> Property DisplayField As String
        Get
            Return _DisplayField
        End Get
        Set(value As String)
            _DisplayField = value
        End Set
    End Property
End Class
