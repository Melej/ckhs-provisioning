﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports App_Code

Public Class AccountBusinessLogic
    Private _dtAccounts As DataTable
    Private _strAppCode As String
    Private _cblAccounts As CheckBoxList
    Private _dtRoleAccounts As DataTable
    Private _dtAppDependecy As DataTable
    Private _eCare As Boolean
    Private _EDM As Boolean




    Public Sub New()
        'getAppDependencyTable()
    End Sub

    Property CBLAccounts As CheckBoxList
        Get

            Return _cblAccounts
        End Get
        Set(value As CheckBoxList)

            _cblAccounts = value

        End Set
    End Property
  



    Property Accounts As DataTable
        Get
            Return _dtAccounts
        End Get
        Set(ByVal value As DataTable)

            _dtAccounts = value

        End Set
    End Property
    Public ReadOnly Property AccountCode As String
        Get
            If Not _strAppCode Is Nothing Then
                Return _strAppCode

            Else

                Return ""

            End If

        End Get
      
    End Property
    Public Function isRegionSpecific(ByVal Account As String) As Boolean
        Dim blReturn As Boolean = False

        Dim rsAccounts() As DataRow
        rsAccounts = _dtAccounts.Select("AppBase='" & Account & "' AND AppSystem = 'RegionSpec'")


        If rsAccounts.Count > 0 Then
            Dim drAccountCode As DataRow = rsAccounts(0)


            _strAppCode = drAccountCode("account_type_cd")


            blReturn = True

        End If


        Return blReturn
    End Function

    Public Function isHospSpecific(ByVal Account As String) As Boolean
        Dim blReturn As Boolean = False

        Dim rsAccounts() As DataRow
        rsAccounts = _dtAccounts.Select("AppBase='" & Account & "' AND AppSystem = 'HospSpec'")


        If rsAccounts.Count > 0 Then
            Dim drAccountCode As DataRow = rsAccounts(0)


            _strAppCode = drAccountCode("account_type_cd")


            blReturn = True

        End If


        Return blReturn
    End Function


    Public Function isEntSpecific(ByVal Account As String) As Boolean
        Dim blReturn As Boolean = False




        Dim rsAccounts() As DataRow
        rsAccounts = _dtAccounts.Select("AppBase='" & Account & "' AND AppSystem = 'EntSpec'")


        If rsAccounts.Count > 0 Then
            Dim drAccountCode As DataRow = rsAccounts(0)


            _strAppCode = drAccountCode("account_type_cd")


            blReturn = True

        End If


        Return blReturn
    End Function

    Public Function GetSystemAccountCode(ByVal Account As String, ByVal System As String) As String

        Dim rsAccounts() As DataRow
        rsAccounts = _dtAccounts.Select("AppBase='" & Account & "' AND AppSystem = '" & System & "'")

        Dim drAccountCode As DataRow = rsAccounts(0)


        Return drAccountCode("account_type_cd").ToString()



    End Function

    Public Function GetApplicationCode(ByVal AppNum As String, ByVal System As String) As String

        Dim rsAccounts() As DataRow
        rsAccounts = _dtAccounts.Select("AppBase='" & AppNum & "' AND AppSystem = '" & System & "'")

        Dim drAccountCode As DataRow = rsAccounts(0)


        Return drAccountCode("ApplicationNum").ToString()


    End Function

    Public Sub CheckAccountDependancy(ByRef cblApplications As CheckBoxList)

        For Each cblItem As ListItem In cblApplications.Items


            If CheckForECare(cblItem.Value) And cblItem.Selected Then
                _eCare = True
            End If
            If CheckForEdm(cblItem.Value) And cblItem.Selected Then
                _EDM = True
            End If

        Next

        SetAccountDependancy(cblApplications)

    End Sub

    Public Sub SetAccountDependancy(ByRef cblApplications As CheckBoxList)

        For Each cblItem As ListItem In cblApplications.Items

            If _eCare Then

                Select Case cblItem.Value
                    Case "3", "4"
                        cblItem.Selected = True
                        _EDM = True
                End Select

            End If



            If _EDM Then

                Select Case cblItem.Value
                    Case "43", "4"
                        cblItem.Selected = True
                        _eCare = True
                End Select
             

            End If


        Next
    End Sub

    Public Property RoleAccounts As DataTable
        Get
            Return _dtRoleAccounts

        End Get
        Set(value As DataTable)

            _dtRoleAccounts = value

        End Set
    End Property

    Protected Function CheckForECare(ByVal Item As String) As Boolean
        Dim blReturn As Boolean = False

        If Item = "43" Then

            blReturn = True

        End If



        Return blReturn
    End Function
    Protected Function CheckForEdm(ByVal Item As String) As Boolean
        Dim blReturn As Boolean = False

        If Item = "3" Then

            blReturn = True

        End If



        Return blReturn
    End Function
    Public ReadOnly Property eCare As Boolean
        Get

            Return _eCare
        End Get
    End Property
    Public ReadOnly Property EDM As Boolean
        Get

            Return _EDM
        End Get
    End Property


    Private Sub getAppDependencyTable()


        Dim thisData As New CommandsSqlAndOleDb("SelAppDependecy", HttpContext.Current.Session("EmployeeConn"))
        _dtAppDependecy = thisData.GetSqlDataTable

    End Sub


End Class
