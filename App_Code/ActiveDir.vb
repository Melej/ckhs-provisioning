﻿Imports Microsoft.VisualBasic
Imports System.DirectoryServices


Public Class ActiveDir
    Private _LDAPPath As String
    Private _DirectoryEntry As DirectoryEntry


    Public Sub New(ByVal ntLogin As String)
        _DirectoryEntry = GetUser(ntLogin)

    End Sub

    Public Function GetDirectoryEntry() As DirectoryEntry
        Dim dirEntry As DirectoryEntry = New DirectoryEntry()
        dirEntry.Path = "LDAP://CCMCDC02/DC=CKHSAD,DC=CROZER,DC=ORG"
        dirEntry.Username = "CKHSAD\Webdev10"
        dirEntry.Password = "c0k3andrum"


        Return dirEntry
    End Function

    Public Function ExtractUserName(ByVal path As String) As String
        Dim userPath As String() = path.Split(New Char() {"\"c})
        Return userPath((userPath.Length - 1))
    End Function


    Public Function GetUser(ByVal NtLogin As String) As DirectoryEntry
        Dim dirEntry As DirectoryEntry = GetDirectoryEntry()
        Dim dirSearch As New DirectorySearcher()

        dirSearch.SearchRoot = dirEntry
        dirSearch.Filter = "(&(objectClass=user)(SAMAccountName=" + NtLogin + "))"
        dirSearch.SearchScope = SearchScope.Subtree

        Dim searchResults As SearchResult = dirSearch.FindOne()


        If Not searchResults Is Nothing Then

            Return searchResults.GetDirectoryEntry()

        Else

            Return Nothing

        End If




    End Function

    Public ReadOnly Property UserAdEntry As DirectoryEntry
        Get
            Return _DirectoryEntry

        End Get
    End Property

End Class
