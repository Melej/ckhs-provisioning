﻿Imports Microsoft.VisualBasic
Imports System.ComponentModel

Public Class Actions

    Private _AccountRequestNum As Integer
    Private _AcctSeqNum As Integer
    Private _ActionNum As Integer
    Private _ActionCd As String
    Private _ActionCodeDesc As String
    Private _ActionDate As String
    Private _AccountTypeCd As String
    Private _TechClientNum As Integer
    Private _TechName As String
    Private _ActionDesc As String
    Public Sub New()

    End Sub
    <DisplayName("Account Request#")> Property AccountRequestNum As Integer
        Get
            Return _AccountRequestNum
        End Get
        Set(value As Integer)
            _AccountRequestNum = value
        End Set
    End Property
    <DisplayName("Account Seq#")> Property AcctSeqNum As Integer
        Get
            Return _AcctSeqNum
        End Get
        Set(value As Integer)
            _AcctSeqNum = value
        End Set
    End Property
    <DisplayName("Action #")> Property ActionNum As Integer
        Get
            Return _ActionNum
        End Get
        Set(value As Integer)
            _ActionNum = value
        End Set
    End Property
    <DisplayName("Action Code")> Property ActionCd As String
        Get
            Return _ActionCd
        End Get
        Set(value As String)
            _ActionCd = value
        End Set
    End Property
    <DisplayName("Action Code Description")> Property ActionCodeDesc As String
        Get
            Return _ActionCodeDesc
        End Get
        Set(value As String)
            _ActionCodeDesc = value
        End Set
    End Property
    <DisplayName("Action Date")> Property ActionDate As String
        Get
            Return _ActionDate
        End Get
        Set(value As String)
            _ActionDate = value
        End Set
    End Property
    <DisplayName("Action Type")> Property AccountTypeCd As String
        Get
            Return _AccountTypeCd
        End Get
        Set(value As String)
            _AccountTypeCd = value
        End Set
    End Property
    <DisplayName("Tech#")> Property TechClientNum As Integer
        Get
            Return _TechClientNum
        End Get
        Set(value As Integer)
            _TechClientNum = value
        End Set
    End Property
    <DisplayName("Tech Name")> Property TechName As String
        Get
            Return _TechName
        End Get
        Set(value As String)
            _TechName = value
        End Set
    End Property
    <DisplayName("Action Desc.")> Property ActionDesc As String
        Get
            Return _ActionDesc
        End Get
        Set(value As String)
            _ActionDesc = value
        End Set
    End Property
End Class
