﻿Imports Microsoft.VisualBasic
Imports System.Reflection
Imports System.ComponentModel

Public Class ObjectListPropertyView
    Private _obj As Object
    Private _viewProperties As New List(Of ObjectListProperty)
    Private _objProperties As New List(Of PropertyInfo)
    Public returnTable As New Table


    Public Sub New(ByRef obj As Object)

        _obj = obj
      
        For Each prop As PropertyInfo In obj.GetType().GetProperties()

            _objProperties.Add(prop)

        Next

    End Sub

    Public Sub AddViewProperty(ByRef PropertyName As String, Optional ByRef Ctrl As Control = Nothing)

        Dim objListprop As New ObjectListProperty(PropertyName, Ctrl)
        _viewProperties.Add(objListprop)


    End Sub

    Public Sub AddViewProperty(ByRef Prop As PropertyInfo, Optional ByRef Ctrl As Control = Nothing)

        Dim objListprop As New ObjectListProperty(Prop.Name, Ctrl)
        _viewProperties.Add(objListprop)


    End Sub


    Public Sub BuildTable()

        'If headerRow IsNot Nothing Then



        'End If

        For Each t In _viewProperties

            Dim tr As New TableRow
            Dim clName As New TableCell
            Dim clValue As New TableCell


            Dim p As PropertyInfo

            p = _obj.GetType().GetProperties().Where(Function(x) x.Name = t.PropertyName).FirstOrDefault()

            Dim attr As DisplayNameAttribute = DirectCast(Attribute.GetCustomAttribute(p, GetType(DisplayNameAttribute)), DisplayNameAttribute)

            clName.Text = attr.DisplayName

            clValue.Controls.Add(ControlCreator(p.GetValue(_obj, Nothing), t.DisplayControl, p.Name))
            ' clValue.Text = p.GetValue(_obj, Nothing)

            tr.Cells.Add(clName)
            tr.Cells.Add(clValue)

            returnTable.Rows.Add(tr)


        Next


    End Sub

    Public ReadOnly Property ObjectViewTable As Table
        Get
            Return returnTable

        End Get
    End Property


    Public Function ControlCreator(ByVal sValue As String, ByRef ctrl As Control, ByRef propName As String) As Control



        If TypeOf (ctrl) Is TextBox Then
            Dim txt As TextBox

            txt = DirectCast(ctrl, TextBox)

            txt.Text = sValue
            txt.ID = "txt" & propName



            Return txt

        End If


        If TypeOf (ctrl) Is Label Then
            Dim lbl As Label

            lbl = DirectCast(ctrl, Label)

            lbl.Text = sValue
            lbl.ID = "lbl" & propName


            Return lbl

        End If

        If TypeOf (ctrl) Is RadioButtonList Then

            Dim rbl As RadioButtonList

            rbl = DirectCast(ctrl, RadioButtonList)

            rbl.SelectedValue = sValue
            rbl.ID = "rbl" & propName

            Return rbl

        End If


        If TypeOf (ctrl) Is DropDownList Then

            Dim ddl As DropDownList

            ddl = DirectCast(ctrl, DropDownList)

            ddl.SelectedValue = sValue
            ddl.ID = "ddl" & propName


            Return ddl

        End If

    End Function

    Private Function _returnTable() As Object
        Throw New NotImplementedException
    End Function

End Class

Public Class ObjectListProperty
    Dim _sPropertyName As String
    Dim _ctrControl As New Control


    


    Public Sub New(ByRef sPropertyName As String, Optional ByRef ctr As Control = Nothing)

        _sPropertyName = sPropertyName

        If ctr Is Nothing Then

            _ctrControl = New Label()

        Else

            _ctrControl = ctr

        End If



    End Sub


    Public Property PropertyName As String
        Get
            Return _sPropertyName
        End Get
        Set(value As String)
            _sPropertyName = value
        End Set
    End Property

    Public Property DisplayControl As Control
        Get
            Return _ctrControl

        End Get
        Set(value As Control)
            _ctrControl = value
        End Set
    End Property


End Class
