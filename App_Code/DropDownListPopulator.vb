﻿Imports Microsoft.VisualBasic
Imports App_Code

Namespace Crozer



    Public Class DropDownListPopulator
        Inherits DropDownList
        Private _StoredProc As String
        Private _DataValue As String
        Private _TextField As String
        Private _dataConn As String

        Public Sub New()
            Dim conn As New SetDBConnection()

            _dataConn = conn.EmployeeConn

        End Sub

        Public Property StroedPrc() As String
            Get
                Return _StoredProc
            End Get
            Set(value As String)
                _StoredProc = value
            End Set
        End Property

        Public Property DataValue() As String
            Get
                Return _DataValue
            End Get
            Set(value As String)
                _DataValue = value
            End Set
        End Property

        Public Property TextField() As String
            Get
                Return _TextField

            End Get
            Set(value As String)
                _TextField = value
            End Set
        End Property




    End Class
End Namespace