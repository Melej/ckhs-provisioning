﻿Imports Microsoft.VisualBasic
Imports System.ComponentModel
Public Class TicketStatus
    Private _DataConn As String
    Private _requestnum As Integer

    Private _TotalTime As String
    Private _TotalacknoledgeTime As String
    Private _ack As String
    Private _acknoledgeTime As String
    Private _currstatuscd As String
    Private _Techname As String
    Private _Clientname As String
    Private _currstatusDesc As String
    Public Sub New()

    End Sub

    Public Sub New(ByVal RequestNum As Integer, ByVal DataConn As String)
        _requestnum = RequestNum
        _DataConn = DataConn
        GetValues()
    End Sub
    Private Sub GetValues()

        Dim cmdSql2 As New SqlCommand
        Dim connSql2 As New SqlConnection(_DataConn)

        Dim drSql2 As SqlClient.SqlDataReader

        connSql2.Open()


        cmdSql2.CommandText = "SelCurrentStatus"
        cmdSql2.CommandType = CommandType.StoredProcedure
        cmdSql2.Connection = connSql2
        cmdSql2.Parameters.Add("@request_num", SqlDbType.Int).Value = _requestnum

        Try


            drSql2 = cmdSql2.ExecuteReader


            While drSql2.Read()
                _requestnum = IIf(IsDBNull(drSql2("request_num")), Nothing, drSql2("request_num"))
                _TotalTime = IIf(IsDBNull(drSql2("TotalTime")), Nothing, drSql2("TotalTime"))

                '_entrydate = IIf(IsDBNull(drSql("entry_date")), Nothing, drSql("entrydate"))
                _TotalacknoledgeTime = IIf(IsDBNull(drSql2("TotalacknoledgeTime")), Nothing, drSql2("TotalacknoledgeTime"))
                _ack = IIf(IsDBNull(drSql2("ack")), Nothing, drSql2("ack"))
                _currstatuscd = IIf(IsDBNull(drSql2("currstatuscd")), Nothing, drSql2("currstatuscd"))
                _acknoledgeTime = IIf(IsDBNull(drSql2("acknoledgeTime")), Nothing, drSql2("acknoledgeTime"))
                _Techname = IIf(IsDBNull(drSql2("Techname")), Nothing, drSql2("Techname"))
                _Clientname = IIf(IsDBNull(drSql2("Clientname")), Nothing, drSql2("Clientname"))
                _currstatusDesc = IIf(IsDBNull(drSql2("currstatusDesc")), Nothing, drSql2("currstatusDesc"))


            End While

        Catch ex As Exception
            Throw ex

        End Try
    End Sub

    Public Property requestnum As Integer
        Get
            Return _requestnum
        End Get

        Set(value As Integer)
            _requestnum = value
        End Set
    End Property

    Public Property TotalTime As String
        Get
            Return _TotalTime
        End Get
        Set(value As String)
            _TotalTime = value
        End Set

    End Property
    Public Property TotalacknoledgeTime As String
        Get
            Return _TotalacknoledgeTime
        End Get
        Set(value As String)
            _TotalacknoledgeTime = value
        End Set

    End Property
    Public Property ack As String
        Get
            Return _ack
        End Get
        Set(value As String)
            _ack = value
        End Set

    End Property
    Public Property acknoledgeTime As String
        Get
            Return _acknoledgeTime
        End Get
        Set(value As String)
            _acknoledgeTime = value
        End Set

    End Property
    Public Property currstatuscd As String
        Get
            Return _currstatuscd
        End Get
        Set(value As String)
            _currstatuscd = value
        End Set

    End Property

    Public Property Techname As String
        Get
            Return _Techname
        End Get
        Set(value As String)
            _Techname = value
        End Set

    End Property
    Public Property Clientname As String
        Get
            Return _Clientname
        End Get
        Set(value As String)
            _Clientname = value
        End Set

    End Property
    Public Property currstatusDesc As String
        Get
            Return _currstatusDesc
        End Get
        Set(value As String)
            _currstatusDesc = value
        End Set

    End Property

End Class
