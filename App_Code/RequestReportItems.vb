﻿Imports Microsoft.VisualBasic
Imports App_Code

Public Class RequestReportItems

    Private _RptRequestItems As New List(Of RPTRequestItems)

    'Private _dataConn As String
    'Private _dtAccountRequestItems As New DataTable
    'Private _ClientAccess As New List(Of ClientAccountAccess)

    Public Sub New()


    End Sub

    Public Sub New(ByRef sConn As String, ByRef sRequest As String)
        If sConn Is Nothing Then
            sConn = HttpContext.Current.Session("CSCConn")
        End If

        'If sDiaplay = "" Then
        '    sDiaplay = "queue"
        'End If

        Using SqlConnection As New SqlConnection(sConn)
            Dim Cmd As New SqlCommand()

            Cmd.CommandText = "SelRequestItems"
            Cmd.Parameters.AddWithValue("@request_num", sRequest)

            'Cmd.Parameters.AddWithValue("@display", sDiaplay)
            '@request_num

            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.Connection = SqlConnection


            SqlConnection.Open()
            Using Cmd

                Using dr = Cmd.ExecuteReader()

                    If dr.HasRows Then

                        Do While dr.Read

                            Dim RPTItems As New RPTRequestItems

                            RPTItems.RequestNum = IIf(IsDBNull(dr("request_num")), Nothing, dr("request_num"))
                            RPTItems.ItemNum = IIf(IsDBNull(dr("request_item_num")), Nothing, dr("request_item_num"))

                            RPTItems.ItemType = IIf(IsDBNull(dr("item_type_cd")), Nothing, dr("item_type_cd"))

                            RPTItems.ItemDetail = IIf(IsDBNull(dr("item_detail")), Nothing, dr("item_detail"))
                            RPTItems.itemDesc = IIf(IsDBNull(dr("item_desc")), Nothing, dr("item_desc"))



                            _RptRequestItems.Add(RPTItems)
                        Loop
                    End If

                End Using
            End Using
        End Using

    End Sub
    Public Sub New(ByRef sConn As String, ByRef sRequest As String, ByRef sType As String)
        If sConn Is Nothing Then
            sConn = HttpContext.Current.Session("CSCConn")
        End If

        'If sDiaplay = "" Then
        '    sDiaplay = "queue"
        'End If

        Using SqlConnection As New SqlConnection(sConn)
            Dim Cmd As New SqlCommand()

            Cmd.CommandText = "SelRequestItems"
            Cmd.Parameters.AddWithValue("@request_num", sRequest)

            Cmd.Parameters.AddWithValue("@itemType", sType)

            '@request_num

            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.Connection = SqlConnection


            SqlConnection.Open()
            Using Cmd

                Using dr = Cmd.ExecuteReader()

                    If dr.HasRows Then

                        Do While dr.Read

                            Dim RPTItems As New RPTRequestItems

                            RPTItems.RequestNum = IIf(IsDBNull(dr("request_num")), Nothing, dr("request_num"))
                            RPTItems.ItemNum = IIf(IsDBNull(dr("request_item_num")), Nothing, dr("request_item_num"))

                            RPTItems.ItemType = IIf(IsDBNull(dr("item_type_cd")), Nothing, dr("item_type_cd"))

                            RPTItems.ItemDetail = IIf(IsDBNull(dr("item_detail")), Nothing, dr("item_detail"))
                            RPTItems.itemDesc = IIf(IsDBNull(dr("item_desc")), Nothing, dr("item_desc"))



                            _RptRequestItems.Add(RPTItems)
                        Loop
                    End If

                End Using
            End Using
        End Using

    End Sub
    Public ReadOnly Property AllItems As List(Of RPTRequestItems)
        Get
            Return _RptRequestItems

        End Get
    End Property
    Public Function GetRequestByItemType(ByVal sType As String) As List(Of RPTRequestItems)

        Dim rptByType = (From reqItem As RPTRequestItems In _RptRequestItems _
                       Where reqItem.ItemType = sType _
                       Select reqItem).ToList

        Return rptByType

    End Function
End Class
Public Class RPTRequestItems

    Public Property RequestNum As Integer
    Public Property ItemNum As String

    Public Property ItemType As String
    Public Property ItemDetail As String
    Public Property itemDesc As String

End Class
