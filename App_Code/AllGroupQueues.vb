﻿Imports Microsoft.VisualBasic
Imports System.ComponentModel

Public Class AllGroupQueuesOld
    Public Property _GroupNum As Integer
    Public Property _GroupName As String
    Public Property _DisplayStatus As String

    Public Sub New()

    End Sub
    <DisplayName("GroupNum")> Property GroupNum As Integer
        Get
            Return _GroupNum
        End Get
        Set(value As Integer)
            _GroupNum = value
        End Set
    End Property
    <DisplayName("GroupName")> Property GroupName As String
        Get
            Return _GroupName
        End Get
        Set(value As String)
            _GroupName = value
        End Set
    End Property
    <DisplayName("DisplayStatus")> Property DisplayStatus As String
        Get
            Return _DisplayStatus
        End Get
        Set(value As String)
            _DisplayStatus = value
        End Set
    End Property
End Class
