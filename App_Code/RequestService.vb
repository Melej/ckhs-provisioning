﻿Imports Microsoft.VisualBasic

Public Class RequestService
    Inherits RequestEntry


    Property ServiceDesc As String
    Property BusinessDesc As String
    Property ImpactDesc As String
    Property RequestedCompleteDate As DateTime
    Property CategoryTypeCd As String
    Property TypeCd As String
    Property ItemCd As String
    Property AssetTag As String
    Property ServiceType As String
    Property ResolutionCd As String
    Property ResolutionDesc As String


End Class
