﻿Imports Microsoft.VisualBasic
Imports App_Code



Public Class RequestActivity

    Public Sub New(ByVal RequestNum As Integer, ByVal TechClientNum As Integer, ByVal ActivityDesc As String, ByVal StatusType As String, ByVal StatusCd As String)
        Dim _dataConn As String

        Dim conn As New SetDBConnection()

        _dataConn = conn.EmployeeConn


        Dim thisData As New CommandsSqlAndOleDb("InsRequestActivites", _dataConn)
        thisData.AddSqlProcParameter("@RequestNum", RequestNum, SqlDbType.Int)
        thisData.AddSqlProcParameter("@TechClientNum", TechClientNum, SqlDbType.Int)
        thisData.AddSqlProcParameter("@ActivityDesc", ActivityDesc, SqlDbType.NVarChar)
        thisData.AddSqlProcParameter("@StatusType", StatusType, SqlDbType.NVarChar)
        thisData.AddSqlProcParameter("@StatusCd", StatusCd, SqlDbType.NVarChar)

        thisData.ExecNonQueryNoReturn()
    End Sub


End Class
