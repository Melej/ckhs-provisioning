﻿Imports Microsoft.VisualBasic
Imports iTextSharp
Imports iTextSharp.text

Namespace App_Code
    Public Class PDFPageEvents
        Implements iTextSharp.text.pdf.IPdfPageEvent

        Private msHeaderInfo As String = ""
        Private msFooterInfo As String = ""
        Private msDateInfo As String = ""
        Private msUserInfo As String = ""
        Private mbDontUseSignature As Boolean
        Private moTemplate As iTextSharp.text.pdf.PdfTemplate
        Private moCB As iTextSharp.text.pdf.PdfContentByte
        Private moBF As iTextSharp.text.pdf.BaseFont = Nothing

        Public Sub New( _
                       ByVal sMachineInfo As String, _
                       ByVal sDateInfo As String, ByVal sFooterInfoIn As String, ByVal sUser As String, ByVal bDontUseSignature As Boolean)
            MyBase.New()
            msFooterInfo = Replace(sFooterInfoIn, "'", "")
            msHeaderInfo = Replace(sMachineInfo, "'", "")
            msDateInfo = sDateInfo
            msUserInfo = Replace(sUser, "'", "")
            mbDontUseSignature = bDontUseSignature
            '===sample implementation==========================================
            ''    You would attach it to the writer as follows:
            ''Dim oWriter As iTextSharp.text.pdf.PdfWriter = iTextSharp.text.pdf.PdfWriter.GetInstance(oDoc, New FileStream(msFilePath, FileMode.Create))


            '-==============================================================
        End Sub
        Public Sub New( _
                       ByVal sMachineInfo As String, _
                       ByVal sDateInfo As String, ByVal sFooterInfoIn As String, ByVal sUser As String)
            MyBase.New()
            msFooterInfo = Replace(sFooterInfoIn, "'", "")
            msHeaderInfo = Replace(sMachineInfo, "'", "")
            msDateInfo = sDateInfo
            msUserInfo = Replace(sUser, "'", "")
            '===sample implementation==========================================
            ''    You would attach it to the writer as follows:
            ''Dim oWriter As iTextSharp.text.pdf.PdfWriter = iTextSharp.text.pdf.PdfWriter.GetInstance(oDoc, New FileStream(msFilePath, FileMode.Create))


            '-==============================================================
        End Sub
        Public Sub New( _
                       ByVal sHeaderInfo As String, _
                       ByVal sDateInfo As String)
            MyBase.New()

            sHeaderInfo = Replace(sHeaderInfo, "'", "")
            msDateInfo = Replace(sDateInfo, "'", "")
            '===sample implementation==========================================
            ''    You would attach it to the writer as follows:
            ''Dim oWriter As iTextSharp.text.pdf.PdfWriter = iTextSharp.text.pdf.PdfWriter.GetInstance(oDoc, New FileStream(msFilePath, FileMode.Create))


            '-==============================================================
        End Sub
#Region "Events"
        Public Sub OnChapter(ByVal writer As iTextSharp.text.pdf.PdfWriter, ByVal document As iTextSharp.text.Document, ByVal paragraphPosition As Single, ByVal title As iTextSharp.text.Paragraph) Implements iTextSharp.text.pdf.IPdfPageEvent.OnChapter

        End Sub

        Public Sub OnChapterEnd(ByVal writer As iTextSharp.text.pdf.PdfWriter, ByVal document As iTextSharp.text.Document, ByVal paragraphPosition As Single) Implements iTextSharp.text.pdf.IPdfPageEvent.OnChapterEnd

        End Sub

        Public Sub OnCloseDocument(ByVal writer As iTextSharp.text.pdf.PdfWriter, ByVal document As iTextSharp.text.Document) Implements iTextSharp.text.pdf.IPdfPageEvent.OnCloseDocument
            moTemplate.BeginText()
            moTemplate.SetFontAndSize(moBF, 8)
            moTemplate.ShowText((writer.PageNumber - 1).ToString)
            moTemplate.EndText()
        End Sub

        Public Sub OnEndPage(ByVal writer As iTextSharp.text.pdf.PdfWriter, ByVal document As iTextSharp.text.Document) Implements _
                                                                                                                            iTextSharp.text.pdf.IPdfPageEvent.OnEndPage
            setHeader(writer, document)
            setFooter(writer, document)
        End Sub

        Public Sub OnGenericTag(ByVal writer As iTextSharp.text.pdf.PdfWriter, ByVal document As iTextSharp.text.Document, ByVal rect As iTextSharp.text.Rectangle, ByVal text As String) Implements iTextSharp.text.pdf.IPdfPageEvent.OnGenericTag

        End Sub

        Public Sub OnOpenDocument(ByVal writer As iTextSharp.text.pdf.PdfWriter, ByVal document As iTextSharp.text.Document) Implements _
                                                                                                                                 iTextSharp.text.pdf.IPdfPageEvent.OnOpenDocument
            Try
                moBF = iTextSharp.text.pdf.BaseFont.CreateFont(iTextSharp.text.pdf.BaseFont.HELVETICA, iTextSharp.text.pdf.BaseFont.CP1252, iTextSharp.text.pdf.BaseFont.NOT_EMBEDDED)
                moCB = writer.DirectContent
                moTemplate = moCB.CreateTemplate(50, 50)

            Catch de As DocumentException

                ' Catch ioe As iTextSharp.text.pdf.PdfException 'IOException

            End Try
        End Sub

        Public Sub OnParagraph(ByVal writer As iTextSharp.text.pdf.PdfWriter, ByVal document As iTextSharp.text.Document, ByVal paragraphPosition As Single) Implements iTextSharp.text.pdf.IPdfPageEvent.OnParagraph

        End Sub

        Public Sub OnParagraphEnd(ByVal writer As iTextSharp.text.pdf.PdfWriter, ByVal document As iTextSharp.text.Document, ByVal paragraphPosition As Single) Implements iTextSharp.text.pdf.IPdfPageEvent.OnParagraphEnd

        End Sub

        Public Sub OnSection(ByVal writer As iTextSharp.text.pdf.PdfWriter, ByVal document As iTextSharp.text.Document, ByVal paragraphPosition As Single, ByVal depth As Integer, ByVal title As iTextSharp.text.Paragraph) Implements iTextSharp.text.pdf.IPdfPageEvent.OnSection

        End Sub

        Public Sub OnSectionEnd(ByVal writer As iTextSharp.text.pdf.PdfWriter, ByVal document As iTextSharp.text.Document, ByVal paragraphPosition As Single) Implements iTextSharp.text.pdf.IPdfPageEvent.OnSectionEnd

        End Sub

        Public Sub OnStartPage(ByVal writer As iTextSharp.text.pdf.PdfWriter, ByVal document As iTextSharp.text.Document) Implements _
                                                                                                                              iTextSharp.text.pdf.IPdfPageEvent.OnStartPage

        End Sub
#End Region

#Region "OnPageEnd"
        Private Sub setHeader(ByVal oWriter As iTextSharp.text.pdf.PdfWriter, ByVal oDocument As Document)
            Dim oTable As New iTextSharp.text.pdf.PdfPTable(1) 'iTextSharp.text.pdf.PdfPTable(3)
            Dim oCell As iTextSharp.text.pdf.PdfPCell
            With oTable
                'oCell = New iTextSharp.text.pdf.PdfPCell(New iTextSharp.text.Phrase("Scheduled Jobs Report", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.NORMAL)))
                'oCell.Border = 0
                'oCell.HorizontalAlignment = Element.ALIGN_CENTER
                '.AddCell(oCell)

                oCell = New iTextSharp.text.pdf.PdfPCell(New iTextSharp.text.Phrase(msHeaderInfo, FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.NORMAL)))
                oCell.Border = 0
                oCell.HorizontalAlignment = Element.ALIGN_CENTER
                .AddCell(oCell)

                'oCell = New iTextSharp.text.pdf.PdfPCell(New iTextSharp.text.Phrase(msDateInfo, FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.NORMAL)))
                'oCell.Border = 0
                'oCell.HorizontalAlignment = Element.ALIGN_CENTER
                '.AddCell(oCell)
            End With

            oTable.TotalWidth = oDocument.PageSize.Width - 20
            oTable.WriteSelectedRows(0, -1, 10, oDocument.PageSize.Height - 15, oWriter.DirectContent)

            '---Line just below header
            'moCB.MoveTo(30, oDocument.PageSize.Height - 35)
            'moCB.LineTo(oDocument.PageSize.Width - 40, oDocument.PageSize.Height - 35)
            'moCB.Stroke()
        End Sub

        Private Sub setFooter(ByVal oWriter As iTextSharp.text.pdf.PdfWriter, ByVal oDocument As iTextSharp.text.Document)
            'three columns at bottom of page
            Dim sText As String
            Dim fLen As Single
            ' ''---Signature Row1 Column 1: Disclaimer
            If mbDontUseSignature = False Then

                sText = "  Signature:___________________________________________" & msUserInfo 'msDateInfo
                fLen = moBF.GetWidthPoint(sText, 8)

                moCB.BeginText()
                moCB.SetFontAndSize(moBF, 8)
                moCB.SetTextMatrix(30, 50)      'left, top  higher than 30
                moCB.ShowText(sText)
                moCB.EndText()
            End If

            ' '' Row2 Column 1: Disclaimer
            sText = msFooterInfo 'msDateInfo
            If sText = "" Then sText = "_"


            fLen = moBF.GetWidthPoint(sText, 8)

            moCB.BeginText()
            moCB.SetFontAndSize(moBF, 8)
            moCB.SetTextMatrix(30, 30)
            moCB.ShowText(sText)
            moCB.EndText()
            '===end
            ''---Column 2:row 41 Date/Time
            'sText = msFooterInfo
            'fLen = moBF.GetWidthPoint(sText, 8)

            'moCB.BeginText()
            'moCB.SetFontAndSize(moBF, 8)
            ''orig moCB.SetTextMatrix(oDocument.PageSize.Width / 2 - fLen / 2, 30)
            'moCB.SetTextMatrix(oDocument.PageSize.Width / 2 - fLen / 2, 41)
            'moCB.ShowText(sText)
            'moCB.EndText()
            '---Row 2 Column 2: Date/Time
            sText = Now.ToString
            fLen = moBF.GetWidthPoint(sText, 8)

            moCB.BeginText()
            moCB.SetFontAndSize(moBF, 8)
            moCB.SetTextMatrix(oDocument.PageSize.Width / 2 - fLen / 2, 30)
            '  moCB.SetTextMatrix(oDocument.PageSize.Width / 2 - fLen / 2, 41)
            moCB.ShowText(sText)
            moCB.EndText()

            '---Column 3: Page Number
            Dim iPageNumber As Integer = oWriter.PageNumber
            sText = "Page " & iPageNumber & " of "
            fLen = moBF.GetWidthPoint(sText, 8)

            moCB.BeginText()
            moCB.SetFontAndSize(moBF, 8)
            moCB.SetTextMatrix(oDocument.PageSize.Width - 90, 30)
            moCB.ShowText(sText)
            moCB.EndText()

            moCB.AddTemplate(moTemplate, oDocument.PageSize.Width - 90 + fLen, 30)
            moCB.BeginText()
            moCB.SetFontAndSize(moBF, 8)
            moCB.SetTextMatrix(280, 820)
            moCB.EndText()

            '---Line just above footer
            moCB.MoveTo(30, 40)
            moCB.LineTo(oDocument.PageSize.Width - 40, 40)
            moCB.Stroke()

        End Sub
        Private Sub setFooter3Column(ByVal oWriter As iTextSharp.text.pdf.PdfWriter, ByVal oDocument As iTextSharp.text.Document)
            'three columns at bottom of page
            'Original format
            Dim sText As String
            Dim fLen As Single

            '---Column 1: Disclaimer
            sText = msFooterInfo 'msDateInfo
            fLen = moBF.GetWidthPoint(sText, 8)

            moCB.BeginText()
            moCB.SetFontAndSize(moBF, 8)
            moCB.SetTextMatrix(30, 30)
            moCB.ShowText(sText)
            moCB.EndText()

            '---Column 2: Date/Time
            sText = Now.ToString
            fLen = moBF.GetWidthPoint(sText, 8)

            moCB.BeginText()
            moCB.SetFontAndSize(moBF, 8)
            moCB.SetTextMatrix(oDocument.PageSize.Width / 2 - fLen / 2, 30)
            moCB.ShowText(sText)
            moCB.EndText()

            '---Column 3: Page Number
            Dim iPageNumber As Integer = oWriter.PageNumber
            sText = "Page " & iPageNumber & " of "
            fLen = moBF.GetWidthPoint(sText, 8)

            moCB.BeginText()
            moCB.SetFontAndSize(moBF, 8)
            moCB.SetTextMatrix(oDocument.PageSize.Width - 90, 30)
            moCB.ShowText(sText)
            moCB.EndText()

            moCB.AddTemplate(moTemplate, oDocument.PageSize.Width - 90 + fLen, 30)
            moCB.BeginText()
            moCB.SetFontAndSize(moBF, 8)
            moCB.SetTextMatrix(280, 820)
            moCB.EndText()

            '---Line just above footer
            moCB.MoveTo(30, 40)
            moCB.LineTo(oDocument.PageSize.Width - 40, 40)
            moCB.Stroke()

        End Sub
#End Region
        'Public Sub onparagraphend(ByVal writer As iTextSharp.text.pdf.PdfWriter, _
        '                  ByVal document As iTextSharp.text.Document, ByVal paragrphposition As Single) Implements iTextSharp.text.pdf.IPdfPageEvent.OnParagraphEnd

        'End Sub
        'Public Sub OnChapter(ByVal writer As iTextSharp.text.pdf.PdfWriter, _
        '                   ByVal document As iTextSharp.text.Document, ByVal paragraphposition As Single, ByVal title As Paragraph) Implements iTextSharp.text.pdf.IPdfPageEvent.OnChapter

        'End Sub
        'Public Sub OnChapterEnd(ByVal writer As iTextSharp.text.pdf.PdfWriter, _
        '                   ByVal document As iTextSharp.text.Document, ByVal paragraphposition As Single) Implements iTextSharp.text.pdf.IPdfPageEvent.OnChapterEnd

        'End Sub
        'Public Sub OnSection(ByVal writer As iTextSharp.text.pdf.PdfWriter, _
        '                ByVal document As iTextSharp.text.Document, ByVal paragraphposition As Single, ByVal depth As Integer, ByVal title As Paragraph) Implements iTextSharp.text.pdf.IPdfPageEvent.OnSection

        'End Sub
        'Public Sub OnSectionEnd(ByVal writer As iTextSharp.text.pdf.PdfWriter, _
        '                   ByVal document As iTextSharp.text.Document, ByVal paragraphposition As Single) Implements iTextSharp.text.pdf.IPdfPageEvent.OnSectionEnd

        'End Sub
        'Public Sub OnGenericTag(ByVal writer As iTextSharp.text.pdf.PdfWriter, _
        '            ByVal document As iTextSharp.text.Document, ByVal rect As Rectangle, ByVal text As String) Implements iTextSharp.text.pdf.IPdfPageEvent.OnGenericTag

        'End Sub
        'Public Sub OnParagraph(ByVal writer As iTextSharp.text.pdf.PdfWriter, _
        '                   ByVal document As iTextSharp.text.Document, ByVal paragraphposition As Single) Implements iTextSharp.text.pdf.IPdfPageEvent.OnParagraph

        'End Sub
        'Public Sub OnStartPage(ByVal writer As iTextSharp.text.pdf.PdfWriter, _
        '                   ByVal document As iTextSharp.text.Document) Implements iTextSharp.text.pdf.IPdfPageEvent.OnStartPage

        'End Sub
    End Class

End Namespace