﻿Imports Microsoft.VisualBasic
Imports System.ComponentModel
Public Class RequestTicket

    Private _DataConn As String
    Private _requestnum As Integer
    Private _clientnum As Integer
    Private _entrydate As String
    Private _closedate As String
    Private _requesttype As String

    Private _FirstName As String
    Private _LastName As String

    Private _categorycd As String
    Private _typecd As String
    Private _itemcd As String
    Private _priority As String
    Private _groupassigntime As String
    Private _assignedgroupnum As String
    Private _currentgroupnum As String
    Private _currentgroupname As String

    Private _techassigntime As String

    Private _assignedtechnum As String
    Private _assignedtechname As String

    Private _currenttechnum As String
    Private _currenttechname As String

    Private _acknowledgetime As String
    Private _currentacktime As String
    Private _platform As String
    Private _resolutioncd As String

    Private _entrytechnum As String
    Private _entrytechname As String

    Private _EMail As String
    Private _empTypeCd As String
    Private _Title As String
    Private _Phone As String
    Private _DepartmentCd As String
    Private _DepartmentName As String

    Private _FacilityCd As String
    Private _FacilityName As String
    Private _BuildingCd As String
    Private _BuildingName As String
    Private _Office As String
    Private _Floor As String
    Private _Address1 As String
    Private _Address2 As String
    Private _City As String
    Private _State As String
    Private _Zip As String

    Private _AssociatedRequests As String

    Private _alternatecontactname As String
    Private _alternatecontactphone As String
    Private _shortdesc As String
    Private _proxyonpc As String

    Private _assettag As String
    Private _ipaddress As String

    Private _completiondate As String
    Private _servicedesc As String
    Private _businessdes As String
    Private _impactdesc As String
    Private _numofterminals As String
    Private _terminalids As String
    Private _terminalinfodesc As String
    Private _numofprinters As String
    Private _printerids As String
    Private _printerinfodesc As String
    Private _numofpc As String
    Private _pcids As String
    Private _pcinfodesc As String
    Private _numofnetdrops As String
    Private _netdropids As String
    Private _netdropsinfodesc As String
    Private _numofother As String
    Private _otherids As String
    Private _otherinfodesc As String
    Private _servicetype As String
    Private _statuscd As String
    Private _lastchangedate As String
    Private _lastentertechnum As String
    Private _lastenteredname As String
    Private _clientexpect As String
    Private _statusdesc As String
    Private _ActivityDesc As String
    Private _categorynum As String
    Private _typenum As String
    Private _itemnum As String
    Private _quickticket As String
    Private _ntlogin As String
    Private _TotalAcknowledgeTime As String
    Private _Sendemail As String
    Private _Surveyexists As String

    'Private _RequestActivityItems As List(Of RequestActivityItem)


    Public Sub New()

    End Sub
    Public Sub New(ByVal RequestNum As Integer, ByVal DataConn As String)
        _requestnum = RequestNum
        _DataConn = DataConn
        GetValues()
    End Sub
    Private Sub GetValues()


        Dim cmdSql As New SqlCommand
        Dim connSql As New SqlConnection(_DataConn)
        Dim drSql As SqlClient.SqlDataReader


        '   connSql.ConnectionString = _DataConn

        cmdSql.CommandText = "selTicketRFSByNumV7"
        cmdSql.CommandType = CommandType.StoredProcedure
        cmdSql.Connection = connSql
        cmdSql.Parameters.Add("@requestnum", SqlDbType.Int).Value = _requestnum


        connSql.Open()

        Try




            drSql = cmdSql.ExecuteReader


            While drSql.Read()
                _requestnum = IIf(IsDBNull(drSql("request_num")), Nothing, drSql("request_num"))

                _clientnum = IIf(IsDBNull(drSql("client_num")), Nothing, drSql("client_num"))
                _entrydate = IIf(IsDBNull(drSql("entrydate")), Nothing, drSql("entrydate"))

                '_entrydate = IIf(IsDBNull(drSql("entry_date")), Nothing, drSql("entrydate"))
                _closedate = IIf(IsDBNull(drSql("close_date")), Nothing, drSql("close_date"))
                _requesttype = IIf(IsDBNull(drSql("requesttype")), Nothing, drSql("requesttype"))

                _FirstName = IIf(IsDBNull(drSql("first_name")), Nothing, drSql("first_name"))
                _LastName = IIf(IsDBNull(drSql("last_name")), Nothing, drSql("last_name"))

                _categorycd = IIf(IsDBNull(drSql("categorycd")), Nothing, drSql("categorycd"))
                _typecd = IIf(IsDBNull(drSql("typecd")), Nothing, drSql("typecd"))
                _itemcd = IIf(IsDBNull(drSql("itemcd")), Nothing, drSql("itemcd"))
                _priority = IIf(IsDBNull(drSql("priority")), Nothing, drSql("priority"))

                _groupassigntime = IIf(IsDBNull(drSql("groupassigntime")), Nothing, drSql("groupassigntime"))
                _assignedgroupnum = IIf(IsDBNull(drSql("assignedgroupnum")), Nothing, drSql("assignedgroupnum"))

                _currentgroupnum = IIf(IsDBNull(drSql("currentgroupnum")), Nothing, drSql("currentgroupnum"))
                _currentgroupname = IIf(IsDBNull(drSql("currentgroupname")), Nothing, drSql("currentgroupname"))

                _techassigntime = IIf(IsDBNull(drSql("techassigntime")), Nothing, drSql("techassigntime"))

                _assignedtechnum = IIf(IsDBNull(drSql("assignedtechnum")), Nothing, drSql("assignedtechnum"))
                _assignedtechname = IIf(IsDBNull(drSql("assignedtechname")), Nothing, drSql("assignedtechname"))

                _currenttechnum = IIf(IsDBNull(drSql("currenttechnum")), Nothing, drSql("currenttechnum"))
                _currenttechname = IIf(IsDBNull(drSql("currenttechname")), Nothing, drSql("currenttechname"))

                _acknowledgetime = IIf(IsDBNull(drSql("acknowledgetime")), Nothing, drSql("acknowledgetime"))
                _currentacktime = IIf(IsDBNull(drSql("currentacktime")), Nothing, drSql("currentacktime"))

                _assettag = IIf(IsDBNull(drSql("assettag")), Nothing, drSql("assettag"))
                _ipaddress = IIf(IsDBNull(drSql("ipaddress")), Nothing, drSql("ipaddress"))

                _platform = IIf(IsDBNull(drSql("platform")), Nothing, drSql("platform"))
                _resolutioncd = IIf(IsDBNull(drSql("resolution_cd")), Nothing, drSql("resolution_cd"))

                _entrytechnum = IIf(IsDBNull(drSql("entrytechnum")), Nothing, drSql("entrytechnum"))
                _entrytechname = IIf(IsDBNull(drSql("entrytechname")), Nothing, drSql("entrytechname"))

                _empTypeCd = IIf(IsDBNull(drSql("empTypeCd")), Nothing, drSql("empTypeCd"))

                _AssociatedRequests = IIf(IsDBNull(drSql("AssociatedRequests")), Nothing, drSql("AssociatedRequests"))

                _EMail = IIf(IsDBNull(drSql("email")), Nothing, drSql("email"))
                _Title = IIf(IsDBNull(drSql("title")), Nothing, drSql("title"))
                _Phone = IIf(IsDBNull(drSql("phone")), Nothing, drSql("phone"))

                _DepartmentCd = IIf(IsDBNull(drSql("department_cd")), Nothing, drSql("department_cd"))
                _DepartmentName = IIf(IsDBNull(drSql("department_name")), Nothing, drSql("department_name"))

                _FacilityCd = IIf(IsDBNull(drSql("facility_cd")), Nothing, drSql("facility_cd"))
                _FacilityName = IIf(IsDBNull(drSql("facility_name")), Nothing, drSql("facility_name"))

                _BuildingCd = IIf(IsDBNull(drSql("building_cd")), Nothing, drSql("building_cd"))
                _BuildingName = IIf(IsDBNull(drSql("building_name")), Nothing, drSql("building_name"))

                _Office = IIf(IsDBNull(drSql("office")), Nothing, drSql("office"))
                _Floor = IIf(IsDBNull(drSql("floor")), Nothing, drSql("floor"))

                _Address1 = IIf(IsDBNull(drSql("address_1")), Nothing, drSql("address_1"))
                _Address2 = IIf(IsDBNull(drSql("address_2")), Nothing, drSql("address_2"))
                _City = IIf(IsDBNull(drSql("city")), Nothing, drSql("city"))
                _State = IIf(IsDBNull(drSql("state")), Nothing, drSql("state"))
                _Zip = IIf(IsDBNull(drSql("zip")), Nothing, drSql("zip"))



                _alternatecontactname = IIf(IsDBNull(drSql("alternatecontactname")), Nothing, drSql("alternatecontactname"))
                _alternatecontactphone = IIf(IsDBNull(drSql("alternatecontactphone")), Nothing, drSql("alternatecontactphone"))
                _shortdesc = IIf(IsDBNull(drSql("shortdesc")), Nothing, drSql("shortdesc"))
                _proxyonpc = IIf(IsDBNull(drSql("proxyonpc")), Nothing, drSql("proxyonpc"))

                'RFS Fields
                _completiondate = IIf(IsDBNull(drSql("completiondate")), Nothing, drSql("completiondate"))
                _servicedesc = IIf(IsDBNull(drSql("servicedesc")), Nothing, drSql("servicedesc"))
                _businessdes = IIf(IsDBNull(drSql("businessdes")), Nothing, drSql("businessdes"))
                _impactdesc = IIf(IsDBNull(drSql("impactdesc")), Nothing, drSql("impactdesc"))

                _numofterminals = IIf(IsDBNull(drSql("num_of_terminals")), Nothing, drSql("num_of_terminals"))
                _terminalids = IIf(IsDBNull(drSql("terminal_ids")), Nothing, drSql("terminal_ids"))
                _terminalinfodesc = IIf(IsDBNull(drSql("terminal_info_desc")), Nothing, drSql("terminal_info_desc"))

                _numofprinters = IIf(IsDBNull(drSql("num_of_printers")), Nothing, drSql("num_of_printers"))
                _printerids = IIf(IsDBNull(drSql("printer_ids")), Nothing, drSql("printer_ids"))
                _printerinfodesc = IIf(IsDBNull(drSql("printer_info_desc")), Nothing, drSql("printer_info_desc"))
                _numofpc = IIf(IsDBNull(drSql("num_of_pc")), Nothing, drSql("num_of_pc"))
                _pcids = IIf(IsDBNull(drSql("pc_ids")), Nothing, drSql("pc_ids"))
                _pcinfodesc = IIf(IsDBNull(drSql("pc_info_desc")), Nothing, drSql("pc_info_desc"))
                _numofnetdrops = IIf(IsDBNull(drSql("num_of_net_drops")), Nothing, drSql("num_of_net_drops"))
                _netdropids = IIf(IsDBNull(drSql("net_drop_ids")), Nothing, drSql("net_drop_ids"))
                _netdropsinfodesc = IIf(IsDBNull(drSql("net_drops_info_desc")), Nothing, drSql("net_drops_info_desc"))
                _numofother = IIf(IsDBNull(drSql("num_of_other")), Nothing, drSql("num_of_other"))

                _otherids = IIf(IsDBNull(drSql("other_ids")), Nothing, drSql("other_ids"))
                _otherinfodesc = IIf(IsDBNull(drSql("other_info_desc")), Nothing, drSql("other_info_desc"))

                '_servicetype = IIf(IsDBNull(drSql("servicetype")), Nothing, drSql("servicetype"))
                '_statuscd = IIf(IsDBNull(drSql("statuscd")), Nothing, drSql("statuscd"))
                '_statusdesc = IIf(IsDBNull(drSql("statusdesc")), Nothing, drSql("statusdesc"))

                '_lastchangedate = IIf(IsDBNull(drSql("lastchangedate")), Nothing, drSql("lastchangedate"))
                '_lastentertechnum = IIf(IsDBNull(drSql("lastentertechnum")), Nothing, drSql("lastentertechnum"))
                '_lastenteredname = IIf(IsDBNull(drSql("lastenteredname")), Nothing, drSql("lastenteredname"))
                '_clientexpect = IIf(IsDBNull(drSql("clientexpect")), Nothing, drSql("clientexpect"))

                _ActivityDesc = IIf(IsDBNull(drSql("ActivityDesc")), Nothing, drSql("ActivityDesc"))
                _TotalAcknowledgeTime = IIf(IsDBNull(drSql("TotalAcknowledgeTime")), Nothing, drSql("TotalAcknowledgeTime"))
                '_categorynum = IIf(IsDBNull(drSql("categorynum")), Nothing, drSql("categorynum"))
                '_typenum = IIf(IsDBNull(drSql("typenum")), Nothing, drSql("typenum"))
                '_itemnum = IIf(IsDBNull(drSql("itemnum")), Nothing, drSql("itemnum"))
                _Sendemail = IIf(IsDBNull(drSql("Sendemail")), Nothing, drSql("Sendemail"))
                _Surveyexists = IIf(IsDBNull(drSql("Surveyexists")), Nothing, drSql("Surveyexists"))


            End While

            '_RequestItems = New RequestItems(CType(_AccountRequestNum, Integer)).RequestItems

            'If Not _receiver_client_num Is Nothing Then

            '    _OtherAccounts = New ClientAccounts(_receiver_client_num).ClientAccounts

            'End If

            connSql.Close()

        Catch ex As Exception
            Throw ex

        End Try

    End Sub
    Public Property requestnum As Integer
        Get
            Return _requestnum
        End Get

        Set(value As Integer)
            _requestnum = value
        End Set
    End Property

    Public Property clientnum As Integer
        Get
            Return _clientnum
        End Get

        Set(value As Integer)
            _clientnum = value
        End Set
    End Property
    Public Property firstname As String
        Get
            Return _FirstName
        End Get
        Set(value As String)
            _FirstName = value
        End Set
    End Property

    Public Property LastName As String
        Get
            Return _LastName
        End Get
        Set(value As String)
            _LastName = value
        End Set
    End Property

    Public Property entrydate As String
        Get
            Return _entrydate
        End Get
        Set(value As String)
            _entrydate = value
        End Set

    End Property
    Public Property closedate As String
        Get
            Return _closedate
        End Get
        Set(value As String)
            _closedate = value
        End Set
    End Property
    Public Property requesttype As String
        Get
            Return _requesttype
        End Get

        Set(value As String)
            _requesttype = value
        End Set
    End Property
    Public Property categorycd As String
        Get
            Return _categorycd
        End Get

        Set(value As String)
            _categorycd = value
        End Set
    End Property
    Public Property typecd As String
        Get
            Return _typecd
        End Get
        Set(value As String)
            _typecd = value
        End Set
    End Property
    Public Property itemcd As String
        Get
            Return _itemcd
        End Get
        Set(value As String)
            _itemcd = value
        End Set
    End Property
    Public Property priority As String
        Get
            Return _priority
        End Get
        Set(value As String)
            _priority = value
        End Set

    End Property
    Public Property groupassigntime As String
        Get
            Return _groupassigntime
        End Get
        Set(value As String)
            _groupassigntime = value
        End Set
    End Property
    Public Property assignedgroupnum As String
        Get
            Return _assignedgroupnum

        End Get
        Set(value As String)
            _assignedgroupnum = value
        End Set
    End Property

    Public Property currentgroupnum As String
        Get
            Return _currentgroupnum
        End Get
        Set(value As String)
            _currentgroupnum = value
        End Set
    End Property
    Public Property currentgroupname As String
        Get
            Return _currentgroupname
        End Get
        Set(value As String)
            _currentgroupname = value
        End Set
    End Property

    Public Property techassigntime As String
        Get
            Return _techassigntime
        End Get
        Set(value As String)
            _techassigntime = value
        End Set
    End Property
    Public Property assignedtechnum As String
        Get
            Return _assignedtechnum
        End Get
        Set(value As String)
            _assignedtechnum = value
        End Set

    End Property
    Public Property assignedtechname As String
        Get
            Return _assignedtechname
        End Get
        Set(value As String)
            _assignedtechname = value
        End Set

    End Property

    Public Property currenttechnum As String
        Get
            Return _currenttechnum
        End Get

        Set(value As String)
            _currenttechnum = value
        End Set

    End Property
    Public Property currenttechname As String
        Get
            Return _currenttechname
        End Get

        Set(value As String)
            _currenttechname = value
        End Set

    End Property

    Public Property acknowledgetime As String
        Get
            Return _acknowledgetime
        End Get
        Set(value As String)
            _acknowledgetime = value
        End Set
    End Property
    Public Property currentacktime As String
        Get
            Return _currentacktime
        End Get
        Set(value As String)
            _currentacktime = value
        End Set
    End Property
    Public Property assettag As String
        Get
            Return _assettag
        End Get
        Set(value As String)
            _assettag = value
        End Set
    End Property
    Public Property platform As String
        Get
            Return _platform
        End Get
        Set(value As String)
            _platform = value
        End Set


    End Property

    Public Property resolutioncd As String
        Get
            Return _resolutioncd
        End Get
        Set(value As String)
            _resolutioncd = value
        End Set


    End Property
    Public Property entrytechnum As String
        Get
            Return _entrytechnum
        End Get

        Set(value As String)
            _entrytechnum = value
        End Set

    End Property
    Public Property entrytechname As String
        Get
            Return _entrytechname
        End Get

        Set(value As String)
            _entrytechname = value
        End Set

    End Property
    <DisplayName("Phone")> Property Phone As String
        Get
            Return _Phone
        End Get
        Set(value As String)
            _Phone = value
        End Set
    End Property

    <DisplayName("EMail")> Property EMail As String
        Get
            Return _EMail
        End Get
        Set(value As String)
            _EMail = value
        End Set
    End Property
    <DisplayName("Facility Code")> Property FacilityCd As String
        Get
            Return _FacilityCd
        End Get
        Set(value As String)
            _FacilityCd = value
        End Set
    End Property

    <DisplayName("Building Code")> Property BuildingCd As String
        Get
            Return _BuildingCd
        End Get
        Set(value As String)
            _BuildingCd = value
        End Set
    End Property
    <DisplayName("Building Name")> Property BuildingName As String
        Get
            Return _BuildingName
        End Get
        Set(value As String)
            _BuildingName = value
        End Set
    End Property

    <DisplayName("Office")> Property Office As String
        Get
            Return _Office
        End Get
        Set(value As String)
            _Office = value
        End Set
    End Property
    <DisplayName("Floor")> Property Floor As String
        Get
            Return _Floor
        End Get
        Set(value As String)
            _Floor = value
        End Set
    End Property
    <DisplayName("Title")> Property Title As String
        Get
            Return _Title
        End Get
        Set(value As String)
            _Title = value
        End Set
    End Property

    <DisplayName("Address Line 1")> Property Address1 As String
        Get
            Return _Address1
        End Get
        Set(value As String)
            _Address1 = value
        End Set
    End Property
    <DisplayName("Address Line 2")> Property Address2 As String
        Get
            Return _Address2
        End Get
        Set(value As String)
            _Address2 = value
        End Set
    End Property
    <DisplayName("City")> Property City As String
        Get
            Return _City
        End Get
        Set(value As String)
            _City = value
        End Set
    End Property
    <DisplayName("State")> Property State As String
        Get
            Return _State
        End Get
        Set(value As String)
            _State = value
        End Set
    End Property
    <DisplayName("Zip")> Property Zip As String
        Get
            Return _Zip
        End Get
        Set(value As String)
            _Zip = value
        End Set
    End Property
    <DisplayName("Department Code")> Property DepartmentCd As String
        Get
            Return _DepartmentCd
        End Get
        Set(value As String)
            _DepartmentCd = value
        End Set
    End Property
    <DisplayName("Department Name")> Property DepartmentName As String
        Get
            Return _DepartmentName
        End Get
        Set(value As String)
            _DepartmentName = value
        End Set
    End Property

    <DisplayName("Facility Name")> Property FacilityName As String
        Get
            Return _FacilityName
        End Get
        Set(value As String)
            _FacilityName = value
        End Set
    End Property



    Public Property alternatecontactname As String
        Get
            Return _alternatecontactname
        End Get

        Set(value As String)
            _alternatecontactname = value
        End Set

    End Property
    Public Property alternatecontactphone As String
        Get
            Return _alternatecontactphone
        End Get

        Set(value As String)
            _alternatecontactphone = value
        End Set

    End Property
    Public Property shortdesc As String
        Get
            Return _shortdesc
        End Get
        Set(value As String)
            _shortdesc = value
        End Set

    End Property
    Public Property proxyonpc As String
        Get
            Return _proxyonpc
        End Get
        Set(value As String)
            _proxyonpc = value
        End Set

    End Property
    Public Property ipaddress As String
        Get
            Return _ipaddress
        End Get

        Set(value As String)
            _ipaddress = value
        End Set

    End Property
    Public Property completiondate As String
        Get
            Return _completiondate
        End Get
        Set(value As String)
            _completiondate = value
        End Set

    End Property
    Public Property servicedesc As String
        Get
            Return _servicedesc
        End Get
        Set(value As String)
            _servicedesc = value
        End Set

    End Property
    Public Property businessdes As String
        Get
            Return _businessdes
        End Get
        Set(value As String)
            _businessdes = value
        End Set

    End Property

    Public Property impactdesc As String
        Get
            Return _impactdesc
        End Get

        Set(value As String)
            _impactdesc = value
        End Set
    End Property

    Public Property numofterminals As String
        Get
            Return _numofterminals
        End Get

        Set(value As String)
            _numofterminals = value
        End Set

    End Property
    Public Property terminalids As String
        Get
            Return _terminalids
        End Get

        Set(value As String)
            _terminalids = value
        End Set

    End Property
    Public Property terminalinfodesc As String
        Get
            Return _terminalinfodesc
        End Get
        Set(value As String)
            _terminalinfodesc = value
        End Set

    End Property
    Public Property numofprinters As String
        Get
            Return _numofprinters
        End Get
        Set(value As String)
            _numofprinters = value
        End Set

    End Property
    Public Property printerids As String
        Get
            Return _printerids
        End Get
        Set(value As String)
            _printerids = value
        End Set
    End Property
    Public Property printerinfodesc As String
        Get
            Return _printerinfodesc
        End Get
        Set(value As String)
            _printerinfodesc = value
        End Set
    End Property
    Public Property numofpc As String
        Get
            Return _numofpc
        End Get
        Set(value As String)
            _numofpc = value
        End Set
    End Property
    Public Property pcids As String
        Get
            Return _pcids
        End Get
        Set(value As String)
            _pcids = value
        End Set
    End Property
    Public Property pcinfodesc As String
        Get
            Return _pcinfodesc
        End Get
        Set(value As String)
            _pcinfodesc = value
        End Set
    End Property
    Public Property numofnetdrops As String
        Get
            Return _numofnetdrops
        End Get
        Set(value As String)
            _numofnetdrops = value
        End Set

    End Property
    Public Property netdropids As String
        Get
            Return _netdropids
        End Get
        Set(value As String)
            _netdropids = value
        End Set

    End Property
    Public Property netdropsinfodesc As String
        Get
            Return _netdropsinfodesc
        End Get
        Set(value As String)
            _netdropsinfodesc = value
        End Set

    End Property
    Public Property numofother As String
        Get
            Return _numofother
        End Get
        Set(value As String)
            _numofother = value
        End Set

    End Property
    Public Property otherids As String
        Get
            Return _otherids
        End Get
        Set(value As String)
            _otherids = value
        End Set

    End Property
    Public Property otherinfodesc As String
        Get
            Return _otherinfodesc
        End Get
        Set(value As String)
            _otherinfodesc = value
        End Set

    End Property
    Public Property servicetype As String
        Get
            Return _servicetype
        End Get
        Set(value As String)
            _servicetype = value
        End Set

    End Property
    Public Property statuscd As String
        Get
            Return _statuscd
        End Get
        Set(value As String)
            _statuscd = value
        End Set

    End Property
    Public Property lastchangedate As String
        Get
            Return _lastchangedate
        End Get
        Set(value As String)
            _lastchangedate = value
        End Set

    End Property
    Public Property lastentertechnum As String
        Get
            Return _lastentertechnum
        End Get
        Set(value As String)
            _lastentertechnum = value
        End Set

    End Property
    Public Property lastenteredname As String
        Get
            Return _lastenteredname
        End Get
        Set(value As String)
            _lastenteredname = value
        End Set

    End Property
    Public Property clientexpect As String
        Get
            Return _clientexpect
        End Get
        Set(value As String)
            _clientexpect = value
        End Set

    End Property
    Public Property statusdesc As String
        Get
            Return _statusdesc
        End Get
        Set(value As String)
            _statusdesc = value
        End Set

    End Property
    Public Property ActivityDesc As String
        Get
            Return _ActivityDesc
        End Get
        Set(value As String)
            _ActivityDesc = value
        End Set

    End Property
    Public Property categorynum As String
        Get
            Return _categorynum
        End Get
        Set(value As String)
            _categorynum = value
        End Set

    End Property
    Public Property typenum As String
        Get
            Return _typenum
        End Get
        Set(value As String)
            _typenum = value
        End Set

    End Property
    Public Property itemnum As String
        Get
            Return _itemnum
        End Get
        Set(value As String)
            _itemnum = value
        End Set

    End Property
    Public Property quickticket As String
        Get
            Return _quickticket
        End Get
        Set(value As String)
            _quickticket = value
        End Set

    End Property
    Public Property ntlogin As String
        Get
            Return _ntlogin
        End Get
        Set(value As String)
            _ntlogin = value
        End Set

    End Property
    Public Property empTypeCd As String
        Get
            Return _empTypeCd
        End Get
        Set(value As String)
            _empTypeCd = value
        End Set

    End Property

    Public Property AssociatedRequests As String
        Get
            Return _AssociatedRequests
        End Get
        Set(value As String)
            _AssociatedRequests = value
        End Set

    End Property
    Public Property TotalAcknowledgeTime As String
        Get
            Return _TotalAcknowledgeTime
        End Get
        Set(value As String)
            _TotalAcknowledgeTime = value
        End Set

    End Property

    Public Property Sendemail As String
        Get
            Return _Sendemail
        End Get
        Set(value As String)
            _Sendemail = value
        End Set

    End Property

    Public Property Surveyexists As String
        Get
            Return _Surveyexists
        End Get
        Set(value As String)
            _Surveyexists = value
        End Set

    End Property



End Class
