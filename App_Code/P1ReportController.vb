﻿Imports Microsoft.VisualBasic
Imports App_Code
Imports System.ComponentModel
Imports System.Collections.Generic
Imports System.Web.HttpContext

Public Class P1ReportController
    Private _P1ReportV As New List(Of P1ReportViewer)
    Private _P1Detail As New List(Of P1DetailReportViewer)
    Private _dataConn As String
    Public Sub GetP1Totals(ByRef sType As String, ByRef sDate As String)

        Using SqlConnection As New SqlConnection(HttpContext.Current.Session("CSCConn"))
            Dim Cmd As New SqlCommand()
            Cmd.CommandText = "SelP1Totals"
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.Parameters.AddWithValue("@TotalType", sType)
            Cmd.Parameters.AddWithValue("@totalyear", sDate)

            Cmd.Connection = SqlConnection

            SqlConnection.Open()
            Using Cmd
                Using dr = Cmd.ExecuteReader()

                    If dr.HasRows Then

                        Do While dr.Read
                            Dim _ReportV As New P1ReportViewer
                            'TotalCount  entryyear   group_name EntryMonthName HourofDay EntryDayOfWeek

                            _ReportV.TotalCount = IIf(IsDBNull(dr("TotalCount")), Nothing, dr("TotalCount"))
                            _ReportV.entryyear = IIf(IsDBNull(dr("entryyear")), Nothing, dr("entryyear"))

                            _ReportV.group_name = IIf(IsDBNull(dr("group_name")), Nothing, dr("group_name"))
                            _ReportV.EntryMonthName = IIf(IsDBNull(dr("EntryMonthName")), Nothing, dr("EntryMonthName"))

                            _ReportV.HourofDay = IIf(IsDBNull(dr("HourofDay")), Nothing, dr("HourofDay"))
                            _ReportV.EntryDayOfWeek = IIf(IsDBNull(dr("EntryDayOfWeek")), Nothing, dr("EntryDayOfWeek"))

                            _ReportV.DisplayField = IIf(IsDBNull(dr("DisplayField")), Nothing, dr("DisplayField"))
                            _P1ReportV.Add(_ReportV)
                        Loop

                    End If
                End Using

            End Using
        End Using

    End Sub
    Public Sub GetP1Details(ByRef sType As String, ByRef sDate As String, ByRef sValue As String)
        Using SqlConnection As New SqlConnection(HttpContext.Current.Session("CSCConn"))
            Dim Cmd As New SqlCommand()
            Cmd.CommandText = "SelP1DetailByType"
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.Parameters.AddWithValue("@TotalType", sType)
            Cmd.Parameters.AddWithValue("@totalyear", sDate)
            Cmd.Parameters.AddWithValue("@totalValue", sValue)

            Cmd.Connection = SqlConnection

            SqlConnection.Open()
            Using Cmd
                Using dr = Cmd.ExecuteReader()

                    If dr.HasRows Then

                        Do While dr.Read
                            Dim _ReportV As New P1DetailReportViewer
                            'TotalCount  entryyear   group_name EntryMonthName HourofDay EntryDayOfWeek

                            _ReportV.RequestNum = IIf(IsDBNull(dr("Request_num")), Nothing, dr("Request_num"))
                            _ReportV.EntryDate = IIf(IsDBNull(dr("Entry_date")), Nothing, dr("Entry_date"))
                            _ReportV.entryyear = IIf(IsDBNull(dr("entryyear")), Nothing, dr("entryyear"))

                            _ReportV.group_name = IIf(IsDBNull(dr("group_name")), Nothing, dr("group_name"))
                            _ReportV.EntryMonthName = IIf(IsDBNull(dr("EntryMonthName")), Nothing, dr("EntryMonthName"))

                            _ReportV.HourofDay = IIf(IsDBNull(dr("HourofDay")), Nothing, dr("HourofDay"))
                            _ReportV.EntryDayOfWeek = IIf(IsDBNull(dr("EntryDayOfWeek")), Nothing, dr("EntryDayOfWeek"))

                            _ReportV.CategoryCd = IIf(IsDBNull(dr("category_cd")), Nothing, dr("category_cd"))
                            _ReportV.TypeCd = IIf(IsDBNull(dr("type_cd")), Nothing, dr("type_cd"))

                            _P1Detail.Add(_ReportV)
                        Loop

                    End If
                End Using

            End Using
        End Using

    End Sub
    Public Function getP1ByDate() As List(Of P1ReportViewer)
        Return _P1ReportV

    End Function
    Public Function getP1Details() As List(Of P1DetailReportViewer)
        Return _P1Detail
    End Function
End Class
