﻿Imports Microsoft.VisualBasic

Public Class PageControls
    Property ctrl As Control
    Property ctrlList As New List(Of Control)



    Public Sub New(ByRef ctrl As Control)

        _ctrl = ctrl



    End Sub

    Public Sub New()


    End Sub


    Public Function FindControlById(ByRef ParentCtrl As Control, ByRef SearchCtrl As String) As Control
        If ParentCtrl.ID = SearchCtrl Then

            Return ParentCtrl

        End If


        For Each c As Control In ParentCtrl.Controls

            Dim FoundControl = FindControlById(c, SearchCtrl)

            If FoundControl IsNot Nothing Then Return FoundControl


            FindControlById(c, SearchCtrl)

        Next

        Return Nothing




    End Function
    Public Sub AddToContolsList(ByVal ctrl As Control)

        _ctrlList.Add(ctrl)
    End Sub


    Public Sub DisableControls(ByRef ctrl As Control)

        For Each c As Control In ctrl.Controls


            If _ctrlList.IndexOf(c) > 0 Then





                If TypeOf c Is TextBox Then

                    Dim txt As TextBox = DirectCast(c, TextBox)
                    txt.ReadOnly = True

                    txt.Attributes.Remove("CssClass")


                End If

                If TypeOf c Is DropDownList Then

                    Dim ddl As DropDownList = DirectCast(c, DropDownList)


                    ddl.Enabled = False

                End If


                If TypeOf c Is RadioButtonList Then

                    Dim rbl As RadioButtonList = DirectCast(c, RadioButtonList)

                    rbl.Enabled = False

                End If


                If TypeOf c Is CheckBoxList Then

                    Dim cbl As CheckBoxList = DirectCast(c, CheckBoxList)

                    For Each li As ListItem In cbl.Items

                        li.Enabled = False
                    Next

                End If


            End If


            DisableControls(c)

        Next



    End Sub

    Public Sub RemoveCssClass(ByRef ctrl As Control, ByVal cssClass As String)

        For Each c As Control In ctrl.Controls


            If TypeOf c Is WebControl Then

                Dim wc As WebControl = DirectCast(c, WebControl)

                If wc.CssClass.ToString.IndexOf(cssClass) > -1 Then

                    wc.CssClass = wc.CssClass.Replace(cssClass, "")

                End If

            End If



            RemoveCssClass(c, cssClass)

        Next



    End Sub


    Public Sub EnableControls(ByRef ctrl As Control)

        For Each c As Control In ctrl.Controls

            If TypeOf c Is TextBox Then

                Dim txt As TextBox = DirectCast(c, TextBox)
                txt.ReadOnly = False




            End If

            If TypeOf c Is DropDownList Then

                Dim ddl As DropDownList = DirectCast(c, DropDownList)


                ddl.Enabled = True

            End If


            If TypeOf c Is RadioButtonList Then

                Dim rbl As RadioButtonList = DirectCast(c, RadioButtonList)

                rbl.Enabled = True

            End If


            If TypeOf c Is CheckBoxList Then

                Dim cbl As CheckBoxList = DirectCast(c, CheckBoxList)

                For Each li As ListItem In cbl.Items

                    li.Enabled = True
                Next

            End If


            EnableControls(c)

        Next



    End Sub
End Class
