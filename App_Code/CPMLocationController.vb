﻿Imports Microsoft.VisualBasic
Imports App_Code
Imports System.ComponentModel
Imports System.Web.HttpContext

Public Class CPMLocationController
    Property Group_Num As String
    Property Group_name As String
    Property GroupTypeCd As String
    Property GroupID As String
    Property LocationTypeCd As String
    Property SpecialtyType As String
    Property ShortName As String
    Property CostCenter As String
    Property EffectiveDate As String
    Property OfficeNPI As String
    Property GroupNPI As String
    Property taxonomy As String
    Property Address1 As String
    Property Address2 As String
    Property City As String
    Property State As String
    Property Zip As String
    Property Phone As String
    Property Fax As String
    Property CLIA As String
    Property ValidationDate As String
    Property DateDeactivated As String
    Property CredentialedName As String
    Property Comments As String

    Private _dataConn As String
    Public Sub New()

    End Sub

    Public Sub New(ByVal dataConn As String)
        If dataConn Is Nothing Then
            dataConn = HttpContext.Current.Session("EmployeeConn")
        End If

        _dataConn = dataConn
    End Sub


    Public Function GetCPMLocation(ByVal sGroupnum As Integer) As CPMLocation
        Dim Cpmlocation As New CPMLocation
        Using sqlConn As New SqlConnection(_dataConn)

            Dim Cmd As New SqlCommand()
            Cmd.CommandText = "SelCPMLocationByGroupNum"
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.Parameters.AddWithValue("@groupnum", sGroupnum)
            Cmd.Connection = sqlConn



            sqlConn.Open()

            Using Cmd

                Using dr = Cmd.ExecuteReader()

                    If dr.HasRows Then

                        Do While dr.Read


                            Try
                                Cpmlocation.Group_Num = IIf(IsDBNull(dr("Group_Num")), Nothing, dr("Group_Num"))
                                Cpmlocation.Group_name = IIf(IsDBNull(dr("Group_name")), Nothing, dr("Group_name"))
                                Cpmlocation.GroupTypeCd = IIf(IsDBNull(dr("GroupTypeCd")), Nothing, dr("GroupTypeCd"))
                                Cpmlocation.GroupID = IIf(IsDBNull(dr("GroupID")), Nothing, dr("GroupID"))
                                Cpmlocation.LocationTypeCd = IIf(IsDBNull(dr("LocationTypeCd")), Nothing, dr("LocationTypeCd"))
                                Cpmlocation.SpecialtyType = IIf(IsDBNull(dr("SpecialtyType")), Nothing, dr("SpecialtyType"))
                                Cpmlocation.ShortName = IIf(IsDBNull(dr("ShortName")), Nothing, dr("ShortName"))
                                Cpmlocation.CostCenter = IIf(IsDBNull(dr("CostCenter")), Nothing, dr("CostCenter"))
                                Cpmlocation.EffectiveDate = IIf(IsDBNull(dr("EffectiveDate")), Nothing, dr("EffectiveDate"))
                                Cpmlocation.OfficeNPI = IIf(IsDBNull(dr("OfficeNPI")), Nothing, dr("OfficeNPI"))
                                Cpmlocation.GroupNPI = IIf(IsDBNull(dr("GroupNPI")), Nothing, dr("GroupNPI"))
                                Cpmlocation.taxonomy = IIf(IsDBNull(dr("taxonomy")), Nothing, dr("taxonomy"))
                                Cpmlocation.Address1 = IIf(IsDBNull(dr("Address1")), Nothing, dr("Address1"))
                                Cpmlocation.Address2 = IIf(IsDBNull(dr("Address2")), Nothing, dr("Address2"))
                                Cpmlocation.City = IIf(IsDBNull(dr("City")), Nothing, dr("City"))
                                Cpmlocation.State = IIf(IsDBNull(dr("State")), Nothing, dr("State"))
                                Cpmlocation.Zip = IIf(IsDBNull(dr("Zip")), Nothing, dr("Zip"))
                                Cpmlocation.Phone = IIf(IsDBNull(dr("Phone")), Nothing, dr("Phone"))
                                Cpmlocation.Fax = IIf(IsDBNull(dr("Fax")), Nothing, dr("Fax"))
                                Cpmlocation.CLIA = IIf(IsDBNull(dr("CLIA")), Nothing, dr("CLIA"))
                                Cpmlocation.ValidationDate = IIf(IsDBNull(dr("ValidationDate")), Nothing, dr("ValidationDate"))
                                Cpmlocation.DateDeactivated = IIf(IsDBNull(dr("DateDeactivated")), Nothing, dr("DateDeactivated"))
                                Cpmlocation.CredentialedName = IIf(IsDBNull(dr("CredentialedName")), Nothing, dr("CredentialedName"))
                                Cpmlocation.Comments = IIf(IsDBNull(dr("Comments")), Nothing, dr("Comments"))



                            Catch ex As Exception

                            End Try

                        Loop

                    End If

                End Using

            End Using

            sqlConn.Close()

        End Using
        Return Cpmlocation
    End Function

    'Property Group_Num As String
    '    Get
    '        Return _Group_Num
    '    End Get
    '    Set(value As String)
    '        _Group_Num = value
    '    End Set
    'End Property


    'Property Group_name As String
    '    Get
    '        Return _Group_name
    '    End Get
    '    Set(value As String)
    '        _Group_name = value
    '    End Set
    'End Property


    'Property GroupTypeCd As String
    '    Get
    '        Return _GroupTypeCd
    '    End Get
    '    Set(value As String)
    '        _GroupTypeCd = value
    '    End Set
    'End Property


    'Property GroupID As String
    '    Get
    '        Return _GroupID
    '    End Get
    '    Set(value As String)
    '        _GroupID = value
    '    End Set
    'End Property


    'Property LocationTypeCd As String
    '    Get
    '        Return _LocationTypeCd
    '    End Get
    '    Set(value As String)
    '        _LocationTypeCd = value
    '    End Set
    'End Property


    'Property SpecialtyType As String
    '    Get
    '        Return _SpecialtyType
    '    End Get
    '    Set(value As String)
    '        _SpecialtyType = value
    '    End Set
    'End Property


    'Property ShortName As String
    '    Get
    '        Return _ShortName
    '    End Get
    '    Set(value As String)
    '        _ShortName = value
    '    End Set
    'End Property


    'Property CostCenter As String
    '    Get
    '        Return _CostCenter
    '    End Get
    '    Set(value As String)
    '        _CostCenter = value
    '    End Set
    'End Property


    'Property EffectiveDate As String
    '    Get
    '        Return _EffectiveDate
    '    End Get
    '    Set(value As String)
    '        _EffectiveDate = value
    '    End Set
    'End Property


    'Property OfficeNPI As String
    '    Get
    '        Return _OfficeNPI
    '    End Get
    '    Set(value As String)
    '        _OfficeNPI = value
    '    End Set
    'End Property


    'Property GroupNPI As String
    '    Get
    '        Return _GroupNPI
    '    End Get
    '    Set(value As String)
    '        _GroupNPI = value
    '    End Set
    'End Property


    'Property taxonomy As String
    '    Get
    '        Return _taxonomy
    '    End Get
    '    Set(value As String)
    '        _taxonomy = value
    '    End Set
    'End Property


    'Property Address1 As String
    '    Get
    '        Return _Address1
    '    End Get
    '    Set(value As String)
    '        _Address1 = value
    '    End Set
    'End Property


    'Property Address2 As String
    '    Get
    '        Return _Address2
    '    End Get
    '    Set(value As String)
    '        _Address2 = value
    '    End Set
    'End Property


    'Property City As String
    '    Get
    '        Return _City
    '    End Get
    '    Set(value As String)
    '        _City = value
    '    End Set
    'End Property


    'Property State As String
    '    Get
    '        Return _State
    '    End Get
    '    Set(value As String)
    '        _State = value
    '    End Set
    'End Property


    'Property Zip As String
    '    Get
    '        Return _Zip
    '    End Get
    '    Set(value As String)
    '        _Zip = value
    '    End Set
    'End Property


    'Property Phone As String
    '    Get
    '        Return _Phone
    '    End Get
    '    Set(value As String)
    '        _Phone = value
    '    End Set
    'End Property


    'Property Fax As String
    '    Get
    '        Return _Fax
    '    End Get
    '    Set(value As String)
    '        _Fax = value
    '    End Set
    'End Property


    'Property CLIA As String
    '    Get
    '        Return _CLIA
    '    End Get
    '    Set(value As String)
    '        _CLIA = value
    '    End Set
    'End Property


    'Property ValidationDate As String
    '    Get
    '        Return _ValidationDate
    '    End Get
    '    Set(value As String)
    '        _ValidationDate = value
    '    End Set
    'End Property


    'Property DateDeactivated As String
    '    Get
    '        Return _DateDeactivated
    '    End Get
    '    Set(value As String)
    '        _DateDeactivated = value
    '    End Set
    'End Property


    'Property CredentialedName As String
    '    Get
    '        Return _CredentialedName
    '    End Get
    '    Set(value As String)
    '        _CredentialedName = value
    '    End Set
    'End Property


    'Property Comments As String
    '    Get
    '        Return _Comments
    '    End Get
    '    Set(value As String)
    '        _Comments = value
    '    End Set
    'End Property


End Class
