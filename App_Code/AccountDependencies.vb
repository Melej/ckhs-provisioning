﻿Imports Microsoft.VisualBasic
Imports System.Web
Imports App_Code

Public Class AccountDependencies
    Private _AccountDependencies As New List(Of AccountDependency)
    Private _dataConn As String
    Private _dtAppDeps As DataTable


    Public Sub New()
        Dim conn As New SetDBConnection()

        _dataConn = conn.EmployeeConn


        Using sqlConn As New SqlConnection(_dataConn)



            Dim Cmd As New SqlCommand()
            Cmd.CommandText = "SelAppDependecy"
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.Connection = sqlConn
           


            sqlConn.Open()

            Using Cmd

                Using RDR = Cmd.ExecuteReader()

                    If RDR.HasRows Then

                        Do While RDR.Read

                            Dim AccountDep As New AccountDependency


                            AccountDep.AppBase = IIf(IsDBNull(RDR.Item("AppBase")), Nothing, RDR.Item("AppBase"))
                            AccountDep.AppCreateDep = IIf(IsDBNull(RDR.Item("AppCreateDep")), Nothing, RDR.Item("AppCreateDep"))
                            AccountDep.AppCreateDepBase = IIf(IsDBNull(RDR.Item("AppCreateDepBase")), Nothing, RDR.Item("AppCreateDepBase"))
                            AccountDep.Hierarchy = IIf(IsDBNull(RDR.Item("Hierarchy")), Nothing, RDR.Item("Hierarchy"))


                            _AccountDependencies.Add(AccountDep)
                        Loop

                    End If

                End Using

            End Using

            sqlConn.Close()

        End Using









     
    End Sub


    Public ReadOnly Property Dependencies As List(Of AccountDependency)
        Get
            Return _AccountDependencies

        End Get
    End Property

End Class
Public Class AccountDependency
    Public Property AppBase As String
    Public Property AppNum As String
    Public Property AppCreateDep As String
    Public Property AppCreateDepBase As String
    Public Property Hierarchy As String


End Class
