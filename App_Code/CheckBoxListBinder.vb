﻿Imports Microsoft.VisualBasic
Imports App_Code

Public Class CheckBoxListBinder
    Dim _cblCommand As CommandsSqlAndOleDb
    Dim _cblControl As CheckBoxList
    Dim _sDataConn As String


    Public Sub New()



    End Sub
    Public Sub New(ByRef cblControl As CheckBoxList)

        _cblControl = cblControl


    End Sub

    Public Function SetDDLCommand(ByVal sProc As String) As CommandsSqlAndOleDb
        _cblCommand = New CommandsSqlAndOleDb(sProc)

        Return _cblCommand

    End Function

    Public Sub AddSQlParam(ByVal sParam As String, ByVal sParamValue As String)

        _cblCommand.AddSqlProcParameter(sParam, sParamValue, SqlDbType.NVarChar)


    End Sub
    Public Sub New(ByRef cblControl As CheckBoxList, ByVal sProc As String, ByVal sDataConn As String)

        _cblControl = cblControl
        _sDataConn = sDataConn
        _cblCommand = New CommandsSqlAndOleDb(sProc, _sDataConn)

    End Sub

    Public Sub BindData(ByRef cblControl As CheckBoxList, ByVal sProc As String, ByVal sValue As String, ByVal sText As String, ByVal sDataConn As String)

        Dim dt As New DataTable
        Dim thisdata As New CommandsSqlAndOleDb(sProc, sDataConn)
        dt = thisdata.GetSqlDataTable

        cblControl.Items.Clear()

        cblControl.DataSource = dt


        cblControl.DataSource = dt
        cblControl.DataValueField = sValue
        cblControl.DataTextField = sText

        cblControl.DataBind()



    End Sub
    Public Sub BindData(ByVal cblValue As String, ByVal cblText As String)

        Dim dt As New DataTable

        dt = _cblCommand.GetSqlDataTable

        _cblControl.Items.Clear()

        _cblControl.DataSource = dt

        _cblControl.DataValueField = cblValue
        _cblControl.DataTextField = cblText

        _cblControl.DataBind()



    End Sub

    Public Sub AddDDLCriteria(ByVal sParameter As String, ByVal sValue As String, ByVal sqlDataType As SqlDbType)
        _cblCommand.AddSqlProcParameter(sParameter, sValue, sqlDataType)

    End Sub
End Class
