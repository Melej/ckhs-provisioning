﻿Imports Microsoft.VisualBasic

Public Class RequestEntry
    Property RequestNum As Integer
    Property SubmitterClientNum As Integer
    Property RequestorClientNum As Integer
    Property Priority As Integer
    Property DateEntered As DateTime
    Property DateClosed As DateTime
    Property RequestType As String
    Property AssignedGroupNum As Integer
    Property DateTimeGroupAssigned As DateTime
    Property CurrentGroupNum As Integer
    Property AssignedTechClientNum As Integer
    Property DateTimeTechAssigned As DateTime
    Property CurrentTechClientNum As DateTime
    Property CurrentDateTimeAck As DateTime
    Property DateAcknowledged As DateTime
    Property AlternativeContact As String
    Property AlternativePhone As String
    Property Status As String


End Class

