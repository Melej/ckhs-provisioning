﻿Imports Microsoft.VisualBasic
Imports System.ComponentModel


Public Class VaccineRequest
    Private _DataConn As String
    Private _client_num As String
    Private _VaccineLotNum As String
    Private _account_request_type As String
    Private _emp_type_cd As String
    Private _VaccineDate As String
    Private _EnteryDate As String
    Private _VaccineOutID As String
    Private _first_name As String
    Private _last_name As String
    Private _mi As String
    Private _siemens_emp_num As String
    Private _security_type_cd As String
    Private _security_desc As String
    Private _entity_cd As String
    Private _department_cd As String
    Private _department_name As String
    Private _facility_cd As String
    Private _facility_name As String
    Private _user_position_desc As String
    Private _building_cd As String
    Private _building_name As String
    Private _floor As String
    Private _entity_name As String
    Private _AllergicEggs As String
    Private _AllergicFluShot As String
    Private _AllergicThimerosal As String
    Private _Allergiclatex As String
    Private _HistoryGBS As String
    Private _FeelSick As String
    Private _UserNumAdministered As String
    Private _InjectionSide As String
    Private _LocationSiteID As String
    Private _Comments As String
    Private _LotNumber As String
    Private _ExpirationDate As String
    Private _Manufacturer As String

    Public Sub New()

    End Sub


    Public Sub New(ByVal VaccineClient As String, ByVal DataConn As String)
        _client_num = VaccineClient
        _DataConn = DataConn
        GetValues()
    End Sub

    Private Sub GetValues()



        Dim cmdSql As New SqlCommand
        Dim connSql As New SqlConnection(_DataConn)
        Dim drSql As SqlClient.SqlDataReader



        '   connSql.ConnectionString = _DataConn

        cmdSql.CommandText = "SelClientForVaccine"
        cmdSql.CommandType = CommandType.StoredProcedure
        cmdSql.Connection = connSql
        cmdSql.Parameters.Add("@client_num", SqlDbType.Int).Value = _client_num


        connSql.Open()

        Try




            drSql = cmdSql.ExecuteReader


            While drSql.Read()


                _client_num = IIf(IsDBNull(drSql("client_num")), Nothing, drSql("client_num"))

                _VaccineLotNum = IIf(IsDBNull(drSql("VaccineLotNum")), Nothing, drSql("VaccineLotNum"))
                _VaccineDate = IIf(IsDBNull(drSql("VaccineDate")), Nothing, drSql("VaccineDate"))

                _EnteryDate = IIf(IsDBNull(drSql("EnteryDate")), Nothing, drSql("EnteryDate"))
                _VaccineOutID = IIf(IsDBNull(drSql("VaccineOutID")), Nothing, drSql("VaccineOutID"))

                _first_name = IIf(IsDBNull(drSql("first_name")), Nothing, drSql("first_name"))
                _last_name = IIf(IsDBNull(drSql("last_name")), Nothing, drSql("last_name"))
                _mi = IIf(IsDBNull(drSql("mi")), Nothing, drSql("mi"))

                _emp_type_cd = IIf(IsDBNull(drSql("emp_type_cd")), Nothing, drSql("emp_type_cd"))
                _siemens_emp_num = IIf(IsDBNull(drSql("siemens_emp_num")), Nothing, drSql("siemens_emp_num"))


                _entity_cd = IIf(IsDBNull(drSql("entity_cd")), Nothing, drSql("entity_cd"))
                _entity_name = IIf(IsDBNull(drSql("entity_name")), Nothing, drSql("entity_name"))

                _department_cd = IIf(IsDBNull(drSql("department_cd")), Nothing, drSql("department_cd"))
                _department_name = IIf(IsDBNull(drSql("department_name")), Nothing, drSql("department_name"))

                _facility_cd = IIf(IsDBNull(drSql("facility_cd")), Nothing, drSql("facility_cd"))
                _facility_name = IIf(IsDBNull(drSql("facility_name")), Nothing, drSql("facility_name"))

                _building_cd = IIf(IsDBNull(drSql("building_cd")), Nothing, drSql("building_cd"))
                _building_name = IIf(IsDBNull(drSql("building_name")), Nothing, drSql("building_name"))
                _floor = IIf(IsDBNull(drSql("floor")), Nothing, drSql("floor"))

                _user_position_desc = IIf(IsDBNull(drSql("user_position_desc")), Nothing, drSql("user_position_desc"))


                _AllergicEggs = IIf(IsDBNull(drSql("AllergicEggs")), Nothing, drSql("AllergicEggs"))
                _AllergicFluShot = IIf(IsDBNull(drSql("AllergicFluShot")), Nothing, drSql("AllergicFluShot"))
                _AllergicThimerosal = IIf(IsDBNull(drSql("AllergicThimerosal")), Nothing, drSql("AllergicThimerosal"))
                _Allergiclatex = IIf(IsDBNull(drSql("Allergiclatex")), Nothing, drSql("Allergiclatex"))

                _LotNumber = IIf(IsDBNull(drSql("LotNumber")), Nothing, drSql("LotNumber"))
                _ExpirationDate = IIf(IsDBNull(drSql("ExpirationDate")), Nothing, drSql("ExpirationDate"))
                _Manufacturer = IIf(IsDBNull(drSql("Manufacturer")), Nothing, drSql("Manufacturer"))

                _HistoryGBS = IIf(IsDBNull(drSql("HistoryGBS")), Nothing, drSql("HistoryGBS"))
                _FeelSick = IIf(IsDBNull(drSql("FeelSick")), Nothing, drSql("FeelSick"))
                _UserNumAdministered = IIf(IsDBNull(drSql("UserNumAdministered")), Nothing, drSql("UserNumAdministered"))
                _InjectionSide = IIf(IsDBNull(drSql("InjectionSide")), Nothing, drSql("InjectionSide"))
                _LocationSiteID = IIf(IsDBNull(drSql("LocationSiteID")), Nothing, drSql("LocationSiteID"))

                _Comments = IIf(IsDBNull(drSql("Comments")), Nothing, drSql("Comments"))


            End While
        Catch ex As Exception

            Throw ex
        End Try
    End Sub
    Public ReadOnly Property client_num As String
        Get
            Return _client_num
        End Get
    End Property

    Public ReadOnly Property VaccineLotNum As String
        Get
            Return _VaccineLotNum
        End Get
    End Property
    Property EmpTypeCd As String
        Get
            Return _emp_type_cd
        End Get
        Set(value As String)
            _emp_type_cd = value
        End Set

    End Property
    Public ReadOnly Property VaccineDate As String
        Get
            Return _VaccineDate
        End Get
    End Property
    Public ReadOnly Property EnteryDate As String
        Get
            Return _EnteryDate
        End Get
    End Property
    Public ReadOnly Property VaccineOutID As String
        Get
            Return _VaccineOutID
        End Get
    End Property
    Public ReadOnly Property FirstName As String
        Get
            Return _first_name
        End Get
    End Property
    Public ReadOnly Property LastName As String
        Get
            Return _last_name
        End Get
    End Property
    Public ReadOnly Property Mi As String
        Get
            Return _mi
        End Get
    End Property
    Public ReadOnly Property siemens_emp_num As String
        Get
            Return _siemens_emp_num
        End Get
    End Property
    Public ReadOnly Property SecurityDesc As String
        Get
            Return _security_desc
        End Get
    End Property
    Public ReadOnly Property EntityCd As String
        Get
            Return _entity_cd
        End Get
    End Property
    Public ReadOnly Property entityname As String
        Get
            Return _entity_name
        End Get
    End Property

    Public ReadOnly Property DepartmentCd As String
        Get
            Return _department_cd
        End Get
    End Property
    Public ReadOnly Property DepartmentName As String
        Get
            Return _department_name
        End Get
    End Property
    Public ReadOnly Property FacilityCd As String
        Get
            Return _facility_cd
        End Get
    End Property
    Public ReadOnly Property UserPositionDesc As String
        Get
            Return _user_position_desc
        End Get
    End Property
    Public ReadOnly Property BuildingCd As String
        Get
            Return _building_cd
        End Get
    End Property
    Public ReadOnly Property BuildingName As String
        Get
            Return _building_name
        End Get
    End Property
    Public ReadOnly Property Floor As String
        Get
            Return _floor
        End Get
    End Property
    Public ReadOnly Property AllergicEggs As String
        Get
            Return _AllergicEggs
        End Get
    End Property
    Public ReadOnly Property AllergicFluShot As String
        Get
            Return _AllergicFluShot
        End Get
    End Property
    Public ReadOnly Property AllergicThimerosal As String
        Get
            Return _AllergicThimerosal
        End Get
    End Property
    Public ReadOnly Property Allergiclatex As String
        Get
            Return _Allergiclatex
        End Get
    End Property
    Public ReadOnly Property HistoryGBS As String
        Get
            Return _HistoryGBS
        End Get
    End Property
    Public ReadOnly Property FeelSick As String
        Get
            Return _FeelSick
        End Get
    End Property
    Public ReadOnly Property UserNumAdministered As String
        Get
            Return _UserNumAdministered
        End Get
    End Property
    Public ReadOnly Property InjectionSide As String
        Get
            Return _InjectionSide
        End Get
    End Property
    Public ReadOnly Property LocationSiteID As String
        Get
            Return _LocationSiteID
        End Get
    End Property
    Public ReadOnly Property DCMHadmitRights As String
        Get
            Return _Comments
        End Get
    End Property
    Public ReadOnly Property LotNumber As String
        Get
            Return _LotNumber
        End Get
    End Property
    Public ReadOnly Property ExpirationDate As String
        Get
            Return _ExpirationDate
        End Get
    End Property
    Public ReadOnly Property Manufacturer As String
        Get
            Return _Manufacturer
        End Get
    End Property

End Class
