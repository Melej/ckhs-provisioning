﻿Imports Microsoft.VisualBasic
Imports System.ComponentModel

Public Class AccountRequest
    Private _DataConn As String
    Private _AccountRequestNum As String
    Private _account_request_num As String
    Private _account_request_type As String
    Private _emp_type_cd As String
    Private _manager_num As String
    Private _requestor_client_num As String
    Private _submitter_client_num As String
    Private _receiver_client_num As String
    Private _start_date As String
    Private _end_date As String
    Private _entered_date As String
    Private _close_date As String
    Private _first_name As String
    Private _last_name As String
    Private _mi As String
    Private _title As String
    Private _suffix As String
    Private _phone As String
    Private _fax As String
    Private _security_type_cd As String
    Private _security_desc As String
    Private _entity_cd As String
    Private _department_cd As String
    Private _department_name As String
    Private _group_num As String
    Private _group_name As String
    Private _facility_cd As String
    Private _facility_name As String
    Private _pager As String
    Private _address_1 As String
    Private _address_2 As String
    Private _city As String
    Private _state As String
    Private _zip As String
    Private _user_position_desc As String
    Private _request_desc As String
    Private _termination_cd As String
    Private _eligible_rehire As String
    Private _valid_date As String
    Private _valid_client_num As String
    Private _building_cd As String
    Private _building_name As String
    Private _floor As String
    Private _office_number As String
    Private _last_day_of_benefits As String
    Private _doctor_master_num As String
    Private _share_drive As String
    Private _npi As String
    Private _taxonomy As String
    Private _provider As String
    Private _han As String
    Private _LicensesNo As String
    Private _TCL As String
    Private _credentialedDate As String
    Private _credentialedDateDCMH As String
    Private _specialty As String
    Private _CCMCadmitRights As String
    Private _DCMHadmitRights As String
    Private _NonHanLocation As String
    Private _VendorName As String
    Private _ModelAfter As String
    Private _RequestTypeDesc As String
    Private _entity_name As String
    Private _requestor_name As String
    Private _requestor_email As String
    Private _submitter_name As String
    Private _submitter_email As String
    Private _Login_Name As String
    Private _ReceivereMail As String
    Private _AuthorizationEmpNum As String
    Private _Writeorders As String
    Private _CCMCConsults As String
    Private _DCMHConsults As String
    Private _TaylorConsults As String
    Private _SpringfieldConsults As String
    Private _MedicareID As String
    Private _BlueCrossID As String
    Private _SureScriptID As String
    Private _DEAID As String
    Private _DEAExtensionID As String
    Private _UniformPhysID As String
    Private _MedicGroupID As String
    Private _MedicGroupPhysID As String
    Private _siemens_emp_num As String
    Private _LocationOfCareID As String
    Private _CopyTo As String
    Private _ValidationStatus As String
    Private _CommLocationOfCareID As String
    Private _CommCopyTo As String
    Private _RoleNum As String
    Private _UserTypeCd As String
    Private _UserTypeDesc As String

    Private _CernerPositionDesc As String

    Private _GNumberGroupNum As String
    Private _CommNumberGroupNum As String
    Private _RSAToken As String

    Private _CPMModel As String
    Private _provmodel As String
    Private _paypathmodel As String
    Private _Schedulable As String
    Private _Televox As String
	Private _itemsadded As String

	Private _RequestItems As List(Of RequestItem)
    Private _OtherAccounts As List(Of ClientAccountAccess)
    Public Sub New()

    End Sub


    Public Sub New(ByVal AccountRequestNum As String, ByVal DataConn As String)
        _AccountRequestNum = AccountRequestNum
        _DataConn = DataConn
        GetValues()
    End Sub

    Private Sub GetValues()



        Dim cmdSql As New SqlCommand
        Dim connSql As New SqlConnection(_DataConn)
        Dim drSql As SqlClient.SqlDataReader



        '   connSql.ConnectionString = _DataConn

        cmdSql.CommandText = "SelAccountRequestV20"
        cmdSql.CommandType = CommandType.StoredProcedure
        cmdSql.Connection = connSql
        cmdSql.Parameters.Add("@account_request_num", SqlDbType.Int).Value = _AccountRequestNum


        connSql.Open()

        Try




            drSql = cmdSql.ExecuteReader


            While drSql.Read()


                _AccountRequestNum = IIf(IsDBNull(drSql("account_request_num")), Nothing, drSql("account_request_num"))

                _account_request_type = IIf(IsDBNull(drSql("account_request_type")), Nothing, drSql("account_request_type"))
                _emp_type_cd = IIf(IsDBNull(drSql("emp_type_cd")), Nothing, drSql("emp_type_cd"))
                _manager_num = IIf(IsDBNull(drSql("manager_num")), Nothing, drSql("manager_num"))

                _requestor_client_num = IIf(IsDBNull(drSql("requestor_client_num")), Nothing, drSql("requestor_client_num"))
                _requestor_name = IIf(IsDBNull(drSql("requestor_name")), Nothing, drSql("requestor_name"))
                _requestor_email = IIf(IsDBNull(drSql("requestor_email")), Nothing, drSql("requestor_email"))

                _submitter_client_num = IIf(IsDBNull(drSql("submitter_client_num")), Nothing, drSql("submitter_client_num"))
                _submitter_name = IIf(IsDBNull(drSql("submitter_name")), Nothing, drSql("submitter_name"))
                _submitter_email = IIf(IsDBNull(drSql("submitter_email")), Nothing, drSql("submitter_email"))

                _receiver_client_num = IIf(IsDBNull(drSql("receiver_client_num")), Nothing, drSql("receiver_client_num"))

                _start_date = IIf(IsDBNull(drSql("start_date")), Nothing, drSql("start_date"))
                _end_date = IIf(IsDBNull(drSql("end_date")), Nothing, drSql("end_date"))
                _entered_date = IIf(IsDBNull(drSql("entered_date")), Nothing, drSql("entered_date"))
                _close_date = IIf(IsDBNull(drSql("close_date")), Nothing, drSql("close_date"))
                _first_name = IIf(IsDBNull(drSql("first_name")), Nothing, drSql("first_name"))
                _last_name = IIf(IsDBNull(drSql("last_name")), Nothing, drSql("last_name"))
                _mi = IIf(IsDBNull(drSql("mi")), Nothing, drSql("mi"))
                _title = IIf(IsDBNull(drSql("title")), Nothing, drSql("title"))
                _suffix = IIf(IsDBNull(drSql("suffix")), Nothing, drSql("suffix"))
                _phone = IIf(IsDBNull(drSql("phone")), Nothing, drSql("phone"))
                _fax = IIf(IsDBNull(drSql("fax")), Nothing, drSql("fax"))
                _security_type_cd = IIf(IsDBNull(drSql("security_type_cd")), Nothing, drSql("security_type_cd"))
                _security_desc = IIf(IsDBNull(drSql("security_desc")), Nothing, drSql("security_desc"))

                _entity_cd = IIf(IsDBNull(drSql("entity_cd")), Nothing, drSql("entity_cd"))
                _entity_name = IIf(IsDBNull(drSql("entity_name")), Nothing, drSql("entity_name"))

                _department_cd = IIf(IsDBNull(drSql("department_cd")), Nothing, drSql("department_cd"))
                _department_name = IIf(IsDBNull(drSql("department_name")), Nothing, drSql("department_name"))

                _group_num = IIf(IsDBNull(drSql("group_num")), Nothing, drSql("group_num"))
                _group_name = IIf(IsDBNull(drSql("group_name")), Nothing, drSql("group_name"))

                _facility_cd = IIf(IsDBNull(drSql("facility_cd")), Nothing, drSql("facility_cd"))
                _facility_name = IIf(IsDBNull(drSql("facility_name")), Nothing, drSql("facility_name"))

                _pager = IIf(IsDBNull(drSql("pager")), Nothing, drSql("pager"))
                _address_1 = IIf(IsDBNull(drSql("address_1")), Nothing, drSql("address_1"))
                _address_2 = IIf(IsDBNull(drSql("address_2")), Nothing, drSql("address_2"))
                _city = IIf(IsDBNull(drSql("city")), Nothing, drSql("city"))
                _state = IIf(IsDBNull(drSql("state")), Nothing, drSql("state"))
                _zip = IIf(IsDBNull(drSql("zip")), Nothing, drSql("zip"))
                _user_position_desc = IIf(IsDBNull(drSql("user_position_desc")), Nothing, drSql("user_position_desc"))
                _request_desc = IIf(IsDBNull(drSql("request_desc")), Nothing, drSql("request_desc"))
                _termination_cd = IIf(IsDBNull(drSql("termination_cd")), Nothing, drSql("termination_cd"))
                _eligible_rehire = IIf(IsDBNull(drSql("eligible_rehire")), Nothing, drSql("eligible_rehire"))
                _valid_date = IIf(IsDBNull(drSql("valid_date")), Nothing, drSql("valid_date"))
                _valid_client_num = IIf(IsDBNull(drSql("valid_client_num")), Nothing, drSql("valid_client_num"))

                _building_cd = IIf(IsDBNull(drSql("building_cd")), Nothing, drSql("building_cd"))
                _building_name = IIf(IsDBNull(drSql("building_name")), Nothing, drSql("building_name"))
                _floor = IIf(IsDBNull(drSql("floor")), Nothing, drSql("floor"))
                _office_number = IIf(IsDBNull(drSql("office_number")), Nothing, drSql("office_number"))

                _last_day_of_benefits = IIf(IsDBNull(drSql("last_day_of_benefits")), Nothing, drSql("last_day_of_benefits"))
                _doctor_master_num = IIf(IsDBNull(drSql("doctor_master_num")), Nothing, drSql("doctor_master_num"))
                _share_drive = IIf(IsDBNull(drSql("share_drive")), Nothing, drSql("share_drive"))
                _npi = IIf(IsDBNull(drSql("npi")), Nothing, drSql("npi"))
                _taxonomy = IIf(IsDBNull(drSql("taxonomy")), Nothing, drSql("taxonomy"))
                '_provider = IIf(IsDBNull(drSql("provider")), Nothing, drSql("provider"))
                _han = IIf(IsDBNull(drSql("han")), Nothing, drSql("han"))
                _LicensesNo = IIf(IsDBNull(drSql("LicensesNo")), Nothing, drSql("LicensesNo"))
                _TCL = IIf(IsDBNull(drSql("TCL")), Nothing, drSql("TCL"))

                _credentialedDate = IIf(IsDBNull(drSql("credentialedDate")), Nothing, drSql("credentialedDate"))
                _credentialedDateDCMH = IIf(IsDBNull(drSql("CredentialedDateDCMH")), Nothing, drSql("CredentialedDateDCMH"))

                _specialty = IIf(IsDBNull(drSql("specialty")), Nothing, drSql("specialty"))
                _CCMCadmitRights = IIf(IsDBNull(drSql("CCMCadmitRights")), Nothing, drSql("CCMCadmitRights"))
                _DCMHadmitRights = IIf(IsDBNull(drSql("DCMHadmitRights")), Nothing, drSql("DCMHadmitRights"))
                '_NonHanLocation = IIf(IsDBNull(drSql("NonHanLocation")), Nothing, drSql("NonHanLocation"))
                _VendorName = IIf(IsDBNull(drSql("VendorName")), Nothing, drSql("VendorName"))
                _ModelAfter = IIf(IsDBNull(drSql("ModelAfter")), Nothing, drSql("ModelAfter"))
                _CernerPositionDesc = IIf(IsDBNull(drSql("CernerPositionDesc")), Nothing, drSql("CernerPositionDesc"))
                _RequestTypeDesc = IIf(IsDBNull(drSql("RequestTypeDesc")), Nothing, drSql("RequestTypeDesc"))

                _Login_Name = IIf(IsDBNull(drSql("login_name")), Nothing, drSql("login_name"))
                _ReceivereMail = IIf(IsDBNull(drSql("receiver_email")), Nothing, drSql("receiver_email"))

                _AuthorizationEmpNum = IIf(IsDBNull(drSql("AuthorizationEmpNum")), Nothing, drSql("AuthorizationEmpNum"))
                _Writeorders = IIf(IsDBNull(drSql("Writeorders")), Nothing, drSql("Writeorders"))
                _CCMCConsults = IIf(IsDBNull(drSql("CCMCConsults")), Nothing, drSql("CCMCConsults"))
                _DCMHConsults = IIf(IsDBNull(drSql("DCMHConsults")), Nothing, drSql("DCMHConsults"))
                _TaylorConsults = IIf(IsDBNull(drSql("TaylorConsults")), Nothing, drSql("TaylorConsults"))
                _SpringfieldConsults = IIf(IsDBNull(drSql("SpringfieldConsults")), Nothing, drSql("SpringfieldConsults"))
                _MedicareID = IIf(IsDBNull(drSql("MedicareID")), Nothing, drSql("MedicareID"))
                _BlueCrossID = IIf(IsDBNull(drSql("BlueCrossID")), Nothing, drSql("BlueCrossID"))
                _SureScriptID = IIf(IsDBNull(drSql("SureScriptID")), Nothing, drSql("SureScriptID"))
                _DEAID = IIf(IsDBNull(drSql("DEAID")), Nothing, drSql("DEAID"))
                _DEAExtensionID = IIf(IsDBNull(drSql("DEAExtensionID")), Nothing, drSql("DEAExtensionID"))
                _UniformPhysID = IIf(IsDBNull(drSql("UniformPhysID")), Nothing, drSql("UniformPhysID"))
                _MedicGroupID = IIf(IsDBNull(drSql("MedicGroupID")), Nothing, drSql("MedicGroupID"))
                _MedicGroupPhysID = IIf(IsDBNull(drSql("MedicGroupPhysID")), Nothing, drSql("MedicGroupPhysID"))
                _siemens_emp_num = IIf(IsDBNull(drSql("siemens_emp_num")), Nothing, drSql("siemens_emp_num"))

                _LocationOfCareID = IIf(IsDBNull(drSql("LocationOfCareID")), Nothing, drSql("LocationOfCareID"))
                _CopyTo = IIf(IsDBNull(drSql("CopyTo")), Nothing, drSql("CopyTo"))
                _ValidationStatus = IIf(IsDBNull(drSql("ValidationStatus")), Nothing, drSql("ValidationStatus"))

                _RoleNum = IIf(IsDBNull(drSql("RoleNum")), Nothing, drSql("RoleNum"))

                _CommLocationOfCareID = IIf(IsDBNull(drSql("CommLocationOfCareID")), Nothing, drSql("CommLocationOfCareID"))
                _CommCopyTo = IIf(IsDBNull(drSql("CommCopyTo")), Nothing, drSql("CommCopyTo"))

                _UserTypeCd = IIf(IsDBNull(drSql("UserTypeCd")), Nothing, drSql("UserTypeCd"))
                _UserTypeDesc = IIf(IsDBNull(drSql("UserTypeDesc")), Nothing, drSql("UserTypeDesc"))

                _GNumberGroupNum = IIf(IsDBNull(drSql("GNumberGroupNum")), Nothing, drSql("GNumberGroupNum"))
                _CommNumberGroupNum = IIf(IsDBNull(drSql("CommNumberGroupNum")), Nothing, drSql("CommNumberGroupNum"))


                _RSAToken = IIf(IsDBNull(drSql("RSAToken")), Nothing, drSql("RSAToken"))

                _CPMModel = IIf(IsDBNull(drSql("CPMModel")), Nothing, drSql("CPMModel"))
                _provmodel = IIf(IsDBNull(drSql("provmodel")), Nothing, drSql("provmodel"))
                _paypathmodel = IIf(IsDBNull(drSql("paypathmodel")), Nothing, drSql("paypathmodel"))
                _Schedulable = IIf(IsDBNull(drSql("Schedulable")), Nothing, drSql("Schedulable"))
                _Televox = IIf(IsDBNull(drSql("Televox")), Nothing, drSql("Televox"))

				_itemsadded = IIf(IsDBNull(drSql("itemsadded")), Nothing, drSql("itemsadded"))



			End While

            _RequestItems = New RequestItems(CType(_AccountRequestNum, Integer)).RequestItems

            If Not _receiver_client_num Is Nothing Then

                _OtherAccounts = New ClientAccounts(_receiver_client_num).ClientAccounts

            End If

        Catch ex As Exception

            Throw ex
        End Try
    End Sub
    Public ReadOnly Property AccountRequestNum As String
        Get
            Return _account_request_num
        End Get
    End Property

    Public ReadOnly Property AccountRequestType As String
        Get
            Return _account_request_type
        End Get
    End Property
    Property EmpTypeCd As String
        Get
            Return _emp_type_cd
        End Get
        Set(value As String)
            _emp_type_cd = value
        End Set

    End Property
    Public ReadOnly Property ManagerNum As String
        Get
            Return _manager_num
        End Get
    End Property
    Public ReadOnly Property RequestorClientNum As String
        Get
            Return _requestor_client_num
        End Get
    End Property
    Public ReadOnly Property SubmitterClientNum As String
        Get
            Return _submitter_client_num
        End Get
    End Property
    Public ReadOnly Property ReceiverClientNum As String
        Get
            Return _receiver_client_num
        End Get
    End Property
    Public ReadOnly Property StartDate As String
        Get
            Return _start_date
        End Get
    End Property
    Public ReadOnly Property EndDate As String
        Get
            Return _end_date
        End Get
    End Property
    Public ReadOnly Property EnteredDate As String
        Get
            Return _entered_date
        End Get
    End Property
    Public ReadOnly Property CloseDate As String
        Get
            Return _close_date
        End Get
    End Property
    Public ReadOnly Property FirstName As String
        Get
            Return _first_name
        End Get
    End Property
    Public ReadOnly Property LastName As String
        Get
            Return _last_name
        End Get
    End Property
    Public ReadOnly Property Mi As String
        Get
            Return _mi
        End Get
    End Property
    Public ReadOnly Property Title As String
        Get
            Return _title
        End Get
    End Property
    Public ReadOnly Property Suffix As String
        Get
            Return _suffix
        End Get
    End Property
    Public ReadOnly Property Phone As String
        Get
            Return _phone
        End Get
    End Property
    Public ReadOnly Property Fax As String
        Get
            Return _fax
        End Get
    End Property
    Public ReadOnly Property SecurityTypeCd As String
        Get
            Return _security_type_cd
        End Get
    End Property
    Public ReadOnly Property SecurityDesc As String
        Get
            Return _security_desc
        End Get
    End Property
    Public ReadOnly Property EntityCd As String
        Get
            Return _entity_cd
        End Get
    End Property
    Public ReadOnly Property DepartmentCd As String
        Get
            Return _department_cd
        End Get
    End Property
    Public ReadOnly Property DepartmentName As String
        Get
            Return _department_name
        End Get
    End Property
    Public ReadOnly Property GroupNum As String
        Get
            Return _group_num
        End Get
    End Property
    Public ReadOnly Property GroupName As String
        Get
            Return _group_name
        End Get
    End Property
    Public ReadOnly Property FacilityCd As String
        Get
            Return _facility_cd
        End Get
    End Property
    Public ReadOnly Property Pager As String
        Get
            Return _pager
        End Get
    End Property
    Public ReadOnly Property Address1 As String
        Get
            Return _address_1
        End Get
    End Property
    Public ReadOnly Property Address2 As String
        Get
            Return _address_2
        End Get
    End Property
    Public ReadOnly Property City As String
        Get
            Return _city
        End Get
    End Property
    Public ReadOnly Property State As String
        Get
            Return _state
        End Get
    End Property
    Public ReadOnly Property Zip As String
        Get
            Return _zip
        End Get
    End Property
    Public ReadOnly Property UserPositionDesc As String
        Get
            Return _user_position_desc
        End Get
    End Property
    Public ReadOnly Property RequestDesc As String
        Get
            Return _request_desc
        End Get
    End Property
    Public ReadOnly Property TerminationCd As String
        Get
            Return _termination_cd
        End Get
    End Property
    Public ReadOnly Property EligibleRehire As String
        Get
            Return _eligible_rehire
        End Get
    End Property
    Public ReadOnly Property ValidDate As String
        Get
            Return _valid_date
        End Get
    End Property
    Public ReadOnly Property ValidClientNum As String
        Get
            Return _valid_client_num
        End Get
    End Property
    Public ReadOnly Property BuildingCd As String
        Get
            Return _building_cd
        End Get
    End Property
    Public ReadOnly Property BuildingName As String
        Get
            Return _building_name
        End Get
    End Property
    Public ReadOnly Property Floor As String
        Get
            Return _floor
        End Get
    End Property
    Public ReadOnly Property OfficeNumber As String
        Get
            Return _office_number
        End Get
    End Property
    Public ReadOnly Property LastDayOfBenefits As String
        Get
            Return _last_day_of_benefits
        End Get
    End Property
    Public ReadOnly Property DoctorNum As String
        Get
            Return _doctor_master_num
        End Get
    End Property
    Public ReadOnly Property ShareDrive As String
        Get
            Return _share_drive
        End Get
    End Property
    Public ReadOnly Property Npi As String
        Get
            Return _npi
        End Get
    End Property
    Public ReadOnly Property Taxonomy As String
        Get
            Return _taxonomy
        End Get
    End Property
    Public ReadOnly Property Provider As String
        Get
            Return _provider
        End Get
    End Property
    Public ReadOnly Property Han As String
        Get
            Return _han
        End Get
    End Property
    Public ReadOnly Property LicensesNo As String
        Get
            Return _LicensesNo
        End Get
    End Property
    Public ReadOnly Property TCL As String
        Get
            Return _TCL
        End Get
    End Property
    Public ReadOnly Property CredentialedDate As String
        Get
            Return _credentialedDate
        End Get
    End Property
    Public ReadOnly Property CredentialedDateDCMH As String
        Get
            Return _credentialedDateDCMH
        End Get
    End Property
    Public ReadOnly Property Specialty As String
        Get
            Return _specialty
        End Get
    End Property
    Public ReadOnly Property CCMCadmitRights As String
        Get
            Return _CCMCadmitRights
        End Get
    End Property
    Public ReadOnly Property DCMHadmitRights As String
        Get
            Return _DCMHadmitRights
        End Get
    End Property
    Public ReadOnly Property NonHanLocation As String
        Get
            Return _NonHanLocation
        End Get
    End Property
    Public ReadOnly Property VendorName As String
        Get
            Return _VendorName
        End Get
    End Property
    Public ReadOnly Property ModelAfter As String
        Get
            Return _ModelAfter
        End Get
    End Property
    Public ReadOnly Property CernerPositionDesc As String
        Get
            Return _CernerPositionDesc
        End Get
    End Property
    Public ReadOnly Property RequestTypeDesc As String
        Get
            Return _RequestTypeDesc
        End Get
    End Property
    Public ReadOnly Property EntityName As String
        Get
            Return _entity_name
        End Get
    End Property
    Public ReadOnly Property RequestorName As String
        Get
            Return _requestor_name
        End Get
    End Property
    Public ReadOnly Property RequestorEmail As String
        Get
            Return _requestor_email
        End Get
    End Property
    Public ReadOnly Property SubmitterName As String
        Get
            Return _submitter_name
        End Get
    End Property
    Public ReadOnly Property SubmitterEmail As String
        Get
            Return _submitter_email
        End Get
    End Property
    Public ReadOnly Property Location As String
        Get
            Return _facility_name
        End Get
    End Property
    Public ReadOnly Property RequestItems As List(Of RequestItem)
        Get
            Return _RequestItems
        End Get
    End Property
    Public ReadOnly Property OtherAccounts As List(Of ClientAccountAccess)
        Get
            Return _OtherAccounts
        End Get
    End Property
    Public ReadOnly Property Login_Name As String
        Get
            Return _Login_Name
        End Get
    End Property
    Public ReadOnly Property ReceivereMail As String
        Get
            Return _ReceivereMail
        End Get
    End Property
    Public ReadOnly Property RoleNum As String
        Get
            Return _RoleNum
        End Get
    End Property

    <DisplayName("Authorization Emp Num")> Property AuthorizationEmpNum As String
        Get
            Return _AuthorizationEmpNum
        End Get
        Set(value As String)
            _AuthorizationEmpNum = value
        End Set
    End Property
    <DisplayName("Write orders")> Property Writeorders As String
        Get
            Return _Writeorders
        End Get
        Set(value As String)
            _Writeorders = value
        End Set
    End Property
    <DisplayName("CCMC Consults")> Property CCMCConsults As String
        Get
            Return _CCMCConsults
        End Get
        Set(value As String)
            _CCMCConsults = value
        End Set
    End Property
    <DisplayName("DCMH Consults")> Property DCMHConsults As String
        Get
            Return _DCMHConsults
        End Get
        Set(value As String)
            _DCMHConsults = value
        End Set
    End Property
    <DisplayName("Taylor Consults")> Property TaylorConsults As String
        Get
            Return _TaylorConsults
        End Get
        Set(value As String)
            _TaylorConsults = value
        End Set
    End Property
    <DisplayName("Springfield Consults")> Property SpringfieldConsults As String
        Get
            Return _SpringfieldConsults
        End Get
        Set(value As String)
            _SpringfieldConsults = value
        End Set
    End Property
    <DisplayName("Medicare ID")> Property MedicareID As String
        Get
            Return _MedicareID
        End Get
        Set(value As String)
            _MedicareID = value
        End Set
    End Property
    <DisplayName("BlueCross ID")> Property BlueCrossID As String
        Get
            Return _BlueCrossID
        End Get
        Set(value As String)
            _BlueCrossID = value
        End Set
    End Property
    <DisplayName("SureScript ID")> Property SureScriptID As String
        Get
            Return _SureScriptID
        End Get
        Set(value As String)
            _SureScriptID = value
        End Set
    End Property
    <DisplayName("DEA ID")> Property DEAID As String
        Get
            Return _DEAID
        End Get
        Set(value As String)
            _DEAID = value
        End Set
    End Property
    <DisplayName("DEA Extension ID")> Property DEAExtensionID As String
        Get
            Return _DEAExtensionID
        End Get
        Set(value As String)
            _DEAExtensionID = value
        End Set
    End Property
    <DisplayName("Uniform Phys ID")> Property UniformPhysID As String
        Get
            Return _UniformPhysID
        End Get
        Set(value As String)
            _UniformPhysID = value
        End Set
    End Property
    <DisplayName("Medic Group ID")> Property MedicGroupID As String
        Get
            Return _MedicGroupID
        End Get
        Set(value As String)
            _MedicGroupID = value
        End Set
    End Property
    <DisplayName("Medic Group Phys ID")> Property MedicGroupPhysID As String
        Get
            Return _MedicGroupPhysID
        End Get
        Set(value As String)
            _MedicGroupPhysID = value
        End Set
    End Property
    <DisplayName("Siemens Emp#")> Property siemens_emp_num As String
        Get
            Return _siemens_emp_num
        End Get
        Set(value As String)
            _siemens_emp_num = value
        End Set
    End Property
    <DisplayName("Location Of Care")> Property LocationOfCareID As String
        Get
            Return _LocationOfCareID
        End Get
        Set(value As String)
            _LocationOfCareID = value
        End Set
    End Property
    <DisplayName("Copy To")> Property CopyTo As String
        Get
            Return _CopyTo
        End Get
        Set(value As String)
            _CopyTo = value
        End Set
    End Property
    <DisplayName("Validation Status")> Property ValidationStatus As String
        Get
            Return _ValidationStatus
        End Get
        Set(value As String)
            _ValidationStatus = value
        End Set
    End Property
    <DisplayName("Comm Location Of Care")> Property CommLocationOfCareID As String
        Get
            Return _CommLocationOfCareID
        End Get
        Set(value As String)
            _CommLocationOfCareID = value
        End Set
    End Property
    <DisplayName("Comm Copy To")> Property CommCopyTo As String
        Get
            Return _CommCopyTo
        End Get
        Set(value As String)
            _CommCopyTo = value
        End Set
    End Property
    <DisplayName("User Type CD ")> Property UserTypeCd As String
        Get
            Return _UserTypeCd
        End Get
        Set(value As String)
            _UserTypeCd = value
        End Set
    End Property
    <DisplayName("User Type Desc ")> Property UserTypeDesc As String
        Get
            Return _UserTypeDesc
        End Get
        Set(value As String)
            _UserTypeDesc = value
        End Set
    End Property

    Public ReadOnly Property GNumberGroupNum As String
        Get
            Return _GNumberGroupNum
        End Get
    End Property
    Public ReadOnly Property CommNumberGroupNum As String
        Get
            Return _CommNumberGroupNum
        End Get
    End Property
    Public ReadOnly Property RSAToken As String
        Get
            Return (_RSAToken)
        End Get
    End Property
    Public ReadOnly Property CPMModel As String
        Get
            Return (_CPMModel)
        End Get
    End Property
    Public ReadOnly Property provmodel As String
        Get
            Return (_provmodel)
        End Get
    End Property
    Public ReadOnly Property paypathmodel As String
        Get
            Return (_paypathmodel)
        End Get
    End Property
    Public ReadOnly Property Schedulable As String
        Get
            Return (_Schedulable)
        End Get
    End Property
    Public ReadOnly Property Televox As String
        Get
            Return (_Televox)
        End Get
    End Property
	Public ReadOnly Property itemsadded As String
		Get
			Return (_itemsadded)
		End Get
	End Property


End Class
