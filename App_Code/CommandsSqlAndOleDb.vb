'Imports System.Web
'Imports System.Web.UI
Imports System.Collections
Imports System.Data.SqlClient
Imports System.Data
Imports System.Data.OleDb
Imports System.Web.HttpContext
Imports System.Web.HttpRequest

Namespace App_Code

    'Namespace Hypnos

    Public Class CommandsSqlAndOleDb
        Protected cmd As SqlCommand 'Shared 
        'Protected conn As SqlConnection
        Protected ds As DataSet '  Shared 
        Dim sErr As String = ""
        Public Sub AddSqlProcParameter(ByVal sArgName As String, ByVal sArgValue As String, _
                                 ByVal sDataType As SqlDbType)
            Dim sParam As New SqlParameter
            sParam.ParameterName = sArgName
            ' sParam.DbType = sDataType
            sParam.SqlDbType = sDataType

            sParam.Value = sArgValue
            cmd.Parameters.Add(sParam)

        End Sub
        Public Sub AddSqlProcParameter(ByVal sArgName As String, ByVal sArgValue As String, _
                                       ByVal sDataType As SqlDbType, ByVal iLength As Integer)
            Dim sParam As New SqlParameter
            sParam.ParameterName = sArgName
            ' sParam.DbType = sDataType
            sParam.SqlDbType = sDataType
            sParam.Size = iLength
            sParam.Value = sArgValue
            cmd.Parameters.Add(sParam)

        End Sub
        Public Sub AddSqlProcParameter(ByVal sArgName As String, ByVal sArgValue As String, _
                                       ByVal sDataType As SqlDbType, ByVal iLength As Integer, ByVal sDir As ParameterDirection)
            Dim sParam As New SqlParameter
            sParam.ParameterName = sArgName
            ' sParam.DbType = sDataType
            sParam.SqlDbType = sDataType
            sParam.Size = iLength
            sParam.Value = sArgValue
            sParam.Direction = sDir
            cmd.Parameters.Add(sParam)


        End Sub
        Public Shared Function getSqlConn(ByVal sConnStringIn As String) As SqlConnection
            'Dim strConn As String = Session("DataConn") 'Session("DataConn") 'Session("DataConn") 'ConnectionMgr.getConnString 'ConfigurationSettings.AppSettings("sqlConnection1.ConnectionString")
            Dim ConnCallProcs As New System.Data.SqlClient.SqlConnection(sConnStringIn)
            Return ConnCallProcs
        End Function
        Public Function setSqlConn(ByVal sConnStringIn As String) As SqlConnection
            'Dim strConn As String = Session("DataConn") 'Session("DataConn") 'Session("DataConn") 'ConnectionMgr.getConnString 'ConfigurationSettings.AppSettings("sqlConnection1.ConnectionString")
            Dim ConnCallProcs As New System.Data.SqlClient.SqlConnection(sConnStringIn)
            Return ConnCallProcs
        End Function
        Public Shared Function getSqlConn() As SqlConnection
            Dim strConn As String = HttpContext.Current.Session("DataConn") 'Session("DataConn") 'Session("DataConn") 'ConnectionMgr.getConnString 'ConfigurationSettings.AppSettings("sqlConnection1.ConnectionString")
            Dim ConnCallProcs As New System.Data.SqlClient.SqlConnection(strConn)
            Return ConnCallProcs
        End Function
        Public Shared Function getOleDbConn() As OleDb.OleDbConnection
            Dim configurationAppSettings As System.Configuration.AppSettingsReader = New System.Configuration.AppSettingsReader
            ' Dim strConn As String = CType(configurationAppSettings.GetValue("AccessConnection2.ConnectionString", GetType(System.String)), String)
            Dim strConn As String = CType(configurationAppSettings.GetValue("OleDBSqlSecurity.ConnectionString", GetType(System.String)), String)

            Dim conn As New OleDbConnection(strConn)
            Return conn
        End Function
        Public Shared Function getDistinctDataSet(ByVal sDistinctField As String, ByVal sSortField As String, ByVal dstIN As DataSet) As DataSet


            Dim dtclone As New DataSet

            dtclone = dstIN.Clone
            dtclone.Clear()
            Dim rows As System.Data.DataRow()

            Dim sCriteria As String
            Dim sValue As String
            For Each r As DataRow In dstIN.Tables(0).Rows


                ' For Each ro As DataRow In dtclone.Tables(0).Rows
                If Not r.IsNull(sDistinctField) Then
                    sValue = r.Item(sDistinctField).ToString()
                    sValue = sValue.Replace("'", "")
                    sCriteria = sDistinctField & "='" & sValue & "'"

                    rows = dtclone.Tables(0).Select(sCriteria, sSortField)
                    If rows.Length > 0 Then 'already in dtclone ignore
                    Else 'not in dtclone add
                        dtclone.Tables(0).ImportRow(r)
                        dtclone.AcceptChanges()

                    End If
                End If
            Next

            Return dtclone
        End Function
        Sub New(ByVal sProc As String)

            cmd = New SqlCommand
            cmd.Parameters.Clear()
            cmd.Connection = getSqlConn()
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = sProc
            'setDefaults()
        End Sub
        Sub New()

            cmd = New SqlCommand
            cmd.Parameters.Clear()
            cmd.Connection = getSqlConn()
            cmd.CommandType = CommandType.StoredProcedure
            '  cmd.CommandText = sProc
            'setDefaults()
        End Sub
        Sub New(ByVal sProc As String, ByVal sConnString As String)

            cmd = New SqlCommand
            cmd.Parameters.Clear()
            cmd.Connection = getSqlConn(sConnString)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = sProc
            'setDefaults()
        End Sub
        Public Sub setCommandText(ByVal sProc As String)
            cmd.CommandText = sProc
        End Sub
        Public Sub setCommandType(ByVal sqlcommand As CommandType)
            cmd.CommandType = sqlcommand
        End Sub
        Public Function GetSqlDataset() As DataSet
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            Dim ds As New DataSet
            ds.Clear()
            'Dim dt As DataTable
            Dim iCount As Integer
            '  Try
            da.Fill(ds)
            If ds.Tables.Count > 0 Then
                iCount = ds.Tables(0).Rows.Count
            End If
            ' dt = ds.Tables(0)
            da.Dispose()
            Return ds
            ' ds.Dispose()
            'new

            'Catch ex As Exception
            '    serr = ex.Message
            ' Throw ex
            'End Try
        End Function
        Public Function GetSqlDatasetCum(ByVal ds As DataSet, ByVal sconn As String) As DataSet
            cmd.Connection.ConnectionString = sconn
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            '   Dim ds As New DataSet
            ' ds.Clear()
            'Dim dt As DataTable
            Dim iCount As Integer
            '  Try
            da.Fill(ds)
            If ds.Tables.Count > 0 Then
                iCount = ds.Tables(0).Rows.Count
            End If
            ' dt = ds.Tables(0)
            da.Dispose()
            Return ds
            ' ds.Dispose()
            'new

            'Catch ex As Exception
            '    serr = ex.Message
            ' Throw ex
            'End Try
        End Function
        Public Function GetSqlDataset(ByVal cmdin As SqlCommand) As DataSet
            Dim da As New SqlClient.SqlDataAdapter(cmdin)
            Dim ds As New DataSet
            ds.Clear()
            'Dim dt As DataTable
            Dim iCount As Integer
            da.Fill(ds)

            If ds.Tables.Count > 0 Then
                iCount = ds.Tables(0).Rows.Count
            End If
            ' dt = ds.Tables(0)
            da.Dispose()
            Return ds
            ' ds.Dispose()
            'new

        End Function

        Public Function GetSqlDatasetMultiResultsets(ByVal iNumResultSets As Integer) As DataSet

            cmd.Connection.Open()
            Dim reader1 As SqlDataReader = cmd.ExecuteReader()

            Dim dt As New DataTable
            Dim dat As New SqlClient.SqlDataAdapter(cmd)
            Dim dst As New DataSet
            dst.Clear()
            For iresultset As Integer = 0 To iNumResultSets - 1
                dt = New DataTable
                dt.Load(reader1)
                dst.Tables.Add(dt)
                ' reader1.NextResult()
            Next
            'Dim dt As DataTable
            Dim iCount As Integer
            ' dat.Fill(ds)
            If dst.Tables.Count > 0 Then
                iCount = dst.Tables(1).Rows.Count
            End If
            cmd.Connection.Close()
            ' dt = ds.Tables(0)
            dat.Dispose()
            dt.Dispose()
            cmd.Dispose()
            Return dst


            'Catch ez As SqlException
            '    sErr = ez.Message
            'End Try
        End Function
        Public Function GetSqlDataset(ByVal sSelectCriteria As String) As DataSet
            Dim da As New SqlClient.SqlDataAdapter(cmd) 'original command
            Dim ds As New DataSet
            Dim dsOut As New DataSet
            dsOut = ds.Clone
            Dim foundRows() As DataRow
            Dim foundrow As DataRow
            ds.Clear()
            'Dim dt As DataTable
            Dim iCount As Integer
            da.Fill(ds) 'first fill
            If ds.Tables.Count > 0 Then
                iCount = ds.Tables(0).Rows.Count
            End If
            foundRows = ds.Tables(0).Select(sSelectCriteria)
            ' dt = ds.Tables(0)
            iCount = ds.Tables(0).Rows.Count
            ' ds.Clear()
            For Each foundrow In foundRows
                dsOut.Tables(0).ImportRow(foundrow)
            Next
            ds.Dispose()
            'new
            da.Dispose()
            Return dsOut

        End Function

        Public Function GetSqlDataset(ByVal dsin As DataSet, ByVal sTableName As String, ByVal sFilter As String, ByVal sSort As String) As DataSet
            ' Dim da As New SqlClient.SqlDataAdapter(cmd) 'original command
            'TODO add 20100223
            ' If listv Then

            Dim dsOut As New DataSet
            dsOut = dsin.Clone
            'Dim dstemp As New DataSet
            Dim foundRows() As DataRow
            Dim foundrow As DataRow
            ' dsout.Clear()
            'Dim dt As DataTable
            Dim iCount As Integer
            'da.Fill(ds) 'first fill
            Try
                If dsin.Tables.Count > 0 Then
                    iCount = dsin.Tables(sTableName).Rows.Count

                    foundRows = dsin.Tables(sTableName).Select(sFilter, sSort)
                    ' dt = ds.Tables(0)
                    iCount = dsin.Tables(sTableName).Rows.Count
                    ' ds.Clear()
                    For Each foundrow In foundRows
                        dsOut.Tables(sTableName).ImportRow(foundrow)
                    Next
                    iCount = dsOut.Tables(sTableName).Rows.Count
                End If
                Return dsOut
                'Catch ex As Exception
                '    sErr = ex.Message
                '    Return dsin
            Finally

                dsin.Dispose()
                dsOut.Dispose()
            End Try

        End Function
        Public Function GetSqlDataset(ByVal dsin As DataSet, ByVal sTableName As String, ByVal sFilter As String, ByVal sSort As String, ByVal bReturnNulls As Boolean) As DataSet
            ' Dim da As New SqlClient.SqlDataAdapter(cmd) 'original command

            Dim dsOut As New DataSet
            dsOut = dsin.Clone
            'Dim dstemp As New DataSet
            Dim foundRows() As DataRow
            Dim foundrow As DataRow
            ' dsout.Clear()
            'Dim dt As DataTable
            Dim iCount As Integer
            'da.Fill(ds) 'first fill
            Try
                If dsin.Tables.Count > 0 Then
                    iCount = dsin.Tables(sTableName).Rows.Count
                End If
                foundRows = dsin.Tables(sTableName).Select(sFilter, sSort)
                ' dt = ds.Tables(0)
                iCount = dsin.Tables(sTableName).Rows.Count
                ' ds.Clear()
                For Each foundrow In foundRows
                    dsOut.Tables(sTableName).ImportRow(foundrow)
                Next
                iCount = dsOut.Tables(sTableName).Rows.Count
                Return dsOut
                'Catch ex As Exception
                '    sErr = ex.Message
                '    Return dsin
            Finally

                dsin.Dispose()
                dsOut.Dispose()
            End Try

        End Function
        Public Function GetSqlDataset(ByVal sSelectCriteria As String, ByVal dsin As DataSet) As DataSet
            ' Dim da As New SqlClient.SqlDataAdapter(cmd) 'original command

            Dim dsOut As New DataSet
            dsOut = dsin.Clone
            'Dim dstemp As New DataSet
            Dim foundRows() As DataRow
            Dim foundrow As DataRow
            ' dsout.Clear()
            'Dim dt As DataTable
            Dim iCount As Integer
            'da.Fill(ds) 'first fill
            Try
                If dsin.Tables.Count > 0 Then
                    iCount = dsin.Tables(0).Rows.Count
                End If
                foundRows = dsin.Tables(0).Select(sSelectCriteria)
                ' dt = ds.Tables(0)
                iCount = dsin.Tables(0).Rows.Count
                ' ds.Clear()
                For Each foundrow In foundRows
                    dsOut.Tables(0).ImportRow(foundrow)
                Next
                iCount = dsOut.Tables(0).Rows.Count
                Return dsOut
                'Catch ex As Exception
                '    sErr = ex.Message
                '    Return dsin
            Finally

                dsin.Dispose()
                dsOut.Dispose()
            End Try

        End Function
        'Public Function GetSqlDatasetWithSortAndCriteria(ByVal sSelectCriteria As String, ByVal sSort As String, ByVal dsin As DataSet) As DataSet
        '    ' Dim da As New SqlClient.SqlDataAdapter(cmd) 'original command
        '    Dim sErr As String
        '    Dim dsOut As New DataSet
        '    dsOut = dsin.Clone
        '    'Dim dstemp As New DataSet
        '    Dim foundRows() As DataRow
        '    Dim foundrow As DataRow
        '    ' dsout.Clear()
        '    'Dim dt As DataTable
        '    Dim iCount As Integer
        '    'da.Fill(ds) 'first fill
        '    Try
        '        If dsin.Tables.Count > 0 Then
        '            iCount = dsin.Tables(0).Rows.Count
        '            ' ds.Clear()
        '            For Each foundrow In foundRows
        '                dsOut.Tables(0).ImportRow(foundrow)
        '            Next
        '            'Dim i As Integer = 0
        '            'If dsin.Tables.Count > 1 Then 'hold on to extra data
        '            '    For i = 1 To dsin.Tables.Count - 1
        '            '        dsOut.Tables.Add(dsin.Tables(iCount))
        '            '    Next
        '            '20111017
        '        End If
        '        iCount = dsOut.Tables(0).Rows.Count
        '        Return dsOut
        '    Catch ex As Exception
        '        sErr = ex.Message
        '        Return dsin
        '        Throw ex
        '    Finally

        '        dsin.Dispose()
        '        dsOut.Dispose()
        '    End Try

        'End Function
        Public Function GetSqlDataTableWithSortAndCriteria(ByVal sSelectCriteria As String, ByVal sSort As String, ByVal dsin As DataSet) As DataTable
            ' Dim da As New SqlClient.SqlDataAdapter(cmd) 'original command
            'new Untested
            Dim sErr As String
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            Dim ds As New DataSet
            Dim dt As New DataTable
            Dim dsOut As New DataSet
            dsOut = dsin.Clone
            'Dim dstemp As New DataSet
            Dim foundRows() As DataRow
            Dim foundrow As DataRow
            ' dsout.Clear()
            'Dim dt As DataTable
            Dim iCount As Integer
            'da.Fill(ds) 'first fill
            Try
                If dsin.Tables.Count > 0 Then
                    iCount = dsin.Tables(0).Rows.Count
                End If
                foundRows = dsin.Tables(0).Select(sSelectCriteria, sSort)
                ' dt = ds.Tables(0)
                iCount = dsin.Tables(0).Rows.Count
                ' ds.Clear()
                For Each foundrow In foundRows
                    dsOut.Tables(0).ImportRow(foundrow)
                    dt.ImportRow(foundrow)
                Next
                iCount = dsOut.Tables(0).Rows.Count
                'da.Fill(ds)
                'If ds.Tables.Count > 0 Then
                '    'iCount = ds.Tables(0).Rows.Count
                '    dt = dsOut.Tables(0)
                'End If
                Return dsOut.Tables(0)

            Catch ex As Exception
                sErr = ex.Message
                ' Throw ex
                Return dt

            Finally

                dt.Dispose()
                ds.Dispose()
                da.Dispose()
                dsin.Dispose()
                dsOut.Dispose()
            End Try

        End Function
        Public Function GetSqlDataTableWithSortAndCriteria(ByVal sSelectCriteria As String, ByVal sSort As String, ByVal dsin As DataTable) As DataTable
            ' Dim da As New SqlClient.SqlDataAdapter(cmd) 'original command
            'new Untested
            Dim sErr As String
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            Dim ds As New DataSet
            Dim dt As New DataTable

            Dim dsOut As New DataSet
            dsOut.Tables.Add(dsin.Clone())
            'Dim dstemp As New DataSet
            Dim foundRows() As DataRow
            Dim foundrow As DataRow
            ' dsout.Clear()
            'Dim dt As DataTable
            Dim iCount As Integer
            'da.Fill(ds) 'first fill
            Try
                If dsin.Rows.Count > 0 Then
                    iCount = dsin.Rows.Count
                End If
                foundRows = dsin.Select(sSelectCriteria, sSort)
                ' dt = ds.Tables(0)

                ' ds.Clear()
                dsOut.Tables(0).Clear() 'get rid of rows only wanted structure
                iCount = dsin.Rows.Count
                iCount = dsOut.Tables(0).Rows.Count
                For Each foundrow In foundRows
                    dsOut.Tables(0).ImportRow(foundrow)
                    '  dt.ImportRow(foundrow)
                    'dt.Rows.Add(foundrow)
                    'nodt.Rows.Add(foundrow)
                Next
                dt = dsOut.Tables(0).Copy()
                iCount = dt.Rows.Count

                Return dt

            Catch ex As Exception
                sErr = ex.Message
                Return dt
            Finally
                dt.Dispose()
                ds.Dispose()
                da.Dispose()
                dsin.Dispose()
                'dsOut.Dispose()
            End Try

        End Function

        Public Function GetSqlDataTable(ByVal dtOrig As DataTable, ByVal arFieldsToUse As ArrayList) As DataTable
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            Dim ds As New DataSet
            ' Dim dtOrig As DataTable = thisData.GetDataTableFromSqlDataSource(sdsSelCaseAct, "")
            Dim myTable As New DataTable
            Dim i As Integer
            'Array example
            'Dim arFieldsToUse() As Integer = {2, 6, 7, 8, 9, 10, 11}

            For i = 0 To arFieldsToUse.Count - 1 ' build wanted columns
                Dim dc As New DataColumn
                dc.ColumnName = dtOrig.Columns(arFieldsToUse(i)).ColumnName
                dc.DataType = dtOrig.Columns(arFieldsToUse(i)).DataType
                'If dc.DataType.ToString = System.Data.SqlDbType.DateTime.ToString Then

                'End If
                myTable.Columns.Add(dc)
                dc.Dispose()
            Next
            Dim icol As Integer = 0
            Dim irow As Integer = 0
            For irow = 0 To dtOrig.Rows.Count - 1 '1arFieldsToUse.GetUpperBound(0) ' 
                Dim dr As DataRow = myTable.NewRow
                For icol = 0 To myTable.Columns.Count - 1
                    If Not IsDBNull(dtOrig.Columns(arFieldsToUse(icol))) Then
                        dr.Item(dtOrig.Columns(arFieldsToUse(icol)).ColumnName) = dtOrig.Rows(irow).Item(arFieldsToUse(icol))
                    End If
                    'don't work
                    'If IsDate(dr.Item(dtOrig.Columns(arFieldsToUse(icol)).ColumnName)) Then
                    '    Format(dr.Item(dtOrig.Columns(arFieldsToUse(icol)).ColumnName), "ddd-dd/MM/yy")
                    'End If
                Next
                myTable.Rows.Add(dr)
                dr = Nothing
            Next

            dtOrig.Dispose()
            ds.Dispose()
            da.Dispose()
            Return myTable
        End Function
        Public Function GetSqlDataTable(ByVal dtOrig As DataTable, ByVal arFieldsToUse() As Integer) As DataTable
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            Dim ds As New DataSet
            ' Dim dtOrig As DataTable = thisData.GetDataTableFromSqlDataSource(sdsSelCaseAct, "")
            Dim myTable As New DataTable
            Dim i As Integer
            'Array example
            'Dim arFieldsToUse() As Integer = {2, 6, 7, 8, 9, 10, 11}

            For i = 0 To arFieldsToUse.GetUpperBound(0) ' build wanted columns
                Dim dc As New DataColumn
                dc.ColumnName = dtOrig.Columns(arFieldsToUse(i)).ColumnName
                dc.DataType = dtOrig.Columns(arFieldsToUse(i)).DataType
                'If dc.DataType.ToString = System.Data.SqlDbType.DateTime.ToString Then

                'End If
                myTable.Columns.Add(dc)
                dc.Dispose()
            Next
            Dim icol As Integer = 0
            Dim irow As Integer = 0
            For irow = 0 To dtOrig.Rows.Count - 1 '1arFieldsToUse.GetUpperBound(0) ' 
                Dim dr As DataRow = myTable.NewRow
                For icol = 0 To myTable.Columns.Count - 1
                    If Not IsDBNull(dtOrig.Columns(arFieldsToUse(icol))) Then
                        dr.Item(dtOrig.Columns(arFieldsToUse(icol)).ColumnName) = dtOrig.Rows(irow).Item(arFieldsToUse(icol))
                    End If
                    'don't work
                    'If IsDate(dr.Item(dtOrig.Columns(arFieldsToUse(icol)).ColumnName)) Then
                    '    Format(dr.Item(dtOrig.Columns(arFieldsToUse(icol)).ColumnName), "ddd-dd/MM/yy")
                    'End If
                Next
                myTable.Rows.Add(dr)
                dr = Nothing
            Next

            dtOrig.Dispose()
            ds.Dispose()
            da.Dispose()
            Return myTable
        End Function
        Public Function GetSqlDataTable() As DataTable

            Dim da As New SqlClient.SqlDataAdapter(cmd)
            Dim ds As New DataSet
            Dim dt As New DataTable

            da.Fill(ds)
            If ds.Tables.Count > 0 Then
                'iCount = ds.Tables(0).Rows.Count
                dt = ds.Tables(0)
            End If

            'new
            ' dt.Dispose()
            ds.Dispose()
            da.Dispose()
            Return dt
        End Function
        Public Function GetScalarReturnInt() As Integer
            cmd.Connection.Open()
            GetScalarReturnInt = cmd.ExecuteScalar
            cmd.Connection.Close()
            cmd.Dispose()
        End Function
        Public Function GetScalarReturnString() As String
            cmd.Connection.Open()
            GetScalarReturnString = cmd.ExecuteScalar
            cmd.Connection.Close()
            cmd.Dispose()
        End Function
        Public Function ExecNonQueryReturnBoolean() As Boolean
            cmd.Connection.Open()
            ExecNonQueryReturnBoolean = cmd.ExecuteNonQuery()
            cmd.Connection.Close()
            cmd.Dispose()
        End Function
        Public Function ExecNonQueryReturnInt() As Integer
            cmd.Connection.Open()
            ExecNonQueryReturnInt = cmd.ExecuteNonQuery()
            cmd.Connection.Close()

            cmd.Dispose()
        End Function
        Public Sub ExecNonQueryNoReturn()
            cmd.Connection.Open()
            cmd.ExecuteNonQuery()
            cmd.Connection.Close()
            cmd.Dispose()
        End Sub
        Public Function ExecQueryReturnDataTable(ByVal sSqlString As String, ByVal sConnStringIn As String) As DataTable
            'in progresscmd.
            cmd = New SqlCommand
            cmd.Connection = getSqlConn(sConnStringIn) '.ConnectionString = sConnStringIn
            cmd.CommandText = sSqlString
            cmd.CommandType = CommandType.Text
            ' cmd.Connection.Open()
            ' cmd.ExecuteNonQuery()
            ' cmd.Connection.Close()
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            Dim ds As New DataSet
            Dim dt As DataTable
            Dim iCount As Integer
            da.Fill(ds)
            iCount = ds.Tables(0).Rows.Count
            dt = ds.Tables(0)
            ' dt.Rows(0).Item(0)
            cmd.Dispose()
            Return dt
        End Function
        Public Function getCommand() As SqlClient.SqlCommand
            Return cmd
        End Function
        Public Sub DataTableToExcel(ByRef dtIn As DataTable)



        End Sub
        Public Function checkFieldsForNulls(ByVal dtIn As DataTable) As DataTable
            Dim i As Integer = 0
            Dim row As Integer = 0
            For row = 0 To dtIn.Rows.Count - 1
                For i = 0 To dtIn.Columns.Count - 1
                    If dtIn.Rows(0).IsNull(i) Then
                        Dim mycol As DataColumn = dtIn.Columns(i)
                        ' mycol.DataType = System.Type.GetType("System.String")
                        Dim stype As String = mycol.DataType.ToString()
                        If stype.ToLower = "system.string" Then
                            dtIn.Rows(0).Item(i) = ""
                        ElseIf stype.ToLower = "system.integer" Then
                            dtIn.Rows(0).Item(i) = 0
                        End If

                    End If
                Next
            Next
            Return dtIn
        End Function
        Public Shared Function FillFields(ByRef dtFieldList As DataTable, ByRef ParentControl As Control) As DataTable

            Dim thisData As New CommandsSqlAndOleDb()

            Dim dtReturn As New DataTable
            dtReturn.Columns.Add("FieldName", GetType(String))
            dtReturn.Columns.Add("FieldValue", GetType(String))



            Dim i As Integer = 0
            Dim stxt As String = ""
            Dim iStart As Integer = 0
            Dim sCurrentFieldName As String = ""
            Dim sCurrentControlName As String = ""
            Dim sCurrentValue As String = ""

            Dim bFoundControl As Boolean
            Dim iCurrentCalue As Integer = 0
            Dim sddl As String = ""


            If dtFieldList Is Nothing Then

                Return dtReturn
                Exit Function

            Else
                If dtFieldList.Rows.Count > 0 Then

                    For i = iStart To dtFieldList.Columns.Count - 1 '0 is casenum
                        'search for textbox
                        bFoundControl = False

                        sCurrentFieldName = dtFieldList.Columns(i).ColumnName.ToString()
                        '===check for textbox control===
                        sCurrentControlName = dtFieldList.Columns(i).ColumnName.ToString() & "TextBox"

                        Dim txt As New TextBox

                        txt = DataCheckingAndValidation.FindWebControlRecursive(ParentControl, sCurrentControlName)
                        If Not txt Is Nothing Then
                            bFoundControl = True
                            If txt.TextMode = TextBoxMode.MultiLine Then

                            End If
                            If Not dtFieldList.Rows(0).IsNull(i) Then

                                txt.Text = dtFieldList.Rows(0).Item(i)

                            End If
                            'Dim mycol As DataColumn = dtFieldList.Columns(i)
                            'Dim sFind As String = "fieldname='" & dtFieldList.Columns(i).ColumnName & "'"
                            'sFind = "ParameterName='" & dtFieldList.Columns(i).ColumnName & "'"
                            'Dim foundrows As DataRow()
                            'Dim iLen As Integer
                            'foundrows = dtFieldList.Select(sFind)
                            ''nope iLen = dtFieldList.Columns(i).MaxLength
                            ''If Not foundrows Is Nothing Then
                            'If foundrows.Length > 0 Then
                            '    iLen = foundrows(0).Item("ParameterSize")
                            '    If iLen > 0 Then
                            '        txt.MaxLength = iLen
                            '    End If
                            'Else 'not in schema table
                            '    If IsDate(dtFieldList.Rows(0).Item(i)) Then
                            '        iLen = 24
                            '    ElseIf IsNumeric(dtFieldList.Rows(0).Item(i)) Then
                            '        iLen = 4
                            '    Else 'String
                            '        iLen = 49
                            '    End If


                            'End If
                        End If
                        ''===check for dropdownlist control===search for ddl
                        sCurrentControlName = dtFieldList.Columns(i).ColumnName.ToString() & "ddl"
                        Dim ddl As New DropDownList
                        Dim lstItem As ListItem
                        ddl = DataCheckingAndValidation.FindWebControlRecursive(ParentControl, sCurrentControlName)
                        If Not ddl Is Nothing Then
                            bFoundControl = True
                            ' If Not dtFieldList.Rows(0).IsNull(i) Then
                            ' If ddl.SelectedIndex > 0 Then
                            If dtFieldList.Rows(0).IsNull(i) Or dtFieldList.Rows(0).Item(i).ToString() = "" Then
                                ' no value for ddl to be set to  ddl.SelectedValue = 1
                                If ddl.Items.Count >= 1 Then
                                    If ddl.Items(0).Value.ToLower.IndexOf("select") < 0 Then
                                        ddl.Items.Insert(0, " Select One")
                                        ddl.SelectedIndex = 0
                                    End If
                                Else 'item count none
                                    ddl.Items.Insert(0, " Select One")
                                    ddl.SelectedIndex = 0
                                End If
                            Else 'field has a value to put into ddl
                                If ddl.Items.Count > 1 Then
                                    lstItem = ddl.Items.FindByValue(dtFieldList.Rows(0).Item(i))
                                    ' ddl.SelectedValue = dtFieldList.Rows(0).Item(i)
                                    If Not lstItem Is Nothing Then
                                        ddl.SelectedValue = dtFieldList.Rows(0).Item(i)
                                    Else
                                        ddl.SelectedIndex = 0
                                    End If
                                Else ' not greater then 0 
                                    If ddl.Items.Count = 1 Then
                                        If ddl.Items(0).Value.ToLower.IndexOf("select") < 0 Then
                                            ddl.Items.Insert(0, "Select One")
                                            ddl.SelectedIndex = 0
                                        End If
                                    Else 'item count none
                                        ddl.Items.Insert(0, "Select One")
                                        ddl.SelectedIndex = 0
                                    End If
                                End If
                            End If


                        End If
                        '===check for Radio Button  control===
                        sCurrentControlName = dtFieldList.Columns(i).ColumnName.ToString() & "rbl" 'Radio button list
                        Dim rbl As New RadioButtonList
                        rbl = DataCheckingAndValidation.FindWebControlRecursive(ParentControl, sCurrentControlName)
                        If Not rbl Is Nothing Then
                            bFoundControl = True
                            If Not dtFieldList.Rows(0).IsNull(i) AndAlso dtFieldList.Rows(0).Item(i) <> "" Then
                                rbl.SelectedValue = dtFieldList.Rows(0).Item(i)


                            End If
                        End If

                        '===check for Listbox   control===
                        sCurrentControlName = dtFieldList.Columns(i).ColumnName.ToString() & "Listbox" 'Listbox list
                        Dim listbox As New ListBox
                        listbox = DataCheckingAndValidation.FindWebControlRecursive(ParentControl, sCurrentControlName)
                        If Not listbox Is Nothing Then
                            bFoundControl = True
                            If Not dtFieldList.Rows(0).IsNull(i) AndAlso dtFieldList.Rows(0).Item(i) <> "" Then
                                listbox.SelectedValue = dtFieldList.Rows(0).Item(i)


                            End If
                        End If

                        '===check for Label control===
                        sCurrentControlName = dtFieldList.Columns(i).ColumnName.ToString() & "label" 'Radio button list
                        Dim lbl As New Label
                        lbl = DataCheckingAndValidation.FindWebControlRecursive(ParentControl, sCurrentControlName)
                        If Not lbl Is Nothing Then
                            bFoundControl = True
                            If Not dtFieldList.Rows(0).IsNull(i) Then
                                lbl.Text = dtFieldList.Rows(0).Item(i)


                            End If
                        End If
                        '=======

                        If bFoundControl = False Then 'control not for this field
                            dtReturn.Rows.Add(dtFieldList.Columns(i).ColumnName, dtFieldList.Rows(0).Item(i))
                        End If
                    Next
                End If
            End If
            Return dtReturn

        End Function

        Public Shared Function FillFields(ByRef dtSchema As DataTable, ByRef dtFieldList As DataTable, _
                                  ByRef ParentControl As Control) As ArrayList

            Dim thisData As New CommandsSqlAndOleDb()

            Dim alMissingControls As New ArrayList
            Dim i As Integer = 0
            Dim stxt As String = ""
            Dim iStart As Integer = 0
            Dim sCurrentFieldName As String = ""
            Dim sCurrentControlName As String = ""
            Dim sCurrentValue As String = ""

            Dim bFoundControl As Boolean
            Dim iCurrentCalue As Integer = 0
            Dim sddl As String = ""

            '===============
            ' Pats Logic 
            '===============


            ''  Try
            'For Each column In 


            If dtFieldList.Rows.Count > 0 Then

                For i = iStart To dtFieldList.Columns.Count - 1 '0 is casenum

                    bFoundControl = False

                    sCurrentFieldName = dtFieldList.Columns(i).ColumnName.ToString()

                    '===check for textbox control===

                    sCurrentControlName = dtFieldList.Columns(i).ColumnName.ToString() & "TextBox"

                    Dim txt As New TextBox

                    txt = DataCheckingAndValidation.FindWebControlRecursive(ParentControl, sCurrentControlName)
                    If Not txt Is Nothing Then
                        bFoundControl = True

                        If Not dtFieldList.Rows(0).IsNull(i) Then

                            txt.Text = dtFieldList.Rows(0).Item(i)

                        End If
                        Dim mycol As DataColumn = dtFieldList.Columns(i)
                        Dim sFind As String = "fieldname='" & dtFieldList.Columns(i).ColumnName & "'"
                        sFind = "sname='" & dtFieldList.Columns(i).ColumnName & "'"
                        Dim foundrows As DataRow()
                        Dim iLen As Integer
                        foundrows = dtSchema.Select(sFind)

                        If foundrows.Length > 0 Then
                            iLen = foundrows(0).Item("slength")
                            If iLen > 0 Then
                                txt.MaxLength = iLen
                            End If
                        Else 'not in schema table
                            If IsDate(dtFieldList.Rows(0).Item(i)) Then
                                iLen = 24
                            ElseIf IsNumeric(dtFieldList.Rows(0).Item(i)) Then
                                iLen = 4
                            Else 'String
                                iLen = 49
                            End If


                        End If
                    End If
                    ''===check for dropdownlist control===search for ddl
                    sCurrentControlName = dtFieldList.Columns(i).ColumnName.ToString() & "ddl"
                    Dim ddl As New DropDownList
                    Dim lstItem As ListItem
                    ddl = DataCheckingAndValidation.FindWebControlRecursive(ParentControl, sCurrentControlName)
                    If Not ddl Is Nothing Then
                        bFoundControl = True


                        If dtFieldList.Rows(0).IsNull(i) Or dtFieldList.Rows(0).Item(i).ToString() = "" Then
                            ' no value for ddl to be set to  ddl.SelectedValue = 1
                            If ddl.Items.Count >= 1 Then
                                If ddl.Items(0).Value.ToLower.IndexOf("select") < 0 Then
                                    ddl.Items.Insert(0, " Select One")
                                    ddl.SelectedIndex = 0
                                End If
                            Else 'item count none
                                ddl.Items.Insert(0, " Select One")
                                ddl.SelectedIndex = 0
                            End If
                        Else 'field has a value to put into ddl
                            If ddl.Items.Count > 1 Then
                                lstItem = ddl.Items.FindByValue(dtFieldList.Rows(0).Item(i))
                                ' ddl.SelectedValue = dtFieldList.Rows(0).Item(i)
                                If Not lstItem Is Nothing Then
                                    ddl.SelectedValue = dtFieldList.Rows(0).Item(i)
                                Else
                                    ddl.SelectedIndex = 0
                                End If
                            Else ' not greater then 0 
                                If ddl.Items.Count = 1 Then
                                    If ddl.Items(0).Value.ToLower.IndexOf("select") < 0 Then
                                        ddl.Items.Insert(0, "Select One")
                                        ddl.SelectedIndex = 0
                                    End If
                                Else 'item count none
                                    ddl.Items.Insert(0, "Select One")
                                    ddl.SelectedIndex = 0
                                End If
                            End If
                        End If


                    End If
                    '===check for Radio Button  control===
                    sCurrentControlName = dtFieldList.Columns(i).ColumnName.ToString() & "rbl" 'Radio button list
                    Dim rbl As New RadioButtonList
                    rbl = DataCheckingAndValidation.FindWebControlRecursive(ParentControl, sCurrentControlName)

                    If Not rbl Is Nothing Then
                        bFoundControl = True
                        If Not dtFieldList.Rows(0).IsNull(i) AndAlso dtFieldList.Rows(0).Item(i) <> "" Then
                            rbl.SelectedValue = dtFieldList.Rows(0).Item(i)


                        End If
                    End If

                    '===check for Listbox   control===
                    sCurrentControlName = dtFieldList.Columns(i).ColumnName.ToString() & "Listbox" 'Listbox list
                    Dim listbox As New ListBox
                    listbox = DataCheckingAndValidation.FindWebControlRecursive(ParentControl, sCurrentControlName)
                    If Not listbox Is Nothing Then
                        bFoundControl = True
                        If Not dtFieldList.Rows(0).IsNull(i) AndAlso dtFieldList.Rows(0).Item(i) <> "" Then
                            listbox.SelectedValue = dtFieldList.Rows(0).Item(i)


                        End If
                    End If


                    '===check for Label control===
                    sCurrentControlName = dtFieldList.Columns(i).ColumnName.ToString() & "label" 'Radio button list
                    Dim lbl As New Label
                    lbl = DataCheckingAndValidation.FindWebControlRecursive(ParentControl, sCurrentControlName)
                    If Not lbl Is Nothing Then
                        bFoundControl = True
                        If Not dtFieldList.Rows(0).IsNull(i) Then
                            lbl.Text = dtFieldList.Rows(0).Item(i)


                        End If
                    End If
                    '=======

                    If bFoundControl = False Then 'control not for this field
                        alMissingControls.Add(dtFieldList.Columns(i).ColumnName & " Ordinal: " & i)
                    End If
                Next

            End If
            Return alMissingControls

        End Function
     
       
        Public Function UpdatedFieldsTable(ByVal dtFieldList As DataTable, ByRef ParentControl As Control) As DataTable
            Dim thisData As New SqlCommand
            thisData = cmd 'CommandReadyForParameters


            Dim ans As Boolean
            Dim s As String = ""
            Try 'mother baby upd

                Dim i As Integer = 0
                Dim stxt As String = ""
                Dim iStart As Integer = 0
                Dim sCurrentFieldName As String = ""
                Dim sCurrentControlName As String = ""
                Dim sCurrentValue As String = ""

                Dim iCurrentCalue As Integer = 0
                Dim sddl As String = ""
                Dim bFillParam As Boolean

                Dim bFoundControl As Boolean

                '==============
                ' Pats logic
                '==============
                Dim dtReturn As DataTable

                dtReturn = dtFieldList.Copy()


                '=============







                For i = 0 To dtFieldList.Columns.Count - 1 '0 is casenum
                    'search for textbox
                    bFillParam = False
                    bFoundControl = False






                    sCurrentFieldName = dtFieldList.Columns(i).ColumnName.ToString()

                    If sCurrentFieldName = "PatientName" Then
                        bFoundControl = True

                    End If


                    '======Search for Textbox=====
                    If bFoundControl = False Then ' once found dont check again for this field
                        sCurrentControlName = dtFieldList.Columns(i).ColumnName.ToString() & "TextBox"
                        Dim txt As New TextBox

                        txt = DataCheckingAndValidation.FindWebControlRecursive(ParentControl, sCurrentControlName)
                        If Not txt Is Nothing Then
                            bFoundControl = True
                            If Not String.IsNullOrEmpty(txt.Text) And sCurrentControlName.ToLower <> "commentstextbox" Then
                                Dim CharsToRemove(2) As Char
                                CharsToRemove(0) = "'"
                                CharsToRemove(1) = ";"
                                Dim arCharsToEsc As New ArrayList
                                arCharsToEsc.Add("'")
                                arCharsToEsc.Add("""")

                                If Not IsDate(txt.Text) Then
                                    txt.Text = DataCheckingAndValidation.RemoveNonCharacters(txt.Text, CharsToRemove, True, True)

                                End If


                                dtReturn.Rows(0).Item(i) = txt.Text
                                bFillParam = True
                            End If
                            If Not String.IsNullOrEmpty(txt.Text) And sCurrentControlName.ToLower = "commentstextbox" Then
                                Dim CharsToRemove(2) As Char
                                CharsToRemove(0) = "'"
                                CharsToRemove(1) = ";"
                                Dim arCharsToEsc As New ArrayList
                                arCharsToEsc.Add("'")
                                arCharsToEsc.Add("""")

                                If Not IsDate(txt.Text) Then
                                    txt.Text = DataCheckingAndValidation.RemoveNonCharacters(txt.Text, CharsToRemove, True)

                                End If


                                dtReturn.Rows(0).Item(i) = txt.Text
                                bFillParam = True
                            End If

                        End If
                    End If
                    '======Search for Label=====
                    If bFoundControl = False Then ' once found dont check again for this field
                        sCurrentControlName = dtFieldList.Columns(i).ColumnName.ToString() & "Label"
                        Dim lbl As New Label


                        lbl = DataCheckingAndValidation.FindWebControlRecursive(ParentControl, sCurrentControlName)
                        If Not lbl Is Nothing Then
                            bFoundControl = True
                            If Not String.IsNullOrEmpty(lbl.Text) Then

                                dtReturn.Rows(0).Item(i) = lbl.Text
                                bFillParam = True
                            End If
                        End If
                    End If
                    '=====search for DropDownList====================== 
                    If bFoundControl = False Then
                        sCurrentControlName = dtFieldList.Columns(i).ColumnName.ToString() & "ddl"
                        Dim ddl As New DropDownList


                        ddl = DataCheckingAndValidation.FindWebControlRecursive(ParentControl, sCurrentControlName)
                        If Not ddl Is Nothing Then
                            bFoundControl = True
                            If ddl.SelectedIndex > -1 And ddl.SelectedValue.ToLower.IndexOf("select") < 0 Then
                                'has a selected inedx and a vlaue other than select
                                dtReturn.Rows(0).Item(i) = ddl.SelectedValue
                                bFillParam = True
                            End If

                        End If
                    End If
                    '=====Search for RadioButtonList===================== 
                    If bFoundControl = False Then
                        sCurrentControlName = dtFieldList.Columns(i).ColumnName.ToString() & "rbl" 'Radio button list
                        Dim rbl As New RadioButtonList

                        rbl = DataCheckingAndValidation.FindWebControlRecursive(ParentControl, sCurrentControlName)
                        If Not rbl Is Nothing Then
                            bFoundControl = True
                            If rbl.SelectedIndex > -1 Then
                                dtReturn.Rows(0).Item(i) = rbl.SelectedValue
                                bFillParam = True

                            End If
                        End If
                    Else 'control no found

                    End If

                    '=====Search for Listbox===================== 
                    If bFoundControl = False Then
                        sCurrentControlName = dtFieldList.Columns(i).ColumnName.ToString() & "Listbox" 'Radio button list
                        Dim Listbox As New ListBox

                        Listbox = DataCheckingAndValidation.FindWebControlRecursive(ParentControl, sCurrentControlName)
                        If Not Listbox Is Nothing Then
                            bFoundControl = True
                            If Listbox.SelectedIndex > -1 Then
                                dtReturn.Rows(0).Item(i) = Listbox.SelectedValue
                                bFillParam = True

                            End If
                        End If
                    Else 'control no found

                    End If


                    If bFoundControl = False Then

                    End If
                    Dim iLen As Integer = 0 ' dtFieldList.Columns.Item(i).MaxLength
                    If bFillParam And (Not dtReturn.Rows(0).IsNull(i)) Then 'has value needs a paramemter
                        ' ''==get length




                        Dim stype As String = dtReturn.Columns(i).DataType.FullName.ToString.ToLower
                        If stype.ToLower = "system.string" Then
                            AddSqlProcParameter("@" & sCurrentFieldName, dtReturn.Rows(0).Item(i), SqlDbType.NVarChar)
                        ElseIf stype.ToLower = "system.integer" Then
                            AddSqlProcParameter("@" & sCurrentFieldName, dtReturn.Rows(0).Item(i), SqlDbType.Int, 4)
                        ElseIf stype.ToLower = "system.int16" Then
                            AddSqlProcParameter("@" & sCurrentFieldName, dtReturn.Rows(0).Item(i), SqlDbType.Int, 4)
                        ElseIf stype.ToLower = "system.int32" Then
                            AddSqlProcParameter("@" & sCurrentFieldName, dtReturn.Rows(0).Item(i), SqlDbType.Int, 4)

                        ElseIf stype.ToLower = "system.byte" Then
                            AddSqlProcParameter("@" & sCurrentFieldName, dtReturn.Rows(0).Item(i), SqlDbType.Int, 1)

                        ElseIf stype.ToLower = "system.datetime" Then
                            AddSqlProcParameter("@" & sCurrentFieldName, dtReturn.Rows(0).Item(i), SqlDbType.DateTime, 16)

                        End If

                    End If
                Next

                If cmd.Connection Is Nothing Then
                    Dim conn = New SqlConnection()
                    Dim thisSetConn As New SetDBConnection()

                    ' conn.ConnectionString = thisSetConn.setConnection()

                    thisData.Connection = conn
                    conn.Open()

                    ans = thisData.ExecuteNonQuery

                    conn.Close()
                    conn = Nothing
                    thisSetConn = Nothing
                Else
                    cmd.Connection.Open()
                    ans = thisData.ExecuteNonQuery

                    cmd.Connection.Close()
                    cmd = Nothing
                End If

                thisData = Nothing
                Return dtReturn
            Catch ex As Exception

                sErr = ex.Message
                Throw ex
            End Try
        End Function

        Public Function UpdateFields(ByVal dtParams As DataTable, ByRef ParentControl As Control) As Integer
            Dim thisData As New SqlCommand
            thisData = cmd

            Dim requestnum As Integer

            Dim ans As Boolean
            Dim s As String = ""
            Try

                Dim i As Integer = 0
                Dim stxt As String = ""
                Dim iStart As Integer = 0
                Dim sCurrentFieldName As String = ""
                Dim sParamName As String
                Dim sCurrentControlName As String = ""
                Dim sCurrentValue As String = ""

                Dim iCurrentCalue As Integer = 0
                Dim sddl As String = ""
                Dim bFillParam As Boolean

                Dim bFoundControl As Boolean

                Dim alMissingControls As New ArrayList

                '==============
                '  Pats Logic
                '==============

                Dim drParam As DataRow


                '==============




                For Each drParam In dtParams.Rows

                    bFillParam = False
                    bFoundControl = False
                    sCurrentValue = ""
                    sParamName = drParam("ParameterName")
                    sParamName = sParamName.Replace("@", "")



                    '======Search for Textbox=====
                    If bFoundControl = False Then ' once found dont check again for this field
                        sCurrentControlName = sParamName & "TextBox"
                        Dim txt As New TextBox

                        txt = DataCheckingAndValidation.FindWebControlRecursive(ParentControl, sCurrentControlName)
                        If Not txt Is Nothing Then
                            bFoundControl = True
                            If Not String.IsNullOrEmpty(txt.Text) And sCurrentControlName.ToLower <> "commentstextbox" Then
                                Dim CharsToRemove(2) As Char
                                CharsToRemove(0) = "'"
                                CharsToRemove(1) = ";"
                                Dim arCharsToEsc As New ArrayList
                                arCharsToEsc.Add("'")
                                arCharsToEsc.Add("""")

                                If Not IsDate(txt.Text) Then
                                    txt.Text = DataCheckingAndValidation.RemoveNonCharacters(txt.Text, CharsToRemove, True, True)

                                End If

                                ' If txt.Text <> "" And txt.Text.Length > 0 Then
                                sCurrentValue = txt.Text
                                bFillParam = True
                            End If
                            If Not String.IsNullOrEmpty(txt.Text) And sCurrentControlName.ToLower = "commentstextbox" Then
                                Dim CharsToRemove(2) As Char
                                CharsToRemove(0) = "'"
                                CharsToRemove(1) = ";"
                                Dim arCharsToEsc As New ArrayList
                                arCharsToEsc.Add("'")
                                arCharsToEsc.Add("""")

                                If Not IsDate(txt.Text) Then
                                    txt.Text = DataCheckingAndValidation.RemoveNonCharacters(txt.Text, CharsToRemove, True)

                                End If


                                sCurrentValue = txt.Text
                                bFillParam = True
                            End If

                        End If
                    End If
                    '======Search for Label=====
                    If bFoundControl = False Then ' once found dont check again for this field
                        sCurrentControlName = sParamName & "Label"
                        Dim lbl As New Label

                        lbl = DataCheckingAndValidation.FindWebControlRecursive(ParentControl, sCurrentControlName)
                        If Not lbl Is Nothing Then
                            bFoundControl = True
                            If Not String.IsNullOrEmpty(lbl.Text) Then

                                sCurrentValue = lbl.Text
                                bFillParam = True
                            End If
                        End If
                    End If
                    '=====search for DropDownList====================== 
                    If bFoundControl = False Then
                        sCurrentControlName = sParamName & "ddl"
                        Dim ddl As New DropDownList


                        ddl = DataCheckingAndValidation.FindWebControlRecursive(ParentControl, sCurrentControlName)
                        If Not ddl Is Nothing Then
                            bFoundControl = True
                            If ddl.SelectedIndex > -1 And ddl.SelectedValue.ToLower.IndexOf("select") < 0 Then

                                sCurrentValue = ddl.SelectedValue
                                bFillParam = True
                            End If

                        End If
                    End If
                    '=====Search for RadioButtonList===================== 
                    If bFoundControl = False Then
                        sCurrentControlName = sParamName & "rbl" 'Radio button list
                        Dim rbl As New RadioButtonList

                        rbl = DataCheckingAndValidation.FindWebControlRecursive(ParentControl, sCurrentControlName)
                        If Not rbl Is Nothing Then
                            bFoundControl = True
                            If rbl.SelectedIndex > -1 Then
                                sCurrentValue = rbl.SelectedValue
                                bFillParam = True

                            End If
                        End If
                    Else
                    End If

                    '=====Search for Listbox===================== 
                    If bFoundControl = False Then
                        sCurrentControlName = sParamName & "Listbox" 'Radio button list
                        Dim Listbox As New ListBox

                        Listbox = DataCheckingAndValidation.FindWebControlRecursive(ParentControl, sCurrentControlName)
                        If Not Listbox Is Nothing Then
                            bFoundControl = True
                            If Listbox.SelectedIndex > -1 Then
                                sCurrentValue = Listbox.SelectedValue
                                bFillParam = True

                            End If
                        End If
                    Else 'control no found

                    End If

                    If bFoundControl = False Then



                    End If
                    Dim iLen As Integer = 0 ' dtFieldList.Columns.Item(i).MaxLength
                    If bFillParam And sCurrentValue <> "" Then 'has value needs a paramemter
                        Dim sqlParamType As String = drParam("ParameterType")

                        Dim iParamLength As Integer


                        iParamLength = CType(drParam("ParameterSize"), Integer)


                        Select Case sqlParamType
                            Case "Int"
                                AddSqlProcParameter("@" & sParamName, sCurrentValue, SqlDbType.Int, iParamLength)

                            Case "NvarChar"
                                AddSqlProcParameter("@" & sParamName, sCurrentValue, SqlDbType.NVarChar, iParamLength)

                            Case "NChar"
                                AddSqlProcParameter("@" & sParamName, sCurrentValue, SqlDbType.NChar, iParamLength)

                            Case "Date"
                                AddSqlProcParameter("@" & sParamName, sCurrentValue, SqlDbType.Date, iParamLength)

                            Case "DateTime"
                                AddSqlProcParameter("@" & sParamName, sCurrentValue, SqlDbType.DateTime, iParamLength)

                            Case Else
                                AddSqlProcParameter("@" & sParamName, sCurrentValue, SqlDbType.NVarChar, 100)
                        End Select









                    End If




                Next
                Dim sAns As String = ""
                If cmd.Connection Is Nothing Then

                Else
                    cmd.Connection.Open()
                    ' ans = thisData.ExecuteNonQuery



                    Dim dreader As SqlClient.SqlDataReader



                    dreader = cmd.ExecuteReader()
                    If dreader.Read() Then
                        If Not IsDBNull(dreader(0)) Then
                            requestnum = dreader(0)
                        End If
                    End If


                    cmd.Connection.Close()
                    cmd = Nothing
                End If


                If ans Then

                    alMissingControls.Insert(0, "Information has been Saved " & sAns)
                Else ' no answer
                    alMissingControls.Insert(0, "Not Saved")
                End If

                thisData = Nothing


                Return requestnum
            Catch ex As Exception

                sErr = ex.Message
                Throw ex
            End Try
        End Function
        Public Function UpdateFields(ByVal dtParams As DataTable, ByVal dtTableValues As DataTable, ByRef ParentControl As Control) As Integer
            Dim thisData As New SqlCommand
            thisData = cmd

            Dim requestnum As Integer

            Dim ans As Boolean
            Dim s As String = ""
            Try

                Dim i As Integer = 0
                Dim stxt As String = ""
                Dim iStart As Integer = 0
                Dim sCurrentFieldName As String = ""
                Dim sParamName As String
                Dim sCurrentControlName As String = ""
                Dim sCurrentValue As String = ""

                Dim iCurrentCalue As Integer = 0
                Dim sddl As String = ""
                Dim bFillParam As Boolean

                Dim bFoundControl As Boolean

                Dim alMissingControls As New ArrayList

                '==============
                '  Pats Logic
                '==============

                Dim drParam As DataRow


                '==============




                For Each drParam In dtParams.Rows

                    bFillParam = False
                    bFoundControl = False
                    sCurrentValue = ""
                    sParamName = drParam("ParameterName")
                    sParamName = sParamName.Replace("@", "")



                    '======Search for Textbox=====
                    If bFoundControl = False Then ' once found dont check again for this field
                        sCurrentControlName = sParamName & "TextBox"
                        Dim txt As New TextBox

                        txt = DataCheckingAndValidation.FindWebControlRecursive(ParentControl, sCurrentControlName)
                        If Not txt Is Nothing And (dtTableValues.Rows(0).Item(sParamName)) Then
                            bFoundControl = True


                            If Not String.IsNullOrEmpty(txt.Text) And sCurrentControlName.ToLower <> "commentstextbox" Then
                                Dim CharsToRemove(2) As Char
                                CharsToRemove(0) = "'"
                                CharsToRemove(1) = ";"
                                Dim arCharsToEsc As New ArrayList
                                arCharsToEsc.Add("'")
                                arCharsToEsc.Add("""")

                                If Not IsDate(txt.Text) Then

                                    txt.Text = DataCheckingAndValidation.RemoveNonCharacters(txt.Text, CharsToRemove, True, True)

                                End If

                                ' If txt.Text <> "" And txt.Text.Length > 0 Then
                                sCurrentValue = txt.Text
                                bFillParam = True
                            End If
                            If Not String.IsNullOrEmpty(txt.Text) And sCurrentControlName.ToLower = "commentstextbox" Then
                                Dim CharsToRemove(2) As Char
                                CharsToRemove(0) = "'"
                                CharsToRemove(1) = ";"
                                Dim arCharsToEsc As New ArrayList
                                arCharsToEsc.Add("'")
                                arCharsToEsc.Add("""")

                                If Not IsDate(txt.Text) Then
                                    txt.Text = DataCheckingAndValidation.RemoveNonCharacters(txt.Text, CharsToRemove, True)

                                End If


                                sCurrentValue = txt.Text
                                bFillParam = True
                            End If

                        End If
                    End If
                    '======Search for Label=====
                    If bFoundControl = False Then ' once found dont check again for this field
                        sCurrentControlName = sParamName & "Label"
                        Dim lbl As New Label

                        lbl = DataCheckingAndValidation.FindWebControlRecursive(ParentControl, sCurrentControlName)
                        If Not lbl Is Nothing Then
                            bFoundControl = True
                            If Not String.IsNullOrEmpty(lbl.Text) Then

                                sCurrentValue = lbl.Text
                                bFillParam = True
                            End If
                        End If
                    End If
                    '=====search for DropDownList====================== 
                    If bFoundControl = False Then
                        sCurrentControlName = sParamName & "ddl"
                        Dim ddl As New DropDownList


                        ddl = DataCheckingAndValidation.FindWebControlRecursive(ParentControl, sCurrentControlName)
                        If Not ddl Is Nothing Then
                            bFoundControl = True
                            If ddl.SelectedIndex > -1 And ddl.SelectedValue.ToLower.IndexOf("select") < 0 Then

                                sCurrentValue = ddl.SelectedValue
                                bFillParam = True
                            End If

                        End If
                    End If
                    '=====Search for RadioButtonList===================== 
                    If bFoundControl = False Then
                        sCurrentControlName = sParamName & "rbl" 'Radio button list
                        Dim rbl As New RadioButtonList

                        rbl = DataCheckingAndValidation.FindWebControlRecursive(ParentControl, sCurrentControlName)
                        If Not rbl Is Nothing Then
                            bFoundControl = True
                            If rbl.SelectedIndex > -1 Then
                                sCurrentValue = rbl.SelectedValue
                                bFillParam = True

                            End If
                        End If
                    Else
                    End If


                    '=====Search for listbox===================== 
                    If bFoundControl = False Then
                        sCurrentControlName = sParamName & "Listbox" 'Radio button list
                        Dim Listbox As New ListBox

                        Listbox = DataCheckingAndValidation.FindWebControlRecursive(ParentControl, sCurrentControlName)
                        If Not Listbox Is Nothing Then
                            bFoundControl = True
                            If Listbox.SelectedIndex > -1 Then
                                sCurrentValue = Listbox.SelectedValue
                                bFillParam = True

                            End If
                        End If
                    Else
                    End If

                    If bFoundControl = False Then



                    End If
                    Dim iLen As Integer = 0 ' dtFieldList.Columns.Item(i).MaxLength
                    If bFillParam And sCurrentValue <> "" Then 'has value needs a paramemter
                        Dim sqlParamType As String = drParam("ParameterType")

                        Dim iParamLength As Integer


                        iParamLength = CType(drParam("ParameterSize"), Integer)


                        Select Case sqlParamType
                            Case "Int"
                                AddSqlProcParameter("@" & sParamName, sCurrentValue, SqlDbType.Int, iParamLength)

                            Case "NvarChar"
                                AddSqlProcParameter("@" & sParamName, sCurrentValue, SqlDbType.NVarChar, iParamLength)

                            Case "NChar"
                                AddSqlProcParameter("@" & sParamName, sCurrentValue, SqlDbType.NChar, iParamLength)

                            Case "Date"
                                AddSqlProcParameter("@" & sParamName, sCurrentValue, SqlDbType.Date, iParamLength)

                            Case "DateTime"
                                AddSqlProcParameter("@" & sParamName, sCurrentValue, SqlDbType.DateTime, iParamLength)

                            Case Else
                                AddSqlProcParameter("@" & sParamName, sCurrentValue, SqlDbType.NVarChar, 100)
                        End Select









                    End If




                Next
                Dim sAns As String = ""
                If cmd.Connection Is Nothing Then

                Else
                    cmd.Connection.Open()
                    ' ans = thisData.ExecuteNonQuery



                    Dim dreader As SqlClient.SqlDataReader



                    dreader = cmd.ExecuteReader()
                    If dreader.Read() Then
                        If Not IsDBNull(dreader(0)) Then
                            requestnum = dreader(0)
                        End If
                    End If


                    cmd.Connection.Close()
                    cmd = Nothing
                End If


                If ans Then

                    alMissingControls.Insert(0, "Information has been Saved " & sAns)
                Else ' no answer
                    alMissingControls.Insert(0, "Not Saved")
                End If

                thisData = Nothing


                Return requestnum
            Catch ex As Exception

                sErr = ex.Message
                Throw ex
            End Try
        End Function
    End Class

    'End Namespace
End Namespace