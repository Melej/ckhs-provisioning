'Imports System.Web
'Imports System.Web.UI
Imports System.Collections
Imports System.Data.SqlClient
Imports System.Data
Imports System.Data.OleDb
Imports System.Web.HttpContext
Imports System.Web.HttpRequest

Namespace App_Code



    Public Class SqlHelper
        Protected cmd As SqlCommand 'Shared 
        Protected ds As DataSet '  Shared 
        Dim sErr As String = ""

        Public Sub AddSqlProcParameter(ByVal sArgName As String, ByVal sArgValue As String, _
                                 ByVal sDataType As SqlDbType)

            Dim sParam As New SqlParameter

            sParam.ParameterName = sArgName
            sParam.SqlDbType = sDataType
            sParam.Value = sArgValue
            cmd.Parameters.Add(sParam)

        End Sub
        Public Sub AddSqlProcParameter(ByVal sArgName As String, ByVal sArgValue As String, _
                                       ByVal sDataType As SqlDbType, ByVal iLength As Integer)

            Dim sParam As New SqlParameter
            sParam.ParameterName = sArgName

            sParam.SqlDbType = sDataType
            sParam.Size = iLength
            sParam.Value = sArgValue
            cmd.Parameters.Add(sParam)

        End Sub
        Public Sub AddSqlProcParameter(ByVal sArgName As String, ByVal sArgValue As String, _
                                       ByVal sDataType As SqlDbType, ByVal iLength As Integer, ByVal sDir As ParameterDirection)
            Dim sParam As New SqlParameter
            sParam.ParameterName = sArgName

            sParam.SqlDbType = sDataType
            sParam.Size = iLength
            sParam.Value = sArgValue
            sParam.Direction = sDir
            cmd.Parameters.Add(sParam)


        End Sub
        Public Shared Function getSqlConn(ByVal sConnStringIn As String) As SqlConnection
             Dim ConnCallProcs As New System.Data.SqlClient.SqlConnection(sConnStringIn)
            Return ConnCallProcs
        End Function
        

        Sub New(ByVal sProc As String, ByVal sConnString As String)

            cmd = New SqlCommand
            cmd.Parameters.Clear()
            cmd.Connection = getSqlConn(sConnString)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = sProc
            'setDefaults()
        End Sub
        Public Sub setCommandText(ByVal sProc As String)
            cmd.CommandText = sProc
        End Sub
        Public Sub setTimeount(ByVal iTime As Integer)
            cmd.CommandTimeout = iTime


        End Sub
        Public Sub setCommandType(ByVal sqlcommand As CommandType)
            cmd.CommandType = sqlcommand
        End Sub
 
        
        Public Function GetSqlDataTable() As DataTable
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            Dim ds As New DataSet
            Dim dt As DataTable
            Dim iCount As Integer
            da.Fill(ds)
            If ds.Tables.Count > 0 Then
                'iCount = ds.Tables(0).Rows.Count
                dt = ds.Tables(0)
            End If

            'new
            ' dt.Dispose()
            ds.Dispose()
            da.Dispose()
            Return dt
        End Function
        Public Function GetScalarReturnInt() As Integer
            cmd.Connection.Open()
            GetScalarReturnInt = cmd.ExecuteScalar
            cmd.Connection.Close()
            cmd.Dispose()
        End Function
        Public Function GetScalarReturnString() As String
            cmd.Connection.Open()
            GetScalarReturnString = cmd.ExecuteScalar
            cmd.Connection.Close()
            cmd.Dispose()
        End Function
        Public Function ExecNonQueryReturnBoolean() As Boolean
            cmd.Connection.Open()
            ExecNonQueryReturnBoolean = cmd.ExecuteNonQuery()
            cmd.Connection.Close()
            cmd.Dispose()
        End Function
        Public Function ExecNonQueryReturnInt() As Integer
            cmd.Connection.Open()
            ExecNonQueryReturnInt = cmd.ExecuteNonQuery()
            cmd.Connection.Close()

            cmd.Dispose()
        End Function
        Public Sub ExecNonQueryNoReturn()
            cmd.Connection.Open()
            cmd.ExecuteNonQuery()
            cmd.Connection.Close()
            cmd.Dispose()
        End Sub
         
        Public Shared Function FillFields(ByRef dtFieldList As DataTable, ByRef ParentControl As Control) As DataTable



            Dim dtReturn As New DataTable
            dtReturn.Columns.Add("FieldName", GetType(String))
            dtReturn.Columns.Add("FieldValue", GetType(String))



            Dim i As Integer = 0
            Dim stxt As String = ""
            Dim iStart As Integer = 0
            Dim sCurrentFieldName As String = ""
            Dim sCurrentControlName As String = ""
            Dim sCurrentValue As String = ""

            Dim bFoundControl As Boolean
            Dim iCurrentCalue As Integer = 0
            Dim sddl As String = ""



            If dtFieldList Is Nothing Then

                Return dtReturn
                Exit Function

            Else
                If dtFieldList.Rows.Count > 0 Then

                    For i = iStart To dtFieldList.Columns.Count - 1 '0 is casenum
                        'search for textbox
                        bFoundControl = False

                        sCurrentFieldName = dtFieldList.Columns(i).ColumnName.ToString()
                        '===check for textbox control===
                        sCurrentControlName = dtFieldList.Columns(i).ColumnName.ToString() & "TextBox"

                        Dim txt As New TextBox

                        txt = DataCheckingAndValidation.FindWebControlRecursive(ParentControl, sCurrentControlName)
                        If Not txt Is Nothing Then
                            bFoundControl = True
                            If txt.TextMode = TextBoxMode.MultiLine Then

                                'txt.Text = dtFieldList.Rows(0).Item(i)

                            End If
                            If Not dtFieldList.Rows(0).IsNull(i) Then

                                txt.Text = dtFieldList.Rows(0).Item(i)

                            End If

                            'Dim mycol As DataColumn = dtFieldList.Columns(i)
                            'Dim sFind As String = "fieldname='" & dtFieldList.Columns(i).ColumnName & "'"
                            'sFind = "ParameterName='" & dtFieldList.Columns(i).ColumnName & "'"
                            'Dim foundrows As DataRow()
                            'Dim iLen As Integer
                            'foundrows = dtFieldList.Select(sFind)
                            ''nope iLen = dtFieldList.Columns(i).MaxLength
                            ''If Not foundrows Is Nothing Then
                            'If foundrows.Length > 0 Then
                            '    iLen = foundrows(0).Item("ParameterSize")
                            '    If iLen > 0 Then
                            '        txt.MaxLength = iLen
                            '    End If
                            'Else 'not in schema table
                            '    If IsDate(dtFieldList.Rows(0).Item(i)) Then
                            '        iLen = 24
                            '    ElseIf IsNumeric(dtFieldList.Rows(0).Item(i)) Then
                            '        iLen = 4
                            '    Else 'String
                            '        iLen = 49
                            '    End If


                            'End If
                        End If
                        ''===check for dropdownlist control===search for ddl
                        sCurrentControlName = dtFieldList.Columns(i).ColumnName.ToString() & "ddl"
                        Dim ddl As New DropDownList
                        Dim lstItem As ListItem
                        ddl = DataCheckingAndValidation.FindWebControlRecursive(ParentControl, sCurrentControlName)
                        If Not ddl Is Nothing Then
                            bFoundControl = True
                            ' If Not dtFieldList.Rows(0).IsNull(i) Then
                            ' If ddl.SelectedIndex > 0 Then
                            If dtFieldList.Rows(0).IsNull(i) Or dtFieldList.Rows(0).Item(i).ToString() = "" Then
                                ' no value for ddl to be set to  ddl.SelectedValue = 1
                                If ddl.Items.Count >= 1 Then
                                    If ddl.Items(0).Value.ToLower.IndexOf("select") < 0 Then
                                        ddl.Items.Insert(0, " Select One")
                                        ddl.SelectedIndex = 0
                                    End If
                                Else 'item count none
                                    ddl.Items.Insert(0, " Select One")
                                    ddl.SelectedIndex = 0
                                End If
                            Else 'field has a value to put into ddl
                                If ddl.Items.Count > 1 Then
                                    lstItem = ddl.Items.FindByValue(dtFieldList.Rows(0).Item(i))
                                    ' ddl.SelectedValue = dtFieldList.Rows(0).Item(i)
                                    If Not lstItem Is Nothing Then
                                        ddl.SelectedValue = dtFieldList.Rows(0).Item(i)
                                    Else
                                        ddl.SelectedIndex = 0
                                    End If
                                Else ' not greater then 0 
                                    If ddl.Items.Count = 1 Then
                                        If ddl.Items(0).Value.ToLower.IndexOf("select") < 0 Then
                                            ddl.Items.Insert(0, "Select One")
                                            ddl.SelectedIndex = 0
                                        End If
                                    Else 'item count none
                                        ddl.Items.Insert(0, "Select One")
                                        ddl.SelectedIndex = 0
                                    End If
                                End If
                            End If


                        End If
                        '===check for Radio Button  control===
                        sCurrentControlName = dtFieldList.Columns(i).ColumnName.ToString() & "rbl" 'Radio button list
                        Dim rbl As New RadioButtonList
                        rbl = DataCheckingAndValidation.FindWebControlRecursive(ParentControl, sCurrentControlName)
                        If Not rbl Is Nothing Then
                            bFoundControl = True
                            If Not dtFieldList.Rows(0).IsNull(i) AndAlso CType(dtFieldList.Rows(0).Item(i).ToString.Trim, String) <> "" Then
                                rbl.SelectedValue = dtFieldList.Rows(0).Item(i)
                            Else


                            End If
                        End If
                        '===check for Label control===
                        sCurrentControlName = dtFieldList.Columns(i).ColumnName.ToString.ToLower() & "Label" 'Radio button list
                        Dim lbl As New Label
                        lbl = DataCheckingAndValidation.FindWebControlRecursive(ParentControl, sCurrentControlName)
                        If Not lbl Is Nothing Then
                            bFoundControl = True
                            If Not dtFieldList.Rows(0).IsNull(i) Then
                                lbl.Text = dtFieldList.Rows(0).Item(i)


                            End If
                        End If
                        '=======

                        If bFoundControl = False Then 'control not for this field
                            dtReturn.Rows.Add(dtFieldList.Columns(i).ColumnName, dtFieldList.Rows(0).Item(i))
                        End If
                    Next
                End If
            End If
            Return dtReturn

        End Function

      

        Public Function UpdatedFieldsTable(ByVal dtFieldList As DataTable, ByRef ParentControl As Control) As DataTable
            Dim thisData As New SqlCommand
            thisData = cmd 'CommandReadyForParameters


            Dim ans As Boolean
            Dim s As String = ""
            Try 'mother baby upd

                Dim i As Integer = 0
                Dim stxt As String = ""
                Dim iStart As Integer = 0
                Dim sCurrentFieldName As String = ""
                Dim sCurrentControlName As String = ""
                Dim sCurrentValue As String = ""

                Dim iCurrentCalue As Integer = 0
                Dim sddl As String = ""
                Dim bFillParam As Boolean

                Dim bFoundControl As Boolean

                '==============
                ' Pats logic
                '==============
                Dim dtReturn As DataTable

                dtReturn = dtFieldList.Copy()


                '=============







                For i = 0 To dtFieldList.Columns.Count - 1 '0 is casenum
                    'search for textbox
                    bFillParam = False
                    bFoundControl = False






                    sCurrentFieldName = dtFieldList.Columns(i).ColumnName.ToString()

                    If sCurrentFieldName = "PatientName" Then
                        bFoundControl = True

                    End If


                    '======Search for Textbox=====
                    If bFoundControl = False Then ' once found dont check again for this field
                        sCurrentControlName = dtFieldList.Columns(i).ColumnName.ToString() & "TextBox"
                        Dim txt As New TextBox

                        txt = DataCheckingAndValidation.FindWebControlRecursive(ParentControl, sCurrentControlName)
                        If Not txt Is Nothing Then
                            bFoundControl = True
                            If Not String.IsNullOrEmpty(txt.Text) And sCurrentControlName.ToLower <> "commentstextbox" Then
                                Dim CharsToRemove(2) As Char
                                CharsToRemove(0) = "'"
                                CharsToRemove(1) = ";"
                                Dim arCharsToEsc As New ArrayList
                                arCharsToEsc.Add("'")
                                arCharsToEsc.Add("""")

                                If Not IsDate(txt.Text) Then
                                    txt.Text = DataCheckingAndValidation.RemoveNonCharacters(txt.Text, CharsToRemove, True, True)

                                End If


                                dtReturn.Rows(0).Item(i) = txt.Text
                                bFillParam = True
                            End If
                            If Not String.IsNullOrEmpty(txt.Text) And sCurrentControlName.ToLower = "commentstextbox" Then
                                Dim CharsToRemove(2) As Char
                                CharsToRemove(0) = "'"
                                CharsToRemove(1) = ";"
                                Dim arCharsToEsc As New ArrayList
                                arCharsToEsc.Add("'")
                                arCharsToEsc.Add("""")

                                If Not IsDate(txt.Text) Then
                                    txt.Text = DataCheckingAndValidation.RemoveNonCharacters(txt.Text, CharsToRemove, True)

                                End If


                                dtReturn.Rows(0).Item(i) = txt.Text
                                bFillParam = True
                            End If

                        End If
                    End If
                    '======Search for Label=====
                    If bFoundControl = False Then ' once found dont check again for this field
                        sCurrentControlName = dtFieldList.Columns(i).ColumnName.ToString() & "Label"
                        Dim lbl As New Label


                        lbl = DataCheckingAndValidation.FindWebControlRecursive(ParentControl, sCurrentControlName)
                        If Not lbl Is Nothing Then
                            bFoundControl = True
                            If Not String.IsNullOrEmpty(lbl.Text) Then

                                dtReturn.Rows(0).Item(i) = lbl.Text
                                bFillParam = True
                            End If
                        End If
                    End If
                    '=====search for DropDownList====================== 
                    If bFoundControl = False Then
                        sCurrentControlName = dtFieldList.Columns(i).ColumnName.ToString() & "ddl"
                        Dim ddl As New DropDownList


                        ddl = DataCheckingAndValidation.FindWebControlRecursive(ParentControl, sCurrentControlName)
                        If Not ddl Is Nothing Then
                            bFoundControl = True
                            If ddl.SelectedIndex > -1 And ddl.SelectedValue.ToLower.IndexOf("select") < 0 Then
                                'has a selected inedx and a vlaue other than select
                                dtReturn.Rows(0).Item(i) = ddl.SelectedValue
                                bFillParam = True
                            End If

                        End If
                    End If
                    '=====Search for RadioButtonList===================== 
                    If bFoundControl = False Then
                        sCurrentControlName = dtFieldList.Columns(i).ColumnName.ToString() & "rbl" 'Radio button list
                        Dim rbl As New RadioButtonList

                        rbl = DataCheckingAndValidation.FindWebControlRecursive(ParentControl, sCurrentControlName)
                        If Not rbl Is Nothing Then
                            bFoundControl = True
                            If rbl.SelectedIndex > -1 Then
                                dtReturn.Rows(0).Item(i) = rbl.SelectedValue
                                bFillParam = True

                            End If
                        End If
                    Else 'control no found

                    End If
                    If bFoundControl = False Then

                    End If
                    Dim iLen As Integer = 0 ' dtFieldList.Columns.Item(i).MaxLength
                    If bFillParam And (Not dtReturn.Rows(0).IsNull(i)) Then 'has value needs a paramemter
                        ' ''==get length




                        Dim stype As String = dtReturn.Columns(i).DataType.FullName.ToString.ToLower
                        If stype.ToLower = "system.string" Then
                            AddSqlProcParameter("@" & sCurrentFieldName, dtReturn.Rows(0).Item(i), SqlDbType.NVarChar)
                        ElseIf stype.ToLower = "system.integer" Then
                            AddSqlProcParameter("@" & sCurrentFieldName, dtReturn.Rows(0).Item(i), SqlDbType.Int, 4)
                        ElseIf stype.ToLower = "system.int16" Then
                            AddSqlProcParameter("@" & sCurrentFieldName, dtReturn.Rows(0).Item(i), SqlDbType.Int, 4)
                        ElseIf stype.ToLower = "system.int32" Then
                            AddSqlProcParameter("@" & sCurrentFieldName, dtReturn.Rows(0).Item(i), SqlDbType.Int, 4)

                        ElseIf stype.ToLower = "system.byte" Then
                            AddSqlProcParameter("@" & sCurrentFieldName, dtReturn.Rows(0).Item(i), SqlDbType.Int, 1)

                        ElseIf stype.ToLower = "system.datetime" Then
                            AddSqlProcParameter("@" & sCurrentFieldName, dtReturn.Rows(0).Item(i), SqlDbType.DateTime, 16)

                        End If

                    End If
                Next

                If cmd.Connection Is Nothing Then
                    Dim conn = New SqlConnection()
                    Dim thisSetConn As New SetDBConnection()

                    ' conn.ConnectionString = thisSetConn.setConnection()

                    thisData.Connection = conn
                    conn.Open()

                    ans = thisData.ExecuteNonQuery

                    conn.Close()
                    conn = Nothing
                    thisSetConn = Nothing
                Else
                    cmd.Connection.Open()
                    ans = thisData.ExecuteNonQuery

                    cmd.Connection.Close()
                    cmd = Nothing
                End If

                thisData = Nothing
                Return dtReturn
            Catch ex As Exception

                sErr = ex.Message
                Throw ex
            End Try
        End Function

        Public Function UpdateFields(ByVal dtParams As DataTable, ByRef ParentControl As Control) As Integer
            Dim thisData As New SqlCommand
            thisData = cmd

            Dim requestnum As Integer

            Dim ans As Boolean
            Dim s As String = ""
            Try

                Dim i As Integer = 0
                Dim stxt As String = ""
                Dim iStart As Integer = 0
                Dim sCurrentFieldName As String = ""
                Dim sParamName As String
                Dim sCurrentControlName As String = ""
                Dim sCurrentValue As String = ""

                Dim iCurrentCalue As Integer = 0
                Dim sddl As String = ""
                Dim bFillParam As Boolean

                Dim bFoundControl As Boolean

                Dim alMissingControls As New ArrayList

                '==============
                '  Pats Logic
                '==============

                Dim drParam As DataRow


                '==============




                For Each drParam In dtParams.Rows

                    bFillParam = False
                    bFoundControl = False
                    sCurrentValue = ""
                    sParamName = drParam("ParameterName")
                    sParamName = sParamName.Replace("@", "")



                    '======Search for Textbox=====
                    ' If bFoundControl = False Then ' once found dont check again for this field
                    sCurrentControlName = sParamName & "TextBox"
                    Dim txt As New TextBox

                    txt = DataCheckingAndValidation.FindWebControlRecursive(ParentControl, sCurrentControlName)
                    If Not txt Is Nothing Then
                        bFoundControl = True
                        If Not String.IsNullOrEmpty(txt.Text) And sCurrentControlName.ToLower <> "commentstextbox" Then
                            Dim CharsToRemove(2) As Char
                            CharsToRemove(0) = "'"
                            CharsToRemove(1) = ";"
                            Dim arCharsToEsc As New ArrayList
                            arCharsToEsc.Add("'")
                            arCharsToEsc.Add("""")

                            If Not IsDate(txt.Text) Then
                                txt.Text = DataCheckingAndValidation.RemoveNonCharacters(txt.Text, CharsToRemove, True, True)

                            End If

                            ' If txt.Text <> "" And txt.Text.Length > 0 Then
                            sCurrentValue = txt.Text
                            bFillParam = True
                        End If
                        If Not String.IsNullOrEmpty(txt.Text) And sCurrentControlName.ToLower = "commentstextbox" Then
                            Dim CharsToRemove(2) As Char
                            CharsToRemove(0) = "'"
                            CharsToRemove(1) = ";"
                            Dim arCharsToEsc As New ArrayList
                            arCharsToEsc.Add("'")
                            arCharsToEsc.Add("""")

                            If Not IsDate(txt.Text) Then
                                txt.Text = DataCheckingAndValidation.RemoveNonCharacters(txt.Text, CharsToRemove, True)

                            End If


                            sCurrentValue = txt.Text
                            bFillParam = True
                        End If

                    End If
                    '  End If
                    '======Search for Label=====
                    If bFoundControl = False Then ' once found dont check again for this field
                        sCurrentControlName = sParamName & "Label"
                        Dim lbl As New Label

                        lbl = DataCheckingAndValidation.FindWebControlRecursive(ParentControl, sCurrentControlName)
                        If Not lbl Is Nothing Then
                            bFoundControl = True
                            If Not String.IsNullOrEmpty(lbl.Text) Then

                                sCurrentValue = lbl.Text
                                bFillParam = True
                            End If
                        End If
                    End If
                    '=====search for DropDownList====================== 
                    '   If bFoundControl = False Then
                    sCurrentControlName = sParamName & "ddl"
                    Dim ddl As New DropDownList


                    ddl = DataCheckingAndValidation.FindWebControlRecursive(ParentControl, sCurrentControlName)
                    If Not ddl Is Nothing Then
                        bFoundControl = True
                        If ddl.SelectedIndex > -1 And ddl.SelectedValue.ToLower.IndexOf("select") < 0 Then

                            sCurrentValue = ddl.SelectedValue
                            bFillParam = True
                        End If

                    End If
                    '  End If
                    '=====Search for RadioButtonList===================== 
                    '  If bFoundControl = False Then
                    sCurrentControlName = sParamName & "rbl" 'Radio button list
                    Dim rbl As New RadioButtonList

                    rbl = DataCheckingAndValidation.FindWebControlRecursive(ParentControl, sCurrentControlName)
                    If Not rbl Is Nothing Then
                        bFoundControl = True
                        If rbl.SelectedIndex > -1 Then
                            sCurrentValue = rbl.SelectedValue
                            bFillParam = True

                        End If
                    End If
                    'Else
                    'End If
                    'If bFoundControl = False Then



                    'End If
                    Dim iLen As Integer = 0 ' dtFieldList.Columns.Item(i).MaxLength
                    If bFillParam And sCurrentValue <> "" Then 'has value needs a paramemter
                        Dim sqlParamType As String = drParam("ParameterType")

                        Dim iParamLength As Integer


                        iParamLength = CType(drParam("ParameterSize"), Integer)


                        Select Case sqlParamType.ToLower
                            Case "int"
                                AddSqlProcParameter("@" & sParamName, sCurrentValue, SqlDbType.Int, iParamLength)

                            Case "nvarchar"
                                AddSqlProcParameter("@" & sParamName, sCurrentValue, SqlDbType.NVarChar, iParamLength)

                            Case "nchar"
                                AddSqlProcParameter("@" & sParamName, sCurrentValue, SqlDbType.NChar, iParamLength)

                            Case "date"
                                AddSqlProcParameter("@" & sParamName, sCurrentValue, SqlDbType.Date, iParamLength)

                            Case "datetime"
                                AddSqlProcParameter("@" & sParamName, sCurrentValue, SqlDbType.DateTime, iParamLength)

                            Case Else
                                AddSqlProcParameter("@" & sParamName, sCurrentValue, SqlDbType.NVarChar, 100)
                        End Select









                    End If




                Next
                Dim sAns As String = ""
                If cmd.Connection Is Nothing Then

                Else
                    cmd.Connection.Open()
                    ' ans = thisData.ExecuteNonQuery



                    Dim dreader As SqlClient.SqlDataReader



                    dreader = cmd.ExecuteReader()
                    If dreader.Read() Then
                        If Not IsDBNull(dreader(0)) Then
                            requestnum = dreader(0)
                        End If
                    End If


                    cmd.Connection.Close()
                    cmd = Nothing
                End If


                If ans Then

                    alMissingControls.Insert(0, "Information has been Saved " & sAns)
                Else ' no answer
                    alMissingControls.Insert(0, "Not Saved")
                End If

                thisData = Nothing


                Return requestnum
            Catch ex As Exception

                sErr = ex.Message
                Throw ex
            End Try
        End Function
        Public Function UpdateFields(ByVal dtParams As DataTable, ByVal dtTableValues As DataTable, ByRef ParentControl As Control) As Integer
            Dim thisData As New SqlCommand
            thisData = cmd

            Dim requestnum As Integer
            Dim sCurrGlobal As String

            Dim ans As Boolean
            Dim s As String = ""
            Try

                Dim i As Integer = 0
                Dim stxt As String = ""
                Dim iStart As Integer = 0
                Dim sCurrentFieldName As String = ""
                Dim sParamName As String
                Dim sOrivinalValue As String = ""
                Dim sCurrentControlName As String = ""
                Dim sCurrentValue As String = ""

                Dim iCurrentCalue As Integer = 0
                Dim sddl As String = ""
                Dim bFillParam As Boolean

                Dim bFoundControl As Boolean

                Dim alMissingControls As New ArrayList

                '==============
                '  Pats Logic
                '==============

                Dim drParam As DataRow


                '==============




                For Each drParam In dtParams.Rows

                    bFillParam = False
                    bFoundControl = False
                    sCurrentValue = ""
                    sParamName = drParam("ParameterName")
                    sParamName = sParamName.Replace("@", "")
                    sCurrGlobal = sParamName
                    '   sOrivinalValue = IIf(dtTableValues.Rows.Count = 0, "", dtTableValues.Rows(0).Item(sParamName).ToString())

                    If dtTableValues.Rows.Count = 0 Then
                        sOrivinalValue = ""
                    Else
                        sOrivinalValue = dtTableValues.Rows(0).Item(sParamName).ToString()
                    End If


                    '======Search for Textbox=====
                    If bFoundControl = False Then ' once found dont check again for this field
                        sCurrentControlName = sParamName & "TextBox"
                        Dim txt As New TextBox

                        txt = DataCheckingAndValidation.FindWebControlRecursive(ParentControl, sCurrentControlName)
                        If Not txt Is Nothing Then 'And (dtTableValues.Rows(0).Item(sParamName)) Then
                            bFoundControl = True

                            If String.IsNullOrEmpty(txt.Text) And sOrivinalValue.Length > 0 Then


                                sCurrentValue = ""
                                bFillParam = True
                            End If


                            If Not String.IsNullOrEmpty(txt.Text) And sCurrentControlName.ToLower <> "commentstextbox" Then
                                Dim CharsToRemove(2) As Char
                                CharsToRemove(0) = "'"
                                CharsToRemove(1) = ";"
                                Dim arCharsToEsc As New ArrayList
                                arCharsToEsc.Add("'")
                                arCharsToEsc.Add("""")

                                If Not IsDate(txt.Text) Then

                                    txt.Text = DataCheckingAndValidation.RemoveNonCharacters(txt.Text, CharsToRemove, True, True)

                                End If

                                ' If txt.Text <> "" And txt.Text.Length > 0 Then
                                sCurrentValue = txt.Text
                                bFillParam = True
                            End If
                            If Not String.IsNullOrEmpty(txt.Text) And sCurrentControlName.ToLower = "commentstextbox" Then
                                Dim CharsToRemove(2) As Char
                                CharsToRemove(0) = "'"
                                CharsToRemove(1) = ";"
                                Dim arCharsToEsc As New ArrayList
                                arCharsToEsc.Add("'")
                                arCharsToEsc.Add("""")

                                If Not IsDate(txt.Text) Then
                                    txt.Text = DataCheckingAndValidation.RemoveNonCharacters(txt.Text, CharsToRemove, True)

                                End If


                                sCurrentValue = txt.Text
                                bFillParam = True
                            End If

                        End If
                    End If
                    '======Search for Label=====
                    If bFoundControl = False Then ' once found dont check again for this field
                        sCurrentControlName = sParamName & "Label"
                        Dim lbl As New Label

                        lbl = DataCheckingAndValidation.FindWebControlRecursive(ParentControl, sCurrentControlName)
                        If Not lbl Is Nothing Then
                            bFoundControl = True
                            If Not String.IsNullOrEmpty(lbl.Text) And lbl.Text.ToLower.IndexOf("select") < 0 Then

                                sCurrentValue = lbl.Text
                                bFillParam = True
                            End If
                        End If
                    End If
                    '=====search for DropDownList====================== 
                    If bFoundControl = False Then
                        sCurrentControlName = sParamName & "ddl"
                        Dim ddl As New DropDownList


                        ddl = DataCheckingAndValidation.FindWebControlRecursive(ParentControl, sCurrentControlName)
                        If Not ddl Is Nothing Then
                            bFoundControl = True
                            If ddl.SelectedIndex > -1 And ddl.SelectedValue.ToLower.IndexOf("select") < 0 Then

                                sCurrentValue = ddl.SelectedValue
                                bFillParam = True
                            End If

                        End If
                    End If
                    '=====Search for RadioButtonList===================== 
                    If bFoundControl = False Then
                        sCurrentControlName = sParamName & "rbl" 'Radio button list
                        Dim rbl As New RadioButtonList

                        rbl = DataCheckingAndValidation.FindWebControlRecursive(ParentControl, sCurrentControlName)
                        If Not rbl Is Nothing Then
                            bFoundControl = True
                            If rbl.SelectedIndex > -1 Then
                                sCurrentValue = rbl.SelectedValue
                                bFillParam = True

                            End If
                        End If
                    Else
                    End If
                    If bFoundControl = False Then



                    End If
                    Dim iLen As Integer = 0 ' dtFieldList.Columns.Item(i).MaxLength
                    If bFillParam Then
                        Dim sqlParamType As String = drParam("ParameterType")

                        Dim iParamLength As Integer


                        iParamLength = CType(drParam("ParameterSize"), Integer)


                        Select Case sqlParamType
                            Case "Int"
                                AddSqlProcParameter("@" & sParamName, sCurrentValue, SqlDbType.Int, iParamLength)

                            Case "NVarChar", "NvarChar"
                                AddSqlProcParameter("@" & sParamName, sCurrentValue, SqlDbType.NVarChar, iParamLength)

                            Case "NChar"
                                AddSqlProcParameter("@" & sParamName, sCurrentValue, SqlDbType.NChar, iParamLength)

                            Case "Date"
                                AddSqlProcParameter("@" & sParamName, sCurrentValue, SqlDbType.Date, iParamLength)

                            Case "DateTime"
                                AddSqlProcParameter("@" & sParamName, sCurrentValue, SqlDbType.DateTime, iParamLength)

                            Case Else
                                AddSqlProcParameter("@" & sParamName, sCurrentValue, SqlDbType.NVarChar, 100)
                        End Select









                    End If




                Next
                Dim sAns As String = ""
                If cmd.Connection Is Nothing Then

                Else
                    cmd.Connection.Open()
                    ' ans = thisData.ExecuteNonQuery



                    Dim dreader As SqlClient.SqlDataReader



                    dreader = cmd.ExecuteReader()
                    If dreader.Read() Then
                        If Not IsDBNull(dreader(0)) Then
                            requestnum = dreader(0)
                        End If
                    End If


                    cmd.Connection.Close()
                    cmd = Nothing
                End If


                If ans Then

                    alMissingControls.Insert(0, "Information has been Saved " & sAns)
                Else ' no answer
                    alMissingControls.Insert(0, "Not Saved")
                End If

                thisData = Nothing


                Return requestnum
            Catch ex As Exception
                'sCurrentControlName
                sErr = ex.Message
                sErr = sErr & sCurrGlobal.ToString & " Caused Error "
                Throw ex
            End Try
        End Function
    End Class

    'End Namespace
End Namespace