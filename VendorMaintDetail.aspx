﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="false" CodeFile="VendorMaintDetail.aspx.vb" Inherits="VendorMaintDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <style type="text/css">
        .style2
        {
            text-align: center;
            background-color: #FFFFCC;
            color: #336666;
            font-weight: bold;
            font-size: x-large;
            height: 43px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
<asp:UpdatePanel ID="uplPanel" runat ="server"  UpdateMode="Conditional">
    <Triggers>
    </Triggers>
   <ContentTemplate>
   <div id="maindiv" runat="server" align="left">
       <table  style="border-style: groove" align="left" width="100%">
        <tr align="center">
           <td class="tableRowHeader">
                  Vendor Maintenance
           </td>
        </tr>
        <tr>
          <td align="left">
          <table border="1" width="100%"  style="empty-cells:show" >

                      <tr>
                        <td  align="center" colspan="4">
                         <table>
                            <tr>
                                <td align="center">

                                        <asp:Button ID ="BtnAddVendor" runat = "server" Text ="Submit "  
                                        CssClass="btnhov" BackColor="#006666"  Width="135px" />

                                        
                                </td>
                                <td>
                                
                                </td>
                                <td align="center" >

                                        <asp:Button ID = "BtnReturn" runat= "server" Text="Reset" 
                                        CssClass="btnhov" BackColor="#006666"  Width="135px" />


                                </td>
                            </tr>
                        </table>
                      </td>
                     </tr> 
                      <tr>
                            <td>
                                <asp:Label ID="VendorTypelb" runat="server" Text="Type" ></asp:Label>
                            </td>
                            <td colspan="3">
                                <asp:RadioButtonList ID="VendorTyperbl" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Selected="True" Value="Vendor" Text="Vendor"></asp:ListItem>
                                    <asp:ListItem Value="Contractor" Text="Contractor"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                      </tr>   
 
                     <tr>
                        <td>
                        
                            <asp:Label ID="VendorNumLbl" runat="server" ></asp:Label>
                        </td>
                     </tr> 
                      <tr>
                          <td>
                              <asp:Label ID="VendorNamelbl" runat="server" Text="Vendor Name">
                               </asp:Label>
                          </td>

                          <td colspan="3">
                               <asp:TextBox ID="VendorNameTextBox" runat="server" Width="450PX">
                                </asp:TextBox>
                          
                          </td>
                       </tr>
                       <tr>
                          <td>
                               <asp:Label ID="Addresslbl" runat="server" Text="Address">
                               </asp:Label>
                          
                          </td>
                          <td colspan="3">
                            <asp:TextBox ID="Address1TextBox" runat="server" Width="450PX" >
                            </asp:TextBox>
                          
                          </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Citylbl" runat="server" Text="City:"></asp:Label>
                            </td>
                            <td colspan="3">
                                <asp:TextBox ID="CityTextBox" runat="server" Width="450PX" >
                                </asp:TextBox>
                            
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Statelbl" runat="server" Text="State:"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID ="StateTextBox" runat="server" >
                                </asp:TextBox>
                            
                            </td>

                            <td>
                                <asp:Label ID="ziplbl" runat="server" Text="Zip:"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID ="ZipTextBox" runat="server" >
                                </asp:TextBox>
                            
                            </td>
                        
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="PhoneLbl" runat="server" Text="Phone:">
                                </asp:Label>    
                            
                            </td>
                            <td colspan="3">
                                <asp:TextBox ID ="PhoneTextBox" runat="server" >
                                </asp:TextBox>
                            

                            </td>
                         </tr>
                                                  <tr>
                            <td>
                                <asp:Label ID="Deactivatelbl" runat="server" Text="Deactivate Vendor:" Visible="false">
                                </asp:Label>
                            </td>
                            <td colspan="3">
                                    <asp:RadioButtonList ID = "DateDeactivatedrbl" runat="server" RepeatDirection="Horizontal" Visible="false">
                                        <asp:ListItem>Yes</asp:ListItem>
                                        <asp:ListItem>No</asp:ListItem>            
                                    </asp:RadioButtonList>
                            </td>
                         </tr>


                   </table>

            </td>
     </tr>
     <tr>
        <td colspan="4">
            <asp:Label id="clinetsattachedlbl"  runat="server" Text=" Clients Attached to This Vendor" Font-Bold="True">
            </asp:Label>
        </td>
     </tr>
     <tr>
             <td colspan="4">
                 <asp:GridView ID="gvAccounts" runat="server" AllowSorting="True" 
                           AutoGenerateColumns="False" BackColor="White" BorderColor="#336666" 
                           BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                           DataKeyNames="client_num" EmptyDataText="No Clients Attached to this Phys. Group" 
                           EnableTheming="False" Font-Size="Medium" GridLines="Horizontal" Width="100%">
                           <FooterStyle BackColor="White" ForeColor="#333333" />
                           <HeaderStyle BackColor="#336666" Font-Bold="False" Font-Size="Small" 
                               ForeColor="White" HorizontalAlign="Left" />
                           <PagerStyle BackColor="#336666" Font-Size="Medium" ForeColor="White" 
                               HorizontalAlign="Left" />
                           <RowStyle BackColor="White" ForeColor="#333333" />
                           <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                           <SortedAscendingCellStyle BackColor="#F7F7F7" />
                           <SortedAscendingHeaderStyle BackColor="#487575" />
                           <SortedDescendingCellStyle BackColor="#E5E5E5" />
                           <SortedDescendingHeaderStyle BackColor="#275353" />
                           <Columns>

                               <asp:CommandField ButtonType="Button" ControlStyle-BackColor="#84A3A3" 
                                      HeaderStyle-HorizontalAlign="Left" ItemStyle-Height="30px" SelectText="Select" 
                                      ShowSelectButton="true">
                                  <ControlStyle BackColor="#84A3A3" />
                                  <HeaderStyle HorizontalAlign="Left" />
                                  <ItemStyle Height="30px" />
                                  </asp:CommandField>
                            

                               <asp:BoundField DataField="fullname" HeaderText="Name" />

                               <asp:BoundField DataField="phone" HeaderText="Phone" />

                               <asp:BoundField DataField="login_name" HeaderText="Login" />
                           </Columns>
                       </asp:GridView>
                           </td>
                         </tr>
  </table>
  </div>
   </ContentTemplate>
  </asp:UpdatePanel>
</asp:Content>

