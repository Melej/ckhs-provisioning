﻿Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports App_Code
Imports AjaxControlToolkit


Partial Class UserMaintenance
    Inherits MyBasePage '   Inherits System.Web.UI.Page


    Dim sActivitytype As String = ""
    Dim cblBinder As New CheckBoxListBinder
    Dim UserInfo As UserSecurity

    Dim iGroupNum As Integer

    Dim ClientNum As Integer
    Dim AccountCd As String
    Dim MaintType As String
    Dim Securitylevel As Integer



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        ClientNum = Request.QueryString("ClientNum")

        
        If Session("LoginID") = "" Then

            Response.Redirect("http://enterprise.crozer.org", True)

        End If

        Securitylevel = Session("SecurityLevelNum")

        UserInfo = New UserSecurity(Session("LoginID"), Session("EmployeeConn"))

        'AccountCd = Request.QueryString("AccountCd")
        'MaintType = Request.QueryString("MaintType")

        'exec [insTechnician] @security_level = 80,
        '	@first_name = 'Jack',
        '	@last_name ='McHugh',
        '	@phone ='610-447-6722',
        '	@e_mail = 'Jack.McHugh@crozer.org',
        '	@fax = null,
        '	@beeper = null,
        '	@nt_login ='mchj02',
        '	@new_request ='no',
        '	@client_num = 48993

        If Securitylevel = 70 Then
            tpOnCall.Visible = False

            tptechnicians.Visible = False
            tpNewSubmitter.Visible = False
            TabPaneGroupView.Visible = False

            tabProv.Visible = False
            tabCscGrp.Visible = False
            tabClientMaint.Visible = False

        Else
            tptechnicians.Visible = True
            tpNewSubmitter.Visible = True
            tpOnCall.Visible = False
            TabPaneGroupView.Visible = True

        End If


        'If Session("Usertype") = "EXternalHelpDeskAnalyst" Then
        '    tpOnCall.Visible = False
        'End If

        If Securitylevel > 79 Then
            tpOnCall.Visible = False
        End If


        tabClientMaint.Visible = False

        If Session("LoginID").ToString.ToLower = "melej" Then

            TabPaneGroupView.Visible = True
            tabClientMaint.Visible = True

        Else


            TabPaneGroupView.Visible = False
			tabCscGrp.Visible = False
			tpOnCall.Visible = False


		End If

        tabCscGrp.Visible = False
        tpOnCall.Visible = False


        If Not IsPostBack Then

            bindAccounts()

            BindOnCallTech()

            LoadAccountTypes("list")

            gvAccounts.SelectedIndex = -1

            Dim ddbTechGroup As New DropDownListBinder(TechGroup2Dddl, "SelTechGroup", Session("CSCConn"))
            'ddbTechGroup.AddDDLCriteria("@display", "call", SqlDbType.NVarChar)
            ddbTechGroup.AddDDLCriteria("@ntlogin", Session("LoginID"), SqlDbType.NVarChar)
            ddbTechGroup.BindData("group_num", "group_name")

            GridGroupView.SelectedIndex = -1



		End If

    End Sub

    Protected Sub LoadAccountTypes(ByVal sLocation As String)
        Dim ddlBinder As New DropDownListBinder

        ddlBinder = New DropDownListBinder(UserTypesddl, "SelUserTypes", Session("EmployeeConn"))
        ddlBinder.AddDDLCriteria("@Client_num", Session("ClientNum"), SqlDbType.Int)
        ddlBinder.AddDDLCriteria("@location", sLocation, SqlDbType.NVarChar)

        ddlBinder.BindData("SecurityLevel", "UserType")
    End Sub

    Protected Sub bindGroupToTech()
        Dim thisData As New CommandsSqlAndOleDb("selTechsByGroupNum", Session("CSCConn"))

        If iGroupNum > 0 Then
            thisData.AddSqlProcParameter("@group_num", iGroupNum, SqlDbType.Int)

        End If
        Dim dt As DataTable

        dt = thisData.GetSqlDataTable

        GridGroupView.DataSource = dt
        GridGroupView.DataBind()


        GridGroupView.SelectedIndex = -1
        TechGroup2Dddl.SelectedIndex = -1

    End Sub

    Protected Sub bindAccounts()


        Dim thisData As New CommandsSqlAndOleDb("SelTechniciansV4", Session("EmployeeConn"))

        'thisData.AddSqlProcParameter("@ntlogin", Session("LoginID"), SqlDbType.NVarChar, 50)
        thisData.AddSqlProcParameter("@txtntlogin", Session("LoginID"), SqlDbType.NVarChar, 50)

        If ClientNum > 0 Then
            thisData.AddSqlProcParameter("@client_num", ClientNum, SqlDbType.Int)

        End If
        

        If SearchLastNameTextBox.Text <> "" Then
            thisData.AddSqlProcParameter("@LastName", SearchLastNameTextBox.Text, SqlDbType.NVarChar, 30)


        End If

        If UserTypesddl.SelectedValue.ToLower <> "select" Then
            thisData.AddSqlProcParameter("@SecurityLevel", UserTypesddl.SelectedValue, SqlDbType.NVarChar, 8)

        End If
        Dim dt As DataTable

        dt = thisData.GetSqlDataTable

        gvAccounts.DataSource = dt
        gvAccounts.DataBind()


        gvAccounts.SelectedIndex = -1


    End Sub
    Protected Sub BindOnCallTech()
        'GridOnCall
        Dim thisData As New CommandsSqlAndOleDb("SelOnCallTechs", Session("CSCConn"))

        Dim dt As DataTable

        dt = thisData.GetSqlDataTable

        GridOnCall.DataSource = dt
        GridOnCall.DataBind()


        If Securitylevel < 80 Then
            GridOnCall.Enabled = False
        End If

    End Sub
    Protected Sub gvAccounts_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvAccounts.RowCommand
        If e.CommandName = "Select" Then

            Dim SecurityLevel As String = ""

            If SecurityLevelText.Text.ToLower = "deactivated" Then
                SecurityLevel = SecurityLevelText.Text
            End If

            Dim ClientNum As String = gvAccounts.DataKeys(Convert.ToInt32(e.CommandArgument))("client_num").ToString()
            Dim SSecurityLevel As String = gvAccounts.DataKeys(Convert.ToInt32(e.CommandArgument))("TechStatus").ToString()

            'gvAccounts.SelectedIndex = -1

            Response.Redirect("./UserMaintDetail.aspx?ClientNum=" & ClientNum & "&SecurityLevel=" & SSecurityLevel, True)


        End If
    End Sub

    Protected Sub bindAddSearch()
        Dim thisData As New CommandsSqlAndOleDb("SelADDTechClients", Session("EmployeeConn"))


		If ADDLastNameTextBox.Text <> "" Then
			thisData.AddSqlProcParameter("@LastName", ADDLastNameTextBox.Text, SqlDbType.NVarChar, 30)
		Else

			Exit Sub
		End If

		If AddFirstNameTextBox.Text <> "" Then
			thisData.AddSqlProcParameter("@firstName", AddFirstNameTextBox.Text, SqlDbType.NVarChar, 30)
		End If

		Dim dt As DataTable

        dt = thisData.GetSqlDataTable

        GvADDClient.DataSource = dt
        GvADDClient.DataBind()


        GvADDClient.SelectedIndex = -1



    End Sub



    Protected Sub UserTypesddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles UserTypesddl.SelectedIndexChanged
        SecurityLevelText.Text = UserTypesddl.SelectedItem.ToString

        SearchLastNameTextBox.Text = ""
    End Sub

    Protected Sub btnSearch_Click(sender As Object, e As System.EventArgs) Handles btnSearch.Click
        bindAccounts()
    End Sub

    Protected Sub BtnAddSearch_Click(sender As Object, e As System.EventArgs) Handles BtnAddSearch.Click
        bindAddSearch()
    End Sub

    Protected Sub GvADDClient_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GvADDClient.RowCommand
        If e.CommandName = "Select" Then
            Dim sActionType As String = "Add"

            Dim ClientNum As String = GvADDClient.DataKeys(Convert.ToInt32(e.CommandArgument))("client_num").ToString()

            GvADDClient.SelectedIndex = -1

            Response.Redirect("./UserMaintDetail.aspx?ClientNum=" & ClientNum & "&ActionType=" & sActionType, True)


        End If
    End Sub

    Protected Sub GridOnCall_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridOnCall.RowCommand
        If e.CommandName = "Select" Then


            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim GroupNum As Integer = GridOnCall.DataKeys(e.CommandArgument)("group_num")
            Dim TechNum As String = GridOnCall.DataKeys(Convert.ToInt32(e.CommandArgument))("tech_num").ToString()

            hdGroup.Text = GroupNum
            hdTech.Text = TechNum

            loadTechGroup()
            'load tech for that group exec sel_techs_by_group_num @group_num = 21
            ' use selected value Technum
        End If


    End Sub
    Protected Sub loadTechGroup()

        Dim ddbTechGroup As New DropDownListBinder(TechGroupDddl, "SelTechGroup", Session("CSCConn"))
        ddbTechGroup.AddDDLCriteria("@display", "call", SqlDbType.NVarChar)
        ddbTechGroup.AddDDLCriteria("@ntlogin", Session("LoginID"), SqlDbType.NVarChar)
        ddbTechGroup.BindData("group_num", "group_name")



        If hdGroup.Text <> "" Then
            TechGroupDddl.SelectedValue = hdGroup.Text
            Dim ddbTechs As New DropDownListBinder(Techddl, "SelTechsByGroupNum", Session("CSCConn"))
            ddbTechs.AddDDLCriteria("@group_num", hdGroup.Text, SqlDbType.NVarChar)
            ddbTechs.BindData("tech_num", "TechName")



        End If

        Dim techNum As Integer

        If hdTech.Text <> "" Then
            techNum = Convert.ToInt32(hdTech.Text)
            If techNum > 0 Then
                Techddl.SelectedValue = hdTech.Text
            ElseIf techNum = 0 Then
                Techddl.SelectedIndex = -1
            End If
        Else
            Techddl.SelectedIndex = -1
        End If

        SelTechlbl.Visible = True
        btnUpdOnCallTech.Visible = True
        Techddl.Visible = True


    End Sub

    Protected Sub btnUpdOnCallTech_Click(sender As Object, e As System.EventArgs) Handles btnUpdOnCallTech.Click
        ' -- InsUpdOnCallTech @groupnum = 23,@technum = 4,@status='deactivate'
        Try
            Dim thisdata As New CommandsSqlAndOleDb("InsUpdOnCallTech", Session("CSCConn"))
            thisdata.AddSqlProcParameter("@groupnum", hdGroup.Text, SqlDbType.Int, 9)
            thisdata.AddSqlProcParameter("@technum", hdTech.Text, SqlDbType.Int, 8)
            thisdata.ExecNonQueryNoReturn()

            hdGroup.Text = ""
            hdTech.Text = ""
            GridOnCall.SelectedIndex = -1

            BindOnCallTech()

            SelTechlbl.Visible = False
            btnUpdOnCallTech.Visible = False
            Techddl.Visible = False


        Catch ex As Exception

        End Try
    End Sub

    Protected Sub Techddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles Techddl.SelectedIndexChanged
        hdTech.Text = Techddl.SelectedValue
    End Sub

    Protected Sub btnResetOncall_Click(sender As Object, e As System.EventArgs) Handles btnResetOncall.Click
        Response.Redirect("~/UserMaintenance.aspx", True)

    End Sub

    Protected Sub btnReset_Click(sender As Object, e As System.EventArgs) Handles btnReset.Click
        Response.Redirect("~/UserMaintenance.aspx", True)

    End Sub

    Protected Sub TechGroup2Dddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles TechGroup2Dddl.SelectedIndexChanged
        iGroupNum = TechGroup2Dddl.SelectedValue
        bindGroupToTech()
    End Sub

    Protected Sub GridGroupView_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridGroupView.RowCommand
        If e.CommandName = "Select" Then

            Dim ClientNum As String = GridGroupView.DataKeys(Convert.ToInt32(e.CommandArgument))("client_num").ToString()

            GridGroupView.SelectedIndex = -1

            Response.Redirect("./UserMaintDetail.aspx?ClientNum=" & ClientNum, True)


        End If

    End Sub
    Protected Sub Maintab_ActiveTabChanged(sender As Object, e As System.EventArgs) Handles Maintab.ActiveTabChanged
        If Maintab.ActiveTabIndex = 4 Then
            Response.Redirect("./OwnerMaintenance.aspx", True)
        End If

        If Maintab.ActiveTabIndex = 5 Then
            Response.Redirect("./TechGroupMaintenance.aspx", True)
        End If

        If Maintab.ActiveTabIndex = 6 Then
            Response.Redirect("./MaintenanceQueue.aspx", True)

        End If
    End Sub

End Class
