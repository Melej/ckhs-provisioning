﻿Imports System
Imports System.IO
Imports System.Drawing
Imports System.Drawing.Printing
Imports System.Collections.Generic
Imports System.Web.UI
Imports System.Data
Imports System.String
Imports App_Code
Imports App_Code.CommandsSqlAndOleDb
Imports C1.C1Excel
Imports C1.Web.UI.Controls
Imports C1.Web.UI.Controls.C1Report
Imports C1.C1Report
Namespace Reports
    Partial Class Reports_CaseListReport
        Inherits System.Web.UI.Page
        Dim path As String
        Dim dsData As New DataSet
        Dim bToggleAltColor As Boolean
        Dim bAutoPrint As Boolean = False
        Dim sReportConn As String = ""
        Dim sRequestNum As String = ""
        Dim requestnum As Integer
        Dim sStart As String = ""
        Dim sStop As String = ""
        Dim sLogin As String = ""
        Dim sGroupByField As String = "" ' "256"
        Dim sGroupByName As String = ""
        Dim sChildSort As String = "unit" 'unit,casemgr,swmgr,atending
        Dim MyPrintingReport As New C1.C1Report.C1Report()
        Dim MyReport As New C1.C1Report.C1Report

        Dim alPrinter As New ArrayList
        Dim alCaseList As New ArrayList
        Dim sReportname As String
        Dim sFeatures As String
        Dim alFeatures As New ArrayList
        Dim alParentNodes As New ArrayList

        Private _substring As String
        Private Property Substring(sGroup As String, p2 As Integer, p3 As Integer) As String
            Get
                Return _substring
            End Get
            Set(value As String)
                _substring = value
            End Set
        End Property
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Dim dtReportData As New DataTable

            sReportConn = Session("CSCConn")

            Dim sFeatures As String = Session("ReportFeatures")
            Dim sParentNodes As String = Session("ParentNodes")

            If Request.QueryString("requestnum") <> "" Then
                sRequestNum = Request.QueryString("requestnum")
                requestnum = CInt(sRequestNum)
            End If

            'ReportName
            If Request.QueryString("ReportName") <> "" Then
                sReportname = Request.QueryString("ReportName")
            End If

            requestnum = 236100
            sRequestNum = "236100"
            sReportname = "TicketRFS"

            'alFeatures.AddRange(sFeatures.Split(","))
            'alParentNodes.AddRange(sParentNodes.Split(","))

            'sReportConn = Session("DataConn")


            'sLogin = Session("loginid")
            'sReportname = Session("ReportName")
            'alCaseList = TryCast(Session("ReportCaseList"), ArrayList)
            'Session("ReportCaseList") = Nothing
            'sGroupByField = Session("GroupByField")
            'sGroupByName = Session("GroupByName")
            'sChildSort = Session("OrderBy")

            C1ReportViewer1.Cache.ShareBetweenSessions = False


            If Not IsPostBack Then
                C1ReportViewer1.Cache.Enabled = False


                If C1ReportViewer1.HasCachedDocument Then
                    C1ReportViewer1.Document = Nothing
                End If


                If Session("ReportType") = "ExportToExcel" Then

                    dtReportData = getReportDataSet(Session("ReportType"))
                    CreateExcel(dtReportData)
                    'ExportUnitsToExcel(dtReportData)
                Else

                    C1ReportViewer1.ReportName = "TicketReport" & DateAndTime.Now.ToString
                    MyPrintingReport = GetReport(sReportname)

                    pdfStream()


                    C1ReportViewer1.ToolsPaneVisible = False
                    C1ReportViewer1.DisplayVisible = False

                    Exit Sub
                    '=================end====================
                End If
            End If





        End Sub
        'Protected Function GetReport() As Object
        '    path = Request.MapPath("xml/ClientInfoV2.xml")
        '    Dim rn As String
        '    Dim MyReport As New C1.C1Report.C1Report '()




        '    Dim dsReportData As New DataSet
        '    Dim dtReportData As New DataTable
        '    MyReport.C1Document.Generate()
        '    MyReport.UseGdiPlusTextRendering = True
        '    MyReport.EmfType = System.Drawing.Imaging.EmfType.EmfPlusOnly



        '    MyReport.Load(path, rn)

        '    MyPrintingReport = GetReport("ClientReportV2")

        '    MyReport.DataSource.Recordset = dtReportData '.Tables(0) 'no .DefaultView

        '    Dim icount As Integer = dtReportData.Rows.Count


        '    Return MyReport
        'End Function
        Protected Function GetReport(ByVal ReportName As String) As Object 'C1.C1Report.C1Report '
            path = Request.MapPath("~/Reports/XML/ClientInfoV2.xml")
            Dim rn As String = ReportName '"AssessmentReport" 'CaseAssessmentReport"

            MyReport.C1Document.Generate()
            MyReport.UseGdiPlusTextRendering = True
            MyReport.EmfType = System.Drawing.Imaging.EmfType.EmfPlusOnly


            MyReport.Load(path, rn)

            MyReport.DataSource.ConnectionString = "Provider=SQLOLEDB.1;" & sReportConn
            Dim Param As String = "selTicketRFSByNumV7(" & sRequestNum & ")"
            MyReport.DataSource.RecordSource = Param

            Return MyReport '.C1Document

        End Function
        Public Function getReportDataSet(ByVal sReportType As String) As DataTable 'DataSet
            Dim irowCount As Integer = 0
            Dim sHospital As String
            Dim sPFCUnits As String
            Dim alPFCUnits As New ArrayList
            Dim thisdata As New CommandsSqlAndOleDb("SelBedsForReports", Session("DataConn")) ' Request.QueryString("ConnString"))


            thisdata.AddSqlProcParameter("@ntLogin", sLogin, SqlDbType.NVarChar, 50)

            dsData = thisdata.GetSqlDataset()
            sHospital = Session("Hospital")


            thisdata = Nothing
            '===============Linq====================================

            Dim ReportRecords = dsData.Tables(0).AsEnumerable()
            Dim dtSelected As DataTable
            Dim dtPatientsOnly As DataTable

            '==returns rows that match case numberw in the myintcases arraylist======

            Select Case sReportType
                Case "PFCValues"


                    Select Case sHospital
                        Case "Crozer"
                            sPFCUnits = ConfigurationManager.AppSettings.Get("CrozerPFCUnits")

                        Case "Dcmh"
                            sPFCUnits = ConfigurationManager.AppSettings.Get("DcmhPFCUnits")

                        Case "Springfield"
                            sPFCUnits = ConfigurationManager.AppSettings.Get("SpringfieldPFCUnits")

                        Case "Taylor"
                            sPFCUnits = ConfigurationManager.AppSettings.Get("TaylorPFCUnits")

                        Case Else
                            sPFCUnits = ConfigurationManager.AppSettings.Get("CrozerPFCUnits")

                    End Select


                    alPFCUnits.AddRange(sPFCUnits.Split(","))



                    Dim query = From o In ReportRecords _
                    Join units In alPFCUnits On o("NurseStationCD") Equals units _
                    Select o Order By o(sGroupByField) Ascending, o(sChildSort) Ascending, o("RoomNo") Ascending, o("OrderNum") Ascending

                    For Each record In query
                        record("GroupByField") = record(sGroupByField)
                        record("GroupByName") = sGroupByName
                    Next


                    dtSelected = query.CopyToDataTable
                    Dim uniqueFactors = query.Count()

                Case "SelectedValues"


                    Dim MyIntCases As New ArrayList
                    For i = 0 To alCaseList.Count - 1
                        MyIntCases.Add(CType(alCaseList(i), Int32))
                    Next


                    '================================================
                    ' Select open beds if parent nodes are selected
                    '================================================

                    If (alFeatures.Contains("PatientsOnly") And alParentNodes.Count > 1) _
                            Or (alFeatures.Contains("PatientsOnly") = False And alParentNodes.Count = 1) _
                            Or (alFeatures.Contains("PatientsOnly") = True And alParentNodes.Count = 1) Then




                        Dim query = From o In ReportRecords _
                      Join cases In MyIntCases On o("PatientBedNum") Equals cases _
                      Select o Order By o(sGroupByField) Ascending, o(sChildSort) Ascending, o("RoomNo") Ascending, o("OrderNum") Ascending



                        For Each record In query
                            record("GroupByField") = record(sGroupByField)
                            record("GroupByName") = sGroupByName
                        Next


                        dtSelected = query.CopyToDataTable

                    Else





                        Dim queryPatientsOnly = From o In ReportRecords _
                        Join units In alParentNodes On o("NurseStationCd") Equals units _
                        Select o Order By o(sGroupByField) Ascending, o(sChildSort) Ascending, o("RoomNo") Ascending, o("OrderNum") Ascending
                        'Where o("PatientAcctNumber").Equals(Nothing)
                        'Where(o.IsNull("PatientAcctNumber"))



                        For Each record In queryPatientsOnly
                            record("GroupByField") = record(sGroupByField)
                            record("GroupByName") = sGroupByName
                        Next


                        dtSelected = queryPatientsOnly.CopyToDataTable


                        'Dim uniqueFactors = query.Count()
                    End If






                Case "ExportToExcel"


                    Dim query = From o In ReportRecords _
                    Select o Order By o("NurseStationCd") Ascending, o("RoomNo") Ascending, o("OrderNum") Ascending

                    For Each record In query
                        record("GroupByField") = record(sGroupByField)
                        record("GroupByName") = sGroupByName
                    Next

                    dtSelected = query.CopyToDataTable
                    Dim uniqueFactors = query.Count()
            End Select



            If alFeatures.Contains("PatientsOnly") = True Then
                Dim arrPatientsOnly As New ArrayList()


                For Each row As DataRow In dtSelected.Rows
                    If IsDBNull(row("PatientAcctNumber")) Then

                        arrPatientsOnly.Add(row)

                    End If
                Next

                For Each nullRow As DataRow In arrPatientsOnly
                    dtSelected.Rows.Remove(nullRow)

                Next

            End If

            '==============End linq=================================




            Return dtSelected
        End Function
        Protected Sub btnGetReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGetReport.Click
            Response.Redirect("~/ListCreator.aspx", True)
        End Sub
        Protected Sub btnPdf_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPdf.Click
            Dim sFilePathName As String = MapPath("~/Reports/temp_files/Report1234.pdf")
            MyPrintingReport.RenderToFile(sFilePathName, C1.C1Report.FileFormatEnum.PDF)

            Dim jScript As String '"
            '  jScript = "<script>CallShowPdf('Generic.pdf');</script>""..Reports/Report1234.pdf" 
            Dim jsFilePathName As String = sFilePathName.Replace("\", "\\") '"../Reports/temp_files/Report1234.pdf" '
            ' //  Response.Write("<script>window.open('" & jsFilePathName & "',target='new')</script>")
            '//Set the appropriate ContentType.
            Response.ContentType = "Application/pdf"
            ' //Get the physical path to the file.
            ' string FilePath = MapPath("acrobat.pdf");
            ' //Write the file directly to the HTTP content output stream.
            Response.WriteFile(sFilePathName)
            Response.End()
        End Sub
        Protected Function getPrinterList() As ArrayList
            'FYI not used
            Dim sPrinterName As String = ""
            Dim AlPrinters As New ArrayList
            For p As Integer = 0 To PrinterSettings.InstalledPrinters.Count - 1
                'MyPrintingReport.Document.PrinterSettings.InstalledPrinters.Count(-1)
                '   alPrinter.Add(
                sPrinterName = PrinterSettings.InstalledPrinters.Item(p) '.)
                AlPrinters.Add(sPrinterName)
            Next
            Return AlPrinters
            Exit Function
            MyPrintingReport.Document.PrinterSettings.PrinterName = "PRINTER34367 on Prntsrv02"
            MyPrintingReport.Document.PrinterSettings.PrinterName = alPrinter.Item(0)
            MyPrintingReport.Document.Print()
        End Function
        Public Sub Printing(ByVal printer As String)
            'FYI not used
            Try
                Dim streamToPrint As StreamReader
                Dim printFont As Font
                Dim filePath As String = "C:\test.txt"
                streamToPrint = New StreamReader(filePath)
                Try
                    printFont = New Font("Arial", 10)
                    Dim pd As New PrintDocument()
                    AddHandler pd.PrintPage, AddressOf pd_PrintPage
                    ' Specify the printer to use.
                    pd.PrinterSettings.PrinterName = printer

                    If pd.PrinterSettings.IsValid Then
                        pd.Print()
                    Else
                        '  MessageBox.Show("Printer is invalid.")
                    End If
                Finally
                    streamToPrint.Close()
                End Try
            Catch ex As Exception
                ' MessageBox.Show(ex.Message)
            End Try
        End Sub
        Private Sub pdfStream()
            Dim length As Integer
            Dim rsp As HttpResponse = Me.Page.Response
            rsp.Buffer = True
            Dim ios As New System.IO.MemoryStream

            MyReport.RenderToStream(ios, C1.C1Report.FileFormatEnum.PDF)


            rsp.Clear()
            rsp.ContentType = "application/pdf"
            length = CInt(ios.Length)
            rsp.OutputStream.Write(ios.GetBuffer(), 0, length)
            rsp.Flush()
            rsp.SuppressContent = True
            ios = Nothing
        End Sub

        Private Sub excelStream()
            'Dim C1XLBook1 As New C1XLBook()
            '' step 2: write content into some cells

            'Dim sheet As XLSheet = C1XLBook1.Sheets(0)

            'Dim i As Integer

            'For i = 0 To 99

            '    sheet(i, 0).Value = i + 1

            'Next i

            '' step 3: save the file

            'C1XLBook1.Save("c:\temp\hello.xls")


            ' *****************
            Dim length As Integer
            Dim rsp As HttpResponse = Me.Page.Response
            rsp.Buffer = True
            Dim ios As New System.IO.MemoryStream

            C1ReportViewer1.Document.RenderToStream(ios, C1.C1Report.FileFormatEnum.Excel)
            rsp.Clear()
            rsp.ContentType = "application/excel"

            length = CInt(ios.Length)
            rsp.OutputStream.Write(ios.GetBuffer(), 0, length)
            rsp.Flush()
            rsp.SuppressContent = True
            ios = Nothing

        End Sub

        Protected Sub CreateExcel(ByVal ReportTable As DataTable)
            Dim gvData As GridView


            Session("UCCurrentMethod") = "CreateDocOrExcel()"
            ' GridView1.Visible = True
            'doesnt work with gridviews that have controls in them
            Response.ClearContent()
            Dim attachment As String = "attachment; filename=ExcelDownload.xls"

            Response.AddHeader("Content-disposition", attachment)
            Response.ContentType = "application/excel"


            Dim strStyle As String = "<style>.text {mso-number-format:\@; } </style>"   ' to help retain leading zeroes
            Response.Write(strStyle)
            Response.Write("<H4 align='center'>")  ' throw in a pageheader just for the heck of it to tell people what the data is and when it was run
            '  Response.Write("Parts Listing as of " & Date.Now.ToString("d"))
            Response.Write(Session("Hospital") & " Room List")
            Response.Write("</H4>")
            Response.Write("<BR>")
            'GridView1.HeaderStyle.Font.Bold = True
            'GridView1.HeaderStyle.BackColor = Drawing.Color.LightGray

            Dim sw As StringWriter = New StringWriter()
            Dim hw As HtmlTextWriter = New HtmlTextWriter(sw)
            Dim dg As New DataGrid


            ' must be name of datasource on page
            dg.DataSource = ReportTable
            Dim i As Integer = dg.Items.Count
            dg.DataBind()
            '  dg.GridLines = GridLines.None
            ' ClearControls(dg)

            dg.HeaderStyle.Font.Bold = True
            dg.HeaderStyle.BackColor = Drawing.Color.LightGray


            dg.RenderControl(hw)
            '   Dim dt As New DataTable
            ' Dim dv As New DataView

            'dv.Table.ToString()
            Response.Write(sw.ToString())
            Response.End()

            ' GridView1.AllowSorting = True
            sw = Nothing
            hw = Nothing
            dg.Dispose()
        End Sub
        Protected Sub ExportUnitsToExcel(ByVal ReportTable As DataTable)
            Dim xlApp As New Microsoft.Office.Interop.Excel.Application
            Dim wBook As New Microsoft.Office.Interop.Excel.Workbook
            Dim wSheet As New Microsoft.Office.Interop.Excel.Worksheet



            wBook = xlApp.Workbooks.Add()
            wSheet = wBook.ActiveSheet()

            Dim dg As New DataGrid
            Dim ds As New DataSet
            Dim dr1 As DataRow


            dg.DataSource = ReportTable

            dg.HeaderStyle.Font.Bold = True
            dg.HeaderStyle.BackColor = Drawing.Color.LightGray

            ds.Tables.Add()

            'For i As Integer = 0 To dg.Columns.Count - 1
            '    ds.Tables(0).Columns.Add(dg.Columns(i).HeaderText)
            'Next

            'For i As Integer = 0 To dg.Columns.Count - 1
            '    dr1 = ds.Tables(0).NewRow

            '    For j As Integer = 0 To dg.Columns.Count - 1

            '        dr1(j) = dg.
            '    Next

            'Next

            Dim dc As DataColumn
            Dim dr As DataRow
            Dim colIndex As Integer = 0
            Dim rowIndex As Integer = 0

            For Each dc In ReportTable.Columns
                colIndex += 1
                xlApp.Cells(1, colIndex) = dc.ColumnName

            Next

            For Each dr In ReportTable.Rows
                rowIndex += 1
                colIndex = 0

                For Each dc In ReportTable.Columns
                    colIndex += 1
                    xlApp.Cells(rowIndex + 1, colIndex) = dr(dc.ColumnName)

                Next


            Next

            wSheet.Columns.AutoFit()

            Dim strFileName As String = "C:\Temp\eBed.xls"
            Dim blnFileOpen As Boolean = False

            Try
                Dim fileTemp As System.IO.FileStream = System.IO.File.OpenWrite(strFileName)
                fileTemp.Close()

            Catch ex As Exception
                blnFileOpen = False
            End Try

            If System.IO.File.Exists(strFileName) Then
                System.IO.File.Delete(strFileName)
            End If



            wBook.SaveAs(strFileName)
            xlApp.Workbooks.Open(strFileName)
            xlApp.Visible = True


            ' must be name of datasource on page
            dg.DataSource = ReportTable
            dg.DataBind()









        End Sub
        Private Sub pd_PrintPage(ByVal sender As Object, ByVal e As PrintPageEventArgs)
            '  Throw New NotImplementedException
        End Sub
    End Class
End Namespace