﻿Imports System
Imports System.IO
Imports System.Web.UI
Imports System.Data
Imports System.String

Imports App_Code



Partial Class Reports_RolesReport
    Inherits MyBasePage
    Dim MyPrintingReport As New C1.C1Report.C1Report()
    Dim MyReport As New C1.C1Report.C1Report
    Dim sReportName As String
    Dim sGroupNumber As String
    Dim sClientNum As String
    Dim sGnumber As String
    Dim sCommGnumber As String
    Dim sGroupType As String
    Dim sInvisionGroupNum As String
    Dim sEmptype As String
    Dim sStartDate As String
    Dim sEndDate As String
    Dim sRequesttype As String
    Dim sStatus As String
    Dim sApplicationnum As String
    Dim sPriority As String
    Dim sProjectNum As String
    Dim sLogin As String

    Dim path As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        sReportName = Request.QueryString("ReportName")
        sGroupType = Request.QueryString("GroupType")
        sGroupNumber = Request.QueryString("GroupNum")
        sClientNum = Request.QueryString("clientnum")
        sGnumber = Request.QueryString("Gnumber")

        sCommGnumber = Request.QueryString("clientnum")
        sInvisionGroupNum = Request.QueryString("InvisionGroupNum")
        sEmptype = Request.QueryString("EmpType")


        sStartDate = Request.QueryString("StartDate")
        sEndDate = Request.QueryString("enddate")
        sRequesttype = Request.QueryString("RequestType")

        sStatus = Request.QueryString("Status")
        sPriority = Request.QueryString("Priority")

        sApplicationnum = Request.QueryString("Applicationnum")

        sProjectNum = Request.QueryString("ProjectNum")

        sLogin = Request.QueryString("LoginName")

        Dim dtReportData As New DataTable

        If sReportName = "" Then
            sReportName = "RolesReport"

            C1ReportViewer1.Cache.ShareBetweenSessions = False
            C1ReportViewer1.Cache.Enabled = False
            MyPrintingReport = GetReport(sReportName)

            C1ReportViewer1.Document = MyPrintingReport

            pdfStream()
        Else

            dtReportData = GetReport(sReportName)
            CreateExcel(dtReportData)

        End If


    End Sub

    Protected Function GetReport(ByVal ReportName As String) As Object
        Dim sXmlFolder As String = "xml\"
        Dim StoredProcedure As String
        Dim dtSelected As DataTable


        path = Request.MapPath(sXmlFolder & "AccountQueueReports.xml")

        MyReport.C1Document.Generate()
        MyReport.UseGdiPlusTextRendering = True
        MyReport.EmfType = System.Drawing.Imaging.EmfType.EmfPlusOnly

        Select Case ReportName
            Case "SurveyReport"

                Dim thisData As New CommandsSqlAndOleDb("SelSurveyReport", Session("CSCConn"))
                dtSelected = thisData.GetSqlDataTable

                Return dtSelected

            Case "ProjectReport"
                Dim thisData As New CommandsSqlAndOleDb("SelProjectDesc", Session("CSCConn"))
                dtSelected = thisData.GetSqlDataTable

                Return dtSelected

            Case "RequestbyProjectReport"
                Dim thisData As New CommandsSqlAndOleDb("SelRequestsByproject", Session("CSCConn"))
                thisData.AddSqlProcParameter("@projectnum", sProjectNum, SqlDbType.NVarChar, 20)

                dtSelected = thisData.GetSqlDataTable

                Return dtSelected

            Case "RolesReport"
                StoredProcedure = "SelRoleAccountsAndOwnerRpt"
                MyReport.Load(path, ReportName)
                MyReport.DataSource.ConnectionString = "Provider=SQLOLEDB.1;" & Session("EmployeeConn")
                MyReport.DataSource.RecordSource = StoredProcedure

            Case "PhysGroupReport"
                Dim thisdata As New CommandsSqlAndOleDb("SelPhysClientsByV4", Session("EmployeeConn")) ' Request.QueryString("ConnString"))
                thisdata.AddSqlProcParameter("@groupNum", sGroupNumber, SqlDbType.Int)

                'If sGroupType <> "" Then

                '    thisdata.AddSqlProcParameter("@GroupType", sGroupType, SqlDbType.NVarChar)

                'ElseIf sInvisionGroupNum <> "" Then
                '    thisdata.AddSqlProcParameter("@InvisionGroupNum", sInvisionGroupNum, SqlDbType.NVarChar)

                'ElseIf sGnumber <> "" Then
                '    thisdata.AddSqlProcParameter("@Gnumber", sGnumber, SqlDbType.NVarChar)
                'ElseIf sCommGnumber <> "" Then
                '    thisdata.AddSqlProcParameter("@CommGNumber", sCommGnumber, SqlDbType.NVarChar)
                'Else
                '    thisdata.AddSqlProcParameter("@groupNum", sGroupNumber, SqlDbType.Int)
                'End If

                dtSelected = thisdata.GetSqlDataTable

                Return dtSelected

            Case "ProviderReport"
                Dim thisData As New CommandsSqlAndOleDb("SelOpenProviderReportV4", Session("EmployeeConn"))
                thisData.AddSqlProcParameter("@startDate", sStartDate, SqlDbType.NVarChar, 12)
                thisData.AddSqlProcParameter("@endDate", sEndDate, SqlDbType.NVarChar, 12)
                thisData.AddSqlProcParameter("@requestTypes", sRequesttype, SqlDbType.NVarChar, 4)


                dtSelected = thisData.GetSqlDataTable

                Return dtSelected

            Case "EndDateReport"
                Dim thisdata As New CommandsSqlAndOleDb("EndDateClients", Session("EmployeeConn")) ' Request.QueryString("ConnString"))
                thisdata.AddSqlProcParameter("@emp_type", sEmptype, SqlDbType.NVarChar)
                thisdata.AddSqlProcParameter("@startDate", sStartDate, SqlDbType.NVarChar, 12)
                thisdata.AddSqlProcParameter("@endDate", sEndDate, SqlDbType.NVarChar, 12)
                thisdata.AddSqlProcParameter("@withEnd", sRequesttype, SqlDbType.NVarChar, 4)

                dtSelected = thisdata.GetSqlDataTable

                Return dtSelected

            Case "ClientReport"
                path = Request.MapPath("~/Reports/XML/ClientInfo.xml")

                MyReport.Load(path, "ClientReport")

                MyReport.DataSource.ConnectionString = "Provider=SQLOLEDB.1;" & Session("EmployeeConn")
                Dim Param As String = "SelClientByNumberV20(" & sClientNum & ")"
                MyReport.DataSource.RecordSource = Param

            Case "TicketRFSReport"


                Dim sprocedures As String = ""

                If sRequesttype = "Projects" Then


                    sprocedures = "SelCurrentprojects"



                Else
                    sprocedures = "SelTicketsAndRFSReportV2"


                End If

                Dim thisData As New CommandsSqlAndOleDb(sprocedures, Session("CSCConn"))
                thisData.AddSqlProcParameter("@startDate", sStartDate, SqlDbType.NVarChar)
                thisData.AddSqlProcParameter("@endDate", sEndDate, SqlDbType.NVarChar)
                thisData.AddSqlProcParameter("@group_num", sGroupNumber, SqlDbType.NVarChar)
                thisData.AddSqlProcParameter("@ticket_rfs", sRequesttype, SqlDbType.NVarChar)
                thisData.AddSqlProcParameter("@close", sStatus, SqlDbType.NVarChar)
                thisData.AddSqlProcParameter("@P1Only", sPriority, SqlDbType.NVarChar)


                'Dim thisData As New CommandsSqlAndOleDb("SelTicketsAndRFSReport", Session("CSCConn"))

                dtSelected = thisData.GetSqlDataTable

                Return dtSelected

            Case "AccountToUserReport"
                Dim thisData As New CommandsSqlAndOleDb("SelAccountsByApplicationV3", Session("EmployeeConn"))
                thisData.AddSqlProcParameter("@Applicationnum", sApplicationnum, SqlDbType.NVarChar)
                thisData.AddSqlProcParameter("@AffiliationID", sEmptype, SqlDbType.NVarChar)
                thisData.AddSqlProcParameter("@enddate", sEndDate, SqlDbType.NVarChar)

                dtSelected = thisData.GetSqlDataTable

                Return dtSelected

            Case "myqueue"


                Dim thisData As New CommandsSqlAndOleDb("SelMyTicketsAndRFSReport", Session("CSCConn"))
                thisData.AddSqlProcParameter("@nt_login", sLogin, SqlDbType.NVarChar)

                dtSelected = thisData.GetSqlDataTable

                Return dtSelected

        End Select



        Return MyReport



    End Function

    Private Sub pdfStream()
        Dim length As Integer

        Dim rsp As HttpResponse = Me.Page.Response

        rsp.Buffer = True

        Dim ios As New System.IO.MemoryStream
        MyReport.RenderToStream(ios, C1.C1Report.FileFormatEnum.PDF)


        rsp.Clear()

        rsp.ContentType = "application/pdf"

        length = CInt(ios.Length)

        rsp.OutputStream.Write(ios.GetBuffer(), 0, length)

        rsp.Flush()

        rsp.SuppressContent = True
        ios = Nothing

    End Sub
    Protected Sub CreateExcel(ByVal ReportTable As DataTable)

        Session("UCCurrentMethod") = "CreateDocOrExcel()"

        'doesnt work with gridviews that have controls in them
        Response.ClearContent()
        Dim attachment As String = "attachment; filename=ExcelDownload.xls"

        Response.AddHeader("Content-disposition", attachment)
        Response.ContentType = "application/excel"


        Dim strStyle As String = "<style>.text {mso-number-format:\@; } </style>"   ' to help retain leading zeroes
        Response.Write(strStyle)
        Response.Write("<H4 align='center'>")  ' throw in a pageheader just for the heck of it to tell people what the data is and when it was run

        Response.Write(Session("Hospital"))
        Response.Write("</H4>")
        Response.Write("<BR>")

        Dim sw As StringWriter = New StringWriter()
        Dim hw As HtmlTextWriter = New HtmlTextWriter(sw)
        Dim dg As New DataGrid


        ' must be name of datasource on page
        dg.DataSource = ReportTable
        Dim i As Integer = dg.Items.Count
        dg.DataBind()

        dg.HeaderStyle.Font.Bold = True
        dg.HeaderStyle.BackColor = Drawing.Color.LightGray


        dg.ItemStyle.Wrap = False

        dg.RenderControl(hw)
        Response.Write(sw.ToString())
        Response.End()

        sw = Nothing
        hw = Nothing
        dg.Dispose()

    End Sub

End Class



