﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ClientInfoRpt.aspx.vb" Inherits="Reports_ClientInfoRpt" %>
<%@ Register Assembly="C1.Web.UI.Controls.C1Report.4" Namespace="C1.Web.UI.Controls.C1Report" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">

    <div>
    
         <asp:Button ID="btnMainPg" runat="server" Text="Back to CSC Main Page" />


            <cc1:C1ReportViewer ID="C1ReportViewer1" runat="server" Width="90%">
            </cc1:C1ReportViewer>


    </div>
    </form>
</body>
</html>
