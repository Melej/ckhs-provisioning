﻿'Imports System.Collections.Generic
'Imports System.Web.UI
'Imports System.Data
'Imports System.Drawing
'Imports App_Code.CommandsSqlAndOleDb
Imports App_Code
Partial Class Reports_ClientInfoRpt
    Inherits MyBasePage '.Web.UI.Page
    Dim MyPrintingReport As New C1.C1Report.C1Report()
    Dim MyReport As New C1.C1Report.C1Report

    Dim path As String
    Dim dsData As New DataSet
    Dim bToggleAltColor As Boolean
    Dim sReportConn As String = ""
    Dim sStart As String = ""
    Dim sStop As String = ""
    Dim sclientnum As String = ""
    Dim iclientnum As Integer

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        sReportConn = Session("EmployeeConn")
        If Request.QueryString("clientnum") <> "" Then
            sclientnum = Request.QueryString("clientnum")
            iclientnum = CInt(sclientnum)
        End If

        Try
            C1ReportViewer1.Cache.ShareBetweenSessions = False
            C1ReportViewer1.Cache.Enabled = False

            If C1ReportViewer1.HasCachedDocument Then

                C1ReportViewer1.Document = Nothing
            End If

            If sclientnum <> "" Then

                C1ReportViewer1.Document = Nothing
                C1ReportViewer1.ReportName = "ClientReportV2" & DateAndTime.Now.ToString
                'C1ReportViewer1.Document = GetReport("ClientReportV2") 'MyReport '.C1Document
                MyPrintingReport = GetReport("ClientReportV2")
                C1ReportViewer1.Document = MyPrintingReport

                pdfStream()


            End If

        Catch ex As Exception
            Dim s As String = ex.Message.ToString
        End Try


    End Sub
    Protected Function GetReport(ByVal ReportName As String) As Object 'C1.C1Report.C1Report '
        path = Request.MapPath("~/Reports/XML/ClientInfoV2.xml")
        Dim rn As String = ReportName '"AssessmentReport" 'CaseAssessmentReport"

        MyReport.C1Document.Generate()
        MyReport.UseGdiPlusTextRendering = True
        MyReport.EmfType = System.Drawing.Imaging.EmfType.EmfPlusOnly


        MyReport.Load(path, rn)

        MyReport.DataSource.ConnectionString = "Provider=SQLOLEDB.1;" & sReportConn
        Dim Param As String = "SelClientByNumberV20(" & sclientnum & ")"
        MyReport.DataSource.RecordSource = Param

        Return MyReport '.C1Document

    End Function

    Protected Sub btnMainPg_Click(sender As Object, e As System.EventArgs) Handles btnMainPg.Click
        Response.Redirect("~/ClientDemo.aspx?clientnum=" & iclientnum, True)

    End Sub

    Private Sub pdfStream()
        Dim length As Integer

        Dim rsp As HttpResponse = Me.Page.Response

        rsp.Buffer = True

        Dim ios As New System.IO.MemoryStream
        MyReport.RenderToStream(ios, C1.C1Report.FileFormatEnum.PDF)


        rsp.Clear()

        rsp.ContentType = "application/pdf"

        length = CInt(ios.Length)

        rsp.OutputStream.Write(ios.GetBuffer(), 0, length)

        rsp.Flush()

        rsp.SuppressContent = True
        ios = Nothing

    End Sub
End Class
