﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PrintListReport.aspx.vb" Inherits="Reports.Reports_CaseListReport" %>

<%@ Register Assembly="C1.Web.UI.Controls.C1Report.4" Namespace="C1.Web.UI.Controls.C1Report" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <%--<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<META HTTP-EQUIV="Expires" CONTENT="-1">src="../JavaScript/CommonNew.js"--%>
	<script type="text/javascript">
		function localPopupWindow(url) {
			PopupWindow = window.open(url, 'PopupWindow', 'height=600,width=650,left=300,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=yes,location=no,directories=no,status=yes')
		}
		function localnewWindow(url) {
			popupWindow = window.open(url, 'popUpWindow', 'height=600,width=650,left=300,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=yes,location=no,directories=no,status=yes')
		}
	</script>

</head>
<body>
	<form id="form1" runat="server">
		<asp:ScriptManager ID="ScriptManager1" runat="server">
		</asp:ScriptManager>
		<div>
			<asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="200">

				<ProgressTemplate>

					<div id="IMGDIV" align="center" valign="middle" runat="server" style="position: absolute; left: 400px; top: 10px; visibility: visible; vertical-align: middle; border-style: inset; border-color: black; background-color: White; width: 210px; height: 75px;">
						Loading Data ...
                   
					<img src="~/Images/AjaxProgress.gif" /><br />

					</div>

				</ProgressTemplate>

			</asp:UpdateProgress>


			<asp:Button ID="btnGetReport" runat="server" Text="Back to Main Page" />
			<asp:Button ID="btnPdf" runat="server" Text="Pdf" />
			<%--<asp:Button ID="btn256" runat="server" Text="256"  Visible="true"/>
         <asp:Button ID="btn2040" runat="server" Text="2040" Visible="true"/>
			--%>
			<asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>

			<cc1:C1ReportViewer ID="C1ReportViewer1" runat="server" Width="100%" AvailableTools="None" Cache-Enabled="False" Cache-Expiration="0">
				<%--<Cache Enabled="False" Expiration="0" ShareBetweenSessions="False"></Cache>--%>
				<Cache Enabled="False" Expiration="0" ShareBetweenSessions="False"></Cache>
			</cc1:C1ReportViewer>
		</div>
    </form>
</body>
</html>
