﻿Imports System.IO
Imports System.Web.UI.DataVisualization
Imports System.Web.UI.DataVisualization.Charting
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.Control
Imports iTextSharp.text.pdf
Imports App_Code
Imports System.Collections.Generic

Partial Class P1ReportDetail
    Inherits System.Web.UI.Page
    Dim sDate As String
    Dim sValue As String
    Dim sType As String

    Dim ReportTotalContrl As New P1ReportController()
    Dim ReportDetailList As New List(Of P1DetailReportViewer)

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        sDate = Request.QueryString("entryyear")
        sValue = Request.QueryString("DisplayField")
        sType = Request.QueryString("Type")

        ReportTotalContrl.getP1Details(sType, sDate, sValue)

        ReportDetailList = ReportTotalContrl.getP1Details()

        lblChartTitle.Text = ""

        Select Case sType.ToLower
            Case "group"
                grdgroup.Visible = True

                grdMonth.Visible = False
                grdHour.Visible = False
                grdDay.Visible = False

                grdgroup.AutoGenerateColumns = False
                grdgroup.DataSource = ReportDetailList
                grdgroup.DataBind()

            Case "month"
                grdMonth.Visible = True

                grdgroup.Visible = False
                grdHour.Visible = False
                grdDay.Visible = False

                grdMonth.AutoGenerateColumns = False
                grdMonth.DataSource = ReportDetailList
                grdMonth.DataBind()

            Case "day"
                grdDay.Visible = True

                grdMonth.Visible = False
                grdgroup.Visible = False
                grdHour.Visible = False

                grdDay.AutoGenerateColumns = False
                grdDay.DataSource = ReportDetailList
                grdDay.DataBind()

            Case "hour"
                grdHour.Visible = True

                grdDay.Visible = False
                grdMonth.Visible = False
                grdgroup.Visible = False

                grdHour.AutoGenerateColumns = False
                grdHour.DataSource = ReportDetailList
                grdHour.DataBind()
            Case Else
                grdgroup.Visible = True

                grdMonth.Visible = False
                grdHour.Visible = False
                grdDay.Visible = False

                grdgroup.AutoGenerateColumns = False
                grdgroup.DataSource = ReportDetailList
                grdgroup.DataBind()

        End Select


        'grdP1Details.AutoGenerateColumns = True
        'grdP1Details.DataSource = ReportDetailList
        'grdP1Details.DataBind()

    End Sub

    Protected Sub BtnReturn_Click(sender As Object, e As System.EventArgs) Handles BtnReturn.Click
        Response.Redirect("~/P1Report.aspx", True)
    End Sub
End Class
