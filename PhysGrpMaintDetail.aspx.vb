﻿Imports System.IO
Imports System.Web


Imports System.Data
Imports System.Data.SqlClient
Imports App_Code

Imports AjaxControlToolkit
Partial Class PhysGrpMaintDetail
    Inherits System.Web.UI.Page

    Dim MyPrintingReport As New C1.C1Report.C1Report()
    Dim MyReport As New C1.C1Report.C1Report

    Dim sActivitytype As String = ""
    Dim cblBinder As New CheckBoxListBinder
    Dim UserInfo As UserSecurity
    Dim iUserNum As Integer
    Dim iUserSecurityLevel As Integer

    Dim FormSignOn As New FormSQL()


    Dim GNumber As String
    Dim GroupNum As String
    Dim CommGNumber As String
    Dim GroupTypeCd As String = ""
    Dim InvisionGroupNum As String
    Dim AccountCd As String
    Dim MaintType As String
    Dim AttachedClients As Integer

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        'GNumber = Request.QueryString("GNumber")
        'CommGNumber = Request.QueryString("CommGNumber")
        'InvisionGroupNum = Request.QueryString("InvisionGroupNum")

        UserInfo = New UserSecurity(Session("LoginID"), Session("EmployeeConn"))

        'UserInfo = Session("objUserSecurity")

        iUserNum = UserInfo.ClientNum
        iUserSecurityLevel = UserInfo.SecurityLevelNum



        If Request.QueryString("GroupNum") = "" Then

            Response.Redirect("./PhysGrpMaintenance.aspx", True)
        Else

            GroupNum = Request.QueryString("GroupNum")
            GroupnumLabel.Text = GroupNum


        End If

        FormSignOn = New FormSQL(Session("EmployeeConn"), "SelPhysGroupsV5", "UpdPhysGroupShortV8", "", Page)
        FormSignOn.AddSelectParm("@Groupnum", GroupNum, SqlDbType.NVarChar, 25)

        If Not IsPostBack Then

            Dim ddlBinderV2 As DropDownListBinder
            ddlBinderV2 = New DropDownListBinder(GroupTypesddl, "SelPhysicianGroupTypes", Session("EmployeeConn"))
            ddlBinderV2.AddDDLCriteria("@Alltypes", "no", SqlDbType.NVarChar)

            ddlBinderV2.BindData("GroupTypeCd", "GroupTypeDesc")

            FormSignOn.FillForm()

            bindAccounts()

            If GroupTypeCdTextBox.Text <> "" Then

                GroupTypeCd = GroupTypeCdTextBox.Text
                GroupTypesddl.SelectedValue = GroupTypeCdTextBox.Text

                Select Case GroupTypeCd.ToLower
                    Case "coverage"
                        Caplbl.Visible = True
                        CapCountTextBox.Visible = True


                    Case "gnum", "commnum"
                        Directlbl.Visible = True
                        DirectEmailTextBox.Visible = True

                        AKAGroupNameTextBox.Visible = True
                        MedAssitlbl.Visible = True


                    Case "invision"
                        duplicatelbl.Visible = True
                        DuplicateInvisionGroupNumTextBox.Visible = True


                End Select

            End If

            'If InvisionGroupNumLabel.Text <> "" Then
            '    CkhnSiteIDrbl.SelectedValue = "invision"
            '    CkhnSiteIDrbl.Enabled = False

            'ElseIf CkhnSiteIDLabel.Text <> "" Then

            '    If CkhnSiteIDLabel.Text = "gnumber" Then
            '        CkhnSiteIDrbl.SelectedValue = "gnumber"

            '    ElseIf CkhnSiteIDLabel.Text.ToLower = "comm" Then
            '        CkhnSiteIDrbl.SelectedValue = "comm"

            '    End If
            'Else
            '    CkhnSiteIDrbl.SelectedValue = "nonperfered"
            'End If


            If AttachedClients = 0 Then
                Deactivatelbl.Visible = True
                DateDeactivatedrbl.Visible = True

                GroupTypesddl.Visible = True
                GroupTypeCdTextBox.Visible = False
            End If
            If AttachedClients > 0 Then
                ' Al long as clients exists attached to this number no Modifications
                GroupTypeCdTextBox.Enabled = False

            End If

            If iUserSecurityLevel < 70 Then
                Response.Redirect("~/SimpleSearch.aspx", True)
            ElseIf iUserSecurityLevel < 90 Then
                BtnUpdateGroup.Visible = False
            End If



            'If UserInfo.NtLogin.ToLower = "melej" Or UserInfo.NtLogin.ToLower = "geip00" Then

            '    GroupIDTextBox.Enabled = True
            'Else
            '    GroupIDTextBox.Enabled = False
            'End If

        End If
    End Sub

    Protected Sub bindAccounts()
        ErrorLbl.Visible = False


        Dim thisData As New CommandsSqlAndOleDb("SelPhysClientsByV4", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@groupNum", GroupnumLabel.Text, SqlDbType.Int)

        'If InvisionGroupNum <> "" Then
        '    thisData.AddSqlProcParameter("@InvisionGroupNum", InvisionGroupNum, SqlDbType.NVarChar)

        'ElseIf GNumber <> "" Then
        '    thisData.AddSqlProcParameter("@Gnumber", GNumber, SqlDbType.NVarChar)
        'ElseIf CommGNumber <> "" Then
        '    thisData.AddSqlProcParameter("@CommGNumber", CommGNumber, SqlDbType.NVarChar)
        'Else
        '    thisData.AddSqlProcParameter("@groupNum", GroupNum, SqlDbType.Int)
        'End If


        Dim dt As DataTable

        dt = thisData.GetSqlDataTable

        AttachedClients = dt.Rows.Count()

        gvAccounts.DataSource = dt
        gvAccounts.DataBind()


        gvAccounts.SelectedIndex = -1


    End Sub
    Protected Sub gvAccounts_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvAccounts.RowCommand
        If e.CommandName = "Select" Then

            Dim ClientNum As String = gvAccounts.DataKeys(Convert.ToInt32(e.CommandArgument))("client_num").ToString()

            gvAccounts.SelectedIndex = -1

            Response.Redirect("./ClientDemo.aspx?ClientNum=" & ClientNum, True)


        End If
    End Sub

    Protected Sub BtnAddReset_Click(sender As Object, e As System.EventArgs) Handles BtnAddReset.Click
        Response.Redirect("./PhysGrpMaintenance.aspx", True)

    End Sub

    Protected Sub BtnUpdateGroup_Click(sender As Object, e As System.EventArgs) Handles BtnUpdateGroup.Click

        'If CkhnSiteIDLabel.Text <> "" Then
        '    ' Select CkhnSiteIDLabel.Text

        '    Select Case CkhnSiteIDLabel.Text
        '        Case "gnumber", "comm"
        '            If GNumberTextBox.Text = "" Then
        '                ErrorLbl.Text = "Need GNumber to Identify"
        '                ErrorLbl.ForeColor = Color.DarkRed
        '                ErrorLbl.Visible = True
        '                Exit Sub

        '            End If

        '        Case "invision"

        '            If InvisionGroupNumTextBox.Text = "" Then
        '                ErrorLbl.Text = "Need Invision# to Identify"
        '                ErrorLbl.ForeColor = Color.DarkRed
        '                ErrorLbl.Visible = True
        '                Exit Sub

        '            End If


        '    End Select
        'End If


        'FormSignOn.AddSelectParm("@Groupnum", GroupNum, SqlDbType.NVarChar, 25)
        FormSignOn.UpdateForm()

        Response.Redirect("./PhysGrpMaintenance.aspx", True)

        'Response.Redirect("./PhysGrpMaintDetail.aspx?GroupNum=" & GroupnumLabel.Text, True)

    End Sub

    'Protected Sub CkhnSiteIDrbl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles CkhnSiteIDrbl.SelectedIndexChanged
    '    CkhnSiteIDLabel.Text = CkhnSiteIDrbl.SelectedValue

    'End Sub

    Sub CreateExcel2()
        ' '' be sure below is pasted in
        ' ''  Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)
        ' '' Confirms that an HtmlForm control is rendered for the specified ASP.NET server control at run time. 
        ' ''    End Sub
        ' GridView1.Columns.RemoveAt(0)
        Response.AddHeader("content-disposition", "attachment;filename=FileName.xls")

        Response.Charset = String.Empty
        Response.ContentType = "application/vnd.ms-excel"
        '  Response.ContentType = "application/vnd.xls"

        Dim sw As System.IO.StringWriter = New System.IO.StringWriter()

        Dim hw As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(sw)

        Dim thisData As New CommandsSqlAndOleDb("SelPhysClientsBy", Session("EmployeeConn"))

        If InvisionGroupNum <> "" Then
            thisData.AddSqlProcParameter("@InvisionGroupNum", InvisionGroupNum, SqlDbType.NVarChar)

        ElseIf GNumber <> "" Then
            thisData.AddSqlProcParameter("@Gnumber", GNumber, SqlDbType.NVarChar)
        ElseIf CommGNumber <> "" Then
            thisData.AddSqlProcParameter("@CommGNumber", CommGNumber, SqlDbType.NVarChar)
        Else
            thisData.AddSqlProcParameter("@groupNum", GroupNum, SqlDbType.Int)
        End If

        Dim dt As DataTable

        dt = thisData.GetSqlDataTable

        gvAccounts.DataSource = dt
        gvAccounts.DataBind()

        '===============================Create form to contain grid
        Dim frm As New HtmlForm()
        gvAccounts.Parent.Controls.Add(frm)
        frm.Attributes("RunAt") = "server"
        frm.Controls.Add(gvAccounts)

        'Chart1.Parent.Controls.Add(frm)
        'frm.Controls.Add(Chart1)
        'Chart1.RenderControl(hw)
        ' GridView1.RenderControl(hw)

        frm.RenderControl(hw)
        Response.Write(sw.ToString())

        Response.End()

    End Sub

    Protected Sub btnPDFReport_Click(sender As Object, e As System.EventArgs) Handles btnPDFReport.Click
        Response.Redirect("./Reports/RolesReport.aspx?ReportName=PhysGroupReport" & "&GroupNum=" & GroupNum, True)
        '            Response.Redirect("./PhysGrpMaintDetail.aspx?GroupNum=" & GroupNumS & "&GNumber=" & sGnumber & "&CommGNumber=" & sCommGnumber, True)

    End Sub

    Protected Sub GroupTypesddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles GroupTypesddl.SelectedIndexChanged
        GroupTypeCdTextBox.Text = GroupTypesddl.SelectedValue

    End Sub
End Class
