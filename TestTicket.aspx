﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="false" CodeFile="TestTicket.aspx.vb" Inherits="TestTicket" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:UpdatePanel runat="server" UpdateMode ="Conditional" ID= "uplTicket">
      <Triggers>

        <asp:AsyncPostBackTrigger ControlID="RequestTyperbl" EventName = "SelectedIndexChanged" />
    </Triggers>

     <ContentTemplate>

         <asp:table  ID="TableJeff" runat="server" BorderWidth="1" Width="100%" CellPadding = "3">
                                 <asp:TableRow>
                                <asp:TableCell CssClass="tableRowHeader" ColumnSpan="4">
                                Client Information
                                </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                             <asp:TableCell CssClass="tableRowHeader" ColumnSpan="4">
                                    <asp:Label ID="ClientnameHeader" runat="server" ></asp:Label>

                            </asp:TableCell>
                        </asp:TableRow>

                        <asp:TableRow>
                            <asp:TableCell VerticalAlign="Middle" ColumnSpan="4" >
                               <asp:Table id="tblCline" runat="server" BorderWidth="2px"  Width="99%"  Font-Size="Small">
                                    <asp:TableRow>
                                        <asp:TableCell HorizontalAlign="Right" Width="250px">
                                            Affiliation:
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Left" Width="350px" >
                                           <asp:Label ID = "emptypecdLabel" runat="server" ></asp:Label>

                                        </asp:TableCell>           
                                        <asp:TableCell HorizontalAlign="Right" Width="250px">
                                            Position:
                                        </asp:TableCell>                     
                                        <asp:TableCell HorizontalAlign="left" Width="350px" >
                                            <asp:Label ID = "UserPositionDescLabel" runat="server"></asp:Label>
                                        </asp:TableCell>
                                
                                
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell HorizontalAlign="Right">
                                         Phone Number:
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Left">
                                            <asp:Label ID = "phoneLabel" runat="server" ></asp:Label>
                                        </asp:TableCell>
       
                                        <asp:TableCell HorizontalAlign="Right" >
                                        Account Name:
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Left" >
                                            <asp:Label ID = "LoginNameLabel" runat="server" ></asp:Label>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell HorizontalAlign="Right">
                                        Fax:
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Left">
                                               <asp:Label ID = "faxLabel" runat="server" MaxLength="12"></asp:Label>
                                        </asp:TableCell>
         
                                        <asp:TableCell HorizontalAlign="Right" >
                                          Email:
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Left" >
                                            <asp:Label ID = "emailLabel" runat="server" Width="250px"></asp:Label>
                                        </asp:TableCell>
                                    </asp:TableRow>
                        
                                    <asp:TableRow>
                                        <asp:TableCell HorizontalAlign="Right" >
                                        Location:
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="left">
                                            <asp:Label ID = "FacilityNameLabel" runat="server"></asp:Label>
          
                                        </asp:TableCell>
        
                                        <asp:TableCell HorizontalAlign="Right">
                                        Building:
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Left" >
                                                <asp:Label ID = "buildingcdLabel" runat="server" ></asp:Label>
                                        </asp:TableCell>
                                    </asp:TableRow>
                        
                                    <asp:TableRow>
                                        <asp:TableCell HorizontalAlign="Right" >
                                            Floor:
                                        </asp:TableCell>

                                        <asp:TableCell HorizontalAlign="Left">
                                                 <asp:Label ID="FloorLabel" runat="server"></asp:Label>
                            
                                        </asp:TableCell>
                                        <asp:TableCell HorizontalAlign="Right">
                                            Department:
                                        </asp:TableCell>

                                        <asp:TableCell HorizontalAlign="Left">
                                            <asp:Label ID ="department_nameLabel" runat="server"></asp:Label>
                                        </asp:TableCell>

                                    </asp:TableRow>
                                </asp:Table>
                            </asp:TableCell>
                        </asp:TableRow>

                       <asp:TableRow>
                          <asp:TableCell CssClass="tableRowSubHeader" VerticalAlign="Middle" ColumnSpan="4">
                                     Select Request Type :
                                    <asp:RadioButtonList ID="RequestTyperbl" runat="server" AutoPostBack="true">
                                         <asp:ListItem Value="ticket" Text="Ticket"></asp:ListItem>
                                         <asp:ListItem Value="rfs" Text="Req. For Service"></asp:ListItem>
                                    
                                    </asp:RadioButtonList>
                          </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell ColumnSpan="4" VerticalAlign="Middle">
                                 <asp:Panel Visible="False" runat="server" ID="ticketPnl">

                                    Ticket 
                                    <asp:Table ID="Table1"  BorderWidth="3px" Width="100%" runat="server">
                                        <asp:TableRow>
                                            <asp:TableCell  ColumnSpan="4"  HorizontalAlign="Left" Width="100%">
                                            
                                                <asp:Label id="shorDeslbl" runat="server" Text = "Short Description" ></asp:Label>
                                            </asp:TableCell>

                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell  ColumnSpan="4"  VerticalAlign="Middle" width="100%">
                                                <asp:TextBox ID="short_descTextBox" runat="server" TextMode="MultiLine" 
                                                    Width="700px"></asp:TextBox>
                                            </asp:TableCell>
                                        
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Right" Width="150px">
                                                <asp:Label ID="altContactTicket" runat="server" Text="Alternate Contact"></asp:Label>
                                            </asp:TableCell >

                                            <asp:TableCell HorizontalAlign="Left" Width="450px">
                                                <asp:TextBox ID="alternate_contact_nameTextBox" runat="server"></asp:TextBox>

                                            </asp:TableCell>

                                            <asp:TableCell HorizontalAlign="Right"  Width="150px">
                                                  <asp:Label ID="assettagLbl" runat="server" Text="Asset Tag"></asp:Label>

                                            </asp:TableCell>

                                            <asp:TableCell HorizontalAlign="Left" Width="450px" >
                                                <asp:TextBox ID="asset_tagTextBox" runat="server"></asp:TextBox>

                                            </asp:TableCell>
                                        
                                        </asp:TableRow>
                                    
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Right">
                                                <asp:Label ID="altContactPhoneTicket" runat="server" Text="Alternate Phone"></asp:Label>
                                            </asp:TableCell >

                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:TextBox ID="alternate_contact_phoneTextBox" runat="server"></asp:TextBox>

                                            </asp:TableCell>

                                            <asp:TableCell HorizontalAlign="Right">
                                                <asp:Label ID="prioritylbl" runat="server" Text="Priority"></asp:Label>

                                            </asp:TableCell>

                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:RadioButtonList ID="priorityrbl" runat="server"  Enabled ="false" RepeatDirection="Horizontal">
                            
                                                <asp:ListItem Value="1" Text="1"></asp:ListItem>
                                                <asp:ListItem Value="2" Text="2"></asp:ListItem>
                                                <asp:ListItem Value="3" Text="3" Selected="True"></asp:ListItem>
                                             
                                            </asp:RadioButtonList>                                                
                                            </asp:TableCell>
                                        
                                        </asp:TableRow>
                                    
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Right">
                                                <asp:Label ID="proxlbl" runat="server" Text="Proxy "></asp:Label>
                                            </asp:TableCell>
                                            
                                            <asp:TableCell>
                                                <asp:TextBox id="proxy_on_pcTextBox" runat="server" ></asp:TextBox>
                                            </asp:TableCell>

                                            <asp:TableCell  HorizontalAlign="Right">
                                                <asp:Label ID="iplbl" runat="server" Text="Ip Address"></asp:Label>
                                            </asp:TableCell>

                                            <asp:TableCell>
                                                <asp:TextBox ID="ip_addressTextBox" runat="server"></asp:TextBox>
                                            </asp:TableCell>
                                        
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell ColumnSpan="4">
                                              <asp:Table ID="Table4" runat="server">
                                               
                                               <asp:TableRow>
                                                  <asp:TableCell>
                                                    <asp:Label ID="Catlbl" runat="server" Text = "Category"></asp:Label>
                                                  </asp:TableCell>    
                                                
                                                  <asp:TableCell>
                                                    <asp:DropDownList ID="category_cdddl" runat="server" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                  </asp:TableCell>    

                                                   <asp:TableCell>
                                                      <asp:Label ID="Typelbl" runat="server" Text = "Type"></asp:Label>
                                                   </asp:TableCell>
  
                                                  <asp:TableCell>
                                                    <asp:DropDownList ID="type_cdddl" runat="server" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                  </asp:TableCell> 
                                               
                                                   <asp:TableCell>
                                                        <asp:Label ID="Itemlbl" runat="server" Text = "Item"></asp:Label>
                                                   </asp:TableCell>    
                                                 
                                                  <asp:TableCell>
                                                    <asp:DropDownList ID="item_cdddl" runat="server" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                  </asp:TableCell>  
                                                                                              
                                              </asp:TableRow>                                        
                                            </asp:Table>
                                            </asp:TableCell>
                                        </asp:TableRow>

                                        <asp:TableRow>

                                        <asp:TableCell ColumnSpan="4">
                                         <asp:GridView ID="GridOnCall" runat="server" AllowSorting="True" 
                                           AutoGenerateColumns="False" BackColor="White" BorderColor="#336666" 
                                           BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                                            DataKeyNames="group_num,tech_num"
                                           EmptyDataText="No On Call" 
                                           EnableTheming="False" Font-Size="Medium" GridLines="Horizontal" Width="100%">
                                           <FooterStyle BackColor="White" ForeColor="#333333" />
                                           <HeaderStyle BackColor="#336666" Font-Bold="False" Font-Size="Small" 
                                               ForeColor="White" HorizontalAlign="Left" />
                                           <PagerStyle BackColor="#336666" Font-Size="Medium" ForeColor="White" 
                                               HorizontalAlign="Left" />
                                           <RowStyle BackColor="White" ForeColor="#333333" />
                                           <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                           <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                           <SortedAscendingHeaderStyle BackColor="#487575" />
                                           <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                           <SortedDescendingHeaderStyle BackColor="#275353" />
                                           <Columns>

                                                <asp:CommandField ButtonType="Button" SelectText="Select" ShowSelectButton= "true" HeaderStyle-HorizontalAlign = "Left"  ControlStyle-BackColor="#84A3A3" ItemStyle-Height="30px" />

                                               <asp:BoundField DataField="group_name" HeaderText="On Call Group Name" />

                                               <asp:BoundField DataField="techname" HeaderText="Currently On Call" />


                                           </Columns>
                                       </asp:GridView>
                                      </asp:TableCell>                     
                                     </asp:TableRow> 

                                     <asp:TableRow>
                                        <asp:TableCell  ColumnSpan="4"   VerticalAlign="Middle">
                                            <asp:Button ID="btnSubmitTicket" runat="server" Font-Bold="True" 
                                                Font-Names="Arial" Font-Size="Small" Height="20px" Text="Submit Ticket" 
                                                    Width="200px" />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    
                                 </asp:Table>

                                  </asp:Panel>
                             </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell ColumnSpan="4" VerticalAlign="Middle">
                               <asp:Panel Visible="False" runat="server" ID="rfsPnl">
                                  <asp:Table ID="Table2"  BorderWidth="3px" Width="100%" runat="server">

                                       <asp:TableRow  CssClass="tableRowSubHeader">
                                            <asp:TableCell  ColumnSpan="4">
                                                Description / Explanation
                                            </asp:TableCell>
                                       </asp:TableRow>

                                       <asp:TableRow >
                                            <asp:TableCell ColumnSpan="4">
                                                <asp:TextBox runat="server" ID="TextBox1" Width="95%" TextMode="MultiLine" Rows = "3"></asp:TextBox>
                                            </asp:TableCell>
                                       </asp:TableRow>

                                       <asp:TableRow  CssClass="tableRowSubHeader">
                                            <asp:TableCell  ColumnSpan="4">
                                                Business Justification / Reason for Request
                                            </asp:TableCell>
                                       </asp:TableRow>

                                       <asp:TableRow>
                                            <asp:TableCell ColumnSpan="4">
                                                <asp:TextBox runat="server" ID ="txtBusinessDesc" TextMode="MultiLine"  Width="95%" Rows ="3"></asp:TextBox>
                                            </asp:TableCell>
                                       </asp:TableRow>

                                       <asp:TableRow  CssClass="tableRowSubHeader">
                                            <asp:TableCell ColumnSpan="4">
                                                List Any Known Departments or Systems This Change Will Impact, or any Deadlines That Could Affect This Request
                                            </asp:TableCell>
                                       </asp:TableRow>

                                       <asp:TableRow>
                                            <asp:TableCell ColumnSpan="4">
                                                <asp:TextBox runat="server" ID="txtImpactDesc" TextMode="MultiLine"  Width="95%" Rows="4"></asp:TextBox>
                                            </asp:TableCell>
                                       </asp:TableRow>

                                       <asp:TableRow  CssClass="tableRowSubHeader">
                                          <asp:TableCell ColumnSpan="4">
                                               Technical Services
                                                </asp:TableCell>
                                       </asp:TableRow>

                                       <asp:TableRow>
                                            <asp:TableCell ColumnSpan="4">
                                                 <asp:Table ID="Table3" runat="server" >
                                                     <asp:TableRow>
                                                        <asp:TableCell>
                                                            Technical Service
                                                        </asp:TableCell>
                                                        <asp:TableCell>
                                                            Number of Devices
                                                        </asp:TableCell>
                                                        <asp:TableCell>
                                                            Device IDs
                                                        </asp:TableCell>
                                                        <asp:TableCell>
                                                            Description
                                                        </asp:TableCell>
                                                        <asp:TableCell>
                                                            Submit
                                                        </asp:TableCell>
                                                    </asp:TableRow>

                                                    <asp:TableRow VerticalAlign="Top">
                                                        <asp:TableCell>
                                                            <asp:DropDownList runat="server" ID="ddlTechService"></asp:DropDownList>
                                                        </asp:TableCell>
                                                        <asp:TableCell>
                                                            <asp:TextBox runat="server" ID = "txtNumTechDevices"></asp:TextBox>
                                                        </asp:TableCell>
                                                        <asp:TableCell>
                                                            <asp:TextBox runat="server" ID="txtTechDeviceIDs"></asp:TextBox>
                                                        </asp:TableCell>
                                                        <asp:TableCell>
                                                        <asp:TextBox runat="server" ID ="txtTechServiceDesc" TextMode="MultiLine"></asp:TextBox>

                                                        </asp:TableCell>
                                                        <asp:TableCell>

                                                        <asp:Button runat="server" ID ="btnAddTechService" Text="Add Tech Service" />

                                                        </asp:TableCell>
                                                      </asp:TableRow>
                                                    </asp:Table>
                                            </asp:TableCell>
                                       </asp:TableRow>

                                        <asp:TableRow>
                                         <asp:TableCell ColumnSpan = "4">
                                            <asp:GridView ID="gvTechServices" runat="server" AllowSorting="True" 
                                                AutoGenerateColumns="False" BackColor="White" BorderColor="#336666"
                                                BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                                                EmptyDataText="" EnableTheming="False" DataKeyNames="SeqNum"
                                                Font-Size="Medium" GridLines="Horizontal" Width="100%">
                                   
                                                    <FooterStyle BackColor="White" ForeColor="#333333" />
                                                    <HeaderStyle BackColor="#336666" Font-Bold="False" ForeColor="White" Font-Size="Small" HorizontalAlign="Left"    />                                    
                                                    <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Left" Font-Size="Medium"/>
                                                    <RowStyle BackColor="White" ForeColor="#333333" />
                                                    <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                                    <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                                    <SortedAscendingHeaderStyle BackColor="#487575" />
                                                    <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                                    <SortedDescendingHeaderStyle BackColor="#275353" />

                                                    <Columns>
                                                                                            
                                                                <asp:CommandField ButtonType="Button" SelectText="Remove" ShowSelectButton= "true" />
                                                                <asp:BoundField  DataField="TechServiceDesc"  HeaderText="" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
				                                                <asp:BoundField  DataField="NumOfDevices"  HeaderText="" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
                                                                <asp:BoundField  DataField="DeviceID"  HeaderText="" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
                                                                <asp:BoundField  DataField="DetailInformation"  HeaderText="" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
                                  
                                                    </Columns>

                                                </asp:GridView>
                                          </asp:TableCell>
                                        </asp:TableRow>                
                                        <asp:TableRow>
                                            <asp:TableCell ColumnSpan = "4">
                                                <asp:Button runat="server" ID="btnSubmit" Text="Submit" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    


                                    </asp:Table>
                                  </asp:Panel>
                             </asp:TableCell>
                        </asp:TableRow>
                                                <asp:TableRow>
                            <asp:TableCell ColumnSpan = "4">
                                    <asp:Label ID="client_numlabel" runat="server" Visible="False"></asp:Label>
                                    <asp:Label ID ="departmentcdLabel" runat="server" Visible="False"></asp:Label>
                                     <asp:Label ID ="TitleLabel" runat="server" Visible="False"></asp:Label>
                                     
                                     <asp:Label ID = "entitycdLabel" runat="server" Visible ="False"></asp:Label>
                                     <asp:Label ID= "buildingnameLabel" runat="server" Visible="False" ></asp:Label>
                                     <asp:Label ID="HDemployeetype" runat="server" Visible="False"></asp:Label> 
                                     <asp:Label ID="suffixlabel" runat="server" Visible="False"></asp:Label>
                                     <asp:Label ID= "AccountRequestTypeLabel" runat="server" Visible="False"></asp:Label>
                                     <asp:Label ID="type_cd" runat="server" Visible="False"></asp:Label>
                                     <asp:Label ID="category_cd"  runat="server" Visible="False"></asp:Label>
                                     <asp:Label ID="RequestType" runat="server" Visible="False"></asp:Label>
                                     
                                    <asp:Label ID="item_cd" runat="server" visible="False" />
                                    <asp:Label ID="requestor_client_numLabel" runat="server" visible="False"></asp:Label>
                                    <asp:Label ID="LocationOfCareIDLabel"  runat="server" visible="False"></asp:Label>
                                    <asp:Label ID ="CoverageGroupNumLabel"  runat="server" visible="False"></asp:Label>
                                    <asp:Label id="NonPerferedGroupnumLabel" runat="server" Visible="False"></asp:Label>

                                    <asp:Label ID="CommLocationOfCareIDLabel" runat="server" visible="False"></asp:Label>
                                     <asp:Label ID="PositionRoleNumLabel" runat="server" Visible="False"></asp:Label>
                                     <asp:Label id="invisionLbl" runat="server" Text="eCare Group Number:"   
                                        Visible="False"></asp:Label>
                                     <asp:TextBox ID="groupnumTextBox" runat="server" Enabled="False" 
                                        Visible="False"></asp:TextBox>
                                     <asp:TextBox ID="InvisionGroupNumTextBox" runat="server"  Enabled="False" 
                                        Visible="False"></asp:TextBox>
                                 </asp:TableCell>
                        </asp:TableRow>

        </asp:table>
    </ContentTemplate>
</asp:UpdatePanel>

</asp:Content>

