﻿Imports System.IO
Imports System.Data '
Imports System.Data.SqlClient
Imports App_Code
Imports AjaxControlToolkit
Imports C1.Web.UI.Controls.C1Window

Partial Class MyRequests
    Inherits MyBasePage '   Inherits System.Web.UI.Page

    Dim UserInfo As UserSecurity

    Dim sActivitytype As String = ""
    Dim cblBinder As New CheckBoxListBinder
    Dim FormSignOn As New FormSQL()
    Dim AccountRequestNum As Integer
    Dim RequestType As String


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'UserInfo = Session("objUserSecurity")

        UserInfo = New UserSecurity(Session("LoginID"), Session("EmployeeConn"))

        WhoclientNumLabel.Text = UserInfo.ClientNum


        If Not IsPostBack Then

            Requestheadlabel.Text = "My Requests as Requestor"
            btnChange.Text = "View Requests as Submitter"
            RequestType = "myrequest"

            '    bindValidateQueue()

            alltimelabel.Text = ""

            fillProviderTable()
            'fillRequestItemsTable()

        End If
    End Sub
    Protected Sub fillProviderTable()


        Dim thisData As New CommandsSqlAndOleDb("SelMyRequestsV4", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@client_num", WhoclientNumLabel.Text, SqlDbType.Int, 20)
        thisData.AddSqlProcParameter("@RequestType", RequestType, SqlDbType.NVarChar, 12)

        If alltimelabel.Text = "yes" Then
            thisData.AddSqlProcParameter("@alltime", alltimelabel.Text, SqlDbType.NVarChar, 12)
        Else
            allrequestsrbl.SelectedValue = "no"
            alltimelabel.Text = ""
        End If
        Dim iRowCount = 1

        Dim dt As DataTable
        dt = thisData.GetSqlDataTable

        Dim rwHeader As New TableRow
        Dim clSelectHeader As New TableCell
        Dim clNameHeader As New TableCell
        Dim clRequestTypeHeader As New TableCell
        Dim clAccountNameHeader As New TableCell
        Dim clReqPhoneHeader As New TableCell

        Dim clRequestDateHeader As New TableCell
        Dim clRequestDateClosedHeader As New TableCell

        Dim clStatusHeader As New TableCell


        clNameHeader.Text = "  For  "
        clRequestTypeHeader.Text = "  Type  "
        clAccountNameHeader.Text = "  By   "
        clReqPhoneHeader.Text = " Phone  "

        clRequestDateHeader.Text = " Date Entered "
        clRequestDateClosedHeader.Text = "  Date Closed  "
        clStatusHeader.Text = "  Status "

        rwHeader.Cells.Add(clSelectHeader)
        rwHeader.Cells.Add(clNameHeader)
        rwHeader.Cells.Add(clRequestTypeHeader)
        rwHeader.Cells.Add(clAccountNameHeader)
        rwHeader.Cells.Add(clReqPhoneHeader)
        rwHeader.Cells.Add(clRequestDateHeader)
        rwHeader.Cells.Add(clRequestDateClosedHeader)

        rwHeader.Cells.Add(clStatusHeader)

        rwHeader.Font.Bold = True
        rwHeader.BackColor = Color.FromName("#006666")
        rwHeader.ForeColor = Color.FromName("White")
        rwHeader.Height = Unit.Pixel(36)

        tblProviderQueue.Rows.Add(rwHeader)

        For Each drRow As DataRow In dt.Rows

            'Dim btnSelect As New Button
            Dim rwData As New TableRow
            Dim btnSelect As New TableCell
            Dim clSelectData As New TableCell
            Dim clNameData As New TableCell
            Dim clRequestTypeData As New TableCell

            Dim clAccountNameData As New TableCell

            Dim clRequestorData As New TableCell
            Dim clReqPhoneData As New TableCell
            Dim clRequestDateData As New TableCell
            Dim clRequestCloseDateData As New TableCell
            Dim clStatusData As New TableCell


            btnSelect.Text = "<a href=""AccountRequestInfo2.aspx?RequestNum=" & drRow("account_request_num") & """ ><u><b> " & drRow("account_request_num") & "</b></u></a>"
            'btnSelect.CommandName = "Select"
            ' btnSelect.CommandArgument = drRow("account_request_num") ' & ";" & sFirstName & ";" & sLastName
            ' btnSelect.Width = 50
            btnSelect.EnableTheming = False

            ' AddHandler btnSelect.Click, AddressOf btnSelect_Click


            ' receiver_full_name,
            'requestor_name,
            'requestor_phone,
            'entered_date
            'closed_date 
            'CurrentStatus




            'clSelectData.Controls.Add(btnSelect)

            clNameData.Text = IIf(IsDBNull(drRow("receiver_full_name")), "", drRow("receiver_full_name"))
            clRequestTypeData.Text = IIf(IsDBNull(drRow("account_request_type_Desc")), "", drRow("account_request_type_Desc"))
            clRequestorData.Text = IIf(IsDBNull(drRow("requestor_name")), "", drRow("requestor_name"))
            clReqPhoneData.Text = IIf(IsDBNull(drRow("requestor_phone")), "", drRow("requestor_phone"))
            clRequestDateData.Text = (IIf(IsDBNull(drRow("entered_date")), "", drRow("entered_date")))
            clRequestCloseDateData.Text = (IIf(IsDBNull(drRow("closed_date")), "", drRow("closed_date")))
            clStatusData.Text = (IIf(IsDBNull(drRow("CurrentStatus")), "", drRow("CurrentStatus")))


            'rwData.Cells.Add(clSelectData)
            rwData.Cells.Add(btnSelect)
            rwData.Cells.Add(clNameData)
            rwData.Cells.Add(clRequestTypeData)
            rwData.Cells.Add(clRequestorData)
            rwData.Cells.Add(clReqPhoneData)
            rwData.Cells.Add(clRequestDateData)
            rwData.Cells.Add(clRequestCloseDateData)
            rwData.Cells.Add(clStatusData)


            If iRowCount > 0 Then

                rwData.BackColor = Color.Bisque

            Else

                rwData.BackColor = Color.LightGray

            End If

            tblProviderQueue.Rows.Add(rwData)

            iRowCount = iRowCount * -1

        Next
        tblProviderQueue.Width = New Unit("100%")

    End Sub



    Protected Sub fillRequestItemsTable()
        Dim thisData As New CommandsSqlAndOleDb("SelRFSTicketsByClient", Session("CSCConn"))
        thisData.AddSqlProcParameter("@clientnum", Session("ClientNum"), SqlDbType.Int)
        'thisData.AddSqlProcParameter("@status", "open", SqlDbType.NVarChar)

        Dim iRowCount = 1

        Dim dt As DataTable
        dt = thisData.GetSqlDataTable

        Dim rwHeader As New TableRow

        Dim clBtnheader As New TableCell
        Dim clRequestTypeHeader As New TableCell
        Dim clDateEnteredHeader As New TableCell
        Dim clStatusHeader As New TableCell
        Dim clDescheader As New TableCell

        clBtnheader.Text = ""
        clRequestTypeHeader.Text = "Type"
        clDateEnteredHeader.Text = "Date Entered"
        clStatusHeader.Text = "Status"
        clDescheader.Text = "Description"


        rwHeader.Cells.Add(clBtnheader)
        rwHeader.Cells.Add(clRequestTypeHeader)
        rwHeader.Cells.Add(clDateEnteredHeader)
        rwHeader.Cells.Add(clStatusHeader)
        rwHeader.Cells.Add(clDescheader)


        rwHeader.Font.Bold = True
        rwHeader.BackColor = Color.FromName("#006666")
        rwHeader.ForeColor = Color.FromName("White")
        rwHeader.Height = Unit.Pixel(36)

        tblRFSTickets.Rows.Add(rwHeader)

        For Each drRow As DataRow In dt.Rows


            Dim rwData As New TableRow

            Dim clBtn As New TableCell

            Dim clReqNum As New TableCell

            Dim claccountRequestNum As New TableCell
            Dim clRequestTypeData As New TableCell
            Dim clDateEntered As New TableCell
            Dim clStatusData As New TableCell
            Dim clDescData As New TableCell

            claccountRequestNum.Text = IIf(IsDBNull(drRow("RequestNum")), "", drRow("RequestNum"))

            clRequestTypeData.Text = IIf(IsDBNull(drRow("RequestType")), "", drRow("RequestType"))
            clDateEntered.Text = IIf(IsDBNull(drRow("EntryDate")), "", drRow("EntryDate"))

            clStatusData.Text = IIf(IsDBNull(drRow("CurrStatus")), "", drRow("CurrStatus"))
            clDescData.Text = IIf(IsDBNull(drRow("ReqDesc")), "", drRow("ReqDesc"))
            clReqNum.Text = "<a href=""ViewRequestTicket.aspx?RequestNum=" & claccountRequestNum.Text & """ ><u><b> " & claccountRequestNum.Text & "</b></u></a>"

            'If clRequestTypeData.Text.ToLower = "ticket" Then
            '    clReqNum.Text = "<a href=""ViewRequestTicket.aspx?RequestNum=" & claccountRequestNum.Text & """ ><u><b> " & claccountRequestNum.Text & "</b></u></a>"
            'Else
            '    clReqNum.Text = "<a href=""http://intranet01/csc/update_rfs.asp?id=" & claccountRequestNum.Text & """ ><u><b> " & claccountRequestNum.Text & "</b></u></a>"
            'End If

            'If Session("SecurityLevelNum") < 70 Then
            '    clReqNum.Text = claccountRequestNum.Text

            'End If

            rwData.Cells.Add(clReqNum)
            rwData.Cells.Add(clRequestTypeData)

            rwData.Cells.Add(clDateEntered)
            rwData.Cells.Add(clStatusData)
            rwData.Cells.Add(clDescData)


            If iRowCount > 0 Then

                rwData.BackColor = Color.Bisque

            Else

                rwData.BackColor = Color.LightGray

            End If

            tblRFSTickets.Rows.Add(rwData)

            iRowCount = iRowCount * -1

        Next
        tblRFSTickets.CellPadding = 10
        tblRFSTickets.Width = New Unit("100%")


    End Sub

    Sub btnSelect_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim btnLinkView As Button = sender

        If btnLinkView.CommandName = "Select" Then




            Dim sArguments As String() = Split(btnLinkView.CommandArgument, ";")

            Response.Redirect("./AccountRequestInfo2.aspx?RequestNum=" & sArguments(0))

            '            Response.Redirect("./AccountRequestItems.aspx?RequestNum=" & sArguments(0))



        End If



    End Sub
    Protected Sub btnChange_Click(sender As Object, e As System.EventArgs) Handles btnChange.Click
        If btnChange.Text = "View My Requests" Then
            btnChange.Text = "View Requests as Submitter"
            Requestheadlabel.Text = "My Requests as Requestor"

            RequestType = "myrequest"

        Else
            Requestheadlabel.Text = "Requests I Submitter for Others"

            btnChange.Text = "View My Requests"

            RequestType = "submitter"


        End If


        tblProviderQueue.Rows.Clear()
        fillProviderTable()
        fillRequestItemsTable()


    End Sub

    Protected Sub allrequestsrbl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles allrequestsrbl.SelectedIndexChanged

        If allrequestsrbl.SelectedValue = "yes" Then
            tblProviderQueue.Dispose()

            alltimelabel.Text = "yes"
        Else
            alltimelabel.Text = ""

        End If
        tblProviderQueue.Rows.Clear()
        fillProviderTable()
        fillRequestItemsTable()

    End Sub
End Class
