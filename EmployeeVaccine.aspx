﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="false" CodeFile="EmployeeVaccine.aspx.vb" Inherits="EmployeeVaccine" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <script type="text/javascript" src="Scripts/FieldValidator.js"></script>
<script type="text/javascript">
    function pageLoad(sender, args) {

        fieldValidator('textInputRequired', 'btnUpdate', 'lblValidation');

    }
</script>
  <style type="text/css">
    .style2
    {
        text-align: center;
    }
    .style3
    {
        width: 100%;/**/
    }
    .styleLot
    {
        font-weight:bold
    }
       
     .styleInfo
    {
         color:Navy;
         font-size:14px;
         font-weight:bolder;
    }
    .styletext
    {
        width: 25%
    }
    
    .textHasFocus
    {
         border-color:Red;
        border-width:medium;
        border-style:outset;
    } 
        
        .style7
        {
            width: 812px;
        }
        
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
<asp:UpdatePanel ID="uplSignOnForm" runat="server"  UpdateMode= "Conditional" >
  <ContentTemplate>
 <table width= "100%">
        <tr>
            <td align="center">

                <table>
                    <tr>
                        <td>
                            <asp:Button ID = "btnSubmitVaccine" runat = "server" Text ="Submit Vaccine Form" 
                            Width="250px"  BackColor="#336666" ForeColor="White" />
                        </td>
                        <td>
                            <asp:Button ID="btnReturnSearch" runat="server" Text = "Return To Search" Width="250px"
                                BackColor="#336666" ForeColor="White" />
                                                    
                        </td>
                        <td>


                        </td>
                    </tr>
                              
                    <tr>
                        <td align ="center">
                        <asp:Label ID = "lblValidation" runat = "server" ForeColor="Red" Font-Size="15px" Font-Bold="True" />
                        </td>
                   </tr>
                  
                </table>
           
            </td>
        </tr>
      <tr>
        <td align="center">
          <asp:Panel id="panDemo" runat="server">
             <table border="1" style="width: 100%">
                <tr>
                        <td class="tableRowHeader" colspan="4" >
                            Vaccine Form Submission
                        </td>
                </tr>
                <tr>
                       <td class="tableRowHeader" colspan="4" >
                            <asp:Label ID="ClientnameHeader" runat="server" ></asp:Label>
                        </td>
                </tr>
                                   
                <tr>
                    <td class="tableRowSubHeader" colspan="4"  align="left">
                     CKHS Affiliation:
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="4" >
                        <asp:RadioButtonList ID ="emp_type_cdrbl" runat = "server" 
                            RepeatDirection="Horizontal" AutoPostBack="True" Font-Size="Smaller">
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                 <td class="tableRowSubHeader"  align="left" colspan="4">
                           Demographic Information
                 </td>
                </tr>
                <tr>
                   <td colspan="4" >
                      <table class="style3" > 
                        <tr>
                            <td>
                                First Name:
                            </td>
                            <td align="left"  >
                                <asp:TextBox width="280px" ID = "first_nameTextBox" runat="server" ></asp:TextBox>
                            </td>
                            <td>
                                MI:
                            </td>
                            <td>
                                <asp:TextBox ID = "miTextBox" runat="server" Width="15px"></asp:TextBox>
                            </td>
                            <td align="left" >
                                Last Name:
                            </td>

                            <td align="left" >
                                <asp:TextBox ID = "last_nameTextBox" width="280px" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                      </table>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="employeelbl" runat="server" text="Employee #"></asp:Label>
                        
                    </td>
                    <td>
                        <asp:Label ID="siemens_emp_numLabel" runat="server"></asp:Label>
                    </td>
                    <td align="right">
                        <asp:Label ID="positlbl" runat="server" Text="Position:" />
                    </td>
                    <td>
                        <asp:Label id="user_position_descLabel" runat="server"></asp:Label>
                    </td>
                </tr>

                <tr>
                    <td align="right" width="20%" >
                        <asp:Label ID="lblpos" runat="server" Text="Position Description:" />
                    </td>
                    <td colspan = "3" align="left" >
                        <asp:DropDownList ID="Rolesddl" runat="server"  Width="250px" AutoPostBack="True"  >
                                    <asp:ListItem Value="Select" Text="Select" ></asp:ListItem>
                                    <asp:ListItem Value="Volunteer" Text="Volunteer" ></asp:ListItem>
                                    <asp:ListItem Value="Student" Text="Student" ></asp:ListItem>
                                    <asp:ListItem Value="ClinicialAffiliate" Text="ClinicialAffiliate" ></asp:ListItem>


                        </asp:DropDownList>

                    </td>
                </tr>
                <tr>
                    <td align="right" >
                    Entity:
                    </td>
                    <td align="left">
                    <asp:DropDownList ID ="entity_cdddl" runat="server" AutoPostBack ="True" >
            
                    </asp:DropDownList>
                    </td>
       
                    <td align="right" >
                        <asp:Label ID="lbldept" runat="server" Text = "Department Name:" />
                    </td>
                    <td align="left"  >
                    <asp:DropDownList ID ="department_cdddl" runat="server" Width="250px" AutoPostBack="True" >
            
                    </asp:DropDownList>
                    </td>
                </tr>

<%--                <tr>
                    <td align="right" >
                    Location:
                    </td>
                    <td align="left" >
         
                        <asp:DropDownList ID ="facility_cdddl" runat="server" AutoPostBack="True">
              
                        </asp:DropDownList>
          
                    </td>
        
                    <td align="right" >
                    Building:
                    </td>
                    <td align="left" >
                        <asp:DropDownList ID="building_cdddl" runat="server" AutoPostBack="True" >
                        </asp:DropDownList>
          
                    </td>
                </tr>

                <tr>
                        <td align="right" >
                            Floor:
                        </td>

                        <td align="left">
                            <asp:DropDownList ID="Floorddl" runat="server" AutoPostBack="True">
            
                            </asp:DropDownList>
                            
                        </td>
                        <td>
                            
                        </td>

                        <td>
                            
                        </td>

                </tr>--%>

                
                <tr>
                    <td colspan = "4">
                        <asp:Label ID="HDemployeetype" runat="server" Visible="False"></asp:Label>
                    </td>
                </tr>
             </table>
           </asp:Panel>
         </td>
        </tr>
        <tr >
         <td align="center">
             <asp:Panel ID="panQuestions" runat="server">
                 <table border="1" style="width: 100%" >
                    <tr>
                        <td colspan="4" align="center" class="styleLot">
                            <table>
                                <tr>
                                    <td>
                                        Lot Number:&nbsp
                                        <asp:Label ID="LotNumberLabel" runat="server" ></asp:Label>
                                    </td>
                                    
                                    <td>
                                        &nbsp;&nbsp; -- Expiration Date:&nbsp
                                        <asp:Label ID="ExpirationDateLabel" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                       &nbsp;&nbsp; -- Manufacturer:&nbsp
                                        <asp:Label ID="ManufacturerLabel" runat="server"></asp:Label>
                                    </td>
                                
                                
                                </tr>                            
                            
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" class="styletext">
<%--                            <asp:Label ID="AElbl" Text ="Allergic To Eggs:" runat="server" ></asp:Label>
--%>                            Allergic To Eggs:
                        </td>
                        <td align="left" class="styletext" >
                                <asp:RadioButtonList ID = "AllergicEggsrbl" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem>Yes</asp:ListItem>
                                <asp:listItem>No</asp:listItem>
                            </asp:RadioButtonList>

                        </td>
                        <td align="right" class="styletext">
                            Allergic To Latex:
                        </td>
                        <td align="left" class="styletext" >
                                <asp:RadioButtonList ID = "Allergiclatexrbl" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem>Yes</asp:ListItem>
                                <asp:listItem>No</asp:listItem>
                            </asp:RadioButtonList>

                        </td>

                    </tr>
                    <tr>
                        <td align="right">
                            Allergic Thimerosal:
                        </td>
                        <td align="left" >
                                <asp:RadioButtonList ID = "AllergicThimerosalrbl" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem>Yes</asp:ListItem>
                                <asp:listItem>No</asp:listItem>
                            </asp:RadioButtonList>

                        </td>
                        <td align="right">
                            Do You Feel Sick:
                        </td>
                        <td align="left" >
                                <asp:RadioButtonList ID = "FeelSickrbl" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem>Yes</asp:ListItem>
                                <asp:listItem>No</asp:listItem>
                            </asp:RadioButtonList>

                        </td>

                    </tr>

                    <tr>
                        <td align="right">
                            History of GBS:
                        </td>
                        <td align="left" >
                                <asp:RadioButtonList ID = "HistoryGBSrbl" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem>Yes</asp:ListItem>
                                <asp:listItem>No</asp:listItem>
                            </asp:RadioButtonList>

                        </td>
                        <td align="right">
                            Injection Side:
                        </td>
                        <td align="left" >
                                <asp:RadioButtonList ID = "InjectionSiderbl" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem>Left</asp:ListItem>
                                <asp:listItem>Right</asp:listItem>
                            </asp:RadioButtonList>

                        </td>

                    </tr>
                    <tr>
                  
                      <td align="right" colspan="2" >
                            Have You ever had a negative reaction to Flu Shot:
                        </td>
                        <td align="left" class="styletext" colspan="2">
                                <asp:RadioButtonList ID = "AllergicFluShotrbl" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem>Yes</asp:ListItem>
                                <asp:listItem>No</asp:listItem>
                            </asp:RadioButtonList>

                        </td>

                    </tr>
                    <tr>
                                        
                        <td align="right" >
                            Location Site:
                        </td>

                        <td align="left">
                            <asp:DropDownList ID="LocationSiteIDddl" runat="server" AutoPostBack="True">
            
                            </asp:DropDownList>
                            
                        </td>
                        <td>
                            
                        </td>

                        <td>
                            
                        </td>                                        
                    </tr>
                    <tr>
                        <td align="right">
                            Injection Out Come:
                        </td>
                        <td align="left"  colspan="3">
                                <asp:RadioButtonList ID = "VaccineOutIDrbl" runat="server" RepeatDirection="Horizontal">
                                                  
                                </asp:RadioButtonList>

                        </td>

                    </tr>
                    <tr>
                        <td colspan="4" align="left" >
                           Comments:
                        </td>
                    </tr>
                    <tr>
                        <td colspan = "4" align="left" >
                            <asp:TextBox ID="CommentsTextBox" runat ="server" TextMode = "MultiLine" Width="100%"></asp:TextBox>
                        </td>
                    </tr>

                    <tr>
                        <td align="right" >
                            Vaccine Date:
                        </td>
                        <td align="left" >
                            <asp:TextBox ID ="VaccineDateTextBox" runat="server" CssClass="calenderClass" ></asp:TextBox>
                        </td>
       
                        <td align="right" >
                        Entery Date:
                        </td>
                        <td align="left" >
                            <asp:TextBox ID="EnteryDateTextBox" runat="server" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>  
                    <tr>
                        <td colspan="4">

                            <asp:Label ID= "nt_loginlabel" runat="server" Visible="False" ></asp:Label>

                            <asp:Label ID= "VaccineNumLabel" runat="server" Visible="False" ></asp:Label>
                            <asp:Label ID="VaccineLotNumlabel" runat="server" Visible="False"></asp:Label>
                            <asp:Label ID="client_numLabel"  runat="server" visible="false"></asp:Label>

                            <asp:Label ID="RoleNumLabel" runat="server" Visible="false"></asp:Label>
                            <asp:Label ID="UserNumAdministeredLabel" runat="server" Visible="false"></asp:Label>
                            <asp:Label ID="department_nameLabel" runat="server" visible="false"></asp:Label>
                        <asp:Label ID ="department_cdLabel" runat="server" Visible="False"></asp:Label>
                        <asp:Label ID = "emp_type_cdLabel" runat="server" Visible="False"></asp:Label>
                        <asp:Label ID="userDepartmentCD"  runat="server" Visible="False"></asp:Label>
                        <asp:Label ID="LocationSiteIDlabel" runat="server" Visible="False"></asp:Label>
                            <asp:Label ID = "facility_cdLabel" runat="server" Visible ="False"></asp:Label>
                            <asp:Label ID = "entity_cdLabel" runat="server" Visible ="False"></asp:Label>
                            <asp:Label ID = "entity_nameLabel" runat="server" Visible ="False"></asp:Label>


                        </td>
                     </tr>
                                 
                </table>
            </asp:Panel>
       </td>
    </tr>
 </table>

   <%-- =================== VaccineNum return ============================================================================--%>
	<asp:Panel ID="pnlReturn" runat="server" Height="450px" Width="575px"  BackColor="#FFFFCC" style="display:none" >
        <table  id="tblSearchFirst" runat="server" style="border: medium groove #000000; height:100%"  align="center" width="100%" >

            <tr>

                <td  colspan="3" align="center" style="font-size: x-large; font-weight: bold; color: #FF0000">
                        Entry for this Employee has been completed! 
                </td>

            </tr>
            <tr>
                <td style="width: 3px">
                </td>

                <td style="font-weight: bold; font-size: larger; text-align: center;" align="center">
                   You can retirve this record by using this ID.
                   <asp:Label ID="ReturnVaccineLbl" runat="server" ></asp:Label>
                </td>
                <td style="width: 3px">
                </td>

            </tr>
            <tr>
                <td style="width: 3px">
                    <asp:Label ID="lblSearched" runat="server" Visible="false"></asp:Label>
                </td>
                <td align="center" >
                    <asp:Button ID="btnCompleted" runat="server" BackColor="#00C000" Font-Bold="True" Font-Names="Arial"
                        Font-Size="Small" ForeColor="Black" Height="35px" Text="Completed" Width="140px" />

                </td>
                <td style="width: 3px">
                </td>
            </tr>
        </table>
        <br />
        </asp:Panel> 

        <ajaxToolkit:ModalPopupExtender ID="VaccineReturnPopup" runat="server" 
             TargetControlID="HdBtnSearch" PopupControlID="pnlReturn" BackgroundCssClass="ModalBackground" >
        </ajaxToolkit:ModalPopupExtender>

        <%--CancelControlID="btnNoSearch"  BackgroundCssClass="ModalBackground" --%>

       <asp:Button ID="HdBtnSearch" runat="server" style="display:none" />


</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>

