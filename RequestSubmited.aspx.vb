﻿Imports System.IO
Imports System.Data '
Imports System.Data.SqlClient
Imports App_Code
Imports AjaxControlToolkit
Imports C1.Web.UI.Controls.C1Window

Partial Class RequestSubmited
    Inherits MyBasePage '   Inherits System.Web.UI.Page


    Dim sActivitytype As String = ""
    Dim cblBinder As New CheckBoxListBinder
    Dim FormViewRequest As New FormSQL()
    Dim AccountRequestNum As Integer
    Dim AccountRequestType As String


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        FormViewRequest = New FormSQL(Session("EmployeeConn"), "SelAccountRequestV20", "ins_account_request", "", Page)
        AccountRequestNum = Request.QueryString("RequestNum")

        If Not IsPostBack Then

            FormViewRequest.AddSelectParm("@account_request_num", AccountRequestNum, SqlDbType.Int, 4)
            FormViewRequest.FillForm()
            fillOpenProviderItemsTable()
            lblRequestNum.Text = AccountRequestNum


        End If

    End Sub


    Protected Sub fillOpenProviderItemsTable()
        Dim thisData As New CommandsSqlAndOleDb("SelRequestItems", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.NVarChar)

        Dim iRowCount = 1

        Dim dt As DataTable
        dt = thisData.GetSqlDataTable

        Dim rwHeader As New TableRow

        Dim clAccountHeader As New TableCell
      



        clAccountHeader.Text = "Accounts Requested"
       



        rwHeader.Cells.Add(clAccountHeader)
      


        rwHeader.Font.Bold = True
        rwHeader.BackColor = Color.FromName("#006666")
        rwHeader.ForeColor = Color.FromName("White")
        rwHeader.Height = Unit.Pixel(36)

        tblOpenProviderRequestItems.Rows.Add(rwHeader)

        For Each drRow As DataRow In dt.Rows


            Dim rwData As New TableRow

            Dim clAccountData As New TableCell
           




            clAccountData.Text = IIf(IsDBNull(drRow("ApplicationDesc")), "", drRow("ApplicationDesc"))
           
            rwData.Cells.Add(clAccountData)
          

            If iRowCount > 0 Then

                rwData.BackColor = Color.Bisque

            Else

                rwData.BackColor = Color.LightGray

            End If

            tblOpenProviderRequestItems.Rows.Add(rwData)

            iRowCount = iRowCount * -1

        Next
        tblOpenProviderRequestItems.CellPadding = 10
        tblOpenProviderRequestItems.Width = New Unit("100%")


    End Sub









End Class
