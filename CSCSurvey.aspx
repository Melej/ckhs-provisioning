﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="false" CodeFile="CSCSurvey.aspx.vb" Inherits="CSCSurvey" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:UpdatePanel runat="server" UpdateMode ="Conditional" ID="uplTicket">
    <ContentTemplate>
     <table width="100%">
        <tr>
         <td>
               <table  border="1" cellpadding="3" width="100%">
                           <tr>
                                <td  colspan= "4"  class ="tableRowHeader">
                                          <asp:Label ID="requestlbl" runat="server" Text =" Ticket/RFS"></asp:Label>
                                            <asp:Label ID="RequestNumLabel" runat="server"></asp:Label>

                               </td>
                           </tr>
                           <tr>
                              <td  align="center" colspan = "4">
                                <table class="style3" > 
                                  <tr>
                                    <td>
                           
                                        <asp:Button runat="server" ID="btnSubmit" Text="Submit Survey" 
                                        CssClass="btnhov" BackColor="#006666"  Width="200px"  />
                                        
                                    </td>

                                    <td>
                           
                                        <asp:Button runat="server" ID="btnReturn" Text="Return to Request" 
                                        CssClass="btnhov" BackColor="#006666"  Width="200px"  />
                                        
                                    </td>
                                   </tr>
                                </table>
                              </td>
                           </tr>
                         <tr align="center">
                            <td colspan="4" align="center">
                                <asp:Label ID = "lblValidation" runat = "server" ForeColor="Red" 
                                    Font-Bold="True"/>
                            </td>
            
                        </tr>

                           <tr>
                            <td colspan = "4">
                                <table class="style3" > 
                                <tr>
                                    <td align="right" >
                                        First Name:
                                    </td>
                                    <td align="left"  >
                                        <asp:TextBox width="280px" ID = "first_nameTextBox" runat="server" ></asp:TextBox>
                                    </td>
                                    <td align="right" >
                                        Last Name:
                                    </td>

                                    <td align="left" >
                                        <asp:TextBox ID = "last_nameTextBox" width="280px" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                               <tr>
                                     <td align="right">
                                        Date Entered:
                                    </td>

                                    <td align="left">
                                        <asp:Label ID="entrydatelabel" runat="server" Font-Bold="True"></asp:Label>
                                    </td>

                                    </td>
       
                                    <td align="right" >
                                        <asp:Label ID="lbldept" runat="server" Text = "Department Name:" />
                                    </td>
                                    <td align="left"  >
                                    <asp:DropDownList ID ="departmentcdddl" runat="server" Width="250px" 
                                            Enabled="False">
            
                                    </asp:DropDownList>
                                    </td>

                                </tr>
                                <tr>

                                   <td align="right">
                                            <asp:Label ID="currGroupLb" runat="server" Text="Current Group:" ></asp:Label>
                                   </td>
                                   <td align="left">
                                         <asp:Label runat="server" ID="currentgroupnameLabel" Font-Bold="True" ></asp:Label>
                                         
                                   </td>

                                    <td align="right" >
                                       
                                        </td>
                                    <td align="left" >
                                        
                                        </td>

                                </tr>
                                <tr>
                                    <td  align="center" colspan="4">
                                        <table ID="Table4" runat="server" width="95%">
                                            <tr id="Tr1" runat="server">
                                                <td id="Td1" runat="server">
                                                <asp:Label ID="Catlbl" runat="server" Text = "Category"></asp:Label>
                                                </td>    
                                                
                                                <td id="Td2" runat="server">
                                                <asp:DropDownList ID="categorycdddl" runat="server" Enabled="False" >
                                                </asp:DropDownList>
                                                </td>    

                                                <td id="Td3" runat="server">
                                                    <asp:Label ID="Typelbl" runat="server" Text = "Type"></asp:Label>
                                                </td>
  
                                                <td id="Td4" runat="server">
                                                <asp:DropDownList ID="typecdddl" runat="server" Enabled="False" >
                                                </asp:DropDownList>
                                                </td> 
                                               
                                                <td id="Td5" runat="server">
                                                    <asp:Label ID="Itemlbl" runat="server" Text = "Item"></asp:Label>
                                                </td>    
                                                 
                                                <td id="Td6" runat="server">
                                                <asp:DropDownList ID="itemcdddl" runat="server" Enabled="False" >
                                                </asp:DropDownList>
                                                </td>  
                                                                                              
                                            </tr>                                        
                                        </table>
                                    </td>

                                </tr>

                           
                             <tr>
                                <td align="right" >
                                Location:
                                </td>
                                <td align="left" >
         
                                    <asp:DropDownList ID ="facilitycdddl" runat="server" Enabled="False" >
              
                                </asp:DropDownList>
          
                                </td>
        
                                <td align="right" >
                                Building:
                                </td>
                                <td align="left" >

                                    <asp:DropDownList ID ="buildingcdddl" runat="server" Enabled="False" >
            
                                    </asp:DropDownList>
            
                                </td>
                        </tr>
                        <tr>
                            <td align="right" >
                                Floor:
                            </td>

                            <td align="left">
                                <asp:DropDownList ID="Floorddl" runat="server" Enabled="False" >
            
                                </asp:DropDownList>
                            
                            </td>
                            <td align="right" >
                                 Office:
                            </td>
                               
                            <td align="left" > 
                                <asp:TextBox ID="OfficeTextbox" runat="server"></asp:TextBox>
                            </td>


                        </tr>
                      </table>
                    </td>
                   </tr>
        <tr >
            <td align="center" colspan="2">
            
                <asp:RadioButtonList ID="RequestTyperbl" runat="server"  Enabled="False" 
                    RepeatDirection="Horizontal">
                        <asp:ListItem Value="ticket" Text="Ticket"></asp:ListItem>
                        <asp:ListItem Value="service" Text="Req. For Service"></asp:ListItem>
                                                           
                </asp:RadioButtonList>          
            
            </td>
            <td align="center" colspan="2">
                Current Status:
                    <asp:Label ID="currstatusDescLabel" runat="server"  Font-Bold="True"></asp:Label>
                   
            </td>
        </tr> 
        <tr align="center" >
            <td align="center" colspan="4"  >
               <div id="maintbl" runat="server">
                
                
                <table width="90%"  style="border-style: groove">

                    <tr align="center">
                        <td >
                            Was the Customer Service Center Analyst courteous and professional?
                        </td>
                    </tr>
                    <tr align="center">
                        <td>
                             
                            <asp:RadioButtonList ID="courteousrbl" runat="server"  RepeatDirection="Horizontal">
                                <asp:ListItem Value="1" Text="Strongly Agree"></asp:ListItem>
                                <asp:ListItem Value="2" Text="Agree"></asp:ListItem>
                                <asp:ListItem Value="3" Text="N/A"></asp:ListItem>
                                <asp:ListItem Value="4" Text="Disagree"></asp:ListItem>
                                <asp:ListItem Value="5" Text="Strongly Disagree"></asp:ListItem>

                                                           
                            </asp:RadioButtonList>  
                        </td>
                    </tr>
                    <tr align="center">
                        <td>
                            ----------------------------------------------------------------------------------------
                        </td>
                    </tr>
                 
                    <tr align="center">
                        <td>
                            Was the Customer Service Center Analyst knowledgeable?
                        </td>
                    </tr>
                    <tr align="center">
                        <td>
                             
                            <asp:RadioButtonList ID="knowledgeablerbl" runat="server"  RepeatDirection="Horizontal">
                                <asp:ListItem Value="1" Text="Strongly Agree"></asp:ListItem>
                                <asp:ListItem Value="2" Text="Agree"></asp:ListItem>
                                <asp:ListItem Value="3" Text="N/A"></asp:ListItem>
                                <asp:ListItem Value="4" Text="Disagree"></asp:ListItem>
                                <asp:ListItem Value="5" Text="Strongly Disagree"></asp:ListItem>

                                                           
                            </asp:RadioButtonList>  
                        </td>
                    </tr>
                    <tr align="center">
                        <td>
                            ----------------------------------------------------------------------------------------
                        </td>
                    </tr>

                    <tr align="center">
                        <td>
                            Was the event resolved in accordance with your expectations?
                        </td>
                    </tr>
                    <tr align="center">
                        <td>
                             
                            <asp:RadioButtonList ID="expectationsrbl" runat="server"  RepeatDirection="Horizontal">
                                <asp:ListItem Value="1" Text="Strongly Agree"></asp:ListItem>
                                <asp:ListItem Value="2" Text="Agree"></asp:ListItem>
                                <asp:ListItem Value="3" Text="N/A"></asp:ListItem>
                                <asp:ListItem Value="4" Text="Disagree"></asp:ListItem>
                                <asp:ListItem Value="5" Text="Strongly Disagree"></asp:ListItem>

                                                           
                            </asp:RadioButtonList>  
                        </td>
                    </tr>
                    <tr align="center">
                        <td>
                            ----------------------------------------------------------------------------------------
                        </td>
                    </tr>              

                    <tr align="center">
                        <td>
                           Was the event resolved in a timely manner?
                        </td>
                    </tr>
                    <tr align="center">
                        <td>
                             
                            <asp:RadioButtonList ID="timelyrbl" runat="server"  RepeatDirection="Horizontal">
                                <asp:ListItem Value="1" Text="Strongly Agree"></asp:ListItem>
                                <asp:ListItem Value="2" Text="Agree"></asp:ListItem>
                                <asp:ListItem Value="3" Text="N/A"></asp:ListItem>
                                <asp:ListItem Value="4" Text="Disagree"></asp:ListItem>
                                <asp:ListItem Value="5" Text="Strongly Disagree"></asp:ListItem>

                                                           
                            </asp:RadioButtonList>  
                        </td>
                    </tr>
                    <tr align="center">
                        <td>
                            ----------------------------------------------------------------------------------------
                        </td>
                    </tr>

                    <tr>
                        <td>
                            Comments:
                        </td>
                    </tr>
                    <tr align="center">
                        <td>
                            <asp:TextBox ID="commenttextbox" runat="server" TextMode="MultiLine" Width="90%" Height="125px">
                                </asp:TextBox>
                        </td>
                    </tr>
                </table>
            
               </div>
            </td>
        
        </tr>    
        <tr>
            <td colspan = "4">
                <asp:DropDownList ID ="entitycdDDL" runat="server"  Visible="false">
            
                </asp:DropDownList>
                <asp:Label ID="nt_login" runat="server" Visible="False"></asp:Label>
                <asp:Label ID="clientnumLabel" runat="server" Visible="False"></asp:Label>
                <asp:Label ID="entryclientnumLabel" runat="server" Visible="False"></asp:Label>

                <asp:Label ID="WhoclientNumLabel" runat="server" Visible="False"></asp:Label>
                        
                <asp:Label ID="SecurityLevelLabel" runat="server" Visible="False"></asp:Label>
                        
                <asp:Label ID ="department_cdLabel" runat="server" Visible="False"></asp:Label>
                <asp:Label ID ="TitleLabel" runat="server" Visible="False"></asp:Label>
                                     
                <asp:Label ID = "entity_cdLabel" runat="server" Visible ="False"></asp:Label>
                <asp:Label ID = "building_cdLabel" runat="server" Visible ="False"></asp:Label>

                <asp:Label ID= "building_nameLabel" runat="server" Visible="False" ></asp:Label>
                <asp:Label ID="HDemployeetype" runat="server" Visible="False"></asp:Label> 

                <asp:Label ID="suffixlabel" runat="server" Visible="False"></asp:Label>
                <asp:Label ID= "AccountRequestTypeLabel" runat="server" Visible="False"></asp:Label>
                <asp:Label ID="typecdLabel" runat="server" Visible="False"></asp:Label>
                <asp:Label ID="categorycdLabel"  runat="server" Visible="False"></asp:Label>
                <asp:Label ID="itemcdLabel" runat="server" visible="False" />
                                        
                <asp:Label ID="facility_cdLabel" runat="server" visible="False" />
                <asp:Label ID="floorLabel" runat="server" visible="False" />
                            
                <asp:Label ID = "currenttechnumLabel" runat="server" Visible ="False"></asp:Label>
                <asp:Label ID = "currentgroupnumLabel" runat="server" Visible ="False"></asp:Label>

                <asp:Label ID="RequestTypeLabel" runat="server" Visible="False"></asp:Label>
                <asp:Label ID="currstatuscdLabel" runat="server" Visible="False"></asp:Label>

                <asp:Label ID="CloseDateLabel" runat="server" Visible="False"></asp:Label>
                                        
                <asp:Label ID="numofterminalsLabel"  runat="server" Visible="False"></asp:Label>
                <asp:label id="entrytechnumLabel" runat="server" Visible="False"></asp:label>
                <asp:Label ID="hdPriorityLb" runat="server" Visible="false"></asp:Label>

                <asp:Label ID="hdexists" runat="server" Visible="false"></asp:Label>
                </td>
            </tr>
     </table>
         </td>
        </tr>
      </table>
    </ContentTemplate>
  </asp:UpdatePanel>
</asp:Content>

