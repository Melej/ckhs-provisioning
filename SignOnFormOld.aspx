﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="true" CodeFile="SignOnFormOld.aspx.vb" Inherits="SignOnForm" %>



<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript" src="Scripts/FieldValidator.js"></script>

   

<script type="text/javascript">

   function pageLoad(sender, args) {

      fieldValidator('textInputRequired', 'btnSubmit', 'lblValidation');

  }



</script>

      
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

   


    <asp:UpdatePanel ID="uplSignOnForm" runat ="server" >

<Triggers>

    <asp:AsyncPostBackTrigger ControlID="entity_cdDDL" EventName = "SelectedIndexChanged" />
    <asp:AsyncPostBackTrigger ControlID="facility_cdddl" EventName = "SelectedIndexChanged" />
    <asp:AsyncPostBackTrigger ControlID="cblApplications" EventName = "SelectedIndexChanged" />
    <asp:AsyncPostBackTrigger ControlID="emp_type_cdrbl" EventName = "SelectedIndexChanged" />
    <asp:AsyncPostBackTrigger ControlID="btnClearAccounts" EventName="Click" />
<asp:AsyncPostBackTrigger ControlID = "btnSubmit" EventName ="Click" />


</Triggers>

<ContentTemplate>






<asp:TextBox runat="server" ID="txtTest" CssClass="tooltip" Visible ="false"></asp:TextBox>
<table  align="left">





<tr>
            <td colspan = "4" align ="center">
                <asp:Label ID = "lblValidation" runat = "server" ForeColor="Blue" Font-Size="20px"/>
            </td>
            
        </tr>
<tr>
<td>


        <table border="3">
        <tr>
                 <td colspan = "4" class="tableRowHeader">
                     Onboarding Form
                 </td>
         </tr>
                <tr>
          
            <td colspan="4" class="tableRowSubHeader"  align="left">
           Form Submission
            </td>
            
        </tr>
        <tr>
        <td colspan="4">
           User submitting:         
           <asp:Label ID = "userSubmittingLabel" runat="server" Font-Bold="true">Test</asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="4">
            Requestor:   <asp:Label ID = "SubmittingOnBehalfOfNameLabel" runat="server" Font-Bold="true"></asp:Label>
             <asp:Label ID = "requestor_client_numLabel" runat="server" visible="false"></asp:Label>
            
              <span style="float:right"> <asp:Button ID="btnSelectOnBehalfOf" runat="server" Text="Select Requestor" />       </span>    
            </td>
        </tr>
       
        
                <tr>
          
            <td colspan="4" class="tableRowSubHeader"  align="left">
            CKHS Affiliation
            </td>
        </tr>
        <tr>
            <td colspan="4">
             <asp:RadioButtonList ID ="emp_type_cdrbl" runat = "server" RepeatDirection="Horizontal" AutoPostBack="true"></asp:RadioButtonList>
             <asp:Panel runat="server" ID ="pnlOtherAffilliation" Visible="false">
              
                Other Affiliation Description: <asp:TextBox runat="server" ID = "OtherAffDescTextBox"></asp:TextBox>
             </asp:Panel>
             <asp:Panel runat="server" ID ="pnlAlliedHealth" Visible="false">
            
            Allied Health Degree:

                <asp:DropDownList runat="server" ID = "OtherAffDescddl">
                  
                </asp:DropDownList> 
                          
             </asp:Panel>
            </td>
        </tr>
         <tr>
            <td colspan="4" class="tableRowSubHeader" align="left">Provider Information</td>
         </tr>
         <tr>
          
                      <td>
                                     Provider:
                                    </td>
                                    <td colspan="3">
                                    <asp:RadioButtonList ID = "providerrbl" runat="server" RepeatDirection="Horizontal">
                                        <asp:ListItem>Yes</asp:ListItem>
                                         <asp:ListItem Selected= "True">No</asp:ListItem>            
                                        </asp:RadioButtonList>
                                    </td>
                                   

         </tr>
         <tr>
                         <td>
                        CKHN-HAN:
                        </td>
                        <td colspan="3">
                        <asp:RadioButtonList ID = "hanrbl" runat="server" RepeatDirection="Horizontal">
                             <asp:ListItem>Yes</asp:ListItem>
                              <asp:ListItem Selected="True">No</asp:ListItem>            
                            </asp:RadioButtonList>
                        </td>
                    
         </tr>
           <tr>
                    <td>
                    NPI
                    </td>
                    <td >
                    <asp:TextBox ID = "npitextbox" runat = "server"></asp:TextBox>
                    </td>
                    <td>
                    License No.
                    </td>
                    <td>
                    <asp:TextBox ID = "LicensesNo" runat="server"></asp:TextBox>
                    </td>
                    

               
                 </tr>
                 <tr>
                    <td>Taxonomy</td>
                    <td colspan ="3">
                    <asp:TextBox ID = "taxonomytextbox" runat="server"></asp:TextBox>
                   
        
                 </tr>
        <tr>
                <td>
                Group Name

                </td>
                <td>
                    <asp:TextBox ID="group_nameTextBox" runat="server"></asp:TextBox>
                </td>
                <td>
                 Group Number

                </td>
                <td>
                    <asp:TextBox ID="group_numTextBox" runat="server"></asp:TextBox>
                </td>
        </tr>
  
  <tr>
    <td colspan="4" class="tableRowSubHeader"  align="left">
    Demographics
    </td>
  </tr>

        <tr>
            <td>
            First Name:           
            
            </td>
             <td>
            <asp:TextBox ID = "first_nameTextBox" runat="server" CssClass="textInput"></asp:TextBox>
              M:
                 <asp:TextBox ID = "middle_initialTextBox" runat="server" Width="15px"></asp:TextBox>
            </td>
        
          <td>
            Last Name:
            </td>
             <td>
            <asp:TextBox ID = "last_nameTextBox" runat="server"></asp:TextBox>
            Suffix: 
            <asp:dropdownlist id="suffixddl"
				tabIndex="3" runat="server" Height="20px" Width="52px">
				<asp:ListItem Value="Select" Selected="True">Select</asp:ListItem>
				<asp:ListItem Value="Sr.">Sr.</asp:ListItem>
				<asp:ListItem Value="Jr.">Jr.</asp:ListItem>
				<asp:ListItem Value="II">II</asp:ListItem>
				<asp:ListItem Value="III">III</asp:ListItem>
				<asp:ListItem Value="IV">IV</asp:ListItem>
                </asp:dropdownlist>


            </td>
        </tr>

     
        <tr>
           <td>Title:</td>
            <td>
               <asp:dropdownlist ID = "Titleddl" runat="server"></asp:dropdownlist>
            </td>
            <td >
            Specialty:
            </td>
            <td>
               <asp:dropdownlist ID = "Specialtyddl" runat="server"></asp:dropdownlist>
            </td>
        </tr>
        <tr>
            <td>
                Position Description:
            </td>
            <td>
                <asp:TextBox ID="user_position_descTextBox" runat="server" Width = "200px"></asp:TextBox>
            </td>
            <td>
             Employee Number:
            </td>
             <td>
                <asp:TextBox ID="AuthorizationEmpNumTextBox" runat="server" Width = "200px"></asp:TextBox>
            </td>
        </tr>
      

         <tr>
            <td>
            Entity:
            </td>
            <td>
            <asp:DropDownList ID ="entity_cdDDL" runat="server" AutoPostBack ="true">
            
            </asp:DropDownList>
            </td>
       
            <td>
            Department Name:
            </td>
            <td>
            <asp:DropDownList ID ="department_cdddl" runat="server">
            
            </asp:DropDownList>
            </td>
        </tr>
         <tr>
            <td>
            Practice Name:
            </td>       
            <td colspan="3">
            <asp:DropDownList ID ="practice_numddl" runat="server">
            
            </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                Non-CKHN Location
            </td>
            <td colspan="3">
                  <asp:TextBox ID = "NonHanLocationTextBox" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>Address 1:</td>
            <td>  <asp:TextBox ID = "address_1textbox" runat="server"></asp:TextBox></td>
            <td>City:</td>
            <td>  <asp:TextBox ID = "citytextbox" runat="server"></asp:TextBox></td>
        </tr>
         <tr>
            <td>Address 2:</td>
            <td>  <asp:TextBox ID = "address_2textbox" runat="server"></asp:TextBox></td>
            <td>State:<asp:TextBox ID = "statetextbox" runat="server" Width="25px" MaxLength="2"></asp:TextBox></td>
            <td>Zip:  <asp:TextBox ID = "ziptextbox" runat="server" Width="50px"></asp:TextBox></td>
        </tr>
       
            <tr>
            <td>
            Phone Number:
            </td>
            <td>
            <asp:TextBox ID = "phoneTextBox" runat="server"></asp:TextBox>
            </td>
       
            <td>
            Pager Number:
            </td>
            <td>
           <asp:TextBox ID = "pagerTextBox" runat="server"></asp:TextBox>
            </td>
        </tr>
   
 
        <tr>
           <td>Fax:</td>
            <td>
            
                <asp:TextBox ID = "faxtextbox" runat="server"></asp:TextBox></td>
           
           
             <td>
            Email:
            </td>
            <td>
           <asp:TextBox ID = "emailTextBox" runat="server"></asp:TextBox>
            </td>
        </tr>
      
         <tr>
            <td>
            Location
            </td>
            <td>
         
              <asp:DropDownList ID ="facility_cdddl" runat="server" AutoPostBack="true">
              
            </asp:DropDownList>
          
            </td>
        
            <td>
            Building
            </td>
            <td>
           
             <asp:DropDownList ID ="building_cdddl" runat="server">
            
            </asp:DropDownList>
          
            </td>
        </tr>
            <tr>
                                                <td>Floor:</td>
                                                <td> <asp:TextBox ID = "floorTextBox" runat="server"/></td>
                                                <td>Office:</td>
                                                <td> <asp:TextBox ID = "officeTextBox" runat="server"></asp:TextBox></td>
                                    </tr>
       
       

  
<tr>
          
            <td>Vendor Name</td>
            <td colspan="3"><asp:TextBox ID ="VendorNameTextBox" runat="server" ></asp:TextBox></td>
        </tr>
<tr>
            <td>
            Start Date:
            </td>
            <td>
               <asp:TextBox ID ="start_dateTextBox" runat="server" CssClass="calenderClass"></asp:TextBox>
            </td>
       
            <td>
            End Date:
            </td>
            <td>
                <asp:TextBox ID="end_dateTextBox" runat="server" CssClass="calenderClass"></asp:TextBox>
            </td>
          </tr>
        
 
<tr>
          
            <td>
                Share Drive
            </td>
            <td >
                 <asp:TextBox ID="share_driveTextBox" runat="server"></asp:TextBox>
            </td>
  
            
            <td>Model After:</td>
            <td> <asp:TextBox ID="ModelAfterTextBox" runat="server" Width="200px"></asp:TextBox></td>
        </tr>
<tr>
            <th colspan = "4"  class="tableRowSubHeader" align="left">
            Accounts
            </th>
        </tr> 
<tr>
         <td >
          Entities
            </td>
            <td colspan="3">
                <asp:CheckBoxList ID = "cblEntities" runat="server" RepeatDirection = "Horizontal">
                </asp:CheckBoxList>
            </td>
            
        </tr>
<tr>
            
            <td>
            Hospitals
            </td>
            <td colspan="3">
                <asp:CheckBoxList ID = "cblFacilities" runat="server" RepeatDirection = "Horizontal">
                </asp:CheckBoxList>
            </td>
      
        
        </tr>
<tr>
            <td>
                Role Template
            </td>
            <td >
            <asp:DropDownList ID = "Rolesddl" runat="server" AutoPostBack = "true"></asp:DropDownList>
            </td>
            <td colspan="2">
                <asp:Button ID = "btnClearAccounts" runat="server" Text ="Clear Selected Accounts" />

            </td>

        </tr>

<tr>
            <td colspan="4">            
            
            <asp:CheckBoxList ID = "cblApplications" runat = "server" RepeatColumns="3">
                      
            
            </asp:CheckBoxList>
            

            <asp:Panel ID ="pnlTCl" runat="server" Visible = "false">
           <span style="font-weight:bold"> TCL for eCare:</span> <asp:TextBox ID="TCLTextBox" runat="server"/>
            </asp:Panel>



            </td>
        
        
        
        </tr>
<tr>
            <td colspan = "4" align = "center">
            Additional Comments:            
            </td>
        
        </tr>
<tr>
            <td colspan = "4">
                <asp:TextBox ID="request_descTextBox" runat ="server" TextMode = "MultiLine" Width="100%"></asp:TextBox>
            
            </td>
        
        </tr>
        
    
<tr>
            <td colspan = "4" align="center">
                <asp:Button ID = "btnSubmit" runat = "server" Text ="Submit" Width="75" BackColor="#336666" ForeColor="White" />
                <asp:Button ID = "btnReturn" runat = "server" Text ="Return" Width="75" BackColor="#336666" ForeColor="White" />
            </td>
            
        </tr>

        </table>

</td>

</tr>

</table>

   
                

    <asp:Panel ID="pnlOnBehalfOf" runat="server" style="display:none; background-color:#efefef;  font-size:large;  width:600px;  border: 2px solid #FFFFCC;">

    <asp:UpdatePanel ID = "uplOnBehalfOf" runat="server" UpdateMode="Conditional" >
   
    
   <ContentTemplate>

                                            
                                                            <table  width="100%" border="1px" >
                                                                <tr>
                                                                    <th class="tableRowHeader" colspan = "4">
                                                                  Search for Requestor
                                                                    </th>
                                                                </tr>
                                                             <tr>
                                                                <td>
                                                                   First Name <asp:TextBox ID = "FirstNameTextBox" runat = "server"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    Last Name <asp:TextBox ID = "LastNameTextBox" runat = "server"></asp:TextBox>
                                                                      <asp:Button ID ="btnSearch" runat = "server" Text ="Search"  />
                                                                </td>
       
  
                                                                
       
                                                            </tr>
                                                             
                                                            <tr>
                                                                <td colspan = "4">
                                                                <div class="scrollContentContainer" >
                                                                
                                                                 <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="200" AssociatedUpdatePanelID="uplOnBehalfOf"  >

                                                                    <ProgressTemplate>

                                                                    <div id="IMGDIV" align="center" valign="middle" runat="server" style="visibility:visible;vertical-align:middle;">
                                                                     Loading Data ...
                   
                                                                   
                   
                                                                    </div>

                                                                    </ProgressTemplate>

                                                        </asp:UpdateProgress>
                                                        

                                                                
                                                                     <asp:GridView ID="gvSearch" runat="server" AllowSorting="True" 
                                                                                                AutoGenerateColumns="False" BackColor="White" BorderColor="#336666"
                                                                                                BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                                                                                                EmptyDataText="No Items Found" EnableTheming="False" DataKeyNames="client_num, login_name"
                                                                                                Font-Size="Medium" GridLines="Horizontal" Width="100%">
                                   
                                                                                                <FooterStyle BackColor="White" ForeColor="#333333" />
                                                                                                <HeaderStyle BackColor="#336666" Font-Bold="False" ForeColor="White" Font-Size="Small" HorizontalAlign="Left"    />                                    
                                                                                                <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Left" Font-Size="Medium"/>
                                                                                                <RowStyle BackColor="White" ForeColor="#333333" />
                                                                                                <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                                                                                <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                                                                                <SortedAscendingHeaderStyle BackColor="#487575" />
                                                                                                <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                                                                                <SortedDescendingHeaderStyle BackColor="#275353" />

                                                                                                <Columns>
                                                                                                    <asp:CommandField ButtonType="Button" SelectText="Select" ShowSelectButton= "true" />

                                         
                                                                                                      <asp:BoundField  DataField="fullname"  HeaderText="Full Name" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
                                                                                      <%--                <asp:BoundField  DataField="login_name"  HeaderText="Login"></asp:BoundField>--%>
                                                                                                      <asp:BoundField  DataField="phone"  HeaderText="Phone" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
                                                                                                      <asp:BoundField  DataField="department_name"  HeaderText="Department" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
				                          



                                  
                                                                                              </Columns>

                                                                                            </asp:GridView>
                                                                
                                                                        </div>
                                                                
                                                                </td>
                                                            
                                                            
                                                            </tr>
                                                          
                                                           
                                                               
                                                                <tr>
                                                                    <td colspan = "4" align = "center" >
                                                                    <asp:Button ID = "btnCloseOnBelafOf" runat = "server" Text="Close" />
                                                                    
                                                                  
                                                                   
                                                                    </td>
                                                                </tr>
                                
                                                            </table>

                                         </ContentTemplate>               
                              </asp:UpdatePanel>
                                 
                                </asp:Panel>

                                <ajaxToolkit:UpdatePanelAnimationExtender ID="UpdatePanelAnimationExtender1" runat="server" TargetControlID="uplOnBehalfOf">
                                                                      <Animations>
                                                                          <OnUpdating>
                                                                           <StyleAction animationtarget="progress" Attribute="display" value="block" />
                                                                        </OnUpdating>
                                                                        <OnUpdated>
                                                                             <StyleAction animationtarget="progress" Attribute="display" value="none" />

                                                                        </OnUpdated>
                                                                    </Animations>
                                                        </ajaxToolkit:UpdatePanelAnimationExtender>
                                 
            
            
             <ajaxToolkit:ModalPopupExtender ID="mpeOnBehalfOf" runat="server"   
                                DynamicServicePath="" Enabled="True" TargetControlID="btnSelectOnBehalfOf"   
                                PopupControlID="pnlOnBehalfOf" BackgroundCssClass="ModalBackground"   
                                DropShadow="true" CancelControlID="btnCloseOnBelafOf">  
             </ajaxToolkit:ModalPopupExtender>  




</ContentTemplate>


        </asp:UpdatePanel>


</asp:Content>

