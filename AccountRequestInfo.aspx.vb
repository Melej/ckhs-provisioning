﻿Imports System.IO
Imports System.Data '
Imports System.Data.SqlClient
Imports App_Code
Imports AjaxControlToolkit
Imports C1.Web.UI.Controls.C1Window

Partial Class AccountRequestInfo
    Inherits MyBasePage '   Inherits System.Web.UI.Page


    Dim sActivitytype As String = ""

    Dim FormSignOn As New FormSQL()
    Dim iRequestNum As Integer
    Dim sValue As String
    Private dtAccounts As New DataTable
    Private AddAccountRequestNum As Integer = 0
    Private DelAccountRequestNum As Integer = 0
    Private dtPendingRequests As DataTable
    Private dtRemoveAccounts As New DataTable
    Private dtAddAccounts As New DataTable
    'Dim BusLog As AccountBusinessLogic
    Dim thisdata As CommandsSqlAndOleDb
    Dim UserInfo As UserSecurity
    Dim AccountRequest As AccountRequest


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'tpAccounts.Visible =
        'tpPendingRequests.Visible = False

        sValue = Request.QueryString("DisplayField")
        iRequestNum = Request.QueryString("RequestNum")

        thisdata = New CommandsSqlAndOleDb("SelApplicationCodes", Session("EmployeeConn"))
        thisdata.ExecNonQueryNoReturn()

        'BusLog = New AccountBusinessLogic
        'BusLog.Accounts = thisdata.GetSqlDataTable


        Dim rblAffiliateBinder As New RadioButtonListBinder(emp_type_cdrbl, "SelAffiliations", Session("EmployeeConn"))
        rblAffiliateBinder.BindData("AffiliationDescLower", "AffiliationDesc")


        FormSignOn = New FormSQL(Session("EmployeeConn"), "SelAccountRequestV3", "UpdFullAccountRequestV3", "", Page)
        FormSignOn.AddSelectParm("@account_request_num", iRequestNum, SqlDbType.Int, 6)


        UserInfo = New UserSecurity(Session("LoginID"), Session("EmployeeConn"))


        If Not IsPostBack Then

            Dim cblFacilitesBinder As New CheckBoxListBinder(cblFacilities, "SelFacilityEntity", Session("EmployeeConn"))
            cblFacilitesBinder.AddDDLCriteria("@EntityFacility", "Facility", SqlDbType.NVarChar)
            cblFacilitesBinder.BindData("Code", "EntityFacilityDesc")

            Dim cblEntityBinder As New CheckBoxListBinder(cblEntities, "SelFacilityEntity", Session("EmployeeConn"))
            cblEntityBinder.AddDDLCriteria("@EntityFacility", "Entity", SqlDbType.NVarChar)
            cblEntityBinder.BindData("Code", "EntityFacilityDesc")

            FormSignOn.FillForm()
            CheckAffiliation()
            fillRequestItemsTable()

            If close_dateTextBox.Text <> "" Then
                Dim ctrl As Control = DataCheckingAndValidation.SetChildControlsRecursiveToEnabledFalse(tpDemographics, False)
                tpAccounts.Visible = False
                tpPendingRequests.Visible = False

            Else '' if this request is open 
                If providerrbl.Text = "Yes" Then
                    ' only 90 security level and above edit these fields
                    If Session("SecurityLevelNum") < 90 Then
                        Dim ctrl As Control = DataCheckingAndValidation.SetChildControlsRecursiveToEnabledFalse(tpDemographics, False)

                    End If
                End If

                Dim thisdata As New CommandsSqlAndOleDb("SelPendingQueuesByRequestNum", Session("EmployeeConn"))
                thisdata.AddSqlProcParameter("@account_request_num", iRequestNum, SqlDbType.Int, 20)

                thisdata.ExecNonQueryNoReturn()

                ViewState("dtAccounts") = thisdata.GetSqlDataTable


                thisdata = New CommandsSqlAndOleDb("SelAvailableAccountsByRequestNumber", Session("EmployeeConn"))
                thisdata.AddSqlProcParameter("@request_num", iRequestNum, SqlDbType.Int, 20)

                thisdata.ExecNonQueryNoReturn()

                ViewState("dtAvailableAccounts") = thisdata.GetSqlDataTable


                Dim ddlBinder As New DropDownListBinder

                ddlBinder.BindData(facility_cdddl, "SelFacilityCodes", "facility_cd", "facility_short_name", Session("EmployeeConn"))
                ddlBinder.BindData(Titleddl, "SelTitles", "TitleDesc", "TitleDesc", Session("EmployeeConn"))
                ddlBinder.BindData(Specialtyddl, "SelSpecialtyCodes", "specialty", "specialty", Session("EmployeeConn"))
                ddlBinder.BindData(practice_numddl, "sel_practices", "practice_num", "parctice_name", Session("EmployeeConn"))

                Dim ddlBuildingBinder As New DropDownListBinder
                ddlBuildingBinder = New DropDownListBinder(building_cdddl, "sel_building_by_facility", Session("EmployeeConn"))
                ddlBuildingBinder.AddDDLCriteria("@facility_cd", facility_cdddl.SelectedValue, SqlDbType.NVarChar)
                ddlBuildingBinder.BindData("building_cd", "building_name")

                Dim sBuilding As String = building_cdTextBox.Text
                building_cdddl.SelectedValue = sBuilding

                displayPendingQueues()


            End If


            dtRemoveAccounts.Columns.Add("account_request_seq_num", GetType(Integer))
            dtRemoveAccounts.Columns.Add("ApplicationNum", GetType(Integer))
            dtRemoveAccounts.Columns.Add("ApplicationDesc", GetType(String))

            dtAddAccounts.Columns.Add("Applicationnum", GetType(Integer))
            dtAddAccounts.Columns.Add("ApplicationDesc", GetType(String))

            ViewState("dtRemoveAccounts") = dtRemoveAccounts
            ViewState("dtAddAccounts") = dtAddAccounts

            'ddlBinder.BindData(entity_cdDDL, "sel_entity_codes", "entity_cd", "hosp_desc", EmployeeConn)
            'department_cdddl.Items.Add("Select Entity")

            emp_type_cdrbl.Focus()

            Page.Controls.Remove(building_cdTextBox)

            CurrentAccounts()
            AvailableAccounts()

        End If
      

    End Sub
    Protected Sub AvailableAccounts()


        grAvailableAccounts.DataSource = ViewState("dtAvailableAccounts")
        grAvailableAccounts.DataBind()


    End Sub
    Protected Sub CurrentAccounts()



        gvCurrentApps.DataSource = ViewState("dtAccounts")
        gvCurrentApps.DataBind()

        'gvRemoveAccounts.DataSource = ViewState("dtRemoveAccounts")
        'gvRemoveAccounts.DataBind()




    End Sub
    Protected Sub facility_cdddl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles facility_cdddl.SelectedIndexChanged
        Dim ddlBinder As DropDownListBinder


        ddlBinder = New DropDownListBinder(building_cdddl, "sel_building_by_facility", Session("EmployeeConn"))
        ddlBinder.AddDDLCriteria("@facility_cd", facility_cdddl.SelectedValue, SqlDbType.NVarChar)
        ddlBinder.BindData("building_cd", "building_name")



    End Sub
    Private Sub insAcctItems(ByVal requestNum As Integer, ByVal appNum As Integer, ByVal comments As String)




        Dim thisdata As New CommandsSqlAndOleDb("InsAccountItemsV3", Session("EmployeeConn"))
        thisdata.AddSqlProcParameter("@account_request_num", requestNum, SqlDbType.Int, 20)
        thisdata.AddSqlProcParameter("@ApplicationNum", appNum, SqlDbType.Int, 8)
        thisdata.AddSqlProcParameter("@account_item_desc", comments, SqlDbType.NVarChar, 255)
        thisdata.AddSqlProcParameter("@validate", "NeedsValidation", SqlDbType.NVarChar, 20)


        thisdata.ExecNonQueryNoReturn()

    End Sub
    Protected Sub btnRemoveAccts_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnInvalidAccts.Click
        Try



            For Each rw As GridViewRow In gvRemoveAccounts.Rows
                If rw.RowType = DataControlRowType.DataRow Then

                    Dim account_request_seq_num As String = gvRemoveAccounts.DataKeys(rw.RowIndex)("account_request_seq_num")





                    Dim thisData As New CommandsSqlAndOleDb("UpdValidateRequestItems", Session("EmployeeConn"))
                    thisData.AddSqlProcParameter("@account_request_seq_num", account_request_seq_num, SqlDbType.Int)
                    thisData.AddSqlProcParameter("@account_request_num", iRequestNum, SqlDbType.Int)
                    thisData.AddSqlProcParameter("@requestor_client_num", Session("ClientNum"), SqlDbType.NVarChar, 30)
                    thisData.AddSqlProcParameter("@validate", "invalid", SqlDbType.NVarChar, 10)



                    thisData.ExecNonQueryNoReturn()


                End If



            Next

        Catch ex As Exception

        End Try
        ' after adding these items reload entire request

        Response.Redirect("~/AccountRequestInfo.aspx?RequestNum=" & iRequestNum, True)

        ' Page.Response.Redirect(Page.Request.Url.ToString(), False)
    End Sub
    Protected Sub btnSubmitAddRequest_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmitAddRequest.Click
        Try

            ' Dim AccountCode As String

            For Each rw As GridViewRow In grAccountsToAdd.Rows
                If rw.RowType = DataControlRowType.DataRow Then

                    Dim appNum As String = grAccountsToAdd.DataKeys(rw.RowIndex)("Applicationnum")
                    insAcctItems(iRequestNum, appNum, "")


                    'If BusLog.isHospSpecific(appNum) Then
                    '    For Each cbFacility As ListItem In cblFacilities.Items

                    '        If cbFacility.Selected Then

                    '            AccountCode = BusLog.GetApplicationCode(appNum, cbFacility.Value)

                    '            insAcctItems(iRequestNum, AccountCode, "")

                    '        End If

                    '    Next

                    'ElseIf BusLog.isEntSpecific(appNum) Then

                    '    For Each cbEnt As ListItem In cblEntities.Items

                    '        If cbEnt.Selected Then

                    '            AccountCode = BusLog.GetApplicationCode(appNum, cbEnt.Value)

                    '            insAcctItems(iRequestNum, AccountCode, "")

                    '        End If

                    '    Next


                    'Else

                    '    insAcctItems(iRequestNum, appNum, "")

                    'End If



                End If

            Next

        Catch ex As Exception

        End Try
        ' after adding these items reload entire request

        Response.Redirect("~/AccountRequestInfo.aspx?RequestNum=" & iRequestNum, True)

        ' Page.Response.Redirect(Page.Request.Url.ToString(), False)

    End Sub
    Private Sub CheckAffiliation()

        pnlAlliedHealth.Visible = False




        Select Case emp_type_cdrbl.SelectedValue

            Case "employee"

                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")

                'Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                'Dim cityReq As New RequiredField(citytextbox, "City")
                'Dim stateReq As New RequiredField(statetextbox, "State")
                'Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                'Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                'Dim faxqReq As New RequiredField(faxtextbox, "Fax")
                '  Dim locReq As New RequiredField(facility_cdddl, "Location")

                ' Dim endReq As New RequiredField(end_dateTextBox, "End Date")


                pnlAlliedHealth.Visible = False
                pnlProvider.Visible = False
                providerrbl.SelectedIndex = -1




            Case "student"

                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")
                Dim titleReq As New RequiredField(Titleddl, "Title")
                Dim specReq As New RequiredField(Specialtyddl, "Specialty")
                Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim faxqReq As New RequiredField(faxtextbox, "Fax")


                pnlAlliedHealth.Visible = False
                pnlProvider.Visible = False
                providerrbl.SelectedIndex = -1

                'Dim locReq As New RequiredField(facility_cdddl, "Location")

                'Dim endReq As New RequiredField(end_dateTextBox, "End Date")

            Case "physician"
                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")
                Dim titleReq As New RequiredField(Titleddl, "Title")
                Dim specReq As New RequiredField(Specialtyddl, "Specialty")
                Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim faxqReq As New RequiredField(faxtextbox, "Fax")

                pnlProvider.Visible = True


                pnlAlliedHealth.Visible = False
                providerrbl.SelectedValue = "Yes"

            Case "non staff clinician"
                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")
                Dim titleReq As New RequiredField(Titleddl, "Title")
                Dim specReq As New RequiredField(Specialtyddl, "Specialty")
                Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim faxqReq As New RequiredField(faxtextbox, "Fax")

                pnlProvider.Visible = True

                pnlAlliedHealth.Visible = False
                providerrbl.SelectedIndex = -1

            Case "resident"
                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")
                Dim titleReq As New RequiredField(Titleddl, "Title")
                Dim specReq As New RequiredField(Specialtyddl, "Specialty")
                Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim faxqReq As New RequiredField(faxtextbox, "Fax")

                pnlProvider.Visible = True

                pnlAlliedHealth.Visible = False
                providerrbl.SelectedValue = "Yes"



            Case "vendor"

                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")

                Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim vendorRequired As New RequiredField(VendorNameTextBox, "Vendor")



                pnlAlliedHealth.Visible = False
                pnlProvider.Visible = False
                providerrbl.SelectedIndex = -1


            Case "contract"

                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")

                Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")


                pnlAlliedHealth.Visible = False
                pnlProvider.Visible = False
                providerrbl.SelectedIndex = -1

            Case "allied health"

                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")
                Dim titleReq As New RequiredField(Titleddl, "Title")
                Dim specReq As New RequiredField(Specialtyddl, "Specialty")
                Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim faxqReq As New RequiredField(faxtextbox, "Fax")



                providerrbl.SelectedIndex = 0

                pnlAlliedHealth.Visible = True
                pnlProvider.Visible = True



                providerrbl.SelectedValue = "Yes"



            Case "other"

                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")

                Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")



                pnlAlliedHealth.Visible = False
                pnlProvider.Visible = False
                providerrbl.SelectedIndex = -1



            Case Else


                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")


                pnlAlliedHealth.Visible = False
                pnlProvider.Visible = False
                providerrbl.SelectedIndex = -1



        End Select


    End Sub
    Private Sub displayPendingQueues()
        GetPendingQueues()


        If dtPendingRequests.Rows.Count > 0 Then

            '  cblApplications.Enabled = False


            Dim rwTitle As New TableRow
            Dim clTitle As New TableCell


            Dim iRowCount As Integer = 1
            Dim rwHeader As New TableRow
            Dim clAccountHdr As New TableCell
            Dim clRequestTypeHdr As New TableCell
            Dim clValidCdHdr As New TableCell
            Dim clRequestorHdr As New TableCell
            Dim clDateHdr As New TableCell


            clTitle.Text = "Pending Requests"
            clTitle.Font.Bold = True
            clTitle.ColumnSpan = 5
            clTitle.BackColor = Color.White
            clTitle.HorizontalAlign = HorizontalAlign.Center
            clTitle.Font.Size = 20



            rwTitle.Cells.Add(clTitle)

            tblPendingQueues.Rows.Add(rwTitle)

            clAccountHdr.Text = "Account"
            clRequestTypeHdr.Text = "Request"
            clValidCdHdr.Text = "Status"
            clRequestorHdr.Text = "Requester"
            clDateHdr.Text = "Request Date"

            rwHeader.Cells.Add(clAccountHdr)
            rwHeader.Cells.Add(clRequestTypeHdr)
            rwHeader.Cells.Add(clValidCdHdr)
            rwHeader.Cells.Add(clRequestorHdr)
            rwHeader.Cells.Add(clDateHdr)

            rwHeader.Font.Bold = True
            rwHeader.BackColor = Color.FromName("#006666")
            rwHeader.ForeColor = Color.FromName("White")
            rwHeader.Height = Unit.Pixel(36)

            tblPendingQueues.Rows.Add(rwHeader)

            tblPendingQueues.Width = New Unit("100%")



            For Each rwPending As DataRow In dtPendingRequests.Rows

                Dim rwData As New TableRow
                Dim clAccountData As New TableCell
                Dim clRequestTypeData As New TableCell
                Dim clValidCdData As New TableCell
                Dim clRequestorData As New TableCell
                Dim clDateData As New TableCell


                clAccountData.Text = IIf(IsDBNull(rwPending("ApplicationDesc")), "", rwPending("ApplicationDesc"))
                clRequestTypeData.Text = IIf(IsDBNull(rwPending("account_request_type")), "", rwPending("account_request_type"))
                clValidCdData.Text = IIf(IsDBNull(rwPending("valid_cd")), "", rwPending("valid_cd"))
                clRequestorData.Text = IIf(IsDBNull(rwPending("requestor_name")), "", rwPending("requestor_name"))
                clDateData.Text = IIf(IsDBNull(rwPending("entered_date")), "", rwPending("entered_date"))

                rwData.Cells.Add(clAccountData)
                rwData.Cells.Add(clRequestTypeData)
                rwData.Cells.Add(clValidCdData)
                rwData.Cells.Add(clRequestorData)
                rwData.Cells.Add(clDateData)


                If iRowCount > 0 Then

                    rwData.BackColor = Color.Bisque

                Else

                    rwData.BackColor = Color.LightGray

                End If



                iRowCount = iRowCount * -1


                tblPendingQueues.Rows.Add(rwData)


            Next
            tpPendingRequests.Visible = True
        Else

            tpPendingRequests.Visible = False
        End If

    End Sub
    Private Sub GetPendingQueues()
        Dim thisdata As New CommandsSqlAndOleDb("SelPendingQueuesByRequestNum", Session("EmployeeConn"))
        thisdata.AddSqlProcParameter("@account_request_num", iRequestNum, SqlDbType.Int, 20)

        dtPendingRequests = thisdata.GetSqlDataTable

    End Sub
    Protected Sub gvCurrentApps_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvCurrentApps.RowCommand

        If e.CommandName = "Select" Then


            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim AccountReqNum As String = gvCurrentApps.DataKeys(Convert.ToInt32(e.CommandArgument))("account_request_seq_num").ToString()
            Dim ApplicationNum As String = gvCurrentApps.DataKeys(Convert.ToInt32(e.CommandArgument))("ApplicationNum").ToString()
            Dim ApplicationDesc As String = gvCurrentApps.DataKeys(Convert.ToInt32(e.CommandArgument))("ApplicationDesc").ToString()


            dtRemoveAccounts = DirectCast(ViewState("dtRemoveAccounts"), DataTable)
            dtRemoveAccounts.Rows.Add(AccountReqNum, ApplicationNum, ApplicationDesc)

            gvRemoveAccounts.DataSource = dtRemoveAccounts
            gvRemoveAccounts.DataBind()
            gvRemoveAccounts.SelectedIndex = -1

            Dim dtAccounts As New DataTable
            dtAccounts = DirectCast(ViewState("dtAccounts"), DataTable)
            dtAccounts.Rows(index).Delete()
            dtAccounts.AcceptChanges()


            ViewState("dtAccounts") = dtAccounts

            gvCurrentApps.DataSource = dtAccounts
            gvCurrentApps.DataBind()



            uplAccounts.Update()

        End If
    End Sub
    Protected Sub gvRemoveAccounts_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvRemoveAccounts.RowCommand

        If e.CommandName = "Select" Then


            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim ApplicationNum As String = gvRemoveAccounts.DataKeys(Convert.ToInt32(e.CommandArgument))("ApplicationNum").ToString()
            Dim AccountReqNum As String = gvRemoveAccounts.DataKeys(Convert.ToInt32(e.CommandArgument))("account_request_seq_num").ToString()
            Dim ApplicationDesc As String = gvRemoveAccounts.DataKeys(Convert.ToInt32(e.CommandArgument))("ApplicationDesc").ToString()

            Dim dtAccounts As New DataTable
            dtAccounts = DirectCast(ViewState("dtAccounts"), DataTable)
            dtAccounts.Rows.Add(AccountReqNum, ApplicationNum, ApplicationDesc)

            ViewState("dtAccounts") = dtAccounts

            gvCurrentApps.DataSource = dtAccounts
            gvCurrentApps.DataBind()
            gvCurrentApps.SelectedIndex = -1



            dtRemoveAccounts = DirectCast(ViewState("dtRemoveAccounts"), DataTable)
            dtRemoveAccounts.Rows(index).Delete()
            dtRemoveAccounts.AcceptChanges()


            ViewState("dtRemoveAccounts") = dtRemoveAccounts

            gvRemoveAccounts.DataSource = dtRemoveAccounts
            gvRemoveAccounts.DataBind()
            gvRemoveAccounts.SelectedIndex = -1



            uplAccounts.Update()

        End If
    End Sub
    Protected Sub grAccountsToAdd_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grAccountsToAdd.RowCommand

        If e.CommandName = "Select" Then


            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim ApplicationNum As String = grAccountsToAdd.DataKeys(Convert.ToInt32(e.CommandArgument))("Applicationnum").ToString()
            Dim ApplicationDesc As String = grAccountsToAdd.DataKeys(Convert.ToInt32(e.CommandArgument))("ApplicationDesc").ToString()

            Dim dtAvailableAccounts As New DataTable
            dtAvailableAccounts = DirectCast(ViewState("dtAvailableAccounts"), DataTable)
            dtAvailableAccounts.Rows.Add(ApplicationNum, ApplicationDesc)

            ViewState("dtAvailableAccounts") = dtAvailableAccounts

            grAvailableAccounts.DataSource = dtAvailableAccounts
            grAvailableAccounts.DataBind()
            grAvailableAccounts.SelectedIndex = -1


            dtAddAccounts = DirectCast(ViewState("dtAddAccounts"), DataTable)
            dtAddAccounts.Rows(index).Delete()
            dtAddAccounts.AcceptChanges()


            ViewState("dtAddAccounts") = dtAddAccounts

            grAccountsToAdd.DataSource = dtAddAccounts
            grAccountsToAdd.DataBind()
            grAccountsToAdd.SelectedIndex = -1

            uplAccounts.Update()

        End If
    End Sub
    Protected Sub grAvailableAccounts_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grAvailableAccounts.RowCommand

        If e.CommandName = "Select" Then


            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim ApplicationNum As String = grAvailableAccounts.DataKeys(Convert.ToInt32(e.CommandArgument))("Applicationnum").ToString()
            Dim ApplicationDesc As String = grAvailableAccounts.DataKeys(Convert.ToInt32(e.CommandArgument))("ApplicationDesc").ToString()


            dtAddAccounts = DirectCast(ViewState("dtAddAccounts"), DataTable)
            dtAddAccounts.Rows.Add(ApplicationNum, ApplicationDesc)

            grAccountsToAdd.DataSource = dtAddAccounts
            grAccountsToAdd.DataBind()
            grAccountsToAdd.SelectedIndex = -1

            Dim dtAvailableAccounts As New DataTable
            dtAvailableAccounts = DirectCast(ViewState("dtAvailableAccounts"), DataTable)
            dtAvailableAccounts.Rows(index).Delete()
            dtAvailableAccounts.AcceptChanges()

            grAvailableAccounts.DataSource = dtAvailableAccounts
            grAvailableAccounts.DataBind()
            grAvailableAccounts.SelectedIndex = -1

            ViewState("dtAvailableAccounts") = dtAvailableAccounts
            ViewState("dtAddAccounts") = dtAddAccounts


            grAccountsToAdd.DataSource = dtAddAccounts
            grAccountsToAdd.DataBind()
            grAccountsToAdd.SelectedIndex = -1


            uplAccounts.Update()

        End If
    End Sub

    'Protected Function checkFacilities() As Boolean
    '    Dim blnReturn = False

    '    For Each item As ListItem In cblFacilities.Items
    '        If item.Selected Then
    '            blnReturn = True

    '        End If


    '    Next


    '    Return blnReturn

    'End Function

    'Protected Function checkEntities() As Boolean
    '    Dim blnReturn As Boolean = False

    '    For Each item As ListItem In cblEntities.Items
    '        If item.Selected Then

    '            blnReturn = True
    '        End If

    '    Next


    '    Return blnReturn
    'End Function
    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click


        FormSignOn.AddInsertParm("@submitter_client_num", UserInfo.ClientNum, SqlDbType.NVarChar, 10)
        FormSignOn.AddInsertParm("@account_request_num", iRequestNum, SqlDbType.Int, 9)
        FormSignOn.UpdateForm()

    End Sub
    Protected Sub fillRequestItemsTable()
        Dim thisData As New CommandsSqlAndOleDb("SelRequestItems", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@account_request_num", iRequestNum, SqlDbType.NVarChar)

        Dim iRowCount = 1

        Dim dt As DataTable
        dt = thisData.GetSqlDataTable

        Dim rwHeader As New TableRow

        Dim clAccountHeader As New TableCell
        Dim clRequestTypeHeader As New TableCell
        Dim clStatusHeader As New TableCell



        clAccountHeader.Text = "Account"
        clRequestTypeHeader.Text = "Request Type"
        clStatusHeader.Text = "Status"



        rwHeader.Cells.Add(clAccountHeader)
        rwHeader.Cells.Add(clRequestTypeHeader)
        rwHeader.Cells.Add(clStatusHeader)


        rwHeader.Font.Bold = True
        rwHeader.BackColor = Color.FromName("#006666")
        rwHeader.ForeColor = Color.FromName("White")
        rwHeader.Height = Unit.Pixel(36)

        tblRequestItems.Rows.Add(rwHeader)

        For Each drRow As DataRow In dt.Rows


            Dim rwData As New TableRow

            Dim clAccountData As New TableCell
            Dim clRequestTypeData As New TableCell
            Dim clStatusData As New TableCell
            Dim clRequestorData As New TableCell
            Dim clReqPhoneData As New TableCell




            clAccountData.Text = IIf(IsDBNull(drRow("ApplicationDesc")), "", drRow("ApplicationDesc"))
            clRequestTypeData.Text = IIf(IsDBNull(drRow("account_request_type")), "", drRow("account_request_type"))
            clStatusData.Text = IIf(IsDBNull(drRow("valid_cd_desc")), "", drRow("valid_cd_desc"))

            rwData.Cells.Add(clAccountData)
            rwData.Cells.Add(clRequestTypeData)
            rwData.Cells.Add(clStatusData)


            If iRowCount > 0 Then

                rwData.BackColor = Color.Bisque

            Else

                rwData.BackColor = Color.LightGray

            End If

            tblRequestItems.Rows.Add(rwData)

            iRowCount = iRowCount * -1

        Next
        tblRequestItems.CellPadding = 10
        tblRequestItems.Width = New Unit("100%")


    End Sub
End Class
