﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="false" CodeFile="AddRemoveAcct.aspx.vb" Inherits="AddRemoveAcctForm" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <style type="text/css">
    .style13
    {
        height: 30px;
    }
    .style15
    {
        font-size: larger;
    }
    
    .style23
    {
        width: 200px;
    }
    .style25
    {
        width: 100%;
    }
    .textHasFocus
    {
         border-color:Red;
        border-width:medium;
        border-style:outset;
    }
   
    .style26
    {
        width: 91px;
    }
    .style27
    {
        width: 110px;
    }
   
    .style28
    {
        width: 80px;
    }
    .style29
    {
        width: 94px;
    }
   
    .style30
    {
        width: 85px;
    }
   
    .style31
    {
        width: 192px;
    }
    .style32
    {
        width: 206px;
    }
   
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

<asp:UpdatePanel ID="uplSignOnForm" runat="server" >
 <ContentTemplate>
      <table width= "100%">
       <tr>
        <td align="center">
            <table>
                <tr>
                    <td align="right">
                    <asp:Label ID="lblusersubmit" runat="server" Text="User Submitting:" 
                        Width="125px" />
                    </td>

                    <td align="left">
                        <asp:Label ID="userSubmittingLabel" Width="150px" runat="server" Font-Bold="true"></asp:Label>
                    </td>

                    <td align="right">
                        Current Requestor:
                    </td>

                    <td align="left">
                        <asp:Label ID="SubmittingOnBehalfOfNameLabel" runat="server" Font-Bold="true"></asp:Label>
                        <asp:Label ID="requestor_client_numLabel" runat="server" visible="false"></asp:Label>
                    </td>
                </tr>
                <tr>
            
                    <td align="center" colspan="4">
                        <asp:Button ID="btnSelectOnBehalfOf" runat="server" Font-Bold="True" 
                            Font-Names="Arial" Font-Size="Small" Height="20px" Text="Change Requestor" 
                            Width="140px" />
                    </td>

                </tr>
           </table>

        </td>
    </tr>

     <tr>
        <td align="left">
             <ajaxToolkit:TabContainer ID="tabs" runat="server" ActiveTabIndex="0"
                Width="100%" style="margin-bottom: 1px" EnableTheming="False" 
                BackColor="#efefef">

                  <ajaxToolkit:TabPanel ID="tpDemographics" runat="server"  BackColor="Gainsboro" Visible = "True" EnableTheming="False">
                     <HeaderTemplate > 
                            <asp:Label ID="Label1" runat="server" Text="Demographics" Font-Bold="True" 
                                ForeColor="Black"></asp:Label>
                     </HeaderTemplate>
                     <ContentTemplate>
                        <table border="3" style="width: 100%">
                           <tr>
                            <td align="left">
                                <asp:Label ID = "lblValidation" runat = "server" ForeColor="Red"/>
                            </td>
                          </tr>
                        <tr>
                                    <td class="tableRowHeader">
                                    Client Account(s) Information
                                    </td>
                        </tr>
                        <tr>
          
                            <td class="tableRowSubHeader"  align="left">
                            Form Submission
                            </td>
            
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button ID = "btnUpdate" runat = "server" Text ="Update Demographics " Width="175px" 
                                    BackColor="#336666" ForeColor="White" />
              
                            </td>
                        </tr>
                        <tr>
                            <td>
<%--                            <asp:Label ID = "submitter_nameLabel" runat="server" Font-Bold="True"></asp:Label>
                                <asp:Label ID = "requestor_nameLabel" runat="server" Font-Bold="True"></asp:Label>--%>
                            </td>
                        </tr>
                        <tr>
                            <td class="tableRowSubHeader"  align="left">
                            CKHS Affiliation:
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:RadioButtonList ID ="emp_type_cdrbl" runat = "server" 
                                    RepeatDirection="Horizontal" AutoPostBack="True" Font-Size="Smaller">
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td>

                            <asp:Panel runat="server" ID ="pnlAlliedHealth" Visible="False">
                            <table>
                                <tr>
                                <td width="30%">
                                        Allied Health Degree:
                                </td>
                                <td>
                                    <asp:DropDownList runat="server"  width="40%" ID = "OtherAffDescddl">
                                    </asp:DropDownList> 
                                </td>
                                </tr>
                
                            </table>
                            </asp:Panel>
                        </td>
                        </tr>
                        <asp:Panel runat="server" ID="pnlProvider" Visible="False">
                            <tr>
                            <td colspan="4"  class="tableRowSubHeader" align="left">
                                Provider Information
                            </td>
                            </tr>
                            <tr>
                            <td colspan="4">
                                <table id="tblprovider"  border="3" width="100%">
                                            
                                <tr>
                                    <td align="right">
                                    <asp:Label ID="lblprov" runat="server" Text = "Provider:"  Width="150px"/>
                                    </td>
                                    <td  align="left" colspan="3">
                                    <asp:RadioButtonList ID = "providerrbl" runat="server" RepeatDirection="Horizontal">
                                        <asp:ListItem>Yes</asp:ListItem>
                                            <asp:ListItem >No</asp:ListItem>            
                                        </asp:RadioButtonList>
                                    </td>

                                </tr>
                                <tr>
                                <td align="right" >
                                Credentialed Date:
                                </td>
                                <td align="left" colspan="3">
                                    <asp:TextBox ID = "credentialedDateTextBox" runat="server" CssClass="calenderClass"></asp:TextBox>
                                </td>
                       
                                </tr>
                                <tr>
                                <td align="right" >
                                    CKHN-HAN:
                                </td>
                                <td align="left" >
                                    <asp:RadioButtonList ID = "hanrbl" runat="server" RepeatDirection="Horizontal">
                                        <asp:ListItem>Yes</asp:ListItem>
                                        <asp:ListItem Selected="True">No</asp:ListItem>            
                                    </asp:RadioButtonList>
                                </td>
                                    <td align="right">
                                            Doctor Number:
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID = "doctor_master_numTextBox" runat="server" Enabled="false"></asp:TextBox>
                                    </td>

                                </tr>
                                <tr>
                                <td align="right">
                                    NPI:
                                </td>
                                <td  align="left" >
                                    <asp:TextBox ID = "npitextbox" runat = "server"></asp:TextBox>
                                </td>
                                <td align="right" >
                                    License No.
                                </td>
                                <td align="left" >
                                        <asp:TextBox ID = "LicensesNo" runat="server"></asp:TextBox>
                                </td>
                                </tr>
                                <tr>
                                <td align="right">
                                    Taxonomy:
                                    </td>
                                <td align="left" >
                                    <asp:TextBox ID = "taxonomytextbox" runat="server"></asp:TextBox>
                                </td>
                                    <td align="right" >
                                        Group Number:
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="group_numTextBox" runat="server"></asp:TextBox>
                                    </td>

                            </tr>
                                <tr>
                                    <td align="right" >
                                    Group Name:

                                    </td>
                                    <td align="left" colspan="3">
                                        <asp:TextBox ID="group_nameTextBox" runat="server" Width="500px"></asp:TextBox>
                                    </td>
                            </tr>
                                <tr>
                                <td align="right" >
                                Practice Name:
                                </td>       
                                <td colspan="3" align="left">
                                <asp:DropDownList ID ="practice_numddl"  Width="500px" runat="server">
            
                                </asp:DropDownList>
                                </td>
                            </tr>
                                <tr>
                                <td align="right">
                                Title:
                                </td>
                                <td align="left" >
                                    <asp:dropdownlist ID = "Titleddl" runat="server"></asp:dropdownlist>
                                </td>
                                <td  align="right" >
                                Specialty:
                                </td>
                                <td align="left" >
                                    <asp:dropdownlist ID = "Specialtyddl" runat="server"></asp:dropdownlist>
                                </td>
                            </tr>
                                <tr>
                                <td align="right" >
                                    Non-CKHN Location:
                                </td>
                                <td align="left" colspan="3">
                                        <asp:TextBox ID = "NonHanLocationTextBox" runat="server" Width="500px"></asp:TextBox>
                                </td>

                            </tr>
                        </table>
                            </td>
                            </tr>
                        </asp:Panel> 
                        <tr>
                        <td>
                            <table id="tbldemo" border="3px" width="100%">
                            <tr>
                            <td colspan="4" class="tableRowSubHeader"  align="left">
                                Demographics
                            </td>
                            </tr>
                            <tr>

                            <td colspan="4" >
                                <table class="style3" > 
                                <tr>
                                    <td>
                                        First Name:
                                    </td>
                                    <td align="left"  >
                                        <asp:TextBox width="280px" ID = "first_nameTextBox" runat="server" ></asp:TextBox>
                                    </td>
                                    <td>
                                        MI:
                                    </td>
                                    <td>
                                        <asp:TextBox ID = "middle_initialTextBox" runat="server" Width="15px"></asp:TextBox>
                                    </td>
                                    <td align="left" >
                                        Last Name:
                                    </td>

                                    <td align="left" >
                                        <asp:TextBox ID = "last_nameTextBox" width="280px" runat="server"></asp:TextBox>
                                    </td>
                                    <td>
                                        Suffix: 
                                    </td>
                                    <td>
                                        <asp:dropdownlist id="suffixddl" runat="server" Height="20px" Width="52px">
				                            <asp:ListItem Value="Sr.">Sr.</asp:ListItem>
				                            <asp:ListItem Value="Jr.">Jr.</asp:ListItem>
				                            <asp:ListItem Value="II">II</asp:ListItem>
				                            <asp:ListItem Value="III">III</asp:ListItem>
				                            <asp:ListItem Value="IV">IV</asp:ListItem>
                                            </asp:dropdownlist>
                                    </td>
                                </tr>
                                </table>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                Employee Number:
                                </td>
                                <td>
                                <asp:Label ID ="siemens_emp_numLabel" runat="server"></asp:Label>
                                </td>
                                <td>Network Logon</td>
                                <td>
                                    <asp:TextBox ID = "login_nameTextBox" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                            <td align="right" width="20%" >
                                <asp:Label ID="lblpos" runat="server" Text="Position Description:" />
                            </td>
                            <td colspan = "3" align="left" >
                                <asp:TextBox ID="user_position_descTextBox" runat="server" Width = "500px"></asp:TextBox>
                            </td>
                        </tr>
                            <tr>
                            <td align="right" >
                            Entity:
                            </td>
                            <td align="left">
                            <asp:DropDownList ID ="entity_cdDDL" runat="server" AutoPostBack ="True" >
            
                            </asp:DropDownList>
                            </td>
       
                            <td align="right" >
                                <asp:Label ID="lbldept" runat="server" Text = "Department Name:" />
                            </td>
                            <td align="left"  >
                            <asp:DropDownList ID ="department_cdddl" runat="server" Width="250px">
            
                            </asp:DropDownList>
                            </td>
                        </tr>
                            <tr>
                            <td  align="right">
                                Address 1:
                                </td>
                            <td align="left"> 
                                    <asp:TextBox ID = "address_1textbox" runat="server" Width="250px"></asp:TextBox>
                                </td>
                            <td align="right" >
                                Address 2:
                                </td>
                            <td align="left" >
                                <asp:TextBox ID = "address_2textbox" runat="server" Width="250px"></asp:TextBox>
                                </td>

                        </tr>
                            <tr>
                                <td align="right">
                                    <asp:Label id="lblcity" runat="server" width="50px"  text="City:" />
                                </td>
                                <td align="left" colspan="3">
                                    <asp:TextBox ID = "citytextbox" runat="server" width="500px"></asp:TextBox>
                                </td>


                            </tr>

                            <tr>

                                <td align="right">
                                    <asp:Label ID="lblstate" runat="server" Text="State:" Width="50px" />
                                </td>
                                <td align="left">
                                    <asp:TextBox ID = "statetextbox" runat="server" Width="50px"></asp:TextBox>
                                    </td>
                                <td align="right" >

                                    <asp:Label ID="lblzip" runat="server" Text="Zip:" Width="25px" />
                                </td>
                                <td align="left">
                                    <asp:TextBox ID = "ziptextbox" runat="server" Width="50px"></asp:TextBox>
                                </td>
                        </tr>
                            <tr>
                            <td align="right"  >
                            Phone Number:
                            </td>
                            <td align="left" >
                            <asp:TextBox ID = "phoneTextBox" runat="server"></asp:TextBox>
                            </td>
       
                            <td align="right" >
                            Pager Number:
                            </td>
                            <td align="left" >
                            <asp:TextBox ID = "pagerTextBox" runat="server"></asp:TextBox>
                            </td>
                        </tr>

                            <tr>
                            <td align="right" >
                            Fax:
                            </td>
                            <td align="left" >
            
                                <asp:TextBox ID = "faxtextbox" runat="server"></asp:TextBox>
                                </td>
         
                                <td align="right" >
                            Email:
                            </td>
                            <td align="left" >
                            <asp:TextBox ID = "e_mailTextBox" runat="server" Width="250px"></asp:TextBox>
                            </td>
                        </tr>
                            <tr>
                            <td align="right" >
                            Location:
                            </td>
                            <td align="left" >
         
                                <asp:DropDownList ID ="facility_cdddl" runat="server" AutoPostBack="True">
              
                            </asp:DropDownList>
          
                            </td>
        
                            <td align="right" >
                            Building:
                            </td>
                            <td align="left" >

                                <asp:DropDownList ID ="building_cdddl" runat="server" CssClass="style3">
            
                            </asp:DropDownList>
          
                            </td>
                        </tr>
                            <tr>
          
                            <td align="right" >
                                Vendor Name:
                                </td>
                            <td align="left"  colspan="3">
                                <asp:TextBox ID ="VendorNameTextBox" runat="server" Width="500px" ></asp:TextBox>
                            </td>
                        </tr>
                            <tr>
                            <td align="right" >
                            Start Date:
                            </td>
                            <td align="left" >
                                <asp:TextBox ID ="start_dateTextBox" runat="server" CssClass="calenderClass"></asp:TextBox>
                            </td>
       
                            <td align="right" >
                            End Date:
                            </td>
                            <td align="left" >
                                <asp:TextBox ID="end_dateTextBox" runat="server" CssClass="calenderClass"></asp:TextBox>
                            </td>
                            </tr>
                           <tr>
                                <td colspan = "4">
                                    <asp:Label ID ="department_cdLabel" runat="server" Visible="False"></asp:Label>
                                
                                    <asp:TextBox ID = "building_cdTextBox" runat="server" Visible ="False"></asp:TextBox>
                                    <asp:Label ID = "emp_type_cdLabel" runat="server" Visible="false"></asp:Label>
                                    <asp:Label ID="HDemployeetype" runat="server" Visible="false"></asp:Label>
                                </td>
                            </tr>
                            </table>
                        </td>
                        </tr>
                       </table>
                     </ContentTemplate>
                  </ajaxToolkit:TabPanel>

                  <ajaxToolkit:TabPanel ID="tpAddItems" runat="server">
                    <HeaderTemplate>
                        <asp:Label ID="lblinner1" runat="server" Text="Request New Accounts" Font-Bold="true" ForeColor="Black"></asp:Label>
                    </HeaderTemplate>
                     <ContentTemplate>
                      <table border="1" style="background-color:#DCDCDC">
                        <tr>
                            <td colspan = "4" align="left">
                            <asp:Button runat="server" ID ="btnSubmitAddRequest" Text = "Submit Add Request"
                                    Width="130px" Height="20px" Font-Names="Arial"  Font-Bold="True" Font-Size="Small" /> 
                            </td>
                        </tr>
                        <tr style="background-color:#FFFFCC; font-weight:bold">
                                <td colspan="2">
                                Accounts available
                                </td>
                                <td colspan="2">
                                Accounts to be added
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2"  valign="top" style="width: 50%">
                                    <asp:GridView ID="grAvailableAccounts" runat="server" AllowSorting="True" 
                                    AutoGenerateColumns="False" BackColor="White" BorderColor="#336666"
                                    BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                                    EmptyDataText="No Applications Found" EnableTheming="False" 
                                    DataKeyNames="Applicationnum, ApplicationDesc"
                                    Font-Size="Medium" GridLines="Horizontal" Width="100%">
                                   
                                    <FooterStyle BackColor="White" ForeColor="#333333" />
                                    <HeaderStyle BackColor="#336666" Font-Bold="False" ForeColor="White" Font-Size="Small" HorizontalAlign="Left"    />                                    
                                    <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Left" Font-Size="Medium"/>
                                    <RowStyle BackColor="White" ForeColor="#333333" />
                                    <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                    <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                    <SortedAscendingHeaderStyle BackColor="#487575" />
                                    <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                    <SortedDescendingHeaderStyle BackColor="#275353" />

                                    <Columns>
                                            <asp:CommandField ButtonType="Button" SelectText="Select" ShowSelectButton= "true" HeaderStyle-HorizontalAlign = "Left"  ControlStyle-BackColor="#84A3A3" ItemStyle-Height="30px" />
                                            <asp:BoundField  DataField="ApplicationDesc"  HeaderText="Application"  HeaderStyle-HorizontalAlign = "Left" ItemStyle-Height="35px"></asp:BoundField>
				                                                                  
                                  
                                    </Columns>
                                    </asp:GridView>
		                        </td>
                                <td colspan="2"  valign="top" style="width: 50%">
                                    <asp:GridView ID="grAccountsToAdd" runat="server" AllowSorting="True" 
                                    AutoGenerateColumns="False" BackColor="White" BorderColor="#336666"
                                    BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                                    EmptyDataText="Select accounts to add" EnableTheming="False" 
                                    DataKeyNames="Applicationnum, ApplicationDesc"
                                    Font-Size="Medium" GridLines="Horizontal" Width="100%">
                                   
                                    <FooterStyle BackColor="White" ForeColor="#333333" />
                                    <HeaderStyle BackColor="#336666" Font-Bold="False" ForeColor="White" Font-Size="Small" HorizontalAlign="Left"    />                                    
                                    <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Left" Font-Size="Medium"/>
                                    <RowStyle BackColor="White" ForeColor="#333333" />
                                    <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                    <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                    <SortedAscendingHeaderStyle BackColor="#487575" />
                                    <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                    <SortedDescendingHeaderStyle BackColor="#275353" />

                                    <Columns>
                                                                                            
                                                <asp:CommandField ButtonType="Button" SelectText="<-- Remove" ShowSelectButton= "true" HeaderStyle-HorizontalAlign = "Left"  ControlStyle-BackColor="#84A3A3" ItemStyle-Height="30px"/>
                                                <asp:BoundField  DataField="ApplicationDesc"  HeaderText="Application"  HeaderStyle-HorizontalAlign = "Left" ItemStyle-Height="35px"></asp:BoundField>
				                                                                  
                                  
                                    </Columns>

                                </asp:GridView>
		                        </td>
                                </tr>
                        </table>
                        </ContentTemplate>
                  </ajaxToolkit:TabPanel>
                  
                  <ajaxToolkit:TabPanel ID="tpRemoveItems" runat="server"  BackColor="Gainsboro" Visible = "True">
                    <HeaderTemplate > 
                        <asp:Label ID="Label4" runat="server" Text="Remove Current Accounts" Font-Bold="true" ForeColor="Black"></asp:Label>
                    </HeaderTemplate>
                      <ContentTemplate>

                        <table width="100%" border="1"  style="background-color:#DCDCDC">
                            <tr>
                                <td colspan = "4" align="left">
                                    <asp:Button runat="server" ID ="btnInvalidAccts" Text = "Submit Invalid Items"  /> 
                                </td>
                            </tr>
                            <tr style="background-color:#FFFFCC; font-weight:bold">
                                <td colspan="2" >
                                Accounts
                                </td>
                                <td colspan="2">
                                Accounts to remove
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2"  valign="top"  style="width: 50%">
                                    <asp:GridView ID="gvCurrentApps" runat="server" AllowSorting="True" 
                                        AutoGenerateColumns="False" BackColor="White" BorderColor="#336666"
                                        BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                                        EmptyDataText="No Applications Found" EnableTheming="False" DataKeyNames="ApplicationNum, ApplicationDesc, create_date, login_name"
                                        Font-Size="Medium" GridLines="Horizontal" Width="100%">
                                   
                                        <FooterStyle BackColor="White" ForeColor="#333333" />
                                        <HeaderStyle BackColor="#336666" Font-Bold="False" ForeColor="White" Font-Size="Small" HorizontalAlign="Left"    />                                    
                                        <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Left" Font-Size="Medium"/>
                                        <RowStyle BackColor="White" ForeColor="#333333" />
                                        <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                        <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                        <SortedAscendingHeaderStyle BackColor="#487575" />
                                        <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                        <SortedDescendingHeaderStyle BackColor="#275353" />

                                        <Columns>
                                                                                            
                                                    <asp:CommandField ButtonType="Button" SelectText="Select" ShowSelectButton= "true" ControlStyle-BackColor="#84A3A3" ItemStyle-Height="30px"/>
                                                    <asp:BoundField  DataField="ApplicationDesc"  HeaderText="Application" HeaderStyle-HorizontalAlign="Left"  ItemStyle-Height="35px"></asp:BoundField>
				                                                                  
                                  
                                        </Columns>
                                        </asp:GridView>
		                            </td>
                                    <td colspan="2" valign="top"  style="width: 50%">
                                        <asp:GridView ID="gvRemoveAccounts" runat="server" AllowSorting="True" 
                                                AutoGenerateColumns="False" BackColor="White" BorderColor="#336666"
                                                BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                                                EmptyDataText="Select applications to be removed" EnableTheming="False" DataKeyNames="ApplicationNum, ApplicationDesc, login_name"
                                                Font-Size="Medium" GridLines="Horizontal" Width="100%" >
                                   
                                            <FooterStyle BackColor="White" ForeColor="#333333" />
                                            <HeaderStyle BackColor="#336666" Font-Bold="False" ForeColor="White" Font-Size="Small" HorizontalAlign="Left"    />                                    
                                            <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Left" Font-Size="Medium"/>
                                            <RowStyle BackColor="White" ForeColor="#333333" />
                                            <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                            <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                            <SortedAscendingHeaderStyle BackColor="#487575" />
                                            <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                            <SortedDescendingHeaderStyle BackColor="#275353" />

                                            <Columns>
                                                    <asp:CommandField ButtonType="Button" SelectText="<-- Remove" ShowSelectButton= "true"   ControlStyle-BackColor="#84A3A3" ItemStyle-Height="30px" />
                                                    <asp:BoundField  DataField="ApplicationDesc"  HeaderText="Application" HeaderStyle-HorizontalAlign="Left" ItemStyle-Height="30px" ></asp:BoundField>
                                            </Columns>
                                            </asp:GridView>
                                        </td>
                                </tr>
                        </table>
                        </ContentTemplate>
                </ajaxToolkit:TabPanel>

                 <ajaxToolkit:TabPanel ID="tpOpenRequest" runat="server"  BackColor="red" >
                      <HeaderTemplate > 
                          <asp:Label ID="Label2" runat="server" Text="Open Requests" Font-Bold="true" ForeColor="Black" ></asp:Label>
                     </HeaderTemplate>
                      <ContentTemplate>
                          <table border="3" width = "100%">
                            <tr>
                                <td colspan = "4">
                                      <asp:Table ID = "tblOpenRequests" runat = "server">
                                     </asp:Table>
            
                                </td>
        
                            </tr>
                           </table>
                       </ContentTemplate>
                  </ajaxToolkit:TabPanel>

<%-- All tabs below will not show on this page but are on AccountInfo page  --%>

<%--                  <ajaxToolkit:TabPanel ID="tpPendingRequests" runat="server" TabIndex="0"  BackColor="red" Visible = "False">
                      <HeaderTemplate > 
                            <asp:Label ID="Label3" runat="server" Text="Pending Requests" Font-Bold="true" ForeColor="Black" ></asp:Label>
                    
                      </HeaderTemplate>
                      <ContentTemplate>
                          <table border="3" width = "100%">
                            <tr>
                                <td colspan = "4">
                                      <asp:Table ID = "tblPendingQueues" runat = "server">
                                     </asp:Table>
            
                                </td>
        
                            </tr>
                           </table>
                       </ContentTemplate>
                  </ajaxToolkit:TabPanel>--%>

<%--                  <ajaxToolkit:TabPanel ID="tpRequestItems" runat="server" TabIndex="0"  BackColor="red" Visible = "False">
                      <HeaderTemplate > 
                            <asp:Label ID="Label6" runat="server" Text="Request Items" Font-Bold="true" ForeColor="Black" ></asp:Label>
                    
                      </HeaderTemplate>
                      <ContentTemplate>
                          <table border="3" width = "100%">
                            <tr>
                                <td colspan = "4">
                                      <asp:Table ID = "tblRequestItems" runat = "server">
                                     </asp:Table>
            
                                </td>
        
                            </tr>
                           </table>
                       </ContentTemplate>
                  </ajaxToolkit:TabPanel>--%>

<%--                  <ajaxToolkit:TabPanel ID="TPActionItems" runat="server" TabIndex="0"  BackColor="red" Visible = "False">
                      <HeaderTemplate > 
                            <asp:Label ID="Label7" runat="server" Text="Action Items" Font-Bold="true" ForeColor="Black" ></asp:Label>
                    
                      </HeaderTemplate>
                      <ContentTemplate>
                          <table border="3" width = "100%">
                            <tr>
                                <td colspan = "4">
                                      <asp:Table ID = "tblActionItems" runat = "server">
                                     </asp:Table>
            
                                </td>
        
                            </tr>
                           </table>
                       </ContentTemplate>
                  </ajaxToolkit:TabPanel>--%>

           </ajaxToolkit:TabContainer>
</td>
</tr>
</table>

  <asp:Panel ID="pnlOnBehalfOf" runat="server" 
        style="display:none; background-color:#efefef;  font-size:large;  width:600px;  border: 2px solid #FFFFCC;">
        <asp:UpdatePanel ID="uplOnBehalfOf" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table align="center" border="1px" width="90%">
                    <tr>
                        <td class="tableRowHeader" colspan="4">
                            Search for Requestor
                        </td>
                    </tr>
                    <tr>
                        <td>
                            First Name
                        </td>
                        <td>
                            <asp:TextBox ID="FirstNameTextBox" runat="server"></asp:TextBox>
                        </td>
                        <td>
                            Last Name
                        </td>
                        <td>
                            <asp:TextBox ID="LastNameTextBox" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <div class="scrollContentContainer">
                                <asp:UpdateProgress ID="UpdateProgress1" runat="server" 
                                    AssociatedUpdatePanelID="uplOnBehalfOf" DisplayAfter="200">
                                    <ProgressTemplate>
                                        <div ID="IMGDIV" runat="server" align="center" 
                                            style="visibility:visible;vertical-align:middle;" valign="middle">
                                            Loading Data ...
                                        </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                                <asp:GridView ID="gvSearch" runat="server" AllowSorting="True" 
                                    AutoGenerateColumns="False" BackColor="White" BorderColor="#336666" 
                                    BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                                    DataKeyNames="client_num, login_name" EmptyDataText="No Items Found" 
                                    EnableTheming="False" Font-Size="Medium" GridLines="Horizontal" Width="100%">
                                    <FooterStyle BackColor="White" ForeColor="#333333" />
                                    <HeaderStyle BackColor="#336666" Font-Bold="False" Font-Size="Small" 
                                        ForeColor="White" HorizontalAlign="Left" />
                                    <PagerStyle BackColor="#336666" Font-Size="Medium" ForeColor="White" 
                                        HorizontalAlign="Left" />
                                    <RowStyle BackColor="White" ForeColor="#333333" />
                                    <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                    <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                    <SortedAscendingHeaderStyle BackColor="#487575" />
                                    <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                    <SortedDescendingHeaderStyle BackColor="#275353" />
                                    <Columns>
                                        <asp:CommandField ButtonType="Button" SelectText="Select" 
                                            ShowSelectButton="true" />
                                        <asp:BoundField DataField="fullname" HeaderStyle-HorizontalAlign="Left" 
                                            HeaderText="Full Name" ItemStyle-HorizontalAlign="Left" />
                                        <asp:BoundField DataField="phone" HeaderStyle-HorizontalAlign="Left" 
                                            HeaderText="Phone" ItemStyle-HorizontalAlign="Left" />
                                        <asp:BoundField DataField="department_name" HeaderStyle-HorizontalAlign="Left" 
                                            HeaderText="Department" ItemStyle-HorizontalAlign="Left" />
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <table>
                                <tr>
                                    <td>
                                        <asp:Button ID="btnSearch" runat="server" Font-Bold="True" Font-Names="Arial" 
                                            Font-Size="Small" Height="20px" Text="Search" Width="125px" />
                                    </td>
                                    <td>
                                        <asp:Button ID="btnCloseOnBelafOf" runat="server" Font-Bold="True" 
                                            Font-Names="Arial" Font-Size="Small" Height="20px" Text="Close" Width="125px" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>

  <asp:Button ID="btnRequestorDummy" Style="display: none" runat="server" Text="Button" />

  <ajaxToolkit:ModalPopupExtender ID="mpeOnBehalfOf" runat="server"   
                  DynamicServicePath="" Enabled="True" TargetControlID="btnSelectOnBehalfOf"   
                PopupControlID="pnlOnBehalfOf" BackgroundCssClass="ModalBackground"   
                DropShadow="true" CancelControlID="btnCloseOnBelafOf">
                    </ajaxToolkit:ModalPopupExtender>  

  
</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>

