﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="false" CodeFile="TerminationReport.aspx.vb" Inherits="TerminationReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">

    <style type="text/css">
        .style12
        {
            height: 30px;
            width: 518px;
        }        
        .ui-datepicker-calendar { 
        display: none; 
        } 
        .style14
        {
            font-size: medium;
        }
        .style17
        {
            width: 418px;
        }
        .style18
        {
            width: 92px;
        }
        .style19
        {
            width: 164px;
        }
        .style20
        {
            width: 145px;
        }
</style> 


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

    <table>
        <tr>
              <td align="center">

                            <strong>Employee Termination Report </strong>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Label ID="LbMonthly" runat="server" Font-Size="Medium" ForeColor="Red"></asp:Label>
                       
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Label ID="lbNoRows" runat="server"  Font-Size="Medium" ForeColor="Red" Visible="false"></asp:Label>
            
            </td>
        
        </tr>

        <tr align="center">
            <td align="center">
                <table  id="tblControls" runat="server"  border="3" width="40%">
                    <tr align="center" >
                        <td align="right">
                            Select a Start Date:
                         </td>
                        <td class="style19" align="left">
                            <asp:TextBox ID="txtStartDate" Class="calandarClass" runat="server" 
                                Width="102px" ></asp:TextBox>
                        </td>

                    </tr>
                    <tr>
                        <td align="right">
                            Select a End Date:</td>
                        <td class="style19" align="left">
                           <asp:TextBox ID="txtEndDate"  Class="calanderClass" runat="server" Width="103px"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center">
                <table>
                    <tr>
                     <td align="right">
                        <asp:Label ID="typeLbl" runat="server" Font-Bold="True" Text="Select Affiliation:"></asp:Label>
                    </td>
                    <td align="left">
                              <asp:DropDownList ID = "Affiliationddl" runat="server"  Width="250px" >
                             </asp:DropDownList>
                             <asp:Label ID="empType" runat="server" Visible="false"></asp:Label>
                    </td>
                 </tr>
               </table>
            </td>
        </tr>
        <tr>
            <td  align="center">
               
               <table id="tblTypeAndReset">
                <tr>
                      <td>
                            <asp:Button ID="btnExcel" runat="server" Text="Excel" height="25px" Width="100px" />
                       </td> 

                       <td>
                             <asp:Button ID="btnUpdate" runat="server" Text="Submit" height="25px" Width="100px" />
                       
                       </td>
                </tr>
              </table>
           </td>
        </tr>
        <tr>
         
            <td align="center">
                <asp:Panel ID="totalpan" runat="server" Visible="false">
                    <table border="2px" width="50%">
                        <tr>
                            <td colspan="2" class="tableRowHeader" align="center">
                                Totals by Affiliation 
                            
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Total Count:
                            </td>
                            <td>
                                <asp:Label ID="totalcountlbl" runat="server">
                                </asp:Label>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                Unknow Affiliation Count:
                            </td>
                            <td>
                                <asp:Label ID="UnknowCountlbl" runat="server">
                                </asp:Label>
                            </td>
                        </tr>
                    
                        <tr>
                            <td>
                                Employee Count:
                            </td>
                            <td>
                                <asp:Label ID="EmployeeCountlbl" runat="server">
                                </asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Contract Count:
                            </td>
                            <td>
                                <asp:Label ID="ContractCountlbl" runat="server">
                                </asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Student Count:
                            </td>
                            <td>
                                <asp:Label ID="StudentCountlbl" runat="server">
                                </asp:Label>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                Physician Count:
                            </td>
                            <td>
                                <asp:Label ID="PhysicianCountlbl" runat="server">
                                </asp:Label>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                Nursing Count:
                            </td>
                            <td>
                                <asp:Label ID="NursingCountlbl" runat="server">
                                </asp:Label>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                Emp with No Number Count:
                            </td>
                            <td>
                                <asp:Label ID="EmpNoNumberCountlbl" runat="server">
                                </asp:Label>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                Non CKHN Count:
                            </td>
                            <td>
                                <asp:Label ID="NonCKHNCountlbl" runat="server">
                                </asp:Label>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                Resident Count:
                            </td>
                            <td>
                                <asp:Label ID="ResidentCountlbl" runat="server">
                                </asp:Label>
                            </td>
                        </tr>

                    </table>
                
               </asp:Panel>
            </td>
         </tr>         
         <tr>
            <td align="center">
                <asp:GridView ID="GridEmployees"  runat="server" 
                     BackColor="#FFFFFF" BorderColor="#FFFFFF"  DataKeyNames="receiver_client_num,AccountsActive"
                     BorderStyle="Double" BorderWidth="3px" CellPadding="2" 
                      EmptyDataText="No Data Found"  EnableTheming="False" 
                    AllowSorting="True"  EnableModelValidation="True"  Font-Size="Smaller"
                    AutoGenerateColumns="false" EditRowStyle-BorderStyle="Ridge" CellSpacing="2"
                     AlternatingRowStyle-BackColor="#CCCCCC" Font-Bold="True" HeaderStyle-CssClass="subheading"  Visible="True">
         

                    <FooterStyle BackColor="White" ForeColor="#333333" />

                    <HeaderStyle BackColor="#D7D5D5" Font-Bold="False" ForeColor="#060606" Font-Size="Small" HorizontalAlign="Left"    />                                    
                    <PagerStyle BackColor="#FFFFFF" ForeColor="#FFFFFF" HorizontalAlign="Left" Font-Size="Medium"/>
                    <RowStyle BackColor="#FFFFFF" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#D7D5D5" Font-Bold="True" ForeColor="#060606" />


                         <Columns>

<%--                                  <asp:CommandField ButtonType="Button" ControlStyle-BackColor="#84A3A3" 
                                      HeaderStyle-HorizontalAlign="Left" ItemStyle-Height="20px" SelectText="Select" 
                                      ShowSelectButton="true">
                                  <ControlStyle BackColor="#84A3A3" />
                                  <HeaderStyle HorizontalAlign="Left" />
                                  <ItemStyle Height="20px" />
                                  </asp:CommandField>--%>

                             <asp:CommandField ButtonType="Button" SelectText="Select" ShowSelectButton= "true" HeaderStyle-HorizontalAlign = "Left" 
                                 ControlStyle-BackColor="#84A3A3" ItemStyle-Height="20px" />

    						<asp:BoundField DataField="full_name" SortExpression="full_name" HeaderText="Employee Name"></asp:BoundField>
							<asp:BoundField DataField="department_name" SortExpression="department_name" HeaderText="Dept."></asp:BoundField>
							<asp:BoundField DataField="employee_num" SortExpression="employee_num" HeaderText="Employee# "></asp:BoundField>

							<asp:BoundField DataField="Term_date" SortExpression="Term_date" HeaderText="Term Date" ></asp:BoundField>
   							<asp:BoundField DataField="AccountsActive" SortExpression="AccountsActive" HeaderText="Active"></asp:BoundField>
							<asp:BoundField DataField="AffiliationDisplay" SortExpression="AffiliationDisplay" HeaderText="Emp Type" ></asp:BoundField>
   							<asp:BoundField DataField="Title" SortExpression="Title" HeaderText="Title" ></asp:BoundField>


                   </Columns> 
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td align="center">
            
                <asp:GridView ID="GridHidden" runat="server" 
                     BackColor="#FFFFFF" BorderColor="#FFFFFF" 
                     BorderStyle="Double" BorderWidth="3px" CellPadding="2" 
                      EmptyDataText="No Data Found"  EnableTheming="False" 
                    AllowSorting="True"  EnableModelValidation="True"  Font-Size="Smaller"
                    AutoGenerateColumns="false" EditRowStyle-BorderStyle="Ridge" CellSpacing="2"
                     AlternatingRowStyle-BackColor="#CCCCCC" Font-Bold="True" HeaderStyle-CssClass="subheading" Visible="false">
                      

                    <FooterStyle BackColor="White" ForeColor="#333333" />

                    <HeaderStyle BackColor="#D7D5D5" Font-Bold="False" ForeColor="#060606" Font-Size="Smaller" HorizontalAlign="Left"    />                                    
                    <PagerStyle BackColor="#FFFFFF" ForeColor="#FFFFFF" HorizontalAlign="Left" Font-Size="Medium"/>
                    <RowStyle BackColor="#FFFFFF" ForeColor="#333333" Font-Size="Smaller" />

                         <Columns>


    						<asp:BoundField DataField="full_name" SortExpression="full_name" HeaderText="Employee Name"></asp:BoundField>
							<asp:BoundField DataField="department_name" SortExpression="department_name" HeaderText="Dept."></asp:BoundField>
							<asp:BoundField DataField="employee_num" SortExpression="employee_num" HeaderText="Employee# "></asp:BoundField>

							<asp:BoundField DataField="Term_date" SortExpression="Term_date" HeaderText="Term Date" ></asp:BoundField>
   							<asp:BoundField DataField="StartDate" SortExpression="StartDate" HeaderText="StartDate"></asp:BoundField>
   							<asp:BoundField DataField="last_Day_worked" SortExpression="last_Day_worked" HeaderText="Last Day Worked"></asp:BoundField>

							<asp:BoundField DataField="AffiliationDisplay" SortExpression="AffiliationDisplay" HeaderText="Emp Type" ></asp:BoundField>

							<asp:BoundField DataField="department_cd" SortExpression="department_cd" HeaderText="Dept#" ></asp:BoundField>
							<asp:BoundField DataField="entity_cd" SortExpression="entity_cd" HeaderText="Entity" ></asp:BoundField>
							<asp:BoundField DataField="entity_desc" SortExpression="entity_desc" HeaderText="Entity Desc" ></asp:BoundField>
							<asp:BoundField DataField="requestor_name" SortExpression="requestor_name" HeaderText="Requestor Name" ></asp:BoundField>

							<asp:BoundField DataField="Title" SortExpression="Title" HeaderText="Title" ></asp:BoundField>
							<asp:BoundField DataField="PositionNum" SortExpression="PositionNum" HeaderText="Position#" ></asp:BoundField>

                            <asp:BoundField DataField="AccountsActive" SortExpression="AccountsActive" HeaderText="AccountsActive" ></asp:BoundField>
                            
							
                            <asp:BoundField DataField="TotalCount" SortExpression="TotalCount" HeaderText="TotalCount" ></asp:BoundField>


							<asp:BoundField DataField="UnknowCount" SortExpression="UnknowCount" HeaderText="UnknowCount" ></asp:BoundField>

							<asp:BoundField DataField="EmployeeCount" SortExpression="EmployeeCount" HeaderText="EmployeeCount" ></asp:BoundField>
							<asp:BoundField DataField="ContractCount" SortExpression="ContractCount" HeaderText="ContractCount" ></asp:BoundField>
							<asp:BoundField DataField="StudentCount" SortExpression="StudentCount" HeaderText="StudentCount" ></asp:BoundField>
							<asp:BoundField DataField="PhysicianCount" SortExpression="PhysicianCount" HeaderText="PhysicianCount" ></asp:BoundField>

							<asp:BoundField DataField="NursingCount" SortExpression="NursingCount" HeaderText="NursingCount" ></asp:BoundField>

							<asp:BoundField DataField="EmpNoNumberCount" SortExpression="EmpNoNumberCount" HeaderText="EmpNoNumberCount" ></asp:BoundField>
							<asp:BoundField DataField="NonCKHNCount" SortExpression="NonCKHNCount" HeaderText="NonCKHNCount" ></asp:BoundField>

							<asp:BoundField DataField="ResidentCount" SortExpression="ResidentCount" HeaderText="ResidentCount" ></asp:BoundField>


                   </Columns> 
                </asp:GridView>
            
            
            </td>
        </tr>
       <tr>
          <td >
            <asp:Label ID="HDStartDate" runat="server" Visible="false"></asp:Label>
            <asp:Label ID="HDEnddate" runat="server" Visible="false"></asp:Label>

        </td>
        </tr>
  </table>
    <script type="text/javascript">
        $(document).ready(function () {
            setJQCalander();
            // just commented $(".calanderClass").datepick({ showOn: 'both', buttonImage: 'Images/jQueryCalendar.gif' });
            //  alert("test select"); //only works first time,dateFormat: 'MM yyyy'
        });

        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);
        function setJQCalander() {
            //  $(".calanderClass").datepick({ showOn: 'both', buttonImage: 'Images/jQueryCalendar.gif' });

            $(".calanderClass").datepick({ onClose: function () {
                // __doPostBack('__Page', 'MyCustomArgument');
            },
                showOn: 'both', buttonImage: 'Images/jQueryCalendar.gif'
            });
            //            $('.date-picker').datepicker({
            //                changeMonth: true,
            //                changeYear: true,
            //                showButtonPanel: true,
            //                dateFormat: 'MM yy',
            //                onClose: function(dateText, inst) {
            //                    var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
            //                    var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
            //                    $(this).datepicker('setDate', new Date(year, month, 1));
            //                }
            //            }); 
        }
        function EndRequestHandler(sender, args) {
            //end of async postback
            // alert("end Async" );
            setJQCalander();


        }

        function BeginRequestHandler(sender, args) {

        }
    </script>
</asp:Content>

