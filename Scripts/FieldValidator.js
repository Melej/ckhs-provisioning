﻿

function fieldValidator(cssRequiredField, btnSubmit, lblMessage) {



    $('[id$=' + btnSubmit + ']').click(function (event) {

        $('input:text.' + cssRequiredField).each(function () {

            var text = $(this);

            if (text.val() == "") {


               

                $(this).focus();
                var attrAlt = $(this).attr("alt");
                $('[id$=' + lblMessage + ']').text(attrAlt + " is a required field.");
                event.preventDefault();
                return false;


            } else {

                $(this).removeClass(cssRequiredField);
            }


        });


    });

}