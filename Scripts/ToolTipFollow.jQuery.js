﻿
function AddToolTip() {

    $(document).ready(function () {
        // Tooltip only Text
        $('.masterTooltip').hover(function (event) {
            // Hover over code
            var title = $(this).attr('title');

            $(this).data('tipText', title).removeAttr('title');

            $('<div class="tooltipMove"></div>')
        .html(title)
        .appendTo('body')
        .css('top', (event.pageY + 10) + 'px')
        .css('left', (event.pageX + 20) + 'px')
        .fadeIn('slow');

            var tipWidth = $('.tooltipMove').width();
            var tipHeight = $('.tooltipMove').height();

            var tipX = $('.tooltipMove').offset().left;
            var tipY = $('.tooltipMove').offset().top;

            var winHeight = $(window).height();
            var winWidth = $(window).width();


            if ((tipY + tipHeight) > winHeight) {

                $('.tooltipMove')
                    .css('top', (tipY - tipHeight - 10) + 'px')
            }

            if ((tipX + tipWidth) > winWidth) {

                $('.tooltipMove')
                    .css('left', (tipX - tipWidth - 10) + 'px')
            }

        }, function () {
            // Hover out code
            $(this).attr('title', $(this).data('tipText'));
            $('.tooltipMove').remove();
        }).mousemove(function (e) {


            var tipWidth = $('.tooltipMove').width();
            var tipHeight = $('.tooltipMove').height();

          

            var winHeight = $(window).height();
            var winWidth = $(window).width();



            var mousex = e.pageX + 20; //Get X coordinates
            var mousey = e.pageY + 10; //Get Y coordinates

            if ((mousey + tipHeight) > winHeight) {

                mousey = e.pageY - tipHeight - 10
            }

            if ((mousex + tipWidth) > winWidth) {

                mousex = e.pageX - tipWidth - 10
            }


            $('.tooltipMove')
                 .css({ top: mousey, left: mousex })
        });
    });

}

