﻿(function($){


$.fn.center = function(){

var element = $(this);

changeCss();

function changeCss(){

var imageHeight = $(element).height();
var imageWidth = $(element).width();

var windowHeight = $(window).height();
var windowWidth = $(window).width();

$(element).css({
    "position" : "absolute", 
    "left" : windowWidth / 2 - imageWidth / 2,
    "top" : windowHeight / 2 - imageHeight / 2,
    
    });

};


};
});