﻿Imports System.IO
Imports System.Data '
Imports System.Data.SqlClient
Imports App_Code
Imports AjaxControlToolkit
Imports C1.Web.UI.Controls.C1Window

Partial Class LDAPValidateRequest
    Inherits MyBasePage '   Inherits System.Web.UI.Page


    Dim sActivitytype As String = ""
    Dim cblBinder As New CheckBoxListBinder
    Dim FormViewRequest As New FormSQL()
    Dim AccountRequestNum As Integer
    Dim AccountRequestType As String
    Dim dtRequestedAccounts As DataTable


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        FormViewRequest = New FormSQL(Session("EmployeeConn"), "SelAccountRequestV3", "ins_account_request", "", Page)
        AccountRequestNum = Request.QueryString("RequestNum")



        If Not IsPostBack Then

            FormViewRequest.AddSelectParm("@account_request_num", AccountRequestNum, SqlDbType.Int, 4)
            FormViewRequest.FillForm()


            bindAppRequests()
            AccountRequestType = FormViewRequest.getFieldValue("account_request_type")

            If AccountRequestType = "addnew" Or NTRequested() Then

                lblSuggestedUserName.Visible = True
                UserNameTextBox.Visible = True
                lblCustomNameTest.Visible = True
                CustomUserNameTextBox.Visible = True
                btnTestCustomUserName.Visible = True
                cbUseCustomName.Visible = True
                lblCustomUserName.Visible = True

                UserNameTextBox.Text = SuggestedUserName(FormViewRequest.getFieldValue("first_name"), FormViewRequest.getFieldValue("last_name"))

            Else

                lblSuggestedUserName.Visible = False
                UserNameTextBox.Visible = False
                lblCustomNameTest.Visible = False
                CustomUserNameTextBox.Visible = False
                btnTestCustomUserName.Visible = False
                cbUseCustomName.Visible = False
                lblCustomUserName.Visible = False
            End If



        End If

    End Sub

    Private Function NTRequested() As Boolean
        Dim blnReturn As Boolean = False



        For Each rw As GridViewRow In gvAppRequests.Rows
            If rw.RowType = DataControlRowType.DataRow Then

                Dim ApplicationNum As String = gvAppRequests.DataKeys(rw.RowIndex)("ApplicationNum")
                If ApplicationNum = 9 Then
                    blnReturn = True

                End If

            End If

        Next





        Return blnReturn
    End Function
    Protected Sub bindAppRequests()


        Dim thisData As New CommandsSqlAndOleDb("SelItemsByNumValidCd", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int)
        thisData.AddSqlProcParameter("@valid_cd", "NeedsValidation", SqlDbType.NVarChar, 20)


        dtRequestedAccounts = thisData.GetSqlDataTable

        gvAppRequests.DataSource = dtRequestedAccounts
        gvAppRequests.DataBind()





    End Sub

    Protected Sub gvAppRequests_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvAppRequests.RowCommand
        If e.CommandName = "Select" Then


        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("./MyAccountQueue.aspx")

    End Sub

    Protected Sub btnValidate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnValidate.Click




        Dim cmd As New CommandsSqlAndOleDb("UpdValidateRequestV20", Session("EmployeeConn"))
        cmd.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int, 6)
        cmd.AddSqlProcParameter("@nt_login", Session("LoginID"), SqlDbType.NVarChar, 20)
        cmd.AddSqlProcParameter("@validate", "Validated", SqlDbType.NVarChar, 20)





        cmd.ExecNonQueryNoReturn()





        If AccountRequestType <> "delacct" And AccountRequestType <> "terminate" Then


            Dim cmdUpdateAccount As New CommandsSqlAndOleDb("UpdAccountRequestV3", Session("EmployeeConn"))
            cmdUpdateAccount.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int, 8)

            cmdUpdateAccount.AddSqlProcParameter("@share_drive", share_driveTextBox.Text, SqlDbType.NVarChar, 20)
            cmdUpdateAccount.AddSqlProcParameter("@ModelAfter", ModelAfterTextBox.Text, SqlDbType.NVarChar, 100)

            If cbUseCustomName.Checked = True Then
                cmdUpdateAccount.AddSqlProcParameter("@login_name", CustomUserNameTextBox.Text, SqlDbType.NVarChar, 20)
            Else

                cmdUpdateAccount.AddSqlProcParameter("@login_name", UserNameTextBox.Text, SqlDbType.NVarChar, 20)
            End If


            cmdUpdateAccount.ExecNonQueryNoReturn()




        End If

        Response.Redirect("./MyAccountQueue.aspx")




    End Sub


    Protected Sub btnInvalid_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim btnInvalid As Button = sender
        Dim gvrCase As GridViewRow = CType(btnInvalid.NamingContainer, GridViewRow)
        Dim HiddenSeqNum As String = gvAppRequests.DataKeys(gvrCase.RowIndex).Values("account_request_seq_num")
        Dim HiddenLogin As String = IIf(IsDBNull(gvAppRequests.DataKeys(gvrCase.RowIndex).Values("login_name")), "", gvAppRequests.DataKeys(gvrCase.RowIndex).Values("login_name"))
        Dim InvalidRequestItem As String = gvAppRequests.Rows(gvrCase.RowIndex).Cells(1).Text



        lblInvalidRequestItem.Text = InvalidRequestItem
        lblHiddenSeqNum.Text = HiddenSeqNum
        lblHiddenLogin.Text = HiddenLogin



        mpeInvalidItem.Show()


    End Sub

    Protected Sub btnInvalidSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnInvalidSubmit.Click

        Dim account_request_seq_num As String = lblHiddenSeqNum.Text
        Dim login_name As String = lblHiddenLogin.Text


        Dim thisData As New CommandsSqlAndOleDb("UpdValidateRequestItems", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@account_request_seq_num", account_request_seq_num, SqlDbType.Int)
        thisData.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int)
        thisData.AddSqlProcParameter("@requestor_client_num", Session("ClientNum"), SqlDbType.NVarChar, 30)
        thisData.AddSqlProcParameter("@validate", "invalid", SqlDbType.NVarChar, 10)
        thisData.AddSqlProcParameter("@action_desc", txtInvalidComments.Text, SqlDbType.NVarChar, 255)


        thisData.ExecNonQueryNoReturn()

        bindAppRequests()

    End Sub

    Private Function SuggestedUserName(ByVal FirstName As String, ByVal LastName As String) As String
        Dim sReturn As String = ""


        Dim cmdSql As New SqlCommand
        Dim connSql As New SqlConnection(Session("EmployeeConn"))
        Dim drSql As SqlClient.SqlDataReader



        '   connSql.ConnectionString = _DataConn

        cmdSql.CommandText = "SelSuggestedUserNameLDAP"
        cmdSql.CommandType = CommandType.StoredProcedure
        cmdSql.Connection = connSql
        cmdSql.Parameters.Add("@FirstName", SqlDbType.NVarChar).Value = FirstName
        cmdSql.Parameters.Add("@LastName", SqlDbType.NVarChar).Value = LastName


        connSql.Open()

        Try




            drSql = cmdSql.ExecuteReader


            While drSql.Read()


                sReturn = IIf(IsDBNull(drSql("SuggestedName")), "", drSql("SuggestedName"))



            End While

        Catch ex As Exception
            Dim er As String = ex.ToString()

        End Try

        connSql.Close()
        Return sReturn
    End Function

    Protected Sub btnTestCustomUserName_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTestCustomUserName.Click

        lblCustomNameTest.Text = TestCustomUserName(CustomUserNameTextBox.Text)

        If lblCustomNameTest.Text = "Account Is Available" Then

            cbUseCustomName.Enabled = True
            cbUseCustomName.Checked = False
        Else

            cbUseCustomName.Enabled = False
            cbUseCustomName.Checked = False
        End If

    End Sub


    Private Function TestCustomUserName(ByVal UserName As String) As String
        Dim sReturn As String = ""


        Dim cmdSql As New SqlCommand
        Dim connSql As New SqlConnection(Session("EmployeeConn"))
        Dim drSql As SqlClient.SqlDataReader



        '   connSql.ConnectionString = _DataConn

        cmdSql.CommandText = "SelTestCustomUserName"
        cmdSql.CommandType = CommandType.StoredProcedure
        cmdSql.Connection = connSql
        cmdSql.Parameters.Add("@Login", SqlDbType.NVarChar).Value = UserName


        connSql.Open()

        If UserName.Length > 0 Then

            Try




                drSql = cmdSql.ExecuteReader


                While drSql.Read()


                    sReturn = IIf(IsDBNull(drSql("ReturnString")), "", drSql("ReturnString"))



                End While

            Catch ex As Exception
                Dim er As String = ex.ToString()

            End Try
        Else

            sReturn = "Must Provide Custom Username to check."

        End If


        connSql.Close()
        Return sReturn
    End Function

    Protected Sub CustomUserNameTextBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CustomUserNameTextBox.TextChanged
        lblCustomNameTest.Text = TestCustomUserName(CustomUserNameTextBox.Text)

        If lblCustomNameTest.Text = "Account Is Available" Then

            cbUseCustomName.Enabled = True
            cbUseCustomName.Checked = False
        Else

            cbUseCustomName.Enabled = False
            cbUseCustomName.Checked = False
        End If

    End Sub
End Class
