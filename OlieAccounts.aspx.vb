﻿Imports System.IO
Imports System.Data
Imports System.Configuration
Imports System.Data.SqlClient
Imports App_Code
Imports AjaxControlToolkit
Imports C1.Web.UI.Controls.C1Window

Partial Class OlieAccounts
    Inherits MyBasePage 'Inherits System.Web.UI.Page '

    Dim UserInfo As UserSecurity


    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        UserInfo = New UserSecurity(Session("LoginID"), Session("EmployeeConn"))

        If Session("SecurityLevelNum") < 70 Then
            Response.Redirect("~/CSCMaster.master.aspx", True)

        End If


        If Not IsPostBack Then

            Requestheadlabel.Text = "Accounts To Send to Olie"


            fillOlieTable()


        End If


        If lblReportType.Text = "" Then
            If Session("SecurityLevelNum") > 100 Then

                btnSendData.Visible = True
            End If
            If Session("SecurityLevelNum") = 95 Or Session("LoginID").ToString.ToLower = "forwoodb" Or Session("LoginID").ToString.ToLower = "burgina" Then
                btnSendData.Visible = True

            End If

        Else
            btnSendData.Visible = False

        End If

    End Sub
    Protected Sub fillOlieTable()
        Dim rpttype As String = lblReportType.Text
        Dim ProcedureName As String = ""

        Select Case rpttype
            Case "filestosend", "needsdata", "past"

                ProcedureName = "SelOlieAccountToSend"

            Case "deletaccts", "delneeds", "delSent"

                ProcedureName = "SelDeleteOlieAccountToSend"

            Case Else
                ProcedureName = "SelOlieAccountToSend"

        End Select

        Dim thisData As New CommandsSqlAndOleDb(ProcedureName, Session("EmployeeConn"))



        If lblReportType.Text <> "" Then
            thisData.AddSqlProcParameter("@reportType", lblReportType.Text, SqlDbType.NVarChar, 12)

        End If

        Dim iRowCount = 1

        Dim dt As DataTable
        dt = thisData.GetSqlDataTable

        Dim rwHeader As New TableRow
        Dim clSelectHeader As New TableCell
        Dim clNameHeader As New TableCell
        Dim clRequestLogin As New TableCell
        Dim clDocMaster As New TableCell
        Dim clDeptCdHeader As New TableCell

        Dim clNetAccessHeader As New TableCell
        Dim clUserTypeHeader As New TableCell

        Dim clTCLHeader As New TableCell
        Dim clEmpTypeHeader As New TableCell


        clNameHeader.Text = "  For  "
        clRequestLogin.Text = "  Account  "
        clDocMaster.Text = "  Doc#   "
        clDeptCdHeader.Text = " Dept Cd.  "

        clNetAccessHeader.Text = " Net Access "
        clUserTypeHeader.Text = "  User Type  "
        clTCLHeader.Text = "  TCL "
        clEmpTypeHeader.Text = " Type "

        rwHeader.Cells.Add(clSelectHeader)
        rwHeader.Cells.Add(clNameHeader)
        rwHeader.Cells.Add(clRequestLogin)
        rwHeader.Cells.Add(clDocMaster)
        rwHeader.Cells.Add(clDeptCdHeader)

        rwHeader.Cells.Add(clNetAccessHeader)
        rwHeader.Cells.Add(clUserTypeHeader)

        rwHeader.Cells.Add(clTCLHeader)
        rwHeader.Cells.Add(clEmpTypeHeader)

        rwHeader.Font.Bold = True
        rwHeader.BackColor = Color.FromName("#006666")
        rwHeader.ForeColor = Color.FromName("White")
        rwHeader.Height = Unit.Pixel(36)

        tblOlieAccounts.Rows.Add(rwHeader)

        For Each drRow As DataRow In dt.Rows

            'Dim btnSelect As New Button
            Dim rwData As New TableRow
            Dim btnSelect As New TableCell
            Dim clSelectData As New TableCell
            Dim clNameData As New TableCell
            Dim clRequestLoginData As New TableCell
            Dim clDocMasterData As New TableCell

            Dim clDeptCdData As New TableCell
            Dim clnetAccessData As New TableCell
            Dim clUserTypeData As New TableCell

            Dim clRequestCloseDateData As New TableCell
            Dim clTCLData As New TableCell
            Dim clEmptypeData As New TableCell


            btnSelect.Text = "<a href=""AccountRequestInfo2.aspx?RequestNum=" & drRow("account_request_num") & """ ><u><b> " & drRow("account_request_num") & "</b></u></a>"
            btnSelect.EnableTheming = False

            clNameData.Text = IIf(IsDBNull(drRow("fullName")), "", drRow("fullName"))
            clRequestLoginData.Text = IIf(IsDBNull(drRow("login_name")), "", drRow("login_name"))
            clDocMasterData.Text = IIf(IsDBNull(drRow("DoctorMasterNumber")), "", drRow("DoctorMasterNumber"))

            clDeptCdData.Text = IIf(IsDBNull(drRow("DeptCd")), "", drRow("DeptCd"))
            clnetAccessData.Text = (IIf(IsDBNull(drRow("NetAccess")), "", drRow("NetAccess")))
            clUserTypeData.Text = (IIf(IsDBNull(drRow("UserTypeCd")), "", drRow("UserTypeCd")))
            clTCLData.Text = (IIf(IsDBNull(drRow("TCL")), "", drRow("TCL")))
            clEmptypeData.Text = (IIf(IsDBNull(drRow("emp_type_cd")), "", drRow("emp_type_cd")))



            'rwData.Cells.Add(clSelectData)
            rwData.Cells.Add(btnSelect)
            rwData.Cells.Add(clNameData)
            rwData.Cells.Add(clRequestLoginData)
            rwData.Cells.Add(clDocMasterData)

            rwData.Cells.Add(clDeptCdData)
            rwData.Cells.Add(clnetAccessData)
            rwData.Cells.Add(clUserTypeData)
            rwData.Cells.Add(clTCLData)
            rwData.Cells.Add(clEmptypeData)


            If iRowCount > 0 Then

                rwData.BackColor = Color.Bisque

            Else

                rwData.BackColor = Color.LightGray

            End If

            tblOlieAccounts.Rows.Add(rwData)

            iRowCount = iRowCount * -1

        Next
        tblOlieAccounts.Width = New Unit("100%")
    End Sub
    Sub btnSelect_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim btnLinkView As Button = sender

        If btnLinkView.CommandName = "Select" Then




            Dim sArguments As String() = Split(btnLinkView.CommandArgument, ";")

            Response.Redirect("./AccountRequestInfo2.aspx?RequestNum=" & sArguments(0))

            '            Response.Redirect("./AccountRequestItems.aspx?RequestNum=" & sArguments(0))



        End If
    End Sub


    'Protected Sub btnPastSent_Click(sender As Object, e As System.EventArgs) Handles btnPastSent.Click
    '    lblReportType.Text = "past"
    '    Requestheadlabel.Text = "Accounts Already Sent to eCare"
    '    btnSendData.Visible = False

    '    fillOlieTable()
    'End Sub

    'Protected Sub btnNeeding_Click(sender As Object, e As System.EventArgs) Handles btnNeeding.Click
    '    lblReportType.Text = "needsdata"
    '    Requestheadlabel.Text = "Accounts Need Data to send to eCare"

    '    btnSendData.Visible = False

    '    fillOlieTable()

    'End Sub

    'Protected Sub btnWaitSend_Click(sender As Object, e As System.EventArgs) Handles btnWaitSend.Click
    '    lblReportType.Text = ""
    '    Requestheadlabel.Text = "Accounts Waiting to be sent to eCare"

    '    btnSendData.Visible = True

    '    fillOlieTable()

    'End Sub
    Protected Sub ExportToFile()
        Dim thisData As New CommandsSqlAndOleDb("SelOlieAccountToSend", Session("EmployeeConn"))

        If lblReportType.Text <> "" Then
            thisData.AddSqlProcParameter("@reportType", lblReportType.Text, SqlDbType.NVarChar, 12)

        End If

        Dim dt As DataTable
        dt = thisData.GetSqlDataTable

        'Build the Text file data.
        Dim txt As String = String.Empty

        For Each column As DataColumn In dt.Columns
            'Add the Header row for Text file.
            txt += column.ColumnName & vbTab & vbTab
        Next

        'Add new line.
        txt += vbCr & vbLf

        For Each row As DataRow In dt.Rows
            For Each column As DataColumn In dt.Columns
                'Add the Data rows.
                txt += row(column.ColumnName).ToString() & vbTab & vbTab
            Next

            'Add new line.
            txt += vbCr & vbLf
        Next

        'Download the Text file.
        Dim path As String = Server.MapPath("~/OlieFiles") 'get file 
        Dim file As System.IO.FileInfo = New System.IO.FileInfo(path)

        Response.Clear()
        Response.Buffer = True
        Response.AddHeader("content-disposition", "attachment;filename=SqlExport.txt")
        Response.Charset = ""
        Response.ContentType = "application/text"
        Response.Output.Write(txt)
        Response.WriteFile(file.FullName)

        Response.Flush()
        Response.End()
    End Sub
    Protected Sub btnSendData_Click(sender As Object, e As System.EventArgs) Handles btnSendData.Click
        Dim rpttype As String = lblReportType.Text
        Dim ProcedureName As String = ""

        Select Case rpttype
            Case "filestosend", "needsdata", "past"

                ProcedureName = "CreateOLIEAccountsV2"

            Case "deletaccts", "delneeds", "delSent"

                ProcedureName = "CreateDeleteOLIEAccountsV2"

            Case Else
                ProcedureName = "CreateOLIEAccountsV2"

        End Select


        Dim thisData As New CommandsSqlAndOleDb(ProcedureName, Session("EmployeeConn"))
        Try
            thisData.ExecNonQueryNoReturn()

        Catch ex As Exception
            Dim sError As String
            sError = ex.ToString()
        End Try

        lblReportType.Text = ""
        'ExportToFile()

        Response.Redirect("~/OlieAccounts.aspx", True)

    End Sub

    Protected Sub OlirFilerbl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles OlirFilerbl.SelectedIndexChanged
        Dim olitype As String = OlirFilerbl.SelectedValue.ToString

        Select Case olitype
            Case "filestosend"

                lblReportType.Text = ""
                Requestheadlabel.Text = "Accounts Waiting to be sent to eCare"
                btnSendData.Visible = True

                fillOlieTable()

            Case "needsdata"
                lblReportType.Text = "needsdata"
                Requestheadlabel.Text = "Accounts Need Data to send to eCare"
                btnSendData.Visible = False


                fillOlieTable()

            Case "past"
                lblReportType.Text = "past"
                Requestheadlabel.Text = "Accounts Already Sent to eCare"
                btnSendData.Visible = False



                fillOlieTable()

            Case "deletaccts"

                lblReportType.Text = "deletaccts"
                Requestheadlabel.Text = "Delete Accounts Waiting to be sent to eCare"
                btnSendData.Visible = True


                fillOlieTable()

            Case "delneeds"
                lblReportType.Text = "delneeds"
                Requestheadlabel.Text = "Delete Accounts Need Data to send to eCare"
                btnSendData.Visible = False



                fillOlieTable()

            Case "delSent"
                lblReportType.Text = "delSent"
                Requestheadlabel.Text = "Delete Accounts Already Sent to eCare"
                btnSendData.Visible = False

                fillOlieTable()

        End Select

        'If OlirFilerbl.SelectedValue = "filestosend" Then
        '    lblReportType.Text = ""
        '    Requestheadlabel.Text = "Accounts Waiting to be sent to eCare"
        '    btnSendData.Visible = True

        '    fillOlieTable()
        'ElseIf OlirFilerbl.SelectedValue = "needsdata" Then
        '    lblReportType.Text = "needsdata"
        '    Requestheadlabel.Text = "Accounts Need Data to send to eCare"
        '    btnSendData.Visible = False

        '    fillOlieTable()

        'ElseIf OlirFilerbl.SelectedValue = "past" Then
        '    lblReportType.Text = "past"
        '    Requestheadlabel.Text = "Accounts Already Sent to eCare"
        '    btnSendData.Visible = False

        '    fillOlieTable()
        'End If

    End Sub
End Class
