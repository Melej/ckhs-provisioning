﻿Imports System.IO
Imports System.Data '
Imports System.Data.SqlClient
Imports App_Code
Imports System.Net
Imports System.Reflection
Imports System.Net.Mail
Imports System.Net.Mail.SmtpClient

Partial Class PrintRequestv8
    Inherits System.Web.UI.Page
    Dim FormSignOn As New FormSQL
    Dim StatusForm As New FormSQL

    Dim UserInfo As UserSecurity
	Dim RequestTickect As RequestTicket


	Dim RequestNum As Integer
	Dim iClientNum As Integer
	Dim iSecurityLevel As Integer
    Dim iTechNum As Integer

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        UserInfo = Session("objUserSecurity")
        RequestNum = Request.QueryString("RequestNum")
        nt_login.Text = Session("LoginID")


        WhoclientNumLabel.Text = UserInfo.ClientNum
        entryclientnumLabel.Text = UserInfo.ClientNum
        iTechNum = UserInfo.TechNum
        entrytechnumLabel.Text = UserInfo.TechNum
        SecurityLevelLabel.Text = UserInfo.SecurityLevelNum



		'RequestNum = 220478


		RequestTickect = New RequestTicket(RequestNum, Session("CSCConn"))

		requestnumlabel.Text = RequestNum




		FormSignOn = New FormSQL(Session("CSCConn"), "selTicketRFSByNumV7", "UpdTicketRFSObjectV8", "", Page)
        FormSignOn.AddSelectParm("@requestnum", RequestNum, SqlDbType.Int, 6)

        StatusForm = New FormSQL(Session("CSCConn"), "SelCurrentStatus", "UpdTicketRFSObjectV8", "", Page)
        StatusForm.AddSelectParm("@request_num", RequestNum, SqlDbType.Int, 6)

        If Not IsPostBack Then

            FormSignOn.FillForm()
            StatusForm.FillForm()


            categorycdLabel.Text = RequestTickect.categorycd
            typecdLabel.Text = RequestTickect.typecd
            itemcdLabel.Text = RequestTickect.itemcd
            clientnumLabel.Text = RequestTickect.clientnum
            currenttechnumLabel.Text = RequestTickect.currenttechnum
            currentgroupnumLabel.Text = RequestTickect.currentgroupnum
            'completiondateTextBox.Text = RequestTickect.completiondate
            'opentimelb.Text = RequestTickect.TotalAcknowledgeTime
            hdPriorityLb.Text = RequestTickect.priority
            hdexists.Text = RequestTickect.Surveyexists
            RequestTypeLabel.Text = RequestTickect.requesttype

            Dim status As String

            status = RequestTickect.currentacktime


            Dim ddlBinder As New DropDownListBinder
            Dim ddlTypeBinder As New DropDownListBinder

            ddlBinder.BindData(entitycdDDL, "selEntityCodesV4", "entity_cd", "hosp_desc", Session("EmployeeConn"))




            If entity_cdLabel.Text = "" Then
                entitycdDDL.SelectedIndex = -1
            Else
                Dim entitycd As String = entity_cdLabel.Text

                entitycd = entitycd.Trim
                entitycdDDL.SelectedValue = entitycd

            End If


            Dim ddbDepartmentBinder As New DropDownListBinder(departmentcdddl, "selDepartmentCodesV4", Session("EmployeeConn"))
            ddbDepartmentBinder.AddDDLCriteria("@entity_cd", entitycdDDL.SelectedValue, SqlDbType.NVarChar)
            ddbDepartmentBinder.BindData("department_cd", "department_name")

            If department_cdLabel.Text = "" Then
                departmentcdddl.SelectedIndex = -1
            ElseIf department_cdLabel.Text <> "" Then
                Try

                    departmentcdddl.SelectedValue = department_cdLabel.Text.Trim
                Catch ex As Exception

                    departmentcdddl.SelectedIndex = -1
                End Try

            End If

            ddlBinder.BindData(facilitycdddl, "SelFacilityCodes", "facility_cd", "facility_short_name", Session("EmployeeConn"))


            If facility_cdLabel.Text = "" Then
                facilitycdddl.SelectedIndex = -1
            ElseIf facility_cdLabel.Text <> "" Then
                Try
                    facilitycdddl.SelectedValue = facility_cdLabel.Text

                Catch ex As Exception
                    facilitycdddl.SelectedIndex = -1

                End Try
            End If


            If building_cdLabel.Text = "" Then
                buildingcdddl.SelectedIndex = -1
            ElseIf building_cdLabel.Text <> "" Then

                Try

                    ddlBinder = New DropDownListBinder(buildingcdddl, "SelBuildingByFacility", Session("EmployeeConn"))
                    ddlBinder.AddDDLCriteria("@facility_cd", facility_cdLabel.Text, SqlDbType.NVarChar)
                    ddlBinder.BindData("building_cd", "building_name")

                    buildingcdddl.SelectedValue = building_cdLabel.Text.Trim

                Catch ex As Exception

                    buildingcdddl.SelectedIndex = -1

                End Try



            End If


            If building_cdLabel.Text <> "" And facility_cdLabel.Text <> "" And floorLabel.Text <> "" Then

                Try
                    Dim ddlFloorBinder As DropDownListBinder

                    ddlFloorBinder = New DropDownListBinder(Floorddl, "SelFloorsByBuilding", Session("EmployeeConn"))
                    ddlFloorBinder.AddDDLCriteria("@facility_cd", facility_cdLabel.Text, SqlDbType.NVarChar)
                    ddlFloorBinder.AddDDLCriteria("@building_cd", building_cdLabel.Text, SqlDbType.NVarChar)

                    ddlFloorBinder.BindData("floor_no", "floor_no")

                    Floorddl.SelectedValue = floorLabel.Text.TrimEnd


                Catch ex As Exception
                    Floorddl.SelectedValue = -1
                End Try
            Else
                Floorddl.SelectedValue = -1

            End If

            ddlBinder = New DropDownListBinder(categorycdddl, "SelCategoryCodes", Session("CSCConn"))
            ddlBinder.AddDDLCriteria("@securitylevel", SecurityLevelLabel.Text, SqlDbType.SmallInt)
            ddlBinder.BindData("category_cd", "Category_Desc")

            If categorycdLabel.Text <> "" Then
                Try
                    categorycdddl.SelectedValue = categorycdLabel.Text.Trim
                Catch ex As Exception
                    categorycdddl.SelectedIndex = -1
                End Try

            ElseIf categorycdLabel.Text = "" Then
                categorycdLabel.Text = "other"
                categorycdddl.SelectedValue = categorycdLabel.Text.Trim

            End If



            ddlTypeBinder = New DropDownListBinder(typecdddl, "SelCategoryType", Session("CSCConn"))
            ddlTypeBinder.AddDDLCriteria("@Category_cd", categorycdLabel.Text, SqlDbType.NVarChar)


            ddlTypeBinder.BindData("type_cd", "CatagoryTypeDesc")
            If typecdLabel.Text <> "" Then
                Try
                    typecdddl.SelectedValue = typecdLabel.Text.Trim.ToLower
                Catch ex As Exception
                    typecdddl.SelectedIndex = -1
                End Try
            ElseIf typecdLabel.Text = "" Then
                typecdLabel.Text = "other"
                typecdddl.SelectedValue = typecdLabel.Text.Trim
            End If

            fillActivityTable()

            If RequestTypeLabel.Text.Trim <> "" Then
                RequestTyperbl.SelectedValue = RequestTypeLabel.Text.Trim

                If RequestTyperbl.SelectedValue = "ticket" Then

                    ticketPnl.Visible = True
                    rfsPnl.Visible = False


                ElseIf RequestTyperbl.SelectedValue = "rfs" Or RequestTyperbl.SelectedValue = "service" Then
                    rfsPnl.Visible = True
                    ticketPnl.Visible = False

                    'priorityrbl.Enabled = False

                    'completiondateTextBox.BackColor() = Color.Yellow
                    'completiondateTextBox.BorderColor() = Color.Black
                    'completiondateTextBox.BorderStyle() = BorderStyle.Solid
                    'completiondateTextBox.BorderWidth = Unit.Pixel(2)

                    'servicedescTextbox.BackColor() = Color.Yellow
                    'servicedescTextbox.BorderColor() = Color.Black
                    'servicedescTextbox.BorderStyle() = BorderStyle.Solid
                    'servicedescTextbox.BorderWidth = Unit.Pixel(2)

                    'businessdesTextBox.BackColor() = Color.Yellow
                    'businessdesTextBox.BorderColor() = Color.Black
                    'businessdesTextBox.BorderStyle() = BorderStyle.Solid
                    'businessdesTextBox.BorderWidth = Unit.Pixel(2)


                    'TechPanl.Visible = True


                End If


            End If

        End If

    End Sub
    Protected Sub fillActivityTable()


        tblActivity.Dispose()
        tblActivity.Rows.Clear()


        Dim thisData As New CommandsSqlAndOleDb("SelRequestActivitiesV7", Session("CSCConn"))
        thisData.AddSqlProcParameter("@request_num", RequestNum, SqlDbType.NVarChar)

        Dim iRowCount = 1

        Dim dt As DataTable

        dt = thisData.GetSqlDataTable

        Dim rwHeader As New TableRow

        Dim clAccountHeader As New TableCell
        Dim clEntryDatepeHeader As New TableCell
        Dim clStatusHeader As New TableCell
        Dim clpriorityheader As New TableCell
        Dim clDescHeader As New TableCell
        Dim clTechnameheader As New TableCell




        clAccountHeader.Text = "Activity#"
        clEntryDatepeHeader.Text = "Entry Date"
        clStatusHeader.Text = "Status"
        clpriorityheader.Text = "Priority"
        clTechnameheader.text = "Tech"
        clDescHeader.Text = "Desc"


        rwHeader.Cells.Add(clAccountHeader)
        rwHeader.Cells.Add(clEntryDatepeHeader)
        rwHeader.Cells.Add(clStatusHeader)
        rwHeader.Cells.Add(clpriorityheader)
        rwHeader.Cells.Add(clTechnameheader)
        rwHeader.Cells.Add(clDescHeader)


        rwHeader.Font.Bold = True
        rwHeader.BackColor = Color.FromName("#006666")
        rwHeader.ForeColor = Color.FromName("White")
        rwHeader.Height = Unit.Pixel(36)

        tblActivity.Rows.Add(rwHeader)

        For Each drRow As DataRow In dt.Rows


            Dim rwData As New TableRow

            Dim clAccountData As New TableCell
            Dim clEntryDateData As New TableCell
            Dim clStatusData As New TableCell
            Dim clPriorityData As New TableCell
            Dim clDescData As New TableCell
            Dim clTechname As New TableCell


            'Dim clReqPhoneData As New TableCell




            clAccountData.Text = IIf(IsDBNull(drRow("request_seq_num")), "", drRow("request_seq_num"))
            clEntryDateData.Text = IIf(IsDBNull(drRow("last_change_date")), "", drRow("last_change_date"))
            clStatusData.Text = IIf(IsDBNull(drRow("status_desc")), "", drRow("status_desc"))
            clPriorityData.Text = IIf(IsDBNull(drRow("current_priority")), "", drRow("current_priority"))
            clDescData.Text = IIf(IsDBNull(drRow("current_activity")), "", drRow("current_activity"))
            clTechname.Text = IIf(IsDBNull(drRow("tech_name")), "", drRow("tech_name"))

            clTechname.Width = Unit.Percentage(10)



            rwData.Cells.Add(clAccountData)
            rwData.Cells.Add(clEntryDateData)
            rwData.Cells.Add(clStatusData)
            rwData.Cells.Add(clPriorityData)
            rwData.Cells.Add(clTechname)

            rwData.Cells.Add(clDescData)



            If iRowCount > 0 Then

                rwData.BackColor = Color.Bisque

            Else

                rwData.BackColor = Color.LightGray

            End If
            Dim thisrowcount As Int16

            thisrowcount = tblActivity.Rows.Count()

            tblActivity.Rows.Add(rwData)

            iRowCount = iRowCount * -1

        Next
        tblActivity.CellPadding = 5
        tblActivity.Width = New Unit("100%")


    End Sub
    'Protected Sub btnPrint_Click(sender As Object, e As System.EventArgs) Handles btnPrint.Click
    '    Response.Redirect("./print_request.asp?id=" & RequestNum, True)

    'End Sub
End Class
