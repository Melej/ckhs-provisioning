﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="false" CodeFile="CredentialQueue.aspx.vb" Inherits="CredentialQueue" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
<table>
<tr>
    <td>
        <asp:Button runat="server" id="BtnExcel" Text="Excel" />
    </td>
</tr>

</table>
<asp:UpdatePanel ID="uplCredQueue" runat ="server" >

<Triggers>

</Triggers>
<ContentTemplate>
    <table style="border-style: groove" align="left" width="100%" cellpadding="5px">
        <tr>
           <td class="tableRowHeader">
               Privileged Queue (Waiting for Privileged Date)
           </td>
        </tr>
        <tr >
                <td>
                        <asp:Table ID = "tblCredentialQueue" runat = "server" Width="100%" Font-Size="16px">


                        </asp:Table>
                </td>
        </tr>
        <tr>
            <td>
              <%--   <asp:GridView ID="GvProviders" runat="server" AllowSorting="True" 
                           AutoGenerateColumns="False" BackColor="White" BorderColor="#336666" 
                           BorderStyle="Double" BorderWidth="3px" CellPadding="3"
                           DataKeyNames="account_request_num" 
                           EmptyDataText="No Accounts found "  
                             EnableTheming="False" Font-Size="Medium" GridLines="Horizontal" Width="100%" CellSpacing="3">

                           <FooterStyle BackColor="White" ForeColor="#333333" />
                           <HeaderStyle BackColor="#336666" Font-Bold="False" Font-Size="Large" 
                               ForeColor="White" HorizontalAlign="Left" VerticalAlign="Middle" />
                           <PagerStyle BackColor="#336666" Font-Size="Medium" ForeColor="White" 
                               HorizontalAlign="Left" />
                           <RowStyle BackColor="Bisque" ForeColor="#333333" Font-Size="Large" />

                            <AlternatingRowStyle BackColor="Gainsboro" ForeColor="#333333" Font-Size="Large" />

                           <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />

                           <SortedAscendingCellStyle BackColor="#F7F7F7" />
                           <SortedAscendingHeaderStyle BackColor="#487575" />
                           <SortedDescendingCellStyle BackColor="#E5E5E5" />
                           <SortedDescendingHeaderStyle BackColor="#275353" />
                           <Columns>

                               <asp:CommandField ButtonType="Button" ControlStyle-BackColor="#84A3A3" 
                                      HeaderStyle-HorizontalAlign="Left" ItemStyle-Height="20px" SelectText="Select" 
                                      ShowSelectButton="true">
                                  <ControlStyle BackColor="#84A3A3" />
                                  <HeaderStyle HorizontalAlign="Left" />
                                  <ItemStyle Height="20px" />
                                  </asp:CommandField>
                            
                               

                               <asp:BoundField DataField="receiver_full_name" ItemStyle-HorizontalAlign="Left" HeaderText="Name " />

                               <asp:BoundField DataField="account_request_type" ItemStyle-HorizontalAlign="Left" HeaderText="Request Type" />

                               <asp:BoundField DataField="requestor_name" ItemStyle-HorizontalAlign="Left" HeaderText="Requestor" />

                               <asp:BoundField DataField="requestor_phone"  ItemStyle-HorizontalAlign="Left" HeaderText="Req Phone" />

                               <asp:BoundField DataField="entered_date"  ItemStyle-HorizontalAlign="Left" HeaderText="Submit Date" />

                           <asp:BoundField DataField="FirstName"  ItemStyle-HorizontalAlign="Left"  Visible="false" />
                               <asp:BoundField DataField="lastName"  ItemStyle-HorizontalAlign="Left"  Visible="false" />
                               <asp:BoundField DataField="StartDate"  ItemStyle-HorizontalAlign="Left"  Visible="false" />
                               <asp:BoundField DataField="AccountType"  ItemStyle-HorizontalAlign="Left"  Visible="false" />
                  

                           </Columns>
                       </asp:GridView>   --%> 

            </td>
        </tr>
    </table>
</ContentTemplate>

</asp:UpdatePanel>
</asp:Content>

