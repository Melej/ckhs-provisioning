﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="false" CodeFile="OlieAccounts.aspx.vb" Inherits="OlieAccounts" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <style type="text/css">
        .style13
    {
        height: 30px;
    }
    </style>

   
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
<asp:UpdatePanel ID="upnlMyrequest" runat ="server" UpdateMode="Conditional" >

<Triggers>


</Triggers>

<ContentTemplate>

<div>

<table style="border-style: groove;" width="100%" align="left">
<tr>
    <td  class="tableRowHeader">
        <asp:Label ID="Requestheadlabel" runat="server" />
    </td>
</tr>

<tr>
   <td colspan="2" >
    <div id ="divQueueOptions" style="text-align:center" >
         <table width="100%">

            <tr>
                <td align="right" colspan="1">
                    <asp:Label ID="OlirViewlbl" runat="server" Text=" Select View " />
                </td>
                <td align="left" colspan="3">
                    <asp:RadioButtonList ID="OlirFilerbl" runat="server" AutoPostBack="true" RepeatDirection="Horizontal" Font-Size="Small" >
                        <asp:ListItem  Selected="True" Value="filestosend" Text="New Accounts Waiting" />
                        <asp:ListItem Value="past" Text="New Accounts Sent" />
                        <asp:ListItem Value="needsdata" Text="New Accounts Needing Data" />
                        <asp:ListItem Value="deletaccts" Text="Delete Accounts Waiting" />
                        <asp:ListItem Value="delSent" Text="Delete Accounts Sent" />
                       <%-- <asp:ListItem Value="delneeds" Text="Delete Accounts Needing Data" />--%>

                  </asp:RadioButtonList>                
                </td>
            </tr>

            <tr>

                <td>
                 <asp:Button runat="server" ID="btnWaitSend" Text = "Accounts Waiting to Send"   Visible="false"
                  CssClass="btnhov" BackColor="#006666"  Width="200px" />
                </td>
                <td>
                 <asp:Button runat="server" ID="btnPastSent" Text = "Accounts Already Sent"   Visible="false"
                 CssClass="btnhov" BackColor="#006666"  Width="200px" />
                </td>
                <td>
                 <asp:Button runat="server" ID="btnNeeding" Text = "Accounts Needing Data"  Visible="false"
                 CssClass="btnhov" BackColor="#006666"  Width="200px" />
                </td>
                <td>
                    <asp:Label id="lblReportType" runat="server" Visible="false"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="4">
                 <asp:Button runat="server" ID="btnSendData" Text = "Send Accounts To eCare "  Visible="false"
                 CssClass="btnhov" BackColor="#006666"  Width="200px" />
                    
                </td>
            </tr>

         </table>
       <hr/>
    </div>
  </td>
 </tr>
    <tr >
        <td>

            <asp:Table  Width="100%" ID = "tblOlieAccounts" runat = "server">
            </asp:Table>
        </td>
    </tr>

</table>
</div>

</ContentTemplate>

</asp:UpdatePanel>
</asp:Content>

