﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="false" CodeFile="OpenProviderRequests.aspx.vb" Inherits="OpenProviderRequests" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

<table>
<tr>
    <td>
        <asp:Button runat="server" id="BtnExcel" Text="Excel" 
         CssClass="btnhov" BackColor="#006666"  Width="145px" /> />
    </td>
</tr>

</table>

<asp:UpdatePanel ID="uplSignOnForm" runat ="server" >

<Triggers>



</Triggers>

<ContentTemplate>
<table style="border-style: groove" align="left">
    <tr>
       <td class="tableRowHeader">
          Open Provider Requests ( Waiting for Accounts to be Created)
       </td>
    </tr>
    <tr>
        <td align="center">
          <table>
            <tr>
                <td align="left">
                    <asp:Label ID="Rowslbl" Font-Bold="true"  runat="server" Text="Rows:" Font-Size="Medium"></asp:Label>
                    <asp:Label  id="rowsreturnlbl" runat="server" Font-Size="Medium" Font-Bold="True"></asp:Label>
                            
                </td>            
                <td>
                    <asp:Button ID="btnOldOpenPrvd" runat="server" Text = "View 30 Day Old Request" 
                     CssClass="btnhov" BackColor="#006666"  Width="245px" />
                
                </td>
                <td>
                
                </td>
            
            </tr>
          </table>

        </td>
    </tr>
    <tr >
        <td>

            <asp:Table ID = "tblProviderQueue" runat = "server" CellPadding="2" CellSpacing="2">
            </asp:Table>
        </td>
    </tr>

</table>

</ContentTemplate>


        </asp:UpdatePanel>


</asp:Content>

