﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="false" CodeFile="CPMLocations.aspx.vb" Inherits="CPMLocations" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <script type="text/javascript" src="Scripts/FieldValidator.js"></script>
<script type="text/javascript">
    function pageLoad(sender, args) {

        fieldValidator('textInputRequired', 'btnSubmit', 'lblValidation');

    }
</script>
    <style type="text/css">
        .style2
        {
            text-align: center;

        }
        .style33
    {
        text-align: center;
        background-color: #FFFFCC;
        color: #336666;
        font-weight: bold;
        font-size: x-large;
        height: 41px;
    }

    </style>
   

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:UpdatePanel ID="CPMForm" runat="server" UpdateMode="Conditional" >
    <ContentTemplate>

   <table width = "100%" >
     <tr align="center" valign="middle">
         <td align="center" valign="middle">
         </td>
     </tr>

        <tr class="tableRowHeader">
                 <td>
                     CPM Office Location Form
                 </td>
        </tr>

        <tr>
            <td align="center">
              <table>
                <tr>
                    <td colspan = "4" align="center">
                        <asp:Label ID="thisenviro" runat="server" Visible="false" />
                    
                    </td>
                </tr>
                <tr>
                   <td align="right">
                      <asp:Label ID="lblusersubmit" runat="server" Text="User Submitting:" Font-Size="Small" Width="125px" />
                   </td>

                   <td align="left">
                        <asp:Label ID="userSubmittingLabel" Width="150px" runat="server" Font-Size="Small" Font-Bold="true"></asp:Label>
                        <asp:Label ID="submitter_client_numLabel" runat="server" visible="false" />
                    </td>
                   <td align="right">
                        <asp:Label ID="lblcurrRequesto" runat="server" Width="200px" Font-Size="Small" Text="Current Auth.Mgr./Delegate:" />
                  </td>
                  <td align="left">
                        <asp:Label ID="SubmittingOnBehalfOfNameLabel" runat="server" Font-Size="Small" Font-Bold="true"></asp:Label>
                        <asp:Label ID="requestor_client_numLabel" runat="server" visible="false"></asp:Label>
                  </td>
                </tr>
              </table>

              <table>
                <tr align="center">
                    <td>
                        <asp:Button ID="btnSubmit" runat="server" Font-Bold="True" Font-Names="Arial" 
                            Font-Size="Small" Height="20px" Text="Submit" Width="140px" />
                    </td>
                    <td>
                        <asp:Button ID="btnSelectOnBehalfOf" runat="server" Font-Bold="True" 
                            Font-Names="Arial" Font-Size="Small" Height="20px" Text="Change Auth.Mgr./Delegate:" 
                            Width="200px" Visible="false" />
                    </td>
                    <td>
                        <asp:Button ID="btnReturn" runat="server" Font-Bold="True" Font-Names="Arial" 
                            Font-Size="Small" Height="20px" Text="Return" Width="140px" />
                    </td>

                    <td>
                        <asp:Button ID="btnUpdateSubmitter" runat="server" Font-Bold="True" 
                            Font-Names="Arial" Font-Size="Small" Height="20px" Text="Update Demographics" 
                            Visible="false" Width="140px"/>
                    </td>
                </tr>
              </table>
           </td>
        </tr>

        <tr>
           <td align="left">

             <ajaxToolkit:TabContainer ID="tabs" runat="server" ActiveTabIndex="0"
                Width="100%"  EnableTheming="False" 
                BackColor="#efefef">

                  <ajaxToolkit:TabPanel ID="tpDemographics" runat="server"  BackColor="Gainsboro" Visible = "True" EnableTheming="False">
                     <HeaderTemplate > 
                            <asp:Label ID="Label1" runat="server" Text="Office Information" Font-Bold="True" 
                                ForeColor="Black"></asp:Label>
                     </HeaderTemplate>
                     <ContentTemplate>

                  <table id="tblOfficedemo" border="0" style="width: 100%">
                      <tr>
                          <td class="tableRowSubHeader"  align="left" colspan="4">
                              Office Details 
                          </td>
                      </tr>
                     <tr>
                            <td align ="center" colspan="4">
                            <asp:Label ID = "lblValidation" runat = "server" ForeColor="Red" Font-Size="12px" Font-Bold="True" />
                            </td>
            
                        </tr>


                       <tr>
                            <td align="right">
                                Location Type:
                            </td>
                            <td align="left">
                                <asp:RadioButtonList ID="LocationTypeCdrbl"  runat="server"  RepeatDirection="Horizontal">
                                      <asp:ListItem Value="office">Office</asp:ListItem>
                                      <asp:ListItem Value="location">Location of Service</asp:ListItem>
                                  </asp:RadioButtonList>
                            </td>

                       </tr>
                      <tr>
                        
                        <td align="right">
                            Name:
                        </td>
                        <td align="left" colspan="3">
                            <asp:TextBox ID="Group_nameTextBox" runat="server" width="500px" ></asp:TextBox>
                        </td>

                            
                      </tr>

                        <tr>

                        <td align="right">
                            Credentialed Name:
                        </td>
                        <td align="left" colspan="3">
                            <asp:TextBox ID="CredentialedNameTextBox" runat="server" width="500px"></asp:TextBox>
                        </td>
                        </tr>
                        <tr>
                            <td colspan="4"><br /></td>
                        </tr>
                        <tr>
                        
                            <td align="right">
                                GNumber:
                            </td>
                            <td align="left">
                                <asp:TextBox ID="GroupIDTextBox" runat="server" ></asp:TextBox>
                            </td>                        
                            <td align="right">
                                Taxonomy:
                            </td>
                            <td align="left">
                                <asp:TextBox ID="taxonomyTextBox" runat="server"></asp:TextBox>
                            </td>

                        </tr>
                        <tr>
                            <td align="right">
                               Group NPI:
                            </td>
                            <td align="left">
                                <asp:TextBox ID="GroupNPITextBox" runat="server" MaxLength="10"></asp:TextBox>
                            </td>
                        
                            <td align="right">
                                Location NPI:
                            </td>
                            <td align="left">
                                
                                <asp:TextBox ID="OfficeNPITextBox" runat="server" MaxLength="10"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                           <td align="right">
                              CostCenter:
                          </td>
                          <td align="left">
                              <asp:TextBox ID="CostCenterTextbox" runat="server" MaxLength="12"></asp:TextBox>
                          </td>


                              <td align="right">
                                  Specialty:
                              </td>
                              <td align="left">
                                  <asp:DropDownList ID="Specialtyddl" runat="server" >
                                  </asp:DropDownList>
                                  <asp:Label ID ="SpecialtLabel" runat="server" Visible="False"></asp:Label>

                              </td>
                          </tr>
                        <tr>
                            <td colspan="4"><br /></td>
                        </tr>

                      <tr>
                          <td align="right">
                              Address 1:
                          </td>
                          <td align="left" colspan="3">
                              <asp:TextBox ID="Address1Textbox" runat="server" Width="350px"></asp:TextBox>

                          </td>
                        </tr>
                        <tr>
                          <td align="right">
                              Address 2:
                          </td>
                          <td align="left" colspan="3">
                              <asp:TextBox ID="Address2textbox" runat="server" Width="350px"></asp:TextBox>
                          </td>
                      </tr>
                      <tr>
                          <td align="right">
                              <asp:Label ID="lblcity" runat="server" text="City:" width="50px" />
                          </td>
                          <td align="left" colspan="3">
                              <asp:TextBox ID="Citytextbox" runat="server" width="500px"></asp:TextBox>
                          </td>
                      </tr>
                      <tr>
                          <td align="right">
                              <asp:Label ID="lblstate" runat="server" Text="State:" Width="50px" />
                          </td>
                          <td align="left">
                              <asp:TextBox ID="Statetextbox" runat="server" Width="50px"></asp:TextBox>
                          </td>
                          <td align="right">
                              <asp:Label ID="lblzip" runat="server" Text="Zip:" Width="25px" />
                          </td>
                          <td align="left">
                              <asp:TextBox ID="Ziptextbox" runat="server" Width="50px"></asp:TextBox>
                          </td>
                      </tr>
                      <tr>
                          <td align="right">
                              Phone Number:
                          </td>
                          <td align="left">
                              <asp:TextBox ID="PhoneTextBox" runat="server" MaxLength="12"></asp:TextBox>
                          </td>
                          <td align="right">
                              Fax Number:
                          </td>
                          <td align="left">
                              <asp:TextBox ID="FaxTextBox" runat="server" MaxLength="12"></asp:TextBox>
                          </td>
                      </tr>
                      <tr>
                              <td align="right">
                                  CLIA:
                              </td>
                              <td align="left">
                                  <asp:TextBox ID="CLIATextBox" runat="server" ></asp:TextBox>
                              </td>
                          <td align="right">
                              Email:
                          </td>
                          <td align="left">
                              <asp:TextBox ID="directeMailTextBox" runat="server"  Width="250px"></asp:TextBox>
                          </td>

                      </tr>


                      <tr>
                          <td align="right">
                              Effective Date:
                          </td>
                          <td align="left">
                              <asp:TextBox ID="EffectiveDateTextBox" runat="server" CssClass="calenderClass" Width="150px"></asp:TextBox>
                          </td>
                          <td align="right">
                                <asp:Label ID="validationlbl" runat="server" Text="Validation Date:" 
                                    Visible="False"></asp:Label>
                          </td>
                          <td align="left">
                              <asp:TextBox ID="ValidationDateTextBox" runat="server" CssClass="calenderClass" 
                                  Visible="False" Width="150px" AutoPostBack="true"></asp:TextBox>
                                  <asp:TextBox ID="hdValidationdate" runat="server" Visible="false"></asp:TextBox>
                          </td>
                      </tr>

                     <tr>
                          <td align="left" colspan="4">
                              Comments:
                          </td>
                      </tr>
                      <tr>
                          <td align="left" colspan="4">
                              <asp:TextBox ID="commentsTextBox" runat="server" TextMode="MultiLine" 
                                  Width="95%"></asp:TextBox>
                            <asp:TextBox id="OrigReqDesc" runat="server" Visible="False"></asp:TextBox>
                          </td>
                      </tr>
                        
                        <tr>
                            <td colspan="4">
                                <asp:Label ID="GroupTypeCDLabel" runat="server" Visible="false"></asp:Label>
                                <asp:Label ID="groupnumLabel" runat="server" Visible="false"></asp:Label>
                                <asp:Label ID="AKAGroupNameLabel" runat="server" Visible="false"></asp:Label>
                                <asp:Label ID="directeMailLabel" runat="server" Visible="false"></asp:Label>
                                <asp:Label ID="CapCountLabel" runat="server" Visible="false"></asp:Label>

                            </td>
                        </tr>


                 </table>

                    </ContentTemplate>
                  </ajaxToolkit:TabPanel>



                  <ajaxToolkit:TabPanel ID="tpAddItems" runat="server" BorderStyle="Solid" BorderWidth="1px">
                    <HeaderTemplate>
                        <asp:Label ID="lblinner1" runat="server" Text="Providers" Font-Bold="true" ForeColor="Black"></asp:Label>
                    </HeaderTemplate>
                        <ContentTemplate>
                           <table border="0" width = "100%">
                            <tr>
                                <td colspan = "4">
                                      <asp:Table ID = "tblProviders" runat = "server" Font-Size="Small">
                                     </asp:Table>
            
                                </td>
        
                            </tr>
                           </table>
                       </ContentTemplate>
                  </ajaxToolkit:TabPanel>

                  <ajaxToolkit:TabPanel ID="tpClosedRequest" runat="server" BorderStyle="Solid" BorderWidth="1px">
                       <HeaderTemplate > 
                          <asp:Label ID="Label3" runat="server" Text="Employees" Font-Bold="true" ForeColor="Black" ></asp:Label>
                     </HeaderTemplate>
                      <ContentTemplate>
                          <table border="0" width = "100%">
                            <tr>
                                <td colspan = "4">
                                      <asp:Table ID = "tblEmployees" runat = "server" Font-Size="Small">
                                     </asp:Table>
            
                                </td>
        
                            </tr>
                           </table>
                       </ContentTemplate>
                   </ajaxToolkit:TabPanel>


                 </ajaxToolkit:TabContainer>


            </td>
        </tr>
   </table>

<%-- =================== RSA Token Address ===============================--%>

<asp:Panel ID="pnlTokenAddress" runat="server"  Visible="false">
  <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>

    <table id="Tokentbl"  border="3px" width="100%">
    <tr>
        <td align="left" class="tableRowSubHeader" colspan="4">
            Cell Phone Information for Remote Access
        </td>
    </tr>

    <tr>
        <td colspan="4" align="center">
            <asp:Label id="lblTokenError" runat="server" BackColor="Red" Visible="false" Font-Size="Large" Font-Bold="True"></asp:Label>
        </td>
    </tr>
                                
    <tr>

        <td align="right">
            Provider:
        </td>
        <td align="left">
            <asp:DropDownList ID="CellPhoneddl" runat="server" Width="250px" AutoPostBack="true">
                                
            </asp:DropDownList>
            <asp:TextBox ID="otherCellprov" runat="server" Visible="false"  MaxLength="25" ></asp:TextBox>
        </td>
        <td align="right">
            Number: ##########
        </td>
        <td align="left">
            <asp:TextBox ID ="TokenPhoneTextBox" runat="server" MaxLength="10" 
                ToolTip="Only enter Numbers no other Characters" ></asp:TextBox>
        </td>

    </tr>

    <tr align="center">
        <td align="left" colspan="4" >

            <asp:TextBox ID="CellProvidertxt" runat="server" Visible="false"></asp:TextBox>
            <asp:TextBox ID ="TokenNameTextBox" runat="server"  Width="80%" Visible="false"></asp:TextBox>
            <asp:TextBox ID ="TokenAddress1TextBox" Width="80%" runat="server" Visible="false"></asp:TextBox>
            <asp:TextBox ID ="TokenAddress2TextBox" runat="server" Width="80%" Visible="false"></asp:TextBox>
                <asp:TextBox ID ="TokencommentsTextBox" runat="server"  Width="90%" TextMode="MultiLine" Visible="false"></asp:TextBox>

            <asp:TextBox ID ="TokenStTextBox" runat="server" Visible="false"></asp:TextBox>
            <asp:TextBox ID ="TokenZipTextBox" runat="server" Visible="false"></asp:TextBox>
            <asp:TextBox ID ="TokenCityTextBox" runat="server" Width="80%" Visible="false"></asp:TextBox>

        </td>
    </tr>
            
    </table>
            			
        </ContentTemplate>
    </asp:UpdatePanel>
 </asp:Panel>

    <asp:Panel ID="pnlOnBehalfOf" runat="server"  Width="70%" Height="60%"  BackColor="#CCCCCC"  style="display:none">
        <asp:UpdatePanel ID="uplOnBehalfOf" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table align="center" border="1px" width="90%">
                   <tr valign="middle">
                        <td colspan="4"></td>
                    </tr>
                    <tr>
                        <td class="tableRowHeader" colspan="4">
                           Search for CKHS Individual who will be Responsable for these Account(s)<br />
                           Listed below are CKHS Directors-Managers-VP 

                        </td>
                    </tr>
                    <tr>
                        <td>
                            Last Name
                        </td>
                        <td>
                            <asp:TextBox ID="LastNameTextBox" runat="server"></asp:TextBox>
                        </td>
                        <td>
                            First Name
                        </td>
                        <td>
                            <asp:TextBox ID="FirstNameTextBox" runat="server"></asp:TextBox>
                        </td>

                    </tr>
                    <tr>
                        <td colspan="4">
                            <div class="scrollContentContainer">
                                <asp:UpdateProgress ID="UpdateProgress1" runat="server" 
                                    AssociatedUpdatePanelID="uplOnBehalfOf" DisplayAfter="200">
                                    <ProgressTemplate>
                                        <div ID="IMGDIV" runat="server" align="center" 
                                            style="visibility:visible;vertical-align:middle;" valign="middle">
                                            Loading Data ...
                                        </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                                <asp:GridView ID="gvSearch" runat="server" AllowSorting="True" 
                                    AutoGenerateColumns="False" BackColor="White" BorderColor="#336666" 
                                    BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                                    DataKeyNames="client_num, login_name" EmptyDataText="No Items Found" 
                                    EnableTheming="False" Font-Size="Medium" GridLines="Horizontal" Width="100%">
                                    <FooterStyle BackColor="White" ForeColor="#333333" />
                                    <HeaderStyle BackColor="#336666" Font-Bold="False" Font-Size="Small" 
                                        ForeColor="White" HorizontalAlign="Left" />
                                    <PagerStyle BackColor="#336666" Font-Size="Medium" ForeColor="White" 
                                        HorizontalAlign="Left" />
                                    <RowStyle BackColor="White" ForeColor="#333333" />
                                    <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                    <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                    <SortedAscendingHeaderStyle BackColor="#487575" />
                                    <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                    <SortedDescendingHeaderStyle BackColor="#275353" />
                                    <Columns>
                                        <asp:CommandField ButtonType="Button" SelectText="Select" 
                                            ShowSelectButton="true" />
                                        <asp:BoundField DataField="fullname" HeaderStyle-HorizontalAlign="Left" 
                                            HeaderText="Full Name" ItemStyle-HorizontalAlign="Left" />
                                        <asp:BoundField DataField="phone" HeaderStyle-HorizontalAlign="Left" 
                                            HeaderText="Phone" ItemStyle-HorizontalAlign="Left" />
                                        <asp:BoundField DataField="department_name" HeaderStyle-HorizontalAlign="Left" 
                                            HeaderText="Department" ItemStyle-HorizontalAlign="Left" />
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <table>
                                <tr>
                                    <td>
                                        <asp:Button ID="btnSearch" runat="server" Font-Bold="True" Font-Names="Arial" 
                                            Font-Size="Small" Height="20px" Text="Search" Width="125px" />
                                    </td>
                                    <td>
                                        <asp:Button ID="btnCloseOnBelafOf" runat="server" Font-Bold="True" 
                                            Font-Names="Arial" Font-Size="Small" Height="20px" Text="Close" Width="125px" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>

    <ajaxToolkit:UpdatePanelAnimationExtender ID="UpdatePanelAnimationExtender1" runat="server" TargetControlID="uplOnBehalfOf">
        <Animations>
            <OnUpdating>
                <StyleAction animationtarget="progress" Attribute="display" value="block" />
            </OnUpdating>
            <OnUpdated>
                <StyleAction animationtarget="progress" Attribute="display" value="none" />
            </OnUpdated>
        </Animations>
    </ajaxToolkit:UpdatePanelAnimationExtender>
    <ajaxToolkit:ModalPopupExtender ID="mpeOnBehalfOf" runat="server" 
       DynamicServicePath="" Enabled="True" TargetControlID="btnSelectOnBehalfOf"   
       PopupControlID="pnlOnBehalfOf" BackgroundCssClass="ModalBackground"   
         DropShadow="true" CancelControlID="btnCloseOnBelafOf">  
        </ajaxToolkit:ModalPopupExtender>

<asp:Button ID="btnUpdateDummy" runat="server" Style="display: none" Text="Button" />

    <ajaxToolkit:ModalPopupExtender ID="mpeUpdateClient" runat="server" 
        BackgroundCssClass="ModalBackground" DropShadow="true" DynamicServicePath="" 
        Enabled="True" PopupControlID="pnlUpdateClient" CancelControlID="btnCloseUpdate"
        TargetControlID="btnUpdateDummy">
    </ajaxToolkit:ModalPopupExtender>

        <asp:Panel ID="pnlUpdateClient" runat="server" 
                    style="display:none; background-color:#efefef;  font-size:large;  width:600px;  border: 2px solid #FFFFCC;">
                    <table border="1px" width="100%">
                        <tr>
                            <th class="tableRowHeader">
                                <asp:Label ID="lblUpdateClientFullName" runat="server"></asp:Label>
                            </th>
                        </tr>
                        <tr>
                            <td>
                                <asp:Table ID="tblUpdateClient" runat="server" Width="100%">
                                    <asp:TableRow>
                                        <asp:TableCell>
                Phone Number
            </asp:TableCell>
                                        <asp:TableCell>
                <asp:TextBox runat="server" ID="txtUpdatePhone"></asp:TextBox>
            </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                    Email
            </asp:TableCell>
                                        <asp:TableCell>
                <asp:TextBox runat="server" ID="txtUpdateEmail" Width="100%"></asp:TextBox>
            </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button ID="btnUpdateClient" runat="server" Text="Update" />
                                <asp:Button ID="btnCloseUpdate" runat="server" Text="Cancel" />
                            </td>
                        </tr>
                    </table>
        </asp:Panel>

    </ContentTemplate>
 </asp:UpdatePanel>
</asp:Content>

