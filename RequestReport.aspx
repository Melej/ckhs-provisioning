﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="false" CodeFile="RequestReport.aspx.vb" Inherits="RequestReport" %>
<%@ Register assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" namespace="System.Web.UI.DataVisualization.Charting" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <style type="text/css">
        .style12
        {
            height: 30px;
            width: 518px;
        }        
        .ui-datepicker-calendar { 
        display: none; 
        } 
        .style14
        {
            font-size: medium;
        }
        .style17
        {
            width: 418px;
        }
        .style18
        {
            width: 92px;
        }
        .style19
        {
            width: 164px;
        }
        .style20
        {
            width: 145px;
        }

        </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
<asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="100"  >
   <ProgressTemplate>
        <div id="IMGDIV" align="center" valign="middle" runat="server" style="position:
                absolute;left: 400px;top:150px;visibility:visible;vertical-align:middle;border-style:
                inset;border-color:black;background-color:White;width:210px;height:75px; z-index: 999;">
        Loading Data ...
            <img src="Images/AjaxProgress.gif" /><br />
        </div>
   </ProgressTemplate>
</asp:UpdateProgress>

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.min.js"></script> 

<%--<asp:UpdatePanel ID = "uplPanel" runat="server">
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="btnUpdate" EventName="Click" />
    </Triggers>
</asp:UpdatePanel>
--%>
<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
    <ContentTemplate>   

<table id="maintbl" width="80%">
<%-- Row 1 --%>
    
    <tr>
        <td align="center">
            <table>
                <tr>
                   <td class="style14" align="center">
                        <strong>Account Request Report Add Account/Delete Accounts</strong>
                    </td>
                 </tr>
                  <tr>
                     <td  class="style14">
                        <asp:Label ID="lblChartTitle" runat="server" Text=""></asp:Label>
                      </td>
                  </tr>
              </table>
         </td>   
    </tr>
 <%-- Row 2 --%>
    <tr>
       <td class="style12" >
         <asp:Label ID="LbMonthly" runat="server"></asp:Label>
      </td>
    </tr>
<%-- Row 3 --%>
<%--     <tr>
       <td class="style17">
          <table id="tblTypeAndReset">
             <tr>
                <td>
                    <asp:Button ID="Reset" runat="server" Text="Reset" height="25px" Width="100px" />

                </td>
                <td>
                    <asp:Button ID="btnExcel" runat="server" Text="Excel" Width="99px" />
                </td> 
                <td>
                    <asp:Button ID="btnUserActivity" runat="server" Text="User Activity" Width="99px" />
                </td>
                <td>
                    <asp:Button ID="btnPdf" runat="server" Text="Print " height="25px" Width="100px" Visible="False" />
               </td>
             </tr>
          </table>
        </td>
     </tr>
--%> <%-- Row 4 --%>
     <tr>
       <td class="style17">
       </td>
     </tr>
 <%-- Row 5 --%>
    <tr align="center">
      <td>

           <asp:Chart ID="Chart1" runat="server" Height="491px"  Visible="false">
                    <series>
                        <asp:Series Name="ChartSeries" ChartType="Pie" ChartArea="MainArea" IsValueShownAsLabel="true">
                        
                        </asp:Series>
                    </series>
                    <chartareas>
                        <asp:ChartArea Name="MainArea" Area3DStyle-Enable3D="true" Area3DStyle-Inclination="30"
                         Area3DStyle-IsClustered="false">
                        </asp:ChartArea>
                    </chartareas>
                    <legends>
                        <asp:Legend >
                        </asp:Legend>
                    </legends>
                </asp:Chart>

      </td>
   </tr>
 <%-- Row 6 --%>
     <tr>
      <td align="center">

                           <asp:GridView ID="grdTotal" runat="server" AllowSorting="True" 
                                        AutoGenerateColumns="False" BackColor="White" BorderColor="#336666"
                                        BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                                        EmptyDataText="No Applications Found" EnableTheming="False" DataKeyNames="ThisYear"
                                        Font-Size="Small" GridLines="Horizontal" Width="99%">
								
                                         <FooterStyle BackColor="White" ForeColor="#333333" />
                                        <HeaderStyle BackColor="#336666" Font-Bold="False" ForeColor="White" Font-Size="Small" HorizontalAlign="Left"    />                                    
                                        <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Left" Font-Size="Medium"/>
                                        <RowStyle BackColor="White" ForeColor="#333333" />
                                        <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                        <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                        <SortedAscendingHeaderStyle BackColor="#487575" />
                                        <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                        <SortedDescendingHeaderStyle BackColor="#275353" />

								<Columns>
                                    <asp:CommandField ButtonType="Button" SelectText=" Select " ShowSelectButton= "true" ControlStyle-BackColor="#84A3A3" ItemStyle-Height="30px"/>

									<asp:BoundField DataField="ThisYear" HeaderText="Year"  HeaderStyle-HorizontalAlign="Center"  ItemStyle-VerticalAlign="Middle" ItemStyle-Height="15%"></asp:BoundField>
                                    <asp:BoundField DataField="GrandTotalPerYear" HeaderText="Total by Year " HeaderStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" ItemStyle-Height="15%" HeaderStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Center"></asp:BoundField>

									<asp:BoundField DataField="TotalClosedByYear" HeaderText="Total Completed" HeaderStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" ItemStyle-Height="15%" HeaderStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                                    
									<asp:BoundField DataField="TotalCurrentlyOpened" HeaderText="Total Curr. Open" HeaderStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" ItemStyle-Height="15%" HeaderStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                                    

									<asp:BoundField DataField="TotalNewRequests" HeaderText="Add New Account Requests" HeaderStyle-HorizontalAlign="Center"  ItemStyle-HorizontalAlign="Center" ItemStyle-Height="15%"></asp:BoundField>
									<asp:BoundField DataField="TotalDeleteRequests" HeaderText="Delete Account Requests" HeaderStyle-HorizontalAlign="Center"  ItemStyle-HorizontalAlign="Center" ItemStyle-Height="15%"></asp:BoundField>


                                  </Columns>

                         </asp:GridView>
      </td>
     </tr>
 <%-- Row 6.A --%>

        <tr>
            <td>
            </td>
        </tr>

 <%-- Row 7 --%>

     <tr>
        <td>
            <asp:Label ID="HDyear" runat="server" Visible="False"></asp:Label>
            <asp:Label ID="HDtype" runat="server" Visible="false"></asp:Label>
        </td>
     </tr>
</table>
   </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="grdTotal" EventName="RowCommand" />
    </Triggers>
 </asp:UpdatePanel>

</asp:Content>

