﻿Imports System
Imports System.Net
Imports System.Net.Mail
Imports System.Net.Mail.SmtpClient
Imports System.IO
Imports System.Data '
Imports System.Data.SqlClient
Imports App_Code
Imports AjaxControlToolkit
Imports C1.Web.UI.Controls.C1Window
Partial Class ProviderDemo
    Inherits System.Web.UI.Page
    Dim sWebReferrer As String = ""
    Dim AccountRequestNum As Integer
    Dim sActivitytype As String = ""
    Dim FormSignOn As New FormSQL()
    Dim NewRequestForm As New FormSQL()
    Dim iClientNum As String
    Dim employeetype As String
    Dim bHasError As Boolean
    Dim ValidationStatus As String = ""

    Dim AccountRequest As AccountRequest

    Private dtAccounts As New DataTable
    Dim UserInfo As UserSecurity
    Private AddAccountRequestNum As Integer = 0
    Private DelAccountRequestNum As Integer = 0
    Private RemoveAccts As DataTable
    Private dtPendingRequests As DataTable
    Private dtRemoveAccounts As New DataTable
    Private dtAddAccounts As New DataTable
    Dim thisdata As CommandsSqlAndOleDb
    Dim Mailobj As System.Net.Mail.SmtpClient = New System.Net.Mail.SmtpClient
    Dim Mailmsg As New System.Net.Mail.MailMessage
    Dim MailAddress As System.Net.Mail.MailAddress
    Dim mailAttachment As System.Net.Mail.Attachment

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        'iClientNum = 48490
        'AccountRequestNum = "554811"

        iClientNum = Request.QueryString("ClientNum")


        If Request.QueryString("requestnum") <> Nothing Then
            AccountRequestNum = Request.QueryString("requestnum")

            AccountRequestNumLabel.Text = Request.QueryString("requestnum")

        End If

        Dim dbAccess As New CkhsEmployeeDbAccess(Session("EmployeeConn"))
        Dim ckhsEmployee = dbAccess.GetCkhsEmployee(iClientNum)

        UserInfo = New UserSecurity(Session("LoginID"), Session("EmployeeConn"))

        'iUserNum = UserInfo.ClientNum
        'sNtLogin = UserInfo.NtLogin
        ' "UpdClientDemoObjectByNumberV4"

        FormSignOn = New FormSQL(Session("EmployeeConn"), "SelClientDemoRequestByNumberV4", "UpdClientDemoObjectByNumberV4", "", Page)
        FormSignOn.AddSelectParm("@client_num", iClientNum, SqlDbType.Int, 6)
        FormSignOn.AddSelectParm("@account_request_num", AccountRequestNum, SqlDbType.Int, 6)
        FormSignOn.AddSelectParm("@FormName", "provider", SqlDbType.NVarChar, 20)

        NewRequestForm = New FormSQL(Session("EmployeeConn"), "SelAccountRequestV20", "InsNewDemoRequestV4", "", Page)
        If Not IsPostBack Then

            'userSubmittingLabel.Text = UserInfo.UserName
            'SubmittingOnBehalfOfNameLabel.Text = UserInfo.UserName
            'requestor_client_numLabel.Text = UserInfo.ClientNum

            'emptypecdrbl.SelectedValue = ckhsEmployee.EmpTypeCd
            'emptypecdLabel.Text = ckhsEmployee.EmpTypeCd
            'HDemployeetype.Text = ckhsEmployee.EmpTypeCd
            _

            FormSignOn.FillForm()

        End If
        'pnlSubmission
        'pnlProvider
        '            Response.Redirect("~/AccountRequestInfo2.aspx?RequestNum=" & AccountRequestNum, True)
        If AccountRequestNumLabel.Text = "0" Then
            pnlSubmission.Visible = True
        Else
            If AccountRequestNumLabel.Text <> Nothing And AccountRequestNum.ToString Is Nothing Or AccountRequestNum = 0 Then
                AccountRequestNum = CInt(AccountRequestNumLabel.Text)

            End If

            If Session("SecurityLevelNum") >= 90 Then
                pnlProvider.Visible = True
            End If
        End If
        If ClientNumLabel.Text <> Nothing And iClientNum Is Nothing Then
            iClientNum = CInt(ClientNumLabel.Text)
        End If
    End Sub

    Protected Sub btnSubmitDemoChange_Click(sender As Object, e As System.EventArgs) Handles btnSubmitDemoChange.Click
        'Submit origional request 
        NewRequestForm.AddInsertParm("@receiver_client_num", iClientNum, SqlDbType.Int, 10)
        NewRequestForm.AddInsertParm("@requestor_client_num", UserInfo.ClientNum, SqlDbType.NVarChar, 10)

        NewRequestForm.AddInsertParm("@submitter_client_num", UserInfo.ClientNum, SqlDbType.Int, 10)
        NewRequestForm.AddInsertParm("@account_request_type", "demoupd", SqlDbType.NVarChar, 9)

        NewRequestForm.UpdateFormReturnReqNum()

        Response.Redirect("~/ClientDemo.aspx?ClientNum=" & iClientNum, True)


    End Sub

    Protected Sub btnCancelDemoChange_Click(sender As Object, e As System.EventArgs) Handles btnCancelDemoChange.Click
        Response.Redirect("~/ClientDemo.aspx?ClientNum=" & iClientNum, True)

        'Response.Redirect("~/SimpleSearch.aspx", True)
    End Sub

    Protected Sub BtnComitDemoChanges_Click(sender As Object, e As System.EventArgs) Handles BtnComitDemoChanges.Click
        ' Approve provider Demographics request

        ApprovalClientNumLabel.Text = UserInfo.ClientNum.ToString

        'FormSignOn.AddInsertParm("@clientnum", iClientNum, SqlDbType.Int, 9)
        'FormSignOn.AddInsertParm("@AccountRequestNum", AccountRequestNumLabel.Text, SqlDbType.Int, 9)
        'FormSignOn.AddInsertParm("@RoleNum", "", SqlDbType.NVarChar, 9)
        FormSignOn.AddInsertParm("@Approval", "yes", SqlDbType.NVarChar, 6)


        FormSignOn.UpdateForm()

        Dim sDate As DateTime = Date.Now()

        lblValidation.Text = "Information Updated at -> " & sDate



        upProviderDemo.Update()

        sendAlertEmail()

        Response.Redirect("~/ClientDemo.aspx?ClientNum=" & iClientNum, True)


    End Sub

    Protected Sub BtnDeniDemoChanges_Click(sender As Object, e As System.EventArgs) Handles BtnDeniDemoChanges.Click
        ApprovalClientNumLabel.Text = UserInfo.ClientNum.ToString

        'FormSignOn.AddInsertParm("@clientnum", iClientNum, SqlDbType.Int, 9)
        'FormSignOn.AddInsertParm("@AccountRequestNum", AccountRequestNumLabel.Text, SqlDbType.Int, 9)
        'FormSignOn.AddInsertParm("@ApprovalClientNum", UserInfo.ClientNum, SqlDbType.Int, 9)
        FormSignOn.AddInsertParm("@Approval", "no", SqlDbType.NVarChar, 6)


        FormSignOn.UpdateForm()

        Dim sDate As DateTime = Date.Now()

        lblValidation.Text = "Information Updated at -> " & sDate



        upProviderDemo.Update()

        Response.Redirect("~/ClientDemo.aspx?ClientNum=" & iClientNum, True)



    End Sub

    Protected Sub btnReturntoDemo_Click(sender As Object, e As System.EventArgs) Handles btnReturntoDemo.Click
        Response.Redirect("~/ClientDemo.aspx?ClientNum=" & iClientNum, True)

    End Sub
    Private Sub sendAlertEmail()
        '547131
        Dim Request As New AccountRequest(AccountRequestNum, Session("EmployeeConn"))
        Dim ProviderItems As New ProviderItemsChanged(AccountRequestNum, Session("EmployeeConn"))

        'Dim EmailBody As New EmailBody
        'Dim EmailInfo As New EmailBody(AccountRequestNum, Session("EmployeeConn"))

        Dim sAppbase As String = "94"
        Dim Link As String

        Dim sSubject As String
        Dim sBody As String

        Dim toList As String()

        '======================
        ' Get current path
        '======================

        'Dim path As String
        'Dim directory As String = ""
        Dim EmailList As String

        Mailmsg.To.Clear()

        'path = HttpContext.Current.Request.Url.AbsoluteUri
        'Dim pathParts() As String = Split(path, "/")
        'Dim i As Integer
        'Do While i < pathParts.Length() - 1

        '    directory = directory + pathParts(i) + "/"
        '    i = i + 1

        'Loop

        If Session("environment").ToString.ToLower = "testing" Then
            'toEmail = "Jeff.Mele@crozer.org"
            Link = "http://webdev3/CSCV2/ClientDemo.aspx?ClientNum=" & iClientNum.ToString
        Else
            Link = "http://Casemgmt02/CSCV2/ClientDemo.aspx?ClientNum=" & iClientNum.ToString

        End If

        '======================
        ' This is only Provider Demo Email



        'For Each EmailAddress In EmailList
        '    If EmailList.Count = 1 Then
        '        If EmailAddress.Email <> "" Then
        '            toEmail = EmailAddress.Email
        '            mailObj.To.Add(New MailAddress(toEmail))
        '        End If
        '        'Else
        '        '    If EmailAddress.Email <> "" Then
        '        '        toEmail = EmailAddress.Email
        '        '        mailObj.To.Add(New MailAddress(toEmail))
        '        '    End If

        '    End If

        'Next

        'EmailProviderCompletedRequestTable



        sSubject = "Provider Demographic Change Request for " & Request.FirstName & " " & Request.LastName & " has been completed "

        sBody = "<!DOCTYPE html>" & _
"<html>" & _
"<head>" & _
"<style>" & _
"table" & _
"{" & _
"border-collapse:collapse;" & _
"font-family: 'Helvetica Neue', 'Lucida Grande', 'Segoe UI', Arial, Helvetica, Verdana, sans-serif;" & _
"}" & _
"table, th, td" & _
"{" & _
"border: 1px solid black;" & _
"padding: 3px" & _
"}" & _
"</style>" & _
"</head>" & _
"" & _
"<body>" & _
"<table cellpadding='5px'>" & _
        "<tr>" & _
            "<td colspan='3' align='center'  style='font-size:large'>" & _
"               " & _
            "<b>Provider Demographic Change Complete</b>" & _
"                 " & _
             "</td>    " & _
        "</tr>" & _
        "<tr>" & _
            "<td style='font-weight:bold' colspan='1'>Name:</td>" & _
            "<td colspan='3'>" & Request.FirstName & " " & Request.LastName & "</td>" & _
        "</tr>" & _
        "<tr>" & _
            "<td style='font-weight:bold' colspan='1'>Provider#:</td>" & _
            "<td colspan='3'>" & Request.DoctorNum & "</td>" & _
        "</tr>" & _
        "<tr>" & _
            "<td style='font-weight:bold' colspan='1'>Requested By:</td>" & _
            "<td colspan='3'>" & Request.RequestorName & "</td>" & _
        "</tr>" & _
           "<tr>" & _
            "<td style='font-weight:bold' colspan='1'>Requestor Email:</td>" & _
            "<td colspan='3'>" & Request.RequestorEmail & "</td>" & _
        "</tr>" & _
        "<tr>" & _
            "<td style='font-weight:bold' colspan='1'>Date Requested:</td>" & _
            "<td colspan='3'>" & Request.EnteredDate & "</td>" & _
        "</tr>" & _
"      " & _
"     " & _
"<tr style='font-weight:bold'><td>Data Field </td><td>Original Data </td><td> Updated Data </td><td>New Data</td></tr>" & _
"<tr><td> First Name </td><td>" & ProviderItems.FirstName & "</td><td>" & ProviderItems.NewFirstName & "</td><td>" & ProviderItems.CurrentFirstName & "</td> </tr>" & _
"<tr><td> Last Name </td><td>" & ProviderItems.LastName & "</td><td>" & ProviderItems.NewlastName & "</td><td>" & ProviderItems.CurrentlastName & "</td></tr>" & _
"<tr><td> MI </td><td>" & ProviderItems.MI & "</td><td>" & ProviderItems.NweMI & "</td><td>" & ProviderItems.NweMI & "</td></tr>" & _
"<tr><td> Address 1 </td><td>" & ProviderItems.Address1 & "</td><td>" & ProviderItems.NewAddress1 & "</td><td>" & ProviderItems.CurrentAddress1 & "</td></tr>" & _
"<tr><td> Address 2 </td><td>" & ProviderItems.Address2 & "</td><td>" & ProviderItems.NewAddress2 & "</td><td>" & ProviderItems.CurrentAddress2 & "</td></tr>" & _
"<tr><td> City </td><td>" & ProviderItems.City & "</td><td>" & ProviderItems.NewCity & "</td><td>" & ProviderItems.CurrentCity & "</td></tr>" & _
"<tr><td> State </td><td>" & ProviderItems.State & "</td><td>" & ProviderItems.NewState & "</td><td>" & ProviderItems.CurrentState & "</td></tr>" & _
"<tr><td> Zip </td><td>" & ProviderItems.Zip & "</td><td>" & ProviderItems.NewZip & "</td><td>" & ProviderItems.CurrentZip & "</td></tr>" & _
"<tr><td> Phone </td><td>" & ProviderItems.Phone & "</td><td>" & ProviderItems.NewPhone & "</td><td>" & ProviderItems.CurrentPhone & "</td></tr>" & _
"<tr><td> Fax </td><td>" & ProviderItems.Fax & "</td><td>" & ProviderItems.NewFax & "</td><td>" & ProviderItems.CurrentFax & "</td></tr>" & _
"</table>" & _
               "</body>" & _
                "</html>"

        sBody = sBody & "<table cellpadding='5px'>" & _
        "<tr>" & _
            "<td align='center'  style='font-size:large'>" & _
"               " & _
            "<b>View Details by clicking on link below</b>" & _
"                 " & _
             "</td>    " & _
        "</tr>" & _
        "<tr>" & _
            "<td align='center'>" & Link & "</td>" & _
        "</tr>"



        EmailList = New EmailListOne(sAppbase, AccountRequestNum, Session("EmployeeConn")).EmailListReturn

        If Session("environment").ToString.ToLower = "testing" Then
            EmailList = "Jeff.Mele@crozer.org"
        End If

        toList = EmailList.Split(";")
        If toList.Length = 1 Then
            Mailmsg.To.Add(New System.Net.Mail.MailAddress(toList(0)))
        Else
        End If
        For i = 0 To toList.Length - 1 ' - 2
            If toList(i).Length > 10 Then ' validity check
                Mailmsg.To.Add(New System.Net.Mail.MailAddress(toList(i)))
            End If
        Next

        'Dim mailObj As New MailMessage
        Mailmsg.From = New MailAddress("CSC@crozer.org")
        Mailmsg.IsBodyHtml = True

        Mailmsg.Subject = sSubject
        Mailmsg.Body = sBody

        Dim smtpClient As New SmtpClient()
        smtpClient.Host = "ah1smtprelay01.altahospitals.com"


        smtpClient.Send(Mailmsg)

        '"<br>" & _
        '"<br>&nbsp;" & "Follow link for full details " & directory & "/ClientDemo.aspx?ClientNum=" & iClientNum.ToString & _


    End Sub
End Class
