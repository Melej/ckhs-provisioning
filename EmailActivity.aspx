﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="EmailActivity.aspx.vb" Inherits="EmailActivity" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Email p1 Activity</title>
    
    <link href="~/Styles/Tables.css" rel="stylesheet" type="text/css" />
    <link href="~/Styles/ToolTip.css" rel="stylesheet" type="text/css" />
    <link href="~/Styles/ModalPopUp.css" rel="stylesheet" type="text/css" />



    <script type="text/javascript">
        function closeWindows() {
            var browserName = navigator.appName;
            var browserVer = parseInt(navigator.appVersion);
            //alert(browserName + " : "+browserVer);

            //document.getElementById("flashContent").innerHTML = "<br>&nbsp;<font face='Arial' color='blue' size='2'><b> You have been logged out of the Game. Please Close Your Browser Window.</b></font>";

            if (browserName == "Microsoft Internet Explorer") {
                var ie7 = (document.all && !window.opera && window.XMLHttpRequest) ? true : false;
                if (ie7) {
                    //This method is required to close a window without any prompt for IE7 & greater versions.
                    window.open('', '_parent', '');
                    window.close();
                }
                else {
                    //This method is required to close a window without any prompt for IE6
                    this.focus();
                    self.opener = this;
                    self.close();
                }
            } else {
                //For NON-IE Browsers except Firefox which doesnt support Auto Close
                try {
                    this.focus();
                    self.opener = this;
                    self.close();
                }
                catch (e) {

                }

                try {
                    window.open('', '_self', '');
                    window.close();
                }
                catch (e) {

                }
            }
        }

    </script>
</head>
<body>



    <form id="form1" runat="server">

       <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" CombineScripts="false">

             <Scripts>
             </Scripts>
    
        <Services>
            
             
             </Services>
     </ajaxToolkit:ToolkitScriptManager>


    <div>
     <asp:Label runat="server" ID ="lblTicketNum"></asp:Label>
    </div>


 <asp:Button ID="btnDummy" Style="display: none" runat="server" Text="Button" />
    
<ajaxToolkit:ModalPopupExtender ID="mpeEmailActivity" runat="server"   
                DynamicServicePath="" Enabled="True" TargetControlID="btnDummy"   
                PopupControlID="pnlEmail" BackgroundCssClass="ModalBackground"   
                DropShadow="true" >  
    </ajaxToolkit:ModalPopupExtender>  
            
            
<asp:Panel ID="pnlEmail" runat="server" style="display:none; background-color:#efefef;  font-size:large;  width:600px;  border: 2px solid #FFFFCC;">

      
                                            
                                                            <table  width="100%" border="1px" >
                                                                <tr>
                                                                    <th class="tableRowHeader">
                                                                  Update Ticket
                                                                    </th>
                                                                </tr>
                                                             <tr>

                                                                <td>
                                                                    <asp:TextBox runat="server" ID="txtActivity" TextMode="MultiLine" Width="95%" Rows="15"></asp:TextBox>

                                         <ajaxToolkit:HtmlEditorExtender ID="htmlEdExActivity" runat="server" DisplaySourceTab="True"  TargetControlID = "txtActivity" OnImageUploadComplete="ajaxFileUpload_OnUploadComplete" >
                                             <Toolbar> 
                                                        <ajaxToolkit:Undo />
                                                        <ajaxToolkit:Redo />
                                                        <ajaxToolkit:Bold />
                                                        <ajaxToolkit:Italic />
                                                        <ajaxToolkit:Underline />
                                                        <ajaxToolkit:StrikeThrough />
                                                        <ajaxToolkit:Subscript />
                                                        <ajaxToolkit:Superscript />
                                                        <ajaxToolkit:JustifyLeft />
                                                        <ajaxToolkit:JustifyCenter />
                                                        <ajaxToolkit:JustifyRight />
                                                        <ajaxToolkit:JustifyFull />
                                                        <ajaxToolkit:InsertOrderedList />
                                                        <ajaxToolkit:InsertUnorderedList />
                                                        <ajaxToolkit:CreateLink />
                                                        <ajaxToolkit:UnLink />
                                                        <ajaxToolkit:RemoveFormat />
                                                        <ajaxToolkit:SelectAll />
                                                        <ajaxToolkit:UnSelect />
                                                        <ajaxToolkit:Delete />
                                                        <ajaxToolkit:Cut />
                                                        <ajaxToolkit:Copy />
                                                        <ajaxToolkit:Paste />
                                                        <ajaxToolkit:BackgroundColorSelector />
                                                        <ajaxToolkit:ForeColorSelector />
                                                        <ajaxToolkit:FontNameSelector />
                                                        <ajaxToolkit:FontSizeSelector />
                                                        <ajaxToolkit:Indent />
                                                        <ajaxToolkit:Outdent />
                                                        <ajaxToolkit:InsertHorizontalRule />
                                                        <ajaxToolkit:HorizontalSeparator />
                                                        <ajaxToolkit:InsertImage />
                                                    </Toolbar>
                                         </ajaxToolkit:HtmlEditorExtender>
                                                                </td>
       
                                                                      
                                                            </tr>
                                                         
                                                               
                                                                <tr>
                                                                    <td align = "center" >

                                                                    <asp:Button ID = "btnUpdate" runat="server" Text = "Update" />
                                                                    <asp:Button ID = "btnCancel" runat = "server" Text="Cancel" />
                                                                    
                                                                  
                                                                   
                                                                    </td>
                                                                </tr>
                                
                                                            </table>

                                
                                 
                                </asp:Panel>



    </form>
</body>
</html>
