﻿Imports System.IO
Imports System.Data '
Imports System.Data.SqlClient
Imports App_Code
Imports AjaxControlToolkit
Imports C1.Web.UI.Controls.C1Window
Partial Class MyAccountQueue
    Inherits MyBasePage 'Inherits System.Web.UI.Page '
    Dim dsQueues As New DataSet("Queues")
    Dim cblBinder As New CheckBoxListBinder

    Dim sAccountGroup As String

    Dim Appinfo As New Applications
    Dim Apps As New Applications



    Dim BaseApps As New List(Of Application)
    Dim StartNewBaseApps As New List(Of MyApplication)

    'Dim OpenRequests As New RequestItems()
    Dim conn As New SetDBConnection()
    Dim sAccountType As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim Security As New UserSecurity(Session("LoginID"), Session("EmployeeConn"))
        Dim appOwnership As List(Of Integer)


        'SelApplicationGroups

        If Not IsPostBack Then

            ' returns a list of applications User checked to view
            appOwnership = Security.AppOwnership
            sAccountGroup = Security.ApplicationGroup


            Dim rblAppgroupBinder As New RadioButtonListBinder(rblAppGgroup, "SelApplicationGroups", Session("EmployeeConn"))

            rblAppgroupBinder.BindData("ApplicationGroup", "DisplayGroupName") '"AffiliationDescLower",

            If Session("ViewAccountRequestType") = "" Or Session("ViewAccountRequestType") = "add" Then
                Session("ViewAccountRequestType") = "add"
                HDViewType.Text = "add"
                'btnAdd #800000
                btnAdd.BackColor = Color.FromName("#800000")
                btnAdd.Height = Unit.Pixel(30)
                LblViewtype.Text = "Now Viewing Add Accounts"

                '006666

            End If

            If sAccountGroup <> "" Then
                rblAppGgroup.SelectedValue = sAccountGroup
                HDGroupType.Text = sAccountGroup
            Else
                rblAppGgroup.SelectedValue = "clinical"
                HDGroupType.Text = "clinical"

            End If


            ' This returns all open requests 
            ' OpenRequests = New RequestItems(Session("ViewAccountRequestType").ToString())

            ' this returns all applications only base numbers not individual sites 31


            If HDGroupType.Text = "clinical" Then

                Appinfo.GetAllApps(HttpContext.Current.Session("EmployeeConn").ToString)



                'Appinfo.GetAllApps(conn.EmployeeConn)
                Dim Appcount As Integer = 0
                Appcount = Appinfo.AllApps.Count

                'BaseApps = Apps.BaseApps()
                BaseApps = Apps.AppBySecurity()

                ' Bind checkBoxs to data
                'cblAccountQueues.Visible = True
                cblAccountQueues.DataSource = BaseApps
                cblAccountQueues.DataValueField = "ApplicationNum"
                cblAccountQueues.DataTextField = "ApplicationDesc"
                cblAccountQueues.DataBind()

                Dim checkBoxes = From it As ListItem In cblAccountQueues.Items
                                 Join own As Integer In appOwnership On it.Value Equals own.ToString
                                 Select it
                For Each itm In checkBoxes
                    itm.Selected = True
                Next

                'GetMyQueueDataSet()

            ElseIf HDGroupType.Text = "financial" Then

                Dim MyAppinfo As New AllMyApplications(sAccountGroup)
                Dim MyApps As New AllMyApplications(sAccountGroup)


                MyAppinfo.GetAllMyApps(HttpContext.Current.Session("EmployeeConn").ToString, sAccountGroup)

                Dim Appcount As Integer = 0
                Appcount = MyAppinfo.AllMyApps.Count
                StartNewBaseApps = MyApps.AppBySecurityGroup(sAccountGroup)

                cblAccountQueues.Dispose()
                tblAccountQueues.Dispose()


                ' Bind checkBoxs to data
                cblAccountQueues.DataSource = StartNewBaseApps
                cblAccountQueues.DataValueField = "ApplicationNum"
                cblAccountQueues.DataTextField = "ApplicationDesc"

                cblAccountQueues.DataBind()

                Dim MycheckBoxes = From it As ListItem In cblAccountQueues.Items
                                 Join own As Integer In appOwnership On it.Value Equals own.ToString
                                 Select it
                For Each itm In MycheckBoxes
                    itm.Selected = True
                Next



                'GetMyQueueDataSet()

            End If

        End If

        ViewWatch()

        GetQueueDataSet()


        'LblLastRefresh.Text = DateTime.Now.ToString()
    End Sub
    Public Sub GetQueueDataSet()


        tblAccountQueues.Rows.Clear()

        Dim OpenRequests As New RequestItems()

        OpenRequests = New RequestItems(Session("ViewAccountRequestType").ToString())

        Dim appHeaderCreated As Boolean = False
        Dim iRowCount As Integer = 1

        ' for each of the 31 base applications 
        For Each cblItem As ListItem In cblAccountQueues.Items
            ' if the app is checked then 
            If cblItem.Selected = True Then



                'now for each of the base apps that are checked
                For Each app As Application In Apps.ApplicationsByBase(cblItem.Value)
                    'New version I think
                    'For Each app As AllMyApplications In Appinfo.ApplicationsByBase(cblItem.Value)

                    appHeaderCreated = False


                    For Each ari As RequestItem In OpenRequests.GetRequestByAppNum(app.ApplicationNum).OrderByDescending(Function(x) x.AccountRequestNum)
                        If appHeaderCreated = False Then


                            Dim rwHeader As New TableRow
                            Dim rwColTitle As New TableRow

                            Dim clHeader As New TableCell
                            Dim clReqNumTitle As New TableCell
                            Dim clNameTitle As New TableCell
                            Dim clLoginTitle As New TableCell
                            Dim clDateEnteredTitle As New TableCell
                            Dim clAffiliatation As New TableCell

                            rwHeader.Font.Bold = True
                            rwHeader.BackColor = Color.FromName("#FFFFCC")
                            rwHeader.ForeColor = Color.FromName("Black")
                            rwHeader.Font.Size = 12

                            clHeader.ColumnSpan = 5

                            If Session("ViewAccountRequestType") = "add" Then
                                clHeader.Text = ari.ApplicationDesc & " Waiting for Completion"

                            ElseIf Session("ViewAccountRequestType") = "delete" Then

                                clHeader.Text = ari.ApplicationDesc & " Waiting to be Deleted"
                            Else
                                clHeader.Text = ari.ApplicationDesc & " PAST 30 Days Invalid Requests"
                            End If

                            rwHeader.Cells.Add(clHeader)

                            tblAccountQueues.Rows.Add(rwHeader)


                            rwColTitle.Font.Bold = True
                            rwColTitle.BackColor = Color.FromName("#006666")
                            rwColTitle.ForeColor = Color.FromName("White")
                            rwColTitle.Height = Unit.Pixel(36)

                            clReqNumTitle.Text = "Req #"
                            clNameTitle.Text = "Client Name"
                            clLoginTitle.Text = "Account Name"
                            clDateEnteredTitle.Text = "Date Entered"
                            clAffiliatation.Text = "Affiliation"

                            rwColTitle.Cells.Add(clReqNumTitle)
                            rwColTitle.Cells.Add(clNameTitle)
                            rwColTitle.Cells.Add(clLoginTitle)
                            rwColTitle.Cells.Add(clDateEnteredTitle)
                            rwColTitle.Cells.Add(clAffiliatation)

                            tblAccountQueues.Rows.Add(rwColTitle)

                            appHeaderCreated = True
                        End If
                        '----------------------------------------
                        ' End Header
                        '----------------------------------------

                        Dim rwData As New TableRow
                        Dim clReqNum As New TableCell
                        Dim clName As New TableCell
                        Dim clLogin As New TableCell
                        Dim clDateEntered As New TableCell
                        Dim clDependency As New TableCell
                        Dim clAffiliataion As New TableCell

                        If Session("ViewAccountRequestType") = "add" Then
                            sAccountType = "add"
                            clReqNum.Text = "<a href=""CreateAccount.aspx?requestnum=" & ari.AccountRequestNum & "&account_seq_num=" & ari.AccountRequestSeqNum & """ ><u><b> " & ari.AccountRequestNum & "</b></u></a>"
                        ElseIf Session("ViewAccountRequestType") = "delete" Then
                            sAccountType = "delete"

                            clReqNum.Text = "<a href=""DeleteAccount.aspx?requestnum=" & ari.AccountRequestNum & "&account_seq_num=" & ari.AccountRequestSeqNum & "&AccountType=" & sAccountType & """ ><u><b> " & ari.AccountRequestNum & "</b></u></a>"
                        Else
                            sAccountType = "invalid"

                            clReqNum.Text = "<a href=""DeleteAccount.aspx?requestnum=" & ari.AccountRequestNum & "&account_seq_num=" & ari.AccountRequestSeqNum & "&AccountType=" & sAccountType & """ ><u><b> " & ari.AccountRequestNum & "</b></u></a>"

                        End If

                        If ari.RequestCodeDesc = "Provider Demo Change" Then
                            clReqNum.Text = "<a href=""ProviderDemo.aspx?requestnum=" & ari.AccountRequestNum & "&account_seq_num=" & ari.AccountRequestSeqNum & """ ><u><b> " & ari.AccountRequestNum & "</b></u></a>"

                        End If
                        'End If

                        clName.Text = ari.ReceiverFullName
                        clLogin.Text = ari.LoginName
                        clDateEntered.Text = ari.EnteredDate
                        clAffiliataion.Text = ari.Affiliation

                        rwData.Cells.Add(clReqNum)
                        rwData.Cells.Add(clName)
                        rwData.Cells.Add(clLogin)
                        rwData.Cells.Add(clDateEntered)
                        rwData.Cells.Add(clAffiliataion)

                        If iRowCount > 0 Then
                            rwData.BackColor = Color.WhiteSmoke
                        Else
                            rwData.BackColor = Color.LightGray
                        End If

                        tblAccountQueues.Rows.Add(rwData)

                        iRowCount = iRowCount * -1

                    Next
                Next
            End If
        Next

        iRowCount = tblAccountQueues.Rows.Count()
        If iRowCount = 0 Then
            Dim rwHeader As New TableRow
            Dim rwColTitle As New TableRow

            Dim clHeader As New TableCell
            Dim clReqNumTitle As New TableCell
            Dim clNameTitle As New TableCell
            Dim clLoginTitle As New TableCell
            Dim clDateEnteredTitle As New TableCell
            Dim clAffiliatation As New TableCell

            rwHeader.Font.Bold = True
            rwHeader.BackColor = Color.FromName("#FFFFCC")
            rwHeader.ForeColor = Color.FromName("Black")
            rwHeader.Font.Size = 12

            clHeader.ColumnSpan = 5
            clHeader.Text = " No Rows To Display "

            rwHeader.Cells.Add(clHeader)

            tblAccountQueues.Rows.Add(rwHeader)




        End If


    End Sub
    Protected Sub cblAccountQueues_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cblAccountQueues.SelectedIndexChanged
        If Session("ViewAccountRequestType") Is Nothing Then
            Session("ViewAccountRequestType") = "add"
        End If



        Dim Security As New UserSecurity(Session("LoginID"), Session("EmployeeConn"))
        Dim appOwnership As List(Of Integer)

        ' returns a list of applications User checked to view
        appOwnership = Security.AppOwnership


        If HDGroupType.Text = "clinical" Then

            GetQueueDataSet()

        ElseIf HDGroupType.Text = "financial" Then
            GetMyQueueDataSet()
        Else
            GetQueueDataSet()

        End If




    End Sub
    Protected Sub ViewWatch()

        sAccountGroup = rblAppGgroup.SelectedValue
        HDGroupType.Text = rblAppGgroup.SelectedValue

        If sAccountGroup = "" Then

            rblAppGgroup.SelectedValue = "clinical"
            HDGroupType.Text = "clinical"

        End If

        Select Case Session("ViewAccountRequestType")
            Case "add"
                btnAdd.BackColor = Color.FromName("#800000")
                btnAdd.Height = Unit.Pixel(30)
                LblViewtype.Text = "Now Viewing Add Accounts"

                btnDelete.BackColor = Color.FromName("#006666")
                btnInvalid.BackColor = Color.FromName("#006666")


            Case "delete"
                btnDelete.BackColor = Color.FromName("#800000")
                btnDelete.Height = Unit.Pixel(30)
                LblViewtype.Text = "Now Viewing Delete Accounts"

                '006666
                btnAdd.BackColor = Color.FromName("#006666")
                btnInvalid.BackColor = Color.FromName("#006666")

            Case "invalid"

                btnInvalid.BackColor = Color.FromName("#800000")
                btnInvalid.Height = Unit.Pixel(30)
                LblViewtype.Text = "Now Viewing Invalid Accounts"

                '006666
                btnAdd.BackColor = Color.FromName("#006666")
                btnDelete.BackColor = Color.FromName("#006666")

            Case Else
                btnAdd.BackColor = Color.FromName("#800000")
                btnAdd.Height = Unit.Pixel(30)
                LblViewtype.Text = "Now Viewing Add Accounts"

                btnDelete.BackColor = Color.FromName("#006666")
                btnInvalid.BackColor = Color.FromName("#006666")

        End Select
    End Sub
    Protected Sub Timer1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        'works

        LblLastRefresh.Text = "Last Refreshed -> " & DateTime.Now.ToString()
        'Session("LastUnitviewRefresh") = LblLastRefresh.Text
        'SqlDataSource1.DataBind()


    End Sub
    Protected Sub rblAppGgroup_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles rblAppGgroup.SelectedIndexChanged

        If rblAppGgroup.SelectedValue <> "" Then
            sAccountGroup = rblAppGgroup.SelectedValue
            HDGroupType.Text = rblAppGgroup.SelectedValue

        ElseIf HDGroupType.Text <> "" Then

            rblAppGgroup.SelectedValue = HDGroupType.Text
            sAccountGroup = HDGroupType.Text

        End If

        Dim MyAppinfo As New AllMyApplications(sAccountGroup)
        Dim MyApps As New AllMyApplications(sAccountGroup)

        Dim NewBaseApps As New List(Of MyApplication)


        Dim Security As New UserSecurity(Session("LoginID"), Session("EmployeeConn"))
        Dim appOwnership As List(Of Integer)

        ' returns a list of applications User checked to view
        appOwnership = Security.AppOwnership


        MyAppinfo.GetAllMyApps(HttpContext.Current.Session("EmployeeConn").ToString, sAccountGroup)

        Dim Appcount As Integer = 0
        Appcount = MyAppinfo.AllMyApps.Count
        NewBaseApps = MyApps.AppBySecurityGroup(sAccountGroup)

        cblAccountQueues.Dispose()
        tblAccountQueues.Dispose()


        ' Bind checkBoxs to data
        cblAccountQueues.DataSource = NewBaseApps
        cblAccountQueues.DataValueField = "ApplicationNum"
        cblAccountQueues.DataTextField = "ApplicationDesc"

        cblAccountQueues.DataBind()

        Dim MycheckBoxes = From it As ListItem In cblAccountQueues.Items
                         Join own As Integer In appOwnership On it.Value Equals own.ToString
                         Select it
        For Each itm In MycheckBoxes
            itm.Selected = True
        Next

        If HDGroupType.Text = "clinical" Then
            tblAccountQueues.Rows.Clear()

            GetQueueDataSet()

        ElseIf HDGroupType.Text = "financial" Then

            tblAccountQueues.Rows.Clear()


            GetMyQueueDataSet()
        Else
            tblAccountQueues.Rows.Clear()



            GetQueueDataSet()

        End If

    End Sub
    Public Sub GetMyQueueDataSet()

        tblAccountQueues.Rows.Clear()

        Dim OpenRequests As New RequestItems()
        OpenRequests = New RequestItems(Session("ViewAccountRequestType").ToString())

        Dim appHeaderCreated As Boolean = False

        Dim MyApps As New AllMyApplications(HDGroupType.Text)
        Dim iRowCount As Integer = 1


        ' for each of the 31 base applications 
        For Each cblItem As ListItem In cblAccountQueues.Items
            ' if the app is checked then 
            If cblItem.Selected = True Then



                'now for each of the base apps that are checked
                For Each app As MyApplication In MyApps.ApplicationsNewByBase(cblItem.Value)
                    'New version I think
                    'For Each app As AllMyApplications In Appinfo.ApplicationsByBase(cblItem.Value)


                    appHeaderCreated = False

                    For Each ari As RequestItem In OpenRequests.GetRequestByAppNum(app.ApplicationNum).OrderByDescending(Function(x) x.AccountRequestNum)

                        If appHeaderCreated = False Then


                            Dim rwHeader As New TableRow
                            Dim rwColTitle As New TableRow

                            Dim clHeader As New TableCell
                            Dim clReqNumTitle As New TableCell
                            Dim clNameTitle As New TableCell
                            Dim clLoginTitle As New TableCell
                            Dim clDateEnteredTitle As New TableCell
                            Dim clAffiliatation As New TableCell

                            rwHeader.Font.Bold = True
                            rwHeader.BackColor = Color.FromName("#FFFFCC")
                            rwHeader.ForeColor = Color.FromName("Black")
                            rwHeader.Font.Size = 12

                            clHeader.ColumnSpan = 5

                            If Session("ViewAccountRequestType") = "add" Then
                                clHeader.Text = ari.ApplicationDesc & " Waiting for Completion"

                            ElseIf Session("ViewAccountRequestType") = "delete" Then

                                clHeader.Text = ari.ApplicationDesc & " Waiting to be Deleted"
                            Else
                                clHeader.Text = ari.ApplicationDesc & " PAST 30 Days Invalid Requests"
                            End If

                            rwHeader.Cells.Add(clHeader)

                            tblAccountQueues.Rows.Add(rwHeader)


                            rwColTitle.Font.Bold = True
                            rwColTitle.BackColor = Color.FromName("#006666")
                            rwColTitle.ForeColor = Color.FromName("White")
                            rwColTitle.Height = Unit.Pixel(36)

                            clReqNumTitle.Text = "Req #"
                            clNameTitle.Text = "Client Name"
                            clLoginTitle.Text = "Account Name"
                            clDateEnteredTitle.Text = "Date Entered"
                            clAffiliatation.Text = "Affiliation"

                            rwColTitle.Cells.Add(clReqNumTitle)
                            rwColTitle.Cells.Add(clNameTitle)
                            rwColTitle.Cells.Add(clLoginTitle)
                            rwColTitle.Cells.Add(clDateEnteredTitle)
                            rwColTitle.Cells.Add(clAffiliatation)

                            tblAccountQueues.Rows.Add(rwColTitle)

                            appHeaderCreated = True
                        End If
                        '----------------------------------------
                        ' End Header
                        '----------------------------------------

                        Dim rwData As New TableRow
                        Dim clReqNum As New TableCell
                        Dim clName As New TableCell
                        Dim clLogin As New TableCell
                        Dim clDateEntered As New TableCell
                        Dim clDependency As New TableCell
                        Dim clAffiliataion As New TableCell

                        If Session("ViewAccountRequestType") = "add" Then
                            sAccountType = "add"
                            clReqNum.Text = "<a href=""CreateAccount.aspx?requestnum=" & ari.AccountRequestNum & "&account_seq_num=" & ari.AccountRequestSeqNum & """ ><u><b> " & ari.AccountRequestNum & "</b></u></a>"
                        ElseIf Session("ViewAccountRequestType") = "delete" Then
                            sAccountType = "delete"

                            clReqNum.Text = "<a href=""DeleteAccount.aspx?requestnum=" & ari.AccountRequestNum & "&account_seq_num=" & ari.AccountRequestSeqNum & "&AccountType=" & sAccountType & """ ><u><b> " & ari.AccountRequestNum & "</b></u></a>"
                        Else
                            sAccountType = "invalid"

                            clReqNum.Text = "<a href=""DeleteAccount.aspx?requestnum=" & ari.AccountRequestNum & "&account_seq_num=" & ari.AccountRequestSeqNum & "&AccountType=" & sAccountType & """ ><u><b> " & ari.AccountRequestNum & "</b></u></a>"

                        End If

                        If ari.RequestCodeDesc = "Provider Demo Change" Then
                            clReqNum.Text = "<a href=""ProviderDemo.aspx?requestnum=" & ari.AccountRequestNum & "&account_seq_num=" & ari.AccountRequestSeqNum & """ ><u><b> " & ari.AccountRequestNum & "</b></u></a>"

                        End If
                        'End If

                        clName.Text = ari.ReceiverFullName
                        clLogin.Text = ari.LoginName
                        clDateEntered.Text = ari.EnteredDate
                        clAffiliataion.Text = ari.Affiliation

                        'If Not sDependency Is Nothing Then

                        '    clDependency.Text = sDependency

                        '    clName.Text = clName.Text & "<br>" & sDependency

                        'End If

                        rwData.Cells.Add(clReqNum)
                        rwData.Cells.Add(clName)
                        rwData.Cells.Add(clLogin)
                        rwData.Cells.Add(clDateEntered)
                        rwData.Cells.Add(clAffiliataion)

                        If iRowCount > 0 Then

                            ' rwData.BackColor = Color.Bisque

                            rwData.BackColor = Color.WhiteSmoke

                        Else

                            rwData.BackColor = Color.LightGray

                        End If

                        tblAccountQueues.Rows.Add(rwData)

                        iRowCount = iRowCount * -1
                    Next
                Next
            End If
        Next
        iRowCount = tblAccountQueues.Rows.Count()

        If iRowCount = 0 Then
            Dim rwHeader As New TableRow
            Dim rwColTitle As New TableRow

            Dim clHeader As New TableCell
            Dim clReqNumTitle As New TableCell
            Dim clNameTitle As New TableCell
            Dim clLoginTitle As New TableCell
            Dim clDateEnteredTitle As New TableCell
            Dim clAffiliatation As New TableCell

            rwHeader.Font.Bold = True
            rwHeader.BackColor = Color.FromName("#FFFFCC")
            rwHeader.ForeColor = Color.FromName("Black")
            rwHeader.Font.Size = 12

            clHeader.ColumnSpan = 5
            clHeader.Text = " No Rows To Display "

            rwHeader.Cells.Add(clHeader)

            tblAccountQueues.Rows.Add(rwHeader)




        End If
    End Sub
    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Session("ViewAccountRequestType") = "add"
        'Dim OpenRequests As New RequestItems()
        'OpenRequests = New RequestItems(Session("ViewAccountRequestType").ToString())
        'btnAdd
        HDViewType.Text = "add"

        btnAdd.BackColor = Color.FromName("#800000")
        btnAdd.Height = Unit.Pixel(30)
        LblViewtype.Text = "Now Viewing Add Accounts"
        '006666
        btnDelete.BackColor = Color.FromName("#006666")
        btnInvalid.BackColor = Color.FromName("#006666")

        If HDGroupType.Text = "clinical" Then
            tblAccountQueues.Rows.Clear()
            GetQueueDataSet()

        ElseIf HDGroupType.Text = "financial" Then
            tblAccountQueues.Rows.Clear()
            GetMyQueueDataSet()
        Else
            tblAccountQueues.Rows.Clear()
            GetQueueDataSet()

        End If

    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Session("ViewAccountRequestType") = "delete"
        HDViewType.Text = "delete"

        'Dim OpenRequests As New RequestItems()
        'OpenRequests = New RequestItems(Session("ViewAccountRequestType").ToString())

        btnDelete.BackColor = Color.FromName("#800000")
        btnDelete.Height = Unit.Pixel(30)
        LblViewtype.Text = "Now Viewing Delete Accounts"

        '006666
        btnAdd.BackColor = Color.FromName("#006666")
        btnInvalid.BackColor = Color.FromName("#006666")


        If HDGroupType.Text = "clinical" Then
            tblAccountQueues.Rows.Clear()
            GetQueueDataSet()

        ElseIf HDGroupType.Text = "financial" Then
            tblAccountQueues.Rows.Clear()
            GetMyQueueDataSet()
        Else
            tblAccountQueues.Rows.Clear()
            GetQueueDataSet()

        End If

    End Sub
    Protected Sub btnInvalid_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnInvalid.Click
        Session("ViewAccountRequestType") = "invalid"
        HDViewType.Text = "invalid"

        'Dim OpenRequests As New RequestItems()
        'OpenRequests = New RequestItems(Session("ViewAccountRequestType").ToString())
        'btnInvalid

        btnInvalid.BackColor = Color.FromName("#800000")
        btnInvalid.Height = Unit.Pixel(30)
        LblViewtype.Text = "Now Viewing Invalid Accounts"

        '006666
        btnAdd.BackColor = Color.FromName("#006666")
        btnDelete.BackColor = Color.FromName("#006666")

        If HDGroupType.Text = "clinical" Then
            tblAccountQueues.Rows.Clear()
            GetQueueDataSet()

        ElseIf HDGroupType.Text = "financial" Then
            tblAccountQueues.Rows.Clear()
            GetMyQueueDataSet()
        Else
            tblAccountQueues.Rows.Clear()
            GetQueueDataSet()

        End If


    End Sub


End Class
