﻿Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports App_Code
Imports AjaxControlToolkit
Imports C1.Web.UI.Controls.C1Window
Imports System.Net.Mail

Partial Class SignOnFormV2
    Inherits MyBasePage '   Inherits System.Web.UI.Page
    Dim sActivitytype As String = ""
    Dim FormSignOn As FormSQL
    Dim AccountRequestNum As Integer
    Dim Cred As String
    Dim Provider As String
    Dim Han As String
    Dim sEmployeeNum As String
    Dim bHasError As Boolean
    Dim thisRole As String
    Dim Searched As Boolean = False
    Dim sTested As String

    Dim dtAdjustedRoles As DataTable
    Dim pageCtrl As New PageControls()
    Delegate Function getgridcellNoE(ByVal sFieldName As String, ByRef gridview1 As GridView) As Integer

    Dim getcellNoE As New getgridcellNoE(AddressOf GridViewFunctionsCls.GetGridCellIndexNoE)

    ' Dim UserInfo As UserSecurity
    ' Dim conn As New SetDBConnection()
    ' Dim c3Controller As New Client3Controller
    ' Dim c3Submitter As New Client3
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'Dim Envoirn As New Environment()
        'thisenviro.Text = Envoirn.getEnvironment
        'testing managers accounts
        'Session("LoginID") = "defs00"
        If lblSearched.Text = "" Then
            SearchPopup.Show()
        End If
        FormSignOn = New FormSQL(Session("EmployeeConn"), "SelAccountRequestV20", "InsAccountRequestV20", "", Page)

        Dim UserInfo = New UserSecurity(Session("LoginID"), Session("EmployeeConn"))


        Dim rblAffiliateBinder As New RadioButtonListBinder(emp_type_cdrbl, "SelAffiliationsV5", Session("EmployeeConn"))

        If Session("SecurityLevelNum") = 50 Then
            rblAffiliateBinder.AddDDLCriteria("@type", "phys", SqlDbType.NVarChar)

        ElseIf Session("SecurityLevelNum") = 45 Then
            rblAffiliateBinder.AddDDLCriteria("@type", "emp", SqlDbType.NVarChar)
        End If

        If Session("SecurityLevelNum") < 60 Then
            cblApplications.Enabled = False
        End If

        If Not IsPostBack Then
            'lblusersubmit
            'userSubmittingLabel
            'SubmittingOnBehalfOfNameLabel
            pnlProvider.Visible = False
            cblApplications.Visible = False

            userSubmittingLabel.Text = UserInfo.UserName
            SubmittingOnBehalfOfNameLabel.Text = UserInfo.UserName
            requestor_client_numLabel.Text = UserInfo.ClientNum
            submitter_client_numLabel.Text = UserInfo.ClientNum
            userDepartmentCD.Text = UserInfo.DepartmentCd



            Dim ddlBinder As New DropDownListBinder

            'Dim cblBinder As New CheckBoxListBinder(cblApplications, "SelApplicationCodes", Session("EmployeeConn"))
            'cblBinder.AddDDLCriteria("@AppSystem", "base", SqlDbType.NVarChar)
            'cblBinder.BindData("ApplicationNum", "ApplicationDesc")


            Dim cblFacilitesBinder As New CheckBoxListBinder(cblFacilities, "SelFacilityEntity", Session("EmployeeConn"))
            cblFacilitesBinder.AddDDLCriteria("@EntityFacility", "Facility", SqlDbType.NVarChar)
            cblFacilitesBinder.BindData("Code", "EntityFacilityDesc")

            Dim cblEntityBinder As New CheckBoxListBinder(cblEntities, "SelFacilityEntity", Session("EmployeeConn"))
            cblEntityBinder.AddDDLCriteria("@EntityFacility", "Entity", SqlDbType.NVarChar)
            cblEntityBinder.BindData("Code", "EntityFacilityDesc")

            rblAffiliateBinder.ToolTip = False
            rblAffiliateBinder.BindData("RoleNum", "AffiliationDisplay") '"AffiliationDescLower",


            ddlBinder.BindData(Rolesddl, "SelEmployeeRolesV2", "rolenum", "roledesc", Session("EmployeeConn"))
            ddlBinder.BindData(Specialtyddl, "SelSpecialtyCodesV2", "specialty", "specialty", Session("EmployeeConn"))
            ddlBinder.BindData(facility_cdddl, "SelFacilityCodes", "facility_cd", "facility_short_name", Session("EmployeeConn"))
            ddlBinder.BindData(group_nameddl, "SelPhysGroups", "group_name", "group_name", Session("EmployeeConn"))
            ddlBinder.BindData(suffixddl, "SelSuffixCodes", "Suffix", "Suffix", Session("EmployeeConn"))

            ddlBinder.BindData(entity_cdDDL, "sel_entity_codes", "entity_cd", "hosp_desc", Session("EmployeeConn"))


            department_cdddl.Items.Add("Select Entity")
            building_cdddl.Items.Add("Select Facility")

            'If providerrbl.Text = "Yes" Then
            '    ' only 90 security level and above edit these fields
            '    If Session("SecurityLevelNum") < 90 Then
            '        Dim ctrl As Control = DataCheckingAndValidation.SetChildControlsRecursiveToEnabledFalse(uplSignOnForm, False)

            '    End If


            'End If

        End If
        'tblprovider
        ' ColorCodeAccounts()
        '  rblAffiliateBinder.addToolTips()
        'If Session("SecurityLevelNum") > 102 Then
        '    thisenviro.Visible = True

        'End If

    End Sub

    Protected Sub Titleddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles Titleddl.SelectedIndexChanged
        TitleLabel.Text = Titleddl.SelectedValue

    End Sub
    Protected Sub Specialtyddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles Specialtyddl.SelectedIndexChanged
        SpecialtLabel.Text = Specialtyddl.SelectedValue

    End Sub


    Protected Sub cblApplications_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cblApplications.SelectedIndexChanged

        Dim BusLog As New AccountBusinessLogic
        '   BusLog.CheckAccountDependancy(cblApplications)

        If BusLog.eCare Then
            pnlTCl.Visible = True
            'Dim reqField As New RequiredField(TCLTextBox, "TCL for eCare")
        Else
            ' pnlTCl.Visible = False
            pageCtrl.RemoveCssClass(pnlTCl, "textInputRequired")
        End If

    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        '  now see if person already exists
        If sTested <> "yes" Then
            CheckExists()
        End If

        If emp_type_cdrbl.SelectedIndex = -1 Then

            lblValidation.Text = "Must select an Affiliation"
            lblValidation.Focus()
            Return


        End If

        If Not checkFacilities() Then

            lblValidation.Text = "Must select a Hospital"
            Return
        End If

        If Not checkEntities() Then

            lblValidation.Text = "Must select an Entity"
            Return

        End If

        If Not checkApplications() Then

            lblValidation.Text = "Must select Application to Access"
            Return

        End If

        CheckRequiredFields()

        Select Case emp_type_cdLabel.Text.ToLower
            Case "physician", "non staff clinician", "resident", "allied health"
                If TitleLabel.Text = "" Then
                    lblValidation.Text = "Must supply Title"
                    Titleddl.Focus()
                    Exit Sub
                End If
                'Specialtyddl
                If SpecialtLabel.Text = "" Then
                    lblValidation.Text = "Must supply Specialty"
                    Specialtyddl.Focus()
                    Exit Sub
                End If

        End Select

        For Each cbFacility As ListItem In cblFacilities.Items
            If cbFacility.Selected Then
                If cbFacility.ToString.ToLower.Trim = "community" Then
                    For Each cbApp As ListItem In cblApplications.Items
                        If cbApp.Selected Then

                            Select Case cbApp.ToString.ToLower.Trim
                                Case "ebed", "ecase", "ecare", "pacs", "pharmacy", "picis (emergency dept.)", "edm"
                                    lblValidation.Text = "InValid Application for Community -> " & cbApp.ToString
                                    lblValidation.Focus()

                                    Return


                            End Select
                        End If

                    Next

                End If


            End If
        Next


        If bHasError = True Then
            lblValidation.Focus()
            Exit Sub
        Else
            lblValidation.Text = ""
        End If

        Dim BusLog As New AccountBusinessLogic
        Dim thisdata As New CommandsSqlAndOleDb("SelApplicationCodes", Session("EmployeeConn"))
        Dim AccountCode As String


        thisdata.ExecNonQueryNoReturn()

        BusLog.Accounts = thisdata.GetSqlDataTable


        'FormSignOn.AddInsertParm("@submitter_client_num", submitter_client_numLabel.Text, SqlDbType.NVarChar, 10)

        FormSignOn.AddInsertParm("@account_request_type", "addnew", SqlDbType.NVarChar, 9)

        FormSignOn.UpdateFormReturnReqNum()

        AccountRequestNum = FormSignOn.AccountRequestNum

        For Each cbItem As ListItem In cblApplications.Items
            If cbItem.Selected Then

                If BusLog.isHospSpecific(cbItem.Value) Then
                    For Each cbFacility As ListItem In cblFacilities.Items

                        If cbFacility.Selected Then

                            AccountCode = BusLog.GetApplicationCode(cbItem.Value, cbFacility.Value)

                            insAcctItems(AccountRequestNum, AccountCode, request_descTextBox.Text)

                        End If

                    Next

                ElseIf BusLog.isEntSpecific(cbItem.Value) Then

                    For Each cbEnt As ListItem In cblEntities.Items

                        If cbEnt.Selected Then

                            AccountCode = BusLog.GetApplicationCode(cbItem.Value, cbEnt.Value)

                            insAcctItems(AccountRequestNum, AccountCode, request_descTextBox.Text)

                        End If

                    Next


                Else

                    insAcctItems(AccountRequestNum, cbItem.Value, request_descTextBox.Text)

                End If

            End If

        Next

        If Session("environment") = "" Then
            Dim Envoirn As New Environment()
            thisenviro.Text = Envoirn.getEnvironment
            Session("environment") = Envoirn.getEnvironment
        End If



        If Session("environment").ToString.ToLower <> "testing" Then

            If providerrbl.SelectedValue = "Yes" Then

                SendProviderEmail(AccountRequestNum)
            End If

        End If

        Response.Redirect("~/RequestSubmited.aspx?RequestNum=" & AccountRequestNum)



    End Sub
    Private Sub CheckExists()
        CheckRequiredFields()
        Dim Submitternum As Integer
        Submitternum = submitter_client_numLabel.Text


        Dim ExistsClientsData As New CommandsSqlAndOleDb("SelExistingClients", Session("EmployeeConn"))
        ExistsClientsData.AddSqlProcParameter("@FirstName", first_nameTextBox.Text, SqlDbType.NVarChar, 20)
        ExistsClientsData.AddSqlProcParameter("@LastName", last_nameTextBox.Text, SqlDbType.NVarChar, 20)
        ExistsClientsData.AddSqlProcParameter("@SubmitterClientNum", Submitternum, SqlDbType.Int, 9)


        grvClientsExists.DataSource = ExistsClientsData.GetSqlDataTable
        grvClientsExists.DataBind()


        'UpdPnlExists.Update()
        ClientExists.Show()


    End Sub
    Private Sub insAcctItems(ByVal requestNum As Integer, ByVal appNum As String, ByVal comments As String)

        Dim dt As New DataTable
        Dim thisdata As New CommandsSqlAndOleDb("InsAccountItemsV3", Session("EmployeeConn"))
        thisdata.AddSqlProcParameter("@account_request_num", requestNum, SqlDbType.Int, 20)
        thisdata.AddSqlProcParameter("@ApplicationNum", appNum, SqlDbType.Int, 8)
        thisdata.AddSqlProcParameter("@account_item_desc", comments, SqlDbType.NVarChar, 255)

        Select Case emp_type_cdrbl.SelectedValue.ToLower

            Case "physician", "non staff clinician", "resident", "allied health"
                thisdata.AddSqlProcParameter("@validate", "NeedsDocNum", SqlDbType.NVarChar, 20)

            Case Else
                thisdata.AddSqlProcParameter("@validate", "NeedsValidation", SqlDbType.NVarChar, 20)

        End Select
        thisdata.AddSqlProcParameter("@who_nt_login", Session("LoginID"), SqlDbType.NVarChar, 50)

        thisdata.ExecNonQueryNoReturn()



    End Sub
    Protected Sub facility_cdddl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles facility_cdddl.SelectedIndexChanged
        Dim ddlBinder As DropDownListBinder


        ddlBinder = New DropDownListBinder(building_cdddl, "sel_building_by_facility", Session("EmployeeConn"))
        ddlBinder.AddDDLCriteria("@facility_cd", facility_cdddl.SelectedValue, SqlDbType.NVarChar)
        ddlBinder.BindData("building_cd", "building_name")


    End Sub


    Protected Sub Rolesddl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Rolesddl.SelectedIndexChanged

        Dim rwRef() As DataRow
        Dim ddlBinderV2 As DropDownListBinder
        ddlBinderV2 = New DropDownListBinder(Rolesddl, "SelEmployeeRolesV2", Session("EmployeeConn"))

        'Rolesddl.Items.Clear()
        'Rolesddl.Dispose()



        ' if the Affiliation is not selsected then populate 
        If emp_type_cdrbl.SelectedValue = "" Then
            Select Case Rolesddl.SelectedValue
                ' Default to Vendor
                Case "1198", "527", "1176", "1177", "1178", "1179", "1180", "1182", "1183", "1185", "1186", "1190", "1191"
                    emp_type_cdrbl.SelectedValue = "1198"

                    ' Employee
                Case "1193", "1169", "1170", "1171", "1172", "1173", "1174", "1188", "1192"
                    emp_type_cdrbl.SelectedValue = "1193"

                    ' Student
                Case "1189"
                    emp_type_cdrbl.SelectedValue = "1189"

                    'resident
                Case "1194"
                    emp_type_cdrbl.SelectedValue = "1194"

                    'Contractor
                Case "1195"
                    emp_type_cdrbl.SelectedValue = "1195"

                    'Non Staff Clinician
                Case "1197"
                    emp_type_cdrbl.SelectedValue = "1197"
                    'thisRole = "1197"

                    'Non CKHN Staff
                Case "1199"
                    emp_type_cdrbl.SelectedValue = "1199"
                    'thisRole = "1199"


                    'Physician
                Case "782"
                    emp_type_cdrbl.SelectedValue = "782"
                    'thisRole = "782"


                    'Allied Health
                Case "1200"
                    emp_type_cdrbl.SelectedValue = "1200"
                    'thisRole = "1200"

                    'Other
                Case "1196"
                    emp_type_cdrbl.SelectedValue = "1196"
                    'thisRole = "1196"

            End Select
        Else
            If Rolesddl.SelectedValue.ToString = "Select" And emp_type_cdrbl.SelectedValue <> "" Then
                Rolesddl.SelectedValue = emp_type_cdrbl.SelectedValue
            End If

            If thisRole = "" And Rolesddl.SelectedValue <> "" Then
                thisRole = Rolesddl.SelectedValue
            End If

            If Rolesddl.SelectedValue <> thisRole Then
                Rolesddl.Items.Clear()

                ddlBinderV2.AddDDLCriteria("@RoleNum", thisRole, SqlDbType.NVarChar)

                thisRole = Rolesddl.SelectedValue

            End If

            ' always have a Rolenum
            Rolesddl.SelectedValue = thisRole



        End If



        If Rolesddl.SelectedValue.ToString <> "Select" And Rolesddl.SelectedValue.ToString <> "" Then
            user_position_descTextBox.Text = Rolesddl.SelectedValue.ToString
            'user_position_descTextBox.Text = sender
            'thisRole = Rolesddl.SelectedValue.ToString

        End If


        ddlBinderV2.BindData("rolenum", "roledesc")

        'If thisRole > 0 Then

        'End If


        If Rolesddl.SelectedValue.ToString = "Select" And thisRole <> "" Then
            Rolesddl.SelectedValue = thisRole
        End If

        Dim cblBinder As New CheckBoxListBinder(cblApplications, "SelApplicationExtensionByRoleNum", Session("EmployeeConn"))

        If Rolesddl.SelectedValue.ToString <> emp_type_cdrbl.SelectedValue.ToString Then
            cblBinder.AddDDLCriteria("@RoleNum", emp_type_cdrbl.SelectedValue.ToString, SqlDbType.NVarChar)

        Else
            cblBinder.AddDDLCriteria("@RoleNum", Rolesddl.SelectedValue.ToString, SqlDbType.NVarChar)


        End If


        cblBinder.BindData("ApplicationNum", "ApplicationDesc")
        cblApplications.Visible = True


        For Each cblItem As ListItem In cblApplications.Items
            Dim sCurritem As String = cblItem.Value
            cblItem.Selected = False

            rwRef = getApplicationsByRole.Select("ApplicationNum='" & cblItem.Value & "'")

            If rwRef.Count > 0 Then

                cblItem.Selected = True

            End If
        Next

        Dim BusLog As New AccountBusinessLogic

        If BusLog.eCare Then
            pnlTCl.Visible = True
        Else
            pnlTCl.Visible = False
            pageCtrl.RemoveCssClass(pnlTCl, "textInputRequired")

        End If

    End Sub

    Protected Function getApplicationsByRole() As DataTable
        Dim thisdata As New CommandsSqlAndOleDb("SelApplicationsByRole", Session("EmployeeConn"))

        If Rolesddl.SelectedValue.ToString = "Select" And thisRole <> "" Then
            Rolesddl.SelectedValue = thisRole
        End If

        thisdata.AddSqlProcParameter("@roleNum", Rolesddl.SelectedValue.ToString, SqlDbType.NVarChar, 9)



        thisdata.ExecNonQueryNoReturn()

        Return thisdata.GetSqlDataTable

    End Function
    Protected Sub btnReturn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReturn.Click
        '  BusinessLogic()
        If AccountRequestNum > 0 Then

            Response.Redirect("Myrequest.aspx")

        Else
            Response.Redirect("SimpleSearch.aspx")

        End If

    End Sub
    Protected Sub emp_type_cdrbl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles emp_type_cdrbl.SelectedIndexChanged

        For Each cblItem As ListItem In cblApplications.Items
            cblItem.Selected = False
        Next




        Dim removeCss As New PageControls(Page)
        removeCss.RemoveCssClass(Page, "textInputRequired")


        emp_type_cdLabel.Text = emp_type_cdrbl.SelectedValue
        'Dim srolenum As String
        'srolenum = emp_type_cdrbl.SelectedItem


        Select Case emp_type_cdrbl.SelectedValue
            'Case "physician/rovider", "non staff clinician", "resident"

            Case "782", "1197", "1194"

                Dim ddbTitleBinder As New DropDownListBinder(Titleddl, "SelTitlesV4", Session("EmployeeConn"))
                ddbTitleBinder.AddDDLCriteria("@titleType", "Physician", SqlDbType.NVarChar)
                ddbTitleBinder.BindData("TitleShortDesc", "TitleDesc")

                If TitleLabel.Text = "" Then
                    Titleddl.SelectedIndex = -1
                Else
                    Titleddl.SelectedValue = TitleLabel.Text

                End If

                If SpecialtLabel.Text = "" Then
                    Specialtyddl.SelectedIndex = -1
                Else
                    Specialtyddl.SelectedValue = SpecialtLabel.Text

                End If

                thisRole = emp_type_cdrbl.SelectedValue

                pnlProvider.Visible = True
                'Case "allied health"
            Case "1200"

                Dim ddbTitleBinder As New DropDownListBinder(Titleddl, "SelTitlesV4", Session("EmployeeConn"))
                ddbTitleBinder.AddDDLCriteria("@titleType", "Alliedhealth", SqlDbType.NVarChar)
                ddbTitleBinder.BindData("TitleShortDesc", "TitleDesc")


                If TitleLabel.Text = "" Then
                    Titleddl.SelectedIndex = -1
                Else
                    Titleddl.SelectedValue = TitleLabel.Text

                End If

                If SpecialtLabel.Text = "" Then
                    Specialtyddl.SelectedIndex = -1
                Else
                    Specialtyddl.SelectedValue = SpecialtLabel.Text

                End If

                pnlProvider.Visible = True
                thisRole = emp_type_cdrbl.SelectedValue

            Case Else
                pnlProvider.Visible = False

                thisRole = emp_type_cdrbl.SelectedValue


        End Select



        CheckRequiredFields()
        If bHasError = True Then
            lblValidation.Focus()
            Exit Sub
        Else
            lblValidation.Text = ""
        End If


        ' Rolesddl.SelectedValue = thisRole
        Rolesddl_SelectedIndexChanged(thisRole, EventArgs.Empty)


    End Sub
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click

        Dim thisData As New CommandsSqlAndOleDb("SelSimpleSearchV2", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@FirstName", FirstNameTextBox.Text, SqlDbType.NVarChar, 20)
        thisData.AddSqlProcParameter("@LastName", LastNameTextBox.Text, SqlDbType.NVarChar, 20)

        gvSearch.DataSource = thisData.GetSqlDataTable
        gvSearch.DataBind()

        uplOnBehalfOf.Update()
        mpeOnBehalfOf.Show()

    End Sub
    Protected Sub gvSearch_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvSearch.RowCommand

        If e.CommandName = "Select" Then
            Dim client_num As String = gvSearch.DataKeys(Convert.ToInt32(e.CommandArgument))("client_num").ToString()
            Dim login_name As String = gvSearch.DataKeys(Convert.ToInt32(e.CommandArgument))("login_name").ToString()
            Dim dbAccess As New CkhsEmployeeDbAccess(Session("EmployeeConn"))
            Dim ckhsEmployee = dbAccess.GetCkhsEmployee(client_num)

            'c3Controller = New Client3Controller(client_num)
            'c3Submitter = c3Controller.GetClientByClientNum(client_num)


            SubmittingOnBehalfOfNameLabel.Text = ckhsEmployee.FullName & " (" & ckhsEmployee.EMail & ")"
            requestor_client_numLabel.Text = client_num
            'btnUpdateSubmitter.Visible = True


            gvSearch.SelectedIndex = -1
            gvSearch.Dispose()

            'mpeOnBehalfOf.Show()


        End If
    End Sub
    Protected Function checkFacilities() As Boolean
        Dim blnReturn = False

        For Each item As ListItem In cblFacilities.Items
            If item.Selected Then
                blnReturn = True

            End If


        Next


        Return blnReturn

    End Function
    Protected Function checkApplications() As Boolean
        Dim blnReturn = False

        For Each item As ListItem In cblApplications.Items
            If item.Selected Then
                blnReturn = True

            End If


        Next


        Return blnReturn



    End Function
    Protected Function checkEntities() As Boolean
        Dim blnReturn As Boolean = False

        For Each item As ListItem In cblEntities.Items
            If item.Selected Then

                blnReturn = True
            End If
        Next
        Return blnReturn
    End Function
    Protected Sub btnClearAccounts_Click(sender As Object, e As System.EventArgs) Handles btnClearAccounts.Click
        For Each item As ListItem In cblApplications.Items
            item.Selected = False

        Next
    End Sub
    Public Sub CheckBoxValueSelector(ByRef cbl As CheckBoxList, ByVal val As String)

        Dim BusLog As New AccountBusinessLogic

        For Each item As ListItem In cbl.Items

            If item.Value = val Then

                item.Selected = True

            End If

        Next

    End Sub
    Protected Sub entity_cdDDL_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles entity_cdDDL.SelectedIndexChanged
        Dim ddbDepartmentBinder As New DropDownListBinder(department_cdddl, "sel_department_codes", Session("EmployeeConn"))
        ddbDepartmentBinder.AddDDLCriteria("@entity_cd", entity_cdDDL.SelectedValue, SqlDbType.NVarChar)
        ddbDepartmentBinder.BindData("department_cd", "department_name")

    End Sub
    'Protected Sub ColorCodeAccounts()
    '    Dim BaseApps As New List(Of Application)
    '    BaseApps = New Applications().BaseApps()

    '    For Each item As ListItem In cblApplications.Items

    '        If BaseApps.Find(Function(x) x.ApplicationNum = item.Value).AppSystem = "HospSpec" Then

    '            item.Attributes.Add("style", "color:red")
    '        ElseIf BaseApps.Find(Function(x) x.ApplicationNum = item.Value).AppSystem = "EntSpec" Then

    '            item.Attributes.Add("style", "color:blue")
    '        End If
    '    Next

    'End Sub
    Protected Sub SendProviderEmail(ByVal AccountRequestNum As Integer)
        '======================
        ' Get current path
        '======================

        Dim path As String
        Dim directory As String = ""
        path = HttpContext.Current.Request.Url.AbsoluteUri
        Dim pathParts() As String = Split(path, "/")
        Dim i As Integer
        Do While i < pathParts.Length() - 1

            directory = directory + pathParts(i) + "/"
            i = i + 1

        Loop

        '======================

        ' test for production or localhost or test
        Dim EmailList As New List(Of EmailDistribution)
        EmailList = New EmailDistributionList().EmailList

        Dim sndEmailTo = From e In EmailList
                         Where e.EmailEvent = "AddNew" And e.AppBase = "provider"
                         Select e


        For Each EmailAddress In sndEmailTo
            Dim toEmail As String = EmailAddress.Email
            Dim sSubject As String
            Dim sBody As String

            sSubject = "Account Request for " & first_nameTextBox.Text & " " & last_nameTextBox.Text & " has been added to the Provider Queue."


            sBody = "<!DOCTYPE html>" & _
                                "<html>" & _
                                "<head>" & _
                                "<style>" & _
                                "table" & _
                                "{" & _
                                "border-collapse:collapse;" & _
                                "font-family: 'Helvetica Neue', 'Lucida Grande', 'Segoe UI', Arial, Helvetica, Verdana, sans-serif;" & _
                                "}" & _
                                "table, th, td" & _
                                "{" & _
                                "border: 1px solid black;" & _
                                "padding: 3px" & _
                                "}" & _
                                "</style>" & _
                                "</head>" & _
                                "" & _
                                "<body>" & _
                                "A new account request has been added to the Provider Queue.  Follow the link below to go to Queue Screen." & _
                                 "<br />" & _
                                "<a href=""" & directory & "/ProviderRequest.aspx?requestnum=" & AccountRequestNum & """ ><u><b>Provider: " & first_nameTextBox.Text & " " & last_nameTextBox.Text & "</b></u></a>" & _
                                "</body>" & _
                            "</html>"


            Dim sToAddress As String = EmailAddress.Email
            Dim mailObj As New MailMessage()
            mailObj.From = New MailAddress("CSC@crozer.org")
            mailObj.To.Add(New MailAddress(sToAddress))
            mailObj.IsBodyHtml = True
            mailObj.Subject = sSubject
            mailObj.Body = sBody

            Dim smtpClient As New SmtpClient()
            smtpClient.Host = "ah1smtprelay01.altahospitals.com"

            smtpClient.Send(mailObj)
        Next

    End Sub
    Protected Sub btnUpdateSubmitter_Click(sender As Object, e As System.EventArgs) Handles btnUpdateSubmitter.Click
        mpeUpdateClient.Show()

        Dim dbAccess As New CkhsEmployeeDbAccess(Session("EmployeeConn"))
        Dim ckhsEmployee = dbAccess.GetCkhsEmployee(requestor_client_numLabel.Text)


        Dim c3Submitter As New Client3

        'c3Submitter = c3Controller.GetClientByClientNum(requestor_client_numLabel.Text)
        lblUpdateClientFullName.Text = ckhsEmployee.FullName
        txtUpdateEmail.Text = ckhsEmployee.EMail
        txtUpdatePhone.Text = ckhsEmployee.Phone

    End Sub
    Protected Sub btnUpdateClient_Click(sender As Object, e As System.EventArgs) Handles btnUpdateClient.Click
        Dim frmHelperRequestTicket As New FormHelper()

        frmHelperRequestTicket.InitializeUpdateForm("UpdClientObjectByNumberV4", tblUpdateClient, "Update")
        frmHelperRequestTicket.AddCmdParameter("@ClientNum", requestor_client_numLabel.Text)
        frmHelperRequestTicket.UpdateForm()

        Dim dbAccess As New CkhsEmployeeDbAccess(Session("EmployeeConn"))
        Dim ckhsEmployee = dbAccess.GetCkhsEmployee(requestor_client_numLabel.Text)

        'c3Controller = New Client3Controller(requestor_client_numLabel.Text)
        'c3Submitter = c3Controller.GetClientByClientNum(requestor_client_numLabel.Text)

        SubmittingOnBehalfOfNameLabel.Text = ckhsEmployee.FullName & " (" & ckhsEmployee.EMail & ")"


        mpeUpdateClient.Hide()
    End Sub
    Public Function CheckRequiredFields() As Boolean
        Dim removeCss As New PageControls(Page)
        removeCss.RemoveCssClass(Page, "textInputRequired")

        emp_type_cdLabel.Text = emp_type_cdrbl.SelectedValue
        Select Case emp_type_cdrbl.SelectedValue
            Case "1193"
                'Case "employee"

                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim Location As New RequiredField(facility_cdddl, "Location")
                Dim building As New RequiredField(building_cdddl, "building")

                pnlProvider.Visible = False
                providerrbl.SelectedIndex = -1



            Case "1189"
                'Case "student"

                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")
                Dim titleReq As New RequiredField(Titleddl, "Title")
                Dim specReq As New RequiredField(Specialtyddl, "Specialty")
                Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim faxqReq As New RequiredField(faxtextbox, "Fax")

                pnlProvider.Visible = False
                providerrbl.SelectedIndex = -1

            Case "782"
                ' Case "physician"
                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")
                Dim titleReq As New RequiredField(Titleddl, "Title")
                Dim specReq As New RequiredField(Specialtyddl, "Specialty")
                Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim faxqReq As New RequiredField(faxtextbox, "Fax")

                pnlProvider.Visible = True

                providerrbl.SelectedValue = "Yes"
                'hanrbl.SelectedValue = "Yes"
                Titleddl.Enabled = True
            Case "1197"
                'Case "non staff clinician"
                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")
                Dim titleReq As New RequiredField(Titleddl, "Title")
                Dim specReq As New RequiredField(Specialtyddl, "Specialty")
                Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim faxqReq As New RequiredField(faxtextbox, "Fax")

                pnlProvider.Visible = True

                providerrbl.SelectedIndex = -1
                Titleddl.Enabled = True

            Case "1194"
                'Case "resident"
                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")
                Dim titleReq As New RequiredField(Titleddl, "Title")
                Dim specReq As New RequiredField(Specialtyddl, "Specialty")
                Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim faxqReq As New RequiredField(faxtextbox, "Fax")

                pnlProvider.Visible = True

                providerrbl.SelectedValue = "Yes"
                Titleddl.Enabled = True



            Case "1198"
                'Case "vendor"

                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")

                Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim vendorRequired As New RequiredField(VendorNameTextBox, "Vendor")
                Dim startReq As New RequiredField(start_dateTextBox, "Start Date")


                pnlProvider.Visible = False
                providerrbl.SelectedIndex = -1

            Case "1195"
                'Case "contract"

                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")

                Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim startReq As New RequiredField(start_dateTextBox, "Start Date")

                pnlProvider.Visible = False
                providerrbl.SelectedIndex = -1

            Case "1200"
                'Case "allied health"

                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")
                Dim alliedReq As New RequiredField(Titleddl, "Title")
                ' Dim specReq As New RequiredField(Specialtyddl, "Specialty")


                Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim faxqReq As New RequiredField(faxtextbox, "Fax")

                'Dim otherAff As New RequiredField(OtherAffDescTextBox, "Other Affiliation Description")

                providerrbl.SelectedIndex = 0

                pnlProvider.Visible = True


                'pnlProvider.Visible = False
                providerrbl.SelectedValue = "Yes"
                'Titleddl.SelectedValue = Nothing
                'Titleddl.Enabled = False

            Case "1196"
                'Case "other"

                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")

                Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")



                pnlProvider.Visible = False
                providerrbl.SelectedIndex = -1



            Case Else


                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")

                pnlProvider.Visible = False
                providerrbl.SelectedIndex = -1



        End Select
        Return bHasError

    End Function

    Protected Sub btnNoSearch_Click(sender As Object, e As System.EventArgs) Handles btnNoSearch.Click
        Response.Redirect("SimpleSearch.aspx")
    End Sub

    Protected Sub btnSearchFirst_Click(sender As Object, e As System.EventArgs) Handles btnSearchFirst.Click
        lblSearched.Text = "TRUE"
        SearchPopup.Hide()
    End Sub

    Protected Sub grvClientsExists_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grvClientsExists.RowCommand
        If e.CommandName = "Select" Then


            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim AccountRequestNum As String = grvClientsExists.DataKeys(Convert.ToInt32(e.CommandArgument))("AccountRequestNum").ToString()
            Dim ClientNum As String = grvClientsExists.DataKeys(Convert.ToInt32(e.CommandArgument))("client_num").ToString()


            If AccountRequestNum <> "0" Then
                Response.Redirect("AccountRequestInfo2.aspx?&RequestNum=" & AccountRequestNum, True)

            End If
            If ClientNum <> "0" Then
                Response.Redirect("ClientDemo.aspx?&ClientNum=" & ClientNum, True)

            End If
        End If

    End Sub

    Protected Sub btnCloseExists_Click(sender As Object, e As System.EventArgs) Handles btnCloseExists.Click
        ClientExists.Hide()
        sTested = "yes"

    End Sub
    'Protected Sub grvClientsExists_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grvClientsExists.RowDataBound
    '    Try
    '        If e.Row.RowType = DataControlRowType.DataRow Then
    '            ' //===================================diag==========================
    '            Dim irow As Integer = e.Row.RowIndex


    '            'sEmployeeNum = e.Row.Cells(getcellNoE("siemens_emp_num", grvClientsExists)).Text

    '            'If sEmployeeNum.Length > 1 Then
    '            '    e.Row.BackColor = System.Drawing.Color.LightGreen
    '            'End If
    '            setColumnInfo(sender, e)

    '        End If
    '    Catch ex As Exception
    '        Throw ex

    '    End Try

    'End Sub

    Protected Sub setColumnInfo(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                ' Siemens Employee#

                sEmployeeNum = e.Row.Cells(getcellNoE("siemensempnum", grvClientsExists)).Text

                If sEmployeeNum.Length > 1 Then
                    e.Row.BackColor = System.Drawing.Color.LightGreen
                End If

            End If

        Catch ex As Exception
            Throw ex

        End Try
    End Sub

    Protected Sub btnReturnExists_Click(sender As Object, e As System.EventArgs) Handles btnReturnExists.Click
        ClientExists.Hide()

    End Sub

End Class
