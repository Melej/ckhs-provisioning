﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="false" CodeFile="PhysGrpMaintDetail.aspx.vb" Inherits="PhysGrpMaintDetail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <style type="text/css">
        .style2
        {
            text-align: center;
            background-color: #FFFFCC;
            color: #336666;
            font-weight: bold;
            font-size: x-large;
            height: 43px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:UpdatePanel ID="uplPanel" runat ="server"  UpdateMode="Conditional">
    <Triggers>
    </Triggers>
   <ContentTemplate>
   <div id="maindiv" runat="server" align="left">
       <table  style="border-style: groove" align="left" width="100%">
        <tr align="center">
           <td class="tableRowHeader">
                  Physician Group Maintenance
           </td>
        </tr>
        <tr>
          <td align="left">
             <table border="1" width="100%"  style="empty-cells:show" >
                         <tr>
                              <td colspan="2" align="center">
                               Physician Groups
          
                              </td>
                        </tr>

                         <tr>
                       <td  align="center" colspan="4">
                        <table>
                            <tr>
                                <td align="center">

                                        <asp:Button ID ="BtnUpdateGroup" runat = "server" Text ="Update " 
                                          CssClass="btnhov" BackColor="#006666"  Width="135px" />

                                         
                                </td>
                                <td>
                                
                                </td>
                                <td align="center" >

                                        <asp:Button ID = "BtnAddReset" runat= "server" Text="Return"  
                                         CssClass="btnhov" BackColor="#006666"  Width="135px" />
                                        

                                </td>
                                <td>
                                
                                </td>
                                <td>
                                
                                    <asp:Button ID="btnPDFReport" runat="server" Text="Excel RPT." 
                                     CssClass="btnhov" BackColor="#006666"  Width="135px" />

                                </td>
                            </tr>
                        </table>
                      </td>
                     </tr>   
                     
                        <tr>
                            <td colspan="4">
                            
                                <asp:Label ID = "ErrorLbl" runat="server" Text="" Visible="false" Font-Bold="true">
                                </asp:Label>
                            </td>
                        </tr>

                           <tr>
                            <td>
                                <asp:Label ID="GrpNumberlbl" runat="server" Text="Group #"></asp:Label>
                            </td>
                            <td colspan="3">
                            
                                <asp:Label ID="GroupnumLabel" runat="server" ></asp:Label>
                            </td>
                           </tr>

                          <tr>
                          <td>
                              <asp:Label ID="GrpNamelbl" runat="server" Text="Group Name">
                               </asp:Label>
                          </td>

                          <td colspan="3">
                               <asp:TextBox ID="Group_nameTextBox" runat="server" Width="450PX">
                                </asp:TextBox>
                          
                          </td>
                         </tr>

                          <tr>
                            <td>
                                <asp:Label ID="GroupTypelbl" runat="server" Text="Group type:"></asp:Label>
                            </td>
                            <td colspan="3">
                                <asp:TextBox ID="GroupTypeCdTextBox" runat="server">
                                </asp:TextBox>
                                <asp:DropDownList ID="GroupTypesddl" runat="server" Width="250px"  AutoPostBack="true" Visible="false">
                                </asp:DropDownList>

                            
                            </td>
                          </tr>

                         <tr>
                          <td>
                               <asp:Label ID="Grplbl" runat="server" Text="Group ID">
                               </asp:Label>
                          
                          </td>
                          <td colspan="3">
                            <asp:TextBox ID="GroupIDTextBox" runat="server">
                            </asp:TextBox>
                          
                          </td>
                          </tr>

                          <tr>
                            <td>
                                <asp:Label ID="duplicatelbl" runat="server"  Visible="false" Text = "Duplicate Invision Group#"></asp:Label>
                            </td>
                            <td colspan="3">
                                    <asp:TextBox ID="DuplicateInvisionGroupNumTextBox" Visible="false" runat="server"></asp:TextBox>
                            </td>
                            
                          </tr>
                         
                         <tr>
                            <td>
                                <asp:Label ID="Directlbl" runat="server" Visible="false" Text="Direct eMail:">
                                </asp:Label>    
                            
                            </td>
                            <td colspan="3">
                                <asp:TextBox ID="DirectEmailTextBox" runat="server"  Width="275px" Visible="false"></asp:TextBox>
                            </td>
                         </tr>
                         <tr>
                            <td>
                                <asp:Label ID="MedAssitlbl" runat="server" Visible="false" Text="MedAssist:">
                                </asp:Label>    

                            </td>
                            <td colspan="3">
                                <asp:TextBox ID="AKAGroupNameTextBox" runat="server"  Width="275px" Visible="false"></asp:TextBox>
                            </td>

                         </tr>
                         <tr>
                            <td>
                                <asp:Label ID="Caplbl" runat="server" Visible="false" Text="Cap Count:"></asp:Label>
                            </td>
                            <td colspan="3">
                                <asp:TextBox ID="CapCountTextBox" runat="server" Visible="false"></asp:TextBox>
                            </td>
                         </tr>
                         <tr>
                            <td>
                                <asp:Label ID="Deactivatelbl" runat="server" Text="Deactivate Physician Group:" Visible="false">
                                </asp:Label>
                            </td>
                            <td colspan="3">
                                    <asp:RadioButtonList ID = "DateDeactivatedrbl" runat="server" RepeatDirection="Horizontal" Visible="false">
                                        <asp:ListItem>Yes</asp:ListItem>
                                        <asp:ListItem>No</asp:ListItem>            
                                    </asp:RadioButtonList>
                            </td>
                         </tr>

                        </table>

        </td>
     </tr>
     <tr>
        <td colspan="4">
            <asp:Label id="clinetsattachedlbl"  runat="server" Text=" Clients Attached to This Group" Font-Bold="True">
            </asp:Label>
        </td>
     </tr>
     <tr>
             <td colspan="4">
                 <asp:GridView ID="gvAccounts" runat="server" AllowSorting="True" 
                           AutoGenerateColumns="False" BackColor="White" BorderColor="#336666" 
                           BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                           DataKeyNames="client_num" EmptyDataText="No Clients Attached to this Phys. Group" 
                           EnableTheming="False" Font-Size="Medium" GridLines="Horizontal" Width="100%">
                           <FooterStyle BackColor="White" ForeColor="#333333" />
                           <HeaderStyle BackColor="#336666" Font-Bold="False" Font-Size="X-Small" 
                               ForeColor="White" HorizontalAlign="Left" />
                           <PagerStyle BackColor="#336666" Font-Size="Medium" ForeColor="White" 
                               HorizontalAlign="Left" />
                           <RowStyle BackColor="White" ForeColor="#333333" Font-Size="X-Small" />
                           <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                           <SortedAscendingCellStyle BackColor="#F7F7F7" />
                           <SortedAscendingHeaderStyle BackColor="#487575" />
                           <SortedDescendingCellStyle BackColor="#E5E5E5" />
                           <SortedDescendingHeaderStyle BackColor="#275353" />
                           <Columns>

                               <asp:CommandField ButtonType="Button" ControlStyle-BackColor="#84A3A3" 
                                      HeaderStyle-HorizontalAlign="Left" ItemStyle-Height="30px" SelectText="Select" 
                                      ShowSelectButton="true">
                                  <ControlStyle BackColor="#84A3A3" />
                                  <HeaderStyle HorizontalAlign="Left" />
                                  <ItemStyle Height="30px" />
                                  </asp:CommandField>
                            

                               <asp:BoundField DataField="fullname" HeaderText="Name" ItemStyle-HorizontalAlign="Left" />

                                <asp:BoundField DataField="DoctorMasterNum" HeaderText="Doctor Master#" ItemStyle-HorizontalAlign="Left" />        
<%-- 

                              <asp:BoundField DataField="phone" HeaderText="Phone" />
                              <asp:BoundField DataField="department_name" HeaderText="Dept. " />
                              
 --%>
                               <asp:BoundField DataField="emp_type_cd" HeaderText="Emp Type"  ItemStyle-HorizontalAlign="Center"/>

                               <asp:BoundField DataField="NPI" HeaderText="NPI"  ItemStyle-HorizontalAlign="Center"/>

                               <asp:BoundField DataField="Specialty" HeaderText="Specialty" ItemStyle-HorizontalAlign="Center"/>
                               
                           </Columns>
                       </asp:GridView>
                           </td>
                         </tr>
  </table>
  </div>
   </ContentTemplate>
  </asp:UpdatePanel>
  <script type="text/javascript">
      $(document).ready(function () {
          setJQCalander();
          // just commented $(".calanderClass").datepick({ showOn: 'both', buttonImage: 'Images/jQueryCalendar.gif' });
          //  alert("test select"); //only works first time,dateFormat: 'MM yyyy'
      });

      Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
      Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);
      function setJQCalander() {
          //  $(".calanderClass").datepick({ showOn: 'both', buttonImage: 'Images/jQueryCalendar.gif' });

          $(".calanderClass").datepick({ onClose: function () {
              // __doPostBack('__Page', 'MyCustomArgument');
          },
              showOn: 'both', buttonImage: 'Images/jQueryCalendar.gif'
          });
          //            $('.date-picker').datepicker({
          //                changeMonth: true,
          //                changeYear: true,
          //                showButtonPanel: true,
          //                dateFormat: 'MM yy',
          //                onClose: function(dateText, inst) {
          //                    var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
          //                    var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
          //                    $(this).datepicker('setDate', new Date(year, month, 1));
          //                }
          //            }); 
      }
      function EndRequestHandler(sender, args) {
          //end of async postback
          // alert("end Async" );
          setJQCalander();


      }

      function BeginRequestHandler(sender, args) {

      }
    </script>
</asp:Content>

