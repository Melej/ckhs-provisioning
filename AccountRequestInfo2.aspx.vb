﻿Imports System.IO
Imports System.Configuration '
Imports System.Data.SqlClient
Imports App_Code
Imports AjaxControlToolkit
Imports C1.Web.UI.Controls.C1Window
Imports System.Net
Imports System.Net.Mail
Imports System.Net.Mail.SmtpClient


Partial Class AccountRequestInfo2
	Inherits System.Web.UI.Page
	Dim sActivitytype As String = ""
	Dim bHasError As Boolean

	Dim FormSignOn As New FormSQL()
	Dim DemoForm As New FormSQL()
	Dim TokenForm As New FormSQL()

	Dim VendorFormSignOn As FormSQL

	Dim iRequestNum As Integer
	Dim iUserNum As Integer
	Dim iUserSecurityLevel As Integer

	Dim RequestoName As String
	Dim iRequestorNum As String
	Dim tokenRequestnum As String

	Dim thisRole As String
	Dim mainRole As String

    Public dtTCLUserTypes As New DataTable

    Dim sValue As String
	Private dtAccounts As New DataTable
	Private AddAccountRequestNum As Integer = 0
	Private DelAccountRequestNum As Integer = 0
	Private dtPendingRequests As DataTable
	Private dtRemoveAccounts As New DataTable
	Private dtAddAccounts As New DataTable
	'Dim BusLog As AccountBusinessLogic
	Dim thisdata As CommandsSqlAndOleDb
	Dim UserInfo As UserSecurity
	Dim AccountRequest As AccountRequest
	Dim TokenRequest As TokenRequest

	Dim Mailobj As System.Net.Mail.SmtpClient = New System.Net.Mail.SmtpClient
	Dim Mailmsg As New System.Net.Mail.MailMessage
	Dim MailAddress As System.Net.Mail.MailAddress
	Dim mailAttachment As System.Net.Mail.Attachment

	Dim ActionControl As New ActionController()
	Dim ActionList As New List(Of Actions)
	Dim sAccountType As String
	Dim ValidationStatus As String
	Dim Deactivated As String
	Dim Requesttype As String

	Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
		sValue = Request.QueryString("DisplayField")
		iRequestNum = Request.QueryString("RequestNum")
		iRequestorNum = Request.QueryString("RequestorNum")
		RequestoName = Request.QueryString("RequestorName")
		Deactivated = Request.QueryString("sDeactivated")


		AccountRequest = New AccountRequest(iRequestNum, Session("EmployeeConn"))

		TokenRequest = New TokenRequest(iRequestNum, Session("EmployeeConn"))

		AuthorizationEmpNumTextBox.Text = AccountRequest.siemens_emp_num
		ValidationStatus = AccountRequest.ValidationStatus
		Requesttype = AccountRequest.AccountRequestType

		If Requesttype = "rehire" Then
			lblRehire.Visible = True
			lblRehire.Text = "THIS IS A REHIRE EMPLOYEE IF POSSIBLE USE SAME ACCOUNT NAMES"

		End If

        If Requesttype = "addpmhact" Then

            Dim sLoginNow As String = Session("LoginID").ToString.ToLower

            Select Case sLoginNow
                Case "geip00", "melej", "burgina"

                    first_nameTextBox.Enabled = True
                    miTextBox.Enabled = True
                    last_nameTextBox.Enabled = True
                Case Else
                    first_nameTextBox.Enabled = False
                    miTextBox.Enabled = False
                    last_nameTextBox.Enabled = False
            End Select

            Dim sLevel As String = Session("SecurityLevelNum").ToString

            Select Case sLevel
                Case "95"

                    first_nameTextBox.Enabled = True
                    miTextBox.Enabled = True
                    last_nameTextBox.Enabled = True
                Case Else
                    first_nameTextBox.Enabled = False
                    miTextBox.Enabled = False
                    last_nameTextBox.Enabled = False
            End Select

        End If
        'account_request_type
        ' lblAccountRequestType.Text = AccountRequest.AccountRequestType
        ' Lblentereddate.Text = AccountRequest.EnteredDate

        If iRequestorNum = "" Then
			iRequestorNum = AccountRequest.RequestorClientNum
			requestor_client_numLabel.Text = AccountRequest.RequestorClientNum
			tokenRequestnum = TokenRequest.AccountRequestNum

		End If

		If RequestoName = "" Then
			'RequestorName
			requestor_nameLabel.Text = AccountRequest.RequestorName
			RequestoName = AccountRequest.RequestorName

		End If

		UserInfo = New UserSecurity(Session("LoginID"), Session("EmployeeConn"))

		iUserNum = UserInfo.ClientNum
		userDepartmentCD.Text = UserInfo.DepartmentCd
		iUserSecurityLevel = UserInfo.SecurityLevelNum
		nt_loginlabel.Text = UserInfo.NtLogin

		If iUserSecurityLevel < 60 Then
			If iUserNum <> iRequestorNum Then
				Response.Redirect("~/SimpleSearch.aspx", True)

			End If
		End If

        FormSignOn = New FormSQL(Session("EmployeeConn"), "SelAccountRequestV20", "UpdFullAccountRequestV20", "", Page)
        FormSignOn.AddSelectParm("@account_request_num", iRequestNum, SqlDbType.Int, 6)

		DemoForm = New FormSQL(Session("EmployeeConn"), "SelClientDemoRequestByNumberV4", "UpdClientDemoObjectByNumberV4", "", Page)
		'DemoForm.AddSelectParm("@client_num", iClientNum, SqlDbType.Int, 6)
		DemoForm.AddSelectParm("@account_request_num", iRequestNum, SqlDbType.Int, 6)

		If tokenRequestnum <> "" Then
			TokenForm = New FormSQL(Session("EmployeeConn"), "SelTokenAddress", "InsUpdAccountTokenAddress", "", Page)
			TokenForm.AddSelectParm("@account_request_num", iRequestNum, SqlDbType.Int, 6)


			Dim ddlCPBinder As New DropDownListBinder
			ddlCPBinder.BindData(CellPhoneddl, "SelCellPhoneProviders", "ProviderExtension", "ProviderName", Session("EmployeeConn"))

			TokenForm.FillForm()
			'TokenForm.UpdateForm()

			If TokenNameTextBox.Text <> "" Then
				' if you really have cellphone info
				pnlTokenAddress.Visible = True



				If TokenNameTextBox.Text <> "" And TokenNameTextBox.Text.ToLower <> "select" Then
					CellPhoneddl.SelectedValue = TokenNameTextBox.Text

				Else
					'CellPhoneddl.SelectedValue = "other"

					'TokenNameTextBox.Text = "other"

					CellPhoneddl.SelectedIndex = -1

				End If
				CellPhoneddl.Enabled = False
				CellPhoneddl.BackColor() = Color.Yellow
				CellPhoneddl.BorderColor() = Color.Black
				CellPhoneddl.BorderStyle() = BorderStyle.Solid
				CellPhoneddl.BorderWidth = Unit.Pixel(2)

				TokenPhoneTextBox.BackColor() = Color.Yellow
				TokenPhoneTextBox.BorderColor() = Color.Black
				TokenPhoneTextBox.BorderStyle() = BorderStyle.Solid
				TokenPhoneTextBox.BorderWidth = Unit.Pixel(2)



			End If


			'TokenAddress1TextBox.BackColor() = Color.Yellow
			'TokenAddress1TextBox.BorderColor() = Color.Black
			'TokenAddress1TextBox.BorderStyle() = BorderStyle.Solid
			'TokenAddress1TextBox.BorderWidth = Unit.Pixel(2)

			'TokenCityTextBox.BackColor() = Color.Yellow
			'TokenCityTextBox.BorderColor() = Color.Black
			'TokenCityTextBox.BorderStyle() = BorderStyle.Solid
			'TokenCityTextBox.BorderWidth = Unit.Pixel(2)


			'TokenStTextBox.BackColor() = Color.Yellow
			'TokenStTextBox.BorderColor() = Color.Black
			'TokenStTextBox.BorderStyle() = BorderStyle.Solid
			'TokenStTextBox.BorderWidth = Unit.Pixel(2)


			'TokenZipTextBox.BackColor() = Color.Yellow
			'TokenZipTextBox.BorderColor() = Color.Black
			'TokenZipTextBox.BorderStyle() = BorderStyle.Solid
			'TokenZipTextBox.BorderWidth = Unit.Pixel(2)


		End If

		emp_type_cdrbl.BackColor() = Color.Yellow
		emp_type_cdrbl.BorderColor() = Color.Black
		emp_type_cdrbl.BorderStyle() = BorderStyle.Solid
		emp_type_cdrbl.BorderWidth = Unit.Pixel(2)



		If Not IsPostBack Then


			Dim rblAffiliateBinder As New RadioButtonListBinder(emp_type_cdrbl, "SelAffiliationsV5", Session("EmployeeConn"))
			' rblAffiliateBinder.BindData("AffiliationDescLower", "AffiliationDisplay")

			'If Session("SecurityLevelNum") < 90 Then
			Select Case AccountRequest.RoleNum
				Case "782", "1197", "1194"
					rblAffiliateBinder.AddDDLCriteria("@type", "phys", SqlDbType.NVarChar)
				Case Else
					If AccountRequest.DoctorNum > 0 Then
						EmpDoclbl.Visible = True
						EmpDoctorTextBox.Text = AccountRequest.DoctorNum.ToString
						EmpDoctorTextBox.Visible = True
						If Session("SecurityLevelNum") < 90 Then
							EmpDoctorTextBox.Enabled = False
						End If

						'rblAffiliateBinder.AddDDLCriteria("@type", "phys", SqlDbType.NVarChar)
						'grDocmster
					Else

						rblAffiliateBinder.AddDDLCriteria("@type", "", SqlDbType.NVarChar)

					End If

			End Select
			'End If


			rblAffiliateBinder.BindData("RoleNum", "AffiliationDisplay") '"AffiliationDescLower",

            RoleNumLabel.Text = AccountRequest.RoleNum
            masterRole.Text = AccountRequest.RoleNum
            emp_type_cdrbl.SelectedValue = AccountRequest.RoleNum

			emp_type_cdLabel.Text = AccountRequest.RoleNum
			'emp_type_cdLabel.Text = AccountRequest.EmpTypeCd
			ClientnameHeader.Text = AccountRequest.FirstName & " " & AccountRequest.LastName
			' HDemployeetype.Text = emp_type_cdrbl.SelectedValue

			FormSignOn.FillForm()

			CheckAffiliation()

			'PastValidation
			'ValidationStatus

			dtRemoveAccounts.Columns.Add("account_request_seq_num", GetType(Integer))
			dtRemoveAccounts.Columns.Add("ApplicationNum", GetType(Integer))
			dtRemoveAccounts.Columns.Add("ApplicationDesc", GetType(String))

			dtAddAccounts.Columns.Add("Applicationnum", GetType(Integer))
			dtAddAccounts.Columns.Add("ApplicationDesc", GetType(String))

			ViewState("dtRemoveAccounts") = dtRemoveAccounts
			ViewState("dtAddAccounts") = dtAddAccounts

			Dim thisdata As New CommandsSqlAndOleDb("SelPendingQueuesByRequestNum", Session("EmployeeConn"))
			thisdata.AddSqlProcParameter("@account_request_num", iRequestNum, SqlDbType.Int, 20)

			thisdata.ExecNonQueryNoReturn()

			ViewState("dtAccounts") = thisdata.GetSqlDataTable


			thisdata = New CommandsSqlAndOleDb("SelAvailableAccountsByRequestNumberV3", Session("EmployeeConn"))
			thisdata.AddSqlProcParameter("@request_num", iRequestNum, SqlDbType.Int, 20)
			thisdata.AddSqlProcParameter("@userClientNum", iUserNum, SqlDbType.Int, 20)

			thisdata.ExecNonQueryNoReturn()

			ViewState("dtAvailableAccounts") = thisdata.GetSqlDataTable

			Dim ddlBinderV2 As DropDownListBinder
            ddlBinderV2 = New DropDownListBinder(Rolesddl, "SelEmployeeRolesV3", Session("EmployeeConn"))

            ddlBinderV2.AddDDLCriteria("@RoleNum", emp_type_cdrbl.SelectedValue, SqlDbType.NVarChar)

            If PositionNumTextBox.Text <> "" Then
                ddlBinderV2.AddDDLCriteria("@positionNum", PositionNumTextBox.Text, SqlDbType.NVarChar)

            End If

            ddlBinderV2.BindData("rolenum", "roledesc")

			Dim ddlBinder As New DropDownListBinder

			'ddlBinder.BindData(Rolesddl, "SelEmployeeRolesV2", "rolenum", "RoleDesc", Session("EmployeeConn"))

			ddlBinder.BindData(facility_cdddl, "SelFacilityCodes", "facility_cd", "facility_short_name", Session("EmployeeConn"))
			ddlBinder.BindData(Specialtyddl, "SelSpecialtyCodesV2", "specialty", "specialty", Session("EmployeeConn"))
			ddlBinder.BindData(entity_cdddl, "selEntityCodesV4", "entity_cd", "hosp_desc", Session("EmployeeConn"))
			ddlBinder.BindData(suffixddl, "SelSuffixCodes", "Suffix", "Suffix", Session("EmployeeConn"))

			Dim ddlInvisionBinder As New DropDownListBinder(group_nameddl, "SelPhysGroupsV5", Session("EmployeeConn"))
			ddlInvisionBinder.AddDDLCriteria("@GroupTypeCd", "invision", SqlDbType.NVarChar)
			ddlInvisionBinder.AddDDLCriteria("@clientpage", "yes", SqlDbType.NVarChar)

			ddlInvisionBinder.BindData("Group_num", "group_name")

            'Dim ddlNonPerferBinder As New DropDownListBinder(Nonperferdddl, "SelPhysGroupsV5", Session("EmployeeConn"))
            'ddlNonPerferBinder.AddDDLCriteria("@GroupTypeCd", "NonPerGrp", SqlDbType.NVarChar)
            'ddlNonPerferBinder.AddDDLCriteria("@clientpage", "yes", SqlDbType.NVarChar)
            '         ddlNonPerferBinder.BindData("Group_num", "group_name")


            Dim ddGnumberBinder As New DropDownListBinder(LocationOfCareIDddl, "SelPhysGroupsV5", Session("EmployeeConn"))
            ddGnumberBinder.AddDDLCriteria("@GroupTypeCd", "CHMG", SqlDbType.NVarChar)
            ddGnumberBinder.AddDDLCriteria("@clientpage", "yes", SqlDbType.NVarChar)
            ddGnumberBinder.BindData("Group_num", "group_name")


            '         Dim ddCommLocationOfCare As New DropDownListBinder(CommLocationOfCareIDddl, "SelPhysGroupsV5", Session("EmployeeConn"))
            'ddCommLocationOfCare.AddDDLCriteria("@GroupTypeCd", "CommNum", SqlDbType.NVarChar)
            'ddCommLocationOfCare.AddDDLCriteria("@clientpage", "yes", SqlDbType.NVarChar)
            '         ddCommLocationOfCare.BindData("Group_num", "group_name")


            '         Dim ddCoverageGroupddl As New DropDownListBinder(CoverageGroupddl, "SelPhysGroupsV5", Session("EmployeeConn"))
            'ddCoverageGroupddl.AddDDLCriteria("@GroupTypeCd", "coverage", SqlDbType.NVarChar)
            'ddCoverageGroupddl.AddDDLCriteria("@clientpage", "yes", SqlDbType.NVarChar)
            '         ddCoverageGroupddl.BindData("Group_num", "Group_name")

            Dim ddEMRBinder As New DropDownListBinder(PhysOfficeddl, "SelAllPhysGroups", Session("EmployeeConn"))
			'ddEMRBinder.AddDDLCriteria("@GroupTypeCd", "GNum", SqlDbType.NVarChar)
			'SelPhysGroupsV5
			ddEMRBinder.BindData("Group_num", "group_name")

			If LocationOfCareIDLabel.Text = "" Then
				LocationOfCareIDddl.SelectedIndex = -1
			Else
				LocationOfCareIDddl.SelectedValue = GNumberGroupNumLabel.Text
				LocationOfCareIDddl.Enabled = False

				'LocationOfCareIDddl.SelectedValue = LocationOfCareIDLabel.Text
			End If

            'If CommLocationOfCareIDLabel.Text = "" Then
            '	CommLocationOfCareIDddl.SelectedIndex = -1
            'Else
            '	CommLocationOfCareIDddl.SelectedValue = CommNumberGroupNumLabel.Text
            '	CommLocationOfCareIDddl.Enabled = False
            '	'CommLocationOfCareIDddl.SelectedValue = CommLocationOfCareIDLabel.Text
            'End If


            If entity_cdLabel.Text = "" Then
				entity_cdddl.SelectedIndex = -1
			Else
				Dim entitycd As String = entity_cdLabel.Text

				entitycd = entitycd.Trim
				entity_cdddl.SelectedValue = entitycd

			End If

			If PositionRoleNumLabel.Text <> "" Then
				Rolesddl.SelectedValue = PositionRoleNumLabel.Text
			ElseIf RoleNumLabel.Text <> "" Then
				Rolesddl.SelectedValue = RoleNumLabel.Text
			Else
				Rolesddl.SelectedIndex = -1
			End If


			If facility_cdLabel.Text = "" Then
				facility_cdddl.SelectedIndex = -1
			Else
				facility_cdddl.SelectedValue = facility_cdLabel.Text
			End If

			If TCLTextBox.Text <> "" Then
				pnlTCl.Visible = True
				TCLTextBox.Visible = True
				UserTypeDescTextBox.Visible = True
				UserTypeCdTextBox.Visible = True
			End If

			If Receiver_eMailLabel.Text <> "" Then
				e_mailTextBox.Text = Receiver_eMailLabel.Text

			End If



            CheckRequiredFields()


			Dim ddbDepartmentBinder As New DropDownListBinder(department_cdddl, "selDepartmentCodesV4", Session("EmployeeConn"))
			ddbDepartmentBinder.AddDDLCriteria("@entity_cd", entity_cdddl.SelectedValue, SqlDbType.NVarChar)
			ddbDepartmentBinder.BindData("department_cd", "department_name")


			If department_cdLabel.Text = "" Then
				department_cdddl.SelectedIndex = -1
			Else
				department_cdddl.SelectedValue = department_cdLabel.Text

			End If


			If SpecialtyLabel.Text = "" Then
				Specialtyddl.SelectedIndex = -1
			Else
				Specialtyddl.SelectedValue = SpecialtyLabel.Text

			End If

			' Gnumber
			If LocationOfCareIDLabel.Text = "" Then
				LocationOfCareIDddl.SelectedIndex = -1
			Else
				LocationOfCareIDddl.SelectedValue = LocationOfCareIDLabel.Text
			End If

            ''Comm Gnumber
            'If CommLocationOfCareIDLabel.Text = "" Then
            '	CommLocationOfCareIDddl.SelectedIndex = -1
            'Else
            '	CommLocationOfCareIDddl.SelectedValue = CommLocationOfCareIDLabel.Text
            'End If

            ''Coverage number
            'If CoverageGroupNumLabel.Text = "" Then
            '	CoverageGroupddl.SelectedIndex = -1
            'Else
            '	CoverageGroupddl.SelectedValue = CoverageGroupNumLabel.Text

            'End If

            ' Invision Number
            If InvisionGroupNumTextBox.Text = "" Then
				group_nameddl.SelectedIndex = -1
			Else
				group_nameddl.SelectedValue = InvisionGroupNumTextBox.Text

			End If

            ''non perfered
            'If NonPerferedGroupnumLabel.Text = "" Then
            '	Nonperferdddl.SelectedIndex = -1
            'Else
            '	Nonperferdddl.SelectedValue = NonPerferedGroupnumLabel.Text

            'End If



            If suffixlabel.Text = "" Then
				suffixddl.SelectedIndex = -1
			Else
				suffixddl.SelectedValue = suffixlabel.Text
			End If

			Dim ddlBuildingBinder As New DropDownListBinder
			ddlBuildingBinder = New DropDownListBinder(building_cdddl, "sel_building_by_facility", Session("EmployeeConn"))
			ddlBuildingBinder.AddDDLCriteria("@facility_cd", facility_cdddl.SelectedValue, SqlDbType.NVarChar)
			ddlBuildingBinder.BindData("building_cd", "building_name")


			If building_cdLabel.Text = "" Then
				building_cdddl.SelectedIndex = -1
			Else
				Dim sBuilding As String = building_cdLabel.Text
				building_cdddl.SelectedValue = sBuilding.Trim

				If building_nameLabel.Text <> "" Then

					building_cdddl.SelectedItem.Text = building_nameLabel.Text
				End If

			End If

			If building_cdddl.SelectedIndex <> -1 Then ' Or building_cdddl.SelectedValue <> "Select" Then

				ddlBinder = New DropDownListBinder(Floorddl, "SelFloorsByBuilding", Session("EmployeeConn"))
				ddlBinder.AddDDLCriteria("@facility_cd", facility_cdddl.SelectedValue, SqlDbType.NVarChar)
				ddlBinder.AddDDLCriteria("@building_cd", building_cdddl.SelectedValue, SqlDbType.NVarChar)

				ddlBinder.BindData("floor_no", "floor_no")

				If FloorTextBox.Text = "" Then
					Floorddl.SelectedValue = -1
				Else
					Dim sFloor As String = FloorTextBox.Text
					Floorddl.SelectedValue = sFloor.Trim

				End If



			End If
			If close_dateTextBox.Text <> "" Then
				'' if the request is closed then do not allow or show control
				' or add or remove account tabs

				Select Case account_request_typeLabel.Text
					Case "delacct", "terminate", "demoupd", "credent", "removecre", "addacct", "addnew"

						Dim ctrl As Control = DataCheckingAndValidation.SetChildControlsRecursiveToEnabledFalse(tpDemographics, False)
						tpRemoveItems.Visible = False
						tpAddItems.Visible = False
						tpPendingRequests.Visible = False
						btnUpdate.Visible = False
						' btnClientDemo
						If receiver_client_numLabel.Text <> "" Then
							btnClientDemo.Enabled = True
							btnClientDemo.Visible = True

						End If

						If account_request_typeLabel.Text = "demoupd" Then
							DemoForm.FillForm()

							btnDemoRequest.Enabled = True
							btnDemoRequest.Visible = True
							TPDemoChanges.Visible = True

						End If
						Select Case emp_type_cdrbl.SelectedValue
							Case "1193"
								PhysOfficepanel.Visible = True
								Select Case PositionRoleNumLabel.Text
									Case "1567"
										'Cant change type
										Rolesddl.Enabled = False
										PhysOfficeddl.Dispose()


										Dim ddOffBinder As New DropDownListBinder(PhysOfficeddl, "SelPhysGroupsV5", Session("EmployeeConn"))
										ddOffBinder.AddDDLCriteria("@GroupTypeCd", "GNum", SqlDbType.NVarChar)
										'SelPhysGroupsV5
										ddOffBinder.BindData("Group_num", "group_name")


										PhysOfficeddl.BackColor() = Color.Yellow
										PhysOfficeddl.BorderColor() = Color.Black
										PhysOfficeddl.BorderStyle() = BorderStyle.Solid
										PhysOfficeddl.BorderWidth = Unit.Pixel(2)

										If group_numTextBox.Text = "" Then
											PhysOfficeddl.SelectedIndex = -1
										Else
											PhysOfficeddl.SelectedValue = group_numTextBox.Text
										End If

										'cpmModelAfterPanel.Visible = True
										'cpmModelEmplPanel.Visible = False

										'If CPMModelTextBox.Text <> "" Then
										'    ViewCPMModelTextBox.Text = CPMModelTextBox.Text

										'End If


										' CPM Fields
										'CPMEmployeesddl.Dispose()

										'Dim ddcmpMOdelafter As New DropDownListBinder
										'ddcmpMOdelafter.BindData(CPMEmployeesddl, "SelCPMClients", "client_num", "FullName", Session("EmployeeConn"))


										'currcpmmodlbl
										'cpmModelEmplPanel


										'ViewCPMModelTextBox
										'CPMModelTextBox
										'provmodelTextBox
										'cpmPhysSchedulePanel
										'SchedulableTextbox
										'TelevoxTextBox

										'CPMEmployeesddl
										'CPMEmployeesddl.BackColor() = Color.Yellow
										'CPMEmployeesddl.BorderColor() = Color.Black
										'CPMEmployeesddl.BorderStyle() = BorderStyle.Solid
										'CPMEmployeesddl.BorderWidth = Unit.Pixel(2)

										start_dateTextBox.BackColor() = Color.Yellow
										start_dateTextBox.BorderColor() = Color.Black
										start_dateTextBox.BorderStyle() = BorderStyle.Solid
										start_dateTextBox.BorderWidth = Unit.Pixel(2)

									Case Else
										If group_numTextBox.Text = "" Then
											PhysOfficeddl.SelectedIndex = -1
										Else
											PhysOfficeddl.SelectedValue = group_numTextBox.Text
										End If

								End Select
							Case "1195"
								contractPanel.Visible = True

							Case Else

						End Select


				End Select

			Else '' if this request is open 
				Select Case emp_type_cdrbl.SelectedValue
					Case "782", "1197"
						AuthorizationEmpNumTextBox.Visible = True
						Authlbl.Visible = True

						'Case "physician", "non staff clinician", "resident"

						Dim ddbTitleBinder As New DropDownListBinder(Titleddl, "SelTitlesV4", Session("EmployeeConn"))
						ddbTitleBinder.AddDDLCriteria("@titleType", "Physician", SqlDbType.NVarChar)
						ddbTitleBinder.BindData("TitleShortDesc", "TitleDesc")

						'FillAffiliations()

						Titleddl.SelectedValue = TitleLabel.Text

						If TitleLabel.Text = "" Then
							Titleddl.SelectedIndex = -1
						Else
							Titleddl.SelectedValue = TitleLabel.Text

						End If

						If SpecialtyLabel.Text = "" Then
							Specialtyddl.SelectedIndex = -1
						Else
							Specialtyddl.SelectedValue = SpecialtyLabel.Text

						End If

						If Session("SecurityLevelNum") < 90 Then
							Dim ctrl As Control = DataCheckingAndValidation.SetChildControlsRecursiveToEnabledFalse(tpDemographics, False)
						Else
							first_nameTextBox.Enabled = False
							miTextBox.Enabled = False
							last_nameTextBox.Enabled = False
							address_1textbox.Enabled = False
							address_2textbox.Enabled = False
							citytextbox.Enabled = False
							lblstate.Enabled = False
							statetextbox.Enabled = False
							lblzip.Enabled = False
							ziptextbox.Enabled = False
							phoneTextBox.Enabled = False
							faxtextbox.Enabled = False

						End If


						If hanrbl.SelectedValue.ToString = "Yes" Then
							hanrbl.Enabled = False
							LocationOfCareIDddl.Visible = True
							ckhnlbl.Visible = True

							'cpmPhysSchedulePanel.Visible = True
							'cpmModelEmplPanel.Visible = True

							'Dim ddcmpMOdelafter As New DropDownListBinder(CPMEmployeesddl, "SelCPMClients", Session("EmployeeConn"))
							'ddcmpMOdelafter.AddDDLCriteria("@Group_num", GNumberGroupNumLabel.Text, SqlDbType.NVarChar)
							'ddcmpMOdelafter.BindData("client_num", "FullName")



							LocationOfCareIDddl.BackColor() = Color.Yellow
							LocationOfCareIDddl.BorderColor() = Color.Black
							LocationOfCareIDddl.BorderStyle() = BorderStyle.Solid
							LocationOfCareIDddl.BorderWidth = Unit.Pixel(2)

							'currcpmmodlbl
							'cpmModelEmplPanel


							'ViewCPMModelTextBox
							'CPMModelTextBox
							'provmodelTextBox
							'cpmPhysSchedulePanel

							'If provmodelTextBox.Text <> "" Then
							'    ViewCPMModelTextBox.Text = provmodelTextBox.Text

							'End If
							'If SchedulableTextbox.Text <> "" Then
							'    Schedulablerbl.SelectedValue = SchedulableTextbox.Text
							'End If


							'If TelevoxTextBox.Text <> "" Then
							'    Televoxrbl.SelectedValue = TelevoxTextBox.Text
							'End If

							'Schedulablerbl
							'Schedulablerbl.BackColor() = Color.Yellow
							'Schedulablerbl.BorderColor() = Color.Black
							'Schedulablerbl.BorderStyle() = BorderStyle.Solid
							'Schedulablerbl.BorderWidth = Unit.Pixel(2)


							''Televoxrbl

							'Televoxrbl.BackColor() = Color.Yellow
							'Televoxrbl.BorderColor() = Color.Black
							'Televoxrbl.BorderStyle() = BorderStyle.Solid
							'Televoxrbl.BorderWidth = Unit.Pixel(2)

							'cpmModelEmplPanel.Visible = True
							'cpmModelAfterPanel.Visible = True

							'lblprov.Visible = True
							'providerrbl.Visible = True

						Else
							LocationOfCareIDddl.Visible = False
							ckhnlbl.Visible = False

							'lblprov.Visible = False
							'providerrbl.Visible = False

						End If




					Case "1194"
                        'Resident
                        AuthorizationEmpNumTextBox.Visible = True
						Authlbl.Visible = True


						Dim ddbTitleBinder As New DropDownListBinder(Titleddl, "SelTitlesV4", Session("EmployeeConn"))
						ddbTitleBinder.AddDDLCriteria("@titleType", "Resident", SqlDbType.NVarChar)
						ddbTitleBinder.BindData("TitleShortDesc", "TitleDesc")

                        Dim ddlRoleBinder As New DropDownListBinder(Rolesddl, "SelEmployeeRolesV3", Session("EmployeeConn"))
                        ddlRoleBinder.AddDDLCriteria("@RoleNum", "1194", SqlDbType.NVarChar)
						ddlRoleBinder.BindData("rolenum", "RoleDesc")

						If TitleLabel.Text = "" Then
							Titleddl.SelectedIndex = -1
						Else
							Titleddl.SelectedValue = TitleLabel.Text

						End If
						If SpecialtyLabel.Text = "" Then
							Specialtyddl.SelectedIndex = -1
						Else
							Specialtyddl.SelectedValue = SpecialtyLabel.Text

						End If

						If TitleLabel.Text = "" Then
							Title2TextBox.Text = "Resident"

						Else
							Title2TextBox.Text = TitleLabel.Text

						End If
						Title2TextBox.Visible = True
						Title2TextBox.Enabled = False
						Titleddl.Visible = False

						SpecialtLabel.Text = "Resident"
						SpecialtLabel.Visible = True
						Specialtyddl.Visible = False

						credentialedDateTextBox.Enabled = False
						credentialedDateTextBox.CssClass = "style2"

                        'CredentialedDateDCMHTextBox.Enabled = False
                        'CredentialedDateDCMHTextBox.CssClass = "style2"

                        CCMCadmitRightsrbl.SelectedValue = "No"
						CCMCadmitRightsrbl.Enabled = False

                        'DCMHadmitRightsrbl.SelectedValue = "No"
                        'DCMHadmitRightsrbl.Enabled = False

                        'Dim ddlSpecialityBinder As New DropDownListBinder(Specialtyddl, "SelSpecialtyCodesV2", Session("EmployeeConn"))
                        'ddlSpecialityBinder.AddDDLCriteria("@SpecialtyType", "Resident", SqlDbType.NVarChar)
                        'ddlSpecialityBinder.BindData("specialty", "specialty")

                        ' in titleddl will be rolenum for residents which will be the new thisrole
                        'ddlBinder.BindData(Rolesddl, "SelEmployeeRolesV2", "rolenum", "RoleDesc", Session("EmployeeConn"))
                        thisRole = emp_type_cdrbl.SelectedValue
						mainRole = emp_type_cdrbl.SelectedValue

						pnlProvider.Visible = True

						If hanrbl.SelectedValue.ToString = "Yes" Then
							LocationOfCareIDddl.Visible = True
							ckhnlbl.Visible = True

							'cpmPhysSchedulePanel.Visible = True
							'cpmModelEmplPanel.Visible = True


							'Dim ddcmpMOdelafter As New DropDownListBinder(CPMEmployeesddl, "SelCPMClients", Session("EmployeeConn"))
							'ddcmpMOdelafter.AddDDLCriteria("@Group_num", GNumberGroupNumLabel.Text, SqlDbType.NVarChar)
							'ddcmpMOdelafter.BindData("client_num", "FullName")

							'If provmodelTextBox.Text <> "" Then
							'    ViewCPMModelTextBox.Text = provmodelTextBox.Text

							'End If
							'If SchedulableTextbox.Text <> "" Then
							'    Schedulablerbl.SelectedValue = SchedulableTextbox.Text
							'End If


							'If TelevoxTextBox.Text <> "" Then
							'    Televoxrbl.SelectedValue = TelevoxTextBox.Text
							'End If


							LocationOfCareIDddl.BackColor() = Color.Yellow
							LocationOfCareIDddl.BorderColor() = Color.Black
							LocationOfCareIDddl.BorderStyle() = BorderStyle.Solid
							LocationOfCareIDddl.BorderWidth = Unit.Pixel(2)


							'Schedulablerbl
							'Schedulablerbl.BackColor() = Color.Yellow
							'Schedulablerbl.BorderColor() = Color.Black
							'Schedulablerbl.BorderStyle() = BorderStyle.Solid
							'Schedulablerbl.BorderWidth = Unit.Pixel(2)

							''Televoxrbl
							'Televoxrbl.BackColor() = Color.Yellow
							'Televoxrbl.BorderColor() = Color.Black
							'Televoxrbl.BorderStyle() = BorderStyle.Solid
							'Televoxrbl.BorderWidth = Unit.Pixel(2)

							'cpmModelEmplPanel.Visible = True
							'cpmModelAfterPanel.Visible = True

						Else
							LocationOfCareIDddl.Visible = False
							ckhnlbl.Visible = False

							'lblprov.Visible = False
							'providerrbl.Visible = False

						End If

						If Session("SecurityLevelNum") < 100 Then
							Dim ctrl As Control = DataCheckingAndValidation.SetChildControlsRecursiveToEnabledFalse(tpDemographics, False)
						End If

					Case "1195", "1198"
						' load vendor list 
						'make vendor text box invisiable
						' make ddl appear
						' 1195 Contractor list
						' 1198 Vendor list
						Vendorddl.Visible = True
						VendorNameTextBox.Visible = False

						AuthorizationEmpNumTextBox.Visible = False
						Authlbl.Visible = False


						Dim ddbVendorBinder As New DropDownListBinder(Vendorddl, "SelVendors", Session("EmployeeConn"))
						ddbVendorBinder.AddDDLCriteria("@type", "vendor", SqlDbType.NVarChar)
						ddbVendorBinder.BindData("VendorNum", "VendorName")

						'If emp_type_cdrbl.SelectedValue = "1198" Then
						'    ddbVendorBinder.AddDDLCriteria("@type", "vendor", SqlDbType.NVarChar)
						'    lblVendor.Text = "Vendor Name:"

						'ElseIf emp_type_cdrbl.SelectedValue = "1195" Then

						'    ddbVendorBinder.AddDDLCriteria("@type", "contractor", SqlDbType.NVarChar)
						'    lblVendor.Text = "Contractor Name:"
						'Else

						'End If

						If VendorNameTextBox.Text = "" Then
							Vendorddl.SelectedIndex = -1
						Else
							Vendorddl.SelectedValue = VendorNumLabel.Text

						End If

						If VendorNumLabel.Text = "0" Then
							otherVendorlbl.Visible = True
							VendorUnknowlbl.Text = VendorNameTextBox.Text
							VendorUnknowlbl.Visible = True
						End If


						'Case "1200"
						'    'Case "allied health"
						'    Dim ddbTitleBinder As New DropDownListBinder(Titleddl, "SelTitlesV4", Session("EmployeeConn"))
						'    ddbTitleBinder.AddDDLCriteria("@titleType", "Alliedhealth", SqlDbType.NVarChar)
						'    ddbTitleBinder.BindData("TitleShortDesc", "TitleDesc")

						'    'FillAffiliations()

						'    If TitleLabel.Text = "" Then
						'        Titleddl.SelectedIndex = -1
						'    Else
						'        Titleddl.SelectedValue = TitleLabel.Text

						'    End If

						'    If SpecialtyLabel.Text = "" Then
						'        Specialtyddl.SelectedIndex = -1
						'    Else
						'        Specialtyddl.SelectedValue = SpecialtyLabel.Text

						'    End If

						'    If Session("SecurityLevelNum") < 90 Then
						'        Dim ctrl As Control = DataCheckingAndValidation.SetChildControlsRecursiveToEnabledFalse(tpDemographics, False)
						'    Else
						'        first_nameTextBox.Enabled = False
						'        miTextBox.Enabled = False
						'        last_nameTextBox.Enabled = False
						'        address_1textbox.Enabled = False
						'        address_2textbox.Enabled = False
						'        citytextbox.Enabled = False
						'        lblstate.Enabled = False
						'        statetextbox.Enabled = False
						'        lblzip.Enabled = False
						'        ziptextbox.Enabled = False
						'        phoneTextBox.Enabled = False
						'        faxtextbox.Enabled = False


						'    End If
					Case "1199"
						AuthorizationEmpNumTextBox.Visible = False
						Authlbl.Visible = False

					Case "1196"
						' Other
						user_position_descTextBox.Enabled = False
						AuthorizationEmpNumTextBox.Visible = False
						Authlbl.Visible = False

					Case "1193"
						AuthorizationEmpNumTextBox.Visible = True
						Authlbl.Visible = True
						PhysOfficepanel.Visible = True

						If group_numTextBox.Text = "" Then
							PhysOfficeddl.SelectedIndex = -1
						Else
							PhysOfficeddl.SelectedValue = group_numTextBox.Text
						End If

						Select Case PositionRoleNumLabel.Text
							Case "1567"
                                'employee with EMRCKHN No longer valid after Cerner
                                Rolesddl.Enabled = False


								PhysOfficeddl.Dispose()


								Dim ddOffBinder As New DropDownListBinder(PhysOfficeddl, "SelPhysGroupsV5", Session("EmployeeConn"))
                                ddOffBinder.AddDDLCriteria("@GroupTypeCd", "CHMG", SqlDbType.NVarChar)
                                'SelPhysGroupsV5
                                ddOffBinder.BindData("Group_num", "group_name")


								PhysOfficeddl.BackColor() = Color.Yellow
								PhysOfficeddl.BorderColor() = Color.Black
								PhysOfficeddl.BorderStyle() = BorderStyle.Solid
								PhysOfficeddl.BorderWidth = Unit.Pixel(2)

								'CPMEmployeesddl.Dispose()

								'Dim ddcmpMOdelafter As New DropDownListBinder
								'ddcmpMOdelafter.BindData(CPMEmployeesddl, "SelCPMClients", "client_num", "FullName", Session("EmployeeConn"))

								'cpmModelEmplPanel.Visible = True
								'cpmModelAfterPanel.Visible = True


								'If CPMModelTextBox.Text <> "" Then
								'    ViewCPMModelTextBox.Text = CPMModelTextBox.Text

								'End If

								''CPMEmployeesddl
								'CPMEmployeesddl.BackColor() = Color.Yellow
								'CPMEmployeesddl.BorderColor() = Color.Black
								'CPMEmployeesddl.BorderStyle() = BorderStyle.Solid
								'CPMEmployeesddl.BorderWidth = Unit.Pixel(2)

								'start_dateTextBox.BackColor() = Color.Yellow
								'start_dateTextBox.BorderColor() = Color.Black
								'start_dateTextBox.BorderStyle() = BorderStyle.Solid
								'start_dateTextBox.BorderWidth = Unit.Pixel(2)

							Case Else

						End Select

				End Select

			End If

            'Page.Controls.Remove(building_cdTextBox)

            If AuthorizationEmpNumTextBox.Text = "" Then
                user_position_descLabel.Text = ""
            End If

            Dim CerPosdataddl As New DropDownListBinder(CernerPosddl, "SelCernerPositionDesc", Session("EmployeeConn"))
            CerPosdataddl.AddDDLCriteria("@UserTypeCd", emp_type_cdLabel.Text, SqlDbType.NVarChar)
            CerPosdataddl.AddDDLCriteria("@Clientupd", "all", SqlDbType.NVarChar)

            CerPosdataddl.BindData("CernerPositionDesc", "CernerPositionDesc")

        End If
        ''' End Postback



        'unlock 2015
        'Select Case userDepartmentCD.Text
        '    Case "832600", "832700", "823100"
        '        If Session("SecurityLevelNum") < 90 And requestor_client_numLabel.Text = iUserNum Then
        '            btnSelectOnBehalfOf.Visible = True
        '            lblValidation.Text = "Must Change Requestor"
        '            btnSelectOnBehalfOf.BackColor() = Color.Yellow
        '            btnSelectOnBehalfOf.BorderColor() = Color.Black
        '            btnSelectOnBehalfOf.ForeColor() = Color.Black
        '            btnSelectOnBehalfOf.BorderStyle() = BorderStyle.Solid
        '            btnSelectOnBehalfOf.BorderWidth = Unit.Pixel(2)
        '        ElseIf Session("SecurityLevelNum") < 90 And requestor_client_numLabel.Text <> iUserNum Then
        '            btnSelectOnBehalfOf.Visible = False

        '        End If
        'End Select



        If Session("SecurityLevelNum") < 56 Then
			emp_type_cdrbl.Enabled = False
			btnSelectOnBehalfOf.Visible = False
			If ValidationStatus.ToLower = "pastvalidation" Then
				' tpAddItems.Enabled 
				grAvailableAccounts.Enabled = False
				lblValidation.Text = " This Request is in the process of completing, if you need to add more accounts please contact help Desk 15-2610"
				lblValidation.Visible = True
			End If
		End If

		If Session("SecurityLevelNum") = 65 Then
			emp_type_cdrbl.Enabled = False
			btnSelectOnBehalfOf.Visible = False

			If requestor_client_numLabel.Text <> iUserNum Then

				tpAddItems.Visible = False
				tpRemoveItems.Visible = False
				tpPendingRequests.Visible = False
				tpRequestItems.Visible = False
				TPActionItems.Visible = False

				' tpAddItems.Enabled 
				grAvailableAccounts.Enabled = False
				lblValidation.Text = " This Request is in the process of completing, if you need to add more accounts please contact help Desk 15-2610"
				lblValidation.Visible = True

			End If

		End If

		If Session("SecurityLevelNum") = 70 Then
			Dim ctrl As Control = DataCheckingAndValidation.SetChildControlsRecursiveToEnabledFalse(tpDemographics, False)
			tpAddItems.Visible = False
			tpRemoveItems.Visible = False
			tpPendingRequests.Visible = False
		End If


        If Session("SecurityLevelNum") = 82 Or Session("SecurityLevelNum") = 95 Or Session("LoginID").ToString.ToLower = "burgina" Or Session("LoginID").ToString.ToLower = "melej" Or Session("LoginID").ToString.ToLower = "atkm00" Then

            ' Only modify Credeintal Requests not Create accounts or Delete accounts
            If (account_request_typeLabel.Text = "credent" Or ValidationStatus.ToLower = "needscred") And close_dateTextBox.Text = "" Then 'NeedsCred

                Dim ctrl As Control = DataCheckingAndValidation.SetChildControlsRecursiveToEnabledFalse(tpDemographics, True)




                'CCMCadmitRightsrbl.Enabled = True

                'DCMHadmitRightsrbl.Enabled = True

                If CCMCadmitRightsrbl.SelectedValue.ToLower = "no" Then

                    credentialedDateTextBox.Enabled = False
                    credentialedDateTextBox.CssClass = "style2"

                    credentialedDateTextBox.BackColor() = Color.White
                    credentialedDateTextBox.BorderColor() = Nothing
                    credentialedDateTextBox.BorderStyle() = BorderStyle.Groove
                    credentialedDateTextBox.BorderWidth = Unit.Pixel(3)

                Else
                    credentialedDateTextBox.Enabled = True
                    credentialedDateTextBox.CssClass = "calenderClass"

                    credentialedDateTextBox.BackColor() = Color.Yellow
                    credentialedDateTextBox.BorderColor() = Color.Black
                    credentialedDateTextBox.BorderStyle() = BorderStyle.Solid
                    credentialedDateTextBox.BorderWidth = Unit.Pixel(2)


                End If

                'If DCMHadmitRightsrbl.SelectedValue.ToLower = "no" Then
                '    CredentialedDateDCMHTextBox.Enabled = False
                '    CredentialedDateDCMHTextBox.CssClass = "style2"

                '    CredentialedDateDCMHTextBox.BackColor() = Color.White
                '    CredentialedDateDCMHTextBox.BorderColor() = Nothing
                '    CredentialedDateDCMHTextBox.BorderStyle() = BorderStyle.Groove
                '    CredentialedDateDCMHTextBox.BorderWidth = Unit.Pixel(3)

                'Else
                '    CredentialedDateDCMHTextBox.Enabled = True
                '    CredentialedDateDCMHTextBox.CssClass = "calenderClass"

                '    CredentialedDateDCMHTextBox.BackColor() = Color.Yellow
                '    CredentialedDateDCMHTextBox.BorderColor() = Color.Black
                '    CredentialedDateDCMHTextBox.BorderStyle() = BorderStyle.Solid
                '    CredentialedDateDCMHTextBox.BorderWidth = Unit.Pixel(2)


                'End If

            Else

                tpRemoveItems.Visible = False
                tpAddItems.Visible = False
                tpPendingRequests.Visible = False
                'btnUpdate.Visible = True
                If receiver_client_numLabel.Text <> "" Then
                    btnClientDemo.Visible = True
                    btnClientDemo.Enabled = True

                End If


                If CCMCadmitRightsrbl.SelectedValue.ToLower = "no" Then
                    credentialedDateTextBox.Enabled = False
                    credentialedDateTextBox.CssClass = "style2"

                End If
                'If DCMHadmitRightsrbl.SelectedValue.ToLower = "no" Then
                '    CredentialedDateDCMHTextBox.Enabled = False
                '    CredentialedDateDCMHTextBox.CssClass = "style2"

                'End If


            End If



        Else
            ' no one else gets
            credentialedDateTextBox.Enabled = False
			CCMCadmitRightsrbl.Enabled = False
			credentialedDateTextBox.CssClass = "style2"

            'CredentialedDateDCMHTextBox.Enabled = False
            'DCMHadmitRightsrbl.Enabled = False
            'CredentialedDateDCMHTextBox.CssClass = "style2"

        End If

		If emp_type_cdrbl.SelectedValue <> RoleNumLabel.Text Then
			emp_type_cdLabel.Text = emp_type_cdrbl.SelectedValue
			mainRole = emp_type_cdrbl.SelectedValue

			SpecialtyLabel.Text = ""

			TitleLabel.Text = Nothing
			Select Case emp_type_cdrbl.SelectedValue

				Case "782", "1197"

					'Case "physician", "non staff clinician", "resident"

					Dim ddbTitleBinder As New DropDownListBinder(Titleddl, "SelTitlesV4", Session("EmployeeConn"))
					ddbTitleBinder.AddDDLCriteria("@titleType", "Physician", SqlDbType.NVarChar)
					ddbTitleBinder.BindData("TitleShortDesc", "TitleDesc")
				Case "1194"
					'Dim ddlBinder As New DropDownListBinder

					Dim ddbTitleBinder As New DropDownListBinder(Titleddl, "SelTitlesV4", Session("EmployeeConn"))
					ddbTitleBinder.AddDDLCriteria("@titleType", "Resident", SqlDbType.NVarChar)
					ddbTitleBinder.BindData("TitleShortDesc", "TitleDesc")

					'Case "1200"

					'    'Case "allied health""
					'    Dim ddbTitleBinder As New DropDownListBinder(Titleddl, "SelTitlesV4", Session("EmployeeConn"))
					'    ddbTitleBinder.AddDDLCriteria("@titleType", "Alliedhealth", SqlDbType.NVarChar)
					'    ddbTitleBinder.BindData("TitleShortDesc", "TitleDesc")

			End Select

			If TitleLabel.Text = "" Then
				Titleddl.SelectedIndex = -1
			Else
				Titleddl.SelectedValue = TitleLabel.Text

			End If

			If SpecialtyLabel.Text = "" Then
				Specialtyddl.SelectedIndex = -1
			Else
				Specialtyddl.SelectedValue = SpecialtyLabel.Text

			End If


		End If

		'If Session("SecurityLevelNum") < 70 And siemens_emp_numLabel.Text <> "" Then

		'    Dim ctrl As Control = DataCheckingAndValidation.SetChildControlsRecursiveToEnabledFalse(tpDemographics, False)

		'End If




		If receiver_client_numLabel.Text <> "" Then
			btnClientDemo.Visible = True
			btnClientDemo.Enabled = True

		End If

		If mainRole = "1194" Then

			SpecialtyLabel.Text = "Resident"
			SpecialtyLabel.Visible = True
			Specialtyddl.Visible = False

			credentialedDateTextBox.Enabled = False
			credentialedDateTextBox.CssClass = "style2"

            'CredentialedDateDCMHTextBox.Enabled = False
            'CredentialedDateDCMHTextBox.CssClass = "style2"

            CCMCadmitRightsrbl.SelectedValue = "No"
			CCMCadmitRightsrbl.Enabled = False

            'DCMHadmitRightsrbl.SelectedValue = "No"
            'DCMHadmitRightsrbl.Enabled = False

        End If

		displayPendingQueues()
		fillRequestItemsTable()

		CurrentAccounts()
        AvailableAccounts()

        If Session("SecurityLevelNum") = 95 Or Session("LoginID").ToString.ToLower = "burgina" Or Session("LoginID").ToString.ToLower = "melej" Or Session("LoginID").ToString.ToLower = "atkm00" Then

            If close_dateTextBox.Text <> "" Then
                TpEditGrid.Visible = False
            Else
                TpEditGrid.Visible = True
                fillEditItemstable()
            End If


        End If

        fillEditItemstable()
        'SelRequestItemsShortV20
        'CheckRequiredFields()

        If (account_request_typeLabel.Text = "credent" Or ValidationStatus.ToLower = "needscred") And close_dateTextBox.Text = "" Then 'NeedsCred
			tpAddItems.Visible = False
			tpRemoveItems.Visible = False
			tpPendingRequests.Visible = False
			btnClientDemo.Visible = False
		ElseIf close_dateTextBox.Text <> "" Then
			btnSelectOnBehalfOf.Visible = False
			btnUpdate.Visible = False
			If receiver_client_numLabel.Text <> "" Then
				btnClientDemo.Visible = True
				btnClientDemo.Enabled = True
			End If
		End If

        If Session("SecurityLevelNum") = 95 Or Session("LoginID").ToString.ToLower = "burgina" Or Session("LoginID").ToString.ToLower = "melej" Then
            AuthorizationEmpNumTextBox.Enabled = True
            DemofirstnameTextBox.Enabled = True
            DemolastnameTextBox.Enabled = True
            first_nameTextBox.Enabled = True
            last_nameTextBox.Enabled = True

        End If

        emp_type_cdrbl.Enabled = False

		ActionControl.GetAllActions(iRequestNum, Session("EmployeeConn"))
		ActionList = ActionControl.AllActions()
		TPActionItems.Controls.Add(fillActionItemsTable(ActionControl.AllActions()))

	End Sub
	Protected Sub AvailableAccounts()


		grAvailableAccounts.DataSource = ViewState("dtAvailableAccounts")
		grAvailableAccounts.DataBind()


	End Sub
	Protected Sub CurrentAccounts()



		gvCurrentApps.DataSource = ViewState("dtAccounts")
		gvCurrentApps.DataBind()

		'gvRemoveAccounts.DataSource = ViewState("dtRemoveAccounts")
		'gvRemoveAccounts.DataBind()




	End Sub
	Protected Sub btnClientDemo_Click(sender As Object, e As System.EventArgs) Handles btnClientDemo.Click
		If Deactivated = "deactivated" Then
			Response.Redirect("~/DeactivatedAccount.aspx?ClientNum=" & receiver_client_numLabel.Text, True)
		Else
			Response.Redirect("~/ClientDemo.aspx?ClientNum=" & receiver_client_numLabel.Text, True)

		End If

	End Sub
	Private Sub BtnPrintReq_Click(sender As Object, e As EventArgs) Handles btnPrintReq.Click
		Response.Redirect("~/PrintAccountRequest.aspx?RequestNum=" & iRequestNum & "&ClientNum=" & receiver_client_numLabel.Text, True)

	End Sub

    'Protected Sub btnDemographicRequest_Click(sender As Object, e As System.EventArgs) Handles btnDemographicRequest.Click
    '    Response.Redirect("~/providerDemo.aspx?clientnum=" & receiver_client_numLabel.Text, True)

    'End Sub

    Private Sub insAcctItems(ByVal requestNum As Integer, ByVal appNum As Integer, ByVal comments As String)




        Dim thisdata As New CommandsSqlAndOleDb("InsAccountItemsV3", Session("EmployeeConn"))
        thisdata.AddSqlProcParameter("@account_request_num", requestNum, SqlDbType.Int, 20)
        thisdata.AddSqlProcParameter("@ApplicationNum", appNum, SqlDbType.Int, 8)
        thisdata.AddSqlProcParameter("@account_item_desc", comments, SqlDbType.NVarChar, 255)


        If ValidationStatus.ToLower = "pastvalidation" Then
            thisdata.AddSqlProcParameter("@validate", "validated", SqlDbType.NVarChar, 20)
        Else
            Select Case emp_type_cdrbl.SelectedValue
                'Case "physician/rovider", "non staff clinician", "resident"  "allied health"

                Case "782", "1197", "1194"

                    thisdata.AddSqlProcParameter("@validate", "NeedsDocNum", SqlDbType.NVarChar, 20)

                Case Else
                    thisdata.AddSqlProcParameter("@validate", "NeedsValidation", SqlDbType.NVarChar, 20)

            End Select
        End If

        thisdata.AddSqlProcParameter("@who_nt_login", Session("LoginID"), SqlDbType.NVarChar, 50)



        thisdata.ExecNonQueryNoReturn()

    End Sub
    Protected Sub btnRemoveAccts_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnInvalidAccts.Click
        Try

            '" Deleted This account from request."

            If txtComment.Text = "" Then
                txtComment.Text = " Deleted This account from request."
            End If

            For Each rw As GridViewRow In gvRemoveAccounts.Rows
                If rw.RowType = DataControlRowType.DataRow Then

                    Dim account_request_seq_num As String = gvRemoveAccounts.DataKeys(rw.RowIndex)("account_request_seq_num")





                    Dim thisData As New CommandsSqlAndOleDb("UpdValidateRequestItemsV4", Session("EmployeeConn"))
                    thisData.AddSqlProcParameter("@account_request_num", iRequestNum, SqlDbType.Int)
                    thisData.AddSqlProcParameter("@nt_login", Session("LoginID"), SqlDbType.NVarChar, 50)
                    'thisData.AddSqlProcParameter("@validate", "deleted", SqlDbType.NVarChar, 10)
                    thisData.AddSqlProcParameter("@validate", "invalid", SqlDbType.NVarChar, 10)

                    thisData.AddSqlProcParameter("@account_request_seq_num", account_request_seq_num, SqlDbType.Int)
                    thisData.AddSqlProcParameter("@action_desc", txtComment.Text, SqlDbType.NVarChar, 300)




                    thisData.ExecNonQueryNoReturn()


                End If



            Next

        Catch ex As Exception

        End Try
        ' after adding these items reload entire request

        Response.Redirect("~/AccountRequestInfo2.aspx?RequestNum=" & iRequestNum, True)

        ' Page.Response.Redirect(Page.Request.Url.ToString(), False)
    End Sub
    Private Sub btnCloseEntireRequest_Click(sender As Object, e As EventArgs) Handles btnCloseEntireRequest.Click
        Try
            Dim thisData As New CommandsSqlAndOleDb("CloseEntireRequest", Session("EmployeeConn"))
            thisData.AddSqlProcParameter("@account_request_num", iRequestNum, SqlDbType.Int)
            thisData.AddSqlProcParameter("@ntlogin", Session("LoginID"), SqlDbType.NVarChar, 50)

            thisData.ExecNonQueryNoReturn()

        Catch ex As Exception

        End Try


        Response.Redirect("~/AccountRequestInfo2.aspx?RequestNum=" & iRequestNum, True)

    End Sub

    Protected Sub Titleddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles Titleddl.SelectedIndexChanged
		TitleLabel.Text = Titleddl.SelectedValue
		mainupd.Update()

	End Sub
	Protected Sub Vendorddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles Vendorddl.SelectedIndexChanged
		Dim vendornum As String = Vendorddl.SelectedValue
		Dim vendorname As String = Vendorddl.SelectedItem.ToString

		VendorFormSignOn = New FormSQL(Session("EmployeeConn"), "SelVendors", "InsUpdVendors", "", Page)
		VendorFormSignOn.AddSelectParm("@VendorNum", vendornum, SqlDbType.Int, 6)

		If vendornum <> 0 Then
			VendorFormSignOn.FillForm()
			'           address_1textbox.Text = address1TextBox.Text
			VendorNameTextBox.Text = vendorname

		Else
			Vendorddl.Visible = False
			VendorNameTextBox.Visible = True

		End If



		'SelVendors @vendorNum = 1
	End Sub
	Protected Sub btnSubmitAddRequest_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmitAddRequest.Click

		Select Case userDepartmentCD.Text
			Case "832600", "832700", "823100"
				If Session("SecurityLevelNum") < 90 And requestor_client_numLabel.Text = submitter_client_numLabel.Text Then
					btnSelectOnBehalfOf.Visible = True
					lblValidation.Text = "Must Change Requestor to Add more accounts to this request."
					lblValidation.Visible = True
					btnSelectOnBehalfOf.BackColor() = Color.Yellow
					btnSelectOnBehalfOf.BorderColor() = Color.Black
					btnSelectOnBehalfOf.ForeColor() = Color.Black
					btnSelectOnBehalfOf.BorderStyle() = BorderStyle.Solid
					btnSelectOnBehalfOf.BorderWidth = Unit.Pixel(2)
					btnSelectOnBehalfOf.Focus()
					Return
				ElseIf Session("SecurityLevelNum") < 90 And requestor_client_numLabel.Text <> submitter_client_numLabel.Text Then
					btnSelectOnBehalfOf.Visible = False

					'lblValidation.Text = ""
					'btnSelectOnBehalfOf.BackColor() = Color.FromKnownColor("336666")
					'btnSelectOnBehalfOf.ForeColor() = Color.White
					'btnSelectOnBehalfOf.BorderStyle() = BorderStyle.Groove
					'btnSelectOnBehalfOf.BorderWidth = Unit.Pixel(1)

				End If
		End Select

		Dim i As Integer

		If EmpDoctorTextBox.Text <> "" Then

			Try
				i = Convert.ToInt32(EmpDoctorTextBox.Text)
			Catch
				EmpDoctorTextBox.Text = ""
				lblValidation.Text = "Invalid Doctor Master Number - Numbers Only"
				Exit Sub
			End Try
			doctor_master_numTextBox.Text = EmpDoctorTextBox.Text

		End If

		Try

			' Dim AccountCode As String

			For Each rw As GridViewRow In grAccountsToAdd.Rows
				If rw.RowType = DataControlRowType.DataRow Then

					Dim appNum As String = grAccountsToAdd.DataKeys(rw.RowIndex)("Applicationnum")
					insAcctItems(iRequestNum, appNum, txtComment.Text)


					'If BusLog.isHospSpecific(appNum) Then
					'    For Each cbFacility As ListItem In cblFacilities.Items

					'        If cbFacility.Selected Then

					'            AccountCode = BusLog.GetApplicationCode(appNum, cbFacility.Value)

					'            insAcctItems(iRequestNum, AccountCode, "")

					'        End If

					'    Next

					'ElseIf BusLog.isEntSpecific(appNum) Then

					'    For Each cbEnt As ListItem In cblEntities.Items

					'        If cbEnt.Selected Then

					'            AccountCode = BusLog.GetApplicationCode(appNum, cbEnt.Value)

					'            insAcctItems(iRequestNum, AccountCode, "")

					'        End If

					'    Next


					'Else

					'    insAcctItems(iRequestNum, appNum, "")

					'End If



				End If

			Next

		Catch ex As Exception

		End Try

		If Session("environment") = "" Then
			Dim Envoirn As New Environment()
			Session("environment") = Envoirn.getEnvironment
		End If


		SendProviderEmail(iRequestNum)

		sendAlertEmail()

		' after adding these items reload entire request

		Response.Redirect("~/AccountRequestInfo2.aspx?RequestNum=" & iRequestNum, True)

		' Page.Response.Redirect(Page.Request.Url.ToString(), False)

	End Sub
	Private Sub sendAlertEmail()
		Dim Request As New AccountRequest(iRequestNum, Session("EmployeeConn"))
		Dim RequestItems As New RequestItems(iRequestNum)

		Dim EmailBody As New EmailBody
		'    Dim sCss As String = File.ReadAllText(Server.MapPath("/Styles/Tables.css"))



		'======================
		' Get current path
		'======================

		Dim path As String
		Dim directory As String = ""
		path = HttpContext.Current.Request.Url.AbsoluteUri
		Dim pathParts() As String = Split(path, "/")
		Dim i As Integer
		Do While i < pathParts.Length() - 1

			directory = directory + pathParts(i) + "/"
			i = i + 1

		Loop

		'======================

		Dim EmailList As New List(Of EmailDistribution)
		EmailList = New EmailDistributionList().EmailList

		Dim sndEmailTo = From r In Request.RequestItems()
						 Join e In EmailList
						 On r.AppBase Equals e.AppBase
						 Where e.EmailEvent = "AddAcct"
						 Select e, r

		For Each EmailAddress In sndEmailTo



			Dim toEmail As String = EmailAddress.e.Email





			Dim sSubject As String
			Dim sBody As String


			sSubject = "Account Request for " & Request.FirstName & " " & Request.LastName & " has been added to the " & EmailAddress.r.ApplicationDesc.ToUpper() & " Queue. 1"


			sBody = "<!DOCTYPE html>" &
								"<html>" &
								"<head>" &
								"<style>" &
								"table" &
								"{" &
								"border-collapse:collapse;" &
								"font-family: 'Helvetica Neue', 'Lucida Grande', 'Segoe UI', Arial, Helvetica, Verdana, sans-serif;" &
								"}" &
								"table, th, td" &
								"{" &
								"border: 1px solid black;" &
								"padding: 3px" &
								"}" &
								"</style>" &
								"</head>" &
								"" &
								"<body>" &
								"A new account request has been added to the " & EmailAddress.r.ApplicationDesc & " Queue.  Follow the link below to go to Queue Screen." &
								 "<br />" &
								"<a href=""" & directory & "/CreateAccount.aspx?requestnum=" & EmailAddress.r.AccountRequestNum & "&account_seq_num=" & EmailAddress.r.AccountRequestSeqNum & """ ><u><b> " & EmailAddress.r.ApplicationDesc.ToUpper() & ": " & Request.FirstName & " " & Request.LastName & "</b></u></a>" &
								"</body>" &
							"</html>"






			Dim sToAddress As String = EmailAddress.e.Email

			If Session("environment").ToString.ToLower = "testing" Then
				sToAddress = "Jeff.Mele@crozer.org"
				sBody = sBody & sToAddress

			End If


			Dim mailObj As New MailMessage()
			mailObj.From = New MailAddress("CSC@crozer.org")
			mailObj.To.Add(New MailAddress(sToAddress))
			mailObj.IsBodyHtml = True
			mailObj.Subject = sSubject
			mailObj.Body = sBody


			Dim smtpClient As New SmtpClient()
            smtpClient.Host = "ah1smtprelay01.altahospitals.com"

            'smtpClient.Host = "Smtp2.Crozer.Org"

            smtpClient.Send(mailObj)




		Next




	End Sub
	Protected Sub emp_type_cdrbl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles emp_type_cdrbl.SelectedIndexChanged

		emp_type_cdLabel.Text = emp_type_cdrbl.SelectedValue


		Dim removeCss As New PageControls(Page)
		removeCss.RemoveCssClass(Page, "textInputRequired")

        If mainRole <> emp_type_cdrbl.SelectedValue Then
			' Clear out for Drop downs
			SpecialtLabel.Text = ""

			'Rolesddl

		End If
		Select Case emp_type_cdrbl.SelectedValue
			'Case "physician/rovider", "non staff clinician", "resident"

			Case "782", "1197"

				Dim ddbTitleBinder As New DropDownListBinder(Titleddl, "SelTitlesV4", Session("EmployeeConn"))
				ddbTitleBinder.AddDDLCriteria("@titleType", "Physician", SqlDbType.NVarChar)
				ddbTitleBinder.BindData("TitleShortDesc", "TitleDesc")

				Titleddl.SelectedIndex = -1



				If emp_type_cdrbl.SelectedValue = "1197" Then
					Dim ddbVendorBinder As New DropDownListBinder(Vendorddl, "SelVendors", Session("EmployeeConn"))
					ddbVendorBinder.AddDDLCriteria("@type", "contractor", SqlDbType.NVarChar)
					'lblVendor.Text = "Contractor Name:"
					ddbVendorBinder.BindData("VendorNum", "VendorName")

				End If

				'If TitleLabel.Text = "" Then
				'    Titleddl.SelectedIndex = -1
				'Else
				'    Titleddl.SelectedValue = TitleLabel.Text

				'End If

				'Specialtyddl.SelectedIndex = -1

				If SpecialtLabel.Text = "" Then
					Specialtyddl.SelectedIndex = -1
				Else
					Specialtyddl.SelectedValue = SpecialtLabel.Text

				End If

				thisRole = emp_type_cdrbl.SelectedValue
				mainRole = emp_type_cdrbl.SelectedValue

				Title2TextBox.Visible = False
				Titleddl.Visible = True

				SpecialtLabel.Visible = False
				Specialtyddl.Visible = True

				pnlProvider.Visible = True

				CCMCadmitRightsrbl.SelectedIndex = -1
				CCMCadmitRightsrbl.Enabled = True

                'DCMHadmitRightsrbl.SelectedIndex = -1
                'DCMHadmitRightsrbl.Enabled = True

                'Case "allied health"

            Case "1194"
                'residents

                Dim ddbTitleBinder As New DropDownListBinder(Titleddl, "SelTitlesV4", Session("EmployeeConn"))
				ddbTitleBinder.AddDDLCriteria("@titleType", "Resident", SqlDbType.NVarChar)
				ddbTitleBinder.BindData("TitleShortDesc", "TitleDesc")

                Dim ddlRoleBinder As New DropDownListBinder(Rolesddl, "SelEmployeeRolesV3", Session("EmployeeConn"))
                ddlRoleBinder.AddDDLCriteria("@RoleNum", "1194", SqlDbType.NVarChar)
				ddlRoleBinder.BindData("rolenum", "RoleDesc")

				'If TitleLabel.Text = "" Then
				'    Titleddl.SelectedIndex = -1
				'Else
				'    Titleddl.SelectedValue = TitleLabel.Text

				'End If


				'If TitleLabel.Text = "" Then

				'    Title2TextBox.Text = "Resident"
				'    Title2TextBox.Visible = True


				'End If

				Title2TextBox.Text = "Resident"
				Title2TextBox.Visible = True
				Titleddl.Visible = False

				SpecialtLabel.Text = "Resident"
				SpecialtLabel.Visible = True
				Specialtyddl.Visible = False


				credentialedDateTextBox.Enabled = False
				credentialedDateTextBox.CssClass = "style2"

                'CredentialedDateDCMHTextBox.Enabled = False
                'CredentialedDateDCMHTextBox.CssClass = "style2"

                CCMCadmitRightsrbl.SelectedValue = "No"
				CCMCadmitRightsrbl.Enabled = False

                'DCMHadmitRightsrbl.SelectedValue = "No"
                'DCMHadmitRightsrbl.Enabled = False

                'Dim ddlSpecialityBinder As New DropDownListBinder(Specialtyddl, "SelSpecialtyCodesV2", Session("EmployeeConn"))
                'ddlSpecialityBinder.AddDDLCriteria("@SpecialtyType", "Resident", SqlDbType.NVarChar)
                'ddlSpecialityBinder.BindData("specialty", "specialty")

                'If SpecialtLabel.Text = "" Then

                '    Specialtyddl.SelectedValue = "Resident"
                'Else
                '    Specialtyddl.SelectedValue = SpecialtLabel.Text

                'End If

                ' in titleddl will be rolenum for residents which will be the new thisrole
                'ddlBinder.BindData(Rolesddl, "SelEmployeeRolesV2", "rolenum", "RoleDesc", Session("EmployeeConn"))

                thisRole = emp_type_cdrbl.SelectedValue
				mainRole = emp_type_cdrbl.SelectedValue

				pnlProvider.Visible = True

				'Case "1200"

				'    Dim ddbTitleBinder As New DropDownListBinder(Titleddl, "SelTitlesV4", Session("EmployeeConn"))
				'    ddbTitleBinder.AddDDLCriteria("@titleType", "Alliedhealth", SqlDbType.NVarChar)
				'    ddbTitleBinder.BindData("TitleShortDesc", "TitleDesc")

				'    Titleddl.SelectedIndex = -1


				'    Specialtyddl.SelectedIndex = -1

				'    Title2TextBox.Visible = False
				'    Titleddl.Visible = True

				'    SpecialtLabel.Visible = False
				'    Specialtyddl.Visible = True

				'    pnlProvider.Visible = True
				'    thisRole = emp_type_cdrbl.SelectedValue
				'    mainRole = emp_type_cdrbl.SelectedValue

				'    CCMCadmitRightsrbl.SelectedIndex = -1
				'    CCMCadmitRightsrbl.Enabled = True

				'    DCMHadmitRightsrbl.SelectedIndex = -1
				'    DCMHadmitRightsrbl.Enabled = True

			Case "1195", "1198"
				' load vendor list 
				'make vendor text box invisiable
				' make ddl appear
				' 1195 Contractor list
				' 1198 Vendor list
				Vendorddl.Visible = True
				VendorNameTextBox.Visible = False

				Dim ddbVendorBinder As New DropDownListBinder(Vendorddl, "SelVendors", Session("EmployeeConn"))
				ddbVendorBinder.AddDDLCriteria("@type", "vendor", SqlDbType.NVarChar)
				ddbVendorBinder.BindData("VendorNum", "VendorName")

				'If emp_type_cdrbl.SelectedValue = "1198" Then
				'    ddbVendorBinder.AddDDLCriteria("@type", "vendor", SqlDbType.NVarChar)
				'    lblVendor.Text = "Vendor Name:"

				'ElseIf emp_type_cdrbl.SelectedValue = "1195" Then

				'    ddbVendorBinder.AddDDLCriteria("@type", "contractor", SqlDbType.NVarChar)
				'    lblVendor.Text = "Contractor Name:"
				'Else

				'End If
				thisRole = emp_type_cdrbl.SelectedValue
				mainRole = emp_type_cdrbl.SelectedValue

			Case Else
				Title2TextBox.Visible = False
				Titleddl.Visible = True

				SpecialtLabel.Visible = False
				Specialtyddl.Visible = True

				pnlProvider.Visible = False

				thisRole = emp_type_cdrbl.SelectedValue
				mainRole = emp_type_cdrbl.SelectedValue


		End Select


        CheckRequiredFields()

		'srolenum = emp_type_cdrbl.SelectedItem
		Dim ddlBinderV2 As DropDownListBinder

        ddlBinderV2 = New DropDownListBinder(Rolesddl, "SelEmployeeRolesV3", Session("EmployeeConn"))

        ddlBinderV2.AddDDLCriteria("@RoleNum", mainRole, SqlDbType.NVarChar)
		ddlBinderV2.BindData("rolenum", "roledesc")

		Rolesddl.SelectedValue = mainRole


        'Dim BusLog As New AccountBusinessLogic
        'BusLog.CheckAccountDependancy(cblApplications)

        'If BusLog.eCare Then
        '    pnlTCl.Visible = True
        '    'Dim reqField As New RequiredField(TCLTextBox, "TCL for eCare")

        'Else
        '    pnlTCl.Visible = False

        '    'pageCtrl.RemoveCssClass(pnlTCl, "textInputRequired")

        'End If
    End Sub
	Protected Sub CCMCadmitRightsrbl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles CCMCadmitRightsrbl.SelectedIndexChanged

        If CCMCadmitRightsrbl.SelectedValue.ToLower = "no" Then

            'CCMCConsultsrbl.SelectedValue = "No"
            'CCMCConsultsrbl.Enabled = False

            'DCMHConsultsrbl.SelectedValue = "No"
            'DCMHConsultsrbl.Enabled = False

            'TaylorConsultsrbl.SelectedValue = "No"
            'TaylorConsultsrbl.Enabled = False

            'SpringfieldConsultsrbl.SelectedValue = "No"
            'SpringfieldConsultsrbl.Enabled = False

            'Writeordersrbl.SelectedValue = "No"
            'Writeordersrbl.Enabled = False

            'WriteordersDCMHrbl.SelectedValue = "No"
            'WriteordersDCMHrbl.Enabled = False

            credentialedDateTextBox.Enabled = False
            credentialedDateTextBox.CssClass = "style2"

            'credentialedDateDCMHTextBox.Enabled = False
            'credentialedDateDCMHTextBox.CssClass = "style2"


        End If

        'CCMCConsultsrbl.SelectedIndex = -1
        'CCMCConsultsrbl.Enabled = True

        'DCMHConsultsrbl.SelectedIndex = -1
        'DCMHConsultsrbl.Enabled = True

        'TaylorConsultsrbl.SelectedIndex = -1
        'TaylorConsultsrbl.Enabled = True

        'SpringfieldConsultsrbl.SelectedIndex = -1
        'SpringfieldConsultsrbl.Enabled = True

        'Writeordersrbl.SelectedIndex = -1
        'Writeordersrbl.Enabled = True

        'WriteordersDCMHrbl.SelectedIndex = -1
        'WriteordersDCMHrbl.Enabled = True



        If CCMCadmitRightsrbl.SelectedValue.ToLower = "no" Then
			credentialedDateTextBox.Enabled = False
			credentialedDateTextBox.CssClass = "style2"

		End If

		If CCMCadmitRightsrbl.SelectedValue.ToLower = "yes" Then
			If Session("SecurityLevelNum") = 82 Or Session("LoginID") = "melej" Or Session("LoginID") = "geip00" Or Session("LoginID") = "RalphC" Then
				credentialedDateTextBox.Enabled = True
				credentialedDateTextBox.CssClass = "calenderClass"

			End If


		End If

		If thisRole = "1194" Then
			credentialedDateTextBox.Enabled = False
			credentialedDateTextBox.CssClass = "style2"

            'credentialedDateDCMHTextBox.Enabled = False
            'credentialedDateDCMHTextBox.CssClass = "style2"
        End If

	End Sub

    'Protected Sub DCMHadmitRightsrbl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles DCMHadmitRightsrbl.SelectedIndexChanged
    '	If CCMCadmitRightsrbl.SelectedValue.ToLower = "no" And DCMHadmitRightsrbl.SelectedValue.ToLower = "no" Then

    '           'CCMCConsultsrbl.SelectedValue = "No"
    '           'CCMCConsultsrbl.Enabled = False

    '           'DCMHConsultsrbl.SelectedValue = "No"
    '           'DCMHConsultsrbl.Enabled = False

    '           'TaylorConsultsrbl.SelectedValue = "No"
    '           'TaylorConsultsrbl.Enabled = False

    '           'SpringfieldConsultsrbl.SelectedValue = "No"
    '           'SpringfieldConsultsrbl.Enabled = False

    '           'Writeordersrbl.SelectedValue = "No"
    '           'Writeordersrbl.Enabled = False

    '           'WriteordersDCMHrbl.SelectedValue = "No"
    '           'WriteordersDCMHrbl.Enabled = False

    '           credentialedDateTextBox.Enabled = False
    '		credentialedDateTextBox.CssClass = "style2"




    '		credentialedDateDCMHTextBox.Enabled = False
    '		credentialedDateDCMHTextBox.CssClass = "style2"

    '	End If

    '       'CCMCConsultsrbl.SelectedIndex = -1
    '       'CCMCConsultsrbl.Enabled = True

    '       'DCMHConsultsrbl.SelectedIndex = -1
    '       'DCMHConsultsrbl.Enabled = True

    '       'TaylorConsultsrbl.SelectedIndex = -1
    '       'TaylorConsultsrbl.Enabled = True

    '       'SpringfieldConsultsrbl.SelectedIndex = -1
    '       'SpringfieldConsultsrbl.Enabled = True

    '       'Writeordersrbl.SelectedIndex = -1
    '       'Writeordersrbl.Enabled = True

    '       'WriteordersDCMHrbl.SelectedIndex = -1
    '       'WriteordersDCMHrbl.Enabled = True

    '       If DCMHadmitRightsrbl.SelectedValue.ToLower = "no" Then
    '		CredentialedDateDCMHTextBox.Enabled = False
    '		CredentialedDateDCMHTextBox.CssClass = "style2"

    '	End If

    '	If DCMHadmitRightsrbl.SelectedValue.ToLower = "yes" Then
    '		If Session("SecurityLevelNum") = 82 Or Session("LoginID") = "melej" Or Session("LoginID") = "geip00" Or Session("LoginID") = "RalphC" Then
    '			CredentialedDateDCMHTextBox.Enabled = True
    '			CredentialedDateDCMHTextBox.CssClass = "calenderClass"

    '		End If


    '	End If


    '	'If CCMCadmitRightsrbl.SelectedValue.ToLower = "yes" Or DCMHadmitRightsrbl.SelectedValue.ToLower = "yes" Then

    '	'    CCMCConsultsrbl.SelectedIndex = -1
    '	'    CCMCConsultsrbl.Enabled = True

    '	'    DCMHConsultsrbl.SelectedIndex = -1
    '	'    DCMHConsultsrbl.Enabled = True

    '	'    TaylorConsultsrbl.SelectedIndex = -1
    '	'    TaylorConsultsrbl.Enabled = True

    '	'    SpringfieldConsultsrbl.SelectedIndex = -1
    '	'    SpringfieldConsultsrbl.Enabled = True

    '	'    Writeordersrbl.SelectedIndex = -1
    '	'    Writeordersrbl.Enabled = True

    '	'    WriteordersDCMHrbl.SelectedIndex = -1
    '	'    WriteordersDCMHrbl.Enabled = True

    '	'    If Session("SecurityLevelNum") = 62 Or Session("LoginID") = "melej" Then
    '	'        credentialedDateTextBox.Enabled = True
    '	'        credentialedDateTextBox.CssClass = "calenderClass"

    '	'        credentialedDateDCMHTextBox.Enabled = True
    '	'        credentialedDateDCMHTextBox.CssClass = "calenderClass"

    '	'    End If

    '	'End If

    '	If thisRole = "1194" Then
    '		credentialedDateTextBox.Enabled = False
    '		credentialedDateTextBox.CssClass = "style2"

    '           'credentialedDateDCMHTextBox.Enabled = False
    '           'credentialedDateDCMHTextBox.CssClass = "style2"
    '       End If

    'End Sub
    Private Sub displayPendingQueues()
		GetPendingQueues()


		If dtPendingRequests.Rows.Count > 0 Then

			'  cblApplications.Enabled = False


			'Dim rwTitle As New TableRow
			'Dim clTitle As New TableCell


			Dim iRowCount As Integer = 1

			Dim rwHeader As New TableRow
			Dim clAccountHdr As New TableCell
			Dim clRequestTypeHdr As New TableCell
			Dim clValidCdHdr As New TableCell
			Dim clRequestorHdr As New TableCell
			Dim clDateHdr As New TableCell
			Dim clRequestTypeDescHdr As New TableCell

			'clTitle.Text = "Pending Requests"
			'clTitle.Font.Bold = True
			'clTitle.ColumnSpan = 6
			'clTitle.BackColor = Color.White
			'clTitle.HorizontalAlign = HorizontalAlign.Center
			'clTitle.Font.Size = 12



			'rwTitle.Cells.Add(clTitle)

			'tblPendingQueues.Rows.Add(rwTitle)

			clAccountHdr.Text = "Account"
			clRequestTypeHdr.Text = "Request"
			clRequestTypeDescHdr.Text = "Type"
			clValidCdHdr.Text = "Status"
			clRequestorHdr.Text = "Requester"
			clDateHdr.Text = "Request Date"

			rwHeader.Cells.Add(clAccountHdr)
			rwHeader.Cells.Add(clRequestTypeHdr)
			rwHeader.Cells.Add(clRequestTypeDescHdr)
			rwHeader.Cells.Add(clValidCdHdr)
			rwHeader.Cells.Add(clRequestorHdr)
			rwHeader.Cells.Add(clDateHdr)

			rwHeader.Font.Bold = True
			rwHeader.BackColor = Color.FromName("#006666")
			rwHeader.ForeColor = Color.FromName("White")
			rwHeader.Height = Unit.Pixel(36)

			tblPendingQueues.Rows.Add(rwHeader)

			tblPendingQueues.Width = New Unit("100%")



			For Each rwPending As DataRow In dtPendingRequests.Rows

				Dim rwData As New TableRow
				Dim clAccountData As New TableCell
				Dim clRequestTypeData As New TableCell
				Dim clValidCdData As New TableCell
				Dim clRequestorData As New TableCell
				Dim clDateData As New TableCell
				Dim clSeqnum As New TableCell
				Dim clReqNum As New TableCell

				clAccountData.Text = IIf(IsDBNull(rwPending("ApplicationDesc")), "", rwPending("ApplicationDesc"))
				clRequestTypeData.Text = IIf(IsDBNull(rwPending("account_request_type")), "", rwPending("account_request_type"))
				clValidCdData.Text = IIf(IsDBNull(rwPending("valid_cd")), "", rwPending("valid_cd"))
				clRequestorData.Text = IIf(IsDBNull(rwPending("requestor_name")), "", rwPending("requestor_name"))
				clDateData.Text = IIf(IsDBNull(rwPending("entered_date")), "", rwPending("entered_date"))
				clSeqnum.Text = IIf(IsDBNull(rwPending("account_request_seq_num")), "", rwPending("account_request_seq_num"))

				'account_request_type
				If clRequestTypeData.Text = "addnew" Or clRequestTypeData.Text = "addacct" Or clRequestTypeData.Text = "rehire" Or clRequestTypeData.Text.ToLower = "addpmhact" Then
					sAccountType = "add"

					clReqNum.Text = "<a href=""CreateAccount.aspx?requestnum=" & iRequestNum & "&account_seq_num=" & clSeqnum.Text & """ ><u><b> " & iRequestNum & "</b></u></a>"
				ElseIf clRequestTypeData.Text = "delacct" Or clRequestTypeData.Text = "delete" Then

					sAccountType = clRequestTypeData.Text

					clReqNum.Text = "<a href=""DeleteAccount.aspx?requestnum=" & iRequestNum & "&account_seq_num=" & clSeqnum.Text & "&AccountType=" & sAccountType & """ ><u><b> " & iRequestNum & "</b></u></a>"

				ElseIf clRequestTypeData.Text = "terminate" Then

					sAccountType = "terminate"
					clReqNum.Text = "<a href=""DeleteAccount.aspx?requestnum=" & iRequestNum & "&account_seq_num=" & clSeqnum.Text & "&AccountType=" & sAccountType & """ ><u><b> " & iRequestNum & "</b></u></a>"

				ElseIf clRequestTypeData.Text = "demoupd" Then

					clReqNum.Text = "<a href=""ProviderDemo.aspx?requestnum=" & iRequestNum & "&ClientNum=" & receiver_client_numLabel.Text & """ ><u><b> " & iRequestNum & "</b></u></a>"

				Else
					sAccountType = "invalid"

					clReqNum.Text = "<a href=""DeleteAccount.aspx?requestnum=" & iRequestNum & "&account_seq_num=" & clSeqnum.Text & "&AccountType=" & sAccountType & """ ><u><b> " & iRequestNum & "</b></u></a>"

				End If

				rwData.Cells.Add(clReqNum)
				rwData.Cells.Add(clAccountData)
				rwData.Cells.Add(clRequestTypeData)
				rwData.Cells.Add(clValidCdData)
				rwData.Cells.Add(clRequestorData)
				rwData.Cells.Add(clDateData)


				If iRowCount > 0 Then

					rwData.BackColor = Color.Bisque

				Else

					rwData.BackColor = Color.LightGray

				End If



				iRowCount = iRowCount * -1


				tblPendingQueues.Rows.Add(rwData)


			Next

			tpPendingRequests.Visible = True
		Else

			tpPendingRequests.Visible = False
		End If

		If Session("SecurityLevelNum") <= 55 Then
			tpPendingRequests.Visible = False

		End If

		If Session("SecurityLevelNum") = 70 Then
			tpPendingRequests.Visible = False

		End If

	End Sub
	Private Sub GetPendingQueues()
		Dim thisdata As New CommandsSqlAndOleDb("SelPendingQueuesByRequestNum", Session("EmployeeConn"))
		thisdata.AddSqlProcParameter("@account_request_num", iRequestNum, SqlDbType.Int, 20)

		dtPendingRequests = thisdata.GetSqlDataTable

	End Sub
	Protected Sub gvCurrentApps_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvCurrentApps.RowCommand

		If e.CommandName = "Select" Then


			Dim index As Integer = Convert.ToInt32(e.CommandArgument)
			Dim AccountReqNum As String = gvCurrentApps.DataKeys(Convert.ToInt32(e.CommandArgument))("account_request_seq_num").ToString()
			Dim ApplicationNum As String = gvCurrentApps.DataKeys(Convert.ToInt32(e.CommandArgument))("ApplicationNum").ToString()
			Dim ApplicationDesc As String = gvCurrentApps.DataKeys(Convert.ToInt32(e.CommandArgument))("ApplicationDesc").ToString()


			dtRemoveAccounts = DirectCast(ViewState("dtRemoveAccounts"), DataTable)
			dtRemoveAccounts.Rows.Add(AccountReqNum, ApplicationNum, ApplicationDesc)

			gvRemoveAccounts.DataSource = dtRemoveAccounts
			gvRemoveAccounts.DataBind()
			gvRemoveAccounts.SelectedIndex = -1

			Dim dtAccounts As New DataTable
			dtAccounts = DirectCast(ViewState("dtAccounts"), DataTable)
			dtAccounts.Rows(index).Delete()
			dtAccounts.AcceptChanges()


			ViewState("dtAccounts") = dtAccounts

			gvCurrentApps.DataSource = dtAccounts
			gvCurrentApps.DataBind()

			If btnInvalidAccts.Visible = False Then
				btnInvalidAccts.Visible = True
				btnAddDeleteComments.Visible = True
			End If
			'uplAccounts.Update()

			gvCurrentApps.SelectedRowStyle.BackColor = Color.White
			gvCurrentApps.SelectedRowStyle.Font.Bold = False
			gvCurrentApps.SelectedRowStyle.ForeColor = Color.Black

			mainupd.Update()

		End If
	End Sub
	Protected Sub gvRemoveAccounts_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvRemoveAccounts.RowCommand

		If e.CommandName = "Select" Then


			Dim index As Integer = Convert.ToInt32(e.CommandArgument)
			Dim ApplicationNum As String = gvRemoveAccounts.DataKeys(Convert.ToInt32(e.CommandArgument))("ApplicationNum").ToString()
			Dim AccountReqNum As String = gvRemoveAccounts.DataKeys(Convert.ToInt32(e.CommandArgument))("account_request_seq_num").ToString()
			Dim ApplicationDesc As String = gvRemoveAccounts.DataKeys(Convert.ToInt32(e.CommandArgument))("ApplicationDesc").ToString()

			Dim dtAccounts As New DataTable
			dtAccounts = DirectCast(ViewState("dtAccounts"), DataTable)
			dtAccounts.Rows.Add(AccountReqNum, ApplicationNum, ApplicationDesc)

			ViewState("dtAccounts") = dtAccounts

			gvCurrentApps.DataSource = dtAccounts
			gvCurrentApps.DataBind()
			gvCurrentApps.SelectedIndex = -1



			dtRemoveAccounts = DirectCast(ViewState("dtRemoveAccounts"), DataTable)

			If dtRemoveAccounts.Rows.Count > 0 Then

				dtRemoveAccounts.Rows(index).Delete()
				dtRemoveAccounts.AcceptChanges()
			End If

			ViewState("dtRemoveAccounts") = dtRemoveAccounts

			gvRemoveAccounts.DataSource = dtRemoveAccounts
			gvRemoveAccounts.DataBind()
			gvRemoveAccounts.SelectedIndex = -1

			gvRemoveAccounts.SelectedRowStyle.BackColor = Color.White
			gvRemoveAccounts.SelectedRowStyle.Font.Bold = False
			gvRemoveAccounts.SelectedRowStyle.ForeColor = Color.Black


			'uplAccounts.Update()
			mainupd.Update()

		End If
	End Sub
	Protected Sub grAccountsToAdd_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grAccountsToAdd.RowCommand

		If e.CommandName = "Select" Then


			Dim index As Integer = Convert.ToInt32(e.CommandArgument)
			Dim ApplicationNum As String = grAccountsToAdd.DataKeys(Convert.ToInt32(e.CommandArgument))("Applicationnum").ToString()
			Dim ApplicationDesc As String = grAccountsToAdd.DataKeys(Convert.ToInt32(e.CommandArgument))("ApplicationDesc").ToString()

			Dim dtAvailableAccounts As New DataTable
			dtAvailableAccounts = DirectCast(ViewState("dtAvailableAccounts"), DataTable)
			dtAvailableAccounts.Rows.Add(ApplicationNum, ApplicationDesc)

			ViewState("dtAvailableAccounts") = dtAvailableAccounts

			grAvailableAccounts.DataSource = dtAvailableAccounts
			grAvailableAccounts.DataBind()
			grAvailableAccounts.SelectedIndex = -1


			dtAddAccounts = DirectCast(ViewState("dtAddAccounts"), DataTable)
			'dtAddAccounts.Rows(index).Delete()
			'dtAddAccounts.AcceptChanges()

			If dtAddAccounts.Rows.Count > 0 Then
				dtAddAccounts.Rows(index).Delete()
				dtAddAccounts.AcceptChanges()

			End If



			ViewState("dtAddAccounts") = dtAddAccounts

			grAccountsToAdd.DataSource = dtAddAccounts
			grAccountsToAdd.DataBind()
			grAccountsToAdd.SelectedIndex = -1

			grAccountsToAdd.SelectedRowStyle.BackColor = Color.White
			grAccountsToAdd.SelectedRowStyle.Font.Bold = False
			grAccountsToAdd.SelectedRowStyle.ForeColor = Color.Black

			'uplAccounts.Update()
			mainupd.Update()

		End If
	End Sub
	Protected Sub grAvailableAccounts_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grAvailableAccounts.RowCommand

		If e.CommandName = "Select" Then


			Dim index As Integer = Convert.ToInt32(e.CommandArgument)
			Dim ApplicationNum As String = grAvailableAccounts.DataKeys(Convert.ToInt32(e.CommandArgument))("Applicationnum").ToString()
			Dim ApplicationDesc As String = grAvailableAccounts.DataKeys(Convert.ToInt32(e.CommandArgument))("ApplicationDesc").ToString()


			dtAddAccounts = DirectCast(ViewState("dtAddAccounts"), DataTable)
			dtAddAccounts.Rows.Add(ApplicationNum, ApplicationDesc)

			grAccountsToAdd.DataSource = dtAddAccounts
			grAccountsToAdd.DataBind()
			grAccountsToAdd.SelectedIndex = -1

			Dim dtAvailableAccounts As New DataTable
			dtAvailableAccounts = DirectCast(ViewState("dtAvailableAccounts"), DataTable)
			dtAvailableAccounts.Rows(index).Delete()
			dtAvailableAccounts.AcceptChanges()

			grAvailableAccounts.DataSource = dtAvailableAccounts
			grAvailableAccounts.DataBind()
			grAvailableAccounts.SelectedIndex = -1

			ViewState("dtAvailableAccounts") = dtAvailableAccounts
			ViewState("dtAddAccounts") = dtAddAccounts


			grAccountsToAdd.DataSource = dtAddAccounts
			grAccountsToAdd.DataBind()
			grAccountsToAdd.SelectedIndex = -1

			If btnSubmitAddRequest.Visible = False Then
				btnSubmitAddRequest.Visible = True
				btnAddComments.Visible = True
			End If
			'uplAccounts.Update()


			grAvailableAccounts.SelectedRowStyle.BackColor = Color.White
			grAvailableAccounts.SelectedRowStyle.Font.Bold = False
			grAvailableAccounts.SelectedRowStyle.ForeColor = Color.Black

			mainupd.Update()

		End If
	End Sub
	Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
		'If HDemployeetype.Text.ToLower <> "" Then
		'    If HDemployeetype.Text.ToLower <> emp_type_cdrbl.SelectedValue.ToLower Then
		'        If emp_type_cdrbl.SelectedValue = "physician" Or emp_type_cdrbl.SelectedValue = "allied health" Then

		'            lblValidation.Text = "Can Not Change Affiliation type to Physician type of any kind. You must get a PROVIDER Modifier to make this change."
		'            emp_type_cdrbl.SelectedValue = HDemployeetype.Text

		'            Exit Sub
		'        End If
		'    End If


		'End If

		' FormSignOn.AddInsertParm("@submitter_client_num", UserInfo.ClientNum, SqlDbType.NVarChar, 10)
		CheckRequiredFields()

		If bHasError = True Then
			Exit Sub
		End If

		Dim i As Integer
		If npitextbox.Text <> "" Then
			Try
				i = Convert.ToInt32(npitextbox.Text)
			Catch
				npitextbox.Text = ""
				lblValidation.Text = " NPI entry invalid - Numbers Only"
				Exit Sub
			End Try

		End If


		Select Case emp_type_cdrbl.SelectedValue
			Case "782", "1197"

				Titleddl.ForeColor = Color.Black
				Specialtyddl.ForeColor = Color.Black

                'CCMCConsultsrbl.ForeColor = Color.Black
                'DCMHConsultsrbl.ForeColor = Color.Black
                'TaylorConsultsrbl.ForeColor = Color.Black
                'SpringfieldConsultsrbl.ForeColor = Color.Black

                'Writeordersrbl.ForeColor = Color.Black
                'WriteordersDCMHrbl.ForeColor = Color.Black

                CCMCadmitRightsrbl.ForeColor = Color.Black
                'DCMHadmitRightsrbl.ForeColor = Color.Black

                If TitleLabel.Text = "" Then
					lblValidation.Text = "Must supply Title"
					lblValidation.ForeColor = Color.Red
					lblValidation.Focus()

					Titleddl.ForeColor = Color.Red
					Exit Sub
				End If
				'Specialtyddl

				If SpecialtyLabel.Text = "" Then
					lblValidation.Text = "Must supply Specialty"
					lblValidation.ForeColor = Color.Red
					lblValidation.Focus()

					Specialtyddl.ForeColor = Color.Red
					Exit Sub
				End If

                'DEA
                'If DEAIDTextBox.Text = "" Then

                '    lblValidation.Text = "Must supply DEA Info "
                '    lblValidation.ForeColor = Color.Red
                '    lblValidation.Focus()

                '    DEAIDTextBox.BackColor() = Color.Red
                '    Exit Sub

                'Else

                '    DEAIDTextBox.BackColor() = Color.Yellow
                '    DEAIDTextBox.BorderColor() = Color.Black
                '    DEAIDTextBox.BorderStyle() = BorderStyle.Solid
                '    DEAIDTextBox.BorderWidth = Unit.Pixel(2)

                'End If

                If npitextbox.Text = "" Then
                    lblValidation.Text = "Must supply NPI Info "
                    lblValidation.ForeColor = Color.Red
                    lblValidation.Focus()

                    npitextbox.BackColor() = Color.Red
                    Exit Sub

                Else

                    npitextbox.BackColor() = Color.Yellow
                    npitextbox.BorderColor() = Color.Black
                    npitextbox.BorderStyle() = BorderStyle.Solid
                    npitextbox.BorderWidth = Unit.Pixel(2)
                End If

                If LicensesNoTextBox.Text = "" Then

                    lblValidation.Text = "Must supply Licenses Info "
                    lblValidation.ForeColor = Color.Red
                    lblValidation.Focus()

                    LicensesNoTextBox.BackColor() = Color.Red
                    Exit Sub

                Else

                    LicensesNoTextBox.BackColor() = Color.Yellow
                    LicensesNoTextBox.BorderColor() = Color.Black
                    LicensesNoTextBox.BorderStyle() = BorderStyle.Solid
                    LicensesNoTextBox.BorderWidth = Unit.Pixel(2)
                End If



                If CCMCadmitRightsrbl.SelectedIndex = -1 Then

					lblValidation.Text = "Must select Admitting Rights for C.T.S."
					lblValidation.ForeColor = Color.Red
					lblValidation.Focus()
					CCMCadmitRightsrbl.ForeColor = Color.Red

					Return

				End If

                'If DCMHadmitRightsrbl.SelectedIndex = -1 Then

                '	lblValidation.Text = "Must select Admitting Rights for DCMH"

                '	lblValidation.ForeColor = Color.Red
                '	lblValidation.Focus()
                '	DCMHadmitRightsrbl.ForeColor = Color.Red

                '	Return

                'End If



                '            If Writeordersrbl.SelectedIndex = -1 Then

                '	lblValidation.Text = "Must select Write Orders for C.T.S."
                '	lblValidation.ForeColor = Color.Red
                '	lblValidation.Focus()
                '	Writeordersrbl.ForeColor = Color.Red

                '	Return

                'End If


                'If WriteordersDCMHrbl.SelectedIndex = -1 Then

                '	lblValidation.Text = "Must select Write Orders for DCMH"
                '	lblValidation.ForeColor = Color.Red
                '	lblValidation.Focus()
                '	WriteordersDCMHrbl.ForeColor = Color.Red

                '	Return

                'End If

                If hanrbl.SelectedValue.ToLower = "yes" Then
					If LocationOfCareIDLabel.Text = "" Then
                        lblValidation.Text = "Must select CHMG Group"
                        lblValidation.ForeColor = Color.Red
						lblValidation.Focus()
						LocationOfCareIDddl.ForeColor = Color.Red

						Return

					End If
				End If

				If hanrbl.SelectedIndex = -1 Then

                    lblValidation.Text = "Must select CHMG Group"

                    lblValidation.ForeColor = Color.Red
					lblValidation.Focus()
					hanrbl.BackColor() = Color.Red

					Return
				Else

					hanrbl.BackColor() = Color.Yellow
					hanrbl.BorderColor() = Color.Black
					hanrbl.BorderStyle() = BorderStyle.Solid
					hanrbl.BorderWidth = Unit.Pixel(2)

				End If

				If hanrbl.SelectedValue.ToLower = "yes" Then

					If LocationOfCareIDLabel.Text = "" Then
                        lblValidation.Text = "Must select CHMG Group"
                        lblValidation.ForeColor = Color.Red
						lblValidation.Focus()
						LocationOfCareIDddl.BackColor() = Color.Red

						Return
					End If

					'If npitextbox.Text = "" Then

					'    lblValidation.Text = "Must Enter NPI"

					'    lblValidation.ForeColor = Color.Red
					'    lblValidation.Focus()
					'    npitextbox.BackColor() = Color.Red

					'    Return

					'End If

					'If LicensesNoTextBox.Text = "" Then
					'    lblValidation.Text = "Must Enter Licenses No"

					'    lblValidation.ForeColor = Color.Red
					'    lblValidation.Focus()
					'    LicensesNoTextBox.BackColor() = Color.Red

					'    Return


					'End If

					'If taxonomytextbox.Text = "" Then
					'    lblValidation.Text = "Must Enter Taxonomy No"

					'    lblValidation.ForeColor = Color.Red
					'    lblValidation.Focus()
					'    taxonomytextbox.BackColor() = Color.Red

					'    Return

					'End If


				End If


				'Case "physician", "non staff clinician", "resident", "allied health"
				'    If TitleLabel.Text = "" Then
				'        lblValidation.Text = "Must supply Title"
				'        Titleddl.Focus()
				'        Exit Sub
				'    End If
				'    'Specialtyddl
				'    If SpecialtLabel.Text = "" Then
				'        lblValidation.Text = "Must supply Specialty"
				'        Specialtyddl.Focus()
				'        Exit Sub
				'    End If
			Case "1194"

				Titleddl.ForeColor = Color.Black
				Specialtyddl.ForeColor = Color.Black

                'CCMCConsultsrbl.ForeColor = Color.Black
                'DCMHConsultsrbl.ForeColor = Color.Black
                'TaylorConsultsrbl.ForeColor = Color.Black
                'SpringfieldConsultsrbl.ForeColor = Color.Black

                'Writeordersrbl.ForeColor = Color.Black
                'WriteordersDCMHrbl.ForeColor = Color.Black

                CCMCadmitRightsrbl.ForeColor = Color.Black
                'DCMHadmitRightsrbl.ForeColor = Color.Black

                If TitleLabel.Text = "" Then
					lblValidation.Text = "Must supply Title"
					lblValidation.ForeColor = Color.Red
					lblValidation.Focus()

					Titleddl.ForeColor = Color.Red
					Exit Sub
				End If
				'Specialtyddl
				If SpecialtLabel.Text = "" Then
					lblValidation.Text = "Must supply Specialty"
					lblValidation.ForeColor = Color.Red
					lblValidation.Focus()

					Specialtyddl.ForeColor = Color.Red
					Exit Sub
				End If


				If CCMCadmitRightsrbl.SelectedIndex = -1 Then

					lblValidation.Text = "Must select Admitting Rights for C.T.S."
					lblValidation.ForeColor = Color.Red
					lblValidation.Focus()
					CCMCadmitRightsrbl.ForeColor = Color.Red

					Return

				End If

                'If DCMHadmitRightsrbl.SelectedIndex = -1 Then

                '	lblValidation.Text = "Must select Admitting Rights for DCMH"

                '	lblValidation.ForeColor = Color.Red
                '	lblValidation.Focus()
                '	DCMHadmitRightsrbl.ForeColor = Color.Red

                '	Return

                'End If

                If hanrbl.SelectedValue.ToLower = "yes" Then
					If LocationOfCareIDLabel.Text = "" Then
                        lblValidation.Text = "Must select CHMG Group"
                        lblValidation.ForeColor = Color.Red
						lblValidation.Focus()
						LocationOfCareIDddl.BackColor() = Color.Red
					Else

						LocationOfCareIDddl.BackColor() = Color.Yellow
						LocationOfCareIDddl.BorderColor() = Color.Black
						LocationOfCareIDddl.BorderStyle() = BorderStyle.Solid
						LocationOfCareIDddl.BorderWidth = Unit.Pixel(2)

					End If
				End If

                'If CCMCConsultsrbl.SelectedIndex = -1 Then

                '    lblValidation.Text = "Must select CCMC Consults"
                '    lblValidation.ForeColor = Color.Red
                '    lblValidation.Focus()
                '    CCMCConsultsrbl.ForeColor = Color.Red

                '    Return

                'End If


                'If DCMHConsultsrbl.SelectedIndex = -1 Then

                '    lblValidation.Text = "Must select DCMH Consults"
                '    lblValidation.ForeColor = Color.Red
                '    lblValidation.Focus()
                '    DCMHConsultsrbl.ForeColor = Color.Red

                '    Return

                'End If

                'If TaylorConsultsrbl.SelectedIndex = -1 Then

                '    lblValidation.Text = "Must select Taylor Consults"
                '    lblValidation.ForeColor = Color.Red
                '    lblValidation.Focus()
                '    TaylorConsultsrbl.ForeColor = Color.Red

                '    Return

                'End If

                'If SpringfieldConsultsrbl.SelectedIndex = -1 Then

                '    lblValidation.Text = "Must select Springfield Consults"
                '    lblValidation.ForeColor = Color.Red
                '    lblValidation.Focus()
                '    SpringfieldConsultsrbl.ForeColor = Color.Red

                '    Return

                'End If


                'If Writeordersrbl.SelectedIndex = -1 Then

                '	lblValidation.Text = "Must select Write Orders for C.T.S."
                '	lblValidation.ForeColor = Color.Red
                '	lblValidation.Focus()
                '	Writeordersrbl.ForeColor = Color.Red

                '	Return

                'End If


                'If WriteordersDCMHrbl.SelectedIndex = -1 Then

                '	lblValidation.Text = "Must select Write Orders for DCMH"
                '	lblValidation.ForeColor = Color.Red
                '	lblValidation.Focus()
                '	WriteordersDCMHrbl.ForeColor = Color.Red

                '	Return

                'End If

                If Rolesddl.SelectedIndex = 0 Then

					lblValidation.Text = "Must select Resident Position"
					lblValidation.ForeColor = Color.Red
					lblValidation.Focus()
					Rolesddl.ForeColor = Color.Red

					Return

				End If

				If hanrbl.SelectedValue.ToLower = "yes" Then
					If LocationOfCareIDLabel.Text = "" Then
                        lblValidation.Text = "Must select CHMG Group"
                        lblValidation.ForeColor = Color.Red
						lblValidation.Focus()
						LocationOfCareIDddl.ForeColor = Color.Red
						Return
					End If
				End If

			Case "1193"
				Select Case PositionRoleNumLabel.Text
					Case "1567"
						If PhysOfficeddl.SelectedIndex < 1 Then
							lblValidation.Text = "Must select Phys Office location"
							lblValidation.ForeColor = Color.Red
							lblValidation.Focus()
							PhysOfficeddl.ForeColor = Color.Red
							Return

						End If

						' More fields tests go here
					Case Else

				End Select

		End Select


		FormSignOn.AddInsertParm("@account_request_num", iRequestNum, SqlDbType.Int, 9)
		'FormSignOn.AddInsertParm("@submitter_client_num", iUserNum, SqlDbType.Int, 9)

		FormSignOn.UpdateForm()



		If ValidationStatus.ToLower = "needscred" Then
            If credentialedDateTextBox.Text <> "" Then
                sendAncillaryEmail()
            End If

        End If

		Dim sDate As DateTime = Date.Now()

		lblValidation.Text = "Information Updated at -> " & sDate

		If account_request_typeLabel.Text = "credent" Or ValidationStatus.ToLower = "needscred" Then
			Response.Redirect("~/" & "CredentialQueue.aspx")

		End If
	End Sub

	Protected Function fillActionItemsTable(ByRef Actionlist As IList(Of Actions)) As Table

		Dim iRowCount = 1
		Dim dt As New Table
		Dim rwHeader As New TableRow

		Dim clActiondateHeader As New TableCell
		Dim clActionCodeDescheader As New TableCell
		Dim clTechHeader As New TableCell
		Dim clDescHeader As New TableCell


		clActionCodeDescheader.Text = "Action Type"
		clActiondateHeader.Text = "Action Date"
		clTechHeader.Text = "Name"
		clDescHeader.Text = "Description"


		rwHeader.Cells.Add(clActionCodeDescheader)
		rwHeader.Cells.Add(clActiondateHeader)
		rwHeader.Cells.Add(clTechHeader)
		rwHeader.Cells.Add(clDescHeader)



		rwHeader.Font.Bold = True
		rwHeader.BackColor = Color.FromName("#006666")
		rwHeader.ForeColor = Color.FromName("White")
		rwHeader.Height = Unit.Pixel(36)

		dt.Rows.Add(rwHeader)

		For Each AList In Actionlist


			Dim rwData As New TableRow

			Dim clActionDate As New TableCell
			Dim clActionCodeDesc As New TableCell
			Dim clTechName As New TableCell
			Dim clDescription As New TableCell

			clActionCodeDesc.Text = AList.ActionCodeDesc
			clActionDate.Text = AList.ActionDate
			clTechName.Text = AList.TechName
			clDescription.Text = AList.ActionDesc


			rwData.Cells.Add(clActionCodeDesc)
			rwData.Cells.Add(clActionDate)
			rwData.Cells.Add(clTechName)
			rwData.Cells.Add(clDescription)


			If iRowCount > 0 Then

				rwData.BackColor = Color.Bisque

			Else

				rwData.BackColor = Color.LightGray

			End If

			dt.Rows.Add(rwData)

			iRowCount = iRowCount * -1

		Next
		dt.Font.Name = "Arial"
		dt.Font.Size = 8

		dt.CellPadding = 10
		dt.Width = New Unit("100%")


		Return dt

	End Function
	Protected Sub entity_cdDDL_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles entity_cdddl.SelectedIndexChanged
		entity_cdLabel.Text = entity_cdddl.SelectedValue

		Dim ddbDepartmentBinder As New DropDownListBinder(department_cdddl, "selDepartmentCodesV4", Session("EmployeeConn"))
		ddbDepartmentBinder.AddDDLCriteria("@entity_cd", entity_cdddl.SelectedValue, SqlDbType.NVarChar)
		ddbDepartmentBinder.BindData("department_cd", "department_name")
		department_cdLabel.Text = ""

	End Sub
	Protected Sub group_nameddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles group_nameddl.SelectedIndexChanged
		group_numTextBox.Text = group_nameddl.SelectedValue
		group_nameTextBox.Text = group_nameddl.SelectedItem.ToString
		InvisionGroupNumTextBox.Text = group_nameddl.SelectedValue

	End Sub
	Protected Sub Specialtyddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles Specialtyddl.SelectedIndexChanged
		SpecialtyLabel.Text = Specialtyddl.SelectedValue
		mainupd.Update()

	End Sub
	Protected Sub PhysOfficeddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles PhysOfficeddl.SelectedIndexChanged
		group_numTextBox.Text = PhysOfficeddl.SelectedValue
		group_nameTextBox.Text = PhysOfficeddl.SelectedItem.ToString
	End Sub

    Protected Sub department_cdddl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles department_cdddl.SelectedIndexChanged
        department_cdLabel.Text = department_cdddl.SelectedValue

    End Sub
    Private Sub CernerPosddl_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CernerPosddl.SelectedIndexChanged

        If CernerPosddl.SelectedValue.ToString.ToLower <> "select" Then
            CernerPositionDescTextBox.Text = CernerPosddl.SelectedValue.ToString
        End If

    End Sub

    Protected Sub SuppSupportrbl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles SuppSupportrbl.SelectedIndexChanged
		If SuppSupportrbl.SelectedValue.ToLower = "yes" Then
			entity_cdDDL.SelectedValue = "150"

			Dim ddbDepartmentBinder As New DropDownListBinder(department_cdddl, "selDepartmentCodesV4", Session("EmployeeConn"))
			ddbDepartmentBinder.AddDDLCriteria("@entity_cd", "150", SqlDbType.NVarChar)
			ddbDepartmentBinder.BindData("department_cd", "department_name")

			department_cdddl.SelectedValue = "601000"
		Else
			entity_cdDDL.SelectedIndex = -1
			department_cdddl.SelectedIndex = -1

		End If

	End Sub


	Protected Sub facility_cdddl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles facility_cdddl.SelectedIndexChanged
		Dim ddlBinder As DropDownListBinder

		facility_cdLabel.Text = facility_cdddl.SelectedValue
		ddlBinder = New DropDownListBinder(building_cdddl, "sel_building_by_facility", Session("EmployeeConn"))
		ddlBinder.AddDDLCriteria("@facility_cd", facility_cdddl.SelectedValue, SqlDbType.NVarChar)
		ddlBinder.BindData("building_cd", "building_name")

		building_cdLabel.Text = ""

		Dim ddlFloorBinder As DropDownListBinder

		ddlFloorBinder = New DropDownListBinder(Floorddl, "SelFloorsByBuilding", Session("EmployeeConn"))
		ddlFloorBinder.AddDDLCriteria("@facility_cd", facility_cdddl.SelectedValue, SqlDbType.NVarChar)
		ddlFloorBinder.AddDDLCriteria("@building_cd", building_cdddl.SelectedValue, SqlDbType.NVarChar)

		ddlFloorBinder.BindData("floor_no", "floor_no")


	End Sub
	Protected Sub building_cdddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles building_cdddl.SelectedIndexChanged
		building_cdLabel.Text = building_cdddl.SelectedValue
		building_nameLabel.Text = building_cdddl.SelectedItem.Text

		Dim ddlFloorBinder As DropDownListBinder

		ddlFloorBinder = New DropDownListBinder(Floorddl, "SelFloorsByBuilding", Session("EmployeeConn"))
		ddlFloorBinder.AddDDLCriteria("@facility_cd", facility_cdddl.SelectedValue, SqlDbType.NVarChar)
		ddlFloorBinder.AddDDLCriteria("@building_cd", building_cdddl.SelectedValue, SqlDbType.NVarChar)

		ddlFloorBinder.BindData("floor_no", "floor_no")

	End Sub
	Protected Sub Floorddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles Floorddl.SelectedIndexChanged
		FloorTextBox.Text = Floorddl.SelectedValue

	End Sub
    Protected Sub fillEditItemstable()

        Dim DTItems As DataTable

        Dim thisData As New CommandsSqlAndOleDb("SelRequestItemsShortV20", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@account_request_num", iRequestNum, SqlDbType.NVarChar)


        DTItems = thisData.GetSqlDataTable


        EditGrid.DataSource = DTItems
        EditGrid.DataBind()



    End Sub

    Protected Sub fillRequestItemsTable()
		Dim thisData As New CommandsSqlAndOleDb("SelRequestItems", Session("EmployeeConn"))
		thisData.AddSqlProcParameter("@account_request_num", iRequestNum, SqlDbType.NVarChar)

		Dim iRowCount = 1

		Dim dt As DataTable
		dt = thisData.GetSqlDataTable

		Dim rwHeader As New TableRow

		Dim clAccountHeader As New TableCell
		Dim clRequestTypeHeader As New TableCell
		Dim clStatusHeader As New TableCell



		clAccountHeader.Text = "Account"
		clRequestTypeHeader.Text = "Request Type"
		clStatusHeader.Text = "Status"



		rwHeader.Cells.Add(clAccountHeader)
		rwHeader.Cells.Add(clRequestTypeHeader)
		rwHeader.Cells.Add(clStatusHeader)


		rwHeader.Font.Bold = True
		rwHeader.BackColor = Color.FromName("#006666")
		rwHeader.ForeColor = Color.FromName("White")
		rwHeader.Height = Unit.Pixel(36)

		tblRequestItems.Rows.Add(rwHeader)

		For Each drRow As DataRow In dt.Rows


			Dim rwData As New TableRow

			Dim clAccountData As New TableCell
			Dim clRequestTypeData As New TableCell
			Dim clStatusData As New TableCell
			Dim clRequestorData As New TableCell
			Dim clReqPhoneData As New TableCell




			clAccountData.Text = IIf(IsDBNull(drRow("ApplicationDesc")), "", drRow("ApplicationDesc"))
			clRequestTypeData.Text = IIf(IsDBNull(drRow("account_request_type")), "", drRow("account_request_type"))
			clStatusData.Text = IIf(IsDBNull(drRow("valid_cd_desc")), "", drRow("valid_cd_desc"))

			rwData.Cells.Add(clAccountData)
			rwData.Cells.Add(clRequestTypeData)
			rwData.Cells.Add(clStatusData)


			If iRowCount > 0 Then

				rwData.BackColor = Color.Bisque

			Else

				rwData.BackColor = Color.LightGray

			End If

			tblRequestItems.Rows.Add(rwData)

			iRowCount = iRowCount * -1

		Next
		tblRequestItems.CellPadding = 10
		tblRequestItems.Width = New Unit("100%")


	End Sub
	Private Sub FillAffiliations()
		'SelInvalidCodes
		Dim dt As DataTable
		Dim thisData As New CommandsSqlAndOleDb
		thisData = New CommandsSqlAndOleDb("SelAffiliationsV5", Session("EmployeeConn"))
		thisData.AddSqlProcParameter("@Type", "phys", SqlDbType.NVarChar, 8)


		dt = thisData.GetSqlDataTable()
		emp_type_cdrbl.DataSource = dt
		emp_type_cdrbl.DataValueField = "RoleNum"
		emp_type_cdrbl.DataTextField = "AffiliationDisplay"

		emp_type_cdrbl.DataBind()
		thisData = Nothing

	End Sub
	Private Sub CheckAffiliation()



        ' HDemployeetype.Text = emp_type_cdrbl.SelectedValue


        Select Case emp_type_cdrbl.SelectedValue

			Case "1193", "1200"
				'Case "employee"


				Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
				Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")




                pnlProvider.Visible = False
				'providerrbl.SelectedIndex = -1



			Case "1189"
				'Case "student"

				Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
				Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")
				Dim titleReq As New RequiredField(Titleddl, "Title")
				Dim specReq As New RequiredField(Specialtyddl, "Specialty")
				Dim add1Req As New RequiredField(address_1textbox, "Address 1")
				Dim cityReq As New RequiredField(citytextbox, "City")
				Dim stateReq As New RequiredField(statetextbox, "State")
				Dim zipReq As New RequiredField(ziptextbox, "ZIP")
				Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
				Dim faxqReq As New RequiredField(faxtextbox, "Fax")



                pnlProvider.Visible = False
				' providerrbl.SelectedIndex = -1

			Case "782"
				'Case "physician"

				Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
				Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")
				Dim titleReq As New RequiredField(Titleddl, "Title")
				Dim specReq As New RequiredField(Specialtyddl, "Specialty")
				Dim add1Req As New RequiredField(address_1textbox, "Address 1")
				Dim cityReq As New RequiredField(citytextbox, "City")
				Dim stateReq As New RequiredField(statetextbox, "State")
				Dim zipReq As New RequiredField(ziptextbox, "ZIP")
				Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim faxqReq As New RequiredField(faxtextbox, "Fax")

                'Dim DEAReq As New RequiredField(DEAIDTextBox, "DEA")

                Dim NPIReq As New RequiredField(npitextbox, "NPI")
                Dim LicensesReq As New RequiredField(LicensesNoTextBox, "Licenses")



                pnlProvider.Visible = True
				'providerrbl.SelectedValue = "Yes"

			Case "1197"

				'Case "non staff clinician"

				Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
				Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")
				Dim titleReq As New RequiredField(Titleddl, "Title")
				Dim specReq As New RequiredField(Specialtyddl, "Specialty")
				Dim add1Req As New RequiredField(address_1textbox, "Address 1")
				Dim cityReq As New RequiredField(citytextbox, "City")
				Dim stateReq As New RequiredField(statetextbox, "State")
				Dim zipReq As New RequiredField(ziptextbox, "ZIP")
				Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
				Dim faxqReq As New RequiredField(faxtextbox, "Fax")


                Dim DEAReq As New RequiredField(DEAIDTextBox, "DEA")
                Dim NPIReq As New RequiredField(npitextbox, "NPI")
                Dim LicensesReq As New RequiredField(LicensesNoTextBox, "Licenses")


                'providerrbl.SelectedIndex = -1
                pnlProvider.Visible = True

			Case "1194"
				'Case "resident"

				Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
				Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")
				Dim titleReq As New RequiredField(Titleddl, "Title")
				Dim specReq As New RequiredField(Specialtyddl, "Specialty")
				Dim add1Req As New RequiredField(address_1textbox, "Address 1")
				Dim cityReq As New RequiredField(citytextbox, "City")
				Dim stateReq As New RequiredField(statetextbox, "State")
				Dim zipReq As New RequiredField(ziptextbox, "ZIP")
				Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
				Dim faxqReq As New RequiredField(faxtextbox, "Fax")



                'providerrbl.SelectedValue = "Yes"
                pnlProvider.Visible = True


			Case "1198"
				' Case "vendor"

				Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
				Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")
				Dim add1Req As New RequiredField(address_1textbox, "Address 1")
				Dim cityReq As New RequiredField(citytextbox, "City")
				Dim stateReq As New RequiredField(statetextbox, "State")
				Dim zipReq As New RequiredField(ziptextbox, "ZIP")
				Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
				Dim vendorRequired As New RequiredField(VendorNameTextBox, "Vendor")



                pnlProvider.Visible = False
				'providerrbl.SelectedIndex = -1


			Case "1195"
				'Case "contract"

				Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
				Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")

				Dim add1Req As New RequiredField(address_1textbox, "Address 1")
				Dim cityReq As New RequiredField(citytextbox, "City")
				Dim stateReq As New RequiredField(statetextbox, "State")
				Dim zipReq As New RequiredField(ziptextbox, "ZIP")
				Dim phoneReq As New RequiredField(phoneTextBox, "Phone")



                pnlProvider.Visible = False
				'providerrbl.SelectedIndex = -1

				'Case "1200"
				'    'Case "allied health"

				'    Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
				'    Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")
				'    Dim titleReq As New RequiredField(Titleddl, "Title")
				'    Dim specReq As New RequiredField(Specialtyddl, "Specialty")
				'    Dim add1Req As New RequiredField(address_1textbox, "Address 1")
				'    Dim cityReq As New RequiredField(citytextbox, "City")
				'    Dim stateReq As New RequiredField(statetextbox, "State")
				'    Dim zipReq As New RequiredField(ziptextbox, "ZIP")
				'    Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
				'    Dim faxqReq As New RequiredField(faxtextbox, "Fax")
				'    'Dim AffddlReq As New RequiredField(OtherAffDescddl, "Allied Dg.")

				'    Dim otherAff As New RequiredField(OtherAffDescTextBox, "Other Affiliation Description")

				'    providerrbl.SelectedIndex = 0
				'    pnlProvider.Visible = True
				'    providerrbl.SelectedValue = "Yes"

				'    pnlOtherAffilliation.Visible = False




			Case "1196"
				'Case "other"

				Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
				Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")

				Dim add1Req As New RequiredField(address_1textbox, "Address 1")
				Dim cityReq As New RequiredField(citytextbox, "City")
				Dim stateReq As New RequiredField(statetextbox, "State")
				Dim zipReq As New RequiredField(ziptextbox, "ZIP")
				Dim phoneReq As New RequiredField(phoneTextBox, "Phone")



                pnlProvider.Visible = False
				'providerrbl.SelectedIndex = -1



			Case Else


				Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
				Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")



                pnlProvider.Visible = False
				' providerrbl.SelectedIndex = -1

				Select Case PositionRoleNumLabel.Text
					Case "1567"

						PhysOfficeddl.BackColor() = Color.Yellow
						PhysOfficeddl.BorderColor() = Color.Black
						PhysOfficeddl.BorderStyle() = BorderStyle.Solid
						PhysOfficeddl.BorderWidth = Unit.Pixel(2)
					Case Else

				End Select


		End Select


	End Sub
	' New check required fields may 2015
	Public Function CheckRequiredFields() As Boolean
		Dim removeCss As New PageControls(Page)
		removeCss.RemoveCssClass(Page, "textInputRequired")

		emp_type_cdLabel.Text = emp_type_cdrbl.SelectedValue

		Select Case emp_type_cdrbl.SelectedValue
			Case "1193", "1200"
				'Case "employee"

				Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
				Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")
				Dim phoneReq As New RequiredField(phoneTextBox, "Phone")

				Dim Location As New RequiredField(facility_cdddl, "Location")
				Dim building As New RequiredField(building_cdddl, "building")

				facility_cdddl.BackColor() = Color.Yellow
				facility_cdddl.BorderColor() = Color.Black
				facility_cdddl.BorderStyle() = BorderStyle.Solid
				facility_cdddl.BorderWidth = Unit.Pixel(2)

				building_cdddl.BackColor() = Color.Yellow
				building_cdddl.BorderColor() = Color.Black
				building_cdddl.BorderStyle() = BorderStyle.Solid
				building_cdddl.BorderWidth = Unit.Pixel(2)

                'Dim Aff As New RequiredField(emp_type_cdrbl, "Affiliation")

                pnlProvider.Visible = False
				'providerrbl.SelectedIndex = -1

				'lblVendor.Text = "Vendor Name:"
				Vendorddl.Visible = False
				VendorNameTextBox.Visible = True
				VendorNameTextBox.Enabled = False




			Case "1189"
				'Case "student"

				Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
				Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")

				Dim titleReq As New RequiredField(Titleddl, "Title")
				Titleddl.BackColor() = Color.Yellow
				Titleddl.BorderColor() = Color.Black
				Titleddl.BorderStyle() = BorderStyle.Solid
				Titleddl.BorderWidth = Unit.Pixel(2)

				Dim specReq As New RequiredField(Specialtyddl, "Specialty")
				Specialtyddl.BackColor() = Color.Yellow
				Specialtyddl.BorderColor() = Color.Black
				Specialtyddl.BorderStyle() = BorderStyle.Solid
				Specialtyddl.BorderWidth = Unit.Pixel(2)

				Dim add1Req As New RequiredField(address_1textbox, "Address 1")
				Dim cityReq As New RequiredField(citytextbox, "City")
				Dim stateReq As New RequiredField(statetextbox, "State")
				Dim zipReq As New RequiredField(ziptextbox, "ZIP")
				Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
				Dim faxqReq As New RequiredField(faxtextbox, "Fax")


                pnlProvider.Visible = False
				'providerrbl.SelectedIndex = -1
				Titleddl.Enabled = True

				'lblVendor.Text = "Vendor Name:"
				Vendorddl.Visible = False
				VendorNameTextBox.Visible = True
				VendorNameTextBox.Enabled = False


			Case "782"
				' Case "physician"
				Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
				Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")

				Dim titleReq As New RequiredField(Titleddl, "Title")
				Titleddl.BackColor() = Color.Yellow
				Titleddl.BorderColor() = Color.Black
				Titleddl.BorderStyle() = BorderStyle.Solid
				Titleddl.BorderWidth = Unit.Pixel(2)

				Dim specReq As New RequiredField(Specialtyddl, "Specialty")
				Specialtyddl.BackColor() = Color.Yellow
				Specialtyddl.BorderColor() = Color.Black
				Specialtyddl.BorderStyle() = BorderStyle.Solid
				Specialtyddl.BorderWidth = Unit.Pixel(2)


				Dim add1Req As New RequiredField(address_1textbox, "Address 1")
				Dim cityReq As New RequiredField(citytextbox, "City")
				Dim stateReq As New RequiredField(statetextbox, "State")
				Dim zipReq As New RequiredField(ziptextbox, "ZIP")
				Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim faxqReq As New RequiredField(faxtextbox, "Fax")


                Dim DEAReq As New RequiredField(DEAIDTextBox, "DEA")
                Dim NPIReq As New RequiredField(npitextbox, "NPI")
                Dim LicensesReq As New RequiredField(LicensesNoTextBox, "Licenses")

                'Dim CCMCConsults As New RequiredField(CCMCConsultsrbl, "CCMC Consults")
                'Dim DCMHConsults As New RequiredField(DCMHConsultsrbl, "DCMH Consults")
                'Dim TaylorConsults As New RequiredField(TaylorConsultsrbl, "Taylor Consults")
                'Dim SpringfieldConsults As New RequiredField(SpringfieldConsultsrbl, "Springfield Consults")

                '            Dim Writeorders As New RequiredField(Writeordersrbl, "Write Orders for C.T.S.")
                'Dim WriteordersDCMH As New RequiredField(WriteordersDCMHrbl, "Write Orders for DCMH")

                Dim CCMCadmitRights As New RequiredField(CCMCadmitRightsrbl, "Admitting Rights for C.T.S.")
                'Dim DCMHadmitRights As New RequiredField(DCMHadmitRightsrbl, "Admitting Rights for DCMH")

                Dim HanReq As New RequiredField(hanrbl, "CHMG Physician")

                If hanrbl.SelectedValue.ToLower = "yes" Then
                    Dim loccareReq As New RequiredField(LocationOfCareIDddl, "CHMG Group")

                    LocationOfCareIDddl.BackColor() = Color.Yellow
					LocationOfCareIDddl.BorderColor() = Color.Black
					LocationOfCareIDddl.BorderStyle() = BorderStyle.Solid
					LocationOfCareIDddl.BorderWidth = Unit.Pixel(2)

					'npitextbox.BackColor() = Color.Yellow
					'npitextbox.BorderColor() = Color.Black
					'npitextbox.BorderStyle() = BorderStyle.Solid
					'npitextbox.BorderWidth = Unit.Pixel(2)

					'LicensesNoTextBox.BackColor() = Color.Yellow
					'LicensesNoTextBox.BorderColor() = Color.Black
					'LicensesNoTextBox.BorderStyle() = BorderStyle.Solid
					'LicensesNoTextBox.BorderWidth = Unit.Pixel(2)

					'taxonomytextbox.BackColor() = Color.Yellow
					'taxonomytextbox.BorderColor() = Color.Black
					'taxonomytextbox.BorderStyle() = BorderStyle.Solid
					'taxonomytextbox.BorderWidth = Unit.Pixel(2)

					'start_dateTextBox.BackColor() = Color.Yellow
					'start_dateTextBox.BorderColor() = Color.Black
					'start_dateTextBox.BorderStyle() = BorderStyle.Solid
					'start_dateTextBox.BorderWidth = Unit.Pixel(3)

					'CPMEmployeesddl
					'CPMEmployeesddl.BackColor() = Color.Yellow
					'CPMEmployeesddl.BorderColor() = Color.Black
					'CPMEmployeesddl.BorderStyle() = BorderStyle.Solid
					'CPMEmployeesddl.BorderWidth = Unit.Pixel(2)


					''Schedulablerbl
					'Schedulablerbl.BackColor() = Color.Yellow
					'Schedulablerbl.BorderColor() = Color.Black
					'Schedulablerbl.BorderStyle() = BorderStyle.Solid
					'Schedulablerbl.BorderWidth = Unit.Pixel(2)

					''Televoxrbl
					'Televoxrbl.BackColor() = Color.Yellow
					'Televoxrbl.BorderColor() = Color.Black
					'Televoxrbl.BorderStyle() = BorderStyle.Solid
					'Televoxrbl.BorderWidth = Unit.Pixel(2)



				End If

				pnlProvider.Visible = True


                'providerrbl.SelectedValue = "Yes"
                'hanrbl.SelectedValue = "Yes"

                'lblVendor.Text = "Vendor Name:"
                Vendorddl.Visible = False
				VendorNameTextBox.Visible = True
				VendorNameTextBox.Enabled = False


				Titleddl.Enabled = True
			Case "1197"
				'Case "non staff clinician"
				Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
				Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")

				Dim titleReq As New RequiredField(Titleddl, "Title")
				Titleddl.BackColor() = Color.Yellow
				Titleddl.BorderColor() = Color.Black
				Titleddl.BorderStyle() = BorderStyle.Solid
				Titleddl.BorderWidth = Unit.Pixel(2)

				Dim specReq As New RequiredField(Specialtyddl, "Specialty")
				Specialtyddl.BackColor() = Color.Yellow
				Specialtyddl.BorderColor() = Color.Black
				Specialtyddl.BorderStyle() = BorderStyle.Solid
				Specialtyddl.BorderWidth = Unit.Pixel(2)


				Dim add1Req As New RequiredField(address_1textbox, "Address 1")
				Dim cityReq As New RequiredField(citytextbox, "City")
				Dim stateReq As New RequiredField(statetextbox, "State")
				Dim zipReq As New RequiredField(ziptextbox, "ZIP")
				Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                'Dim vendorRequired As New RequiredField(VendorNameTextBox, "Vendor")
                Dim faxqReq As New RequiredField(faxtextbox, "Fax")


                Dim DEAReq As New RequiredField(DEAIDTextBox, "DEA")
                Dim NPIReq As New RequiredField(npitextbox, "NPI")
                Dim LicensesReq As New RequiredField(LicensesNoTextBox, "Licenses")

                pnlProvider.Visible = True

                'providerrbl.SelectedIndex = -1
                Titleddl.Enabled = True

				VendorNameTextBox.Visible = False
				Vendorddl.Visible = True
				'lblVendor.Text = "Contractor Name:"


			Case "1194"
				'Case "resident"
				Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
				Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")
				Dim titleReq As New RequiredField(Titleddl, "Title")

				Titleddl.BackColor() = Color.Yellow
				Titleddl.BorderColor() = Color.Black
				Titleddl.BorderStyle() = BorderStyle.Solid
				Titleddl.BorderWidth = Unit.Pixel(2)

				Dim specReq As New RequiredField(Specialtyddl, "Specialty")
				Specialtyddl.BackColor() = Color.Yellow
				Specialtyddl.BorderColor() = Color.Black
				Specialtyddl.BorderStyle() = BorderStyle.Solid
				Specialtyddl.BorderWidth = Unit.Pixel(2)


				Dim add1Req As New RequiredField(address_1textbox, "Address 1")
				Dim cityReq As New RequiredField(citytextbox, "City")
				Dim stateReq As New RequiredField(statetextbox, "State")
				Dim zipReq As New RequiredField(ziptextbox, "ZIP")
				Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                'Dim faxqReq As New RequiredField(faxtextbox, "Fax")

                'Dim CCMCConsults As New RequiredField(CCMCConsultsrbl, "CCMC Consults")
                'Dim DCMHConsults As New RequiredField(DCMHConsultsrbl, "DCMH Consults")
                'Dim TaylorConsults As New RequiredField(TaylorConsultsrbl, "Taylor Consults")
                'Dim SpringfieldConsults As New RequiredField(SpringfieldConsultsrbl, "Springfield Consults")

                'Dim Writeorders As New RequiredField(Writeordersrbl, "Write Orders for C.T.S.")
                'Dim WriteordersDCMH As New RequiredField(WriteordersDCMHrbl, "Write Orders for DCMH")

                pnlProvider.Visible = True

                'providerrbl.SelectedValue = "Yes"
                Titleddl.Enabled = True

				'lblVendor.Text = "Vendor Name:"
				Vendorddl.Visible = False
				VendorNameTextBox.Visible = True
				VendorNameTextBox.Enabled = False



			Case "1198"
				'Case "vendor" 

				Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
				Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")

				Dim add1Req As New RequiredField(address_1textbox, "Address 1")
				Dim cityReq As New RequiredField(citytextbox, "City")
				Dim stateReq As New RequiredField(statetextbox, "State")
				Dim zipReq As New RequiredField(ziptextbox, "ZIP")
				Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
				Dim vendorRequired As New RequiredField(VendorNameTextBox, "Vendor")
				Dim startReq As New RequiredField(start_dateTextBox, "Start Date")

				Dim VendorReq As New RequiredField(Vendorddl, "Vendor")
				Vendorddl.BackColor() = Color.Yellow
				Vendorddl.BorderColor() = Color.Black
				Vendorddl.BorderStyle() = BorderStyle.Solid
				Vendorddl.BorderWidth = Unit.Pixel(2)

				VendorNameTextBox.Visible = False
				Vendorddl.Visible = True
                'lblVendor.Text = "Vendor Name:"


                pnlProvider.Visible = False
				'providerrbl.SelectedIndex = -1

			Case "1195"
				'Case "contract"

				Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
				Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")

				Dim add1Req As New RequiredField(address_1textbox, "Address 1")
				Dim cityReq As New RequiredField(citytextbox, "City")
				Dim stateReq As New RequiredField(statetextbox, "State")
				Dim zipReq As New RequiredField(ziptextbox, "ZIP")
				Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
				Dim vendorRequired As New RequiredField(VendorNameTextBox, "Vendor")
				Dim startReq As New RequiredField(start_dateTextBox, "Start Date")

				Dim VendorReq As New RequiredField(Vendorddl, "Vendor")
				Vendorddl.BackColor() = Color.Yellow
				Vendorddl.BorderColor() = Color.Black
				Vendorddl.BorderStyle() = BorderStyle.Solid
				Vendorddl.BorderWidth = Unit.Pixel(2)

				'lblVendor.Text = "Contrator Name:"
				Vendorddl.Visible = True

				VendorNameTextBox.Visible = False

                pnlProvider.Visible = False
				'providerrbl.SelectedIndex = -1

				'Case "1200"
				'    'Case "allied health"

				'    Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
				'    Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")

				'    Dim alliedReq As New RequiredField(Titleddl, "Title")
				'    Titleddl.BackColor() = Color.Yellow
				'    Titleddl.BorderColor() = Color.Black
				'    Titleddl.BorderStyle() = BorderStyle.Solid
				'    Titleddl.BorderWidth = Unit.Pixel(2)

				'    Dim add1Req As New RequiredField(address_1textbox, "Address 1")
				'    Dim cityReq As New RequiredField(citytextbox, "City")
				'    Dim stateReq As New RequiredField(statetextbox, "State")
				'    Dim zipReq As New RequiredField(ziptextbox, "ZIP")
				'    Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
				'    Dim faxqReq As New RequiredField(faxtextbox, "Fax")

				'    Dim CCMCConsults As New RequiredField(CCMCConsultsrbl, "CCMC Consults")
				'    Dim DCMHConsults As New RequiredField(DCMHConsultsrbl, "DCMH Consults")
				'    Dim TaylorConsults As New RequiredField(TaylorConsultsrbl, "Taylor Consults")
				'    Dim SpringfieldConsults As New RequiredField(SpringfieldConsultsrbl, "Springfield Consults")

				'    Dim Writeorders As New RequiredField(Writeordersrbl, "Write Orders for C.T.S.")
				'    Dim WriteordersDCMH As New RequiredField(WriteordersDCMHrbl, "Write Orders for DCMH")

				'    Dim CCMCadmitRights As New RequiredField(CCMCadmitRightsrbl, "Admitting Rights for C.T.S.")
				'    Dim DCMHadmitRights As New RequiredField(DCMHadmitRightsrbl, "Admitting Rights for DCMH")

				'    providerrbl.SelectedIndex = 0

				'    pnlProvider.Visible = True

				'    VendorNameTextBox.Visible = True
				'    VendorNameTextBox.Enabled = True
				'    'lblVendor.Text = "Vendor Name:"
				'    Vendorddl.Visible = False


				'    pnlOtherAffilliation.Visible = False
				'    'pnlProvider.Visible = False
				'    providerrbl.SelectedValue = "Yes"
				'    'Titleddl.SelectedValue = Nothing
				'    'Titleddl.Enabled = False

			Case "1196", "1199"
				'Case "other" Non ckhn staff

				Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
				Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")

				Dim add1Req As New RequiredField(address_1textbox, "Address 1")
				Dim cityReq As New RequiredField(citytextbox, "City")
				Dim stateReq As New RequiredField(statetextbox, "State")
				Dim zipReq As New RequiredField(ziptextbox, "ZIP")
				Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
				'Dim vendorRequired As New RequiredField(VendorNameTextBox, "Vendor")

				Dim startReq As New RequiredField(start_dateTextBox, "Start Date")




                pnlProvider.Visible = False
				'providerrbl.SelectedIndex = -1

				VendorNameTextBox.Visible = True
				VendorNameTextBox.Enabled = False

				'lblVendor.Text = "Vendor Name:"
				Vendorddl.Visible = False




			Case Else

				Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
				Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")
                'Dim Aff As New RequiredField(emp_type_cdrbl, "Affiliation")


                pnlProvider.Visible = False
				'providerrbl.SelectedIndex = -1



		End Select

		Dim RoleDesc As New RequiredField(Rolesddl, "Position Description")
		Rolesddl.BackColor() = Color.Yellow
		Rolesddl.BorderColor() = Color.Black
		Rolesddl.BorderStyle() = BorderStyle.Solid
		Rolesddl.BorderWidth = Unit.Pixel(2)

		Return bHasError

	End Function
	Protected Sub btnDemoRequest_Click(sender As Object, e As System.EventArgs) Handles btnDemoRequest.Click
		Response.Redirect("~/providerDemo.aspx?clientnum=" & receiver_client_numLabel.Text, True)

	End Sub
	Protected Sub btnSelectOnBehalfOf_Click(sender As Object, e As System.EventArgs) Handles btnSelectOnBehalfOf.Click
		BeHalOfPopup2.Show()
	End Sub
	Protected Sub btnCloseOnBelafOf_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCloseOnBelafOf.Click
		BeHalOfPopup2.Hide()

		'    pnlOnBehalfOf.Visible = False

	End Sub
	Protected Sub gvSearch_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvSearch.RowCommand

		If e.CommandName = "Select" Then

			Dim client_num As String = gvSearch.DataKeys(Convert.ToInt32(e.CommandArgument))("client_num").ToString()
			Dim login_name As String = gvSearch.DataKeys(Convert.ToInt32(e.CommandArgument))("login_name").ToString()

			Dim dbAccess As New CkhsEmployeeDbAccess(Session("EmployeeConn"))
			Dim ckhsEmployee = dbAccess.GetCkhsEmployee(client_num)

            'c3Controller = New Client3Controller(client_num)

            Dim iclient As Integer = 0


            requestor_nameLabel.Text = ckhsEmployee.FullName & " (" & ckhsEmployee.EMail & ")"
			requestor_client_numLabel.Text = client_num

            iclient = Convert.ToInt32(requestor_client_numLabel.Text)

            Dim thisData As New CommandsSqlAndOleDb("UpdAccountRequestor", Session("EmployeeConn"))
            thisData.AddSqlProcParameter("@accountrequestnum", iRequestNum, SqlDbType.Int)
            thisData.AddSqlProcParameter("@clientnum", iclient, SqlDbType.Int)

            thisData.ExecNonQueryNoReturn()


            SearchFirstNameTextBox.Text = ""
			SearchLastNameTextBox.Text = ""

			gvSearch.Dispose()
			gvSearch.DataBind()
			gvSearch.SelectedIndex = -1

			pnlOnBehalfOf.Visible = False


			Response.Redirect("./AccountRequestInfo2.aspx?RequestNum=" & iRequestNum & "&RequestorNum=" & requestor_client_numLabel.Text & "&RequestorName=" & requestor_nameLabel.Text, True)


		End If
	End Sub
	Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click

		Dim thisData As New CommandsSqlAndOleDb("SelActiveCKHSEmployees", Session("EmployeeConn"))
		thisData.AddSqlProcParameter("@FirstName", SearchFirstNameTextBox.Text, SqlDbType.NVarChar, 20)
		thisData.AddSqlProcParameter("@LastName", SearchLastNameTextBox.Text, SqlDbType.NVarChar, 20)


		gvSearch.DataSource = thisData.GetSqlDataTable
		gvSearch.DataBind()

		BeHalOfPopup2.Show()

	End Sub

	Protected Sub btnAddComments_Click(sender As Object, e As System.EventArgs) Handles btnAddComments.Click
		CommentsPopup.Show()
	End Sub
	Protected Sub btnAddDeleteComments_Click(sender As Object, e As System.EventArgs) Handles btnAddDeleteComments.Click
		CommentsPopup.Show()
	End Sub
	Protected Sub SendProviderEmail(ByVal AccountRequestNum As Integer)
		'======================
		' Get current path
		'======================

		Dim path As String
		Dim directory As String = ""
		Dim toList As String()

		path = HttpContext.Current.Request.Url.AbsoluteUri
		Dim pathParts() As String = Split(path, "/")
		Dim i As Integer

		Dim EmailList As String


		Do While i < pathParts.Length() - 1

			directory = directory + pathParts(i) + "/"
			i = i + 1

		Loop

		Mailmsg.To.Clear()


		'Dim toEmail As String = EmailAddress.Email
		Dim sSubject As String
		Dim sBody As String
		Dim fulleMailList As String

		sSubject = "New Account has been ADDED to open Request for " & first_nameTextBox.Text & " " & last_nameTextBox.Text


		sBody = "<!DOCTYPE html>" &
							"<html>" &
							"<head>" &
							"<style>" &
							"table" &
							"{" &
							"border-collapse:collapse;" &
							"font-family: 'Helvetica Neue', 'Lucida Grande', 'Segoe UI', Arial, Helvetica, Verdana, sans-serif;" &
							"}" &
							"table, th, td" &
							"{" &
							"border: 1px solid black;" &
							"padding: 3px" &
							"}" &
							"</style>" &
							"</head>" &
							"" &
							"<body>" &
							"A new Account has been Added to an Open Request .  Follow the link below to go to view details of the request." &
							 "<br />" &
							"<a href=""" & directory & "/AccountRequestInfo2.aspx?requestnum=" & AccountRequestNum & """ ><u><b>Name on Accounts: " & first_nameTextBox.Text & " " & last_nameTextBox.Text & "</b></u></a>" &
							"</body>" &
						"</html>"


		EmailList = New EmailListOne(AccountRequestNum, Session("EmployeeConn")).EmailListReturn

		fulleMailList = EmailList
		If Session("environment").ToString.ToLower = "testing" Then
			EmailList = "Jeff.Mele@crozer.org"
			sBody = sBody & fulleMailList

		End If

		toList = EmailList.Split(";")
		If toList.Length = 1 Then
			Mailmsg.To.Add(New System.Net.Mail.MailAddress(toList(0)))
		Else
		End If
		For i = 0 To toList.Length - 1 ' - 2
			If toList(i).Length > 10 Then ' validity check
				Mailmsg.To.Add(New System.Net.Mail.MailAddress(toList(i)))
			End If
		Next

		'Dim mailObj As New MailMessage
		Mailmsg.From = New MailAddress("CSC@crozer.org")
		Mailmsg.IsBodyHtml = True

		Mailmsg.Subject = sSubject
		Mailmsg.Body = sBody

		Dim smtpClient As New SmtpClient()
        smtpClient.Host = "ah1smtprelay01.altahospitals.com"

        smtpClient.Send(Mailmsg)

	End Sub

    Protected Sub Rolesddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles Rolesddl.SelectedIndexChanged
        'always use the Main Role
        mainRole = emp_type_cdrbl.SelectedValue

        'cpmModelEmplPanel.Visible = False
        'cpmModelAfterPanel.Visible = False

        'CPMEmployeesddl.SelectedIndex = -1
        'cpmModelAfterTextbox.Text = ""


        If Rolesddl.SelectedValue.ToString <> "Select" Then
            user_position_descTextBox.Text = Rolesddl.SelectedItem.ToString
            PositionNumTextBox.Text = Rolesddl.SelectedValue

            If thisRole Is Nothing Then
                thisRole = Rolesddl.SelectedValue
            End If
        ElseIf Rolesddl.SelectedValue.ToString = "Select" And mainRole <> Nothing Then
            Rolesddl.SelectedValue = mainRole
            'PositionNumTextBox
        End If


        Select Case RoleNumLabel.Text 'RoleNumLabel.Text
            'Case "782", "1195", "1199", "1193", "1567"


            Case Else

                If RoleNumLabel.Text <> "" Then 'RoleNumLabel.Text <> "" Then

                    Dim tcldata As New CommandsSqlAndOleDb("SelInvisionUserTypeV20", Session("EmployeeConn"))
                    tcldata.AddSqlProcParameter("@rolenum", PositionNumTextBox.Text, SqlDbType.NVarChar, 8)
                    'tcldata.AddSqlProcParameter("@rolenum", RoleNumLabel.Text, SqlDbType.NVarChar, 8)
                    tcldata.GetSqlDataset()




                    dtTCLUserTypes = tcldata.GetSqlDataTable


                End If

                If dtTCLUserTypes.Rows.Count = 1 Then
                    'TCLddl.SelectedIndex = 0

                    TCLTextBox.Text = dtTCLUserTypes.Rows(0)("TCL").ToString

                    UserTypeDescTextBox.Text = dtTCLUserTypes.Rows(0)("UserTypeDesc").ToString
                    UserTypeCdTextBox.Text = dtTCLUserTypes.Rows(0)("UserTypecd").ToString

                    pnlTCl.Visible = True

                    TCLTextBox.Visible = True
                    UserTypeDescTextBox.Visible = True
                    UserTypeCdTextBox.Visible = True

                    If masterRole.Text = "1193" Then

                        If Session("SecurityLevelNum") = 95 Or Session("LoginID").ToString.ToLower = "burgina" Or Session("LoginID").ToString.ToLower = "melej" Or Session("LoginID").ToString.ToLower = "atkm00" Then
                            btnChangeTCL.Visible = True
                            btnChangeTCL.Width = Unit.Pixel("200")



                            btnChangeTCL.Text = "Show All User Types"
                        End If

                    End If


                ElseIf dtTCLUserTypes.Rows.Count = 0 Then
                    pnlTCl.Visible = False

                ElseIf Session("SecurityLevelNum") = 95 Or Session("LoginID").ToString.ToLower = "burgina" Or Session("LoginID").ToString.ToLower = "melej" Or Session("LoginID").ToString.ToLower = "atkm00" Then

                    gvTCL.DataSource = dtTCLUserTypes
                    gvTCL.DataBind()

                    gvTCL.Visible = True
                    gvTCL.BackColor() = Color.Yellow
                    gvTCL.BorderColor() = Color.Black
                    gvTCL.BorderStyle() = BorderStyle.Solid
                    gvTCL.BorderWidth = Unit.Pixel(2)
                    gvTCL.RowStyle.BackColor() = Color.Yellow

                    pnlTCl.Visible = True
                    TCLTextBox.Visible = True
                    UserTypeDescTextBox.Visible = True
                    UserTypeCdTextBox.Visible = True

                    If masterRole.Text = "1193" Then

                        btnChangeTCL.Visible = True
                        btnChangeTCL.Text = "Show All User Types"
                        btnChangeTCL.Width = Unit.Pixel("200")

                    End If

                End If


        End Select


        Select Case PositionNumTextBox.Text

            Case "1567"
                PhysOfficeddl.Dispose()

                Dim ddEMRBinder As New DropDownListBinder(PhysOfficeddl, "SelPhysGroupsV5", Session("EmployeeConn"))
                ddEMRBinder.AddDDLCriteria("@GroupTypeCd", "GNum", SqlDbType.NVarChar)
                'SelPhysGroupsV5
                ddEMRBinder.BindData("Group_num", "group_name")

                PhysOfficepanel.Visible = True

                PhysOfficeddl.BackColor() = Color.Yellow
                PhysOfficeddl.BorderColor() = Color.Black
                PhysOfficeddl.BorderStyle() = BorderStyle.Solid
                PhysOfficeddl.BorderWidth = Unit.Pixel(2)
                'EMROfficeddl
                PhysOfficepanel.Visible = True

                'CPMEmployeesddl.Dispose()

                'Dim ddcmpMOdelafter As New DropDownListBinder
                'ddcmpMOdelafter.BindData(CPMEmployeesddl, "SelCPMClients", "client_num", "FullName", Session("EmployeeConn"))

                'cpmModelEmplPanel.Visible = True

                ''CPMEmployeesddl
                'CPMEmployeesddl.BackColor() = Color.Yellow
                'CPMEmployeesddl.BorderColor() = Color.Black
                'CPMEmployeesddl.BorderStyle() = BorderStyle.Solid
                'CPMEmployeesddl.BorderWidth = Unit.Pixel(2)


                'start_dateTextBox.BackColor() = Color.Yellow
                'start_dateTextBox.BorderColor() = Color.Black
                'start_dateTextBox.BorderStyle() = BorderStyle.Solid
                'start_dateTextBox.BorderWidth = Unit.Pixel(2)



        End Select


    End Sub
    Protected Sub btnChangeTCL_Click(sender As Object, e As System.EventArgs) Handles btnChangeTCL.Click
        'gvTCL.Visible = True
        'gvTCL.BackColor() = Color.Yellow
        'gvTCL.BorderColor() = Color.Black
        'gvTCL.BorderStyle() = BorderStyle.Solid
        'gvTCL.BorderWidth = Unit.Pixel(2)
        'gvTCL.RowStyle.BackColor() = Color.Yellow

        'btnChangeTCL.Visible = False

        If btnChangeTCL.Text = "Show All User Types" Then

            gvTCL.DataSource = Nothing
            gvTCL.DataBind()
            gvTCL.Dispose()


            Dim tcldata As New CommandsSqlAndOleDb("SelInvisionUserTypeV2", Session("EmployeeConn"))
            'ckhsEmployee.RoleNum  RoleNumLabel.Text
            tcldata.AddSqlProcParameter("@rolenum", masterRole.Text, SqlDbType.NVarChar, 8)
            tcldata.AddSqlProcParameter("@Clientupd", "all", SqlDbType.NVarChar, 3)

            tcldata.GetSqlDataset()

            dtTCLUserTypes = tcldata.GetSqlDataTable

            gvTCL.DataSource = dtTCLUserTypes
            gvTCL.DataBind()

            gvTCL.Visible = True
            'gvTCL.BackColor() = Color.Yellow
            'gvTCL.BorderColor() = Color.Black
            'gvTCL.BorderStyle() = BorderStyle.Solid
            'gvTCL.BorderWidth = Unit.Pixel(2)
            'gvTCL.RowStyle.BackColor() = Color.Yellow

            pnlTCl.Visible = True
            TCLTextBox.Visible = True
            UserTypeDescTextBox.Visible = True
            UserTypeCdTextBox.Visible = True
            ' btnChangeTCL.Visible = True

            btnChangeTCL.Text = "Close Types"

        ElseIf btnChangeTCL.Text = "Change TCL" Or btnChangeTCL.Text = "Select TCL" Then
            GetTCL()
            btnChangeTCL.Text = "Show All User Types"

        ElseIf btnChangeTCL.Text = "Close Types" Then
            gvTCL.Dispose()
            gvTCL.Visible = False
            btnChangeTCL.Text = "Show All User Types"
        Else
            gvTCL.Visible = True
            btnChangeTCL.Text = "Show All User Types"
            'gvTCL.BackColor() = Color.Yellow
            'gvTCL.BorderColor() = Color.Black
            'gvTCL.BorderStyle() = BorderStyle.Solid
            'gvTCL.BorderWidth = Unit.Pixel(2)
            'gvTCL.RowStyle.BackColor() = Color.Yellow

            'btnChangeTCL.Visible = False

        End If
    End Sub
    Protected Sub GetTCL()

        Dim tcldata As New CommandsSqlAndOleDb("SelInvisionUserTypeV2", Session("EmployeeConn"))
        If PositionNumTextBox.Text <> "" And PositionNumTextBox.Text <> "0" Then
            tcldata.AddSqlProcParameter("@rolenum", PositionNumTextBox.Text, SqlDbType.NVarChar, 8)
        Else
            tcldata.AddSqlProcParameter("@rolenum", RoleNumLabel.Text, SqlDbType.NVarChar, 8)
        End If
        If TCLTextBox.Text <> "" Then
            tcldata.AddSqlProcParameter("@Clientupd", "yes", SqlDbType.NVarChar, 8)

        End If


        If TitleLabel.Text <> "" Then
            tcldata.AddSqlProcParameter("@UserTypeCd", TitleLabel.Text, SqlDbType.NVarChar, 25)

        End If


        tcldata.GetSqlDataset()




        dtTCLUserTypes = tcldata.GetSqlDataTable
        gvTCL.DataSource = dtTCLUserTypes
        gvTCL.DataBind()


        If dtTCLUserTypes.Rows.Count = 1 Then
            'TCLddl.SelectedIndex = 0

            TCLTextBox.Text = dtTCLUserTypes.Rows(0)("TCL").ToString
            UserTypeDescTextBox.Text = dtTCLUserTypes.Rows(0)("UserTypeDesc").ToString
            UserTypeCdTextBox.Text = dtTCLUserTypes.Rows(0)("UserTypecd").ToString

            pnlTCl.Visible = True
            TCLTextBox.Visible = True
            UserTypeDescTextBox.Visible = True
            UserTypeCdTextBox.Visible = True

            'TCLddl.Visible = False

            gvTCL.Visible = False

        ElseIf dtTCLUserTypes.Rows.Count = 0 Then
            pnlTCl.Visible = False

        ElseIf TCLTextBox.Text <> "" And dtTCLUserTypes.Rows.Count > 1 Then
            pnlTCl.Visible = True
            TCLTextBox.Visible = True
            UserTypeDescTextBox.Visible = True
            UserTypeCdTextBox.Visible = True

            gvTCL.Visible = True
            'btnChangeTCL.Visible = True

        ElseIf TCLTextBox.Text = "" And dtTCLUserTypes.Rows.Count > 1 Then
            pnlTCl.Visible = True
            TCLTextBox.Visible = True
            UserTypeDescTextBox.Visible = True
            UserTypeCdTextBox.Visible = True

            gvTCL.Visible = True

            btnChangeTCL.Text = "Close Types"

            'btnChangeTCL.Visible = True

        End If

    End Sub
    Protected Sub gvTCL_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvTCL.RowCommand
        If e.CommandName = "Select" Then


            TCLTextBox.Text = gvTCL.DataKeys(Convert.ToInt32(e.CommandArgument))("TCL").ToString()
            UserTypeCdTextBox.Text = gvTCL.DataKeys(Convert.ToInt32(e.CommandArgument))("UserTypeCd").ToString()
            UserTypeDescTextBox.Text = gvTCL.DataKeys(Convert.ToInt32(e.CommandArgument))("UserTypeDesc").ToString()

            If TCLTextBox.Text.ToLower = "remove" Then
                TCLTextBox.Text = ""
                UserTypeCdTextBox.Text = ""
                UserTypeDescTextBox.Text = ""

                gvTCL.Visible = False
                btnChangeTCL.Visible = True
            Else
                pnlTCl.Visible = True
                btnChangeTCL.Visible = True
                btnChangeTCL.Text = "Select TCL"
            End If
        End If

    End Sub


    Protected Sub hanrbl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles hanrbl.SelectedIndexChanged
		If hanrbl.SelectedValue.ToString = "Yes" Then
			LocationOfCareIDddl.Visible = True
			ckhnlbl.Visible = True

			'lblprov.Visible = True
			'providerrbl.Visible = True

			LocationOfCareIDddl.BackColor() = Color.Yellow
			LocationOfCareIDddl.BorderColor() = Color.Black
			LocationOfCareIDddl.BorderStyle() = BorderStyle.Solid
			LocationOfCareIDddl.BorderWidth = Unit.Pixel(2)

			'npitextbox.BackColor() = Color.Yellow
			'npitextbox.BorderColor() = Color.Black
			'npitextbox.BorderStyle() = BorderStyle.Solid
			'npitextbox.BorderWidth = Unit.Pixel(2)

			'LicensesNoTextBox.BackColor() = Color.Yellow
			'LicensesNoTextBox.BorderColor() = Color.Black
			'LicensesNoTextBox.BorderStyle() = BorderStyle.Solid
			'LicensesNoTextBox.BorderWidth = Unit.Pixel(2)

			'taxonomytextbox.BackColor() = Color.Yellow
			'taxonomytextbox.BorderColor() = Color.Black
			'taxonomytextbox.BorderStyle() = BorderStyle.Solid
			'taxonomytextbox.BorderWidth = Unit.Pixel(2)

			'start_dateTextBox.BackColor() = Color.Yellow
			'start_dateTextBox.BorderColor() = Color.Black
			'start_dateTextBox.BorderStyle() = BorderStyle.Solid
			'start_dateTextBox.BorderWidth = Unit.Pixel(3)



		Else
			LocationOfCareIDddl.Visible = False
			ckhnlbl.Visible = False
			LocationOfCareIDLabel.Text = ""

			'lblprov.Visible = False
			'providerrbl.Visible = False
			'providerrbl.SelectedIndex = -1

		End If

	End Sub
	Protected Sub LocationOfCareIDddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles LocationOfCareIDddl.SelectedIndexChanged
		' Gnumber

		'CPMEmployeesddl.Dispose()

		If LocationOfCareIDLabel.Text <> "" Then

			LocationOfCareIDLabel.Text = LocationOfCareIDddl.SelectedItem.ToString
			GNumberGroupNumLabel.Text = LocationOfCareIDddl.SelectedValue

			'Dim ddcmpMOdelafter As New DropDownListBinder(CPMEmployeesddl, "SelCPMClients", Session("EmployeeConn"))
			'ddcmpMOdelafter.AddDDLCriteria("@Group_num", GNumberGroupNumLabel.Text, SqlDbType.NVarChar)
			'ddcmpMOdelafter.BindData("client_num", "FullName")

		ElseIf LocationOfCareIDddl.SelectedValue.ToLower = "select" Or LocationOfCareIDddl.SelectedValue = "99999" Then

			LocationOfCareIDLabel.Text = Nothing
			GNumberGroupNumLabel.Text = Nothing

			'Dim ddcmpMOdelafter As New DropDownListBinder(CPMEmployeesddl, "SelCPMClients", Session("EmployeeConn"))
			'ddcmpMOdelafter.AddDDLCriteria("@cpmtype ", "physician", SqlDbType.NVarChar)
			'ddcmpMOdelafter.BindData("client_num", "FullName")


		Else

			LocationOfCareIDLabel.Text = LocationOfCareIDddl.SelectedItem.ToString
			GNumberGroupNumLabel.Text = LocationOfCareIDddl.SelectedValue

			'Dim ddcmpMOdelafter As New DropDownListBinder(CPMEmployeesddl, "SelCPMClients", Session("EmployeeConn"))
			'ddcmpMOdelafter.AddDDLCriteria("@Group_num", GNumberGroupNumLabel.Text, SqlDbType.NVarChar)
			'ddcmpMOdelafter.BindData("client_num", "FullName")


		End If

		'cpmModelEmplPanel.Visible = True
		'cpmPhysSchedulePanel.Visible = True

		''CPMEmployeesddl
		'CPMEmployeesddl.BackColor() = Color.Yellow
		'CPMEmployeesddl.BorderColor() = Color.Black
		'CPMEmployeesddl.BorderStyle() = BorderStyle.Solid
		'CPMEmployeesddl.BorderWidth = Unit.Pixel(2)


		''Schedulablerbl
		'Schedulablerbl.BackColor() = Color.Yellow
		'Schedulablerbl.BorderColor() = Color.Black
		'Schedulablerbl.BorderStyle() = BorderStyle.Solid
		'Schedulablerbl.BorderWidth = Unit.Pixel(2)

		''Televoxrbl
		'Televoxrbl.BackColor() = Color.Yellow
		'Televoxrbl.BorderColor() = Color.Black
		'Televoxrbl.BorderStyle() = BorderStyle.Solid
		'Televoxrbl.BorderWidth = Unit.Pixel(2)




	End Sub
    'Protected Sub CommLocationOfCareIDddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles CommLocationOfCareIDddl.SelectedIndexChanged
    '	If CommLocationOfCareIDLabel.Text <> "" Then

    '		CommLocationOfCareIDLabel.Text = CommLocationOfCareIDddl.SelectedItem.ToString
    '		CommNumberGroupNumLabel.Text = CommLocationOfCareIDddl.SelectedValue


    '	ElseIf CommLocationOfCareIDddl.SelectedValue.ToLower = "select" Or CommLocationOfCareIDddl.SelectedValue = "99999" Then

    '		CommLocationOfCareIDLabel.Text = Nothing
    '		CommNumberGroupNumLabel.Text = Nothing

    '	Else

    '		CommLocationOfCareIDLabel.Text = CommLocationOfCareIDddl.SelectedItem.ToString
    '		CommNumberGroupNumLabel.Text = CommLocationOfCareIDddl.SelectedValue


    '	End If
    'End Sub

    '   Protected Sub CoverageGroupddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles CoverageGroupddl.SelectedIndexChanged
    '	' Coverage group
    '	If CoverageGroupNumLabel.Text <> "" Then

    '		CoverageGroupNumLabel.Text = CoverageGroupddl.SelectedValue

    '	ElseIf CoverageGroupddl.SelectedValue.ToLower = "select" Or CoverageGroupddl.SelectedValue = "99999" Then

    '		CoverageGroupNumLabel.Text = Nothing
    '	Else

    '		CoverageGroupNumLabel.Text = CoverageGroupddl.SelectedValue

    '	End If

    'End Sub

    '   Protected Sub Nonperferdddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles Nonperferdddl.SelectedIndexChanged
    '	' Non perfered group
    '	If NonPerferedGroupnumLabel.Text <> "" Then

    '		NonPerferedGroupnumLabel.Text = Nonperferdddl.SelectedValue

    '	ElseIf Nonperferdddl.SelectedValue.ToLower = "select" Or Nonperferdddl.SelectedValue = "99999" Then

    '		NonPerferedGroupnumLabel.Text = Nothing
    '	Else

    '		NonPerferedGroupnumLabel.Text = Nonperferdddl.SelectedValue

    '	End If
    'End Sub

    'Protected Sub CPMEmployeesddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles CPMEmployeesddl.SelectedIndexChanged

    '    If CPMEmployeesddl.SelectedValue < 0 Then
    '        cpmModelEmplPanel.Visible = False
    '        CPMEmployeesddl.SelectedIndex = -1
    '        cpmModelAfterPanel.Visible = True
    '    Else
    '        ViewCPMModelTextBox.Text = CPMEmployeesddl.SelectedItem.ToString
    '        cpmModelAfterPanel.Visible = True
    '    End If
    '    Select PositionRoleNumLabel.Text
    '        Case "1567"
    '            CPMModelTextBox.Text = ViewCPMModelTextBox.Text
    '        Case "1553"
    '            provmodelTextBox.Text = ViewCPMModelTextBox.Text
    '    End Select

    'End Sub
    'Protected Sub Schedulablerbl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles Schedulablerbl.SelectedIndexChanged
    '    SchedulableTextbox.Text = Schedulablerbl.SelectedValue.ToLower

    'End Sub

    'Protected Sub Televoxrbl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles Televoxrbl.SelectedIndexChanged
    '    TelevoxTextBox.Text = Televoxrbl.SelectedValue.ToLower
    'End Sub
    Private Sub sendAncillaryEmail()
		Dim Request As New AccountRequest(iRequestNum, Session("EmployeeConn"))
		Dim RequestItems As New RequestItems(iRequestNum)
		Dim EmailBody As New EmailBody

		Dim sSubject As String
		Dim sBody As String

		Dim EmailList As String
		Dim sAppbase As String = ""
		Dim Link As String

		Dim sAccounts As String = ""

		If Session("environment").ToString.ToLower = "testing" Then
			'toEmail = "Jeff.Mele@crozer.org"
			Link = "http://webdev3/CSCV2/AccountRequestInfo2.aspx?RequestNum=" & iRequestNum.ToString
		Else
			Link = "http://Casemgmt02/CSCV2/AccountRequestInfo2.aspx?RequestNum=" & iRequestNum.ToString

		End If

		sSubject = "Provider/Physician Credential Date for " & Request.FirstName & " " & Request.LastName & " ."


		sBody = "<!DOCTYPE html>" &
"<html>" &
"<head>" &
"<style>" &
"table" &
"{" &
"border-collapse:collapse;" &
"font-family: 'Helvetica Neue', 'Lucida Grande', 'Segoe UI', Arial, Helvetica, Verdana, sans-serif;" &
"}" &
"table, th, td" &
"{" &
"border: 1px solid black;" &
"padding: 3px" &
"}" &
"</style>" &
"</head>" &
"" &
"<body>" &
"<table cellpadding='5px'>" &
"<tr>" &
	"<td colspan='3' align='center'  style='font-size:large'>" &
"               " &
	"<b>Provider/Physician Credential Date Update</b>" &
"                 " &
	 "</td>    " &
"</tr>" &
"<tr>" &
	"<td style='font-weight:bold' colspan='1'>Name:</td>" &
	"<td colspan='3'>" & Request.FirstName & " " & Request.LastName & "</td>" &
"</tr>" &
"<tr>" &
	"<td style='font-weight:bold' colspan='1'>Requested By:</td>" &
	"<td colspan='3'>" & Request.RequestorName & "</td>" &
"</tr>" &
   "<tr>" &
	"<td style='font-weight:bold' colspan='1'>Requestor Email:</td>" &
	"<td colspan='3'>" & Request.RequestorEmail & "</td>" &
"</tr>" &
"<tr>" &
	"<td style='font-weight:bold' colspan='1'>Date Requested:</td>" &
	"<td colspan='3'>" & Request.EnteredDate & "</td>" &
"</tr>" &
"      " &
"     " &
"<tr style='font-weight:bold'><td>Information </td><td> Data </td></tr>" &
"<tr><td> First Name </td><td>" & Request.FirstName & "</td></tr>" &
"<tr><td> Last Name </td><td>" & Request.LastName & "</td></tr>" &
"<tr><td> CCMC Credential Date</td><td>" & Request.CredentialedDate & "</td></tr>" &
"<tr><td> DCMH Credential Date</td><td>" & Request.CredentialedDateDCMH & "</td></tr>" &
"<tr><td> Address 1 </td><td>" & Request.Address1 & "</td></tr>" &
"<tr><td> Address 2 </td><td>" & Request.Address2 & "</td></tr>" &
"<tr><td> City </td><td>" & Request.City & "</td></tr>" &
"<tr><td> State </td><td>" & Request.State & "</td></tr>" &
"<tr><td> Zip </td><td>" & Request.Zip & "</td></tr>" &
"<tr><td> Phone </td><td>" & Request.Phone & "</td></tr>" &
"<tr><td> Fax </td><td>" & Request.Fax & "</td></tr>" &
"<tr><td> Doctor Master#</td><td>" & doctor_master_numTextBox.Text & "</td></tr>" &
"<tr><td> NPI</td><td>" & Request.Npi & "</td></tr>" &
"<tr><td> License No</td><td>" & Request.LicensesNo & "</td></tr>" &
"<tr><td> Specialty</td><td>" & Request.Specialty & "</td></tr>" &
"<tr><td> Title</td><td>" & Request.Title & "</td></tr>" &
"</table>" &
	   "</body>" &
		"</html>"

		sBody = sBody & "<table cellpadding='5px'>" &
		"<tr>" &
			"<td align='center'  style='font-size:large'>" &
"               " &
			"<b>View Details by clicking on link below</b>" &
"                 " &
			 "</td>    " &
		"</tr>" &
		"<tr>" &
			"<td align='center'>" & Link & "</td>" &
		"</tr>"


		'ActionControl.GetAllActions(AccountRequestNum, Session("EmployeeConn"))

		EmailList = New EmailListOne(1100, iRequestNum, Session("EmployeeConn")).EmailListReturn


		Dim sToAddress As String = Request.SubmitterEmail

		Dim toList As String()

		toList = EmailList.Split(";")
		If toList.Length = 1 Then
			Mailmsg.To.Add(New System.Net.Mail.MailAddress(toList(0)))
		Else
		End If
		For i = 0 To toList.Length - 1 ' - 2
			If toList(i).Length > 10 Then ' validity check
				Mailmsg.To.Add(New System.Net.Mail.MailAddress(toList(i)))
			End If
		Next

		Mailmsg.From = New MailAddress("ckhsitcsc@crozer.org")
		Mailmsg.IsBodyHtml = True

		Mailmsg.Subject = sSubject
		Mailmsg.Body = sBody

		Dim smtpClient As New SmtpClient()
        smtpClient.Host = "ah1smtprelay01.altahospitals.com"


        If Session("environment") = "Production" Then
            smtpClient.Send(Mailmsg)
        End If


        Dim thisData As New CommandsSqlAndOleDb("InsAccounteMailTo", Session("EmployeeConn"))
		thisData.AddSqlProcParameter("@account_request_num", iRequestNum, SqlDbType.Int)
		thisData.AddSqlProcParameter("@ToAddress", EmailList, SqlDbType.NVarChar, 4000)
		thisData.AddSqlProcParameter("@subject", sSubject, SqlDbType.NVarChar, 200)
		thisData.AddSqlProcParameter("@body", sBody, SqlDbType.NVarChar, 4000)

		thisData.ExecNonQueryNoReturn()




	End Sub

    Private Sub EditGrid_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles EditGrid.RowCommand
        If e.CommandName = "Select" Then

            lblloginmsg.Text = ""
            lblloginmsg.Visible = False

            Dim sAccountRequest As String
            Dim sAccountSeqNum As String
            Dim sAppNum As String
            Dim sLogin As String
            Dim sAppdesc As String


            sAccountRequest = EditGrid.DataKeys(Convert.ToInt32(e.CommandArgument))("account_request_num").ToString()
            sAccountSeqNum = EditGrid.DataKeys(Convert.ToInt32(e.CommandArgument))("account_request_seq_num").ToString()
            sAppNum = EditGrid.DataKeys(Convert.ToInt32(e.CommandArgument))("ApplicationNum").ToString()
            sLogin = EditGrid.DataKeys(Convert.ToInt32(e.CommandArgument))("login_name").ToString()
            sAppdesc = EditGrid.DataKeys(Convert.ToInt32(e.CommandArgument))("ApplicationDesc").ToString()

            txtAppName.Text = sAppdesc
            txtLogin.Text = sLogin

            HDAcctnum.Text = sAccountRequest
            HDAcctSeq.Text = sAccountSeqNum
            HDAppnum.Text = sAppNum

            EditGrid.SelectedIndex = -1

            'Response.Redirect("./AccountRequestInfo2.aspx?requestnum=" & sAccountRequest, True)


        End If
    End Sub

    Private Sub btnCommitLogin_Click(sender As Object, e As EventArgs) Handles btnCommitLogin.Click

        If txtLogin.Text <> "" Then

            Try

                If HDAllitems.Text = "" Then
                    HDAllitems.Text = "no"
                End If

                Dim thisData As New CommandsSqlAndOleDb("UpdAccountItemV20", Session("EmployeeConn"))
                thisData.AddSqlProcParameter("@account_request_num", HDAcctnum.Text, SqlDbType.Int)
                thisData.AddSqlProcParameter("@account_request_seq_num", HDAcctSeq.Text, SqlDbType.Int)
                thisData.AddSqlProcParameter("@ApplicationNum", HDAppnum.Text, SqlDbType.Int)
                thisData.AddSqlProcParameter("@login_name", txtLogin.Text, SqlDbType.NVarChar, 50)
                thisData.AddSqlProcParameter("@allitems", HDAllitems.Text, SqlDbType.NVarChar, 3)


                thisData.ExecNonQueryNoReturn()

            Catch ex As Exception
                lblloginmsg.Text = ex.ToString
                lblloginmsg.Visible = True
            End Try

            txtAppName.Text = ""
            txtLogin.Text = ""

            HDAcctnum.Text = ""
            HDAcctSeq.Text = ""
            HDAppnum.Text = ""
            HDAllitems.Text = "no"

            fillEditItemstable()
            EditGrid.SelectedIndex = -1
            AllItemsrbl.SelectedValue = "no"


        End If


    End Sub

    Private Sub AllItemsrbl_SelectedIndexChanged(sender As Object, e As EventArgs) Handles AllItemsrbl.SelectedIndexChanged
        HDAllitems.text = AllItemsrbl.SelectedValue
    End Sub


End Class
