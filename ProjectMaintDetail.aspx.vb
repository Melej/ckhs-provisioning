﻿Imports System.IO
Imports System.Data '
Imports System.Data.SqlClient
Imports App_Code
Imports AjaxControlToolkit

Partial Class ProjectMaintDetail
    Inherits System.Web.UI.Page
    Dim iProjectNum As Integer
    Dim FormSignOn As New FormSQL()
    'Dim VendorFormSignOn As FormSQL
    'Dim UpdPhyFormSignOn As FormSQL
    Dim iClientNum As Integer
    Dim iUserNum As Integer
    Dim iUserSecurityLevel As Integer
    Dim dtSelected As DataTable

    Dim UserInfo As UserSecurity


    Dim ProjectNumS As String
    Dim thisdata As CommandsSqlAndOleDb
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        UserInfo = New UserSecurity(Session("LoginID"), Session("EmployeeConn"))

        iUserNum = UserInfo.ClientNum
        iUserSecurityLevel = UserInfo.SecurityLevelNum
        'userDepartmentCD.Text = UserInfo.DepartmentCd

        Select Case iUserSecurityLevel
            Case (81), (80), (90), (91), (92), (93), (94), (100), (105)

            Case Else
                Response.Redirect("~/SimpleSearch.aspx", True)

        End Select

        If Request.QueryString("ProjectNum") <> "" Then
            iProjectNum = Request.QueryString("ClientNum")
            ProjectNameTextBox.Text = Request.QueryString("ProjectNum")
        End If

        Dim ProjectContr As New ProjectController()
        FormSignOn = New FormSQL(Session("CSCConn"), "SelProjectDesc", "InsUpProject", "", Page)
        FormSignOn.AddInsertParm("@ProjectDesc", ProjectDesctxt.Text, SqlDbType.NVarChar, 275)


        If Not IsPostBack Then

            Dim ddlBinder As New DropDownListBinder(Technicianddl, "SelTechniciansV8", Session("EmployeeConn"))
            ddlBinder.AddDDLCriteria("@techType", "project", SqlDbType.NVarChar)

            ddlBinder.BindData("client_num", "tech_name")


        End If


        If ProjectNameTextBox.Text <> "" Then


            Dim GetProJect As New ProjectController()
            GetProJect.GetProjectByNum(iProjectNum, Session("CSCConn"))

            ProjectDesctxt.Text = GetProJect.ProjectDesc
            ProjectNameTextBox.Text = GetProJect.ProjectName
            ProjectShortNameTextBox.Text = GetProJect.ProjectShortName
            StartDateTextBox.Text = GetProJect.StartDate.ToString
            EnddateTextBox.Text = GetProJect.endDate

            PlannedDateTextBox.Text = GetProJect.plannedDate
            ActualDateTextBox.Text = GetProJect.actualDate


            If StartDateTextBox.Text = "1/1/0001 12:00:00 AM" Then
                StartDateTextBox.Text = ""
            End If

            If EnddateTextBox.Text = "12:00:00 AM" Then
                EnddateTextBox.Text = ""
            End If

            If PlannedDateTextBox.Text = "12:00:00 AM" Then
                PlannedDateTextBox.Text = ""
            End If

            If ActualDateTextBox.Text = "12:00:00 AM" Then
                ActualDateTextBox.Text = ""
            End If

            iClientNum = GetProJect.ClientNum
            iProjectNum = GetProJect.ProjectNum
            ProjectNumTextBox.Text = iProjectNum
            client_numLabel.Text = iClientNum

            If client_numLabel.Text <> "" Then
                Technicianddl.SelectedValue = iClientNum
            End If


            fillTable()

            gvTicketDetail.Visible = True

            BtnAddProject.Text = "Update"
            BtnExcel2.Visible = True

        End If


    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click

        Dim thisData As New CommandsSqlAndOleDb("SelActiveCKHSEmployees", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@FirstName", FirstNameTextBox.Text, SqlDbType.NVarChar, 20)
        thisData.AddSqlProcParameter("@LastName", LastNameTextBox.Text, SqlDbType.NVarChar, 20)

        gvSearch.DataSource = thisData.GetSqlDataTable
        gvSearch.DataBind()

        'uplDepartmentOwner.Update()
        mpeProjecowner.Show()

    End Sub
    Protected Sub gvSearch_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvSearch.RowCommand
        If e.CommandName = "Select" Then
            Dim client_num As String = gvSearch.DataKeys(Convert.ToInt32(e.CommandArgument))("client_num").ToString()
            Dim login_name As String = gvSearch.DataKeys(Convert.ToInt32(e.CommandArgument))("login_name").ToString()
            Dim dbAccess As New CkhsEmployeeDbAccess(Session("EmployeeConn"))
            Dim ckhsEmployee = dbAccess.GetCkhsEmployee(client_num)

            'c3Controller = New Client3Controller(client_num)
            'c3Submitter = c3Controller.GetClientByClientNum(client_num)


            'requestor_client_numLabel.Text = client_num
            'btnUpdateSubmitter.Visible = True


            DepartmentOwnerTextbox.Text = ckhsEmployee.FullName & " (" & ckhsEmployee.EMail & ")"

            DepartmentOwnerTextbox.Enabled = False

            gvSearch.SelectedIndex = -1
            gvSearch.Dispose()

            mpeProjecowner.Hide()


            'Select Case userDepartmentCD.Text
            '    Case "832600", "832700", "823100"
            '        If Session("SecurityLevelNum") < 90 And requestor_client_numLabel.Text = submitter_client_numLabel.Text Then
            '            btnSelectOnBehalfOf.Visible = True
            '            lblValidation.Text = "Must Change Requestor"
            '            lblValidation.Focus()
            '        ElseIf Session("SecurityLevelNum") < 90 And requestor_client_numLabel.Text <> submitter_client_numLabel.Text Then
            '            lblValidation.Text = ""
            '            btnSelectOnBehalfOf.BackColor() = Color.FromKnownColor("336666")
            '            btnSelectOnBehalfOf.ForeColor() = Color.White
            '            btnSelectOnBehalfOf.BorderStyle() = BorderStyle.Groove
            '            btnSelectOnBehalfOf.BorderWidth = Unit.Pixel(1)



            '        End If


            'End Select

        End If

    End Sub
    Protected Sub fillTable()

        Dim thisData As New CommandsSqlAndOleDb("SelRequestsByproject", Session("CSCConn"))

        thisData.AddSqlProcParameter("@projectNum", ProjectNumTextBox.Text, SqlDbType.NVarChar)

        Dim iRowCount = 1
        Dim iTotalRowcount = 0

        Dim dt As DataTable
        dt = thisData.GetSqlDataTable

        gvTicketDetail.DataSource = thisData.GetSqlDataTable

        gvTicketDetail.DataBind()

        Session("projectRequeststbl") = dt


    End Sub

    Protected Sub btnDepartmentOwner_Click(sender As Object, e As System.EventArgs) Handles btnDepartmentOwner.Click
        mpeProjecowner.Show()

    End Sub
End Class
