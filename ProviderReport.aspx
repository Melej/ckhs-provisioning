﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="false" CodeFile="ProviderReport.aspx.vb" Inherits="ProviderReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <style type="text/css">
        .style3
    {
        width: 371px;
    }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="100"  >
   <ProgressTemplate>
        <div id="IMGDIV" align="center" valign="middle" runat="server" style="position:
                absolute;left: 400px;top:150px;visibility:visible;vertical-align:middle;border-style:
                inset;border-color:black;background-color:White;width:210px;height:75px; z-index: 999;">
        Loading Data ...
            <img src="Images/AjaxProgress.gif" /><br />
        </div>
   </ProgressTemplate>
</asp:UpdateProgress>
 <asp:UpdatePanel ID="uplPanel" runat ="server"  UpdateMode="Conditional">
  <ContentTemplate>
    <table  style="border-style: groove" align="left" border="3px" width="100%">
        <tr align="center">
           <td class="tableRowHeader" colspan="2">
                  Provider Report
           </td>
        </tr>
        <tr>
            <td  align="center" class="style3">
                                
                <asp:Button ID="btnExcelReport" runat="server" Text="Excel RPT."  Width="120px" Height="30px"
                            Font-Names="Arial" ForeColor="White" Font-Bold="True" Font-Size="Small" />
            </td>
             <td align="center" >
                        <asp:Button ID="btnSubmit" runat="server" Text = "Submit" />
             </td>

        </tr>
        <tr>
            <td align="right" class="style3">
                Start Date:
            </td>
            <td align="left">
                <asp:TextBox ID="start_dateTextBox" runat="server" CssClass="calenderClass"></asp:TextBox>
            </td>
        </tr>
        <tr>
             <td align="right" class="style3">
                End Date:
            </td>
            <td align="left">
                <asp:TextBox ID="end_dateTextBox" runat="server" CssClass="calenderClass"></asp:TextBox>
            </td>

        </tr>
        <tr>
            <td class="style3" align="right" >
                Display All Provider Request Open and Closed?
            </td>
            <td align="left">
                <asp:RadioButtonList ID = "AllRequestrbl" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="all">Yes</asp:ListItem>
                    <asp:listItem Selected="True" Value="no">No</asp:listItem>

            </asp:RadioButtonList>

            </td>
        
        </tr>

        <tr>
             <td colspan="2">
                            <asp:GridView ID="GvPhysicians" runat="server" AllowSorting="True" 
                           AutoGenerateColumns="False" BackColor="White" BorderColor="#336666" 
                           BorderStyle="Double" BorderWidth="3px" CellPadding="1" 
                           DataKeyNames="accountrequestnum" 
                           EmptyDataText="No Accounts found " 
                           EnableTheming="False" Font-Size="Medium" GridLines="Horizontal" Width="100%">
                           <FooterStyle BackColor="White" ForeColor="#333333" />
                           <HeaderStyle BackColor="#336666" Font-Bold="False" Font-Size="X-Small" 
                               ForeColor="White" HorizontalAlign="Left" VerticalAlign="Middle" />
                           <PagerStyle BackColor="#336666" Font-Size="Medium" ForeColor="White" 
                               HorizontalAlign="Left" />
                           <RowStyle BackColor="White" ForeColor="#333333" Font-Size="Small" />
                           <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                           <SortedAscendingCellStyle BackColor="#F7F7F7" />
                           <SortedAscendingHeaderStyle BackColor="#487575" />
                           <SortedDescendingCellStyle BackColor="#E5E5E5" />
                           <SortedDescendingHeaderStyle BackColor="#275353" />
                           <Columns>

                               <asp:CommandField ButtonType="Button" ControlStyle-BackColor="#84A3A3" 
                                      HeaderStyle-HorizontalAlign="Left" ItemStyle-Height="20px" SelectText="Select" 
                                      ShowSelectButton="true">
                                  <ControlStyle BackColor="#84A3A3" />
                                  <HeaderStyle HorizontalAlign="Left" />
                                  <ItemStyle Height="20px" />
                                  </asp:CommandField>
                            
                               <asp:BoundField DataField="receiverfullname"  ItemStyle-HorizontalAlign="Left" HeaderText="Name" />

                               <asp:BoundField DataField="doctormasternum" ItemStyle-HorizontalAlign="Left" HeaderText="Provider# " />

                               <asp:BoundField DataField="title" ItemStyle-HorizontalAlign="Left" HeaderText="Title" />

                               <asp:BoundField DataField="specialty" ItemStyle-HorizontalAlign="Left" HeaderText="Specialty" />

                               <asp:BoundField DataField="entereddate"  ItemStyle-HorizontalAlign="Left" HeaderText="Entry Date" />

                           </Columns>
                       </asp:GridView>                            
            </td>
        </tr>
        <tr>
          <td colspan="2">
 
            <asp:Table  Width="100%" ID = "tblEndDate" runat = "server" Visible="false">
            </asp:Table>
          </td>
        </tr>

  </table> 
  
  </ContentTemplate>
 
 </asp:UpdatePanel>
</asp:Content>

