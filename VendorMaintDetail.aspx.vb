﻿Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports App_Code

Imports AjaxControlToolkit
Partial Class VendorMaintDetail
    Inherits System.Web.UI.Page
    Dim sActivitytype As String = ""
    Dim cblBinder As New CheckBoxListBinder
    Dim UserInfo As UserSecurity
    Dim FormSignOn As New FormSQL()

    Dim iUserNum As Integer
    Dim iUserSecurityLevel As Integer


    Dim VendorNum As String
    Dim AccountCd As String
    Dim MaintType As String
    Dim AttachedClients As Integer

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        VendorNum = Request.QueryString("VendorNum")

        UserInfo = New UserSecurity(Session("LoginID"), Session("EmployeeConn"))

        iUserNum = UserInfo.ClientNum
        iUserSecurityLevel = UserInfo.SecurityLevelNum

        If iUserSecurityLevel < 70 Then
            Response.Redirect("~/SimpleSearch.aspx", True)
        ElseIf iUserSecurityLevel < 90 Then
            BtnAddVendor.Visible = False
        End If

        FormSignOn = New FormSQL(Session("EmployeeConn"), "SelVendors", "InsUpdVendors", "", Page)
        FormSignOn.AddSelectParm("@VendorNum", VendorNum, SqlDbType.Int, 6)
        If Not IsPostBack Then
            If VendorNum.ToString = "" Then

                Response.Redirect("./VendorMaintenance.aspx", True)

            End If

            VendorNumLbl.Text = VendorNum.ToString

            FormSignOn.FillForm()
            bindAccounts()

            If AttachedClients = 0 Then
                Deactivatelbl.Visible = True
                DateDeactivatedrbl.Visible = True

            End If

        End If

    End Sub
    Protected Sub bindAccounts()
        'ErrorLbl.Visible = False


        Dim thisData As New CommandsSqlAndOleDb("SelVendorClientsBy", Session("EmployeeConn"))

        If VendorNum > 0 Then
            thisData.AddSqlProcParameter("@VendorNum", VendorNum, SqlDbType.Int)

        End If



        Dim dt As DataTable

        dt = thisData.GetSqlDataTable

        AttachedClients = dt.Rows.Count()

        gvAccounts.DataSource = dt
        gvAccounts.DataBind()


        gvAccounts.SelectedIndex = -1


    End Sub

    Protected Sub BtnReturn_Click(sender As Object, e As System.EventArgs) Handles BtnReturn.Click
        Response.Redirect("./VendorMaintenance.aspx", True)
    End Sub

    Protected Sub gvAccounts_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvAccounts.RowCommand
        If e.CommandName = "Select" Then

            Dim ClientNum As String = gvAccounts.DataKeys(Convert.ToInt32(e.CommandArgument))("client_num").ToString()

            gvAccounts.SelectedIndex = -1

            Response.Redirect("./ClientDemo.aspx?ClientNum=" & ClientNum, True)


        End If
    End Sub

    Protected Sub BtnAddVendor_Click(sender As Object, e As System.EventArgs) Handles BtnAddVendor.Click
        ' FormSignOn.AddInsertParm("@group_Num", GroupNum, SqlDbType.Int, 9)
        FormSignOn.AddInsertParm("@Vendornum", VendorNumLbl.Text, SqlDbType.Int, 9)
        FormSignOn.UpdateForm()

        Response.Redirect("./VendorMaintenance.aspx", True)

    End Sub
End Class
