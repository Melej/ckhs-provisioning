﻿Imports System.IO
Imports System.Data '
Imports System.Data.SqlClient
Imports App_Code
Imports AjaxControlToolkit
'Imports C1.Web.UI.Controls.C1Window

Partial Class CSCOwnerMaintenance
    Inherits System.Web.UI.Page
    Dim UserInfo As UserSecurity
    Dim iUserNum As Integer
    Dim iUserSecurityLevel As Integer

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        UserInfo = New UserSecurity(Session("LoginID"), Session("EmployeeConn"))

        iUserNum = UserInfo.ClientNum
        iUserSecurityLevel = UserInfo.SecurityLevelNum

        If iUserSecurityLevel < 70 Then
            Response.Redirect("~/SimpleSearch.aspx", True)
        ElseIf iUserSecurityLevel < 90 Then
            Dim ctrl As Control = DataCheckingAndValidation.SetChildControlsRecursiveToEnabledFalse(uplAppMaintenance, False)
            CSCApplicationsddl.Enabled = True
        End If


        If Not IsPostBack Then

            Dim ddlAppBinder As New DropDownListBinder

            ddlAppBinder = New DropDownListBinder(CSCApplicationsddl, "SelCSCApplicationsCodes", Session("CSCConn"))

            'ddlAppBinder.AddDDLCriteria("@AppSystem", "base", SqlDbType.NVarChar)
            'ddlAppBinder.AddDDLCriteria("@ApplicationGroup", "all", SqlDbType.NVarChar)

            ddlAppBinder.BindData("ApplicationCd", "ApplicationName")



            'Dim AppBases As List(Of Application) = New Applications().BaseApps()
            'Applicationsddl.DataSource = AppBases
            'Applicationsddl.DataValueField = "AppBase"
            'Applicationsddl.DataTextField = "ApplicationDesc"
            'Applicationsddl.DataBind()


            bindAccounts()


        End If

        ApplicationCdlbl.Text = CSCApplicationsddl.SelectedValue


    End Sub


    Protected Sub bindAddSearch()
        Dim thisData As New CommandsSqlAndOleDb("SelADDTechClients", Session("EmployeeConn"))


        If ADDLastNameTextBox.Text <> "" Then
            thisData.AddSqlProcParameter("@LastName", ADDLastNameTextBox.Text, SqlDbType.NVarChar, 30)

        Else
            Exit Sub
        End If

        thisData.AddSqlProcParameter("@allClients", "all", SqlDbType.NVarChar, 6)




        Dim dt As DataTable

        dt = thisData.GetSqlDataTable

        GvADDClient.DataSource = dt
        GvADDClient.DataBind()

        GvADDClient.Visible = True

        GvADDClient.SelectedIndex = -1



    End Sub
    Protected Sub BtnAddSearch_Click(sender As Object, e As System.EventArgs) Handles BtnAddSearch.Click
        bindAddSearch()
    End Sub

    Protected Sub GvADDClient_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GvADDClient.RowCommand
        If e.CommandName = "Select" Then
            Dim sActionType As String = "Add"

            Dim ClientNum As String = GvADDClient.DataKeys(Convert.ToInt32(e.CommandArgument))("client_num").ToString()

            Dim ClientName As String = GvADDClient.DataKeys(Convert.ToInt32(e.CommandArgument))("fullname").ToString()

            ' add client here
            ClientNumlbl.Text = ClientNum
            Clientnamelbl.Text = ClientName

            GvADDClient.SelectedIndex = -1

            GvADDClient.Visible = False
            ADDLastNameTextBox.Text = ""
            tabs.ActiveTabIndex = 1

            pancontactNames.Visible = True

            ' Response.Redirect("./CSCOwnerMaintenance.aspx", True)


        End If
    End Sub

    Protected Sub CSCApplicationsddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles CSCApplicationsddl.SelectedIndexChanged

        tabs.Visible = True
        ApplicationCdlbl.Text = CSCApplicationsddl.SelectedValue

        bindAppOwners()
        reset()

    End Sub
    Protected Sub reset()

        Clientnamelbl.Text = ""
        ClientNumlbl.Text = ""
        TechNamelbl.Text = ""
        TechNumlbl.Text = ""
        VendorNamelbl.Text = ""
        VendorNumlbl.Text = ""
        ClientTechNum.Text = ""

        If ApplicationCdlbl.Text = "" Then

            Dim ddlAppBinder As New DropDownListBinder

            ddlAppBinder = New DropDownListBinder(CSCApplicationsddl, "SelCSCApplicationsCodes", Session("CSCConn"))

            ddlAppBinder.BindData("ApplicationCd", "ApplicationName")

            CSCApplicationsddl.SelectedIndex = -1

        End If
        tabs.ActiveTabIndex = 0
    End Sub
    Protected Sub bindAccounts()


        Dim thisData As New CommandsSqlAndOleDb("SelTechContacts", Session("EmployeeConn"))

        'thisData.AddSqlProcParameter("@ntlogin", Session("LoginID"), SqlDbType.NVarChar, 50)
        'thisData.AddSqlProcParameter("@txtntlogin", Session("LoginID"), SqlDbType.NVarChar, 50)

        'If ClientNum > 0 Then
        '    thisData.AddSqlProcParameter("@client_num", ClientNum, SqlDbType.Int)

        'End If


        'If SearchLastNameTextBox.Text <> "" Then
        '    thisData.AddSqlProcParameter("@LastName", SearchLastNameTextBox.Text, SqlDbType.NVarChar, 30)


        'End If

        'If UserTypesddl.SelectedValue.ToLower <> "select" Then
        '    thisData.AddSqlProcParameter("@SecurityLevel", UserTypesddl.SelectedValue, SqlDbType.NVarChar, 8)

        'End If
        Dim dt As DataTable

        dt = thisData.GetSqlDataTable

        gvAccounts.DataSource = dt
        gvAccounts.DataBind()


        gvAccounts.SelectedIndex = -1


    End Sub

    Protected Sub gvAccounts_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvAccounts.RowCommand
        If e.CommandName = "Select" Then

            Dim ClientNum As String = gvAccounts.DataKeys(Convert.ToInt32(e.CommandArgument))("client_num").ToString()
            Dim TechNum As String = gvAccounts.DataKeys(Convert.ToInt32(e.CommandArgument))("tech_num").ToString()

            Dim TechName As String = gvAccounts.DataKeys(Convert.ToInt32(e.CommandArgument))("tech_name").ToString()

            ' add client here
            TechNumlbl.Text = TechNum
            TechNamelbl.Text = TechName
            ClientTechNum.Text = ClientNum

            gvAccounts.SelectedIndex = -1

            gvAccounts.Visible = False
            tabs.ActiveTabIndex = 2
            bindVendors()

            GvVendors.SelectedIndex = -1

            pancontactNames.Visible = True

            ' Response.Redirect("./CSCOwnerMaintenance.aspx", True)


        End If

    End Sub
    Protected Sub bindAppOwners()


        Dim thisData As New CommandsSqlAndOleDb("SelCSCApplicationSupport", Session("CSCConn"))
        thisData.AddSqlProcParameter("@ApplicationCd", CSCApplicationsddl.SelectedValue, SqlDbType.NVarChar)
        Dim dt As DataTable

        dt = thisData.GetSqlDataTable

        gvAppOwners.DataSource = dt
        gvAppOwners.DataBind()

        For Each drRow As DataRow In dt.Rows
            AppCommentsTextBox.Text = IIf(IsDBNull(drRow("Comments")), "", drRow("Comments"))
            ClientNumlbl.Text = IIf(IsDBNull(drRow("clientnum")), "", drRow("clientnum"))

            VendorNumlbl.Text = IIf(IsDBNull(drRow("VendorNum")), "", drRow("VendorNum"))
        Next

        If ClientNumlbl.Text = "" And VendorNumlbl.Text = "" Then

            gvAppOwners.Visible = False

        Else
            gvAppOwners.Visible = True

        End If
        pancontactNames.Visible = True
        gvAppOwners.SelectedIndex = -1


    End Sub

    Protected Sub tabs_ActiveTabChanged(sender As Object, e As System.EventArgs) Handles tabs.ActiveTabChanged
        If tabs.ActiveTabIndex = 1 Then
            gvAccounts.SelectedIndex = -1
            gvAccounts.Visible = True
        End If

        If tabs.ActiveTabIndex = 2 Then
            bindVendors()
            GvVendors.Visible = True
            ' LoadGroupTypes("list")
            GvVendors.SelectedIndex = -1
        End If

    End Sub
    Public Sub bindVendors()


        Dim thisData As New CommandsSqlAndOleDb("SelVendors", Session("EmployeeConn"))

        'If SearchVendorNameTextBox.Text = "" Then
        '    thisData.AddSqlProcParameter("@VendorName", "all", SqlDbType.NVarChar)
        'Else
        '    thisData.AddSqlProcParameter("@VendorName", SearchVendorNameTextBox.Text, SqlDbType.NVarChar)
        'End If
        'thisData.AddSqlProcParameter("@VendorName", "all", SqlDbType.NVarChar)
        thisData.AddSqlProcParameter("@type", "vendor", SqlDbType.NVarChar)

        Dim dt As DataTable

        dt = thisData.GetSqlDataTable

        GvVendors.DataSource = dt
        GvVendors.DataBind()


        GvVendors.SelectedIndex = -1
    End Sub


    Protected Sub GvVendors_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GvVendors.RowCommand
        If e.CommandName = "Select" Then

            Dim VendorNum As String = GvVendors.DataKeys(Convert.ToInt32(e.CommandArgument))("VendorNum").ToString()

            Dim VendorName As String = GvVendors.DataKeys(Convert.ToInt32(e.CommandArgument))("VendorName").ToString()

            VendorNumlbl.Text = VendorNum
            VendorNamelbl.Text = VendorName

            GvVendors.SelectedIndex = -1


            tabs.ActiveTabIndex = 0

            pancontactNames.Visible = True

            ' Response.Redirect("./CSCOwnerMaintenance.aspx", True)


        End If

    End Sub

    Protected Sub btnReset_Click(sender As Object, e As System.EventArgs) Handles btnReset.Click
        ApplicationCdlbl.Text = ""
        reset()

    End Sub

    Protected Sub btnSubmitContact_Click(sender As Object, e As System.EventArgs) Handles btnSubmitContact.Click

        If ClientNumlbl.Text <> "" Then

            Dim thisData As New CommandsSqlAndOleDb("InsCSCTechOwner", Session("CSCConn"))

            thisData.AddSqlProcParameter("@ApplicationCd", ApplicationCdlbl.Text, SqlDbType.NVarChar)
            thisData.AddSqlProcParameter("@client_num", ClientNumlbl.Text, SqlDbType.NVarChar)

            thisData.AddSqlProcParameter("@group_num", "0", SqlDbType.NVarChar)
            thisData.AddSqlProcParameter("@VendorNum", "0", SqlDbType.NVarChar)
            thisData.AddSqlProcParameter("@OwnerTypecd", "client", SqlDbType.NVarChar)


            thisData.ExecNonQueryNoReturn()
        End If

        If TechNumlbl.Text <> "" Then


            Dim thisData As New CommandsSqlAndOleDb("InsCSCTechOwner", Session("CSCConn"))

            thisData.AddSqlProcParameter("@ApplicationCd", ApplicationCdlbl.Text, SqlDbType.NVarChar)
            thisData.AddSqlProcParameter("@client_num", ClientTechNum.Text, SqlDbType.NVarChar)

            thisData.AddSqlProcParameter("@group_num", "0", SqlDbType.NVarChar)

            thisData.AddSqlProcParameter("@VendorNum", "0", SqlDbType.NVarChar)
            thisData.AddSqlProcParameter("@OwnerTypecd", "tech", SqlDbType.NVarChar)


            thisData.ExecNonQueryNoReturn()
        End If

        If VendorNumlbl.Text <> "" And VendorNumlbl.Text <> "0" Then

            Dim thisData As New CommandsSqlAndOleDb("InsCSCTechOwner", Session("CSCConn"))

            thisData.AddSqlProcParameter("@ApplicationCd", ApplicationCdlbl.Text, SqlDbType.NVarChar)
            thisData.AddSqlProcParameter("@client_num", "0", SqlDbType.NVarChar)
            thisData.AddSqlProcParameter("@VendorNum", VendorNumlbl.Text, SqlDbType.NVarChar)

            thisData.AddSqlProcParameter("@group_num", "0", SqlDbType.NVarChar)

            thisData.AddSqlProcParameter("@OwnerTypecd", "vendor", SqlDbType.NVarChar)

            thisData.ExecNonQueryNoReturn()
        End If


        Dim UpthisData As New CommandsSqlAndOleDb("UpdCSCApplicationCodes", Session("CSCConn"))

        UpthisData.AddSqlProcParameter("@ApplicationCd", ApplicationCdlbl.Text, SqlDbType.NVarChar)
        UpthisData.AddSqlProcParameter("@Comments", AppCommentsTextBox.Text, SqlDbType.NVarChar)
        UpthisData.ExecNonQueryNoReturn()


        '@ApplicationDesc nvarchar(500) = null,
        '@CSCAppdisplay nvarchar(3) = null,
        '@AccountQueueDisplay nvarchar(3) = null


        Response.Redirect("./CSCOwnerMaintenance.aspx", True)
        ' bindAppOwners()

    End Sub

    Protected Sub gvAppOwners_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvAppOwners.RowCommand
        If e.CommandName = "Select" Then
            Dim client_num As String = gvAppOwners.DataKeys(Convert.ToInt32(e.CommandArgument))("clientnum").ToString()
            Dim group_num As String = gvAppOwners.DataKeys(Convert.ToInt32(e.CommandArgument))("groupNum").ToString()
            Dim vendor_num As String = gvAppOwners.DataKeys(Convert.ToInt32(e.CommandArgument))("vendornum").ToString()
            Dim OwnerTypecd As String = gvAppOwners.DataKeys(Convert.ToInt32(e.CommandArgument))("OwnerTypecd").ToString()




            Dim thisData As New CommandsSqlAndOleDb("DelCSCApplicationOwner", Session("CSCConn"))
            thisData.AddSqlProcParameter("@ApplicationCd", ApplicationCdlbl.Text, SqlDbType.NVarChar)
            thisData.AddSqlProcParameter("@client_num", client_num, SqlDbType.Int)
            thisData.AddSqlProcParameter("@group_num", group_num, SqlDbType.Int)
            thisData.AddSqlProcParameter("@vendor_num", vendor_num, SqlDbType.Int)

            thisData.AddSqlProcParameter("@OwnerType", OwnerTypecd, SqlDbType.NVarChar)
            thisData.AddSqlProcParameter("@AddRemove", "Remove", SqlDbType.NVarChar)

            thisData.ExecNonQueryNoReturn()

            'Dim updEmail As New CommandsSqlAndOleDb("InsUpdEmailDistribution", Session("EmployeeConn"))
            'updEmail.AddSqlProcParameter("@AppBase", Applicationsddl.SelectedValue, SqlDbType.Int)
            'updEmail.AddSqlProcParameter("@tech_num", tech_num, SqlDbType.Int)
            'updEmail.AddSqlProcParameter("@EventID", "AddAcct", SqlDbType.NVarChar)
            'updEmail.AddSqlProcParameter("@Action", "Remove", SqlDbType.NVarChar)


            'updEmail.ExecNonQueryNoReturn()

            '  Response.Redirect("~/OwnerMaintenance.aspx", True)

            bindAppOwners()

            gvAppOwners.SelectedIndex = -1

        End If

    End Sub

    Protected Sub BtnCSCRpt_Click(sender As Object, e As System.EventArgs) Handles BtnCSCRpt.Click
        Response.Redirect("./CSCApplicationListRpt.aspx", True)
    End Sub
End Class
