﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="false" CodeFile="SearchByEmpNumber.aspx.vb" Inherits="SearchByEmpNumber" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

<asp:UpdatePanel ID="uplSearch" runat ="server" >

<Triggers>

   <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />

</Triggers>

 <ContentTemplate>
<table style="border-style: groove" align="left" width="100%">
<tr >
<td>
  <asp:Panel ID="pansearch" runat="server" Width = "100%" DefaultButton="btnSearch" >
    <table width="100%" border ="2">
      <tr>
        <th align="center" colspan="4" class="tableRowHeader">
       
           CKHS Vaccine Collection

        </th>

      </tr>
      <tr>
     
         <td align="right" style="font-weight: bold">
            Scan Employee Badge
        </td>
        <td colspan="3">
         <asp:TextBox ID = "EmployeeNumberTextBox" runat = "server"></asp:TextBox>
        </td>
        </tr>
              <tr>
     
         <td align="right" style="font-weight: bold">
            Last Name
        </td>
        <td>
         <asp:TextBox ID = "LastNameTextBox" runat = "server"></asp:TextBox>
        </td>

          <td align="right" style="font-weight: bold">
           First Name
         
        </td>
        <td>
           <asp:TextBox ID = "FirstNameTextBox" runat = "server"></asp:TextBox>
        </td>
        </tr>
      <tr>


          <td align="left" colspan="4">
              <asp:Label ID="lblValidation" runat="server" ForeColor="Blue" Visible="false">Test tool tip</asp:Label>
          </td>
      <tr>
        <td  align="center" colspan="4">
            <table>
                <tr>
                    <td align="center">

                            <asp:Button ID ="btnSearch" runat = "server" Text ="Search"  Width="120px" Height="30px"
                                 Font-Names="Arial" ForeColor="White" Font-Bold="True" Font-Size="Small" />
                    </td>
                    <td align="center">
                            <asp:Button ID = "btnNotFound" runat= "server" Text="Not Found"  Width="120px" Height="30px"
                                 Font-Names="Arial" ForeColor="White" Font-Bold="True" Font-Size="Small" />
                    
                    </td>
                    <td align="center" >

                            <asp:Button ID = "btnReset" runat= "server" Text="Reset"  Width="120px" Height="30px"
                                 Font-Names="Arial" ForeColor="White" Font-Bold="True" Font-Size="Small" />

                    </td>
                </tr>
            </table>
    
        </td>
    </tr>
      <tr>
        <td colspan ="4">
         <asp:GridView ID="gvSearch" runat="server" AllowSorting="True"  AutoGenerateColumns="False" BackColor="White" BorderColor="#336666"
              BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
              EmptyDataText="No Items Found" EnableTheming="False" DataKeyNames="client_num,VaccineNum"
              Font-Size="Medium" GridLines="Horizontal" Width="100%">
                                   
            <FooterStyle BackColor="White" ForeColor="#333333" />
            <HeaderStyle BackColor="#336666" Font-Bold="False" ForeColor="White" Font-Size="Small" HorizontalAlign="Left"    />                                    
            <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Left" Font-Size="Medium"/>
            <RowStyle BackColor="White" ForeColor="#333333" />
            <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F7F7F7" />
            <SortedAscendingHeaderStyle BackColor="#487575" />
            <SortedDescendingCellStyle BackColor="#E5E5E5" />
            <SortedDescendingHeaderStyle BackColor="#275353" />
            <Columns>
                <asp:CommandField ButtonType="Button" SelectText="Select" ShowSelectButton= "true" ControlStyle-BackColor="#84A3A3" ItemStyle-Height="30px" />
                <asp:BoundField  DataField="fullname"  HeaderText="Full Name" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" ItemStyle-Height="35px" ></asp:BoundField>
                <asp:BoundField  DataField="client_num"  HeaderText="ID" Visible="true"></asp:BoundField>

                <%--<asp:BoundField  DataField="phone"  HeaderText="Phone" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>--%>
                
                <asp:BoundField  DataField="department_name"  HeaderText="Department" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
  				<asp:BoundField  DataField="VaccineOutComeDesc"  HeaderText="Out Come" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>

				    <%-- <asp:BoundField  DataField="login_name"  HeaderText="Network Login" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>--%>
<%--                <asp:TemplateField HeaderText = "Network Login" HeaderStyle-HorizontalAlign = "Left" ItemStyle-HorizontalAlign="Left">
                   <ItemTemplate>
                          <asp:Label ID="lblLoginName" runat="server"><%# If(Eval("login_name"), "")%></asp:Label>                                                
                    </ItemTemplate>
                </asp:TemplateField>--%>
            </Columns>
        </asp:GridView>
		
        <asp:GridView ID="gvRequests" runat="server" AllowSorting="True" 
             AutoGenerateColumns="False" BackColor="White" BorderColor="#336666"
             BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
             EmptyDataText="No Items Found" EnableTheming="False" DataKeyNames="account_request_num"
             Font-Size="Medium" GridLines="Horizontal" Width="100%">
                                   
            <FooterStyle BackColor="White" ForeColor="#333333" />
            <HeaderStyle BackColor="#336666" Font-Bold="False" ForeColor="White" Font-Size="Small" HorizontalAlign="Left"    />                                    
            <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Left" Font-Size="Medium"/>
            <RowStyle BackColor="White" ForeColor="#333333" />
            <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F7F7F7" />
            <SortedAscendingHeaderStyle BackColor="#487575" />
            <SortedDescendingCellStyle BackColor="#E5E5E5" />
            <SortedDescendingHeaderStyle BackColor="#275353" />

            <Columns>
                    <asp:CommandField ButtonType="Button" SelectText="Select" ShowSelectButton= "true" ControlStyle-BackColor="#84A3A3" ItemStyle-Height="30px"  />                                                                                 
                    <asp:BoundField  DataField="fullname"  HeaderText="Full Name" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" ItemStyle-Height="35px" ></asp:BoundField>
                    <asp:BoundField  DataField="entered_date"  HeaderText="Entered Date" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
                    <asp:BoundField  DataField="RequestorFullName"  HeaderText="Requestor" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
				    <asp:BoundField  DataField="SubmitterFullName"  HeaderText="Submitter" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
				    <asp:BoundField  DataField="RequestStatus"  HeaderText="RequestStatus" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
                                  
                    
            </Columns>
         </asp:GridView>
        </td>
    </tr>
   </table>
  </asp:Panel>
 </td>
</tr>

    
    


</table>

</ContentTemplate>

</asp:UpdatePanel>
</asp:Content>

