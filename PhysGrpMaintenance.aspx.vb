﻿Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports App_Code

Imports AjaxControlToolkit
Partial Class PhysGrpMaintenance
    Inherits System.Web.UI.Page
    Dim sActivitytype As String = ""
    Dim cblBinder As New CheckBoxListBinder
    Dim UserInfo As UserSecurity
    Dim FormSignOn As New FormSQL()

    Dim GNumber As String
    Dim GroupNum As String
    Dim AccountCd As String
    Dim MaintType As String

    Dim iUserNum As Integer
    Dim iUserSecurityLevel As Integer

    Dim sGnumber As String = ""
    Dim sCommGnumber As String = ""
    Dim GType As String = ""

    Dim sInvisionNumber As String = ""
    Dim GroupNumS As String = ""
    Dim GroupTypeCd As String = ""
    Dim dt As DataTable



    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        GNumber = Request.QueryString("GNumber")
        GroupNum = Request.QueryString("GroupNum")

        If Request.QueryString("GroupType") <> "" Then
            GType = Request.QueryString("GroupType")

            GroupTypeCdTextbox.Text = Request.QueryString("GroupType")

        End If

        FormSignOn = New FormSQL(Session("EmployeeConn"), "SelPhysGroupsV5", "InsJustPhysGroupV8", "", Page)
        FormSignOn.AddSelectParm("@Groupnum", GroupNum, SqlDbType.NVarChar, 25)

        UserInfo = New UserSecurity(Session("LoginID"), Session("EmployeeConn"))

        iUserNum = UserInfo.ClientNum
        iUserSecurityLevel = UserInfo.SecurityLevelNum
        'userDepartmentCD.Text = UserInfo.DepartmentCd

        Select Case iUserSecurityLevel
            Case (81), (90), (91), (92), (93), (94), (100), (105)
                tpNewSubmitter.Visible = True
            Case Else
                Response.Redirect("~/SimpleSearch.aspx", True)

        End Select

        'If iUserSecurityLevel < 70 Then
        '    Response.Redirect("~/SimpleSearch.aspx", True)
        'ElseIf iUserSecurityLevel < 90 Or iUserSecurityLevel <> 81 Then
        '    tpNewSubmitter.Visible = False
        'ElseIf iUserSecurityLevel > 89 Or iUserSecurityLevel = 81 Then
        '    tpNewSubmitter.Visible = True

        'End If
        'GNumber = "1"

        If GroupTypesddl.SelectedValue = "" Then
            Dim ddlBinderV2 As DropDownListBinder
            ddlBinderV2 = New DropDownListBinder(GroupTypesddl, "SelPhysicianGroupTypes", Session("EmployeeConn"))
            ddlBinderV2.BindData("GroupTypeCd", "GroupTypeDesc")

        End If


        If GroupTypelist2.SelectedValue = "" Then

            Dim ddlBinderGroup2 As DropDownListBinder

            ddlBinderGroup2 = New DropDownListBinder(GroupTypelist2, "SelPhysicianGroupTypes", Session("EmployeeConn"))
            ddlBinderGroup2.BindData("GroupTypeCd", "GroupTypeDesc")


        End If

        GroupTypelist2.BackColor() = Color.Yellow
        GroupTypelist2.BorderColor() = Color.Black
        GroupTypelist2.BorderStyle() = BorderStyle.Solid
        GroupTypelist2.BorderWidth = Unit.Pixel(2)

        If Not IsPostBack Then

            'If GNumber <> "" Then

            '    tabs.ActiveTabIndex = 1

            'End If

            tabs.ActiveTabIndex = 0

            If GroupTypeCdTextbox.Text <> "" Then
                GroupTypesddl.SelectedValue = GroupTypeCdTextbox.Text
                bindAccounts()


            Else
                bindAccounts()

                ' LoadGroupTypes("list")
                GvPhysGroup.SelectedIndex = -1

            End If
        End If

    End Sub
    'Protected Sub LoadGroupTypes(ByVal sLocation As String)
    '    Dim ddlBinder As New DropDownListBinder

    '    ddlBinder = New DropDownListBinder(GroupTypesddl, "SelUserTypes", Session("EmployeeConn"))
    '    ddlBinder.AddDDLCriteria("@Client_num", Session("ClientNum"), SqlDbType.Int)
    '    ddlBinder.AddDDLCriteria("@location", sLocation, SqlDbType.NVarChar)

    '    ddlBinder.BindData("SecurityLevel", "UserType")
    'End Sub




    Protected Sub bindAccounts()

        Dim thisData As New CommandsSqlAndOleDb("SelPhysGroupsV5", Session("EmployeeConn"))

        If SearchGroupNameTextBox.Text <> "" Then
            thisData.AddSqlProcParameter("@name", SearchGroupNameTextBox.Text, SqlDbType.NVarChar)


            'Select Case groupTypeText.Text
            '    Case "ckhn"
            '        'thisData.AddSqlProcParameter("@CkhnSiteID", groupTypeText.Text, SqlDbType.NVarChar)
            '        thisData.AddSqlProcParameter("@GroupTypeCd", "gnumber", SqlDbType.NVarChar)
            '    Case "comm"
            '        thisData.AddSqlProcParameter("@CommGNumber", "comm", SqlDbType.NVarChar)

            '    Case "all"
            '        thisData.AddSqlProcParameter("@Group_num", "all", SqlDbType.NVarChar)
            '    Case "invision"
            '        thisData.AddSqlProcParameter("@Group_num", "invision", SqlDbType.NVarChar)
            '    Case "nonperfered"
            '        thisData.AddSqlProcParameter("@Group_num", "nonperfered", SqlDbType.NVarChar)
            '    Case "deactivated"
            '        thisData.AddSqlProcParameter("@Group_num", "deactivated", SqlDbType.NVarChar)

            'End Select
        ElseIf GroupTypeCdTextbox.Text <> "" Then
            thisData.AddSqlProcParameter("@GroupTypeCd", GroupTypeCdTextbox.Text, SqlDbType.NVarChar)

        End If


        'thisData.AddSqlProcParameter("@GroupID", GroupIDtextbox.Text, SqlDbType.NVarChar)

        'If SearchGroupNameTextBox.Text <> "" Then
        '    thisData.AddSqlProcParameter("@name", SearchGroupNameTextBox.Text, SqlDbType.NVarChar)
        'End If

        'If SearchLastNameTextBox.Text <> "" Then
        '    thisData.AddSqlProcParameter("@LastName", SearchLastNameTextBox.Text, SqlDbType.NVarChar, 30)


        'End If

        'If UserTypesddl.SelectedValue.ToLower <> "select" Then
        '    thisData.AddSqlProcParameter("@SecurityLevel", UserTypesddl.SelectedValue, SqlDbType.NVarChar, 8)

        'End If
        Dim dt As DataTable

        dt = thisData.GetSqlDataTable

        GvPhysGroup.DataSource = dt
        GvPhysGroup.DataBind()


        GvPhysGroup.SelectedIndex = -1

        GridHidden.DataSource = dt
        GridHidden.DataBind()

    End Sub

    Protected Sub GvPhysGroup_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GvPhysGroup.RowCommand

        If e.CommandName = "Select" Then

            GroupNumS = GvPhysGroup.DataKeys(Convert.ToInt32(e.CommandArgument))("Group_num").ToString()
            GroupTypeCd = GvPhysGroup.DataKeys(Convert.ToInt32(e.CommandArgument))("GroupTypeCd").ToString()
            GvPhysGroup.SelectedIndex = -1

            'If GroupTypeCdTextbox.Text <> "" Then
            '    Select Case GroupTypeCdTextbox.Text
            '        Case "ckhn"
            '            sGnumber = GvPhysGroup.DataKeys(Convert.ToInt32(e.CommandArgument))("GNumber").ToString()
            '        Case "comm"
            '            sCommGnumber = GvPhysGroup.DataKeys(Convert.ToInt32(e.CommandArgument))("CommGNumber").ToString()
            '        Case "invision"
            '            sInvisionNumber = GvPhysGroup.DataKeys(Convert.ToInt32(e.CommandArgument))("InvisionGroupNum").ToString()
            '    End Select
            'End If
            If GroupTypeCd.ToLower = "gnum" Then
                Response.Redirect("./CPMLocations.aspx?GroupNum=" & GroupNumS, True)

            Else

                Response.Redirect("./PhysGrpMaintDetail.aspx?GroupNum=" & GroupNumS & "&GroupTypeCd=" & GroupTypeCd, True)
            End If
            '& "&GNumber=" & sGnumber & "&CommGNumber=" & sCommGnumber & "&InvisionGroupNum=" & sInvisionNumber



        End If
    End Sub

    Protected Sub bindAddSearch()
        Dim thisData As New CommandsSqlAndOleDb("SelADDTechClients", Session("EmployeeConn"))


        'If ADDLastNameTextBox.Text <> "" Then
        '    thisData.AddSqlProcParameter("@LastName", ADDLastNameTextBox.Text, SqlDbType.NVarChar, 30)

        'Else
        '    Exit Sub
        'End If

        Dim dt As DataTable

        dt = thisData.GetSqlDataTable

        GvPhysGroup.DataSource = dt
        GvPhysGroup.DataBind()


        GvPhysGroup.SelectedIndex = -1

        GridHidden.DataSource = dt
        GridHidden.DataBind()



    End Sub



    'Protected Sub UserTypesddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles UserTypesddl.SelectedIndexChanged
    '    SecurityLevelText.Text = UserTypesddl.SelectedItem.ToString

    '    SearchLastNameTextBox.Text = ""
    'End Sub

    Protected Sub btnSearch_Click(sender As Object, e As System.EventArgs) Handles btnSearch.Click
        btnPrintAll.Visible = True

        CurrentSelectLable.Text = "Search by " & GroupTypeCdTextbox.Text.ToString

        If SearchGroupNameTextBox.Text = "" Then
            If GroupTypeCdTextbox.Text <> "" Then
                CurrentSelectLable.Text = "Search by " & GroupTypeCdTextbox.Text.ToString
            End If
        Else
            CurrentSelectLable.Text = "Search by " & SearchGroupNameTextBox.Text.ToString


        End If
        'If GroupTypeCdTextbox.Text <> "" Then
        '    Select Case GroupTypeCdTextbox.Text
        '        Case "ckhn"
        '            CurrentSelectLable.Text = "Current Selection All CKHN Groups"
        '            btnPrintAll.Visible = True
        '            btnPrintAll.Text = "Export CKHN Groups"
        '        Case "comm"
        '            CurrentSelectLable.Text = "Current Selection All Comm Groups"
        '            btnPrintAll.Visible = True
        '            btnPrintAll.Text = "Export Comm Groups"
        '        Case "invision"
        '            CurrentSelectLable.Text = "Current Selection All Invision Groups"
        '            btnPrintAll.Visible = True
        '            btnPrintAll.Text = "Export Invision Groups"
        '    End Select
        'End If

        bindAccounts()
        'SearchGroupNameTextBox.Text = ""

    End Sub

    'Protected Sub BtnAddSearch_Click(sender As Object, e As System.EventArgs) Handles BtnAddSearch.Click
    '    'bindAddSearch()
    'End Sub

    'Protected Sub GvPhysGroup_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GvPhysGroup.RowCommand
    '    If e.CommandName = "Select" Then
    '        Dim sActionType As String = "Add"

    '        Dim ClientNum As String = GvPhysGroup.DataKeys(Convert.ToInt32(e.CommandArgument))("client_num").ToString()

    '        GvPhysGroup.SelectedIndex = -1

    '        Response.Redirect("./UserMaintDetail.aspx?ClientNum=" & ClientNum & "&ActionType=" & sActionType, True)


    '    End If
    'End Sub

    Protected Sub GroupTypesddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles GroupTypesddl.SelectedIndexChanged
        GroupTypeCdTextbox.Text = GroupTypesddl.SelectedValue
        bindAccounts()

        'GroupTypesddl.SelectedValue = "Select"
        'GroupTypesddl.SelectedIndex = -1


        If GroupTypesddl.SelectedValue = "GNum" Then
            directlbl.Visible = True

            directeMailTextBox.Visible = True
        ElseIf GroupTypesddl.SelectedValue = "Coverage" Then

        End If


    End Sub

    Protected Sub btnReset_Click(sender As Object, e As System.EventArgs) Handles btnReset.Click
        btnPrintAll.Visible = False
        CurrentSelectLable.Text = "All Groups"
        GroupTypesddl.SelectedIndex = -1
        SearchGroupNameTextBox.Text = ""
        bindAccounts()

    End Sub

    Protected Sub BtnAddGroup_Click(sender As Object, e As System.EventArgs) Handles BtnAddGroup.Click
        'InsPhysGroup
        If Group_nameTextBox.Text = "" Then
            BtnAddGroup.BackColor = Color.FromName("#800000")

            lblAddgroupmsg.Text = " Must supply Group Name"
            lblAddgroupmsg.Visible = True
            Group_nameTextBox.Focus()

            Group_nameTextBox.BackColor() = Color.Yellow
            Group_nameTextBox.BorderColor() = Color.Black
            Group_nameTextBox.BorderStyle() = BorderStyle.Solid
            Group_nameTextBox.BorderWidth = Unit.Pixel(2)

            Exit Sub
        End If

        If GroupIDtextbox.Text = "" Then
            BtnAddGroup.BackColor = Color.FromName("#800000")

            lblAddgroupmsg.Text = " Must supply Group ID "
            lblAddgroupmsg.Visible = True
            GroupIDtextbox.Focus()

            GroupIDtextbox.BackColor() = Color.Yellow
            GroupIDtextbox.BorderColor() = Color.Black
            GroupIDtextbox.BorderStyle() = BorderStyle.Solid
            GroupIDtextbox.BorderWidth = Unit.Pixel(2)

            Exit Sub
        End If

        ' FormSignOn.AddInsertParm("@group_Num", GroupNum, SqlDbType.Int, 9)
        FormSignOn.UpdateForm()

        Response.Redirect("./PhysGrpMaintenance.aspx", True)

    End Sub

    Protected Sub btnPrintAll_Click(sender As Object, e As System.EventArgs) Handles btnPrintAll.Click
        'Dim thisData As New CommandsSqlAndOleDb("SelPhysGroupsV5", Session("EmployeeConn"))
        If GroupTypeCdTextbox.Text = "" And SearchGroupNameTextBox.Text = "" Then
            lblmessage.Text = "Must select a Group Type or Name to Search "
            lblmessage.Visible = "true"


            Exit Sub
        End If


        If SearchGroupNameTextBox.Text <> "" Then
            Dim thisData As New CommandsSqlAndOleDb("SelPhysGroupsV5", Session("EmployeeConn"))

            thisData.AddSqlProcParameter("@name", SearchGroupNameTextBox.Text, SqlDbType.NVarChar)
            dt = thisData.GetSqlDataTable

        ElseIf GroupTypeCdTextbox.Text <> "" Then

            Dim groupthisData As New CommandsSqlAndOleDb("SelPhysByGroups", Session("EmployeeConn"))
            groupthisData.AddSqlProcParameter("@groupTypeCd", GroupTypeCdTextbox.Text, SqlDbType.NVarChar, 12)
            '    thisData.AddSqlProcParameter("@groupNum", ADDLastNameTextBox.Text, SqlDbType.NVarChar, 30)

            dt = groupthisData.GetSqlDataTable
        End If

        Dim iRowCount = 1


        CreateExcelByTable(dt)

        'GroupNumS = ""
        'Dim sGroupType As String
        'If GroupTypeCdTextbox.Text <> "" Then

        '    Select Case GroupTypeCdTextbox.Text
        '        Case "ckhn"
        '            sGroupType = "AllGGroups"
        '        Case "comm"
        '            sGroupType = "AllCommGroups"
        '        Case "invision"
        '            sGroupType = "AllInvision"

        '    End Select
        'End If

        'Response.Redirect("./Reports/RolesReport.aspx?ReportName=PhysGroupReport" & "&GroupNum=" & GroupNumS, True)


    End Sub

    Protected Sub GroupTypelist2_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles GroupTypelist2.SelectedIndexChanged

        If GroupTypelist2.SelectedValue = "GNum" Or GroupTypelist2.SelectedValue = "CommNum" Then
            Response.Redirect("./CPMLocations.aspx", True)

            directlbl.Visible = True
            directeMailTextBox.Visible = True

        ElseIf GroupTypelist2.SelectedValue = "Coverage" Then
            Caplbl.Visible = True
            CapCount.Visible = True
        Else

            directlbl.Visible = False
            directeMailTextBox.Visible = False

        End If

        If GroupTypelist2.SelectedValue.ToLower = "select" Then
            errorlbl.Text = "Need to Identify Group type"
            errorlbl.Visible = True
        End If

        GroupTypeCdTextbox.Text = GroupTypelist2.SelectedValue

        BtnAddGroup.Enabled = True
        BtnAddGroup.BackColor = Color.FromName("#800000")

        PhysOfficepan.Enabled = True

        Group_nameTextBox.Focus()



    End Sub

    Protected Sub GvPhysGroup_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GvPhysGroup.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then  ' Check to make sure it isn't not getting header or footer

            '   Dim drv As DataRowView = CType(e.Row.Cells, DataRowView)

            Dim Validid As String = GvPhysGroup.DataKeys(e.Row.RowIndex)("Validaton")
            Dim validationlbl As Label

            validationlbl = e.Row.FindControl("validationlbl")
            If Validid = "yes" Then
                Dim sAccts As String = "Needs Validation"

                validationlbl.Attributes.Add("class", "masterTooltip")
                validationlbl.Attributes.Add("title", sAccts)
                validationlbl.ForeColor = Color.Blue
            End If

        End If


    End Sub


    Protected Sub tabs_ActiveTabChanged(sender As Object, e As System.EventArgs) Handles tabs.ActiveTabChanged
        If GroupTypeCdTextbox.Text.ToLower = "gnum" Then
            Response.Redirect("./CPMLocations.aspx", True)

        End If
    End Sub

    Protected Sub BtnAddReset_Click(sender As Object, e As System.EventArgs) Handles BtnAddReset.Click
        Response.Redirect("./PhysGrpMaintenance.aspx", True)
    End Sub
    Protected Sub btnExcel_Click(sender As Object, e As System.EventArgs) Handles btnExcel.Click
        bindAccounts()

        CreateExcel2()
    End Sub
    Sub CreateExcel2()



        Response.AddHeader("content-disposition", "attachment;filename=FileName.xls")

        Response.Charset = String.Empty
        Response.ContentType = "application/vnd.ms-excel"
        '  Response.ContentType = "application/vnd.xls"

        Dim sw As System.IO.StringWriter = New System.IO.StringWriter()

        Dim hw As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(sw)

        '===============================Create form to contain grid
        Dim frm As New HtmlForm()

        GridHidden.Visible = True

        GridHidden.Columns.RemoveAt(0)
        GridHidden.Parent.Controls.Add(frm)
        frm.Attributes("RunAt") = "server"
        frm.Controls.Add(GridHidden)

        frm.RenderControl(hw)
        Response.Write(sw.ToString())

        Response.End()

        GridHidden.Visible = False

    End Sub
    'Private Sub btnGroupPhysExcel_Click(sender As Object, e As EventArgs) Handles btnGroupPhysExcel.Click

    '    If GroupTypeCdTextbox.Text = "" Then
    '        lblmessage.Text = "Must select a Group Type "
    '        lblmessage.Visible = "true"


    '        Exit Sub
    '    End If


    '    Dim thisData As New CommandsSqlAndOleDb("SelPhysByGroups", Session("EmployeeConn"))
    '    thisData.AddSqlProcParameter("@groupTypeCd", GroupTypeCdTextbox.Text, SqlDbType.NVarChar, 12)
    '    '    thisData.AddSqlProcParameter("@groupNum", ADDLastNameTextBox.Text, SqlDbType.NVarChar, 30)

    '    Dim iRowCount = 1

    '    dt = thisData.GetSqlDataTable

    '    CreateExcelByTable(dt)

    'End Sub
    Protected Sub CreateExcelByTable(ByVal ReportTable As DataTable)
        'Dim gvData As GridView


        Session("UCCurrentMethod") = "CreateDocOrExcel()"
        ' GridView1.Visible = True
        'doesnt work with gridviews that have controls in them
        Response.ClearContent()
        Dim attachment As String = "attachment; filename=ExcelDownload.xls"

        HttpContext.Current.Response.ContentType = "application/excel"
        HttpContext.Current.Response.AddHeader("Content-disposition", attachment)


        Dim strStyle As String = "<style>.text {mso-number-format:\@; } </style>"   ' to help retain leading zeroes
        Response.Write(strStyle)
        Response.Write("<H4 align='center'>")  ' throw in a pageheader just for the heck of it to tell people what the data is and when it was run
        '  Response.Write("Parts Listing as of " & Date.Now.ToString("d"))
        Response.Write(Session("Hospital") & " Room List")
        Response.Write("</H4>")
        Response.Write("<BR>")
        'GridView1.HeaderStyle.Font.Bold = True
        'GridView1.HeaderStyle.BackColor = Drawing.Color.LightGray

        Dim sw As StringWriter = New StringWriter()
        Dim hw As HtmlTextWriter = New HtmlTextWriter(sw)
        Dim dg As New DataGrid


        ' must be name of datasource on page
        dg.DataSource = ReportTable
        Dim i As Integer = dg.Items.Count
        dg.DataBind()
        '  dg.GridLines = GridLines.None
        ' ClearControls(dg)

        dg.HeaderStyle.Font.Bold = True
        dg.HeaderStyle.BackColor = Drawing.Color.LightGray


        dg.RenderControl(hw)
        '   Dim dt As New DataTable
        ' Dim dv As New DataView

        'dv.Table.ToString()
        Response.Write(sw.ToString())
        Response.End()

        ' GridView1.AllowSorting = True
        sw = Nothing
        hw = Nothing
        dg.Dispose()
    End Sub


End Class
