﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="false" CodeFile="P1Report.aspx.vb" Inherits="Reports_P1Report" %>
<%--<%@ Register assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" namespace="System.Web.UI.DataVisualization.Charting" tagprefix="asp" %>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <style type="text/css">
        .style12
        {
            height: 30px;
            width: 518px;
        }        
        .ui-datepicker-calendar { 
        display: none; 
        } 
        .style14
        {
            font-size: medium;
        }
        .style17
        {
            width: 418px;
        }
        .style18
        {
            width: 92px;
        }
        .style19
        {
            width: 164px;
        }
        .style20
        {
            width: 145px;
        }

        </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.min.js"></script> 

<%--<asp:UpdatePanel ID = "uplPanel" runat="server">
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="btnUpdate" EventName="Click" />
    </Triggers>
</asp:UpdatePanel>
--%>
<asp:Button ID="btnExcel" runat="server" Text="Excel" height="25px" Width="100px" />
 <asp:UpdatePanel ID="uplPanel" runat ="server"  UpdateMode="Conditional">
  <ContentTemplate>

   <table id="maintbl">
<%-- Row 1 --%>
    <tr>
        <td align="center">
            <table>
                <tr align="center">
                   <td class="style14">
                        <strong>Priority 1 Report </strong>
                    </td>
                 </tr>
                  <tr>
                     <td  class="style14">
                        <asp:Label ID="lblChartTitle" runat="server" Text="Label" Visible="false"></asp:Label>
                      </td>
                  </tr>
                  <tr>
                    <td>
                    
                              <asp:RadioButtonList ID="rblCharttype" runat="server" AutoPostBack="True" 
                                RepeatDirection="Horizontal" Visible="false">
                                <asp:ListItem Selected="True">Column</asp:ListItem>
                                <asp:ListItem>Bar</asp:ListItem>
                                <asp:ListItem>Radar</asp:ListItem>
                            </asp:RadioButtonList>
                    </td>
                  
                  </tr>
              </table>
         </td>   
    </tr>
 <%-- Row 2 --%>
    <tr align="center">
       <td class="style12"  align="center">
         <table  width="375px" id="tblControls" runat="server" style="border-style: groove">
            <tr align="center" >
                <td class="style20" align="right">
                    Select a Type:
                </td>
                <td class="style19" align="left">
                    <asp:DropDownList ID="Typeddl" runat="server">
                        <asp:ListItem Value="Year">Year</asp:ListItem>
                        <asp:ListItem Value="month">Month</asp:ListItem>            
                        <asp:ListItem Value="day">Day</asp:ListItem>            
                        <asp:ListItem Value="hour">Hour</asp:ListItem>  
                        <asp:ListItem Selected="True" Value="group">Tech Group</asp:ListItem>  

                    </asp:DropDownList>

                </td>
                <td class="style20" align="right">
                    Select Year:
                </td>
                <td align="left">
                      <asp:DropDownList ID="Datesddl" runat="server">

                    </asp:DropDownList>

                </td>
             </tr>
              <tr align="center">
                <td class="style18" colspan="4">
                    <asp:Button ID="btnUpdate" runat="server" 
                        Text="Submit" height="25px" Width="100px" />
              
                </td>
            </tr>
          </table>
         <asp:Label ID="LbMonthly" runat="server"></asp:Label>
      </td>
    </tr>

 <%-- Row 5 --%>

<%--    <tr align="center">
      <td>
      
           <asp:Chart ID="Chart1" runat="server" Height="491px" Width="800px"  Visible="false">
                    <series>
                        <asp:Series Name="M">
                        </asp:Series>
                        <asp:Series Name="T">
                        </asp:Series>

                    </series>
                    <chartareas>
                        <asp:ChartArea Name="ChartArea1"> 
                        </asp:ChartArea>
                    </chartareas>
                    <legends>
                        <asp:Legend >
                        </asp:Legend>
                    </legends>
                </asp:Chart>

      </td>
   </tr>
--%> <%-- Row 6 --%>
     <tr>
      <td align="center">
        <table>
            <tr>
               <td>
<%--                        default Grid--%>

                           <asp:GridView ID="grdTotal" runat="server" 
                            AutoGenerateColumns="False" GridLines="Horizontal" CellPadding="4" BorderStyle="Double"
								BorderColor="#336666" Font-Names="Verdana" EnableTheming="False" Font-Bold="True" 
                              EnableModelValidation="True" Width="100%" DataKeyNames="entryyear,DisplayField" 
                              PageSize="100" Font-Size="Small">
								
								<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#336666"></HeaderStyle>
								<AlternatingRowStyle BackColor="LightGray" />
								<Columns>
<%--                                  <asp:TemplateField ShowHeader="False">
                                    <ItemTemplate>
                                        <asp:Button ID="Button1" Width="69px" height="25px" runat="server" CausesValidation="false" 
                                            CommandName="Select" Text="Details" BackColor = "#006666" />
                                    </ItemTemplate>
                                </asp:TemplateField>--%>

									<asp:BoundField DataField="entryYear" SortExpression="entryYear" HeaderText="Entry Year"></asp:BoundField>
									<asp:BoundField DataField="DisplayField" SortExpression="DisplayField" HeaderText="P1 Count"></asp:BoundField>

                        <%--
                                  <asp:CommandField ButtonType="Button" ShowSelectButton= "True" />

									<asp:BoundField DataField="CounsultCount" SortExpression="CounsultCount" HeaderText="Consult Count"></asp:BoundField>
									<asp:BoundField DataField="SWIssueDesc" SortExpression="SWIssueDesc" HeaderText="Consult Desc"></asp:BoundField>
									<asp:BoundField DataField="DisplayDate" SortExpression="DisplayDate" HeaderText="Date"></asp:BoundField>

									<asp:BoundField DataField="SWIssueCd" SortExpression="SWIssueCd" HeaderText="SWCD" Visible="true"></asp:BoundField>
                                    --%>

                                  </Columns>
                                <RowStyle Width="100%" />
                         </asp:GridView>


                </td>
            </tr>
            <tr>
                <td>
                        <asp:GridView ID="grdMonth" runat="server" 
                            AutoGenerateColumns="True" GridLines="Horizontal" CellPadding="4" BorderStyle="Double"
								BorderColor="#336666" Font-Names="Verdana" EnableTheming="False" Font-Bold="True" 
                              EnableModelValidation="True" Width="100%" DataKeyNames="entryyear,DisplayField" 
                              PageSize="100" Font-Size="Small" Visible="false" AllowSorting="true">
								
								<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#336666"></HeaderStyle>
								<AlternatingRowStyle BackColor="LightGray" />
								<Columns>
                                     <asp:TemplateField ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:Button ID="Button1" Width="69px" height="25px" runat="server" CausesValidation="false" 
                                                CommandName="Select" Text="Details" ControlStyle-BackColor="#84A3A3" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                    			
                                	<asp:BoundField DataField="entryYear" SortExpression="entryYear" HeaderText="Entry Year"></asp:BoundField>
									<asp:BoundField DataField="TotalCount" SortExpression="TotalCount" HeaderText="P1 Count"></asp:BoundField>
                                	<asp:BoundField DataField="DisplayField" SortExpression="DisplayField" HeaderText="Month"></asp:BoundField>

                                  </Columns>
                                <RowStyle Width="100%" />
                         </asp:GridView>

                </td>
            </tr>

            <tr>
                <td>
                        <asp:GridView ID="grdDay" runat="server" 
                            AutoGenerateColumns="True" GridLines="Horizontal" CellPadding="4" BorderStyle="Double"
								BorderColor="#336666" Font-Names="Verdana" EnableTheming="False" Font-Bold="True" 
                              EnableModelValidation="True" Width="100%" DataKeyNames="entryyear,DisplayField" 
                              PageSize="100" Font-Size="Small" Visible="false" AllowSorting="true" >
								
								<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#336666"></HeaderStyle>
								<AlternatingRowStyle BackColor="LightGray" />
								<Columns>
                                     <asp:TemplateField ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:Button ID="Button1" Width="69px" height="25px" runat="server" CausesValidation="false" 
                                                CommandName="Select" Text="Details" ControlStyle-BackColor="#84A3A3" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                    			
                                	<asp:BoundField DataField="entryYear" SortExpression="entryYear" HeaderText="Entry Year"></asp:BoundField>
									<asp:BoundField DataField="TotalCount" SortExpression="TotalCount" HeaderText="P1 Count"></asp:BoundField>
                                	<asp:BoundField DataField="DisplayField" SortExpression="DisplayField" HeaderText="Day of Week"></asp:BoundField>

                                  </Columns>
                                <RowStyle Width="100%" />
                         </asp:GridView>

                </td>
            </tr>

            <tr>
                <td>
                        <asp:GridView ID="grdHour" runat="server" 
                            AutoGenerateColumns="True" GridLines="Horizontal" CellPadding="4" BorderStyle="Double"
								BorderColor="#336666" Font-Names="Verdana" EnableTheming="False" Font-Bold="True" 
                              EnableModelValidation="True" Width="100%" DataKeyNames="entryyear,DisplayField" 
                              PageSize="100" Font-Size="Small" Visible="false" AllowSorting="true">
								
								<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#336666"></HeaderStyle>
								<AlternatingRowStyle BackColor="LightGray" />
								<Columns>
                                     <asp:TemplateField ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:Button ID="Button1" Width="69px" height="25px" runat="server" CausesValidation="false" 
                                                CommandName="Select" Text="Details" ControlStyle-BackColor="#84A3A3" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                    			
                                	<asp:BoundField DataField="entryYear" SortExpression="entryYear" HeaderText="Entry Year"></asp:BoundField>
									<asp:BoundField DataField="TotalCount" SortExpression="TotalCount" HeaderText="P1 Count"></asp:BoundField>
                                	<asp:BoundField DataField="DisplayField" SortExpression="DisplayField" HeaderText="Hour of Day"></asp:BoundField>

                                  </Columns>
                                <RowStyle Width="100%" />
                         </asp:GridView>

                </td>
            </tr>

            <tr>
                <td>
                        <asp:GridView ID="grdgroup" runat="server" 
                            AutoGenerateColumns="True" GridLines="Horizontal" CellPadding="4" BorderStyle="Double"
								BorderColor="#336666" Font-Names="Verdana" EnableTheming="False" Font-Bold="True" 
                              EnableModelValidation="True" Width="100%" DataKeyNames="entryyear,DisplayField" 
                              PageSize="100" Font-Size="Small" Visible="false" AllowSorting="true">
								
								<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#336666"></HeaderStyle>
								<AlternatingRowStyle BackColor="LightGray" />
								<Columns>
                                     <asp:TemplateField ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:Button ID="Button1" Width="69px" height="25px" runat="server" CausesValidation="false" 
                                                CommandName="Select" Text="Details" ControlStyle-BackColor="#84A3A3" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                    			
                                	<asp:BoundField DataField="entryYear" SortExpression="entryYear" HeaderText="Entry Year"></asp:BoundField>
									<asp:BoundField DataField="TotalCount" SortExpression="TotalCount" HeaderText="P1 Count"></asp:BoundField>
                                	<asp:BoundField DataField="DisplayField" SortExpression="DisplayField" HeaderText="Tech Group"></asp:BoundField>

                                  </Columns>
                                <RowStyle Width="100%" />
                         </asp:GridView>

                </td>
            </tr>

        </table>
      </td>
     </tr>
 <%-- Row 6.A --%>

        <tr>
            <td>
            </td>
        </tr>

 <%-- Row 7 --%>

     <tr>
        <td>
            <asp:Label ID="HDyear" runat="server" Visible="False"></asp:Label>
            <asp:Label ID="HDtype" runat="server" Visible="false"></asp:Label>
        </td>
     </tr>
</table>
  </ContentTemplate>

 </asp:UpdatePanel>
</asp:Content>

