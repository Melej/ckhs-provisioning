﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="false" CodeFile="MyQueue.aspx.vb" Inherits="MyQueue" %>

<%--<%@ Register assembly="System.Web.DataVisualization" namespace="System.Web.UI.DataVisualization.Charting" tagprefix="asp" %>
--%>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <style type="text/css">
        .ui-datepicker-calendar { 
        display: none; 
        } 
        .style14
        {
            font-size: medium;
        }
        .style19
        {
            width: 164px;
        }
        .style20
        {
            width: 145px;
        }
 
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

    <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="100"  >
       <ProgressTemplate>
            <div id="IMGDIV" align="center" valign="middle" runat="server" style="position:
                    absolute;left: 200px;top:150px;visibility:visible;vertical-align:middle;border-style:
                    inset;border-color:black;background-color:White;width:210px;height:75px; z-index: 999;">
            Loading Data ...
                <img src="Images/AjaxProgress.gif" /><br />
            </div>
       </ProgressTemplate>
    </asp:UpdateProgress>

    <asp:UpdatePanel ID = "uplPanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional" >
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="cblGroupAccountQueues" EventName="SelectedIndexChanged" />
                <asp:AsyncPostBackTrigger ControlID="rblShowGroup" EventName="SelectedIndexChanged" />
               

<%--            
                
                <asp:AsyncPostBackTrigger ControlID="Timer1" EventName="Tick" />
                <asp:AsyncPostBackTrigger ControlID="rblStatus" EventName="SelectedIndexChanged" />
                 --%>

            </Triggers>

   <ContentTemplate>
    <table width="100%">
        <tr>
              <th align="center" colspan="4" class="tableRowHeader">
       
               CKHS Customer Service Center Queues 

            </th>

        </tr>

        <tr>
            <td  align="center" colspan="2">
               <asp:Panel ID="pnlGroup" runat="server" Visible="true">
                  <table  id="tblControls" runat="server" width="100%" border="3">
                   <tr>
                        <td align="center">
                        <asp:Image ID="Image4" runat="server" ImageUrl="~/images/CSCturbo.png" Height="135" Width="150px" />
                        </td>
                        <td align="center" colspan="3">
                            <table>
                                <tr align="center">

                                    <td align="right">
                                        View By
                                    </td>

                                    <td align="left">
                        
                                        <asp:RadioButtonList ID="rblShowGroup" runat="server" RepeatDirection="Horizontal" AutoPostBack="true" >
                            
                                            <asp:ListItem Value="myqueue" Text="My Queue"></asp:ListItem>
                                            <asp:ListItem Value="groupqueue" Text="Group Queue"></asp:ListItem>
                                            
                                            <asp:ListItem Value="unassigned" Text="Unassigned Tickets\RFS"></asp:ListItem>
                                            <asp:ListItem Value="openp1" Text="Open P1"></asp:ListItem>
                                            <asp:ListItem Value="notech" Text="Group No Tech"></asp:ListItem>
                                            
                                        </asp:RadioButtonList>
                                
                                    </td>
                                   
                                </tr> 
                    
                                <tr>
                                
                                    <td colspan="2">
                                    
                                        <asp:Panel ID="PanView" runat="server" >
                                            <table>
                                                <tr>
                                    
                                               <td align="right">
                                                   <asp:Label ID="lblView" runat="server" Text="View Open or Closed"></asp:Label>
                                                </td>
                                                <td>
                        
                                                    <asp:RadioButtonList ID="rblStatus" runat="server" RepeatDirection="Horizontal"
                                                         AutoPostBack="false" >
                            
                                                         <asp:ListItem Value="both" Text="All"></asp:ListItem>
                                                        <asp:ListItem Value="Open" Text="Open" Selected="True"></asp:ListItem>
                                                        <asp:ListItem Value="Closed" Text="Closed"></asp:ListItem>
                                                    </asp:RadioButtonList>
                                
                                                </td>
                                    
                                    
                                                </tr>
                                
                                            </table>

                                           </asp:Panel> 

                                    </td>

                                </tr>

                            
                                <tr>

                                    <td align="right">
                                        View Tickets or RFS
                                    </td>

                                     <td>
                        
                                        <asp:RadioButtonList ID="rbltype" runat="server" RepeatDirection="Horizontal" AutoPostBack="false" >
                            
                                            <asp:ListItem Value="both" Text="Tickets & RFS " Selected="True"></asp:ListItem>
                                            <asp:ListItem Value="Tickets" Text="Tickets"></asp:ListItem>
                                            <asp:ListItem Value="RFS" Text="RFS"></asp:ListItem>
                                            <asp:ListItem Value="Projects" Text="Projects"></asp:ListItem>
                                             
                                        </asp:RadioButtonList>
                                    </td>
                                   
                                </tr>   
                                <tr>

                                   <td align="right">
                                        View Priority Type
                                    </td>
                                    <td>
                        
                                        <asp:RadioButtonList ID="rblPriority" runat="server" RepeatDirection="Horizontal"
                                             AutoPostBack="false" >
                            
                                             <asp:ListItem Value="all" Text="All" Selected="True"></asp:ListItem>
                                            <asp:ListItem Value="1" Text="1"></asp:ListItem>
                                            <asp:ListItem Value="2" Text="2"></asp:ListItem>
                                            <asp:ListItem Value="3" Text="3"></asp:ListItem>

                                        </asp:RadioButtonList>
                                
                                    </td>

                                </tr>
                                
                                <tr>
                                    <td colspan="2" align="center">
                                        <asp:Panel ID="pnlDate" runat="server">
                                        
                                        <table>
                                        
                                            <tr>
                                                    <td align="right"  width="70">
                                                        From Date:
                                                     </td>
                                                    <td class="style19" align="left">
                                                        <asp:TextBox ID="txtStartDate" Class="calanderClass" runat="server" 
                                                            Width="102px" AutoPostBack="false"></asp:TextBox>
                                                    </td>
                                                    <td align="right" width="70">
                                                        To Date:</td>
                                                    <td class="style19" align="left">
                                                       <asp:TextBox ID="txtEndDate"  Class="calanderClass" runat="server" Width="103px" 
                                                        AutoPostBack="false"></asp:TextBox>
                                                    </td>

                                            </tr>                                        
                                        
                                        </table>
                                      </asp:Panel>

                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="left">
                                    
                                        <asp:Label ID="LblLastRefresh" runat="server" Text = "" Font-Bold="True"></asp:Label>
                                    </td>
                                </tr>
                           </table>
                        
                        </td>
                    </tr>
                  </table>
                </asp:Panel>
            </td>
        </tr>

        <tr>
            <td align="center">
               <asp:Label ID="LbMonthly" runat="server" Font-Bold="True" Font-Size="Medium" ForeColor="Red"></asp:Label>
            
            </td>
            <td align="left">

                <asp:Label ID="lbTotal" runat="server" Font-Bold="True" Font-Size="Medium" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="2">
               
               <table id="tblTypeAndReset">
                <tr>
                       <td>
                             <asp:Button ID="btnSubmit" runat="server" Text="Submit" height="25px" Width="100px"  Visible="true"/>
                       
                       </td>

                       <td>
                       
                            <asp:Button ID="btnTicketDash" runat="server" Text="Ticket Dash Board " height="25px" Width="200px" />
                       
                       </td>

                      <td>

                           <asp:Button ID="btnReset" runat="server" Text="Reset" height="25px" Width="100px" />

                       </td>
                      <td>
                            <asp:Button ID="btnExcel" runat="server" Text="Excel" height="25px" Width="100px" />
                       </td> 
                </tr>
              </table>
           </td>
        </tr>

         <tr>
                <td align="left" >
                    <asp:Button ID="btnShowAll" runat="server" Text="Check All" Width="150px" />
                    
                    <%--<asp:Label ID="LblQueues" runat="server" Text="Queues" Width="150px"></asp:Label>--%>
                </td>
                <td>
                    <asp:Label ID="lblCurrentView" runat="server" Font-Bold="True" 
                        Font-Size="Medium" ForeColor="Red" ></asp:Label>
                </td>
         </tr>

         <tr>
            <td valign = "top" align="left" style="font-size: small" width="150px">

               <div style="overflow:auto; font-size: x-small;" class="subContainer" >
                    <asp:CheckBoxList ID = "cblGroupAccountQueues" runat = "server" AutoPostBack="True" Height="100%" />

               </div>
            </td>

            <td valign = "top">


                   <div style="overflow:auto;" class="subContainer" >

                    <asp:Table ID = "tblGroupAccountQueues" runat = "server" Height="100%"> 
            
                    </asp:Table>
                       <br></br>
                       <br></br>
                       <br></br>
                       <br></br>
                  </div>
            </td>
         </tr>
         <tr>
            <td colspan="2">
                   <asp:Label ID="connid" runat="server" Visible="false"></asp:Label>

            </td>
         </tr>
        <tr>
          <td colspan="2" >
            <asp:Label ID="HDRadioBt" runat="server" Visible="False"></asp:Label>
            <asp:Label ID="HDView" runat="server" Visible="false"></asp:Label>
            <asp:Label ID="hdDisplay" runat="server" Visible="false"></asp:Label>
            <asp:Label ID="hdtechnumLabel" runat="server" Visible="false"></asp:Label>
            <asp:Label ID="hdStatus" runat="server" Visible="false"></asp:Label>
            <asp:Label ID="hdType" runat="server" Visible="false"></asp:Label>
            <asp:Label ID="HDStartDate" runat="server" Visible="false"></asp:Label>
            <asp:Label ID="HDEnddate" runat="server" Visible="false"></asp:Label>
            <asp:Label ID="hdPriority" runat="server" Visible="false"></asp:Label>
            <asp:Label ID="hdGroups" runat="server" Visible="false"></asp:Label>
            <asp:Label ID="Label1" runat="server" Visible="false"></asp:Label>
        </td>
        </tr>

    </table>

  <asp:Timer ID="TimerControl2" runat="server" Enabled="true" Interval="240000"  >
         
 </asp:Timer>
   </ContentTemplate>

 </asp:UpdatePanel>
<%--    <asp:TimerControl ID="TimerControl2" runat="server" Enabled="false" Interval="240000" >
    240000         
 </asp:TimerControl>--%>
 

   <script type="text/javascript">
            $(document).ready(function () {
                setJQCalander();
                // just commented $(".calanderClass").datepick({ showOn: 'both', buttonImage: 'Images/jQueryCalendar.gif' });
                //  alert("test select"); //only works first time,dateFormat: 'MM yyyy'
            });

            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
            Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);
            function setJQCalander() {
                // User must hit submit
                $(".calanderClass").datepick({ showOn: 'both', buttonImage: 'Images/jQueryCalendar.gif' });

            }
            function EndRequestHandler(sender, args) {
                //end of async postback
                // alert("end Async" );
                setJQCalander();


            }

            function BeginRequestHandler(sender, args) {

            }
    </script>

</asp:Content>
