﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="false" CodeFile="SurveyReport.aspx.vb" Inherits="SurveyReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <script type="text/javascript">


        $(function () {

            $(':text').bind('keydown', function (e) { //on keydown for all textboxes  

                if (e.keyCode == 13) //if this is enter key  

                    e.preventDefault();

            });

        });
       

        
</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
<asp:UpdatePanel ID="uplPanel" runat ="server"  UpdateMode="Conditional">
   <ContentTemplate>
   <div id="maindiv" runat="server" align="left">
    <table  style="border-style: groove" align="left" width="100%">
        <tr align="center">
           <td class="tableRowHeader">
                  CSC Survey Report
           </td>
        </tr>
        <tr align="center">
            <td align="center">
                <table width="50%"  style="font-size: small;">
                        <tr align="center">
                            <td>
                            
                                                Survey Questions
                            </td>
                        </tr>
                        <tr>
                            <td>
                                    1 Was the Customer Service Center Analyst courteous and professional?
                            </td>
                        </tr>                

                        <tr>
                            <td>
                                    2 Was the Customer Service Center Analyst knowledgeable?
                            </td>
                        </tr>                
                        <tr>
                            <td>
                                    3 Was the event resolved in accordance with your expectations?
                            </td>
                        </tr>                
                        <tr>
                            <td>
                                    4 Was the event resolved in a timely manner?
                            </td>
                        </tr>                
                </table>
            </td>
        
        </tr>
        <tr>
            <td>
                <asp:Label  ID="errorlbl" runat="server" Visible="false">
                </asp:Label>

                <br />
            </td>
        
        </tr>

        <tr>
          <td align="left">

                       <table border="1" width="100%"  style="empty-cells:show" >
                            <tr>
                                <td align="center" colspan="4">
                                <asp:Button ID="btnExcel" runat="server" Text="Excel" height="25px" Width="100px" />
                                </td>
                            </tr>
                         <tr>
                           <td colspan="4">
                            <asp:GridView ID="GvSurveys" runat="server" AllowSorting="True" 
                           AutoGenerateColumns="False" BackColor="White" BorderColor="#336666" 
                           BorderStyle="Double" BorderWidth="3px" CellPadding="1" 
                           DataKeyNames="request_num" 
                           EmptyDataText="No Projects" 
                           EnableTheming="False" Font-Size="Medium" GridLines="Horizontal" Width="100%">
                           <FooterStyle BackColor="White" ForeColor="#333333" />
                           <HeaderStyle BackColor="#336666" Font-Bold="False" Font-Size="X-Small" 
                               ForeColor="White" HorizontalAlign="Left" VerticalAlign="Middle" />
                           <PagerStyle BackColor="#336666" Font-Size="Medium" ForeColor="White" 
                               HorizontalAlign="Left" />
                           <RowStyle BackColor="White" ForeColor="#333333" Font-Size="X-Small" />
                           <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                           <SortedAscendingCellStyle BackColor="#F7F7F7" />
                           <SortedAscendingHeaderStyle BackColor="#487575" />
                           <SortedDescendingCellStyle BackColor="#E5E5E5" />
                           <SortedDescendingHeaderStyle BackColor="#275353" />
                           <Columns>

                               <asp:CommandField ButtonType="Button" ControlStyle-BackColor="#84A3A3" 
                                      HeaderStyle-HorizontalAlign="Left" ItemStyle-Height="20px" SelectText="Select" 
                                      ShowSelectButton="true">
                                  <ControlStyle BackColor="#84A3A3" />
                                  <HeaderStyle HorizontalAlign="Left" />
                                  <ItemStyle Height="20px" />
                                  </asp:CommandField>
                            
                            	  


                               <asp:BoundField DataField="EntryDate"  SortExpression="EntryDate" ItemStyle-HorizontalAlign="Left" HeaderText="Entry Date" />

                               <asp:BoundField DataField="CloseDate" SortExpression="CloseDate" ItemStyle-HorizontalAlign="Center" HeaderText="Close Date" />

                               <asp:BoundField DataField="CloseTechName" SortExpression="CloseTechName" ItemStyle-HorizontalAlign="Center" HeaderText="Tech Name" />

                               <asp:BoundField DataField="TotalTime" SortExpression="TotalTime" ItemStyle-HorizontalAlign="Center" HeaderText="Total Time" />
                               <asp:BoundField DataField="Response1Desc" SortExpression="Response1Desc" ItemStyle-HorizontalAlign="Center" HeaderText="Response 1" />

                               <asp:BoundField DataField="Response2Desc" SortExpression="Response2Desc" ItemStyle-HorizontalAlign="Center" HeaderText="Response 2" />

                               <asp:BoundField DataField="Response3Desc" SortExpression="Response3Desc" ItemStyle-HorizontalAlign="Center" HeaderText="Response 3" />

                               <asp:BoundField DataField="Response4Desc" SortExpression="Response4Desc" ItemStyle-HorizontalAlign="Center" HeaderText="Response 4" />
                               <asp:BoundField DataField="CommentsYes" SortExpression="CommentsYes" ItemStyle-HorizontalAlign="Center" HeaderText="Comments" />

                               <asp:BoundField DataField="ShortDesc" SortExpression="ShortDesc" ItemStyle-HorizontalAlign="Center"  HeaderText="Short Desc" ItemStyle-Width="20%" />

                           </Columns>
                       </asp:GridView>                            
                           </td>
                         </tr>
                        </table>
        </td>
     </tr>
  </table>

  </div>
   </ContentTemplate>
  </asp:UpdatePanel>
</asp:Content>

