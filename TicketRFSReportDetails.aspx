﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="false" CodeFile="TicketRFSReportDetails.aspx.vb" Inherits="TicketRFSReportDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
  <style type="text/css">
        .ui-datepicker-calendar { 
        display: none; 
        } 
        .style19
        {
            width: 164px;
        }
         
  </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

    <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="100"  >
       <ProgressTemplate>
            <div id="IMGDIV"  align="center" valign="middle" runat="server" style="position:
                    absolute;left: 200px;top:75px;visibility:visible;vertical-align:middle;border-style:
                    inset;border-color:black;background-color:White;width:210px;height:75px; z-index: 999;" >
            Loading Data ...
                <img src="Images/AjaxProgress.gif" /><br />
            </div>
       </ProgressTemplate>
    </asp:UpdateProgress>
        <table>
            <tr>
            <td align="center" colspan="2">
               
               <table id="tblTypeAndReset">
                <tr>
                       <td>
                             <asp:Button ID="btnSubmit" runat="server" Text="Submit" height="25px" Width="100px" />
                       
                       </td>

                      <td>
                            <asp:Button ID="btnExcel" runat="server" Text="Excel" height="25px" Width="100px" />
                       </td> 
                       <td>
                            <asp:Button ID="btnDashboard" runat="server" Text ="Ticket Dashboard" height="25px" Width="130px" />

                       </td>

                      <td>
                           <asp:Button ID="btnReset" runat="server" Text="Reset" height="25px" Width="100px" />

                       </td>

                </tr>
              </table>
           </td>
        </tr>
        </table>

    <asp:UpdatePanel ID = "uplPanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional" >
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="cblGroupAccountQueues" EventName="SelectedIndexChanged" />

            </Triggers>

   <ContentTemplate>
    <table width="100%">
        <tr>
          <td align="center" width="100%" colspan="2">
              <strong>Tickets and RFS Report</strong>

            </td>   
        </tr>
        <tr>
            <td align="center" width="100%" colspan="2">
    
                        <asp:Label ID="lbDateType" runat="server" Font-Bold="True" 
                        text=""
                        Font-Size="Medium" ForeColor="#000099" ></asp:Label>            
            </td>
        </tr>
        <tr>
            <td width="100%" colspan="2" align="center">
                <table  id="tblControls" runat="server" width="69%" border="3">
                    <tr>
                        <td align="right"  width="70">
                            From Date:
                         </td>
                        <td class="style19" align="left">
                            <asp:TextBox ID="txtStartDate" Class="calanderClass" runat="server" 
                                Width="102px" AutoPostBack="false" ToolTip="Choose date when Ticket\RFS was entered" ></asp:TextBox>
                        </td>
                        <td align="right" width="70">
                            To Date:</td>
                        <td class="style19" align="left">
                           <asp:TextBox ID="txtEndDate"  Class="calanderClass" runat="server" Width="103px" 
                           AutoPostBack="false" ToolTip="Choose date when Ticket\RFS was entered" ></asp:TextBox>
                        </td>
                    </tr>

                    <tr>
                        <td align="center" colspan="4">
                            <table>

<%--                                  <asp:UpdatePanel ID = "UpdatePanel1" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional" >
                                     <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="rbltype" EventName="SelectedIndexChanged" />        
                                            <asp:AsyncPostBackTrigger ControlID="rblStatus" EventName="SelectedIndexChanged" />
                                    </Triggers>

                                 <ContentTemplate>--%>
                                 <tr>
                                    <td align="right">
                                    
                                        View by Entry Date or Closed Date:
                                    </td>
                                    <td align="left">
                                    
                                                                   
                                        <asp:RadioButtonList ID="rblDateType" runat="server" RepeatDirection="Horizontal" AutoPostBack="false" >
                            
                                            
                                            <asp:ListItem Value="entry" Selected="True" Text="Entry Date"></asp:ListItem>
                                            <asp:ListItem Value="close" Text="Close Date"></asp:ListItem>
                                             
                                        </asp:RadioButtonList>
                                
                                    </td>
                             
                                 </tr>
                                <tr>

                                   <td align="right">
                                        View Open or Closed
                                    </td>
                                    <td>
                        
                                        <asp:RadioButtonList ID="rblStatus" runat="server" RepeatDirection="Horizontal"
                                             AutoPostBack="false" >
                            
                                             <asp:ListItem Value="both" Text="All"></asp:ListItem>
                                            <asp:ListItem Value="Open" Text="Open"></asp:ListItem>
                                            <asp:ListItem Value="Closed" Text="Closed"></asp:ListItem>
                                        </asp:RadioButtonList>
                                
                                    </td>

                                </tr>
                            
                                <tr>

                                    <td align="right">
                                        View Tickets or RFS
                                    </td>

                                     <td>
                        
                                        <asp:RadioButtonList ID="rbltype" runat="server" RepeatDirection="Horizontal" AutoPostBack="false" >
                            
                                            <asp:ListItem Value="both" Text="All"></asp:ListItem>
                                            <asp:ListItem Value="Tickets" Text="Tickets"></asp:ListItem>
                                            <asp:ListItem Value="RFS" Text="RFS"></asp:ListItem>
                                             
                                        </asp:RadioButtonList>
                                
                                    </td>
                                   
                                </tr>   
                                <tr>
                                    <td align="right">
                                        Priority Level:
                                    </td>
                                    <td align="left">
                                    
                                        <asp:RadioButtonList ID="rblP1" runat="server" RepeatDirection="Horizontal" AutoPostBack="false">
                                        
                                            <asp:ListItem  Value="all" Selected="True" Text="All"></asp:ListItem>
                                            <asp:ListItem  Value="1" Text="P1"></asp:ListItem>
                                            <asp:ListItem  Value="2" Text="P2"></asp:ListItem>
                                            <asp:ListItem  Value="3" Text="P3"></asp:ListItem>

                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>

                                   <td align="right">
                                        View by How Many Groups Assigned
                                    </td>
                                    <td align="left">
                        
                                        <asp:RadioButtonList ID="rblPriority" runat="server" RepeatDirection="Horizontal"
                                             AutoPostBack="false" >
                            
                                             <asp:ListItem Value="all" Text="All" Selected="True"></asp:ListItem>
                                            <asp:ListItem Value="1" Text="Level 1"></asp:ListItem>
                                            <asp:ListItem Value="2" Text="Level 2"></asp:ListItem>
                                            <asp:ListItem Value="3" Text="Level 3"></asp:ListItem>
                                            <asp:ListItem Value="4" Text="Level 4+"></asp:ListItem>

                                        </asp:RadioButtonList>
                                
                                    </td>

                                </tr>
                                <tr>
                                    <td align="right">
                                        View by Who Touched First:
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="rblFirstTouch" runat="server" RepeatDirection="Horizontal"
                                            Autopostback="false">
                                            
                                            <asp:ListItem Value="all" Text="All" Selected="True"></asp:ListItem>
                                            <asp:ListItem Value="r4" Text="R4 Staff"></asp:ListItem>
                                            <asp:ListItem Value="crozer" Text="Crozer"></asp:ListItem>
                                            <asp:ListItem Value="client" Text="Client"></asp:ListItem>
                                           
                                           </asp:RadioButtonList>
                                    
                                    </td>
                                </tr>

                                <tr>
                                        <td align="right">
                                            View By Category
                                        
                                        </td>
                                       <td align="left">
                                               <asp:DropDownList ID="categorycdddl" runat="server" AutoPostBack="false">
                                               </asp:DropDownList>
                                       </td>    
                                </tr>

                                
<%--                               </ContentTemplate>

                               </asp:UpdatePanel>--%>
                         
                            </table>
                        
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center">
               <asp:Label ID="LbMonthly" runat="server" Font-Bold="True" Font-Size="Medium" ForeColor="Red"></asp:Label>
            
            </td>
            <td align="center">

                <asp:Label ID="lbTotal" runat="server" Font-Bold="True" Font-Size="Medium" ForeColor="Red"></asp:Label>
            </td>
        </tr>

         <tr>
                <td align="left" >
                    <asp:Button ID="btnShowAll" runat="server" Text="Select All Groups" Width="150px" />
                </td>
                <td>
                    <asp:Label ID="lblCurrentView" runat="server" Font-Bold="True" 
                        Font-Size="Medium" ForeColor="Red" ></asp:Label>
                </td>
         </tr>

         <tr>
            <td valign = "top" align="left" style="font-size: small">

               <div style="overflow:auto;" class="subContainer" >
                    <asp:CheckBoxList ID = "cblGroupAccountQueues" runat = "server" />

               </div>
            </td>

            <td valign = "top">


                   <div style="overflow:auto;" class="subContainer" >

                    <asp:Table ID = "tblGroupAccountQueues" runat = "server" Width="100%"> 
            
                    </asp:Table>
                
               
                    
<%--                       <br></br>
                       <br></br>
                       <br></br>
                       <br></br>
                       <br></br>
                       <br></br>
                       <br></br>
                       <br></br>--%>

                  </div>
            </td>
         </tr>

        <tr>
          <td colspan="2" >
            <asp:Label ID="HDRadioBt" runat="server" Visible="False"></asp:Label>
            <asp:Label ID="HDView" runat="server" Visible="false"></asp:Label>
            <asp:Label ID="hdDisplay" runat="server" Visible="false"></asp:Label>
            <asp:Label ID="hdtechnumLabel" runat="server" Visible="false"></asp:Label>
            <asp:Label ID="hdStatus" runat="server" Visible="false"></asp:Label>
            <asp:Label ID="hdType" runat="server" Visible="false"></asp:Label>
            <asp:Label ID="HDStartDate" runat="server" Visible="false"></asp:Label>
            <asp:Label ID="HDEnddate" runat="server" Visible="false"></asp:Label>
            <asp:Label ID="hdGroups" runat="server" Visible="false"></asp:Label>
            <asp:Label ID="hdPriority" runat="server" Visible="false"></asp:Label>
            <asp:Label ID="HDSecurityLevelLabel" runat="server" Visible="false"></asp:Label>
            <asp:Label ID="HDDatetype" runat="server" Visible="false"></asp:Label>
            <asp:Label ID="HDCategory" runat="server" Visible="false"></asp:Label>
            <asp:Label ID="HDTouch" runat="server" Visible="false"></asp:Label>
            <asp:Label ID="HDPriorityLevel" runat="server" Visible="false"></asp:Label>

            <asp:Label ID="HDSortDirection" runat="server" Visible="false"></asp:Label>

        </td>
        </tr>
        <tr>
            <td colspan="2">
            
                  <asp:GridView ID="GridView1"  runat="server" 
                     BackColor="White" BorderColor="#7eb5b5"  Visible="false"
                     BorderStyle="None"
                      EmptyDataText="No Data Found"  EnableTheming="false" 
                    AllowSorting="True"  EnableModelValidation="True"  Font-Size="Smaller"
                    AutoGenerateColumns="true" 
                     Font-Bold="True" DataKeyNames="request_num">

                    <FooterStyle BackColor="White" ForeColor="#333333" />
                    <HeaderStyle BackColor="#7eb5b5" Font-Bold="False" ForeColor="White" Font-Size="Small" HorizontalAlign="Left"    />                                    
                    <PagerStyle BackColor="#7eb5b5" ForeColor="White" HorizontalAlign="Left" Font-Size="Medium"/>

                         <Columns>


                        
                   </Columns> 
                </asp:GridView>
            </td>
        </tr>


    </table>

   </ContentTemplate>
  </asp:UpdatePanel>

<%--  <asp:TimerControl ID="TimerControl2" runat="server" Enabled="false" Interval="240000" >
    240000         
 </asp:TimerControl>--%>
 <%--<asp:Timer ID="Timer1" runat="server"  Enabled="True" Interval="240000">

 </asp:Timer>
 --%>   <script type="text/javascript">
            $(document).ready(function () {
                setJQCalander();
                // just commented $(".calanderClass").datepick({ showOn: 'both', buttonImage: 'Images/jQueryCalendar.gif' });
                //  alert("test select"); //only works first time,dateFormat: 'MM yyyy'
            });

            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
            Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);
            function setJQCalander() {
                // User must hit submit
                $(".calanderClass").datepick({ showOn: 'both', buttonImage: 'Images/jQueryCalendar.gif' });

            }
            function EndRequestHandler(sender, args) {
                //end of async postback
                // alert("end Async" );
                setJQCalander();


            }

            function BeginRequestHandler(sender, args) {

            }
    </script>
</asp:Content>

