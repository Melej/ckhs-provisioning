﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="false" CodeFile="P1ReportDetail.aspx.vb" Inherits="P1ReportDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <style type="text/css">
        .style12
        {
            height: 30px;
            width: 518px;
        }        
        .ui-datepicker-calendar { 
        display: none; 
        } 
        .style14
        {
            font-size: medium;
        }
        .style17
        {
            width: 418px;
        }
        .style18
        {
            width: 92px;
        }
        .style19
        {
            width: 164px;
        }
        .style20
        {
            width: 145px;
        }

        </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

<table id="maintbl">
<%-- Row 1 --%>

    <tr>
        <td>
            <table>
                <tr>
                   <td class="style14">
                        <strong>Detail Report for Priority Tickets</strong>
                    </td>
                 </tr>
                  <tr>
                     <td  class="style14">
                        <asp:Label ID="lblChartTitle" runat="server" Text="Label" Visible="false"></asp:Label>
                      </td>
                  </tr>
              </table>
         </td>   
    </tr>
 <%-- Row 2 --%>
    <tr>
       <td class="style12" >
         <table  id="tblControls" runat="server">
            <tr>
                <td class="style20">
                    
                </td>
                <td class="style19">
                         <asp:Button ID="BtnReturn" runat="server" 
                            Text="Return" height="25px" Width="100px" />

                </td>
                <td class="style18">
                    <asp:Button ID="btnPrnt" runat="server" 
                        Text="Print" height="25px" Width="100px" Visible="false" />
              
                </td>

            </tr>
          </table>
      </td>
    </tr>
<%-- Row 3 --%>

 <%-- Row 4 --%>
             <tr>
                <td width="100%">
                        <asp:GridView ID="grdMonth" runat="server" 
                            AutoGenerateColumns="False" GridLines="Horizontal" CellPadding="4" BorderStyle="Double"
								BorderColor="#336666" Font-Names="Verdana" EnableTheming="False" Font-Bold="True" 
                              EnableModelValidation="True" Width="100%" DataKeyNames="Requestnum" 
                              PageSize="100" Font-Size="Small" Visible="false" >
								
								<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#336666"></HeaderStyle>
								<AlternatingRowStyle BackColor="LightGray" />
								<Columns>
                                     <asp:TemplateField ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:Button ID="Button1" Width="69px" height="25px" runat="server" CausesValidation="false" 
                                                CommandName="Select" Text="Details" ControlStyle-BackColor="#84A3A3" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                    			
                                	<asp:BoundField DataField="EntryDate" SortExpression="EntryDate" HeaderText="Entry Date"></asp:BoundField>
									<asp:BoundField DataField="EntryDayOfWeek" SortExpression="EntryDayOfWeek" HeaderText="Day of Wk."></asp:BoundField>
                                	<asp:BoundField DataField="HourofDay" SortExpression="HourofDay" HeaderText="Hour"></asp:BoundField>
                                	<asp:BoundField DataField="Group_name" SortExpression="Group_name" HeaderText="Group"></asp:BoundField>
                                	<asp:BoundField DataField="CategoryCd" SortExpression="CategoryCd" HeaderText="Category"></asp:BoundField>
                                	<asp:BoundField DataField="TypeCd" SortExpression="TypeCd" HeaderText="Type"></asp:BoundField>


                                  </Columns>
                                <RowStyle Width="100%" />
                         </asp:GridView>

                </td>
            </tr>

            <tr>
                <td width="100%">
                        <asp:GridView ID="grdDay" runat="server" 
                            AutoGenerateColumns="False" GridLines="Horizontal" CellPadding="4" BorderStyle="Double"
								BorderColor="#336666" Font-Names="Verdana" EnableTheming="False" Font-Bold="True" 
                              EnableModelValidation="True" Width="100%" DataKeyNames="Requestnum" 
                              PageSize="100" Font-Size="Small" Visible="false" >
								
								<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#336666"></HeaderStyle>
								<AlternatingRowStyle BackColor="LightGray" />
								<Columns>
                                     <asp:TemplateField ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:Button ID="Button1" Width="69px" height="25px" runat="server" CausesValidation="false" 
                                                CommandName="Select" Text="Details" ControlStyle-BackColor="#84A3A3" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                    			
                                   	<asp:BoundField DataField="EntryDate" SortExpression="EntryDate" HeaderText="Entry Date"></asp:BoundField>
                                	<asp:BoundField DataField="EntryMonthName" SortExpression="EntryMonthName" HeaderText="Month"></asp:BoundField>

                                	<asp:BoundField DataField="HourofDay" SortExpression="HourofDay" HeaderText="Hour"></asp:BoundField>
                                	<asp:BoundField DataField="Group_name" SortExpression="Group_name" HeaderText="Group"></asp:BoundField>
                                	<asp:BoundField DataField="CategoryCd" SortExpression="CategoryCd" HeaderText="Category"></asp:BoundField>
                                	<asp:BoundField DataField="TypeCd" SortExpression="TypeCd" HeaderText="Type"></asp:BoundField>

                                  </Columns>
                                <RowStyle Width="100%" />
                         </asp:GridView>

                </td>
            </tr>

            <tr>
                <td width="100%">
                        <asp:GridView ID="grdHour" runat="server" 
                            AutoGenerateColumns="False" GridLines="Horizontal" CellPadding="4" BorderStyle="Double"
								BorderColor="#336666" Font-Names="Verdana" EnableTheming="False" Font-Bold="True" 
                              EnableModelValidation="True" Width="100%" DataKeyNames="Requestnum" 
                              PageSize="100" Font-Size="Small" Visible="false" >
								
								<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#336666"></HeaderStyle>
								<AlternatingRowStyle BackColor="LightGray" />
								<Columns>
                                     <asp:TemplateField ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:Button ID="Button1" Width="69px" height="25px" runat="server" CausesValidation="false" 
                                                CommandName="Select" Text="Details" ControlStyle-BackColor="#84A3A3" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                   	<asp:BoundField DataField="EntryDate" SortExpression="EntryDate" HeaderText="Entry Date"></asp:BoundField>
                                	<asp:BoundField DataField="EntryMonthName" SortExpression="EntryMonthName" HeaderText="Month"></asp:BoundField>

									<asp:BoundField DataField="EntryDayOfWeek" SortExpression="EntryDayOfWeek" HeaderText="Day of Wk."></asp:BoundField>

                                	<asp:BoundField DataField="Group_name" SortExpression="Group_name" HeaderText="Group"></asp:BoundField>
                                	<asp:BoundField DataField="CategoryCd" SortExpression="CategoryCd" HeaderText="Category"></asp:BoundField>
                                	<asp:BoundField DataField="TypeCd" SortExpression="TypeCd" HeaderText="Type"></asp:BoundField>
                    			

                                  </Columns>
                                <RowStyle Width="100%" />
                         </asp:GridView>

                </td>
            </tr>

            <tr>
                <td width="100%">
                        <asp:GridView ID="grdgroup" runat="server" 
                            AutoGenerateColumns="False" GridLines="Horizontal" CellPadding="4" BorderStyle="Double"
								BorderColor="#336666" Font-Names="Verdana" EnableTheming="False" Font-Bold="True" 
                              EnableModelValidation="True" Width="100%" DataKeyNames="Requestnum" 
                              PageSize="100" Font-Size="Small" Visible="false" >
								
								<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#336666"></HeaderStyle>
								<AlternatingRowStyle BackColor="LightGray" />
								<Columns>
                                     <asp:TemplateField ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:Button ID="Button1" Width="69px" height="25px" runat="server" CausesValidation="false" 
                                                CommandName="Select" Text="Details" ControlStyle-BackColor="#84A3A3" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                    			

                                   	<asp:BoundField DataField="EntryDate" SortExpression="EntryDate" HeaderText="Entry Date"></asp:BoundField>
                                	<asp:BoundField DataField="EntryMonthName" SortExpression="EntryMonthName" HeaderText="Month"></asp:BoundField>

									<asp:BoundField DataField="EntryDayOfWeek" SortExpression="EntryDayOfWeek" HeaderText="Day of Wk."></asp:BoundField>
                                	<asp:BoundField DataField="HourofDay" SortExpression="HourofDay" HeaderText="Hour"></asp:BoundField>
                                	<asp:BoundField DataField="CategoryCd" SortExpression="CategoryCd" HeaderText="Category"></asp:BoundField>
                                	<asp:BoundField DataField="TypeCd" SortExpression="TypeCd" HeaderText="Type"></asp:BoundField>

                                  </Columns>
                                <RowStyle Width="100%" />
                         </asp:GridView>

                </td>
            </tr>

</table>

</asp:Content>

