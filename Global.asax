<%@ Application Language="VB" %>
<%@ Import  namespace="System.Web.Caching" %>
<%@ Import  namespace="System.Net" %>
<%@ Import Namespace="App_Code" %>

<script runat="server">

    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs on application startup
    End Sub

    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs on application shutdown
    End Sub

    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when an unhandled error occurs
    End Sub


    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)

        Session("loginid") = ""
        Session("Currfile") = ""
        Session("ReportName") = ""
        Session("CSCConn") = ""
        Session("CSCIN1Conn") = ""
        Session("VaccineConn") = ""
        Session("EmployeeConn") = ""
        Session("technum") = ""
        Session("ClientNum") = ""
        Session("SecurityLevelNum") = ""
        Session("Usertype") = ""
        Session("environment") = ""
        Session("path") = ""
        Session("CSCNumber") = ""
        Session("ShowAll") = ""
        Session("cscqueue") = ""

        Session("MyQueue") = ""
        Session("NoTech") = ""
        Session("P1Queue") = ""

        Session("ShowAll") = ""
        Session("RPTStartDate") = ""
        Session("RPTEndDate") = ""
        Session("RPTGroups") = ""
        Session("RPTStatus") = ""
        Session("RPTType") = ""
        Session("RPTPriority") = ""
        Session("RPTCategory") = ""
        Session("RPTPriLevel") = ""
        Session("RPTTouch") = ""
        Session("RPTDateType") = ""

        Session("PageError") = "New Session No Error"
        Session("LeftNavSelectedButton") = ""
        Session("ViewRequestType") = ""
        Session("ViewAccountRequestType") = ""
        Session("PreviousSearchCriteria") = ""

        Dim sRootAndServer As String = ""
        Dim dt As New DataTable

        '  dsSecurity.Tables.Clear()
        Dim sWebRoot As String = ""
        Dim sServer As String = ""
        Dim conn As New SetDBConnection()
        Dim Envoirn As New Environment()


        'GetHostEntry(HttpContext.Current.Request.ServerVariables.Item("REMOTE_HOST))

        Session("EmployeeConn") = conn.EmployeeConn

        Session("VaccineConn") = conn.VaccineConn

        Session("environment") = Envoirn.getEnvironment


        sServer = Request.ServerVariables("SERVER_NAME")
        Session("Server") = sServer

        '====== To turn on Production for testing ==========
        ''===== Uncomment next line ======================
        'sServer = "casemgmt02"
        'Session("environment") = "Production"


        ' Production Data and web site
        'HttpContext.Current.Session("EmployeeConn") = ConfigurationManager.AppSettings.Get("ProductionEmployee.ConnectionString")
        'HttpContext.Current.Session("CSCConn") = ConfigurationManager.AppSettings.Get("ProductionCSC.ConnectionString")
        'HttpContext.Current.Session("CSCIN1Conn") = ConfigurationManager.AppSettings.Get("ProductionCSCIN1.ConnectionString")

        Dim sPath As String = Request.ServerVariables("PATH_TRANSLATED")
        Dim sURL As String = Request.ServerVariables("URL")



        If sServer = ConfigurationManager.AppSettings.Get("ProdWebServers") Then
            ' Production Data and web site
            If Not sPath.ToString.ToLower.IndexOf("test") = -1 Then
                ' Test directory on CASEMGMT02
                HttpContext.Current.Session("EmployeeConn") = ConfigurationManager.AppSettings.Get("ProductionEmployee.ConnectionString")
                HttpContext.Current.Session("CSCConn") = ConfigurationManager.AppSettings.Get("ProductionCSC.ConnectionString")


                'HttpContext.Current.Session("EmployeeConn") = ConfigurationManager.AppSettings.Get("TestProdServerEmployee.ConnectionString")
                'HttpContext.Current.Session("CSCConn") = ConfigurationManager.AppSettings.Get("TestProdServerCSC.ConnectionString")
                'HttpContext.Current.Session("CSCIN1Conn") = ConfigurationManager.AppSettings.Get("TestCSC.ConnectionString")

                Session("path") = "test"
            Else
                ' Production
                HttpContext.Current.Session("EmployeeConn") = ConfigurationManager.AppSettings.Get("ProductionEmployee.ConnectionString")
                HttpContext.Current.Session("CSCConn") = ConfigurationManager.AppSettings.Get("ProductionCSC.ConnectionString")
                'HttpContext.Current.Session("CSCIN1Conn") = ConfigurationManager.AppSettings.Get("ProductionCSCIN1.ConnectionString")

                Session("path") = "prod"
            End If

        ElseIf sServer = ConfigurationManager.AppSettings.Get("TestWebServers") Then
            ' Test Site and test data
            HttpContext.Current.Session("EmployeeConn") = ConfigurationManager.AppSettings.Get("TestEmployee.ConnectionString")
            HttpContext.Current.Session("CSCConn") = ConfigurationManager.AppSettings.Get("TestCSC.ConnectionString")
            'HttpContext.Current.Session("CSCIN1Conn") = ConfigurationManager.AppSettings.Get("TestCSC.ConnectionString")
            Session("path") = "test"

        ElseIf sServer = "localhost" Then ' ConfigurationManager.AppSettings.Get("LocalhostWebServers") Then
            ' Test Site and test data
            HttpContext.Current.Session("EmployeeConn") = ConfigurationManager.AppSettings.Get("TestEmployee.ConnectionString")
            HttpContext.Current.Session("CSCConn") = ConfigurationManager.AppSettings.Get("TestCSC.ConnectionString")
            ' HttpContext.Current.Session("CSCIN1Conn") = ConfigurationManager.AppSettings.Get("TestCSC.ConnectionString")
            Session("path") = "test"

        Else
            ' else default to test
            'HttpContext.Current.Session("EmployeeConn") = ConfigurationManager.AppSettings.Get("TestEmployee.ConnectionString")
            'HttpContext.Current.Session("CSCConn") = ConfigurationManager.AppSettings.Get("TestCSC.ConnectionString")
            'HttpContext.Current.Session("CSCIN1Conn") = ConfigurationManager.AppSettings.Get("TestCSC.ConnectionString")
            'Session("path") = "test"


            ' Production
            HttpContext.Current.Session("EmployeeConn") = ConfigurationManager.AppSettings.Get("ProductionEmployee.ConnectionString")
            HttpContext.Current.Session("CSCConn") = ConfigurationManager.AppSettings.Get("ProductionCSC.ConnectionString")
            'HttpContext.Current.Session("CSCIN1Conn") = ConfigurationManager.AppSettings.Get("ProductionCSCIN1.ConnectionString")

            Session("path") = "prod"

        End If

        Session("WebServer") = sServer

        Dim sDomainAndLogin As String = Request.ServerVariables("REMOTE_USER") 'Request.Url.AbsolutePath()
        Dim arrDomainAndLogin As String() = Split(sDomainAndLogin, "\")

        Session("loginid") = arrDomainAndLogin(1)

        'Session("loginid") = "WeisenbD"
        'Session("loginid") = "kena04"


        Dim Security As New UserSecurity(Session("LoginID"), Session("EmployeeConn"))

        Session("objUserSecurity") = Security

        Session("ClientNum") = Security.ClientNum
        Session("TechClientNum") = Security.TechNum
        Session("UserName") = Security.UserName
        Session("SecurityLevelNum") = Security.SecurityLevelNum
        Session("Usertype") = Security.UserType
        ' Session("Usertype") = "ProviderReqestors"


        Session("AppMode") = "CSC"
        Session("AppName") = "Customer Service Center"
        Session("StartTime") = DateTime.Now()

        Application("EmailServer") = ConfigurationManager.AppSettings.Get("SmtpServer")

        If Security.SecurityLevelNum < 25 Then
            Response.Redirect("~/SecurityV2.aspx")
        End If

    End Sub

    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)


    End Sub

    Sub Application_BeginRequest(ByVal sender As Object, ByVal e As EventArgs)


    End Sub



</script> 