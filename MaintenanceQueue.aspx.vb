﻿Imports System.IO
Imports System.Data '
Imports System.Data.SqlClient
Imports App_Code
Imports AjaxControlToolkit
Imports C1.Web.UI.Controls.C1Window

Partial Class MaintenanceQueue
    Inherits MyBasePage '   Inherits System.Web.UI.Page


    Dim sActivitytype As String = ""
    Dim cblBinder As New CheckBoxListBinder
    Dim FormSignOn As New FormSQL()
    Dim AccountRequestNum As Integer
    Dim ddlBinder As New DropDownListBinder
    Dim dtMiantenance As DataTable



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
      



        If Not IsPostBack Then
            ddlBinder = New DropDownListBinder(ddlAccounts, "SelApplicationCodes", Session("EmployeeConn"))
            ddlBinder.AddDDLCriteria("@AppSystem", "all", SqlDbType.NVarChar)
            ddlBinder.BindData("ApplicationNum", "ApplicationDesc")

            'bindValidateQueue()


        End If



    End Sub







    Sub btnSelect_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'Dim btnLinkView As Button = sender

        'If btnLinkView.CommandName = "Select" Then

        '    Dim sArguments As String() = Split(btnLinkView.CommandArgument, ";")
        '    Session("AccountRequestNum") = sArguments(0)
        '    Dim Name As String = sArguments(1)
        '    Dim LastName As String = sArguments(2)

        '    Dim thisData As New CommandsSqlAndOleDb("sel_advanced_search", EmployeeConn)

        '    thisData.AddSqlProcParameter("@first_name", Name, SqlDbType.NVarChar, 20)
        '    thisData.AddSqlProcParameter("@last_name", LastName, SqlDbType.NVarChar, 20)

        '    Dim dt As DataTable

        '    dt = thisData.GetSqlDataTable

        '    fillSimilarAccountsTable(dt)

        '    'dgSimilarAccounts.DataSource = dt
        '    'dgSimilarAccounts.DataBind()
        '    'dgSimilarAccounts.Visible = True
        '    'dgExistingAccounts.Visible = False

        '    'DataGrid3.Visible = False
        '    lblAccountExist.Text = "Does This Client Already Exist? " & Name & " " & LastName

        '    btnContinue.Visible = True
        '    btnInvalid.Visible = True


        '    lblHeader.Text = "Current Clients With Similar Names"



        'End If



    End Sub




    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click

        getMaintenanceTable()

    End Sub

    Protected Sub getMaintenanceTable()

        If ddlAccounts.SelectedIndex > 0 Then

            Dim thisData As New CommandsSqlAndOleDb("SelAccountsForMaint", Session("EmployeeConn"))
            thisData.AddSqlProcParameter("@account_type_cd", ddlAccounts.SelectedValue, SqlDbType.NVarChar, 8)
            thisData.AddSqlProcParameter("@MaintType", rblSearchType.SelectedValue, SqlDbType.NVarChar, 12)

            dtMiantenance = thisData.GetSqlDataTable


            fillMaintenanceTable()

        End If






    End Sub

    Protected Sub fillMaintenanceTable()
        Dim iRowCount = 1


        Dim rwHeader As New TableRow
        Dim clClientNumHeader As New TableCell
        Dim clNameHeader As New TableCell
        Dim clLoginHeader As New TableCell
        Dim clAccountTypeHeader As New TableCell


        clClientNumHeader.Text = "Client Num"
        clNameHeader.Text = "Name"
        clLoginHeader.Text = "Login Name"
        clAccountTypeHeader.Text = "Account Type"


        rwHeader.Cells.Add(clClientNumHeader)
        rwHeader.Cells.Add(clNameHeader)
        rwHeader.Cells.Add(clLoginHeader)
        rwHeader.Cells.Add(clAccountTypeHeader)


        rwHeader.Font.Bold = True
        rwHeader.BackColor = Color.FromName("#006666")
        rwHeader.ForeColor = Color.FromName("White")
        rwHeader.Height = Unit.Pixel(36)

        tblQueue.Rows.Add(rwHeader)

        For Each drRow As DataRow In dtMiantenance.Rows

            Dim btnSelect As New Button
            Dim rwData As New TableRow

            Dim clClientNum As New TableCell
            Dim clNameData As New TableCell
            Dim clLoginData As New TableCell
            Dim clAccountTypeData As New TableCell



            If rblSearchType.SelectedValue = "account" Then

                'clClientNum.Text = "<a href=""UserMaintenance.aspx?ClientNum=" & drRow("client_num") & "&AccountCd=" & drRow("ApplicationNum") & "&MaintType=" & rblSearchType.SelectedValue & """ ><u><b> " & drRow("client_num") & "</b></u></a>"
                clClientNum.Text = drRow("client_num")

            ElseIf rblSearchType.SelectedValue = "users" Then
                'clClientNum.Text = "<a href=""MultipleUsersMaintenance.aspx?ClientNum=" & drRow("client_num") & "&AccountCd=" & drRow("ApplicationNum") & "&MaintType=" & rblSearchType.SelectedValue & """ ><u><b> " & drRow("client_num") & "</b></u></a>"
                clClientNum.Text = drRow("client_num")
            End If
            clNameData.Text = IIf(IsDBNull(drRow("Name")), "", drRow("Name"))
            clLoginData.Text = IIf(IsDBNull(drRow("loginName")), "", drRow("loginName"))
            ' clAccountNameData.Text = IIf(IsDBNull(drRow("login_name")), "", drRow("login_name"))
            '  clAccountTypeData.Text = IIf(IsDBNull(drRow("ApplicationDesc")), "", drRow("ApplicationDesc"))





            rwData.Cells.Add(clClientNum)
            rwData.Cells.Add(clNameData)
            rwData.Cells.Add(clLoginData)
            rwData.Cells.Add(clAccountTypeData)


            If iRowCount > 0 Then

                rwData.BackColor = Color.Bisque

            Else

                rwData.BackColor = Color.LightGray

            End If

            tblQueue.Rows.Add(rwData)

            iRowCount = iRowCount * -1


        Next

        tblQueue.Width = New Unit("100%")


    End Sub

End Class
