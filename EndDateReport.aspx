﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="false" CodeFile="EndDateReport.aspx.vb" Inherits="EndDateReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <style type="text/css">
        .style2
        {
            text-align: center;
            background-color: #FFFFCC;
            color: #336666;
            font-weight: bold;
            font-size: x-large;
            height: 43px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
<%--<asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="100"  >
   <ProgressTemplate>
        <div id="IMGDIV" align="center" valign="middle" runat="server" style="position:
                absolute;left: 400px;top:150px;visibility:visible;vertical-align:middle;border-style:
                inset;border-color:black;background-color:White;width:210px;height:75px; z-index: 999;">
        Loading Data ...
            <img src="Images/AjaxProgress.gif" /><br />
        </div>
   </ProgressTemplate>
</asp:UpdateProgress>--%>
 <asp:UpdatePanel ID="uplPanel" runat ="server"  UpdateMode="Conditional">
  <ContentTemplate>
    <table  style="border-style: groove" align="left" border="3px" width="100%">
        <tr align="center">
           <td class="tableRowHeader" colspan="2">
                  End Date Report
           </td>
        </tr>
        <tr>
            <td  align="center">
                                
                <asp:Button ID="btnPDFReport" runat="server" Text="Excel RPT."  Width="120px" Height="30px"
                            Font-Names="Arial" ForeColor="White" Font-Bold="True" Font-Size="Small" />
            </td>
             <td align="center" >
                        <asp:Button ID="btnSubmit" runat="server" Text = "Submit" />
             </td>

        </tr>
        <tr>
            <td align="right">
                Start Date:
            </td>
            <td align="left">
                <asp:TextBox ID="start_dateTextBox" runat="server" CssClass="calenderClass"></asp:TextBox>
            </td>
        </tr>
        <tr>
             <td align="right">
                End Date:
            </td>
            <td align="left">
                <asp:TextBox ID="end_dateTextBox" runat="server" CssClass="calenderClass"></asp:TextBox>
            </td>

        </tr>
        <tr>
            <td>
                Display Accounts with End Date?
            </td>
            <td>
                <asp:RadioButtonList ID = "EndDaterbl" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Selected="True"  >Yes</asp:ListItem>
                    <asp:listItem>No</asp:listItem>

            </asp:RadioButtonList>

            </td>
        
        </tr>
        <tr>
            <td colspan="2" align="center">
                <table>
                    <tr>
                    
                        <td align="right">
                            Select Type:
                        </td>
                        <td align="left">
                                  <asp:DropDownList ID = "Affiliationddl" runat="server" AutoPostBack="true">
                                 </asp:DropDownList>
                                 <asp:Label ID="empType" runat="server" Visible="false"></asp:Label>
                        </td>

                    
                    </tr>
                </table>
            </td>

        </tr>
        <tr >
        <td colspan="2">
 
            <asp:Table  Width="100%" ID = "tblEndDate" runat = "server">
            </asp:Table>
        </td>
     </tr>
        <tr>
             <td colspan="2">
                            <asp:GridView ID="GvClients" runat="server" AllowSorting="True" 
                           AutoGenerateColumns="False" BackColor="White" BorderColor="#336666" 
                           BorderStyle="Double" BorderWidth="3px" CellPadding="1" 
                           DataKeyNames="client_num"  Visible="false"
                           EmptyDataText="No Accounts found " 
                           EnableTheming="False" Font-Size="Medium" GridLines="Horizontal" Width="100%">
                           <FooterStyle BackColor="White" ForeColor="#333333" />
                           <HeaderStyle BackColor="#336666" Font-Bold="False" Font-Size="X-Small" 
                               ForeColor="White" HorizontalAlign="Left" VerticalAlign="Middle" />
                           <PagerStyle BackColor="#336666" Font-Size="Medium" ForeColor="White" 
                               HorizontalAlign="Left" />
                           <RowStyle BackColor="White" ForeColor="#333333" Font-Size="X-Small" />
                           <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                           <SortedAscendingCellStyle BackColor="#F7F7F7" />
                           <SortedAscendingHeaderStyle BackColor="#487575" />
                           <SortedDescendingCellStyle BackColor="#E5E5E5" />
                           <SortedDescendingHeaderStyle BackColor="#275353" />
                           <Columns>

                               <asp:CommandField ButtonType="Button" ControlStyle-BackColor="#84A3A3" 
                                      HeaderStyle-HorizontalAlign="Left" ItemStyle-Height="20px" SelectText="Select" 
                                      ShowSelectButton="true">
                                  <ControlStyle BackColor="#84A3A3" />
                                  <HeaderStyle HorizontalAlign="Left" />
                                  <ItemStyle Height="20px" />
                                  </asp:CommandField>
                            
                               <asp:BoundField DataField="last_name"  ItemStyle-HorizontalAlign="Center" HeaderText="last Name" />

                               <asp:BoundField DataField="first_name" ItemStyle-HorizontalAlign="Center" HeaderText="first_name" />

                               <asp:BoundField DataField="start_date" ItemStyle-HorizontalAlign="Center" HeaderText="Start Date " />

                               <asp:BoundField DataField="end_date" ItemStyle-HorizontalAlign="Center" HeaderText="End Date" />

                               <asp:BoundField DataField="emp_type_cd" ItemStyle-HorizontalAlign="Center" HeaderText="Affiliation" />

                               <asp:BoundField DataField="AccountCount"  ItemStyle-HorizontalAlign="Center" HeaderText="Number of Accounts" />

                           </Columns>
                       </asp:GridView>                            
            </td>
        </tr>

  </table> 
  
  </ContentTemplate>
 
 </asp:UpdatePanel>

</asp:Content>

