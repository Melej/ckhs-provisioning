Imports App_Code
Partial Class Security
    Inherits MyBasePage 'System.Web.UI.Page

    Dim sDeactivated As String = ""
    Dim imSecurityLevel As Integer = 10
    Dim imUserNum As Integer = 0
    Dim smDefaultUnit, smCurrentUnit As String
    Dim smSecurityType As String = "general"
    Dim sAbsPath As String
    Dim sPage, sFromPage As String
    Dim bChangePassword As Boolean
    Dim bDeactivated As Boolean
    Dim bPasswordValidOrNotRequired As Boolean
    Dim dsSecurity As New System.Data.DataSet
    'Dim Sec As New SecurityFunctions(Txtpanlogin.Text, True)
    Dim sDefaultUnit As String = ""
    Dim dt As New DataTable

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        sAbsPath = Request.Url.AbsolutePath()
        'Dim sSplitPath As String()

        'Dim u As Integer = UBound(sSplitPath)
        'sPage = sSplitPath(u)
        ntlogin.Text = Session("loginid")
        '++++++++++++++End get Page name+++++++++++++++++++++++++++++++
        'If Not IsPostBack Then

        '    '+++++++++++Get calling page++++++++++++++++++++++++++++++++++++
        '    sSplitPath = Split(HDreferer.Text, "/")
        '    u = UBound(sSplitPath)
        '    '+++++++++++Get calling page++++++++++++++++++++++++++++++++++++
        'End If



        lblSecurityMessage.Text = "You have insufficient privileges to gain access to CSC V2. You must be a Crozer Employee that has manager rights. You are currently logged on as a user that has very limited access. If you feel that this is incorrect please contact the help desk at @ 15-2610 and identify this as CSC V2."
        
        '==============Security cut and Paste===================================
        Dim sLoginID As String = Session("loginid")

        InfoText.Text = "User Ntlogin : " & Session("loginid")



        'usertype           SecurityLevel
        '-------------------- ------------- 
        'Interface            0
        'General              10
        'Housekeeping         10
        'Transport            10
        'TransportSup         30
        'HouseSup             30
        'NurseStation         45
        'NurseAdmin           50 
        'HouseAdmin           50
        'TransportAdmin       50
        'PFC                  75
        'HospitalAdmin        75
        'SysAdmin             100


        ''Catch ex As Exception
        ''    Session("PageError") = "Error from Page " & sPage & " Procedure Load  Message:" & ex.Message
        ''    response.Redirect("~/"& "ErrorDisplay.aspx?Page=" & sAbsPath)
        ''End Try


    End Sub
    Private Sub KillSessions()
        Session("SecurityLevel") = 10
        Session("SecurityType") = "general"
        Session("DefaultUnit") = ""
        Session("loginid") = "" ' Nothing
        Session("LoginIDUserNum") = ""

    End Sub

    

    
End Class
