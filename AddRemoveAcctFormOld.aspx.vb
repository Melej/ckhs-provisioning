﻿Imports System.IO
Imports System.Data '
Imports System.Data.SqlClient
Imports App_Code
Imports AjaxControlToolkit
Imports C1.Web.UI.Controls.C1Window

Partial Class AddRemoveAcctForm
    Inherits MyBasePage '   Inherits System.Web.UI.Page

    Dim sWebReferrer As String = ""
    Dim sActivitytype As String = ""
    Dim FormSignOn As New FormSQL()
    Dim iClientNum As Integer
    Private dtAccounts As New DataTable
    Dim UserInfo As UserSecurity
    Private AddAccountRequestNum As Integer = 0
    Private DelAccountRequestNum As Integer = 0
    Private dtPendingRequests As DataTable
    Private dtRemoveAccounts As New DataTable
    Private dtAddAccounts As New DataTable
    Dim BusLog As AccountBusinessLogic
    Dim thisdata As CommandsSqlAndOleDb


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    
        thisdata = New CommandsSqlAndOleDb("SelApplicationCodes", Session("EmployeeConn"))
        thisdata.ExecNonQueryNoReturn()

        BusLog = New AccountBusinessLogic
        BusLog.Accounts = thisdata.GetSqlDataTable

        UserInfo = New UserSecurity(Session("LoginID"), Session("EmployeeConn"))
       
        Dim frmSec As New FormSecurity(Session("objUserSecurity"))

        'frmSec.DisableWebControlByRole(login_nameTextBox, "System Admin,Provider Master")


        iClientNum = Request.QueryString("ClientNum")

        FormSignOn = New FormSQL(Session("EmployeeConn"), "SelClientByNumberV3", "UpdClientByNumberV3", "", Page)
        FormSignOn.AddSelectParm("@client_num", iClientNum, SqlDbType.Int, 6)

        ' CheckIfCameFromTicket()

        If Not IsPostBack Then



            Add_userSubmittingLabel.Text = UserInfo.UserName
            Add_SubmittingOnBehalfOfNameLabel.Text = UserInfo.UserName
            Add_requestor_client_numLabel.Text = UserInfo.ClientNum


            Remove_userSubmittingLabel.Text = UserInfo.UserName
            Remove_SubmittingOnBehalfOfNameLabel.Text = UserInfo.UserName
            Remove_requestor_client_numLabel.Text = UserInfo.ClientNum


            Dim rblAffiliateBinder As New RadioButtonListBinder(emp_type_cdrbl, "SelAffiliations", Session("EmployeeConn"))
            rblAffiliateBinder.BindData("AffiliationDescLower", "AffiliationDesc")

            Dim cblFacilitesBinder As New CheckBoxListBinder(cblFacilities, "SelFacilityEntity", Session("EmployeeConn"))
            cblFacilitesBinder.AddDDLCriteria("@EntityFacility", "Facility", SqlDbType.NVarChar)
            cblFacilitesBinder.BindData("Code", "EntityFacilityDesc")

            Dim cblEntityBinder As New CheckBoxListBinder(cblEntities, "SelFacilityEntity", Session("EmployeeConn"))
            cblEntityBinder.AddDDLCriteria("@EntityFacility", "Entity", SqlDbType.NVarChar)
            cblEntityBinder.BindData("Code", "EntityFacilityDesc")

            dtRemoveAccounts.Columns.Add("ApplicationNum", GetType(Integer))
            dtRemoveAccounts.Columns.Add("ApplicationDesc", GetType(String))
            dtRemoveAccounts.Columns.Add("login_name", GetType(String))


            dtAddAccounts.Columns.Add("AppBase", GetType(Integer))
            dtAddAccounts.Columns.Add("ApplicationDesc", GetType(String))



            ViewState("dtRemoveAccounts") = dtRemoveAccounts
            ViewState("dtAddAccounts") = dtAddAccounts

            Dim thisdata As New CommandsSqlAndOleDb("SelAllClientAccountsByNumber", Session("EmployeeConn"))
            thisdata.AddSqlProcParameter("@client_num", iClientNum, SqlDbType.Int, 20)

            thisdata.ExecNonQueryNoReturn()

            gvCurrentUserAccounts.DataSource = thisdata.GetSqlDataTable
            gvCurrentUserAccounts.DataBind()

            ViewState("dtAccounts") = thisdata.GetSqlDataTable


            thisdata = New CommandsSqlAndOleDb("SelAvailableAccountsByNumber", Session("EmployeeConn"))
            thisdata.AddSqlProcParameter("@client_num", iClientNum, SqlDbType.Int, 20)

            thisdata.ExecNonQueryNoReturn()

            ViewState("dtAvailableAccounts") = thisdata.GetSqlDataTable


            Dim ddlBinder As New DropDownListBinder

            ddlBinder.BindData(facility_cdddl, "SelFacilityCodes", "facility_cd", "facility_short_name", Session("EmployeeConn"))
            ddlBinder.BindData(Titleddl, "SelTitles", "TitleDesc", "TitleDesc", Session("EmployeeConn"))
            ddlBinder.BindData(Specialtyddl, "SelSpecialtyCodesV2", "specialty", "specialty", Session("EmployeeConn"))
            ddlBinder.BindData(practice_numddl, "sel_practices", "practice_num", "parctice_name", Session("EmployeeConn"))
            ddlBinder.BindData(entity_cdDDL, "sel_entity_codes", "entity_cd", "hosp_desc", Session("EmployeeConn"))


            FormSignOn.FillForm()


            '---------------------------------------------------
            ' Logic after the form has been filled
            '====================================================

            'ddlBinder.BindData(entity_cdDDL, "sel_entity_codes", "entity_cd", "hosp_desc", EmployeeConn)
            'department_cdddl.Items.Add("Select Entity")


            ddlBinder = New DropDownListBinder(building_cdddl, "sel_building_by_facility", Session("EmployeeConn"))
            ddlBinder.AddDDLCriteria("@facility_cd", facility_cdddl.SelectedValue, SqlDbType.NVarChar)
            ddlBinder.BindData("building_cd", "building_name")


            Dim ddbDepartmentBinder As New DropDownListBinder(department_cdddl, "sel_department_codes", Session("EmployeeConn"))
            ddbDepartmentBinder.AddDDLCriteria("@entity_cd", entity_cdDDL.SelectedValue, SqlDbType.NVarChar)
            ddbDepartmentBinder.BindData("department_cd", "department_name")

            department_cdddl.SelectedValue = department_cdLabel.Text

            building_cdddl.SelectedValue = building_cdTextBox.Text




            ' building_cdddl.SelectedValue = sBuilding



            CurrentAccounts()
            AvailableAccounts()


        End If

        building_cdTextBox.ID = "building_cdTextBoxHidden"
        displayPendingQueues()

    End Sub

    'Protected Sub CheckIfCameFromTicket()
    '    If sWebReferrer.IndexOf("intranet") <> -1 Then

    '        btnBackToTicket.Visible = True

    '    Else

    '        btnBackToTicket.Visible = False

    '    End If


    'End Sub
  
    Protected Sub AvailableAccounts()
        

        grAvailableAccounts.DataSource = ViewState("dtAvailableAccounts")
        grAvailableAccounts.DataBind()


    End Sub

    Protected Sub CurrentAccounts()
       


        gvCurrentApps.DataSource = ViewState("dtAccounts")
        gvCurrentApps.DataBind()

        'gvRemoveAccounts.DataSource = ViewState("dtRemoveAccounts")
        'gvRemoveAccounts.DataBind()




    End Sub

    Protected Sub facility_cdddl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles facility_cdddl.SelectedIndexChanged
        Dim ddlBinder As DropDownListBinder


        ddlBinder = New DropDownListBinder(building_cdddl, "sel_building_by_facility", Session("EmployeeConn"))
        ddlBinder.AddDDLCriteria("@facility_cd", facility_cdddl.SelectedValue, SqlDbType.NVarChar)
        ddlBinder.BindData("building_cd", "building_name")



    End Sub

  

    Private Sub insAcctItems(ByVal requestNum As Integer, ByVal appNum As Integer, ByVal comments As String, ByVal LoginName As String)




        Dim thisdata As New CommandsSqlAndOleDb("InsAccountItemsV3", Session("EmployeeConn"))
        thisdata.AddSqlProcParameter("@account_request_num", requestNum, SqlDbType.Int, 20)
        thisdata.AddSqlProcParameter("@ApplicationNum", appNum, SqlDbType.Int, 8)
        thisdata.AddSqlProcParameter("@account_item_desc", comments, SqlDbType.NVarChar, 255)
        thisdata.AddSqlProcParameter("@validate", "NeedsValidation", SqlDbType.NVarChar, 20)
        thisdata.AddSqlProcParameter("@login_name", LoginName, SqlDbType.NVarChar, 100)



        thisdata.ExecNonQueryNoReturn()



    End Sub





    Protected Sub btnRemoveAccts_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemoveAccts.Click
        Try

            Dim DelReqNum As Integer = AccountRequestNum("delacct")

            For Each rw As GridViewRow In gvRemoveAccounts.Rows
                If rw.RowType = DataControlRowType.DataRow Then

                    Dim appNum As String = gvRemoveAccounts.DataKeys(rw.RowIndex)("ApplicationNum")
                    Dim LoginName As String = gvRemoveAccounts.DataKeys(rw.RowIndex)("login_name")
                  
                    insAcctItems(DelReqNum, appNum, "", LoginName)




                End If



            Next

            Response.Redirect("~/RequestSubmited.aspx?RequestNum=" & DelReqNum)

        Catch ex As Exception

        End Try


    End Sub
  
    Protected Sub btnSubmitAddRequest_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmitAddRequest.Click
        Try


            Dim AccountCode As String
            Dim AddReqNum As Integer = AccountRequestNum("addacct")
            Dim blnHost As Boolean = False
            Dim blnEnt As Boolean = False


            '=========================================================
            ' Checks if Hosp / Ent needed
            '=========================================================



            For Each rw As GridViewRow In grAccountsToAdd.Rows
                If rw.RowType = DataControlRowType.DataRow Then

                    Dim appNum As String = grAccountsToAdd.DataKeys(rw.RowIndex)("AppBase")
                    Dim appDesc As String = grAccountsToAdd.DataKeys(rw.RowIndex)("ApplicationDesc")




                    If BusLog.isHospSpecific(appNum) Then
                        For Each cbFacility As ListItem In cblFacilities.Items

                            If cbFacility.Selected Then

                                blnHost = True

                            End If

                        Next

                        If blnHost = False Then

                            lblValidateAccountRequest.Text = "Must select Hospital for " & appDesc

                            Return
                        End If

                    ElseIf BusLog.isEntSpecific(appNum) Then

                        For Each cbEnt As ListItem In cblEntities.Items

                            If cbEnt.Selected Then

                                blnEnt = True

                            End If

                        Next

                        If blnEnt = False Then

                            lblValidateAccountRequest.Text = "Must select Entity for " & appDesc
                            Return
                        End If

                  

                    End If


                End If

            Next
            
            '=========================================================
            ' Inserts values if Entity / Hospital check has passed
            '=========================================================


            For Each rw As GridViewRow In grAccountsToAdd.Rows
                If rw.RowType = DataControlRowType.DataRow Then

                    Dim appNum As String = grAccountsToAdd.DataKeys(rw.RowIndex)("AppBase")
                    Dim appDesc As String = grAccountsToAdd.DataKeys(rw.RowIndex)("ApplicationDesc")




                    If BusLog.isHospSpecific(appNum) Then
                        For Each cbFacility As ListItem In cblFacilities.Items

                            If cbFacility.Selected Then

                                AccountCode = BusLog.GetApplicationCode(appNum, cbFacility.Value)

                                insAcctItems(AddReqNum, AccountCode, "", "")

                                blnHost = True

                            End If

                        Next

                        If blnHost = False Then

                            lblValidateAccountRequest.Text = "Must select Hospital for " & appDesc

                        End If

                    ElseIf BusLog.isEntSpecific(appNum) Then

                        For Each cbEnt As ListItem In cblEntities.Items

                            If cbEnt.Selected Then

                                AccountCode = BusLog.GetApplicationCode(appNum, cbEnt.Value)

                                insAcctItems(AddReqNum, AccountCode, "", "")

                                blnEnt = True
                            End If

                        Next

                        If blnEnt = False Then

                            lblValidateAccountRequest.Text = "Must select Entity for " & appDesc

                        End If

                    Else

                        insAcctItems(AddReqNum, appNum, "", "")

                    End If


                End If

            Next


            Response.Redirect("~/RequestSubmited.aspx?RequestNum=" & AddReqNum)





        Catch ex As Exception

        End Try


    End Sub

    Private Function AccountRequestNum(ByVal sRequestType As String) As Integer
        Dim iRequestClientNum As Integer

        If sRequestType = "addacct" Then
            iRequestClientNum = Add_requestor_client_numLabel.Text

        End If

        If sRequestType = "delacct" Then
            iRequestClientNum = Remove_requestor_client_numLabel.Text

        End If

        FormSignOn = New FormSQL(Session("EmployeeConn"), "sel_client_by_number", "InsAccountRequestV3", "", Page)
        FormSignOn.AddInsertParm("@requestor_client_num", iRequestClientNum, SqlDbType.NVarChar, 10)
        FormSignOn.AddInsertParm("@submitter_client_num", Session("ClientNum"), SqlDbType.NVarChar, 10)
        FormSignOn.AddInsertParm("@receiver_client_num", iClientNum, SqlDbType.Int, 10)
        FormSignOn.AddInsertParm("@account_request_type", sRequestType, SqlDbType.NVarChar, 9)
        FormSignOn.UpdateFormReturnReqNum()

        AccountRequestNum = FormSignOn.AccountRequestNum

    End Function

    Private Sub displayPendingQueues()
        GetPendingQueues()


        If dtPendingRequests.Rows.Count > 0 Then

            '  cblApplications.Enabled = False


            Dim rwTitle As New TableRow
            Dim clTitle As New TableCell


            Dim iRowCount As Integer = 1
            Dim rwHeader As New TableRow
            Dim clAccountHdr As New TableCell
            Dim clRequestTypeHdr As New TableCell
            Dim clValidCdHdr As New TableCell
            Dim clRequestorHdr As New TableCell
            Dim clDateHdr As New TableCell


            clTitle.Text = "Pending Requests"
            clTitle.Font.Bold = True
            clTitle.ColumnSpan = 5
            clTitle.BackColor = Color.White
            clTitle.HorizontalAlign = HorizontalAlign.Center
            clTitle.Font.Size = 20



            rwTitle.Cells.Add(clTitle)

            tblPendingQueues.Rows.Add(rwTitle)

            clAccountHdr.Text = "Account"
            clRequestTypeHdr.Text = "Request"
            clValidCdHdr.Text = "Status"
            clRequestorHdr.Text = "Requester"
            clDateHdr.Text = "Request Date"

            rwHeader.Cells.Add(clAccountHdr)
            rwHeader.Cells.Add(clRequestTypeHdr)
            rwHeader.Cells.Add(clValidCdHdr)
            rwHeader.Cells.Add(clRequestorHdr)
            rwHeader.Cells.Add(clDateHdr)

            rwHeader.Font.Bold = True
            rwHeader.BackColor = Color.FromName("#006666")
            rwHeader.ForeColor = Color.FromName("White")
            rwHeader.Height = Unit.Pixel(36)

            tblPendingQueues.Rows.Add(rwHeader)

            tblPendingQueues.Width = New Unit("100%")



            For Each rwPending As DataRow In dtPendingRequests.Rows

                Dim rwData As New TableRow
                Dim clAccountData As New TableCell
                Dim clRequestTypeData As New TableCell
                Dim clValidCdData As New TableCell
                Dim clRequestorData As New TableCell
                Dim clDateData As New TableCell


                clAccountData.Text = rwPending("ApplicationDesc")
                clRequestTypeData.Text = rwPending("account_request_type")
                clValidCdData.Text = IIf(IsDBNull(rwPending("valid_cd")), "", rwPending("valid_cd"))
                clRequestorData.Text = IIf(IsDBNull(rwPending("requestor_name")), "", rwPending("requestor_name"))
                clDateData.Text = IIf(IsDBNull(rwPending("entered_date")), "", rwPending("entered_date"))

                rwData.Cells.Add(clAccountData)
                rwData.Cells.Add(clRequestTypeData)
                rwData.Cells.Add(clValidCdData)
                rwData.Cells.Add(clRequestorData)
                rwData.Cells.Add(clDateData)


                If iRowCount > 0 Then

                    rwData.BackColor = Color.Bisque

                Else

                    rwData.BackColor = Color.LightGray

                End If



                iRowCount = iRowCount * -1


                tblPendingQueues.Rows.Add(rwData)


            Next
            tpPendingRequests.Visible = True
        Else

            tpPendingRequests.Visible = False
        End If

    End Sub

    Private Sub GetPendingQueues()
        Dim thisdata As New CommandsSqlAndOleDb("SelPendingQueuesByRecieverNum", Session("EmployeeConn"))
        thisdata.AddSqlProcParameter("@reciever_client_num", iClientNum, SqlDbType.Int, 20)

        dtPendingRequests = thisdata.GetSqlDataTable

    End Sub

    Protected Sub gvCurrentApps_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvCurrentApps.RowCommand

        If e.CommandName = "Select" Then


            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim ApplicationNum As String = gvCurrentApps.DataKeys(Convert.ToInt32(e.CommandArgument))("ApplicationNum").ToString()
            Dim ApplicationDesc As String = gvCurrentApps.DataKeys(Convert.ToInt32(e.CommandArgument))("ApplicationDesc").ToString()
            Dim CreateDate As String = gvCurrentApps.DataKeys(Convert.ToInt32(e.CommandArgument))("create_date").ToString()
            Dim LoginName As String = gvCurrentApps.DataKeys(Convert.ToInt32(e.CommandArgument))("login_name").ToString()

            dtRemoveAccounts = DirectCast(ViewState("dtRemoveAccounts"), DataTable)
            dtRemoveAccounts.Rows.Add(ApplicationNum, ApplicationDesc, LoginName)

            gvRemoveAccounts.DataSource = dtRemoveAccounts
            gvRemoveAccounts.DataBind()

            Dim dtAccounts As New DataTable
            dtAccounts = DirectCast(ViewState("dtAccounts"), DataTable)
            dtAccounts.Rows(index).Delete()
            dtAccounts.AcceptChanges()


            ViewState("dtAccounts") = dtAccounts

            gvCurrentApps.DataSource = dtAccounts
            gvCurrentApps.DataBind()



            uplAccounts.Update()

        End If
    End Sub
    Protected Sub gvRemoveAccounts_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvRemoveAccounts.RowCommand

        If e.CommandName = "Select" Then


            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim ApplicationNum As String = gvRemoveAccounts.DataKeys(Convert.ToInt32(e.CommandArgument))("ApplicationNum").ToString()
            Dim ApplicationDesc As String = gvRemoveAccounts.DataKeys(Convert.ToInt32(e.CommandArgument))("ApplicationDesc").ToString()

            Dim dtAccounts As New DataTable
            dtAccounts = DirectCast(ViewState("dtAccounts"), DataTable)
            dtAccounts.Rows.Add(ApplicationNum, ApplicationDesc)

            ViewState("dtAccounts") = dtAccounts

            gvCurrentApps.DataSource = dtAccounts
            gvCurrentApps.DataBind()




            dtRemoveAccounts = DirectCast(ViewState("dtRemoveAccounts"), DataTable)
            dtRemoveAccounts.Rows(index).Delete()
            dtRemoveAccounts.AcceptChanges()


            ViewState("dtRemoveAccounts") = dtRemoveAccounts

            gvRemoveAccounts.DataSource = dtRemoveAccounts
            gvRemoveAccounts.DataBind()



            uplAccounts.Update()

        End If
    End Sub
    Protected Sub grAccountsToAdd_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grAccountsToAdd.RowCommand

        If e.CommandName = "Select" Then


            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim ApplicationNum As String = grAccountsToAdd.DataKeys(Convert.ToInt32(e.CommandArgument))("AppBase").ToString()
            Dim ApplicationDesc As String = grAccountsToAdd.DataKeys(Convert.ToInt32(e.CommandArgument))("ApplicationDesc").ToString()

            Dim dtAvailableAccounts As New DataTable
            dtAvailableAccounts = DirectCast(ViewState("dtAvailableAccounts"), DataTable)
            dtAvailableAccounts.Rows.Add(ApplicationNum, ApplicationDesc)

            ViewState("dtAvailableAccounts") = dtAvailableAccounts

            grAvailableAccounts.DataSource = dtAvailableAccounts
            grAvailableAccounts.DataBind()


            dtAddAccounts = DirectCast(ViewState("dtAddAccounts"), DataTable)
            dtAddAccounts.Rows(index).Delete()
            dtAddAccounts.AcceptChanges()


            ViewState("dtAddAccounts") = dtAddAccounts

            grAccountsToAdd.DataSource = dtAddAccounts
            grAccountsToAdd.DataBind()


            uplAccounts.Update()

        End If
    End Sub
    Protected Sub grAvailableAccounts_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grAvailableAccounts.RowCommand

        If e.CommandName = "Select" Then


            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim ApplicationNum As String = grAvailableAccounts.DataKeys(Convert.ToInt32(e.CommandArgument))("AppBase").ToString()
            Dim ApplicationDesc As String = grAvailableAccounts.DataKeys(Convert.ToInt32(e.CommandArgument))("ApplicationDesc").ToString()


            dtAddAccounts = DirectCast(ViewState("dtAddAccounts"), DataTable)
            dtAddAccounts.Rows.Add(ApplicationNum, ApplicationDesc)

            grAccountsToAdd.DataSource = dtAddAccounts
            grAccountsToAdd.DataBind()

            Dim dtAvailableAccounts As New DataTable
            dtAvailableAccounts = DirectCast(ViewState("dtAvailableAccounts"), DataTable)
            dtAvailableAccounts.Rows(index).Delete()
            dtAvailableAccounts.AcceptChanges()

            grAvailableAccounts.DataSource = dtAvailableAccounts
            grAvailableAccounts.DataBind()

            ViewState("dtAvailableAccounts") = dtAvailableAccounts
            ViewState("dtAddAccounts") = dtAddAccounts

            grAccountsToAdd.DataSource = dtAddAccounts
            grAccountsToAdd.DataBind()

            uplAccounts.Update()

        End If
    End Sub

    Protected Function checkFacilities() As Boolean
        Dim blnReturn = False

        For Each item As ListItem In cblFacilities.Items
            If item.Selected Then
                blnReturn = True

            End If


        Next


        Return blnReturn

    End Function

    Protected Function checkEntities() As Boolean
        Dim blnReturn As Boolean = False

        For Each item As ListItem In cblEntities.Items
            If item.Selected Then

                blnReturn = True
            End If

        Next


        Return blnReturn
    End Function

    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        If emp_type_cdrbl.SelectedIndex < 0 Then

            lblValidation.Text = "Must Select an Affiliation"

        Else


            FormSignOn.AddInsertParm("@client_num", iClientNum, SqlDbType.Int, 9)
            FormSignOn.UpdateForm()
            lblValidation.Text = "Information Updated"

        End If

    End Sub

    Protected Sub btnCloseOnBelafOf_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCloseOnBelafOf.Click

        mpeOnBehalfOf.Hide()

    End Sub
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click

        Dim thisData As New CommandsSqlAndOleDb("SelSimpleSearch", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@FirstName", FirstNameTextBox.Text, SqlDbType.NVarChar, 20)
        thisData.AddSqlProcParameter("@LastName", LastNameTextBox.Text, SqlDbType.NVarChar, 20)






        gvSearch.DataSource = thisData.GetSqlDataTable
        gvSearch.DataBind()

        uplOnBehalfOf.Update()
        mpeOnBehalfOf.Show()

    End Sub

    Protected Sub btnRemoveOnBehalfOf_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemoveOnBehalfOf.Click

       
        ViewState("RequestorSearch") = "Remove"
        mpeOnBehalfOf.Show()

    End Sub

    Protected Sub btnSelectOnBehalfOf_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSelectOnBehalfOf.Click

        ViewState("RequestorSearch") = "Add"

        mpeOnBehalfOf.Show()

    End Sub

    Protected Sub gvSearch_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvSearch.RowCommand

        If e.CommandName = "Select" Then
            Dim client_num As String = gvSearch.DataKeys(Convert.ToInt32(e.CommandArgument))("client_num").ToString()


            If ViewState("RequestorSearch") = "Add" Then

                Add_SubmittingOnBehalfOfNameLabel.Text = gvSearch.Rows(Convert.ToInt32(e.CommandArgument)).Cells(1).Text
                Add_requestor_client_numLabel.Text = client_num


                ViewState("RequestorSearch") = ""
            End If
            If ViewState("RequestorSearch") = "Remove" Then

                Remove_SubmittingOnBehalfOfNameLabel.Text = gvSearch.Rows(Convert.ToInt32(e.CommandArgument)).Cells(1).Text
                Remove_requestor_client_numLabel.Text = client_num

                ViewState("RequestorSearch") = ""
            End If

            uplAccounts.Update()

            mpeOnBehalfOf.Hide()



        End If
    End Sub
    Protected Sub gvCurrentUserAccounts_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvCurrentUserAccounts.RowCommand
        If e.CommandName = "Select" Then
            Dim ApplicationNum As String = gvCurrentUserAccounts.DataKeys(Convert.ToInt32(e.CommandArgument))("ApplicationNum").ToString()
            Dim ApplicationDesc As String = gvCurrentUserAccounts.DataKeys(Convert.ToInt32(e.CommandArgument))("ApplicationDesc").ToString()
            Dim LoginName As String = gvCurrentUserAccounts.DataKeys(Convert.ToInt32(e.CommandArgument))("login_name").ToString()
            ViewState("LoginEditCreateDate") = gvCurrentUserAccounts.DataKeys(Convert.ToInt32(e.CommandArgument))("create_date").ToString()


            pnlEditLogin.Visible = True
            pnlDuplicatAccounts.Visible = False


            lblEditAccountName.Text = ApplicationDesc
            txtEditAccountName.Text = LoginName
            txtEditAccountNum.Text = ApplicationNum

        End If
    End Sub

    Protected Sub btnEditAccountName_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditAccountName.Click

        Dim thisData As New CommandsSqlAndOleDb("SelDuplicateAccountLogins", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@ApplicationNum", txtEditAccountNum.Text, SqlDbType.NVarChar, 20)
        thisData.AddSqlProcParameter("@login_name", txtEditAccountName.Text, SqlDbType.NVarChar, 20)


        Dim dtDuplicateAccounts As DataTable
        dtDuplicateAccounts = thisData.GetSqlDataTable


        If dtDuplicateAccounts.Rows.Count = 0 Then

            UpdateLoginAccount()

        Else
            For Each dr As DataRow In dtDuplicateAccounts.Rows

                Try


              
                Dim rwDup As New TableRow
                Dim clDup As New TableCell
                    clDup.Text = "<a target='_blank' href=""AddRemoveAcctForm.aspx?ClientNum=" & dr("client_num") & """ ><u><b> " & dr("DupName") & "</b></u></a>"
                    '  clDup. = "<asp:HyperLink  NavigateUrl='AddRemoveAcctForm.aspx?ClientNum=" & dr("client_num") & " runat='server' Text='" & dr("DupName") & "' Target='_blank'>" & dr("DupName") & "</asp:HyperLink>"
                rwDup.Cells.Add(clDup)

                tblDubNames.Rows.Add(rwDup)

                Catch ex As Exception

                End Try
            Next

            pnlDuplicatAccounts.Visible = True
        End If

      

    End Sub


    Protected Sub btnEditLoginNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditLoginNo.Click

        pnlEditLogin.Visible = False


    End Sub


    Protected Sub btnEditLoginYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditLoginYes.Click

        pnlEditLogin.Visible = False
        UpdateLoginAccount()

    End Sub


    Public Sub UpdateLoginAccount()

        Dim dataUpdateAcct As New CommandsSqlAndOleDb("UpdAccountLoginByNumberV3", Session("EmployeeConn"))
        dataUpdateAcct.AddSqlProcParameter("@client_num", iClientNum, SqlDbType.Int, 20)
        dataUpdateAcct.AddSqlProcParameter("@ApplicationNum", txtEditAccountNum.Text, SqlDbType.Int, 8)
        dataUpdateAcct.AddSqlProcParameter("@create_date", ViewState("LoginEditCreateDate"), SqlDbType.DateTime, 255)
        dataUpdateAcct.AddSqlProcParameter("@login_name", txtEditAccountName.Text, SqlDbType.NVarChar, 255)
        dataUpdateAcct.AddSqlProcParameter("@tech_client_num", Session("ClientNum"), SqlDbType.NVarChar, 255)

        dataUpdateAcct.ExecNonQueryNoReturn()

        BindCurrentApps()

    End Sub

    Public Sub BindCurrentApps()


        Dim thisdata As New CommandsSqlAndOleDb("SelAllClientAccountsByNumber", Session("EmployeeConn"))
        thisdata.AddSqlProcParameter("@client_num", iClientNum, SqlDbType.Int, 20)

        thisdata.ExecNonQueryNoReturn()

        gvCurrentUserAccounts.DataSource = thisdata.GetSqlDataTable
        gvCurrentUserAccounts.DataBind()


    End Sub
    Protected Sub entity_cdDDL_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles entity_cdDDL.SelectedIndexChanged
        Dim ddbDepartmentBinder As New DropDownListBinder(department_cdddl, "sel_department_codes", Session("EmployeeConn"))
        ddbDepartmentBinder.AddDDLCriteria("@entity_cd", entity_cdDDL.SelectedValue, SqlDbType.NVarChar)
        ddbDepartmentBinder.BindData("department_cd", "department_name")
        department_cdLabel.Text = ""

    End Sub
    Protected Sub department_cdddl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles department_cdddl.SelectedIndexChanged
       department_cdLabel.Text = department_cdddl.SelectedValue

    End Sub

    'Protected Sub btnBackToTicket_click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBackToTicket.Click

    '    Response.Redirect(sWebReferrer)


    'End Sub
End Class
