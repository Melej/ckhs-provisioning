﻿Imports System.IO
Imports System.Data '
Imports System.Data.SqlClient
Imports App_Code
Imports AjaxControlToolkit
Imports C1.Web.UI.Controls.C1Window
Imports System.Net.Mail

Partial Class DeleteAccount
    Inherits System.Web.UI.Page 'Inherits MyBasePage 


    Dim FormSignOn As New FormSQL()

    Dim sActivitytype As String = ""
    Dim cblBinder As New CheckBoxListBinder
    'Dim FormViewRequest As New FormSQL()
    Dim AccountRequestNum As Integer
    Dim AccountSeqNum As Integer
    Dim AccountRequestType As String
    Dim AppNum As Integer
    Dim AccountDesc As String
    Dim LoginName As String
    Dim AccountType As String
    Dim ItemValidCd As String

    Dim dtSubApplications As DataTable

    Dim RequestItems As RequestItems
    Dim thisRequest As New RequestItem
    Dim AccRequest As AccountRequest
    Dim ActionControl As New ActionController()
    Dim ActionList As New List(Of Actions)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        AccountRequestNum = Request.QueryString("RequestNum")
        AccountSeqNum = Request.QueryString("account_seq_num")
        AccountType = Request.QueryString("AccountType")

        If Session("SecurityLevelNum") < 70 Then
            Response.Redirect("./SimpleSearch.aspx", True)

        End If

        If AccountType Is Nothing Then
            AccountType = "delete"
        End If
        AccRequest = New AccountRequest(AccountRequestNum, Session("EmployeeConn"))


        FormSignOn = New FormSQL(Session("EmployeeConn"), "SelAccountRequestV20", "UpdFullAccountRequestV20", "", Page)
        FormSignOn.AddSelectParm("@account_request_num", AccountRequestNum, SqlDbType.Int, 6)

        RequestItems = New RequestItems(AccountRequestNum)
        thisRequest = RequestItems.GetRequestItemBySeqNum(AccountSeqNum)

        Dim rblAffiliateBinder As New RadioButtonListBinder(emp_type_cdrbl, "SelAffiliationsV4", Session("EmployeeConn"))
        rblAffiliateBinder.BindData("AffiliationDescLower", "AffiliationDisplay")

        lblheader.Text = "Delete Account Requests"

        If AccountType.ToLower = "invalid" Then
            lblheader.Text = "Invalid Account Requests"
            tpheader.Visible = False
        End If

        Dim ctrl As Control = DataCheckingAndValidation.SetChildControlsRecursiveToEnabledFalse(tpDemographics, False)

        LoginIDTextBox.Enabled = False
        T1LocationOfCareIDTextBox.Enabled = False
        T1CopyToTextBox.Enabled = False
        T1CommLocationOfCareIDtextBox.Enabled = False
        T1CommCopytoTextBox.Enabled = False

        btnGoToRequest.Enabled = True

        RequestItems = New RequestItems(AccountRequestNum)
        thisRequest = RequestItems.GetRequestItemBySeqNum(AccountSeqNum)

        AppNum = thisRequest.ApplicationNum
        ItemCompleteDateTextBox.Text = thisRequest.ItemCompleteDate
        LoginIDTextBox.Text = thisRequest.LoginName
        ItemValidCd = thisRequest.ValidCd
        lblAppSystem.Text = thisRequest.AppSystem

        If T1LocationOfCareIDTextBox.Text.ToString = "" Then
            T1LocationOfCareIDTextBox.Text = thisRequest.LocationOfCareID
        End If

        If T1CopyToTextBox.Text.ToString = "" Then
            T1CopyToTextBox.Text = thisRequest.CopyTo
        End If

        If T1CommLocationOfCareIDtextBox.Text.ToString = "" Then
            T1CommLocationOfCareIDtextBox.Text = thisRequest.CommLocationOfCareID
        End If

        If T1CommCopytoTextBox.Text.ToString = "" Then
            T1CommCopytoTextBox.Text = thisRequest.CommCopyTo
        End If

        Select Case AppNum
            'Case 4  ' get an email name
            '    lblAppname.Visible = True
            '    AppnameTextBox.Visible = True
            '    lblAppname.Text = "Suppply a Valid eMail"
            '    AppnameTextBox.Text = first_namelabel.Text + "." + last_namelabel.Text + "@crozer.org"


            Case 52, 53 ' 51 EMR CKHN 53 EMR Community


                lblLocationofCare.Visible = True
                T1LocationOfCareIDTextBox.Visible = True

                lblCopyTo.Visible = True
                T1CopyToTextBox.Visible = True

                lblCommLocationofcare.Visible = True
                T1CommLocationOfCareIDtextBox.Visible = True

                lblCommCopyTo.Visible = True
                T1CommCopytoTextBox.Visible = True

                LblDoctormaster.Visible = True
                lbldoctormastertxt.Visible = True


            Case 91, 92 ' 91 Interface CKHN 93 interface Community 

                interpnl.Visible = True

                lblLocationofCare.Visible = True
                T1LocationOfCareIDTextBox.Visible = True

                lblCopyTo.Visible = True
                T1CopyToTextBox.Visible = True

                lblCommLocationofcare.Visible = True
                T1CommLocationOfCareIDtextBox.Visible = True

                lblCommCopyTo.Visible = True
                T1CommCopytoTextBox.Visible = True

                LblDoctormaster.Visible = True
                lbldoctormastertxt.Visible = True

                lbladdcopyto.Visible = True
                CopyTorbl.Visible = True

                lbladdMedass.Visible = True
                MedAssrbl.Visible = True

                lblBounced.Visible = True
                Bouncedrbl.Visible = True

                'Case 102, 103, 104, 105, 109, 110, 111, 112, 115, 116, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129
                '    ' expose fincial level Approve button
                '    ' btnSubmitApprovBy.Visible = True
                'Case 106, 107, 108
                '    ' expose fincial level and Dollar amount
                '    financialPnl.Visible = True
                '    lblLevel.Visible = True
                '    lblLevelSubmited.Visible = True
                '    lbldollar.Visible = True
                '    lblDollarSubmitted.Visible = True

                'Case 117, 118, 119
                '    ' expose fincial level 
                '    financialPnl.Visible = True
                '    lblLevel.Visible = True
                '    lblLevelSubmited.Visible = True

                '    lbldollar.Visible = False
                '    lblDollarSubmitted.Visible = False

        End Select

        ' FormViewRequest.AddSelectParm("@account_request_num", AccountRequestNum, SqlDbType.Int, 4)
        'FormViewRequest.FillForm()



        AccountRequestType = FormSignOn.getFieldValue("account_request_type")

        AccountLabel.Text = AccountDesc






        If Not IsPostBack Then

            'LoginIDTextBox.Text = LoginName
            LoginIDTextBox.Text = thisRequest.LoginName
            CommentsTextBox.Text = thisRequest.AccountItemDesc

            FormSignOn.FillForm()

            'GetApplication()

            CheckAffiliation()

            'If close_dateTextBox.Text <> "" Then
            '    Dim ctrl As Control = DataCheckingAndValidation.SetChildControlsRecursiveToEnabledFalse(tpDemographics, False)

            'Else '' if this request is open 
            '    If providerrbl.Text = "Yes" Then
            '        ' only 90 security level and above edit these fields
            '        If Session("SecurityLevelNum") < 90 Then
            '            Dim ctrl As Control = DataCheckingAndValidation.SetChildControlsRecursiveToEnabledFalse(tpDemographics, False)

            '        End If
            '    End If
            'End If



            Dim ddlBinder As New DropDownListBinder

            ddlBinder.BindData(facility_cdddl, "SelFacilityCodes", "facility_cd", "facility_short_name", Session("EmployeeConn"))
            ddlBinder.BindData(Titleddl, "SelTitles", "TitleDesc", "TitleDesc", Session("EmployeeConn"))
            ddlBinder.BindData(Specialtyddl, "SelSpecialtyCodesV2", "specialty", "specialty", Session("EmployeeConn"))
            ddlBinder.BindData(practice_numddl, "sel_practices", "practice_num", "parctice_name", Session("EmployeeConn"))
            ddlBinder.BindData(entity_cdDDL, "sel_entity_codes", "entity_cd", "hosp_desc", Session("EmployeeConn"))

            Dim ddbDepartmentBinder As New DropDownListBinder(department_cdddl, "sel_department_codes", Session("EmployeeConn"))
            ddbDepartmentBinder.AddDDLCriteria("@entity_cd", entity_cdDDL.SelectedValue, SqlDbType.NVarChar)
            ddbDepartmentBinder.BindData("department_cd", "department_name")

            department_cdddl.SelectedValue = department_cdLabel.Text



            Dim ddlBuildingBinder As New DropDownListBinder
            ddlBuildingBinder = New DropDownListBinder(building_cdddl, "sel_building_by_facility", Session("EmployeeConn"))
            ddlBuildingBinder.AddDDLCriteria("@facility_cd", facility_cdddl.SelectedValue, SqlDbType.NVarChar)
            ddlBuildingBinder.BindData("building_cd", "building_name")

            Dim sBuilding As String = building_cdTextBox.Text
            building_cdddl.SelectedValue = sBuilding

            Select Case AppNum
                Case 97, 98, 99, 100

                    Select Case lblAppSystem.Text
                        Case "15"

                            CCMCNurseStationspan.Visible = True
                        Case "12"
                            dcmhnursepan.Visible = True
                        Case "19"
                            taylornursepan.Visible = True
                        Case "16"
                            SpringNurseStationsPan.Visible = True

                    End Select
                    LoadNursestations(AppNum)
                Case 136
                    O365Panel.Visible = True
                    Load365Locations(AppNum)

            End Select
        End If


        ActionControl.GetAllActions(AccountRequestNum, Session("EmployeeConn"))
        ActionList = ActionControl.AllActions()

        TPActionItems.Controls.Add(fillActionItemsTable(ActionControl.AllActions()))

        fillRequestItemsTable()

        btnRequestDetails.Attributes.Add("onclick", "newPopup('RequestDetail.aspx?RequestNum=" & AccountRequestNum & " ')")

        If ItemCompleteDateTextBox.Text <> "" Then
            btnCreateSubmit.Enabled = False
            btnInvalid.Enabled = False
            LblMainInfo.Text = "This Request item is completed."

        End If
        If ItemValidCd.ToLower = "needsvalidation" Then
            btnCreateSubmit.Enabled = False
            btnInvalid.Enabled = False
            LblMainInfo.Text = "This Request or Account has not yet been Validated."
        End If
        If Session("SecurityLevelNum") < 60 Then

            Dim Mainctrl As Control = DataCheckingAndValidation.SetChildControlsRecursiveToEnabledFalse(tpheader, False)
            btnGoToRequest.Visible = False

        End If

        If Session("Usertype").ToString.ToLower = "provaccount" Then
            btnGoToRequest.Visible = False
            tpRequestItems.Visible = False

        End If



    End Sub


    'Protected Sub GetApplication()
    '    Dim sReturn As String = ""

    '    Dim thisData As New CommandsSqlAndOleDb("SelItemsByNumValidCdSeqNumV3", Session("EmployeeConn"))
    '    thisData.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int)
    '    If AccountType.ToLower = "delete" Or AccountType.ToLower = "terminate" Or AccountType.ToLower = "delacct" Then
    '        thisData.AddSqlProcParameter("@valid_cd", "validated", SqlDbType.NVarChar, 12)
    '    Else
    '        thisData.AddSqlProcParameter("@valid_cd", "invalid", SqlDbType.NVarChar, 12)

    '    End If

    '    thisData.AddSqlProcParameter("@account_request_seq_num", AccountSeqNum, SqlDbType.NVarChar, 12)

    '    thisData.ExecNonQueryNoReturn()

    '    AccountDesc = thisData.GetSqlDataTable.Rows(0).Item("ApplicationDesc")
    '    AppNum = thisData.GetSqlDataTable.Rows(0).Item("ApplicationNum")
    '    LoginName = IIf(IsDBNull(thisData.GetSqlDataTable.Rows(0).Item("login_name")), "", thisData.GetSqlDataTable.Rows(0).Item("login_name"))





    'End Sub


    Protected Sub btnCreateSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCreateSubmit.Click


        Dim thisData As New CommandsSqlAndOleDb("UpdAccountItemsV21", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int)
        thisData.AddSqlProcParameter("@account_request_seq_num", AccountSeqNum, SqlDbType.Int)
        thisData.AddSqlProcParameter("@ApplicationNum", AppNum, SqlDbType.Int, 30)

        thisData.AddSqlProcParameter("@item_desc", CommentsTextBox.Text, SqlDbType.NVarChar, 255)
        thisData.AddSqlProcParameter("@account_request_type", AccountRequestType, SqlDbType.NVarChar, 255)
        thisData.AddSqlProcParameter("@nt_login", Session("loginid"), SqlDbType.NVarChar, 10)

        thisData.ExecNonQueryNoReturn()

        Select Case AppNum
            Case 97, 98, 99, 100, 136
                InsertNursestations()
                'Mak

        End Select


        Response.Redirect("./MyAccountQueue.aspx")

        ' Response.Redirect("./AccountRequestInfo2.aspx?requestnum=" & AccountRequestNum) ' & "&account_seq_num=" & AccountSeqNum & "&AccountType=" & AccountType, True)

    End Sub
  

    Protected Sub btnInvalid_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnInvalid.Click
        FillInvalidCodes()

        If CommentsTextBox.Text <> "" Then
            InvalidDescTextBox.Text = CommentsTextBox.Text

            CommentsTextBox.Text = ""

        End If
        With PanInvalid
            .Visible = True
            With .Style
                .Add("LEFT", "260px")
                .Add("TOP", "190px")
                .Add("POSITION", "absolute")
                .Add("Z-INDEX", "999")
            End With
        End With


    End Sub
    Private Sub FillInvalidCodes()
        'SelInvalidCodes
        Dim dt As DataTable
        Dim thisData As New CommandsSqlAndOleDb
        thisData = New CommandsSqlAndOleDb("SelInvalidCodes", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@invalidType", "delete", SqlDbType.NVarChar, 12)

        dt = thisData.GetSqlDataTable()
        Invalidddl.DataSource = dt
        Invalidddl.DataValueField = "Invalidcd"
        Invalidddl.DataTextField = "InvalidDesc"
        Invalidddl.DataBind()
        thisData = Nothing

    End Sub
    Protected Sub fillRequestItemsTable()
        Dim thisData As New CommandsSqlAndOleDb("SelRequestItemsV6", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.NVarChar)

        Dim iRowCount = 1

        Dim dt As DataTable
        dt = thisData.GetSqlDataTable

        Dim rwHeader As New TableRow

        Dim clBtnheader As New TableCell
        Dim clAccountHeader As New TableCell
        Dim clRequestTypeHeader As New TableCell
        Dim clStatusHeader As New TableCell


        clBtnheader.Text = ""
        clAccountHeader.Text = "Account"
        clRequestTypeHeader.Text = "Request Type"
        clStatusHeader.Text = "Status"


        rwHeader.Cells.Add(clBtnheader)
        rwHeader.Cells.Add(clAccountHeader)
        rwHeader.Cells.Add(clRequestTypeHeader)
        rwHeader.Cells.Add(clStatusHeader)


        rwHeader.Font.Bold = True
        rwHeader.BackColor = Color.FromName("#006666")
        rwHeader.ForeColor = Color.FromName("White")
        rwHeader.Height = Unit.Pixel(36)

        tblRequestItems.Rows.Add(rwHeader)

        For Each drRow As DataRow In dt.Rows


            Dim rwData As New TableRow

            Dim clBtn As New TableCell

            Dim claccountRequest As New TableCell
            Dim claRequestSeqNum As New TableCell

            Dim clReqNum As New TableCell

            Dim clAccountData As New TableCell
            Dim clRequestTypeData As New TableCell
            Dim clStatusData As New TableCell
            Dim clRequestorData As New TableCell
            Dim clReqPhoneData As New TableCell

            clAccountData.Text = IIf(IsDBNull(drRow("ApplicationDesc")), "", drRow("ApplicationDesc"))
            clRequestTypeData.Text = IIf(IsDBNull(drRow("request_code_desc")), "", drRow("request_code_desc"))
            clStatusData.Text = IIf(IsDBNull(drRow("valid_cd")), "", drRow("valid_cd"))

            claccountRequest.Text = IIf(IsDBNull(drRow("account_request_num")), "", drRow("account_request_num"))
            claRequestSeqNum.Text = IIf(IsDBNull(drRow("account_request_seq_num")), "", drRow("account_request_seq_num"))

            clReqNum.Text = "<a href=""DeleteAccount.aspx?requestnum=" & claccountRequest.Text & "&account_seq_num=" & claRequestSeqNum.Text & "&AccountType=" & AccountType & """ ><u><b> " & claccountRequest.Text & "</b></u></a>"


            ' clBtn.Attributes("onClick") = ' "btn_Click( " & claccountRequest.Text & " , " & claRequestSeqNum.Text & ")" '
            'Selbtn.Attributes("onClick") = Response.Redirect("./CreateAccount.aspx?requestnum=" & claccountRequest.Text & "&account_seq_num=" & claRequestSeqNum.Text, True)

            ' clBtn.Controls.Add(DirectCast(Selbtn, Button))

            rwData.Cells.Add(clReqNum)
            rwData.Cells.Add(clAccountData)
            rwData.Cells.Add(clRequestTypeData)
            rwData.Cells.Add(clStatusData)


            If iRowCount > 0 Then

                rwData.BackColor = Color.Bisque

            Else

                rwData.BackColor = Color.LightGray

            End If

            tblRequestItems.Rows.Add(rwData)

            iRowCount = iRowCount * -1

        Next
        tblRequestItems.CellPadding = 10
        tblRequestItems.Width = New Unit("100%")


    End Sub
    Private Sub CheckAffiliation()
        pnlOtherAffilliation.Visible = False
        pnlAlliedHealth.Visible = False




        Select Case emp_type_cdrbl.SelectedValue

            Case "employee"

                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")

                'Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                'Dim cityReq As New RequiredField(citytextbox, "City")
                'Dim stateReq As New RequiredField(statetextbox, "State")
                'Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                'Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                'Dim faxqReq As New RequiredField(faxtextbox, "Fax")
                '  Dim locReq As New RequiredField(facility_cdddl, "Location")

                ' Dim endReq As New RequiredField(end_dateTextBox, "End Date")

                pnlOtherAffilliation.Visible = False
                pnlAlliedHealth.Visible = False
                pnlProvider.Visible = False
                providerrbl.SelectedIndex = -1




            Case "student"

                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")
                Dim titleReq As New RequiredField(Titleddl, "Title")
                Dim specReq As New RequiredField(Specialtyddl, "Specialty")
                Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim faxqReq As New RequiredField(faxtextbox, "Fax")

                pnlOtherAffilliation.Visible = False
                pnlAlliedHealth.Visible = False
                pnlProvider.Visible = False
                providerrbl.SelectedIndex = -1

                'Dim locReq As New RequiredField(facility_cdddl, "Location")

                'Dim endReq As New RequiredField(end_dateTextBox, "End Date")

            Case "physician"
                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")
                Dim titleReq As New RequiredField(Titleddl, "Title")
                Dim specReq As New RequiredField(Specialtyddl, "Specialty")
                Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim faxqReq As New RequiredField(faxtextbox, "Fax")

                pnlProvider.Visible = True

                pnlOtherAffilliation.Visible = False
                pnlAlliedHealth.Visible = False
                providerrbl.SelectedValue = "Yes"

            Case "non staff clinician"
                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")
                Dim titleReq As New RequiredField(Titleddl, "Title")
                Dim specReq As New RequiredField(Specialtyddl, "Specialty")
                Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim faxqReq As New RequiredField(faxtextbox, "Fax")

                pnlProvider.Visible = True
                pnlOtherAffilliation.Visible = False
                pnlAlliedHealth.Visible = False
                providerrbl.SelectedIndex = -1

            Case "resident"
                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")
                Dim titleReq As New RequiredField(Titleddl, "Title")
                Dim specReq As New RequiredField(Specialtyddl, "Specialty")
                Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim faxqReq As New RequiredField(faxtextbox, "Fax")

                pnlProvider.Visible = True
                pnlOtherAffilliation.Visible = False
                pnlAlliedHealth.Visible = False
                providerrbl.SelectedValue = "Yes"



            Case "vendor"

                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")

                Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim vendorRequired As New RequiredField(VendorNameTextBox, "Vendor")


                pnlOtherAffilliation.Visible = False
                pnlAlliedHealth.Visible = False
                pnlProvider.Visible = False
                providerrbl.SelectedIndex = -1


            Case "contract"

                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")

                Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")

                pnlOtherAffilliation.Visible = False
                pnlAlliedHealth.Visible = False
                pnlProvider.Visible = False
                providerrbl.SelectedIndex = -1

            Case "allied health"

                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")
                Dim titleReq As New RequiredField(Titleddl, "Title")
                Dim specReq As New RequiredField(Specialtyddl, "Specialty")
                Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim faxqReq As New RequiredField(faxtextbox, "Fax")

                'Dim otherAff As New RequiredField(OtherAffDescTextBox, "Other Affiliation Description")

                providerrbl.SelectedIndex = 0

                pnlAlliedHealth.Visible = True
                pnlProvider.Visible = True

                pnlOtherAffilliation.Visible = False

                providerrbl.SelectedValue = "Yes"



            Case "other"

                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")

                Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")

                'Dim otherAff As New RequiredField(OtherAffDescTextBox, "Other Affiliation Description")

                pnlOtherAffilliation.Visible = True

                pnlAlliedHealth.Visible = False
                pnlProvider.Visible = False
                providerrbl.SelectedIndex = -1



            Case Else


                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")

                pnlOtherAffilliation.Visible = False
                pnlAlliedHealth.Visible = False
                pnlProvider.Visible = False
                providerrbl.SelectedIndex = -1



        End Select


    End Sub
    Protected Function fillActionItemsTable(ByRef Actionlist As IList(Of Actions)) As Table

        Dim iRowCount = 1
        Dim dt As New Table
        Dim rwHeader As New TableRow

        Dim clActiondateHeader As New TableCell
        Dim clActionCodeDescheader As New TableCell
        Dim clTechHeader As New TableCell
        Dim clDescHeader As New TableCell


        clActionCodeDescheader.Text = "Action Type"
        clActiondateHeader.Text = "Action Date"
        clTechHeader.Text = "Name"
        clDescHeader.Text = "Description"


        rwHeader.Cells.Add(clActionCodeDescheader)
        rwHeader.Cells.Add(clActiondateHeader)
        rwHeader.Cells.Add(clTechHeader)
        rwHeader.Cells.Add(clDescHeader)



        rwHeader.Font.Bold = True
        rwHeader.BackColor = Color.FromName("#006666")
        rwHeader.ForeColor = Color.FromName("White")
        rwHeader.Height = Unit.Pixel(36)

        dt.Rows.Add(rwHeader)

        For Each AList In Actionlist


            Dim rwData As New TableRow

            Dim clActionDate As New TableCell
            Dim clActionCodeDesc As New TableCell
            Dim clTechName As New TableCell
            Dim clDescription As New TableCell

            clActionCodeDesc.Text = AList.ActionCodeDesc
            clActionDate.Text = AList.ActionDate
            clTechName.Text = AList.TechName
            clDescription.Text = AList.ActionDesc


            rwData.Cells.Add(clActionCodeDesc)
            rwData.Cells.Add(clActionDate)
            rwData.Cells.Add(clTechName)
            rwData.Cells.Add(clDescription)


            If iRowCount > 0 Then

                rwData.BackColor = Color.Bisque

            Else

                rwData.BackColor = Color.LightGray

            End If

            dt.Rows.Add(rwData)

            iRowCount = iRowCount * -1

        Next
        dt.CellPadding = 10
        dt.Width = New Unit("100%")


        Return dt

    End Function

    Protected Sub btnSubmitInvalid_Click(sender As Object, e As System.EventArgs) Handles btnSubmitInvalid.Click
        Dim thisData As New CommandsSqlAndOleDb("UpdValidateRequestItemsV4", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int)
        thisData.AddSqlProcParameter("@account_request_seq_num", AccountSeqNum, SqlDbType.Int)
        thisData.AddSqlProcParameter("@nt_login", Session("LoginID"), SqlDbType.NVarChar, 50)
        thisData.AddSqlProcParameter("@validate", "invalid", SqlDbType.NVarChar, 10)
        If Invalidddl.SelectedIndex > 0 Then
            thisData.AddSqlProcParameter("@Invalidcd", Invalidddl.SelectedValue, SqlDbType.NVarChar, 75)
        End If

        thisData.AddSqlProcParameter("@action_desc", InvalidDescTextBox.Text, SqlDbType.NVarChar, 255)


        thisData.ExecNonQueryNoReturn()

        Response.Redirect("./AccountRequestInfo2.aspx?requestnum=" & AccountRequestNum, True)

    End Sub

    Protected Sub btnCancelInvalid_Click(sender As Object, e As System.EventArgs) Handles btnCancelInvalid.Click
        PanInvalid.Visible = False
        Invalidddl.SelectedIndex = -1
        InvalidDescTextBox.Text = Nothing

    End Sub
    Protected Sub btnGoToRequest_Click(sender As Object, e As System.EventArgs) Handles btnGoToRequest.Click
        Response.Redirect("./AccountRequestInfo2.aspx?requestnum=" & AccountRequestNum, True)
    End Sub
    'Protected Sub LoadclientNursestations(ByVal iAppnum As Integer)

    '    Select Case lblAppSystem.Text
    '        Case "15"

    '            Dim cblCCMCNurBinder As New CheckBoxListBinder(cblCCMCNursestations, "SelAvailableSubAccountsByClientNumberV8", Session("EmployeeConn"))

    '            cblCCMCNurBinder.AddDDLCriteria("@client_num", iClientNum, SqlDbType.Int)
    '            cblCCMCNurBinder.AddDDLCriteria("@userClientNum", submitter_client_numLabel.Text, SqlDbType.Int)
    '            cblCCMCNurBinder.AddDDLCriteria("@FacilityCD", lblAppSystem.Text, SqlDbType.NVarChar)
    '            cblCCMCNurBinder.AddDDLCriteria("@ApplicationNum", iAppnum, SqlDbType.Int)
    '            cblCCMCNurBinder.BindData("SubApplicationnum", "SubAppDesc")


    '        Case "12"


    '            Dim cblDCMHNurBinder As New CheckBoxListBinder(cblDCMHNursestataions, "SelAvailableSubAccountsByClientNumberV8", Session("EmployeeConn"))
    '            cblDCMHNurBinder.AddDDLCriteria("@client_num", iClientNum, SqlDbType.Int)
    '            cblDCMHNurBinder.AddDDLCriteria("@userClientNum", submitter_client_numLabel.Text, SqlDbType.Int)
    '            cblDCMHNurBinder.AddDDLCriteria("@FacilityCD", lblAppSystem.Text, SqlDbType.NVarChar)
    '            cblDCMHNurBinder.AddDDLCriteria("@ApplicationNum", iAppnum, SqlDbType.Int)
    '            cblDCMHNurBinder.BindData("SubApplicationnum", "SubAppDesc")


    '        Case "19"

    '            Dim cblTaylorNurBinder As New CheckBoxListBinder(cblTaylorNursestataions, "SelAvailableSubAccountsByClientNumberV8", Session("EmployeeConn"))
    '            cblTaylorNurBinder.AddDDLCriteria("@client_num", iClientNum, SqlDbType.Int)
    '            cblTaylorNurBinder.AddDDLCriteria("@userClientNum", submitter_client_numLabel.Text, SqlDbType.Int)
    '            cblTaylorNurBinder.AddDDLCriteria("@FacilityCD", lblAppSystem.Text, SqlDbType.NVarChar)
    '            cblTaylorNurBinder.AddDDLCriteria("@ApplicationNum", iAppnum, SqlDbType.Int)
    '            cblTaylorNurBinder.BindData("SubApplicationnum", "SubAppDesc")

    '        Case "16"

    '            Dim cblSpringNurBinder As New CheckBoxListBinder(cblSpringfieldNursestataions, "SelAvailableSubAccountsByClientNumberV8", Session("EmployeeConn"))
    '            cblSpringNurBinder.AddDDLCriteria("@client_num", iClientNum, SqlDbType.Int)
    '            cblSpringNurBinder.AddDDLCriteria("@userClientNum", submitter_client_numLabel.Text, SqlDbType.Int)
    '            cblSpringNurBinder.AddDDLCriteria("@FacilityCD", lblAppSystem.Text, SqlDbType.NVarChar)
    '            cblSpringNurBinder.AddDDLCriteria("@ApplicationNum", iAppnum, SqlDbType.Int)
    '            cblSpringNurBinder.BindData("SubApplicationnum", "SubAppDesc")



    '    End Select


    'End Sub
    Protected Sub LoadNursestations(ByVal sAppnum As String)

        Dim thisdata As New CommandsSqlAndOleDb("SelSubAcctountsItemsV3", Session("EmployeeConn"))
        thisdata.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int, 9)

        thisdata.ExecNonQueryNoReturn()

        dtSubApplications = thisdata.GetSqlDataTable

        Dim SubAppnum As String = ""
        Dim SubAppcd As String = ""
        Dim subAppDesc As String = ""

        Select Case lblAppSystem.Text
            Case "15"

                For Each cblcItem As ListItem In cblCCMCNursestations.Items
                    cblcItem.Selected = False
                    'cblItem.Enabled = False
                Next

                Dim cblCCMCNurBinder As New CheckBoxListBinder(cblCCMCNursestations, "SelNursestationByFacility", Session("EmployeeConn"))
                cblCCMCNurBinder.AddDDLCriteria("@facilityCd", "15", SqlDbType.NVarChar)
                cblCCMCNurBinder.AddDDLCriteria("@SubApps", "yes", SqlDbType.NVarChar)
                cblCCMCNurBinder.AddDDLCriteria("@applicationnum", sAppnum, SqlDbType.NVarChar)
                cblCCMCNurBinder.BindData("SubApplicationNum", "NurseStationDesc")

                For Each row As DataRow In dtSubApplications.Rows
                    SubAppnum = row("SubApplicationNum")
                    SubAppcd = row("SubAppCd")
                    subAppDesc = row("SubAppDesc")

                    For Each cblcItem As ListItem In cblCCMCNursestations.Items
                        Dim sCurritem As String = cblcItem.Value

                        If sCurritem = SubAppnum Then
                            cblcItem.Selected = True
                        End If

                    Next
                Next

            Case "12"

                For Each cblDItem As ListItem In cblDCMHNursestataions.Items
                    cblDItem.Selected = False
                    'cblItem.Enabled = False
                Next

                Dim cblDCMHNurBinder As New CheckBoxListBinder(cblDCMHNursestataions, "SelNursestationByFacility", Session("EmployeeConn"))
                cblDCMHNurBinder.AddDDLCriteria("@facilityCd", "12", SqlDbType.NVarChar)
                cblDCMHNurBinder.AddDDLCriteria("@SubApps", "yes", SqlDbType.NVarChar)
                cblDCMHNurBinder.AddDDLCriteria("@applicationnum", sAppnum, SqlDbType.NVarChar)
                cblDCMHNurBinder.BindData("SubApplicationNum", "NurseStationDesc")



                For Each row As DataRow In dtSubApplications.Rows
                    SubAppnum = row("SubApplicationNum")
                    SubAppcd = row("SubAppCd")
                    subAppDesc = row("SubAppDesc")

                    For Each cblDItem As ListItem In cblDCMHNursestataions.Items
                        Dim sCurritem As String = cblDItem.Value

                        If sCurritem = SubAppnum Then
                            cblDItem.Selected = True
                        End If

                    Next
                Next

            Case "19"
                For Each cblTItem As ListItem In cblTaylorNursestataions.Items
                    cblTItem.Selected = False
                    'cblItem.Enabled = False
                Next

                Dim cblTaylorNurBinder As New CheckBoxListBinder(cblTaylorNursestataions, "SelNursestationByFacility", Session("EmployeeConn"))
                cblTaylorNurBinder.AddDDLCriteria("@facilityCd", "19", SqlDbType.NVarChar)
                cblTaylorNurBinder.AddDDLCriteria("@SubApps", "yes", SqlDbType.NVarChar)
                cblTaylorNurBinder.AddDDLCriteria("@applicationnum", sAppnum, SqlDbType.NVarChar)
                cblTaylorNurBinder.BindData("SubApplicationNum", "NurseStationDesc")


                For Each row As DataRow In dtSubApplications.Rows
                    SubAppnum = row("SubApplicationNum")
                    SubAppcd = row("SubAppCd")
                    subAppDesc = row("SubAppDesc")

                    For Each cblTItem As ListItem In cblTaylorNursestataions.Items
                        Dim sCurritem As String = cblTItem.Value

                        If sCurritem = SubAppnum Then
                            cblTItem.Selected = True
                        End If

                    Next
                Next

            Case "16"


                For Each cblSItem As ListItem In cblSpringfieldNursestataions.Items
                    cblSItem.Selected = False
                    'cblItem.Enabled = False
                Next

                Dim cblSpringNurBinder As New CheckBoxListBinder(cblSpringfieldNursestataions, "SelNursestationByFacility", Session("EmployeeConn"))
                cblSpringNurBinder.AddDDLCriteria("@facilityCd", "16", SqlDbType.NVarChar)
                cblSpringNurBinder.AddDDLCriteria("@SubApps", "yes", SqlDbType.NVarChar)
                cblSpringNurBinder.AddDDLCriteria("@applicationnum", sAppnum, SqlDbType.NVarChar)
                cblSpringNurBinder.BindData("SubApplicationNum", "NurseStationDesc")


                For Each row As DataRow In dtSubApplications.Rows
                    SubAppnum = row("SubApplicationNum")
                    SubAppcd = row("SubAppCd")
                    subAppDesc = row("SubAppDesc")

                    For Each cblSItem As ListItem In cblSpringfieldNursestataions.Items
                        Dim sCurritem As String = cblSItem.Value

                        If sCurritem = SubAppnum Then
                            cblSItem.Selected = True
                        End If

                    Next
                Next

        End Select





    End Sub
    Protected Sub Load365Locations(ByVal sAppnum As String)

        Dim thisdata As New CommandsSqlAndOleDb("SelSubAcctountsItemsV3", Session("EmployeeConn"))
        thisdata.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int, 9)
        thisdata.AddSqlProcParameter("@ApplicationNum", sAppnum, SqlDbType.Int, 9)

        thisdata.ExecNonQueryNoReturn()

        dtSubApplications = thisdata.GetSqlDataTable


        Dim SubAppnum As String = ""
        Dim SubAppcd As String = ""
        Dim subAppDesc As String = ""

        For Each cbl365Item As ListItem In cblo365.Items
            cbl365Item.Selected = False
            'cblItem.Enabled = False
        Next


        Dim cblo365Binder As New CheckBoxListBinder(cblo365, "Selo365Groups", Session("EmployeeConn"))
        cblo365Binder.BindData("SubApplicationNum", "SubAppDesc")

        For Each row As DataRow In dtSubApplications.Rows
            SubAppnum = row("SubApplicationNum")
            SubAppcd = row("SubAppCd")
            subAppDesc = row("SubAppDesc")

            For Each cblSItem As ListItem In cblo365.Items
                Dim sCurritem As String = cblSItem.Value

                If sCurritem = SubAppnum Then
                    cblSItem.Selected = True
                End If

            Next
        Next



    End Sub
    Protected Sub InsertNursestations()

        'Dim SubAppnum As String = ""
        'Dim SubAppcd As String = ""
        'Dim subAppDesc As String = ""

        Select Case AppNum
            Case 136
                For Each cblo365Item As ListItem In cblo365.Items
                    Dim sCurritem As String = cblo365Item.Value

                    If cblo365Item.Selected = True Then

                        'If sCurritem = 324 Then
                        '    Facility = 20
                        'Else
                        '    Facility = 26

                        'End If

                        Dim SubmitData As New CommandsSqlAndOleDb("UpdSubAccountItemsV6", Session("EmployeeConn"))
                        SubmitData.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int, 9)
                        SubmitData.AddSqlProcParameter("@ApplicationNum", AppNum, SqlDbType.Int, 9)
                        SubmitData.AddSqlProcParameter("@SubApplicationNum", sCurritem, SqlDbType.Int, 9)
                        'SubmitData.AddSqlProcParameter("@SubAppCd", cblcItem.Value, SqlDbType.NVarChar, 20)
                        SubmitData.AddSqlProcParameter("@new_login_name", LoginIDTextBox.Text, SqlDbType.NVarChar, 50)

                        SubmitData.AddSqlProcParameter("@nt_login", Session("LoginID"), SqlDbType.NVarChar, 50)

                        SubmitData.ExecNonQueryNoReturn()

                        'Dim SubmitData As New CommandsSqlAndOleDb("InsAccountSubApplicationV3", Session("EmployeeConn"))
                        'SubmitData.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int, 9)
                        'SubmitData.AddSqlProcParameter("@ApplicationNum", AppNum, SqlDbType.Int, 9)
                        'SubmitData.AddSqlProcParameter("@SubApplicationNum", cblo365Item.Value, SqlDbType.Int, 9)
                        'SubmitData.AddSqlProcParameter("@FacilityCd", Facility, SqlDbType.Int, 9)
                        'SubmitData.AddSqlProcParameter("@validate", ValidCd, SqlDbType.NVarChar, 20)

                        'SubmitData.AddSqlProcParameter("@who_nt_login", Session("LoginID"), SqlDbType.NVarChar, 50)

                        'SubmitData.ExecNonQueryNoReturn()


                    End If

                Next

            Case Else
                Select Case lblAppSystem.Text
                    Case "15"

                        For Each cblcItem As ListItem In cblCCMCNursestations.Items
                            Dim sCurritem As String = cblcItem.Value

                            If cblcItem.Selected = True Then


                                Dim SubmitData As New CommandsSqlAndOleDb("UpdSubAccountItemsV6", Session("EmployeeConn"))
                                SubmitData.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int, 9)
                                SubmitData.AddSqlProcParameter("@ApplicationNum", AppNum, SqlDbType.Int, 9)
                                SubmitData.AddSqlProcParameter("@SubApplicationNum", sCurritem, SqlDbType.Int, 9)
                                'SubmitData.AddSqlProcParameter("@SubAppCd", cblcItem.Value, SqlDbType.NVarChar, 20)
                                SubmitData.AddSqlProcParameter("@new_login_name", LoginIDTextBox.Text, SqlDbType.NVarChar, 50)

                                SubmitData.AddSqlProcParameter("@nt_login", Session("LoginID"), SqlDbType.NVarChar, 50)

                                SubmitData.ExecNonQueryNoReturn()

                            End If

                        Next

                    Case "12"

                        For Each cblDItem As ListItem In cblDCMHNursestataions.Items

                            Dim sCurritem As String = cblDItem.Value

                            If cblDItem.Selected = True Then


                                Dim SubmitData As New CommandsSqlAndOleDb("UpdSubAccountItemsV6", Session("EmployeeConn"))
                                SubmitData.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int, 9)
                                SubmitData.AddSqlProcParameter("@ApplicationNum", AppNum, SqlDbType.Int, 9)
                                SubmitData.AddSqlProcParameter("@SubApplicationNum", sCurritem, SqlDbType.Int, 9)
                                'SubmitData.AddSqlProcParameter("@SubAppCd", cblDItem.Value, SqlDbType.NVarChar, 20)
                                SubmitData.AddSqlProcParameter("@new_login_name", LoginIDTextBox.Text, SqlDbType.NVarChar, 50)

                                SubmitData.AddSqlProcParameter("@nt_login", Session("LoginID"), SqlDbType.NVarChar, 50)

                                SubmitData.ExecNonQueryNoReturn()

                            End If

                        Next

                    Case "19"

                        For Each cblTItem As ListItem In cblTaylorNursestataions.Items

                            Dim sCurritem As String = cblTItem.Value


                            If cblTItem.Selected = True Then

                                Dim SubmitData As New CommandsSqlAndOleDb("UpdSubAccountItemsV6", Session("EmployeeConn"))
                                SubmitData.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int, 9)
                                SubmitData.AddSqlProcParameter("@ApplicationNum", AppNum, SqlDbType.Int, 9)
                                SubmitData.AddSqlProcParameter("@SubApplicationNum", sCurritem, SqlDbType.Int, 9)
                                'SubmitData.AddSqlProcParameter("@SubAppCd", cblTItem.Value, SqlDbType.NVarChar, 20)
                                SubmitData.AddSqlProcParameter("@new_login_name", LoginIDTextBox.Text, SqlDbType.NVarChar, 50)

                                SubmitData.AddSqlProcParameter("@nt_login", Session("LoginID"), SqlDbType.NVarChar, 50)

                                SubmitData.ExecNonQueryNoReturn()

                            End If

                        Next

                    Case "16"


                        For Each cblSItem As ListItem In cblSpringfieldNursestataions.Items
                            Dim sCurritem As String = cblSItem.Value


                            If cblSItem.Selected = True Then
                                'cblItem.Enabled = False

                                Dim SubmitData As New CommandsSqlAndOleDb("UpdSubAccountItemsV6", Session("EmployeeConn"))
                                SubmitData.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int, 9)
                                SubmitData.AddSqlProcParameter("@ApplicationNum", AppNum, SqlDbType.Int, 9)
                                SubmitData.AddSqlProcParameter("@SubApplicationNum", sCurritem, SqlDbType.Int, 9)
                                'SubmitData.AddSqlProcParameter("@SubAppCd", cblSItem.Value, SqlDbType.NVarChar, 20)
                                SubmitData.AddSqlProcParameter("@new_login_name", LoginIDTextBox.Text, SqlDbType.NVarChar, 50)

                                SubmitData.AddSqlProcParameter("@nt_login", Session("LoginID"), SqlDbType.NVarChar, 50)

                                SubmitData.ExecNonQueryNoReturn()

                            End If

                        Next

                End Select

        End Select



    End Sub
End Class
