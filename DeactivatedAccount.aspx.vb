﻿Imports System.IO
Imports System.Data '
Imports System.Data.SqlClient
Imports App_Code
Imports AjaxControlToolkit
Imports C1.Web.UI.Controls.C1Window
Imports System.Net
Imports System.Net.Mail
Imports System.Net.Mail.SmtpClient
Partial Class DeactivatedAccount
    Inherits MyBasePage '   Inherits System.Web.UI.Page
    Dim AccountRequestNum As Integer
    Dim sActivitytype As String = ""
    Dim FormSignOn As New FormSQL()
    Dim VendorFormSignOn As FormSQL
    Dim NewRequestForm As New FormSQL()

    Dim iClientNum As String

    Dim employeetype As String
    Dim bHasError As Boolean
    Dim ValidationStatus As String = ""
    Dim sItemComplet As String = ""
    Dim RequestoName As String
    Dim iRequestorNum As String
    Dim sIsPhysician As String = ""

    Dim thisRole As String
    Dim mainRole As String

    Dim AccountCount As Integer = 0
    Dim OpenRequestCount As Integer = 0
    Dim AccountRequest As AccountRequest

    Dim dsRehire As DataSet
    Dim openRehireReqnum As Integer
    Dim openRehireAccountReqnum As Integer

    Dim Mailobj As System.Net.Mail.SmtpClient = New System.Net.Mail.SmtpClient
    Dim Mailmsg As New System.Net.Mail.MailMessage
    Dim MailAddress As System.Net.Mail.MailAddress
    Dim mailAttachment As System.Net.Mail.Attachment

    Private dtAccounts As New DataTable
    Dim UserInfo As UserSecurity
    Private AddAccountRequestNum As Integer = 0
    Private DelAccountRequestNum As Integer = 0
    Private RemoveAccts As DataTable
    Private dtPendingRequests As DataTable
    Private dtRemoveAccounts As New DataTable
    Private dtAddAccounts As New DataTable
    Private dtDocmster As New DataTable
    Private dtTCLUserTypes As New DataTable
    Private dtRehire As New DataTable

    Dim BusLog As AccountBusinessLogic
    Dim thisdata As CommandsSqlAndOleDb
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        iClientNum = Request.QueryString("ClientNum")
        iRequestorNum = Request.QueryString("RequestorNum")
        RequestoName = Request.QueryString("RequestorName")

        Dim dbAccess As New CkhsEmployeeDbAccess(Session("EmployeeConn"))
        Dim ckhsEmployee = dbAccess.GetDeactivatedEmployee(iClientNum)

        UserInfo = New UserSecurity(Session("LoginID"), Session("EmployeeConn"))



        FormSignOn = New FormSQL(Session("EmployeeConn"), "SelDeactivatedClientByNumberV20", "InsAccountRequestV20", "", Page)
        FormSignOn.AddSelectParm("@client_num", iClientNum, SqlDbType.Int, 6)

        If Session("SecurityLevelNum") < 64 Then
            Response.Redirect("~/SimpleSearch.aspx", True)

        End If


        If Not IsPostBack Then

            userSubmittingLabel.Text = UserInfo.UserName
            SubmittingOnBehalfOfNameLabel.Text = UserInfo.UserName
            submitter_client_numLabel.Text = UserInfo.ClientNum
            requestor_client_numLabel.Text = UserInfo.ClientNum
            userDepartmentCD.Text = UserInfo.DepartmentCd

            If iRequestorNum <> "" Then
                requestor_client_numLabel.Text = iRequestorNum
                'RequestorName
                SubmittingOnBehalfOfNameLabel.Text = RequestoName
            End If


            Dim rblAffiliateBinder As New RadioButtonListBinder(emptypecdrbl, "SelAffiliationsV5", Session("EmployeeConn"))

            Select Case ckhsEmployee.RoleNum
                Case "782", "1197", "1194"
                    rblAffiliateBinder.AddDDLCriteria("@type", "phys", SqlDbType.NVarChar)
                Case Else
                    If ckhsEmployee.DoctorMasterNum > 0 Then
                        EmpDoclbl.Visible = True
                        EmpDoctorTextBox.Text = ckhsEmployee.DoctorMasterNum.ToString
                        EmpDoctorTextBox.Visible = True
                        If Session("SecurityLevelNum") < 90 Then
                            EmpDoctorTextBox.Enabled = False
                        End If

                    Else
                        rblAffiliateBinder.AddDDLCriteria("@type", "", SqlDbType.NVarChar)
                    End If

            End Select

            rblAffiliateBinder.ToolTip = False
            rblAffiliateBinder.BindData("RoleNum", "AffiliationDisplay")

            mainRole = ckhsEmployee.RoleNum
            RoleNumLabel.Text = ckhsEmployee.RoleNum
            emptypecdrbl.SelectedValue = ckhsEmployee.RoleNum
            PositionNumTextBox.Text = ckhsEmployee.PositionNum


            emptypecdLabel.Text = ckhsEmployee.EmpTypeCd
            HDemployeetype.Text = ckhsEmployee.EmpTypeCd

            ClientnameHeader.Text = ckhsEmployee.FullName

            Dim provider As String
            provider = ckhsEmployee.Provider

            FormSignOn.FillForm()
            CheckAffiliation()



            Dim OpenRehireRequestdata As New CommandsSqlAndOleDb("SelRehireRequest", Session("EmployeeConn"))
            OpenRehireRequestdata.AddSqlProcParameter("@client_num", iClientNum, SqlDbType.Int, 20)
            OpenRehireRequestdata.ExecNonQueryNoReturn()

            dtRehire = OpenRehireRequestdata.GetSqlDataTable

            dsRehire = OpenRehireRequestdata.GetSqlDataset()

            If dtRehire.Rows.Count > 0 Then

                openRehireReqnum = IIf(IsDBNull(dsRehire.Tables(0).Rows(0).Item("RehireRequestNum")), 0, dsRehire.Tables(0).Rows(0).Item("RehireRequestNum"))

                openRehireAccountReqnum = IIf(IsDBNull(dsRehire.Tables(0).Rows(0).Item("Account_Request_num")), 0, dsRehire.Tables(0).Rows(0).Item("Account_Request_num"))
                RehireRequestNumTextBox.Text = openRehireReqnum

                RehireAccountReqnumTextBox.Text = openRehireAccountReqnum


                If openRehireReqnum > 0 Then
                    TerminateLabel.Visible = True

                    If openRehireAccountReqnum > 0 Then
                        TerminateLabel.Text = "Client Has an Open Rehire Account Request"

                        btnUpdate.Text = "Go To Account Request"

                    Else
                        TerminateLabel.Text = "Client Has an Open Rehire Request"

                        btnUpdate.Text = "Go To SignOn Form"

                    End If

                End If
            End If




            Dim thisdata As New CommandsSqlAndOleDb("SelAllClientAccountsByNumberV4", Session("EmployeeConn"))
            thisdata.AddSqlProcParameter("@client_num", iClientNum, SqlDbType.Int, 20)
            thisdata.AddSqlProcParameter("@deactivated", "yes", SqlDbType.NVarChar, 6)

            thisdata.ExecNonQueryNoReturn()

            ViewState("dtAccounts") = thisdata.GetSqlDataTable

            Dim thisdataDoc As New CommandsSqlAndOleDb("SelPhysiciansDoocV2", Session("EmployeeConn"))
            thisdataDoc.AddSqlProcParameter("@clientnum", iClientNum, SqlDbType.Int, 20)

            thisdataDoc.ExecNonQueryNoReturn()

            ViewState("dtDocmster") = thisdataDoc.GetSqlDataTable


            Dim ddlBinder As New DropDownListBinder

            Dim ddlBinderV2 As DropDownListBinder
            ddlBinderV2 = New DropDownListBinder(Rolesddl, "SelEmployeeRolesV3", Session("EmployeeConn"))

            ddlBinderV2.AddDDLCriteria("@RoleNum", emptypecdrbl.SelectedValue, SqlDbType.NVarChar)

            If PositionNumTextBox.Text <> "" Then
                ddlBinderV2.AddDDLCriteria("@positionNum", PositionNumTextBox.Text, SqlDbType.NVarChar)

            End If
            ddlBinderV2.BindData("rolenum", "roledesc")

            ' ddlBinder.BindData(Rolesddl, "SelEmployeeRolesV2", "rolenum", "RoleDesc", Session("EmployeeConn"))

            ddlBinder.BindData(facilitycdddl, "SelFacilityCodes", "facility_cd", "facility_short_name", Session("EmployeeConn"))
            ddlBinder.BindData(Specialtyddl, "SelSpecialtyCodesV2", "specialty", "specialty", Session("EmployeeConn"))
            ddlBinder.BindData(entitycdDDL, "selEntityCodesV4", "entity_cd", "hosp_desc", Session("EmployeeConn"))
            ' ddlBinder.BindData(AlliedHealthddl, "SelAlliedHealthPos", "AlliedHealthPosShort", "AlliedHealthPosShort", Session("EmployeeConn"))
            ddlBinder.BindData(suffixddl, "SelSuffixCodes", "Suffix", "Suffix", Session("EmployeeConn"))

            Dim ddlInvisionBinder As New DropDownListBinder(group_nameddl, "SelPhysGroupsV5", Session("EmployeeConn"))
            ddlInvisionBinder.AddDDLCriteria("@GroupTypeCd", "invision", SqlDbType.NVarChar)
            ddlInvisionBinder.AddDDLCriteria("@clientpage", "yes", SqlDbType.NVarChar)

            ddlInvisionBinder.BindData("Group_num", "group_name")

            'Dim ddlNonPerferBinder As New DropDownListBinder(Nonperferdddl, "SelPhysGroupsV5", Session("EmployeeConn"))
            'ddlNonPerferBinder.AddDDLCriteria("@GroupTypeCd", "NonPerGrp", SqlDbType.NVarChar)
            'ddlNonPerferBinder.AddDDLCriteria("@clientpage", "yes", SqlDbType.NVarChar)

            'ddlNonPerferBinder.BindData("Group_num", "group_name")

            Dim ddGnumberBinder As New DropDownListBinder(LocationOfCareIDddl, "SelPhysGroupsV5", Session("EmployeeConn"))
            ddGnumberBinder.AddDDLCriteria("@GroupTypeCd", "CHMG", SqlDbType.NVarChar)
            ddGnumberBinder.AddDDLCriteria("@clientpage", "yes", SqlDbType.NVarChar)

            ddGnumberBinder.BindData("Group_num", "group_name")

            'Dim ddCommLocationOfCare As New DropDownListBinder(CommLocationOfCareIDddl, "SelPhysGroupsV5", Session("EmployeeConn"))
            'ddCommLocationOfCare.AddDDLCriteria("@GroupTypeCd", "CommNum", SqlDbType.NVarChar)
            'ddCommLocationOfCare.AddDDLCriteria("@clientpage", "yes", SqlDbType.NVarChar)

            'ddCommLocationOfCare.BindData("Group_num", "group_name")

            'Dim ddCoverageGroupddl As New DropDownListBinder(CoverageGroupddl, "SelPhysGroupsV5", Session("EmployeeConn"))
            'ddCoverageGroupddl.AddDDLCriteria("@GroupTypeCd", "coverage", SqlDbType.NVarChar)
            'ddCoverageGroupddl.AddDDLCriteria("@clientpage", "yes", SqlDbType.NVarChar)

            'ddCoverageGroupddl.BindData("Group_num", "Group_name")



            If PositionRoleNumLabel.Text <> "" Then
                Rolesddl.SelectedValue = PositionRoleNumLabel.Text
            ElseIf RoleNumLabel.Text <> "" Then
                Rolesddl.SelectedValue = RoleNumLabel.Text
                PositionRoleNumLabel.Text = RoleNumLabel.Text
                PositionRoleNumTextBox.Text = RoleNumLabel.Text
            Else
                Rolesddl.SelectedIndex = -1
            End If

            If entitycdLabel.Text = "" Then
                entitycdDDL.SelectedIndex = -1
            Else
                Dim entitycd As String = entitycdLabel.Text

                entitycd = entitycd.Trim
                entitycdDDL.SelectedValue = entitycd

            End If

            If facilitycdLabel.Text = "" Then
                facilitycdddl.SelectedIndex = -1
            Else
                facilitycdddl.SelectedValue = facilitycdLabel.Text
            End If

            ' Gnumber
            If LocationOfCareIDLabel.Text = "" Then
                LocationOfCareIDddl.SelectedIndex = -1
            Else
                LocationOfCareIDddl.SelectedValue = LocationOfCareIDLabel.Text
            End If

            'Comm Gnumber
            'If CommLocationOfCareIDLabel.Text = "" Then
            '    CommLocationOfCareIDddl.SelectedIndex = -1
            'Else
            '    CommLocationOfCareIDddl.SelectedValue = CommLocationOfCareIDLabel.Text
            'End If

            ''Coverage number
            'If CoverageGroupNumLabel.Text = "" Then
            '    CoverageGroupddl.SelectedIndex = -1
            'Else
            '    CoverageGroupddl.SelectedValue = CoverageGroupNumLabel.Text

            'End If

            ' Invision Number
            If InvisionGroupNumTextBox.Text = "" Then
                group_nameddl.SelectedIndex = -1
            Else
                group_nameddl.SelectedValue = InvisionGroupNumTextBox.Text

            End If

            'non perfered
            'If NonPerferedGroupnumLabel.Text = "" Then
            '    Nonperferdddl.SelectedIndex = -1
            'Else
            '    Nonperferdddl.SelectedValue = NonPerferedGroupnumLabel.Text

            'End If

            Dim ddbDepartmentBinder As New DropDownListBinder(departmentcdddl, "selDepartmentCodesV4", Session("EmployeeConn"))
            ddbDepartmentBinder.AddDDLCriteria("@entity_cd", entitycdDDL.SelectedValue, SqlDbType.NVarChar)
            ddbDepartmentBinder.BindData("department_cd", "department_name")

            If departmentcdLabel.Text = "" Then
                departmentcdddl.SelectedIndex = -1
            Else
                departmentcdddl.SelectedValue = departmentcdLabel.Text

            End If


            If SpecialtyLabel.Text = "" Then
                Specialtyddl.SelectedIndex = -1
            Else
                Specialtyddl.SelectedValue = SpecialtyLabel.Text

            End If

            If groupnameTextBox.Text = "" Then
                group_nameddl.SelectedIndex = -1
            Else
                group_nameddl.SelectedValue = groupnumTextBox.Text
            End If

            If suffixlabel.Text = "" Then
                suffixddl.SelectedIndex = -1
            Else
                suffixddl.SelectedValue = suffixlabel.Text
            End If

            ddlBinder = New DropDownListBinder(buildingcdddl, "sel_building_by_facility", Session("EmployeeConn"))
            ddlBinder.AddDDLCriteria("@facility_cd", facilitycdddl.SelectedValue, SqlDbType.NVarChar)
            ddlBinder.BindData("building_cd", "building_name")


            If buildingcdTextBox.Text = "" Then
                buildingcdddl.SelectedValue = -1
            Else
                Dim sBuilding As String = buildingcdTextBox.Text
                buildingcdddl.SelectedValue = sBuilding.Trim
                If buildingnameLabel.Text <> "" Then

                    buildingcdddl.SelectedItem.Text = buildingnameLabel.Text
                End If

            End If

            If buildingcdddl.SelectedIndex <> -1 Then

                ddlBinder = New DropDownListBinder(Floorddl, "SelFloorsByBuilding", Session("EmployeeConn"))
                ddlBinder.AddDDLCriteria("@facility_cd", facilitycdddl.SelectedValue, SqlDbType.NVarChar)
                ddlBinder.AddDDLCriteria("@building_cd", buildingcdddl.SelectedValue, SqlDbType.NVarChar)

                ddlBinder.BindData("floor_no", "floor_no")

                If FloorTextBox.Text = "" Then
                    Floorddl.SelectedValue = -1
                Else
                    Dim sFloor As String = FloorTextBox.Text
                    Floorddl.SelectedValue = sFloor.Trim

                End If



            End If

            Select Case emptypecdrbl.SelectedValue
                Case "782", "1197"

                    SiemensEmpNumTextBox.Visible = True
                    Emplbl.Visible = True

                    If Titleddl.SelectedIndex <= 0 Then
                        Dim ddbTitleBinder As New DropDownListBinder(Titleddl, "SelTitlesV4", Session("EmployeeConn"))
                        ddbTitleBinder.AddDDLCriteria("@titleType", "Physician", SqlDbType.NVarChar)
                        ddbTitleBinder.BindData("TitleShortDesc", "TitleDesc")

                        If TitleLabel.Text = "" Then
                            Titleddl.SelectedIndex = -1
                        Else
                            Titleddl.SelectedValue = TitleLabel.Text

                        End If

                    End If



                    If SpecialtyLabel.Text = "" Then
                        Specialtyddl.SelectedIndex = -1
                    Else
                        Specialtyddl.SelectedValue = SpecialtyLabel.Text

                    End If

                    'If DoctorMasterNumTextBox.Text <> "" Then
                    '    docMstrPnl.Visible = True
                    'End If

                    If hanrbl.SelectedValue.ToString = "Yes" Then
                        LocationOfCareIDddl.Visible = True
                        ckhnlbl.Visible = True

                        LocationOfCareIDddl.BackColor() = Color.Yellow
                        LocationOfCareIDddl.BorderColor() = Color.Black
                        LocationOfCareIDddl.BorderStyle() = BorderStyle.Solid
                        LocationOfCareIDddl.BorderWidth = Unit.Pixel(2)

                        lblprov.Visible = True
                        providerrbl.Visible = True

                    Else
                        LocationOfCareIDddl.Visible = False
                        ckhnlbl.Visible = False

                        lblprov.Visible = False
                        providerrbl.Visible = False

                    End If



                Case "1194"
                    'Dim ddlBinder As New DropDownListBinder
                    SiemensEmpNumTextBox.Visible = True
                    Emplbl.Visible = True


                    Dim ddbTitleBinder As New DropDownListBinder(Titleddl, "SelTitlesV4", Session("EmployeeConn"))
                    ddbTitleBinder.AddDDLCriteria("@titleType", "Resident", SqlDbType.NVarChar)
                    ddbTitleBinder.BindData("TitleShortDesc", "TitleDesc")

                    Dim ddlRoleBinder As New DropDownListBinder(Rolesddl, "SelEmployeeRolesV2", Session("EmployeeConn"))
                    ddlRoleBinder.AddDDLCriteria("@RoleNum", "1194", SqlDbType.NVarChar)
                    ddlRoleBinder.BindData("rolenum", "RoleDesc")

                    If TitleLabel.Text = "" Then
                        Titleddl.SelectedIndex = -1
                    Else
                        Titleddl.SelectedValue = TitleLabel.Text

                    End If

                    If SpecialtyLabel.Text = "" Then
                        Specialtyddl.SelectedIndex = -1
                    Else
                        Specialtyddl.SelectedValue = SpecialtyLabel.Text

                    End If

                    If TitleLabel.Text = "" Then
                        Title2Label.Text = "Resident"

                    Else
                        Title2Label.Text = TitleLabel.Text

                    End If
                    Title2Label.Visible = True
                    Titleddl.Visible = False

                    SpecialtyLabel.Text = "Resident"
                    SpecialtyLabel.Visible = True
                    Specialtyddl.Visible = False

                    credentialedDateTextBox.Enabled = False
                    credentialedDateTextBox.CssClass = "style2"

                    'CredentialedDateDCMHTextBox.Enabled = False
                    'CredentialedDateDCMHTextBox.CssClass = "style2"

                    CCMCadmitRightsrbl.SelectedValue = "No"
                    'CCMCadmitRightsrbl.Enabled = False

                    'DCMHadmitRightsrbl.SelectedValue = "No"
                    'DCMHadmitRightsrbl.Enabled = False

                    If hanrbl.SelectedValue.ToString = "Yes" Then
                        LocationOfCareIDddl.Visible = True
                        ckhnlbl.Visible = True

                        LocationOfCareIDddl.BackColor() = Color.Yellow
                        LocationOfCareIDddl.BorderColor() = Color.Black
                        LocationOfCareIDddl.BorderStyle() = BorderStyle.Solid
                        LocationOfCareIDddl.BorderWidth = Unit.Pixel(2)

                        lblprov.Visible = True
                        providerrbl.Visible = True

                    Else
                        LocationOfCareIDddl.Visible = False
                        ckhnlbl.Visible = False

                        lblprov.Visible = False
                        providerrbl.Visible = False

                    End If

                    pnlProvider.Visible = True


                Case "1195", "1198"
                    ' load vendor list 
                    'make vendor text box invisiable
                    ' make ddl appear
                    Vendorddl.Visible = True
                    VendorNameTextBox.Visible = False

                    SiemensEmpNumTextBox.Visible = False
                    Emplbl.Visible = False

                    Dim ddbVendorBinder As New DropDownListBinder(Vendorddl, "SelVendors", Session("EmployeeConn"))
                    ddbVendorBinder.AddDDLCriteria("@type", "vendor", SqlDbType.NVarChar)
                    ddbVendorBinder.BindData("VendorNum", "VendorName")


                    If VendorNameTextBox.Text = "" Then
                        Vendorddl.SelectedIndex = -1
                    Else
                        Vendorddl.SelectedValue = VendorNumLabel.Text

                    End If

                    If VendorNumLabel.Text = "0" Then
                        otherVendorlbl.Visible = True
                        VendorUnknowlbl.Text = VendorNameTextBox.Text
                        VendorUnknowlbl.Visible = True
                    End If

                Case "1199", "1196"
                    SiemensEmpNumTextBox.Visible = False
                    Emplbl.Visible = False

                Case "1193"
                    SiemensEmpNumTextBox.Visible = True
                    Emplbl.Visible = True


                Case Else


            End Select


            If AccountRequestTypeLabel.Text = "terminate" Then
                TerminateLabel.Visible = True

                'btnSelectOnBehalfOf.Visible = False
                btnUpdate.Visible = False
                'btnSubmitDemoChange.Visible = False

            End If

            If emptypecdLabel.Text = "other" Then
                userpositiondescTextBox.Enabled = False
            End If


            firstnameTextBox.Focus()

            If SiemensEmpNumTextBox.Text = "" Then
                siemdesclbl.Text = "User Position:"
                'userpositiondescLabel.Text = ""

            End If
        End If
        ' End Post Back


        If Session("SecurityLevelNum") < 64 Then
            Dim ctrl As Control = DataCheckingAndValidation.SetChildControlsRecursiveToEnabledFalse(tpDemographics, False)

            netlbl.Visible = False
            loginnameTextBox.Visible = False

            'btnPrintClient.Visible = False
            'btnSelectOnBehalfOf.Visible = False

            Select Case emptypecdrbl.SelectedValue
                Case "782", "1197", "1194"
                    sIsPhysician = "yes"
            End Select

        End If

        Select Case emptypecdrbl.SelectedValue
            Case "782", "1197" ', "1194"
                sIsPhysician = "yes"

                'Case "physician", "non staff clinician", "resident", "allied health"
                ' no one should change a physicians who is a Crozer employee to any other Role
                If SiemensEmpNumTextBox.Text <> "" And DoctorMasterNumTextBox.Text <> "" Then
                    emptypecdrbl.Enabled = False
                End If


        End Select


        If Session("SecurityLevelNum") < 50 Then

			btnUpdate.Visible = False
			btnReactivate.Visible = False
			'btnSubmitDemoChange.Visible = False
			SiemensEmpNumTextBox.Visible = False

        End If

        ' not to allow 55 request accounts for themeselves
        If Session("SecurityLevelNum") = 55 And Session("ClientNum") = iClientNum Then

			'btnPrintClient.Visible = False
			'btnSelectOnBehalfOf.Visible = False
			btnUpdate.Visible = False
			btnReactivate.Visible = False
			'btnSubmitDemoChange.Visible = False

		End If

        If Session("SecurityLevelNum") <= 55 And AccountRequestTypeLabel.Text <> "" Then
            TerminateLabel.Text = "This Client has open an request to Add more Accounts please contact Help Desk 15-2610."
            TerminateLabel.Visible = True
			btnUpdate.Visible = False
			btnReactivate.Visible = False
			'btnSubmitDemoChange.Visible = False

		End If


        If emptypecdrbl.SelectedValue <> RoleNumLabel.Text Then
            SpecialtyLabel.Text = ""
            mainRole = emptypecdrbl.SelectedValue
            RoleNumLabel.Text = emptypecdrbl.SelectedValue
            emptypecdLabel.Text = emptypecdrbl.SelectedValue
            TitleLabel.Text = Nothing
            Select Case emptypecdrbl.SelectedValue
                Case "782", "1197"
                    sIsPhysician = "yes"

                    'Case "physician", "non staff clinician", "resident"

                    Dim ddbTitleBinder As New DropDownListBinder(Titleddl, "SelTitlesV4", Session("EmployeeConn"))
                    ddbTitleBinder.AddDDLCriteria("@titleType", "Physician", SqlDbType.NVarChar)
                    ddbTitleBinder.BindData("TitleShortDesc", "TitleDesc")


                Case "1194"
                    'Dim ddlBinder As New DropDownListBinder
                    sIsPhysician = "yes"

                    Dim ddbTitleBinder As New DropDownListBinder(Titleddl, "SelTitlesV4", Session("EmployeeConn"))
                    ddbTitleBinder.AddDDLCriteria("@titleType", "Resident", SqlDbType.NVarChar)
                    ddbTitleBinder.BindData("TitleShortDesc", "TitleDesc")


            End Select

            If TitleLabel.Text = "" Then
                Titleddl.SelectedIndex = -1
            Else
                Titleddl.SelectedValue = TitleLabel.Text

            End If

            If SpecialtyLabel.Text = "" Then
                Specialtyddl.SelectedIndex = -1
            Else
                Specialtyddl.SelectedValue = SpecialtyLabel.Text

            End If

        End If

        If mainRole = "1194" Then

            SpecialtyLabel.Text = "Resident"
            SpecialtyLabel.Visible = True
            Specialtyddl.Visible = False

            credentialedDateTextBox.Enabled = False
            'credentialedDateTextBox.CssClass = "style2"

            'CredentialedDateDCMHTextBox.Enabled = False
            'CredentialedDateDCMHTextBox.CssClass = "style2"

            CCMCadmitRightsrbl.SelectedValue = "No"
            CCMCadmitRightsrbl.Enabled = False

            'DCMHadmitRightsrbl.SelectedValue = "No"
            'DCMHadmitRightsrbl.Enabled = False

        End If





        groupnumTextBox.Enabled = False
        InvisionGroupNumTextBox.Enabled = False


        If Session("SecurityLevelNum") = 82 Or Session("SecurityLevelNum") = 95 Or Session("LoginID").ToString.ToLower = "geip00" Or Session("LoginID") = "burgina" Or Session("LoginID").ToString.ToLower = "melej" Then
            'Session("SecurityLevelNum") = 82 Or Session("LoginID") = "melej" Or Session("LoginID") = "geip00" Or Session("LoginID") = "RalphC" Then
            Dim ctrl As Control = DataCheckingAndValidation.SetChildControlsRecursiveToEnabledFalse(tpDemographics, True)

            credentialedDateTextBox.Enabled = False
            credentialedDateTextBox.CssClass = "style2"

            'CredentialedDateDCMHTextBox.Enabled = False
            'CredentialedDateDCMHTextBox.CssClass = "style2"

            'credentialedDateTextBox.Enabled = True
            'credentialedDateTextBox.CssClass = "calenderClass"

            'CredentialedDateDCMHTextBox.Enabled = True
            'CredentialedDateDCMHTextBox.CssClass = "calenderClass"
            Select Case emptypecdrbl.SelectedValue
                Case "782", "1197" ', "1194"

                    'Case "physician", "non staff clinician", "resident", "allied health"
                    ' no one should change a physicians who is a Crozer employee to any other Role
                    If SiemensEmpNumTextBox.Text <> "" And DoctorMasterNumTextBox.Text <> "" Then
                        emptypecdrbl.Enabled = False
                    End If


            End Select
            ' show button non privideged
            'If CCMCadmitRightsrbl.SelectedValue.ToLower = "yes" Then
            '    btnNonPrivileged.Visible = True
            'End If


            ' show button non privideged
            'If DCMHadmitRightsrbl.SelectedValue.ToLower = "yes" Then
            '    btnNonPrivileged.Visible = True
            'End If

            If CCMCadmitRightsrbl.SelectedValue.ToLower = "no" Then

                credentialedDateTextBox.Enabled = False
                credentialedDateTextBox.CssClass = "style2"

            End If

            'If DCMHadmitRightsrbl.SelectedValue.ToLower = "no" Then
            '	CredentialedDateDCMHTextBox.Enabled = False
            '	CredentialedDateDCMHTextBox.CssClass = "style2"

            'End If

        Else

            'OpenRequestCount


            credentialedDateTextBox.Enabled = False
            'CCMCadmitRightsrbl.Enabled = False
            '    credentialedDateTextBox.CssClass = "style2"

            'CredentialedDateDCMHTextBox.Enabled = False
            ' DCMHadmitRightsrbl.Enabled = False
            '    CredentialedDateDCMHTextBox.CssClass = "style2"


        End If

        If mainRole = "1195" Or mainRole = "1198" Then

            VendorNameTextBox.Enabled = True
        Else
            VendorNameTextBox.Enabled = False

        End If

        'If siemensempnumTextBox.Text = "" Then
        '    If sIsPhysician <> "yes" Then


        '        If AccountCount = 0 And OpenRequestCount = 0 Then

        '            If Session("SecurityLevelNum") > 79 And Session("SecurityLevelNum") <> 82 Then
        '                btnDeactivateClient.Visible = True

        '            End If

        '        End If

        '    End If

        'End If


        TCLTextBox.Enabled = False
        UserTypeDescTextBox.Enabled = False
        UserTypeCdTextBox.Enabled = False


        ' Only super can modify

        If Session("SecurityLevelNum") < 89 Then
            Dim ctrl As Control = DataCheckingAndValidation.SetChildControlsRecursiveToEnabledFalse(tpDemographics, False)



        End If

        If RehireRequestNumTextBox.Text <> "" Then
            openRehireReqnum = CInt(RehireRequestNumTextBox.Text)
        End If

        If RehireAccountReqnumTextBox.Text <> "" Then
            openRehireAccountReqnum = CInt(RehireAccountReqnumTextBox.Text)

        End If


        fillOpenRequestTable()
        CurrentAccounts()
        fillClosedRequestTable()

        fillRequestItemsTable()

        'btnUpdate

        If Session("SecurityLevelNum") = 95 Or Session("LoginID").ToString.ToLower = "geip00" Or Session("LoginID").ToString.ToLower = "melej" Or Session("LoginID").ToString.ToLower = "burgina" Then
            btnUpdate.Visible = True
            btnReactivate.Visible = True
        Else
            btnUpdate.Visible = False
			btnReactivate.Visible = False
		End If




    End Sub
    Protected Sub fillRequestItemsTable()


        Dim thisData As New CommandsSqlAndOleDb("SelRFSTicketsByClient", Session("CSCConn"))
        thisData.AddSqlProcParameter("@clientnum", iClientNum, SqlDbType.Int)

        Dim iRowCount = 1

        Dim dt As New DataTable
        dt = thisData.GetSqlDataTable

        Dim rwHeader As New TableRow

        Dim clBtnheader As New TableCell
        Dim clRequestTypeHeader As New TableCell
        Dim clDateEnteredHeader As New TableCell
        Dim clStatusHeader As New TableCell
        Dim clDescheader As New TableCell

        clBtnheader.Text = ""
        clRequestTypeHeader.Text = "Type"
        clDateEnteredHeader.Text = "Date Entered"
        clStatusHeader.Text = "Status"
        clDescheader.Text = "Description"


        rwHeader.Cells.Add(clBtnheader)
        rwHeader.Cells.Add(clRequestTypeHeader)
        rwHeader.Cells.Add(clDateEnteredHeader)
        rwHeader.Cells.Add(clStatusHeader)
        rwHeader.Cells.Add(clDescheader)


        rwHeader.Font.Bold = True
        rwHeader.BackColor = Color.FromName("#006666")
        rwHeader.ForeColor = Color.FromName("White")
        rwHeader.Height = Unit.Pixel(36)

        tblRFSTickets.Rows.Add(rwHeader)

        For Each drRow As DataRow In dt.Rows


            Dim rwData As New TableRow

            Dim clBtn As New TableCell

            Dim clReqNum As New TableCell

            Dim claccountRequestNum As New TableCell
            Dim clRequestTypeData As New TableCell
            Dim clDateEntered As New TableCell
            Dim clStatusData As New TableCell
            Dim clDescData As New TableCell

            claccountRequestNum.Text = IIf(IsDBNull(drRow("RequestNum")), "", drRow("RequestNum"))

            clRequestTypeData.Text = IIf(IsDBNull(drRow("RequestType")), "", drRow("RequestType"))
            clDateEntered.Text = IIf(IsDBNull(drRow("EntryDate")), "", drRow("EntryDate"))

            clStatusData.Text = IIf(IsDBNull(drRow("CurrStatus")), "", drRow("CurrStatus"))
            clDescData.Text = IIf(IsDBNull(drRow("ReqDesc")), "", drRow("ReqDesc"))


            clReqNum.Text = "<a href=""ViewRequestTicket.aspx?RequestNum=" & claccountRequestNum.Text & """ ><u><b> " & claccountRequestNum.Text & "</b></u></a>"

            'clReqNum.Text = "<a href=""http://intranet01/csc/edit_ticket.asp?id=" & claccountRequestNum.Text & """ ><u><b> " & claccountRequestNum.Text & "</b></u></a>"

            'If clRequestTypeData.Text.ToLower = "ticket" Then
            '    clReqNum.Text = "<a href=""http://intranet01/csc/edit_ticket.asp?id=" & claccountRequestNum.Text & """ ><u><b> " & claccountRequestNum.Text & "</b></u></a>"
            'Else
            '    clReqNum.Text = "<a href=""http://intranet01/csc/update_rfs.asp?id=" & claccountRequestNum.Text & """ ><u><b> " & claccountRequestNum.Text & "</b></u></a>"
            'End If

            If Session("SecurityLevelNum") < 70 Then
                clReqNum.Text = claccountRequestNum.Text

            End If

            rwData.Cells.Add(clReqNum)
            rwData.Cells.Add(clRequestTypeData)

            rwData.Cells.Add(clDateEntered)
            rwData.Cells.Add(clStatusData)
            rwData.Cells.Add(clDescData)


            If iRowCount > 0 Then

                rwData.BackColor = Color.Bisque

            Else

                rwData.BackColor = Color.LightGray

            End If

            tblRFSTickets.Rows.Add(rwData)

            iRowCount = iRowCount * -1

        Next
        tblRFSTickets.Font.Size = 8
        tblRFSTickets.CellPadding = 4
        tblRFSTickets.Width = New Unit("100%")


    End Sub
    Protected Sub fillOpenRequestTable()
        Dim thisData As New CommandsSqlAndOleDb("SelOpenRequestByClient", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@client_num", iClientNum, SqlDbType.NVarChar)

        Dim iRowCount = 1

        Dim dt As DataTable
        dt = thisData.GetSqlDataTable

        OpenRequestCount = dt.Rows.Count

        Dim rwHeader As New TableRow

        Dim clRequestHeader As New TableCell
        Dim clReceiverNameHeader As New TableCell
        Dim clRequestorNameHeader As New TableCell
        Dim clSubmitterNameHeader As New TableCell
        Dim clReceiverHeader As New TableCell
        Dim clRequestTypeHeader As New TableCell

        Dim cldateHeader As New TableCell

        'account_request_num int null,
        'ReceiverName nvarchar(150) null,
        'RequestorName nvarchar(150) null,
        'SubmitterName  nvarchar(150) null,
        'Receiver nvarchar(25) null,
        'entered_date datetime null)


        clRequestHeader.Text = "Req#"
        clReceiverNameHeader.Text = "Receiver"
        clRequestorNameHeader.Text = "Requestor"
        clSubmitterNameHeader.Text = "Submitter"
        clReceiverHeader.Text = "Who was"
        clRequestTypeHeader.Text = "Type"

        cldateHeader.Text = "Date"


        rwHeader.Cells.Add(clRequestHeader)
        rwHeader.Cells.Add(clReceiverNameHeader)
        rwHeader.Cells.Add(clRequestorNameHeader)

        rwHeader.Cells.Add(clSubmitterNameHeader)
        rwHeader.Cells.Add(clReceiverHeader)
        rwHeader.Cells.Add(clRequestTypeHeader)

        rwHeader.Cells.Add(cldateHeader)


        rwHeader.Font.Bold = True
        rwHeader.BackColor = Color.FromName("#006666")
        rwHeader.ForeColor = Color.FromName("White")
        rwHeader.Height = Unit.Pixel(36)

        tblOpenRequests.Rows.Add(rwHeader)

        For Each drRow As DataRow In dt.Rows


            Dim rwData As New TableRow
            Dim btnSelect As New Button

            'Dim clRequestData As New TableCell
            Dim clSelectData As New TableCell
            Dim clReceiverNameData As New TableCell
            Dim clRequestorNameData As New TableCell
            Dim clSubmitterNameData As New TableCell
            Dim clReceiverData As New TableCell
            Dim clTypeData As New TableCell
            Dim cldateData As New TableCell

            btnSelect.Text = "Select"
            btnSelect.CommandName = "Select"
            btnSelect.CommandArgument = drRow("account_request_num")
            ' btnSelect.Width = 50
            btnSelect.EnableTheming = False

            AddHandler btnSelect.Click, AddressOf btnSelect_Click

            clReceiverNameData.Text = IIf(IsDBNull(drRow("ReceiverName")), "", drRow("ReceiverName"))
            clRequestorNameData.Text = IIf(IsDBNull(drRow("RequestorName")), "", drRow("RequestorName"))
            clSubmitterNameData.Text = IIf(IsDBNull(drRow("SubmitterName")), "", drRow("SubmitterName"))

            clReceiverData.Text = IIf(IsDBNull(drRow("Receiver")), "", drRow("Receiver"))
            clTypeData.Text = IIf(IsDBNull(drRow("account_request_type")), "", drRow("account_request_type"))

            cldateData.Text = IIf(IsDBNull(drRow("entered_date")), "", drRow("entered_date"))

            'add button to grid
            clSelectData.Controls.Add(btnSelect)

            rwData.Cells.Add(clSelectData)
            rwData.Cells.Add(clReceiverNameData)
            rwData.Cells.Add(clRequestorNameData)
            rwData.Cells.Add(clSubmitterNameData)

            rwData.Cells.Add(clReceiverData)
            rwData.Cells.Add(clTypeData)

            rwData.Cells.Add(cldateData)


            If iRowCount > 0 Then

                rwData.BackColor = Color.Bisque

            Else

                rwData.BackColor = Color.LightGray

            End If

            tblOpenRequests.Rows.Add(rwData)

            iRowCount = iRowCount * -1

        Next
        tblOpenRequests.Font.Name = "Arial"
        tblOpenRequests.Font.Size = 8

        tblOpenRequests.CellPadding = 10
        tblOpenRequests.Width = New Unit("100%")


    End Sub

    Protected Sub fillClosedRequestTable()
        Dim thisData As New CommandsSqlAndOleDb("SelClosedRequestByClient", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@client_num", iClientNum, SqlDbType.NVarChar)

        Dim iRowCount = 1

        Dim dt As DataTable
        dt = thisData.GetSqlDataTable

        Dim rwHeader As New TableRow

        Dim clRequestHeader As New TableCell
        Dim clReceiverNameHeader As New TableCell
        Dim clRequestorNameHeader As New TableCell
        Dim clSubmitterNameHeader As New TableCell
        Dim clReceiverHeader As New TableCell
        Dim clRequestTypeHeader As New TableCell

        Dim cldateHeader As New TableCell

        'account_request_num int null,
        'ReceiverName nvarchar(150) null,
        'RequestorName nvarchar(150) null,
        'SubmitterName  nvarchar(150) null,
        'Receiver nvarchar(25) null,
        'entered_date datetime null)


        clRequestHeader.Text = "Req#"
        clReceiverNameHeader.Text = "Receiver"
        clRequestorNameHeader.Text = "Requestor"
        clSubmitterNameHeader.Text = "Submitter"
        clReceiverHeader.Text = "Who was"
        clRequestTypeHeader.Text = "Type"
        cldateHeader.Text = "Date"


        rwHeader.Cells.Add(clRequestHeader)
        rwHeader.Cells.Add(clReceiverNameHeader)
        rwHeader.Cells.Add(clRequestorNameHeader)

        rwHeader.Cells.Add(clSubmitterNameHeader)
        rwHeader.Cells.Add(clReceiverHeader)
        rwHeader.Cells.Add(clRequestTypeHeader)

        rwHeader.Cells.Add(cldateHeader)


        rwHeader.Font.Bold = True
        rwHeader.BackColor = Color.FromName("#006666")
        rwHeader.ForeColor = Color.FromName("White")
        rwHeader.Height = Unit.Pixel(36)

        tblClosedRequest.Rows.Add(rwHeader)

        For Each drRow As DataRow In dt.Rows


            Dim rwData As New TableRow
            Dim btnSelect As New Button

            'Dim clRequestData As New TableCell
            Dim clSelectData As New TableCell
            Dim clReceiverNameData As New TableCell
            Dim clRequestorNameData As New TableCell
            Dim clSubmitterNameData As New TableCell
            Dim clReceiverData As New TableCell
            Dim clTypeData As New TableCell

            Dim cldateData As New TableCell

            btnSelect.Text = "Select"
            btnSelect.CommandName = "Select"
            btnSelect.CommandArgument = drRow("account_request_num")
            ' btnSelect.Width = 50
            'account_request_type

            btnSelect.EnableTheming = False

            AddHandler btnSelect.Click, AddressOf btnSelect_Click

            clReceiverNameData.Text = IIf(IsDBNull(drRow("ReceiverName")), "", drRow("ReceiverName"))
            clRequestorNameData.Text = IIf(IsDBNull(drRow("RequestorName")), "", drRow("RequestorName"))
            clSubmitterNameData.Text = IIf(IsDBNull(drRow("SubmitterName")), "", drRow("SubmitterName"))

            clReceiverData.Text = IIf(IsDBNull(drRow("Receiver")), "", drRow("Receiver"))
            clTypeData.Text = IIf(IsDBNull(drRow("account_request_type")), "", drRow("account_request_type"))

            cldateData.Text = IIf(IsDBNull(drRow("entered_date")), "", drRow("entered_date"))

            'add button to grid
            clSelectData.Controls.Add(btnSelect)

            rwData.Cells.Add(clSelectData)
            rwData.Cells.Add(clReceiverNameData)
            rwData.Cells.Add(clRequestorNameData)
            rwData.Cells.Add(clSubmitterNameData)

            rwData.Cells.Add(clReceiverData)
            rwData.Cells.Add(clTypeData)
            rwData.Cells.Add(cldateData)


            If iRowCount > 0 Then

                rwData.BackColor = Color.Bisque

            Else

                rwData.BackColor = Color.LightGray

            End If

            tblClosedRequest.Rows.Add(rwData)

            iRowCount = iRowCount * -1

        Next



        tblClosedRequest.Font.Name = "Arial"
        tblClosedRequest.Font.Size = 8

        tblClosedRequest.CellPadding = 10
        tblClosedRequest.Width = New Unit("100%")


    End Sub
    Sub btnSelect_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim btnLinkView As Button = sender

        If btnLinkView.CommandName = "Select" Then
            Dim sArguments As String() = Split(btnLinkView.CommandArgument, ";")

            Response.Redirect("./AccountRequestInfo2.aspx?RequestNum=" & sArguments(0) & "&sDeactivated=deactivated", True)

            '            Response.Redirect("./OpenProviderRequestItems.aspx?RequestNum=" & sArguments(0))

        End If
    End Sub

    Private Sub CheckAffiliation()

        'pnlAlliedHealth.Visible = False


        HDemployeetype.Text = emptypecdrbl.SelectedValue


        Select Case emptypecdrbl.SelectedValue
            Case "1193", "1200"
                'Case "employee"

                Dim firstNameReq As New RequiredField(firstnameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(lastnameTextBox, "Last Name")



                'pnlAlliedHealth.Visible = False

                pnlProvider.Visible = False
                providerrbl.SelectedIndex = -1


            Case "1189"
                'Case "student"

                Dim firstNameReq As New RequiredField(firstnameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(lastnameTextBox, "Last Name")
                Dim titleReq As New RequiredField(Titleddl, "Title")
                Dim specReq As New RequiredField(Specialtyddl, "Specialty")
                Dim add1Req As New RequiredField(address1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim faxqReq As New RequiredField(faxtextbox, "Fax")


                'pnlAlliedHealth.Visible = False

                pnlProvider.Visible = False
                providerrbl.SelectedIndex = -1

            Case "782"
                'Case "physician"
                Dim firstNameReq As New RequiredField(firstnameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(lastnameTextBox, "Last Name")
                Dim titleReq As New RequiredField(Titleddl, "Title")
                Dim specReq As New RequiredField(Specialtyddl, "Specialty")
                Dim add1Req As New RequiredField(address1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim faxqReq As New RequiredField(faxtextbox, "Fax")


                'pnlAlliedHealth.Visible = False

                pnlProvider.Visible = True
                'providerrbl.SelectedValue = "Yes"

            Case "1197"

                'Case "non staff clinician"
                Dim firstNameReq As New RequiredField(firstnameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(lastnameTextBox, "Last Name")
                Dim titleReq As New RequiredField(Titleddl, "Title")

                Dim specReq As New RequiredField(Specialtyddl, "Specialty")
                Dim add1Req As New RequiredField(address1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim faxqReq As New RequiredField(faxtextbox, "Fax")


                ' pnlAlliedHealth.Visible = False

                providerrbl.SelectedIndex = -1
                pnlProvider.Visible = True
            Case "1194"
                'Case "resident"
                Dim firstNameReq As New RequiredField(firstnameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(lastnameTextBox, "Last Name")
                Dim titleReq As New RequiredField(Titleddl, "Title")

                Dim specReq As New RequiredField(Specialtyddl, "Specialty")
                Dim add1Req As New RequiredField(address1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim faxqReq As New RequiredField(faxtextbox, "Fax")


                ' pnlAlliedHealth.Visible = False

                'providerrbl.SelectedValue = "Yes"
                pnlProvider.Visible = True

            Case "1198"
                ' Case "vendor"

                Dim firstNameReq As New RequiredField(firstnameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(lastnameTextBox, "Last Name")
                Dim add1Req As New RequiredField(address1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim vendorRequired As New RequiredField(VendorNameTextBox, "Vendor")


                'pnlAlliedHealth.Visible = False
                pnlProvider.Visible = False
                providerrbl.SelectedIndex = -1

            Case "1195"
                'Case "contract"

                Dim firstNameReq As New RequiredField(firstnameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(lastnameTextBox, "Last Name")

                Dim add1Req As New RequiredField(address1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")

                'pnlAlliedHealth.Visible = False

                pnlProvider.Visible = False
                providerrbl.SelectedIndex = -1


            Case "1196"
                'Case "other"

                Dim firstNameReq As New RequiredField(firstnameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(lastnameTextBox, "Last Name")

                Dim add1Req As New RequiredField(address1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")


                'pnlAlliedHealth.Visible = False

                pnlProvider.Visible = False
                providerrbl.SelectedIndex = -1



            Case Else


                Dim firstNameReq As New RequiredField(firstnameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(lastnameTextBox, "Last Name")


                ' pnlAlliedHealth.Visible = False

                pnlProvider.Visible = False
                providerrbl.SelectedIndex = -1



        End Select


    End Sub
    Protected Sub CurrentAccounts()

        gvCurrentUserAccounts.DataSource = ViewState("dtAccounts")
        gvCurrentUserAccounts.DataBind()

    
    End Sub
    'Protected Sub btnReactivate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReactivate.Click

    '    Dim thisdata As New CommandsSqlAndOleDb("InsRehireRequest", Session("EmployeeConn"))
    '    thisdata.AddSqlProcParameter("@client_num", iClientNum, SqlDbType.Int, 20)
    '    thisdata.AddSqlProcParameter("@siemens_emp_num", iClientNum, SqlDbType.Int, 20)

    '    thisdata.ExecNonQueryNoReturn()

    '    Response.Redirect("~/AddRemoveAcctForm.aspx?ClientNum=" & iClientNum)

    'End Sub

    Protected Sub btnUpdate_Click(sender As Object, e As System.EventArgs) Handles btnUpdate.Click
        If openRehireReqnum > 0 Then

            If openRehireAccountReqnum > 0 Then

                Response.Redirect("~/SignOnForm.aspx?ClientNum=" & iClientNum & "&ReHireRequestNum=" & openRehireReqnum, True)

            Else
                Response.Redirect("~/SignOnForm.aspx?ClientNum=" & iClientNum & "&ReHireRequestNum=" & openRehireReqnum, True)


            End If

        End If

        ReactivateClient.Show()

    End Sub

    Protected Sub btnSubmitRequest_Click(sender As Object, e As System.EventArgs) Handles btnSubmitRequest.Click
        Dim iReActivateRequestNum As Integer

        Dim thisdata As New CommandsSqlAndOleDb("InsRehireRequest", Session("EmployeeConn"))
        thisdata.AddSqlProcParameter("@client_num", iClientNum, SqlDbType.Int, 20)
        thisdata.AddSqlProcParameter("@siemens_emp_num", SiemensEmpNumTextBox.Text, SqlDbType.NVarChar, 16)

        iReActivateRequestNum = thisdata.GetScalarReturnInt()



        Response.Redirect("~/SignOnForm.aspx?ClientNum=" & iClientNum & "&ReHireRequestNum=" & iReActivateRequestNum, True)

    End Sub

	Private Sub btnReactivate_Click(sender As Object, e As EventArgs) Handles btnReactivate.Click

		Dim thisdata As New CommandsSqlAndOleDb("ReActivateClientV4", Session("EmployeeConn"))
		thisdata.AddSqlProcParameter("@client_num", iClientNum, SqlDbType.Int, 20)
		thisdata.AddSqlProcParameter("@withAccounts", "", SqlDbType.NVarChar, 6)

		thisdata.ExecNonQueryNoReturn()


		Response.Redirect("~/SimpleSearch.aspx", True)

	End Sub
End Class
