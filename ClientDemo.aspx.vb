﻿Imports System.IO
Imports System.Data '
Imports System.Data.SqlClient
Imports App_Code
Imports AjaxControlToolkit
Imports C1.Web.UI.Controls.C1Window
Imports System.Net
Imports System.Net.Mail
Imports System.Net.Mail.SmtpClient
Imports System.DirectoryServices
Imports System.Globalization
Imports System.Text.RegularExpressions

Partial Class ClientDemo
    Inherits System.Web.UI.Page
    Dim sWebReferrer As String = ""
    Dim AccountRequestNum As Integer
    Dim sActivitytype As String = ""
    Dim FormSignOn As New FormSQL()
    Dim VendorFormSignOn As FormSQL
    Dim UpdPhyFormSignOn As FormSQL
    Dim NewRequestForm As New FormSQL()
    Dim iClientNum As String

    Dim employeetype As String
    Dim blnReturn As Boolean = False
    Dim bHasError As Boolean
    'Dim ValidationStatus As String = ""
    Dim sItemComplet As String = ""
    Dim RequestoName As String
    Dim iRequestorNum As String
    Dim sIsPhysician As String = ""
    Dim sLabelMsg As String = ""

    Private dtSubApplications As New DataTable


	Dim thisRole As String
	Dim mainRole As String
	Dim errormsg As String = ""


	Dim AccountCount As Integer = 0
    Dim OpenRequestCount As Integer = 0
    Dim totAcct As Integer = 0


    Dim AccountRequest As AccountRequest



    Dim Mailobj As System.Net.Mail.SmtpClient = New System.Net.Mail.SmtpClient
    Dim Mailmsg As New System.Net.Mail.MailMessage
    Dim MailAddress As System.Net.Mail.MailAddress
    Dim mailAttachment As System.Net.Mail.Attachment

    Private dtAccounts As New DataTable
    Dim UserInfo As UserSecurity
    Private AddAccountRequestNum As Integer = 0
    Private DelAccountRequestNum As Integer = 0
    Private RemoveAccts As DataTable
    Private dtPendingRequests As DataTable
    Private dtRemoveAccounts As New DataTable
    Private dtAddAccounts As New DataTable

    Private dtAddFinAccounts As New DataTable

    Private dtDocmster As New DataTable
    Private dtCPMGroups As New DataTable

    Private dtTCLUserTypes As New DataTable


    Dim thisdata As CommandsSqlAndOleDb
	' Dim ClientInfoList As New List(Of Client3)
	Dim CPMClasses As List(Of String)
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load



        iClientNum = Request.QueryString("ClientNum")
        iRequestorNum = Request.QueryString("RequestorNum")
        RequestoName = Request.QueryString("RequestorName")

        If Request.QueryString("msg") <> "" Then
            sItemComplet = Request.QueryString("msg")

            If sItemComplet.Length > 2 Then
                TerminateLabel.Text = "Can not complet this request Client has open request that may include the account type your requesting."
                TerminateLabel.Visible = True

            End If

        End If


        If Request.QueryString("label") <> "" Then ' this comes from the update of client calls back to this page line 2748
            sLabelMsg = Request.QueryString("label")
            lblValidation.Text = Request.QueryString("label")
        End If

        Dim dbAccess As New CkhsEmployeeDbAccess(Session("EmployeeConn"))
        Dim ckhsEmployee = dbAccess.GetCkhsEmployee(iClientNum)
        'put ckhsEmployee in the HttpSession


        'c3Controller = New Client3Controller(iClientNum)
        If ckhsEmployee.LastName = "" Then
            ' must be deactivated no last name 
            Response.Redirect("~/DeactivatedAccount.aspx?ClientNum=" & iClientNum, True)

        End If

        'BusLog = New AccountBusinessLogic
        'BusLog.Accounts = thisdata.GetSqlDataTable

        UserInfo = New UserSecurity(Session("LoginID"), Session("EmployeeConn"))

        securitylevel.Text = UserInfo.SecurityLevelNum.ToString


        FormSignOn = New FormSQL(Session("EmployeeConn"), "SelClientByNumberV20", "UpdClientObjectByNumberV20", "", Page)
        FormSignOn.AddSelectParm("@client_num", iClientNum, SqlDbType.Int, 6)
        FormSignOn.AddInsertParm("@submitterClientNum", Session("ClientNum"), SqlDbType.Int, 6)

        NewRequestForm = New FormSQL(Session("EmployeeConn"), "SelAccountRequestV20", "InsNewAccountRequestForClientV20", "", Page)


        'Dim frmSec As New FormSecurity(Session("objUserSecurity"))
        'frmSec.DisableWebControlByRole(login_nameTextBox, "System Admin,Provider Master")

        emptypecdrbl.BackColor() = Color.Yellow
        emptypecdrbl.BorderColor() = Color.Black
        emptypecdrbl.BorderStyle() = BorderStyle.Solid
        emptypecdrbl.BorderWidth = Unit.Pixel(2)
        ' CheckIfCameFromTicket()

        If Not IsPostBack Then

            userSubmittingLabel.Text = UserInfo.UserName
            SubmittingOnBehalfOfNameLabel.Text = UserInfo.UserName
            submitter_client_numLabel.Text = UserInfo.ClientNum
            requestor_client_numLabel.Text = UserInfo.ClientNum
            userDepartmentCD.Text = UserInfo.DepartmentCd

            If iRequestorNum <> "" Then
                requestor_client_numLabel.Text = iRequestorNum
                'RequestorName
                SubmittingOnBehalfOfNameLabel.Text = RequestoName
            End If


            Dim rblAffiliateBinder As New RadioButtonListBinder(emptypecdrbl, "SelAffiliationsV5", Session("EmployeeConn"))
            'If Session("SecurityLevelNum") < 90 Then
            Select Case ckhsEmployee.RoleNum
                Case "782", "1197", "1194"
                    rblAffiliateBinder.AddDDLCriteria("@type", "phys", SqlDbType.NVarChar)
                Case "1200", "1193"
                    If ckhsEmployee.SiemensEmpNum <> "" Then
                        rblAffiliateBinder.AddDDLCriteria("@type", "ckhsemp", SqlDbType.NVarChar)
                    Else

                        rblAffiliateBinder.AddDDLCriteria("@type", "nonEmp", SqlDbType.NVarChar)
                    End If
                Case "1189"
                    If ckhsEmployee.SiemensEmpNum <> "" Then
                        rblAffiliateBinder.AddDDLCriteria("@type", "ckhsemp", SqlDbType.NVarChar)
                    Else
                        rblAffiliateBinder.AddDDLCriteria("@type", "student", SqlDbType.NVarChar)

                    End If

                Case "1195", "1198", "1196", "1199"

                    If ckhsEmployee.SiemensEmpNum <> "" Then
                        rblAffiliateBinder.AddDDLCriteria("@type", "", SqlDbType.NVarChar)
                    Else
                        rblAffiliateBinder.AddDDLCriteria("@type", "contract", SqlDbType.NVarChar)

                    End If


                Case Else
                    ' if a Doctor master number exist
                    If ckhsEmployee.DoctorMasterNum > 0 Then
                        EmpDoclbl.Visible = True
                        EmpDoctorTextBox.Text = ckhsEmployee.DoctorMasterNum.ToString
                        EmpDoctorTextBox.Visible = True
                        If Session("SecurityLevelNum") < 90 Then
                            EmpDoctorTextBox.Enabled = False
                        End If

                    Else
                        rblAffiliateBinder.AddDDLCriteria("@type", "", SqlDbType.NVarChar)
                    End If

            End Select
            'End If

            rblAffiliateBinder.ToolTip = False

            rblAffiliateBinder.BindData("RoleNum", "AffiliationDisplay") '"AffiliationDescLower",

            If RoleNumLabel.Text = "" Then

                masterRole.Text = ckhsEmployee.RoleNum
                mainRole = ckhsEmployee.RoleNum
                RoleNumLabel.Text = ckhsEmployee.RoleNum
                emptypecdrbl.SelectedValue = ckhsEmployee.RoleNum
                PositionNumTextBox.Text = ckhsEmployee.PositionNum
                PositionRoleNumTextBox.Text = ckhsEmployee.PositionRoleNum
            End If


            'emptypecdrbl.SelectedValue = ckhsEmployee.EmpTypeCd
            emptypecdLabel.Text = ckhsEmployee.EmpTypeCd
            HDemployeetype.Text = ckhsEmployee.EmpTypeCd

            ClientnameHeader.Text = ckhsEmployee.FullName
            ' Does not workk cant pass controler to class
            'dbAccess.CheckEmployeeType(HDemployeetype.Text)

            Dim provider As String
            provider = ckhsEmployee.Provider

            FormSignOn.FillForm()
            CheckAffiliation()

            '---------------------------------------------------
            ' Logic after the form has been filled
            '====================================================
            '+++++++++++++++++++++++++++++++++++++++++++++++++++
            ' Clinical Accounts Origional eMail and network
            '++++++++++++++++++++++++++++++++++++++++++++++++++
            dtRemoveAccounts.Columns.Add("ApplicationNum", GetType(Integer))
            dtRemoveAccounts.Columns.Add("ApplicationDesc", GetType(String))
            dtRemoveAccounts.Columns.Add("login_name", GetType(String))
            dtRemoveAccounts.Columns.Add("Subappnum", GetType(String))


            dtAddAccounts.Columns.Add("ApplicationNum", GetType(Integer))
            dtAddAccounts.Columns.Add("ApplicationDesc", GetType(String))

            dtAddFinAccounts.Columns.Add("ApplicationNum", GetType(Integer))
            dtAddFinAccounts.Columns.Add("ApplicationDesc", GetType(String))

            dtDocmster.Columns.Add("client_num", GetType(Integer))
            dtDocmster.Columns.Add("DoctormasterNumChar", GetType(String))


            ViewState("dtRemoveAccounts") = dtRemoveAccounts
            ViewState("dtAddAccounts") = dtAddAccounts
            ViewState("dtAddFinAccounts") = dtAddFinAccounts

            Dim thisdata As New CommandsSqlAndOleDb("SelAllClientAccountsByNumberV8", Session("EmployeeConn"))
            thisdata.AddSqlProcParameter("@client_num", iClientNum, SqlDbType.Int, 20)
            thisdata.ExecNonQueryNoReturn()
            ViewState("dtAccounts") = thisdata.GetSqlDataTable


            thisdata = New CommandsSqlAndOleDb("SelAvailableAccountsByClientNumberV6", Session("EmployeeConn"))
            thisdata.AddSqlProcParameter("@client_num", iClientNum, SqlDbType.Int, 20)

            Select Case userDepartmentCD.Text
                Case "832600", "832700", "823100"

                    thisdata.AddSqlProcParameter("@userClientNum", submitter_client_numLabel.Text, SqlDbType.Int, 20)

                Case "822600"
                    If Session("SecurityLevelNum") > 80 Then
                        thisdata.AddSqlProcParameter("@userClientNum", submitter_client_numLabel.Text, SqlDbType.Int, 20)
                    Else
                        thisdata.AddSqlProcParameter("@userClientNum", requestor_client_numLabel.Text, SqlDbType.Int, 20)

                    End If
                Case "724051"

                    If Session("SecurityLevelNum") > 79 Then
                        thisdata.AddSqlProcParameter("@userClientNum", submitter_client_numLabel.Text, SqlDbType.Int, 20)
                    Else
                        thisdata.AddSqlProcParameter("@userClientNum", requestor_client_numLabel.Text, SqlDbType.Int, 20)

                    End If

                Case Else
                    thisdata.AddSqlProcParameter("@userClientNum", requestor_client_numLabel.Text, SqlDbType.Int, 20)

            End Select
            thisdata.AddSqlProcParameter("@ApplicationGroup", "clinical", SqlDbType.NVarChar, 24)

            thisdata.ExecNonQueryNoReturn()
            ViewState("dtAvailableAccounts") = thisdata.GetSqlDataTable

            '++++++++++++++++++ End of Clinical Accounts +++++++++++++++
            '+++++++++++++++++++++++++++++++++++++++++++++++++++
            ' NEW 2016 FINANICAL Accounts 
            '++++++++++++++++++++++++++++++++++++++++++++++++++

            Dim thisFindata = New CommandsSqlAndOleDb("SelAvailableAccountsByClientNumberV6", Session("EmployeeConn"))
            thisFindata.AddSqlProcParameter("@client_num", iClientNum, SqlDbType.Int, 20)

            Select Case userDepartmentCD.Text
                Case "832600", "832700", "823100"

                    thisFindata.AddSqlProcParameter("@userClientNum", submitter_client_numLabel.Text, SqlDbType.Int, 20)
                Case "822200"
                    If Session("SecurityLevelNum") > 80 Then
                        thisFindata.AddSqlProcParameter("@userClientNum", submitter_client_numLabel.Text, SqlDbType.Int, 20)
                    Else
                        thisFindata.AddSqlProcParameter("@userClientNum", requestor_client_numLabel.Text, SqlDbType.Int, 20)
                    End If

                Case "724051"

                    If Session("SecurityLevelNum") > 79 Then
                        thisFindata.AddSqlProcParameter("@userClientNum", submitter_client_numLabel.Text, SqlDbType.Int, 20)
                    Else
                        thisFindata.AddSqlProcParameter("@userClientNum", requestor_client_numLabel.Text, SqlDbType.Int, 20)

                    End If

                Case Else

                    If Session("SecurityLevelNum") = 65 Then
                        thisFindata.AddSqlProcParameter("@userClientNum", submitter_client_numLabel.Text, SqlDbType.Int, 20)
                    Else
                        thisFindata.AddSqlProcParameter("@userClientNum", requestor_client_numLabel.Text, SqlDbType.Int, 20)

                    End If

            End Select


            thisFindata.AddSqlProcParameter("@ApplicationGroup", "financial", SqlDbType.NVarChar, 24)

            thisFindata.ExecNonQueryNoReturn()
            ViewState("dtAvailFincnialAccounts") = thisFindata.GetSqlDataTable


            Dim thisdataDoc As New CommandsSqlAndOleDb("SelPhysiciansDoocV2", Session("EmployeeConn"))
            thisdataDoc.AddSqlProcParameter("@clientnum", iClientNum, SqlDbType.Int, 20)

            thisdataDoc.ExecNonQueryNoReturn()

            ViewState("dtDocmster") = thisdataDoc.GetSqlDataTable


            Dim ddlBinder As New DropDownListBinder
            Dim ddlBinderSpec As New DropDownListBinder

            Dim ddlBinderV2 As DropDownListBinder
            ddlBinderV2 = New DropDownListBinder(Rolesddl, "SelEmployeeRolesV3", Session("EmployeeConn"))

            ddlBinderV2.AddDDLCriteria("@RoleNum", emptypecdrbl.SelectedValue, SqlDbType.NVarChar)

            If PositionNumTextBox.Text <> "" Then
                ddlBinderV2.AddDDLCriteria("@positionNum", PositionNumTextBox.Text, SqlDbType.NVarChar)

            End If

            ddlBinderV2.BindData("rolenum", "roledesc")

            ' ddlBinder.BindData(Rolesddl, "SelEmployeeRolesV2", "rolenum", "RoleDesc", Session("EmployeeConn"))

            ddlBinder.BindData(facilitycdddl, "SelFacilityCodes", "facility_cd", "facility_short_name", Session("EmployeeConn"))

            ddlBinderSpec.BindData(Specialtyddl, "SelSpecialtyCodesV2", "specialty", "specialty", Session("EmployeeConn"))

            ddlBinder.BindData(entitycdDDL, "selEntityCodesV4", "entity_cd", "hosp_desc", Session("EmployeeConn"))
            ' ddlBinder.BindData(AlliedHealthddl, "SelAlliedHealthPos", "AlliedHealthPosShort", "AlliedHealthPosShort", Session("EmployeeConn"))
            ddlBinder.BindData(suffixddl, "SelSuffixCodes", "Suffix", "Suffix", Session("EmployeeConn"))

            Dim ddlInvisionBinder As New DropDownListBinder(group_nameddl, "SelPhysGroupsV5", Session("EmployeeConn"))
            ddlInvisionBinder.AddDDLCriteria("@GroupTypeCd", "invision", SqlDbType.NVarChar)
            ddlInvisionBinder.AddDDLCriteria("@clientpage", "yes", SqlDbType.NVarChar)

            ddlInvisionBinder.BindData("Group_num", "group_name")

            'Dim ddlNonPerferBinder As New DropDownListBinder(Nonperferdddl, "SelPhysGroupsV5", Session("EmployeeConn"))
            'ddlNonPerferBinder.AddDDLCriteria("@GroupTypeCd", "NonPerGrp", SqlDbType.NVarChar)
            'ddlNonPerferBinder.AddDDLCriteria("@clientpage", "yes", SqlDbType.NVarChar)
            'ddlNonPerferBinder.BindData("Group_num", "group_name")

            Dim ddGnumberBinder As New DropDownListBinder(LocationOfCareIDddl, "SelPhysGroupsV5", Session("EmployeeConn"))
            ddGnumberBinder.AddDDLCriteria("@GroupTypeCd", "CHMG", SqlDbType.NVarChar)
            ddGnumberBinder.AddDDLCriteria("@clientpage", "yes", SqlDbType.NVarChar)

            ddGnumberBinder.BindData("Group_num", "group_name")

            'Dim ddCommLocationOfCare As New DropDownListBinder(CommLocationOfCareIDddl, "SelPhysGroupsV5", Session("EmployeeConn"))
            'ddCommLocationOfCare.AddDDLCriteria("@GroupTypeCd", "CommNum", SqlDbType.NVarChar)
            'ddCommLocationOfCare.AddDDLCriteria("@clientpage", "yes", SqlDbType.NVarChar)
            'ddCommLocationOfCare.BindData("Group_num", "group_name")

            'Dim ddCoverageGroupddl As New DropDownListBinder(CoverageGroupddl, "SelPhysGroupsV5", Session("EmployeeConn"))
            'ddCoverageGroupddl.AddDDLCriteria("@GroupTypeCd", "coverage", SqlDbType.NVarChar)
            'ddCoverageGroupddl.AddDDLCriteria("@clientpage", "yes", SqlDbType.NVarChar)
            'ddCoverageGroupddl.BindData("Group_num", "Group_name")


            Dim ddEMRBinder As New DropDownListBinder(PhysOfficeddl, "SelAllPhysGroups", Session("EmployeeConn"))
            'ddEMRBinder.AddDDLCriteria("@GroupTypeCd", "GNum", SqlDbType.NVarChar)
            'SelPhysGroupsV5
            ddEMRBinder.BindData("Group_num", "group_name")

            If PositionRoleNumLabel.Text <> "" Then
                Rolesddl.SelectedValue = PositionRoleNumLabel.Text
            ElseIf RoleNumLabel.Text <> "" Then
                Rolesddl.SelectedValue = RoleNumLabel.Text
                PositionRoleNumLabel.Text = RoleNumLabel.Text
                PositionRoleNumTextBox.Text = RoleNumLabel.Text
            Else
                Rolesddl.SelectedIndex = -1
            End If



            If entitycdLabel.Text = "" Then
                entitycdDDL.SelectedIndex = -1
            Else
                Dim entitycd As String = entitycdLabel.Text

                entitycd = entitycd.Trim
                entitycdDDL.SelectedValue = entitycd

            End If

            If facilitycdLabel.Text = "" Then
                facilitycdddl.SelectedIndex = -1
            Else
                facilitycdddl.SelectedValue = facilitycdLabel.Text
            End If

            ' Gnumber
            If LocationOfCareIDLabel.Text = "" Then
                LocationOfCareIDddl.SelectedIndex = -1
                LocationOfCareIDddl.Visible = True

            Else
                LocationOfCareIDddl.SelectedValue = GNumberGroupNumLabel.Text
                origionalGnumber.Text = GNumberGroupNumLabel.Text

                CPMViewGrouplbl.Text = LocationOfCareIDddl.SelectedItem.ToString
                CPMViewGrouplbl.Visible = True
                'ViewCPMPan.Visible = True
                LocationOfCareIDddl.Visible = False

                'LocationOfCareIDLabel.Text = LocationOfCareIDddl.SelectedItem.ToString

            End If

            'Comm Gnumber
            'If CommLocationOfCareIDLabel.Text = "" Then
            '    CommLocationOfCareIDddl.SelectedIndex = -1
            'Else
            '    ' CommLocationOfCareIDLabel.Text = CommLocationOfCareIDddl.SelectedItem.ToString
            '    CommLocationOfCareIDddl.SelectedValue = CommNumberGroupNumLabel.Text
            '    origionalCommNumber.Text = CommNumberGroupNumLabel.Text
            'End If

            ''Coverage number
            'If CoverageGroupNumLabel.Text = "" Then
            '    CoverageGroupddl.SelectedIndex = -1
            'Else
            '    CoverageGroupddl.SelectedValue = CoverageGroupNumLabel.Text

            'End If

            ' Invision Number
            If InvisionGroupNumTextBox.Text = "" Then
                group_nameddl.SelectedIndex = -1
            Else
                group_nameddl.SelectedValue = InvisionGroupNumTextBox.Text
                origionalgroupNum.Text = InvisionGroupNumTextBox.Text
            End If

            'non perfered
            'If NonPerferedGroupnumLabel.Text = "" Then
            '    Nonperferdddl.SelectedIndex = -1
            'Else
            '    Nonperferdddl.SelectedValue = NonPerferedGroupnumLabel.Text
            '    origionalNonperGroup.Text = NonPerferedGroupnumLabel.Text
            'End If

            Dim ddbDepartmentBinder As New DropDownListBinder(departmentcdddl, "selDepartmentCodesV4", Session("EmployeeConn"))
            ddbDepartmentBinder.AddDDLCriteria("@entity_cd", entitycdDDL.SelectedValue, SqlDbType.NVarChar)
            ddbDepartmentBinder.BindData("department_cd", "department_name")

            If departmentcdLabel.Text = "" Then
                departmentcdddl.SelectedIndex = -1
            Else
                departmentcdddl.SelectedValue = departmentcdLabel.Text

            End If


            If SpecialtyLabel.Text = "" Then
                Specialtyddl.SelectedIndex = -1
            Else
                Specialtyddl.SelectedValue = SpecialtyLabel.Text

            End If

            'If groupnameTextBox.Text = "" Then
            '    group_nameddl.SelectedIndex = -1
            'Else
            '    group_nameddl.SelectedValue = groupnumTextBox.Text
            'End If

            If suffixlabel.Text = "" Then
                suffixddl.SelectedIndex = -1
            Else
                suffixddl.SelectedValue = suffixlabel.Text
            End If

            ' practice_numddl.SelectedValue = practice_numLabel.Text


            ddlBinder = New DropDownListBinder(buildingcdddl, "sel_building_by_facility", Session("EmployeeConn"))
            ddlBinder.AddDDLCriteria("@facility_cd", facilitycdddl.SelectedValue, SqlDbType.NVarChar)
            ddlBinder.BindData("building_cd", "building_name")


            If buildingcdTextBox.Text = "" Then
                buildingcdddl.SelectedValue = -1
            Else
                Dim sBuilding As String = buildingcdTextBox.Text
                buildingcdddl.SelectedValue = sBuilding.Trim
                If buildingnameLabel.Text <> "" Then

                    buildingcdddl.SelectedItem.Text = buildingnameLabel.Text
                End If

            End If

            If buildingcdddl.SelectedIndex <> -1 Then

                ddlBinder = New DropDownListBinder(Floorddl, "SelFloorsByBuilding", Session("EmployeeConn"))
                ddlBinder.AddDDLCriteria("@facility_cd", facilitycdddl.SelectedValue, SqlDbType.NVarChar)
                ddlBinder.AddDDLCriteria("@building_cd", buildingcdddl.SelectedValue, SqlDbType.NVarChar)

                ddlBinder.BindData("floor_no", "floor_no")

                If FloorTextBox.Text = "" Then
                    Floorddl.SelectedValue = -1
                Else
                    Dim sFloor As String = FloorTextBox.Text
                    Floorddl.SelectedValue = sFloor.Trim

                End If



            End If

            Select Case emptypecdrbl.SelectedValue
                Case "782", "1197"

                    siemensempnumTextBox.Visible = True
                    Emplbl.Visible = True

                    'Case "physician", "non staff clinician", "resident"

                    If Titleddl.SelectedIndex <= 0 Then
                        Dim ddbTitleBinder As New DropDownListBinder(Titleddl, "SelTitlesV4", Session("EmployeeConn"))
                        ddbTitleBinder.AddDDLCriteria("@titleType", "Physician", SqlDbType.NVarChar)
                        ddbTitleBinder.BindData("TitleShortDesc", "TitleDesc")

                        'FillAffiliations()

                        If TitleLabel.Text = "" Then
                            Titleddl.SelectedIndex = -1
                        Else
                            Titleddl.SelectedValue = TitleLabel.Text

                        End If

                    End If



                    If SpecialtyLabel.Text = "" Then
                        Specialtyddl.SelectedIndex = -1
                    Else
                        Specialtyddl.SelectedValue = SpecialtyLabel.Text

                    End If
                    ' use to secure here
                    If DoctorMasterNumTextBox.Text <> "" Then
                        docMstrPnl.Visible = True
                        'grDocmster
                    End If

                    'hanrbl.BackColor() = Color.Yellow
                    'hanrbl.BorderColor() = Color.Black
                    'hanrbl.BorderStyle() = BorderStyle.Solid
                    'hanrbl.BorderWidth = Unit.Pixel(3)

                    If hanrbl.SelectedValue.ToString = "Yes" Then

                        ' change role 1567
                        LocationOfCareIDddl.Visible = True
                        ckhnlbl.Visible = True

                        LocationOfCareIDddl.BackColor() = Color.Yellow
                        LocationOfCareIDddl.BorderColor() = Color.Black
                        LocationOfCareIDddl.BorderStyle() = BorderStyle.Solid
                        LocationOfCareIDddl.BorderWidth = Unit.Pixel(2)


                        'cpmPhysSchedulePanel.Visible = True
                        'cpmModelEmplPanel.Visible = True
                        'educationpan.Visible = True

                        'Dim ddcmpMOdelafter As New DropDownListBinder(CPMEmployeesddl, "SelCPMClients", Session("EmployeeConn"))
                        'ddcmpMOdelafter.AddDDLCriteria("@Group_num", GNumberGroupNumLabel.Text, SqlDbType.NVarChar)
                        'ddcmpMOdelafter.BindData("client_num", "FullName")


                        'Dim rblEducationBinder As New ListBoxBinder(educationListbox, "SelCPMTrainCodes", Session("EmployeeConn"))
                        'rblEducationBinder.AddListBoxParameter("@client_num", iClientNum, SqlDbType.Int)
                        'rblEducationBinder.BindData("CPMTrainCd", "CPMTraindesc")

                        'If provmodelTextBox.Text <> "" Then
                        '    ViewCPMModelTextBox.Text = provmodelTextBox.Text

                        'End If
                        'If SchedulableTextbox.Text <> "" Then
                        '    Schedulablerbl.SelectedValue = SchedulableTextbox.Text
                        'End If


                        'If TelevoxTextBox.Text <> "" Then
                        '    Televoxrbl.SelectedValue = TelevoxTextBox.Text
                        'End If

                        'cpmModelEmplPanel.Visible = True
                        'cpmModelAfterPanel.Visible = True

                        ' end of CPM validation 2018
                        'rest of comments nothing

                        ' make yellow and manadtory
                        'npitextbox
                        'npitextbox.BackColor() = Color.Yellow
                        'npitextbox.BorderColor() = Color.Black
                        'npitextbox.BorderStyle() = BorderStyle.Solid
                        'npitextbox.BorderWidth = Unit.Pixel(2)


                        'LicensesNoTextBox
                        'LicensesNoTextBox.BackColor() = Color.Yellow
                        'LicensesNoTextBox.BorderColor() = Color.Black
                        'LicensesNoTextBox.BorderStyle() = BorderStyle.Solid
                        'LicensesNoTextBox.BorderWidth = Unit.Pixel(2)

                        'taxonomytextbox
                        'taxonomytextbox.BackColor() = Color.Yellow
                        'taxonomytextbox.BorderColor() = Color.Black
                        'taxonomytextbox.BorderStyle() = BorderStyle.Solid
                        'taxonomytextbox.BorderWidth = Unit.Pixel(2)

                        'startdateTextBox
                        'startdateTextBox.BackColor() = Color.Yellow
                        'startdateTextBox.BorderColor() = Color.Black
                        'startdateTextBox.BorderStyle() = BorderStyle.Solid
                        'startdateTextBox.BorderWidth = Unit.Pixel(2)


                        'Dim rblEducationBinder As New ListBoxBinder

                        'rblEducationBinder.BindData(educationListbox, "SelCPMTrainCodes", "CPMTrainCd", "CPMTraindesc", Session("EmployeeConn"))
                        'educationDateTextBox.BackColor() = Color.Yellow
                        'educationDateTextBox.BorderColor() = Color.Black
                        'educationDateTextBox.BorderStyle() = BorderStyle.Solid
                        'educationDateTextBox.BorderWidth = Unit.Pixel(2)


                        'LocationOfCareIDddl.BackColor() = Color.Yellow
                        'LocationOfCareIDddl.BorderColor() = Color.Black
                        'LocationOfCareIDddl.BorderStyle() = BorderStyle.Solid
                        'LocationOfCareIDddl.BorderWidth = Unit.Pixel(2)

                        'Schedulablerbl
                        'Schedulablerbl.BackColor() = Color.Yellow
                        'Schedulablerbl.BorderColor() = Color.Black
                        'Schedulablerbl.BorderStyle() = BorderStyle.Solid
                        'Schedulablerbl.BorderWidth = Unit.Pixel(2)

                        'Televoxrbl
                        'Televoxrbl.BackColor() = Color.Yellow
                        'Televoxrbl.BorderColor() = Color.Black
                        'Televoxrbl.BorderStyle() = BorderStyle.Solid
                        'Televoxrbl.BorderWidth = Unit.Pixel(2)


                        'CPMEmployeesddl
                        'CPMEmployeesddl.BackColor() = Color.Yellow
                        'CPMEmployeesddl.BorderColor() = Color.Black
                        'CPMEmployeesddl.BorderStyle() = BorderStyle.Solid
                        'CPMEmployeesddl.BorderWidth = Unit.Pixel(2)

                        'startdateTextBox.BackColor() = Color.Yellow
                        'startdateTextBox.BorderColor() = Color.Black
                        'startdateTextBox.BorderStyle() = BorderStyle.Solid
                        'startdateTextBox.BorderWidth = Unit.Pixel(2)


                        'lblprov.Visible = True
                        'providerrbl.Visible = True

                    Else
                        LocationOfCareIDddl.Visible = False
                        ckhnlbl.Visible = False

                        'lblprov.Visible = False
                        'providerrbl.Visible = False

                    End If



                Case "1194" 'Residents
                    'Dim ddlBinder As New DropDownListBinder
                    siemensempnumTextBox.Visible = True
                    Emplbl.Visible = True


                    Dim ddbTitleBinder As New DropDownListBinder(Titleddl, "SelTitlesV4", Session("EmployeeConn"))
                    ddbTitleBinder.AddDDLCriteria("@titleType", "Resident", SqlDbType.NVarChar)
                    ddbTitleBinder.BindData("TitleShortDesc", "TitleDesc")

                    Dim ddlRoleBinder As New DropDownListBinder(Rolesddl, "SelEmployeeRolesV3", Session("EmployeeConn"))
                    ddlRoleBinder.AddDDLCriteria("@RoleNum", "1194", SqlDbType.NVarChar)
                    ddlRoleBinder.BindData("rolenum", "RoleDesc")



                    If TitleLabel.Text = "" Then
                        Titleddl.SelectedIndex = -1
                    Else
                        Titleddl.SelectedValue = TitleLabel.Text

                    End If

                    If SpecialtyLabel.Text = "" Then
                        Specialtyddl.SelectedIndex = -1
                    Else
                        Specialtyddl.SelectedValue = SpecialtyLabel.Text

                    End If

                    If TitleLabel.Text = "" Then
                        Title2Label.Text = "Resident"

                    Else
                        Title2Label.Text = TitleLabel.Text

                    End If
                    Title2Label.Visible = True
                    Titleddl.Visible = False

                    SpecialtyLabel.Text = "Resident"
                    SpecialtyLabel.Visible = True
                    Specialtyddl.Visible = False

                    credentialedDateTextBox.Enabled = False
                    credentialedDateTextBox.CssClass = "style2"

                    'CredentialedDateDCMHTextBox.Enabled = False
                    'CredentialedDateDCMHTextBox.CssClass = "style2"

                    CCMCadmitRightsrbl.SelectedValue = "No"
                    'CCMCadmitRightsrbl.Enabled = False

                    'DCMHadmitRightsrbl.SelectedValue = "No"
                    'DCMHadmitRightsrbl.Enabled = False

                    'hanrbl.BackColor() = Color.Yellow
                    'hanrbl.BorderColor() = Color.Black
                    'hanrbl.BorderStyle() = BorderStyle.Solid
                    'hanrbl.BorderWidth = Unit.Pixel(3)


                    If hanrbl.SelectedValue.ToString = "Yes" Then
                        ' remove all CPM validation 2018
                        'hanrbl.Enabled = False

                        'ckhnlbl.Visible = True
                        'cpmPhysSchedulePanel.Visible = True
                        'cpmModelEmplPanel.Visible = True
                        'educationpan.Visible = True


                        'Dim rblEducationBinder As New ListBoxBinder(educationListbox, "SelCPMTrainCodes", Session("EmployeeConn"))
                        'rblEducationBinder.AddListBoxParameter("@client_num", iClientNum, SqlDbType.Int)
                        'rblEducationBinder.BindData("CPMTrainCd", "CPMTraindesc")

                        'educationDateTextBox.BackColor() = Color.Yellow
                        'educationDateTextBox.BorderColor() = Color.Black
                        'educationDateTextBox.BorderStyle() = BorderStyle.Solid
                        'educationDateTextBox.BorderWidth = Unit.Pixel(2)

                        'Dim ddcmpMOdelafter As New DropDownListBinder(CPMEmployeesddl, "SelCPMClients", Session("EmployeeConn"))
                        'ddcmpMOdelafter.AddDDLCriteria("@Group_num", GNumberGroupNumLabel.Text, SqlDbType.NVarChar)
                        'ddcmpMOdelafter.BindData("client_num", "FullName")



                        'If provmodelTextBox.Text <> "" Then
                        '    ViewCPMModelTextBox.Text = provmodelTextBox.Text

                        'End If
                        'If SchedulableTextbox.Text <> "" Then
                        '    Schedulablerbl.SelectedValue = SchedulableTextbox.Text
                        'End If


                        'If TelevoxTextBox.Text <> "" Then
                        '    Televoxrbl.SelectedValue = TelevoxTextBox.Text
                        'End If

                        'cpmModelEmplPanel.Visible = True
                        'cpmModelAfterPanel.Visible = True

                        ' End of remove all CPM validation 2018

                        'LocationOfCareIDddl.Visible = True
                        'Dim rblEducationBinder As New ListBoxBinder
                        'rblEducationBinder.BindData(educationListbox, "SelCPMTrainCodes", "CPMTrainCd", "CPMTraindesc", Session("EmployeeConn"))



                        ' make yellow and manadtory
                        'npitextbox.BackColor() = Color.Yellow
                        'npitextbox.BorderColor() = Color.Black
                        'npitextbox.BorderStyle() = BorderStyle.Solid
                        'npitextbox.BorderWidth = Unit.Pixel(2)


                        'LicensesNoTextBox.BackColor() = Color.Yellow
                        'LicensesNoTextBox.BorderColor() = Color.Black
                        'LicensesNoTextBox.BorderStyle() = BorderStyle.Solid
                        'LicensesNoTextBox.BorderWidth = Unit.Pixel(2)

                        'taxonomytextbox.BackColor() = Color.Yellow
                        'taxonomytextbox.BorderColor() = Color.Black
                        'taxonomytextbox.BorderStyle() = BorderStyle.Solid
                        'taxonomytextbox.BorderWidth = Unit.Pixel(2)

                        'LocationOfCareIDddl.BackColor() = Color.Yellow
                        'LocationOfCareIDddl.BorderColor() = Color.Black
                        'LocationOfCareIDddl.BorderStyle() = BorderStyle.Solid
                        'LocationOfCareIDddl.BorderWidth = Unit.Pixel(2)

                        'Schedulablerbl
                        'Schedulablerbl.BackColor() = Color.Yellow
                        'Schedulablerbl.BorderColor() = Color.Black
                        'Schedulablerbl.BorderStyle() = BorderStyle.Solid
                        'Schedulablerbl.BorderWidth = Unit.Pixel(2)

                        'Televoxrbl
                        'Televoxrbl.BackColor() = Color.Yellow
                        'Televoxrbl.BorderColor() = Color.Black
                        'Televoxrbl.BorderStyle() = BorderStyle.Solid
                        'Televoxrbl.BorderWidth = Unit.Pixel(2)

                        'CPMEmployeesddl
                        'CPMEmployeesddl.BackColor() = Color.Yellow
                        'CPMEmployeesddl.BorderColor() = Color.Black
                        'CPMEmployeesddl.BorderStyle() = BorderStyle.Solid
                        'CPMEmployeesddl.BorderWidth = Unit.Pixel(2)

                        'startdateTextBox.BackColor() = Color.Yellow
                        'startdateTextBox.BorderColor() = Color.Black
                        'startdateTextBox.BorderStyle() = BorderStyle.Solid
                        'startdateTextBox.BorderWidth = Unit.Pixel(2)


                    Else
                        LocationOfCareIDddl.Visible = False
                        ckhnlbl.Visible = False

                        'lblprov.Visible = False
                        'providerrbl.Visible = False

                    End If

                    'Dim ddlSpecialityBinder As New DropDownListBinder(Specialtyddl, "SelSpecialtyCodesV2", Session("EmployeeConn"))
                    'ddlSpecialityBinder.AddDDLCriteria("@SpecialtyType", "Resident", SqlDbType.NVarChar)
                    'ddlSpecialityBinder.BindData("specialty", "specialty")

                    ' in titleddl will be rolenum for residents which will be the new thisrole
                    'thisRole = emptypecdrbl.SelectedValue
                    'mainRole = emptypecdrbl.SelectedValue

                    pnlProvider.Visible = True



                Case "1195", "1198"
                    ' load vendor list 
                    'make vendor text box invisiable
                    ' make ddl appear
                    ' 1195 Contractor list
                    ' 1198 Vendor list
                    Vendorddl.Visible = True
                    VendorNameTextBox.Visible = False

                    siemensempnumTextBox.Visible = False
                    Emplbl.Visible = False


                    Dim ddbVendorBinder As New DropDownListBinder(Vendorddl, "SelVendors", Session("EmployeeConn"))

                    Select Case PositionRoleNumTextBox.Text
                        Case "1554", "1560"


                            ddbVendorBinder.AddDDLCriteria("@type", "prospect", SqlDbType.NVarChar)
                            ddbVendorBinder.BindData("VendorNum", "VendorName")


                            If VendorNameTextBox.Text = "" Then
                                Vendorddl.SelectedIndex = -1
                            Else
                                Vendorddl.SelectedValue = VendorNumLabel.Text

                            End If

                            If VendorNumLabel.Text = "0" Then
                                otherVendorlbl.Visible = True
                                VendorUnknowlbl.Text = VendorNameTextBox.Text
                                VendorUnknowlbl.Visible = True
                            End If

                        Case Else


                            ddbVendorBinder.AddDDLCriteria("@type", "vendor", SqlDbType.NVarChar)
                            ddbVendorBinder.BindData("VendorNum", "VendorName")
                            lblVendor.Visible = True

                            'If emptypecdrbl.SelectedValue = "1198" Then
                            '    ddbVendorBinder.AddDDLCriteria("@type", "vendor", SqlDbType.NVarChar)
                            '    lblVendor.Text = "Vendor Name:"

                            'ElseIf emptypecdrbl.SelectedValue = "1195" Then

                            '    ddbVendorBinder.AddDDLCriteria("@type", "contractor", SqlDbType.NVarChar)
                            '    lblVendor.Text = "Contractor Name:"
                            'Else

                            'End If

                            If VendorNameTextBox.Text = "" Then
                                Vendorddl.SelectedIndex = -1
                            Else
                                Vendorddl.SelectedValue = VendorNumLabel.Text
                                HDVendorNum.Text = VendorNumLabel.Text

                            End If

                            If VendorNumLabel.Text = "0" Then
                                otherVendorlbl.Visible = True
                                VendorUnknowlbl.Text = VendorNameTextBox.Text
                                VendorUnknowlbl.Visible = True
                            End If

                    End Select


                Case "1199", "1196"
                    siemensempnumTextBox.Visible = False
                    Emplbl.Visible = False

                Case "1193"
                    siemensempnumTextBox.Visible = True
                    Emplbl.Visible = True

                    If DoctorMasterNumTextBox.Text <> "" Then
                        DoctorMasterNumLabel.Visible = True
                        behaviorallbl.Visible = True

                    End If

                    PhysOfficepanel.Visible = True


                    'If groupnumTextBox.Text = "" Then
                    '    PhysOfficeddl.SelectedIndex = -1
                    'Else
                    '    PhysOfficeddl.SelectedValue = groupnumTextBox.Text
                    'End If


                    Select Case PositionRoleNumLabel.Text
                        Case "1567"
                            ' Lock position drop down
                            'Rolesddl.Enabled = False

                            PhysOfficeddl.Dispose()


                            Dim ddOffBinder As New DropDownListBinder(PhysOfficeddl, "SelPhysGroupsV5", Session("EmployeeConn"))
                            ddOffBinder.AddDDLCriteria("@GroupTypeCd", "GNum", SqlDbType.NVarChar)
                            ddOffBinder.AddDDLCriteria("@clientpage", "yes", SqlDbType.NVarChar)

                            'SelPhysGroupsV5
                            ddOffBinder.BindData("Group_num", "group_name")

                            'Dim rblEducationBinder As New ListBoxBinder(educationListbox, "SelCPMTrainCodes", Session("EmployeeConn"))
                            'rblEducationBinder.AddDDLCriteria("@client_num", iClientNum, SqlDbType.Int)
                            'rblEducationBinder.BindData("CPMTrainCd", "CPMTraindesc")

                            'Dim rblEducationBinder As New ListBoxBinder(educationListbox, "SelCPMTrainCodes", Session("EmployeeConn"))
                            'rblEducationBinder.AddListBoxParameter("@client_num", iClientNum, SqlDbType.Int)
                            'rblEducationBinder.BindData("CPMTrainCd", "CPMTraindesc")

                            'rblEducationBinder.BindData(educationListbox, "SelCPMTrainCodes", "CPMTrainCd", "CPMTraindesc", Session("EmployeeConn"))


                            'educationDateTextBox.BackColor() = Color.Yellow
                            'educationDateTextBox.BorderColor() = Color.Black
                            'educationDateTextBox.BorderStyle() = BorderStyle.Solid
                            'educationDateTextBox.BorderWidth = Unit.Pixel(2)


                            'educationpan.Visible = True

                            'PhysOfficeddl.BackColor() = Color.Yellow
                            'PhysOfficeddl.BorderColor() = Color.Black
                            'PhysOfficeddl.BorderStyle() = BorderStyle.Solid
                            'PhysOfficeddl.BorderWidth = Unit.Pixel(2)

                            'If groupnumTextBox.Text = "" Then
                            '    PhysOfficeddl.SelectedIndex = -1
                            'Else
                            '    PhysOfficeddl.SelectedValue = groupnumTextBox.Text
                            'End If


                            'CPMEmployeesddl.Dispose()

                            'Dim ddcmpMOdelafter As New DropDownListBinder
                            'ddcmpMOdelafter.BindData(CPMEmployeesddl, "SelCPMClients", "client_num", "FullName", Session("EmployeeConn"))

                            'cpmModelEmplPanel.Visible = True
                            'cpmModelAfterPanel.Visible = True

                            'If CPMModelTextBox.Text <> "" Then
                            '    ViewCPMModelTextBox.Text = CPMModelTextBox.Text

                            'End If

                            'CPMClasses

                            'CPMEmployeesddl
                            'CPMEmployeesddl.BackColor() = Color.Yellow
                            'CPMEmployeesddl.BorderColor() = Color.Black
                            'CPMEmployeesddl.BorderStyle() = BorderStyle.Solid
                            'CPMEmployeesddl.BorderWidth = Unit.Pixel(2)

                            'startdateTextBox.BackColor() = Color.Yellow
                            'startdateTextBox.BorderColor() = Color.Black
                            'startdateTextBox.BorderStyle() = BorderStyle.Solid
                            'startdateTextBox.BorderWidth = Unit.Pixel(2)


                        Case Else
                            'If groupnumTextBox.Text = "" Then
                            '    PhysOfficeddl.SelectedIndex = -1
                            'Else
                            '    PhysOfficeddl.SelectedValue = groupnumTextBox.Text
                            'End If


                    End Select

                Case "1200"


                    '    'Case "allied health"
                    '    Dim ddbTitleBinder As New DropDownListBinder(Titleddl, "SelTitlesV4", Session("EmployeeConn"))
                    '    ddbTitleBinder.AddDDLCriteria("@titleType", "Alliedhealth", SqlDbType.NVarChar)
                    '    ddbTitleBinder.BindData("TitleShortDesc", "TitleDesc")

                    '    'FillAffiliations()

                    '    If TitleLabel.Text = "" Then
                    '        Titleddl.SelectedIndex = -1
                    '    Else
                    '        Titleddl.SelectedValue = TitleLabel.Text

                    '    End If
                    '    If SpecialtyLabel.Text = "" Then
                    '        Specialtyddl.SelectedIndex = -1
                    '    Else
                    '        Specialtyddl.SelectedValue = SpecialtyLabel.Text

                    '    End If

                    '    If Session("SecurityLevelNum") < 90 Then

                    '        Dim ctrl As Control = DataCheckingAndValidation.SetChildControlsRecursiveToEnabledFalse(tpDemographics, False)
                    '    Else
                    '        btnUpdate.Text = "Update Non-Demographics"
                    '        btnSubmitDemoChange.Visible = True
                    '        doctormasternumTextBox.Enabled = True
                    '        doctormasternumTextBox.Enabled = True
                    '        'firstnameTextBox.Enabled = False
                    '        'miTextBox.Enabled = False
                    '        'lastnameTextBox.Enabled = False
                    '        'address1textbox.Enabled = False
                    '        'address2textbox.Enabled = False
                    '        'citytextbox.Enabled = False
                    '        'lblstate.Enabled = False
                    '        'statetextbox.Enabled = False
                    '        'lblzip.Enabled = False
                    '        'ziptextbox.Enabled = False
                    '        'phoneTextBox.Enabled = False
                    '        'faxtextbox.Enabled = False

                    '    End If

                Case Else


            End Select

            Dim CerPosdataddl As New DropDownListBinder(CernerPosddl, "SelCernerPositionDesc", Session("EmployeeConn"))
            CerPosdataddl.AddDDLCriteria("@UserTypeCd", emptypecdLabel.Text, SqlDbType.NVarChar)
            CerPosdataddl.AddDDLCriteria("@Clientupd", "all", SqlDbType.NVarChar)

            CerPosdataddl.BindData("CernerPositionDesc", "CernerPositionDesc")


            If Account_Request_Type.Text <> "" And AccountRequestTypeLabel.Text = "" Then
                AccountRequestTypeLabel.Text = Account_Request_Type.Text
            End If

            'addpmhact
            Select Case AccountRequestTypeLabel.Text
                Case "terminate"
                    TerminateLabel.Visible = True

                    btnSelectOnBehalfOf.Visible = False
                    btnUpdate.Visible = False
                    btnSubmitDemoChange.Visible = False
                    tpAddItems.Visible = False
                    tpRemoveItems.Visible = False
                    tpAddFinanItems.Visible = False

                Case "addpmhact"
                    Dim accountreqnum As String
                    accountreqnum = AddAccountRequestnumLabel.Text

                    Response.Redirect("./SignOnForm.aspx?ClientNum=" & iClientNum & "&AccountRequestNum=" & accountreqnum)
            End Select
            ' All bets are off if client has open Terminate request
            'If AccountRequestTypeLabel.Text = "terminate" Then
            '	TerminateLabel.Visible = True

            '	btnSelectOnBehalfOf.Visible = False
            '	btnUpdate.Visible = False
            '	btnSubmitDemoChange.Visible = False
            '	tpAddItems.Visible = False
            '	tpRemoveItems.Visible = False
            '	tpAddFinanItems.Visible = False


            'End If


            firstnameTextBox.Focus()

            'If HDemployeetype.Text = "allied health" Then
            '    Titleddl.Enabled = False
            '    AlliedHealthddl.SelectedValue = TitleLabel.Text
            '    TitleLabel.Text = ""
            '    Titleddl.SelectedIndex = -1
            'End If


            If emptypecdLabel.Text = "other" Then
                userpositiondescTextBox.Enabled = False
            End If


            'If providerrbl.Text = "Yes" Then
            '    ' only 90 security level and above edit these fields
            '    If Session("SecurityLevelNum") < 90 Then
            '        Dim ctrl As Control = DataCheckingAndValidation.SetChildControlsRecursiveToEnabledFalse(tpDemographics, False)

            '    End If
            'End If

            ' building_cdddl.SelectedValue = sBuilding
            'CurrentAccounts()
            'AvailableAccounts()

            If siemensempnumTextBox.Text = "" Then
                siemdesclbl.Text = "User Position:"
                'userpositiondescLabel.Text = ""

            End If


            ' TCL 
            Dim index1 = BuildIndex(ViewState("dtAccounts"), 0)

            Dim found1 = ItemExists(index1, 44)

            Dim found2 = ItemExists(index1, 45)

            'Dim foundNetwork = ItemExists(index1, 9)
            'Dim foundMak = ItemExists(index1, 7)

            'Dim foundPyxis = ItemExists(index1, 13)
            'Dim foundEDM = ItemExists(index1, 3)

            'If found1 = True Or found2 = True Or foundNetwork = True Or foundMak = True Or foundPyxis = True Or foundEDM = True Then

            '    ' check for open Position request
            '    'If AccountRequestTypeLabel.Text <> "positchg" Or AccountRequestTypeLabel.Text <> "terminate" Then
            '    '    btnChangPosition.Visible = True
            '    'End If
            '    'positionrequest is not open show button

            'End If

            If found1 = True Or found2 = True Or TCLTextBox.Text <> "" Then
                If DoctorMasterNumTextBox.Text <> "" Then
                    Select Case emptypecdrbl.SelectedValue
                        Case "782", "1197", "1194"
                            EmpDoclbl.Visible = False
                            EmpDoctorTextBox.Visible = False

                        Case Else
                            EmpDoctorTextBox.Text = DoctorMasterNumTextBox.Text
                            EmpDoclbl.Visible = True
                            EmpDoctorTextBox.Visible = True

                    End Select
                End If

                If RoleNumLabel.Text <> "" Then
                    Dim tcldata As New CommandsSqlAndOleDb("SelInvisionUserTypeV2", Session("EmployeeConn"))

                    If PositionNumTextBox.Text <> "" And PositionNumTextBox.Text <> "0" Then

                        tcldata.AddSqlProcParameter("@rolenum", PositionNumTextBox.Text, SqlDbType.NVarChar, 8)
                    Else
                        tcldata.AddSqlProcParameter("@rolenum", RoleNumLabel.Text, SqlDbType.NVarChar, 8)
                    End If

                    If TCLTextBox.Text <> "" Then
                        tcldata.AddSqlProcParameter("@Clientupd", "yes", SqlDbType.NVarChar, 8)

                    End If

                    If TitleLabel.Text <> "" Then
                        tcldata.AddSqlProcParameter("@UserTypeCd", TitleLabel.Text, SqlDbType.NVarChar, 25)
                        '' then change the role number back

                    End If

                    tcldata.GetSqlDataset()


                    ' if account has a ecare override position and role use master role
                    ' pnlTCl.visiable = true


                    dtTCLUserTypes = tcldata.GetSqlDataTable
                    gvTCL.DataSource = dtTCLUserTypes
                    gvTCL.DataBind()


                    If dtTCLUserTypes.Rows.Count = 1 Then
                        'TCLddl.SelectedIndex = 0

                        TCLTextBox.Text = dtTCLUserTypes.Rows(0)("TCL").ToString
                        UserTypeDescTextBox.Text = dtTCLUserTypes.Rows(0)("UserTypeDesc").ToString
                        UserTypeCdTextBox.Text = dtTCLUserTypes.Rows(0)("UserTypecd").ToString

                        pnlTCl.Visible = True
                        TCLTextBox.Visible = True
                        UserTypeDescTextBox.Visible = True
                        UserTypeCdTextBox.Visible = True

                        'TCLddl.Visible = False

                        gvTCL.Visible = False

                    ElseIf dtTCLUserTypes.Rows.Count = 0 Then
                        pnlTCl.Visible = False

                    ElseIf TCLTextBox.Text <> "" And dtTCLUserTypes.Rows.Count > 1 Then
                        pnlTCl.Visible = True
                        TCLTextBox.Visible = True
                        UserTypeDescTextBox.Visible = True
                        UserTypeCdTextBox.Visible = True

                        gvTCL.Visible = False
                        'btnChangeTCL.Visible = True

                    ElseIf TCLTextBox.Text = "" And dtTCLUserTypes.Rows.Count > 1 Then
                        pnlTCl.Visible = True
                        TCLTextBox.Visible = True
                        UserTypeDescTextBox.Visible = True
                        UserTypeCdTextBox.Visible = True

                        gvTCL.Visible = False

                        If Session("SecurityLevelNum") = 95 Or Session("LoginID").ToString.ToLower = "burgina" Or Session("LoginID").ToString.ToLower = "melej" Or Session("LoginID").ToString.ToLower = "atkm00" Then

                            btnChangeTCL.Text = "Select TCL"
                            btnChangeTCL.Enabled = True
                            'btnChangeTCL.Visible = True

                        End If


                    Else
                            pnlTCl.Visible = True
                        TCLTextBox.Visible = True
                        UserTypeDescTextBox.Visible = True
                        UserTypeCdTextBox.Visible = True

                        gvTCL.Visible = True

                    End If

                Else


                    pnlTCl.Visible = True

                    TCLTextBox.Visible = True
                    UserTypeDescTextBox.Visible = True
                    UserTypeCdTextBox.Visible = True


                End If

                TCLTextBox.Enabled = False
                UserTypeDescTextBox.Enabled = False
                UserTypeCdTextBox.Enabled = False
            Else
                '' new test 
                If RoleNumLabel.Text <> "" Then
                    Dim tcldata As New CommandsSqlAndOleDb("SelInvisionUserTypeV2", Session("EmployeeConn"))

                    If PositionNumTextBox.Text <> "" And PositionNumTextBox.Text <> "0" Then

                        tcldata.AddSqlProcParameter("@rolenum", PositionNumTextBox.Text, SqlDbType.NVarChar, 8)
                    Else
                        tcldata.AddSqlProcParameter("@rolenum", RoleNumLabel.Text, SqlDbType.NVarChar, 8)
                    End If

                    If TCLTextBox.Text <> "" Then
                        tcldata.AddSqlProcParameter("@Clientupd", "yes", SqlDbType.NVarChar, 8)

                    End If

                    If TitleLabel.Text <> "" Then
                        tcldata.AddSqlProcParameter("@UserTypeCd", TitleLabel.Text, SqlDbType.NVarChar, 25)
                        '' then change the role number back

                    End If

                    tcldata.GetSqlDataset()


                    ' if account has a ecare override position and role use master role
                    ' pnlTCl.visiable = true


                    dtTCLUserTypes = tcldata.GetSqlDataTable
                    gvTCL.DataSource = dtTCLUserTypes
                    gvTCL.DataBind()


                    If dtTCLUserTypes.Rows.Count = 1 Then
                        'TCLddl.SelectedIndex = 0

                        TCLTextBox.Text = dtTCLUserTypes.Rows(0)("TCL").ToString
                        UserTypeDescTextBox.Text = dtTCLUserTypes.Rows(0)("UserTypeDesc").ToString
                        UserTypeCdTextBox.Text = dtTCLUserTypes.Rows(0)("UserTypecd").ToString

                        pnlTCl.Visible = True
                        TCLTextBox.Visible = True
                        UserTypeDescTextBox.Visible = True
                        UserTypeCdTextBox.Visible = True

                        'TCLddl.Visible = False

                        gvTCL.Visible = False

                    ElseIf dtTCLUserTypes.Rows.Count = 0 Then
                        pnlTCl.Visible = False

                    ElseIf TCLTextBox.Text <> "" And dtTCLUserTypes.Rows.Count > 1 Then
                        pnlTCl.Visible = True
                        TCLTextBox.Visible = True
                        UserTypeDescTextBox.Visible = True
                        UserTypeCdTextBox.Visible = True

                        gvTCL.Visible = False
                        ' btnChangeTCL.Visible = True

                    ElseIf TCLTextBox.Text = "" And dtTCLUserTypes.Rows.Count > 1 Then
                        pnlTCl.Visible = True
                        TCLTextBox.Visible = True
                        UserTypeDescTextBox.Visible = True
                        UserTypeCdTextBox.Visible = True

                        gvTCL.Visible = False

                        If Session("SecurityLevelNum") = 95 Or Session("LoginID").ToString.ToLower = "burgina" Or Session("LoginID").ToString.ToLower = "melej" Or Session("LoginID").ToString.ToLower = "atkm00" Then

                            btnChangeTCL.Text = "Select TCL"
                            btnChangeTCL.Enabled = True
                            ' btnChangeTCL.Visible = True

                        End If


                    Else
                            pnlTCl.Visible = True
                        TCLTextBox.Visible = True
                        UserTypeDescTextBox.Visible = True
                        UserTypeCdTextBox.Visible = True

                        gvTCL.Visible = True

                    End If

                Else


                    pnlTCl.Visible = True

                    TCLTextBox.Visible = True
                    UserTypeDescTextBox.Visible = True
                    UserTypeCdTextBox.Visible = True


                End If
            End If
            Dim sLoginNow As String = Session("LoginID").ToString.ToLower

            'Select Case sLoginNow
            '    Case "geip00", "melej", "mizem", "evaa03", "burgina"

            '        If loginnameTextBox.Text <> "" Then

            '            'RetriveLDAP(loginnameTextBox.Text)

            '            CheckADFields()


            '            'btnUpdldapdemo.Visible = True
            '            'btnsearcldap.Visible = True

            '        End If

            '        If securitylevel.Text <> "" Then
            '            Session("SecurityLevelNum") = CInt(securitylevel.Text)

            '        End If


            '    Case Else


            'End Select

            'If Session("LoginID").ToString.ToLower = "geip00" Or Session("LoginID").ToString.ToLower = "melej" Then

            '	If loginnameTextBox.Text <> "" Then

            '		RetriveLDAP(loginnameTextBox.Text)

            '		CheckADFields()


            '		'btnUpdldapdemo.Visible = True
            '		'btnsearcldap.Visible = True

            '	End If

            '	'' end postback
            '	If securitylevel.Text <> "" Then
            '		Session("SecurityLevelNum") = CInt(securitylevel.Text)

            '	End If

            'End If

            If Session("SecurityLevelNum") < 40 Then
                tpAddItems.Visible = False
                tpAddFinanItems.Visible = False
            End If

            If Session("SecurityLevelNum") < 64 Then
                Dim ctrl As Control = DataCheckingAndValidation.SetChildControlsRecursiveToEnabledFalse(tpDemographics, False)


                ' if Session("Usertype")  'ProviderAnalyst,ProviderReqestors
                'tpAddItems.Visible = False
                tpRemoveItems.Visible = False
                tpOpenRequest.Visible = False
                tpClosedRequest.Visible = False

                'Emplbl.Visible = False
                'siemensempnumLabel.Visible = False
                netlbl.Visible = False
                loginnameTextBox.Visible = False
                loginnameTextBox.Enabled = False

                btnPrintClient.Visible = False
                btnSelectOnBehalfOf.Visible = False


                Select Case emptypecdrbl.SelectedValue
                    Case "782", "1197", "1194"
                        sIsPhysician = "yes"

                        'Case "physician", "non staff clinician", "resident", "allied health"

                        'If Session("Usertype") = "ProviderAnalyst" Or Session("Usertype") = "ProviderReqestors" Then
                        '    tpAddItems.Visible = True
                        '    tpRemoveItems.Visible = True

                        'End If

                End Select

            End If

            If Session("SecurityLevelNum") > 79 Then
                tpLDAP.Visible = True
                GetLDAP()
            End If


        End If

        ' END of POST BACK

        Select Case emptypecdrbl.SelectedValue
            Case "782", "1197", "1194"

                If Session("SecurityLevelNum") < 79 Then
                    Dim ctrl As Control = DataCheckingAndValidation.SetChildControlsRecursiveToEnabledFalse(tpDemographics, False)
                    btnSubmitDemoChange.Visible = True
                    btnSubmitDemoChange.Enabled = True

                ElseIf Session("SecurityLevelNum") < 89 Then
                    Dim ctrl As Control = DataCheckingAndValidation.SetChildControlsRecursiveToEnabledFalse(tpDemographics, False)

                    btnUpdate.Visible = False
                    btnSubmitDemoChange.Visible = True
                    btnSubmitDemoChange.Enabled = True
                    'DoctorMasterNumTextBox.Enabled = True
                    'firstnameTextBox.Enabled = False
                    'miTextBox.Enabled = False
                    'lastnameTextBox.Enabled = False
                    'address1textbox.Enabled = False
                    'address2textbox.Enabled = False
                    'citytextbox.Enabled = False
                    'lblstate.Enabled = False
                    'statetextbox.Enabled = False
                    'lblzip.Enabled = False
                    'ziptextbox.Enabled = False
                    'phoneTextBox.Enabled = False
                    'faxtextbox.Enabled = False
                Else
                    btnUpdate.Visible = True
                    btnSubmitDemoChange.Visible = True
                    DoctorMasterNumTextBox.Enabled = True
                End If

                ' turn this off for everyone
                emptypecdrbl.Enabled = False
        End Select


        ' unlock 2015
        'Select Case userDepartmentCD.Text
        '    Case "832600", "832700", "823100"
        '        If Session("SecurityLevelNum") < 80 And requestor_client_numLabel.Text = submitter_client_numLabel.Text Then
        '            btnSelectOnBehalfOf.Visible = True
        '            lblValidation.Text = "To Add Accounts You Must Change Requestor"
        '            lblValidation.Visible = True
        '            btnSelectOnBehalfOf.BackColor() = Color.Yellow
        '            btnSelectOnBehalfOf.BorderColor() = Color.Black
        '            btnSelectOnBehalfOf.ForeColor() = Color.Black
        '            btnSelectOnBehalfOf.BorderStyle() = BorderStyle.Solid
        '            btnSelectOnBehalfOf.BorderWidth = Unit.Pixel(2)
        '            btnSelectOnBehalfOf.Focus()
        '            'Return
        '        ElseIf Session("SecurityLevelNum") < 90 And requestor_client_numLabel.Text <> submitter_client_numLabel.Text Then
        '            lblValidation.Text = ""
        '            btnSelectOnBehalfOf.BackColor() = Color.FromKnownColor("336666")
        '            btnSelectOnBehalfOf.ForeColor() = Color.White
        '            btnSelectOnBehalfOf.BorderStyle() = BorderStyle.Groove
        '            btnSelectOnBehalfOf.BorderWidth = Unit.Pixel(1)

        '        End If
        'End Select

        Select Case emptypecdrbl.SelectedValue
            Case "782", "1197" ', "1194"
                sIsPhysician = "yes"

                'Case "physician", "non staff clinician", "resident", "allied health"
                ' no one should change a physicians who is a Crozer employee to any other Role
                If siemensempnumTextBox.Text <> "" And DoctorMasterNumTextBox.Text <> "" Then
                    emptypecdrbl.Enabled = False
                End If


        End Select


        If Session("SecurityLevelNum") < 50 Then

            tpCurrentAccounts.Visible = False
            btnUpdate.Visible = False
            btnSubmitDemoChange.Visible = False
            siemensempnumTextBox.Visible = False
            tpRequestItems.Visible = False


        End If

        If Session("SecurityLevelNum") = 65 Then

            'tpCurrentAccounts.Visible = False
            tpRemoveItems.Visible = False
            btnUpdate.Visible = False
            btnSubmitDemoChange.Visible = False
            siemensempnumTextBox.Visible = False
            btnChangeType.Visible = False

        End If

        ' not to allow 55 request accounts for themeselves
        If Session("SecurityLevelNum") = 40 And Session("ClientNum") = iClientNum Then

            tpAddItems.Visible = False
            tpRemoveItems.Visible = False
            btnPrintClient.Visible = False
            btnSelectOnBehalfOf.Visible = False
            btnUpdate.Visible = False
            btnSubmitDemoChange.Visible = False
            tpAddFinanItems.Visible = False
        End If

        If Session("SecurityLevelNum") <= 55 And AccountRequestTypeLabel.Text <> "" Then
            tpAddItems.Visible = False
            TerminateLabel.Text = "This Client has open an request To Add more Accounts please contact Help Desk 15-2610."
            TerminateLabel.Visible = True
            btnUpdate.Visible = False
            btnSubmitDemoChange.Visible = False
            tpAddFinanItems.Visible = False
        End If

        ' R4 security 
        If Session("SecurityLevelNum") = 70 Then
            ' R4 
            tpAddItems.Visible = False
            tpRemoveItems.Visible = False
            btnPrintClient.Visible = False
            btnSelectOnBehalfOf.Visible = False
            btnUpdate.Visible = False
            btnSubmitDemoChange.Visible = False
            tpAddFinanItems.Visible = False
            btnChangeType.Visible = False

        End If

        If emptypecdrbl.SelectedValue <> RoleNumLabel.Text Then

            'SpecialtyLabel.Text = ""
            'mainRole = emptypecdrbl.SelectedValue
            'RoleNumLabel.Text = emptypecdrbl.SelectedValue
            'emptypecdLabel.Text = emptypecdrbl.SelectedValue
            'TitleLabel.Text = Nothing

            Select Case emptypecdrbl.SelectedValue
                Case "782", "1197"
                    sIsPhysician = "yes"

                    'Case "physician", "non staff clinician", "resident"

                    Dim ddbTitleBinder As New DropDownListBinder(Titleddl, "SelTitlesV4", Session("EmployeeConn"))
                    ddbTitleBinder.AddDDLCriteria("@titleType", "Physician", SqlDbType.NVarChar)
                    ddbTitleBinder.BindData("TitleShortDesc", "TitleDesc")

                    If Session("Usertype") = "ProviderAnalyst" Or Session("Usertype") = "ProviderReqestors" Then
                        tpAddItems.Visible = True
                        tpRemoveItems.Visible = True

                    End If

                    If TitleLabel.Text = "" Then
                        Titleddl.SelectedIndex = -1
                    Else
                        Titleddl.SelectedValue = TitleLabel.Text

                    End If

                    If SpecialtyLabel.Text = "" Then
                        Specialtyddl.SelectedIndex = -1
                    Else
                        Specialtyddl.SelectedValue = SpecialtyLabel.Text

                    End If

                Case "1194"
                    'Dim ddlBinder As New DropDownListBinder
                    sIsPhysician = "yes"

                    Dim ddlRoleBinder As New DropDownListBinder(Rolesddl, "SelEmployeeRolesV3", Session("EmployeeConn"))
                    ddlRoleBinder.AddDDLCriteria("@RoleNum", "1194", SqlDbType.NVarChar)
                    ddlRoleBinder.BindData("rolenum", "RoleDesc")

                    Dim ddbTitleBinder As New DropDownListBinder(Titleddl, "SelTitlesV4", Session("EmployeeConn"))
                    ddbTitleBinder.AddDDLCriteria("@titleType", "Resident", SqlDbType.NVarChar)
                    ddbTitleBinder.BindData("TitleShortDesc", "TitleDesc")




                    If TitleLabel.Text = "" Then
                        Titleddl.SelectedIndex = -1
                    ElseIf userpositiondescTextBox.Text <> "" Then
                        Titleddl.SelectedValue = userpositiondescTextBox.Text
                    Else

                        Titleddl.SelectedValue = TitleLabel.Text

                    End If

                    'If SpecialtyLabel.Text = "" Then
                    '    Specialtyddl.SelectedIndex = -1
                    'Else
                    '    Specialtyddl.SelectedValue = SpecialtyLabel.Text

                    'End If

                    If TitleLabel.Text = "" Then
                        Title2Label.Text = "Resident"

                    Else
                        Title2Label.Text = TitleLabel.Text

                    End If
                    Title2Label.Visible = True
                    Titleddl.Visible = False

                    SpecialtyLabel.Text = "Resident"
                    SpecialtyLabel.Visible = True
                    Specialtyddl.Visible = False


                    SpecialtyLabel.Text = "Resident"
                    SpecialtyLabel.Visible = True
                    Specialtyddl.Visible = False

                    credentialedDateTextBox.Enabled = False
                    'credentialedDateTextBox.CssClass = "style2"

                    'CredentialedDateDCMHTextBox.Enabled = False
                    'CredentialedDateDCMHTextBox.CssClass = "style2"

                    CCMCadmitRightsrbl.SelectedValue = "No"
                    CCMCadmitRightsrbl.Enabled = False

                    'DCMHadmitRightsrbl.SelectedValue = "No"
                    'DCMHadmitRightsrbl.Enabled = False


            End Select


        End If
        If mainRole Is Nothing Then
            mainRole = RoleNumLabel.Text

        End If



        'groupnumTextBox.Enabled = False
        InvisionGroupNumTextBox.Enabled = False

        'buildingcdTextBox.ID = "building_cdTextBoxHidden"
        fillOpenRequestTable()
        fillClosedRequestTable()
        FillCurrentAccounts() ' View of current accounts


        CurrentAccounts() ' tab to remove current accounts

        AvailableAccounts() ' account to add to this account

        AvailFinancialAccounts() ' fincnial accounts

        Docmasters()



        CheckRequiredFields()

        If gvCurrentApps.Rows.Count < 1 Then
            loginnameTextBox.Enabled = False
        End If


        If Session("SecurityLevelNum") = 82 Or Session("SecurityLevelNum") = 95 Or Session("LoginID").ToString.ToLower = "atkm00" Or Session("LoginID").ToString.ToLower = "melej" Or Session("LoginID").ToString.ToLower = "burgina" Then
            'Session("SecurityLevelNum") = 82 Or Session("LoginID") = "melej" Or Session("LoginID") = "geip00" Or Session("LoginID") = "RalphC" Then
            Dim ctrl As Control = DataCheckingAndValidation.SetChildControlsRecursiveToEnabledFalse(tpDemographics, True)

            btnUpdate.Visible = True
            btnUpdate.Enabled = True

            Select Case emptypecdrbl.SelectedValue
                Case "782", "1197" ', "1194"

                    btnSubmitDemoChange.Visible = True
                    btnSubmitDemoChange.Enabled = True

                    credentialedDateTextBox.Enabled = True
                    credentialedDateTextBox.CssClass = "calenderClass"

                    'CredentialedDateDCMHTextBox.Enabled = True
                    'CredentialedDateDCMHTextBox.CssClass = "calenderClass"

                    'credentialedDateTextBox.Enabled = True
                    'credentialedDateTextBox.CssClass = "calenderClass"

                    'CredentialedDateDCMHTextBox.Enabled = True
                    'CredentialedDateDCMHTextBox.CssClass = "calenderClass"
                    'Case "physician", "non staff clinician", "resident", "allied health"
                    ' no one should change a physicians who is a Crozer employee to any other Role
                    If siemensempnumTextBox.Text <> "" And DoctorMasterNumTextBox.Text <> "" Then
                        emptypecdrbl.Enabled = False
                    End If


                    ' show button non privideged
                    If CCMCadmitRightsrbl.SelectedValue.ToLower = "yes" Then
                        btnNonPrivileged.Visible = True
                    End If


                    ' show button non privideged
                    'If DCMHadmitRightsrbl.SelectedValue.ToLower = "yes" Then
                    '    btnNonPrivileged.Visible = True
                    'End If

                    If CCMCadmitRightsrbl.SelectedValue.ToLower = "no" Then

                        credentialedDateTextBox.Enabled = False
                        credentialedDateTextBox.CssClass = "style2"

                    End If

                    'If DCMHadmitRightsrbl.SelectedValue.ToLower = "no" Then
                    '    CredentialedDateDCMHTextBox.Enabled = False
                    '    CredentialedDateDCMHTextBox.CssClass = "style2"

                    'End If

            End Select

        Else

            'OpenRequestCount


            credentialedDateTextBox.Enabled = False
            'CCMCadmitRightsrbl.Enabled = False
            '    credentialedDateTextBox.CssClass = "style2"

            'CredentialedDateDCMHTextBox.Enabled = False
            ' DCMHadmitRightsrbl.Enabled = False
            '    CredentialedDateDCMHTextBox.CssClass = "style2"


        End If

        AlternatePhoneTextBox.Enabled = False

        Select Case emptypecdrbl.SelectedValue
            Case "782", "1194"

                ' turn this off for everyone
                emptypecdrbl.Enabled = False
                'If PositionRoleNumTextBox.Text = "1553" Then
                '    ' Lock position drop down
                '    Rolesddl.Enabled = False
                'End If

            Case "1193"
                If PositionRoleNumTextBox.Text = "1567" Then
                    ' Lock position drop down
                    'Rolesddl.Enabled = False
                End If


        End Select





        If mainRole = "1195" Or mainRole = "1198" Then

            VendorNameTextBox.Enabled = True
        Else
            VendorNameTextBox.Enabled = False

        End If

        '     Select PositionRoleNumTextBox.Text
        '        Case "1554", "1560"

        '            Dim ddbVendorBinder As New DropDownListBinder(Vendorddl, "SelVendors", Session("EmployeeConn"))

        '            ddbVendorBinder.AddDDLCriteria("@type", "prospect", SqlDbType.NVarChar)
        '            ddbVendorBinder.BindData("VendorNum", "VendorName")


        '            If VendorNameTextBox.Text = "" Then
        '                Vendorddl.SelectedIndex = -1
        '            Else
        '                Vendorddl.SelectedValue = VendorNumLabel.Text

        'End If

        '            If VendorNumLabel.Text = "0" Then
        '                otherVendorlbl.Visible = True
        '                VendorUnknowlbl.Text = VendorNameTextBox.Text
        '                VendorUnknowlbl.Visible = True
        '            End If
        '    End Select


        If siemensempnumTextBox.Text = "" Then
            'If sIsPhysician <> "yes" Then


            If AccountCount = 0 And OpenRequestCount = 0 Then

                If Session("SecurityLevelNum") > 85 Then ' And Session("SecurityLevelNum") <> 82 Then
                    btnDeactivateClient.Visible = True

                End If

            End If

            If AccountCount > 0 And OpenRequestCount = 0 Then

                If Session("SecurityLevelNum") = 95 Or Session("LoginID").ToString.ToLower = "melej" Or Session("LoginID").ToString.ToLower = "burgina" Then
                    btnDeactivateClient.Visible = True

                End If

            End If

            ' End If

        End If

        ' TCL 
        Dim index2 = BuildIndex(ViewState("dtAccounts"), 0)

        Dim found3 = ItemExists(index2, 44)

        Dim found4 = ItemExists(index2, 45)

        If found3 = True Or found4 = True Then

            If RoleNumLabel.Text <> "" Then
                Select Case btnChangeTCL.Text ' <> "Show All User Types" Or btnChangeTCL.Text <> "Close Types" Then
                    Case "Select TCL", ""



                        Dim tcldata As New CommandsSqlAndOleDb("SelInvisionUserTypeV2", Session("EmployeeConn"))
                        If PositionNumTextBox.Text <> "" Then
                            tcldata.AddSqlProcParameter("@rolenum", PositionNumTextBox.Text, SqlDbType.NVarChar, 8)
                        Else
                            tcldata.AddSqlProcParameter("@rolenum", RoleNumLabel.Text, SqlDbType.NVarChar, 8)
                        End If

                        If TCLTextBox.Text <> "" Then
                            tcldata.AddSqlProcParameter("@Clientupd", "yes", SqlDbType.NVarChar, 8)

                        End If


                        If TitleLabel.Text <> "" Then
                            tcldata.AddSqlProcParameter("@UserTypeCd", TitleLabel.Text, SqlDbType.NVarChar, 25)

                        End If


                        tcldata.GetSqlDataset()




                        dtTCLUserTypes = tcldata.GetSqlDataTable
                        gvTCL.DataSource = dtTCLUserTypes
                        gvTCL.DataBind()

                    Case "Show All User Types", "Close Types"
                End Select

                Select Case PositionRoleNumTextBox.Text
                    Case "1193", "782"

                        pnlTCl.Visible = True
                        TCLTextBox.Visible = True
                        UserTypeDescTextBox.Visible = True
                        UserTypeCdTextBox.Visible = True

                        gvTCL.Visible = False
                        'btnChangeTCL.Text = "Select TCL"
                        If Session("SecurityLevelNum") = 95 Or Session("LoginID").ToString.ToLower = "burgina" Or Session("LoginID").ToString.ToLower = "melej" Or Session("LoginID").ToString.ToLower = "atkm00" Then

                            btnChangeTCL.Visible = True
                        End If

                        'gvTCL.BackColor() = Color.Yellow
                        'gvTCL.BorderColor() = Color.Black
                        'gvTCL.BorderStyle() = BorderStyle.Solid
                        'gvTCL.BorderWidth = Unit.Pixel(2)
                        'gvTCL.RowStyle.BackColor() = Color.Yellow



                    Case Else




                        If dtTCLUserTypes.Rows.Count = 1 Then
                            'TCLddl.SelectedIndex = 0

                            TCLTextBox.Text = dtTCLUserTypes.Rows(0)("TCL").ToString
                            UserTypeDescTextBox.Text = dtTCLUserTypes.Rows(0)("UserTypeDesc").ToString
                            UserTypeCdTextBox.Text = dtTCLUserTypes.Rows(0)("UserTypecd").ToString

                            pnlTCl.Visible = True
                            TCLTextBox.Visible = True
                            UserTypeDescTextBox.Visible = True
                            UserTypeCdTextBox.Visible = True

                            'TCLddl.Visible = False

                            gvTCL.Visible = False

                        ElseIf dtTCLUserTypes.Rows.Count = 0 Then
                            pnlTCl.Visible = False

                        ElseIf TCLTextBox.Text = "" And dtTCLUserTypes.Rows.Count > 1 Then
                            If Session("SecurityLevelNum") = 95 Or Session("LoginID").ToString.ToLower = "burgina" Or Session("LoginID").ToString.ToLower = "melej" Or Session("LoginID").ToString.ToLower = "atkm00" Then

                                pnlTCl.Visible = True
                                TCLTextBox.Visible = True
                                UserTypeDescTextBox.Visible = True
                                UserTypeCdTextBox.Visible = True

                                gvTCL.Visible = False
                                btnChangeTCL.Text = "Select TCL"

                                btnChangeTCL.Visible = True

                            End If


                        ElseIf TCLTextBox.Text <> "" And dtTCLUserTypes.Rows.Count > 1 Then
                            If Session("SecurityLevelNum") = 95 Or Session("LoginID").ToString.ToLower = "burgina" Or Session("LoginID").ToString.ToLower = "melej" Or Session("LoginID").ToString.ToLower = "atkm00" Then

                                pnlTCl.Visible = True
                                TCLTextBox.Visible = True
                                UserTypeDescTextBox.Visible = True
                                UserTypeCdTextBox.Visible = True

                                gvTCL.Visible = False
                                btnChangeTCL.Visible = True
                            End If

                        ElseIf Session("SecurityLevelNum") = 95 Or Session("LoginID").ToString.ToLower = "burgina" Or Session("LoginID").ToString.ToLower = "melej" Or Session("LoginID").ToString.ToLower = "atkm00" Then

                            pnlTCl.Visible = True
                            TCLTextBox.Visible = True
                            UserTypeDescTextBox.Visible = True
                            UserTypeCdTextBox.Visible = True

                            gvTCL.Visible = True
                            gvTCL.BackColor() = Color.Yellow
                            gvTCL.BorderColor() = Color.Black
                            gvTCL.BorderStyle() = BorderStyle.Solid
                            gvTCL.BorderWidth = Unit.Pixel(2)
                            gvTCL.RowStyle.BackColor() = Color.Yellow

                        End If
                End Select



            Else

                pnlTCl.Visible = True

                TCLTextBox.Visible = True
                UserTypeDescTextBox.Visible = True
                UserTypeCdTextBox.Visible = True


            End If

            If Session("SecurityLevelNum") < 50 Then

                gvTCL.Visible = False
            End If



        End If


        Dim Accountlogin As String = ""

        Accountlogin = Session("LoginID").ToString.ToLower

        Select Case Accountlogin
            Case "atkm00", "barl19", "geip00", "melej", "durr05", "burgina"
                siemensempnumTextBox.Enabled = True
                'btnNewTicketRequest.Visible = True

                If totAcct > 1 Then
                    btnChangeType.Visible = True
                Else
                    emptypecdrbl.Enabled = False
                End If

            Case Else

        End Select

        Dim sLevel As String = Session("SecurityLevelNum").ToString

        Select Case sLevel
            Case "95"

                siemensempnumTextBox.Enabled = True
                'btnNewTicketRequest.Visible = True

                If totAcct > 1 Then
                    btnChangeType.Visible = True
                Else
                    emptypecdrbl.Enabled = False
                End If
            Case Else

        End Select

        'If Session("LoginID").ToString.ToLower = "geip00" Or Session("LoginID").ToString.ToLower = "melej" Or Session("LoginID").ToString.ToLower = "durr05" Or Session("LoginID").ToString.ToLower = "higginsd" Then
        '          siemensempnumTextBox.Enabled = True
        '          If totAcct > 1 Then
        '              btnChangeType.Visible = True
        '          Else
        '              emptypecdrbl.Enabled = False
        '          End If

        '          btnNewTicketRequest.Visible = True
        '      End If

        'If Session("SecurityLevelNum") > 69 Then
        '          btnNewTicketRequest.Visible = True
        '          btnNewTicketRequest.Enabled = True
        '      End If

        If Session("SecurityLevelNum") = 95 Or Session("LoginID").ToString.ToLower = "geip00" Or Session("LoginID").ToString.ToLower = "melej" Or Session("LoginID").ToString.ToLower = "atkm00" Or Session("LoginID").ToString.ToLower = "burgina" Then
            docMstrPnl.Enabled = True

        Else
            docMstrPnl.Enabled = False

        End If

        fillRequestItemsTable()
        'fillCPMClasseTable()
        ' R4 Security = 70
        ' our CSC 72 
        ' until change 
        If Session("SecurityLevelNum") > 69 Then

            If totAcct > 1 Then
                btnChangeType.Visible = True
            Else
                emptypecdrbl.Enabled = False
            End If
        End If

        If AccountRequestTypeLabel.Text = "terminate" Then
            'btnNewTicketRequest.Visible = True

            TerminateLabel.Visible = True

            btnSelectOnBehalfOf.Visible = False
            btnUpdate.Visible = False
            btnSubmitDemoChange.Visible = False
            tpAddItems.Visible = False
            tpRemoveItems.Visible = False
            tpAddFinanItems.Visible = False


        End If




        'ViewCPMModelTextBox.Enabled = False

        TCLTextBox.Enabled = False
        UserTypeDescTextBox.Enabled = False
        UserTypeCdTextBox.Enabled = False

        'If Session("LoginID").ToString.ToLower = "geip00" Or Session("LoginID").ToString.ToLower = "melej" Then

        '	btnNewTicketRequest.Visible = True

        'Else

        '	btnNewTicketRequest.Visible = False


        'End If

        'displayPendingQueues()

    End Sub
    Public Sub GetLDAP()



        'SeLAllUsersADAccountNames


        Dim thisdata As New CommandsSqlAndOleDb("SeLAllUsersADAccountNames", Session("EmployeeConn"))
        thisdata.AddSqlProcParameter("@client_num", iClientNum, SqlDbType.Int, 20)

        dtAccounts = thisdata.GetSqlDataTable

        If dtAccounts.Rows.Count > 1 Then
            Dim ddAccountBinder As New DropDownListBinder(ADAccountddl, "SeLAllUsersADAccountNames", Session("EmployeeConn"))
            ddAccountBinder.AddDDLCriteria("@client_num", iClientNum, SqlDbType.NVarChar)
            ddAccountBinder.BindData("LoginName", "LoginName")

            LDAPLoginNameTextbox.Visible = True
            ADaccountslbl.Visible = True
            ADAccountddl.Visible = True
            BtnSearchLDAP.Visible = True

        End If

        Dim LDAPControl As New LDAPController(iClientNum, Session("EmployeeConn"))

        LDAPControl.GetAllLDAPItems()

        'AllLDAPItems = LDAPControl.AllLDAPItems()
        LDAPLoginNameTextbox.Text = LDAPControl.LDAPLoginName
        FNameTextBox.Text = LDAPControl.Fname
        LnameTextBox.Text = LDAPControl.LName
        LDAPeMailTextBox.Text = LDAPControl.LDAPEmail
        LDAPtelephonenumberTextBox.Text = LDAPControl.LDAPtelephonenumber
        LastVerificationDateTextBox.Text = LDAPControl.LastVerificationDate
        modifytimestampTextBox.Text = LDAPControl.modifytimestamp
        LastDeviceLogonTextBox.Text = LDAPControl.LastDeviceLogon
        LastDomainLogonTextBox.Text = LDAPControl.LastDomainLogon
        userAccountControlTextBox.Text = LDAPControl.userAccountControl
        LockOutDateTextBox.Text = LDAPControl.LockOutDate
        LDAPEmpnumTextBox.Text = LDAPControl.LDAPEmpnum
        LDAPdepartmentTextBox.Text = LDAPControl.LDAPdepartment

    End Sub
    Private Sub BtnSearchLDAP_Click(sender As Object, e As EventArgs) Handles BtnSearchLDAP.Click

        LDAPLoginNameTextbox.Text = ""
        FNameTextBox.Text = ""
        LnameTextBox.Text = ""
        LDAPeMailTextBox.Text = ""
        LDAPtelephonenumberTextBox.Text = ""
        LastVerificationDateTextBox.Text = ""
        modifytimestampTextBox.Text = ""
        LastDeviceLogonTextBox.Text = ""
        LastDomainLogonTextBox.Text = ""
        userAccountControlTextBox.Text = ""
        LockOutDateTextBox.Text = ""
        LDAPEmpnumTextBox.Text = ""
        LDAPdepartmentTextBox.Text = ""


        Dim LDAPControl As New LDAPController(iClientNum, SelectedADlbl.Text, Session("EmployeeConn"))

        LDAPControl.GetAllLDAPItems()

        'AllLDAPItems = LDAPControl.AllLDAPItems()
        LDAPLoginNameTextbox.Text = LDAPControl.LDAPLoginName
        FNameTextBox.Text = LDAPControl.Fname
        LnameTextBox.Text = LDAPControl.LName
        LDAPeMailTextBox.Text = LDAPControl.LDAPEmail
        LDAPtelephonenumberTextBox.Text = LDAPControl.LDAPtelephonenumber
        LastVerificationDateTextBox.Text = LDAPControl.LastVerificationDate
        modifytimestampTextBox.Text = LDAPControl.modifytimestamp
        LastDeviceLogonTextBox.Text = LDAPControl.LastDeviceLogon
        LastDomainLogonTextBox.Text = LDAPControl.LastDomainLogon
        userAccountControlTextBox.Text = LDAPControl.userAccountControl
        LockOutDateTextBox.Text = LDAPControl.LockOutDate
        LDAPEmpnumTextBox.Text = LDAPControl.LDAPEmpnum
        LDAPdepartmentTextBox.Text = LDAPControl.LDAPdepartment

    End Sub

    Private Sub ADAccountddl_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ADAccountddl.SelectedIndexChanged

        SelectedADlbl.Text = ADAccountddl.SelectedValue


    End Sub
    Private Function BuildIndex(table As DataTable, keyColumnIndex As Integer) As List(Of String)
        Dim index As New List(Of String)(table.Rows.Count)
        For Each row As DataRow In table.Rows
            index.Add(row(keyColumnIndex))
        Next
        index.Sort()
        
        Return index
    End Function
    Private Function ItemExists(index As List(Of String), key As String) As Boolean
        Dim newindex As Integer = index.BinarySearch(key)
        If newindex >= 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub AvailableAccounts()

        grAvailableAccounts.DataSource = ViewState("dtAvailableAccounts")
        grAvailableAccounts.DataBind()

    End Sub
    Protected Sub AvailFinancialAccounts()

        GrFinAvailable.DataSource = ViewState("dtAvailFincnialAccounts")
        GrFinAvailable.DataBind()

    End Sub
    Protected Sub CurrentAccounts()
        ' Load All account types Fincial and Clinical 
        gvCurrentApps.DataSource = ViewState("dtAccounts")
        gvCurrentApps.DataBind()

    End Sub
    Protected Sub Docmasters()
        gvDocmster.DataSource = ViewState("dtDocmster")
        gvDocmster.DataBind()


    End Sub
    Protected Sub GetTCL()

        Dim tcldata As New CommandsSqlAndOleDb("SelInvisionUserTypeV2", Session("EmployeeConn"))
        If PositionNumTextBox.Text <> "" And PositionNumTextBox.Text <> "0" Then
            tcldata.AddSqlProcParameter("@rolenum", PositionNumTextBox.Text, SqlDbType.NVarChar, 8)
        Else
            tcldata.AddSqlProcParameter("@rolenum", RoleNumLabel.Text, SqlDbType.NVarChar, 8)
        End If
        If TCLTextBox.Text <> "" Then
            tcldata.AddSqlProcParameter("@Clientupd", "yes", SqlDbType.NVarChar, 8)

        End If


        If TitleLabel.Text <> "" Then
            tcldata.AddSqlProcParameter("@UserTypeCd", TitleLabel.Text, SqlDbType.NVarChar, 25)

        End If


        tcldata.GetSqlDataset()




        dtTCLUserTypes = tcldata.GetSqlDataTable
        gvTCL.DataSource = dtTCLUserTypes
        gvTCL.DataBind()


        If dtTCLUserTypes.Rows.Count = 1 Then
            'TCLddl.SelectedIndex = 0

            TCLTextBox.Text = dtTCLUserTypes.Rows(0)("TCL").ToString
            UserTypeDescTextBox.Text = dtTCLUserTypes.Rows(0)("UserTypeDesc").ToString
            UserTypeCdTextBox.Text = dtTCLUserTypes.Rows(0)("UserTypecd").ToString

            pnlTCl.Visible = True
            TCLTextBox.Visible = True
            UserTypeDescTextBox.Visible = True
            UserTypeCdTextBox.Visible = True

            'TCLddl.Visible = False

            gvTCL.Visible = False

        ElseIf dtTCLUserTypes.Rows.Count = 0 Then
            pnlTCl.Visible = False

        ElseIf TCLTextBox.Text <> "" And dtTCLUserTypes.Rows.Count > 1 Then
            pnlTCl.Visible = True
            TCLTextBox.Visible = True
            UserTypeDescTextBox.Visible = True
            UserTypeCdTextBox.Visible = True

            gvTCL.Visible = True
            'btnChangeTCL.Visible = True

        ElseIf TCLTextBox.Text = "" And dtTCLUserTypes.Rows.Count > 1 Then
            pnlTCl.Visible = True
            TCLTextBox.Visible = True
            UserTypeDescTextBox.Visible = True
            UserTypeCdTextBox.Visible = True

            gvTCL.Visible = True

            btnChangeTCL.Text = "Close Types"

            'btnChangeTCL.Visible = True

        End If

    End Sub
    Private Sub CernerPosddl_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CernerPosddl.SelectedIndexChanged

        If CernerPosddl.SelectedValue.ToString.ToLower <> "select" Then
            CernerPositionDescTextBox.Text = CernerPosddl.SelectedValue.ToString
        End If



    End Sub

    Protected Sub btnSubmitAddRequest_Click(sender As Object, e As System.EventArgs) Handles btnSubmitAddRequest.Click

        If Session("SecurityLevelNum") < 90 And iClientNum <> submitter_client_numLabel.Text Then
            btnSelectOnBehalfOf.Visible = False
        ElseIf iClientNum = submitter_client_numLabel.Text Then
            btnSelectOnBehalfOf.Visible = True
            lblValidation.Text = "Must Change Requestor"
            lblValidation.Visible = True
            btnSelectOnBehalfOf.BackColor() = Color.Yellow
            btnSelectOnBehalfOf.BorderColor() = Color.Black
            btnSelectOnBehalfOf.ForeColor() = Color.Black
            btnSelectOnBehalfOf.BorderStyle() = BorderStyle.Solid
            btnSelectOnBehalfOf.BorderWidth = Unit.Pixel(2)
            Return

        End If

        '   Select Case userDepartmentCD.Text
        '       Case "832600", "832700", "823100"
        '           If Session("SecurityLevelNum") <90 And requestor_client_numLabel.Text= submitter_client_numLabel.Text Then
        'btnSelectOnBehalfOf.Visible= True
        'lblValidation.Text="Must Change Requestor"
        '							   lblValidation.Visible= True
        'btnSelectOnBehalfOf.BackColor() = Color.Yellow
        '               btnSelectOnBehalfOf.BorderColor() = Color.Black
        '               btnSelectOnBehalfOf.ForeColor() = Color.Black
        '               btnSelectOnBehalfOf.BorderStyle() = BorderStyle.Solid
        '               btnSelectOnBehalfOf.BorderWidth = Unit.Pixel(2)
        '               Return
        '           ElseIf Session("SecurityLevelNum") < 90 And requestor_client_numLabel.Text <> submitter_client_numLabel.Text Then
        '               btnSelectOnBehalfOf.Visible = False
        '               'lblValidation.Text = ""
        '               'btnSelectOnBehalfOf.BackColor() = Color.FromKnownColor("336666")
        '               'btnSelectOnBehalfOf.ForeColor() = Color.White
        '               'btnSelectOnBehalfOf.BorderStyle() = BorderStyle.Groove
        '               'btnSelectOnBehalfOf.BorderWidth = Unit.Pixel(1)

        '           End If



        '   End Select

        If grAccountsToAdd.Rows.Count > 0 Then


            For Each rw As GridViewRow In grAccountsToAdd.Rows
                If rw.RowType = DataControlRowType.DataRow Then

                    Dim appNum As String = grAccountsToAdd.DataKeys(rw.RowIndex)("Applicationnum")


                    Select Case appNum
                        Case "156"

                            CheckCellPhone()
                            If blnReturn = True Then
                                Exit Sub
                            End If


                    End Select
                End If

            Next



            ' NewRequestForm.AddInsertParm("@requestor_client_num", requestor_client_numLabel.Text, SqlDbType.NVarChar, 10)
            NewRequestForm.AddInsertParm("@receiver_client_num", iClientNum, SqlDbType.Int, 10)

            ' NewRequestForm.AddInsertParm("@submitter_client_num", UserInfo.ClientNum, SqlDbType.Int, 10)
            NewRequestForm.AddInsertParm("@account_request_type", "addacct", SqlDbType.NVarChar, 9)
            NewRequestForm.AddInsertParm("@AccountComment", txtComment.Text, SqlDbType.NVarChar, 500)
            NewRequestForm.UpdateFormReturnReqNum()
            AccountRequestNum = NewRequestForm.AccountRequestNum

            AccountRequest = New AccountRequest(AccountRequestNum, Session("EmployeeConn"))

            ValidationStatus.Text = AccountRequest.ValidationStatus

            Try

                ' Dim AccountCode As String

                For Each rw As GridViewRow In grAccountsToAdd.Rows
                    If rw.RowType = DataControlRowType.DataRow Then

                        Dim appNum As String = grAccountsToAdd.DataKeys(rw.RowIndex)("Applicationnum")
                        insAcctItems(AccountRequestNum, appNum, loginnameTextBox.Text, "addacct", "")
                        ' All sub Applications
                        Select Case appNum
                            Case 13, 97, 98, 99, 100, 136
								' insert Nursestation if selected
								InsertNursestations(AccountRequestNum, appNum, txtComment.Text, lblAppSystem.Text, "NeedsValidation")

							Case 65
								InsertNurseInstructor(AccountRequestNum)



						End Select

                        'If sItemComplet.Length > 2 Then
                        '    TerminateLabel.Text = "Can Not complet this request Client has open request that may include account your requesting."
                        '    TerminateLabel.Visible = True

                        'End If
                    End If

                Next

                If sItemComplet.Length > 2 Then
                    Response.Redirect("~/ClientDemo.aspx?ClientNum=" & iClientNum & "&msg=" & sItemComplet, True)
                Else
                    Response.Redirect("~/AccountRequestInfo2.aspx?RequestNum=" & AccountRequestNum, True)
                End If
            Catch ex As Exception
				errormsg = ex.Message
			End Try

            If Session("environment") = "" Then
                Dim Envoirn As New Environment()

                Session("environment") = Envoirn.getEnvironment
            End If


            SendProviderEmail(AccountRequestNum)
        Else
            btnSubmitAddRequest.Visible = False
            btnAddComments.Visible = False

        End If

    End Sub
    Protected Sub btnSubmitFinAccount_Click(sender As Object, e As System.EventArgs) Handles btnSubmitFinAccount.Click

        Select Case userDepartmentCD.Text
            Case "832600", "832700", "823100"
                If Session("SecurityLevelNum") < 90 And requestor_client_numLabel.Text = submitter_client_numLabel.Text Then
                    btnSelectOnBehalfOf.Visible = True
                    lblValidation.Text = "Must Change Requestor"
                    lblValidation.Visible = True
                    btnSelectOnBehalfOf.BackColor() = Color.Yellow
                    btnSelectOnBehalfOf.BorderColor() = Color.Black
                    btnSelectOnBehalfOf.ForeColor() = Color.Black
                    btnSelectOnBehalfOf.BorderStyle() = BorderStyle.Solid
                    btnSelectOnBehalfOf.BorderWidth = Unit.Pixel(2)
                    Return
                ElseIf Session("SecurityLevelNum") < 90 And requestor_client_numLabel.Text <> submitter_client_numLabel.Text Then
                    btnSelectOnBehalfOf.Visible = False
                    'lblValidation.Text = ""
                    'btnSelectOnBehalfOf.BackColor() = Color.FromKnownColor("336666")
                    'btnSelectOnBehalfOf.ForeColor() = Color.White
                    'btnSelectOnBehalfOf.BorderStyle() = BorderStyle.Groove
                    'btnSelectOnBehalfOf.BorderWidth = Unit.Pixel(1)

                End If



        End Select

        If GrFinAddAccount.Rows.Count > 0 Then
            ' NewRequestForm.AddInsertParm("@requestor_client_num", requestor_client_numLabel.Text, SqlDbType.NVarChar, 10)
            NewRequestForm.AddInsertParm("@receiver_client_num", iClientNum, SqlDbType.Int, 10)

            ' NewRequestForm.AddInsertParm("@submitter_client_num", UserInfo.ClientNum, SqlDbType.Int, 10)
            NewRequestForm.AddInsertParm("@account_request_type", "addacct", SqlDbType.NVarChar, 9)
            NewRequestForm.AddInsertParm("@AccountComment", txtComment.Text, SqlDbType.NVarChar, 500)
            NewRequestForm.UpdateFormReturnReqNum()
            AccountRequestNum = NewRequestForm.AccountRequestNum

            AccountRequest = New AccountRequest(AccountRequestNum, Session("EmployeeConn"))

            ValidationStatus.Text = AccountRequest.ValidationStatus

            Try

                ' Dim AccountCode As String

                For Each rw As GridViewRow In GrFinAddAccount.Rows
                    If rw.RowType = DataControlRowType.DataRow Then

                        Dim appNum As String = GrFinAddAccount.DataKeys(rw.RowIndex)("Applicationnum")
                        insAcctItems(AccountRequestNum, appNum, loginnameTextBox.Text, "addacct", "")

                        'If sItemComplet.Length > 2 Then
                        '    TerminateLabel.Text = "Can Not complet this request Client has open request that may include account your requesting."
                        '    TerminateLabel.Visible = True

                        'End If
                    End If

                Next

                If sItemComplet.Length > 2 Then
                    Response.Redirect("~/ClientDemo.aspx?ClientNum=" & iClientNum & "&msg=" & sItemComplet, True)
                Else
                    Response.Redirect("~/AccountRequestInfo2.aspx?RequestNum=" & AccountRequestNum, True)
                End If
            Catch ex As Exception

            End Try

            If Session("environment") = "" Then
                Dim Envoirn As New Environment()

                Session("environment") = Envoirn.getEnvironment
            End If


            SendProviderEmail(AccountRequestNum)
        Else
            btnSubmitAddRequest.Visible = False
            btnAddComments.Visible = False

        End If
    End Sub
    Protected Sub Titleddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles Titleddl.SelectedIndexChanged
        TitleLabel.Text = Titleddl.SelectedValue
        TitleLabel.Text = Titleddl.SelectedValue


        'TCLddl.SelectedIndex = -1
        TCLTextBox.Text = ""
        UserTypeCdTextBox.Text = ""
        UserTypeDescTextBox.Text = ""
        gvTCL.Dispose()


        Dim tcldata As New CommandsSqlAndOleDb("SelInvisionUserTypeV2", Session("EmployeeConn"))
        tcldata.AddSqlProcParameter("@rolenum", emptypecdrbl.SelectedValue, SqlDbType.NVarChar, 8)
        tcldata.AddSqlProcParameter("@UserTypeCd", Titleddl.SelectedValue, SqlDbType.NVarChar, 25)
        If TCLTextBox.Text <> "" Then
            tcldata.AddSqlProcParameter("@Clientupd", "yes", SqlDbType.NVarChar, 8)

        End If


        tcldata.GetSqlDataset()




        dtTCLUserTypes = tcldata.GetSqlDataTable
        gvTCL.DataSource = dtTCLUserTypes
        gvTCL.DataBind()

        'TCLddl.DataSource = dtTCLUserTypes

        'TCLddl.DataTextField = "UserTypeDesc"
        'TCLddl.DataValueField = "TCL"
        'TCLddl.DataBind()

        'TCLddl.Items.Insert(0, "Select")

        If dtTCLUserTypes.Rows.Count = 1 Then
            'TCLddl.SelectedIndex = 0

            TCLTextBox.Text = dtTCLUserTypes.Rows(0)("TCL").ToString
            UserTypeDescTextBox.Text = dtTCLUserTypes.Rows(0)("UserTypeDesc").ToString
            UserTypeCdTextBox.Text = dtTCLUserTypes.Rows(0)("UserTypecd").ToString

            pnlTCl.Visible = True
            TCLTextBox.Visible = True
            UserTypeDescTextBox.Visible = True
            UserTypeCdTextBox.Visible = True

            'TCLddl.Visible = False

            gvTCL.Visible = False

        ElseIf dtTCLUserTypes.Rows.Count = 0 Then
            pnlTCl.Visible = False

        Else
            pnlTCl.Visible = True
            TCLTextBox.Visible = True
            UserTypeDescTextBox.Visible = True
            UserTypeCdTextBox.Visible = True


            gvTCL.Visible = True

        End If

        clientForm.Update()

    End Sub
    Protected Sub Specialtyddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles Specialtyddl.SelectedIndexChanged
        SpecialtyLabel.Text = Specialtyddl.SelectedItem.ToString
        clientForm.Update()

    End Sub
    Protected Sub CheckECare()

        Dim index1 = BuildIndex(ViewState("dtAccounts"), 0)

        Dim found1 = ItemExists(index1, 44)

        Dim found2 = ItemExists(index1, 45)

        If found1 = True Or found2 = True Then
            If RoleNumLabel.Text <> "" Then


                Dim tcldata As New CommandsSqlAndOleDb("SelInvisionUserTypeV2", Session("EmployeeConn"))
                If PositionNumTextBox.Text <> "" Then
                    tcldata.AddSqlProcParameter("@rolenum", PositionNumTextBox.Text, SqlDbType.NVarChar, 8)
                Else
                    tcldata.AddSqlProcParameter("@rolenum", RoleNumLabel.Text, SqlDbType.NVarChar, 8)
                End If

                If TCLTextBox.Text <> "" Then
                    tcldata.AddSqlProcParameter("@Clientupd", "yes", SqlDbType.NVarChar, 8)

                End If


                If TitleLabel.Text <> "" Then
                    tcldata.AddSqlProcParameter("@UserTypeCd", TitleLabel.Text, SqlDbType.NVarChar, 25)

                End If


                tcldata.GetSqlDataset()




                dtTCLUserTypes = tcldata.GetSqlDataTable
                gvTCL.DataSource = dtTCLUserTypes
                gvTCL.DataBind()


                If dtTCLUserTypes.Rows.Count = 1 Then
                    'TCLddl.SelectedIndex = 0

                    TCLTextBox.Text = dtTCLUserTypes.Rows(0)("TCL").ToString
                    UserTypeDescTextBox.Text = dtTCLUserTypes.Rows(0)("UserTypeDesc").ToString
                    UserTypeCdTextBox.Text = dtTCLUserTypes.Rows(0)("UserTypecd").ToString

                    pnlTCl.Visible = True
                    TCLTextBox.Visible = True
                    UserTypeDescTextBox.Visible = True
                    UserTypeCdTextBox.Visible = True

                    'TCLddl.Visible = False

                    gvTCL.Visible = False

                ElseIf dtTCLUserTypes.Rows.Count = 0 Then
                    pnlTCl.Visible = False

                Else
                    pnlTCl.Visible = True
                    TCLTextBox.Visible = True
                    UserTypeDescTextBox.Visible = True
                    UserTypeCdTextBox.Visible = True

                    gvTCL.Visible = True
                    'gvTCL.BackColor() = Color.Yellow
                    'gvTCL.BorderColor() = Color.Black
                    'gvTCL.BorderStyle() = BorderStyle.Solid
                    'gvTCL.BorderWidth = Unit.Pixel(2)
                    'gvTCL.RowStyle.BackColor() = Color.Yellow

                End If
            Else

                pnlTCl.Visible = True

                TCLTextBox.Visible = True
                UserTypeDescTextBox.Visible = True
                UserTypeCdTextBox.Visible = True


            End If

            TCLTextBox.Enabled = False
            UserTypeDescTextBox.Enabled = False
            UserTypeCdTextBox.Enabled = False


        End If


    End Sub
    'Protected Sub Specialtyddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles Specialtyddl.SelectedIndexChanged
    '    SpecialtyLabel.Text = Specialtyddl.SelectedItem.ToString
    '    uplSignOnForm.Update()

    'End Sub
    Protected Sub Rolesddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles Rolesddl.SelectedIndexChanged
        'always use the Main Role


        mainRole = emptypecdrbl.SelectedValue

        If ecareexists.Text = "" Then
            pnlTCl.Visible = False
            dtTCLUserTypes.Dispose()
            gvTCL.Dispose()

        End If



        ' TCLTextBox.Text = ""
        UserTypeCdTextBox.Text = ""
        UserTypeDescTextBox.Text = ""
        RoleNumLabel.Text = ""
        gvTCL.SelectedIndex = -1



        If Rolesddl.SelectedValue.ToString <> "Select" Then
            userpositiondescTextBox.Text = Rolesddl.SelectedItem.ToString

            RoleNumLabel.Text = Rolesddl.SelectedValue

            'If PositionRoleNumLabel.Text = "1567" Then
            '    If RoleNumLabel.Text <> PositionRoleNumLabel.Text Then
            '        educationpan.Visible = False
            '    End If
            'End If

            PositionRoleNumLabel.Text = Rolesddl.SelectedValue
            PositionNumTextBox.Text = Rolesddl.SelectedValue
            'TitleLabel.Text = Rolesddl.SelectedItem.ToString

            If thisRole Is Nothing Then
                thisRole = Rolesddl.SelectedValue
            End If
        ElseIf Rolesddl.SelectedValue.ToString = "Select" And mainRole <> Nothing Then
            Rolesddl.SelectedValue = mainRole
            PositionNumTextBox.Text = mainRole
        End If

        Select Case RoleNumLabel.Text
            Case "1193", "782"

            Case "1554", "1560"

                Dim ddbVendorBinder As New DropDownListBinder(Vendorddl, "SelVendors", Session("EmployeeConn"))

                ddbVendorBinder.AddDDLCriteria("@type", "prospect", SqlDbType.NVarChar)
                ddbVendorBinder.BindData("VendorNum", "VendorName")

                'Case "1567"

                '    PhysOfficeddl.Dispose()

                '    Dim ddEMRBinder As New DropDownListBinder(PhysOfficeddl, "SelPhysGroupsV5", Session("EmployeeConn"))
                '    ddEMRBinder.AddDDLCriteria("@GroupTypeCd", "GNum", SqlDbType.NVarChar)
                '    ddEMRBinder.AddDDLCriteria("@clientpage", "yes", SqlDbType.NVarChar)
                '    'SelPhysGroupsV5
                '    ddEMRBinder.BindData("Group_num", "group_name")


                '    CPMEmployeesddl.Dispose()

                '    Dim ddcmpMOdelafter As New DropDownListBinder
                '    ddcmpMOdelafter.BindData(CPMEmployeesddl, "SelCPMClients", "client_num", "FullName", Session("EmployeeConn"))

                '    PhysOfficepanel.Visible = True
                '    cpmModelAfterPanel.Visible = True
                '    cpmModelEmplPanel.Visible = True

                '    PhysOfficeddl.BackColor() = Color.Yellow
                '    PhysOfficeddl.BorderColor() = Color.Black
                '    PhysOfficeddl.BorderStyle() = BorderStyle.Solid
                '    PhysOfficeddl.BorderWidth = Unit.Pixel(2)
                '    'EMROfficeddl

                '    CPMEmployeesddl.BackColor() = Color.Yellow
                '    CPMEmployeesddl.BorderColor() = Color.Black
                '    CPMEmployeesddl.BorderStyle() = BorderStyle.Solid
                '    CPMEmployeesddl.BorderWidth = Unit.Pixel(2)

            Case Else

                PhysOfficepanel.Visible = False

                'cpmModelAfterPanel.Visible = False
                'cpmModelEmplPanel.Visible = False

                Dim tcldata As New CommandsSqlAndOleDb("SelInvisionUserTypeV2", Session("EmployeeConn"))
                If PositionNumTextBox.Text <> "" Then
                    tcldata.AddSqlProcParameter("@rolenum", PositionNumTextBox.Text, SqlDbType.NVarChar, 8)
                Else
                    tcldata.AddSqlProcParameter("@rolenum", RoleNumLabel.Text, SqlDbType.NVarChar, 8)
                End If

                If TCLTextBox.Text <> "" Then
                    tcldata.AddSqlProcParameter("@Clientupd", "yes", SqlDbType.NVarChar, 8)

                End If


                tcldata.GetSqlDataset()




                dtTCLUserTypes = tcldata.GetSqlDataTable


                If dtTCLUserTypes.Rows.Count = 1 Then
                    'TCLddl.SelectedIndex = 0

                    TCLTextBox.Text = dtTCLUserTypes.Rows(0)("TCL").ToString

                    UserTypeDescTextBox.Text = dtTCLUserTypes.Rows(0)("UserTypeDesc").ToString
                    UserTypeCdTextBox.Text = dtTCLUserTypes.Rows(0)("UserTypecd").ToString

                    pnlTCl.Visible = True

                    TCLTextBox.Visible = True
                    UserTypeDescTextBox.Visible = True
                    UserTypeCdTextBox.Visible = True

                    gvTCL.Visible = False



                ElseIf dtTCLUserTypes.Rows.Count = 0 Then
                    pnlTCl.Visible = False

                ElseIf dtTCLUserTypes.Rows.Count > 1 Then

                    gvTCL.DataSource = dtTCLUserTypes
                    gvTCL.DataBind()

                    gvTCL.Visible = True
                    'btnChangeTCL.Visible = True
                    'gvTCL.BackColor() = Color.Yellow
                    'gvTCL.BorderColor() = Color.Black
                    'gvTCL.BorderStyle() = BorderStyle.Solid
                    'gvTCL.BorderWidth = Unit.Pixel(2)
                    'gvTCL.RowStyle.BackColor() = Color.Yellow


                    pnlTCl.Visible = True

                    TCLTextBox.Visible = True
                    UserTypeDescTextBox.Visible = True
                    UserTypeCdTextBox.Visible = True

                End If


        End Select

        Select Case mainRole
            'Case "1194"
            '    Titleddl.SelectedValue = Title2TextBox.Text
            '    'Title2TextBox.Text
            '    TitleLabel.Text = Titleddl.SelectedItem.ToString

            '    'Title2TextBox.Text = Titleddl.SelectedItem.ToString

            '    Title2TextBox.Visible = True
            '    Titleddl.Visible = False

            Case "1190", "1191", "1192"
                'CheckEntitiesHosp()

                'SuppSupportrbl.SelectedValue = "Yes"

                entitycdDDL.SelectedValue = "150"

                Dim ddbDepartmentBinder As New DropDownListBinder(departmentcdddl, "selDepartmentCodesV4", Session("EmployeeConn"))
                ddbDepartmentBinder.AddDDLCriteria("@entity_cd", "150", SqlDbType.NVarChar)
                ddbDepartmentBinder.BindData("department_cd", "department_name")

                departmentcdddl.SelectedValue = "601000"

            Case "1554", "1560"

                Dim ddbVendorBinder As New DropDownListBinder(Vendorddl, "SelVendors", Session("EmployeeConn"))

                ddbVendorBinder.AddDDLCriteria("@type", "prospect", SqlDbType.NVarChar)
                ddbVendorBinder.BindData("VendorNum", "VendorName")

            Case Else
                entitycdDDL.SelectedIndex = -1
                departmentcdddl.SelectedIndex = -1
                'SuppSupportrbl.SelectedIndex = -1

        End Select


    End Sub


    'Protected Sub btnAddDuplicateDcoMaster_Click(sender As Object, e As System.EventArgs) Handles btnAddDuplicateDcoMaster.Click
    '    If doctormasternumTextBox.Text = DupDoctorMstrtxt.Text Then
    '        Exit Sub
    '    End If

    '    If DupDoctorMstrtxt.Text = "" Then
    '        Exit Sub
    '    End If

    '    Dim thisdata As New CommandsSqlAndOleDb("InsDuplicatePhysicianMasterV2", Session("EmployeeConn"))
    '    thisdata.AddSqlProcParameter("@origionalDocmaster", doctormasternumTextBox.Text, SqlDbType.NVarChar, 25)
    '    thisdata.AddSqlProcParameter("@DupDocMaster", DupDoctorMstrtxt.Text, SqlDbType.NVarChar, 25)

    '    thisdata.ExecNonQueryNoReturn()


    '    DupDoctorMstrtxt.Text = ""



    'End Sub


    Private Sub insAcctItems(ByVal requestNum As Integer, ByVal appNum As Integer, ByVal LoginName As String, ByVal requestType As String, ByVal sMore As String)

        Dim cmdSql As New SqlCommand
        Dim connSql As New SqlConnection(Session("EmployeeConn"))
        Dim drSql As SqlClient.SqlDataReader

        '   connSql.ConnectionString = _DataConn

        cmdSql.CommandText = "InsAccountItemsV3"
        cmdSql.CommandType = CommandType.StoredProcedure
        cmdSql.Connection = connSql


        cmdSql.Parameters.Add("@account_request_num", SqlDbType.Int, 20).Value = requestNum
        cmdSql.Parameters.Add("@ApplicationNum", SqlDbType.Int, 8).Value = appNum
        cmdSql.Parameters.Add("@More", SqlDbType.NVarChar, 8).Value = sMore
        ' used to identify base for sub apps


        Dim sComment As String

        sComment = txtComment.Text


        If requestType = "addacct" Then
            If appNum = 156 Then
                cmdSql.Parameters.Add("@account_item_desc", SqlDbType.NVarChar, 255).Value = txtComment.Text
                cmdSql.Parameters.Add("@GroupDesc", SqlDbType.NVarChar, 75).Value = CellProvidertxt.Text


            Else

                If sComment = "" Then


                    cmdSql.Parameters.Add("@account_item_desc", SqlDbType.NVarChar, 255).Value = "Requesting New Account"
                Else
                    cmdSql.Parameters.Add("@account_item_desc", SqlDbType.NVarChar, 255).Value = sComment

                End If
            End If

        ElseIf requestType = "delacct" Then
            If sComment = "" Then
                cmdSql.Parameters.Add("@account_item_desc", SqlDbType.NVarChar, 255).Value = "Remove Current Account"
            Else
                cmdSql.Parameters.Add("@account_item_desc", SqlDbType.NVarChar, 255).Value = sComment

            End If

        End If


        Select Case ValidationStatus.Text.ToLower.ToString
            Case "pastvalidation", "validated"
                cmdSql.Parameters.Add("@validate", SqlDbType.NVarChar, 20).Value = "validated"
                ValidationStatus.Text = "validated"

            Case Else
                Select Case emptypecdrbl.SelectedValue
                    'Case "physician/rovider", "non staff clinician", "resident"  "allied health"

                    Case "782", "1197", "1194"
                        If DoctorMasterNumTextBox.Text <> "" Then
                            cmdSql.Parameters.Add("@validate", SqlDbType.NVarChar, 20).Value = "NeedsValidation"

                        Else
                            cmdSql.Parameters.Add("@validate", SqlDbType.NVarChar, 20).Value = "NeedsDocNum"
                        End If

                    Case Else

                        cmdSql.Parameters.Add("@validate", SqlDbType.NVarChar, 20).Value = "NeedsValidation"

                End Select

        End Select

        'If ValidationStatus.Text.ToLower.ToString = "pastvalidation" Then
        '    cmdSql.Parameters.Add("@validate", SqlDbType.NVarChar, 20).Value = "validated"
        'ElseIf ValidationStatus.Text.ToLower.ToString Is Nothing Or ValidationStatus.Text.ToLower.ToString = "" Then
        '    cmdSql.Parameters.Add("@validate", SqlDbType.NVarChar, 20).Value = "NeedsValidation"

        'Else
        '    Select Case emptypecdrbl.SelectedValue
        '        'Case "physician/rovider", "non staff clinician", "resident"  "allied health"

        '        Case "782", "1197", "1194"
        '            If DoctorMasterNumTextBox.Text <> "" Then
        '                cmdSql.Parameters.Add("@validate", SqlDbType.NVarChar, 20).Value = "NeedsValidation"

        '            Else
        '                cmdSql.Parameters.Add("@validate", SqlDbType.NVarChar, 20).Value = "NeedsDocNum"
        '            End If

        '    End Select

        'End If

        cmdSql.Parameters.Add("@login_name", SqlDbType.NVarChar, 50).Value = LoginName
        cmdSql.Parameters.Add("@who_nt_login", SqlDbType.NVarChar, 50).Value = Session("LoginID")


        connSql.Open()

        Try


            drSql = cmdSql.ExecuteReader


            While drSql.Read()


                sItemComplet = IIf(IsDBNull(drSql("msg")), "", drSql("msg"))

            End While

        Catch ex As Exception
            Dim er As String = ex.ToString()

        End Try

        connSql.Close()

        'Dim thisdata As New CommandsSqlAndOleDb("InsAccountItemsV3", Session("EmployeeConn"))

        'thisdata.AddSqlProcParameter("@account_request_num", requestNum, SqlDbType.Int, 20)
        'thisdata.AddSqlProcParameter("@ApplicationNum", appNum, SqlDbType.Int, 8)

        'If requestType = "addacct" Then
        '    thisdata.AddSqlProcParameter("@account_item_desc", "Requesting New Account", SqlDbType.NVarChar, 255)
        'ElseIf requestType = "delacct" Then
        '    thisdata.AddSqlProcParameter("@account_item_desc", "Remove Current Account", SqlDbType.NVarChar, 255)

        'End If


        'If ValidationStatus.ToLower.ToString = "pastvalidation" Then
        '    thisdata.AddSqlProcParameter("@validate", "validated", SqlDbType.NVarChar, 20)
        'ElseIf ValidationStatus.ToLower.ToString Is Nothing Or ValidationStatus.ToLower.ToString = "" Then
        '    thisdata.AddSqlProcParameter("@validate", "NeedsValidation", SqlDbType.NVarChar, 20)
        'Else
        '    thisdata.AddSqlProcParameter("@validate", "NeedsValidation", SqlDbType.NVarChar, 20)
        'End If


        'thisdata.AddSqlProcParameter("@login_name", LoginName, SqlDbType.NVarChar, 50)
        'thisdata.AddSqlProcParameter("@who_nt_login", Session("LoginID"), SqlDbType.NVarChar, 50)

        'thisdata.ExecNonQueryNoReturn()


    End Sub
    Private Sub btnUpdComment_Click(sender As Object, e As EventArgs) Handles btnUpdComment.Click
        requestCommentsPan.Visible = False

    End Sub

    Private Sub btnCancelComment_Click(sender As Object, e As EventArgs) Handles btnCancelComment.Click
        txtComment.Text = Nothing
        requestCommentsPan.Visible = False
    End Sub

    Protected Sub btnInvalidAccts_Click(sender As Object, e As System.EventArgs) Handles btnInvalidAccts.Click


        If gvRemoveAccounts.Rows.Count > 0 Then

            NewRequestForm.AddInsertParm("@receiver_client_num", iClientNum, SqlDbType.Int, 10)

            'NewRequestForm.AddInsertParm("@submitter_client_num", UserInfo.ClientNum, SqlDbType.Int, 10)
            NewRequestForm.AddInsertParm("@account_request_type", "delacct", SqlDbType.NVarChar, 9)
            NewRequestForm.UpdateFormReturnReqNum()
            AccountRequestNum = NewRequestForm.AccountRequestNum

            AccountRequest = New AccountRequest(AccountRequestNum, Session("EmployeeConn"))

            ValidationStatus.Text = AccountRequest.ValidationStatus

            Try
                ' each row goes here

                For Each rw As GridViewRow In gvRemoveAccounts.Rows
                    If rw.RowType = DataControlRowType.DataRow Then

                        Dim appNum As Integer = gvRemoveAccounts.DataKeys(rw.RowIndex)("ApplicationNum")
                        Dim loginName As String = gvRemoveAccounts.DataKeys(rw.RowIndex)("login_name")
                        Dim Subapp As Integer = gvRemoveAccounts.DataKeys(rw.RowIndex)("Subappnum")

                        Select Case appNum
                            Case 13, 97, 98, 99, 100, 136

                                ' insert for sub apps & Nursestation if selected
                                If Subapp <> "0" Then
                                    insAcctItems(AccountRequestNum, appNum, loginName, "delacct", "yes")

                                    DeleteNursestsion(AccountRequestNum, appNum, txtComment.Text, Subapp)
                                Else
                                    'remove Delete all sub apps and main
                                    insAcctItems(AccountRequestNum, appNum, loginName, "delacct", "no")

                                End If
                            Case Else
                                insAcctItems(AccountRequestNum, appNum, loginName, "delacct", "")



                        End Select

                    End If

                Next

            Catch ex As Exception

            End Try

            ' Check to see if request has Valid items if no items found then request is removed
            Dim thisdata As New CommandsSqlAndOleDb("DelEmptyRequest", Session("EmployeeConn"))
            thisdata.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int, 20)
            thisdata.AddSqlProcParameter("@who_nt_login", Session("LoginID"), SqlDbType.NVarChar, 50)
            thisdata.ExecNonQueryNoReturn()

            If sItemComplet.Length > 2 Then
                Response.Redirect("~/ClientDemo.aspx?ClientNum=" & iClientNum & "&msg=" & sItemComplet, True)
            Else

                Response.Redirect("~/AccountRequestInfo2.aspx?RequestNum=" & AccountRequestNum, True)
            End If


        Else
            btnSubmitAddRequest.Visible = False
            btnAddComments.Visible = False

        End If
    End Sub
    Protected Sub Vendorddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles Vendorddl.SelectedIndexChanged
        Dim vendornum As String = Vendorddl.SelectedValue
        Dim vendorname As String = Vendorddl.SelectedItem.ToString

		If HDVendorNum.Text <> vendornum Then


			VendorFormSignOn = New FormSQL(Session("EmployeeConn"), "SelVendors", "InsUpdVendors", "", Page)
			VendorFormSignOn.AddSelectParm("@VendorNum", vendornum, SqlDbType.Int, 6)

			If Vendorddl.SelectedValue = 0 Then
				VendorNameTextBox.Enabled = True
				VendorNameTextBox.Text = "Other"
				VendorNameTextBox.Visible = True
				Vendorddl.Visible = False
			Else
				HDVendorNum.Text = vendornum
				VendorNumLabel.Text = vendornum

				If address1textbox.Text = "" And citytextbox.Text = "" And phoneTextBox.Text = "" Then

					VendorFormSignOn.FillForm()

				End If

				VendorNameTextBox.Text = vendorname

			End If


		End If

		address1textbox.Text = address1textbox.Text



        'SelVendors @vendorNum = 1
    End Sub
    Protected Sub CCMCadmitRightsrbl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles CCMCadmitRightsrbl.SelectedIndexChanged

        If CCMCadmitRightsrbl.SelectedValue.ToLower = "no" Then

            'CCMCConsultsrbl.SelectedValue = "No"
            'CCMCConsultsrbl.Enabled = False

            'DCMHConsultsrbl.SelectedValue = "No"
            'DCMHConsultsrbl.Enabled = False

            'TaylorConsultsrbl.SelectedValue = "No"
            'TaylorConsultsrbl.Enabled = False

            'SpringfieldConsultsrbl.SelectedValue = "No"
            'SpringfieldConsultsrbl.Enabled = False

            'Writeordersrbl.SelectedValue = "No"
            'Writeordersrbl.Enabled = False

            'WriteordersDCMHrbl.SelectedValue = "No"
            'WriteordersDCMHrbl.Enabled = False

            credentialedDateTextBox.Enabled = False
            'credentialedDateTextBox.CssClass = "style2"

            'credentialedDateDCMHTextBox.Enabled = False
            'credentialedDateDCMHTextBox.CssClass = "style2"


        End If


        If CCMCadmitRightsrbl.SelectedValue.ToLower = "no" Then
            credentialedDateTextBox.Enabled = False
            credentialedDateTextBox.CssClass = "style2"

        End If

        'If CCMCadmitRightsrbl.SelectedValue.ToLower = "yes" Then
        '    If Session("SecurityLevelNum") = 82 Or Session("LoginID") = "melej" Or Session("LoginID") = "geip00" Or Session("LoginID") = "RalphC" Then
        '        credentialedDateTextBox.Enabled = True
        '        credentialedDateTextBox.CssClass = "calenderClass"

        '    End If


        'End If


        'If CCMCadmitRightsrbl.SelectedValue.ToLower = "yes" Or DCMHadmitRightsrbl.SelectedValue.ToLower = "yes" Then

        '    CCMCConsultsrbl.SelectedIndex = -1
        '    CCMCConsultsrbl.Enabled = True

        '    DCMHConsultsrbl.SelectedIndex = -1
        '    DCMHConsultsrbl.Enabled = True

        '    TaylorConsultsrbl.SelectedIndex = -1
        '    TaylorConsultsrbl.Enabled = True

        '    SpringfieldConsultsrbl.SelectedIndex = -1
        '    SpringfieldConsultsrbl.Enabled = True

        '    Writeordersrbl.SelectedIndex = -1
        '    Writeordersrbl.Enabled = True

        '    WriteordersDCMHrbl.SelectedIndex = -1
        '    WriteordersDCMHrbl.Enabled = True

        '    If Session("SecurityLevelNum") = 82 Or Session("LoginID") = "melej" Then
        '        credentialedDateTextBox.Enabled = True
        '        credentialedDateTextBox.CssClass = "calenderClass"

        '        CredentialedDateDCMHTextBox.Enabled = True
        '        CredentialedDateDCMHTextBox.CssClass = "calenderClass"

        '    End If
        'End If
        If thisRole Is Nothing Then
            thisRole = PositionRoleNumLabel.Text
        End If

        If thisRole = "1194" Then
            credentialedDateTextBox.Enabled = True
            credentialedDateTextBox.CssClass = "calenderClass"

            'CredentialedDateDCMHTextBox.Enabled = True
            'CredentialedDateDCMHTextBox.CssClass = "calenderClass"

        End If

    End Sub

    'Protected Sub DCMHadmitRightsrbl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles DCMHadmitRightsrbl.SelectedIndexChanged
    '    If CCMCadmitRightsrbl.SelectedValue.ToLower = "no" Then

    '        'CCMCConsultsrbl.SelectedValue = "No"
    '        'CCMCConsultsrbl.Enabled = False

    '        'DCMHConsultsrbl.SelectedValue = "No"
    '        'DCMHConsultsrbl.Enabled = False

    '        'TaylorConsultsrbl.SelectedValue = "No"
    '        'TaylorConsultsrbl.Enabled = False

    '        'SpringfieldConsultsrbl.SelectedValue = "No"
    '        'SpringfieldConsultsrbl.Enabled = False

    '        'Writeordersrbl.SelectedValue = "No"
    '        'Writeordersrbl.Enabled = False

    '        'WriteordersDCMHrbl.SelectedValue = "No"
    '        'WriteordersDCMHrbl.Enabled = False

    '        credentialedDateTextBox.Enabled = False
    '        credentialedDateTextBox.CssClass = "style2"

    '        'CredentialedDateDCMHTextBox.Enabled = False
    '        'credentialedDateDCMHTextBox.CssClass = "style2"

    '    End If

    '    'If DCMHadmitRightsrbl.SelectedValue.ToLower = "no" Then
    '    '    CredentialedDateDCMHTextBox.Enabled = False
    '    '    CredentialedDateDCMHTextBox.CssClass = "style2"

    '    'End If

    '    'If DCMHadmitRightsrbl.SelectedValue.ToLower = "yes" Then
    '    '    If Session("SecurityLevelNum") = 82 Or Session("LoginID") = "melej" Or Session("LoginID") = "geip00" Or Session("LoginID") = "RalphC" Then

    '    '        CredentialedDateDCMHTextBox.Enabled = True
    '    '        CredentialedDateDCMHTextBox.CssClass = "calenderClass"

    '    '    End If


    '    'End If




    '    'If CCMCadmitRightsrbl.SelectedValue.ToLower = "yes" Or DCMHadmitRightsrbl.SelectedValue.ToLower = "yes" Then

    '    '    CCMCConsultsrbl.SelectedIndex = -1
    '    '    CCMCConsultsrbl.Enabled = True

    '    '    DCMHConsultsrbl.SelectedIndex = -1
    '    '    DCMHConsultsrbl.Enabled = True

    '    '    TaylorConsultsrbl.SelectedIndex = -1
    '    '    TaylorConsultsrbl.Enabled = True

    '    '    SpringfieldConsultsrbl.SelectedIndex = -1
    '    '    SpringfieldConsultsrbl.Enabled = True

    '    '    Writeordersrbl.SelectedIndex = -1
    '    '    Writeordersrbl.Enabled = True

    '    '    WriteordersDCMHrbl.SelectedIndex = -1
    '    '    WriteordersDCMHrbl.Enabled = True

    '    '    If Session("SecurityLevelNum") = 82 Or Session("LoginID") = "melej" Then
    '    '        credentialedDateTextBox.Enabled = True
    '    '        credentialedDateTextBox.CssClass = "calenderClass"

    '    '        CredentialedDateDCMHTextBox.Enabled = True
    '    '        CredentialedDateDCMHTextBox.CssClass = "calenderClass"

    '    '    End If

    '    ' End If
    '    If thisRole Is Nothing Then
    '        thisRole = PositionRoleNumLabel.Text
    '    End If


    '    If thisRole = "1194" Then
    '        credentialedDateTextBox.Enabled = False
    '        credentialedDateTextBox.CssClass = "style2"

    '        'CredentialedDateDCMHTextBox.Enabled = False
    '        'CredentialedDateDCMHTextBox.CssClass = "style2"
    '    End If

    'End Sub

    Protected Sub gvCurrentApps_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvCurrentApps.RowCommand

        If e.CommandName = "Select" Then


            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim ApplicationNum As Integer = gvCurrentApps.DataKeys(e.CommandArgument)("ApplicationNum")
            Dim ApplicationDesc As String = gvCurrentApps.DataKeys(Convert.ToInt32(e.CommandArgument))("ApplicationDesc").ToString()
            Dim LoginName As String = gvCurrentApps.DataKeys(Convert.ToInt32(e.CommandArgument))("login_name").ToString()
            Dim Subappnum As String = gvCurrentApps.DataKeys(Convert.ToInt32(e.CommandArgument))("SubApplicationNum").ToString()



            dtRemoveAccounts = DirectCast(ViewState("dtRemoveAccounts"), DataTable)
            dtRemoveAccounts.Rows.Add(ApplicationNum, ApplicationDesc, LoginName, Subappnum)

            gvRemoveAccounts.DataSource = dtRemoveAccounts
            gvRemoveAccounts.DataBind()

            Dim dtAccounts As New DataTable
            dtAccounts = DirectCast(ViewState("dtAccounts"), DataTable)
            dtAccounts.Rows(index).Delete()
            dtAccounts.AcceptChanges()


            ViewState("dtAccounts") = dtAccounts

            gvCurrentApps.DataSource = dtAccounts
            gvCurrentApps.DataBind()

            If btnInvalidAccts.Visible = False Then
                btnInvalidAccts.Visible = True
                btnAddDeleteComments.Visible = True
            End If

            gvCurrentApps.SelectedRowStyle.BackColor = Color.White
            gvCurrentApps.SelectedRowStyle.Font.Bold = False
            gvCurrentApps.SelectedRowStyle.ForeColor = Color.Black
            'uplAccounts.Update()
            clientForm.Update()

        End If
    End Sub
    Protected Sub gvRemoveAccounts_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvRemoveAccounts.RowCommand

        If e.CommandName = "Select" Then


            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim ApplicationNum As String = gvRemoveAccounts.DataKeys(Convert.ToInt32(e.CommandArgument))("ApplicationNum").ToString()
            Dim ApplicationDesc As String = gvRemoveAccounts.DataKeys(Convert.ToInt32(e.CommandArgument))("ApplicationDesc").ToString()
            Dim LoginName As String = gvRemoveAccounts.DataKeys(Convert.ToInt32(e.CommandArgument))("login_name").ToString()
            Dim Subappnum As String = gvCurrentApps.DataKeys(Convert.ToInt32(e.CommandArgument))("SubApplicationNum").ToString()


            Dim dtAccounts As New DataTable
            dtAccounts = DirectCast(ViewState("dtAccounts"), DataTable)
            dtAccounts.Rows.Add(ApplicationNum, ApplicationDesc, LoginName)

            ViewState("dtAccounts") = dtAccounts

            gvCurrentApps.DataSource = dtAccounts
            gvCurrentApps.DataBind()
            gvCurrentApps.SelectedIndex = -1



            dtRemoveAccounts = DirectCast(ViewState("dtRemoveAccounts"), DataTable)


            If dtRemoveAccounts.Rows.Count > 0 Then

                dtRemoveAccounts.Rows(index).Delete()
                dtRemoveAccounts.AcceptChanges()
            End If

            ViewState("dtRemoveAccounts") = dtRemoveAccounts

            gvRemoveAccounts.DataSource = dtRemoveAccounts
            gvRemoveAccounts.DataBind()
            gvRemoveAccounts.SelectedIndex = -1

            gvRemoveAccounts.SelectedRowStyle.BackColor = Color.White
            gvRemoveAccounts.SelectedRowStyle.Font.Bold = False
            gvRemoveAccounts.SelectedRowStyle.ForeColor = Color.Black


            Select Case ApplicationNum
                Case 97
                    lblAppSystem.Text = ""


                    lblAppSystem.Text = "15"
                    'For Each cblcItem As ListItem In cblCCMCNursestations.Items
                    '    cblcItem.Selected = False
                    '    'cblItem.Enabled = False
                    'Next

                    'CCMCNurseStationspan.Visible = False

                Case 98
                    lblAppSystem.Text = ""
                    'FacilityCd 12
                    lblAppSystem.Text = "12"

                    'For Each cblDItem As ListItem In cblDCMHNursestataions.Items
                    '    cblDItem.Selected = False
                    '    'cblItem.Enabled = False
                    'Next

                    'dcmhnursepan.Visible = False

                Case 99

                    lblAppSystem.Text = ""
                    'FacilityCd 19
                    lblAppSystem.Text = "19"

                    'For Each cblTItem As ListItem In cblTaylorNursestataions.Items
                    '    cblTItem.Selected = False
                    '    'cblItem.Enabled = False
                    'Next


                    'taylornursepan.Visible = False

                Case 100

                    lblAppSystem.Text = ""
                    'FacilityCd 16
                    lblAppSystem.Text = "16"

                    'For Each cblSItem As ListItem In cblSpringfieldNursestataions.Items
                    '    cblSItem.Selected = False
                    '    'cblItem.Enabled = False
                    'Next


                    'SpringNurseStationsPan.Visible = False

            End Select


            'uplAccounts.Update()
            clientForm.Update()

        End If
    End Sub
    Protected Sub grAccountsToAdd_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grAccountsToAdd.RowCommand

        If e.CommandName = "Select" Then


            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim ApplicationNum As String = grAccountsToAdd.DataKeys(Convert.ToInt32(e.CommandArgument))("Applicationnum").ToString()
            Dim ApplicationDesc As String = grAccountsToAdd.DataKeys(Convert.ToInt32(e.CommandArgument))("ApplicationDesc").ToString()

            Select Case ApplicationNum
                Case 156

                    TokenPhoneTextBox.Text = ""
                    CellProvidertxt.Text = ""

                    CellPhoneddl.BackColor() = Color.Yellow
                    CellPhoneddl.BorderColor() = Color.Black
                    CellPhoneddl.BorderStyle() = BorderStyle.Solid
                    CellPhoneddl.BorderWidth = Unit.Pixel(2)


                    TokenPhoneTextBox.BackColor() = Color.Yellow
                    TokenPhoneTextBox.BorderColor() = Color.Black
                    TokenPhoneTextBox.BorderStyle() = BorderStyle.Solid
                    TokenPhoneTextBox.BorderWidth = Unit.Pixel(2)

                    PanelCellphone.Visible = False

                Case 97
                    lblAppSystem.Text = ""
                    'lblAppSystem.Text = "15"

                    For Each cblcItem As ListItem In cblCCMCNursestations.Items
                        cblcItem.Selected = False
                    Next

                    CCMCNurseStationspan.Visible = False

                Case 98
                    lblAppSystem.Text = ""
                    'FacilityCd 12
                    'lblAppSystem.Text = "12"

                    For Each cblDItem As ListItem In cblDCMHNursestataions.Items
                        cblDItem.Selected = False
                    Next

                    dcmhnursepan.Visible = False

                Case 99

                    lblAppSystem.Text = ""
                    'FacilityCd 19
                    'lblAppSystem.Text = "19"

                    For Each cblTItem As ListItem In cblTaylorNursestataions.Items
                        cblTItem.Selected = False
                    Next


                    taylornursepan.Visible = False

                Case 100

                    lblAppSystem.Text = ""
                    'FacilityCd 16
                    'lblAppSystem.Text = "16"

                    For Each cblSItem As ListItem In cblSpringfieldNursestataions.Items
                        cblSItem.Selected = False
                    Next


                    SpringNurseStationsPan.Visible = False
                Case 136
                    ' 365 portal

                    O365Panel.Visible = False

                    For Each cbl365Item As ListItem In cblo365.Items
                        cbl365Item.Selected = False
                    Next

            End Select



            Dim dtAvailableAccounts As New DataTable
            dtAvailableAccounts = DirectCast(ViewState("dtAvailableAccounts"), DataTable)
            dtAvailableAccounts.Rows.Add(ApplicationNum, ApplicationDesc)

            ViewState("dtAvailableAccounts") = dtAvailableAccounts

            grAvailableAccounts.DataSource = dtAvailableAccounts
            grAvailableAccounts.DataBind()


            dtAddAccounts = DirectCast(ViewState("dtAddAccounts"), DataTable)
            dtAddAccounts.Rows(index).Delete()
            dtAddAccounts.AcceptChanges()


            ViewState("dtAddAccounts") = dtAddAccounts

            grAccountsToAdd.DataSource = dtAddAccounts
            grAccountsToAdd.DataBind()

            grAccountsToAdd.SelectedRowStyle.BackColor = Color.White
            grAccountsToAdd.SelectedRowStyle.Font.Bold = False
            grAccountsToAdd.SelectedRowStyle.ForeColor = Color.Black


        End If
    End Sub
    Protected Sub GrFinAddAccount_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GrFinAddAccount.RowCommand
        If e.CommandName = "Select" Then


            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim ApplicationNum As String = GrFinAddAccount.DataKeys(Convert.ToInt32(e.CommandArgument))("Applicationnum").ToString()
            Dim ApplicationDesc As String = GrFinAddAccount.DataKeys(Convert.ToInt32(e.CommandArgument))("ApplicationDesc").ToString()

            Dim dtAvailFincnialAccounts As New DataTable
            dtAvailFincnialAccounts = DirectCast(ViewState("dtAvailFincnialAccounts"), DataTable)
            dtAvailFincnialAccounts.Rows.Add(ApplicationNum, ApplicationDesc)

            ViewState("dtAvailFincnialAccounts") = dtAvailFincnialAccounts

            GrFinAvailable.DataSource = dtAvailFincnialAccounts
            GrFinAvailable.DataBind()


            dtAddFinAccounts = DirectCast(ViewState("dtAddFinAccounts"), DataTable)
            dtAddFinAccounts.Rows(index).Delete()
            dtAddFinAccounts.AcceptChanges()


            ViewState("dtAddFinAccounts") = dtAddFinAccounts

            GrFinAddAccount.DataSource = dtAddFinAccounts
            GrFinAddAccount.DataBind()

            GrFinAddAccount.SelectedRowStyle.BackColor = Color.White
            GrFinAddAccount.SelectedRowStyle.Font.Bold = False
            GrFinAddAccount.SelectedRowStyle.ForeColor = Color.Black


        End If

    End Sub

    Protected Sub grAvailableAccounts_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grAvailableAccounts.RowCommand

        If e.CommandName = "Select" Then


            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim ApplicationNum As String = grAvailableAccounts.DataKeys(Convert.ToInt32(e.CommandArgument))("Applicationnum").ToString()
            Dim ApplicationDesc As String = grAvailableAccounts.DataKeys(Convert.ToInt32(e.CommandArgument))("ApplicationDesc").ToString()



            dtAddAccounts = DirectCast(ViewState("dtAddAccounts"), DataTable)
            dtAddAccounts.Rows.Add(ApplicationNum, ApplicationDesc)

            grAccountsToAdd.DataSource = dtAddAccounts
            grAccountsToAdd.DataBind()

            Dim dtAvailableAccounts As New DataTable
            dtAvailableAccounts = DirectCast(ViewState("dtAvailableAccounts"), DataTable)
            dtAvailableAccounts.Rows(index).Delete()
            dtAvailableAccounts.AcceptChanges()

            grAvailableAccounts.DataSource = dtAvailableAccounts
            grAvailableAccounts.DataBind()

            ViewState("dtAvailableAccounts") = dtAvailableAccounts
            ViewState("dtAddAccounts") = dtAddAccounts

            grAccountsToAdd.DataSource = dtAddAccounts
            grAccountsToAdd.DataBind()

            If btnSubmitAddRequest.Visible = False Then
                btnSubmitAddRequest.Visible = True
                btnAddComments.Visible = True

            End If


            grAvailableAccounts.SelectedRowStyle.BackColor = Color.White
            grAvailableAccounts.SelectedRowStyle.Font.Bold = False
            grAvailableAccounts.SelectedRowStyle.ForeColor = Color.Black
            Select Case ApplicationNum
                Case 136
                    '365 portl
                    LoadclientNursestations(ApplicationNum)

                    Checkfor365()
                    O365Panel.Visible = True




				Case 156

                    Dim ddlCPBinder As New DropDownListBinder
                    ddlCPBinder.BindData(CellPhoneddl, "SelCellPhoneProviders", "ProviderExtension", "ProviderName", Session("EmployeeConn"))

                    PanelCellphone.Visible = True

					'CellPhonePopup.Show()
				Case 65

					lblAppSystem.Text = "15"
					LoadclientNursestations(ApplicationNum)

					lblAppSystem.Text = "12"
					LoadclientNursestations(ApplicationNum)

					lblAppSystem.Text = "19"
					LoadclientNursestations(ApplicationNum)

					lblAppSystem.Text = "16"
					LoadclientNursestations(ApplicationNum)

					CCMCNurseStationspan.Visible = True
					dcmhnursepan.Visible = True
					taylornursepan.Visible = True
					SpringNurseStationsPan.Visible = True

				Case 97
					lblAppSystem.Text = "15"

                    SpringNurseStationsPan.Visible = False
                    taylornursepan.Visible = False
                    dcmhnursepan.Visible = False
                    CCMCNurseStationspan.Visible = False

                    LoadclientNursestations(ApplicationNum)
                    CCMCNurseStationspan.Visible = True

				Case 98
					lblAppSystem.Text = "12"

                    SpringNurseStationsPan.Visible = False
                    taylornursepan.Visible = False
                    dcmhnursepan.Visible = False
                    CCMCNurseStationspan.Visible = False

                    LoadclientNursestations(ApplicationNum)
                    dcmhnursepan.Visible = True

				Case 99

					lblAppSystem.Text = "19"

                    SpringNurseStationsPan.Visible = False
                    taylornursepan.Visible = False
                    dcmhnursepan.Visible = False
                    CCMCNurseStationspan.Visible = False

                    LoadclientNursestations(ApplicationNum)
                    taylornursepan.Visible = True

				Case 100

					lblAppSystem.Text = "16"

                    SpringNurseStationsPan.Visible = False
                    taylornursepan.Visible = False
                    dcmhnursepan.Visible = False
                    CCMCNurseStationspan.Visible = False

                    LoadclientNursestations(ApplicationNum)
                    SpringNurseStationsPan.Visible = True

            End Select

            'uplAccounts.Update()
            clientForm.Update()
            'uplAccounts.Update()

        End If
    End Sub

    Protected Sub GrFinAvailable_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GrFinAvailable.RowCommand
        If e.CommandName = "Select" Then


            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim ApplicationNum As String = GrFinAvailable.DataKeys(Convert.ToInt32(e.CommandArgument))("Applicationnum").ToString()
            Dim ApplicationDesc As String = GrFinAvailable.DataKeys(Convert.ToInt32(e.CommandArgument))("ApplicationDesc").ToString()


            dtAddFinAccounts = DirectCast(ViewState("dtAddFinAccounts"), DataTable)
            dtAddFinAccounts.Rows.Add(ApplicationNum, ApplicationDesc)

            GrFinAddAccount.DataSource = dtAddFinAccounts
            GrFinAddAccount.DataBind()

            Dim dtAvailFincnialAccounts As New DataTable
            dtAvailFincnialAccounts = DirectCast(ViewState("dtAvailFincnialAccounts"), DataTable)
            dtAvailFincnialAccounts.Rows(index).Delete()
            dtAvailFincnialAccounts.AcceptChanges()

            GrFinAvailable.DataSource = dtAvailFincnialAccounts
            GrFinAvailable.DataBind()

            ViewState("dtAvailFincnialAccounts") = dtAvailFincnialAccounts
            ViewState("dtAddFinAccounts") = dtAddFinAccounts

            GrFinAddAccount.DataSource = dtAddFinAccounts
            GrFinAddAccount.DataBind()

            If btnSubmitFinAccount.Visible = False Then
                btnSubmitFinAccount.Visible = True
                btnAddFinComments.Visible = True

            End If


            GrFinAvailable.SelectedRowStyle.BackColor = Color.White
            GrFinAvailable.SelectedRowStyle.Font.Bold = False
            GrFinAvailable.SelectedRowStyle.ForeColor = Color.Black


            'uplAccounts.Update()
            clientForm.Update()
            'uplAccounts.Update()

        End If

    End Sub

    Protected Sub gvTCL_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvTCL.RowCommand
        If e.CommandName = "Select" Then


            TCLTextBox.Text = gvTCL.DataKeys(Convert.ToInt32(e.CommandArgument))("TCL").ToString()
            UserTypeCdTextBox.Text = gvTCL.DataKeys(Convert.ToInt32(e.CommandArgument))("UserTypeCd").ToString()
            UserTypeDescTextBox.Text = gvTCL.DataKeys(Convert.ToInt32(e.CommandArgument))("UserTypeDesc").ToString()

            If TCLTextBox.Text.ToLower = "remove" Then
                TCLTextBox.Text = ""
                UserTypeCdTextBox.Text = ""
                UserTypeDescTextBox.Text = ""

                gvTCL.Visible = False
                'btnChangeTCL.Visible = True
            Else
                pnlTCl.Visible = True
                'btnChangeTCL.Visible = True
                btnChangeTCL.Text = "Select TCL"
            End If
        End If

    End Sub

    Protected Sub btnSubmitDemoChange_Click(sender As Object, e As System.EventArgs) Handles btnSubmitDemoChange.Click
        Response.Redirect("~/providerDemo.aspx?clientnum=" & iClientNum)

        'iClientNum

    End Sub
    Protected Sub btnChangeType_Click(sender As Object, e As System.EventArgs) Handles btnChangeType.Click
        Response.Redirect("~/ChangeAffiliation.aspx?clientnum=" & iClientNum)

    End Sub
	Protected Sub UpdateLDAP()

		Dim AD_entry As New DirectoryEntry("LDAP//CCMCDC02/DC=CKHSAD,DC=CROZER,DC=ORG", System.Configuration.ConfigurationManager.AppSettings("ckhsad\ADSI"), System.Configuration.ConfigurationManager.AppSettings("crozer#123"))

		Dim dirEntryResults As New DirectoryEntry() 'searchResults.Path
		'dirEntryResults.Properties("telephoneNumber").Value = phoneTextBox.Text
		'sAMAccountName
		'title
		dirEntryResults.CommitChanges()
		dirEntryResults.Close()
	End Sub

	Protected Sub NewUPdateLDAP()
		'		Dim openDS As IADsOpenDSObject

		'		Dim x As IADs
		'		On Error GoTo Cleanup

		'Set x = GetObject("LDAP://CN=JeffSmith,CN=Users,DC=Fabrikam, DC=Com") 
		'x.Put "givenName", "Jeff"
		'x.Put "sn", "Smith"
		'x.SetInfo    ' Commit to the directory.

		'Cleanup:
		'		If (Err.Number <> 0) Then
		'			MsgBox("An error has occurred. " & Err.Number)
		'		End If
		'   Set x = Nothing

		'		Dim Namespace As IADsOpenDSObject
		'		Dim User As IADsUser
		'		Dim NewName As Variant
		'		Dim sUserName As String
		'		Dim sPassword As String

		'		On Error GoTo CleanUp

		'		Set Namespace = GetObject("LDAP:")

		'		' Insert code to safely get the user name and password

		'		Set User = Namespace.OpenDSObject("LDAP://MyMachine/CN=Administrator,CN=Users,DC=MyDomain,DC=Fabrikam,DC=COM", sUserName, sPassword, ADS_SECURE_AUTHENTICATION)

		'		NewName = InputBox("Enter a new name:")

		'		' Set using IADs::PutMethod
		'		User.Put "FullName", NewName
		'		User.SetInfo

		'		Exit Sub

		'CleanUp:
		'			Set IADsOpenDSObject = Nothing
		'			Set IADsUser = Nothing

	End Sub
    'Protected Sub btnsearcldap_Click(sender As Object, e As System.EventArgs) Handles btnsearcldap.Click
    '	UpdateUserADAccount(loginnameTextBox.Text, "")

    'End Sub

    'Protected Sub btnUpdldapdemo_Click(sender As Object, e As System.EventArgs) Handles btnUpdldapdemo.Click
    '	UpdateUserADAccount(loginnameTextBox.Text, "yes")

    'End Sub

    'Public Sub CheckADFields()

    '	LdapPhonelbl.Visible = True
    '	lblldapInfo.Visible = True

    '	If testphonelbl.Text <> phoneTextBox.Text Then
    '		btnsearcldap.Visible = True
    '		LdapPhonelbl.Text = "Update Phone  "
    '	Else
    '		btnsearcldap.Visible = True
    '		LdapPhonelbl.Text = "Phone Is Up To Date  "
    '	End If

    '	If testemployeenumberlbl.Text = "" And PMHEmployeeIDLabel.Text <> "" Then
    '		btnUpdldapdemo.Visible = True
    '		lblldapInfo.Text = "No Emp# found In AD"
    '		Exit Sub

    '	End If

    '	If testemployeenumberlbl.Text = "" And PMHEmployeeIDLabel.Text = "" Then
    '		btnUpdldapdemo.Visible = False
    '		lblldapInfo.Visible = False
    '		Exit Sub

    '	End If

    '	If testemployeenumberlbl.Text <> "" And PMHEmployeeIDLabel.Text <> "" Then

    '		If testemployeenumberlbl.Text <> PMHEmployeeIDLabel.Text Then
    '			btnUpdldapdemo.Visible = True
    '			lblldapInfo.Text = "Check Emp# With HR"
    '			Exit Sub

    '		End If

    '	End If

    '	If testemployeenumberlbl.Text <> "" And PMHEmployeeIDLabel.Text = "" Then
    '		btnUpdldapdemo.Visible = False
    '		lblldapInfo.Visible = False
    '		Exit Sub

    '	End If

    '	If testtitlelbl.Text = "" Then
    '		btnUpdldapdemo.Visible = True
    '		lblldapInfo.Text = "No Title found In AD"
    '		Exit Sub

    '	End If

    '	If testtitlelbl.Text.ToLower <> userpositiondescLabel.Text.ToLower Then
    '		btnUpdldapdemo.Visible = True
    '		lblldapInfo.Text = "Check Title With HR"
    '		Exit Sub
    '	End If

    '	If DepartmentNameLabel.Text = "" Then
    '		btnUpdldapdemo.Visible = True
    '		lblldapInfo.Text = "No Dept found In AD"
    '		Exit Sub

    '	End If

    '	If DepartmentNameLabel.Text.ToLower <> PMHDepartmentLabel.Text.ToLower Then
    '		btnUpdldapdemo.Visible = True
    '		lblldapInfo.Text = "Check Dept With HR"
    '		Exit Sub
    '	End If


    '	lblldapInfo.Text = "AD Fields are Up To Date"

    'End Sub
    Public Sub RetriveLDAP(ByVal userLogin As String)
		'RetriveLDAP(loginnameTextBox.Text)
		' Use for LDAP Search 

		Dim rdirectory As New DirectoryEntry("LDAP://CCMCDC03/DC=CKHSAD,DC=CROZER,DC=ORG", System.Configuration.ConfigurationManager.AppSettings("ckhsad\ADSI"), System.Configuration.ConfigurationManager.AppSettings("crozer#123"))

		Dim rfilter As String = "(SAMAccountName=acctname)"

		Dim rdirSearcher As DirectorySearcher = New DirectorySearcher(rdirectory, rfilter)
		'   1. Search the Active Directory for the speied user
		rdirSearcher.PageSize = 1000

		rdirSearcher.Filter = "(&(objectCategory=Person)(objectClass=user) (SAMAccountName=" & userLogin & "))"

		rdirSearcher.SearchScope = SearchScope.Subtree


		Dim RsearchResults As SearchResult = rdirSearcher.FindOne()


		If Not RsearchResults Is Nothing Then
			Dim RdirEntryResults As New DirectoryEntry(RsearchResults.Path)

			Dim testphone As String = ""


			testphonelbl.Text = RdirEntryResults.Properties("telephoneNumber").Value

			testemployeenumberlbl.Text = RdirEntryResults.Properties("employeeNumber").Value

			testtitlelbl.Text = RdirEntryResults.Properties("title").Value
			DepartmentNameLabel.Text = RdirEntryResults.Properties("department").Value


		End If

	End Sub
    'Public Sub UpdateUserADAccount(ByVal userLogin As String, ByVal empnum As String)

    '	Try


    '		'Dim directory As New DirectoryEntry("LDAP://CCMCDC03/DC=CKHSAD,DC=CROZER,DC=ORG", System.Configuration.ConfigurationManager.AppSettings("ckhsad\ADSI"), System.Configuration.ConfigurationManager.AppSettings("crozer#123"))

    '		Dim directory = GetDirectoryEntry()
    '		Dim filter As String = "(SAMAccountName=acctname)"

    '		Dim dirSearcher As DirectorySearcher = New DirectorySearcher(directory, filter)
    '		'   1. Search the Active Directory for the speied user
    '		dirSearcher.Filter = "(&(objectCategory=Person)(objectClass=user) (SAMAccountName=" & userLogin & "))"

    '		dirSearcher.SearchScope = SearchScope.Subtree


    '		Dim searchResults As SearchResult = dirSearcher.FindOne()

    '		If Not searchResults Is Nothing Then
    '			Dim dirEntryResults As New DirectoryEntry(searchResults.Path)

    '			'The properties listed here may be different then the 
    '			'properties in your Active Directory so they may need to be 
    '			'changed according to your network

    '			'   2. Set the new property values for the specified user
    '			Dim testphone As String = ""
    '			Dim newphone As String = phoneTextBox.Text

    '			If empnum = "" Then
    '				testphone = dirEntryResults.Properties("telephoneNumber").Value

    '				dirEntryResults.Properties("telephoneNumber").Value = phoneTextBox.Text
    '			ElseIf empnum = "yes" Then
    '				dirEntryResults.Properties("employeeNumber").Value = PMHEmployeeIDLabel.Text
    '				'dirEntryResults.Properties("employeeID").Value = PMHEmployeeIDLabel.Text

    '				dirEntryResults.Properties("title").Value = userpositiondescLabel.Text
    '				dirEntryResults.Properties("department").Value = PMHDepartmentLabel.Text
    '				'PMHEmployeeID
    '				'PMHDepartment
    '			End If

    '			'SetProperty(dirEntryResults, "department", userDepartment)
    '			'SetProperty(dirEntryResults, "title", userTitle)
    '			'SetProperty(dirEntryResults, "phone", userPhoneExt)

    '			'   3. Commit the changes
    '			Try
    '				dirEntryResults.CommitChanges()
    '				LDAPlbl.Text = "Data Updated"
    '				LDAPlbl.Visible = True
    '			Catch ex As Exception
    '				btnsearcldap.Visible = False
    '				'LDAPlbl.Text = "This LDAP info must be changed by Admin Only"
    '				LDAPlbl.Text = ex.Message.ToString
    '				LDAPlbl.Visible = True

    '			End Try
    '			'   4. Close & Cleanup
    '			dirEntryResults.Close()
    '		End If
    '		'   4a. Close & Cleanup
    '		directory.Close()
    '	Catch ex As Exception
    '		LDAPlbl.Text = ex.Message.ToString
    '		LDAPlbl.Visible = True

    '	End Try

    'End Sub
    Public Shared Function GetDirectoryEntry() As DirectoryEntry
        Dim dirEntry As DirectoryEntry = New DirectoryEntry()
		dirEntry.Path = "LDAP://CCMCDC03/DC=CKHSAD,DC=CROZER,DC=ORG"
		dirEntry.Username = "ckhsad\ADSI"
        dirEntry.Password = "crozer#123"
        Return (dirEntry)
    End Function
    Public Shared Sub SetADProperty(ByVal de As DirectoryEntry, ByVal pName As String, ByVal pValue As String)
        'First make sure the property value isnt "nothing"
        If Not pValue Is Nothing Then
            'Check to see if the DirectoryEntry contains this property already
            If de.Properties.Contains(pName) Then 'The DE contains this property
                'Update the properties value
                de.Properties(pName)(0) = pValue
            Else    'Property doesnt exist
                'Add the property and set it's value
                de.Properties(pName).Add(pValue)
            End If
        End If
    End Sub
    Protected Sub Checkfor365()

        'For Each cbMainApp As ListItem In cblApplications.Items
        '    If cbMainApp.Selected Then
        '        Select Case cbMainApp.Value
        '            Case 136
        For Each cbApp As ListItem In cblo365.Items
            Select Case cbApp.Value
                Case 248, 138
                    If cbApp.Selected Then

                    Else
                        If Session("environment").ToString.ToLower <> "testing" Then

                            If cbApp.Value = 138 Then

                                cbApp.Selected = True


                                cbApp.Enabled = False

                            End If
                        ElseIf Session("environment").ToString.ToLower = "testing" Then

                            If cbApp.Value = 248 Then

                                cbApp.Selected = True


                                cbApp.Enabled = False

                            End If

                        End If
                    End If


            End Select

        Next

        '        End Select
        '    End If
        'Next





    End Sub


    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        'If HDemployeetype.Text.ToLower <> "" Then
        '    If HDemployeetype.Text.ToLower <> emptypecdrbl.SelectedValue.ToLower Then
        '        If emptypecdrbl.SelectedValue = "physician" Or emptypecdrbl.SelectedValue = "allied health" Then

        '            lblValidation.Text = "Can Not Change Affiliation type to Physician type of any kind. You must get a PROVIDER Modifier to make this change."
        '            emptypecdrbl.SelectedValue = HDemployeetype.Text

        '            Exit Sub
        '        End If
        '    End If

        'End If


        If emptypecdrbl.SelectedIndex < 0 Then

            lblValidation.Text = "Must Select an Affiliation"

        End If



        Select Case emptypecdrbl.SelectedValue
            Case "782", "1197", "1194"

                Titleddl.ForeColor = Color.Black
                'Specialtyddl.ForeColor = Color.Black

                'CCMCConsultsrbl.ForeColor = Color.Black
                'DCMHConsultsrbl.ForeColor = Color.Black
                'TaylorConsultsrbl.ForeColor = Color.Black
                'SpringfieldConsultsrbl.ForeColor = Color.Black

                'Writeordersrbl.ForeColor = Color.Black
                'WriteordersDCMHrbl.ForeColor = Color.Black

                CCMCadmitRightsrbl.ForeColor = Color.Black
                'DCMHadmitRightsrbl.ForeColor = Color.Black

                If TitleLabel.Text = "" Then
                    lblValidation.Text = "Must supply Title"
                    lblValidation.ForeColor = Color.Red
                    lblValidation.Focus()

                    Titleddl.ForeColor = Color.Red
                    Exit Sub
                End If
                'Specialtyddl
                If SpecialtyLabel.Text = "" Then
                    lblValidation.Text = "Must supply Specialty"
                    lblValidation.ForeColor = Color.Red
                    lblValidation.Focus()

                    Specialtyddl.ForeColor = Color.Red
                    Exit Sub
                End If


                If CCMCadmitRightsrbl.SelectedIndex = -1 Then

                    lblValidation.Text = "Must select Admitting Rights "
                    lblValidation.ForeColor = Color.Red
                    lblValidation.Focus()
                    CCMCadmitRightsrbl.ForeColor = Color.Red

                    Return

                End If

                'DEA
                'If DEAIDTextBox.Text = "" Then

                '    lblValidation.Text = "Must supply DEA Info "
                '    lblValidation.ForeColor = Color.Red
                '    lblValidation.Focus()

                '    DEAIDTextBox.BackColor() = Color.Red
                '    Exit Sub

                'Else

                '    DEAIDTextBox.BackColor() = Color.Yellow
                '    DEAIDTextBox.BorderColor() = Color.Black
                '    DEAIDTextBox.BorderStyle() = BorderStyle.Solid
                '    DEAIDTextBox.BorderWidth = Unit.Pixel(2)

                'End If

                If npitextbox.Text = "" Then
                    lblValidation.Text = "Must supply NPI Info "
                    lblValidation.ForeColor = Color.Red
                    lblValidation.Focus()

                    npitextbox.BackColor() = Color.Red
                    Exit Sub

                Else

                    npitextbox.BackColor() = Color.Yellow
                    npitextbox.BorderColor() = Color.Black
                    npitextbox.BorderStyle() = BorderStyle.Solid
                    npitextbox.BorderWidth = Unit.Pixel(2)
                End If

                If LicensesNoTextBox.Text = "" Then

                    lblValidation.Text = "Must supply Licenses Info "
                    lblValidation.ForeColor = Color.Red
                    lblValidation.Focus()

                    LicensesNoTextBox.BackColor() = Color.Red
                    Exit Sub

                Else

                    LicensesNoTextBox.BackColor() = Color.Yellow
                    LicensesNoTextBox.BorderColor() = Color.Black
                    LicensesNoTextBox.BorderStyle() = BorderStyle.Solid
                    LicensesNoTextBox.BorderWidth = Unit.Pixel(2)
                End If

                'If DCMHadmitRightsrbl.SelectedIndex = -1 Then

                '    lblValidation.Text = "Must select Admitting Rights for DCMH"

                '    lblValidation.ForeColor = Color.Red
                '    lblValidation.Focus()
                '    DCMHadmitRightsrbl.ForeColor = Color.Red

                '    Return

                'End If


                'If hanrbl.SelectedValue.ToString = "Yes" Then

                '    Dim ViewCPMModelreq As New RequiredField(CPMEmployeesddl, "CPM Model")

                '    Dim npiReq As New RequiredField(npitextbox, "NPI")
                '    Dim LicensesNoReq As New RequiredField(LicensesNoTextBox, "Licenses No")
                '    Dim taxonomyreq As New RequiredField(taxonomytextbox, "Taxonomy")
                '    Dim startdatereq As New RequiredField(startdateTextBox, "Start Date")


                '    If Schedulablerbl.SelectedIndex = -1 Then

                '        lblValidation.Text = "Must select Schedulable Item"
                '        lblValidation.ForeColor = Color.Red
                '        lblValidation.Focus()
                '        Schedulablerbl.ForeColor = Color.Red

                '        Return

                '    End If
                '    If Televoxrbl.SelectedIndex = -1 Then

                '        lblValidation.Text = "Must select Schedulable Item"
                '        lblValidation.ForeColor = Color.Red
                '        lblValidation.Focus()
                '        Televoxrbl.ForeColor = Color.Red

                '        Return

                '    End If

                '    If ViewCPMModelTextBox.Text = "" Then

                '        lblValidation.Text = "Must select CPM Model After"
                '        lblValidation.ForeColor = Color.Red
                '        lblValidation.Focus()
                '        CPMEmployeesddl.ForeColor = Color.Red

                '        Return


                '    End If

                'End If




                'If TCLTextBox.Text = "" Then
                '    lblValidation.Text = "Must select TCL Type"
                '    lblValidation.ForeColor = Color.Red
                '    lblValidation.Focus()

                '    Specialtyddl.ForeColor = Color.Red
                '    Exit Sub
                'End If

                'If CCMCConsultsrbl.SelectedIndex = -1 Then

                '    lblValidation.Text = "Must select CCMC Consults"
                '    lblValidation.ForeColor = Color.Red
                '    lblValidation.Focus()
                '    CCMCConsultsrbl.ForeColor = Color.Red

                '    Return

                'End If


                'If DCMHConsultsrbl.SelectedIndex = -1 Then

                '    lblValidation.Text = "Must select DCMH Consults"
                '    lblValidation.ForeColor = Color.Red
                '    lblValidation.Focus()
                '    DCMHConsultsrbl.ForeColor = Color.Red

                '    Return

                'End If

                'If TaylorConsultsrbl.SelectedIndex = -1 Then

                '    lblValidation.Text = "Must select Taylor Consults"
                '    lblValidation.ForeColor = Color.Red
                '    lblValidation.Focus()
                '    TaylorConsultsrbl.ForeColor = Color.Red

                '    Return

                'End If

                'If SpringfieldConsultsrbl.SelectedIndex = -1 Then

                '    lblValidation.Text = "Must select Springfield Consults"
                '    lblValidation.ForeColor = Color.Red
                '    lblValidation.Focus()
                '    SpringfieldConsultsrbl.ForeColor = Color.Red

                '    Return

                'End If



                'If Writeordersrbl.SelectedIndex = -1 Then

                '    lblValidation.Text = "Must select Write Orders for C.T.S."
                '    lblValidation.ForeColor = Color.Red
                '    lblValidation.Focus()
                '    Writeordersrbl.ForeColor = Color.Red

                '    Return

                'End If


                'If WriteordersDCMHrbl.SelectedIndex = -1 Then

                '    lblValidation.Text = "Must select Write Orders for DCMH"
                '    lblValidation.ForeColor = Color.Red
                '    lblValidation.Focus()
                '    WriteordersDCMHrbl.ForeColor = Color.Red

                '    Return

                'End If

                'If hanrbl.SelectedValue.ToLower = "yes" Then
                '    If LocationOfCareIDLabel.Text = "" Then
                '        lblValidation.Text = "Must select CKHN Group"
                '        lblValidation.ForeColor = Color.Red
                '        lblValidation.Focus()
                '        LocationOfCareIDddl.ForeColor = Color.Red
                '        Return
                '    End If
                '    'More test here


                '    'npitextbox, "NPI")
                '    'LicensesNoTextBox, "Licenses No")
                '    'taxonomytextbox, "Taxonomy")
                '    'startdateTextBox, "Start Date")
                '    'Schedulablerbl, "Schedule")
                '    'Televoxrbl, "Televox")

                'End If


                'Case "physician", "non staff clinician", "resident", "allied health"
                '    If TitleLabel.Text = "" Then
                '        lblValidation.Text = "Must supply Title"
                '        Titleddl.Focus()
                '        Exit Sub
                '    End If
                '    'Specialtyddl
                '    If SpecialtLabel.Text = "" Then
                '        lblValidation.Text = "Must supply Specialty"
                '        Specialtyddl.Focus()
                '        Exit Sub
                '    End If

                'Case "1193"
                'Select Case PositionRoleNumLabel.Text
                '    Case "1567"

                '        If PhysOfficeddl.SelectedIndex < 1 Then
                '            lblValidation.Text = "Must select Phys Office location"
                '            lblValidation.ForeColor = Color.Red
                '            lblValidation.Focus()
                '            PhysOfficeddl.ForeColor = Color.Red
                '            Return
                '            ' More fields tests go here


                '        End If
                '        If ViewCPMModelTextBox.Text = "" Then

                '            lblValidation.Text = "Must select CPM Model After"
                '            lblValidation.ForeColor = Color.Red
                '            lblValidation.Focus()
                '            CPMEmployeesddl.ForeColor = Color.Red

                '            Return


                '        End If


                '        'startdateTextBox, "Start Date")

                '    Case Else

                'End Select
        End Select


        'If startdateTextBox.Text <> "" Then
        '    CheckDate(startdateTextBox.Text)
        'End If

        ' 
        CheckRequiredFields()

        'If TitleLabel.Text = "" Then
        '    lblValidation.Text = "Must supply Title"
        '    Titleddl.Focus()
        '    Exit Sub
        'End If
        'If RoleNumLabel.Text = "1200" And (PositionNumTextBox.Text = "" Or PositionNumTextBox.Text = "0") Then

        '    'RoleNumLabel.Text
        '    lblValidation.Text = "Must Specify a Nursing Type"
        '    lblValidation.ForeColor = Color.Red
        '    lblValidation.Focus()

        '    Titleddl.ForeColor = Color.Red
        '    Exit Sub

        'End If

        'If RoleNumLabel.Text = "1189" And (PositionNumTextBox.Text = "" Or PositionNumTextBox.Text = "0") Then
        '    lblValidation.Text = "Must Specify a Student Type"
        '    lblValidation.ForeColor = Color.Red
        '    lblValidation.Focus()

        '    Titleddl.ForeColor = Color.Red
        '    Exit Sub

        'End If



        Dim i As Integer
        If npitextbox.Text <> "" Then

            Try
                i = Convert.ToInt32(npitextbox.Text)
            Catch
                npitextbox.Text = ""
                lblValidation.Text = " NPI entry invalid - Numbers Only"
                Exit Sub
            End Try

        End If

        If DoctorMasterNumTextBox.Text <> "" Then

            Try
                i = Convert.ToInt32(DoctorMasterNumTextBox.Text)
            Catch
                DoctorMasterNumTextBox.Text = ""
                lblValidation.Text = "Invalid Doctor Master Number - Numbers Only"
                Exit Sub
            End Try

        End If


        If EmpDoctorTextBox.Text <> "" Then

            Try
                i = Convert.ToInt32(EmpDoctorTextBox.Text)
            Catch
                EmpDoctorTextBox.Text = ""
                lblValidation.Text = "Invalid Doctor Master Number - Numbers Only"
                Exit Sub
            End Try

            DoctorMasterNumTextBox.Text = EmpDoctorTextBox.Text

        End If

        '' City State zip and phone Fax 
        'If citytextbox.Text <> "" Then

        '    If (Regex.IsMatch(citytextbox.Text, "^[\.\-A-Za-z]") = False) Then

        '        lblValidation.Text = "City name can Not have numbers."
        '        lblValidation.ForeColor = Color.Red
        '        'citytextbox.Text = ""
        '        citytextbox.Focus()
        '        citytextbox.BackColor() = Color.Red
        '        Exit Sub

        '    End If

        'End If

        If statetextbox.Text <> "" Then

            If (Regex.IsMatch(statetextbox.Text, "^[\.\-A-Za-z]") = False) Then
                lblValidation.Visible = True
                lblValidation.Text = "State can Not have numbers."
                lblValidation.ForeColor = Color.Red
                'statetextbox.Text = ""

                statetextbox.Focus()
                statetextbox.BackColor() = Color.Red
                Exit Sub
            Else
                lblValidation.Visible = False
                statetextbox.BackColor() = Color.Empty

            End If
        End If

        If phoneTextBox.Text <> "" Then
            If Not IsPhoneNumberValid(phoneTextBox.Text) Then
                Dim isvalid = False
                lblValidation.Visible = True
                lblValidation.Text = "*Invalid Phone Number - Alph Chaaracter has been detected -  Format (999)-000.0000 "

                phoneTextBox.Focus()
                phoneTextBox.BackColor() = Color.Red

                Exit Sub
            Else
                lblValidation.Visible = False
                lblValidation.Text = ""

                phoneTextBox.BackColor() = Color.Empty

            End If

        End If
        If pagerTextBox.Text <> "" Then
            If Not IsPhoneNumberValid(pagerTextBox.Text) Then
                Dim isvalid = False
                lblValidation.Visible = True
                lblValidation.Text = "*Invalid Pager Number - Alph Chaaracter has been detected -  Format (999)-000.0000 "

                pagerTextBox.Focus()
                pagerTextBox.BackColor() = Color.Red

                Exit Sub
            Else
                lblValidation.Visible = False
                lblValidation.Text = ""
                pagerTextBox.BackColor() = Color.Empty

            End If

        End If


        If faxtextbox.Text <> "" Then
            If Not IsPhoneNumberValid(faxtextbox.Text) Then
                Dim isvalid = False
                lblValidation.Visible = True
                lblValidation.Text = "*Invalid Fax Number - Alph Chaaracter has been detected -  Format (999)-000.0000 "

                faxtextbox.Focus()
                faxtextbox.BackColor() = Color.Red

                Exit Sub
            Else
                lblValidation.Visible = False
                lblValidation.Text = ""
                faxtextbox.BackColor() = Color.Empty
            End If


        End If


        If ziptextbox.Text <> "" Then

            If (Regex.IsMatch(ziptextbox.Text, "^[0-9]") = False) Then

                lblValidation.Text = "Zip Code can only contain numbers."
                lblValidation.ForeColor = Color.Red
                lblValidation.Visible = True

                'ziptextbox.Text = ""

                ziptextbox.Focus()
                ziptextbox.BackColor() = Color.Red
                Exit Sub
            Else

                lblValidation.Visible = False
                ziptextbox.BackColor() = Color.Empty

            End If

        End If





        FormSignOn.AddInsertParm("@clientnum", iClientNum, SqlDbType.Int, 9)
        FormSignOn.UpdateForm()

        Dim sDate As DateTime = Date.Now()

        lblValidation.Text = "Information Updated at -> " & sDate


        'Dim stProvider As String = FormSignOn.getFieldValue("provider").Trim()
        'Dim isProvider As String = providerrbl.SelectedValue

        'If (stProvider = "" Or stProvider.ToLower = "no") And isProvider.ToLower = "yes" Then

        '    Dim FormProvider = New FormSQL(Session("EmployeeConn"), "SelAccountRequestV6", "d", "", Page)

        '    FormProvider.AddInsertParm("@submitter_client_num", UserInfo.ClientNum, SqlDbType.NVarChar, 10)
        '    'FormProvider.AddInsertParm("@requestor_client_num", UserInfo.ClientNum, SqlDbType.NVarChar, 10)
        '    FormProvider.AddInsertParm("@receiver_client_num", iClientNum, SqlDbType.Int, 30)
        '    FormProvider.AddInsertParm("@account_request_type", "provider", SqlDbType.NVarChar, 9)

        '    FormProvider.UpdateFormReturnReqNum()

        '    Dim AccountRequestNum As Integer = FormProvider.AccountRequestNum


        '    If Session("environment").ToString.ToLower <> "testing" Then
        '        SendProviderEmail(AccountRequestNum)

        '    End If


        'End If

        gvTCL.Visible = False

        clientForm.Update()


        'Select Case RoleNumLabel.Text


        '    Case "782", "1197", "1194"
        '        If CPMModelTextBox.Text <> "" Then


        '            insAcctDriveModel(iClientNum, "provmodel", CPMModelTextBox.Text, "", "", "")

        '        End If
        '        If Schedulablerbl.SelectedValue.ToLower <> "" Then

        '            insAcctDriveModel(iClientNum, "schedulemodel", Schedulablerbl.SelectedValue.ToLower, "", "", "")

        '        End If
        '        If Televoxrbl.SelectedValue.ToLower <> "" Then
        '            insAcctDriveModel(iClientNum, "televoxmodel", Televoxrbl.SelectedValue.ToLower, "", "", "")
        '        End If

        '    Case "1193"

        '        If CPMModelTextBox.Text <> "" Then
        '            insAcctDriveModel(iClientNum, "cpmmodel", CPMModelTextBox.Text, "", "", "")

        '        End If
        'End Select

        Response.Redirect("~/ClientDemo.aspx?ClientNum=" & iClientNum & "&label=" & lblValidation.Text, True)



    End Sub
    Private Sub insAcctDriveModel(ByVal requestNum As Integer, ByVal ModelCd As String, ByVal comments As String, ByVal CPMTrainCd As String, ByVal Requested_Date As String, ByVal DriveLetter As String)


        Dim thisdata As New CommandsSqlAndOleDb("InsUpdClientModelsAndDrivesV3", Session("EmployeeConn"))
        thisdata.AddSqlProcParameter("@client_num", requestNum, SqlDbType.Int, 20)


        thisdata.AddSqlProcParameter("@ModelCd", ModelCd, SqlDbType.NVarChar, 24)
        thisdata.AddSqlProcParameter("@CPMTrainCd", CPMTrainCd, SqlDbType.NVarChar, 24)

        If CPMTrainCd <> "" Then
            thisdata.AddSqlProcParameter("@ApplicationNum", 157, SqlDbType.Int, 8)

            thisdata.AddSqlProcParameter("@Requested_Date", Requested_Date, SqlDbType.NVarChar, 24)

        End If

        thisdata.AddSqlProcParameter("@DriveLetter", DriveLetter, SqlDbType.NVarChar, 24)

        thisdata.AddSqlProcParameter("@account_item_desc", comments, SqlDbType.NVarChar, 255)

        thisdata.AddSqlProcParameter("@who_nt_login", Session("LoginID"), SqlDbType.NVarChar, 50)

        thisdata.ExecNonQueryNoReturn()



    End Sub
    Protected Sub btnCloseOnBelafOf_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCloseOnBelafOf.Click
        pnlOnBehalfOf.Visible = False
        'ModBeHalOfPopup.Hide()

    End Sub
    Protected Sub btnSelectOnBehalfOf_Click(sender As Object, e As System.EventArgs) Handles btnSelectOnBehalfOf.Click
        'ModBeHalOfPopup.Show()

        'btnSelectOnBehalfOf
        'make Panel visiable
        With pnlOnBehalfOf
            .Visible = True
            With .Style
                .Add("LEFT", "20px")
                .Add("TOP", "50px")
                .Add("POSITION", "absolute")
                .Add("Z-INDEX", "999")
            End With
        End With
    End Sub
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click

        Dim thisData As New CommandsSqlAndOleDb("SelActiveCKHSEmployees", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@FirstName", SearchFirstNameTextBox.Text, SqlDbType.NVarChar, 20)
        thisData.AddSqlProcParameter("@LastName", SearchLastNameTextBox.Text, SqlDbType.NVarChar, 20)


        gvSearch.DataSource = thisData.GetSqlDataTable
        gvSearch.DataBind()

        'ModBeHalOfPopup.Show()

        'uplOnBehalfOf.Update()
        'mpeOnBehalfOf.Show()

    End Sub

    Protected Sub btnDeactivateClient_Click(sender As Object, e As System.EventArgs) Handles btnDeactivateClient.Click
        Dim thisData As New CommandsSqlAndOleDb("DelDeactivatedClients", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@clientnum", iClientNum, SqlDbType.Int, 20)

        thisData.AddSqlProcParameter("@ntlogin", Session("LoginID").ToString, SqlDbType.NVarChar, 50)


        thisData.ExecNonQueryNoReturn()

        Response.Redirect("~/SimpleSearch.aspx", True)


    End Sub

    Private Shared Function IsPhoneNumberValid(phoneNumber As String) As Boolean
        Dim result As String = ""
        Dim chars As Char() = phoneNumber.ToCharArray()
        For count = 0 To chars.GetLength(0) - 1
            Dim tempChar As Char = chars(count)
            If [Char].IsDigit(tempChar) Or "()+-., ".Contains(tempChar.ToString()) Then

                result += StripNonAlphaNumeric(tempChar)
            Else
                Return False
            End If

        Next
        Return True
        'Return result.Length = 10 'Length of US phone numbers is 10
    End Function

    Private Shared Function StripNonAlphaNumeric(value As String) As String
        Dim regex = New Regex("[^0-9a-zA-Z]", RegexOptions.None)
        Dim result As String = ""
        If regex.IsMatch(value) Then
            result = regex.Replace(value, "")
        Else
            result = value
        End If

        Return result
    End Function

    Protected Sub LoadclientNursestations(ByVal iAppnum As Integer)

        For Each cblcItem As ListItem In cblCCMCNursestations.Items
            cblcItem.Selected = False
            'cblItem.Enabled = False
        Next

        For Each cblDItem As ListItem In cblDCMHNursestataions.Items
            cblDItem.Selected = False
            'cblItem.Enabled = False
        Next

        For Each cblTItem As ListItem In cblTaylorNursestataions.Items
            cblTItem.Selected = False
            'cblItem.Enabled = False
        Next

        For Each cblSItem As ListItem In cblSpringfieldNursestataions.Items
            cblSItem.Selected = False
            'cblItem.Enabled = False
        Next

        For Each cbl365Item As ListItem In cblo365.Items
            cbl365Item.Selected = False
            'cblItem.Enabled = False
        Next

		'cblo365

		Select Case iAppnum
			Case 136

				Dim cbl365Binder As New CheckBoxListBinder(cblo365, "SelAvailableSubAccountsByClientNumberV8", Session("EmployeeConn"))

				cbl365Binder.AddDDLCriteria("@client_num", iClientNum, SqlDbType.Int)
				cbl365Binder.AddDDLCriteria("@userClientNum", submitter_client_numLabel.Text, SqlDbType.Int)
				' cblCCMCNurBinder.AddDDLCriteria("@FacilityCD", lblAppSystem.Text, SqlDbType.NVarChar)
				cbl365Binder.AddDDLCriteria("@ApplicationNum", iAppnum, SqlDbType.Int)
				cbl365Binder.BindData("SubApplicationnum", "SubAppDesc")

				Checkfor365()

		End Select


		Select Case lblAppSystem.Text
            Case "15"

				' convert back to appnum for nurse instructor
				If iAppnum = 65 Then
					iAppnum = 97
				End If

				Dim cblCCMCNurBinder As New CheckBoxListBinder(cblCCMCNursestations, "SelAvailableSubAccountsByClientNumberV8", Session("EmployeeConn"))

                cblCCMCNurBinder.AddDDLCriteria("@client_num", iClientNum, SqlDbType.Int)
                cblCCMCNurBinder.AddDDLCriteria("@userClientNum", submitter_client_numLabel.Text, SqlDbType.Int)
                cblCCMCNurBinder.AddDDLCriteria("@FacilityCD", lblAppSystem.Text, SqlDbType.NVarChar)
                cblCCMCNurBinder.AddDDLCriteria("@ApplicationNum", iAppnum, SqlDbType.Int)
                cblCCMCNurBinder.BindData("SubApplicationnum", "SubAppDesc")


            Case "12"

				' convert back to appnum for nurse instructor
				If iAppnum = 65 Then
					iAppnum = 98
				End If

				Dim cblDCMHNurBinder As New CheckBoxListBinder(cblDCMHNursestataions, "SelAvailableSubAccountsByClientNumberV8", Session("EmployeeConn"))
                cblDCMHNurBinder.AddDDLCriteria("@client_num", iClientNum, SqlDbType.Int)
                cblDCMHNurBinder.AddDDLCriteria("@userClientNum", submitter_client_numLabel.Text, SqlDbType.Int)
                cblDCMHNurBinder.AddDDLCriteria("@FacilityCD", lblAppSystem.Text, SqlDbType.NVarChar)
                cblDCMHNurBinder.AddDDLCriteria("@ApplicationNum", iAppnum, SqlDbType.Int)
                cblDCMHNurBinder.BindData("SubApplicationnum", "SubAppDesc")


            Case "19"

				' convert back to appnum for nurse instructor
				If iAppnum = 65 Then
					iAppnum = 99
				End If

				Dim cblTaylorNurBinder As New CheckBoxListBinder(cblTaylorNursestataions, "SelAvailableSubAccountsByClientNumberV8", Session("EmployeeConn"))
                cblTaylorNurBinder.AddDDLCriteria("@client_num", iClientNum, SqlDbType.Int)
                cblTaylorNurBinder.AddDDLCriteria("@userClientNum", submitter_client_numLabel.Text, SqlDbType.Int)
                cblTaylorNurBinder.AddDDLCriteria("@FacilityCD", lblAppSystem.Text, SqlDbType.NVarChar)
                cblTaylorNurBinder.AddDDLCriteria("@ApplicationNum", iAppnum, SqlDbType.Int)
                cblTaylorNurBinder.BindData("SubApplicationnum", "SubAppDesc")

            Case "16"

				' convert back to appnum for nurse instructor
				If iAppnum = 65 Then
					iAppnum = 100
				End If

				Dim cblSpringNurBinder As New CheckBoxListBinder(cblSpringfieldNursestataions, "SelAvailableSubAccountsByClientNumberV8", Session("EmployeeConn"))
                cblSpringNurBinder.AddDDLCriteria("@client_num", iClientNum, SqlDbType.Int)
                cblSpringNurBinder.AddDDLCriteria("@userClientNum", submitter_client_numLabel.Text, SqlDbType.Int)
                cblSpringNurBinder.AddDDLCriteria("@FacilityCD", lblAppSystem.Text, SqlDbType.NVarChar)
                cblSpringNurBinder.AddDDLCriteria("@ApplicationNum", iAppnum, SqlDbType.Int)
                cblSpringNurBinder.BindData("SubApplicationnum", "SubAppDesc")



        End Select


    End Sub
    Protected Sub DeleteNursestsion(ByVal requestNum As Integer, ByVal appNum As Integer, ByVal comments As String, ByVal Subaccount As Integer)

        Dim iFacility As Integer

        Select Case appNum
            Case "136"
                If Subaccount = 324 Then
                    iFacility = 20
                Else
                    iFacility = 26

                End If

            Case 97
                iFacility = 15
            Case 98
                iFacility = 12

            Case 99
                iFacility = 19

            Case 100

                iFacility = 16

        End Select


        If ValidationStatus.Text = "" Then
            ValidationStatus.Text = "NeedsValidation"
        End If

        Dim SubmitData As New CommandsSqlAndOleDb("InsAccountSubApplicationV3", Session("EmployeeConn"))
        SubmitData.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int, 9)
        SubmitData.AddSqlProcParameter("@ApplicationNum", appNum, SqlDbType.Int, 9)
        SubmitData.AddSqlProcParameter("@SubApplicationNum", Subaccount, SqlDbType.Int, 9)
        SubmitData.AddSqlProcParameter("@FacilityCd", iFacility, SqlDbType.Int, 9)
        SubmitData.AddSqlProcParameter("@validate", ValidationStatus.Text, SqlDbType.NVarChar, 20)

        SubmitData.AddSqlProcParameter("@who_nt_login", Session("LoginID"), SqlDbType.NVarChar, 50)

        SubmitData.ExecNonQueryNoReturn()


    End Sub
    Protected Sub InsertNursestations(ByVal requestNum As Integer, ByVal appNum As String, ByVal comments As String, ByVal Facility As String, ByVal sValidationStatus As String)

        Dim SubAppnum As String = ""
        Dim SubAppcd As String = ""
        Dim subAppDesc As String = ""

        Select Case appNum
            Case "136"
                'o365 accounts
                'cblo365
                For Each cblo365Item As ListItem In cblo365.Items
                    Dim sCurritem As String = cblo365Item.Value

                    If cblo365Item.Selected = True Then

                        If sCurritem = 324 Then
                            Facility = 20
                        Else
                            Facility = 26

                        End If

                        Dim SubmitData As New CommandsSqlAndOleDb("InsAccountSubApplicationV3", Session("EmployeeConn"))
                        SubmitData.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int, 9)
                        SubmitData.AddSqlProcParameter("@ApplicationNum", appNum, SqlDbType.Int, 9)
                        SubmitData.AddSqlProcParameter("@SubApplicationNum", cblo365Item.Value, SqlDbType.Int, 9)
                        SubmitData.AddSqlProcParameter("@FacilityCd", Facility, SqlDbType.Int, 9)
                        SubmitData.AddSqlProcParameter("@validate", sValidationStatus, SqlDbType.NVarChar, 20)

                        SubmitData.AddSqlProcParameter("@who_nt_login", Session("LoginID"), SqlDbType.NVarChar, 50)

                        SubmitData.ExecNonQueryNoReturn()


                    End If

                Next

            Case Else


                Select Case lblAppSystem.Text
                    Case "15"

                        For Each cblcItem As ListItem In cblCCMCNursestations.Items

                            If cblcItem.Selected = True Then

                                Dim SubmitData As New CommandsSqlAndOleDb("InsAccountSubApplicationV3", Session("EmployeeConn"))
                                SubmitData.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int, 9)
                                SubmitData.AddSqlProcParameter("@ApplicationNum", appNum, SqlDbType.Int, 9)
                                SubmitData.AddSqlProcParameter("@SubApplicationNum", cblcItem.Value, SqlDbType.Int, 9)
                                SubmitData.AddSqlProcParameter("@FacilityCd", Facility, SqlDbType.Int, 9)
                                SubmitData.AddSqlProcParameter("@validate", sValidationStatus, SqlDbType.NVarChar, 20)

                                SubmitData.AddSqlProcParameter("@who_nt_login", Session("LoginID"), SqlDbType.NVarChar, 50)

                                SubmitData.ExecNonQueryNoReturn()

                            End If

                        Next

                    Case "12"

                        For Each cblDItem As ListItem In cblDCMHNursestataions.Items
                            If cblDItem.Selected = True Then


                                Dim SubmitData As New CommandsSqlAndOleDb("InsAccountSubApplicationV3", Session("EmployeeConn"))
                                SubmitData.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int, 9)
                                SubmitData.AddSqlProcParameter("@ApplicationNum", appNum, SqlDbType.Int, 9)
                                SubmitData.AddSqlProcParameter("@SubApplicationNum", cblDItem.Value, SqlDbType.Int, 9)
                                SubmitData.AddSqlProcParameter("@FacilityCd", Facility, SqlDbType.Int, 9)
                                SubmitData.AddSqlProcParameter("@validate", sValidationStatus, SqlDbType.NVarChar, 20)

                                SubmitData.AddSqlProcParameter("@who_nt_login", Session("LoginID"), SqlDbType.NVarChar, 50)

                                SubmitData.ExecNonQueryNoReturn()


                            End If

                        Next

                    Case "19"

                        For Each cblTItem As ListItem In cblTaylorNursestataions.Items


                            If cblTItem.Selected = True Then
                                'cblItem.Enabled = False

                                ' get subapp number

                                Dim SubmitData As New CommandsSqlAndOleDb("InsAccountSubApplicationV3", Session("EmployeeConn"))
                                SubmitData.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int, 9)
                                SubmitData.AddSqlProcParameter("@ApplicationNum", appNum, SqlDbType.Int, 9)
                                SubmitData.AddSqlProcParameter("@SubApplicationNum", cblTItem.Value, SqlDbType.Int, 9)
                                SubmitData.AddSqlProcParameter("@FacilityCd", Facility, SqlDbType.Int, 9)
                                SubmitData.AddSqlProcParameter("@validate", sValidationStatus, SqlDbType.NVarChar, 20)

                                SubmitData.AddSqlProcParameter("@who_nt_login", Session("LoginID"), SqlDbType.NVarChar, 50)

                                SubmitData.ExecNonQueryNoReturn()

                            End If

                        Next

                    Case "16"


                        For Each cblSItem As ListItem In cblSpringfieldNursestataions.Items


                            If cblSItem.Selected = True Then

                                Dim SubmitData As New CommandsSqlAndOleDb("InsAccountSubApplicationV3", Session("EmployeeConn"))
                                SubmitData.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int, 9)
                                SubmitData.AddSqlProcParameter("@ApplicationNum", appNum, SqlDbType.Int, 9)
                                SubmitData.AddSqlProcParameter("@SubApplicationNum", cblSItem.Value, SqlDbType.Int, 9)
                                SubmitData.AddSqlProcParameter("@FacilityCd", Facility, SqlDbType.Int, 9)
                                SubmitData.AddSqlProcParameter("@validate", sValidationStatus, SqlDbType.NVarChar, 20)

                                SubmitData.AddSqlProcParameter("@who_nt_login", Session("LoginID"), SqlDbType.NVarChar, 50)

                                SubmitData.ExecNonQueryNoReturn()

                            End If

                        Next

                End Select

        End Select


    End Sub
	Protected Sub InsertNurseInstructor(ByVal requestNum As Integer)


		For Each cblcItem As ListItem In cblCCMCNursestations.Items

			If cblcItem.Selected = True Then
				Dim SubmitData As New CommandsSqlAndOleDb("InsAccountSubApplicationV3", Session("EmployeeConn"))
				SubmitData.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int, 9)
				SubmitData.AddSqlProcParameter("@ApplicationNum", 97, SqlDbType.Int, 9)
				SubmitData.AddSqlProcParameter("@SubApplicationNum", cblcItem.Value, SqlDbType.Int, 9)
				SubmitData.AddSqlProcParameter("@FacilityCd", 15, SqlDbType.Int, 9)
				SubmitData.AddSqlProcParameter("@validate", "NeedsValidation", SqlDbType.NVarChar, 20)
				SubmitData.AddSqlProcParameter("@who_nt_login", Session("LoginID"), SqlDbType.NVarChar, 50)

				SubmitData.ExecNonQueryNoReturn()

			End If

		Next


		For Each cblDItem As ListItem In cblDCMHNursestataions.Items
			If cblDItem.Selected = True Then

				Dim SubmitData As New CommandsSqlAndOleDb("InsAccountSubApplicationV3", Session("EmployeeConn"))
				SubmitData.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int, 9)
				SubmitData.AddSqlProcParameter("@ApplicationNum", 98, SqlDbType.Int, 9)
				SubmitData.AddSqlProcParameter("@SubApplicationNum", cblDItem.Value, SqlDbType.Int, 9)
				SubmitData.AddSqlProcParameter("@FacilityCd", 12, SqlDbType.Int, 9)
				SubmitData.AddSqlProcParameter("@validate", "NeedsValidation", SqlDbType.NVarChar, 20)
				SubmitData.AddSqlProcParameter("@who_nt_login", Session("LoginID"), SqlDbType.NVarChar, 50)

				SubmitData.ExecNonQueryNoReturn()


			End If

		Next


		For Each cblTItem As ListItem In cblTaylorNursestataions.Items

			If cblTItem.Selected = True Then
				'cblItem.Enabled = False

				' get subapp number
				Dim SubmitData As New CommandsSqlAndOleDb("InsAccountSubApplicationV3", Session("EmployeeConn"))
				SubmitData.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int, 9)
				SubmitData.AddSqlProcParameter("@ApplicationNum", 99, SqlDbType.Int, 9)
				SubmitData.AddSqlProcParameter("@SubApplicationNum", cblTItem.Value, SqlDbType.Int, 9)
				SubmitData.AddSqlProcParameter("@FacilityCd", 19, SqlDbType.Int, 9)
				SubmitData.AddSqlProcParameter("@validate", "NeedsValidation", SqlDbType.NVarChar, 20)
				SubmitData.AddSqlProcParameter("@who_nt_login", Session("LoginID"), SqlDbType.NVarChar, 50)

				SubmitData.ExecNonQueryNoReturn()

			End If

		Next


		For Each cblSItem As ListItem In cblSpringfieldNursestataions.Items


			If cblSItem.Selected = True Then
				Dim SubmitData As New CommandsSqlAndOleDb("InsAccountSubApplicationV3", Session("EmployeeConn"))
				SubmitData.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int, 9)
				SubmitData.AddSqlProcParameter("@ApplicationNum", 100, SqlDbType.Int, 9)
				SubmitData.AddSqlProcParameter("@SubApplicationNum", cblSItem.Value, SqlDbType.Int, 9)
				SubmitData.AddSqlProcParameter("@FacilityCd", 16, SqlDbType.Int, 9)
				SubmitData.AddSqlProcParameter("@validate", "NeedsValidation", SqlDbType.NVarChar, 20)
				SubmitData.AddSqlProcParameter("@who_nt_login", Session("LoginID"), SqlDbType.NVarChar, 50)

				SubmitData.ExecNonQueryNoReturn()

			End If

		Next

		Dim SubmitDataNI As New CommandsSqlAndOleDb("InsNurseInstructorAccountItemsV3", Session("EmployeeConn"))
		SubmitDataNI.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int, 9)
		SubmitDataNI.AddSqlProcParameter("@who_nt_login", Session("LoginID"), SqlDbType.NVarChar, 50)

		SubmitDataNI.AddSqlProcParameter("@NewLogin", loginnameTextBox.Text, SqlDbType.NVarChar, 50)
		SubmitDataNI.AddSqlProcParameter("@requestType", "addacct", SqlDbType.NVarChar, 12)
		SubmitDataNI.AddSqlProcParameter("@validate", "NeedsValidation", SqlDbType.NVarChar, 20)



		SubmitDataNI.ExecNonQueryNoReturn()


	End Sub



	Private Sub FillAffiliations()
        'SelInvalidCodes
        emptypecdrbl.Items.Clear()

        Dim dt As DataTable
        Dim thisData As New CommandsSqlAndOleDb
        thisData = New CommandsSqlAndOleDb("SelAffiliationsV5", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@Type", "phys", SqlDbType.NVarChar, 8)


        dt = thisData.GetSqlDataTable()
        emptypecdrbl.DataSource = dt
        emptypecdrbl.DataValueField = "RoleNum"
        emptypecdrbl.DataTextField = "AffiliationDisplay"
        emptypecdrbl.DataBind()
        thisData = Nothing

    End Sub
    Protected Sub cblCCMCAll_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cblCCMCAll.SelectedIndexChanged

        If cblCCMCAll.SelectedValue = "ccmcall" Then

            For Each cblcItem As ListItem In cblCCMCNursestations.Items
                cblcItem.Selected = True
                'cblItem.Enabled = False
            Next

        Else
            For Each cblcItem As ListItem In cblCCMCNursestations.Items
                cblcItem.Selected = False
                'cblItem.Enabled = False
            Next

        End If
    End Sub
    Protected Sub cblDCMHAll_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cblDCMHAll.SelectedIndexChanged

        If cblDCMHAll.SelectedValue = "dcmhall" Then

            For Each cblcItem As ListItem In cblDCMHNursestataions.Items
                cblcItem.Selected = True
                'cblItem.Enabled = False
            Next

        Else
            For Each cblcItem As ListItem In cblDCMHNursestataions.Items
                cblcItem.Selected = False
                'cblItem.Enabled = False
            Next

        End If
    End Sub
    Protected Sub cblTAll_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cblTAll.SelectedIndexChanged

        If cblTAll.SelectedValue = "taylorall" Then

            For Each cblcItem As ListItem In cblTaylorNursestataions.Items
                cblcItem.Selected = True
                'cblItem.Enabled = False
            Next

        Else
            For Each cblcItem As ListItem In cblTaylorNursestataions.Items
                cblcItem.Selected = False
                'cblItem.Enabled = False
            Next

        End If
    End Sub

    Protected Sub cblSAll_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cblSAll.SelectedIndexChanged

        If cblSAll.SelectedValue = "springall" Then

            For Each cblcItem As ListItem In cblSpringfieldNursestataions.Items
                cblcItem.Selected = True
                'cblItem.Enabled = False
            Next

        Else
            For Each cblcItem As ListItem In cblSpringfieldNursestataions.Items
                cblcItem.Selected = False
                'cblItem.Enabled = False
            Next

        End If
    End Sub


    Protected Sub gvSearch_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvSearch.RowCommand

        If e.CommandName = "Select" Then

            Dim client_num As String = gvSearch.DataKeys(Convert.ToInt32(e.CommandArgument))("client_num").ToString()
            Dim login_name As String = gvSearch.DataKeys(Convert.ToInt32(e.CommandArgument))("login_name").ToString()

            Dim dbAccess As New CkhsEmployeeDbAccess(Session("EmployeeConn"))
            Dim ckhsEmployee = dbAccess.GetCkhsEmployee(client_num)

            'c3Controller = New Client3Controller(client_num)


            SubmittingOnBehalfOfNameLabel.Text = ckhsEmployee.FullName & " (" & ckhsEmployee.EMail & ")"
            requestor_client_numLabel.Text = client_num
            SelectedChgRequestor.Text = client_num

            SearchFirstNameTextBox.Text = ""
            SearchLastNameTextBox.Text = ""

            gvSearch.Dispose()
            gvSearch.DataBind()
            gvSearch.SelectedIndex = -1

            ' unlock 2015
            'Select Case userDepartmentCD.Text
            '    Case "832600", "832700", "823100"
            '        If Session("SecurityLevelNum") < 90 And requestor_client_numLabel.Text = submitter_client_numLabel.Text Then
            '            btnSelectOnBehalfOf.Visible = True
            '            lblValidation.Text = "Must Change Requestor"
            '            lblValidation.Focus()
            '        ElseIf Session("SecurityLevelNum") < 90 And requestor_client_numLabel.Text <> submitter_client_numLabel.Text Then
            '            lblValidation.Text = ""
            '            btnSelectOnBehalfOf.BackColor() = Color.FromKnownColor("336666")
            '            btnSelectOnBehalfOf.ForeColor() = Color.White
            '            btnSelectOnBehalfOf.BorderStyle() = BorderStyle.Groove
            '            btnSelectOnBehalfOf.BorderWidth = Unit.Pixel(1)



            '        End If


            'End Select

            pnlOnBehalfOf.Visible = False

            Response.Redirect("./ClientDemo.aspx?ClientNum=" & iClientNum & "&RequestorNum=" & requestor_client_numLabel.Text & "&RequestorName=" & SubmittingOnBehalfOfNameLabel.Text, True)
            'btnUpdateSubmitter.Visible = True


        End If
    End Sub
    Protected Sub entitycdDDL_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles entitycdDDL.SelectedIndexChanged
        entitycdLabel.Text = entitycdDDL.SelectedValue

        Dim ddbDepartmentBinder As New DropDownListBinder(departmentcdddl, "selDepartmentCodesV4", Session("EmployeeConn"))
        ddbDepartmentBinder.AddDDLCriteria("@entity_cd", entitycdDDL.SelectedValue, SqlDbType.NVarChar)
        ddbDepartmentBinder.BindData("department_cd", "department_name")
        departmentcdLabel.Text = ""

    End Sub
    Protected Sub departmentcdddl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles departmentcdddl.SelectedIndexChanged
        departmentcdLabel.Text = departmentcdddl.SelectedValue

    End Sub
    Protected Sub facilitycdddl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles facilitycdddl.SelectedIndexChanged
        Dim ddlBinder As DropDownListBinder

        facilitycdLabel.Text = facilitycdddl.SelectedValue


        ddlBinder = New DropDownListBinder(buildingcdddl, "sel_building_by_facility", Session("EmployeeConn"))
        ddlBinder.AddDDLCriteria("@facility_cd", facilitycdddl.SelectedValue, SqlDbType.NVarChar)
        ddlBinder.BindData("building_cd", "building_name")

        Dim ddlFloorBinder As DropDownListBinder

        ddlFloorBinder = New DropDownListBinder(Floorddl, "SelFloorsByBuilding", Session("EmployeeConn"))
        ddlFloorBinder.AddDDLCriteria("@facility_cd", facilitycdddl.SelectedValue, SqlDbType.NVarChar)
        ddlFloorBinder.AddDDLCriteria("@building_cd", buildingcdddl.SelectedValue, SqlDbType.NVarChar)

        ddlFloorBinder.BindData("floor_no", "floor_no")

    End Sub
    Protected Sub buildingcdddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles buildingcdddl.SelectedIndexChanged
        buildingcdTextBox.Text = buildingcdddl.SelectedValue
        buildingnameLabel.Text = buildingcdddl.SelectedItem.Text

        Dim ddlFloorBinder As DropDownListBinder

        ddlFloorBinder = New DropDownListBinder(Floorddl, "SelFloorsByBuilding", Session("EmployeeConn"))
        ddlFloorBinder.AddDDLCriteria("@facility_cd", facilitycdddl.SelectedValue, SqlDbType.NVarChar)
        ddlFloorBinder.AddDDLCriteria("@building_cd", buildingcdddl.SelectedValue, SqlDbType.NVarChar)

        ddlFloorBinder.BindData("floor_no", "floor_no")

    End Sub
    Protected Sub Floorddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles Floorddl.SelectedIndexChanged
        FloorTextBox.Text = Floorddl.SelectedValue

    End Sub
    'Protected Sub btnNewTicketRequest_Click(sender As Object, e As System.EventArgs) Handles btnNewTicketRequest.Click
    '    'Response.Redirect("~/" & "NewRequestTicket.aspx?ClientNum=" & iClientNum, True)


    'End Sub

    Protected Sub btnPrintClient_Click(sender As Object, e As System.EventArgs) Handles btnPrintClient.Click
        Response.Redirect("~/PrintClient.aspx?clientnum=" & iClientNum)

        'Response.Redirect("~/Reports/RolesReport.aspx?ReportName=ClientReport" & "&GroupNum=0" & "&clientnum=" & iClientNum)
        ' Response.Redirect("~/Reports/ClientInfoRpt.aspx?clientnum=" & iClientNum)

    End Sub
    Protected Sub SendProviderEmail(ByVal AccountRequestNum As Integer)
        '======================
        ' Get current path
        '======================

        Dim path As String
        Dim directory As String = ""
        Dim toList As String()

        path = HttpContext.Current.Request.Url.AbsoluteUri
        Dim pathParts() As String = Split(path, "/")
        Dim i As Integer

        Dim EmailList As String


        Do While i < pathParts.Length() - 1

            directory = directory + pathParts(i) + "/"
            i = i + 1

        Loop

        Mailmsg.To.Clear()


        'Dim toEmail As String = EmailAddress.Email
        Dim sSubject As String
        Dim sBody As String
        Dim fulleMailList As String

        sSubject = "New Account has been ADDED for Current Client " & firstnameTextBox.Text & " " & lastnameTextBox.Text


        sBody = "<!DOCTYPE html>" & _
                            "<html>" & _
                            "<head>" & _
                            "<style>" & _
                            "table" & _
                            "{" & _
                            "border-collapse:collapse;" & _
                            "font-family: 'Helvetica Neue', 'Lucida Grande', 'Segoe UI', Arial, Helvetica, Verdana, sans-serif;" & _
                            "}" & _
                            "table, th, td" & _
                            "{" & _
                            "border: 1px solid black;" & _
                            "padding: 3px" & _
                            "}" & _
                            "</style>" & _
                            "</head>" & _
                            "" & _
                            "<body>" & _
                            "A new Account has been Added for a current Client.  Follow the link below to go to view details of the request." & _
                             "<br />" & _
                            "<a href=""" & directory & "/AccountRequestInfo2.aspx?requestnum=" & AccountRequestNum & """ ><u><b>Name on Accounts: " & firstnameTextBox.Text & " " & lastnameTextBox.Text & "</b></u></a>" & _
                            "</body>" & _
                        "</html>"


        EmailList = New EmailListOne(AccountRequestNum, Session("EmployeeConn")).EmailListReturn

        fulleMailList = EmailList
        If Session("environment").ToString.ToLower = "testing" Then
            EmailList = "Jeff.Mele@crozer.org"
            sBody = sBody & fulleMailList

        End If

        toList = EmailList.Split(";")
        If toList.Length = 1 Then
            Mailmsg.To.Add(New System.Net.Mail.MailAddress(toList(0)))
        Else
        End If
        For i = 0 To toList.Length - 1 ' - 2
            If toList(i).Length > 10 Then ' validity check
                Mailmsg.To.Add(New System.Net.Mail.MailAddress(toList(i)))
            End If
        Next

        'Dim mailObj As New MailMessage
        Mailmsg.From = New MailAddress("CSC@crozer.org")
        Mailmsg.IsBodyHtml = True

        Mailmsg.Subject = sSubject
        Mailmsg.Body = sBody

        Dim smtpClient As New SmtpClient()
        smtpClient.Host = "ah1smtprelay01.altahospitals.com"

        smtpClient.Send(Mailmsg)

    End Sub

    Protected Sub fillClosedRequestTable()
        Dim thisData As New CommandsSqlAndOleDb("SelClosedRequestByClient", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@client_num", iClientNum, SqlDbType.NVarChar)

        Dim iRowCount = 1

        Dim dt As DataTable
        dt = thisData.GetSqlDataTable

        Dim rwHeader As New TableRow

        Dim clRequestHeader As New TableCell
        Dim clReceiverNameHeader As New TableCell
        Dim clRequestorNameHeader As New TableCell
        Dim clSubmitterNameHeader As New TableCell
        Dim clReceiverHeader As New TableCell
        Dim clRequestTypeHeader As New TableCell

        Dim cldateHeader As New TableCell

        'account_request_num int null,
        'ReceiverName nvarchar(150) null,
        'RequestorName nvarchar(150) null,
        'SubmitterName  nvarchar(150) null,
        'Receiver nvarchar(25) null,
        'entered_date datetime null)


        clRequestHeader.Text = "Req#"
        clReceiverNameHeader.Text = "Receiver"
        clRequestorNameHeader.Text = "Requestor"
        clSubmitterNameHeader.Text = "Submitter"
        clReceiverHeader.Text = "Who was"
        clRequestTypeHeader.Text = "Type"
        cldateHeader.Text = "Date"


        rwHeader.Cells.Add(clRequestHeader)
        rwHeader.Cells.Add(clReceiverNameHeader)
        rwHeader.Cells.Add(clRequestorNameHeader)

        rwHeader.Cells.Add(clSubmitterNameHeader)
        rwHeader.Cells.Add(clReceiverHeader)
        rwHeader.Cells.Add(clRequestTypeHeader)

        rwHeader.Cells.Add(cldateHeader)


        rwHeader.Font.Bold = True
        rwHeader.BackColor = Color.FromName("#006666")
        rwHeader.ForeColor = Color.FromName("White")
        rwHeader.Height = Unit.Pixel(36)

        tblClosedRequest.Rows.Add(rwHeader)

        For Each drRow As DataRow In dt.Rows


            Dim rwData As New TableRow
            Dim btnSelect As New Button

            'Dim clRequestData As New TableCell
            Dim clSelectData As New TableCell
            Dim clReceiverNameData As New TableCell
            Dim clRequestorNameData As New TableCell
            Dim clSubmitterNameData As New TableCell
            Dim clReceiverData As New TableCell
            Dim clTypeData As New TableCell

            Dim cldateData As New TableCell

            btnSelect.Text = "Select"
            btnSelect.CommandName = "Select"
            btnSelect.CommandArgument = drRow("account_request_num")
            ' btnSelect.Width = 50
            'account_request_type

            btnSelect.EnableTheming = False

            AddHandler btnSelect.Click, AddressOf btnSelect_Click

            clReceiverNameData.Text = IIf(IsDBNull(drRow("ReceiverName")), "", drRow("ReceiverName"))
            clRequestorNameData.Text = IIf(IsDBNull(drRow("RequestorName")), "", drRow("RequestorName"))
            clSubmitterNameData.Text = IIf(IsDBNull(drRow("SubmitterName")), "", drRow("SubmitterName"))

            clReceiverData.Text = IIf(IsDBNull(drRow("Receiver")), "", drRow("Receiver"))
            clTypeData.Text = IIf(IsDBNull(drRow("account_request_type")), "", drRow("account_request_type"))

            cldateData.Text = IIf(IsDBNull(drRow("entered_date")), "", drRow("entered_date"))

            'add button to grid
            clSelectData.Controls.Add(btnSelect)

            rwData.Cells.Add(clSelectData)
            rwData.Cells.Add(clReceiverNameData)
            rwData.Cells.Add(clRequestorNameData)
            rwData.Cells.Add(clSubmitterNameData)

            rwData.Cells.Add(clReceiverData)
            rwData.Cells.Add(clTypeData)
            rwData.Cells.Add(cldateData)


            If iRowCount > 0 Then

                rwData.BackColor = Color.Bisque

            Else

                rwData.BackColor = Color.LightGray

            End If

            tblClosedRequest.Rows.Add(rwData)

            iRowCount = iRowCount * -1

        Next



        tblClosedRequest.Font.Name = "Arial"
        tblClosedRequest.Font.Size = 8

        tblClosedRequest.CellPadding = 10
        tblClosedRequest.Width = New Unit("100%")


    End Sub
    Protected Sub FillCurrentAccounts()

        Dim thisData As New CommandsSqlAndOleDb("SelAllClientAccountsByNumberV8", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@client_num", iClientNum, SqlDbType.NVarChar)

        Dim iRowCount = 1

        Dim dt As DataTable
        dt = thisData.GetSqlDataTable
        AccountCount = dt.Rows.Count


        Dim rwHeader As New TableRow

        Dim clApplicationDescHeader As New TableCell
        Dim clLoginNameHeader As New TableCell
        Dim clTermDateHeader As New TableCell
        Dim clCreateDateHeader As New TableCell
        Dim clCreatedByHeader As New TableCell


        clApplicationDescHeader.Text = "Application#"
        clLoginNameHeader.Text = "Account Name"

        clCreatedByHeader.Text = "Created By"
        clCreateDateHeader.Text = "Create Date"

        clTermDateHeader.Text = "Term. Date"


        rwHeader.Cells.Add(clApplicationDescHeader)
        rwHeader.Cells.Add(clLoginNameHeader)

        rwHeader.Cells.Add(clCreatedByHeader)
        rwHeader.Cells.Add(clCreateDateHeader)

        rwHeader.Cells.Add(clTermDateHeader)



        rwHeader.Font.Bold = True
        rwHeader.BackColor = Color.FromName("#006666")
        rwHeader.ForeColor = Color.FromName("White")
        rwHeader.Height = Unit.Pixel(36)

        tblCurrentAccounts.Rows.Add(rwHeader)

        'ApplicationNum
        'ApplicationDesc 
        'login_name 
        'acct_term_date
        'create_date
        'CreatedBy 
        'AppBase

        For Each drRow As DataRow In dt.Rows


            Dim rwData As New TableRow

            Dim clApplicationDesc As New TableCell
            Dim clLoginName As New TableCell

            Dim clCreatedBy As New TableCell
            Dim clCreateDate As New TableCell

            Dim clTermDate As New TableCell



            clApplicationDesc.Text = IIf(IsDBNull(drRow("ApplicationDesc")), "", drRow("ApplicationDesc"))
            clLoginName.Text = IIf(IsDBNull(drRow("login_name")), "", drRow("login_name"))
            clTermDate.Text = IIf(IsDBNull(drRow("acct_term_date")), "", drRow("acct_term_date"))

            clCreateDate.Text = IIf(IsDBNull(drRow("create_date")), "", drRow("create_date"))
            clCreatedBy.Text = IIf(IsDBNull(drRow("CreatedBy")), "", drRow("CreatedBy"))

            If clApplicationDesc.Text.ToString.ToLower.IndexOf("ecare") = 0 Then
                ecareexists.Text = "yes"
            End If


            rwData.Cells.Add(clApplicationDesc)
            rwData.Cells.Add(clLoginName)

            rwData.Cells.Add(clCreatedBy)
            rwData.Cells.Add(clCreateDate)

            rwData.Cells.Add(clTermDate)



            If iRowCount > 0 Then

                rwData.BackColor = Color.Bisque

            Else

                rwData.BackColor = Color.LightGray

            End If

            tblCurrentAccounts.Rows.Add(rwData)

            iRowCount = iRowCount * -1

        Next

        totAcct = tblCurrentAccounts.Rows.Count()

        tblCurrentAccounts.Font.Name = "Arial"
        tblCurrentAccounts.Font.Size = 8

        tblCurrentAccounts.CellPadding = 10
        tblCurrentAccounts.Width = New Unit("100%")

    End Sub
    Protected Sub fillOpenRequestTable()
        Dim thisData As New CommandsSqlAndOleDb("SelOpenRequestByClient", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@client_num", iClientNum, SqlDbType.NVarChar)

        Dim iRowCount = 1

        Dim dt As DataTable
        dt = thisData.GetSqlDataTable

        OpenRequestCount = dt.Rows.Count

        Dim rwHeader As New TableRow

        Dim clRequestHeader As New TableCell
        Dim clReceiverNameHeader As New TableCell
        Dim clRequestorNameHeader As New TableCell
        Dim clSubmitterNameHeader As New TableCell
        Dim clReceiverHeader As New TableCell
        Dim clRequestTypeHeader As New TableCell

        Dim cldateHeader As New TableCell

        'account_request_num int null,
        'ReceiverName nvarchar(150) null,
        'RequestorName nvarchar(150) null,
        'SubmitterName  nvarchar(150) null,
        'Receiver nvarchar(25) null,
        'entered_date datetime null)


        clRequestHeader.Text = "Req#"
        clReceiverNameHeader.Text = "Receiver"
        clRequestorNameHeader.Text = "Requestor"
        clSubmitterNameHeader.Text = "Submitter"
        clReceiverHeader.Text = "Who was"
        clRequestTypeHeader.Text = "Type"

        cldateHeader.Text = "Date"


        rwHeader.Cells.Add(clRequestHeader)
        rwHeader.Cells.Add(clReceiverNameHeader)
        rwHeader.Cells.Add(clRequestorNameHeader)

        rwHeader.Cells.Add(clSubmitterNameHeader)
        rwHeader.Cells.Add(clReceiverHeader)
        rwHeader.Cells.Add(clRequestTypeHeader)

        rwHeader.Cells.Add(cldateHeader)


        rwHeader.Font.Bold = True
        rwHeader.BackColor = Color.FromName("#006666")
        rwHeader.ForeColor = Color.FromName("White")
        rwHeader.Height = Unit.Pixel(36)

        tblOpenRequests.Rows.Add(rwHeader)

        For Each drRow As DataRow In dt.Rows


            Dim rwData As New TableRow
            Dim btnSelect As New Button

            'Dim clRequestData As New TableCell
            Dim clSelectData As New TableCell
            Dim clReceiverNameData As New TableCell
            Dim clRequestorNameData As New TableCell
            Dim clSubmitterNameData As New TableCell
            Dim clReceiverData As New TableCell
            Dim clTypeData As New TableCell
            Dim cldateData As New TableCell

            btnSelect.Text = "Select"
            btnSelect.CommandName = "Select"
            btnSelect.CommandArgument = drRow("account_request_num")
            ' btnSelect.Width = 50
            btnSelect.EnableTheming = False

            AddHandler btnSelect.Click, AddressOf btnSelect_Click

            clReceiverNameData.Text = IIf(IsDBNull(drRow("ReceiverName")), "", drRow("ReceiverName"))
            clRequestorNameData.Text = IIf(IsDBNull(drRow("RequestorName")), "", drRow("RequestorName"))
            clSubmitterNameData.Text = IIf(IsDBNull(drRow("SubmitterName")), "", drRow("SubmitterName"))

            clReceiverData.Text = IIf(IsDBNull(drRow("Receiver")), "", drRow("Receiver"))
            clTypeData.Text = IIf(IsDBNull(drRow("account_request_type")), "", drRow("account_request_type"))

            cldateData.Text = IIf(IsDBNull(drRow("entered_date")), "", drRow("entered_date"))

            'add button to grid
            clSelectData.Controls.Add(btnSelect)

            rwData.Cells.Add(clSelectData)
            rwData.Cells.Add(clReceiverNameData)
            rwData.Cells.Add(clRequestorNameData)
            rwData.Cells.Add(clSubmitterNameData)

            rwData.Cells.Add(clReceiverData)
            rwData.Cells.Add(clTypeData)

            rwData.Cells.Add(cldateData)


            If iRowCount > 0 Then

                rwData.BackColor = Color.Bisque

            Else

                rwData.BackColor = Color.LightGray

            End If

            tblOpenRequests.Rows.Add(rwData)

            iRowCount = iRowCount * -1

        Next
        tblOpenRequests.Font.Name = "Arial"
        tblOpenRequests.Font.Size = 8

        tblOpenRequests.CellPadding = 10
        tblOpenRequests.Width = New Unit("100%")


    End Sub

    Protected Sub fillRequestItemsTable()
        Dim thisData As New CommandsSqlAndOleDb("SelRFSTicketsByClient", Session("CSCConn"))
        thisData.AddSqlProcParameter("@clientnum", iClientNum, SqlDbType.Int)
        'thisData.AddSqlProcParameter("@status", "open", SqlDbType.NVarChar)

        Dim iRowCount = 1

        Dim dt As DataTable
        dt = thisData.GetSqlDataTable

        Dim rwHeader As New TableRow

        Dim clBtnheader As New TableCell
        Dim clRequestTypeHeader As New TableCell
        Dim clDateEnteredHeader As New TableCell
        Dim clStatusHeader As New TableCell
        Dim clDescheader As New TableCell

        clBtnheader.Text = ""
        clRequestTypeHeader.Text = "Type"
        clDateEnteredHeader.Text = "Date Entered"
        clStatusHeader.Text = "Status"
        clDescheader.Text = "Description"


        rwHeader.Cells.Add(clBtnheader)
        rwHeader.Cells.Add(clRequestTypeHeader)
        rwHeader.Cells.Add(clDateEnteredHeader)
        rwHeader.Cells.Add(clStatusHeader)
        rwHeader.Cells.Add(clDescheader)


        rwHeader.Font.Bold = True
        rwHeader.BackColor = Color.FromName("#006666")
        rwHeader.ForeColor = Color.FromName("White")
        rwHeader.Height = Unit.Pixel(36)

        tblRFSTickets.Rows.Add(rwHeader)

        For Each drRow As DataRow In dt.Rows


            Dim rwData As New TableRow

            Dim clBtn As New TableCell

            Dim clReqNum As New TableCell

            Dim claccountRequestNum As New TableCell
            Dim clRequestTypeData As New TableCell
            Dim clDateEntered As New TableCell
            Dim clStatusData As New TableCell
            Dim clDescData As New TableCell

            claccountRequestNum.Text = IIf(IsDBNull(drRow("RequestNum")), "", drRow("RequestNum"))

            clRequestTypeData.Text = IIf(IsDBNull(drRow("RequestType")), "", drRow("RequestType"))
            clDateEntered.Text = IIf(IsDBNull(drRow("EntryDate")), "", drRow("EntryDate"))

            clStatusData.Text = IIf(IsDBNull(drRow("CurrStatus")), "", drRow("CurrStatus"))
            clDescData.Text = IIf(IsDBNull(drRow("ReqDesc")), "", drRow("ReqDesc"))
            clReqNum.Text = "<a href=""ViewRequestTicket.aspx?RequestNum=" & claccountRequestNum.Text & """ ><u><b> " & claccountRequestNum.Text & "</b></u></a>"

            'If clRequestTypeData.Text.ToLower = "ticket" Then
            '    clReqNum.Text = "<a href=""ViewRequestTicket.aspx?RequestNum=" & claccountRequestNum.Text & """ ><u><b> " & claccountRequestNum.Text & "</b></u></a>"
            'Else
            '    clReqNum.Text = "<a href=""http://intranet01/csc/update_rfs.asp?id=" & claccountRequestNum.Text & """ ><u><b> " & claccountRequestNum.Text & "</b></u></a>"
            'End If

            If Session("SecurityLevelNum") < 70 Then
                If Session("SecurityLevelNum") <> 60 Then
                    clReqNum.Text = claccountRequestNum.Text
                End If
            End If

            rwData.Cells.Add(clReqNum)
            rwData.Cells.Add(clRequestTypeData)

            rwData.Cells.Add(clDateEntered)
            rwData.Cells.Add(clStatusData)
            rwData.Cells.Add(clDescData)


            If iRowCount > 0 Then

                rwData.BackColor = Color.Bisque

            Else

                rwData.BackColor = Color.LightGray

            End If

            tblRFSTickets.Rows.Add(rwData)

            iRowCount = iRowCount * -1

        Next
        tblRFSTickets.CellPadding = 10
        tblRFSTickets.Width = New Unit("100%")


    End Sub
    Protected Sub fillCPMClasseTable()
        'Dim thisData As New CommandsSqlAndOleDb("SelCpmClassesByClient", Session("EmployeeConn"))
        'thisData.AddSqlProcParameter("@client_num", iClientNum, SqlDbType.Int)
        ''thisData.AddSqlProcParameter("@status", "open", SqlDbType.NVarChar)

        'Dim iRowCount = 1

        'Dim dt As DataTable
        'dt = thisData.GetSqlDataTable

        'Dim rwHeader As New TableRow

        'Dim clBtnheader As New TableCell
        'Dim clRequestTypeHeader As New TableCell
        'Dim clDateEnteredHeader As New TableCell
        'Dim clStatusHeader As New TableCell
        'Dim clDescheader As New TableCell

        'clBtnheader.Text = ""
        'clRequestTypeHeader.Text = "Class Desc"
        'clDateEnteredHeader.Text = "Date Entered"
        'clStatusHeader.Text = "Status"
        'clDescheader.Text = "Description"


        ''rwHeader.Cells.Add(clBtnheader)
        'rwHeader.Cells.Add(clRequestTypeHeader)
        'rwHeader.Cells.Add(clDateEnteredHeader)
        'rwHeader.Cells.Add(clStatusHeader)
        'rwHeader.Cells.Add(clDescheader)


        'rwHeader.Font.Bold = True
        'rwHeader.BackColor = Color.FromName("#006666")
        'rwHeader.ForeColor = Color.FromName("White")
        'rwHeader.Height = Unit.Pixel(36)

        'tblCPMClasses.Rows.Add(rwHeader)

        'For Each drRow As DataRow In dt.Rows


        '    Dim rwData As New TableRow

        '    Dim clBtn As New TableCell

        '    Dim clReqNum As New TableCell

        '    Dim claccountRequestNum As New TableCell
        '    Dim clRequestTypeData As New TableCell
        '    Dim clDateEntered As New TableCell
        '    Dim clStatusData As New TableCell
        '    Dim clDescData As New TableCell
        '    Dim clCreateDate As New TableCell

        '    clCreateDate.Text = IIf(IsDBNull(drRow("created_Date")), "", drRow("created_Date"))

        '    claccountRequestNum.Text = IIf(IsDBNull(drRow("account_request_num")), "", drRow("account_request_num"))

        '    clRequestTypeData.Text = IIf(IsDBNull(drRow("CPMTraindesc")), "", drRow("CPMTraindesc"))
        '    clDateEntered.Text = IIf(IsDBNull(drRow("Requested_Date")), "", drRow("Requested_Date"))

        '    clStatusData.Text = IIf(IsDBNull(drRow("CPMTrainStatus")), "", drRow("CPMTrainStatus"))
        '    clDescData.Text = IIf(IsDBNull(drRow("TrainDesc")), "", drRow("TrainDesc"))
        '    'clReqNum.Text = "<a href=""ViewRequestTicket.aspx?RequestNum=" & claccountRequestNum.Text & """ ><u><b> " & claccountRequestNum.Text & "</b></u></a>"

        '    'If clCreateDate.Text.ToLower <> "" Then
        '    '    clReqNum.Text = "<a href=""ViewRequestTicket.aspx?RequestNum=" & claccountRequestNum.Text & """ ><u><b> " & claccountRequestNum.Text & "</b></u></a>"
        '    'Else
        '    '    clReqNum.Text = "<a href=""http://intranet01/csc/update_rfs.asp?id=" & claccountRequestNum.Text & """ ><u><b> " & claccountRequestNum.Text & "</b></u></a>"
        '    'End If

        '    'If Session("SecurityLevelNum") < 70 Then
        '    '    clReqNum.Text = claccountRequestNum.Text

        '    'End If

        '    'rwData.Cells.Add(clReqNum)
        '    rwData.Cells.Add(clRequestTypeData)

        '    rwData.Cells.Add(clDateEntered)
        '    rwData.Cells.Add(clStatusData)
        '    rwData.Cells.Add(clDescData)


        '    If iRowCount > 0 Then

        '        rwData.BackColor = Color.Bisque

        '    Else

        '        rwData.BackColor = Color.LightGray

        '    End If

        '    tblCPMClasses.Rows.Add(rwData)

        '    iRowCount = iRowCount * -1

        'Next
        'tblCPMClasses.CellPadding = 10
        'tblCPMClasses.Width = New Unit("100%")



    End Sub
    Sub btnSelect_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim btnLinkView As Button = sender

        If btnLinkView.CommandName = "Select" Then
            Dim sArguments As String() = Split(btnLinkView.CommandArgument, ";")

            Response.Redirect("./AccountRequestInfo2.aspx?RequestNum=" & sArguments(0))

            '            Response.Redirect("./OpenProviderRequestItems.aspx?RequestNum=" & sArguments(0))

        End If
    End Sub
    Private Sub CheckAffiliation()

        'pnlAlliedHealth.Visible = False


        HDemployeetype.Text = emptypecdrbl.SelectedValue


        Select Case emptypecdrbl.SelectedValue
            Case "1193", "1200"
                'Case "employee"

                Dim firstNameReq As New RequiredField(firstnameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(lastnameTextBox, "Last Name")



                'pnlAlliedHealth.Visible = False

                pnlProvider.Visible = False
                'providerrbl.SelectedIndex = -1

                'Select Case PositionRoleNumLabel.Text
                '    Case "1567"
                '        Dim startdatereq As New RequiredField(startdateTextBox, "Start Date")

                '        Dim ViewCPMModelreq As New RequiredField(CPMEmployeesddl, "CPM Model")

                '        'CPMEmployeesddl
                '        CPMEmployeesddl.BackColor() = Color.Yellow
                '        CPMEmployeesddl.BorderColor() = Color.Black
                '        CPMEmployeesddl.BorderStyle() = BorderStyle.Solid
                '        CPMEmployeesddl.BorderWidth = Unit.Pixel(2)

                '        PhysOfficeddl.BackColor() = Color.Yellow
                '        PhysOfficeddl.BorderColor() = Color.Black
                '        PhysOfficeddl.BorderStyle() = BorderStyle.Solid
                '        PhysOfficeddl.BorderWidth = Unit.Pixel(2)
                '    Case Else

                'End Select

            Case "1189"
                'Case "student"

                Dim firstNameReq As New RequiredField(firstnameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(lastnameTextBox, "Last Name")
                Dim titleReq As New RequiredField(Titleddl, "Title")
                Dim specReq As New RequiredField(Specialtyddl, "Specialty")
                Dim add1Req As New RequiredField(address1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim faxqReq As New RequiredField(faxtextbox, "Fax")


                'pnlAlliedHealth.Visible = False

                pnlProvider.Visible = False
                'providerrbl.SelectedIndex = -1

            Case "782"
                'Case "physician"
                Dim firstNameReq As New RequiredField(firstnameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(lastnameTextBox, "Last Name")
                Dim titleReq As New RequiredField(Titleddl, "Title")
                Dim specReq As New RequiredField(Specialtyddl, "Specialty")
                Dim add1Req As New RequiredField(address1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim faxqReq As New RequiredField(faxtextbox, "Fax")


                'pnlAlliedHealth.Visible = False

                pnlProvider.Visible = True
                'providerrbl.SelectedValue = "Yes"

                'If hanrbl.SelectedValue.ToString = "Yes" Then

                '    Dim ViewCPMModelreq As New RequiredField(CPMEmployeesddl, "CPM Model")


                '    Dim npiReq As New RequiredField(npitextbox, "NPI")
                '    Dim LicensesNoReq As New RequiredField(LicensesNoTextBox, "Licenses No")
                '    Dim taxonomyreq As New RequiredField(taxonomytextbox, "Taxonomy")
                '    Dim startdatereq As New RequiredField(startdateTextBox, "Start Date")
                '    Dim Schedulablereq As New RequiredField(Schedulablerbl, "Schedule")
                '    Dim Televoxrblreq As New RequiredField(Televoxrbl, "Televox")
                'End If


                'npitextbox
                'LicensesNoTextBox
                'taxonomytextbox
                'startdateTextBox
                'Schedulablerbl
                'Televoxrbl


            Case "1197"

                'Case "non staff clinician"
                Dim firstNameReq As New RequiredField(firstnameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(lastnameTextBox, "Last Name")
                Dim titleReq As New RequiredField(Titleddl, "Title")

                Dim specReq As New RequiredField(Specialtyddl, "Specialty")
                Dim add1Req As New RequiredField(address1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim faxqReq As New RequiredField(faxtextbox, "Fax")


                ' pnlAlliedHealth.Visible = False

                'providerrbl.SelectedIndex = -1
                pnlProvider.Visible = True
            Case "1194"
                'Case "resident"
                Dim firstNameReq As New RequiredField(firstnameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(lastnameTextBox, "Last Name")
                Dim titleReq As New RequiredField(Titleddl, "Title")

                Dim specReq As New RequiredField(Specialtyddl, "Specialty")
                Dim add1Req As New RequiredField(address1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim faxqReq As New RequiredField(faxtextbox, "Fax")


                ' pnlAlliedHealth.Visible = False

                'providerrbl.SelectedValue = "Yes"
                pnlProvider.Visible = True
                'If hanrbl.SelectedValue.ToString = "Yes" Then

                '    Dim ViewCPMModelreq As New RequiredField(CPMEmployeesddl, "CPM Model")

                '    Dim npiReq As New RequiredField(npitextbox, "NPI")
                '    Dim LicensesNoReq As New RequiredField(LicensesNoTextBox, "Licenses No")
                '    Dim taxonomyreq As New RequiredField(taxonomytextbox, "Taxonomy")
                '    Dim startdatereq As New RequiredField(startdateTextBox, "Start Date")
                '    Dim Schedulablereq As New RequiredField(Schedulablerbl, "Schedule")
                '    Dim Televoxrblreq As New RequiredField(Televoxrbl, "Televox")
                'End If


            Case "1198"
                ' Case "vendor"

                Dim firstNameReq As New RequiredField(firstnameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(lastnameTextBox, "Last Name")
                Dim add1Req As New RequiredField(address1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim vendorRequired As New RequiredField(VendorNameTextBox, "Vendor")


                'pnlAlliedHealth.Visible = False
                pnlProvider.Visible = False
                'providerrbl.SelectedIndex = -1

            Case "1195"
                'Case "contract"

                Dim firstNameReq As New RequiredField(firstnameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(lastnameTextBox, "Last Name")

                Dim add1Req As New RequiredField(address1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")


                'pnlAlliedHealth.Visible = False

                pnlProvider.Visible = False
                'providerrbl.SelectedIndex = -1



            Case "1196"
                'Case "other"

                Dim firstNameReq As New RequiredField(firstnameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(lastnameTextBox, "Last Name")

                Dim add1Req As New RequiredField(address1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")


                'pnlAlliedHealth.Visible = False

                pnlProvider.Visible = False
                'providerrbl.SelectedIndex = -1



            Case Else


                Dim firstNameReq As New RequiredField(firstnameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(lastnameTextBox, "Last Name")


                ' pnlAlliedHealth.Visible = False

                pnlProvider.Visible = False
                'providerrbl.SelectedIndex = -1

                pnlProvider.Visible = False
                ' providerrbl.SelectedIndex = -1



        End Select


    End Sub
    Protected Sub emptypecdrbl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles emptypecdrbl.SelectedIndexChanged

        pnlTCl.Visible = False
        gvTCL.Dispose()


        Dim removeCss As New PageControls(Page)
        removeCss.RemoveCssClass(Page, "textInputRequired")


        'AlliedHealthddl.SelectedIndex = -1
        emptypecdLabel.Text = emptypecdrbl.SelectedValue
        mainRole = emptypecdrbl.SelectedValue

        tpAddItems.Visible = False
        tpAddFinanItems.Visible = False

        'pnlAlliedHealth.Visible = False

        CheckRequiredFields()



        Select Case emptypecdrbl.SelectedValue
            Case "782", "1197"
                'Case "physician", "non staff clinician", "resident", "allied health"

                If Session("SecurityLevelNum") < 90 Then
                    Dim ctrl As Control = DataCheckingAndValidation.SetChildControlsRecursiveToEnabledFalse(tpDemographics, False)
                Else
                    DoctorMasterNumTextBox.Enabled = True
                End If

                'If Session("SecurityLevelNum") = 82 Or Session("LoginID") = "melej" Or Session("LoginID") = "RalphC" Then
                '    credentialedDateTextBox.Enabled = True
                '    CredentialedDateDCMHTextBox.Enabled = True

                '    credentialedDateTextBox.CssClass = "calenderClass"
                '    CredentialedDateDCMHTextBox.CssClass = "calenderClass"

                'End If
                Title2Label.Visible = False
                Titleddl.Visible = True

                SpecialtyLabel.Text = ""
                SpecialtyLabel.Visible = False
                Specialtyddl.Visible = True


                If Titleddl.SelectedIndex <= 0 Then

                    Titleddl.Visible = True
                    Dim ddbTitleBinder As New DropDownListBinder(Titleddl, "SelTitlesV4", Session("EmployeeConn"))
                    ddbTitleBinder.AddDDLCriteria("@titleType", "Physician", SqlDbType.NVarChar)
                    ddbTitleBinder.BindData("TitleShortDesc", "TitleDesc")

                    'FillAffiliations()

                    If TitleLabel.Text = "" Then
                        Titleddl.SelectedIndex = -1
                    Else
                        Titleddl.SelectedValue = TitleLabel.Text

                    End If

                End If



                If SpecialtyLabel.Text = "" Then
                    Specialtyddl.SelectedIndex = -1
                Else
                    Specialtyddl.SelectedValue = SpecialtyLabel.Text

                End If

            Case "1194"
                If Session("SecurityLevelNum") < 90 Then
                    Dim ctrl As Control = DataCheckingAndValidation.SetChildControlsRecursiveToEnabledFalse(tpDemographics, False)
                Else
                    DoctorMasterNumTextBox.Enabled = True
                End If

                credentialedDateTextBox.Enabled = False
                credentialedDateTextBox.CssClass = "style2"

                'CredentialedDateDCMHTextBox.Enabled = False
                'CredentialedDateDCMHTextBox.CssClass = "style2"

                CCMCadmitRightsrbl.SelectedValue = "No"
                CCMCadmitRightsrbl.Enabled = False

                'DCMHadmitRightsrbl.SelectedValue = "No"
                'DCMHadmitRightsrbl.Enabled = False

                thisRole = emptypecdrbl.SelectedValue
                mainRole = emptypecdrbl.SelectedValue

            Case "1195", "1198"
                ' load vendor list 
                'make vendor text box invisiable
                ' make ddl appear
                ' 1195 Contractor list
                ' 1198 Vendor list
                Vendorddl.Visible = True
                VendorNameTextBox.Visible = False

                Dim ddbVendorBinder As New DropDownListBinder(Vendorddl, "SelVendors", Session("EmployeeConn"))
                ddbVendorBinder.AddDDLCriteria("@type", "vendor", SqlDbType.NVarChar)
                ddbVendorBinder.BindData("VendorNum", "VendorName")
                thisRole = emptypecdrbl.SelectedValue
                mainRole = emptypecdrbl.SelectedValue

                'If emp_type_cdrbl.SelectedValue = "1198" Then
                '    ddbVendorBinder.AddDDLCriteria("@type", "vendor", SqlDbType.NVarChar)
                '    'lblVendor.Text = "Vendor Name:"

                'ElseIf emp_type_cdrbl.SelectedValue = "1195" Then

                '    ddbVendorBinder.AddDDLCriteria("@type", "contractor", SqlDbType.NVarChar)
                '    'lblVendor.Text = "Contractor Name:"
                'Else

                'End If


        End Select


        'srolenum = emp_type_cdrbl.SelectedItem
        Dim ddlBinderV2 As DropDownListBinder

		ddlBinderV2 = New DropDownListBinder(Rolesddl, "SelEmployeeRolesV3", Session("EmployeeConn"))

		ddlBinderV2.AddDDLCriteria("@RoleNum", mainRole, SqlDbType.NVarChar)
        ddlBinderV2.BindData("rolenum", "roledesc")

        Rolesddl.SelectedValue = mainRole
        Rolesddl_SelectedIndexChanged(e, System.EventArgs.Empty)



    End Sub
    Public Function CheckRequiredFields() As Boolean
        Dim removeCss As New PageControls(Page)
        removeCss.RemoveCssClass(Page, "textInputRequired")

        emptypecdLabel.Text = emptypecdrbl.SelectedValue
        Select Case emptypecdrbl.SelectedValue
            Case "1193", "1200"
                'Case "employee"

                Dim firstNameReq As New RequiredField(firstnameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(lastnameTextBox, "Last Name")

                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")

                Dim Location As New RequiredField(facilitycdddl, "Location")
                Dim building As New RequiredField(buildingcdddl, "building")

                facilitycdddl.BackColor() = Color.Yellow
                facilitycdddl.BorderColor() = Color.Black
                facilitycdddl.BorderStyle() = BorderStyle.Solid
                facilitycdddl.BorderWidth = Unit.Pixel(2)

                buildingcdddl.BackColor() = Color.Yellow
                buildingcdddl.BorderColor() = Color.Black
                buildingcdddl.BorderStyle() = BorderStyle.Solid
                buildingcdddl.BorderWidth = Unit.Pixel(2)



                ' pnlAlliedHealth.Visible = False
                pnlProvider.Visible = False
                'providerrbl.SelectedIndex = -1

                'lblVendor.Text = "Vendor Name:"
                Vendorddl.Visible = False
                VendorNameTextBox.Visible = True
                VendorNameTextBox.Enabled = False

                'Select Case PositionRoleNumLabel.Text
                '    Case "1567" ' CPM Employees
                '        Dim startdatereq As New RequiredField(startdateTextBox, "Start Date")

                '        Dim ViewCPMModelreq As New RequiredField(CPMEmployeesddl, "CPM Model")


                '        If educationListbox.SelectedIndex > -1 Then
                '            Dim EducationText As New RequiredField(educationDateTextBox, "Education Date")

                '        End If

                '        'CPMEmployeesddl
                '        'CPMEmployeesddl.BackColor() = Color.Yellow
                '        'CPMEmployeesddl.BorderColor() = Color.Black
                '        'CPMEmployeesddl.BorderStyle() = BorderStyle.Solid
                '        'CPMEmployeesddl.BorderWidth = Unit.Pixel(2)

                '        'PhysOfficeddl.BackColor() = Color.Yellow
                '        'PhysOfficeddl.BorderColor() = Color.Black
                '        'PhysOfficeddl.BorderStyle() = BorderStyle.Solid
                '        'PhysOfficeddl.BorderWidth = Unit.Pixel(2)
                '    Case Else

                'End Select

            Case "1189"
                ' Case "student"

                Dim firstNameReq As New RequiredField(firstnameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(lastnameTextBox, "Last Name")
                Dim titleReq As New RequiredField(Titleddl, "Title")
                Titleddl.BackColor() = Color.Yellow
                Titleddl.BorderColor() = Color.Black
                Titleddl.BorderStyle() = BorderStyle.Solid
                Titleddl.BorderWidth = Unit.Pixel(2)

                Dim specReq As New RequiredField(Specialtyddl, "Specialty")
                Specialtyddl.BackColor() = Color.Yellow
                Specialtyddl.BorderColor() = Color.Black
                Specialtyddl.BorderStyle() = BorderStyle.Solid
                Specialtyddl.BorderWidth = Unit.Pixel(2)

                Dim add1Req As New RequiredField(address1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim faxqReq As New RequiredField(faxtextbox, "Fax")


                'pnlAlliedHealth.Visible = False
                pnlProvider.Visible = False
                'providerrbl.SelectedIndex = -1
                Titleddl.Enabled = True

                'lblVendor.Text = "Vendor Name:"
                Vendorddl.Visible = False
                VendorNameTextBox.Visible = True
                VendorNameTextBox.Enabled = False

                'Dim locReq As New RequiredField(facility_cdddl, "Location")

                'Dim endReq As New RequiredField(end_dateTextBox, "End Date")
            Case "782"
                '  Case "physician"
                If Titleddl.SelectedIndex <= 0 Then
                    Dim ddbTitleBinder As New DropDownListBinder(Titleddl, "SelTitlesV4", Session("EmployeeConn"))
                    ddbTitleBinder.AddDDLCriteria("@titleType", "Physician", SqlDbType.NVarChar)
                    ddbTitleBinder.BindData("TitleShortDesc", "TitleDesc")

                    If TitleLabel.Text = "" Then
                        Titleddl.SelectedIndex = -1
                    Else
                        Titleddl.SelectedValue = TitleLabel.Text
                        Title2Label.Text = TitleLabel.Text

                    End If

                End If

                If Specialtyddl.SelectedValue <> "" Then
                    SpecialtyLabel.Text = Specialtyddl.SelectedValue

                ElseIf SpecialtyLabel.Text = "" Then
                    Specialtyddl.SelectedIndex = -1
                End If

                'If SpecialtyLabel.Text = "" Then
                '    Specialtyddl.SelectedIndex = -1
                'Else
                '    Specialtyddl.SelectedValue = SpecialtyLabel.Text

                'End If

                Dim firstNameReq As New RequiredField(firstnameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(lastnameTextBox, "Last Name")
                Dim titleReq As New RequiredField(Titleddl, "Title")
                Dim hanreq As New RequiredField(hanrbl, " CHMG Group")

                Titleddl.BackColor() = Color.Yellow
                Titleddl.BorderColor() = Color.Black
                Titleddl.BorderStyle() = BorderStyle.Solid
                Titleddl.BorderWidth = Unit.Pixel(2)


                Dim specReq As New RequiredField(Specialtyddl, "Specialty")
                Specialtyddl.BackColor() = Color.Yellow
                Specialtyddl.BorderColor() = Color.Black
                Specialtyddl.BorderStyle() = BorderStyle.Solid
                Specialtyddl.BorderWidth = Unit.Pixel(2)


                Dim add1Req As New RequiredField(address1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim faxqReq As New RequiredField(faxtextbox, "Fax")

                'Dim DEAReq As New RequiredField(DEAIDTextBox, "DEA")

                Dim NPIReq As New RequiredField(npitextbox, "NPI")
                Dim LicensesReq As New RequiredField(LicensesNoTextBox, "Licenses")

                'Dim CCMCConsults As New RequiredField(CCMCConsultsrbl, "CCMC Consults")
                'Dim DCMHConsults As New RequiredField(DCMHConsultsrbl, "DCMH Consults")
                'Dim TaylorConsults As New RequiredField(TaylorConsultsrbl, "Taylor Consults")
                'Dim SpringfieldConsults As New RequiredField(SpringfieldConsultsrbl, "Springfield Consults")

                'Dim Writeorders As New RequiredField(Writeordersrbl, "Write Orders for C.T.S.")
                'Dim WriteordersDCMH As New RequiredField(WriteordersDCMHrbl, "Write Orders for DCMH")

                Dim CCMCadmitRights As New RequiredField(CCMCadmitRightsrbl, "Admitting Rights for C.T.S.")
                'Dim DCMHadmitRights As New RequiredField(DCMHadmitRightsrbl, "Admitting Rights for DCMH")

                'If hanrbl.SelectedValue.ToString = "Yes" Then

                '    Dim ViewCPMModelreq As New RequiredField(CPMEmployeesddl, "CPM Model")

                '    Dim npiReq As New RequiredField(npitextbox, "NPI")
                '    Dim LicensesNoReq As New RequiredField(LicensesNoTextBox, "Licenses No")
                '    Dim taxonomyreq As New RequiredField(taxonomytextbox, "Taxonomy")
                '    Dim startdatereq As New RequiredField(startdateTextBox, "Start Date")

                '    Dim Schedulablereq As New RequiredField(Schedulablerbl, "Schedule")
                '    Dim Televoxrblreq As New RequiredField(Televoxrbl, "Televox")


                '    If educationListbox.SelectedIndex > -1 Then
                '        Dim EducationText As New RequiredField(educationDateTextBox, "Education Date")

                '    End If

                'End If

                pnlProvider.Visible = True


                'providerrbl.SelectedValue = "Yes"


                'lblVendor.Text = "Vendor Name:"
                Vendorddl.Visible = False
                VendorNameTextBox.Enabled = False
                VendorNameTextBox.Visible = False


            Case "1197"
                'Case "non staff clinician"
                Dim firstNameReq As New RequiredField(firstnameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(lastnameTextBox, "Last Name")
                Dim titleReq As New RequiredField(Titleddl, "Title")
                Titleddl.BackColor() = Color.Yellow
                Titleddl.BorderColor() = Color.Black
                Titleddl.BorderStyle() = BorderStyle.Solid
                Titleddl.BorderWidth = Unit.Pixel(2)


                Dim specReq As New RequiredField(Specialtyddl, "Specialty")
                Specialtyddl.BackColor() = Color.Yellow
                Specialtyddl.BorderColor() = Color.Black
                Specialtyddl.BorderStyle() = BorderStyle.Solid
                Specialtyddl.BorderWidth = Unit.Pixel(2)

                Dim add1Req As New RequiredField(address1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim faxqReq As New RequiredField(faxtextbox, "Fax")
                Dim hanreq As New RequiredField(hanrbl, " CHMG group ")


                Dim ddbTitleBinder As New DropDownListBinder(Titleddl, "SelTitlesV4", Session("EmployeeConn"))
                ddbTitleBinder.AddDDLCriteria("@titleType", "physician", SqlDbType.NVarChar)
                ddbTitleBinder.BindData("TitleShortDesc", "TitleDesc")

                If TitleLabel.Text = "" Then
                    Titleddl.SelectedIndex = -1
                Else
                    Titleddl.SelectedValue = TitleLabel.Text
                    Title2Label.Text = TitleLabel.Text


                End If

                If Specialtyddl.SelectedValue <> "" Then
                    SpecialtyLabel.Text = Specialtyddl.SelectedValue

                ElseIf SpecialtyLabel.Text = "" Then
                    Specialtyddl.SelectedIndex = -1
                End If

                'If SpecialtyLabel.Text = "" Then
                '    Specialtyddl.SelectedIndex = -1
                'Else
                '    Specialtyddl.SelectedValue = SpecialtyLabel.Text

                'End If

                pnlProvider.Visible = True

                ' pnlAlliedHealth.Visible = False
                'providerrbl.SelectedIndex = -1

                VendorNameTextBox.Visible = False
                Vendorddl.Visible = True
                'lblVendor.Text = "Contractor Name:"
                'VendorNameTextBox.Enabled = True


            Case "1194"
                'Case "resident"
                'Case "resident"
                Dim firstNameReq As New RequiredField(firstnameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(lastnameTextBox, "Last Name")
                Dim titleReq As New RequiredField(Titleddl, "Title")

                Titleddl.BackColor() = Color.Yellow
                Titleddl.BorderColor() = Color.Black
                Titleddl.BorderStyle() = BorderStyle.Solid
                Titleddl.BorderWidth = Unit.Pixel(2)

                Dim specReq As New RequiredField(Specialtyddl, "Specialty")
                Specialtyddl.BackColor() = Color.Yellow
                Specialtyddl.BorderColor() = Color.Black
                Specialtyddl.BorderStyle() = BorderStyle.Solid
                Specialtyddl.BorderWidth = Unit.Pixel(2)


                Dim add1Req As New RequiredField(address1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                'Dim faxqReq As New RequiredField(faxtextbox, "Fax")

                'Dim CCMCConsults As New RequiredField(CCMCConsultsrbl, "CCMC Consults")
                'Dim DCMHConsults As New RequiredField(DCMHConsultsrbl, "DCMH Consults")
                'Dim TaylorConsults As New RequiredField(TaylorConsultsrbl, "Taylor Consults")
                'Dim SpringfieldConsults As New RequiredField(SpringfieldConsultsrbl, "Springfield Consults")

                'Dim Writeorders As New RequiredField(Writeordersrbl, "Write Orders for C.T.S.")
                'Dim WriteordersDCMH As New RequiredField(WriteordersDCMHrbl, "Write Orders for DCMH")

                Dim hanreq As New RequiredField(hanrbl, " CHMG group ")

                'If hanrbl.SelectedValue.ToString = "Yes" Then

                '    Dim ViewCPMModelreq As New RequiredField(CPMEmployeesddl, "CPM Model")

                '    Dim npiReq As New RequiredField(npitextbox, "NPI")
                '    Dim LicensesNoReq As New RequiredField(LicensesNoTextBox, "Licenses No")
                '    Dim taxonomyreq As New RequiredField(taxonomytextbox, "Taxonomy")
                '    Dim startdatereq As New RequiredField(startdateTextBox, "Start Date")
                '    Dim Schedulablereq As New RequiredField(Schedulablerbl, "Schedule")
                '    Dim Televoxrblreq As New RequiredField(Televoxrbl, "Televox")
                'End If

                pnlProvider.Visible = True

                'providerrbl.SelectedValue = "Yes"
                Titleddl.Enabled = True

                'lblVendor.Text = "Vendor Name:"
                Vendorddl.Visible = False
                VendorNameTextBox.Visible = True
                VendorNameTextBox.Enabled = False



            Case "1198"
                ' Case "vendor"

                Dim firstNameReq As New RequiredField(firstnameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(lastnameTextBox, "Last Name")

                Dim add1Req As New RequiredField(address1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim vendorRequired As New RequiredField(VendorNameTextBox, "Vendor")
                Dim startReq As New RequiredField(startdateTextBox, "Start Date")

                Dim VendorReq As New RequiredField(Vendorddl, "Vendor")
                Vendorddl.BackColor() = Color.Yellow
                Vendorddl.BorderColor() = Color.Black
                Vendorddl.BorderStyle() = BorderStyle.Solid
                Vendorddl.BorderWidth = Unit.Pixel(2)

                VendorNameTextBox.Visible = False
                Vendorddl.Visible = True
                'lblVendor.Text = "Vendor Name:"


                pnlProvider.Visible = False
                'providerrbl.SelectedIndex = -1

            Case "1195"
                ' Case "contract"

                Dim firstNameReq As New RequiredField(firstnameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(lastnameTextBox, "Last Name")

                Dim add1Req As New RequiredField(address1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim startReq As New RequiredField(startdateTextBox, "Start Date")
                Dim vendorRequired As New RequiredField(VendorNameTextBox, "Vendor")
                Dim VendorReq As New RequiredField(Vendorddl, "Vendor")
                Vendorddl.BackColor() = Color.Yellow
                Vendorddl.BorderColor() = Color.Black
                Vendorddl.BorderStyle() = BorderStyle.Solid
                Vendorddl.BorderWidth = Unit.Pixel(2)

                'lblVendor.Text = "Contrator Name:"
                Vendorddl.Visible = True

                VendorNameTextBox.Visible = False


                'pnlAlliedHealth.Visible = False
                pnlProvider.Visible = False
                'providerrbl.SelectedIndex = -1


            Case "1196", "1199"
                ' Case "other"

                Dim firstNameReq As New RequiredField(firstnameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(lastnameTextBox, "Last Name")

                Dim add1Req As New RequiredField(address1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")

                Dim startReq As New RequiredField(startdateTextBox, "Start Date")


                'pnlAlliedHealth.Visible = False
                pnlProvider.Visible = False
                'providerrbl.SelectedIndex = -1

                VendorNameTextBox.Visible = True
                VendorNameTextBox.Enabled = False
                'lblVendor.Text = "Vendor Name:"
                Vendorddl.Visible = False

            Case Else


                Dim firstNameReq As New RequiredField(firstnameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(lastnameTextBox, "Last Name")


                'pnlAlliedHealth.Visible = False
                pnlProvider.Visible = False
                'providerrbl.SelectedIndex = -1



        End Select

        Dim RoleDesc As New RequiredField(Rolesddl, "Position Description")
        Rolesddl.BackColor() = Color.Yellow
        Rolesddl.BorderColor() = Color.Black
        Rolesddl.BorderStyle() = BorderStyle.Solid
        Rolesddl.BorderWidth = Unit.Pixel(2)

        Return bHasError

    End Function
    Protected Sub CellPhoneddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles CellPhoneddl.SelectedIndexChanged
        CellProvidertxt.Text = CellPhoneddl.SelectedValue
        If CellProvidertxt.Text.ToLower = "other" Then
            CellPhoneddl.Visible = False

            otherCellprov.BackColor() = Color.Yellow
            otherCellprov.BorderColor() = Color.Black
            otherCellprov.BorderStyle() = BorderStyle.Solid
            otherCellprov.BorderWidth = Unit.Pixel(2)

            otherCellprov.Visible = True

        End If



    End Sub

    Protected Sub btnCanCellPhone_Click(sender As Object, e As System.EventArgs) Handles btnCanCellPhone.Click

        Dim Applicationnum As String = "156"
        Dim ApplicationDesc As String = "- CellPhone 4 Remote Access"

        'Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        'Dim ApplicationNum As String = grAccountsToAdd.DataKeys(Convert.ToInt32(e.CommandArgument))("Applicationnum").ToString()
        'Dim ApplicationDesc As String = grAccountsToAdd.DataKeys(Convert.ToInt32(e.CommandArgument))("ApplicationDesc").ToString()

        Dim dtAvailableAccounts As New DataTable
        dtAvailableAccounts = DirectCast(ViewState("dtAvailableAccounts"), DataTable)
        dtAvailableAccounts.Rows.Add(Applicationnum, ApplicationDesc)

        ViewState("dtAvailableAccounts") = dtAvailableAccounts

        grAvailableAccounts.DataSource = dtAvailableAccounts

        grAvailableAccounts.DataBind()


        dtAddAccounts = DirectCast(ViewState("dtAddAccounts"), DataTable)
        dtAddAccounts.Rows.Clear()

        dtAddAccounts.AcceptChanges()

        dtAddAccounts.Select("ApplicationDesc")

        ViewState("dtAddAccounts") = dtAddAccounts

        grAccountsToAdd.DataSource = dtAddAccounts
        grAccountsToAdd.DataBind()

        grAccountsToAdd.SelectedRowStyle.BackColor = Color.White
        grAccountsToAdd.SelectedRowStyle.Font.Bold = False
        grAccountsToAdd.SelectedRowStyle.ForeColor = Color.Black

        TokenPhoneTextBox.Text = ""
        CellProvidertxt.Text = ""
        lblTokenError.Text = ""

        PanelCellphone.Visible = False


    End Sub
    Public Function CheckCellPhone() As Boolean

        'Public Function isValid() As Boolean
        If CellProvidertxt.Text = "" Then
            lblTokenError.Text = " Must Select a Cell Phone Provider "
            lblTokenError.Visible = True
            CellPhoneddl.Focus()

            TokenPhoneTextBox.BackColor() = Color.Red
            blnReturn = True
            Return blnReturn

        End If

        Dim nPhone As Long
        Dim sPhone As String = TokenPhoneTextBox.Text.TrimEnd

        Long.TryParse(sPhone, nPhone)

        If nPhone = False Then
            lblTokenError.Text = "Phone Number must only contain numbers."
            lblTokenError.Visible = True
            TokenPhoneTextBox.BackColor() = Color.Yellow
            TokenPhoneTextBox.BorderColor() = Color.Black
            TokenPhoneTextBox.BorderStyle() = BorderStyle.Solid
            TokenPhoneTextBox.BorderWidth = Unit.Pixel(2)

            TokenPhoneTextBox.Focus()
            blnReturn = True

            Return blnReturn

        End If

        If CellProvidertxt.Text <> "" Then
            If TokenPhoneTextBox.Text = "" Then
                lblTokenError.Text = " Must supply a Cell Phone number "
                lblTokenError.Visible = True
                TokenPhoneTextBox.BackColor() = Color.Yellow
                TokenPhoneTextBox.BorderColor() = Color.Black
                TokenPhoneTextBox.BorderStyle() = BorderStyle.Solid
                TokenPhoneTextBox.BorderWidth = Unit.Pixel(2)

                TokenPhoneTextBox.Focus()

                TokenPhoneTextBox.BackColor() = Color.Red
                blnReturn = True
                Return blnReturn
            Else

                If (TokenPhoneTextBox.Text.Length) <> 10 Then

					lblTokenError.Text = "Phone Number Not correct length."
					lblTokenError.Visible = True
                    TokenPhoneTextBox.BackColor() = Color.Yellow
                    TokenPhoneTextBox.BorderColor() = Color.Black
                    TokenPhoneTextBox.BorderStyle() = BorderStyle.Solid
                    TokenPhoneTextBox.BorderWidth = Unit.Pixel(2)

                    TokenPhoneTextBox.Focus()
                    blnReturn = True
                    Return blnReturn

                End If

            End If

            txtComment.Text = TokenPhoneTextBox.Text & CellProvidertxt.Text
            'CellPhonePopup.Hide()
            'PanelCellphone.Visible = False

        End If

        Return blnReturn

    End Function
    'Protected Sub btnAddCellPhone_Click(sender As Object, e As System.EventArgs) Handles btnAddCellPhone.Click
    '    If CellProvidertxt.Text <> "" Then
    '        If TokenPhoneTextBox.Text = "" Then
    '            lblTokenError.Text = " Must supply a Cell Phone number "
    '            lblTokenError.Visible = True
    '            lblTokenError.Focus()

    '            TokenPhoneTextBox.BackColor() = Color.Red
    '            Exit Sub
    '        Else
    '            If Regex.IsMatch(TokenPhoneTextBox.Text, "^[0-9 ]") = False Then

    '                lblTokenError.Text = "Phone Number must only contain numbers."
    '                lblTokenError.Visible = True

    '                Exit Sub

    '            End If


    '            TokenPhoneTextBox.BackColor() = Color.Yellow
    '            TokenPhoneTextBox.BorderColor() = Color.Black
    '            TokenPhoneTextBox.BorderStyle() = BorderStyle.Solid
    '            TokenPhoneTextBox.BorderWidth = Unit.Pixel(2)


    '        End If

    '        txtComment.Text = TokenPhoneTextBox.Text & CellProvidertxt.Text
    '        'CellPhonePopup.Hide()
    '        PanelCellphone.Visible = False

    '    End If
    'End Sub

    Protected Sub btnAddComments_Click(sender As Object, e As System.EventArgs) Handles btnAddComments.Click
        requestCommentsPan.Visible = True
        'CommentsPopup.Show()
    End Sub
    Protected Sub btnAddFinComments_Click(sender As Object, e As System.EventArgs) Handles btnAddFinComments.Click
        requestCommentsPan.Visible = True
        'CommentsPopup.Show()

    End Sub

    Protected Sub btnAddDeleteComments_Click(sender As Object, e As System.EventArgs) Handles btnAddDeleteComments.Click
        requestCommentsPan.Visible = True
        'CommentsPopup.Show()
    End Sub

    Protected Sub btnSubmitNonPrivileged_Click(sender As Object, e As System.EventArgs) Handles btnSubmitNonPrivileged.Click

        ' execute proc to delete priviledeg dates and mark everything NO
        NewRequestForm.AddInsertParm("@receiver_client_num", iClientNum, SqlDbType.Int, 10)
        NewRequestForm.AddInsertParm("@account_request_type", "removecre", SqlDbType.NVarChar, 9)
        NewRequestForm.UpdateFormReturnReqNum()
        AccountRequestNum = NewRequestForm.AccountRequestNum

        ModalPrivilegedPop.Hide()

        sendAncillaryEmail()

        Response.Redirect("~/" & "SimpleSearch.aspx")
    End Sub

    Protected Sub btnNonPrivileged_Click(sender As Object, e As System.EventArgs) Handles btnNonPrivileged.Click
        ModalPrivilegedPop.Show()

    End Sub

    Protected Sub btnCloseNonPrivileged_Click(sender As Object, e As System.EventArgs) Handles btnCloseNonPrivileged.Click
        ModalPrivilegedPop.Hide()

    End Sub
    Protected Sub hanrbl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles hanrbl.SelectedIndexChanged
        If hanrbl.SelectedValue.ToString = "Yes" Then
            LocationOfCareIDddl.Visible = True
            ckhnlbl.Visible = True

            LocationOfCareIDddl.BackColor() = Color.Yellow
            LocationOfCareIDddl.BorderColor() = Color.Black
            LocationOfCareIDddl.BorderStyle() = BorderStyle.Solid
            LocationOfCareIDddl.BorderWidth = Unit.Pixel(2)

            'lblprov.Visible = True
            'providerrbl.Visible = True

        Else
            LocationOfCareIDddl.Visible = False
            ckhnlbl.Visible = False
            LocationOfCareIDLabel.Text = ""

            'lblprov.Visible = False
            'providerrbl.Visible = False
            'providerrbl.SelectedIndex = -1

        End If

    End Sub
    Protected Sub group_nameddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles group_nameddl.SelectedIndexChanged
        ' Invision Group 
        If InvisionGroupNumTextBox.Text <> "" Then
            groupnameTextBox.Text = group_nameddl.SelectedItem.ToString
            'groupnumTextBox.Text = group_nameddl.SelectedValue
            InvisionGroupNumTextBox.Text = group_nameddl.SelectedValue
        ElseIf group_nameddl.SelectedValue.ToLower = "select" Or group_nameddl.SelectedValue = "99999" Then
            groupnameTextBox.Text = Nothing
            'groupnumTextBox.Text = Nothing
            InvisionGroupNumTextBox.Text = Nothing
        Else

            groupnameTextBox.Text = group_nameddl.SelectedItem.ToString
            'groupnumTextBox.Text = group_nameddl.SelectedValue
            InvisionGroupNumTextBox.Text = group_nameddl.SelectedValue

        End If
        'origionalgroupNum.Text
        If origionalgroupNum.Text <> InvisionGroupNumTextBox.Text Then
            If lblSearched.Text = "" Then
                lblSearched.Text = "TRUE"

                PhysgroupPop.Show()
            End If

        End If


    End Sub

    Protected Sub LocationOfCareIDddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles LocationOfCareIDddl.SelectedIndexChanged
        ' Gnumber
        If LocationOfCareIDLabel.Text <> "" Then
            LocationOfCareIDLabel.Text = LocationOfCareIDddl.SelectedItem.ToString
            GNumberGroupNumLabel.Text = LocationOfCareIDddl.SelectedValue
            'origionalGnumber
        ElseIf LocationOfCareIDddl.SelectedValue.ToLower = "select" Or LocationOfCareIDddl.SelectedValue = "99999" Then

            LocationOfCareIDLabel.Text = Nothing
            GNumberGroupNumLabel.Text = Nothing

        Else
            LocationOfCareIDLabel.Text = LocationOfCareIDddl.SelectedItem.ToString
            GNumberGroupNumLabel.Text = LocationOfCareIDddl.SelectedValue
        End If

        'If origionalGnumber.Text <> GNumberGroupNumLabel.Text Then
        '    If lblSearched.Text = "" Then
        '        lblSearched.Text = "TRUE"

        '        If GNumberGroupNumLabel.Text = "0" Then

        '            NonHanlbl.Text = " You have identified this physician to be removed from CHMG group. Submit a New Request to remove their access from CKHN-EMR and Interface group. "
        '            lblBackout.Text = " To Undo this action do not click on button -> Update Demographics."

        '            NonHanlbl.Visible = True
        '            lblBackout.Visible = True

        '            hanrbl.SelectedValue = "No"
        '        End If
        '        PhysgroupPop.Show()
        '    End If

        'End If

    End Sub
    'Protected Sub CommLocationOfCareIDddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles CommLocationOfCareIDddl.SelectedIndexChanged
    '    ' Comm Gnumber 
    '    If CommLocationOfCareIDLabel.Text <> "" Then

    '        CoverageGroupNumLabel.Text = CoverageGroupddl.SelectedItem.ToString
    '        CommNumberGroupNumLabel.Text = CoverageGroupddl.SelectedValue


    '    ElseIf CommLocationOfCareIDddl.SelectedValue.ToLower = "select" Or CommLocationOfCareIDddl.SelectedValue = "99999" Then

    '        CommLocationOfCareIDLabel.Text = Nothing
    '        CommNumberGroupNumLabel.Text = Nothing
    '    Else

    '        CoverageGroupNumLabel.Text = CoverageGroupddl.SelectedItem.ToString
    '        CommNumberGroupNumLabel.Text = CoverageGroupddl.SelectedValue

    '    End If
    '    If origionalCommNumber.Text <> CommNumberGroupNumLabel.Text Then
    '        If lblSearched.Text = "" Then
    '            lblSearched.Text = "TRUE"

    '            PhysgroupPop.Show()
    '        End If

    '    End If

    'End Sub

    'Protected Sub CoverageGroupddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles CoverageGroupddl.SelectedIndexChanged
    '    ' Coverage group
    '    If CoverageGroupNumLabel.Text <> "" Then

    '        CoverageGroupNumLabel.Text = CoverageGroupddl.SelectedValue

    '    ElseIf CoverageGroupddl.SelectedValue.ToLower = "select" Or CoverageGroupddl.SelectedValue = "99999" Then

    '        CoverageGroupNumLabel.Text = Nothing
    '    Else

    '        CoverageGroupNumLabel.Text = CoverageGroupddl.SelectedValue

    '    End If

    'End Sub

    'Protected Sub Nonperferdddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles Nonperferdddl.SelectedIndexChanged
    '    ' Non perfered group
    '    If NonPerferedGroupnumLabel.Text <> "" Then

    '        NonPerferedGroupnumLabel.Text = Nonperferdddl.SelectedValue

    '    ElseIf Nonperferdddl.SelectedValue.ToLower = "select" Or Nonperferdddl.SelectedValue = "99999" Then

    '        NonPerferedGroupnumLabel.Text = Nothing
    '    Else

    '        NonPerferedGroupnumLabel.Text = Nonperferdddl.SelectedValue

    '    End If
    '    If origionalNonperGroup.Text <> NonPerferedGroupnumLabel.Text Then
    '        If lblSearched.Text = "" Then
    '            lblSearched.Text = "TRUE"

    '            PhysgroupPop.Show()
    '        End If

    '    End If


    'End Sub
    Protected Sub PhysOfficeddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles PhysOfficeddl.SelectedIndexChanged
        'groupnumTextBox.Text = PhysOfficeddl.SelectedValue
        groupnameTextBox.Text = PhysOfficeddl.SelectedItem.ToString
    End Sub
    Private Sub sendAncillaryEmail()
        Dim c3Controller As New Client3Controller(iClientNum, "no", Session("EmployeeConn"))
        Dim EmailBody As New EmailBody

        Dim sSubject As String
        Dim sBody As String

        Dim EmailList As String
        Dim sAppbase As String = ""
        Dim Link As String

        Dim sAccounts As String = ""

        If Session("environment").ToString.ToLower = "testing" Then
            'toEmail = "Jeff.Mele@crozer.org"
            Link = "http://webdev3/CSCV2/ClientDemo.aspx?ClientNum=" & iClientNum.ToString
        Else
            Link = "http://Casemgmt02/CSCV2/ClientDemo.aspx?ClientNum=" & iClientNum.ToString

        End If

        sSubject = "Provider/Physician NO LONGER CREDENTIAL for " & c3Controller.FirstName & " " & c3Controller.LastName & " ."


        sBody = "<!DOCTYPE html>" & _
"<html>" & _
"<head>" & _
"<style>" & _
"table" & _
"{" & _
"border-collapse:collapse;" & _
"font-family: 'Helvetica Neue', 'Lucida Grande', 'Segoe UI', Arial, Helvetica, Verdana, sans-serif;" & _
"}" & _
"table, th, td" & _
"{" & _
"border: 1px solid black;" & _
"padding: 3px" & _
"}" & _
"</style>" & _
"</head>" & _
"" & _
"<body>" & _
"<table cellpadding='5px'>" & _
"<tr>" & _
    "<td colspan='3' align='center'  style='font-size:large'>" & _
"               " & _
    "<b>Provider/Physician No Longer credentialed Update</b>" & _
"                 " & _
     "</td>    " & _
"</tr>" & _
"<tr>" & _
    "<td style='font-weight:bold' colspan='1'>Name:</td>" & _
    "<td colspan='3'>" & c3Controller.FirstName & " " & c3Controller.LastName & "</td>" & _
"</tr>" & _
"<tr>" & _
    "<td style='font-weight:bold' colspan='1'>Requested By:</td>" & _
    "<td colspan='3'>" & SubmittingOnBehalfOfNameLabel.Text & "</td>" & _
"</tr>" & _
"<tr>" & _
    "<td style='font-weight:bold' colspan='1'>Date Requested:</td>" & _
    "<td colspan='3'>" & c3Controller.LastUpdated & "</td>" & _
"</tr>" & _
"      " & _
"     " & _
"<tr style='font-weight:bold'><td>Information </td><td> Data </td></tr>" & _
"<tr><td> First Name </td><td>" & c3Controller.FirstName & "</td></tr>" & _
"<tr><td> Last Name </td><td>" & c3Controller.LastName & "</td></tr>" & _
"<tr><td> Address 1 </td><td>" & c3Controller.Address1 & "</td></tr>" & _
"<tr><td> Address 2 </td><td>" & c3Controller.Address2 & "</td></tr>" & _
"<tr><td> City </td><td>" & c3Controller.City & "</td></tr>" & _
"<tr><td> State </td><td>" & c3Controller.State & "</td></tr>" & _
"<tr><td> Zip </td><td>" & c3Controller.Zip & "</td></tr>" & _
"<tr><td> Phone </td><td>" & c3Controller.Phone & "</td></tr>" & _
"<tr><td> Fax </td><td>" & c3Controller.Fax & "</td></tr>" & _
"<tr><td> Doctor Master#</td><td>" & c3Controller.DoctorMasterNum & "</td></tr>" & _
"<tr><td> NPI</td><td>" & c3Controller.Npi & "</td></tr>" & _
"<tr><td> License No</td><td>" & c3Controller.LicensesNo & "</td></tr>" & _
"<tr><td> Specialty</td><td>" & c3Controller.Specialty & "</td></tr>" & _
"<tr><td> Title</td><td>" & c3Controller.Title & "</td></tr>" & _
"</table>" & _
       "</body>" & _
        "</html>"

        sBody = sBody & "<table cellpadding='5px'>" & _
        "<tr>" & _
            "<td align='center'  style='font-size:large'>" & _
"               " & _
            "<b>View Details by clicking on link below</b>" & _
"                 " & _
             "</td>    " & _
        "</tr>" & _
        "<tr>" & _
            "<td align='center'>" & Link & "</td>" & _
        "</tr>"


        'ActionControl.GetAllActions(AccountRequestNum, Session("EmployeeConn"))

        EmailList = New EmailListOne(1200, "0", Session("EmployeeConn")).EmailListReturn


        Dim toList As String()

        toList = EmailList.Split(";")
        If toList.Length = 1 Then
            Mailmsg.To.Add(New System.Net.Mail.MailAddress(toList(0)))
        Else
        End If
        For i = 0 To toList.Length - 1 ' - 2
            If toList(i).Length > 10 Then ' validity check
                Mailmsg.To.Add(New System.Net.Mail.MailAddress(toList(i)))
            End If
        Next

        Mailmsg.From = New MailAddress("CSC@crozer.org")
        Mailmsg.IsBodyHtml = True

        Mailmsg.Subject = sSubject
        Mailmsg.Body = sBody

        Dim smtpClient As New SmtpClient()
        smtpClient.Host = "ah1smtprelay01.altahospitals.com"


        smtpClient.Send(Mailmsg)




    End Sub

    Protected Sub btnChangeTCL_Click(sender As Object, e As System.EventArgs) Handles btnChangeTCL.Click
        'gvTCL.Visible = True
        'gvTCL.BackColor() = Color.Yellow
        'gvTCL.BorderColor() = Color.Black
        'gvTCL.BorderStyle() = BorderStyle.Solid
        'gvTCL.BorderWidth = Unit.Pixel(2)
        'gvTCL.RowStyle.BackColor() = Color.Yellow

        'btnChangeTCL.Visible = False

        If btnChangeTCL.Text = "Show All User Types" Then

            gvTCL.DataSource = Nothing
            gvTCL.DataBind()
            gvTCL.Dispose()


            Dim tcldata As New CommandsSqlAndOleDb("SelInvisionUserTypeV2", Session("EmployeeConn"))
            'ckhsEmployee.RoleNum  RoleNumLabel.Text
            tcldata.AddSqlProcParameter("@rolenum", masterRole.Text, SqlDbType.NVarChar, 8)
            tcldata.AddSqlProcParameter("@Clientupd", "all", SqlDbType.NVarChar, 3)

            tcldata.GetSqlDataset()

            dtTCLUserTypes = tcldata.GetSqlDataTable

            gvTCL.DataSource = dtTCLUserTypes
            gvTCL.DataBind()

            gvTCL.Visible = True
            'gvTCL.BackColor() = Color.Yellow
            'gvTCL.BorderColor() = Color.Black
            'gvTCL.BorderStyle() = BorderStyle.Solid
            'gvTCL.BorderWidth = Unit.Pixel(2)
            'gvTCL.RowStyle.BackColor() = Color.Yellow

            pnlTCl.Visible = True
            TCLTextBox.Visible = True
            UserTypeDescTextBox.Visible = True
            UserTypeCdTextBox.Visible = True
            ' btnChangeTCL.Visible = True

            btnChangeTCL.Text = "Close Types"

        ElseIf btnChangeTCL.Text = "Change TCL" Or btnChangeTCL.Text = "Select TCL" Then
            GetTCL()
            btnChangeTCL.Text = "Show All User Types"

        ElseIf btnChangeTCL.Text = "Close Types" Then
            gvTCL.Dispose()
            gvTCL.Visible = False
            btnChangeTCL.Text = "Show All User Types"
        Else
            gvTCL.Visible = True
            btnChangeTCL.Text = "Show All User Types"
            'gvTCL.BackColor() = Color.Yellow
            'gvTCL.BorderColor() = Color.Black
            'gvTCL.BorderStyle() = BorderStyle.Solid
            'gvTCL.BorderWidth = Unit.Pixel(2)
            'gvTCL.RowStyle.BackColor() = Color.Yellow

            'btnChangeTCL.Visible = False

        End If
    End Sub



    Protected Sub gvDocmster_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvDocmster.RowCommand
        If e.CommandName = "Select" Then
            Dim ClientNumS As String
            Dim DocMasterNUm As String

            ClientNumS = gvDocmster.DataKeys(Convert.ToInt32(e.CommandArgument))("clientnum").ToString()
            DocMasterNUm = gvDocmster.DataKeys(Convert.ToInt32(e.CommandArgument))("DoctormasterNum").ToString()

            'If GroupTypeCdTextbox.Text <> "" Then
            '    Select Case GroupTypeCdTextbox.Text
            '        Case "ckhn"
            '            sGnumber = GvPhysGroup.DataKeys(Convert.ToInt32(e.CommandArgument))("GNumber").ToString()
            '        Case "comm"
            '            sCommGnumber = GvPhysGroup.DataKeys(Convert.ToInt32(e.CommandArgument))("CommGNumber").ToString()
            '        Case "invision"
            '            sInvisionNumber = GvPhysGroup.DataKeys(Convert.ToInt32(e.CommandArgument))("InvisionGroupNum").ToString()
            '    End Select
            'End If

            'Response.Redirect("./PhysGrpMaintDetail.aspx?GroupNum=" & GroupNumS & "&GroupTypeCd=" & GroupTypeCd, True)
            '& "&GNumber=" & sGnumber & "&CommGNumber=" & sCommGnumber & "&InvisionGroupNum=" & sInvisionNumber


            UpdPhyFormSignOn = New FormSQL(Session("EmployeeConn"), "SelPhysiciansDoocV2", "UpdPhysicianMasterV6", "", Page)
            UpdPhyFormSignOn.AddSelectParm("@clientnum", ClientNumS, SqlDbType.Int, 6) '

            UpdPhyFormSignOn.AddInsertParm("@clientnum", ClientNumS, SqlDbType.Int, 6) '

            UpdPhyFormSignOn.AddInsertParm("@DateDeactivated", "yes", SqlDbType.NVarChar, 24)


            UpdPhyFormSignOn.UpdateForm()

            Response.Redirect("./ClientDemo.aspx?ClientNum=" & ClientNumS, True)



        End If


    End Sub








    'Protected Sub CPMEmployeesddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles CPMEmployeesddl.SelectedIndexChanged
    '    If CPMEmployeesddl.SelectedValue = 0 Then
    '        cpmModelEmplPanel.Visible = False
    '        CPMEmployeesddl.SelectedIndex = -1
    '        cpmModelAfterPanel.Visible = True
    '    Else
    '        ViewCPMModelTextBox.Text = CPMEmployeesddl.SelectedItem.ToString
    '        cpmModelAfterPanel.Visible = True


    '    End If
    '    Select Case PositionRoleNumLabel.Text
    '        Case "1567" 'Employee
    '            CPMModelTextBox.Text = ViewCPMModelTextBox.Text
    '            insAcctDriveModel(iClientNum, "cpmmodel", CPMModelTextBox.Text, "", "", "")

    '        Case "1553", "782" 'CPM Physician
    '            If hanrbl.SelectedValue.ToString = "Yes" Then
    '                provmodelTextBox.Text = ViewCPMModelTextBox.Text
    '                insAcctDriveModel(iClientNum, "provmodel", provmodelTextBox.Text, "", "", "")

    '            End If


    '    End Select
    'End Sub
    'Protected Sub Schedulablerbl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles Schedulablerbl.SelectedIndexChanged
    '    SchedulableTextbox.Text = Schedulablerbl.SelectedValue.ToLower

    'End Sub

    'Protected Sub Televoxrbl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles Televoxrbl.SelectedIndexChanged
    '    TelevoxTextBox.Text = Televoxrbl.SelectedValue.ToLower
    'End Sub

    'Protected Sub btnviewCPMLoc_Click(sender As Object, e As System.EventArgs) Handles btnviewCPMLoc.Click

    '    If btnviewCPMLoc.Text <> "Close View" Then

    '        'make panel visable
    '        CPMCKNLoc.Visible = True

    '        ' load drop down for adding 
    '        Dim ddCMPBinder As New DropDownListBinder(CPMLocationddl, "SelPhysGroupsV5", Session("EmployeeConn"))
    '        ddCMPBinder.AddDDLCriteria("@GroupTypeCd", "GNum", SqlDbType.NVarChar)
    '        ddCMPBinder.AddDDLCriteria("@clientpage", "yes", SqlDbType.NVarChar)

    '        ddCMPBinder.BindData("Group_num", "group_name")

    '        CPMDocLocs()

    '        btnviewCPMLoc.Text = "Close View"

    '    Else

    '        CPMCKNLoc.Visible = False

    '        btnviewCPMLoc.Text = "View/Add CPM Loc."

    '    End If



    'End Sub
    'Protected Sub CPMDocLocs()
    '    'load grid to view all current locations to remove them

    '    Dim thisdataDoc As New CommandsSqlAndOleDb("SelCPMPhysiciansGroupsByDoocV3", Session("EmployeeConn"))
    '    thisdataDoc.AddSqlProcParameter("@clientnum", iClientNum, SqlDbType.Int, 20)


    '    thisdataDoc.ExecNonQueryNoReturn()

    '    ViewState("dtCPMGroups") = thisdataDoc.GetSqlDataTable


    '    gvCPMList.DataSource = ViewState("dtCPMGroups")
    '    gvCPMList.DataBind()



    'End Sub
    'Protected Sub CPMLocationddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles CPMLocationddl.SelectedIndexChanged
    '    'add new CPM location to phys
    '    Dim sGroup As String = CPMLocationddl.SelectedValue.ToString


    '    Dim thisdata As New CommandsSqlAndOleDb("InsUpdCPMPhysicianMasterV6", Session("EmployeeConn"))
    '    thisdata.AddSqlProcParameter("@Doctormaster", DoctorMasterNumTextBox.Text, SqlDbType.NVarChar, 6)

    '    thisdata.AddSqlProcParameter("@client_num", iClientNum, SqlDbType.Int, 20)

    '    thisdata.AddSqlProcParameter("@GroupNum", sGroup, SqlDbType.NVarChar, 24)

    '    thisdata.AddSqlProcParameter("@LastName", lastnameTextBox.Text, SqlDbType.NVarChar, 30)
    '    thisdata.AddSqlProcParameter("@FirstName", firstnameTextBox.Text, SqlDbType.NVarChar, 20)


    '    thisdata.AddSqlProcParameter("@GroupTypeCd", "GNum", SqlDbType.NVarChar, 12)


    '    thisdata.ExecNonQueryNoReturn()

    '    CPMLocationddl.SelectedIndex = -1

    '    CPMDocLocs()

    'End Sub

    'Protected Sub gvCPMList_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvCPMList.RowCommand
    '    If e.CommandName = "Select" Then


    '        Dim sGroup As String = gvCPMList.DataKeys(Convert.ToInt32(e.CommandArgument))("Group_num").ToString()

    '        Dim thisdata As New CommandsSqlAndOleDb("InsUpdCPMPhysicianMasterV6", Session("EmployeeConn"))
    '        thisdata.AddSqlProcParameter("@Doctormaster", DoctorMasterNumTextBox.Text, SqlDbType.NVarChar, 6)

    '        thisdata.AddSqlProcParameter("@client_num", iClientNum, SqlDbType.Int, 20)

    '        thisdata.AddSqlProcParameter("@GroupNum", sGroup, SqlDbType.NVarChar, 24)

    '        thisdata.AddSqlProcParameter("@LastName", lastnameTextBox.Text, SqlDbType.NVarChar, 30)
    '        thisdata.AddSqlProcParameter("@FirstName", firstnameTextBox.Text, SqlDbType.NVarChar, 20)


    '        thisdata.AddSqlProcParameter("@GroupTypeCd", "GNum", SqlDbType.NVarChar, 12)

    '        thisdata.AddSqlProcParameter("@Deactivate", "yes", SqlDbType.NVarChar, 12)

    '        thisdata.ExecNonQueryNoReturn()

    '        CPMLocationddl.SelectedIndex = -1
    '        gvCPMList.SelectedIndex = -1

    '        CPMDocLocs()

    '    End If

    'End Sub

    'Protected Sub educationListbox_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles educationListbox.SelectedIndexChanged
    '    educationListbox.Enabled = False
    '    cpmEducationSelected.Text = educationListbox.SelectedValue
    '    cpmEducationText.Text = educationListbox.SelectedItem.ToString


    'End Sub
    'Protected Sub btnEducationSubmit_Click(sender As Object, e As System.EventArgs) Handles btnEducationSubmit.Click
    '    'InsModelsAndDriveItemsV3
    '    'line 904

    '    If educationListbox.SelectedIndex = -1 Then
    '        cpmedulabel.Text = "Must Select a Class"
    '        cpmedulabel.ForeColor = Color.Red
    '        cpmedulabel.Visible = True

    '        educationDateTextBox.ForeColor = Color.Red
    '        educationDateTextBox.Focus()

    '        Exit Sub
    '    End If
    '    If educationDateTextBox.Text = "" Then
    '        cpmedulabel.Text = "Must Supply a Date for Class"
    '        cpmedulabel.ForeColor = Color.Red
    '        cpmedulabel.Visible = True

    '        educationDateTextBox.ForeColor = Color.Red
    '        educationDateTextBox.Focus()

    '        Exit Sub

    '    End If

    '    If startdateTextBox.Text = "" Then
    '        startdateTextBox.Text = DateTime.Today()
    '    End If

    '    Try
    '        'Submit main request if non are open
    '        NewRequestForm.AddInsertParm("@receiver_client_num", iClientNum, SqlDbType.Int, 10)

    '        ' NewRequestForm.AddInsertParm("@submitter_client_num", UserInfo.ClientNum, SqlDbType.Int, 10)
    '        NewRequestForm.AddInsertParm("@account_request_type", "addacct", SqlDbType.NVarChar, 9)
    '        NewRequestForm.AddInsertParm("@AccountComment", txtComment.Text, SqlDbType.NVarChar, 500)
    '        NewRequestForm.UpdateFormReturnReqNum()
    '        AccountRequestNum = NewRequestForm.AccountRequestNum

    '        AccountRequest = New AccountRequest(AccountRequestNum, Session("EmployeeConn"))

    '        ValidationStatus.Text = AccountRequest.ValidationStatus

    '        'Submit item 
    '        Dim thisdata As New CommandsSqlAndOleDb("InsAccountItemsV3", Session("EmployeeConn"))
    '        thisdata.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int, 20)
    '        thisdata.AddSqlProcParameter("@ApplicationNum", 158, SqlDbType.Int, 8)
    '        thisdata.AddSqlProcParameter("@account_item_desc", " CPM Education class " & cpmEducationText.Text & " on " & educationDateTextBox.Text & " Employee Start Date " & startdateTextBox.Text, SqlDbType.NVarChar, 255)
    '        thisdata.AddSqlProcParameter("@validate", "validated", SqlDbType.NVarChar, 20)
    '        thisdata.AddSqlProcParameter("@who_nt_login", Session("LoginID"), SqlDbType.NVarChar, 50)
    '        thisdata.AddSqlProcParameter("@CPMTrainCD", cpmEducationSelected.Text, SqlDbType.NVarChar, 12)


    '        thisdata.ExecNonQueryNoReturn()


    '        'Submit Education item
    '        insEducationModel(AccountRequestNum, "", "", cpmEducationSelected.Text, "", "")
    '        Response.Redirect("~/ClientDemo.aspx?ClientNum=" & iClientNum & "&msg=" & sItemComplet, True)

    '        'If sItemComplet.Length > 2 Then
    '        '    Response.Redirect("~/ClientDemo.aspx?ClientNum=" & iClientNum & "&msg=" & sItemComplet, True)
    '        'Else
    '        '    Response.Redirect("~/AccountRequestInfo2.aspx?RequestNum=" & AccountRequestNum, True)
    '        'End If
    '    Catch ex As Exception

    '    End Try

    'End Sub
    'Private Sub insEducationModel(ByVal requestNum As Integer, ByVal ModelCd As String, ByVal comments As String, ByVal CPMTrainCd As String, ByVal Requested_Date As String, ByVal DriveLetter As String)


    '    Dim thisdata As New CommandsSqlAndOleDb("InsModelsAndDriveItemsV3", Session("EmployeeConn"))
    '    thisdata.AddSqlProcParameter("@account_request_num", requestNum, SqlDbType.Int, 20)


    '    thisdata.AddSqlProcParameter("@ModelCd", ModelCd, SqlDbType.NVarChar, 24)
    '    thisdata.AddSqlProcParameter("@CPMTrainCd", CPMTrainCd, SqlDbType.NVarChar, 24)

    '    If CPMTrainCd <> "" Then
    '        thisdata.AddSqlProcParameter("@ApplicationNum", 158, SqlDbType.Int, 8)

    '        thisdata.AddSqlProcParameter("@Requested_Date", educationDateTextBox.Text, SqlDbType.NVarChar, 24)

    '    End If

    '    thisdata.AddSqlProcParameter("@DriveLetter", DriveLetter, SqlDbType.NVarChar, 24)

    '    thisdata.AddSqlProcParameter("@validate", "validated", SqlDbType.NVarChar, 20)

    '    thisdata.AddSqlProcParameter("@account_item_desc", " CPM Education class " & cpmEducationText.Text & " on " & educationDateTextBox.Text & " Employee Start Date " & startdateTextBox.Text, SqlDbType.NVarChar, 255)

    '    thisdata.AddSqlProcParameter("@who_nt_login", Session("LoginID"), SqlDbType.NVarChar, 50)

    '    thisdata.ExecNonQueryNoReturn()



    'End Sub
    'Protected Sub CheckDate(ByVal sDate As String)
    '    sDate = "01/10/2000"
    '    Dim regex As Regex = New Regex("(((0|1)[0-9]|2[0-9]|3[0-1])//(0[1-9]|1[0-2])//((19|20)\d\d))$")

    '    'Verify whether date entered in dd/MM/yyyy format.
    '    Dim isValid As Boolean = regex.IsMatch(sDate.Trim)

    '    'Verify whether entered date is Valid date.
    '    Dim dt As DateTime
    '    isValid = DateTime.TryParseExact(sDate, "dd/MM/yyyy", New CultureInfo("en-GB"), DateTimeStyles.None, dt)
    '    If Not isValid Then
    '        lblValidation.Text = "Invalid Date."

    '    End If
    'End Sub

End Class
