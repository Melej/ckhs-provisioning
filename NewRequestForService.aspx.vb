﻿Imports App_Code

Imports System.Reflection
Imports System.ComponentModel
Partial Class NewRequestForService
    Inherits System.Web.UI.Page
    Dim c3Requestor As New Client3
    Dim UserInfo As UserSecurity
    Dim ddlBinder As New DropDownListBinder
    Dim frmHelper As New FormHelper
    Dim Client3Contrl As New Client3Controller()
    Dim dtTechServices As New DataTable





    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        UserInfo = Session("objUserSecurity")


        If ViewState("objDtTechServices") Is Nothing Then

            ViewState("SeqNum") = 1

            dtTechServices.Columns.Add("SeqNum")
            dtTechServices.Columns.Add("TechServiceID")
            dtTechServices.Columns.Add("TechServiceDesc")
            dtTechServices.Columns.Add("NumOfDevices")
            dtTechServices.Columns.Add("DeviceID")
            dtTechServices.Columns.Add("DetailInformation")

            ViewState("objDtTechServices") = dtTechServices


        End If

        If Not IsPostBack Then

            ddlBinder = New DropDownListBinder(ddlCategoryTypeCd, "SelCategoryCodes", Session("CSCConn"))
            ddlBinder.BindData("Category_cd", "category_desc")

            'ddlBinder = New DropDownListBinder(ddlCategoryTypeCd, "sel_category_codes", Session("EmployeeConn"))
            'ddlBinder.BindData("category_num", "category_cd")

            'ddlBinder = New DropDownListBinder(ddlTypeCd, "sel_category_type_codes", Session("EmployeeConn"))
            'ddlBinder.BindData("type_num", "type_cd")

            'ddlBinder = New DropDownListBinder(ddlItemCd, "sel_category_type_items", Session("EmployeeConn"))
            'ddlBinder.BindData("item_num", "item_cd")

            'ddlBinder = New DropDownListBinder(ddlChangeGroup, "sel_group_names", Session("EmployeeConn"))
            'ddlBinder.BindData("group_num", "group_name")

            'ddlBinder = New DropDownListBinder(ddlStatus, "sel_user_status_codes", Session("EmployeeConn"))
            'ddlBinder.BindData("status_cd", "status_desc")

            'ddlBinder = New DropDownListBinder(ddlTechService, "SelTechServices", Session("EmployeeConn"))
            'ddlBinder.BindData("TechServiceID", "TechServiceDesc")

        End If


    End Sub
    Protected Sub btnSearchClients_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearchClients.Click

        Dim thisData As New CommandsSqlAndOleDb("SelSimpleSearchV2", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@FirstName", FirstNameTextBox.Text, SqlDbType.NVarChar, 20)
        thisData.AddSqlProcParameter("@LastName", LastNameTextBox.Text, SqlDbType.NVarChar, 20)


        gvSearch.DataSource = thisData.GetSqlDataTable
        gvSearch.DataBind()

        uplSearchClient.Update()
        mpeSearchClient.Show()

    End Sub
    Protected Sub gvSearch_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvSearch.RowCommand

        If e.CommandName = "Select" Then
            Dim client_num As String = gvSearch.DataKeys(Convert.ToInt32(e.CommandArgument))("client_num").ToString()



            c3Requestor = Client3Contrl.GetClientByClientNum(client_num)



            '   ViewState("objc3Requestor") = c3Requestor


            Dim frmHelperRequestor As New FormHelper()
            frmHelperRequestor.FillForm(c3Requestor, Page, "Requestor")

            mpeSearchClient.Hide()
            btnUpdate.Visible = True
            uplTicket.Update()

        End If
    End Sub

    Protected Sub btnSearch_Click(sender As Object, e As System.EventArgs) Handles btnSearch.Click
        mpeSearchClient.Show()
    End Sub

    Protected Sub btnSubmitTechServices_Click(sender As Object, e As System.EventArgs) Handles btnAddTechService.Click
        dtTechServices = DirectCast(ViewState("objDtTechServices"), DataTable)
        dtTechServices.Rows.Add(ViewState("SeqNum"), ddlTechService.SelectedValue, ddlTechService.SelectedItem, txtNumTechDevices.Text, txtTechDeviceIDs.Text, txtTechServiceDesc.Text)

        ViewState("objDtTechServices") = dtTechServices
        ViewState("SeqNum") = ViewState("SeqNum") + 1
        BindTechServices()
        uplTicket.Update()
    End Sub

    Protected Sub SubmitTechServices(ByVal RequestNum As String)
        dtTechServices = DirectCast(ViewState("objDtTechServices"), DataTable)
        Dim conn As New SetDBConnection()

        Dim _dataConn As String = conn.EmployeeConn


        Using sqlConn As New SqlConnection(_dataConn)



            Dim Cmd As New SqlCommand()
            Cmd.CommandText = "InsRequestTechServices"
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.Connection = sqlConn


            sqlConn.Open()

            Using Cmd

                For Each dr In dtTechServices.Rows

                    Cmd.Parameters.Clear()

                    Cmd.Parameters.AddWithValue("@RequestNum", RequestNum)
                    Cmd.Parameters.AddWithValue("@TechServiceID", dr("TechServiceID"))
                    Cmd.Parameters.AddWithValue("@NumOfDevices", dr("NumOfDevices"))
                    Cmd.Parameters.AddWithValue("@DeviceID", dr("DeviceID"))
                    Cmd.Parameters.AddWithValue("@DetailInformation", dr("DetailInformation"))


                    Cmd.ExecuteNonQuery()

                Next




            End Using

            sqlConn.Close()

        End Using
    End Sub


    Protected Sub btnSubmit_Click(sender As Object, e As System.EventArgs) Handles btnSubmit.Click



        If c3Requestor Is Nothing Then

            '  txtRecommendation.Text = "Select User"
            '//Do something
        Else

            Dim frmHelperRequestEntry As New FormHelper()

            frmHelperRequestEntry.InitializeUpdateForm("InsNewRequestEntry", Page)
            frmHelperRequestEntry.AddCmdParameter("@SubmitterClientNum", UserInfo.ClientNum)
            '  frmHelperRequestEntry.AddCmdParameter("@RequestorClientNum", c3Requestor.ClientNum)

            frmHelperRequestEntry.AddCmdParameter("@RequestType", "ticket")
            frmHelperRequestEntry.UpdateForm("@NextRequestNum")


            Dim frmHelperRequestTicket As New FormHelper()

            frmHelperRequestTicket.InitializeUpdateForm("InsUpdRequestService", Page)
            frmHelperRequestTicket.AddCmdParameter("@RequestNum", frmHelperRequestEntry.OutParamValue)
            frmHelperRequestTicket.UpdateForm()


            SubmitTechServices(frmHelperRequestEntry.OutParamValue)


        End If





    End Sub

    Protected Sub BindTechServices()


        gvTechServices.DataSource = ViewState("objDtTechServices")
        gvTechServices.DataBind()
    End Sub


    Protected Sub btnCloseSearchClient_Click(sender As Object, e As System.EventArgs) Handles btnCloseSearchClient.Click
        mpeSearchClient.Hide()

    End Sub
    Protected Sub btnCloseUpdate_Click(sender As Object, e As System.EventArgs) Handles btnCloseUpdate.Click

        mpeUpdateClient.Hide()

    End Sub

    Protected Sub btnUpdate_Click(sender As Object, e As System.EventArgs) Handles btnUpdate.Click

        UpdateClientPanel()
        mpeUpdateClient.Show()

    End Sub

    Protected Sub UpdateClientPanel()


        Dim c3Update As New Client3
        c3Update = Client3Contrl.GetClientByClientNum(lblRequestorClientNum.Text)


        frmHelper.FillForm(c3Update, tblUpdateClient, "Update")


        ddlBinder.BindData(ddlUpdateFacilityCd, "SelFacilityCodes", "facility_cd", "facility_short_name", Session("EmployeeConn"))



    End Sub

    Protected Sub btnUpdateClient_Click(sender As Object, e As System.EventArgs) Handles btnUpdateClient.Click
        Dim frmHelperRequestTicket As New FormHelper()

        frmHelperRequestTicket.InitializeUpdateForm("UpdClientObjectByNumberV3", tblUpdateClient, "Update")
        frmHelperRequestTicket.AddCmdParameter("@ClientNum", lblRequestorClientNum.Text)
        frmHelperRequestTicket.UpdateForm()

        c3Requestor = Client3Contrl.GetClientByClientNum(lblRequestorClientNum.Text)



        '   ViewState("objc3Requestor") = c3Requestor



        frmHelperRequestTicket.FillForm(c3Requestor, Page, "Requestor")


        mpeUpdateClient.Hide()
    End Sub

    Protected Sub ddlChangeGroup_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlChangeGroup.SelectedIndexChanged
        ddlBinder = New DropDownListBinder(ddlChangeTech, "selTechnicians", Session("EmployeeConn"))
        ddlBinder.AddDDLCriteria("@GroupNum", ddlChangeGroup.SelectedValue, SqlDbType.Int)
        ddlBinder.BindData("client_num", "TechName")

        uplTicket.Update()

    End Sub
End Class
