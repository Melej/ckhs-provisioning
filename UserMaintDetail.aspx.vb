﻿Imports System.IO
Imports System.Data '
Imports System.Data.SqlClient
Imports App_Code
Imports AjaxControlToolkit

Partial Class UserMaintDetail
    Inherits System.Web.UI.Page
    Dim UserInfo As UserSecurity
    ' Dim c3Controller As New Client3Controller
    ' Dim Client3Contrl As New Client3Controller()

    Dim AllGroupQueuesInfo As New GroupQueues
    Dim AllMyGroupInfo As New GroupQueues


    Dim allgrouplist As New List(Of AllGroupQueues)
    Dim MyGrouplist As New List(Of AllMyGroups)
    Dim MyDTGroup As New List(Of AllMyGroups)
    Dim MySecLevel As New List(Of AllMyGroups)

    Dim MyDefaultGroup As New List(Of AllMyGroups)

    'Dim MyDtGroup As New AllGroupQueues

    Dim GroupOwnership As New List(Of String)

    Dim bHasError As Boolean
    Dim ssecuritylevel As String

    ' Dim InsFormViewRequest As New FormSQL()

    Dim FormViewRequest As New FormSQL()
    Dim ClientNum As Integer
    Dim ActionType As String
    Dim Securitylevel As Integer


    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        If Request.QueryString("ClientNum") <> "" Then
            ClientNum = Request.QueryString("ClientNum")
        Else
            Response.Redirect("./SimpleSearch.aspx", True)

        End If


        If Request.QueryString("ActionType") <> "" Then

            ActionType = Request.QueryString("ActionType")

        End If

        If Request.QueryString("SecurityLevel") <> "" Then

            ssecuritylevel = Request.QueryString("SecurityLevel")

        Else
            ssecuritylevel = ""

        End If

        Securitylevel = Session("SecurityLevelNum")

        If Request.QueryString("GroupNum") <> "" Then
            HDGroupNum.Text = Request.QueryString("GroupNum")

        End If

        FormViewRequest = New FormSQL(Session("EmployeeConn"), "SelTechniciansV4", "insUpdTechnicianV4", "", Page)
        'InsFormViewRequest = New FormSQL(Session("EmployeeConn"),  "SelTechniciansV4", "insUpdTechnicianV4", "", Page)

        useraccountsddl.BackColor() = Color.Yellow
        useraccountsddl.BorderColor() = Color.Black
        useraccountsddl.BorderStyle() = BorderStyle.Solid
        useraccountsddl.BorderWidth = Unit.Pixel(2)

        If Session("LoginID") <> "" Then
            txtntloginTextBox.Text = Session("LoginID").ToString

        End If
        If Not IsPostBack Then

            Dim rblAppgroupBinder As New RadioButtonListBinder(rblAppGgroup, "SelApplicationGroups", Session("EmployeeConn"))
            rblAppgroupBinder.BindData("ApplicationGroup", "DisplayGroupName") '"AffiliationDescLower",

            If ActionType = "Add" Then

                btnAddNewTech.Visible = True

                Dim c3Controller = New Client3Controller(ClientNum, "no", Session("EmployeeConn"))

                first_nameTextBox.Text = c3Controller.FirstName
                miTextBox.Text = c3Controller.MI
                last_nameTextBox.Text = c3Controller.LastName
                siemensempnumLabel.Text = c3Controller.SiemensEmpNum
                ntloginTextBox.Text = c3Controller.LoginName
                userpositiondescTextBox.Text = c3Controller.UserPositionDesc
                phoneTextBox.Text = c3Controller.Phone
                e_mailTextBox.Text = c3Controller.EMail

                'new_request_pageLabel.Text = c3Controller.n
                'TechGroupDddl.Enabled = False

            Else
                '@SecurityLevel

                btnUpdateTech.Visible = True

                If ssecuritylevel.ToLower = "deactivated" Then
                    FormViewRequest.AddSelectParm("@SecurityLevel", 16, SqlDbType.Int, 6)

                End If
                FormViewRequest.AddSelectParm("@txtntlogin", txtntloginTextBox.Text, SqlDbType.NVarChar, 50)

                FormViewRequest.AddSelectParm("@client_num", ClientNum, SqlDbType.Int, 6)

                FormViewRequest.FillForm()

                If ssecuritylevel.ToLower = "deactivated" Then
                    'btnDeactivate.Text = "Re-Activate Technicians"
                    btnDeactivate.Visible = False
                    btnUpdate.Visible = False

                    'lblGroups.Visible = False
                    'TechGroupDddl.Visible = False

                End If
                If ApplicationGroupTextBox.Text <> "" Then
                    rblAppGgroup.SelectedValue = ApplicationGroupTextBox.Text
                Else
                    rblAppGgroup.SelectedValue = "clinical"

                End If

            End If

            'Dim ddbTechGroup As New DropDownListBinder(TechGroupDddl, "SelTechGroup", Session("CSCConn"))
            'ddbTechGroup.AddDDLCriteria("@ntlogin", Session("LoginID"), SqlDbType.NVarChar)
            'ddbTechGroup.BindData("group_num", "group_name")

            Dim ddlBinder As New DropDownListBinder(useraccountsddl, "SelUserAccountTypes", Session("EmployeeConn"))
            ddlBinder.AddDDLCriteria("@ntLogin", Session("LoginID"), SqlDbType.NChar)
            ddlBinder.BindData("SecurityLevel", "UserType")

            If security_level_numLabel.Text <> "" Then

                useraccountsddl.SelectedValue = security_level_numLabel.Text

                If Convert.ToInt32(security_level_numLabel.Text) < 50 Or Convert.ToInt32(security_level_numLabel.Text) = 82 Then
                    'TechGroupDddl.Visible = False
                    cblGroups.Visible = False
                End If
            End If


            If new_request_pageLabel.Text <> "" Then

                rblP1email.SelectedValue = new_request_pageLabel.Text.ToLower

            End If


            BindGroups()

        End If

        If Session("SecurityLevelNum") < 79 Then

            'Dim ctrl As Control = DataCheckingAndValidation.SetChildControlsRecursiveToEnabledFalse(uplTicket, False)
            btnUpdateTech.Enabled = False
            btnAddNewTech.Visible = False
            btnDeactivate.Visible = False
        End If

        If Session("SecurityLevelNum") < 99 Then
            rblP1email.Enabled = False
        End If



        'exec SelUserAccountTypes @ntLogin = 'proe00'

    End Sub


    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("./UserMaintenance.aspx")

    End Sub
    Protected Sub BindGroups()
        AllGroupQueuesInfo.GetAllGroupQueues(HttpContext.Current.Session("CSCConn").ToString, "", Session("LoginID"))

        allgrouplist.Clear()
        MyGrouplist.Clear()


        allgrouplist = AllGroupQueuesInfo.AllGroups




        Dim technum As Integer
        If tech_numLabel.Text <> "" Then
            technum = Convert.ToInt32(tech_numLabel.Text)
        Else
            Exit Sub
        End If

        AllMyGroupInfo.GetMyQueues(HttpContext.Current.Session("CSCConn").ToString, technum)


        MyGrouplist = AllMyGroupInfo.AllMyGroups()


        MyDefaultGroup = AllMyGroupInfo.MydefaultGroup()
        Dim securitylevel As Integer

        securitylevel = MyGrouplist.Item(0).SecurityLevel


        'MyDTGroup = ToDataTable(MyGrouplist)


        GroupOwnership = AllMyGroupInfo.GroupOwnership

        'cblGroups
        'cblGroups.Dispose()

        'cblGroups.DataSource = allgrouplist

        'cblGroups.DataValueField = "GroupNum"
        'cblGroups.DataTextField = "GroupName"
        'cblGroups.DataBind()


        'Dim checkBoxes = From it As ListItem In cblGroups.Items
        '     Join own As String In GroupOwnership On it.Value Equals own.ToString
        '     Select it
        'For Each itm In checkBoxes
        '    itm.Selected = True
        '    ' Email this group or not and change color


        '    If MyGrouplist.Item(1).ToString.ToLower = "yes" Then
        '        itm.Text = itm.Text & " Receives Email "

        '    Else
        '        itm.Text = itm.Text & " No Email"


        '    End If



        'Next

        'cblGroups.Visible = False

        If securitylevel > 64 Then


            GetMyQueue()

            Dim ddDefGroup As New DropDownListBinder(DefGroupddl, "SelGroupsByTechNumV8", Session("CSCConn"))
            ddDefGroup.AddDDLCriteria("@techNum", tech_numLabel.Text, SqlDbType.NVarChar)
            ddDefGroup.BindData("group_num", "group_name")

            'AllMyGroupInfo.GetMyQueues(HttpContext.Current.Session("CSCConn").ToString, technum)



            If MyDefaultGroup.Count > 0 Then

                DefGroupddl.BackColor() = Color.LightCyan
                DefGroupddl.BorderColor() = Color.Black
                DefGroupddl.BorderStyle() = BorderStyle.Solid
                DefGroupddl.BorderWidth = Unit.Pixel(2)

                defaultGrpName.Text = MyDefaultGroup.Item(0).GroupNum.ToString

                DefGroupddl.SelectedValue = defaultGrpName.Text.Trim

            End If

            If HDGroupNum.Text <> "" Then


                Dim sGroupName As String

                MyDefaultGroup = AllMyGroupInfo.MySelectedGroup(CInt(HDGroupNum.Text))


                sGroupName = MyDefaultGroup.Item(0).GroupName
                lblSelectedGroupName.Text = MyDefaultGroup.Item(0).GroupName
                hdEmail.Text = MyDefaultGroup.Item(0).EmailGroup

                'MySelectedGroup(HDGroupNum.Text)
                If hdEmail.Text = "N/A" Then
                    lblAddGroup.Text = "Add Tech to this Group"
                    lblemail.Text = "Receive Email for this Group"

                ElseIf hdEmail.Text.ToLower = "no" Then
                    lblemail.Text = "Receive Email for this Group"
                    rblEmailMe.SelectedValue = "no"

                    lblAddGroup.Text = "Remove Tech from this Group"
                    ChkAddgroup.SelectedValue = "no"
                    HDType.Text = "keep"
                Else
                    lblemail.Text = "Receive Email for this Group"
                    rblEmailMe.SelectedValue = "yes"


                    lblAddGroup.Text = "Remove Tech from this Group"
                    ChkAddgroup.SelectedValue = "no"
                    HDType.Text = "keep"
                End If

                AddEmailPopup.Show()

            End If

        Else
            rblP1email.Visible = False
            lblP1email.Visible = False

            defaultGrplbl.Visible = False
            DefGroupddl.Visible = False

            lblAppgroup.Visible = False
            rblAppGgroup.Visible = False

            tblMyGroupAccountQueues1.Visible = False
            tblMyGroupAccountQueues2.Visible = False

        End If

    End Sub
    Public Sub GetMyQueue()

        Dim appHeaderCreated As Boolean = False
        Dim iOrderBy As String
        Dim currGroupNum As Integer


        appHeaderCreated = False

        Dim iRowCount As Integer = 1
        Dim iRowCountItems As Integer = 0


        For Each ari As AllMyGroups In MyGrouplist


            currGroupNum = 0

            iOrderBy = ari.GroupName
            currGroupNum = ari.GroupNum

            If iRowCountItems = 0 Or iRowCountItems = 11 Then
                '*** Create header and write 

                'rwSuperHeader.Font.Bold = True
                'rwSuperHeader.BackColor = Color.FromName("#FFFFCC")
                'rwSuperHeader.ForeColor = Color.FromName("Black")
                'rwSuperHeader.Font.Size = 12


                Dim rwHeader As New TableRow
                Dim rwColTitle As New TableRow

                Dim clGroupNumTitle As New TableCell
                Dim clGroupNameTitle As New TableCell

                Dim clGroupStatusTitle As New TableCell
                Dim clGroupEmailTitle As New TableCell

                'Dim clGroupNameTitle As New TableCell
                'Dim clTechNameTitle As New TableCell
                'Dim clBDescTitle As New TableCell
                'Dim cltypeTitle As New TableCell

                'rwHeader.Font.Bold = True
                'rwHeader.BackColor = Color.FromName("#FFFFCC")
                'rwHeader.ForeColor = Color.FromName("Black")
                'rwHeader.Font.Size = 10

                'If Session("ViewRequestType") = "open" Then
                '    clHeader.Text = ari.GroupName & " Group "

                'ElseIf Session("ViewRequestType") = "closed" Then
                '    clHeader.Text = ari.GroupName & " Closed Cases "
                'Else
                '    clHeader.Text = ari.RequestTypeDesc & " PAST 30 Days Invalid Requests"
                'End If



                rwColTitle.Font.Bold = True
                rwColTitle.BackColor = Color.FromName("#006666")
                rwColTitle.ForeColor = Color.FromName("White")
                rwColTitle.Height = Unit.Pixel(36)

                clGroupNumTitle.Text = " "
                clGroupNameTitle.Text = "Group Name"
                clGroupStatusTitle.Text = "Status "
                clGroupEmailTitle.Text = "Email "

                'cltypeTitle.Text = "Cat. Type"
                'clpriorityTitle.Text = "Priority"
                'clFacilityTitle.Text = "Facility"
                'clGroupNameTitle.Text = "Group Name"
                'clTechNameTitle.Text = "Tech Name"


                rwColTitle.Cells.Add(clGroupNumTitle)
                rwColTitle.Cells.Add(clGroupNameTitle)
                rwColTitle.Cells.Add(clGroupStatusTitle)
                rwColTitle.Cells.Add(clGroupEmailTitle)


                'rwColTitle.Cells.Add(clpriorityTitle)
                'rwColTitle.Cells.Add(clBDescTitle)
                'rwColTitle.Cells.Add(clGroupNameTitle)
                'rwColTitle.Cells.Add(clTechNameTitle)

                rwColTitle.Font.Size = 10


                If iRowCountItems = 0 Then

                    tblMyGroupAccountQueues1.Rows.Add(rwColTitle)

                ElseIf iRowCountItems = 11 Then

                    tblMyGroupAccountQueues2.Rows.Add(rwColTitle)

                End If


                appHeaderCreated = True

            End If

            '----------------------------------------
            ' End Header
            '----------------------------------------

            If currGroupNum > 0 Then


                Dim rwData As New TableRow
                Dim clGroupNum As New TableCell
                Dim clGroupName As New TableCell

                Dim clStatus As New TableCell
                Dim clEmail As New TableCell

                'Dim clReqType As New TableCell
                'Dim clCategory As New TableCell
                'Dim clbDesc As New TableCell
                'Dim clTimeInQueue As New TableCell
                'Dim clpriority As New TableCell
                'Dim clFacility As New TableCell
                'Dim clTechName As New TableCell


                'If Session("ViewRequestType") = "" Then
                '    Session("ViewRequestType") = "open"
                'End If
                'clGroupNum.Text = "<a href=""#"" id=""btnSelectQueue""> " & "Select" & "</a>"


                'OnClientClick="Display()"UserMaintDetail.aspx?ClientNum=" & ClientNum 

                clGroupNum.Text = "<a href=""UserMaintDetail.aspx?ClientNum=" & ClientNum & "&GroupNum=" & ari.GroupNum & """><u><b> " & "Select" & "</b></u></a>"

                'clGroupNum.Text = ari.GroupNum

                clGroupName.Text = ari.GroupName
                clStatus.Text = ari.displaystatus
                clEmail.Text = ari.EmailGroup

                'clpriority.Text = ari.priority
                'clFacility.Text = ari.facility_name
                'clTechName.Text = ari.TechName
                'clTimeInQueue.Text = ari.TimeInQueue

                rwData.Cells.Add(clGroupNum)
                rwData.Cells.Add(clGroupName)
                rwData.Cells.Add(clStatus)
                rwData.Cells.Add(clEmail)

                'rwData.Cells.Add(clReqType)
                'rwData.Cells.Add(clFacility)
                'rwData.Cells.Add(clpriority)
                'rwData.Cells.Add(clbDesc)


                rwData.Font.Size = 10

                'P1 backgroud
                If clEmail.Text.ToLower = "yes" Then
                    clGroupNum.ForeColor = Color.Red
                    clGroupName.ForeColor = Color.Red
                    clStatus.ForeColor = Color.Red
                    clEmail.ForeColor = Color.Red

                    clEmail.Text = "Receive Email"

                ElseIf clEmail.Text.ToLower = "no" Then
                    clEmail.Text = "No Email"

                    clGroupNum.ForeColor = Color.Green
                    clGroupName.ForeColor = Color.Green
                    clStatus.ForeColor = Color.Green
                    clEmail.ForeColor = Color.Green

                Else
                    clEmail.Text = "N/A"

                End If

                If clStatus.Text.ToLower = "queueonly" Then
                    clStatus.Text = "Queue Only"
                Else
                    clStatus.Text = "Queue/On Call"

                End If

                If iRowCount > 0 Then

                    ' rwData.BackColor = Color.Bisque

                    rwData.BackColor = Color.WhiteSmoke

                Else

                    rwData.BackColor = Color.LightGray

                End If



                If iRowCountItems < 11 Then

                    tblMyGroupAccountQueues1.Rows.Add(rwData)

                ElseIf iRowCountItems >= 11 Then

                    tblMyGroupAccountQueues2.Rows.Add(rwData)

                End If



                iRowCount = iRowCount * -1

                iRowCountItems = iRowCountItems + 1
            End If

        Next

        'tblMyGroupAccountQueues1.Width = New Unit("100%")
        'tblMyGroupAccountQueues1.Height = New Unit("100%")

        'tblMyGroupAccountQueues1.Visible = True


        'tblMyGroupAccountQueues2.Width = New Unit("100%")
        'tblMyGroupAccountQueues2.Height = New Unit("100%")

        'tblMyGroupAccountQueues2.Visible = True



    End Sub


    Public Function ToDataTable(Of T)(ByVal items As List(Of t)) As DataTable

        Dim dataTable As DataTable = New DataTable(GetType(T).Name)
        Dim Props As Reflection.PropertyInfo() = GetType(T).GetProperties(Reflection.BindingFlags.[Public] Or Reflection.BindingFlags.Instance)

        For Each prop As Reflection.PropertyInfo In Props
            Dim type = (If(prop.PropertyType.IsGenericType AndAlso prop.PropertyType.GetGenericTypeDefinition() = GetType(Nullable(Of )), Nullable.GetUnderlyingType(prop.PropertyType), prop.PropertyType))
            dataTable.Columns.Add(prop.Name, type)
        Next

        For Each item As T In items
            Dim values = New Object(Props.Length - 1) {}

            For i As Integer = 0 To Props.Length - 1
                values(i) = Props(i).GetValue(item, Nothing)
            Next

            dataTable.Rows.Add(values)
        Next

        Return dataTable


    End Function
    Protected Sub btnAddNewTech_Click(sender As Object, e As System.EventArgs) Handles btnAddNewTech.Click

        If ActionType = "Add" Then

            lblValidation.Text = ""

            If security_level_numLabel.Text = "" Then
                lblValidation.Text = "Must supply Security Level"
                lblValidation.ForeColor = Color.Red
                lblValidation.Focus()

                useraccountsddl.ForeColor = Color.Red
                Exit Sub

            End If



            CheckRequiredFields()

            If bHasError = True Then
                lblValidation.Focus()
                Exit Sub
            Else
                lblValidation.Text = ""
            End If

            FormViewRequest.AddInsertParm("@client_num", ClientNum, SqlDbType.Int, 9)

            'FormViewRequest.AddInsertParm("@txtntlogin", txtntloginTextBox.Text, SqlDbType.NVarChar, 50)

            FormViewRequest.UpdateFormReturnTechNum()

            tech_numLabel.Text = FormViewRequest.Technum

            If tech_numLabel.Text <> "" Then

                Response.Redirect("./UserMaintDetail.aspx?ClientNum=" & ClientNum & "&SecurityLevel=" & security_level_numLabel.Text, True)

            End If
        End If

    End Sub

    Protected Sub btnUpdate_Click(sender As Object, e As System.EventArgs) Handles btnUpdate.Click

        'FormViewRequest.AddInsertParm("@clientnum", iClientNum, SqlDbType.Int, 9)


        If ActionType = "Add" Then

            lblValidation.Text = ""

            If security_level_numLabel.Text = "" Then
                lblValidation.Text = "Must supply Security Level"
                lblValidation.ForeColor = Color.Red
                lblValidation.Focus()

                useraccountsddl.ForeColor = Color.Red
                Exit Sub

            End If



            CheckRequiredFields()

            If bHasError = True Then
                lblValidation.Focus()
                Exit Sub
            Else
                lblValidation.Text = ""
            End If

            FormViewRequest.AddInsertParm("@client_num", ClientNum, SqlDbType.Int, 9)

            'FormViewRequest.AddInsertParm("@txtntlogin", txtntloginTextBox.Text, SqlDbType.NVarChar, 50)

            FormViewRequest.UpdateFormReturnTechNum()

            tech_numLabel.Text = FormViewRequest.Technum

            If tech_numLabel.Text <> "" Then

                Response.Redirect("./UserMaintDetail.aspx?ClientNum=" & ClientNum & "&SecurityLevel=" & security_level_numLabel.Text, True)

            End If
        Else

            FormViewRequest.AddInsertParm("@client_num", ClientNum, SqlDbType.Int, 9)
            'FormViewRequest.AddInsertParm("@txtntlogin", txtntloginTextBox.Text, SqlDbType.NVarChar, 50)

            FormViewRequest.UpdateForm()


        End If

        If HDGroupNum.Text <> "" And tech_numLabel.Text <> "" Then

            Dim groupnum As Integer
            Dim technum As Integer

            groupnum = Convert.ToInt32(HDGroupNum.Text)
            technum = Convert.ToInt32(tech_numLabel.Text)


            Dim thisdata As New CommandsSqlAndOleDb("InsUpdTechGroup", Session("CSCConn"))
            thisdata.AddSqlProcParameter("@groupnum", groupnum, SqlDbType.Int, 9)
            thisdata.AddSqlProcParameter("@technum", technum, SqlDbType.Int, 8)
            thisdata.AddSqlProcParameter("@Type", HDType.Text, SqlDbType.NVarChar, 12)
            thisdata.AddSqlProcParameter("@emailapp", hdEmail.Text, SqlDbType.NVarChar, 6)



            thisdata.ExecNonQueryNoReturn()

            BindGroups()



            HDGroupNum.Text = ""

            'TechGroupDddl.SelectedIndex = -1

        End If

        If defaultGrpName.Text <> "" Then

            Dim thisdata As New CommandsSqlAndOleDb("UpdDefaultTechGroup", Session("CSCConn"))
            thisdata.AddSqlProcParameter("@groupnum", defaultGrpName.Text, SqlDbType.Int)
            thisdata.AddSqlProcParameter("@technum", tech_numLabel.Text, SqlDbType.Int)
            thisdata.ExecNonQueryNoReturn()

        End If

        HDGroupNum.Text = ""
        AddEmailPopup.Hide()

        Response.Redirect("./UserMaintDetail.aspx?ClientNum=" & ClientNum & "&SecurityLevel=" & security_level_numLabel.Text, True)

    End Sub

    'Protected Sub TechGroupDddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles TechGroupDddl.SelectedIndexChanged
    '    hdnewgroup.Text = TechGroupDddl.SelectedValue

    'End Sub
    Public Function CheckRequiredFields() As Boolean
        Dim removeCss As New PageControls(Page)
        removeCss.RemoveCssClass(Page, "textInputRequired")

        Dim SecurLeve As New RequiredField(useraccountsddl, "Security Level")


        useraccountsddl.BackColor() = Color.Yellow
        useraccountsddl.BorderColor() = Color.Black
        useraccountsddl.BorderStyle() = BorderStyle.Solid
        useraccountsddl.BorderWidth = Unit.Pixel(2)

        Return bHasError

    End Function

    Protected Sub useraccountsddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles useraccountsddl.SelectedIndexChanged
        security_level_numLabel.Text = useraccountsddl.SelectedValue
    End Sub

    Protected Sub btnDeactivate_Click(sender As Object, e As System.EventArgs) Handles btnDeactivate.Click
        ' UpdTechGroups
        lblValidation.Text = ""


        CheckRequiredFields()

        If bHasError = True Then
            lblValidation.Focus()
            Exit Sub
        Else
            lblValidation.Text = ""
        End If

        If Convert.ToInt32(security_level_numLabel.Text) < 17 Then
            lblValidation.Text = "Must supply Security Level"
            lblValidation.ForeColor = Color.Red
            lblValidation.Focus()

            useraccountsddl.ForeColor = Color.Red
            Exit Sub

        End If

        If tech_numLabel.Text <> "" Then

            Dim technum As Integer

            technum = Convert.ToInt32(tech_numLabel.Text)


            Dim thisdata As New CommandsSqlAndOleDb("UpdTechGroups", Session("CSCConn"))
            thisdata.AddSqlProcParameter("@technum", technum, SqlDbType.Int, 8)

            If ssecuritylevel.ToLower = "deactivated" Then ' if its deactivate then you click to Activate it
                thisdata.AddSqlProcParameter("@status", "activate", SqlDbType.NVarChar, 12)

            Else
                thisdata.AddSqlProcParameter("@status", "deactivate", SqlDbType.NVarChar, 12)


            End If
            thisdata.ExecNonQueryNoReturn()

            If ssecuritylevel.ToLower <> "deactivated" Then
                ' if tech is on call 
                Dim OnCalldata As New CommandsSqlAndOleDb("InsUpdOnCallTech", Session("CSCConn"))
                OnCalldata.AddSqlProcParameter("@technum", technum, SqlDbType.Int, 8)
                OnCalldata.AddSqlProcParameter("@status", "deactivate", SqlDbType.NVarChar, 12)

                OnCalldata.ExecNonQueryNoReturn()
            End If

            'InsUpdOnCallTech


            If ssecuritylevel.ToLower = "deactivated" Then ' if its deactivate then you click to Activate it
                ' this re-activates tech
                Dim iSecuritylevel As Integer = 0


                If Convert.ToInt32(security_level_numLabel.Text) < 17 Then
                    iSecuritylevel = 25
                    security_level_numLabel.Text = "25"

                End If
                date_deactivatedlabel.Text = ""

                'FormViewRequest.AddInsertParm("@security_level_num", Securitylevel, SqlDbType.Int, 6)
                FormViewRequest.AddInsertParm("@client_num", ClientNum, SqlDbType.Int, 6)
                ' FormViewRequest.AddSelectParm("@date_deactivated", "", SqlDbType.DateTime, 24)


                FormViewRequest.UpdateForm()


                BindGroups()

                hdnewgroup.Text = ""
                date_deactivatedlabel.Text = ""

                'TechGroupDddl.SelectedIndex = -1

                btnDeactivate.Text = "Deactivate Technician"
                btnUpdate.Visible = True

                'lblGroups.Visible = True
                'TechGroupDddl.Visible = True

                iSecuritylevel = Nothing
                ssecuritylevel = ""

                Response.Redirect("./UserMaintDetail.aspx?ClientNum=" & ClientNum & "&SecurityLevel=" & ssecuritylevel, True)
            Else
                ' this deactivates tech

                date_deactivatedlabel.Text = Today.Date()

                security_level_numLabel.Text = "16"


                FormViewRequest.AddInsertParm("@client_num", ClientNum, SqlDbType.Int, 6)
                'FormViewRequest.AddInsertParm("@date_deactivated", date_deactivatedlabel.Text, SqlDbType.DateTime, 24)


                FormViewRequest.UpdateForm()

                ' remove tech groups
                Dim DelTechdata As New CommandsSqlAndOleDb("UpdTechGroups", Session("CSCConn"))
                DelTechdata.AddSqlProcParameter("@technum", technum, SqlDbType.Int, 8)

                DelTechdata.AddSqlProcParameter("@status", "deactivate", SqlDbType.NVarChar, 12)


                DelTechdata.ExecNonQueryNoReturn()

                Response.Redirect("./UserMaintenance.aspx", True)


            End If





        End If

    End Sub

    Protected Sub rblAppGgroup_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles rblAppGgroup.SelectedIndexChanged
        ApplicationGroupTextBox.Text = rblAppGgroup.SelectedValue
    End Sub

    Protected Sub DefGroupddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles DefGroupddl.SelectedIndexChanged
        defaultGrpName.Text = DefGroupddl.SelectedValue

    End Sub


    Protected Sub rblEmailMe_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles rblEmailMe.SelectedIndexChanged
        hdEmail.Text = rblEmailMe.SelectedValue
    End Sub

    Protected Sub ChkAddgroup_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ChkAddgroup.SelectedIndexChanged
        If hdEmail.Text = "N/A" Then
            ' no email or group so add
            If ChkAddgroup.SelectedValue = "yes" Then
                HDType.Text = "add"

            End If

        Else
            ' has an email or not remove from the group
            If ChkAddgroup.SelectedValue = "yes" Then
                HDType.Text = "remove"
            End If

        End If
    End Sub

    Protected Sub btnUpdateTech_Click(sender As Object, e As System.EventArgs) Handles btnUpdateTech.Click

        If defaultGrpName.Text <> "" Then

            Dim thisdata As New CommandsSqlAndOleDb("UpdDefaultTechGroup", Session("CSCConn"))
            thisdata.AddSqlProcParameter("@groupnum", defaultGrpName.Text, SqlDbType.Int)
            thisdata.AddSqlProcParameter("@technum", tech_numLabel.Text, SqlDbType.Int)
            thisdata.ExecNonQueryNoReturn()

        End If


        FormViewRequest.AddInsertParm("@client_num", ClientNum, SqlDbType.Int, 9)

        FormViewRequest.UpdateForm()

        Response.Redirect("./UserMaintDetail.aspx?ClientNum=" & ClientNum & "&SecurityLevel=" & security_level_numLabel.Text, True)


        ' BindGroups()


    End Sub

    Protected Sub rblP1email_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles rblP1email.SelectedIndexChanged
        new_request_pageLabel.Text = rblP1email.SelectedValue
    End Sub
End Class
