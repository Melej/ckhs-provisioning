﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="false" CodeFile="CreateDeleteAccount.aspx.vb" Inherits="CreateDeleteAccount" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

<asp:UpdatePanel runat="server" ID="uplPage" UpdateMode="Conditional">
<ContentTemplate>
  <ajaxToolkit:TabContainer ID="tabs" runat="server" ActiveTabIndex="0"
          Width="100%" style="margin-bottom: 1px" EnableTheming="False" 
          BackColor="#efefef">

    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" TabIndex="0"  BackColor="Gainsboro" Visible = "True">
        <HeaderTemplate > 
            <asp:Label ID="Label2" runat="server" Text="This Account Request" Font-Bold="true"></asp:Label>
        </HeaderTemplate>
         <ContentTemplate>


             <table border="3" id="tblHeader" style="empty-cells:show" cellpadding="5px" 
                 width="100%">
           <tr>
                <th colspan = "4" class="tableRowHeader">
                  Create Account Request
                </th>     
           </tr>
          
           <tr>
            <td colspan = "2" style="font-weight:bold">
               Client Name
            </td>
             <td colspan = "2" >
       
                <asp:Label ID = "first_namelabel" runat = "server"></asp:Label>&nbsp<asp:Label ID = "last_namelabel" runat = "server" ></asp:Label>
            </td>
            
           </tr>
           <tr>
            <td colspan = "2" style="font-weight:bold">
              Requestor Name
            </td>
           <td colspan = "2" >
                <asp:Label ID = "requestor_namelabel" runat = "server"></asp:Label>
            </td>
           </tr>
           <tr>
                 <td colspan = "2" style="font-weight:bold">
                    Request Type
                </td>     
         
                 <td colspan = "2" >
                     <asp:Label ID = "RequestTypeDesclabel" runat = "server"></asp:Label>
                </td>     
           </tr>
           <tr>
               
                  <td style="font-weight:bold" colspan="2">
                 Submit Date
                  </td>
        
               
          
                <td colspan="2">
                    <asp:Label ID = "entered_datelabel" runat = "server"></asp:Label>
                </td>
            </tr>
           
            <tr>
                <td colspan="4"  >
                  <table>
                    <tr>
                        <td>
                        </td>
                        <td>
                             <asp:Button ID = "btnCreateSubmit" runat = "server" Text ="Submit"  Width="125" BackColor="#336666" ForeColor="White"/>
                         </td>

                        <td>
                              <asp:Button ID = "btnInvalid" runat = "server" Text ="Invalid"  Width="125" BackColor="#336666" ForeColor="White"/>
                        </td>
                        <td>
                            <asp:Button ID = "btnSaveItemInfo" runat = "server" Text ="Save Comments"  Width="175" BackColor="#336666" ForeColor="White" />
                        </td>

                        
                    </tr>
                  
                  </table>



                </td>
            </tr>

            <tr>
                <td colspan = "4" class ="tableRowSubHeader">
                Account Information
                </td>
            </tr>
            <tr>
                <td style="font-weight:bold" >
                  Account
                </td>
                <td colspan = "3">
                    <asp:Label ID ="AccountLabel" runat = "server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="font-weight:bold" >
                   User Name
                </td>
                <td colspan="3">
                    <asp:TextBox runat = "server" ID = "LoginIDTextBox"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="font-weight:bold" valign="top" colspan="4">
                    Comments
                </td>
            </tr>
            <tr>
                <td colspan = "4">
                
                 <asp:TextBox runat = "server" ID = "CommentsTextBox" TextMode="MultiLine" Width="98%" Rows="5"></asp:TextBox>
                 <br />
                 <br />
                 <span style="float:right"> </span>
                </td>
            </tr>
            <tr>
                <td colspan = "4" >
                <span style="float:center">
                    </span>

                                        <asp:Button runat="server" ID ="btnRequestDetails" Text ="Request Details" Visible="false" />

                </td>
            </tr>
        </table>
        </ContentTemplate>
     </ajaxToolkit:TabPanel>


     <ajaxToolkit:TabPanel ID="tpDemographics" runat="server" TabIndex="0"  BackColor="Gainsboro" Visible = "True">
        <HeaderTemplate > 
            <asp:Label ID="Label1" runat="server" Text="Request" Font-Bold="true"></asp:Label>
                    
        </HeaderTemplate>
                      <ContentTemplate>
                        <table border="3" style="background-color: gainsboro">
                                    <tr>
                                        <td colspan = "4" align ="center">
                                            <asp:Label ID = "lblValidation" runat = "server" ForeColor="Red"/>
                                        </td>
            
                                    </tr>
                                    <%--<tr>
                                        <td colspan = "4">
                                            <asp:Button ID = "btnSubmit" runat = "server" Text ="Submit" Width="75" />
                                            <asp:Button ID = "btnReturn" runat = "server" Text ="Return" Width="75" />
                                        </td>
            
                                    </tr>--%>
                                    <tr>
                                             <td colspan = "4" class="tableRowHeader">
                                                Account Request Information
                                             </td>
                                    </tr>
                                    <tr>
          
                                        <td colspan="4" class="tableRowSubHeader"  align="left">
                                       Form Submission
                                        </td>
            
                                    </tr>
                                    <tr>
                                        <td colspan = "4" align="center">
                                            <asp:Button ID = "btnUpdate" runat = "server" Text ="Update" Width="75" BackColor="#336666" ForeColor="White" />
              
                                        </td>
                                    </tr>
                                    <tr>
                                    <td colspan="4">
                                       User submitting:         
                                       <asp:Label ID = "submitter_nameLabel" runat="server" Font-Bold="true"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                        Requestor:   <asp:Label ID = "lbRequestor" runat="server" Font-Bold="true"></asp:Label>
            
              
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" class="tableRowSubHeader"  align="left">
                                        CKHS Affiliation:
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                            <asp:RadioButtonList ID ="emp_type_cdrbl" runat = "server" RepeatDirection="Horizontal"  AutoPostBack="true" Font-Size="Smaller">
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>


                                                                    <tr>
                                   <td colspan="4">
                                     <asp:RadioButtonList ID ="RadioButtonList1" runat = "server" RepeatDirection="Horizontal" AutoPostBack="true">
                                     </asp:RadioButtonList>

                                     <asp:Panel runat="server" ID ="pnlAlliedHealth" Visible="false">
                                        <table width="100%">
                                          <tr>
                                            <td width="30%">
                                                 Allied Health Degree:
                                            </td>
                                            <td>
                                               <asp:DropDownList runat="server"  width="40%" ID = "OtherAffDescddl">
                                                </asp:DropDownList> 
                                            </td>
                                          </tr>
                
                                        </table>
                                     </asp:Panel>
                                    </td>
                              </tr>

                                    <asp:Panel runat="server" ID="pnlProvider" Visible="false">
                                      <tr>
                                        <td colspan="4" class="tableRowSubHeader" align="left">
                                            Provider Information
                                        </td>
                                      </tr>

                                   
                                      <tr>
                                         <td>
                                                Provider:
                                          </td>
                                           <td colspan="3">
                                            <asp:RadioButtonList ID = "providerrbl" runat="server" RepeatDirection="Horizontal" Enabled = "false">
                                                <asp:ListItem>Yes</asp:ListItem>
                                                    <asp:ListItem >No</asp:ListItem>            
                                                </asp:RadioButtonList>
                                            </td>

                                     </tr>
                                      <tr>
                                        <td>
                                        Credentialed Date:
                                        </td>
                                        <td colspan="3">
                                            <asp:TextBox ID = "credentialedDateTextBox" runat="server" CssClass="calenderClass"></asp:TextBox>
                                        </td>
                       
                                     </tr>
                                      <tr>
                                        <td>
                                         CKHN-HAN:
                                        </td>
                                        <td colspan="3">
                                            <asp:RadioButtonList ID = "hanrbl" runat="server" RepeatDirection="Horizontal">
                                                <asp:ListItem>Yes</asp:ListItem>
                                                <asp:ListItem Selected="True">No</asp:ListItem>            
                                            </asp:RadioButtonList>
                                        </td>
                    
                                     </tr>
                                      <tr>
                                        <td>
                                            NPI
                                        </td>
                                        <td >
                                            <asp:TextBox ID = "npitextbox" runat = "server"></asp:TextBox>
                                        </td>
                                        <td>
                                            License No.
                                        </td>
                                        <td>
                                             <asp:TextBox ID = "LicensesNo" runat="server"></asp:TextBox>
                                        </td>
                                     </tr>
                                      <tr>
                                        <td>
                                            Taxonomy
                                         </td>
                                        <td colspan ="3">
                                            <asp:TextBox ID = "taxonomytextbox" runat="server">
                                          </asp:TextBox>
                                        </td>
        
                                    </tr>
                                      <tr>
                                          <td>
                                            Group Name

                                            </td>
                                            <td>
                                                <asp:TextBox ID="group_nameTextBox" runat="server"></asp:TextBox>
                                            </td>
                                            <td>
                                                Group Number

                                            </td>
                                            <td>
                                                <asp:TextBox ID="group_numTextBox" runat="server"></asp:TextBox>
                                            </td>
                                    </tr>
                                      <tr>
                                        <td>
                                        Practice Name:
                                        </td>       
                                        <td colspan="3">
                                        <asp:DropDownList ID ="practice_numddl"  width = "95%" runat="server">
            
                                        </asp:DropDownList>
                                        </td>
                                    </tr>
                                      <tr>
                                       <td>Title:</td>
                                        <td>
                                           <asp:dropdownlist ID = "Titleddl" runat="server"></asp:dropdownlist>
                                        </td>
                                        <td >
                                        Specialty
                                        </td>
                                        <td>
                                           <asp:dropdownlist ID = "Specialtyddl" runat="server"></asp:dropdownlist>
                                        </td>
                                    </tr>
                                      <tr>
                                        <td>
                                            Non-CKHN Location
                                        </td>
                                        <td colspan="3">
                                              <asp:TextBox ID = "NonHanLocationTextBox" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                   
                                   </asp:Panel> 
                                      <tr>
                                        <td colspan="4" class="tableRowSubHeader"  align="left">
                                            Demographics
                                        </td>
                                      </tr>
                                      <tr>
                                        <td>
                                        First Name:           
            
                                        </td>
                                         <td>
                                        <asp:TextBox ID = "first_nameTextBox" runat="server" CssClass="textInput"></asp:TextBox>
                                          M:
                                             <asp:TextBox ID = "middle_initialTextBox" runat="server" Width="15px"></asp:TextBox>
                                        </td>
        
                                      <td>
                                        Last Name:
                                        </td>
                                         <td>
                                        <asp:TextBox ID = "last_nameTextBox" runat="server"></asp:TextBox>
                                        Suffix: 
                                        <asp:dropdownlist id="suffixddl"
				                            tabIndex="3" runat="server" Height="20px" Width="52px">
				                            <asp:ListItem Value="------" Selected="True">------</asp:ListItem>
				                            <asp:ListItem Value="Sr.">Sr.</asp:ListItem>
				                            <asp:ListItem Value="Jr.">Jr.</asp:ListItem>
				                            <asp:ListItem Value="II">II</asp:ListItem>
				                            <asp:ListItem Value="III">III</asp:ListItem>
				                            <asp:ListItem Value="IV">IV</asp:ListItem>
                                            </asp:dropdownlist>


                                        </td>
                                    </tr>
                                      <tr>
                                        <td>
                                            Position Description:
                                        </td>
                                        <td colspan = "3">
                                            <asp:TextBox ID="user_position_descTextBox" runat="server" Width = "200px"></asp:TextBox>
                                        </td>
                                    </tr>
                                      <tr>
                                        <td>
                                        Entity:
                                        </td>
                                        <td>
                                        <asp:DropDownList ID ="entity_cdDDL" runat="server" AutoPostBack ="true">
            
                                        </asp:DropDownList>
                                        </td>
       
                                        <td>
                                        Department Name:
                                        </td>
                                        <td>
                                        <asp:DropDownList ID ="department_cdddl" runat="server">
            
                                        </asp:DropDownList>
                                        </td>
                                    </tr>
                                      <tr>
                                        <td>Address 1:</td>
                                        <td>  <asp:TextBox ID = "address_1textbox" runat="server"></asp:TextBox></td>
                                        <td>City:</td>
                                        <td>  <asp:TextBox ID = "citytextbox" runat="server"></asp:TextBox></td>
                                    </tr>
                                      <tr>
                                        <td>Address 2:</td>
                                        <td>  <asp:TextBox ID = "address_2textbox" runat="server"></asp:TextBox></td>
                                        <td>State:<asp:TextBox ID = "statetextbox" runat="server" Width="25px"></asp:TextBox></td>
                                        <td>Zip:  <asp:TextBox ID = "ziptextbox" runat="server" Width="50px"></asp:TextBox></td>
                                    </tr>
                                      <tr>
                                        <td>
                                        Phone Number:
                                        </td>
                                        <td>
                                        <asp:TextBox ID = "phoneTextBox" runat="server"></asp:TextBox>
                                        </td>
       
                                        <td>
                                        Pager Number:
                                        </td>
                                        <td>
                                       <asp:TextBox ID = "pagerTextBox" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>

                                      <tr>
                                       <td>Fax:</td>
                                        <td>
            
                                            <asp:TextBox ID = "faxtextbox" runat="server"></asp:TextBox></td>
           
           
                                         <td>
                                        Email:
                                        </td>
                                        <td>
                                       <asp:TextBox ID = "e_mailTextBox" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                      <tr>
                                        <td>
                                        Location
                                        </td>
                                        <td>
         
                                          <asp:DropDownList ID ="facility_cdddl" runat="server" AutoPostBack="true">
              
                                        </asp:DropDownList>
          
                                        </td>
        
                                        <td>
                                        Building
                                        </td>
                                        <td>
                                        <asp:TextBox ID = "building_cdTextBox" runat="server" Visible ="false"></asp:TextBox>
                                         <asp:DropDownList ID ="building_cdddl" runat="server">
            
                                        </asp:DropDownList>
          
                                        </td>
                                    </tr>
                                      <tr>
          
                                        <td>Vendor Name</td>
                                        <td colspan="3"><asp:TextBox ID ="VendorNameTextBox" runat="server" ></asp:TextBox></td>
                                    </tr>
                                      <tr>
                                        <td>
                                        Start Date:
                                        </td>
                                        <td>
                                           <asp:TextBox ID ="start_dateTextBox" runat="server" CssClass="calenderClass"></asp:TextBox>
                                        </td>
       
                                        <td>
                                        End Date:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="end_dateTextBox" runat="server" CssClass="calenderClass"></asp:TextBox>
                                        </td>
                                      </tr>
                                      <tr>
                                       
                                        <td>
                                            Share Drive
                                        </td>
                                        <td >
                                             <asp:TextBox ID="share_driveTextBox" runat="server"></asp:TextBox>
                                        </td>
                                        <td>
                                            Model After:
                                        </td>
                                        <td> 
                                            <asp:TextBox ID="ModelAfterTextBox" runat="server"></asp:TextBox>
                                        </td>
                                       </tr>
                                      <tr>
                                        <td>
                                            Request Close Date:
                                        </td>
                                        <td>
                                            <asp:TextBox ID= "close_dateTextBox" runat="server"></asp:TextBox>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td colspan = "4">
                                            <asp:TextBox ID="request_descTextBox" runat ="server" TextMode = "MultiLine" Width="100%"></asp:TextBox>
                                         </td>
                                      </tr>
                           
                        </table>

                      </ContentTemplate>

      </ajaxToolkit:TabPanel>
      <ajaxToolkit:TabPanel ID="tpRequestItems" runat="server" TabIndex="0"  BackColor="red" Visible = "True">
                      <HeaderTemplate > 
                            <asp:Label ID="Label6" runat="server" Text="All Request Items" Font-Bold="true"></asp:Label>
                    
                      </HeaderTemplate>
                      <ContentTemplate>
                          <table border="3" width = "100%">
                            <tr>
                                <td colspan = "4">
                                      <asp:Table ID = "tblRequestItems" runat = "server">
                                     </asp:Table>
            
                                </td>
        
                            </tr>
                           </table>
                  </ContentTemplate>
         </ajaxToolkit:TabPanel>                
    </ajaxToolkit:TabContainer>



</ContentTemplate>
</asp:UpdatePanel>


</asp:Content>



