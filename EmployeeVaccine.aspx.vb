﻿Imports System.IO
Imports System.Data '
Imports System.Data.SqlClient
Imports App_Code
Imports AjaxControlToolkit
Imports C1.Web.UI.Controls.C1Window
Imports System.Net
Imports System.Net.Mail
Imports System.Net.Mail.SmtpClient

Partial Class EmployeeVaccine
    Inherits System.Web.UI.Page
    Dim bHasError As Boolean

    Dim FormSignOn As New FormSQL()
    Dim CompleteVaccine As New FormSQL()
    Dim VendorFormSignOn As FormSQL

    Dim iClientNum As String
    Dim iUserNum As Integer

    Dim RequestoName As String
    Dim iRequestorNum As String

    Dim thisRole As String
    Dim mainRole As String

    Dim VaccineNum As String = ""

    Dim sValue As String
    Private dtAccounts As New DataTable
    Private AddAccountRequestNum As Integer = 0
    Private DelAccountRequestNum As Integer = 0
    Private dtPendingRequests As DataTable
    Private dtRemoveAccounts As New DataTable
    Private dtAddAccounts As New DataTable
    'Dim BusLog As AccountBusinessLogic
    Dim thisdata As CommandsSqlAndOleDb
    Dim UserInfo As UserSecurity
    Dim VaccRequest As VaccineRequest

    Dim Mailobj As System.Net.Mail.SmtpClient = New System.Net.Mail.SmtpClient
    Dim Mailmsg As New System.Net.Mail.MailMessage
    Dim MailAddress As System.Net.Mail.MailAddress
    Dim mailAttachment As System.Net.Mail.Attachment

    Dim sAccountType As String
    Dim ValidationStatus As String
    Dim sNotfound As String = ""


    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        iClientNum = Request.QueryString("clientNum")
        sNotfound = Request.QueryString("NotFound")
        VaccineNum = Request.QueryString("VaccineNum")



        'account_request_type
        ' lblAccountRequestType.Text = AccountRequest.AccountRequestType
        ' Lblentereddate.Text = AccountRequest.EnteredDate


        UserInfo = New UserSecurity(Session("LoginID"), Session("EmployeeConn"))

        iUserNum = UserInfo.ClientNum
        UserNumAdministeredLabel.Text = UserInfo.ClientNum

        userDepartmentCD.Text = UserInfo.DepartmentCd


        FormSignOn = New FormSQL(Session("VaccineConn"), "SelClientForVaccine", "InsEmployeeVaccine", "", Page)
        FormSignOn.AddSelectParm("@client_num", iClientNum, SqlDbType.Int, 6)

        CompleteVaccine = New FormSQL(Session("VaccineConn"), "SelClientWithVaccines", "InsEmployeeVaccine", "", Page)

        'emp_type_cdrbl.BackColor() = Color.Yellow
        'emp_type_cdrbl.BorderColor() = Color.Black
        'emp_type_cdrbl.BorderStyle() = BorderStyle.Solid
        'emp_type_cdrbl.BorderWidth = Unit.Pixel(2)

        If Not IsPostBack Then
            Dim rblAffiliateBinder As New RadioButtonListBinder(emp_type_cdrbl, "SelAffiliationsV4", Session("EmployeeConn"))
            rblAffiliateBinder.BindData("AffiliationDescLower", "AffiliationDisplay") '"AffiliationDescLower",

            Dim ddlBinder As New DropDownListBinder

            'ddlBinder.BindData(facility_cdddl, "SelFacilityCodes", "facility_cd", "facility_short_name", Session("EmployeeConn"))
            ddlBinder.BindData(entity_cdddl, "selEntityCodesV4", "entity_cd", "hosp_desc", Session("EmployeeConn"))

            Dim rblVaccineBinder As New RadioButtonListBinder(VaccineOutIDrbl, "SelVaccinOutcome", Session("VaccineConn"))
            rblVaccineBinder.BindData("VaccineOutID", "VaccineOutComeDesc")

            Dim ddlLocationBinder As New DropDownListBinder
            ddlLocationBinder = New DropDownListBinder(LocationSiteIDddl, "SelFluShotLocations", Session("VaccineConn"))
            ddlLocationBinder.BindData("LocationSiteID", "LocationDesc")



            If sNotfound = "no" Then
                Dim ctrl As Control = DataCheckingAndValidation.SetChildControlsRecursiveToEnabledFalse(panDemo, True)

                siemens_emp_numLabel.Visible = False
                employeelbl.Visible = False
                positlbl.Visible = False
                user_position_descLabel.Visible = False
                emp_type_cdrbl.Visible = False

                Rolesddl.Visible = True
                lblpos.Visible = True

                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")

                Dim Position As New RequiredField(Rolesddl, "Position")

                If VaccineNumLabel.Text = "" Then
                    client_numLabel.Text = "0"
                    iClientNum = 0

                End If

            Else
                lblpos.Visible = False
                Rolesddl.Visible = False

                Dim ctrl As Control = DataCheckingAndValidation.SetChildControlsRecursiveToEnabledFalse(panDemo, False)
            End If

            VaccRequest = New VaccineRequest(iClientNum, Session("EmployeeConn"))
            siemens_emp_numLabel.Text = VaccRequest.siemens_emp_num


            If siemens_emp_numLabel.Text <> "" Then



            End If

            If iClientNum > 0 Then
                emp_type_cdrbl.SelectedValue = VaccRequest.EmpTypeCd.ToLower
                emp_type_cdLabel.Text = VaccRequest.EmpTypeCd

            End If

            ClientnameHeader.Text = VaccRequest.FirstName & " " & VaccRequest.LastName


            If entity_cdLabel.Text = "" Then
                entity_cdddl.SelectedIndex = -1
            Else
                Dim entitycd As String = entity_cdLabel.Text

                entitycd = entitycd.Trim
                entity_cdddl.SelectedValue = entitycd

            End If

            'If facility_cdLabel.Text = "" Then
            '    facility_cdddl.SelectedIndex = -1
            'Else
            '    facility_cdddl.SelectedValue = facility_cdLabel.Text
            'End If

            Dim ddbDepartmentBinder As New DropDownListBinder(department_cdddl, "selDepartmentCodesV4", Session("EmployeeConn"))
            ddbDepartmentBinder.AddDDLCriteria("@entity_cd", entity_cdddl.SelectedValue, SqlDbType.NVarChar)
            ddbDepartmentBinder.BindData("department_cd", "department_name")


            If department_cdLabel.Text = "" Then
                department_cdddl.SelectedIndex = -1
            Else
                department_cdddl.SelectedValue = department_cdLabel.Text

            End If


            'Dim ddlBuildingBinder As New DropDownListBinder
            'ddlBuildingBinder = New DropDownListBinder(building_cdddl, "sel_building_by_facility", Session("EmployeeConn"))
            'ddlBuildingBinder.AddDDLCriteria("@facility_cd", facility_cdddl.SelectedValue, SqlDbType.NVarChar)
            'ddlBuildingBinder.BindData("building_cd", "building_name")


            'If building_cdLabel.Text = "" Then
            '    building_cdddl.SelectedIndex = -1
            'Else
            '    Dim sBuilding As String = building_cdLabel.Text
            '    building_cdddl.SelectedValue = sBuilding.Trim

            '    If building_nameLabel.Text <> "" Then

            '        building_cdddl.SelectedItem.Text = building_nameLabel.Text
            '    End If

            'End If

            'If building_cdddl.SelectedIndex <> -1 Then ' Or building_cdddl.SelectedValue <> "Select" Then

            '    ddlBinder = New DropDownListBinder(Floorddl, "SelFloorsByBuilding", Session("EmployeeConn"))
            '    ddlBinder.AddDDLCriteria("@facility_cd", facility_cdddl.SelectedValue, SqlDbType.NVarChar)
            '    ddlBinder.AddDDLCriteria("@building_cd", building_cdddl.SelectedValue, SqlDbType.NVarChar)

            '    ddlBinder.BindData("floor_no", "floor_no")

            '    If FloorTextBox.Text = "" Then
            '        Floorddl.SelectedValue = -1
            '    Else
            '        Dim sFloor As String = FloorTextBox.Text
            '        Floorddl.SelectedValue = sFloor.Trim

            '    End If



            'End If


            If VaccineNum <> "" Then
                CompleteVaccine.AddSelectParm("@client_num", iClientNum, SqlDbType.Int, 6)

                CompleteVaccine.AddSelectParm("@VaccineNum", VaccineNum, SqlDbType.Int, 6)
                CompleteVaccine.FillForm()

                Dim ctrl As Control = DataCheckingAndValidation.SetChildControlsRecursiveToEnabledFalse(panDemo, False)

                Dim Quesctrl As Control = DataCheckingAndValidation.SetChildControlsRecursiveToEnabledFalse(panQuestions, False)

                btnSubmitVaccine.Visible = False

            Else
                FormSignOn.FillForm()


                VaccineDateTextBox.Text = Today
                EnteryDateTextBox.Text = Today

                CheckRequiredFields()
            End If


        End If

    End Sub

    Protected Sub btnReturnSearch_Click(sender As Object, e As System.EventArgs) Handles btnReturnSearch.Click
        Response.Redirect("~/SearchByEmpNumber.aspx", True)
    End Sub

    Protected Sub entity_cdddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles entity_cdddl.SelectedIndexChanged
        Dim ddbDepartmentBinder As New DropDownListBinder(department_cdddl, "selDepartmentCodesV4", Session("EmployeeConn"))
        ddbDepartmentBinder.AddDDLCriteria("@entity_cd", entity_cdddl.SelectedValue, SqlDbType.NVarChar)
        ddbDepartmentBinder.BindData("department_cd", "department_name")

        entity_nameLabel.Text = entity_cdddl.SelectedItem.ToString
    End Sub
    Protected Sub department_cdddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles department_cdddl.SelectedIndexChanged
        department_nameLabel.Text = department_cdddl.SelectedItem.ToString
    End Sub

    Protected Sub btnSubmitVaccine_Click(sender As Object, e As System.EventArgs) Handles btnSubmitVaccine.Click

        CheckRequiredFields()

        If sNotfound = "no" Then

            If first_nameTextBox.Text = "" Then
                lblValidation.Text = "Must supply First Name"
                lblValidation.ForeColor = Color.Red
                lblValidation.Focus()

                first_nameTextBox.ForeColor = Color.Red
                Exit Sub
            End If

            If last_nameTextBox.Text = "" Then
                lblValidation.Text = "Must supply Last Name"
                lblValidation.ForeColor = Color.Red
                lblValidation.Focus()

                last_nameTextBox.ForeColor = Color.Red
                Exit Sub
            End If

        End If

        If AllergicEggsrbl.SelectedIndex = -1 Then

            lblValidation.Text = "Must select Allergic Eggs"
            lblValidation.ForeColor = Color.Red
            lblValidation.Focus()
            AllergicEggsrbl.ForeColor = Color.Red

            Return

        End If

        If Allergiclatexrbl.SelectedIndex = -1 Then

            lblValidation.Text = "Must select Allergic to Latex"
            lblValidation.ForeColor = Color.Red
            lblValidation.Focus()
            Allergiclatexrbl.ForeColor = Color.Red

            Return

        End If

        If AllergicThimerosalrbl.SelectedIndex = -1 Then

            lblValidation.Text = "Must select Allergic to Thimerosal"
            lblValidation.ForeColor = Color.Red
            lblValidation.Focus()
            AllergicThimerosalrbl.ForeColor = Color.Red

            Return

        End If

        If FeelSickrbl.SelectedIndex = -1 Then

            lblValidation.Text = "Must select Do You Feel Sick"
            lblValidation.ForeColor = Color.Red
            lblValidation.Focus()
            FeelSickrbl.ForeColor = Color.Red

            Return

        End If

        If HistoryGBSrbl.SelectedIndex = -1 Then

            lblValidation.Text = "Must select History of GBS"
            lblValidation.ForeColor = Color.Red
            lblValidation.Focus()
            HistoryGBSrbl.ForeColor = Color.Red

            Return

        End If

        If InjectionSiderbl.SelectedIndex = -1 Then

            lblValidation.Text = "Must supply Injection Side"
            lblValidation.ForeColor = Color.Red
            lblValidation.Focus()
            InjectionSiderbl.ForeColor = Color.Red

            Return

        End If

        If AllergicFluShotrbl.SelectedIndex = -1 Then

            lblValidation.Text = "Must supply Allergic to Flu Shot"
            lblValidation.ForeColor = Color.Red
            lblValidation.Focus()
            AllergicFluShotrbl.ForeColor = Color.Red

            Return

        End If


        If LocationSiteIDddl.SelectedIndex = -1 Then

            lblValidation.Text = "Must supply Location Site"
            lblValidation.ForeColor = Color.Red
            lblValidation.Focus()
            LocationSiteIDddl.ForeColor = Color.Red

            Return

        End If

        If VaccineOutIDrbl.SelectedIndex = -1 Then

            lblValidation.Text = "Must supply Vaccination Out Come"
            lblValidation.ForeColor = Color.Red
            lblValidation.Focus()
            VaccineOutIDrbl.ForeColor = Color.Red

            Return

        End If

        If bHasError = True Then
            lblValidation.Focus()
            Exit Sub
        Else
            lblValidation.Text = ""
        End If


        'Dim thisdata As New CommandsSqlAndOleDb("InsEmployeeVaccine", Session("VaccineConn"))
        'thisdata.AddSqlProcParameter("@VaccineNum", "", SqlDbType.NVarChar, 25)
        'thisdata.AddSqlProcParameter("@UserNum", iUserNum, SqlDbType.Int, 9)
        'thisdata.AddSqlProcParameter("@NtLogin", Session("LoginID"), SqlDbType.NVarChar, 50)
        'thisdata.AddSqlProcParameter("@VaccineLotNum", VaccineLotNumlabel.Text, SqlDbType.Int, 9)
        'thisdata.ExecNonQueryNoReturn()

        FormSignOn.AddInsertParm("@NtLogin", Session("LoginID"), SqlDbType.NVarChar, 50)

        'FormSignOn.UpdateForm()

        FormSignOn.UpdateFormReturnVaccine()

        VaccineNum = FormSignOn.VaccineNum

        ReturnVaccineLbl.Text = VaccineNum

        VaccineReturnPopup.Show()




    End Sub

    Public Function CheckRequiredFields() As Boolean
        Dim removeCss As New PageControls(Page)
        removeCss.RemoveCssClass(Page, "textInputRequired")

        If sNotfound = "no" Then
            Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
            Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")
            Dim Position As New RequiredField(Rolesddl, "Position")

        End If



        Dim Eggs As New RequiredField(AllergicEggsrbl, "Allergic Eggs")
        Dim latex As New RequiredField(Allergiclatexrbl, "Allergic latex")
        Dim Thimerosal As New RequiredField(AllergicThimerosalrbl, "Allergic Thimerosal")
        Dim Sick As New RequiredField(FeelSickrbl, "Feel Sick")
        Dim gbs As New RequiredField(HistoryGBSrbl, "History GBS")
        Dim Inject As New RequiredField(InjectionSiderbl, "Injection Side")
        Dim FluShot As New RequiredField(AllergicFluShotrbl, "Allergic Flu Shot")

        Dim Location As New RequiredField(LocationSiteIDddl, "Location Site")
        Dim VaccinOut As New RequiredField(VaccineOutIDrbl, "Vaccin Out Come")

        Return bHasError

    End Function

    Protected Sub Rolesddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles Rolesddl.SelectedIndexChanged
        user_position_descLabel.Text = Rolesddl.SelectedValue.ToString

    End Sub

    Protected Sub btnCompleted_Click(sender As Object, e As System.EventArgs) Handles btnCompleted.Click
        VaccineReturnPopup.Hide()
        Response.Redirect("~/SearchByEmpNumber.aspx", True)


    End Sub

End Class
