﻿Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports App_Code
Imports AjaxControlToolkit


Partial Class ViewImage
    Inherits System.Web.UI.Page
    Dim RequestNum As Integer
    Private Declare Function SetForegroundWindow Lib "user32" (ByVal hwnd As Long) _
As Long

    Private Declare Function FindWindow Lib "user32" Alias _
    "FindWindowA" (ByVal lpClassName As String, ByVal lpWindowName As String) _
    As Long
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        RequestNum = Request.QueryString("RequestNum")

        If Not IsPostBack Then
            DisplayFromDB()

            Try


            Catch
            End Try

            Try

            Catch SQLexc As System.Data.SqlClient.SqlException
                Response.Write("Read Failed : " & SQLexc.ToString())
            End Try

            Page.Dispose()

        End If

    End Sub
    Sub DisplayFromDB()

        HDrequestnum.Text = RequestNum

        Using SqlConnection As New SqlConnection(Session("CSCConn"))
            Dim Cmd As New SqlCommand()
            Cmd.CommandText = "SelRequestImages"
            Cmd.Parameters.AddWithValue("@request_num", RequestNum)

            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.Connection = SqlConnection

            SqlConnection.Open()
            Using Cmd

                Using dr = Cmd.ExecuteReader()
                    If dr.HasRows Then

                        Do While dr.Read



                            Dim imageInBytes As Byte() = IIf(IsDBNull(dr("FileContent")), Nothing, dr("FileContent"))
                            Dim Fileextension As String = IIf(IsDBNull(dr("FileType")), Nothing, dr("FileType"))
                            Dim filename As String = IIf(IsDBNull(dr("FileName")), Nothing, dr("FileName"))

                            filenamelbl.Text = filename

                            'delete it before writing to file

                            Dim loca As String
                            loca = Server.MapPath("~\\CSCFileUpload\\")

                            File.Delete(loca & filenamelbl.Text)
                            File.Delete(loca & "/image1.jpeg")
                            File.Delete(loca & "/image1.jpg")



                            If Fileextension = "image/jpeg" Or Fileextension = "image/jpg" Or Fileextension = "image/png" Or Fileextension = "image/gif" Then

                                Dim Myfile As New FileStream(loca & "/image1.jpeg", FileMode.CreateNew)
                                Dim MyRite As New BinaryWriter(Myfile)
                                MyRite.Write(imageInBytes)
                                MyRite.Flush()
                                MyRite.Close()

                            End If

                            If Fileextension = "application/vnd.ms-excel" Then
                                If File.Exists(loca & "/excel1.xls") Then
                                    File.Delete(loca & "/excel1.xls")
                                End If

                                Dim Myfile As New FileStream(loca & "/excel1.xls", FileMode.CreateNew)
                                Dim MyRite As New BinaryWriter(Myfile)

                                MyRite.Write(imageInBytes)
                                MyRite.Flush()
                                MyRite.Close()

                                Createxcel()

                            End If

                            If Fileextension = "application/vnd.openxmlformats-officedocument.presentationml.presentation" Then
                                If File.Exists(loca & "/Power.pptx") Then
                                    File.Delete(loca & "/Power.pptx")
                                End If

                                Dim Myfile As New FileStream(loca & "/Power.pptx", FileMode.CreateNew)
                                Dim MyRite As New BinaryWriter(Myfile)

                                MyRite.Write(imageInBytes)
                                MyRite.Flush()
                                MyRite.Close()


                                createPP()
                            End If

                            If Fileextension = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" Then

                                If File.Exists(loca & "/excel1.xlsx") Then
                                    File.Delete(loca & "/excel1.xlsx")
                                End If

                                Dim Myfile As New FileStream(loca & "/excel1.xlsx", FileMode.CreateNew)
                                Dim MyRite As New BinaryWriter(Myfile)

                                MyRite.Write(imageInBytes)
                                MyRite.Flush()
                                MyRite.Close()

                                Createxcelsheet()

                            End If


                            If Fileextension = "application/pdf" Then

                                If File.Exists(loca & "/pdf1.pdf") Then
                                    File.Delete(loca & "/pdf1.pdf")
                                End If

                                Dim Myfile As New FileStream(loca & "/pdf1.pdf", FileMode.CreateNew)
                                Dim MyRite As New BinaryWriter(Myfile)

                                MyRite.Write(imageInBytes)
                                MyRite.Flush()
                                MyRite.Close()

                                createpdf()

                            End If

                            If Fileextension.Contains("application") Or Fileextension = "application/octet-stream" Or Fileextension = ("application/vnd.openxmlformats-officedocument.wordprocessingml.document") Then

                                If File.Exists(loca & "/word1.doc") Then
                                    File.Delete(loca & "/word1.doc")
                                End If

                                Dim Myfile As New FileStream(loca & "/word1.doc", FileMode.CreateNew)
                                Dim MyRite As New BinaryWriter(Myfile)

                                MyRite.Write(imageInBytes)
                                MyRite.Flush()
                                MyRite.Close()

                                Createword()

                            End If

                        Loop
                    End If

                End Using
            End Using
            SqlConnection.Close()

        End Using

    End Sub
    Sub Createxcel()

        Dim filepath = Server.MapPath("~/CSCFileUpload/excel1.xls")

        Response.ContentType = "application/vnd.ms-excel"

        Response.AddHeader("content-disposition", "attachment;filename=excel1.xls")

        Response.Charset = String.Empty

        Response.WriteFile(filepath)
        Response.Flush()
        Response.End()

    End Sub
    Sub createPP()

        Dim filepath = Server.MapPath("~/CSCFileUpload/Power.pptx")

        Response.ContentType = "application/vnd.ms-powerpoint"

        Response.AddHeader("content-disposition", "attachment;filename=Power.pptx")

        Response.Charset = String.Empty

        Response.WriteFile(filepath)
        Response.Flush()
        Response.End()

    End Sub
    Sub Createxcelsheet()

        Dim filepath = Server.MapPath("~/CSCFileUpload/excel1.xlsx")

        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"

        Response.AddHeader("content-disposition", "attachment;filename=excel1.xlsx")

        Response.Charset = String.Empty

        Response.WriteFile(filepath)
        Response.Flush()
        Response.End()

    End Sub
    Sub createpdf()

        Dim filepath = Server.MapPath("~/CSCFileUpload/pdf1.pdf")

        Response.ContentType = "application/vnd.pdf"

        Response.AddHeader("content-disposition", "attachment;filename=pdf1.pdf")

        Response.Charset = String.Empty
        Response.WriteFile(filepath)
        Response.Flush()
        Response.End()

    End Sub
    Sub Createword()

        Dim filepath = Server.MapPath("~/CSCFileUpload/word1.doc")

        Response.ContentType = "application/vnd.ms-word"

        Response.AddHeader("content-disposition", "attachment;filename=word1.doc")

        Response.Charset = String.Empty

        Response.WriteFile(filepath)
        Response.Flush()
        Response.End()

    End Sub

    Protected Sub btnReturn_Click(sender As Object, e As System.EventArgs) Handles btnReturn.Click
        Response.Redirect("./ViewRequestTicket.aspx?RequestNum=" & RequestNum, True)
    End Sub


End Class
