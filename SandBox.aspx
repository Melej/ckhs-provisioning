﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="True" CodeFile="SandBox.aspx.vb" Inherits="SandBox" %>



<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">


<style type="text/css"> 

.TestAfterCss:after
{ 
content:url(Images/arrow16.png);
position:relative;
display:inline;
margin-left: 5px;
}
.TestAfterCss
{ 
    vertical-align:middle;
}

</style>

<script type="text/javascript" src="Scripts/FieldValidator.js"></script>

   

<script type="text/javascript">


    $(document).ready(function () {
        // Tooltip only Text

        $(".CharLimit").keyup(function () {
            var i = $(".CharLimit").val().length;
            $("#counter").val(i);
            if (i > 10) {
                $("#warning").show()
            } else {
                $("#warning").hide()
            }

            $('<div class="tooltipMove"></div>')
                .html(i + ' / 50')
                .appendTo('body');

            var txtY = $(this).position().top;
            var txtX = $(this).position().left;
            $('.tooltipMove')
                .css({ top: txtY, left: txtX })


        });

        $(".CharLimit").focus(function () {
            $(this).css({ background-color: 'red' })

        });

    });


</script>


      
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

 <asp:label runat="server" ID = "lblTest" CssClass="TestAfterCss">Test this puppy out</asp:label>
 <div class ="TestAfterCss"></div>


 <asp:TextBox runat="server" TextMode="MultiLine" ID="txtTest" CssClass = "CharLimit">
 </asp:TextBox>
 <div id="warning" style="color: red; display: none">
  Sorry, too much text.
</div>
</br>
<input type="text" id="counter" disabled="disabled" value="0"/>
  <%-- OnClientClick="myfunction(); return false;"--%>
</asp:Content>

