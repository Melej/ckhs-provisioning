﻿Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports App_Code

Partial Class ActiveManagers
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        'SqlDataSource1.ConnectionString = Session("EmployeeConn")

        Dim ddlBinder As New DropDownListBinder


        If Not IsPostBack Then

            ddlBinder.BindData(entity_cdDDL, "sel_entity_codes", "entity_cd", "hosp_desc", Session("EmployeeConn"))
            ddlBinder.BindData(department_cdddl, "SelActiveDepartmentCodes", "department_cd", "DepartmentDesc", Session("EmployeeConn"))

            entity_cdDDL.SelectedIndex = -1
            department_cdddl.SelectedIndex = -1
            grActiveManagers.SelectedIndex = -1

        End If
        'SelActiveDepartmentCodes
        ' Me.CurrentManagers()

        'grActiveManagers.DataBind()
        'SelActiveDepartmentCodes

        'sel_entity_codes 


    End Sub
    Protected Sub CurrentManagers()

        ' exec SelActiveManagers
        ' exec SelActiveManagers @entity_cd = '150'
        ' exec SelActiveManagers @department_cd='930000'
        ' exec SelActiveManagers @lastName = 'ralph'


        Dim ActiveManagersData As New CommandsSqlAndOleDb("SelActiveManagers", Session("EmployeeConn"))

        If LastNameTextBox.Text <> "" Then
            ActiveManagersData.AddSqlProcParameter("@LastName", LastNameTextBox.Text, SqlDbType.NVarChar, 30)


        End If

        If entity_cdDDL.SelectedValue.ToLower <> "select" Then
            ActiveManagersData.AddSqlProcParameter("@entity_cd", entity_cdDDL.SelectedValue, SqlDbType.NVarChar, 8)

        End If

        If department_cdddl.SelectedValue.ToLower <> "select" Then
            ActiveManagersData.AddSqlProcParameter("@department_cd", department_cdddl.SelectedValue, SqlDbType.NVarChar, 16)

        End If

        ViewState("dtActiveManagers") = ActiveManagersData.GetSqlDataTable

        grActiveManagers.DataSource = ViewState("dtActiveManagers")
        grActiveManagers.DataBind()

        LastNameTextBox.Text = ""
        departmentText.Text = ""
        department_cdddl.SelectedIndex = -1

        entityText.Text = ""
        entity_cdDDL.SelectedIndex = -1



    End Sub

    Protected Sub grActiveManagers_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grActiveManagers.RowCommand
        If e.CommandName = "Select" Then
            Dim client_num As String = grActiveManagers.DataKeys(Convert.ToInt32(e.CommandArgument))("client_num").ToString()
            Response.Redirect("~/ClientDemo.aspx?ClientNum=" & client_num, True)

        End If
    End Sub

    Protected Sub grActiveManagers_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grActiveManagers.RowDataBound

        If e.Row.RowType = DataControlRowType.DataRow Then

        End If
    End Sub

    Protected Sub SqlDataSource1_Init(sender As Object, e As System.EventArgs) Handles SqlDataSource1.Init
        SqlDataSource1.ConnectionString = Session("EmployeeConn")
    End Sub
    Protected Sub OnPageIndexChanging(sender As Object, e As GridViewPageEventArgs)
        grActiveManagers.PageIndex = e.NewPageIndex
        Me.CurrentManagers()
    End Sub

    Protected Sub btnSearch_Click(sender As Object, e As System.EventArgs) Handles btnSearch.Click
        CurrentManagers()
    End Sub

    Protected Sub entity_cdDDL_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles entity_cdDDL.SelectedIndexChanged
        entityText.Text = entity_cdDDL.SelectedItem.ToString

        departmentText.Text = ""
        department_cdddl.SelectedIndex = -1

        LastNameTextBox.Text = ""

    End Sub

    Protected Sub department_cdddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles department_cdddl.SelectedIndexChanged
        departmentText.Text = department_cdddl.SelectedItem.ToString

        entityText.Text = ""
        entity_cdDDL.SelectedIndex = -1

        LastNameTextBox.Text = ""


    End Sub

    Protected Sub btnReset_Click(sender As Object, e As System.EventArgs) Handles btnReset.Click
        LastNameTextBox.Text = ""
        departmentText.Text = ""
        department_cdddl.SelectedIndex = -1

        entityText.Text = ""
        entity_cdDDL.SelectedIndex = -1

    End Sub
End Class
