﻿Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports App_Code
Imports AjaxControlToolkit
Imports C1.Web.UI.Controls.C1Window
Imports System.Net
Imports System.Net.Mail
Imports System.Net.Mail.SmtpClient

Partial Class CPMLocations
    Inherits System.Web.UI.Page
    Dim FormSignOn As FormSQL
    Dim NewLocationForm As FormSQL

    Dim iGroupNum As Integer
    Dim bHasError As Boolean


    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        Dim UserInfo = New UserSecurity(Session("LoginID"), Session("EmployeeConn"))

        iGroupNum = Request.QueryString("GroupNum")
        groupnumLabel.Text = Request.QueryString("GroupNum")

        GroupTypeCDLabel.Text = "Gnum"

        'If Session("SecurityLevelNum") = 50 Then
        '    rblAffiliateBinder.AddDDLCriteria("@type", "phys", SqlDbType.NVarChar)

        'ElseIf Session("SecurityLevelNum") = 40 Or Session("SecurityLevelNum") = 45 Then
        '    rblAffiliateBinder.AddDDLCriteria("@type", "emp", SqlDbType.NVarChar)

        'End If

        If Session("SecurityLevelNum") < 79 Then
            Response.Redirect("~/SimpleSearch.aspx", True)
        End If

        FormSignOn = New FormSQL(Session("EmployeeConn"), "SelCPMLocationByGroupNum", "UpdPhysGroupV5", "", Page)
        FormSignOn.AddSelectParm("@groupnum", iGroupNum, SqlDbType.Int, 9)


        NewLocationForm = New FormSQL(Session("EmployeeConn"), "SelCPMLocationByGroupNum", "InsPhysGroupV4", "", Page)


        If iGroupNum > 0 Then
            Dim CPMLoc As New CPMLocationController(Session("EmployeeConn"))

            Dim cpmLocation = CPMLoc.GetCPMLocation(iGroupNum)

            If CPMLoc.ValidationDate <> "" Then

                ValidationDateTextBox.Text = CPMLoc.ValidationDate
                hdValidationdate.Text = CPMLoc.ValidationDate
            ElseIf hdValidationdate.Text <> "" Then

                ValidationDateTextBox.Text = hdValidationdate.Text

            End If


        Else
            iGroupNum = 0

        End If


        If Not IsPostBack Then

            FormSignOn.FillForm()

            Dim ddlBinder As New DropDownListBinder


            ddlBinder.BindData(Specialtyddl, "SelSpecialtyCodesV2", "specialty", "specialty", Session("EmployeeConn"))

            'FormSignOn.AddSelectParm("@GroupNum", iGroupNum, SqlDbType.Int, 9)

            CheckRequiredFields()

            If ValidationDateTextBox.Text = "" And iGroupNum > 0 Then
                If Session("SecurityLevelNum") > 89 Then
                    ValidationDateTextBox.Visible = True
                    validationlbl.Visible = True
                End If
            Else

                If Session("SecurityLevelNum") > 89 Then
                    ValidationDateTextBox.Visible = True
                    validationlbl.Visible = True

                Else
                    ValidationDateTextBox.Visible = True
                    validationlbl.Visible = True
                    ValidationDateTextBox.Enabled = False
                End If
            End If

        End If
        MandatoryFields()

        If groupnumLabel.Text <> "" Then

            btnSubmit.Text = "Update"

            FillPhysicianAccounts()
            FillEmployees()

        End If

    End Sub

    Protected Sub FillPhysicianAccounts()

        Dim PhysicianCount As Integer
        Dim iRowCount = 1
        Dim thisData As New CommandsSqlAndOleDb("SelPhysClientsByV4", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@groupNum", GroupnumLabel.Text, SqlDbType.Int)
        thisData.AddSqlProcParameter("@type", "physician", SqlDbType.NVarChar)



        Dim dt As DataTable

        dt = thisData.GetSqlDataTable

        PhysicianCount = dt.Rows.Count

        'fullname
        'DoctorMasterNum
        'emp_type_cd
        'NPI
        'Specialty

        Dim rwHeader As New TableRow

        Dim clClientHeader As New TableCell
        Dim clClientNameHeader As New TableCell
        Dim clClientDocNumHeader As New TableCell
        Dim clClientNPIHeader As New TableCell
        Dim clClientSpecHeader As New TableCell


        clClientHeader.Text = "Client#"
        clClientNameHeader.Text = "Name"
        clClientDocNumHeader.Text = "Doc Master#"

        clClientNPIHeader.Text = "NPI"
        clClientSpecHeader.Text = "Specialty"


        rwHeader.Cells.Add(clClientHeader)
        rwHeader.Cells.Add(clClientNameHeader)
        rwHeader.Cells.Add(clClientDocNumHeader)

        rwHeader.Cells.Add(clClientNPIHeader)
        rwHeader.Cells.Add(clClientSpecHeader)

        rwHeader.Font.Bold = True
        rwHeader.BackColor = Color.FromName("#006666")
        rwHeader.ForeColor = Color.FromName("White")
        rwHeader.Height = Unit.Pixel(36)

        tblProviders.Rows.Add(rwHeader)

        For Each drRow As DataRow In dt.Rows


            Dim rwData As New TableRow
            Dim btnSelect As New Button

            'Dim clRequestData As New TableCell
            Dim clSelectData As New TableCell
            Dim clClientNameData As New TableCell
            Dim clClientDocNumData As New TableCell
            Dim clClientNPIData As New TableCell
            Dim clSpecialtyData As New TableCell

            btnSelect.Text = "Select"
            btnSelect.CommandName = "Select"
            btnSelect.CommandArgument = drRow("client_num")
            ' btnSelect.Width = 50
            btnSelect.EnableTheming = False

            AddHandler btnSelect.Click, AddressOf btnSelect_Click

            clClientNameData.Text = IIf(IsDBNull(drRow("fullname")), "", drRow("fullname"))
            clClientDocNumData.Text = IIf(IsDBNull(drRow("DoctorMasterNum")), "", drRow("DoctorMasterNum"))
            clClientNPIData.Text = IIf(IsDBNull(drRow("NPI")), "", drRow("NPI"))

            clSpecialtyData.Text = IIf(IsDBNull(drRow("Specialty")), "", drRow("Specialty"))


            'add button to grid
            clSelectData.Controls.Add(btnSelect)

            rwData.Cells.Add(clSelectData)
            rwData.Cells.Add(clClientNameData)
            rwData.Cells.Add(clClientDocNumData)
            rwData.Cells.Add(clClientNPIData)

            rwData.Cells.Add(clSpecialtyData)

            If iRowCount > 0 Then

                rwData.BackColor = Color.Bisque

            Else

                rwData.BackColor = Color.LightGray

            End If

            tblProviders.Rows.Add(rwData)

            iRowCount = iRowCount * -1

        Next
        tblProviders.Font.Name = "Arial"
        tblProviders.Font.Size = 8

        tblProviders.CellPadding = 10
        tblProviders.Width = New Unit("100%")



    End Sub

    Protected Sub FillEmployees()
        Dim EmployeeCount As Integer
        Dim iRowCount = 1
        Dim thisData As New CommandsSqlAndOleDb("SelPhysClientsByV4", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@groupNum", groupnumLabel.Text, SqlDbType.Int)
        thisData.AddSqlProcParameter("@type", "employee", SqlDbType.NVarChar)



        Dim dt As DataTable

        dt = thisData.GetSqlDataTable

        EmployeeCount = dt.Rows.Count

        'fullname
        'DoctorMasterNum
        'emp_type_cd
        'NPI
        'Specialty

        Dim rwHeader As New TableRow

        Dim clClientHeader As New TableCell
        Dim clClientNameHeader As New TableCell
        Dim clClientDocNumHeader As New TableCell


        clClientHeader.Text = "Client#"
        clClientNameHeader.Text = "Name"
        clClientDocNumHeader.Text = "Dept Name"



        rwHeader.Cells.Add(clClientHeader)
        rwHeader.Cells.Add(clClientNameHeader)
        rwHeader.Cells.Add(clClientDocNumHeader)


        rwHeader.Font.Bold = True
        rwHeader.BackColor = Color.FromName("#006666")
        rwHeader.ForeColor = Color.FromName("White")
        rwHeader.Height = Unit.Pixel(36)

        tblEmployees.Rows.Add(rwHeader)

        For Each drRow As DataRow In dt.Rows


            Dim rwData As New TableRow
            Dim btnSelect As New Button

            'Dim clRequestData As New TableCell
            Dim clSelectData As New TableCell
            Dim clClientNameData As New TableCell
            Dim clClientDocNumData As New TableCell
            Dim clClientNPIData As New TableCell
            Dim clSpecialtyData As New TableCell

            btnSelect.Text = "Select"
            btnSelect.CommandName = "Select"
            btnSelect.CommandArgument = drRow("client_num")
            ' btnSelect.Width = 50
            btnSelect.EnableTheming = False

            AddHandler btnSelect.Click, AddressOf btnSelect_Click

            clClientNameData.Text = IIf(IsDBNull(drRow("fullname")), "", drRow("fullname"))
            clClientDocNumData.Text = IIf(IsDBNull(drRow("department_name")), "", drRow("department_name"))


            'add button to grid
            clSelectData.Controls.Add(btnSelect)

            rwData.Cells.Add(clSelectData)
            rwData.Cells.Add(clClientNameData)
            rwData.Cells.Add(clClientDocNumData)

            If iRowCount > 0 Then

                rwData.BackColor = Color.Bisque

            Else

                rwData.BackColor = Color.LightGray

            End If

            tblEmployees.Rows.Add(rwData)

            iRowCount = iRowCount * -1

        Next
        tblEmployees.Font.Name = "Arial"
        tblEmployees.Font.Size = 8

        tblEmployees.CellPadding = 10
        tblEmployees.Width = New Unit("100%")


    End Sub
    Sub btnSelect_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim btnLinkView As Button = sender

        If btnLinkView.CommandName = "Select" Then
            Dim sArguments As String() = Split(btnLinkView.CommandArgument, ";")

            Response.Redirect("./ClientDemo.aspx?ClientNum=" & sArguments(0))

            '            Response.Redirect("./OpenProviderRequestItems.aspx?RequestNum=" & sArguments(0))

        End If
    End Sub

    Public Function CheckRequiredFields() As Boolean
        Dim removeCss As New PageControls(Page)
        removeCss.RemoveCssClass(Page, "textInputRequired")

        Dim LocationTypeReq As New RequiredField(LocationTypeCdrbl, "Location Type ")
        Dim GroupNameReq As New RequiredField(Group_nameTextBox, "Group Name")
        'GroupNPI

        'Dim CredentialNameReq As New RequiredField(CredentialedNameTextBox, "Credential Name")

        'Dim GroupNPINameReq As New RequiredField(GroupNPITextBox, "Group NPI ")
        'Dim LocationNPIReq As New RequiredField(OfficeNPITextBox, "Location NPI")

        'Dim taxonomyReq As New RequiredField(taxonomyTextBox, "Taxonomy ")

        'Dim specReq As New RequiredField(Specialtyddl, "Specialty")
        'Specialtyddl.BackColor() = Color.Yellow
        'Specialtyddl.BorderColor() = Color.Black
        'Specialtyddl.BorderStyle() = BorderStyle.Solid
        'Specialtyddl.BorderWidth = Unit.Pixel(2)

        'Dim Gnum As New RequiredField(GroupIDTextBox, "Gnum #")

        'Dim add1Req As New RequiredField(Address1Textbox, "Address 1")

        'Dim cityReq As New RequiredField(Citytextbox, "City")
        'Dim stateReq As New RequiredField(Statetextbox, "State")
        'Dim zipReq As New RequiredField(Ziptextbox, "ZIP")
        'Dim phoneReq As New RequiredField(PhoneTextBox, "Phone")
        'Dim faxqReq As New RequiredField(FaxTextBox, "Fax")

        'Dim EffectiveReq As New RequiredField(EffectiveDateTextBox, "Effective Date ")


        'Dim CostReq As New RequiredField(CostCenterTextbox, "Cost Center ")

        Return bHasError

    End Function
    Protected Sub MandatoryFields()

        'Specialtyddl.BackColor() = Color.Yellow
        'Specialtyddl.BorderColor() = Color.Black
        'Specialtyddl.BorderStyle() = BorderStyle.Solid
        'Specialtyddl.BorderWidth = Unit.Pixel(2)

        'CostCenterTextbox.BackColor() = Color.Yellow
        'CostCenterTextbox.BorderColor() = Color.Black
        'CostCenterTextbox.BorderStyle() = BorderStyle.Solid
        'CostCenterTextbox.BorderWidth = Unit.Pixel(2)

        'CredentialedNameTextBox.BackColor() = Color.Yellow
        'CredentialedNameTextBox.BorderColor() = Color.Black
        'CredentialedNameTextBox.BorderStyle() = BorderStyle.Solid
        'CredentialedNameTextBox.BorderWidth = Unit.Pixel(2)

        Group_nameTextBox.BackColor() = Color.Yellow
        Group_nameTextBox.BorderColor() = Color.Black
        Group_nameTextBox.BorderStyle() = BorderStyle.Solid
        Group_nameTextBox.BorderWidth = Unit.Pixel(2)

        'OfficeNPITextBox.BackColor() = Color.Yellow
        'OfficeNPITextBox.BorderColor() = Color.Black
        'OfficeNPITextBox.BorderStyle() = BorderStyle.Solid
        'OfficeNPITextBox.BorderWidth = Unit.Pixel(2)

        'GroupNPITextBox.BackColor() = Color.Yellow
        'GroupNPITextBox.BorderColor() = Color.Black
        'GroupNPITextBox.BorderStyle() = BorderStyle.Solid
        'GroupNPITextBox.BorderWidth = Unit.Pixel(2)


        'GroupIDTextBox.BackColor() = Color.Yellow
        'GroupIDTextBox.BorderColor() = Color.Black
        'GroupIDTextBox.BorderStyle() = BorderStyle.Solid
        'GroupIDTextBox.BorderWidth = Unit.Pixel(2)

        LocationTypeCdrbl.BackColor() = Color.Yellow
        LocationTypeCdrbl.BorderColor() = Color.Black
        LocationTypeCdrbl.BorderStyle() = BorderStyle.Solid
        LocationTypeCdrbl.BorderWidth = Unit.Pixel(2)

        'Address1Textbox.BackColor() = Color.Yellow
        'Address1Textbox.BorderColor() = Color.Black
        'Address1Textbox.BorderStyle() = BorderStyle.Solid
        'Address1Textbox.BorderWidth = Unit.Pixel(2)

        'Citytextbox.BackColor() = Color.Yellow
        'Citytextbox.BorderColor() = Color.Black
        'Citytextbox.BorderStyle() = BorderStyle.Solid
        'Citytextbox.BorderWidth = Unit.Pixel(2)


        'Statetextbox.BackColor() = Color.Yellow
        'Statetextbox.BorderColor() = Color.Black
        'Statetextbox.BorderStyle() = BorderStyle.Solid
        'Statetextbox.BorderWidth = Unit.Pixel(2)


        'Ziptextbox.BackColor() = Color.Yellow
        'Ziptextbox.BorderColor() = Color.Black
        'Ziptextbox.BorderStyle() = BorderStyle.Solid
        'Ziptextbox.BorderWidth = Unit.Pixel(2)

        'PhoneTextBox.BackColor() = Color.Yellow
        'PhoneTextBox.BorderColor() = Color.Black
        'PhoneTextBox.BorderStyle() = BorderStyle.Solid
        'PhoneTextBox.BorderWidth = Unit.Pixel(2)


        'FaxTextBox.BackColor() = Color.Yellow
        'FaxTextBox.BorderColor() = Color.Black
        'FaxTextBox.BorderStyle() = BorderStyle.Solid
        'FaxTextBox.BorderWidth = Unit.Pixel(2)

        'taxonomyTextBox.BackColor() = Color.Yellow
        'taxonomyTextBox.BorderColor() = Color.Black
        'taxonomyTextBox.BorderStyle() = BorderStyle.Solid
        'taxonomyTextBox.BorderWidth = Unit.Pixel(2)

        'EffectiveDateTextBox.BackColor() = Color.Yellow
        'EffectiveDateTextBox.BorderColor() = Color.Black
        'EffectiveDateTextBox.BorderStyle() = BorderStyle.Solid
        'EffectiveDateTextBox.BorderWidth = Unit.Pixel(3)

        'If iGroupNum > 0 Then

        '    ValidationDateTextBox.BackColor() = Color.Yellow
        '    ValidationDateTextBox.BorderColor() = Color.Black
        '    ValidationDateTextBox.BorderStyle() = BorderStyle.Solid
        '    ValidationDateTextBox.BorderWidth = Unit.Pixel(3)

        '    ValidationDateTextBox.Visible = True
        '    validationlbl.Visible = True

        'Else
        '    ValidationDateTextBox.Visible = False
        '    validationlbl.Visible = False
        'End If

    End Sub
    Protected Sub btnSubmit_Click(sender As Object, e As System.EventArgs) Handles btnSubmit.Click
        CheckRequiredFields()
        'ValidationDateTextBox
        'If btnSubmit.Text.ToLower = "update" Then

        '    If ValidationDateTextBox.Text = "" Then
        '        If Session("SecurityLevelNum") > 89 Then

        '            lblValidation.Text = "Must supply a Validation Date."
        '            lblValidation.Visible = True
        '            SetFocus(ValidationDateTextBox)

        '            Exit Sub

        '        End If
        '    End If
        'End If


        'If (Regex.IsMatch(CostCenterTextbox.Text, "^[0-9 ]") = False) Then

        '    lblValidation.Text = "Cost Center must only contain numbers."
        '    lblValidation.Visible = True
        '    CostCenterTextbox.Text = ""
        '    SetFocus(CostCenterTextbox)

        '    Exit Sub

        'End If

        If LocationTypeCdrbl.SelectedIndex = -1 Then

            lblValidation.Text = "Must Select Location Type."
            lblValidation.Visible = True
            SetFocus(LocationTypeCdrbl)

            Exit Sub


        End If

        'If (Regex.IsMatch(Ziptextbox.Text, "^[0-9 ]") = False) Then

        '    lblValidation.Text = "Zip Code must only contain numbers."
        '    lblValidation.Visible = True
        '    Ziptextbox.Text = ""

        '    SetFocus(Ziptextbox)

        '    Exit Sub

        'End If


        'If (Regex.IsMatch(OfficeNPITextBox.Text, "^[0-9 ]") = False) Then

        '    lblValidation.Text = "Location NPI must only contain numbers."
        '    lblValidation.Visible = True
        '    OfficeNPITextBox.Text = ""

        '    SetFocus(OfficeNPITextBox)

        '    Exit Sub

        'End If


        'If (Regex.IsMatch(GroupNPITextBox.Text, "^[0-9 ]") = False) Then

        '    lblValidation.Text = "Group NPI must only contain numbers."
        '    lblValidation.Visible = True
        '    GroupNPITextBox.Text = ""

        '    SetFocus(GroupNPITextBox)

        '    Exit Sub

        'End If

        If iGroupNum > 0 Then

            'FormSignOn.AddInsertParm("@GroupNum", iGroupNum, SqlDbType.Int, 9)

            FormSignOn.UpdateForm()

        Else

            'FormSignOn.AddInsertParm("@GroupNum", iGroupNum, SqlDbType.Int, 9)
            NewLocationForm.InsertData()

        End If
        Response.Redirect("~/" & "PhysGrpMaintenance.aspx?GroupType=Gnum", True)


    End Sub

    Protected Sub btnReturn_Click(sender As Object, e As System.EventArgs) Handles btnReturn.Click
        Response.Redirect("~/" & "PhysGrpMaintenance.aspx?GroupType=Gnum", True)

    End Sub

    Protected Sub ValidationDateTextBox_TextChanged(sender As Object, e As System.EventArgs) Handles ValidationDateTextBox.TextChanged
        hdValidationdate.Text = ValidationDateTextBox.Text
    End Sub
End Class
