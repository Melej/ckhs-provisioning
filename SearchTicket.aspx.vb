﻿Imports System.IO
Imports System.Data '
Imports System.Data.SqlClient
Imports App_Code
Imports AjaxControlToolkit
Imports C1.Web.UI.Controls.C1Window

Partial Class SearchTicket
    Inherits MyBasePage '   Inherits System.Web.UI.Page
    Dim sErr As String = ""
    Dim conn As New SetDBConnection()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


		If Session("SecurityLevelNum") < 25 Then
			Response.Redirect("~/Security.aspx")
		End If

		If Session("SecurityLevelNum") < 69 Then
			Response.Redirect("~/MyRequests.aspx")

		End If


		Dim tp As New ToolTipHelper()

		lblValidation.Attributes.Add("class", "masterTooltip")
        lblValidation.Attributes.Add("title", tp.ToolTipHtml())


		Dim ddlBinder As New DropDownListBinder

		If Not IsPostBack Then

			If Session("PreviousSearchCriteria") <> "" Then
				' split  at :
				Dim Searcharr As String()
				Dim Searchtype As String = ""
				Dim SearchValue As String = ""

				Searcharr = Split(Session("PreviousSearchCriteria"), ":")

				Searchtype = Searcharr(0)
				SearchValue = Searcharr(1)

				Select Case Searchtype
					Case "Request"
						request_numTextBox.Text = SearchValue
					Case "Lastname"
						last_nameTextBox.Text = SearchValue
						btnSearchTicket_Click(e, EventArgs.Empty)

					Case "Keyword"
						KeywordTextBox.Text = SearchValue
						btnSearchTicket_Click(e, EventArgs.Empty)

					Case "Assettag"
						AssetTextBox.Text = SearchValue
						btnSearchTicket_Click(e, EventArgs.Empty)

					Case "Group"
						HDCSCGrp.Text = SearchValue
						cscgroupddl.SelectedValue = SearchValue
						btnSearchTicket_Click(e, EventArgs.Empty)

					Case "Tech"
						HDCSCTech.Text = SearchValue
						CSCTechddl.SelectedValue = SearchValue
						btnSearchTicket_Click(e, EventArgs.Empty)

					Case Else



				End Select

			End If

			'ddlBinder.BindData(Departmentddl, "sel_department_codes", "department_cd", "department_name", Session("EmployeeConn"))

			Dim ddlCurrGroup As New DropDownListBinder(cscgroupddl, "SelTechGroup", Session("CSCConn"))
			ddlCurrGroup.AddDDLCriteria("@display", "queue", SqlDbType.NVarChar)
			ddlCurrGroup.AddDDLCriteria("@ntlogin", Session("LoginID"), SqlDbType.NVarChar)

			ddlCurrGroup.BindData("group_num", "group_name")


			Dim ddlCurrTech As New DropDownListBinder(CSCTechddl, "SelTechniciansForRpt", Session("EmployeeConn"))


			ddlCurrTech.AddDDLCriteria("@status", "active", SqlDbType.NVarChar)

			ddlCurrTech.BindData("client_num", "tech_name")

		End If

		If Session("SecurityLevelNum") > 79 Then
            btnTicketDash.Visible = True

        End If



    End Sub
    Protected Sub btnSearchTicket_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearchTicket.Click
        lblValidation.Text = "Test"
        gvSearch.Visible = True
        gvSearch.SelectedIndex = -1

        gvRequests.Visible = True
        gvRequests.SelectedIndex = -1

        gvAssetView.Visible = True
        gvAssetView.SelectedIndex = -1

        gvCSCGroup.Visible = True
        tblByGroup.Visible = True
        gvCSCGroup.SelectedIndex = -1

        lblValidation.Visible = False
		SearchStatuslbl.Text = ""

		'Session("PreviousSearchCriteria") = ""

		If request_numTextBox.Text <> "" Then

			Session("PreviousSearchCriteria") = "Request:" & request_numTextBox.Text

			Dim thisData As New CommandsSqlAndOleDb("selTicketRFSByNumV7", Session("CSCConn"))
            thisData.AddSqlProcParameter("@requestnum", request_numTextBox.Text, SqlDbType.NVarChar, 20)

            Dim dt As DataTable
            Dim RequestNum As Integer
            Dim ReturnRows As Integer

            dt = thisData.GetSqlDataTable

            ReturnRows = dt.Rows.Count()


            If ReturnRows > 0 Then
                RequestNum = IIf(IsDBNull(dt.Rows(0)("request_num")), Nothing, dt.Rows(0)("request_num"))

                If RequestNum > 0 Then
                    Response.Redirect("./ViewRequestTicket.aspx?RequestNum=" & RequestNum, True)

                End If
            Else
                searchlbl.Text = ""
                lblValidation.Text = "Ticket/RFS not found"
                lblValidation.Visible = True
                Exit Sub
            End If

			'smallreset()

		End If


        If last_nameTextBox.Text.Length > 0 Then
			' fillRequestbyLastNameTable(last_nameTextBox.Text)
			Session("PreviousSearchCriteria") = "Lastname:" & last_nameTextBox.Text

			Dim thisData As New CommandsSqlAndOleDb("SelSimpleSearchV3", Session("EmployeeConn"))
            thisData.AddSqlProcParameter("@LastName", last_nameTextBox.Text, SqlDbType.NVarChar, 20)

            gvSearch.DataSource = thisData.GetSqlDataTable
            gvSearch.DataBind()

            gvSearch.Visible = True

            Dim emptytbl As New DataTable
            emptytbl = thisData.GetSqlDataTable

            Session("emptytbl") = emptytbl

            Dim theseRows As Integer

            theseRows = emptytbl.Rows.Count()



            gvRequests.Visible = False
            gvAssetView.Visible = False

            gvCSCGroup.Visible = False
            tblByGroup.Visible = False

            gvTechView.Visible = False


			'smallreset()

			searchlbl.Text = Nothing

            SearchStatuslbl.Text = "Search Results for name: " & last_nameTextBox.Text & " Rows: " & theseRows

        End If

        'KeywordTextBox

        If KeywordTextBox.Text.Length > 0 Then
			Session("PreviousSearchCriteria") = "Keyword:" & KeywordTextBox.Text

			Dim thisData As New CommandsSqlAndOleDb("selTicketByKeyword", Session("CSCConn"))
            thisData.AddSqlProcParameter("@keyword", KeywordTextBox.Text, SqlDbType.NVarChar, 250)

            gvRequests.DataSource = thisData.GetSqlDataTable
            gvRequests.DataBind()

            gvRequests.Visible = True


            Dim Keywordtbl As New DataTable
            Keywordtbl = thisData.GetSqlDataTable

            Session("Keywordtbl") = Keywordtbl

            Dim theseRows As Integer

            theseRows = Keywordtbl.Rows.Count()


            gvSearch.Visible = False
            gvAssetView.Visible = False

            gvCSCGroup.Visible = False
            tblByGroup.Visible = False

            gvTechView.Visible = False


			'smallreset()

			searchlbl.Text = "Open and Closed TICKETS & RFS"
            searchlbl.ForeColor = Color.DarkCyan

            SearchStatuslbl.Text = "Search Results for Keyname: " & KeywordTextBox.Text & " Rows: " & theseRows

        End If

		'selTicketByAssetTag

		If AssetTextBox.Text.Length > 0 Then
			Session("PreviousSearchCriteria") = "Assettag:" & AssetTextBox.Text


			Dim thisData As New CommandsSqlAndOleDb("selTicketByAssetTag", Session("CSCConn"))
			thisData.AddSqlProcParameter("@assetTag", AssetTextBox.Text, SqlDbType.NVarChar, 30)

			gvAssetView.DataSource = thisData.GetSqlDataTable
			gvAssetView.DataBind()

			Dim AssTagtbl As New DataTable
			AssTagtbl = thisData.GetSqlDataTable

			Dim theseRows As Integer

			theseRows = AssTagtbl.Rows.Count()

			gvAssetView.Visible = True
			Session("asstagtbl") = AssTagtbl


			gvSearch.Visible = False
			gvRequests.Visible = False
			gvCSCGroup.Visible = False
			gvTechView.Visible = False

			'smallreset()

			searchlbl.Text = "Open and Closed TICKETS & RFS"
			searchlbl.ForeColor = Color.DarkCyan

			SearchStatuslbl.Text = "Search Results for Asset Tag : " & AssetTextBox.Text & " Rows: " & theseRows

		End If

		If HDCSCGrp.Text <> "" Then

			'fillTicketsByCSCGroup(HDCSCGrp.Text)
			Session("PreviousSearchCriteria") = "Group:" & HDCSCGrp.Text

			Dim thisData As New CommandsSqlAndOleDb("SelSearchTicketByCSCGroupV3", Session("CSCConn"))
            thisData.AddSqlProcParameter("@groupnum", HDCSCGrp.Text, SqlDbType.NVarChar, 20)

            gvCSCGroup.DataSource = thisData.GetSqlDataTable

            Dim cscgrouptbl As New DataTable
            Dim cscGroupcount As Integer
            'Dim ReqLink As New DataColumn("Req#", GetType(String))

            searchlbl.Text = "Open TICKETS & RFS Only for " & HDCSCGroupName.Text
            searchlbl.ForeColor = Color.Red

            cscgrouptbl = thisData.GetSqlDataTable
            Session("cscgrouptbl") = cscgrouptbl

            cscGroupcount = cscgrouptbl.Rows.Count()


            gvCSCGroup.DataBind()

            gvCSCGroup.Visible = True

            gvSearch.Visible = False
            gvRequests.Visible = False
            gvAssetView.Visible = False
            gvTechView.Visible = False


			'AssetTextBox.Text = Nothing
			'smallreset()

			'cscgroupddl.SelectedIndex = -1
			'CSCTechddl.SelectedIndex = -1


			SearchStatuslbl.Text = "Search Results by CSC Group : " & HDCSCGroupName.Text & " Rows: " & cscGroupcount

        End If

        If HDCSCTech.Text <> "" Then

			Session("PreviousSearchCriteria") = "Tech:" & HDCSCTech.Text

			Dim thisData As New CommandsSqlAndOleDb("SelSearchTicketByCSCTechV3", Session("CSCConn"))
            thisData.AddSqlProcParameter("@clientnum", HDCSCTech.Text, SqlDbType.NVarChar, 20)

            gvTechView.DataSource = thisData.GetSqlDataTable
            gvTechView.DataBind()

            gvTechView.Visible = True

            searchlbl.Text = "Open & Closed TICKETS & RFS for " & HDTechname.Text
            searchlbl.ForeColor = Color.Red


            Dim cscTech As New DataTable
            cscTech = thisData.GetSqlDataTable

            Dim theseRows As Integer

            theseRows = cscTech.Rows.Count()

            cscTech = thisData.GetSqlDataTable
            Session("cscTech") = cscTech




            gvSearch.Visible = False
            gvRequests.Visible = False
            gvAssetView.Visible = False
            gvCSCGroup.Visible = False

            KeywordTextBox.Text = Nothing
            last_nameTextBox.Text = Nothing
            'AssetTextBox.Text = Nothing
            request_numTextBox.Text = Nothing

            HDCSCGrp.Text = Nothing
            HDCSCTech.Text = Nothing

            cscgroupddl.SelectedIndex = -1
            CSCTechddl.SelectedIndex = -1

            SearchStatuslbl.Text = "Search Results by CSC Tech : " & HDTechname.Text & " Rows: " & theseRows

			'smallreset()

		End If

    End Sub
    Public Sub smallreset()
        KeywordTextBox.Text = Nothing
        last_nameTextBox.Text = Nothing
        AssetTextBox.Text = Nothing
        request_numTextBox.Text = Nothing

        HDCSCGrp.Text = Nothing
        HDCSCTech.Text = Nothing

        cscgroupddl.SelectedIndex = -1
        CSCTechddl.SelectedIndex = -1

    End Sub

    Public Sub Reset()
        ' clear out all fields
        Dim emptytbl As New DataTable
        Dim cscTech As New DataTable
        Dim cscgrouptbl As New DataTable
        Dim AssTagtbl As New DataTable
        Dim Keywordtbl As New DataTable


        KeywordTextBox.Text = Nothing
        last_nameTextBox.Text = Nothing
        AssetTextBox.Text = Nothing
        request_numTextBox.Text = Nothing

        gvSearch.Visible = False
        gvSearch.SelectedIndex = -1

        gvRequests.Visible = False
        gvRequests.SelectedIndex = -1

        gvAssetView.Visible = False
        gvAssetView.SelectedIndex = -1

        gvSearch.DataSource = emptytbl
        gvSearch.DataBind()

        gvRequests.DataSource = Keywordtbl
        gvRequests.DataBind()

        gvAssetView.DataSource = AssTagtbl
        gvAssetView.DataBind()

        gvCSCGroup.DataSource = cscgrouptbl
        gvCSCGroup.DataBind()

        gvTechView.DataSource = cscTech
        gvTechView.DataBind()


        HDCSCGrp.Text = Nothing
        HDCSCTech.Text = Nothing

        cscgroupddl.SelectedIndex = -1
        CSCTechddl.SelectedIndex = -1

        SearchStatuslbl.Text = ""
    End Sub
    Protected Sub fillRequestbyLastNameTable(ByVal sLastname As String)
        Dim thisData As New CommandsSqlAndOleDb("SelSimpleSearchV3", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@LastName", sLastname, SqlDbType.NVarChar, 20)

        Dim iRowCount = 1

        Dim dt As DataTable
        dt = thisData.GetSqlDataTable

        Dim rwHeader As New TableRow

        Dim clBtnheader As New TableCell
        Dim clFullNameHeader As New TableCell
        Dim clphoneHeader As New TableCell
        Dim clTicketStatusHeader As New TableCell
        Dim clloginnameheader As New TableCell

        clBtnheader.Text = ""
        clFullNameHeader.Text = "Name"
        clphoneHeader.Text = "Phone"
        clTicketStatusHeader.Text = "Status"
        clloginnameheader.Text = " Account"


        rwHeader.Cells.Add(clBtnheader)
        rwHeader.Cells.Add(clFullNameHeader)
        rwHeader.Cells.Add(clphoneHeader)
        rwHeader.Cells.Add(clTicketStatusHeader)
        rwHeader.Cells.Add(clloginnameheader)


        rwHeader.Font.Bold = True
        rwHeader.BackColor = Color.FromName("#006666")
        rwHeader.ForeColor = Color.FromName("White")
        rwHeader.Height = Unit.Pixel(36)

        tblSearchByName.Rows.Add(rwHeader)

        For Each drRow As DataRow In dt.Rows


            Dim rwData As New TableRow

            'Dim clBtn As New TableCell

            Dim clReqNum As New TableCell

            Dim claccountRequestNum As New TableCell
            Dim clFullNameData As New TableCell
            Dim clphone As New TableCell
            Dim clTicketStatus As New TableCell
            Dim clloginname As New TableCell

            claccountRequestNum.Text = IIf(IsDBNull(drRow("client_num")), "", drRow("client_num"))

            clFullNameData.Text = IIf(IsDBNull(drRow("FullName")), "", drRow("FullName"))
            clphone.Text = IIf(IsDBNull(drRow("phone")), "", drRow("phone"))

            clTicketStatus.Text = IIf(IsDBNull(drRow("TicketStatus")), "", drRow("TicketStatus"))
            clloginname.Text = IIf(IsDBNull(drRow("login_name")), "", drRow("login_name"))
            clReqNum.Text = "<a href=""ClientDemo.aspx?ClientNum==" & claccountRequestNum.Text & """ ><u><b> " & claccountRequestNum.Text & "</b></u></a>"



            rwData.Cells.Add(clReqNum)
            rwData.Cells.Add(clFullNameData)

            rwData.Cells.Add(clphone)
            rwData.Cells.Add(clTicketStatus)
            rwData.Cells.Add(clloginname)


            If iRowCount > 0 Then

                rwData.BackColor = Color.Bisque

            Else

                rwData.BackColor = Color.LightGray

            End If

            tblSearchByName.Rows.Add(rwData)

            iRowCount = iRowCount * -1

        Next
        tblSearchByName.CellPadding = 10
        tblSearchByName.Width = New Unit("100%")

        tblSearchByName.Visible = True


    End Sub

    Protected Sub fillTicketsByCSCGroup(ByVal sGroup As String)
        Dim thisData As New CommandsSqlAndOleDb("SelSearchTicketByCSCGroupV3", Session("CSCConn"))
        thisData.AddSqlProcParameter("@groupnum", sGroup, SqlDbType.NVarChar, 20)

        Dim iRowCount = 1

        Dim dt As DataTable
        dt = thisData.GetSqlDataTable

        Session("CSCGrouptable") = dt


        Dim theseRows As Integer
        theseRows = dt.Rows.Count()

        SearchStatuslbl.Text = "Search Results by CSC Group : " & HDCSCGroupName.Text & " Rows: " & theseRows


        Dim rwHeader As New TableRow

        Dim clBtnheader As New TableCell
        Dim clFullNameHeader As New TableCell
        Dim clTechNameHeader As New TableCell
        Dim clentrydateHeader As New TableCell
        Dim clrequesttypeheader As New TableCell

        'request_num
        'client_num
        'FullName 
        'last_name
        'entry_date
        'request_type
        'category_cd
        'priority
        'asset_tag
        'current_group_num
        'current_group_name
        'TechName

        clBtnheader.Text = ""
        clFullNameHeader.Text = "Client Name"
        clTechNameHeader.Text = "Tech Name"
        clentrydateHeader.Text = "Entred Date"
        clrequesttypeheader.Text = " Type "


        rwHeader.Cells.Add(clBtnheader)
        rwHeader.Cells.Add(clFullNameHeader)
        rwHeader.Cells.Add(clTechNameHeader)
        rwHeader.Cells.Add(clentrydateHeader)
        rwHeader.Cells.Add(clrequesttypeheader)


        rwHeader.Font.Bold = True
        rwHeader.BackColor = Color.FromName("#006666")
        rwHeader.ForeColor = Color.FromName("White")
        rwHeader.Height = Unit.Pixel(36)

        tblByGroup.Rows.Add(rwHeader)

        dt.DefaultView.Sort = "entry_date"

        'For Each drRow As DataView In dt.DefaultView
        For Each drRow As DataRow In dt.Rows


            Dim rwData As New TableRow

            Dim clBtn As New TableCell

            Dim clReqNum As New TableCell


            Dim clRequestNum As New TableCell
            Dim clFullNameData As New TableCell
            Dim clTechName As New TableCell
            Dim clentrydate As New TableCell
            Dim clrequesttype As New TableCell

            clRequestNum.Text = IIf(IsDBNull(drRow("Request")), "", drRow("Request"))
            clReqNum.Text = "<a href=""ViewRequestTicket.aspx?RequestNum=" & clRequestNum.Text & """ ><u><b> " & clRequestNum.Text & "</b></u></a>"


            clFullNameData.Text = IIf(IsDBNull(drRow("FullName")), "", drRow("FullName"))
            clTechName.Text = IIf(IsDBNull(drRow("TechName")), "", drRow("TechName"))

            clentrydate.Text = IIf(IsDBNull(drRow("entry_date")), "", drRow("entry_date"))
            clrequesttype.Text = IIf(IsDBNull(drRow("request_type")), "", drRow("request_type"))




            rwData.Cells.Add(clReqNum)
            rwData.Cells.Add(clFullNameData)

            rwData.Cells.Add(clTechName)
            rwData.Cells.Add(clentrydate)
            rwData.Cells.Add(clrequesttype)


            If iRowCount > 0 Then

                rwData.BackColor = Color.Bisque

            Else

                rwData.BackColor = Color.LightGray

            End If

            tblByGroup.Rows.Add(rwData)

            iRowCount = iRowCount * -1

        Next
        tblByGroup.CellPadding = 10
        tblByGroup.Width = New Unit("100%")

        tblByGroup.Visible = True


    End Sub

    Private Function GetSortDirection(ByVal column As String) As String

        ' By default, set the sort direction to ascending.
        Dim sortDirection = "ASC"

        ' Retrieve the last column that was sorted.
        Dim sortExpression = TryCast(ViewState("SortExpression"), String)

        If sortExpression IsNot Nothing Then
            ' Check if the same column is being sorted.
            ' Otherwise, the default value can be returned.
            If sortExpression = column Then
                Dim lastDirection = TryCast(ViewState("SortDirection"), String)
                If lastDirection IsNot Nothing _
                  AndAlso lastDirection = "ASC" Then

                    sortDirection = "DESC"

                End If
            End If
        End If

        ' Save new values in ViewState.
        ViewState("SortDirection") = sortDirection
        ViewState("SortExpression") = column

        Return sortDirection

    End Function
    Protected Sub gvSearch_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvSearch.RowCommand
        If e.CommandName = "Select" Then
            Dim client_num As String = gvSearch.DataKeys(Convert.ToInt32(e.CommandArgument))("client_num").ToString()
            'If rblSystem.SelectedValue = "clients" Then
            '    Response.Redirect("~/ClientDemo.aspx?ClientNum=" & client_num, True)

            'ElseIf rblSystem.SelectedValue = "deactivated" Then
            '    Response.Redirect("~/DeactivatedAccount.aspx?ClientNum=" & client_num)

            'End If

            Response.Redirect("~/ClientDemo.aspx?ClientNum=" & client_num, True)

        End If

    End Sub
    
    Protected Sub gvRequests_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvRequests.RowCommand
        'Dim request_num As String

        'request_num = gvRequests.DataKeys(Convert.ToInt32(e.CommandArgument))("request_num").ToString()

        'Response.Redirect("~/ViewRequestTicket.aspx?RequestNum=" & request_num, True)

    End Sub
    Protected Sub gvAssetView_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvAssetView.RowCommand

        If e.CommandName = "Select" Then

            Dim request_num As String

            request_num = gvAssetView.DataKeys(Convert.ToInt32(e.CommandArgument))("request_num").ToString()

            Response.Redirect("~/ViewRequestTicket.aspx?RequestNum=" & request_num, True)

        End If


    End Sub
    Protected Sub gvAssetView_Sorting(sender As Object, e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvAssetView.Sorting
        Dim sortdt = TryCast(Session("asstagtbl"), DataTable)

        If sortdt IsNot Nothing Then

            'Sort the data.
            sortdt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression)
            gvAssetView.DataSource = sortdt
            gvAssetView.DataBind()

        End If

    End Sub

    Protected Sub gvSearch_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvSearch.RowDataBound

        If (e.Row.RowType = DataControlRowType.DataRow) Then  ' Check to make sure it isn't not getting header or footer

            '   Dim drv As DataRowView = CType(e.Row.Cells, DataRowView)

            Dim ClientNum As String = gvSearch.DataKeys(e.Row.RowIndex)("client_num")
            Dim lblUserName As Label

            lblUserName = e.Row.FindControl("lblLoginName")
            Dim accts As New ClientAccounts(ClientNum)
            If accts.AccountsByAcctNum(9).Count > 1 Then
                Dim sAccts As String = ""

                For Each acct As ClientAccountAccess In accts.AccountsByAcctNum(9)

                    sAccts = sAccts + acct.LoginName + "<br>"


                Next

                lblUserName.Attributes.Add("class", "masterTooltip")
                lblUserName.Attributes.Add("title", sAccts)
                lblUserName.ForeColor = Color.Blue
            End If

            'Dim btnAssign As Button
            'Dim btnAccept As Button

            'btnAssign = e.Row.FindControl("btnLinkView")
            'btnAccept = e.Row.FindControl("btnAcceptCase")


            'If diseaseID <> "" Then

            '    btnAssign.Visible = False

            'Else

            '    btnAssign.Visible = True

            'End If

            'If CaseNtLogin <> Session("ntLogin") And CaseNtLogin <> "" Then

            '    btnAccept.Visible = True

            'ElseIf CaseNtLogin = Session("ntLogin") Or CaseNtLogin = "" Then

            '    btnAccept.Visible = False

            'End If
        End If

    End Sub

    Protected Sub btnReset_Click(sender As Object, e As System.EventArgs) Handles btnReset.Click
        Reset()
    End Sub

    Protected Sub cscgroupddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cscgroupddl.SelectedIndexChanged
        HDCSCGrp.Text = cscgroupddl.SelectedValue
        HDCSCGroupName.Text = cscgroupddl.SelectedItem.ToString


    End Sub

    Protected Sub CSCTechddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles CSCTechddl.SelectedIndexChanged
        HDCSCTech.Text = CSCTechddl.SelectedValue
        HDTechname.Text = CSCTechddl.SelectedItem.ToString


    End Sub

    Protected Sub CSCTyperbl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles CSCTyperbl.SelectedIndexChanged
        hdCSCStatus.Text = CSCTyperbl.SelectedValue

        Dim ddlCurrTech As New DropDownListBinder(CSCTechddl, "SelTechniciansForRpt", Session("EmployeeConn"))


        ddlCurrTech.AddDDLCriteria("@status", hdCSCStatus.Text, SqlDbType.NVarChar)

        ddlCurrTech.BindData("client_num", "tech_name")

    End Sub

    Protected Sub gvCSCGroup_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvCSCGroup.RowCommand

        If e.CommandName = "Select" Then

            Dim request_num As String

            request_num = gvCSCGroup.DataKeys(Convert.ToInt32(e.CommandArgument))("request_num").ToString()

            Response.Redirect("~/ViewRequestTicket.aspx?RequestNum=" & request_num, True)

        End If
        If e.CommandName = "Sort" Then


        End If
    End Sub
    Protected Sub gvCSCGroup_Sorting(sender As Object, e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvCSCGroup.Sorting
        Dim sortdt = TryCast(Session("cscgrouptbl"), DataTable)

        If sortdt IsNot Nothing Then

            'Sort the data.
            sortdt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression)
            gvCSCGroup.DataSource = sortdt
            gvCSCGroup.DataBind()

        End If
    End Sub

    Protected Sub gvTechView_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvTechView.RowCommand
        If e.CommandName = "Select" Then

            Dim request_num As String

            request_num = gvTechView.DataKeys(Convert.ToInt32(e.CommandArgument))("request_num").ToString()

            Response.Redirect("~/ViewRequestTicket.aspx?RequestNum=" & request_num, True)

        End If
    End Sub
    Protected Sub gvTechView_Sorting(sender As Object, e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvTechView.Sorting
        Dim sortdt = TryCast(Session("cscTech"), DataTable)

        If sortdt IsNot Nothing Then

            'Sort the data.
            sortdt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression)
            gvTechView.DataSource = sortdt
            gvTechView.DataBind()

        End If
    End Sub


    'Protected Sub gvCSCGroup_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvCSCGroup.RowDataBound

    '    If e.Row.RowType = DataControlRowType.DataRow Then
    '        'setColumnInfo(sender, e)

    '        Dim lblRequest As Label
    '        Dim iRequestNum As String = gvCSCGroup.DataKeys(e.Row.RowIndex)("Request")

    '        lblRequest = e.Row.FindControl("lblRequest")

    '        lblRequest.Text = iRequestNum.ToString


    '    End If

    'End Sub
    'Protected Sub setColumnInfo(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
    '    Try
    '        If e.Row.RowType = DataControlRowType.DataRow Then
    '            Dim iRequestNum As String = ""

    '            iRequestNum = e.Row.Cells(getcellNoE("Request", gvCSCGroup)).Text


    '            e.Row.Cells(0).Text = "<a href=ViewRequestTicket.aspx?RequestNum=" & iRequestNum.ToString & "</a>"

    '            'e.Row.Cells(1).Text = "<a href=ViewRequestTicket.aspx?RequestNum=" & iRequestNum.ToString & "</a>"

    '        End If
    '    Catch ex As Exception
    '        sErr = ex.Message
    '        Session("handledError") &= "RowDataBound " & sErr
    '        Throw ex
    '        ' LogTextFileWriter.DoSomeFileWritingStuff(Session("UCCurrentMethod") + ex.Message)
    '    End Try
    'End Sub


    Protected Sub gvSearch_Sorting(sender As Object, e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvSearch.Sorting
        Dim sortdt = TryCast(Session("emptytbl"), DataTable)

        If sortdt IsNot Nothing Then

            'Sort the data.
            sortdt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression)
            gvSearch.DataSource = sortdt
            gvSearch.DataBind()

        End If
    End Sub

    'Protected Sub gvRequests_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvRequests.RowDataBound
    '    Try
    '        If e.Row.RowType = DataControlRowType.DataRow Then
    '            e.Row.Cells(3).Width = Unit.Pixel(200)

    '        End If
    '    Catch ex As Exception
    '        sErr = ex.Message
    '        Session("handledError") &= "RowDataBound " & sErr
    '        Throw ex
    '        ' LogTextFileWriter.DoSomeFileWritingStuff(Session("UCCurrentMethod") + ex.Message)
    '    End Try

    'End Sub

    Protected Sub gvRequests_Sorting(sender As Object, e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvRequests.Sorting
        Dim sortdt = TryCast(Session("Keywordtbl"), DataTable)

        If sortdt IsNot Nothing Then

            'Sort the data.
            sortdt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression)
            gvRequests.DataSource = sortdt
            gvRequests.DataBind()

        End If

    End Sub

    Protected Sub btnTicketDash_Click(sender As Object, e As System.EventArgs) Handles btnTicketDash.Click
        Response.Redirect("./TicketDashboard.aspx", True)
    End Sub

    'Protected Sub tabs_ActiveTabChanged(sender As Object, e As System.EventArgs) Handles tabs.ActiveTabChanged
    '    If tabs.ActiveTabIndex = 0 Then
    '        Response.Redirect("./SearchTicket.aspx", True)

    '    ElseIf tabs.ActiveTabIndex = 1 Then
    '        Response.Redirect("./TicketDashboard.aspx", True)

    '    ElseIf tabs.ActiveTabIndex = 2 Then
    '        Response.Redirect("./ProjectMaintenance.aspx", True)
    '    End If

    'End Sub
End Class
