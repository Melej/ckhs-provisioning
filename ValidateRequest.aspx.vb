﻿Imports System.IO
Imports System.Data '
Imports System.Data.SqlClient
Imports App_Code
Imports AjaxControlToolkit
Imports C1.Web.UI.Controls.C1Window
Imports System.Net
Imports System
Imports System.Net.Mail

Partial Class ValidateRequest
    Inherits MyBasePage '   Inherits System.Web.UI.Page

    Dim AccountSeqNum As Integer
    Dim sActivitytype As String = ""
    Dim cblBinder As New CheckBoxListBinder
    Dim FormViewRequest As New FormSQL()
    Dim AccountRequestNum As Integer
    Dim AccountRequestType As String
    Dim dtRequestedAccounts As DataTable


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        FormViewRequest = New FormSQL(Session("EmployeeConn"), "SelAccountRequestV20", "InsAccountRequestV20", "", Page)
        AccountRequestNum = Request.QueryString("RequestNum")
        Dim req As New AccountRequest(AccountRequestNum, Session("EmployeeConn"))


        If Not IsPostBack Then

            FormViewRequest.AddSelectParm("@account_request_num", AccountRequestNum, SqlDbType.Int, 4)
            FormViewRequest.FillForm()

            bindAppRequests()
            AccountRequestType = FormViewRequest.getFieldValue("account_request_type")

            If AccountRequestType = "rehire" Then
                lblRehire.Visible = True
                lblRehire.Text = "THIS IS A REHIRE EMPLOYEE CHECK FOR a DEACTIVATED ACCOUNT , Use That account Name if Possible"

            End If
			'If AccountRequestType = "addpmhact" Then

			'	Response.Redirect("~/AccountRequestInfo2.aspx?RequestNum=" & AccountRequestNum, True)

			'End If

			'Nurse instructor
			If PositionRoleNumLabel.Text = "1426" Then
					If login_nameLabel.Text <> "" Then
						UserNameTextBox.Text = login_nameLabel.Text
					Else
						UserNameTextBox.Text = SuggestedUserName(FormViewRequest.getFieldValue("first_name").ToString, FormViewRequest.getFieldValue("last_name").ToString)
						'UserNameTextBox.Text = UserNameTextBox.Text.Substring(0, 2) & UserNameTextBox.Text.Substring(4, 2) & "NI"
					End If

				Else
					UserNameTextBox.Text = SuggestedUserName(FormViewRequest.getFieldValue("first_name").ToString, FormViewRequest.getFieldValue("last_name").ToString)

				End If

				lblCustomNameTest.Text = "Must Test User Name to Validate Request"

			End If

	End Sub

    Private Function NTRequested() As Boolean
        Dim blnReturn As Boolean = False



        For Each rw As GridViewRow In gvAppRequests.Rows
            If rw.RowType = DataControlRowType.DataRow Then

                Dim ApplicationNum As String = gvAppRequests.DataKeys(rw.RowIndex)("ApplicationNum")
                If ApplicationNum = 9 Then
                    blnReturn = True

                End If

            End If

        Next





        Return blnReturn
    End Function
    Protected Sub bindAppRequests()


        Dim thisData As New CommandsSqlAndOleDb("SelItemsByNumValidCd", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int)
        thisData.AddSqlProcParameter("@valid_cd", "NeedsValidation", SqlDbType.NVarChar, 20)


        dtRequestedAccounts = thisData.GetSqlDataTable

        gvAppRequests.DataSource = dtRequestedAccounts
        gvAppRequests.DataBind()

        Dim itemcount As Integer

        itemcount = gvAppRequests.Rows.Count()

        If gvAppRequests.Rows.Count() <= 0 Then
            Response.Redirect("./ValidateQueue.aspx")
        End If


    End Sub

    Protected Sub gvAppRequests_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvAppRequests.RowCommand
        FillInvalidCodes()


        With pnlInvalidItem
            .Visible = True
            With .Style
                .Add("LEFT", "260px")
                .Add("TOP", "190px")
                .Add("POSITION", "absolute")
                .Add("Z-INDEX", "999")
            End With
        End With


        'If e.CommandName = "Select" Then


        'End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("./ValidateQueue.aspx")

    End Sub
    Protected Sub btnSubmitInvalid_Click(sender As Object, e As System.EventArgs) Handles btnSubmitInvalid.Click
        AccountSeqNum = lblHiddenSeqNum.Text

        Dim thisData As New CommandsSqlAndOleDb("UpdValidateRequestItemsV4", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int)
        thisData.AddSqlProcParameter("@account_request_seq_num", AccountSeqNum, SqlDbType.Int)
        thisData.AddSqlProcParameter("@nt_login", Session("LoginID"), SqlDbType.NVarChar, 50)
        thisData.AddSqlProcParameter("@validate", "invalid", SqlDbType.NVarChar, 10)
        If Invalidddl.SelectedIndex > 0 Then
            thisData.AddSqlProcParameter("@Invalidcd", Invalidddl.SelectedValue, SqlDbType.NVarChar, 75)
        End If

        thisData.AddSqlProcParameter("@action_desc", InvalidDescTextBox.Text, SqlDbType.NVarChar, 300)


        thisData.ExecNonQueryNoReturn()
        bindAppRequests()

    End Sub

    Protected Sub btnValidate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnValidate.Click

        Dim cmd As New CommandsSqlAndOleDb("UpdValidateRequestV4", Session("EmployeeConn"))
        cmd.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int, 6)
        cmd.AddSqlProcParameter("@nt_login", Session("LoginID"), SqlDbType.NVarChar, 20)
        cmd.AddSqlProcParameter("@validate", "Validated", SqlDbType.NVarChar, 20)


        cmd.ExecNonQueryNoReturn()

        If AccountRequestType <> "delacct" And AccountRequestType <> "terminate" Then

            If UserNameTextBox.Text = "" Then
                currLoginName.Text = "Must Supply an Account Name"
                currLoginName.Visible = True
                Exit Sub
            End If

			Dim cmdUpdateAccount As New CommandsSqlAndOleDb("UpdAccountRequestV9", Session("EmployeeConn"))
			cmdUpdateAccount.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int, 8)
            cmdUpdateAccount.AddSqlProcParameter("@share_drive", share_driveTextBox.Text, SqlDbType.NVarChar, 20)
            cmdUpdateAccount.AddSqlProcParameter("@ModelAfter", ModelAfterTextBox.Text, SqlDbType.NVarChar, 100)
            cmdUpdateAccount.AddSqlProcParameter("@New_login_name", UserNameTextBox.Text, SqlDbType.NVarChar, 20)
            cmdUpdateAccount.AddSqlProcParameter("@nt_login", Session("LoginID"), SqlDbType.NVarChar, 50)
            cmdUpdateAccount.AddSqlProcParameter("@validate", "validated", SqlDbType.NVarChar, 10)

            'If cbUseCustomName.Checked = True Then

            '    cmdUpdateAccount.AddSqlProcParameter("@New_login_name", CustomUserNameTextBox.Text, SqlDbType.NVarChar, 20)
            'Else

            '    cmdUpdateAccount.AddSqlProcParameter("@New_login_name", UserNameTextBox.Text, SqlDbType.NVarChar, 20)
            'End If

            cmdUpdateAccount.ExecNonQueryNoReturn()

            'If Session("environment").ToString.ToLower <> "testing" Then
            '    sendAlertEmail()
            'End If

            sendAlertEmail()

            Response.Redirect("./ValidateQueue.aspx")

        End If

        Response.Redirect("./ValidateQueue.aspx")




    End Sub


    Protected Sub btnInvalid_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim btnInvalid As Button = sender
        Dim gvrCase As GridViewRow = CType(btnInvalid.NamingContainer, GridViewRow)
        Dim HiddenSeqNum As String = gvAppRequests.DataKeys(gvrCase.RowIndex).Values("account_request_seq_num")
        Dim HiddenLogin As String = IIf(IsDBNull(gvAppRequests.DataKeys(gvrCase.RowIndex).Values("login_name")), "", gvAppRequests.DataKeys(gvrCase.RowIndex).Values("login_name"))
        Dim InvalidRequestItem As String = gvAppRequests.Rows(gvrCase.RowIndex).Cells(1).Text



        lblInvalidRequestItem.Text = InvalidRequestItem
        lblHiddenSeqNum.Text = HiddenSeqNum
        lblHiddenLogin.Text = HiddenLogin



        mpeInvalidItem.Show()


    End Sub

    'Protected Sub btnInvalidSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnInvalidSubmit.Click

    '    Dim account_request_seq_num As String = lblHiddenSeqNum.Text
    '    Dim login_name As String = lblHiddenLogin.Text


    '    Dim thisData As New CommandsSqlAndOleDb("UpdValidateRequestItems", Session("EmployeeConn"))
    '    thisData.AddSqlProcParameter("@account_request_seq_num", account_request_seq_num, SqlDbType.Int)
    '    thisData.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int)
    '    thisData.AddSqlProcParameter("@requestor_client_num", Session("ClientNum"), SqlDbType.NVarChar, 30)
    '    thisData.AddSqlProcParameter("@validate", "invalid", SqlDbType.NVarChar, 10)
    '    thisData.AddSqlProcParameter("@action_desc", txtInvalidComments.Text, SqlDbType.NVarChar, 255)


    '    thisData.ExecNonQueryNoReturn()

    '    bindAppRequests()

    'End Sub

    Private Function SuggestedUserName(ByVal FirstName As String, ByVal LastName As String) As String
        Dim sReturn As String = ""


        Dim cmdSql As New SqlCommand
        Dim connSql As New SqlConnection(Session("EmployeeConn"))
        Dim drSql As SqlClient.SqlDataReader

        If receiver_client_numLabel.Text = "" Then
            receiver_client_numLabel.Text = "0"
        End If

		'   connSql.ConnectionString = _DataConn

		cmdSql.CommandText = "SelSuggestedUserNameLDAPV9"
		cmdSql.CommandType = CommandType.StoredProcedure
        cmdSql.Connection = connSql
        cmdSql.Parameters.Add("@FirstName", SqlDbType.NVarChar).Value = FirstName
        cmdSql.Parameters.Add("@LastName", SqlDbType.NVarChar).Value = LastName
        cmdSql.Parameters.Add("@client_num", SqlDbType.Int).Value = receiver_client_numLabel.Text


        connSql.Open()

        Try




            drSql = cmdSql.ExecuteReader


            While drSql.Read()



				sReturn = IIf(IsDBNull(drSql("SuggestedName")), "", drSql("SuggestedName"))
                CurrentAccountLabel.Text = IIf(IsDBNull(drSql("CurrentAccount")), "", drSql("CurrentAccount"))
				HDCurrentADAccount.Text = IIf(IsDBNull(drSql("ClienthasthisAD")), "", drSql("ClienthasthisAD"))
				HDEcareAccount.Text = IIf(IsDBNull(drSql("eCareAccount")), "", drSql("eCareAccount"))
				HDADNotFounf.Text = IIf(IsDBNull(drSql("ADNotFound")), "", drSql("ADNotFound"))


				If HDCurrentADAccount.Text = CurrentAccountLabel.Text Then
					currLoginName.Text = "This User HAS THIS AD ACCOUNT: "
					currLoginName.ForeColor = Color.Purple

					If CurrentAccountLabel.Text = sReturn Then
						currLoginName.Text = "This User HAS THIS AD ACCOUNT: "
						currLoginName.ForeColor = Color.Purple
					ElseIf HDEcareAccount.Text <> "" Then
						currLoginName.Text = HDEcareAccount.Text

					End If

				ElseIf CurrentAccountLabel.Text = "" Then
					CurrentAccountLabel.Visible = False
					CurrentAccountLabel.Visible = False
					currLoginName.Visible = False


				ElseIf CurrentAccountLabel.Text = sReturn Then
					If HDCurrentADAccount.Text = CurrentAccountLabel.Text Then
						currLoginName.Text = "This User HAS THIS AD ACCOUNT: "
						currLoginName.ForeColor = Color.Purple

					ElseIf HDCurrentADAccount.Text <> CurrentAccountLabel.Text Then

						currLoginName.Text = HDEcareAccount.Text
						CurrentAccountLabel.Text = HDCurrentADAccount.Text
						currLoginName.ForeColor = Color.Purple

					End If

					If HDCurrentADAccount.Text = "" Then

						currLoginName.Text = "This is a New Account Name."
						currLoginName.ForeColor = Color.Green

					End If

				ElseIf CurrentAccountLabel.Text <> sReturn Then
					If HDCurrentADAccount.Text = "" Then
						currLoginName.Text = "WARNING: Another User HAS THIS AD ACCOUNT: "

						currLoginName.ForeColor = Color.Red
					End If

				End If

				If HDADNotFounf.Text <> "" Then
					currLoginName.Text = "This is a New Account Name."
					currLoginName.ForeColor = Color.Green
				End If

			End While

        Catch ex As Exception
            Dim er As String = ex.ToString()

        End Try

        connSql.Close()
        Return sReturn
    End Function

    Protected Sub btnTestCustomUserName_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTestCustomUserName.Click
        lblCustomNameTest.Text = TestCustomUserName(UserNameTextBox.Text)
        btnValidate.Enabled = True
        lblCustomUserName.Text = "Name Tested"

        'If lblCustomNameTest.Text = "Account Is Available" Then

        ' cbUseCustomName.Enabled = True
        ' cbUseCustomName.Checked = False
        'Else

        'cbUseCustomName.Enabled = False
        'cbUseCustomName.Checked = False
        'End If

    End Sub


    Private Function TestCustomUserName(ByVal UserName As String) As String
        Dim sReturn As String = ""



		Dim cmdSql As New SqlCommand
        Dim connSql As New SqlConnection(Session("EmployeeConn"))
        Dim drSql As SqlClient.SqlDataReader



		'   connSql.ConnectionString = _DataConn

		cmdSql.CommandText = "SelTestCustomUserNameV3"
		cmdSql.CommandType = CommandType.StoredProcedure
        cmdSql.Connection = connSql
        cmdSql.Parameters.Add("@Login", SqlDbType.NVarChar).Value = UserName


        connSql.Open()

        If UserName.Length > 0 Then

            Try




                drSql = cmdSql.ExecuteReader


                While drSql.Read()


                    sReturn = IIf(IsDBNull(drSql("ReturnString")), "", drSql("ReturnString"))



                End While

            Catch ex As Exception
                Dim er As String = ex.ToString()

            End Try
        Else

            sReturn = "Must Provide Username to check."

        End If


        connSql.Close()
        Return sReturn
    End Function

    'Protected Sub CustomUserNameTextBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CustomUserNameTextBox.TextChanged
    '    lblCustomNameTest.Text = TestCustomUserName(CustomUserNameTextBox.Text)

    '    If lblCustomNameTest.Text = "Account Is Available" Then

    '        'cbUseCustomName.Enabled = True
    '        'cbUseCustomName.Checked = False
    '    Else

    '        'cbUseCustomName.Enabled = False
    '        'cbUseCustomName.Checked = False
    '    End If

    'End Sub
    Private Sub FillInvalidCodes()
        'SelInvalidCodes
        Dim dt As DataTable
        Dim thisData As New CommandsSqlAndOleDb
        thisData = New CommandsSqlAndOleDb("SelInvalidCodes", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@invalidType", "create", SqlDbType.NVarChar, 12)


        dt = thisData.GetSqlDataTable()
        Invalidddl.DataSource = dt
        Invalidddl.DataValueField = "Invalidcd"
        Invalidddl.DataTextField = "InvalidDesc"
        Invalidddl.DataBind()
        thisData = Nothing

    End Sub
    Protected Sub btnGoToRequest_Click(sender As Object, e As System.EventArgs) Handles btnGoToRequest.Click
        Response.Redirect("./AccountRequestInfo2.aspx?requestnum=" & AccountRequestNum, True)
    End Sub
    Private Sub sendAlertEmail()
        '547131
        Dim Request As New AccountRequest(AccountRequestNum, Session("EmployeeConn"))
        Dim RequestItems As New RequestItems(AccountRequestNum)
        Dim EmailBody As New EmailBody
        '    Dim sCss As String = File.ReadAllText(Server.MapPath("/Styles/Tables.css"))



        '======================
        ' Get current path
        '======================

        Dim path As String
        Dim directory As String = ""
        path = HttpContext.Current.Request.Url.AbsoluteUri
        Dim pathParts() As String = Split(path, "/")
        Dim i As Integer
        Do While i < pathParts.Length() - 1

            directory = directory + pathParts(i) + "/"
            i = i + 1

        Loop

        '======================

        Dim EmailList As New List(Of EmailDistribution)
        EmailList = New EmailDistributionList().EmailList

        Dim sndEmailTo = From r In Request.RequestItems()
                         Join e In EmailList
                         On r.AppBase Equals e.AppBase
                         Where e.EmailEvent = "AddAcct"
                         Select e, r

        For Each EmailAddress In sndEmailTo



            Dim toEmail As String = EmailAddress.e.Email





            Dim sSubject As String
            Dim sBody As String


            sSubject = "Account Validate Request for " & Request.FirstName & " " & Request.LastName & " has been added to the " & EmailAddress.r.ApplicationDesc.ToUpper() & " Queue. 2"


            sBody = "<!DOCTYPE html>" & _
                                "<html>" & _
                                "<head>" & _
                                "<style>" & _
                                "table" & _
                                "{" & _
                                "border-collapse:collapse;" & _
                                "font-family: 'Helvetica Neue', 'Lucida Grande', 'Segoe UI', Arial, Helvetica, Verdana, sans-serif;" & _
                                "}" & _
                                "table, th, td" & _
                                "{" & _
                                "border: 1px solid black;" & _
                                "padding: 3px" & _
                                "}" & _
                                "</style>" & _
                                "</head>" & _
                                "" & _
                                "<body>" & _
                                "A new account request has been added to the " & EmailAddress.r.ApplicationDesc & " Queue.  Follow the link below to go to Queue Screen." & _
                                 "<br />" & _
                                "<a href=""" & directory & "/CreateAccount.aspx?requestnum=" & EmailAddress.r.AccountRequestNum & "&account_seq_num=" & EmailAddress.r.AccountRequestSeqNum & """ ><u><b> " & EmailAddress.r.ApplicationDesc.ToUpper() & ": " & Request.FirstName & " " & Request.LastName & "</b></u></a>" & _
                                "</body>" & _
                            "</html>"






            Dim sToAddress As String = EmailAddress.e.Email

            If Session("environment").ToString.ToLower = "testing" Then
                sToAddress = "Jeff.Mele@crozer.org"
                sBody = sBody & sToAddress

            End If


            Dim mailObj As New MailMessage()
            mailObj.From = New MailAddress("CSC@crozer.org")
            mailObj.To.Add(New MailAddress(sToAddress))
            mailObj.IsBodyHtml = True
            mailObj.Subject = sSubject
            mailObj.Body = sBody


            Dim smtpClient As New SmtpClient()
            smtpClient.Host = "ah1smtprelay01.altahospitals.com"



            smtpClient.Send(mailObj)




        Next




    End Sub
End Class
