﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="false" CodeFile="ClientDemo.aspx.vb" Inherits="ClientDemo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <script type="text/javascript" src="Scripts/FieldValidator.js"></script>
<script type="text/javascript">
    function pageLoad(sender, args) {

        fieldValidator('textInputRequired', 'btnUpdate', 'lblValidation');

    }
   
  function maknull() {
    document.getElementById("text").value = "";
  }

</script>
<style type="text/css">
    .textHasFocus
    {
         border-color:Red;
        border-width:medium;
        border-style:outset;
    }
   .styleMan
   {
    border-color:Black;
        border-width:2px;
        border-style:Solid;
   }
   
    .style33
    {
        text-align: center;
        background-color: #FFFFCC;
        color: #336666;
        font-weight: bold;
        font-size: x-large;
        height: 41px;
    }
   
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:UpdatePanel ID="clientForm" runat="server" UpdateMode="Conditional" >
<%--        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="LocationOfCareIDddl" EventName = "SelectedIndexChanged" />
        
        </Triggers>--%>

 <ContentTemplate>
<%-- <div id="maindiv" runat="server" align="left">--%>

  <table width= "100%">
    <tr align="center" valign="middle">
     <td align="center" valign="middle">

     </td>
   </tr>
    <tr>
        <td align="center">
            <table>
                <tr align="center" >
                    <td align="right">
                        <asp:Label ID="lblusersubmit" runat="server" Text="User Submitting:" 
                        Width="125px" />
                    </td>

                    <td align="left">
                        <asp:Label ID="userSubmittingLabel" Width="150px" runat="server" Font-Bold="true"></asp:Label>
                    </td>

                    <td align="right">
                        <asp:Label ID="lblcurrRequestor" runat="server" Text="Current Auth.Mgr./Delegate:" Width="200px"></asp:Label>
                    </td>

                    <td align="left">
                        <asp:Label ID="SubmittingOnBehalfOfNameLabel" runat="server" Font-Bold="true"></asp:Label>
                    </td>
                </tr>
                <tr align="center" >
                    <td>
                    </td>
                    <td align="center" >
                       <asp:Button ID="btnPrintClient" runat="server" Text="Print Client" 
                       CssClass="btnhov" BackColor="#006666"  Width="140px" Height="20px" />
                    </td>
                    <td align="center">
                        <asp:Button ID="btnSelectOnBehalfOf" runat="server" Text="Change  Auth.Mgr./Delegate:" 
                            CssClass="btnhov" BackColor="#006666"  Width="240px" Height="20px" />

                    </td>
                    <td>
                    </td>
                </tr>
                <tr align="center">
                    <td align="center" colspan="4" >
                        <asp:Label ID = "lblValidation" runat = "server" ForeColor="Red" Font-Bold="true"/>
                    </td>
                </tr>


           </table>

        </td>
    </tr>
      <tr>
          <td align="center">
              <asp:Panel ID="pnlOnBehalfOf" runat="server"  Width="80%" Height="80%"  BackColor="#CCCCCC"  Visible="false" >
                <table align="center" border="1px" width="90%" bgcolor="#CCCCCC">
                    <tr valign="middle">
                        <td colspan="4"></td>
                    </tr>
                    <tr>
                        <td class="tableRowHeader" colspan="4">
                            Search for CKHS Individual who will be Responsable for these Account(s)<br />
                           Listed below are ONLY CKHS Employees
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Last Name
                        </td>
                        <td>
                            <asp:TextBox ID="SearchLastNameTextBox" runat="server"></asp:TextBox>
                        </td>
                        <td>
                            First Name
                        </td>
                        <td>
                            <asp:TextBox ID="SearchFirstNameTextBox" runat="server"></asp:TextBox>
                        </td>

                    </tr>
                    <tr>
                        <td colspan="4">
                            <div class="scrollContentContainer">
                                <asp:GridView ID="gvSearch" runat="server" AllowSorting="True" 
                                    AutoGenerateColumns="False" BackColor="White" BorderColor="#336666" 
                                    BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                                    DataKeyNames="client_num, login_name" EmptyDataText="No Items Found" 
                                    EnableTheming="False" Font-Size="Medium" GridLines="Horizontal" Width="100%">
                                    <FooterStyle BackColor="White" ForeColor="#333333" />
                                    <HeaderStyle BackColor="#336666" Font-Bold="False" Font-Size="Small" 
                                        ForeColor="White" HorizontalAlign="Left" />
                                    <PagerStyle BackColor="#336666" Font-Size="Medium" ForeColor="White" 
                                        HorizontalAlign="Left" />
                                    <RowStyle BackColor="White" ForeColor="#333333" />
                                    <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                    <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                    <SortedAscendingHeaderStyle BackColor="#487575" />
                                    <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                    <SortedDescendingHeaderStyle BackColor="#275353" />
                                    <Columns>
                                        <asp:CommandField ButtonType="Button" SelectText="Select" 
                                            ShowSelectButton="true" />
                                        <asp:BoundField DataField="fullname" HeaderStyle-HorizontalAlign="Left" 
                                            HeaderText="Full Name" ItemStyle-HorizontalAlign="Left" />
                                        <asp:BoundField DataField="phone" HeaderStyle-HorizontalAlign="Left" 
                                            HeaderText="Phone" ItemStyle-HorizontalAlign="Left" />
                                        <asp:BoundField DataField="department_name" HeaderStyle-HorizontalAlign="Left" 
                                            HeaderText="Department" ItemStyle-HorizontalAlign="Left" />
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <table>
                                <tr>
                                    <td>
                                        <asp:Button ID="btnSearch" runat="server" Font-Bold="True" Font-Names="Arial" 
                                            Font-Size="Small" Height="20px" Text="Search" Width="125px" />
                                    </td>
                                    <td>
                                        <asp:Button ID="btnCloseOnBelafOf" runat="server" Font-Bold="True" 
                                            Font-Names="Arial" Font-Size="Small" Height="20px" Text="Close" Width="125px" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                </table>
              </asp:Panel>
          </td>

      </tr>
     <tr>
        <td align="left">
             <ajaxToolkit:TabContainer ID="tabs" runat="server" ActiveTabIndex="0"
                Width="100%"  EnableTheming="true" 
                BackColor="#efefef">

                  <ajaxToolkit:TabPanel ID="tpDemographics" runat="server"  BackColor="Gainsboro" Visible = "True" EnableTheming="true"  TabIndex="0">
                     <HeaderTemplate > 
                            <asp:Label ID="Label1" runat="server" Text="Demographics" Font-Bold="True" 
                                ForeColor="Black"></asp:Label>
                     </HeaderTemplate>
                     <ContentTemplate>
                        <table border="0" style="width: 100%">

                        <tr>
                                    <td class="style33">
                                    Client Account(s) Information
                                    </td>
                        </tr>
                        <tr>
                                    <td class="tableRowHeader">
                                        <asp:Label ID="ClientnameHeader" runat="server" ></asp:Label>
                             </td>
                        </tr>
                        <tr>
          
                            <td class="tableRowSubHeader"  align="left">
                            Form Submission
                            </td>
            
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Label ID="TerminateLabel" runat="server" 
                                 Text="This Account currently has an Open Terminate Request"  Visible="False" 
                                    Font-Bold="True" ForeColor="#FF3300" />
                            </td>
                        <tr>
                            <td align="center">
                                <asp:Button ID="btnChangeType" runat="server" Text="Change Affiliation" Visible="False"
                                CssClass="btnhov" BackColor="#006666"  Width="175px" />

                                <asp:Button runat="server" ID = "btnNewTicketRequest" Text ="New Ticket/RFS"  Visible="False"
                                    CssClass="btnhov" BackColor="#006666"  Width="175px" />


                                <asp:Button ID = "btnUpdate" runat = "server" Text ="Update Demographics "
                                      CssClass="btnhov" BackColor="#006666"  Width="175px" />

              
                                <asp:Button ID = "btnSubmitDemoChange" runat = "server" Text ="Change Demographics " Visible="False"
                                      CssClass="btnhov" BackColor="#006666"  Width="175px" />


                                <asp:Button ID="btnNonPrivileged" runat="server" Text="No Longer Privileged"  Visible="False"
                                      CssClass="btnhov" BackColor="#006666"  Width="175px" />


                                <asp:Button ID="btnDeactivateClient" runat="server" Text = "De-Activate Client"  Visible="False" 
                                ToolTip="Client has no Accounts and Does not have a Crozer Emp#" 
                                      CssClass="btnhov" BackColor="#006666"  Width="175px" />


                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="tableRowSubHeader"  align="left">
                            CKHS Affiliation:
                            </td>
                        </tr>
                        <tr>
                            <td  align="center">
                                <asp:RadioButtonList ID ="emptypecdrbl" runat = "server" 
                                    RepeatDirection="Horizontal" AutoPostBack="True" Font-Size="Smaller" >
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <asp:Panel runat="server" ID="pnlProvider" Visible="False">
                            <tr>
                            <td colspan="4"  class="tableRowSubHeader" align="left">
                                Provider Information
                            </td>
                            </tr>
                            <tr>
                            <td colspan="4">
                                <table id="tblprovider"  border="1" width="100%">
                                            

                                <tr>
                                    <td align="right" >
                                        <asp:Label ID="priviledlbl" runat="server" Text=" Privileged Date:" 
                                        ToolTip="Only Credentialing Office will enter this data, Indicates the date the Provider was credentialied on."  Width="150px" />

                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID = "credentialedDateTextBox" runat="server" ToolTip="Only Credentialing Office will enter this data"
                                            Enabled="False"></asp:TextBox>
                                    </td>
<%--                                    <td align="right">
                                      DCMH Privileged Date:
                                  </td>
                                  <td align="left">
                                      <asp:TextBox ID="CredentialedDateDCMHTextBox" runat="server" 
                                           Enabled="False"></asp:TextBox>
                                  </td>--%>

                                    <td align="right">
                                         <asp:Label ID="ccmcadmitlbl" runat="server" Text ="Privileged Rights:" Width="160px" />                                   
                                    </td>
                                    <td align="left" >
                                           <asp:RadioButtonList ID = "CCMCadmitRightsrbl" runat="server" 
                                           AutoPostBack="True" RepeatDirection="Horizontal">
                                            <asp:ListItem>Yes</asp:ListItem>
                                            <asp:listItem>No</asp:listItem>
                                        </asp:RadioButtonList>

                                    </td>
<%--                                    <td align="right">
                                        <asp:Label ID="dcmhlbl" runat="server" Text ="DCMH Privileged Rights:" Width="150px" />
                                     </td>

                                    <td align="left" >
                                           <asp:RadioButtonList ID = "DCMHadmitRightsrbl" runat="server" AutoPostBack="True"
                                           RepeatDirection="Horizontal">
                                            <asp:ListItem>Yes</asp:ListItem>
                                            <asp:listItem>No</asp:listItem>
                                        </asp:RadioButtonList>

                                    </td>--%>

                                </tr>

                                <tr>
                                    <td align="right" >
                                        CHMG:
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID = "hanrbl" runat="server" RepeatDirection="Horizontal" 
                                            AutoPostBack="True">
                                            <asp:ListItem>Yes</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>            
                                        </asp:RadioButtonList>
                                    </td>

                                    <td align="right">
                                            Doctor Number:
                                    </td>
                                    <td align="left" >
                                        <asp:TextBox ID = "DoctorMasterNumTextBox" runat="server" Enabled="False" 
                                            MaxLength="6"></asp:TextBox>
                                    </td>

                                </tr>
                                <tr>
 
                                    <td colspan="4">
                                     <asp:Panel Visible="False" runat="server" ID="docMstrPnl">
                                         <table id="docmst" width="100%">
<%--                                            <tr>
                                                <td colspan="2" align="center">
                                                    <asp:Button ID="btnAddDuplicateDcoMaster" runat="server" Text="Submit Dup. Doctor Master#" 
                                                    CssClass="btnhov" BackColor="#006666"  Width="250px" />


                                                </td>
                                            </tr>

                                            <tr>
                                                <td align="center">
                                                    <asp:Label ID="AddDupmstlbl" runat="server" Text="Add Another Doctor Master Number:"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="DupDoctorMstrtxt" runat="server"></asp:TextBox>
                                                </td>

                                            </tr>--%>
                                            <tr align="center">
                                                <td  align="center">
                                                  <div>
                                                    <asp:GridView ID="gvDocmster" runat="server" AllowSorting="True" 
                                                            AutoGenerateColumns="False" BackColor="White" BorderColor="#336666"
                                                            BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                                                            EmptyDataText="No Docmaster # Found" EnableTheming="False" 
                                                            DataKeyNames="clientnum,DoctormasterNum"
                                                            Font-Size="Small" GridLines="Horizontal" Width="40%" >
                                   
                                                            <FooterStyle BackColor="White" ForeColor="#333333" />
                                                            <HeaderStyle BackColor="#336666" Font-Bold="False" ForeColor="White" Font-Size="Small" HorizontalAlign="Left"    />                                    
                                                            <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Left" Font-Size="Medium"/>
                                                            <RowStyle BackColor="White" ForeColor="#333333" />


                                                            <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                                            <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                                            <SortedAscendingHeaderStyle BackColor="#487575" />
                                                            <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                                            <SortedDescendingHeaderStyle BackColor="#275353" />

                                                            <Columns>
                                                                <asp:CommandField ButtonType="Button" SelectText="Remove" 
                                                                      ShowSelectButton="True">
                                                                    <ControlStyle BackColor="#84A3A3" />
                                                                      <HeaderStyle HorizontalAlign="Left" />
                                                                      <ItemStyle Height="20px" />
                                                                      </asp:CommandField>
                                                               <asp:BoundField  DataField="DoctormasterNum"  HeaderText="Doctor Master#">
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                <ItemStyle Height="35px" />
                                                                </asp:BoundField>
				                                                                  
                                  
                                                           </Columns>
                                                         </asp:GridView>
                                                     </div>
                                                
                                                </td>
                                            
                                            </tr>
                                         </table>
                                     
                                     </asp:Panel>
                                 </td>
                                            
                                </tr>
                                <tr>
                                    <td align="right">
                                        NPI:
                                    </td>
                                    <td  align="left" >
                                        <asp:TextBox ID = "npitextbox" runat = "server" MaxLength="10"></asp:TextBox>
                                    </td>
                                    <td align="right" >
                                        License No.
                                    </td>
                                    <td align="left" >
                                            <asp:TextBox ID = "LicensesNoTextBox" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                      Taxonomy:
                                    </td>
                                    <td align="left"  >
                                       <asp:TextBox ID = "taxonomytextbox" runat="server"></asp:TextBox>
                                    </td>
                                   <td align="right">
                                 <%-- Group Number:--%>
                              </td>

                              <td align="left">
                                  <%--<asp:TextBox ID="groupnumTextBox" runat="server" Enabled="false" ></asp:TextBox>--%>
                              </td>
                             </tr>

                            <tr>
                               <td align="right">
                                    DEA ID:
                               </td>
                               <td align="left">
                                  <asp:TextBox ID="DEAIDTextBox" runat="server" Width="250px"></asp:TextBox>
                               </td>

                               <td align="right">
                                   <%-- DEA Extension:--%>
                               </td>
                               <td align="left">
                                  <%--<asp:TextBox ID="DEAExtensionIDTextBox" runat="server" Width="250px"></asp:TextBox>--%>
                               </td>

                            </tr>


                                    <tr>
                                  <td colspan="4">
                                   <table border="3px" width="100%">

                                        <tr>
                                        <td colspan="4"  class="tableRowSubHeader" align="center">
                                            Physician Groups 
                                        </td>
                                        </tr>

 <%--                                       <tr>
                                            <td align="right" style="width: 220px">
                                                CKHN Location of Care Email:
                                            </td>
                                            <td align="left" colspan="3">
                                                <asp:Label ID="DirectEmailLabel" runat="server" ></asp:Label>
                                            </td>

                                        </tr>--%>

                                        <tr>
                                            <td colspan="4" align="center">
                                               <asp:Panel Visible="False" runat="server" ID="CPMCKNLoc">
                                                <div>
                                                
                                                 <table>
                                                    <tr>
                                                    
                                                        <td align="right">
                                                           <asp:Label ID ="Label7" runat="server" Text="CHMG:" 
                                                                Visible="False" ></asp:Label>
                                       
                                                         </td>
                                                         <td colspan="3" align="left">
                                                            <asp:DropDownList ID = "CPMLocationddl" runat="server" AutoPostBack="True" 
                                                                 Width="90%">
                                                            </asp:DropDownList>
                                        
                                                         </td>
                                                    
                                                    
                                                    </tr>
                                                    <tr>
                                                        <td colspan="4">
                                                          <div>
                                                            <asp:GridView ID="gvCPMList" runat="server" AllowSorting="True" 
                                                                    AutoGenerateColumns="False" BackColor="White" BorderColor="#336666"
                                                                    BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                                                                    EmptyDataText="No Docmaster # Found" EnableTheming="False" 
                                                                    DataKeyNames="Group_num"
                                                                    Font-Size="Small" GridLines="Horizontal" Width="100%" >
                                   
                                                                   <FooterStyle BackColor="White" ForeColor="#333333" />
                                                                    <HeaderStyle BackColor="#336666" Font-Bold="False" ForeColor="White" Font-Size="Small" HorizontalAlign="Left"    />                                    
                                                                    <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Left" Font-Size="Medium"/>
                                                                    <RowStyle BackColor="White" ForeColor="#333333" />
                                                                    <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                                                    <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                                                    <SortedAscendingHeaderStyle BackColor="#487575" />
                                                                    <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                                                    <SortedDescendingHeaderStyle BackColor="#275353" />
                                                                    <Columns>
                                                                        <asp:CommandField ButtonType="Button" SelectText="Remove Group" 
                                                                            ShowSelectButton= "True" >
                                                                        <ControlStyle BackColor="#84A3A3" />
                                                                        <ItemStyle Height="30px" />
                                                                        </asp:CommandField>
                                                                        <asp:BoundField  DataField="Group_name" SortExpression="Group_name" 
                                                                            HeaderText="Group Name" >
                                                                        <HeaderStyle HorizontalAlign="Left" />
                                                                        <ItemStyle Height="35px" HorizontalAlign="Left" />
                                                                        </asp:BoundField>

                
                                                                    </Columns>

                                                                 </asp:GridView>
                                                             </div>

                                                        </td>
                                                    
                                                    </tr>
                                                 </table>
                                               
                                                </div>
                                                </asp:Panel>
                                            </td>
                                        </tr>
<%--                                        <tr>
                                            <td  colspan="4" align="center">
                                              <asp:Panel ID="ViewCPMPan" runat="server" Visible="False">
                                                <div>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Button ID="btnviewCPMLoc" runat="server" text="View/Add CPM Loc." 
                                                                CssClass="btnhov" BackColor="#006666"  Width="170px" />
                                                                

                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>

                                              
                                              </asp:Panel>
                                            </td>
                                        </tr>
--%>                                        <tr>
                                             <td align="right">
                                               <asp:Label ID ="ckhnlbl" runat="server" Text="CHMG:" 
                                                     Visible="False" ></asp:Label>
                                       
                                             </td>
                                             <td colspan="3" align="left">
                                                <asp:DropDownList ID = "LocationOfCareIDddl" runat="server"  Visible="False" 
                                                     AutoPostBack="True" Width="90%">
                                                </asp:DropDownList>
                                                <asp:Label ID="CPMViewGrouplbl" runat="server" Visible="False" Width="90%"></asp:Label>
                                             </td>
                                
                                
                                        </tr>

<%--                                        <tr>

                                                <td align="right">
                                                    Comm. Location Of Care:
                                                </td>
                                                <td colspan="3" align="left">
                                                    <asp:DropDownList ID = "CommLocationOfCareIDddl" runat="server" 
                                                        AutoPostBack="True" Width="90%">
                                            
                                                    </asp:DropDownList>
                                                
                                                </td>
                                
                                        </tr>--%>

                                        <tr>
                                            <td align="right" >
                                               Provider Group Names:
                                            </td>       
                                            <td colspan="3" align="left">
                                                <asp:DropDownList ID ="group_nameddl"  Width="90%" AutoPostBack="True" 
                                                    runat="server">
            
                                                </asp:DropDownList>
                                             </td>
                                        </tr>



<%--                                        <tr>
                                            <td  align="right">
                                                Coverage Group ID:
                                            </td>
                                            <td  colspan="3" align="left">
                                                    <asp:DropDownList ID = "CoverageGroupddl" runat="server" Width="90%">
                                            
                                                    </asp:DropDownList>

                                            </td>
                                        </tr>

                                        <tr>
                                
                                            <td  align="right">
                                                Neighbor Phys Groups:
                                            </td>
                                            <td  colspan="3" align="left">
                                                    <asp:DropDownList ID = "Nonperferdddl" runat="server" AutoPostBack="True" 
                                                        Width="90%">
                                            
                                                    </asp:DropDownList>
                                            </td>
                                
                                        </tr>

                                        <tr>

                                             <td align="right">
                                                  CKHN Copy To MedAssist.:
                                             </td>
                                             <td>
                                                <asp:Label ID = "CopyToLabel" runat="server" Width="245px"></asp:Label>
                                             </td>

                                             <td align="right">
                                                    Comm. Copy To MedAssist.:
                                             </td>
                                             <td>
                                                    <asp:Label ID = "CommCopyToLabel" runat="server" Width="245px"></asp:Label>
                                             </td>
                                        </tr>--%>

                                    </table>
                                </td>
                                </tr>


                             <tr>
                                <td align="right">
                                Title:
                                </td>
                                <td align="left" >
                                    <asp:dropdownlist ID = "Titleddl" runat="server" AutoPostBack="True"></asp:dropdownlist>
                                     <asp:Label ID="Title2Label" runat="server" Width="250px" Visible="False"></asp:Label>

                                </td>
                                <td  align="right" >
                                Specialty:
                                </td>
                                <td align="left" >
                                    <asp:dropdownlist ID = "Specialtyddl" runat="server" AutoPostBack="True" ></asp:dropdownlist>
                                     <asp:Label ID = "SpecialtyLabel" runat="server" Visible ="False"></asp:Label>

                                </td>
                            </tr>
<%--                            <tr>
                                <td align="right">
                                    CCMC Consults:
                                </td>
                                <td align="left" >
                                       <asp:RadioButtonList ID = "CCMCConsultsrbl" runat="server" RepeatDirection="Horizontal">
                                        <asp:ListItem>Yes</asp:ListItem>
                                        <asp:listItem>No</asp:listItem>
                                    </asp:RadioButtonList>

                                </td>

                                <td align="right">
                                    Springfield Consults:
                                </td>
                                <td align="left" >
                                       <asp:RadioButtonList ID = "SpringfieldConsultsrbl" runat="server" RepeatDirection="Horizontal">
                                        <asp:ListItem>Yes</asp:ListItem>
                                        <asp:listItem>No</asp:listItem>
                                    </asp:RadioButtonList>

                               </td>

                            </tr>
                            <tr>
                                <td align="right">
                                    Taylor Consults:
                                </td>
                                <td align="left" >
                                       <asp:RadioButtonList ID = "TaylorConsultsrbl" runat="server" RepeatDirection="Horizontal">
                                        <asp:ListItem>Yes</asp:ListItem>
                                        <asp:listItem>No</asp:listItem>
                                    </asp:RadioButtonList>

                                </td>
                                <td align="right">
                                    DCMH Consults:
                                </td>
                                <td align="left" >
                                       <asp:RadioButtonList ID = "DCMHConsultsrbl" runat="server" RepeatDirection="Horizontal">
                                        <asp:ListItem>Yes</asp:ListItem>
                                        <asp:listItem>No</asp:listItem>
                                    </asp:RadioButtonList>

                               </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    Write Orders for C.T.S. Bedded Patients:
                                </td>
                                    <td align="left" >
                                        <asp:RadioButtonList ID = "Writeordersrbl" runat="server" RepeatDirection="Horizontal">
                                        <asp:ListItem>Yes</asp:ListItem>
                                        <asp:listItem>No</asp:listItem>
                                    </asp:RadioButtonList>

                                </td>

                                <td align="right">
                                    Write Orders for DCMH Bedded Patients:
                                </td>
                                    <td align="left" >
                                        <asp:RadioButtonList ID = "WriteordersDCMHrbl" runat="server" RepeatDirection="Horizontal">
                                        <asp:ListItem>Yes</asp:ListItem>
                                        <asp:listItem>No</asp:listItem>
                                    </asp:RadioButtonList>

                                </td>
                            </tr>--%>

<%--                            <tr>
                               <td align="right">
                                    Medicaid ID:
                               </td>
                               <td align="left">
                                  <asp:TextBox ID="MedicareIDTextBox" runat="server" Width="250px"></asp:TextBox>
                               </td>

                               <td align="right">
                                    BlueCross ID:
                               </td>
                               <td align="left">
                                  <asp:TextBox ID="BlueCrossIDTextBox" runat="server" Width="250px"></asp:TextBox>
                               </td>

                            </tr>--%>



<%--                            <tr>
                               <td align="right">
                                    Medic Group ID:
                               </td>
                               <td align="left">
                                  <asp:TextBox ID="MedicGroupIDTextBox" runat="server" Width="250px"></asp:TextBox>
                               </td>

                               <td align="right">
                                    Medic Group Phys ID:
                               </td>
                               <td align="left">
                                  <asp:TextBox ID="MedicGroupPhysIDTextBox" runat="server" Width="250px"></asp:TextBox>
                               </td>

                            </tr>
                            <tr>
                                <td align="right">
                                    Uniform Physician ID:
                                </td>
                                 <td align="left">
                                 <asp:TextBox ID="UniformPhysIDTextBox" runat="server" Width="250px"></asp:TextBox>
                               </td>
                                <td align="right">  
                                    Sure Script ID:
                               </td>
                               <td align="left">
                                  <asp:TextBox ID="SureScriptIDTextBox" runat="server" Width="250px"></asp:TextBox>
                               </td>

                            </tr>--%>


                        </table>
                            </td>
                            </tr>
                        </asp:Panel> 
                        <tr>
                            <td>
                                <asp:Panel ID="pnlTCl" runat="server" Visible="False">
                                 <tr>
                                   <td>
                                    <table  id="tbltcl" border="3px" width="100%">
                                      <tr>
                                            <td align="left" class="tableRowSubHeader" colspan="4">
                                                TCL / User Type
                                            </td>
                                        </tr>
                                      <tr>
                                        <td align="right">
                                                <asp:Label ID="LblTCL" runat="server" Text="TCL/ Invision User Type:" />
                                        </td>
                                        <td colspan="3">
                
                                                <asp:TextBox ID="TCLTextBox" runat="server" Visible="False"  Enabled="False" 
                                                    Width="110px"></asp:TextBox>
                                                <asp:TextBox ID="UserTypeCdTextBox" runat="server" Visible="False" 
                                                    Enabled="False" Width="110px"></asp:TextBox>

                                                <asp:TextBox ID="UserTypeDescTextBox" runat="server" Visible="False" 
                                                    Enabled="False" Width="300px"></asp:TextBox>
        
                                        </td>
                                     </tr>
                                     <tr>
                                        <td align="center" colspan="4">
                                            <asp:Button ID="btnChangeTCL" runat="server" Text="Change TCL" Visible="False"
                                                CssClass="btnhov" BackColor="#006666"  Width="150px"  />
                                        </td>
                                     </tr>
                                      <tr>
                                        <td align="center" colspan="4">


                                            <div>
                                            <asp:GridView ID="gvTCL" runat="server"
                                                    AutoGenerateColumns="False" BackColor="White" BorderColor="#336666"
                                                    BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                                                    EmptyDataText="No Docmaster # Found" EnableTheming="False" 
                                                    DataKeyNames="TCL,UserTypeCd,UserTypeDesc" Visible="False"
                                                    Font-Size="Small" GridLines="Horizontal" Width="100%">
                                   
                                                    <FooterStyle BackColor="White" ForeColor="#333333" />
                                                    <HeaderStyle BackColor="#336666" Font-Bold="False" ForeColor="White" Font-Size="Small" HorizontalAlign="Left"    />                                    
                                                    <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Left" Font-Size="Medium"/>
                                                    <RowStyle BackColor="White" ForeColor="#333333" />


                                                    <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                                    <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                                    <SortedAscendingHeaderStyle BackColor="#487575" />
                                                    <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                                    <SortedDescendingHeaderStyle BackColor="#275353" />

                                                    <Columns>
                                                            <asp:CommandField ButtonType="Button" ShowSelectButton= "True" >
                                                            <ControlStyle BackColor="#84A3A3" />
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <ItemStyle Height="30px" />
                                                            </asp:CommandField>
                                                            <asp:BoundField  DataField="TCL"  HeaderText="TCL">
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <ItemStyle Height="35px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField  DataField="UserTypeCd"  HeaderText="User Type">
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <ItemStyle Height="35px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField  DataField="UserTypeDesc"  HeaderText="User Type Desc.">
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <ItemStyle Height="35px" />
                                                            </asp:BoundField>
				                                                                  
                                  
                                                    </Columns>
                                                    </asp:GridView>
                                                </div>


                                        </td>

                      
                                    </tr>
                                    <tr>
                                           <td>
                                                <asp:Label ID="EmpDoclbl" runat="server" Text="This Account Doc Master#" 
                                                    Visible="False"></asp:Label>
                                            </td>
                                           <td align="left" colspan="3">
                                                <asp:TextBox ID = "EmpDoctorTextBox" runat="server" MaxLength="6" 
                                                    Visible="False" ></asp:TextBox>
                                          </td>
                                        </tr>


                                    </table>             
                                  </td>
                                 </tr>
                               </asp:Panel>                            
                            
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Panel ID="pnlCererUserPositions" runat="server" Visible="true">
                                    <table border="3" width="100%">
                                        <tr>
                                            <td align="left" class="tableRowSubHeader" colspan="2">
                                                Cerner User Positions

                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <asp:Label ID="lblCerberpos" runat="server" Text ="Cerner Position" />
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="CernerPositionDescTextBox" runat="server" Width="300px" Enabled="false"></asp:TextBox>
                                                <asp:TextBox ID="UserCategoryCdTextBox" runat="server" Visible="false"></asp:TextBox>
                                            </td>
                                        </tr>


                                  <tr>
                                      <td align="right">
                                           Cerner Position:

                                      </td>
                                    <td align="left"  >
                                               <asp:DropDownList ID = "CernerPosddl" runat="server" Visible="true" Width="80%" AutoPostBack="true"> 
                                               </asp:DropDownList>
                                        </td>
                    
                                </tr>

                                </table>  
                              </asp:Panel>
                            </td>

                        </tr>
                        <tr>
                        <td>
                          <table id="tbldemo" border="1px" width="100%">
                            <tr>
                            <td colspan="4" class="tableRowSubHeader"  align="left">
                                Demographics

<%--                                  <asp:Button ID="btnsearcldap" runat="server" Text="Update AD/eMail Phone" Visible="false"
                                    CssClass="btnhov" BackColor="#CCCCCC"  Width="180px" ForeColor="Black" Height="35px" />

								 <asp:Button ID="btnUpdldapdemo" runat="server" Text="Update AD Emp. Numb." Visible="false" 
									     CssClass="btnhov" BackColor="#CCCCCC"  Width="180px" ForeColor="Black" Height="35px"/>

								  <asp:Label ID="LDAPlbl" runat="server" Text ="" Visible="false" ForeColor="Red" Font-Bold="true">
								  </asp:Label>

                              <asp:Label id="LdapPhonelbl" runat="server" Visible="false" ForeColor="Yellow" Font-Bold="true">
                              </asp:Label>

	                              <asp:Label id="lblldapInfo" runat="server" Visible="false" ForeColor="Yellow" Font-Bold="true">
                              </asp:Label>--%>

                            </td>
                            </tr>
                            <tr>

                            <td colspan="4" >
                                <table class="style3" > 
                                <tr>
                                    <td>
                                        First Name:
                                    </td>
                                    <td align="left"  >
                                        <asp:TextBox width="280px" ID = "firstnameTextBox" runat="server" ></asp:TextBox>
                                    </td>
                                    <td>
                                        MI:
                                    </td>
                                    <td>
                                        <asp:TextBox ID = "miTextBox" runat="server" Width="15px"></asp:TextBox>
                                    </td>
                                    <td align="left" >
                                        Last Name:
                                    </td>

                                    <td align="left" >
                                        <asp:TextBox ID = "lastnameTextBox" width="280px" runat="server"></asp:TextBox>
                                    </td>
                                    <td>
                                        Suffix: 
                                    </td>
                                    <td>
                                        <asp:dropdownlist id="suffixddl" runat="server" Height="20px" Width="52px">
                                            </asp:dropdownlist>
                                    </td>
                                </tr>
                                </table>
                                </td>
                            </tr>

                            <tr>
                                <td align="right">
                                    <asp:Label ID="Emplbl" runat="server" Text="CKHS Employee #" ></asp:Label>
                                </td>

                                <td>
                                    <asp:TextBox ID ="siemensempnumTextBox" runat="server" Enabled="False"></asp:TextBox>
                                </td>
                                <td align="right">
                                    <asp:Label ID="siemdesclbl" runat="server" Text="PMH Position:"></asp:Label>
                                </td>
                                <td>
                                    <%--<asp:Label id="userpositiondescLabel" runat="server"></asp:Label>--%>
                                 
                                    <asp:Label id="PMHTitlePositionLabel" runat="server"></asp:Label>


                                </td>
                            </tr>
							  <tr>
								  <td align="right">
                                    <asp:Label ID="PMHemplbl" runat="server" Text="PMH Employee #" ></asp:Label>

								  </td>
								  <td>
                                    <asp:Label ID="PMHEmployeeIDLabel" runat="server" Enabled="False" ></asp:Label>

								  </td>

								  <td align="right">
                                    <asp:Label ID="PMHDeptlbl" runat="server" Text="PMH Department:" ></asp:Label>

								  </td>
								  <td>
                                    <asp:Label ID="PMHDepartmentLabel" runat="server" Enabled="False"></asp:Label>

								  </td>


							  </tr>
                            <tr>
                                <td align="right">
                                    <asp:Label ID="netlbl" runat="server" Text="Network Logon:" ></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID = "loginnameTextBox" runat="server"></asp:TextBox>
                                </td>
								<td align="right">
                                    <asp:Label id="ckhsdeptlbl" runat="server" Text="CKHS Dept:" ></asp:Label>
                                </td>
                                <td>
                                    <asp:Label id="DepartmentNameLabel" runat="server" ></asp:Label>

                                </td>
                            </tr>
                              <tr>
                                  <td align="right">
                                      <asp:label ID="lblpmhEmpCat" runat="server" Text="Emp. Category:" />
                                  </td>
                                  <td>
                                      <asp:Label ID="EmploymentCategoryNameLabel" runat="server" />
                                  </td>
                                  <td align="right">

                                  </td>
                                  <td>

                                  </td>
                              </tr>
                            <tr>
								<td align="right" width="20%" >
									<asp:Label ID="lblpos" runat="server" Text="Position Description:" />
								</td>
								<td align="left" >
                                    <asp:DropDownList ID="Rolesddl" runat="server"  AutoPostBack="True"  >
                                    
                                    </asp:DropDownList>


                                    <asp:TextBox ID="PositionRoleNumTextBox"  runat="server" Width="50px" 
                                        Visible="False"></asp:TextBox>
                                    <asp:TextBox ID="PositionNumTextBox"  runat="server" Width="50px" 
                                        Visible="False"></asp:TextBox>
                                    <asp:TextBox ID="userpositiondescTextBox" runat="server" Width = "500px" 
                                        Visible="False"></asp:TextBox>

	                            </td>

                                <td>
                                    <asp:Label id="behaviorallbl" runat="server" Text="BH Master#" Visible="False"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label id="DoctorMasterNumLabel" runat="server" Visible="False"></asp:Label>

                                </td>
                            </tr>
                            <tr>
                            <td align="right" >
                            Entity:
                            </td>
                            <td align="left">
                            <asp:DropDownList ID ="entitycdDDL" runat="server" AutoPostBack ="True" >
            
                            </asp:DropDownList>
                            </td>
       
                            <td align="right" >
                                <asp:Label ID="lbldept" runat="server" Text = "Department Name:" />
                            </td>
                            <td align="left"  >
                            <asp:DropDownList ID ="departmentcdddl" runat="server" Width="250px" AutoPostBack="True">
            
                            </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td  align="right">
                                Address 1:
                                </td>
                            <td align="left"> 
                                    <asp:TextBox ID = "address1textbox" runat="server" Width="250px"></asp:TextBox>
                                </td>
                            <td align="right" >
                                Address 2:
                                </td>
                            <td align="left" >
                                <asp:TextBox ID = "address2textbox" runat="server" Width="250px"></asp:TextBox>
                                </td>

                        </tr>
                            <tr>
                                <td align="right">
                                    <asp:Label id="lblcity" runat="server" width="50px"  text="City:" />
                                </td>
                                <td align="left" colspan="3">
                                    <asp:TextBox ID = "citytextbox" runat="server" width="500px"></asp:TextBox>
                                </td>


                            </tr>

                            <tr>

                                <td align="right">
                                    <asp:Label ID="lblstate" runat="server" Text="State:" Width="50px" />
                                </td>
                                <td align="left">
                                    <asp:TextBox ID = "statetextbox" runat="server" Width="50px"></asp:TextBox>
                                    </td>
                                <td align="right" >

                                    <asp:Label ID="lblzip" runat="server" Text="Zip:" Width="25px" />
                                </td>
                                <td align="left">
                                    <asp:TextBox ID = "ziptextbox" runat="server" Width="50px"></asp:TextBox>
                                </td>
                        </tr>
                        <tr>
                            <td align="right"  >
                            Phone Number:
                            </td>
                            <td align="left" >
                            <asp:TextBox ID = "phoneTextBox" runat="server" MaxLength="12"></asp:TextBox>


                            </td>
       
                            <td align="right" >
                            Pager Number:
                            </td>
                            <td align="left" >
                            <asp:TextBox ID = "pagerTextBox" runat="server" MaxLength="12"></asp:TextBox>
                            </td>
                        </tr>

                        <tr>
                            <td align="right" >
                            Fax:
                            </td>
                            <td align="left" >
            
                                <asp:TextBox ID = "faxtextbox" runat="server" MaxLength="12"></asp:TextBox>
                                </td>
         
                                <td align="right" >
                            Email:
                            </td>
                            <td align="left" >
                            <asp:TextBox ID = "emailTextBox" runat="server" Width="250px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" >
                            Remote Access:
                            </td>
                            <td align="left" colspan="3">
                                <asp:TextBox id = "AlternatePhoneTextBox" runat="server" Width="250px" 
                                    Enabled="False"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" >
                            Location:
                            </td>
                            <td align="left" >
         
                                <asp:DropDownList ID ="facilitycdddl" runat="server" AutoPostBack="True">
              
                            </asp:DropDownList>
          
                            </td>
        
                            <td align="right" >
                            Building:
                            </td>
                            <td align="left" >

                                <asp:DropDownList ID ="buildingcdddl" runat="server" AutoPostBack="True">
            
                            </asp:DropDownList>
          
                            </td>
                        </tr>
                        <tr>
                            <td align="right" >
                                Floor:
                            </td>

                            <td align="left">
                                <asp:DropDownList ID="Floorddl" runat="server" AutoPostBack="True">
            
                                </asp:DropDownList>
                            
                            </td>
                            <td align="right" >
                                 Office:
                            </td>
                               
                            <td align="left" > 
                                <asp:TextBox ID="OfficeTextbox" runat="server"></asp:TextBox>
                            </td>

                        </tr>
                        <tr>
                             <asp:Panel ID="PhysOfficepanel" runat="server" Visible="False">
                              <div>
                                        <td align="right" style="width: 220px">
                                            <asp:Label ID ="Label5" runat="server" Text="Physician Office:"  ></asp:Label>

                                        </td>
                                        <td colspan="3" align="left">
                                            <asp:DropDownList ID = "PhysOfficeddl" runat="server"  Width="90%">
    
                                            </asp:DropDownList>
                                        </td>
                                </div>
                              </asp:Panel>
                        </tr>        

<%--                        <tr>
                            <td colspan="4" align="center">
                                <div>
                                    <asp:Panel ID="cpmModelAfterPanel" runat="server" Visible="False">

                                        <table>
                                            <tr>
                                                <td>
                                                        <asp:Label id="currcpmmodlbl" runat="server" Text="Current CPM Model:"></asp:Label>
                                                </td>
                                                <td>
                                                                <asp:TextBox ID="ViewCPMModelTextBox" runat="server"  Enabled="False" 
                                                                    Width="500px"></asp:TextBox>

                                                                <asp:TextBox ID="CPMModelTextBox" runat="server"   Visible="False" 
                                                                    Width="500px"></asp:TextBox>
                                                                <asp:TextBox ID="provmodelTextBox" runat="server"   Visible="False" ></asp:TextBox>

                                                </td>
                                            </tr>

                                                  
                                        </table>                                                
                                                
                                    </asp:Panel>
                                </div>
                            </td>
                        </tr>
--%>
<%--                        <tr>
                            <td colspan="4" align="center">
                                <div>
                                <asp:Panel ID="cpmModelEmplPanel" runat="server" Visible="False">
                                                
                                    <table>
                                        <tr>
                                            <td>
                                                    <asp:Label id="cpmempModellbl" runat="server" Text="Select emplyee for CPM Model:"></asp:Label>

                                            </td>
                                            <td>
                                                <asp:DropDownList ID="CPMEmployeesddl" runat="server" Width="500px" 
                                                    AutoPostBack="false">
                                
                                            </asp:DropDownList>
                                                                
                                            </td>

                                        </tr>
                                                  
                                    </table>
                                </asp:Panel>
                                </div>
                            </td>
                        </tr>
--%>                        

<%--                         <tr>
                            <td colspan="4" align="center">
                                <div>
                                    <asp:Panel ID="cpmPhysSchedulePanel" runat="server" Visible="False">
                                                
                                    <table>
                                        <tr>
                                            <td>
                                                    <asp:Label id="schdeulbl" runat="server" Text="Schedulable:"></asp:Label>

                                            </td>
                                            <td>

                                                <asp:RadioButtonList ID = "Schedulablerbl" runat="server" RepeatDirection="Horizontal">
                                                    <asp:ListItem Value="yes">Yes</asp:ListItem>
                                                    <asp:listItem Value="no">No</asp:listItem>
                                                </asp:RadioButtonList>
                                                <asp:TextBox ID="SchedulableTextbox" runat="server" Visible="False"></asp:TextBox>

                                            </td>
                                            <td>
                                                <asp:Label ID="teltlbl" runat="server" text="Televox:" />
                                
                                            </td>
                                            <td>

                                                <asp:RadioButtonList ID = "Televoxrbl" runat="server" RepeatDirection="Horizontal">
                                                    <asp:ListItem Value="yes">Yes</asp:ListItem>
                                                    <asp:listItem Value="no">No</asp:listItem>
                                                </asp:RadioButtonList>
                                                <asp:TextBox ID="TelevoxTextBox" runat="server" Visible="False"></asp:TextBox>
                                         
                                      
                                            </td>
                                        </tr>
                                                  
                                    </table>
                                </asp:Panel>                                          
                                </div>
                            </td>
                        </tr>
--%>

<%--                        <tr>
                            <td colspan="4">
                             <div>
                               <asp:Panel ID="educationpan" runat="server" Visible="False">
                                 <updatepanel>

                                <table id="education" border="1px" width="100%">
                                    <tr>
                                        <td colspan="4" class="tableRowSubHeader"  align="left">
                                            CPM Education
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" align="center">
                                            <asp:Label ID="cpmedulabel" runat="server" Visible="false"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" align="center">
                                            <asp:CheckBoxList ID="educationListbox" runat="server"  RepeatColumns="3" 
                                                AutoPostBack="false">
                                                
                                            </asp:CheckBoxList>
                                            <asp:TextBox ID="cpmEducationSelected" runat="server" Visible="False" ></asp:TextBox>
                                            <asp:TextBox ID="cpmEducationText" runat="server" Visible="False"></asp:TextBox>
                                        </td>
                                    
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="right">
                                            Enter Date for Class:
                                        </td>
                                        <td colspan="2" align="left">
                                            <asp:TextBox ID="educationDateTextBox" runat="server" ToolTip="Each new education Class needs a date"
                                                CssClass="calenderClass"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" align="center">
                                        
                                            <asp:Button ID="btnEducationSubmit" runat="server" Text="Submit Class" 
                                            CssClass="btnhov" BackColor="#006666"  Width="170px" />
                                            
                                        </td>
                                    </tr>
                                </table>
                              
                            </updatepanel>
                              
                              </asp:Panel>

                            
                            </div>            
                                            
                           </td>
                        </tr>
--%>                                         

                         <tr>
                            <td colspan="4">
                               <table>
                                    <tr>
                                        <td>
                                            <asp:Label ID="otherVendorlbl" runat="server" Visible="False" 
                                                Text="Unknow Vendor/Contractor:"></asp:Label>
                                        </td>
                                        <td align="left" colspan="3">
                                            <asp:Label ID="VendorUnknowlbl" runat="server" Visible="False"></asp:Label>
                                        </td>
                                    
                                    </tr>

                                  <tr>
                                      <td align="right" >
                                          <asp:Label ID="lblVendor" runat="server" Text="Contractor/Vendor Name:" Visible="false"></asp:Label>
                                        </td>
                                      <td align="left"  colspan="3">
                                            <asp:TextBox ID ="VendorNameTextBox" runat="server" Enabled="False" 
                                                Width="500px" ></asp:TextBox>
                                              <asp:DropDownList ID="Vendorddl" runat="server" Width="500px" Visible="False" 
                                                AutoPostBack="true">
                                
                                            </asp:DropDownList>
                                            <asp:Label ID="VendorNumLabel" runat="server" Visible="False"></asp:Label>

                                      </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                         <tr>
                            <td align="right" >
                            Start Date:
                            </td>
                            <td align="left" >
                                <asp:TextBox ID ="startdateTextBox" runat="server"  CssClass="calenderClass"></asp:TextBox>
                            </td>
       
                            <td align="right" >
                            End Date:
                            </td>
                            <td align="left" >
                                <asp:TextBox ID="enddateTextBox" runat="server" CssClass="calenderClass"></asp:TextBox>
                            </td>
                            </tr>
                           <tr>
                                <td colspan = "4">
                                    <asp:Label ID ="departmentcdLabel" runat="server" Visible="False"></asp:Label>
                                     <asp:Label ID ="TitleLabel" runat="server" Visible="False"></asp:Label>
                                     <asp:TextBox ID = "buildingcdTextBox" runat="server" Visible ="False"></asp:TextBox>
                                    <asp:Label ID = "emptypecdLabel" runat="server" Visible="False"></asp:Label>
                                    <asp:Label ID = "HDemptypecdLabel" runat="server" Visible="False"></asp:Label>

                                     <asp:Label ID = "facilitycdLabel" runat="server" Visible ="False"></asp:Label>
                                     <asp:Label ID = "entitycdLabel" runat="server" Visible ="False"></asp:Label>
                                     <asp:Label ID= "buildingnameLabel" runat="server" Visible="False" ></asp:Label>
                                     <asp:Label ID="HDemployeetype" runat="server" Visible="False"></asp:Label> 
									 <asp:Label ID="HDVendorNum" runat="server" Visible="False"></asp:Label> 

                                     <asp:Label ID="suffixlabel" runat="server" Visible="False"></asp:Label>
                                     <asp:Label ID= "AccountRequestTypeLabel" runat="server" Visible="False"></asp:Label>
                                    <asp:Label ID= "Account_Request_Type" runat="server" Visible="False"></asp:Label>
									 <asp:Label ID= "AddAccountRequestnumLabel" runat="server" Visible="False"></asp:Label>

                                     <asp:Label ID="RoleNumLabel" runat="server" Visible="False"></asp:Label>
                                     <asp:Label ID="userDepartmentCD"  runat="server" Visible="False"></asp:Label>
                                     <asp:Label ID="SelectedChgRequestor" runat="server" Visible="False"></asp:Label>
                                     <asp:TextBox ID="FloorTextBox" runat="server" Visible="False"></asp:TextBox>
                                    <asp:Label ID="submitter_client_numLabel" runat="server" visible="False" />
                                    <asp:Label ID="requestor_client_numLabel" runat="server" visible="False"></asp:Label>

                                    <asp:Label ID="origionalGnumber"  runat="server" visible="False"></asp:Label>
                                   <asp:Label ID="LocationOfCareIDLabel"  runat="server" visible="False"></asp:Label>
                                     <asp:Label ID="GNumberGroupNumLabel"  runat="server" visible="False"></asp:Label>

                                    <asp:Label ID ="CoverageGroupNumLabel"  runat="server" visible="False"></asp:Label>

                                    <asp:Label id="NonPerferedGroupnumLabel" runat="server" Visible="False"></asp:Label>
                                    <asp:Label id="origionalNonperGroup" runat="server" Visible="False"></asp:Label>


                                    <asp:Label ID="origionalCommNumber"  runat="server" visible="False"></asp:Label>
                                    <asp:Label ID="CommLocationOfCareIDLabel" runat="server" visible="False"></asp:Label>
                                    <asp:Label ID="CommNumberGroupNumLabel" runat="server" visible="False"></asp:Label>

                                    <asp:Label ID="origionalgroupNum"  runat="server" visible="False"></asp:Label>
                                     <asp:TextBox ID="masterRole" runat="server"  Visible="false"></asp:TextBox>
                                     <asp:TextBox ID="ecareexists" runat="server" Visible="false"></asp:TextBox>
                                     <asp:TextBox ID="groupnameTextBox" runat="server" Width="500px" 
                                        Visible="False"></asp:TextBox>
                                     <asp:Label ID="PositionRoleNumLabel" runat="server" Visible="False"></asp:Label>
                                     <asp:Label id="invisionLbl" runat="server" Text="eCare Group Number:"   
                                        Visible="False"></asp:Label>
<%--                                     <asp:TextBox ID="groupnumTextBox" runat="server" Enabled="False" 
                                        Visible="False"></asp:TextBox>--%>

                                     <asp:TextBox ID="InvisionGroupNumTextBox" runat="server"  Enabled="False" 
                                        Visible="False"></asp:TextBox>

                                    <asp:TextBox ID="CellProvidertxt" runat="server" Visible="False"></asp:TextBox>

                                    <asp:TextBox ID="ValidationStatus" runat="server" Visible="false"></asp:TextBox>

                                    <asp:Label ID="securitylevel" runat="server" Visible="false"></asp:Label>


									<asp:Label ID="testphonelbl" runat="server" Visible="false"></asp:Label>
									<asp:Label ID="testemployeenumberlbl" runat="server" Visible="false"></asp:Label>
									<asp:Label ID="testtitlelbl" runat="server" Visible="false"></asp:Label>
									


                                 </td>
                            </tr>
                            </table>
                        </td>
                        </tr>
                       </table>
                     </ContentTemplate>
                  </ajaxToolkit:TabPanel>



                  <ajaxToolkit:TabPanel ID="tpAddItems" runat="server" TabIndex="1">
                    <HeaderTemplate>
                        <asp:Label ID="lblinner1" runat="server" Text="Request New Accounts" Font-Bold="true" ForeColor="Black"></asp:Label>
                    </HeaderTemplate>
                        <ContentTemplate>

                           <table border="0" style="background-color:#DCDCDC">
                            <tr>
                                <td colspan = "2" align="center">
                                <asp:Button runat="server" ID ="btnSubmitAddRequest" Text = "Submit Requested Account(s)" Visible="false"
                                CssClass="btnhov" BackColor="#660000"  Width="220px" Height="50px" />

                                </td>
                                <td colspan = "2" align="center"> 
                                <asp:Button ID = "btnAddComments" runat = "server" Text ="Add Comments " Visible="false"
                                    CssClass="btnhov" BackColor="#660000"  Width="200px" height="50px"/>


                              </td>
                            </tr>

                            <tr>
                                <td colspan = "4" align="center">
                            	<%-- =================== Cell phone for access ==================================================--%>

	                                <asp:Panel ID="PanelCellphone" runat="server" Visible="false" >
                                        <table  id="Tabphone" runat="server" style="border: medium groove #000000; height:100%"  align="center" width="100%" >
                                            <tr>

                                                 <td class="tableRowSubHeader" colspan="4" align="center">
                                                        Cell Phone Information for Remote Access
                                                 </td>
                 
                                            </tr>
                                            <tr>
                                                <td colspan="4" align="center">
                                                    <asp:Label id="lblTokenError" runat="server" BackColor="Red" Visible="false" Font-Size="Large" Font-Bold="True"></asp:Label>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="width: 3px; height: 51px;">
                                                    Providers:
                                                </td>
                                                <td align="left">
                                                    <asp:DropDownList ID="CellPhoneddl" runat="server" Width="250px" BackColor="Yellow" CssClass="styleMan">
                                
                                                    </asp:DropDownList>
                                                    <asp:TextBox ID="otherCellprov" runat="server" Visible="false"  MaxLength="25" ></asp:TextBox>
                                                </td>
                                                <td align="right">
                                                    Number: ##########
                                                </td>
                                                <td align="left">
                                                    <asp:TextBox ID ="TokenPhoneTextBox" runat="server" MaxLength="10" 
                                                        ToolTip="Only enter Numbers no other Characters" CssClass="styleMan" BackColor="Yellow"></asp:TextBox>
                                                </td>


                                            </tr>
                                            <tr>
                                                <td>
                                                </td>
                                                <td  colspan="2" align="center" >
<%--                                                    <asp:Button ID="btnAddCellPhone" runat="server" BackColor="#00C000" Font-Bold="True" Font-Names="Arial"
                                                        Font-Size="Small" ForeColor="Black" Height="25px" Text="Add" Width="120px" />
--%>
                                                    <asp:Button ID="btnCanCellPhone" runat="server" BackColor="DimGray" Font-Bold="True"
                                                        Font-Names="Arial" Font-Size="Small" ForeColor="White" Height="25px" Text="Cancel CellPhone Request"
                                                        Visible="true" Width="200px" ToolTip="Return " />

                                                </td>
                                                <td>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td  colspan="4">
                                                    <asp:Label ID="space" runat="server" Height="25px" ></asp:Label>
                                                </td>
                                            </tr>

                                        </table>        
                                    </asp:Panel>
                                 </td>
                            </tr>

                             <tr>
                                 <td align="center" colspan="4">
   	                              <%-- =================== Nurse Stations for mak Pyxis ===============================--%>
                                  <asp:Panel id="CCMCNurseStationspan" runat="server"  Visible="false">
                                    <table  border="3px" width="100%">
                                        <tr>
                                            <td>
                                              CCMC
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                 <asp:CheckBoxList ID="cblCCMCAll" runat="server" RepeatColumns="0"
                                                     AutoPostBack="true">
                                                     <asp:ListItem Value="ccmcall" Text="All CCMC" ></asp:ListItem>
                                                 </asp:CheckBoxList>                                    
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                               <asp:CheckBoxList ID="cblCCMCNursestations" runat="server" RepeatColumns="2"
                                                     AutoPostBack="true" >
                                                 </asp:CheckBoxList>                                    
                                            </td>


                                        </tr>
                                    </table>
                                  </asp:Panel>
                                  <asp:Panel ID="dcmhnursepan" runat="server" Visible="false">
                                    <table  border="3px" width="100%">
                                        <tr>
                                            <td>
                                                DCMH
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                 <asp:CheckBoxList ID="cblDCMHAll" runat="server" RepeatColumns="0"
                                                     AutoPostBack="true" >
                                                     <asp:ListItem Value="dcmhall" Text="All DCMH"></asp:ListItem>
                                                 </asp:CheckBoxList>                                    
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                               <asp:CheckBoxList ID="cblDCMHNursestataions" runat="server" RepeatColumns="3"
                                                     AutoPostBack="true">
                                                 </asp:CheckBoxList>                                    
                                            </td>
                                        </tr>
                                    </table>
                          
                                  </asp:Panel>
                                  <asp:Panel ID="taylornursepan" runat="server" Visible="false">
                                    <table  border="3px" width="100%">
                                        <tr>
                                            <td valign="top">
                                            Taylor
                                            </td>
                                        </tr>

                                        <tr>
                                            <td valign="top">
                                                 <asp:CheckBoxList ID="cblTAll" runat="server" RepeatColumns="0"
                                                     AutoPostBack="true" >
                                                     <asp:ListItem Value="taylorall" Text="All Taylor"></asp:ListItem>
                                                 </asp:CheckBoxList>                                    
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                               <asp:CheckBoxList ID="cblTaylorNursestataions" runat="server" RepeatColumns="3"
                                                     AutoPostBack="true">
                                                 </asp:CheckBoxList>                                    
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblAppSystem" runat="server" Visible="false"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                          
                                  </asp:Panel>

                                  <asp:Panel ID="SpringNurseStationsPan" runat="server"  Visible="false">

                                     <table id="Table2"  border="3px" width="100%">
                                        <tr>
                                    
                                            <td>
                                              Springfield
                                             </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                 <asp:CheckBoxList ID="cblSAll" runat="server" RepeatColumns="0"
                                                     AutoPostBack="true">
                                                     <asp:ListItem Value="springall" Text="All Springfield"></asp:ListItem>
                                                 </asp:CheckBoxList>
                                            </td>
                                    
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                               <asp:CheckBoxList ID="cblSpringfieldNursestataions" runat="server" RepeatColumns="3"
                                                     AutoPostBack="true" >
                                                 </asp:CheckBoxList>

                                            </td>
                                
                                        </tr>
                                     </table>
                                    </asp:Panel>
                                 </td>
                             </tr> 
                               <tr>
                                    <td align="center" colspan="4" >
                                        <asp:Panel ID="requestCommentsPan" runat="server" Visible="false">

                                              <table  id="tblComments" runat="server" style="border: medium groove #000000; height:100%"  align="center" width="100%" >
                                                <tr>
                                                    <td style="width: 3px">
                                                    </td>
                                                    <td style="font-weight: bold; font-size: larger; text-align: center;" align="center">
                                                        Add Comment</td>
                                                    <td style="width: 3px">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 3px; height: 151px;">
                                                    </td>
                                                    <td align="center" style="height: 150px" >
                                                        <asp:TextBox ID="txtComment" runat="server" Height="100px" 
                                                        TextMode="MultiLine" Width="450px" MaxLength="75"></asp:TextBox>

                                                    </td>
                                                    <td style="width: 3px; height: 111px;">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 3px">
                                                    </td>
                                                    <td align="center" >
                                                        <asp:Button ID="btnUpdComment" runat="server" BackColor="#00C000" Font-Bold="True" Font-Names="Arial"
                                                            Font-Size="Small" ForeColor="Black" Height="25px" Text="Add" Width="120px" />

                                                        <asp:Button ID="btnCancelComment" runat="server" BackColor="DimGray" Font-Bold="True"
                                                            Font-Names="Arial" Font-Size="Small" ForeColor="White" Height="25px" Text="Cancel"
                                                            Visible="true" Width="120px" ToolTip="Return " />

                                                    </td>
                                                    <td style="width: 3px">
                                                    </td>
                                                </tr>
                                            </table>
      


                                        </asp:Panel>


                                    </td>
                               </tr>
                             <tr>
                                 <td align="center" colspan="4">
   	                              <%-- =================== 365 List sites ===============================--%>

                                  <asp:Panel ID="O365Panel" runat="server"  Visible="false">

                                     <table id="Table3"  border="3px" width="100%">
                                        <tr>
                                            <td>
                                                O365 asccounts
                                    
                                            </td>
                                            <td>
                                                 <asp:CheckBoxList ID="cblo365" runat="server" RepeatColumns="0"
                                                     AutoPostBack="false">
                                                 </asp:CheckBoxList>
                                            </td>
                                    
                                        </tr>
                                     </table>
                                    </asp:Panel>
                                 </td>
                             </tr>

                            <tr style="background-color:#FFFFCC; font-weight:bold">
                                <td colspan="2">
                                Accounts available
                                </td>
                                <td colspan="2">
                                Accounts to be added
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2"  valign="top" style="width: 50%">
                                    <asp:GridView ID="grAvailableAccounts" runat="server" AllowSorting="True" 
                                    AutoGenerateColumns="False" BackColor="White" BorderColor="#336666"
                                    BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                                    EmptyDataText="No Applications Found" EnableTheming="False" 
                                    DataKeyNames="Applicationnum, ApplicationDesc"
                                    Font-Size="Small" GridLines="Horizontal" Width="100%">
                                   
                                    <FooterStyle BackColor="White" ForeColor="#333333" />
                                    <HeaderStyle BackColor="#336666" Font-Bold="False" ForeColor="White" Font-Size="Small" HorizontalAlign="Left"    />                                    
                                    <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Left" Font-Size="Medium"/>
                                    <RowStyle BackColor="White" ForeColor="#333333" />


                                    <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                    <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                    <SortedAscendingHeaderStyle BackColor="#487575" />
                                    <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                    <SortedDescendingHeaderStyle BackColor="#275353" />

                                    <Columns>
                                            <asp:CommandField ButtonType="Button" SelectText="Select" ShowSelectButton= "true" HeaderStyle-HorizontalAlign = "Left"  ControlStyle-BackColor="#84A3A3" ItemStyle-Height="30px" />
                                            <asp:BoundField  DataField="ApplicationDesc"  HeaderText="Application"  HeaderStyle-HorizontalAlign = "Left" ItemStyle-Height="35px"></asp:BoundField>
				                                                                  
                                  
                                    </Columns>
                                    </asp:GridView>
		                        </td>
                                <td colspan="2"  valign="top" style="width: 50%">
                                    <asp:GridView ID="grAccountsToAdd" runat="server" AllowSorting="True" 
                                    AutoGenerateColumns="False" BackColor="White" BorderColor="#336666"
                                    BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                                    EmptyDataText="Select accounts to add" EnableTheming="False" 
                                    DataKeyNames="Applicationnum, ApplicationDesc"
                                    Font-Size="Medium" GridLines="Horizontal" Width="100%">
                                   
                                    <FooterStyle BackColor="White" ForeColor="#333333" />
                                    <HeaderStyle BackColor="#336666" Font-Bold="False" ForeColor="White" Font-Size="Small" HorizontalAlign="Left"    />                                    
                                    <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Left" Font-Size="Medium"/>
                                    <RowStyle BackColor="White" ForeColor="#333333" />
                                    <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                    <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                    <SortedAscendingHeaderStyle BackColor="#487575" />
                                    <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                    <SortedDescendingHeaderStyle BackColor="#275353" />

                                    <Columns>
                                                                                            
                                                <asp:CommandField ButtonType="Button" SelectText="Cancel" ShowSelectButton= "true"  ControlStyle-BackColor="#84A3A3" ItemStyle-Height="30px"/>
                                                <asp:BoundField  DataField="ApplicationDesc"  HeaderText="Application"  HeaderStyle-HorizontalAlign = "Left" ItemStyle-Height="35px"></asp:BoundField>
				                                                                  
                                  
                                    </Columns>

                                </asp:GridView>
		                        </td>
                                </tr>
                              </table>

                           </ContentTemplate>
                  </ajaxToolkit:TabPanel>


                  <ajaxToolkit:TabPanel ID="tpAddFinanItems" runat="server">
                    <HeaderTemplate>
                        <asp:Label ID="lblFininner" runat="server" Text="Request New Financial Accounts" Font-Bold="true" ForeColor="Black"></asp:Label>
                    </HeaderTemplate>
                      <ContentTemplate>

                           <table border="0" style="background-color:#DCDCDC">
                            <tr>
                                <td colspan = "2" align="left">
                                <asp:Button runat="server" ID ="btnSubmitFinAccount" Text = "Submit New Finance Accounts" Visible="false"
                                    CssClass="btnhov" BackColor="#006666"  Width="200px" />


                            </td>
                            <td colspan = "2" align="left"> 
                                <asp:Button ID = "btnAddFinComments" runat = "server" Text ="Add Comments " Visible="false"
                                    CssClass="btnhov" BackColor="#006666"  Width="175px" />


                                </td>
                            </tr>

                            <tr style="background-color:#FFFFCC; font-weight:bold">
                                <td colspan="2">
                                Financial Accounts available
                                </td>
                                <td colspan="2">
                                Accounts to be added
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2"  valign="top" style="width: 50%">
                                    <asp:GridView ID="GrFinAvailable" runat="server" AllowSorting="True" 
                                    AutoGenerateColumns="False" BackColor="White" BorderColor="#336666"
                                    BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                                    EmptyDataText="No Applications Found" EnableTheming="False" 
                                    DataKeyNames="Applicationnum, ApplicationDesc"
                                    Font-Size="Small" GridLines="Horizontal" Width="100%">
                                   
                                    <FooterStyle BackColor="White" ForeColor="#333333" />
                                    <HeaderStyle BackColor="#336666" Font-Bold="False" ForeColor="White" Font-Size="Small" HorizontalAlign="Left"    />                                    
                                    <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Left" Font-Size="Medium"/>
                                    <RowStyle BackColor="White" ForeColor="#333333" />


                                    <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                    <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                    <SortedAscendingHeaderStyle BackColor="#487575" />
                                    <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                    <SortedDescendingHeaderStyle BackColor="#275353" />

                                    <Columns>
                                            <asp:CommandField ButtonType="Button" SelectText="Select" ShowSelectButton= "true" HeaderStyle-HorizontalAlign = "Left"  ControlStyle-BackColor="#84A3A3" ItemStyle-Height="30px" />
                                            <asp:BoundField  DataField="ApplicationDesc"  HeaderText="Application"  HeaderStyle-HorizontalAlign = "Left" ItemStyle-Height="35px"></asp:BoundField>
				                                                                  
                                  
                                    </Columns>
                                    </asp:GridView>
		                        </td>
                                <td colspan="2"  valign="top" style="width: 50%">
                                    <asp:GridView ID="GrFinAddAccount" runat="server" AllowSorting="True" 
                                    AutoGenerateColumns="False" BackColor="White" BorderColor="#336666"
                                    BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                                    EmptyDataText="Select accounts to add" EnableTheming="False" 
                                    DataKeyNames="Applicationnum, ApplicationDesc"
                                    Font-Size="Medium" GridLines="Horizontal" Width="100%">
                                   
                                    <FooterStyle BackColor="White" ForeColor="#333333" />
                                    <HeaderStyle BackColor="#336666" Font-Bold="False" ForeColor="White" Font-Size="Small" HorizontalAlign="Left"    />                                    
                                    <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Left" Font-Size="Medium"/>
                                    <RowStyle BackColor="White" ForeColor="#333333" />
                                    <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                    <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                    <SortedAscendingHeaderStyle BackColor="#487575" />
                                    <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                    <SortedDescendingHeaderStyle BackColor="#275353" />

                                    <Columns>
                                                                                            
                                                <asp:CommandField ButtonType="Button" SelectText="Cancel" ShowSelectButton= "true"  ControlStyle-BackColor="#84A3A3" ItemStyle-Height="30px"/>
                                                <asp:BoundField  DataField="ApplicationDesc"  HeaderText="Application"  HeaderStyle-HorizontalAlign = "Left" ItemStyle-Height="35px"></asp:BoundField>
				                                                                  
                                  
                                    </Columns>

                                </asp:GridView>
		                        </td>
                                </tr>
                              </table>
                      </ContentTemplate>
                  </ajaxToolkit:TabPanel>

                  
                  <ajaxToolkit:TabPanel ID="tpRemoveItems" runat="server"  BackColor="Gainsboro" Visible = "True">
                    <HeaderTemplate > 
                        <asp:Label ID="Label4" runat="server" Text="Remove Current Accounts" Font-Bold="true" ForeColor="Black"></asp:Label>
                    </HeaderTemplate>
                      <ContentTemplate>

                        <table id="tblRemoveItems" width="100%" border="0"  style="background-color:#DCDCDC">
                            <tr>
                                <td  colspan="2" align="center">
                                    <asp:Button runat="server" ID ="btnInvalidAccts" Text = "Submit Delete Account Request"  Visible="false"
                                        CssClass="btnhov" BackColor="#660000"  Width="250px" Height="50px"/>
                                     
                                </td>
                                <td colspan="2" align="center"> 
                                    <asp:Button ID = "btnAddDeleteComments" runat = "server" Text ="Add Comments " Visible="false"
                                        CssClass="btnhov" BackColor="#660000"  Width="175px" Height="50px" />

                                 </td>

                            </tr>
                            <tr style="background-color:#FFFFCC; font-weight:bold">
                                <td colspan="2" >
                                Current Accounts
                                </td>
                                <td colspan="2">
                                Accounts to remove
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2"  valign="top"  style="width: 50%">
                                    <asp:GridView ID="gvCurrentApps" runat="server" AllowSorting="True" 
                                        AutoGenerateColumns="False" BackColor="White" BorderColor="#336666"
                                        BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                                        EmptyDataText="No Applications Found" EnableTheming="False" DataKeyNames="ApplicationDesc, ApplicationNum,login_name,SubApplicationNum"
                                        Font-Size="Small" GridLines="Horizontal" Width="100%">
                                   
                                        <FooterStyle BackColor="White" ForeColor="#333333" />
                                        <HeaderStyle BackColor="#336666" Font-Bold="False" ForeColor="White" Font-Size="Small" HorizontalAlign="Left"    />                                    
                                        <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Left" Font-Size="Medium"/>
                                        <RowStyle BackColor="White" ForeColor="#333333" />
                                        <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                        <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                        <SortedAscendingHeaderStyle BackColor="#487575" />
                                        <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                        <SortedDescendingHeaderStyle BackColor="#275353" />

                                        <Columns>
                                                                                            
                                                    <asp:CommandField ButtonType="Button" SelectText="Select" ShowSelectButton= "true" ControlStyle-BackColor="#84A3A3" ItemStyle-Height="30px"/>
                                                    <asp:BoundField  DataField="ApplicationDesc"  HeaderText="Application" HeaderStyle-HorizontalAlign="Left"  ItemStyle-Height="35px"></asp:BoundField>
                                                    <asp:BoundField  DataField="login_name"  HeaderText="Login Name" HeaderStyle-HorizontalAlign="Left"  ItemStyle-Height="35px"></asp:BoundField>
				                                                                  
                                  
                                        </Columns>
                                        </asp:GridView>
		                            </td>
                                    <td colspan="2" valign="top"  style="width: 50%">
                                        <asp:GridView ID="gvRemoveAccounts" runat="server" AllowSorting="True" 
                                                AutoGenerateColumns="False" BackColor="White" BorderColor="#336666"
                                                BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                                                EmptyDataText="Select applications to be removed" EnableTheming="False" DataKeyNames="ApplicationNum,ApplicationDesc,login_name, Subappnum"
                                                Font-Size="Small" GridLines="Horizontal" Width="100%" >
                                   
                                            <FooterStyle BackColor="White" ForeColor="#333333" />
                                            <HeaderStyle BackColor="#336666" Font-Bold="False" ForeColor="White" Font-Size="Small" HorizontalAlign="Left"    />                                    
                                            <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Left" Font-Size="Medium"/>
                                            <RowStyle BackColor="White" ForeColor="#333333" />
                                            <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                            <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                            <SortedAscendingHeaderStyle BackColor="#487575" />
                                            <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                            <SortedDescendingHeaderStyle BackColor="#275353" />

                                            <Columns>
                                                    <asp:CommandField ButtonType="Button" SelectText="Cancel" ShowSelectButton= "true"   ControlStyle-BackColor="#84A3A3" ItemStyle-Height="30px" />
                                                    <asp:BoundField  DataField="ApplicationNum"  HeaderText="#" HeaderStyle-HorizontalAlign="Left" ItemStyle-Height="30px" ></asp:BoundField>

                                                    <asp:BoundField  DataField="ApplicationDesc"  HeaderText="Application" HeaderStyle-HorizontalAlign="Left" ItemStyle-Height="30px" ></asp:BoundField>
                                                     <asp:BoundField  DataField="login_name"  HeaderText="Login Name" HeaderStyle-HorizontalAlign="Left"  ItemStyle-Height="35px"></asp:BoundField>

                                                     <asp:BoundField  DataField="Subappnum"  HeaderText="Sub App" HeaderStyle-HorizontalAlign="Left"  Visible="false" ItemStyle-Height="35px"></asp:BoundField>
                                            </Columns>
                                            </asp:GridView>
                                        </td>
                                </tr>
                        </table>
                        </ContentTemplate>
                </ajaxToolkit:TabPanel>

                 <ajaxToolkit:TabPanel ID="tpOpenRequest" runat="server" BorderStyle="Solid" BorderWidth="1px">
                      <HeaderTemplate > 
                          <asp:Label ID="Label2" runat="server" Text="Open Acct. Requests" Font-Bold="true" ForeColor="Black" ></asp:Label>
                     </HeaderTemplate>
                      <ContentTemplate>
                          <table border="0" width = "100%">
                            <tr>
                                <td colspan = "4">
                                      <asp:Table ID = "tblOpenRequests" runat = "server" Font-Size="Small">
                                     </asp:Table>
            
                                </td>
        
                            </tr>
                           </table>
                       </ContentTemplate>
                  </ajaxToolkit:TabPanel>

                   <ajaxToolkit:TabPanel ID="tpClosedRequest" runat="server" BorderStyle="Solid" BorderWidth="1px">
                       <HeaderTemplate > 
                          <asp:Label ID="Label3" runat="server" Text="Closed Acct. Requests" Font-Bold="true" ForeColor="Black" ></asp:Label>
                     </HeaderTemplate>
                      <ContentTemplate>
                          <table border="0" width = "100%">
                            <tr>
                                <td colspan = "4">
                                      <asp:Table ID = "tblClosedRequest" runat = "server" Font-Size="Small">
                                     </asp:Table>
            
                                </td>
        
                            </tr>
                           </table>
                       </ContentTemplate>
                   </ajaxToolkit:TabPanel>

                  <ajaxToolkit:TabPanel ID="tpCurrentAccounts" runat="server" TabIndex="0"  BorderStyle="Solid" BorderWidth="1px">
                      <HeaderTemplate > 
                            <asp:Label ID="lblcurr" runat="server" Text="Current Accounts" Font-Bold="true" ForeColor="Black" ></asp:Label>
                    
                      </HeaderTemplate>
                      <ContentTemplate>
                          <table border="0" width = "100%">
                            <tr>
                                <td colspan = "4">
                                      <asp:Table ID = "tblCurrentAccounts" runat = "server">
                                     </asp:Table>
            
                                </td>
        
                            </tr>
                           </table>
                       </ContentTemplate>
                  </ajaxToolkit:TabPanel>

                  <ajaxToolkit:TabPanel ID="tpRequestItems" runat="server" TabIndex="0" BorderStyle="Solid" BorderWidth="1px" Visible="false">
                  <HeaderTemplate>
                      <asp:Label ID="Label6" runat="server" Font-Bold="true" ForeColor="Black" 
                          Text="RFS/Tickets"></asp:Label>
                  </HeaderTemplate>
                  <ContentTemplate>
                      <table border="0" width="100%">
                          <tr>
                              <td colspan="4">
                                  <asp:Table ID="tblRFSTickets" runat="server">
                                  </asp:Table>
                              </td>
                          </tr>
                      </table>
                  </ContentTemplate>
              </ajaxToolkit:TabPanel>

              <ajaxToolkit:TabPanel ID="tpLDAP" runat="server" TabIndex="0" BorderStyle="Solid" BorderWidth="1px" Visible="false">
                  <HeaderTemplate>
                      <asp:Label ID="Label8" runat="server" Font-Bold="true" ForeColor="Black" 
                          Text="LDAP"></asp:Label>
                  </HeaderTemplate>
                  <ContentTemplate>
                      <table border="0" width="100%" style="background-color: #CCCCCC">
                          <tr>
                              <td colspan="4">
                                  <table>
                                      <tr>
                                          <td align="left"> Account Name:</td>
                                          <td> 
                                              <asp:TextBox ID="LDAPLoginNameTextbox" runat="server" Enabled="false"></asp:TextBox>
                                          </td>
                                          <td align="right">
                                              <asp:Label ID="ADaccountslbl" runat="server" Text="Select AD Account:" Visible="false"></asp:Label>
                                          </td>
                                           <td align="left" >
                                            <asp:dropdownlist ID = "ADAccountddl" runat="server" AutoPostBack="False" Visible="false" >

                                            </asp:dropdownlist>
                                             <asp:Label ID = "SelectedADlbl" runat="server" Visible ="False"></asp:Label>

                                        </td>

                                      </tr>
                                    <tr>
                                        <td colspan="2">

                                        </td>
                                        <td align="center" colspan="2">

                                               <asp:Button ID="BtnSearchLDAP" runat="server" Text = "Get LDAP"  Visible="False" 
                                                    ToolTip="Displays LDAP information for this AD account" 
                                                    CssClass="btnhov" BackColor="#006666"  Width="175px" />

                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            First Name:
                                        </td>
                                        
                                        <td align="left" >
                                            Last Name:
                                        </td>
                                        <td align="left" >
                                            eMail:
                                        </td>
                                         <td align="left" >
                                            Phone:
                                        </td>
                                    </tr>

                                      <tr>
                                        <td align="left"  >
                                            <asp:Textbox width="280px" ID = "FNameTextBox" runat="server"  Enabled="false"></asp:Textbox>
                                        </td>


                                        <td align="left" >
                                            <asp:Textbox ID = "LnameTextBox" width="280px" runat="server" Enabled="false"></asp:Textbox>
                                        </td>
                                    


                                        <td align="left" >
                                            <asp:Textbox ID = "LDAPeMailTextBox" width="280px" runat="server" Enabled="false"></asp:Textbox>
                                        </td>


                                        <td align="left" >
                                            <asp:Textbox ID = "LDAPtelephonenumberTextBox" width="280px" runat="server" Enabled="false"></asp:Textbox>
                                        </td>

                                </tr>
                                <tr>
                                    <td>
                                        Last Verification Date:

                                    </td>
                                       
                                    <td align="left" >
                                            Last LDAP Modify Date:
                                       </td>

                                        <td align="left" >
                                            Last Device Logon:
                                        </td>

                                         <td align="left" >
                                            Last Domain Logon:
                                        </td>
                                    </tr>
                                      <tr>
                                        <td align="left"  >
                                            <asp:Textbox width="280px" ID = "LastVerificationDateTextBox" runat="server" Enabled="false"></asp:Textbox>
                                        </td>

                                        <td align="left" >
                                            <asp:Textbox ID = "modifytimestampTextBox" width="280px" runat="server" Enabled="false"></asp:Textbox>
                                        </td>

 

                                        <td align="left" >
                                            <asp:Textbox ID = "LastDeviceLogonTextBox" width="280px" runat="server" Enabled="false"></asp:Textbox>
                                        </td>


                                        <td align="left" >
                                            <asp:Textbox ID = "LastDomainLogonTextBox" width="280px" runat="server" Enabled="false"></asp:Textbox>
                                        </td>
                                    </tr>
                                    <tr>

                                        <td>
                                            Account Status:

                                        </td>
                                        <td align="left" >
                                            Lock Out Date:
                                        </td>

                                        <td align="left" >
                                            Employee#:
                                        </td>

                                         <td align="left" >
                                            Department:
                                        </td>
                                        </tr>
                                      <tr>

                                        <td align="left"  >
                                            <asp:Textbox width="280px" ID = "userAccountControlTextBox" runat="server" Enabled="false"></asp:Textbox>
                                        </td>


                                        <td align="left" >
                                            <asp:Textbox ID = "LockOutDateTextBox" width="280px" runat="server" Enabled="false"></asp:Textbox>
                                        </td>


                                        <td align="left" >
                                            <asp:Textbox ID = "LDAPEmpnumTextBox" width="280px" runat="server" Enabled="false"></asp:Textbox>
                                        </td>


                                        <td align="left" >
                                            <asp:Textbox ID = "LDAPdepartmentTextBox" width="280px" runat="server" Enabled="false"></asp:Textbox>
                                        </td>
                                    </tr>
                              </td>
                          </tr>
                      </table>
                  </ContentTemplate>
              </ajaxToolkit:TabPanel>




<%--              <ajaxToolkit:TabPanel ID="tpCPMitems" runat="server" TabIndex="0" BorderStyle="Solid" BorderWidth="1px">
                  <HeaderTemplate>
                      <asp:Label ID="Label8" runat="server" Font-Bold="true" ForeColor="Black" 
                          Text="CPM Classes"></asp:Label>
                  </HeaderTemplate>
                  <ContentTemplate>
                      <table border="0" width="100%">
                          <tr>
                              <td colspan="4">
                                  <asp:Table ID="tblCPMClasses" runat="server">
                                  </asp:Table>
                              </td>
                          </tr>
                      </table>
                  </ContentTemplate>
              </ajaxToolkit:TabPanel>

--%>
<%-- All tabs below will not show on this page but are on AccountInfo page  --%>

<%--                  <ajaxToolkit:TabPanel ID="tpRequestItems" runat="server" TabIndex="0"  BackColor="red" Visible = "False">
                      <HeaderTemplate > 
                            <asp:Label ID="Label6" runat="server" Text="Request Items" Font-Bold="true" ForeColor="Black" ></asp:Label>
                    
                      </HeaderTemplate>
                      <ContentTemplate>
                          <table border="3" width = "100%">
                            <tr>
                                <td colspan = "4">
                                      <asp:Table ID = "tblRequestItems" runat = "server">
                                     </asp:Table>
            
                                </td>
        
                            </tr>
                           </table>
                       </ContentTemplate>
                  </ajaxToolkit:TabPanel>--%>

<%--                  <ajaxToolkit:TabPanel ID="TPActionItems" runat="server" TabIndex="0"  BackColor="red" Visible = "False">
                      <HeaderTemplate > 
                            <asp:Label ID="Label7" runat="server" Text="Action Items" Font-Bold="true" ForeColor="Black" ></asp:Label>
                    
                      </HeaderTemplate>
                      <ContentTemplate>
                          <table border="3" width = "100%">
                            <tr>
                                <td colspan = "4">
                                      <asp:Table ID = "tblActionItems" runat = "server">
                                     </asp:Table>
            
                                </td>
        
                            </tr>
                           </table>
                       </ContentTemplate>
                  </ajaxToolkit:TabPanel>--%>

           </ajaxToolkit:TabContainer>
        </td>
     </tr>
</table>
  <%-- =================== Change one of 5 physician group location Submit Demo change request=======================================--%>
	<asp:Panel ID="pnlChangePhysGroup" runat="server" Height="450px" Width="575px"  BackColor="#FFFFCC" style="display:none" >
        <table  id="tblChangePhysGroup" runat="server" style="border: medium groove #000000; height:100%"  align="center" width="100%" >

            <tr>

                <td  colspan="3" align="center" style="font-size: x-large; font-weight: bold; color: #FF0000">
                        You have Altered one of the Physician Groups Submit a Demographic Change Request if necessary!
                </td>

            </tr>
            <tr>

                <td  colspan="3" align="center" style="font-size: large; font-weight: bold; color: #FF0000">
                    <asp:Label ID="NonHanlbl" runat="server" Visible="false" />
                    <br />

                </td>

            </tr>
            <tr>
                <td colspan="3" align="center" style="font-size: medium; font-weight: bold; color: #800080">
                    <asp:Label ID="lblBackout" runat="server" Visible="false" />
                
                </td>
            </tr>
            <tr>
                <td style="width: 3px">
                </td>

                <td style="font-weight: bold; font-size: larger; text-align: center;" align="center">
                   This form should be filled if the physician has changed office Address, City Phone or Fax!
                </td>
                <td style="width: 3px">
                </td>

            </tr>
            <tr>
                <td style="width: 3px">
                    <asp:Label ID="lblSearched" runat="server" Visible="false"></asp:Label>
                </td>
                <td align="center" >
                    <asp:Button ID="btnChangePhysGroup" runat="server" BackColor="DimGray" Font-Bold="True"
                        Font-Names="Arial" Font-Size="Small" ForeColor="White" Height="35px" Text="Continue"
                        Visible="true" Width="140px" ToolTip="Return " />

                </td>
                <td style="width: 3px">
                </td>
            </tr>
        </table>
        <br />
        </asp:Panel> 

        <ajaxToolkit:ModalPopupExtender ID="PhysgroupPop" runat="server" 
             TargetControlID="HdBtnPhysgroup" PopupControlID="pnlChangePhysGroup" 
            BackgroundCssClass="ModalBackground" >
        </ajaxToolkit:ModalPopupExtender>

        <%--CancelControlID="btnNoSearch"--%>

       <asp:Button ID="HdBtnPhysgroup" runat="server" style="display:none" />


<%---  +++++++++++++ ob --%>


<%--        <ajaxToolkit:ModalPopupExtender ID="ModBeHalOfPopup" runat="server" 
             TargetControlID="HdBtnSearch" PopupControlID="pnlOnBeHalfOf" 
             BackgroundCssClass="ModalBackground">
        </ajaxToolkit:ModalPopupExtender>--%>


    <%--    BackgroundCssClass="ModalBackground"--%>
        <%--CancelControlID="btnNoSearch"--%>

       <asp:Button ID="HdBtnSearch" runat="server" style="display:none" />
       <%--  ======================== Non privildege panel ========================== --%>
       	<asp:Panel ID="pnlNonPrivileged" runat="server" Height="250px" Width="275px"  BackColor="#FFFFCC" style="display:none" >
        <table  id="Table1" runat="server" style="border: medium groove #000000; height:100%"  align="center" width="100%" >
            <tr>
                <td style="width: 3px">
                </td>
                <td style="font-weight: bold; font-size: larger; text-align: center;" align="center">
                    Clicking Continue will REMOVE Admitting Privileges for this Phyiscian</td>
                <td style="width: 3px">
                </td>
            </tr>
            <tr>
                <td style="width: 3px">
                </td>
                <td align="center" >
                    <asp:Button ID="btnSubmitNonPrivileged" runat="server" BackColor="#00C000" Font-Bold="True" Font-Names="Arial"
                        Font-Size="Small" ForeColor="Black" Height="25px" Text="Continue" Width="120px" />

                    <asp:Button ID="btnCloseNonPrivileged" runat="server" BackColor="DimGray" Font-Bold="True"
                        Font-Names="Arial" Font-Size="Small" ForeColor="White" Height="25px" Text="Cancel"
                        Visible="true" Width="120px" ToolTip="Return " />

                </td>
                <td style="width: 3px">
                </td>
            </tr>
        </table>
        <br />
        </asp:Panel> 

        <ajaxToolkit:ModalPopupExtender ID="ModalPrivilegedPop" runat="server" 
            TargetControlID="HdBtnPrivileged" PopupControlID="pnlNonPrivileged" 
            BackgroundCssClass="ModalBackground" CancelControlID="btnCloseNonPrivileged">
            </ajaxToolkit:ModalPopupExtender>

            <asp:Button ID="HdBtnPrivileged" runat="server" style="display:none" />

	<%-- =================== Comments ============================================================================--%>

<%--	<asp:Panel ID="pnlComment" runat="server" Height="250px" Width="275px"  BackColor="#FFFFCC" style="display:none" >
        <table  id="tblComments" runat="server" style="border: medium groove #000000; height:100%"  align="center" width="100%" >
            <tr>
                <td style="width: 3px">
                </td>
                <td style="font-weight: bold; font-size: larger; text-align: center;" align="center">
                    Add Comment</td>
                <td style="width: 3px">
                </td>
            </tr>
            <tr>
                <td style="width: 3px; height: 151px;">
                </td>
                <td align="center" style="height: 150px" >
                    <asp:TextBox ID="txtComment" runat="server" Height="100px" 
                    TextMode="MultiLine" Width="250px" MaxLength="75"></asp:TextBox>

                </td>
                <td style="width: 3px; height: 111px;">
                </td>
            </tr>
            <tr>
                <td style="width: 3px">
                </td>
                <td align="center" >
                    <asp:Button ID="btnUpdComment" runat="server" BackColor="#00C000" Font-Bold="True" Font-Names="Arial"
                        Font-Size="Small" ForeColor="Black" Height="25px" Text="Add" Width="120px" />

                    <asp:Button ID="btnCancelComment" runat="server" BackColor="DimGray" Font-Bold="True"
                        Font-Names="Arial" Font-Size="Small" ForeColor="White" Height="25px" Text="Cancel"
                        Visible="true" Width="120px" ToolTip="Return " />

                </td>
                <td style="width: 3px">
                </td>
            </tr>
        </table>
        <br />
        </asp:Panel> 

        <ajaxToolkit:ModalPopupExtender ID="CommentsPopup" runat="server" 
            TargetControlID="HdBtnComments" PopupControlID="pnlComment" 
            BackgroundCssClass="ModalBackground" CancelControlID="btnCancelComment">
            </ajaxToolkit:ModalPopupExtender>

            <asp:Button ID="HdBtnComments" runat="server" style="display:none" />--%>



<%--      <ajaxToolkit:ModalPopupExtender ID="CellPhonePopup" runat="server" 
            TargetControlID="HDBtnCellPhone" PopupControlID="PanelCellphone" 
            BackgroundCssClass="ModalBackground" CancelControlID="btnCanCellPhone">
            </ajaxToolkit:ModalPopupExtender>

            <asp:Button ID="HDBtnCellPhone" runat="server" style="display:none" />
--%>


<%--  <asp:Button ID="btnRequestorDummy" Style="display: none" runat="server" Text="Button" />

  <ajaxToolkit:ModalPopupExtender ID="mpeOnBehalfOf" runat="server"   
                  DynamicServicePath="" Enabled="True" TargetControlID="btnSelectOnBehalfOf"   
                PopupControlID="pnlOnBehalfOf" BackgroundCssClass="ModalBackground"   
                DropShadow="true" CancelControlID="btnCloseOnBelafOf">
  </ajaxToolkit:ModalPopupExtender>--%>  

<%--  </div>--%>
</ContentTemplate>
</asp:UpdatePanel>

</asp:Content>

