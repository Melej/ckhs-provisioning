﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="false" CodeFile="NewRequestForService.aspx.vb" Inherits="NewRequestForService"  ViewStateMode="Enabled"%>
<%@ Register TagPrefix="crz" Namespace="Crozer" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
 <asp:UpdatePanel runat="server" UpdateMode ="Conditional" ID= "uplTicket">
  <ContentTemplate>
    <asp:Panel ID="pnlTest" runat="server">

    </asp:Panel>
    <asp:Table ID="Table1" runat="server" BorderWidth="1" Width="100%" CellPadding = "3">
       <asp:TableRow>
            <asp:TableCell ColumnSpan = "4" CssClass="tableRowHeader">
                New Request For Service
            </asp:TableCell>
       </asp:TableRow>

       <asp:TableRow  CssClass="tableRowSubHeader">
            <asp:TableCell  ColumnSpan="4">
                Ticket Requestor
            </asp:TableCell>
       </asp:TableRow>

       <asp:TableRow>
              <asp:TableCell CssClass="tableCellDisplay">
                    Search For user
              </asp:TableCell>
              <asp:TableCell ColumnSpan="3">
                <asp:Button runat="server" ID="btnSearch" Text="Search" />
              </asp:TableCell>
       </asp:TableRow>

       <asp:TableRow  CssClass="tableRowSubHeader">
            <asp:TableCell  ColumnSpan="4">
                Demographics
           </asp:TableCell>
       </asp:TableRow>

       <asp:TableRow runat="server" ID="rwClientInfo">
           <asp:TableCell CssClass="tableCellDisplay">
               Name 
            </asp:TableCell>

            <asp:TableCell >  
                <asp:Label runat="server" ID ="lblRequestorClientNum" Visible="false"></asp:Label>
                <asp:Label runat="server" ID="lblRequestorFullName"></asp:Label>
            </asp:TableCell>    
            <asp:TableCell  CssClass="tableCellDisplay">
                Department
           </asp:TableCell>
           <asp:TableCell >       
                <asp:Label runat="server" ID="lblRequestorDepartmentName"></asp:Label>
            </asp:TableCell> 
       </asp:TableRow>

       <asp:TableRow>
            <asp:TableCell  CssClass="tableCellDisplay">
                Phone
            </asp:TableCell>

            <asp:TableCell >
                <asp:Label runat="server" ID="lblRequestorPhone"></asp:Label>
            </asp:TableCell>
             <asp:TableCell  CssClass="tableCellDisplay">
                Email
             </asp:TableCell>
             <asp:TableCell>
                <asp:Label runat="server" ID="lblRequestorEmail"></asp:Label>
            </asp:TableCell>
       </asp:TableRow>
       <asp:TableRow>
             <asp:TableCell  CssClass="tableCellDisplay">
                   Entity 
              </asp:TableCell>

              <asp:TableCell >
                    <asp:Label runat="server" ID ="lblRequestorEntityName"></asp:Label>
               </asp:TableCell>
        
               <asp:TableCell  CssClass="tableCellDisplay">
                    Location
                </asp:TableCell>
                <asp:TableCell >
                    <asp:Label runat="server" ID="lblRequestorFacilityName"></asp:Label>
                </asp:TableCell>
       </asp:TableRow>
       
       <asp:TableRow>
           <asp:TableCell CssClass="tableCellDisplay">
                Alternate Contact
            </asp:TableCell>
            <asp:TableCell>
                <asp:TextBox runat="server" ID = "txtAlternativeContact"></asp:TextBox>
            </asp:TableCell>
            <asp:TableCell CssClass="tableCellDisplay">
              Alternate Phone
            </asp:TableCell>

            <asp:TableCell>
                <asp:TextBox runat="server" ID = "txtAlternativePhone"></asp:TextBox>
            </asp:TableCell>
                                      
       </asp:TableRow>

       <asp:TableRow >
            <asp:TableCell>
                <asp:button text="Update Info" runat="server" ID="btnUpdate" Visible="false" />
            </asp:TableCell>
                                   
       </asp:TableRow>
       
       <asp:TableRow  CssClass="tableRowSubHeader">
            <asp:TableCell  ColumnSpan="4">
                Ticket Status
            </asp:TableCell>
       </asp:TableRow>

       <asp:TableRow >
            <asp:TableCell  CssClass="tableCellDisplay">
                Priority
            </asp:TableCell>
            <asp:TableCell>
                <asp:RadioButtonList runat="server" ID="rblPriority" RepeatDirection="Horizontal">
                            <asp:ListItem>1</asp:ListItem>      
                            <asp:ListItem>2</asp:ListItem>      
                            <asp:ListItem>3</asp:ListItem>      
                            </asp:RadioButtonList>
            </asp:TableCell>
            <asp:TableCell ColumnSpan="2">
                <asp:Label runat="server" ID="lblChangePriority"></asp:Label>
            </asp:TableCell>
       </asp:TableRow>

       <asp:TableRow>
            <asp:TableCell CssClass="tableCellDisplay">
            Desired Completion Date
            </asp:TableCell>
            <asp:TableCell ColumnSpan="3">
                <asp:TextBox runat="server" ID="txRequestedCompleteDate" CssClass="calenderClass"  ></asp:TextBox>
            </asp:TableCell>
       </asp:TableRow>

       <asp:TableRow>
            <asp:TableCell  CssClass="tableCellDisplay">
                        Status 
                </asp:TableCell>
                <asp:TableCell>
                    <asp:DropDownList runat="server" ID ="ddlStatus"></asp:DropDownList>
                </asp:TableCell>
                <asp:TableCell ColumnSpan ="2">
                </asp:TableCell>
       </asp:TableRow>

       <asp:TableRow>
            <asp:TableCell  CssClass="tableCellDisplay">
                      Assign Group 
            </asp:TableCell>
                                        
             <asp:TableCell ColumnSpan ="3" >
                <asp:DropDownList runat="server" ID="ddlChangeGroup" AutoPostBack = "true"></asp:DropDownList>
             </asp:TableCell>
       </asp:TableRow>

       <asp:TableRow>
            <asp:TableCell  CssClass="tableCellDisplay">
                      Assign Tech 
             </asp:TableCell>

             <asp:TableCell ColumnSpan="3" >
                <asp:DropDownList runat="server" ID="ddlChangeTech"></asp:DropDownList>
              </asp:TableCell>
       </asp:TableRow>

       <asp:TableRow>
            <asp:TableCell ColumnSpan="4" CssClass="tableCellDisplay" >
                <span style="display:inline-block">
                    Category <asp:DropDownList runat="server" ID="ddlCategoryTypeCd"></asp:DropDownList>
                    Type  <asp:DropDownList runat="server" ID="ddlTypeCd"></asp:DropDownList>
                    Item  <asp:DropDownList runat="server" ID="ddlItemCd"></asp:DropDownList>
                </span>
          </asp:TableCell>
       </asp:TableRow>

       <asp:TableRow  CssClass="tableRowSubHeader">
            <asp:TableCell  ColumnSpan="4">
                Description / Explanation
            </asp:TableCell>
       </asp:TableRow>

       <asp:TableRow >
            <asp:TableCell ColumnSpan="4">
                <asp:TextBox runat="server" ID="txtServiceDesc" Width="95%" TextMode="MultiLine" Rows = "3"></asp:TextBox>
            </asp:TableCell>
       </asp:TableRow>

       <asp:TableRow  CssClass="tableRowSubHeader">
            <asp:TableCell  ColumnSpan="4">
                Business Justification / Reason for Request
            </asp:TableCell>
       </asp:TableRow>

       <asp:TableRow>
            <asp:TableCell ColumnSpan="4">
                <asp:TextBox runat="server" ID ="txtBusinessDesc" TextMode="MultiLine"  Width="95%" Rows ="3"></asp:TextBox>
            </asp:TableCell>
       </asp:TableRow>

       <asp:TableRow  CssClass="tableRowSubHeader">
            <asp:TableCell ColumnSpan="4">
                List Any Known Departments or Systems This Change Will Impact, or any Deadlines That Could Affect This Request
            </asp:TableCell>
       </asp:TableRow>

       <asp:TableRow>
            <asp:TableCell ColumnSpan="4">
                <asp:TextBox runat="server" ID="txtImpactDesc" TextMode="MultiLine"  Width="95%" Rows="4"></asp:TextBox>
            </asp:TableCell>
       </asp:TableRow>

       <asp:TableRow  CssClass="tableRowSubHeader">
          <asp:TableCell ColumnSpan="4">
               Technical Services
                </asp:TableCell>
       </asp:TableRow>

       <asp:TableRow>
            <asp:TableCell ColumnSpan="4">
                 <asp:Table runat="server" >
                     <asp:TableRow>
                        <asp:TableCell>
                            Technical Service
                        </asp:TableCell>
                        <asp:TableCell>
                            Number of Devices
                        </asp:TableCell>
                        <asp:TableCell>
                            Device IDs
                        </asp:TableCell>
                        <asp:TableCell>
                            Description
                        </asp:TableCell>
                        <asp:TableCell>
                            Submit
                        </asp:TableCell>
                    </asp:TableRow>

                    <asp:TableRow VerticalAlign="Top">
                        <asp:TableCell>
                            <asp:DropDownList runat="server" ID="ddlTechService"></asp:DropDownList>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" ID = "txtNumTechDevices"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" ID="txtTechDeviceIDs"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell>
                        <asp:TextBox runat="server" ID ="txtTechServiceDesc" TextMode="MultiLine"></asp:TextBox>

                        </asp:TableCell>
                        <asp:TableCell>

                        <asp:Button runat="server" ID ="btnAddTechService" Text="Add Tech Service" />

                        </asp:TableCell>
                      </asp:TableRow>
                    </asp:Table>
            </asp:TableCell>
       </asp:TableRow>

        <asp:TableRow>
         <asp:TableCell ColumnSpan = "4">
            <asp:GridView ID="gvTechServices" runat="server" AllowSorting="True" 
                AutoGenerateColumns="False" BackColor="White" BorderColor="#336666"
                BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                EmptyDataText="" EnableTheming="False" DataKeyNames="SeqNum"
                Font-Size="Medium" GridLines="Horizontal" Width="100%">
                                   
                    <FooterStyle BackColor="White" ForeColor="#333333" />
                    <HeaderStyle BackColor="#336666" Font-Bold="False" ForeColor="White" Font-Size="Small" HorizontalAlign="Left"    />                                    
                    <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Left" Font-Size="Medium"/>
                    <RowStyle BackColor="White" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#F7F7F7" />
                    <SortedAscendingHeaderStyle BackColor="#487575" />
                    <SortedDescendingCellStyle BackColor="#E5E5E5" />
                    <SortedDescendingHeaderStyle BackColor="#275353" />

                    <Columns>
                                                                                            
                                <asp:CommandField ButtonType="Button" SelectText="Remove" ShowSelectButton= "true" />
                                <asp:BoundField  DataField="TechServiceDesc"  HeaderText="" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
				                <asp:BoundField  DataField="NumOfDevices"  HeaderText="" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
                                <asp:BoundField  DataField="DeviceID"  HeaderText="" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
                                <asp:BoundField  DataField="DetailInformation"  HeaderText="" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
                                  
                    </Columns>

                </asp:GridView>
          </asp:TableCell>
        </asp:TableRow>                

        <asp:TableRow>
            <asp:TableCell ColumnSpan = "4">
                <asp:Button runat="server" ID="btnSubmit" Text="Submit" />
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>

      <ajaxToolkit:ModalPopupExtender ID="mpeSearchClient" runat="server"   
        DynamicServicePath="" Enabled="True" TargetControlID="btnSearch"   
        PopupControlID="pnlSearchClient" BackgroundCssClass="ModalBackground"   
         DropShadow="true" >  
      </ajaxToolkit:ModalPopupExtender>  
    <asp:Panel ID="pnlSearchClient" runat="server" style="display:none; background-color:#efefef;  font-size:large;  width:600px;  border: 2px solid #FFFFCC;">
        <asp:UpdatePanel ID = "uplSearchClient" runat="server" UpdateMode="Conditional" >
            <ContentTemplate>
    
                <table  width="100%" border="1px" >
                    <tr>
                        <th class="tableRowHeader" colspan = "4">
                        Search for User
                        </th>
                    </tr>
                    <tr>
                    <td>
                        First Name <asp:TextBox ID = "FirstNameTextBox" runat = "server"></asp:TextBox>
                    </td>
                    <td>
                        Last Name <asp:TextBox ID = "LastNameTextBox" runat = "server"></asp:TextBox>
                            <asp:Button ID ="btnSearchClients" runat = "server" Text ="Search"  />
                    </td>
                    </tr>
                <tr>
                    <td colspan = "4">
                    <div class="scrollContentContainer" >
                        <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="200" AssociatedUpdatePanelID="uplSearchClient"  >
                        <ProgressTemplate>
                            <div id="IMGDIV" align="center" valign="middle" runat="server" style="visibility:visible;vertical-align:middle;">
                            Loading Data ...
                            </div>
                        </ProgressTemplate>
                        </asp:UpdateProgress>
                                                                
                        <asp:GridView ID="gvSearch" runat="server" AllowSorting="True" 
                            AutoGenerateColumns="False" BackColor="White" BorderColor="#336666"
                            BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                            EmptyDataText="No Items Found" EnableTheming="False" DataKeyNames="client_num"
                            Font-Size="Medium" GridLines="Horizontal" Width="100%">
                                   
                            <FooterStyle BackColor="White" ForeColor="#333333" />
                            <HeaderStyle BackColor="#336666" Font-Bold="False" ForeColor="White" Font-Size="Small" HorizontalAlign="Left"    />                                    
                            <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Left" Font-Size="Medium"/>
                            <RowStyle BackColor="White" ForeColor="#333333" />
                            <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#F7F7F7" />
                            <SortedAscendingHeaderStyle BackColor="#487575" />
                            <SortedDescendingCellStyle BackColor="#E5E5E5" />
                            <SortedDescendingHeaderStyle BackColor="#275353" />

                            <Columns>
                                <asp:CommandField ButtonType="Button" SelectText="Select" ShowSelectButton= "true" />

                                         
                                    <asp:BoundField  DataField="fullname"  HeaderText="Full Name" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
                    <%--                <asp:BoundField  DataField="login_name"  HeaderText="Login"></asp:BoundField>--%>
                                    <asp:BoundField  DataField="phone"  HeaderText="Phone" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
                                    <asp:BoundField  DataField="department_name"  HeaderText="Department" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
                                  
                            </Columns>
                            </asp:GridView>
                        </div>
                        </td>
                    </tr>
                    <tr>
                            <td colspan = "4" align = "center" >
                            <asp:Button ID = "btnCloseSearchClient" runat = "server" Text="Close" />
                            </td>
                        </tr>
                    </table>
            </ContentTemplate>               
        </asp:UpdatePanel>
    </asp:Panel>

    <ajaxToolkit:ModalPopupExtender ID="mpeUpdateClient" runat="server"   
    DynamicServicePath="" Enabled="True" TargetControlID="btnUpdateDummy"   
    PopupControlID="pnlUpdateClient" BackgroundCssClass="ModalBackground"   
    DropShadow="true" >  
    </ajaxToolkit:ModalPopupExtender>  
                <asp:Panel ID="pnlUpdateClient" runat="server" style="display:none; background-color:#efefef;  font-size:large;  width:600px;  border: 2px solid #FFFFCC;">
                    <table  width="100%" border="1px" >
                        <tr>
                            <th class="tableRowHeader" colspan = "4">
                            <asp:Label runat="server" ID ="lblUpdateClientFullName"></asp:Label>
                            </th>
                        </tr>

                        <tr>
                                                                
                                                                
                        <td colspan="4">
                            <div class="scrollContentContainer" >
                                                                
                                <asp:Table runat="server" ID="tblUpdateClient">
                                    <asp:TableRow>
                                        <asp:TableCell ColumnSpan="2">
                                            <asp:Label runat="server" ID="lblUpdateFullName"></asp:Label>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                                                       
                                <asp:TableRow>
                                <asp:TableCell>
                                    Phone Number
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:TextBox runat="server" ID="txtUpdatePhone"></asp:TextBox>
                                </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                <asp:TableCell>
                                        Location
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:DropDownList runat="server" ID="ddlUpdateFacilityCd"></asp:DropDownList>
                                </asp:TableCell>
                                </asp:TableRow>
                                </asp:Table>
                                                                
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan = "4" align = "center" >
                            <asp:Button ID = "btnUpdateClient" runat = "server" Text="Update" />
                            <asp:Button ID = "btnCloseUpdate" runat = "server" Text="Cancel" />
                       </td>
                     </tr>
                  </table>
                </asp:Panel>
          <asp:Button ID="btnRequestorDummy" Style="display: none" runat="server" Text="Button" />
            <asp:Button ID="btnUpdateDummy" Style="display: none" runat="server" Text="Button" />
    </ContentTemplate>
  </asp:UpdatePanel>
</asp:Content>

  
