﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="True" CodeFile="TestSignOnForm.aspx.vb" Inherits="TestSignOnForm" %>



<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">


<style type="text/css"> 
table
{
border-collapse:collapse;
}

table, th, td
{
border: 1px solid black;
}

fieldset {
border:1px solid black;

}

legend {
  position: relative;
  padding: 0.2em 0.5em;
  border:1px solid black;
  color:black;
  left:25px;
  text-align:left;
  }
.container {width: 960px; margin: 0 auto; overflow: hidden; height:210px;}
 
.tooltiptest {
		
	position:absolute;
	border:1px solid black;
	background-color:blue;
	border-radius:5px;
	padding:10px;
	color:white;
	font-size:12px;
    Font: Arial;
    max-height:30px !important;
}

.tooltiptest:after{
    content:'';
    
    width:0;
    height:0;
    position:absolute;

    border-top: 8px solid transparent;
    border-bottom: 8px solid transparent;
    border-right:8px solid black;
    left:-8px;

    top:7px;
}

</style>

<script type="text/javascript" src="Scripts/FieldValidator.js"></script>

   

<script type="text/javascript">

$(document).ready(function () {
    fieldValidator('textInputRequired', 'btnReturn', 'lblvalidate')
});


$(document).ready(function () {
    // Tooltip only Text


    $('.TextTooltip').focus(function (e) {
        // Hover over code



        var title = $(this).attr('title');
        var maxLen = $(this).attr('maxlength');
        var title = title + ' ' + maxLen

        $(this).data('tipText', title).removeAttr('title');
        $(".tooltiptest").css({ display: 'block' });
        $(".tooltiptest").css({ 'opacity': .1 });
        $('<span class="tooltiptest"></span>')
                .text(title)
                .appendTo('body')
                .animate({ opacity: .9 }, 200);

        var pos = $(this).position();
        var x = (e.pageX + $(this).offset().left)
        var y = (e.pageY + $(this).offset().top)

        if ((x + $(this).outerWidth()) >= $(window).width()) {
            x = e.pageX - $(".tooltiptest").outerWidth();
        }

        if ((y + $(this).outerHeight()) >= $(window).height()) {
            y = e.pageY - $(".tooltiptest").outerHeight();
        }

        var x = $(this).offset().left;
        var y = $(this).offset().top;
        var elH = $(this).height();
        var toolW = $('.tooltiptest').outerWidth();
        var toolH = $('.tooltiptest').outerHeight();
        var windowTop = $(window).scrollTop();
        var relativeTop = (y - windowTop);
        var toolY = (y - (elH / 2) - toolH);

        if (toolY > relativeTop) {
            var toolY = (y + elH)
        }

        var mousex = pos.left + 20; //Get X coordinates
        var mousey = pos.top - 40; //Get Y coordinates
        $('.tooltiptest')
                .css({ top: toolY, left: x })
    });

    $('.TextTooltip').blur(function () {
        // Hover over code


        $(this).attr('title', $(this).data('tipText'));

        $('.tooltiptest').remove();
    });
});

</script>
<a href="TestSignOnForm.aspx">TestSignOnForm.aspx</a>
      
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">


     <asp:Button ID="btnReturn" Text="text" runat="server" CssClass="BtnValidate"/>
     <asp:Label ID="lblvalidate" runat="server">Submit</asp:Label>


 <asp:TextBox ID="tb1" runat="server" CssClass="TextTooltip" alt="Tb1" title="test new tool tip" TextMode = "MultiLine" Rows="3" MaxLength="100"  /><br />
 <asp:TextBox ID="TextBox1" runat="server" CssClass="textInputRequired"  /><br />
 <asp:TextBox ID="TextBox2" runat="server" CssClass="textInputRequired"  /><br />
 <asp:TextBox ID="tb2" runat="server" />

    
  <a href="http://jquery.com/" class="tooltip" alt="tooltip">jQuery</a>
   <asp:Button runat="server" ID="btnTestEmail" Text="Test Email" Visible ="true"/>
   <br />
  
        <table cellpadding="5px">
            <tr>
                <td colspan="2" align="center"  style="font-size:large">
               
                <b>Account Request Complete</b>
                 
                 </td>    
            </tr>
            <tr>
                <td style="font-weight:bold">Name:</td>
                <td>Client Name Requested</td>
            </tr>
            <tr>
                <td style="font-weight:bold">Requested By:</td>
                <td>Requested BY</td>
            </tr>
            <tr>
                <td style="font-weight:bold">Date Requested:</td>
                <td>9/15/2013</td>
            </tr>
      
     
    <tr>
        <td colspan="2">
           <b> Accounts: </b>       
        </td>

    </tr>
    <tr>
     <td colspan="2">
       <ul>
        <li>Email</li>
        <li>eCare</li>
        <li>Network</li>
        <li>MAK</li>
        <li>EMR</li>
      </ul>
     </td>
    
    </tr>

  
    
   </table>

  <%-- OnClientClick="myfunction(); return false;"--%>
</asp:Content>

