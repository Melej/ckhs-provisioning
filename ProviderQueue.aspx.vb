﻿Imports System.IO
Imports System.Data

Imports System.Data.DataSet
Imports System.Data.SqlClient
Imports System.Web.UI.Control
Imports iTextSharp.text.pdf

Imports App_Code
Imports AjaxControlToolkit
Imports C1.Web.UI.Controls.C1Window

Partial Class ProviderQueue
    Inherits MyBasePage '   Inherits System.Web.UI.Page


    Dim sActivitytype As String = ""
    Dim cblBinder As New CheckBoxListBinder
    Dim FormSignOn As New FormSQL()
    Dim AccountRequestNum As Integer
    Dim dt As DataTable


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load



        If Not IsPostBack Then


            '    bindValidateQueue()



        End If
        fillProviderTable()
    End Sub


    Protected Sub fillProviderTable()
        Dim thisData As New CommandsSqlAndOleDb("SelProviderQueue", Session("EmployeeConn"))
        Dim iRowCount = 1

        dt = thisData.GetSqlDataTable



        If dt.Rows.Count > 0 Then




            Dim rwHeader As New TableRow
            Dim clSelectHeader As New TableCell
            Dim clNameHeader As New TableCell
            Dim clRequestTypeHeader As New TableCell
            Dim clRequestorHeader As New TableCell
            Dim clReqPhoneHeader As New TableCell
            Dim clSubmitDateHeader As New TableCell

            clNameHeader.Text = "Name"
            clRequestTypeHeader.Text = "Request Type"
            clSubmitDateHeader.Text = "Submit Date"
            clRequestorHeader.Text = "Requestor"
            clReqPhoneHeader.Text = "Req Phone"

            rwHeader.Cells.Add(clSelectHeader)
            rwHeader.Cells.Add(clNameHeader)
            rwHeader.Cells.Add(clRequestTypeHeader)
            rwHeader.Cells.Add(clRequestorHeader)
            rwHeader.Cells.Add(clReqPhoneHeader)
            rwHeader.Cells.Add(clSubmitDateHeader)


            rwHeader.Font.Bold = True
            rwHeader.BackColor = Color.FromName("#006666")
            rwHeader.ForeColor = Color.FromName("White")
            rwHeader.Height = Unit.Pixel(36)

            tblProviderQueue.Rows.Add(rwHeader)

            For Each drRow As DataRow In dt.Rows

                Dim btnSelect As New Button
                Dim rwData As New TableRow
                Dim clSelectData As New TableCell
                Dim clNameData As New TableCell
                Dim clRequestTypeData As New TableCell
                Dim clRequestorData As New TableCell
                Dim clReqPhoneData As New TableCell
                Dim clSubmitDateData As New TableCell

                Dim sFirstName As String = IIf(IsDBNull(drRow("first_name")), "", drRow("first_name"))
                Dim sLastName As String = IIf(IsDBNull(drRow("last_name")), "", drRow("last_name"))


                btnSelect.Text = "Select"
                btnSelect.CommandName = "Select"
                btnSelect.CommandArgument = drRow("account_request_num") & ";" & sFirstName & ";" & sLastName
                ' btnSelect.Width = 50
                btnSelect.EnableTheming = False

                AddHandler btnSelect.Click, AddressOf btnSelect_Click







                clSelectData.Controls.Add(btnSelect)
                clNameData.Text = IIf(IsDBNull(drRow("receiver_full_name")), "", drRow("receiver_full_name"))
                clRequestTypeData.Text = IIf(IsDBNull(drRow("account_request_type")), "", drRow("account_request_type"))
                clSubmitDateData.Text = IIf(IsDBNull(drRow("entered_date")), "", drRow("entered_date"))
                clRequestorData.Text = IIf(IsDBNull(drRow("requestor_name")), "", drRow("requestor_name"))
                clReqPhoneData.Text = IIf(IsDBNull(drRow("requestor_phone")), "", drRow("requestor_phone"))




                rwData.Cells.Add(clSelectData)
                rwData.Cells.Add(clNameData)
                rwData.Cells.Add(clRequestTypeData)
                rwData.Cells.Add(clRequestorData)
                rwData.Cells.Add(clReqPhoneData)
                rwData.Cells.Add(clSubmitDateData)

                If iRowCount > 0 Then

                    rwData.BackColor = Color.Bisque

                Else

                    rwData.BackColor = Color.LightGray

                End If

                tblProviderQueue.Rows.Add(rwData)

                iRowCount = iRowCount * -1

            Next



        Else
            Dim rwNoData As New TableRow
            Dim clNoData As New TableCell

            clNoData.Text = "No Pending Requests Found"
            rwNoData.Cells.Add(clNoData)

            tblProviderQueue.Rows.Add(rwNoData)


        End If


    End Sub



  

    Sub btnSelect_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim btnLinkView As Button = sender

        If btnLinkView.CommandName = "Select" Then




            Dim sArguments As String() = Split(btnLinkView.CommandArgument, ";")
            Session("AccountRequestNum") = sArguments(0)

            Response.Redirect("./ProviderRequest.aspx?RequestNum=" & Session("AccountRequestNum"))



        End If



    End Sub
    Protected Sub BtnExcel_Click(ByVal sender As Object, e As System.EventArgs) Handles BtnExcel.Click
        CreateExcel(dt)
    End Sub
    Protected Sub CreateExcel(ByVal ReportTable As DataTable)
        Dim gvData As GridView


        Session("UCCurrentMethod") = "CreateDocOrExcel()"
        ' GridView1.Visible = True
        'doesnt work with gridviews that have controls in them
        Response.ClearContent()
        Dim attachment As String = "attachment; filename=ExcelDownload.xls"

        HttpContext.Current.Response.ContentType = "application/excel"
        HttpContext.Current.Response.AddHeader("Content-disposition", attachment)


        Dim strStyle As String = "<style>.text {mso-number-format:\@; } </style>"   ' to help retain leading zeroes
        Response.Write(strStyle)
        Response.Write("<H4 align='center'>")  ' throw in a pageheader just for the heck of it to tell people what the data is and when it was run
        '  Response.Write("Parts Listing as of " & Date.Now.ToString("d"))
        Response.Write(Session("Hospital") & " Room List")
        Response.Write("</H4>")
        Response.Write("<BR>")
        'GridView1.HeaderStyle.Font.Bold = True
        'GridView1.HeaderStyle.BackColor = Drawing.Color.LightGray

        Dim sw As StringWriter = New StringWriter()
        Dim hw As HtmlTextWriter = New HtmlTextWriter(sw)
        Dim dg As New DataGrid


        ' must be name of datasource on page
        dg.DataSource = ReportTable
        Dim i As Integer = dg.Items.Count
        dg.DataBind()
        '  dg.GridLines = GridLines.None
        ' ClearControls(dg)

        dg.HeaderStyle.Font.Bold = True
        dg.HeaderStyle.BackColor = Drawing.Color.LightGray


        dg.RenderControl(hw)
        '   Dim dt As New DataTable
        ' Dim dv As New DataView

        'dv.Table.ToString()
        Response.Write(sw.ToString())
        Response.End()

        ' GridView1.AllowSorting = True
        sw = Nothing
        hw = Nothing
        dg.Dispose()
    End Sub
End Class
