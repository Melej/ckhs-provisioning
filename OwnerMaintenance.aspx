﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="false" CodeFile="OwnerMaintenance.aspx.vb" Inherits="OwnerMaintenance" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">


<asp:UpdatePanel ID="uplAppMaintenance" runat ="server" >

<Triggers>
   
   <asp:AsyncPostBackTrigger ControlID="Applicationsddl" EventName ="SelectedIndexChanged" />

</Triggers>

<ContentTemplate>
<table class="wrapper" style="border-style: groove" align="left">
<tr>
<td>




      <table width="100%" border ="3">
    <tr>
        <th colspan="2" align ="center" class="tableRowHeader">
        
            Provisioning Accounts Ownership 
        </th>

    </tr>
    <tr>
        <td colspan="2" align = "center">
            <asp:Button ID="btnReturn" runat="server" Text = "Return to Main Screen"  Width="175px"/>
        </td>
    </tr>
    <tr>
        <td>
                Select Application
        </td>
        <td>
            <asp:DropDownList ID = "Applicationsddl" runat = "server" AutoPostBack="true">
            </asp:DropDownList>
        </td>
        
    </tr>
   <tr>
        <td>
                Add Viewer/Owner
        </td>
        <td>
             <asp:DropDownList ID = "Technicianddl" runat = "server" Visible="false">
            </asp:DropDownList>
              <asp:Button ID = "btnAddOwner" runat="server" Text= "Add Owner" Visible="false"
               CssClass="btnhov" BackColor="#006666"  Width="175px" />
        </td>
        
    </tr>
    <tr>
        <td colspan = "2">
        Current Owners
        </td>
    </tr>
    <tr>
        <td colspan = "2">
            
         <asp:GridView ID="gvAppOwners" runat="server" AllowSorting="True" 
                                    AutoGenerateColumns="False" BackColor="White" BorderColor="#336666"
                                    BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                                    EmptyDataText="No Owners" EnableTheming="False" DataKeyNames="tech_num, AppBase"
                                    Font-Size="Medium" GridLines="Horizontal" Width="100%">
                                   
                                    <FooterStyle BackColor="White" ForeColor="#333333" />
                                    <HeaderStyle BackColor="#336666" Font-Bold="False" ForeColor="White" Font-Size="Small" HorizontalAlign="Left"    />                                    
                                    <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Left" Font-Size="Medium"/>
                                    <RowStyle BackColor="White" ForeColor="#333333" />
                                    <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                    <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                    <SortedAscendingHeaderStyle BackColor="#487575" />
                                    <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                    <SortedDescendingHeaderStyle BackColor="#275353" />

                                    <Columns>
                                    
                                          <asp:CommandField ButtonType="Button" SelectText="Remove" ShowSelectButton= "true" />

                                          <asp:BoundField  DataField="Owner"  HeaderText="Viewer/Owner" HeaderStyle-HorizontalAlign ="Left"></asp:BoundField>
				                          <asp:TemplateField HeaderText="Receive Email Alert/Add " ItemStyle-HorizontalAlign="Center">
                                            
                                            <ItemTemplate>
                                                 <asp:CheckBox ID="ckbAcctEmail" runat="server" AutoPostBack="true" 
                                                               OnCheckedChanged="ckbAcctEmail_OnCheckedChanged"
                                                               Checked= '<%# Convert.ToBoolean(Eval("ReceiveNewAcctEmail")) %>'
                                                                 />
                                            </ItemTemplate>
                                          
                                          </asp:TemplateField>
                                          




                                  
                                  </Columns>

                                </asp:GridView>
		

	
        </td>
    </tr>
  
   </table>





</td>

</tr>

</table>

</ContentTemplate>


        </asp:UpdatePanel>


</asp:Content>

