﻿Imports System.IO
Imports System.Data '
Imports System.Data.SqlClient
Imports App_Code
Imports AjaxControlToolkit
'Imports Microsoft.Office.Interop.Excel
Imports C1.Web.UI.Controls.C1Window
'Imports Microsoft.Office.Interop

Partial Class SearchEmployeeVaccine
    Inherits MyBasePage '   Inherits System.Web.UI.Page

    Dim conn As New SetDBConnection()
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim tp As New ToolTipHelper()
        EmployeeNumberTextBox.Focus()

        lblValidation.Attributes.Add("class", "masterTooltip")
        lblValidation.Attributes.Add("title", tp.ToolTipHtml())



        If Not IsPostBack Then

            Dim ddlBinder As New DropDownListBinder
            ddlBinder.BindData(entity_cdddl, "selEntityCodesV4", "entity_cd", "hosp_desc", Session("EmployeeConn"))



        End If
        If Session("SecurityLevelNum") < 25 Then
            Response.Redirect("~/Security.aspx")
        End If

        If Session("SecurityLevelNum") < 40 Then
            Response.Redirect("~/MyRequests.aspx")

        End If




    End Sub
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        lblValidation.Text = "Test"
        gvVaccineSearch.Visible = True
        gvVaccineSearch.SelectedIndex = -1

        lblValidation.Visible = False


        Dim thisData As New CommandsSqlAndOleDb("SelClientWithVaccines", Session("VaccineConn"))


        If EmployeeNumberTextBox.Text <> "" Then
            thisData.AddSqlProcParameter("@employeenumber", EmployeeNumberTextBox.Text, SqlDbType.NVarChar, 20)

        ElseIf LastNameTextBox.Text <> "" Then
            ' thisData.AddSqlProcParameter("@FirstName", FirstNameTextBox.Text, SqlDbType.NVarChar, 20)
            thisData.AddSqlProcParameter("@LastName", LastNameTextBox.Text, SqlDbType.NVarChar, 20)
            'thisData.AddSqlProcParameter("@System", rblSystem.SelectedValue, SqlDbType.NVarChar, 20)
            'thisData.AddSqlProcParameter("@NtLogin", NtLoginTextBox.Text, SqlDbType.NVarChar, 20)
            entity_cdLabel.Text = ""
        ElseIf entity_cdLabel.Text <> "" Then
            thisData.AddSqlProcParameter("@entity_cd", entity_cdLabel.Text, SqlDbType.NVarChar, 8)
            thisData.AddSqlProcParameter("@department_cd", department_cdLabel.Text, SqlDbType.NVarChar, 8)
        ElseIf VaccineNumTextBox.Text <> "" Then
            thisData.AddSqlProcParameter("@VaccineNum", VaccineNumTextBox.Text, SqlDbType.NVarChar, 8)

        ElseIf VaccineLotNumTextBox.Text <> "" Then
            thisData.AddSqlProcParameter("@InVaccineLotNum", VaccineLotNumTextBox.Text, SqlDbType.NVarChar, 8)


        ElseIf sixmonths.SelectedIndex > -1 Then

            thisData.AddSqlProcParameter("@sixmonth", "yes", SqlDbType.NVarChar, 3)


        End If

        gvVaccineSearch.DataSource = thisData.GetSqlDataTable
        gvVaccineSearch.DataBind()

        Dim rowsfound As Integer
        Dim client_num As String

        rowsfound = gvVaccineSearch.Rows.Count


        If EmployeeNumberTextBox.Text <> "" Then

            If rowsfound = 1 Then
                For i = 0 To gvVaccineSearch.Rows.Count - 1
                    If gvVaccineSearch.Rows(i).Cells(2).Text <> "" Then
                        gvVaccineSearch.SelectedIndex = i
                        client_num = gvVaccineSearch.SelectedRow.Cells(2).Text
                        Exit For
                    End If
                Next

                If client_num <> "" Then
                    Response.Redirect("~/EmployeeVaccine.aspx?ClientNum=" & client_num, True)

                End If

                'client_num = gvSearch.SelectedRow.Cells(0).Text
                'client_num = gvSearch.SelectedRow.Cells("client_num").ToString
            End If

        End If



    End Sub
    Protected Sub gvVaccineSearch_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles gvVaccineSearch.SelectedIndexChanged
        Dim client_num As String
        Dim VaccineNum As String

        gvVaccineSearch.SelectedIndex = 1

        client_num = gvVaccineSearch.SelectedRow.Cells(0).Text
        client_num = gvVaccineSearch.SelectedRow.Cells("client_num").ToString

        VaccineNum = gvVaccineSearch.SelectedRow.Cells(0).Text
        VaccineNum = gvVaccineSearch.SelectedRow.Cells("VaccineNum").ToString

        Response.Redirect("~/EmployeeVaccine.aspx?ClientNum=" & client_num & "&Vaccinenum=" & VaccineNum, True)

    End Sub

    Protected Sub gvVaccineSearch_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvVaccineSearch.RowCommand
        If e.CommandName = "Select" Then

            Dim client_num As String = gvVaccineSearch.DataKeys(Convert.ToInt32(e.CommandArgument))("client_num").ToString()
            Dim VaccineNum As String = gvVaccineSearch.DataKeys(Convert.ToInt32(e.CommandArgument))("VaccineNum").ToString()

            Response.Redirect("~/EmployeeVaccine.aspx?ClientNum=" & client_num & "&VaccineNum=" & VaccineNum, True)

        End If

    End Sub

    Protected Sub gvVaccineSearch_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvVaccineSearch.RowDataBound


        If (e.Row.RowType = DataControlRowType.DataRow) Then  ' Check to make sure it isn't not getting header or footer

            '   Dim drv As DataRowView = CType(e.Row.Cells, DataRowView)

            Dim ClientNum As String = gvVaccineSearch.DataKeys(e.Row.RowIndex)("client_num")
            Dim lblUserName As Label

            lblUserName = e.Row.FindControl("lblLoginName")
            Dim accts As New ClientAccounts(ClientNum)
            If accts.AccountsByAcctNum(9).Count > 1 Then
                Dim sAccts As String = ""

                For Each acct As ClientAccountAccess In accts.AccountsByAcctNum(9)

                    sAccts = sAccts + acct.LoginName + "<br>"


                Next

                lblUserName.Attributes.Add("class", "masterTooltip")
                lblUserName.Attributes.Add("title", sAccts)
                lblUserName.ForeColor = Color.Blue
            End If

        End If

    End Sub
    Public Sub Reset()
        ' clear out all fields
        EmployeeNumberTextBox.Text = ""
        EmployeeNumberTextBox.Focus()

        Dim emptytbl As New System.Data.DataTable

        gvVaccineSearch.Visible = False
        gvVaccineSearch.SelectedIndex = -1

        gvVaccineSearch.DataSource = emptytbl
        gvVaccineSearch.DataBind()

        VaccineNumTextBox.Text = ""

        LastNameTextBox.Text = ""
        VaccineLotNumTextBox.Text = ""

        entity_cdddl.SelectedIndex = -1
        department_cdddl.SelectedIndex = -1
        sixmonths.SelectedIndex = -1


    End Sub
    Protected Sub btnReset_Click(sender As Object, e As System.EventArgs) Handles btnReset.Click
        Reset()
    End Sub
    Protected Sub entity_cdddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles entity_cdddl.SelectedIndexChanged
        Dim ddbDepartmentBinder As New DropDownListBinder(department_cdddl, "selDepartmentCodesV4", Session("EmployeeConn"))
        ddbDepartmentBinder.AddDDLCriteria("@entity_cd", entity_cdddl.SelectedValue, SqlDbType.NVarChar)
        ddbDepartmentBinder.BindData("department_cd", "department_name")

        entity_cdLabel.Text = entity_cdddl.SelectedValue.ToString
    End Sub
    Protected Sub department_cdddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles department_cdddl.SelectedIndexChanged
        department_cdLabel.Text = department_cdddl.SelectedValue.ToString
    End Sub

    Protected Sub btnExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExcel.Click
        CreateExcel2()
    End Sub
    Sub CreateExcel2()



        Response.AddHeader("content-disposition", "attachment;filename=FileName.xls")

        Response.Charset = String.Empty

        Response.ClearContent()
        Response.ContentType = "application/vnd.ms-excel"
        'Response.ContentType = "application/vnd.xls"


        Dim sw As System.IO.StringWriter = New System.IO.StringWriter()

        Dim hw As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(sw)

        '===============================Create form to contain grid
        Dim frm As New HtmlForm()

        Dim thisData As New CommandsSqlAndOleDb("SelClientWithVaccines", Session("VaccineConn"))


        If EmployeeNumberTextBox.Text <> "" Then
            thisData.AddSqlProcParameter("@employeenumber", EmployeeNumberTextBox.Text, SqlDbType.NVarChar, 20)

        ElseIf LastNameTextBox.Text <> "" Then
            ' thisData.AddSqlProcParameter("@FirstName", FirstNameTextBox.Text, SqlDbType.NVarChar, 20)
            thisData.AddSqlProcParameter("@LastName", LastNameTextBox.Text, SqlDbType.NVarChar, 20)
            'thisData.AddSqlProcParameter("@System", rblSystem.SelectedValue, SqlDbType.NVarChar, 20)
            'thisData.AddSqlProcParameter("@NtLogin", NtLoginTextBox.Text, SqlDbType.NVarChar, 20)
            entity_cdLabel.Text = ""
        ElseIf entity_cdLabel.Text <> "" Then
            thisData.AddSqlProcParameter("@entity_cd", entity_cdLabel.Text, SqlDbType.NVarChar, 8)
            thisData.AddSqlProcParameter("@department_cd", department_cdLabel.Text, SqlDbType.NVarChar, 8)
        ElseIf sixmonths.SelectedIndex > -1 Then

            thisData.AddSqlProcParameter("@sixmonth", "yes", SqlDbType.NVarChar, 3)


        End If

        Dim ClientVaccineDT As System.Data.DataTable

        Dim ds As System.Data.DataSet

        ClientVaccineDT = thisData.GetSqlDataTable
        ds = thisData.GetSqlDataset

        'Create StringWriter and use to create CSV
        'Using sw As New StringWriter()
        Using htw As New HtmlTextWriter(sw)
            'Instantiate DataGrid
            Dim dg As New DataGrid()
            dg.DataSource = ds.Tables(0)
            dg.DataBind()
            dg.RenderControl(htw)
            Response.Write(sw.ToString())
            Response.[End]()
        End Using
        'End Using


        Response.End()
        Exit Sub


        'gvVaccineSearch.DataSource = thisData.GetSqlDataTable
        'gvVaccineSearch.DataBind()

        'gvVaccineSearch.Columns.RemoveAt(0)
        'gvVaccineSearch.Parent.Controls.Add(frm)
        'frm.Attributes("RunAt") = "server"
        'frm.Controls.Add(gvVaccineSearch)

        'frm.RenderControl(hw)
        'Response.Write(sw.ToString())

        'Response.End()

    End Sub

End Class
