﻿Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports App_Code
Imports AjaxControlToolkit
Imports C1.Web.UI.Controls.C1Window
Imports System.Net
Imports System.Net.Mail
Imports System.Net.Mail.SmtpClient

Partial Class SignOnFormFinanical
    Inherits MyBasePage '   Inherits System.Web.UI.Page
    Dim sActivitytype As String = ""
    Dim FormSignOn As FormSQL
    Dim ReactivateForm As FormSQL
	Dim VendorFormSignOn As FormSQL
	Dim PMHSignon As FormSQL

	Dim AccountRequestNum As Integer
    Dim Cred As String
    Dim Provider As String
    Dim Han As String
    Dim sEmployeeNum As String
    Dim bHasError As Boolean
    Dim thisRole As String
    Dim mainRole As String

    Dim ReactivateClientNum As Integer
    Dim ReHireRequestNum As Integer
	Dim PMHRequestNum As Integer

	Dim Requesttype As String
	Dim sclose As String
	Dim sItemsadded As String


	Dim Searched As Boolean = False
    Private dtApplications As New DataTable
    Public dtTCLUserTypes As New DataTable

    Dim Mailobj As System.Net.Mail.SmtpClient = New System.Net.Mail.SmtpClient
    Dim Mailmsg As New System.Net.Mail.MailMessage
    Dim MailAddress As System.Net.Mail.MailAddress
    Dim mailAttachment As System.Net.Mail.Attachment

	Dim AccountRequest As AccountRequest


	Dim dtAdjustedRoles As DataTable
    Dim pageCtrl As New PageControls()
    Delegate Function getgridcellNoE(ByVal sFieldName As String, ByRef gridview1 As GridView) As Integer

    Dim getcellNoE As New getgridcellNoE(AddressOf GridViewFunctionsCls.GetGridCellIndexNoE)
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'Dim Envoirn As New Environment()
        'thisenviro.Text = Envoirn.getEnvironment
        'testing managers accounts
        'Session("LoginID") = "defs00"
        If lblSearched.Text = "" Then
            SearchPopup.Show()
        End If
        FormSignOn = New FormSQL(Session("EmployeeConn"), "SelAccountRequestV20", "InsAccountRequestV20", "", Page)
        PMHSignon = New FormSQL(Session("EmployeeConn"), "SelAccountRequestV20", "UpdFullAccountRequestV20", "", Page)


        Dim UserInfo = New UserSecurity(Session("LoginID"), Session("EmployeeConn"))


        'If Session("SecurityLevelNum") = 50 Then
        '    rblAffiliateBinder.AddDDLCriteria("@type", "phys", SqlDbType.NVarChar)

        'ElseIf Session("SecurityLevelNum") = 40 Or Session("SecurityLevelNum") = 45 Then
        '    rblAffiliateBinder.AddDDLCriteria("@type", "emp", SqlDbType.NVarChar)
        'End If

        If Session("SecurityLevelNum") > 39 And Session("SecurityLevelNum") < 60 Then
            'cblApplications.Enabled = False
            btnSelectOnBehalfOf.Visible = False
        End If

        
        If Request.QueryString("EmpRoleNum") <> "" Then
            'emp_type_cdrbl.SelectedValue = Request.QueryString("EmpRoleNum")
            HDEmpRoleNum.Text = Request.QueryString("EmpRoleNum").ToString

        End If

        If Request.QueryString("MainRolenum") <> "" Then
            HdMainrole.Text = Request.QueryString("MainRolenum").ToString
            lblSearched.Text = "TRUE"
            SearchPopup.Hide()

        End If

		If Request.QueryString("ClientNum") <> "" Then
			ReactivateClientNum = Request.QueryString("ClientNum")
			ReactivateClientNumTextBox.Text = Request.QueryString("ClientNum")
		End If

		'If Request.QueryString("ReHireRequestNum") <> "" Then
		'	ReHireRequestNum = Request.QueryString("ReHireRequestNum")
		'	RehireRequestNumTextBox.Text = Request.QueryString("ReHireRequestNum")
		'End If

		If Request.QueryString("AccountRequestNum") <> "" Then
			PMHRequestNum = Request.QueryString("AccountRequestNum")
			PMHRequestNumTextBox.Text = Request.QueryString("AccountRequestNum")
			emp_type_cdrbl.Enabled = False

			AccountRequest = New AccountRequest(PMHRequestNum, Session("EmployeeConn"))

			Requesttype = AccountRequest.AccountRequestType
			RequesttypeTextBox.Text = AccountRequest.AccountRequestType

			start_dateTextBox.Text = AccountRequest.StartDate
			request_descTextBox.Text = AccountRequest.RequestDesc

			If request_descTextBox.Text <> "" Then
				request_descTextBox.ForeColor() = Color.Red
			End If
			sclose = AccountRequest.CloseDate
			sItemsadded = AccountRequest.itemsadded

			' all items for this request has been submitted any changes done thur account request
			If sItemsadded <> "" Then
				Response.Redirect("~/AccountRequestInfo2.aspx?requestnum=" & PMHRequestNum)
			End If
			'if Requesttype = "addpmhact" then
		End If

		'/SignOnForm.aspx?ClientNum=4076&ReHireRequestNum=10
		'/SignOnForm.aspx?ClientNum=61755&AccountRequestNum=586356

		If Requesttype = "" Then '"addpmhact"
			If lblSearched.Text = "" Then
				SearchPopup.Show()
			End If
		End If





		emp_type_cdrbl.BackColor() = Color.Yellow
        emp_type_cdrbl.BorderColor() = Color.Black
        emp_type_cdrbl.BorderStyle() = BorderStyle.Solid
        emp_type_cdrbl.BorderWidth = Unit.Pixel(2)


        If Not IsPostBack Then
            'lblusersubmit
            'userSubmittingLabel
            'SubmittingOnBehalfOfNameLabel
            pnlProvider.Visible = False
            pnlTCl.Visible = False
            pnlTokenAddress.Visible = False

            'cblApplications.Visible = False

            userSubmittingLabel.Text = UserInfo.UserName
            SubmittingOnBehalfOfNameLabel.Text = UserInfo.UserName
            requestor_client_numLabel.Text = UserInfo.ClientNum
            submitter_client_numLabel.Text = UserInfo.ClientNum
            userDepartmentCD.Text = UserInfo.DepartmentCd

            Dim rblAffiliateBinder As New RadioButtonListBinder(emp_type_cdrbl, "SelAffiliationsV5", Session("EmployeeConn"))

            rblAffiliateBinder.ToolTip = False
            rblAffiliateBinder.BindData("RoleNum", "AffiliationDisplay") '"AffiliationDescLower",

            If HDEmpRoleNum.Text <> "" Then
                emp_type_cdrbl.SelectedValue = CInt(HDEmpRoleNum.Text)
                emp_type_cdrbl.Enabled = False

                masterRole.Text = emp_type_cdrbl.SelectedValue
                RoleNumLabel.Text = emp_type_cdrbl.SelectedValue
                'always use the Main Role
                mainRole = emp_type_cdrbl.SelectedValue


            End If



            Dim ddlBinder As New DropDownListBinder

            Dim cblBinder As New CheckBoxListBinder(cblApplications, "SelApplicationCodes", Session("EmployeeConn"))
            cblBinder.AddDDLCriteria("@AppSystem", "base", SqlDbType.NVarChar)
            cblBinder.AddDDLCriteria("@ApplicationGroup", "financial", SqlDbType.NVarChar)

            cblBinder.BindData("ApplicationNum", "ApplicationDesc")


            Dim cblFacilitesBinder As New CheckBoxListBinder(cblFacilities, "SelFacilityEntity", Session("EmployeeConn"))
            cblFacilitesBinder.AddDDLCriteria("@EntityFacility", "Facility", SqlDbType.NVarChar)
            cblFacilitesBinder.BindData("Code", "EntityFacilityDesc")

            Dim cblEntityBinder As New CheckBoxListBinder(cblEntities, "SelFacilityEntity", Session("EmployeeConn"))
            cblEntityBinder.AddDDLCriteria("@EntityFacility", "Entity", SqlDbType.NVarChar)
            cblEntityBinder.BindData("Code", "EntityFacilityDesc")

            Dim cblRegionBinder As New CheckBoxListBinder(cblRegions, "SelFacilityEntity", Session("EmployeeConn"))
            cblRegionBinder.AddDDLCriteria("@EntityFacility", "region", SqlDbType.NVarChar)
            cblRegionBinder.BindData("Code", "EntityFacilityDesc")

            ddlBinder.BindData(Specialtyddl, "SelSpecialtyCodesV2", "specialty", "specialty", Session("EmployeeConn"))
            ddlBinder.BindData(facility_cdddl, "SelFacilityCodes", "facility_cd", "facility_short_name", Session("EmployeeConn"))
            ddlBinder.BindData(suffixddl, "SelSuffixCodes", "Suffix", "Suffix", Session("EmployeeConn"))
            ddlBinder.BindData(entity_cdDDL, "selEntityCodesV4", "entity_cd", "hosp_desc", Session("EmployeeConn"))

            Dim ddlInvisionBinder As New DropDownListBinder(group_nameddl, "SelPhysGroupsV5", Session("EmployeeConn"))
            ddlInvisionBinder.AddDDLCriteria("@GroupTypeCd", "invision", SqlDbType.NVarChar)

            ddlInvisionBinder.BindData("Group_num", "group_name")

            'Dim ddlNonPerferBinder As New DropDownListBinder(Nonperferdddl, "SelPhysGroupsV5", Session("EmployeeConn"))
            'ddlNonPerferBinder.AddDDLCriteria("@GroupTypeCd", "NonPerGrp", SqlDbType.NVarChar)

            'ddlNonPerferBinder.BindData("Group_num", "group_name")

            Dim ddGnumberBinder As New DropDownListBinder(LocationOfCareIDddl, "SelPhysGroupsV5", Session("EmployeeConn"))
            ddGnumberBinder.AddDDLCriteria("@GroupTypeCd", "CHMG", SqlDbType.NVarChar)

            ddGnumberBinder.BindData("Group_num", "group_name")

            'Dim ddCommLocationOfCare As New DropDownListBinder(CommLocationOfCareIDddl, "SelPhysGroupsV5", Session("EmployeeConn"))
            'ddCommLocationOfCare.AddDDLCriteria("@GroupTypeCd", "CommNum", SqlDbType.NVarChar)

            'ddCommLocationOfCare.BindData("Group_num", "group_name")

            'Dim ddCoverageGroupddl As New DropDownListBinder(CoverageGroupddl, "SelPhysGroupsV5", Session("EmployeeConn"))
            'ddCoverageGroupddl.AddDDLCriteria("@GroupTypeCd", "coverage", SqlDbType.NVarChar)

            'ddCoverageGroupddl.BindData("Group_num", "Group_name")

            Dim ddlCPBinder As New DropDownListBinder
            ddlCPBinder.BindData(CellPhoneddl, "SelCellPhoneProviders", "ProviderExtension", "ProviderName", Session("EmployeeConn"))

            Select Case HDEmpRoleNum.Text

                Case "1195", "1198"

                    Dim ddbVendorBinder As New DropDownListBinder(Vendorddl, "SelVendors", Session("EmployeeConn"))

                    If mainRole = "1554" Then
                        ddbVendorBinder.AddDDLCriteria("@type", "prospect", SqlDbType.NVarChar)

                    Else
                        ddbVendorBinder.AddDDLCriteria("@type", "vendor", SqlDbType.NVarChar)


                    End If
                    ddbVendorBinder.BindData("VendorNum", "VendorName")
                Case Else
                    Vendorddl.Enabled = False

            End Select

            

            If HdMainrole.Text <> "" Then
                Dim ddlBinderV2 As DropDownListBinder

                ddlBinderV2 = New DropDownListBinder(Rolesddl, "SelEmployeeRolesV2", Session("EmployeeConn"))


                ddlBinderV2.AddDDLCriteria("@RoleNum", HdMainrole.Text, SqlDbType.NVarChar)
                ddlBinderV2.BindData("rolenum", "roledesc")

                Rolesddl.SelectedValue = HdMainrole.Text
                Rolesddl.Enabled = False

                thisRole = HdMainrole.Text
                pnlDemo.Enabled = True
                Rolesddl_SelectedIndexChanged(thisRole, EventArgs.Empty)


                ChangColor()

                'ChangColorNoCheck()


                'pnlOtherAffilliation.Visible = False

                CheckRequiredFields()

            End If


            facility_cdddl.SelectedIndex = 0

            department_cdddl.Items.Add("Select Entity")
            building_cdddl.Items.Add("Select Facility")



            If ReactivateClientNum > 0 Then
                lblSearched.Text = "TRUE"
				SearchPopup.Hide()
				Dim sClientSearch As String = ""


				If PMHRequestNumTextBox.Text = "" Then
					sClientSearch = "yes"

				Else
					sClientSearch = "no"

				End If

				Dim c3Controller As New Client3Controller(ReactivateClientNum, sClientSearch, Session("EmployeeConn"))

                Select Case c3Controller.RoleNum
                    Case "782", "1197", "1194"
                        If Requesttype = "" Then '" addpmhact"	show short list
                            rblAffiliateBinder.AddDDLCriteria("@type", "contract", SqlDbType.NVarChar)
                        Else
                            rblAffiliateBinder.AddDDLCriteria("@type", "phys", SqlDbType.NVarChar)
                        End If


                    Case Else
                        ' if a Doctor master number exist
                        If c3Controller.DoctorMasterNum.ToString <> "" Then
                            doctor_master_numTextBox.Text = c3Controller.DoctorMasterNum.ToString
                            doctor_master_numTextBox.Visible = True
                            If Session("SecurityLevelNum") < 90 Then
                                doctor_master_numTextBox.Enabled = False
                            End If

                            'rblAffiliateBinder.AddDDLCriteria("@type", "phys", SqlDbType.NVarChar)
                            'grDocmster
                        Else
                            If Requesttype = "" Then '" addpmhact"	show short list
                                rblAffiliateBinder.AddDDLCriteria("@type", "contract", SqlDbType.NVarChar)
                            Else
                                rblAffiliateBinder.AddDDLCriteria("@type", "", SqlDbType.NVarChar)
                            End If

                        End If

                End Select

                doctor_master_numTextBox.Text = c3Controller.DoctorMasterNum.ToString
                npitextbox.Text = c3Controller.Npi
                LicensesNotextbox.Text = c3Controller.LicensesNo
                taxonomytextbox.Text = c3Controller.Taxonomy
                AuthorizationEmpNumTextBox.Text = c3Controller.SiemensEmpNum

                address_1textbox.Text = c3Controller.Address1
                address_2textbox.Text = c3Controller.Address2
                citytextbox.Text = c3Controller.City
                statetextbox.Text = c3Controller.State
                ziptextbox.Text = c3Controller.Zip
                phoneTextBox.Text = c3Controller.Phone
                faxtextbox.Text = c3Controller.Fax

                Title2TextBox.Text = c3Controller.Title
                SpecialtLabel.Text = c3Controller.Specialty

                mainRole = c3Controller.RoleNum
                masterRole.Text = c3Controller.RoleNum
                RoleNumLabel.Text = c3Controller.RoleNum
                emp_type_cdrbl.SelectedValue = c3Controller.RoleNum
                'Position_NumTextBox.Text = c3Controller.PositionNum

                'emptypecdrbl.SelectedValue = ckhsEmployee.EmpTypeCd
                emp_type_cdLabel.Text = c3Controller.EmpTypeCd

                first_nameTextBox.Text = c3Controller.FirstName
                last_nameTextBox.Text = c3Controller.LastName
                miTextBox.Text = c3Controller.MI

                lblUpdateClientFullName.Text = c3Controller.FullName

                UserPositionDescTextBox.Text = c3Controller.UserPositionDesc
                DepartmentNameTextBox.Text = c3Controller.DepartmentName
                RequesttypeTextBox.Text = c3Controller.AccountRequestType
                CernerPositionDescTextBox.Text = c3Controller.CernerPositionDesc

                LoadEmployeeType()

                'emp_type_cdrbl_SelectedIndexChanged(c3Controller.EmpTypeCd, e)


            End If


        End If

		If RequesttypeTextBox.Text <> "" Then

            pnluserPosition.Visible = True

            first_nameTextBox.Enabled = False
			last_nameTextBox.Enabled = False

		End If


        '  unlock 2015
        'Select Case userDepartmentCD.Text
        '    Case "832600", "832700", "823100"
        '        If Session("SecurityLevelNum") < 90 And requestor_client_numLabel.Text = submitter_client_numLabel.Text Then
        '            btnSelectOnBehalfOf.Visible = True
        '            lblValidation.Text = "Must Change Requestor"
        '            btnSelectOnBehalfOf.BackColor() = Color.Yellow
        '            btnSelectOnBehalfOf.BorderColor() = Color.Black
        '            btnSelectOnBehalfOf.ForeColor() = Color.Black
        '            btnSelectOnBehalfOf.BorderStyle() = BorderStyle.Solid
        '            btnSelectOnBehalfOf.BorderWidth = Unit.Pixel(2)
        '        ElseIf Session("SecurityLevelNum") < 90 And requestor_client_numLabel.Text <> submitter_client_numLabel.Text Then
        '            lblValidation.Text = ""
        '            btnSelectOnBehalfOf.BackColor() = Color.FromKnownColor("336666")
        '            btnSelectOnBehalfOf.ForeColor() = Color.White
        '            btnSelectOnBehalfOf.BorderStyle() = BorderStyle.Groove
        '            btnSelectOnBehalfOf.BorderWidth = Unit.Pixel(1)

        '        End If


        'End Select
        If Session("LoginID").ToString.ToLower = "geip00" Or Session("LoginID").ToString.ToLower = "melej" Or Session("LoginID").ToString.ToLower = "atkm00" Then
            AuthorizationEmpNumTextBox.Enabled = True

        End If
        If Session("SecurityLevelNum") = 95 Or Session("LoginID").ToString.ToLower = "burgina" Or Session("LoginID").ToString.ToLower = "melej" Then
            btnCloseRequest.Visible = True

        End If

    End Sub
    Protected Sub facility_cdddl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles facility_cdddl.SelectedIndexChanged
        facility_cdTextBox.Text = facility_cdddl.SelectedValue

        Dim ddlBinder As DropDownListBinder


        ddlBinder = New DropDownListBinder(building_cdddl, "sel_building_by_facility", Session("EmployeeConn"))
        ddlBinder.AddDDLCriteria("@facility_cd", facility_cdddl.SelectedValue, SqlDbType.NVarChar)
        ddlBinder.BindData("building_cd", "building_name")

        Dim ddlFloorBinder As DropDownListBinder

        ddlFloorBinder = New DropDownListBinder(Floorddl, "SelFloorsByBuilding", Session("EmployeeConn"))
        ddlFloorBinder.AddDDLCriteria("@facility_cd", facility_cdddl.SelectedValue, SqlDbType.NVarChar)
        ddlFloorBinder.AddDDLCriteria("@building_cd", building_cdddl.SelectedValue, SqlDbType.NVarChar)

        ddlFloorBinder.BindData("floor_no", "floor_no")

        ChangColorNoCheck()

        'ChangColor()
    End Sub
    Protected Sub Floorddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles Floorddl.SelectedIndexChanged
        FloorTextBox.Text = Floorddl.SelectedValue
        ChangColorNoCheck()

        'ChangColor()
    End Sub

    Protected Sub building_cdddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles building_cdddl.SelectedIndexChanged
        building_cdTextBox.Text = building_cdddl.SelectedValue
        'buildingnameLabel.Text = building_cdddl.SelectedItem.Text

        Dim ddlFloorBinder As DropDownListBinder

        ddlFloorBinder = New DropDownListBinder(Floorddl, "SelFloorsByBuilding", Session("EmployeeConn"))
        ddlFloorBinder.AddDDLCriteria("@facility_cd", facility_cdddl.SelectedValue, SqlDbType.NVarChar)
        ddlFloorBinder.AddDDLCriteria("@building_cd", building_cdddl.SelectedValue, SqlDbType.NVarChar)

        ddlFloorBinder.BindData("floor_no", "floor_no")

        CheckforRadiology()

        ChangColorNoCheck()

        'ChangColor()
    End Sub


    Protected Sub Titleddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles Titleddl.SelectedIndexChanged
        TitleLabel.Text = Titleddl.SelectedValue


        'TCLddl.SelectedIndex = -1
        TCLTextBox.Text = ""
        UserTypeCdTextBox.Text = ""
        UserTypeDescTextBox.Text = ""
        user_position_descTextBox.Text = emp_type_cdrbl.SelectedValue

        getTCLforRole()


        'Dim tcldata As New CommandsSqlAndOleDb("SelInvisionUserTypeV2", Session("EmployeeConn"))
        'tcldata.AddSqlProcParameter("@rolenum", emp_type_cdrbl.SelectedValue, SqlDbType.NVarChar, 8)
        'tcldata.AddSqlProcParameter("@UserTypeCd", Titleddl.SelectedValue, SqlDbType.NVarChar, 25)


        'tcldata.GetSqlDataset()




        'dtTCLUserTypes = tcldata.GetSqlDataTable
        'gvTCL.DataSource = dtTCLUserTypes
        'gvTCL.DataBind()

        'TCLddl.DataSource = dtTCLUserTypes

        'TCLddl.DataTextField = "UserTypeDesc"
        'TCLddl.DataValueField = "TCL"
        'TCLddl.DataBind()

        'TCLddl.Items.Insert(0, "Select")

        'If dtTCLUserTypes.Rows.Count = 1 Then
        '    'TCLddl.SelectedIndex = 0

        '    TCLTextBox.Text = dtTCLUserTypes.Rows(0)("TCL").ToString
        '    UserTypeDescTextBox.Text = dtTCLUserTypes.Rows(0)("UserTypeDesc").ToString
        '    UserTypecdTextBox.Text = dtTCLUserTypes.Rows(0)("UserTypecd").ToString

        '    pnlTCl.Visible = True
        '    TCLTextBox.Visible = True
        '    UserTypeDescTextBox.Visible = True
        '    UserTypecdTextBox.Visible = True

        '    'TCLddl.Visible = False

        '    gvTCL.Visible = False

        'ElseIf dtTCLUserTypes.Rows.Count = 0 Then
        '    pnlTCl.Visible = False

        'Else
        '    pnlTCl.Visible = True
        '    TCLTextBox.Visible = True
        '    UserTypeDescTextBox.Visible = True
        '    UserTypeCdTextBox.Visible = True


        '    'TCLddl.Visible = True

        '    'TCLddl.SelectedIndex = -1

        '    'TCLddl.BackColor() = Color.Yellow
        '    'TCLddl.BorderColor() = Color.Black
        '    'TCLddl.BorderStyle() = BorderStyle.Solid
        '    'TCLddl.BorderWidth = Unit.Pixel(2)

        '    gvTCL.Visible = True
        '    gvTCL.BackColor() = Color.Yellow
        '    gvTCL.BorderColor() = Color.Black
        '    gvTCL.BorderStyle() = BorderStyle.Solid
        '    gvTCL.BorderWidth = Unit.Pixel(2)
        '    gvTCL.RowStyle.BackColor() = Color.Yellow

        'End If

        'ChangColorNoCheck()

        ChangColor() ' yes change app and color


    End Sub

    Protected Sub gvTCL_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvTCL.RowCommand
        If e.CommandName = "Select" Then


            TCLTextBox.Text = gvTCL.DataKeys(Convert.ToInt32(e.CommandArgument))("TCL").ToString()
            UserTypeCdTextBox.Text = gvTCL.DataKeys(Convert.ToInt32(e.CommandArgument))("UserTypeCd").ToString()
            UserTypeDescTextBox.Text = gvTCL.DataKeys(Convert.ToInt32(e.CommandArgument))("UserTypeDesc").ToString()

            gvTCL.Visible = False


            pnlTCl.Visible = True
            TCLTextBox.Visible = True
            UserTypeDescTextBox.Visible = True
            UserTypeCdTextBox.Visible = True

            btnChangeTCL.Visible = True
            btnChangeTCL.Text = "Show All User Types"

            ModelAfterTextBox.Text = ""

            ModelAfterTextBox.Text = "Refer to TCL " & TCLTextBox.Text & " and " & UserTypeCdTextBox.Text

            CheckforRadiology()

            ChangColor()


        End If

    End Sub

    Protected Sub Specialtyddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles Specialtyddl.SelectedIndexChanged
        SpecialtLabel.Text = Specialtyddl.SelectedValue
        ChangColor()

    End Sub
    Private Sub CernerPosddl_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CernerPosddl.SelectedIndexChanged

        If CernerPosddl.SelectedValue.ToString.ToLower <> "select" Then
            CernerPositionDescTextBox.Text = CernerPosddl.SelectedValue.ToString
        End If

    End Sub
    Protected Sub cblApplications_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cblApplications.SelectedIndexChanged

        'If cblApplications.SelectedValue.ToString.ToLower = "mak" Then
        '    request_descTextBox.Text = "Use This Mak ID ="
        '    request_descTextBox.BackColor() = Color.Yellow
        '    request_descTextBox.BorderColor() = Color.Black
        '    request_descTextBox.BorderStyle() = BorderStyle.Solid
        '    request_descTextBox.BorderWidth = Unit.Pixel(2)
        'End If


        CheckForFinancial()

        'CheckforToken()

        CheckECare()

        'pnlClaimMgt.Visible = True

        'Dim BusLog As New AccountBusinessLogic
        ''   BusLog.CheckAccountDependancy(cblApplications)
        'If BusLog.eCare Then
        '    pnlTCl.Visible = True
        '    'Dim reqField As New RequiredField(TCLTextBox, "TCL for eCare")
        'Else
        '    ' pnlTCl.Visible = False
        '    pageCtrl.RemoveCssClass(pnlTCl, "textInputRequired")
        'End If
        ChangColorNoCheck()

        'ChangColor()

    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        ' see if they change the requestor for IS
        lblValidation.Text = ""
        'unlock 2015
        'Select Case userDepartmentCD.Text
        '    Case "832600", "832700", "823100"
        '        If Session("SecurityLevelNum") < 90 And requestor_client_numLabel.Text = submitter_client_numLabel.Text Then
        '            btnSelectOnBehalfOf.Visible = True
        '            lblValidation.Text = "Must Change Requestor"
        '            lblValidation.Focus()
        '            Return
        '        End If


        'End Select



        CheckRequiredFields()

        CheckFineMail()


        If emp_type_cdrbl.SelectedIndex = -1 Then

            lblValidation.Text = "Must select an Affiliation"
            lblValidation.ForeColor = Color.Red

            lblValidation.Focus()
            emp_type_cdrbl.BackColor() = Color.Red
            Return


        End If


        If RoleNumLabel.Text = "1200" Then
            lblValidation.Text = "Must Specify a Nursing Type"
            lblValidation.ForeColor = Color.Red
            lblValidation.Focus()

            Titleddl.BackColor() = Color.Red
            Exit Sub
        Else

            Titleddl.BackColor() = Color.Yellow
            Titleddl.BorderColor() = Color.Black
            Titleddl.BorderStyle() = BorderStyle.Solid
            Titleddl.BorderWidth = Unit.Pixel(2)


        End If

        If RoleNumLabel.Text = "1189" Then
            lblValidation.Text = "Must Specify a Student Type"
            lblValidation.ForeColor = Color.Red
            lblValidation.Focus()

            Titleddl.BackColor() = Color.Red
            Exit Sub
        Else

            Titleddl.BackColor() = Color.Yellow
            Titleddl.BorderColor() = Color.Black
            Titleddl.BorderStyle() = BorderStyle.Solid
            Titleddl.BorderWidth = Unit.Pixel(2)

        End If

        cblFacilities.ForeColor = Color.Black
        cblEntities.ForeColor = Color.Black

        facility_cdddl.ForeColor = Color.Black
        building_cdddl.ForeColor = Color.Black



        Select Case emp_type_cdrbl.SelectedValue
            Case "782", "1197"

                'Titleddl.ForeColor = Color.Black
                Specialtyddl.ForeColor = Color.Black

                'Writeordersrbl.ForeColor = Color.Black
                'WriteordersDCMHrbl.ForeColor = Color.Black
                'CCMCadmitRightsrbl.ForeColor = Color.Black
                'DCMHadmitRightsrbl.ForeColor = Color.Black

                If TitleLabel.Text = "" Then
                    lblValidation.Text = "Must supply Title"
                    lblValidation.ForeColor = Color.Red
                    lblValidation.Focus()

                    Titleddl.BackColor() = Color.Red
                    Exit Sub

                Else

                    Titleddl.BackColor() = Color.Yellow
                    Titleddl.BorderColor() = Color.Black
                    Titleddl.BorderStyle() = BorderStyle.Solid
                    Titleddl.BorderWidth = Unit.Pixel(2)

                End If

                If npitextbox.Text = "" Then
                    lblValidation.Text = "Must supply NPI Info "
                    lblValidation.ForeColor = Color.Red
                    lblValidation.Focus()

                    npitextbox.BackColor() = Color.Red
                    Exit Sub

                Else

                    npitextbox.BackColor() = Color.Yellow
                    npitextbox.BorderColor() = Color.Black
                    npitextbox.BorderStyle() = BorderStyle.Solid
                    npitextbox.BorderWidth = Unit.Pixel(2)
                End If

                If LicensesNotextbox.Text = "" Then

                    lblValidation.Text = "Must supply Licenses Info "
                    lblValidation.ForeColor = Color.Red
                    lblValidation.Focus()

                    LicensesNotextbox.BackColor() = Color.Red
                    Exit Sub

                Else

                    LicensesNotextbox.BackColor() = Color.Yellow
                    LicensesNotextbox.BorderColor() = Color.Black
                    LicensesNotextbox.BorderStyle() = BorderStyle.Solid
                    LicensesNotextbox.BorderWidth = Unit.Pixel(2)
                End If

                'Specialtyddl
                If SpecialtLabel.Text = "" Then
                    lblValidation.Text = "Must supply Specialty"
                    lblValidation.ForeColor = Color.Red
                    lblValidation.Focus()

                    Specialtyddl.BackColor() = Color.Red
                    Exit Sub
                Else

                    Specialtyddl.BackColor() = Color.Yellow
                    Specialtyddl.BorderColor() = Color.Black
                    Specialtyddl.BorderStyle() = BorderStyle.Solid
                    Specialtyddl.BorderWidth = Unit.Pixel(2)

                End If

                If ecareappSelectedlbl.Text = "43" Then

                    If TCLTextBox.Text = "" Then
                        lblValidation.Text = "Must select User Type and TCL "
                        lblValidation.ForeColor = Color.Red
                        lblValidation.Focus()

                        'UserTypeCdTextBox.BackColor() = Color.Red
                        Exit Sub


                    End If

                End If


                If CCMCadmitRightsrbl.SelectedIndex = -1 Then

                    lblValidation.Text = "Must select Admitting Rights for C.T.S."
                    lblValidation.ForeColor = Color.Red
                    lblValidation.Focus()
                    CCMCadmitRightsrbl.BackColor() = Color.Red

                    Return
                Else

                    CCMCadmitRightsrbl.BackColor() = Color.Yellow
                    CCMCadmitRightsrbl.BorderColor() = Color.Black
                    CCMCadmitRightsrbl.BorderStyle() = BorderStyle.Solid
                    CCMCadmitRightsrbl.BorderWidth = Unit.Pixel(2)


                End If

                'If DCMHadmitRightsrbl.SelectedIndex = -1 Then

                '    lblValidation.Text = "Must select Admitting Rights for DCMH"

                '    lblValidation.ForeColor = Color.Red
                '    lblValidation.Focus()
                '    DCMHadmitRightsrbl.BackColor() = Color.Red

                '    Return
                'Else

                '    DCMHadmitRightsrbl.BackColor() = Color.Yellow
                '    DCMHadmitRightsrbl.BorderColor() = Color.Black
                '    DCMHadmitRightsrbl.BorderStyle() = BorderStyle.Solid
                '    DCMHadmitRightsrbl.BorderWidth = Unit.Pixel(2)

                'End If

                If hanrbl.SelectedIndex = -1 Then

                    lblValidation.Text = "Must select CKHN-HAN"

                    lblValidation.ForeColor = Color.Red
                    lblValidation.Focus()
                    hanrbl.BackColor() = Color.Red

                    Return
                Else

                    hanrbl.BackColor() = Color.Yellow
                    hanrbl.BorderColor() = Color.Black
                    hanrbl.BorderStyle() = BorderStyle.Solid
                    hanrbl.BorderWidth = Unit.Pixel(2)

                End If

                'If Writeordersrbl.SelectedIndex = -1 Then

                '    lblValidation.Text = "Must select Write Orders for C.T.S."
                '    lblValidation.ForeColor = Color.Red
                '    lblValidation.Focus()
                '    Writeordersrbl.BackColor() = Color.Red

                '    Return
                'Else

                '    Writeordersrbl.BackColor() = Color.Yellow
                '    Writeordersrbl.BorderColor() = Color.Black
                '    Writeordersrbl.BorderStyle() = BorderStyle.Solid
                '    Writeordersrbl.BorderWidth = Unit.Pixel(2)

                'End If


                'If WriteordersDCMHrbl.SelectedIndex = -1 Then

                '    lblValidation.Text = "Must select Write Orders for DCMH"
                '    lblValidation.ForeColor = Color.Red
                '    lblValidation.Focus()
                '    WriteordersDCMHrbl.BackColor() = Color.Red

                '    Return
                'Else

                '    WriteordersDCMHrbl.BackColor() = Color.Yellow
                '    WriteordersDCMHrbl.BorderColor() = Color.Black
                '    WriteordersDCMHrbl.BorderStyle() = BorderStyle.Solid
                '    WriteordersDCMHrbl.BorderWidth = Unit.Pixel(2)


                'End If

                If hanrbl.SelectedValue.ToLower = "yes" Then

                    If LocationOfCareIDLabel.Text = "" Then
                        lblValidation.Text = "Must select CKHN Group"
                        lblValidation.ForeColor = Color.Red
                        lblValidation.Focus()
                        LocationOfCareIDddl.BackColor() = Color.Red

                        Return
                    End If


                    If IsNumeric(npitextbox.Text) Then

                    Else

                        lblValidation.Text = "Must Enter Numbers Only for NPI"

                        lblValidation.ForeColor = Color.Red
                        lblValidation.Focus()
                        npitextbox.BackColor() = Color.Red

                        Return

                    End If

                    If npitextbox.Text = "" Then

                        lblValidation.Text = "Must Enter NPI"
                        lblValidation.ForeColor = Color.Red
                        lblValidation.Focus()
                        npitextbox.BackColor() = Color.Red

                        Return

                    End If

                    If LicensesNotextbox.Text = "" Then
                        lblValidation.Text = "Must Enter Licenses No"

                        lblValidation.ForeColor = Color.Red
                        lblValidation.Focus()
                        LicensesNotextbox.BackColor() = Color.Red

                        Return

                    End If

                    If taxonomytextbox.Text = "" Then
                        lblValidation.Text = "Must Enter Taxonomy No"

                        lblValidation.ForeColor = Color.Red
                        lblValidation.Focus()
                        taxonomytextbox.BackColor() = Color.Red

                        Return

                    End If


                End If


                ' Residents
            Case "1194"

                Titleddl.ForeColor = Color.Black
                Specialtyddl.ForeColor = Color.Black

                'Writeordersrbl.ForeColor = Color.Black
                'WriteordersDCMHrbl.ForeColor = Color.Black
                'CCMCadmitRightsrbl.ForeColor = Color.Black
                'DCMHadmitRightsrbl.ForeColor = Color.Black

                If TitleLabel.Text = "" Then
                    lblValidation.Text = "Must supply Title"
                    lblValidation.ForeColor = Color.Red
                    lblValidation.Focus()

                    Titleddl.BackColor() = Color.Red
                    Exit Sub
                Else

                    Titleddl.BackColor() = Color.Yellow
                    Titleddl.BorderColor() = Color.Black
                    Titleddl.BorderStyle() = BorderStyle.Solid
                    Titleddl.BorderWidth = Unit.Pixel(2)

                End If

                'Specialtyddl
                If SpecialtLabel.Text = "" Then
                    lblValidation.Text = "Must supply Specialty"
                    lblValidation.ForeColor = Color.Red
                    lblValidation.Focus()

                    Specialtyddl.BackColor() = Color.Red
                    Exit Sub
                Else

                    Specialtyddl.BackColor() = Color.Yellow
                    Specialtyddl.BorderColor() = Color.Black
                    Specialtyddl.BorderStyle() = BorderStyle.Solid
                    Specialtyddl.BorderWidth = Unit.Pixel(2)

                End If


                'If Writeordersrbl.SelectedIndex = -1 Then

                '    lblValidation.Text = "Must select Write Orders for C.T.S."
                '    lblValidation.ForeColor = Color.Red
                '    lblValidation.Focus()
                '    Writeordersrbl.BackColor() = Color.Red

                '    Return
                'Else

                '    Writeordersrbl.BackColor() = Color.Yellow
                '    Writeordersrbl.BorderColor() = Color.Black
                '    Writeordersrbl.BorderStyle() = BorderStyle.Solid
                '    Writeordersrbl.BorderWidth = Unit.Pixel(2)
                'End If


                'If WriteordersDCMHrbl.SelectedIndex = -1 Then

                '    lblValidation.Text = "Must select Write Orders for DCMH"
                '    lblValidation.ForeColor = Color.Red
                '    lblValidation.Focus()
                '    WriteordersDCMHrbl.BackColor() = Color.Red

                '    Return

                'Else

                '    WriteordersDCMHrbl.BackColor() = Color.Yellow
                '    WriteordersDCMHrbl.BorderColor() = Color.Black
                '    WriteordersDCMHrbl.BorderStyle() = BorderStyle.Solid
                '    WriteordersDCMHrbl.BorderWidth = Unit.Pixel(2)
                'End If

                If Rolesddl.SelectedIndex = 0 Then

                    lblValidation.Text = "Must select Resident Position"
                    lblValidation.ForeColor = Color.Red
                    lblValidation.Focus()
                    Rolesddl.BackColor() = Color.Red

                    Return

                Else

                    Rolesddl.BackColor() = Color.Yellow
                    Rolesddl.BorderColor() = Color.Black
                    Rolesddl.BorderStyle() = BorderStyle.Solid
                    Rolesddl.BorderWidth = Unit.Pixel(2)

                End If

                If hanrbl.SelectedValue.ToLower = "yes" Then
                    If LocationOfCareIDLabel.Text = "" Then
                        lblValidation.Text = "Must select CHMG Group"
                        lblValidation.ForeColor = Color.Red
                        lblValidation.Focus()
                        LocationOfCareIDddl.BackColor() = Color.Red
                    Else

                        LocationOfCareIDddl.BackColor() = Color.Yellow
                        LocationOfCareIDddl.BorderColor() = Color.Black
                        LocationOfCareIDddl.BorderStyle() = BorderStyle.Solid
                        LocationOfCareIDddl.BorderWidth = Unit.Pixel(2)

                    End If
                End If

            Case "1193", "1200"


                If facility_cdddl.SelectedIndex = 0 Then
                    lblValidation.Text = "Must select an Location"
                    lblValidation.ForeColor = Color.Red
                    lblValidation.Focus()
                    facility_cdddl.BackColor() = Color.Red
                    Return
                Else

                    facility_cdddl.BackColor() = Color.Yellow
                    facility_cdddl.BorderColor() = Color.Black
                    facility_cdddl.BorderStyle() = BorderStyle.Solid
                    facility_cdddl.BorderWidth = Unit.Pixel(2)

                End If

                If building_cdddl.SelectedIndex = 0 Then
                    lblValidation.Text = "Must select an Building"
                    lblValidation.ForeColor = Color.Red
                    lblValidation.Focus()

                    building_cdddl.BackColor() = Color.Red
                    Return

                Else

                    building_cdddl.BackColor() = Color.Yellow
                    building_cdddl.BorderColor() = Color.Black
                    building_cdddl.BorderStyle() = BorderStyle.Solid
                    building_cdddl.BorderWidth = Unit.Pixel(2)

                End If

            Case "1195", "1198"
                If Vendorddl.SelectedIndex = 0 Then

                    lblValidation.Text = "Must select Vendor "
                    lblValidation.ForeColor = Color.Red
                    lblValidation.Focus()
                    Vendorddl.BackColor() = Color.Red
                    Return
                Else

                    Vendorddl.BackColor() = Color.Yellow
                    Vendorddl.BorderColor() = Color.Black
                    Vendorddl.BorderStyle() = BorderStyle.Solid
                    Vendorddl.BorderWidth = Unit.Pixel(2)


                End If

                'EMail
                If e_mailTextBox.Text = "" Then
                    lblValidation.Text = "Must supply EMail"
                    lblValidation.ForeColor = Color.Red
                    lblValidation.Focus()

                    e_mailTextBox.BackColor() = Color.Red
                    Exit Sub
                Else

                    e_mailTextBox.BackColor() = Color.Yellow
                    e_mailTextBox.BorderColor() = Color.Black
                    e_mailTextBox.BorderStyle() = BorderStyle.Solid
                    e_mailTextBox.BorderWidth = Unit.Pixel(2)

                End If


        End Select

        If Not checkFacilities() Then

            lblValidation.Text = "Must select a Hospital"
            lblValidation.ForeColor = Color.Red
            lblValidation.Focus()
            cblFacilities.BackColor() = Color.Red

            Return
        Else

            cblFacilities.BackColor() = Color.Yellow
            cblFacilities.BorderColor() = Color.Black
            cblFacilities.BorderStyle() = BorderStyle.Solid
            cblFacilities.BorderWidth = Unit.Pixel(2)

        End If

        If Not checkEntities() Then

            lblValidation.Text = "Must select an Entity"
            lblValidation.ForeColor = Color.Red
            lblValidation.Focus()
            cblEntities.BackColor() = Color.Red
            Return

        Else

            cblEntities.BackColor() = Color.Yellow
            cblEntities.BorderColor() = Color.Black
            cblEntities.BorderStyle() = BorderStyle.Solid
            cblEntities.BorderWidth = Unit.Pixel(2)

        End If


        If Not checkApplications() Then

            lblValidation.Text = "Must select Application to Access"
            Return

        End If

        For Each cbApp As ListItem In cblApplications.Items
            If cbApp.Selected Then

                Select Case cbApp.Value
                    Case 117 ' "nthrive claims mgmt"

                        If ClaimMgtrbl.SelectedIndex = -1 Then

                            lblValidation.Text = "Must select nTHRIVE Claims Mgmt. Level"
                            lblValidation.ForeColor = Color.Red
                            lblValidation.Focus()
                            ClaimMgtrbl.BackColor() = Color.Red

                            Return
                        Else

                            ClaimMgtrbl.BackColor() = Color.Yellow
                            ClaimMgtrbl.BorderColor() = Color.Black
                            ClaimMgtrbl.BorderStyle() = BorderStyle.Solid
                            ClaimMgtrbl.BorderWidth = Unit.Pixel(2)

                        End If

                    Case 106
                        'Patient Accounting


                        If PAtientAcctrbl.SelectedIndex = -1 Then

                            lblValidation.Text = "Must select Patient Accounting Level"
                            lblValidation.ForeColor = Color.Red
                            lblValidation.Focus()
                            PAtientAcctrbl.BackColor() = Color.Red

                            Return
                        Else

                            PAtientAcctrbl.BackColor() = Color.Yellow
                            PAtientAcctrbl.BorderColor() = Color.Black
                            PAtientAcctrbl.BorderStyle() = BorderStyle.Solid
                            PAtientAcctrbl.BorderWidth = Unit.Pixel(2)

                        End If

                        'cblPAtientAcct
                        If PAtientAcctrbl.SelectedValue = 2 Then
                            If patamount.Text = "" Then
                                lblValidation.Text = "Must select Patient Accounting Amount."
                                lblValidation.ForeColor = Color.Red
                                lblValidation.Focus()
                                PatientAcctddl.BackColor() = Color.Red

                                Return
                            Else
                                PatientAcctddl.BackColor() = Color.Yellow
                                PatientAcctddl.BorderColor() = Color.Black
                                PatientAcctddl.BorderStyle() = BorderStyle.Solid
                                PatientAcctddl.BorderWidth = Unit.Pixel(2)

                            End If
                        End If


                    Case 118
                        '"care payments"

                        If CarePaymentrbl.SelectedIndex = -1 Then

                            lblValidation.Text = "Must select Care Payment Level"
                            lblValidation.ForeColor = Color.Red
                            lblValidation.Focus()
                            CarePaymentrbl.BackColor() = Color.Red

                            Return
                        Else

                            CarePaymentrbl.BackColor() = Color.Yellow
                            CarePaymentrbl.BorderColor() = Color.Black
                            CarePaymentrbl.BorderStyle() = BorderStyle.Solid
                            CarePaymentrbl.BorderWidth = Unit.Pixel(2)
                        End If

                    Case 119
                        '"sci scheduler"
                        'cblScISchedule
                        If ScISchedulerbl.SelectedIndex = -1 Then

                            lblValidation.Text = "Must select Care Payment Level"
                            lblValidation.ForeColor = Color.Red
                            lblValidation.Focus()
                            ScISchedulerbl.BackColor() = Color.Red

                            Return
                        Else

                            ScISchedulerbl.BackColor() = Color.Yellow
                            ScISchedulerbl.BorderColor() = Color.Black
                            ScISchedulerbl.BorderStyle() = BorderStyle.Solid
                            ScISchedulerbl.BorderWidth = Unit.Pixel(2)
                        End If

                    Case 43 '"ecare"
                        If UserTypeCdTextBox.Text = "" Then
                            lblValidation.Text = "Must Specify User Type and TCL"
                            lblValidation.ForeColor = Color.Red
                            lblValidation.Focus()

                            'Titleddl.BackColor() = Color.Red
                            Exit Sub


                        End If

                    Case 156
                        'load fields
                        If CellProvidertxt.Text.ToLower = "other" Then

                            If otherCellprov.Text = "" Then
                                lblTokenError.Text = " Must supply Provider Information"
                                lblTokenError.Visible = True
                                lblTokenError.Focus()

                                otherCellprov.BackColor() = Color.Red
                                Exit Sub
                            Else

                                otherCellprov.BackColor() = Color.Yellow
                                otherCellprov.BorderColor() = Color.Black
                                otherCellprov.BorderStyle() = BorderStyle.Solid
                                otherCellprov.BorderWidth = Unit.Pixel(2)

                            End If
                        ElseIf CellProvidertxt.Text = "" Then
                            CellPhoneddl.Visible = True
                            lblTokenError.Text = " Must supply Provider Information"
                            lblTokenError.Visible = True
                            lblTokenError.Focus()

                            CellPhoneddl.BackColor() = Color.Red
                            Exit Sub
                        Else

                            CellPhoneddl.BackColor() = Color.Yellow
                            CellPhoneddl.BorderColor() = Color.Black
                            CellPhoneddl.BorderStyle() = BorderStyle.Solid
                            CellPhoneddl.BorderWidth = Unit.Pixel(2)


                        End If

                        If TokenPhoneTextBox.Text = "" Then
                            lblTokenError.Text = " Must supply a Cell Phone number "
                            lblTokenError.Visible = True
                            lblTokenError.Focus()

                            TokenPhoneTextBox.BackColor() = Color.Red
                            Exit Sub
                        Else
                            If (Regex.IsMatch(TokenPhoneTextBox.Text, "^[0-9 ]") = False) Then

                                lblTokenError.Text = "Phone Number must only contain numbers."
                                lblTokenError.Visible = True
                                TokenPhoneTextBox.Text = ""

                                Exit Sub

                            End If


                            TokenPhoneTextBox.BackColor() = Color.Yellow
                            TokenPhoneTextBox.BorderColor() = Color.Black
                            TokenPhoneTextBox.BorderStyle() = BorderStyle.Solid
                            TokenPhoneTextBox.BorderWidth = Unit.Pixel(2)


                        End If



                    Case 121  ' "general ledger/ap/payroll"
                        If cblRegions.SelectedIndex = -1 Then

                            cblRegions.BackColor() = Color.Yellow
                            cblRegions.BorderColor() = Color.Black
                            cblRegions.BorderStyle() = BorderStyle.Solid
                            cblRegions.BorderWidth = Unit.Pixel(2)

                            cblRegions.Visible = True
                            Regionlbl.Visible = True

                        End If



                End Select
            End If

        Next


        'For Each cbFacility As ListItem In cblFacilities.Items
        '    If cbFacility.Selected Then
        '        If cbFacility.ToString.ToLower.Trim = "community" Then
        '            For Each cbApp As ListItem In cblApplications.Items
        '                If cbApp.Selected Then

        '                    Select Case cbApp.ToString.ToLower.Trim
        '                        Case "ebed", "ecase", "ecare", "pacs", "pharmacy", "picis (emergency dept.)", "edm", "pyxis"
        '                            lblValidation.Text = "InValid Application for Community -> " & cbApp.ToString
        '                            lblValidation.Focus()

        '                            cblFacilities.BackColor() = Color.Red


        '                            Return
        '                    End Select
        '                End If

        '            Next

        '        End If


        '    End If
        'Next

        CheckForFinancial()


        If bHasError = True Then
            lblValidation.Focus()
            Exit Sub
        Else
            lblValidation.Text = ""
        End If

        Dim BusLog As New AccountBusinessLogic
        Dim thisdata As New CommandsSqlAndOleDb("SelApplicationCodes", Session("EmployeeConn"))
        thisdata.AddSqlProcParameter("@AppSystem", "all", SqlDbType.NVarChar, 24)
        thisdata.AddSqlProcParameter("@ApplicationGroup", "financial", SqlDbType.NVarChar, 24)

        thisdata.ExecNonQueryNoReturn()

        BusLog.Accounts = thisdata.GetSqlDataTable

        Dim AccountCode As String

		'if this is not a PMH request
		If Requesttype = "" Then '"addpmhact"

			'  now see if person already exists
			If TestednameLabel.Text <> "yes" Then
				CheckExists()
				lblCloseExists.Focus()
				Exit Sub
			End If

		End If

		'FormSignOn.AddInsertParm("@submitter_client_num", submitter_client_numLabel.Text, SqlDbType.NVarChar, 10)

		'ReactivateClientNum
		'ReActivateRequestNum
		If ReactivateClientNum > 0 Then
            FormSignOn.AddInsertParm("@account_request_type", "rehire", SqlDbType.NVarChar, 9)
            'FormSignOn.AddInsertParm("@ReactivateClientNum", CInt(ReactivateClientNumTextBox.Text), SqlDbType.Int, 9)
            'FormSignOn.AddInsertParm("@ReHireRequestNum", CInt(RehireRequestNumTextBox.Text), SqlDbType.Int, 9)

        Else
            FormSignOn.AddInsertParm("@account_request_type", "addnew", SqlDbType.NVarChar, 9)

        End If
        'Dim gname As String = group_nameTextBox.Text
        'Dim gnum As String = group_numTextBox.Text

        If PMHRequestNumTextBox.Text <> "" Then
			PMHSignon.AddInsertParm("@account_request_num", CInt(PMHRequestNumTextBox.Text), SqlDbType.Int, 9)

			PMHSignon.UpdateFormReturnReqNum()
			AccountRequestNum = CInt(PMHRequestNumTextBox.Text)
			insPMHAddedItems(AccountRequestNum)

		Else
			FormSignOn.UpdateFormReturnReqNum()

			AccountRequestNum = FormSignOn.AccountRequestNum

		End If


		For Each cbItem As ListItem In cblApplications.Items
            If cbItem.Selected Then
                If cbItem.Value = 156 Then
                    SubmitTokenAddress()
                End If
                If BusLog.isHospSpecific(cbItem.Value) Then
                    For Each cbFacility As ListItem In cblFacilities.Items

                        If cbFacility.Selected Then

                            AccountCode = BusLog.GetApplicationCode(cbItem.Value, cbFacility.Value)

                            insAcctItems(AccountRequestNum, AccountCode, request_descTextBox.Text)

                        End If

                    Next
                ElseIf BusLog.isRegionSpecific(cbItem.Value) Then

                    For Each cbreg As ListItem In cblRegions.Items

                        If cbreg.Selected Then

                            AccountCode = BusLog.GetApplicationCode(cbItem.Value, cbreg.Value)

                            insAcctItems(AccountRequestNum, AccountCode, request_descTextBox.Text)

                        End If

                    Next

                ElseIf BusLog.isEntSpecific(cbItem.Value) Then

                    For Each cbEnt As ListItem In cblEntities.Items

                        If cbEnt.Selected Then

                            AccountCode = BusLog.GetApplicationCode(cbItem.Value, cbEnt.Value)

                            insAcctItems(AccountRequestNum, AccountCode, request_descTextBox.Text)

                        End If

                    Next


                Else

                    insAcctItems(AccountRequestNum, cbItem.Value, request_descTextBox.Text)

                End If

            End If

        Next

        Select Case emp_type_cdrbl.SelectedValue

            Case "1195", "1198" ' if contractor/vendor

                If e_mailTextBox.Text <> "" Then '' if email is not null

                    Dim thisdatae As New CommandsSqlAndOleDb("InsAccountItemsV3", Session("EmployeeConn"))
                    thisdatae.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int, 20)
                    thisdatae.AddSqlProcParameter("@ApplicationNum", 155, SqlDbType.Int, 8)

                    thisdatae.AddSqlProcParameter("@account_item_desc", e_mailTextBox.Text, SqlDbType.NVarChar, 255)
                    thisdatae.AddSqlProcParameter("@who_nt_login", Session("LoginID"), SqlDbType.NVarChar, 50)

                    thisdatae.ExecNonQueryNoReturn()

                End If
        End Select
        'Select Case emp_type_cdrbl.SelectedValue
        '    'Case "physician/rovider", "non staff clinician", "resident"  "allied health"

        '    Case "782", "1197", "1194"
        '        If GNumberGroupNumLabel.Text <> "" Then

        '            ' Insert for CKHN-HAN with G# to interface Queue
        '            insAcctItems(AccountRequestNum, "91", LocationOfCareIDLabel.Text)

        '        End If

        '        If CoverageGroupNumLabel.Text <> "" Then

        '            ' Insert for Comm interface Queue
        '            insAcctItems(AccountRequestNum, "93", CoverageGroupNumLabel.Text)

        '        End If

        '        If CoverageGroupNumLabel.Text <> "" Or GNumberGroupNumLabel.Text <> "" Then
        '            insAcctItems(AccountRequestNum, "26", "Misys account Needed")
        '        End If


        'End Select

        If Session("environment") = "" Then
            Dim Envoirn As New Environment()
            thisenviro.Text = Envoirn.getEnvironment
            Session("environment") = Envoirn.getEnvironment
        End If


        SendProviderEmail(AccountRequestNum)

        'If Session("environment").ToString.ToLower <> "testing" Then

        '    SendProviderEmail(AccountRequestNum)

        'End If

        Response.Redirect("~/RequestSubmited.aspx?RequestNum=" & AccountRequestNum)



    End Sub
    Protected Sub SubmitTokenAddress()

        If CellProvidertxt.Text <> "" Then

            TokenPhoneTextBox.Text = TokenPhoneTextBox.Text & CellProvidertxt.Text

            TokenNameTextBox.Text = CellProvidertxt.Text

        ElseIf otherCellprov.Text <> "" Then
            TokenPhoneTextBox.Text = TokenPhoneTextBox.Text & otherCellprov.Text
            TokenNameTextBox.Text = otherCellprov.Text
        End If

        Dim cmdTokenAddress As New CommandsSqlAndOleDb("InsUpdAccountTokenAddress", Session("EmployeeConn"))
        cmdTokenAddress.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int, 8)
        'TokenPhoneTextBox
        cmdTokenAddress.AddSqlProcParameter("@TokenPhone", TokenPhoneTextBox.Text, SqlDbType.NVarChar, 50)
        ' provider name 
        cmdTokenAddress.AddSqlProcParameter("@TokenName", TokenNameTextBox.Text, SqlDbType.NVarChar, 75)

        'cmdTokenAddress.AddSqlProcParameter("@TokenAddress1", TokenAddress1TextBox.Text, SqlDbType.NVarChar, 75)
        'cmdTokenAddress.AddSqlProcParameter("@TokenCity", TokenCityTextBox.Text, SqlDbType.NVarChar, 75)
        'cmdTokenAddress.AddSqlProcParameter("@TokenSt", TokenStTextBox.Text, SqlDbType.NVarChar, 25)

        'cmdTokenAddress.AddSqlProcParameter("@TokenZip", TokenZipTextBox.Text, SqlDbType.NVarChar, 15)

        cmdTokenAddress.ExecNonQueryNoReturn()


    End Sub

    Protected Sub cblFacilities_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cblFacilities.SelectedIndexChanged
        CheckForFinancial()
        CheckforToken()
        CheckECare()
        ChangColorNoCheck()

        'ChangColor()

        'Dim tbTCL As New DataTable

        'tbTCL.Columns.Add(New DataColumn("TCL"))

        'tbTCL.Columns.Add(New DataColumn("Hospital"))


        'TCLCCMC.Text = ""
        'TCLDCMH.Text = ""
        'TCLTaylor.Text = ""
        'TCLSpring.Text = ""


        'Dim hospitalcount As Integer = 0
        ''Dim OrigReqDesc As String = ""
        'Dim MakCont As Boolean
        'Dim CurrCont As Boolean

        'If OrigReqDesc.Text = "" Then
        '    OrigReqDesc.Text = request_descTextBox.Text

        'End If

        'request_descTextBox.Text = ""


        'Dim Rolepass As String = ""
        'If RoleNumLabel.Text <> "" Then
        '    Rolepass = RoleNumLabel.Text
        'ElseIf thisRole <> emp_type_cdrbl.SelectedValue Then
        '    Rolepass = thisRole
        'Else
        '    Rolepass = emp_type_cdrbl.SelectedValue
        'End If

        ' count how many hospitals checked if only then do not execute
        'For Each cbHospitals As ListItem In cblFacilities.Items
        '    If cbHospitals.Selected Then
        '        hospitalcount = hospitalcount + 1
        '    End If
        'Next

        'Select Case Rolepass
        '    Case "950", "1193", "1195", "1422", "1425", "1428", "1430", "1431"

        '    Case Else

        '        If hospitalcount > 1 Then

        '            ' check all hospitals
        '            For Each cbHospitals As ListItem In cblFacilities.Items
        '                If cbHospitals.Selected Then

        '                    Dim sCurrHospital As String = cbHospitals.ToString

        '                    Select Case sCurrHospital.ToString.TrimEnd

        '                        Case "CCMC"
        '                            Dim tcldata As New CommandsSqlAndOleDb("SelInvisionUserType", Session("EmployeeConn"))
        '                            tcldata.AddSqlProcParameter("@rolenum", Rolepass, SqlDbType.NVarChar, 8)

        '                            tcldata.GetSqlDataset()

        '                            dtTCLUserTypes = tcldata.GetSqlDataTable

        '                            If dtTCLUserTypes.Rows.Count > 0 Then

        '                                TCLCCMC.Text = dtTCLUserTypes.Rows(0)("TCL").ToString

        '                                If TCLCCMC.Text <> "" Then

        '                                    tbTCL.Rows.Add(TCLCCMC.Text, "15")

        '                                    If TCLCCMC.Text <> TCLTextBox.Text Then

        '                                        request_descTextBox.Text = request_descTextBox.Text & "  CCMC TCL:" & TCLCCMC.Text
        '                                        TCLTextBox.Text = TCLCCMC.Text

        '                                    End If
        '                                End If

        '                            End If


        '                            dtTCLUserTypes.Dispose()


        '                            ' Always test for non union hospital#5
        '                            Dim uniontcldata As New CommandsSqlAndOleDb("SelInvisionUserType", Session("EmployeeConn"))
        '                            uniontcldata.AddSqlProcParameter("@rolenum", Rolepass, SqlDbType.NVarChar, 8)


        '                            uniontcldata.GetSqlDataset()

        '                            dtTCLUserTypes = uniontcldata.GetSqlDataTable
        '                            If dtTCLUserTypes.Rows.Count > 0 Then
        '                                UnionTCL.Text = dtTCLUserTypes.Rows(0)("TCL").ToString

        '                                If UnionTCL.Text <> "" And UnionTCL.Text <> "TCL" Then

        '                                    tbTCL.Rows.Add(UnionTCL.Text, "5")


        '                                    request_descTextBox.Text = request_descTextBox.Text & "  Non Union TCL:" & UnionTCL.Text

        '                                End If

        '                            End If
        '                        Case "DCMH"

        '                            Dim dcmhtcldata As New CommandsSqlAndOleDb("SelInvisionUserType", Session("EmployeeConn"))
        '                            dcmhtcldata.AddSqlProcParameter("@rolenum", Rolepass, SqlDbType.NVarChar, 8)
        '                            dcmhtcldata.GetSqlDataset()

        '                            dtTCLUserTypes = dcmhtcldata.GetSqlDataTable

        '                            If dtTCLUserTypes.Rows.Count > 0 Then

        '                                TCLDCMH.Text = dtTCLUserTypes.Rows(0)("TCL").ToString

        '                                If TCLDCMH.Text <> "" Then

        '                                    If TCLTextBox.Text <> TCLDCMH.Text Then

        '                                        tbTCL.Rows.Add(TCLDCMH.Text, "12")

        '                                        request_descTextBox.Text = request_descTextBox.Text & "  DCMH TCL:" & TCLDCMH.Text

        '                                        TCLTextBox.Text = TCLDCMH.Text

        '                                    End If

        '                                End If

        '                            End If

        '                            dtTCLUserTypes.Dispose()


        '                        Case "Taylor"
        '                            Dim taylortcldata As New CommandsSqlAndOleDb("SelInvisionUserType", Session("EmployeeConn"))
        '                            taylortcldata.AddSqlProcParameter("@rolenum", Rolepass, SqlDbType.NVarChar, 8)

        '                            taylortcldata.GetSqlDataset()

        '                            dtTCLUserTypes = taylortcldata.GetSqlDataTable

        '                            If dtTCLUserTypes.Rows.Count > 0 Then

        '                                TCLTaylor.Text = dtTCLUserTypes.Rows(0)("TCL").ToString
        '                                If TCLTaylor.Text <> "" Then

        '                                    If TCLTextBox.Text <> TCLTaylor.Text Then

        '                                        tbTCL.Rows.Add(TCLTaylor.Text, "19")


        '                                        request_descTextBox.Text = request_descTextBox.Text & "  Taylor TCL:" & TCLTaylor.Text


        '                                        TCLTextBox.Text = TCLTaylor.Text
        '                                    End If

        '                                End If
        '                            End If

        '                            dtTCLUserTypes.Dispose()

        '                        Case "Springfield"
        '                            Dim sprtcldata As New CommandsSqlAndOleDb("SelInvisionUserType", Session("EmployeeConn"))
        '                            sprtcldata.AddSqlProcParameter("@rolenum", Rolepass, SqlDbType.NVarChar, 8)

        '                            sprtcldata.GetSqlDataset()

        '                            dtTCLUserTypes = sprtcldata.GetSqlDataTable

        '                            If dtTCLUserTypes.Rows.Count > 0 Then

        '                                TCLSpring.Text = dtTCLUserTypes.Rows(0)("TCL").ToString

        '                                If TCLSpring.Text <> "" Then

        '                                    If TCLTextBox.Text <> TCLSpring.Text Then
        '                                        tbTCL.Rows.Add(TCLSpring.Text, "16")

        '                                        request_descTextBox.Text = request_descTextBox.Text & "  Springfield TCL:" & TCLSpring.Text

        '                                        TCLTextBox.Text = TCLSpring.Text

        '                                    End If
        '                                End If

        '                                dtTCLUserTypes.Dispose()
        '                            End If


        '                    End Select

        '                End If

        '            Next

        '            pnlTCl.Visible = True

        '        ElseIf hospitalcount = 1 And TCLTextBox.Text = "" Then

        '            Dim sCurrHospital As String = ""

        '            For Each cbHospitals As ListItem In cblFacilities.Items

        '                If cbHospitals.Selected Then

        '                    sCurrHospital = cbHospitals.Value

        '                End If
        '            Next

        '            Dim tcldata As New CommandsSqlAndOleDb("SelInvisionUserType", Session("EmployeeConn"))
        '            tcldata.AddSqlProcParameter("@rolenum", Rolepass, SqlDbType.NVarChar, 8)
        '            tcldata.AddSqlProcParameter("@hospitalnum", sCurrHospital, SqlDbType.NVarChar, 8)

        '            tcldata.GetSqlDataset()

        '            dtTCLUserTypes = tcldata.GetSqlDataTable


        '            TCLTextBox.Text = dtTCLUserTypes.Rows(0)("TCL").ToString
        '            UserTypeDescTextBox.Text = dtTCLUserTypes.Rows(0)("UserTypeDesc").ToString
        '            UserTypeCdTextBox.Text = dtTCLUserTypes.Rows(0)("UserTypeCd").ToString

        '            tbTCL.Rows.Add(TCLTextBox.Text, sCurrHospital)

        '            request_descTextBox.Text = OrigReqDesc.Text

        '            pnlTCl.Visible = True
        '            TCLTextBox.Visible = True
        '            UserTypeDescTextBox.Visible = True
        '            UserTypeCdTextBox.Visible = True

        '        End If

        '        MakCont = OrigReqDesc.Text.Contains("Mak ID")
        '        CurrCont = request_descTextBox.Text.Contains("Mak ID")
        '        If MakCont = True And CurrCont = False Then
        '            request_descTextBox.Text = request_descTextBox.Text & " Please Add Mak ID "
        '        End If

        'End Select

        'request_descTextBox.Text = OrigReqDesc & request_descTextBox.Text
        'request_descTextBox

    End Sub

    Protected Sub cblEntities_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cblEntities.SelectedIndexChanged
        ChangColorNoCheck()
        'ChangColor()
    End Sub

    Private Sub CheckExists()
        CheckRequiredFields()
        Dim Submitternum As Integer
        Submitternum = submitter_client_numLabel.Text


        Dim ExistsClientsData As New CommandsSqlAndOleDb("SelExistingClients", Session("EmployeeConn"))
        ExistsClientsData.AddSqlProcParameter("@FirstName", first_nameTextBox.Text, SqlDbType.NVarChar, 20)
        ExistsClientsData.AddSqlProcParameter("@LastName", last_nameTextBox.Text, SqlDbType.NVarChar, 20)
        ExistsClientsData.AddSqlProcParameter("@SubmitterClientNum", Submitternum, SqlDbType.Int, 9)


        grvClientsExists.DataSource = ExistsClientsData.GetSqlDataTable
        grvClientsExists.DataBind()


        'UpdPnlExists.Update()
        ClientExists.Show()


    End Sub
    Protected Sub CheckForFinancial()
        Dim appname As String

        For Each cbApp As ListItem In cblApplications.Items
            If cbApp.Selected Then

                appname = cbApp.ToString
                'cbApp.ToString.ToLower.Trim

                '"nTHRIVE Claims Mgmt"
                'ClaimMgtID.Text

                '"Patient Accounting"
                'PatientAcctID.Text

                '"Insight"
                'InsightID.Text

                '"SCI Scheduler"
                'SCIid.Text

                '"Care Payments"
                'CarePayID.Text


                Select Case appname
                    Case "nTHRIVE Claims Mgmt"
                        If ClaimMgtID.Text = "" Then


                            'nTHRIVE Claims Mgmt
                            Dim cblClaimBinder As New RadioButtonListBinder(ClaimMgtrbl, "SelApplicationGroupNum", Session("EmployeeConn"))
                            cblClaimBinder.AddDDLCriteria("@ApplicationNum ", "117", SqlDbType.NVarChar)
                            cblClaimBinder.BindData("ApplicationGroupNum", "GroupDesc")

                            pnlClaimMgt.Visible = True

                            ClaimMgtrbl.BackColor() = Color.Yellow
                            ClaimMgtrbl.BorderColor() = Color.Black
                            ClaimMgtrbl.BorderStyle() = BorderStyle.Solid
                            ClaimMgtrbl.BorderWidth = Unit.Pixel(2)
                        End If

                    Case "Patient Accounting"
                        'Patient Accounting

                        If PatientAcctID.Text = "" Then



                            Dim cblPTABinder As New RadioButtonListBinder(PAtientAcctrbl, "SelApplicationGroupNum", Session("EmployeeConn"))
                            cblPTABinder.AddDDLCriteria("@ApplicationNum ", "106", SqlDbType.NVarChar)
                            cblPTABinder.BindData("ApplicationGroupNum", "GroupDesc")

                            panPatientAcct.Visible = True
                            'cblPAtientAcct
                            PAtientAcctrbl.BackColor() = Color.Yellow
                            PAtientAcctrbl.BorderColor() = Color.Black
                            PAtientAcctrbl.BorderStyle() = BorderStyle.Solid
                            PAtientAcctrbl.BorderWidth = Unit.Pixel(2)

                            PatientAcctddl.BackColor() = Color.Yellow
                            PatientAcctddl.BorderColor() = Color.Black
                            PatientAcctddl.BorderStyle() = BorderStyle.Solid
                            PatientAcctddl.BorderWidth = Unit.Pixel(2)

                        End If

                    Case "Care Payments"

                        If CarePayID.Text = "" Then



                            Dim cblPayBinder As New RadioButtonListBinder(CarePaymentrbl, "SelApplicationGroupNum", Session("EmployeeConn"))
                            cblPayBinder.AddDDLCriteria("@ApplicationNum ", "118", SqlDbType.NVarChar)
                            cblPayBinder.BindData("ApplicationGroupNum", "GroupDesc")

                            pnlCarePay.Visible = True
                            'cblCarePayment
                            CarePaymentrbl.BackColor() = Color.Yellow
                            CarePaymentrbl.BorderColor() = Color.Black
                            CarePaymentrbl.BorderStyle() = BorderStyle.Solid
                            CarePaymentrbl.BorderWidth = Unit.Pixel(2)
                        End If

                    Case "SCI Scheduler"

                        If SCIid.Text = "" Then


                            Dim cblSchedBinder As New RadioButtonListBinder(ScISchedulerbl, "SelApplicationGroupNum", Session("EmployeeConn"))
                            cblSchedBinder.AddDDLCriteria("@ApplicationNum ", "119", SqlDbType.NVarChar)
                            cblSchedBinder.BindData("ApplicationGroupNum", "GroupDesc")

                            panSCISchedule.Visible = True
                            'cblScISchedule
                            ScISchedulerbl.BackColor() = Color.Yellow
                            ScISchedulerbl.BorderColor() = Color.Black
                            ScISchedulerbl.BorderStyle() = BorderStyle.Solid
                            ScISchedulerbl.BorderWidth = Unit.Pixel(2)
                        End If


                    Case "General Ledger/AP/Payroll"
                        cblRegions.Visible = True
                        Regionlbl.Visible = True

                        cblRegions.BackColor() = Color.Yellow
                        cblRegions.BorderColor() = Color.Black
                        cblRegions.BorderStyle() = BorderStyle.Solid
                        cblRegions.BorderWidth = Unit.Pixel(2)

                End Select

            End If
        Next

    End Sub
    Protected Sub CheckforRadiology()
        For Each cbApp As ListItem In cblApplications.Items
            If cbApp.Selected = False Then
                'not select items
                Select Case cbApp.Value
                    Case 1, 3, 10, 12, 139, 142, 145, 148, 151
                        If TCLTextBox.Text.ToLower = "radsigon" Then
                            cbApp.Selected = True
                        End If
                    Case 154
                        'Pioneer urget care
                        If facility_cdTextBox.Text = "25" Then
                            If building_cdTextBox.Text = "1" Then
                                cbApp.Selected = True
                            End If
                        End If

                End Select
            End If

        Next

    End Sub
    Protected Sub CheckforToken()
        For Each cbApp As ListItem In cblApplications.Items
            If cbApp.Selected Then

                Select Case cbApp.Value
                    Case 156
                        'load fields


                        pnlTokenAddress.Visible = True

                        CellPhoneddl.BackColor() = Color.Yellow
                        CellPhoneddl.BorderColor() = Color.Black
                        CellPhoneddl.BorderStyle() = BorderStyle.Solid
                        CellPhoneddl.BorderWidth = Unit.Pixel(2)


                        TokenPhoneTextBox.BackColor() = Color.Yellow
                        TokenPhoneTextBox.BorderColor() = Color.Black
                        TokenPhoneTextBox.BorderStyle() = BorderStyle.Solid
                        TokenPhoneTextBox.BorderWidth = Unit.Pixel(2)


                        'If TokenTest.Text <> "yes" Then
                        '    TokenNameTextBox.Text = first_nameTextBox.Text & " " & last_nameTextBox.Text
                        '    TokenAddress1TextBox.Text = address_1textbox.Text
                        '    TokenAddress2TextBox.Text = address_2textbox.Text

                        '    TokenCityTextBox.Text = citytextbox.Text
                        '    TokenStTextBox.Text = statetextbox.Text
                        '    TokenPhoneTextBox.Text = phoneTextBox.Text
                        '    TokenZipTextBox.Text = ziptextbox.Text

                        '    TokenNameTextBox.BackColor() = Color.Yellow
                        '    TokenNameTextBox.BorderColor() = Color.Black
                        '    TokenNameTextBox.BorderStyle() = BorderStyle.Solid
                        '    TokenNameTextBox.BorderWidth = Unit.Pixel(2)

                        '    TokenAddress1TextBox.BackColor() = Color.Yellow
                        '    TokenAddress1TextBox.BorderColor() = Color.Black
                        '    TokenAddress1TextBox.BorderStyle() = BorderStyle.Solid
                        '    TokenAddress1TextBox.BorderWidth = Unit.Pixel(2)

                        '    TokenCityTextBox.BackColor() = Color.Yellow
                        '    TokenCityTextBox.BorderColor() = Color.Black
                        '    TokenCityTextBox.BorderStyle() = BorderStyle.Solid
                        '    TokenCityTextBox.BorderWidth = Unit.Pixel(2)


                        '    TokenStTextBox.BackColor() = Color.Yellow
                        '    TokenStTextBox.BorderColor() = Color.Black
                        '    TokenStTextBox.BorderStyle() = BorderStyle.Solid
                        '    TokenStTextBox.BorderWidth = Unit.Pixel(2)


                        '    TokenZipTextBox.BackColor() = Color.Yellow
                        '    TokenZipTextBox.BorderColor() = Color.Black
                        '    TokenZipTextBox.BorderStyle() = BorderStyle.Solid
                        '    TokenZipTextBox.BorderWidth = Unit.Pixel(2)

                        '    Return

                        'End If
                End Select
            ElseIf cbApp.Value = 156 Then
                pnlTokenAddress.Visible = False

                TokenNameTextBox.Text = ""
                TokenAddress1TextBox.Text = ""
                TokenAddress2TextBox.Text = ""

                TokenCityTextBox.Text = ""
                TokenStTextBox.Text = ""
                TokenPhoneTextBox.Text = ""
                TokenZipTextBox.Text = ""
                TokencommentsTextBox.Text = ""

            End If
        Next


    End Sub
    Protected Sub CellPhoneddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles CellPhoneddl.SelectedIndexChanged
        CellProvidertxt.Text = CellPhoneddl.SelectedValue
        If CellProvidertxt.Text.ToLower = "other" Then
            CellPhoneddl.Visible = False

            otherCellprov.BackColor() = Color.Yellow
            otherCellprov.BorderColor() = Color.Black
            otherCellprov.BorderStyle() = BorderStyle.Solid
            otherCellprov.BorderWidth = Unit.Pixel(2)

            otherCellprov.Visible = True

        End If
        ChangColorNoCheck()

    End Sub

    Protected Sub CheckFineMail()

        For Each cbApp As ListItem In cblApplications.Items
            If cbApp.Selected = False Then
                'not select items
                Select Case cbApp.Value
                    Case 110

                        Select Case emp_type_cdrbl.SelectedValue

                            Case "1195", "1198"

                                If e_mailTextBox.Text = "" Then

                                    lblValidation.Text = "Must enter and Email "
                                    lblValidation.ForeColor = Color.Red
                                    lblValidation.Focus()

                                    e_mailTextBox.BackColor() = Color.Yellow
                                    e_mailTextBox.BorderColor() = Color.Black
                                    e_mailTextBox.BorderStyle() = BorderStyle.Solid
                                    e_mailTextBox.BorderWidth = Unit.Pixel(2)

                                End If

                        End Select
                End Select
            End If

        Next
    End Sub
    Protected Sub CheckECare()
        'TCLddl.Visible = False

        gvTCL.Visible = False


        TCLTextBox.Visible = False
        UserTypeDescTextBox.Visible = False
        UserTypeCdTextBox.Visible = False
        Dim TclCount As Integer

        TclCount = gvTCL.Rows.Count()

        For Each cbApp As ListItem In cblApplications.Items
            If cbApp.Selected Then

				Select Case cbApp.Value 'eCare
					Case "43"

						If UserTypeCdTextBox.Text = "" Then
							pnlTCl.Visible = True
							Dim Rolepass As String = ""

							Rolepass = RoleNumLabel.Text
							If RoleNumLabel.Text <> emp_type_cdrbl.SelectedValue Then
								Rolepass = RoleNumLabel.Text
							Else
								Rolepass = emp_type_cdrbl.SelectedValue
							End If



							Select Case emp_type_cdrbl.SelectedValue

								'Case "physician/rovider", "non staff clinician", "resident"  "allied health"

								Case "1193"
									'NeedsProviderrbl.Visible = True
									'DoesNeedprovlbl.Visible = True
									'NeedsProviderrbl.SelectedValue = "No"
									'NeedsProviderrbl.BackColor() = Color.Yellow
									'NeedsProviderrbl.BorderColor() = Color.Black
									'NeedsProviderrbl.BorderStyle() = BorderStyle.Solid
									'NeedsProviderrbl.BorderWidth = Unit.Pixel(2)

									Dim tcldata As New CommandsSqlAndOleDb("SelInvisionUserTypeV2", Session("EmployeeConn"))
									tcldata.AddSqlProcParameter("@rolenum", Rolepass, SqlDbType.NVarChar, 8)

									tcldata.GetSqlDataset()




									dtTCLUserTypes = tcldata.GetSqlDataTable

									If dtTCLUserTypes.Rows.Count = 1 Then
										'TCLddl.SelectedIndex = 0

										TCLTextBox.Text = dtTCLUserTypes.Rows(0)("TCL").ToString
										UserTypeDescTextBox.Text = dtTCLUserTypes.Rows(0)("UserTypeDesc").ToString
										UserTypeCdTextBox.Text = dtTCLUserTypes.Rows(0)("UserTypeCd").ToString
										'TCLddl.Visible = False

										gvTCL.Visible = False

										pnlTCl.Visible = True
										TCLTextBox.Visible = True
										UserTypeDescTextBox.Visible = True
										UserTypeCdTextBox.Visible = True


									ElseIf dtTCLUserTypes.Rows.Count > 1 Then

										gvTCL.DataSource = dtTCLUserTypes
										gvTCL.DataBind()

										gvTCL.Visible = True
										gvTCL.BackColor() = Color.Yellow
										gvTCL.BorderColor() = Color.Black
										gvTCL.BorderStyle() = BorderStyle.Solid
										gvTCL.BorderWidth = Unit.Pixel(2)
										gvTCL.RowStyle.BackColor() = Color.Yellow

										pnlTCl.Visible = True
										TCLTextBox.Visible = True
										UserTypeDescTextBox.Visible = True
										UserTypeCdTextBox.Visible = True

									ElseIf dtTCLUserTypes.Rows.Count = 0 Then
										pnlTCl.Visible = False

									End If

									'TCLddl.DataSource = dtTCLUserTypes

									'TCLddl.DataTextField = "UserTypeDesc"
									'TCLddl.DataValueField = "TCL"
									'TCLddl.DataBind()

									'TCLddl.Items.Insert(0, "Select")

									'TCLddl.Visible = True


									'Dim ddTCLBinder As New DropDownListBinder(TCLddl, "SelInvisionTCL", Session("EmployeeConn"))
									'ddTCLBinder.AddDDLCriteria("@rolenum", emp_type_cdrbl.SelectedValue, SqlDbType.NVarChar)

									'ddTCLBinder.BindData("TCL", "UserTypeDesc")

								Case "782"
									If UserTypeCdTextBox.Text = "" And TclCount = 0 Then
										pnlTCl.Visible = False


									ElseIf TclCount > 0 Then

										TCLTextBox.Visible = True
										UserTypeDescTextBox.Visible = True
										UserTypeCdTextBox.Visible = True
										gvTCL.Visible = True

										pnlTCl.Visible = True
									ElseIf UserTypeCdTextBox.Text <> "" Then
										TCLTextBox.Visible = True
										UserTypeDescTextBox.Visible = True
										UserTypeCdTextBox.Visible = True
										gvTCL.Visible = False

										pnlTCl.Visible = True


									End If

									' Case "1195", "1196", "1189"

									'NeedsProviderrbl.Visible = True
									'DoesNeedprovlbl.Visible = True
									'NeedsProviderrbl.SelectedValue = "No"
									'NeedsProviderrbl.BackColor() = Color.Yellow
									'NeedsProviderrbl.BorderColor() = Color.Black
									'NeedsProviderrbl.BorderStyle() = BorderStyle.Solid
									'NeedsProviderrbl.BorderWidth = Unit.Pixel(2)

								Case Else
									If Rolepass = "1200" Then
										Rolepass = "918"
									End If

									Dim tcldata As New CommandsSqlAndOleDb("SelInvisionUserTypeV2", Session("EmployeeConn"))
									tcldata.AddSqlProcParameter("@rolenum", Rolepass, SqlDbType.NVarChar, 8)

									tcldata.GetSqlDataset()




									dtTCLUserTypes = tcldata.GetSqlDataTable

									If dtTCLUserTypes.Rows.Count = 1 Then
										'TCLddl.SelectedIndex = 0

										TCLTextBox.Text = dtTCLUserTypes.Rows(0)("TCL").ToString
										UserTypeDescTextBox.Text = dtTCLUserTypes.Rows(0)("UserTypeDesc").ToString
										UserTypeCdTextBox.Text = dtTCLUserTypes.Rows(0)("UserTypeCd").ToString
										'TCLddl.Visible = False

										gvTCL.Visible = False

										pnlTCl.Visible = True
										TCLTextBox.Visible = True
										UserTypeDescTextBox.Visible = True
										UserTypeCdTextBox.Visible = True


									ElseIf dtTCLUserTypes.Rows.Count > 1 Then

										gvTCL.DataSource = dtTCLUserTypes
										gvTCL.DataBind()

										gvTCL.Visible = True
										gvTCL.BackColor() = Color.Yellow
										gvTCL.BorderColor() = Color.Black
										gvTCL.BorderStyle() = BorderStyle.Solid
										gvTCL.BorderWidth = Unit.Pixel(2)
										gvTCL.RowStyle.BackColor() = Color.Yellow

										pnlTCl.Visible = True
										TCLTextBox.Visible = True
										UserTypeDescTextBox.Visible = True
										UserTypeCdTextBox.Visible = True

									ElseIf dtTCLUserTypes.Rows.Count = 0 Then
										pnlTCl.Visible = False

									End If

									'TCLTextBox.Visible = True
									'UserTypeCdTextBox.Visible = True
									'UserTypeDescTextBox.Visible = True
									'gvTCL.Visible = True
							End Select


							Return
						ElseIf UserTypeCdTextBox.Text <> "" Or TclCount > 0 Then
							TCLTextBox.Visible = True
							UserTypeDescTextBox.Visible = True
							UserTypeCdTextBox.Visible = True

						End If

						'Case "136"
						'	Load365Locations()
						'	O365Panel.Visible = True



				End Select

			End If

        Next

		'Select Case cbApp.ToString.ToLower.Trim
		'	Case "ecare"

		'		If UserTypeCdTextBox.Text = "" Then
		'			pnlTCl.Visible = True
		'			Dim Rolepass As String = ""

		'			Rolepass = RoleNumLabel.Text
		'			If RoleNumLabel.Text <> emp_type_cdrbl.SelectedValue Then
		'				Rolepass = RoleNumLabel.Text
		'			Else
		'				Rolepass = emp_type_cdrbl.SelectedValue
		'			End If



		'			Select Case emp_type_cdrbl.SelectedValue

		'						'Case "physician/rovider", "non staff clinician", "resident"  "allied health"

		'				Case "1193"

		'					Dim tcldata As New CommandsSqlAndOleDb("SelInvisionUserTypeV2", Session("EmployeeConn"))
		'					tcldata.AddSqlProcParameter("@rolenum", Rolepass, SqlDbType.NVarChar, 8)

		'					tcldata.GetSqlDataset()

		'					dtTCLUserTypes = tcldata.GetSqlDataTable

		'					If dtTCLUserTypes.Rows.Count = 1 Then
		'						'TCLddl.SelectedIndex = 0

		'						TCLTextBox.Text = dtTCLUserTypes.Rows(0)("TCL").ToString
		'						UserTypeDescTextBox.Text = dtTCLUserTypes.Rows(0)("UserTypeDesc").ToString
		'						UserTypeCdTextBox.Text = dtTCLUserTypes.Rows(0)("UserTypeCd").ToString
		'						'TCLddl.Visible = False

		'						gvTCL.Visible = False

		'						pnlTCl.Visible = True
		'						TCLTextBox.Visible = True
		'						UserTypeDescTextBox.Visible = True
		'						UserTypeCdTextBox.Visible = True


		'					ElseIf dtTCLUserTypes.Rows.Count > 1 Then

		'						gvTCL.DataSource = dtTCLUserTypes
		'						gvTCL.DataBind()

		'						gvTCL.Visible = True
		'						gvTCL.BackColor() = Color.Yellow
		'						gvTCL.BorderColor() = Color.Black
		'						gvTCL.BorderStyle() = BorderStyle.Solid
		'						gvTCL.BorderWidth = Unit.Pixel(2)
		'						gvTCL.RowStyle.BackColor() = Color.Yellow

		'						pnlTCl.Visible = True
		'						TCLTextBox.Visible = True
		'						UserTypeDescTextBox.Visible = True
		'						UserTypeCdTextBox.Visible = True

		'					ElseIf dtTCLUserTypes.Rows.Count = 0 Then
		'						pnlTCl.Visible = False

		'					End If


		'				Case "782"
		'					If UserTypeCdTextBox.Text = "" And TclCount = 0 Then
		'						pnlTCl.Visible = False


		'					ElseIf TclCount > 0 Then

		'						TCLTextBox.Visible = True
		'						UserTypeDescTextBox.Visible = True
		'						UserTypeCdTextBox.Visible = True
		'						gvTCL.Visible = True

		'						pnlTCl.Visible = True
		'					ElseIf UserTypeCdTextBox.Text <> "" Then
		'						TCLTextBox.Visible = True
		'						UserTypeDescTextBox.Visible = True
		'						UserTypeCdTextBox.Visible = True
		'						gvTCL.Visible = False

		'						pnlTCl.Visible = True


		'					End If

		'				Case Else
		'					If Rolepass = "1200" Then
		'						Rolepass = "918"
		'					End If

		'					Dim tcldata As New CommandsSqlAndOleDb("SelInvisionUserTypeV2", Session("EmployeeConn"))
		'					tcldata.AddSqlProcParameter("@rolenum", Rolepass, SqlDbType.NVarChar, 8)

		'					tcldata.GetSqlDataset()


		'					dtTCLUserTypes = tcldata.GetSqlDataTable

		'					If dtTCLUserTypes.Rows.Count = 1 Then
		'						'TCLddl.SelectedIndex = 0

		'						TCLTextBox.Text = dtTCLUserTypes.Rows(0)("TCL").ToString
		'						UserTypeDescTextBox.Text = dtTCLUserTypes.Rows(0)("UserTypeDesc").ToString
		'						UserTypeCdTextBox.Text = dtTCLUserTypes.Rows(0)("UserTypeCd").ToString
		'						'TCLddl.Visible = False

		'						gvTCL.Visible = False

		'						pnlTCl.Visible = True
		'						TCLTextBox.Visible = True
		'						UserTypeDescTextBox.Visible = True
		'						UserTypeCdTextBox.Visible = True


		'					ElseIf dtTCLUserTypes.Rows.Count > 1 Then

		'						gvTCL.DataSource = dtTCLUserTypes
		'						gvTCL.DataBind()

		'						gvTCL.Visible = True
		'						gvTCL.BackColor() = Color.Yellow
		'						gvTCL.BorderColor() = Color.Black
		'						gvTCL.BorderStyle() = BorderStyle.Solid
		'						gvTCL.BorderWidth = Unit.Pixel(2)
		'						gvTCL.RowStyle.BackColor() = Color.Yellow

		'						pnlTCl.Visible = True
		'						TCLTextBox.Visible = True
		'						UserTypeDescTextBox.Visible = True
		'						UserTypeCdTextBox.Visible = True

		'					ElseIf dtTCLUserTypes.Rows.Count = 0 Then
		'						pnlTCl.Visible = False

		'					End If

		'			End Select


		'			Return
		'		ElseIf UserTypeCdTextBox.Text <> "" Or TclCount > 0 Then
		'			TCLTextBox.Visible = True
		'			UserTypeDescTextBox.Visible = True
		'			UserTypeCdTextBox.Visible = True

		'		End If


		'End Select

	End Sub
    Protected Sub ClaimMgtrbl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ClaimMgtrbl.SelectedIndexChanged
        ClaimMgtID.Text = ClaimMgtrbl.SelectedItem.ToString
        ClaimMgtGroupNum.Text = ClaimMgtrbl.SelectedValue.ToString

    End Sub


    Protected Sub PAtientAcctrbl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles PAtientAcctrbl.SelectedIndexChanged
        PatientAcctID.Text = PAtientAcctrbl.SelectedItem.ToString
        PatientAcctGroupNum.Text = PAtientAcctrbl.SelectedValue.ToString

        ptamountlbl.Visible = True
        PatientAcctddl.Visible = True
        'patamount.Visible = True

    End Sub

    Protected Sub PatientAcctddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles PatientAcctddl.SelectedIndexChanged
        If PatientAcctddl.SelectedIndex > 0 Then
            patamount.Text = PatientAcctddl.SelectedItem.Text

        End If
    End Sub

    Protected Sub ScISchedulerbl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ScISchedulerbl.SelectedIndexChanged
        SCIid.Text = ScISchedulerbl.SelectedItem.ToString
        SCIGroupNum.Text = ScISchedulerbl.SelectedValue.ToString
    End Sub

    Protected Sub CarePaymentrbl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles CarePaymentrbl.SelectedIndexChanged
        CarePayID.Text = CarePaymentrbl.SelectedItem.ToString
        CarePayGroupNum.Text = CarePaymentrbl.SelectedValue.ToString
    End Sub

    Private Sub insAcctItems(ByVal requestNum As Integer, ByVal appNum As String, ByVal comments As String)

        Dim dt As New DataTable
        Dim thisdata As New CommandsSqlAndOleDb("InsAccountItemsV3", Session("EmployeeConn"))
        thisdata.AddSqlProcParameter("@account_request_num", requestNum, SqlDbType.Int, 20)
        thisdata.AddSqlProcParameter("@ApplicationNum", appNum, SqlDbType.Int, 8)

        Select Case emp_type_cdrbl.SelectedValue
            'Case "physician/rovider", "non staff clinician", "resident"  "allied health"

            Case "782", "1197", "1194"

                thisdata.AddSqlProcParameter("@validate", "NeedsDocNum", SqlDbType.NVarChar, 20)

            Case Else
                'thisdata.AddSqlProcParameter("@validate", "NeedsValidation", SqlDbType.NVarChar, 20)

                If NeedsProviderrbl.SelectedValue = "Yes" Then
                    thisdata.AddSqlProcParameter("@validate", "NeedsDocNum", SqlDbType.NVarChar, 20)

                Else
                    thisdata.AddSqlProcParameter("@validate", "NeedsValidation", SqlDbType.NVarChar, 20)

                End If

        End Select
        Select Case appNum
            Case "7"

                thisdata.AddSqlProcParameter("@account_item_desc", comments, SqlDbType.NVarChar, 255)
            Case "106", "107", "108"

                If PatientAcctGroupNum.Text = "2" Then
                    thisdata.AddSqlProcParameter("@GroupDesc", patamount.Text, SqlDbType.NVarChar, 35)
                Else
                    thisdata.AddSqlProcParameter("@GroupDesc", PatientAcctID.Text, SqlDbType.NVarChar, 35)
                End If

                thisdata.AddSqlProcParameter("@ApplicationGroupNum", PatientAcctGroupNum.Text, SqlDbType.NVarChar, 25)

            Case "117"

                thisdata.AddSqlProcParameter("@GroupDesc", ClaimMgtID.Text, SqlDbType.NVarChar, 35)
                thisdata.AddSqlProcParameter("@ApplicationGroupNum", ClaimMgtGroupNum.Text, SqlDbType.NVarChar, 20)
            Case "118"
                thisdata.AddSqlProcParameter("@GroupDesc", CarePayID.Text, SqlDbType.NVarChar, 35)
                thisdata.AddSqlProcParameter("@ApplicationGroupNum", CarePayGroupNum.Text, SqlDbType.NVarChar, 25)

            Case "119"
                thisdata.AddSqlProcParameter("@GroupDesc", SCIid.Text, SqlDbType.NVarChar, 35)
                thisdata.AddSqlProcParameter("@ApplicationGroupNum", SCIGroupNum.Text, SqlDbType.NVarChar, 25)


        End Select

        thisdata.AddSqlProcParameter("@who_nt_login", Session("LoginID"), SqlDbType.NVarChar, 50)

        thisdata.ExecNonQueryNoReturn()



    End Sub
    Protected Sub CheckEntitiesHosp()
        '' check all entities 
        For Each cbentities As ListItem In cblEntities.Items
            If cbentities.Selected = False Then
                cbentities.Selected = True
            End If
        Next


        ' check all hospitals
        For Each cbHospitals As ListItem In cblFacilities.Items
            Dim sCurrHospital As String = cbHospitals.ToString

            If cbHospitals.Selected = False And sCurrHospital.ToString.ToLower <> "community" Then
                cbHospitals.Selected = True

            End If
        Next

    End Sub

    Protected Sub Rolesddl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Rolesddl.SelectedIndexChanged

        lblValidation.Text = ""

        For Each cblItem As ListItem In cblApplications.Items
            cblItem.Selected = False
            'cblItem.Enabled = False
        Next


        If Session("SecurityLevelNum") < 70 Then
            For Each cblItem As ListItem In cblApplications.Items

                cblItem.Enabled = False
            Next

        End If

        For Each cblEntitem As ListItem In cblEntities.Items
            cblEntitem.Selected = False
        Next

        For Each cblFacItem As ListItem In cblFacilities.Items
            cblFacItem.Selected = False
        Next

        pnlTCl.Visible = False
        TCLTextBox.Text = ""
        UserTypeCdTextBox.Text = ""
        UserTypeDescTextBox.Text = ""



        Dim rwRef() As DataRow
        Dim ddlBinderV2 As DropDownListBinder

        'always use the Main Role
        mainRole = emp_type_cdrbl.SelectedValue


        request_descTextBox.Text = ""
        request_descTextBox.BackColor() = Color.White
        request_descTextBox.BorderStyle = BorderStyle.None
        request_descTextBox.BorderWidth = Unit.Pixel(2)

        If Rolesddl.SelectedValue.ToString <> "Select" Then
            user_position_descTextBox.Text = Rolesddl.SelectedValue
            mainRole = Rolesddl.SelectedValue

            Title2TextBox.Text = Rolesddl.SelectedItem.ToString
            If thisRole Is Nothing Then
                thisRole = Rolesddl.SelectedValue
            End If
        ElseIf Rolesddl.SelectedValue.ToString = "Select" And mainRole <> Nothing Then
            ddlBinderV2 = New DropDownListBinder(Rolesddl, "SelEmployeeRolesV2", Session("EmployeeConn"))

            ddlBinderV2.AddDDLCriteria("@RoleNum", mainRole, SqlDbType.NVarChar)
            ddlBinderV2.BindData("rolenum", "roledesc")

            Rolesddl.SelectedValue = mainRole

        End If
        ' ADD get Table here
        Dim thisdata As New CommandsSqlAndOleDb("SelApplicationExtensionByRoleNum", Session("EmployeeConn"))

        If Rolesddl.SelectedValue.ToString = "Select" And thisRole <> "" Then
            Rolesddl.SelectedValue = thisRole
        End If

        RoleNumLabel.Text = thisRole

        thisdata.AddSqlProcParameter("@roleNum", Rolesddl.SelectedValue.ToString, SqlDbType.NVarChar, 9)
        thisdata.AddSqlProcParameter("@display", "base", SqlDbType.NVarChar, 9)


        thisdata.ExecNonQueryNoReturn()

        dtApplications = thisdata.GetSqlDataTable

        Dim Appnum As Integer
        Dim appDeafult As String
        Dim snotice As String = ""

        For Each row As DataRow In dtApplications.Rows
            Appnum = row("ApplicationNum")
            appDeafult = row("DefaultValue")
            gAppName = row("ApplicationDesc")



            For Each cblItem As ListItem In cblApplications.Items
                Dim sCurritem As String = cblItem.Value

                If sCurritem = Appnum And appDeafult.ToLower = "yes" Then
                    cblItem.Selected = True


                    cblItem.Enabled = True
                End If
                If sCurritem = Appnum And appDeafult.ToLower = "no" Then
                    cblItem.Enabled = True
                    cblItem.Attributes.Add("Style", "color:blue;")



                End If

                If cblItem.ToString.ToLower = "mak" And cblItem.Selected = True Then

                    snotice = "true"
                End If
            Next
        Next row

        If snotice = "true" Then
            If request_descTextBox.Text = "" Then
                request_descTextBox.Text = "Use This Mak ID ="
            Else
                request_descTextBox.Text = request_descTextBox.Text & " Please Add Mak ID "
            End If

            request_descTextBox.BackColor() = Color.Yellow
            request_descTextBox.BorderColor() = Color.Black
            request_descTextBox.BorderStyle() = BorderStyle.Solid
            request_descTextBox.BorderWidth = Unit.Pixel(2)
        End If



        Dim BusLog As New AccountBusinessLogic

        If BusLog.eCare Then
            pnlTCl.Visible = True
        Else
            pnlTCl.Visible = False
            pageCtrl.RemoveCssClass(pnlTCl, "textInputRequired")

        End If
        CheckForFinancial()
        CheckECare()
        CheckforToken()

        Select Case RoleNumLabel.Text
            Case "782", "1195", "1199", "1193"


            Case Else

                If RoleNumLabel.Text <> "" Then

                    Dim tcldata As New CommandsSqlAndOleDb("SelInvisionUserTypeV2", Session("EmployeeConn"))
                    tcldata.AddSqlProcParameter("@rolenum", RoleNumLabel.Text, SqlDbType.NVarChar, 8)

                    tcldata.GetSqlDataset()




                    dtTCLUserTypes = tcldata.GetSqlDataTable


                End If

                If dtTCLUserTypes.Rows.Count = 1 Then
                    'TCLddl.SelectedIndex = 0

                    TCLTextBox.Text = dtTCLUserTypes.Rows(0)("TCL").ToString

                    UserTypeDescTextBox.Text = dtTCLUserTypes.Rows(0)("UserTypeDesc").ToString
                    UserTypeCdTextBox.Text = dtTCLUserTypes.Rows(0)("UserTypecd").ToString

                    pnlTCl.Visible = True

                    TCLTextBox.Visible = True
                    UserTypeDescTextBox.Visible = True
                    UserTypeCdTextBox.Visible = True

                    If masterRole.Text = "1193" Then
                        btnChangeTCL.Visible = True
                        btnChangeTCL.Width = Unit.Pixel("200")



                        btnChangeTCL.Text = "Show All User Types"

                    End If


                ElseIf dtTCLUserTypes.Rows.Count = 0 Then
                    pnlTCl.Visible = False

                Else
                    gvTCL.DataSource = dtTCLUserTypes
                    gvTCL.DataBind()

                    gvTCL.Visible = True
                    gvTCL.BackColor() = Color.Yellow
                    gvTCL.BorderColor() = Color.Black
                    gvTCL.BorderStyle() = BorderStyle.Solid
                    gvTCL.BorderWidth = Unit.Pixel(2)
                    gvTCL.RowStyle.BackColor() = Color.Yellow

                    pnlTCl.Visible = True
                    TCLTextBox.Visible = True
                    UserTypeDescTextBox.Visible = True
                    UserTypeCdTextBox.Visible = True

                    If masterRole.Text = "1193" Then
                        btnChangeTCL.Visible = True
                        btnChangeTCL.Text = "Show All User Types"
                        btnChangeTCL.Width = Unit.Pixel("200")

                    End If

                End If


        End Select


        Select Case mainRole
            Case "1194"
                Titleddl.SelectedValue = Title2TextBox.Text
                'Title2TextBox.Text
                TitleLabel.Text = Titleddl.SelectedItem.ToString

                'Title2TextBox.Text = Titleddl.SelectedItem.ToString

                Title2TextBox.Visible = True
                Titleddl.Visible = False

            Case "1190", "1191", "1192"
                CheckEntitiesHosp()

                SuppSupportrbl.SelectedValue = "Yes"

                entity_cdDDL.SelectedValue = "150"

                Dim ddbDepartmentBinder As New DropDownListBinder(department_cdddl, "selDepartmentCodesV4", Session("EmployeeConn"))
                ddbDepartmentBinder.AddDDLCriteria("@entity_cd", "150", SqlDbType.NVarChar)
                ddbDepartmentBinder.BindData("department_cd", "department_name")

                department_cdddl.SelectedValue = "601000"

            Case "1554"

                Dim ddbVendorBinder As New DropDownListBinder(Vendorddl, "SelVendors", Session("EmployeeConn"))
                ddbVendorBinder.AddDDLCriteria("@type", "prospect", SqlDbType.NVarChar)

                ddbVendorBinder.BindData("VendorNum", "VendorName")

            Case "1423", "1559"
                VendorNameTextBox.Visible = False

                Dim ddbVendorBinder As New DropDownListBinder(Vendorddl, "SelVendors", Session("EmployeeConn"))
                ddbVendorBinder.AddDDLCriteria("@type", "vendor", SqlDbType.NVarChar)

                ddbVendorBinder.BindData("VendorNum", "VendorName")

            Case Else
                entity_cdDDL.SelectedIndex = -1
                department_cdddl.SelectedIndex = -1
                SuppSupportrbl.SelectedIndex = -1

        End Select

        Dim dtCernerPosition As New DataTable

        Dim positiondata As New CommandsSqlAndOleDb("SelCernerPositionDesc", Session("EmployeeConn"))

        If Rolesddl.SelectedValue.ToString = "Select" And thisRole <> "" Then
            Rolesddl.SelectedValue = thisRole
        End If
        positiondata.AddSqlProcParameter("@roleNum", Rolesddl.SelectedValue.ToString, SqlDbType.NVarChar, 9)

        dtCernerPosition = positiondata.GetSqlDataTable

        CernerPositionDescTextBox.Text = dtCernerPosition.Rows(0)("CernerPositionDesc").ToString
        UserCategoryCdTextBox.Text = dtCernerPosition.Rows(0)("UserCategoryCd").ToString

        Dim CerPosdataddl As New DropDownListBinder(CernerPosddl, "SelCernerPositionDesc", Session("EmployeeConn"))
        CerPosdataddl.AddDDLCriteria("@UserTypeCd", UserCategoryCdTextBox.Text, SqlDbType.NVarChar)

        CerPosdataddl.BindData("CernerPositionDesc", "CernerPositionDesc")

        'ChangColorNoCheck()

        ChangColor()
    End Sub

    Protected Function getApplicationsByRole() As DataTable
        'Use to exec SelApplicationsByRole
        Dim thisdata As New CommandsSqlAndOleDb("SelApplicationExtensionByRoleNum", Session("EmployeeConn"))

        If Rolesddl.SelectedValue.ToString = "Select" And thisRole <> "" Then
            Rolesddl.SelectedValue = thisRole
        End If

        thisdata.AddSqlProcParameter("@roleNum", Rolesddl.SelectedValue.ToString, SqlDbType.NVarChar, 9)
        thisdata.AddSqlProcParameter("@display", "base", SqlDbType.NVarChar, 9)



        thisdata.ExecNonQueryNoReturn()

        Return thisdata.GetSqlDataTable

    End Function
    Protected Sub btnReturn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReturn.Click
        '  BusinessLogic()
        If ReactivateClientNum > 0 Then
            ' submit Cancel Rehire Request

            Dim thisdata As New CommandsSqlAndOleDb("UpdRehirRequest", Session("EmployeeConn"))
            thisdata.AddSqlProcParameter("@client_num", ReactivateClientNum, SqlDbType.Int, 9)
            thisdata.AddSqlProcParameter("@rehireRequestNum", ReHireRequestNum, SqlDbType.Int, 9)
            thisdata.AddSqlProcParameter("@rehireStatus", "canceled", SqlDbType.NVarChar, 12)

            thisdata.ExecNonQueryNoReturn()
        End If

        If AccountRequestNum > 0 Then
            Response.Redirect("Myrequest.aspx")
        Else
            Response.Redirect("SimpleSearch.aspx")

        End If


    End Sub
	Protected Sub LoadEmployeeType()
		pnlDemo.Enabled = True

		pnlTCl.Visible = False
		btnChangeTCL.Visible = False
		btnChangeTCL.Text = "Change TCL"

		'NeedsProviderrbl.Visible = False
		'DoesNeedprovlbl.Visible = False


		VendorNameTextBox.Text = ""
		address_1textbox.Text = ""
		citytextbox.Text = ""
		statetextbox.Text = ""
		ziptextbox.Text = ""
		phoneTextBox.Text = ""

		Dim removeCss As New PageControls(Page)
		removeCss.RemoveCssClass(Page, "textInputRequired")


        emp_type_cdLabel.Text = emp_type_cdrbl.SelectedItem.ToString
		RoleNumLabel.Text = emp_type_cdrbl.SelectedValue

		' retain Employee Master Role
		masterRole.Text = emp_type_cdrbl.SelectedValue

		'always use the Main Role
		mainRole = emp_type_cdrbl.SelectedValue

		'srolenum = emp_type_cdrbl.SelectedItem
		Dim ddlBinderV2 As DropDownListBinder

		ddlBinderV2 = New DropDownListBinder(Rolesddl, "SelEmployeeRolesV2", Session("EmployeeConn"))

		ddlBinderV2.AddDDLCriteria("@RoleNum", mainRole, SqlDbType.NVarChar)
		ddlBinderV2.BindData("rolenum", "roledesc")


		Select Case emp_type_cdrbl.SelectedValue
			'Case "physician/rovider", "non staff clinician", "resident"

			Case "782", "1197"

				empLbl.Visible = True
				AuthorizationEmpNumTextBox.Visible = True


				facility_cdddl.BackColor() = Color.White
				facility_cdddl.BorderColor() = Color.Black
				facility_cdddl.BorderStyle() = BorderStyle.Groove
				facility_cdddl.BorderWidth = Unit.Pixel(1)

				building_cdddl.BackColor() = Color.White
				building_cdddl.BorderColor() = Color.Black
				building_cdddl.BorderStyle() = BorderStyle.Groove
				building_cdddl.BorderWidth = Unit.Pixel(1)

				Dim ddbTitleBinder As New DropDownListBinder(Titleddl, "SelTitlesV4", Session("EmployeeConn"))
				ddbTitleBinder.AddDDLCriteria("@titleType", "Physician", SqlDbType.NVarChar)
				ddbTitleBinder.BindData("TitleShortDesc", "TitleDesc")

				Titleddl.SelectedIndex = -1


				Titleddl.BackColor() = Color.Yellow
				Titleddl.BorderColor() = Color.Black
				Titleddl.BorderStyle() = BorderStyle.Solid
				Titleddl.BorderWidth = Unit.Pixel(2)

				SuppSupportrbl.Enabled = False
				Specialtyddl.SelectedIndex = -1


                'Session("LoginID") = "melej" Or

                If Session("SecurityLevelNum") = 82 Or Session("LoginID") = "geip00" Or Session("LoginID") = "melej" Then
                    credentialedDateTextBox.Enabled = True
                    credentialedDateTextBox.CssClass = "calenderClass"

                    'CredentialedDateDCMHTextBox.CssClass = "calenderClass"
                    'CredentialedDateDCMHTextBox.Enabled = True

                End If



                thisRole = emp_type_cdrbl.SelectedValue
				mainRole = emp_type_cdrbl.SelectedValue

				Title2TextBox.Visible = False
				Titleddl.Visible = True

				SpecialtLabel.Visible = False
				Specialtyddl.Visible = True

				pnlProvider.Visible = True


				CCMCadmitRightsrbl.SelectedIndex = -1
				CCMCadmitRightsrbl.Enabled = True

                'DCMHadmitRightsrbl.SelectedIndex = -1
                'DCMHadmitRightsrbl.Enabled = True

                'Case "resident"
            Case "1194"
				'Dim ddlBinder As New DropDownListBinder
				SuppSupportrbl.Enabled = False

				empLbl.Visible = True
				AuthorizationEmpNumTextBox.Visible = True

				Dim ddbTitleBinder As New DropDownListBinder(Titleddl, "SelTitlesV4", Session("EmployeeConn"))
				ddbTitleBinder.AddDDLCriteria("@titleType", "Resident", SqlDbType.NVarChar)
				ddbTitleBinder.BindData("TitleShortDesc", "TitleDesc")

				Dim ddlRoleBinder As New DropDownListBinder(Rolesddl, "SelEmployeeRolesV2", Session("EmployeeConn"))
				ddlRoleBinder.AddDDLCriteria("@RoleNum", "1194", SqlDbType.NVarChar)
				ddlRoleBinder.BindData("rolenum", "RoleDesc")

				'If TitleLabel.Text = "" Then
				'    Titleddl.SelectedIndex = -1
				'Else
				'    Titleddl.SelectedValue = TitleLabel.Text

				'End If


				'If TitleLabel.Text = "" Then

				'    Title2TextBox.Text = "Resident"
				'    Title2TextBox.Visible = True


				'End If

				TitleLabel.Text = "Resident"

				Title2TextBox.Text = "Resident"
				Title2TextBox.Visible = True
				Titleddl.Visible = False

				SpecialtLabel.Text = "Resident"
				SpecialtLabel.Visible = True
				Specialtyddl.Visible = False

				SuppSupportrbl.Enabled = False


				credentialedDateTextBox.Enabled = False
				credentialedDateTextBox.CssClass = "style2"

                'CredentialedDateDCMHTextBox.Enabled = False
                'CredentialedDateDCMHTextBox.CssClass = "style2"

                CCMCadmitRightsrbl.SelectedValue = "No"
				CCMCadmitRightsrbl.Enabled = False

                'DCMHadmitRightsrbl.SelectedValue = "No"
                'DCMHadmitRightsrbl.Enabled = False

                thisRole = emp_type_cdrbl.SelectedValue
				mainRole = emp_type_cdrbl.SelectedValue

				Rolesddl.SelectedIndex = -1

				'SelectedValue.ToString = "Select"

				pnlProvider.Visible = True


			Case "1195", "1198"
				' load vendor list 
				'make vendor text box invisiable
				' make ddl appear
				' 1195 Contractor list
				' 1198 Vendor list

				empLbl.Visible = False
				AuthorizationEmpNumTextBox.Visible = False


				SuppSupportrbl.Enabled = False

				Vendorddl.Visible = True

				VendorNameTextBox.Visible = False

				Dim ddbVendorBinder As New DropDownListBinder(Vendorddl, "SelVendors", Session("EmployeeConn"))

				If mainRole = "1554" Then
					ddbVendorBinder.AddDDLCriteria("@type", "prospect", SqlDbType.NVarChar)

				Else
					ddbVendorBinder.AddDDLCriteria("@type", "vendor", SqlDbType.NVarChar)


				End If
				ddbVendorBinder.BindData("VendorNum", "VendorName")
				thisRole = emp_type_cdrbl.SelectedValue
				mainRole = emp_type_cdrbl.SelectedValue


			Case "1193", "1200"
				'NeedsProviderrbl.Visible = True
				'DoesNeedprovlbl.Visible = True

				Title2TextBox.Visible = False
				Titleddl.Visible = True

				SpecialtLabel.Visible = False
				Specialtyddl.Visible = True

				pnlProvider.Visible = False

				thisRole = emp_type_cdrbl.SelectedValue
				mainRole = emp_type_cdrbl.SelectedValue

				empLbl.Visible = True
				AuthorizationEmpNumTextBox.Visible = True

			Case "1196", "1199"

				empLbl.Visible = False
				AuthorizationEmpNumTextBox.Visible = False
				SuppSupportrbl.Enabled = False


			Case Else
				Title2TextBox.Visible = False
				Titleddl.Visible = True

				SpecialtLabel.Visible = False
				Specialtyddl.Visible = True

				pnlProvider.Visible = False

				thisRole = emp_type_cdrbl.SelectedValue
				mainRole = emp_type_cdrbl.SelectedValue

				SuppSupportrbl.Enabled = False


		End Select


        CheckRequiredFields()

		If bHasError = True Then
			lblValidation.Focus()
			Exit Sub
		Else
			lblValidation.Text = ""
		End If


		'Rolesddl.SelectedValue = thisRole

		'dont select role if Resident
		If thisRole <> "1194" Then
			' this kicks off selecting applications that will be selected for that role
			Rolesddl_SelectedIndexChanged(thisRole, EventArgs.Empty)
		End If


	End Sub
	Private Sub insPMHAddedItems(ByVal requestNum As Integer)
		Dim thisdata As New CommandsSqlAndOleDb("InsAccountReqActionItemV9", Session("EmployeeConn"))
		thisdata.AddSqlProcParameter("@account_request_num", requestNum, SqlDbType.Int, 20)
		'@actionCD
		thisdata.AddSqlProcParameter("@actionCD", "itemsadded", SqlDbType.NVarChar, 20)

		thisdata.AddSqlProcParameter("@who_nt_login", Session("LoginID"), SqlDbType.NVarChar, 50)

		thisdata.ExecNonQueryNoReturn()

	End Sub
	Protected Sub emp_type_cdrbl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles emp_type_cdrbl.SelectedIndexChanged
        pnlDemo.Enabled = True

        For Each cblItem As ListItem In cblApplications.Items
            cblItem.Selected = False
        Next
        For Each cblEntitem As ListItem In cblEntities.Items
            cblEntitem.Selected = False
        Next

        For Each cblFacItem As ListItem In cblFacilities.Items
            cblFacItem.Selected = False
        Next

        pnlTCl.Visible = False
        btnChangeTCL.Visible = False
        btnChangeTCL.Text = "Change TCL"

        'NeedsProviderrbl.Visible = False
        'DoesNeedprovlbl.Visible = False


        VendorNameTextBox.Text = ""
        address_1textbox.Text = ""
        citytextbox.Text = ""
        statetextbox.Text = ""
        ziptextbox.Text = ""
        phoneTextBox.Text = ""

        Dim removeCss As New PageControls(Page)
        removeCss.RemoveCssClass(Page, "textInputRequired")


        emp_type_cdLabel.Text = emp_type_cdrbl.SelectedItem.ToString
        RoleNumLabel.Text = emp_type_cdrbl.SelectedValue
        'Dim srolenum As String
        If masterRole.Text <> "" Then
            Response.Redirect("SignOnFormFinanical.aspx")
            Exit Sub

        End If

        ' retain Employee Master Role
        masterRole.Text = emp_type_cdrbl.SelectedValue

        'always use the Main Role
        mainRole = emp_type_cdrbl.SelectedValue

        'srolenum = emp_type_cdrbl.SelectedItem
        Dim ddlBinderV2 As DropDownListBinder

        ddlBinderV2 = New DropDownListBinder(Rolesddl, "SelEmployeeRolesV2", Session("EmployeeConn"))

        ddlBinderV2.AddDDLCriteria("@RoleNum", mainRole, SqlDbType.NVarChar)
        ddlBinderV2.BindData("rolenum", "roledesc")

        user_position_descTextBox.Text = emp_type_cdrbl.SelectedValue

        Select Case emp_type_cdrbl.SelectedValue
            'Case "physician/rovider", "non staff clinician", "resident"

            Case "782", "1197"

                facility_cdddl.BackColor() = Color.White
                facility_cdddl.BorderColor() = Color.Black
                facility_cdddl.BorderStyle() = BorderStyle.Groove
                facility_cdddl.BorderWidth = Unit.Pixel(1)

                building_cdddl.BackColor() = Color.White
                building_cdddl.BorderColor() = Color.Black
                building_cdddl.BorderStyle() = BorderStyle.Groove
                building_cdddl.BorderWidth = Unit.Pixel(1)

                Dim ddbTitleBinder As New DropDownListBinder(Titleddl, "SelTitlesV4", Session("EmployeeConn"))
                ddbTitleBinder.AddDDLCriteria("@titleType", "Physician", SqlDbType.NVarChar)
                ddbTitleBinder.BindData("TitleShortDesc", "TitleDesc")

                Titleddl.SelectedIndex = -1


                Titleddl.BackColor() = Color.Yellow
                Titleddl.BorderColor() = Color.Black
                Titleddl.BorderStyle() = BorderStyle.Solid
                Titleddl.BorderWidth = Unit.Pixel(2)

                hanrbl.BackColor() = Color.Yellow
                hanrbl.BorderColor() = Color.Black
                hanrbl.BorderStyle() = BorderStyle.Solid
                hanrbl.BorderWidth = Unit.Pixel(3)

                empLbl.Visible = True
                AuthorizationEmpNumTextBox.Visible = True


                SuppSuppLbl.Visible = False
                SuppSupportrbl.Visible = False
                SuppSupportrbl.Enabled = False
                Specialtyddl.SelectedIndex = -1

                DoesNeedprovlbl.Visible = False
                NeedsProviderrbl.Visible = False


                If Session("SecurityLevelNum") = 82 Or Session("LoginID").ToString.ToLower = "geip00" Or Session("LoginID").ToString.ToLower = "melej" Then
                    credentialedDateTextBox.Enabled = True
                    credentialedDateTextBox.CssClass = "calenderClass"

                    'CredentialedDateDCMHTextBox.CssClass = "calenderClass"
                    'CredentialedDateDCMHTextBox.Enabled = True

                End If



                thisRole = emp_type_cdrbl.SelectedValue
                mainRole = emp_type_cdrbl.SelectedValue

                Title2TextBox.Visible = False
                Titleddl.Visible = True

                SpecialtLabel.Visible = False
                Specialtyddl.Visible = True

                pnlProvider.Visible = True


                'credentialedDateTextBox.Enabled = True
                'credentialedDateTextBox.CssClass = "calenderClass"

                'CredentialedDateDCMHTextBox.Enabled = True
                'CredentialedDateDCMHTextBox.CssClass = "calenderClass"

                CCMCadmitRightsrbl.SelectedIndex = -1
                CCMCadmitRightsrbl.Enabled = True

                'DCMHadmitRightsrbl.SelectedIndex = -1
                'DCMHadmitRightsrbl.Enabled = True

                'Case "resident"
            Case "1194"
                'Dim ddlBinder As New DropDownListBinder
                SuppSupportrbl.Enabled = False

                empLbl.Visible = True
                AuthorizationEmpNumTextBox.Visible = True

                Dim ddbTitleBinder As New DropDownListBinder(Titleddl, "SelTitlesV4", Session("EmployeeConn"))
                ddbTitleBinder.AddDDLCriteria("@titleType", "Resident", SqlDbType.NVarChar)
                ddbTitleBinder.BindData("TitleShortDesc", "TitleDesc")

                Dim ddlRoleBinder As New DropDownListBinder(Rolesddl, "SelEmployeeRolesV2", Session("EmployeeConn"))
                ddlRoleBinder.AddDDLCriteria("@RoleNum", "1194", SqlDbType.NVarChar)
                ddlRoleBinder.BindData("rolenum", "RoleDesc")


                TitleLabel.Text = "Resident"

                Title2TextBox.Text = "Resident"
                Title2TextBox.Visible = True
                Titleddl.Visible = False

                SpecialtLabel.Text = "Resident"
                SpecialtLabel.Visible = True
                Specialtyddl.Visible = False

                SuppSupportrbl.Enabled = False

                credentialedDateTextBox.Enabled = False
                credentialedDateTextBox.CssClass = "style2"

                'CredentialedDateDCMHTextBox.Enabled = False
                'CredentialedDateDCMHTextBox.CssClass = "style2"

                CCMCadmitRightsrbl.SelectedValue = "No"
                CCMCadmitRightsrbl.Enabled = False

                'DCMHadmitRightsrbl.SelectedValue = "No"
                'DCMHadmitRightsrbl.Enabled = False

                thisRole = emp_type_cdrbl.SelectedValue
                mainRole = emp_type_cdrbl.SelectedValue

                Rolesddl.SelectedIndex = -1

                'SelectedValue.ToString = "Select"

                pnlProvider.Visible = True

            Case "1195", "1198"
                ' load vendor list 
                'make vendor text box invisiable

                e_mailTextBox.Enabled = True

                empLbl.Visible = False
                AuthorizationEmpNumTextBox.Visible = False


                SuppSupportrbl.Enabled = False

                Vendorddl.Visible = True

                VendorNameTextBox.Visible = False

                Dim ddbVendorBinder As New DropDownListBinder(Vendorddl, "SelVendors", Session("EmployeeConn"))

                If mainRole = "1554" Then
                    ddbVendorBinder.AddDDLCriteria("@type", "prospect", SqlDbType.NVarChar)

                Else
                    ddbVendorBinder.AddDDLCriteria("@type", "vendor", SqlDbType.NVarChar)


                End If
                ddbVendorBinder.BindData("VendorNum", "VendorName")

                thisRole = emp_type_cdrbl.SelectedValue
                mainRole = emp_type_cdrbl.SelectedValue



            Case "1193", "1200"
                'NeedsProviderrbl.Visible = True
                'DoesNeedprovlbl.Visible = True

                Title2TextBox.Visible = False
                Titleddl.Visible = True

                SpecialtLabel.Visible = False
                Specialtyddl.Visible = True

                pnlProvider.Visible = False

                thisRole = emp_type_cdrbl.SelectedValue
                mainRole = emp_type_cdrbl.SelectedValue

                empLbl.Visible = True
                AuthorizationEmpNumTextBox.Visible = True

            Case "1196", "1199"

                empLbl.Visible = False
                AuthorizationEmpNumTextBox.Visible = False
                SuppSupportrbl.Enabled = False


            Case Else
                Title2TextBox.Visible = False
                Titleddl.Visible = True

                SpecialtLabel.Visible = False
                Specialtyddl.Visible = True

                pnlProvider.Visible = False

                thisRole = emp_type_cdrbl.SelectedValue
                mainRole = emp_type_cdrbl.SelectedValue

                SuppSupportrbl.Enabled = False


        End Select

        ChangColor()

        'ChangColorNoCheck()

        CheckRequiredFields()

        If bHasError = True Then
            lblValidation.Focus()
            Exit Sub
        Else
            lblValidation.Text = ""
        End If


        'Rolesddl.SelectedValue = thisRole

        'dont select role if Resident
        'If thisRole <> "1194" Then
        '    ' this kicks off selecting applications that will be selected for that role
        '    Rolesddl_SelectedIndexChanged(thisRole, EventArgs.Empty)
        'End If


    End Sub
    Protected Sub CCMCadmitRightsrbl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles CCMCadmitRightsrbl.SelectedIndexChanged

        If CCMCadmitRightsrbl.SelectedValue.ToLower = "no" Then

            'CCMCConsultsrbl.SelectedValue = "No"
            'CCMCConsultsrbl.Enabled = False

            'DCMHConsultsrbl.SelectedValue = "No"
            'DCMHConsultsrbl.Enabled = False

            'TaylorConsultsrbl.SelectedValue = "No"
            'TaylorConsultsrbl.Enabled = False

            'SpringfieldConsultsrbl.SelectedValue = "No"
            'SpringfieldConsultsrbl.Enabled = False

            'Writeordersrbl.SelectedValue = "No"
            'Writeordersrbl.Enabled = False

            'WriteordersDCMHrbl.SelectedValue = "No"
            'WriteordersDCMHrbl.Enabled = False

            credentialedDateTextBox.Enabled = False
            credentialedDateTextBox.CssClass = "style2"

            'CredentialedDateDCMHTextBox.Enabled = False
            'CredentialedDateDCMHTextBox.CssClass = "style2"


        End If

        If CCMCadmitRightsrbl.SelectedValue.ToLower = "no" Then
            credentialedDateTextBox.Enabled = False
            credentialedDateTextBox.CssClass = "style2"

        End If

        If CCMCadmitRightsrbl.SelectedValue.ToLower = "yes" Then
            If Session("SecurityLevelNum") = 82 Or Session("LoginID") = "melej" Or Session("LoginID") = "geip00" Or Session("LoginID") = "RalphC" Then
                credentialedDateTextBox.Enabled = True
                credentialedDateTextBox.CssClass = "calenderClass"

            End If


        End If

        'If CCMCadmitRightsrbl.SelectedValue.ToLower = "yes" Or DCMHadmitRightsrbl.SelectedValue.ToLower = "yes" Then

        '    CCMCConsultsrbl.SelectedIndex = -1
        '    CCMCConsultsrbl.Enabled = True

        '    DCMHConsultsrbl.SelectedIndex = -1
        '    DCMHConsultsrbl.Enabled = True

        '    TaylorConsultsrbl.SelectedIndex = -1
        '    TaylorConsultsrbl.Enabled = True

        '    SpringfieldConsultsrbl.SelectedIndex = -1
        '    SpringfieldConsultsrbl.Enabled = True

        '    Writeordersrbl.SelectedIndex = -1
        '    Writeordersrbl.Enabled = True

        '    WriteordersDCMHrbl.SelectedIndex = -1
        '    WriteordersDCMHrbl.Enabled = True

        '    If Session("SecurityLevelNum") = 82 Or Session("LoginID") = "melej" Then
        '        credentialedDateTextBox.Enabled = True
        '        credentialedDateTextBox.CssClass = "calenderClass"

        '        CredentialedDateDCMHTextBox.Enabled = True
        '        CredentialedDateDCMHTextBox.CssClass = "calenderClass"

        '    End If
        'End If

        If thisRole = "1194" Then ' Resident
            credentialedDateTextBox.Enabled = False
            credentialedDateTextBox.CssClass = "style2"

            'CredentialedDateDCMHTextBox.Enabled = False
            'CredentialedDateDCMHTextBox.CssClass = "style2"

            CCMCadmitRightsrbl.SelectedValue = "No"
            CCMCadmitRightsrbl.Enabled = False

            'DCMHadmitRightsrbl.SelectedValue = "No"
            'DCMHadmitRightsrbl.Enabled = False
        End If
        ChangColorNoCheck()

        'ChangColor()


    End Sub

    'Protected Sub DCMHadmitRightsrbl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles DCMHadmitRightsrbl.SelectedIndexChanged
    '    If CCMCadmitRightsrbl.SelectedValue.ToLower = "no" And DCMHadmitRightsrbl.SelectedValue.ToLower = "no" Then

    '        CCMCConsultsrbl.SelectedValue = "No"
    '        CCMCConsultsrbl.Enabled = False

    '        DCMHConsultsrbl.SelectedValue = "No"
    '        DCMHConsultsrbl.Enabled = False

    '        TaylorConsultsrbl.SelectedValue = "No"
    '        TaylorConsultsrbl.Enabled = False

    '        SpringfieldConsultsrbl.SelectedValue = "No"
    '        SpringfieldConsultsrbl.Enabled = False

    '        Writeordersrbl.SelectedValue = "No"
    '        Writeordersrbl.Enabled = False

    '        WriteordersDCMHrbl.SelectedValue = "No"
    '        WriteordersDCMHrbl.Enabled = False

    '        credentialedDateTextBox.Enabled = False
    '        credentialedDateTextBox.CssClass = "style2"

    '        CredentialedDateDCMHTextBox.Enabled = False
    '        CredentialedDateDCMHTextBox.CssClass = "style2"


    '    End If

    '    If DCMHadmitRightsrbl.SelectedValue.ToLower = "no" Then
    '        CredentialedDateDCMHTextBox.Enabled = False
    '        CredentialedDateDCMHTextBox.CssClass = "style2"

    '    End If

    '    If DCMHadmitRightsrbl.SelectedValue.ToLower = "yes" Then
    '        If Session("SecurityLevelNum") = 82 Or Session("LoginID") = "melej" Or Session("LoginID") = "geip00" Or Session("LoginID") = "RalphC" Then

    '            CredentialedDateDCMHTextBox.Enabled = True
    '            CredentialedDateDCMHTextBox.CssClass = "calenderClass"

    '        End If


    '    End If

    '    'If CCMCadmitRightsrbl.SelectedValue.ToLower = "yes" Or DCMHadmitRightsrbl.SelectedValue.ToLower = "yes" Then

    '    '    CCMCConsultsrbl.SelectedIndex = -1
    '    '    CCMCConsultsrbl.Enabled = True

    '    '    DCMHConsultsrbl.SelectedIndex = -1
    '    '    DCMHConsultsrbl.Enabled = True

    '    '    TaylorConsultsrbl.SelectedIndex = -1
    '    '    TaylorConsultsrbl.Enabled = True

    '    '    SpringfieldConsultsrbl.SelectedIndex = -1
    '    '    SpringfieldConsultsrbl.Enabled = True

    '    '    Writeordersrbl.SelectedIndex = -1
    '    '    Writeordersrbl.Enabled = True

    '    '    WriteordersDCMHrbl.SelectedIndex = -1
    '    '    WriteordersDCMHrbl.Enabled = True

    '    '    If Session("SecurityLevelNum") = 82 Or Session("LoginID") = "melej" Then
    '    '        credentialedDateTextBox.Enabled = True
    '    '        credentialedDateTextBox.CssClass = "calenderClass"

    '    '        CredentialedDateDCMHTextBox.Enabled = True
    '    '        CredentialedDateDCMHTextBox.CssClass = "calenderClass"

    '    '        CCMCadmitRightsrbl.SelectedIndex = -1
    '    '        CCMCadmitRightsrbl.Enabled = True

    '    '        DCMHadmitRightsrbl.SelectedIndex = -1
    '    '        DCMHadmitRightsrbl.Enabled = True


    '    '    End If

    '    'End If

    '    If thisRole = "1194" Then
    '        credentialedDateTextBox.Enabled = False
    '        credentialedDateTextBox.CssClass = "style2"

    '        CredentialedDateDCMHTextBox.Enabled = False
    '        CredentialedDateDCMHTextBox.CssClass = "style2"

    '        CCMCadmitRightsrbl.SelectedValue = "No"
    '        CCMCadmitRightsrbl.Enabled = False

    '        DCMHadmitRightsrbl.SelectedValue = "No"
    '        DCMHadmitRightsrbl.Enabled = False
    '    End If
    '    ChangColorNoCheck()
    '    'ChangColor()

    'End Sub

    Protected Sub Vendorddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles Vendorddl.SelectedIndexChanged

        Dim vendornum As String = Vendorddl.SelectedValue
        Dim vendorname As String = Vendorddl.SelectedItem.ToString

        VendorFormSignOn = New FormSQL(Session("EmployeeConn"), "SelVendors", "InsUpdVendors", "", Page)
        VendorFormSignOn.AddSelectParm("@VendorNum", vendornum, SqlDbType.Int, 6)

        If vendornum <> 0 Then
            VendorFormSignOn.FillForm()
            address_1textbox.Text = address1TextBox.Text
            VendorNameTextBox.Text = vendorname

            VendorNameTextBox.Visible = False
            VendorNumLabel.Text = vendornum.ToString()

        Else

            VendorNameTextBox.Text = vendorname
            VendorNumLabel.Text = vendornum.ToString()

            VendorNameTextBox.Visible = False

        End If

        ChangColorNoCheck()

        'ChangColor()
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click

        Dim thisData As New CommandsSqlAndOleDb("SelActiveCKHSEmployees", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@FirstName", FirstNameTextBox.Text, SqlDbType.NVarChar, 20)
        thisData.AddSqlProcParameter("@LastName", LastNameTextBox.Text, SqlDbType.NVarChar, 20)

        gvSearch.DataSource = thisData.GetSqlDataTable
        gvSearch.DataBind()

        uplOnBehalfOf.Update()
        mpeOnBehalfOf.Show()

    End Sub
    Protected Sub gvSearch_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvSearch.RowCommand

        If e.CommandName = "Select" Then
            Dim client_num As String = gvSearch.DataKeys(Convert.ToInt32(e.CommandArgument))("client_num").ToString()
            Dim login_name As String = gvSearch.DataKeys(Convert.ToInt32(e.CommandArgument))("login_name").ToString()
            Dim dbAccess As New CkhsEmployeeDbAccess(Session("EmployeeConn"))
            Dim ckhsEmployee = dbAccess.GetCkhsEmployee(client_num)

            'c3Controller = New Client3Controller(client_num)
            'c3Submitter = c3Controller.GetClientByClientNum(client_num)


            SubmittingOnBehalfOfNameLabel.Text = ckhsEmployee.FullName & " (" & ckhsEmployee.EMail & ")"
            requestor_client_numLabel.Text = client_num
            'btnUpdateSubmitter.Visible = True


            gvSearch.SelectedIndex = -1
            gvSearch.Dispose()

            Select Case userDepartmentCD.Text
                Case "832600", "832700", "823100"
                    If Session("SecurityLevelNum") < 90 And requestor_client_numLabel.Text = submitter_client_numLabel.Text Then
                        btnSelectOnBehalfOf.Visible = True
                        lblValidation.Text = "Must Change Requestor"
                        lblValidation.Focus()
                    ElseIf Session("SecurityLevelNum") < 90 And requestor_client_numLabel.Text <> submitter_client_numLabel.Text Then
                        lblValidation.Text = ""
                        btnSelectOnBehalfOf.BackColor() = Color.FromKnownColor("336666")
                        btnSelectOnBehalfOf.ForeColor() = Color.White
                        btnSelectOnBehalfOf.BorderStyle() = BorderStyle.Groove
                        btnSelectOnBehalfOf.BorderWidth = Unit.Pixel(1)



                    End If


            End Select


            'mpeOnBehalfOf.Show()


        End If
    End Sub
    Protected Function checkFacilities() As Boolean
        Dim blnReturn = False

        For Each item As ListItem In cblFacilities.Items
            If item.Selected Then
                blnReturn = True

            End If


        Next


        Return blnReturn

    End Function
    Protected Function checkApplications() As Boolean
        Dim blnReturn = False

        For Each item As ListItem In cblApplications.Items
            If item.Selected Then
                blnReturn = True

            End If
        Next
        Return blnReturn
    End Function
    Protected Function checkEntities() As Boolean
        Dim blnReturn As Boolean = False

        For Each item As ListItem In cblEntities.Items
            If item.Selected Then

                blnReturn = True
            End If
        Next
        Return blnReturn
    End Function
    Protected Sub btnClearAccounts_Click(sender As Object, e As System.EventArgs) Handles btnClearAccounts.Click
        For Each item As ListItem In cblApplications.Items
            item.Selected = False

        Next
    End Sub
    Public Sub CheckBoxValueSelector(ByRef cbl As CheckBoxList, ByVal val As String)

        Dim BusLog As New AccountBusinessLogic

        For Each item As ListItem In cbl.Items

            If item.Value = val Then

                item.Selected = True

            End If

        Next

    End Sub
    Protected Sub entity_cdDDL_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles entity_cdDDL.SelectedIndexChanged
        Dim ddbDepartmentBinder As New DropDownListBinder(department_cdddl, "selDepartmentCodesV4", Session("EmployeeConn"))
        ddbDepartmentBinder.AddDDLCriteria("@entity_cd", entity_cdDDL.SelectedValue, SqlDbType.NVarChar)
        ddbDepartmentBinder.BindData("department_cd", "department_name")
        ChangColor()

    End Sub
    Protected Sub SendProviderEmail(ByVal AccountRequestNum As Integer)
        '======================
        ' Get current path
        '======================

        Dim path As String
        Dim directory As String = ""
        Dim toList As String()

        path = HttpContext.Current.Request.Url.AbsoluteUri
        Dim pathParts() As String = Split(path, "/")
        Dim i As Integer

        Dim EmailList As String


        Do While i < pathParts.Length() - 1

            directory = directory + pathParts(i) + "/"
            i = i + 1

        Loop

        Mailmsg.To.Clear()


        'Dim toEmail As String = EmailAddress.Email
        Dim sSubject As String
        Dim sBody As String
        Dim fulleMailList As String

        sSubject = "Account Request for " & first_nameTextBox.Text & " " & last_nameTextBox.Text & " has been added request to the CSC Provisioning Application."


        sBody = "<!DOCTYPE html>" & _
                            "<html>" & _
                            "<head>" & _
                            "<style>" & _
                            "table" & _
                            "{" & _
                            "border-collapse:collapse;" & _
                            "font-family: 'Helvetica Neue', 'Lucida Grande', 'Segoe UI', Arial, Helvetica, Verdana, sans-serif;" & _
                            "}" & _
                            "table, th, td" & _
                            "{" & _
                            "border: 1px solid black;" & _
                            "padding: 3px" & _
                            "}" & _
                            "</style>" & _
                            "</head>" & _
                            "" & _
                            "<body>" & _
                            "A new Provisioning Account request has been submitted.  Follow the link below to go to view details of the request." & _
                             "<br />" & _
                            "<a href=""" & directory & "/AccountRequestInfo2.aspx?requestnum=" & AccountRequestNum & """ ><u><b>Name on Accounts: " & first_nameTextBox.Text & " " & last_nameTextBox.Text & "</b></u></a>" & _
                            "</body>" & _
                        "</html>"


        EmailList = New EmailListOne(AccountRequestNum, Session("EmployeeConn")).EmailListReturn

        fulleMailList = EmailList
        If Session("environment").ToString.ToLower = "testing" Then
            EmailList = "Jeff.Mele@crozer.org"
            sBody = sBody & fulleMailList

        End If

        toList = EmailList.Split(";")
        If toList.Length = 1 Then
            Mailmsg.To.Add(New System.Net.Mail.MailAddress(toList(0)))
        Else
        End If
        For i = 0 To toList.Length - 1 ' - 2
            If toList(i).Length > 10 Then ' validity check
                Mailmsg.To.Add(New System.Net.Mail.MailAddress(toList(i)))
            End If
        Next

        'Dim mailObj As New MailMessage
        Mailmsg.From = New MailAddress("CSC@crozer.org")
        Mailmsg.IsBodyHtml = True

        Mailmsg.Subject = sSubject
        Mailmsg.Body = sBody

        Dim smtpClient As New SmtpClient()
        smtpClient.Host = "ah1smtprelay01.altahospitals.com"

        If Session("environment") = "Production" Then
            smtpClient.Send(Mailmsg)
        End If


        Dim thisData As New CommandsSqlAndOleDb("InsAccounteMailTo", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int)
        thisData.AddSqlProcParameter("@ToAddress", EmailList, SqlDbType.NVarChar, 4000)
        thisData.AddSqlProcParameter("@subject", sSubject, SqlDbType.NVarChar, 200)
        thisData.AddSqlProcParameter("@body", sBody, SqlDbType.NVarChar, 4000)

        thisData.ExecNonQueryNoReturn()
    End Sub


    Protected Sub btnUpdateSubmitter_Click(sender As Object, e As System.EventArgs) Handles btnUpdateSubmitter.Click
        mpeUpdateClient.Show()

        Dim dbAccess As New CkhsEmployeeDbAccess(Session("EmployeeConn"))
        Dim ckhsEmployee = dbAccess.GetCkhsEmployee(requestor_client_numLabel.Text)


        Dim c3Submitter As New Client3

        'c3Submitter = c3Controller.GetClientByClientNum(requestor_client_numLabel.Text)
        lblUpdateClientFullName.Text = ckhsEmployee.FullName
        txtUpdateEmail.Text = ckhsEmployee.EMail
        txtUpdatePhone.Text = ckhsEmployee.Phone

    End Sub
    Protected Sub btnUpdateClient_Click(sender As Object, e As System.EventArgs) Handles btnUpdateClient.Click
        Dim frmHelperRequestTicket As New FormHelper()

        frmHelperRequestTicket.InitializeUpdateForm("UpdClientObjectByNumberV20", tblUpdateClient, "Update")
        frmHelperRequestTicket.AddCmdParameter("@ClientNum", requestor_client_numLabel.Text)
        frmHelperRequestTicket.UpdateForm()

        Dim dbAccess As New CkhsEmployeeDbAccess(Session("EmployeeConn"))
        Dim ckhsEmployee = dbAccess.GetCkhsEmployee(requestor_client_numLabel.Text)

        SubmittingOnBehalfOfNameLabel.Text = ckhsEmployee.FullName & " (" & ckhsEmployee.EMail & ")"

        mpeUpdateClient.Hide()
    End Sub
    Public Function CheckRequiredFields() As Boolean
        Dim removeCss As New PageControls(Page)
        removeCss.RemoveCssClass(Page, "textInputRequired")

        Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
        Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")

        emp_type_cdLabel.Text = emp_type_cdrbl.SelectedValue

        Select Case emp_type_cdrbl.SelectedValue
            Case "1193", "1200"
                'Case "employee"


                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")

                Dim Location As New RequiredField(facility_cdddl, "Location")
                Dim building As New RequiredField(building_cdddl, "building")

                facility_cdddl.BackColor() = Color.Yellow
                facility_cdddl.BorderColor() = Color.Black
                facility_cdddl.BorderStyle() = BorderStyle.Solid
                facility_cdddl.BorderWidth = Unit.Pixel(2)

                building_cdddl.BackColor() = Color.Yellow
                building_cdddl.BorderColor() = Color.Black
                building_cdddl.BorderStyle() = BorderStyle.Solid
                building_cdddl.BorderWidth = Unit.Pixel(2)

                'Dim Aff As New RequiredField(emp_type_cdrbl, "Affiliation")

                pnlProvider.Visible = False
                'providerrbl.SelectedIndex = -1

                'lblVendor.Text = "Vendor Name:"
                Vendorddl.Visible = False
                VendorNameTextBox.Visible = True
                VendorNameTextBox.Enabled = False

            Case "1189"
                'Case "student"

                Dim titleReq As New RequiredField(Titleddl, "Title222")
                Titleddl.BackColor() = Color.Yellow
                Titleddl.BorderColor() = Color.Black
                Titleddl.BorderStyle() = BorderStyle.Solid
                Titleddl.BorderWidth = Unit.Pixel(2)

                Dim specReq As New RequiredField(Specialtyddl, "Specialty")
                Specialtyddl.BackColor() = Color.Yellow
                Specialtyddl.BorderColor() = Color.Black
                Specialtyddl.BorderStyle() = BorderStyle.Solid
                Specialtyddl.BorderWidth = Unit.Pixel(2)

                Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim faxqReq As New RequiredField(faxtextbox, "Fax")


                pnlProvider.Visible = False
                'providerrbl.SelectedIndex = -1
                Titleddl.Enabled = True

                'lblVendor.Text = "Vendor Name:"
                Vendorddl.Visible = False
                VendorNameTextBox.Visible = True
                VendorNameTextBox.Enabled = False


            Case "782", "1197"
                ' Case "physician"
                Dim specReq As New RequiredField(Specialtyddl, "Specialty")
                Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim faxqReq As New RequiredField(faxtextbox, "Fax")
                Dim Titlereq2 As New RequiredField(TitleLabel, "Title")

                'Dim Writeorders As New RequiredField(Writeordersrbl, "Write Orders for C.T.S.")
                'Dim WriteordersDCMH As New RequiredField(WriteordersDCMHrbl, "Write Orders for DCMH")

                Dim CCMCadmitRights As New RequiredField(CCMCadmitRightsrbl, "Admitting Rights ??")
                'Dim DCMHadmitRights As New RequiredField(DCMHadmitRightsrbl, "Admitting Rights for DCMH")

                Dim HanReq As New RequiredField(hanrbl, "HAN-CKHN Physician")

                If hanrbl.SelectedValue.ToLower = "yes" Then
                    Dim loccareReq As New RequiredField(LocationOfCareIDddl, "CKHN Group")

                    LocationOfCareIDddl.BackColor() = Color.Yellow
                    LocationOfCareIDddl.BorderColor() = Color.Black
                    LocationOfCareIDddl.BorderStyle() = BorderStyle.Solid
                    LocationOfCareIDddl.BorderWidth = Unit.Pixel(2)

                    npitextbox.BackColor() = Color.Yellow
                    npitextbox.BorderColor() = Color.Black
                    npitextbox.BorderStyle() = BorderStyle.Solid
                    npitextbox.BorderWidth = Unit.Pixel(2)

                    LicensesNotextbox.BackColor() = Color.Yellow
                    LicensesNotextbox.BorderColor() = Color.Black
                    LicensesNotextbox.BorderStyle() = BorderStyle.Solid
                    LicensesNotextbox.BorderWidth = Unit.Pixel(2)

                    taxonomytextbox.BackColor() = Color.Yellow
                    taxonomytextbox.BorderColor() = Color.Black
                    taxonomytextbox.BorderStyle() = BorderStyle.Solid
                    taxonomytextbox.BorderWidth = Unit.Pixel(2)

                End If

                pnlProvider.Visible = True



                Vendorddl.Visible = False
                VendorNameTextBox.Visible = True
                VendorNameTextBox.Enabled = False


                Titleddl.Enabled = True

            Case "1194"
                'Case "resident"

                Dim titleReq As New RequiredField(Titleddl, "Title")

                Dim specReq As New RequiredField(Specialtyddl, "Specialty")
                Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                'Dim faxqReq As New RequiredField(faxtextbox, "Fax")

                'Dim Writeorders As New RequiredField(Writeordersrbl, "Write Orders for C.T.S.")
                'Dim WriteordersDCMH As New RequiredField(WriteordersDCMHrbl, "Write Orders for DCMH")


                pnlProvider.Visible = True

                'providerrbl.SelectedValue = "Yes"
                Titleddl.Enabled = True

                'lblVendor.Text = "Vendor Name:"
                Vendorddl.Visible = False
                VendorNameTextBox.Visible = True
                VendorNameTextBox.Enabled = False



            Case "1198", "1195"

                'Case "vendor" 


                Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim vendorRequired As New RequiredField(VendorNameTextBox, "Vendor")
                Dim startReq As New RequiredField(start_dateTextBox, "Start Date")

                Dim VendorReq As New RequiredField(Vendorddl, "Vendor")

                Dim email As New RequiredField(e_mailTextBox, "EMail")

                VendorNameTextBox.Visible = False
                Vendorddl.Visible = True
                'lblVendor.Text = "Vendor Name:"


                pnlProvider.Visible = False
                'providerrbl.SelectedIndex = -1

            Case "1196", "1199"
                'Case "other" Non ckhn staff


                Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                'Dim vendorRequired As New RequiredField(VendorNameTextBox, "Vendor")

                Dim startReq As New RequiredField(start_dateTextBox, "Start Date")


                pnlProvider.Visible = False
                'providerrbl.SelectedIndex = -1

                VendorNameTextBox.Visible = True
                VendorNameTextBox.Enabled = False

                'lblVendor.Text = "Vendor Name:"
                Vendorddl.Visible = False




            Case Else

                'Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                'Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")
                'Dim Aff As New RequiredField(emp_type_cdrbl, "Affiliation")


                pnlProvider.Visible = False
                'providerrbl.SelectedIndex = -1



        End Select

        For Each cbAppi As ListItem In cblApplications.Items
            Dim sCurrApp As String = cbAppi.ToString
            If cbAppi.Selected = True Then

                Select Case sCurrApp
                    Case "MAK"
                        lblValidation.Text = "MAK Application needs Comments to identify this account"
                        If request_descTextBox.Text = "" Then
                            request_descTextBox.Text = "Use This Mak ID ="

                        Else
                            request_descTextBox.Text = request_descTextBox.Text & " Add ID for Mak"
                        End If

                        Dim Comments As New RequiredField(request_descTextBox, "Comments")
                        Return bHasError

                    Case "nTHRIVE Claims Mgmt"

                        Dim ClaimMgtDesc As New RequiredField(ClaimMgtrbl, "nTHRIVE Claims Mgmt.")

                        ClaimMgtrbl.BackColor() = Color.Yellow
                        ClaimMgtrbl.BorderColor() = Color.Black
                        ClaimMgtrbl.BorderStyle() = BorderStyle.Solid
                        ClaimMgtrbl.BorderWidth = Unit.Pixel(2)
                        'Return bHasError

                    Case "Patient Accounting"
                        'Patient Accounting

                        'cblPAtientAcct
                        Dim PatAcctDesc As New RequiredField(PAtientAcctrbl, "Patient Accounting")

                        PAtientAcctrbl.BackColor() = Color.Yellow
                        PAtientAcctrbl.BorderColor() = Color.Black
                        PAtientAcctrbl.BorderStyle() = BorderStyle.Solid
                        PAtientAcctrbl.BorderWidth = Unit.Pixel(2)
                        'Return bHasError

                        'Dim PatDollDesc As New RequiredField(patamount, "Supply Dollar Amount")

                        'patamount.BackColor() = Color.Yellow
                        'patamount.BorderColor() = Color.Black
                        'patamount.BorderStyle() = BorderStyle.Solid
                        'patamount.BorderWidth = Unit.Pixel(2)

                        'Return bHasError

                    Case "Care Payments"

                        'cblCarePayment
                        Dim CarePayDesc As New RequiredField(CarePaymentrbl, "Care Payments")
                        CarePaymentrbl.BackColor() = Color.Yellow
                        CarePaymentrbl.BorderColor() = Color.Black
                        CarePaymentrbl.BorderStyle() = BorderStyle.Solid
                        CarePaymentrbl.BorderWidth = Unit.Pixel(2)

                        ' Return bHasError


                    Case "SCI Scheduler"
                        'cblScISchedule
                        Dim SCIDesc As New RequiredField(ScISchedulerbl, "SCI Schedule")
                        ScISchedulerbl.BackColor() = Color.Yellow
                        ScISchedulerbl.BorderColor() = Color.Black
                        ScISchedulerbl.BorderStyle() = BorderStyle.Solid
                        ScISchedulerbl.BorderWidth = Unit.Pixel(2)

                        'Return bHasError

                    Case "General Ledger/AP/Payroll"
                        'cblRegions
                        Dim RegDesc As New RequiredField(cblRegions, "General Ledger/AP/Payroll")
                        cblRegions.BackColor() = Color.Yellow
                        cblRegions.BorderColor() = Color.Black
                        cblRegions.BorderStyle() = BorderStyle.Solid
                        cblRegions.BorderWidth = Unit.Pixel(2)

                        cblRegions.Visible = True
                        Regionlbl.Visible = True

                End Select

            End If

        Next

        Dim RoleDesc As New RequiredField(Rolesddl, "Position Description")
        Rolesddl.BackColor() = Color.Yellow
        Rolesddl.BorderColor() = Color.Black
        Rolesddl.BorderStyle() = BorderStyle.Solid
        Rolesddl.BorderWidth = Unit.Pixel(2)

        Dim EntDesc As New RequiredField(cblEntities, "Entity Description")

        cblEntities.BackColor() = Color.Yellow
        cblEntities.BorderColor() = Color.Black
        cblEntities.BorderStyle() = BorderStyle.Solid
        cblEntities.BorderWidth = Unit.Pixel(2)

        Dim FacilityDesc As New RequiredField(cblFacilities, "Facility Description")

        cblFacilities.BackColor() = Color.Yellow
        cblFacilities.BorderColor() = Color.Black
        cblFacilities.BorderStyle() = BorderStyle.Solid
        cblFacilities.BorderWidth = Unit.Pixel(2)

        Return bHasError

    End Function

    Protected Sub btnNoSearch_Click(sender As Object, e As System.EventArgs) Handles btnNoSearch.Click
        Response.Redirect("SimpleSearch.aspx")
    End Sub

    Protected Sub btnSearchFirst_Click(sender As Object, e As System.EventArgs) Handles btnSearchFirst.Click
        lblSearched.Text = "TRUE"
        SearchPopup.Hide()
    End Sub

    Protected Sub grvClientsExists_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grvClientsExists.RowCommand
        If e.CommandName = "Select" Then


            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim AccountRequestNum As String = grvClientsExists.DataKeys(Convert.ToInt32(e.CommandArgument))("AccountRequestNum").ToString()
            Dim ClientNum As String = grvClientsExists.DataKeys(Convert.ToInt32(e.CommandArgument))("client_num").ToString()


            If AccountRequestNum <> "0" Then
                Response.Redirect("AccountRequestInfo2.aspx?&RequestNum=" & AccountRequestNum, True)

            End If
            If ClientNum <> "0" Then
                Response.Redirect("ClientDemo.aspx?&ClientNum=" & ClientNum, True)

            End If
        End If

    End Sub

    Protected Sub btnCloseExists_Click(sender As Object, e As System.EventArgs) Handles btnCloseExists.Click
        ClientExists.Hide()
        TestednameLabel.Text = "yes"
        lblValidation.ForeColor = Color.Green
        lblValidation.Text = "Click Submit button to complete request"
        btnSubmit.Focus()
    End Sub
    Protected Sub setColumnInfo(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                ' Siemens Employee#

                sEmployeeNum = e.Row.Cells(getcellNoE("siemensempnum", grvClientsExists)).Text

                If sEmployeeNum.Length > 1 Then
                    e.Row.BackColor = System.Drawing.Color.LightGreen
                End If

            End If

        Catch ex As Exception
            Throw ex

        End Try
    End Sub

    Protected Sub btnReturnExists_Click(sender As Object, e As System.EventArgs) Handles btnReturnExists.Click
        ClientExists.Hide()

    End Sub

    'Protected Sub grvClientsExists_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grvClientsExists.RowDataBound
    '    Try
    '        If e.Row.RowType = DataControlRowType.DataRow Then
    '            ' //===================================diag==========================
    '            Dim irow As Integer = e.Row.RowIndex


    '            'sEmployeeNum = e.Row.Cells(getcellNoE("siemens_emp_num", grvClientsExists)).Text

    '            'If sEmployeeNum.Length > 1 Then
    '            '    e.Row.BackColor = System.Drawing.Color.LightGreen
    '            'End If
    '            setColumnInfo(sender, e)

    '        End If
    '    Catch ex As Exception
    '        Throw ex

    '    End Try

    'End Sub

    'Protected Sub ColorCodeAccounts()
    '    Dim BaseApps As New List(Of Application)
    '    BaseApps = New Applications().BaseApps()

    '    For Each item As ListItem In cblApplications.Items

    '        If BaseApps.Find(Function(x) x.ApplicationNum = item.Value).AppSystem = "HospSpec" Then

    '            item.Attributes.Add("style", "color:red")
    '        ElseIf BaseApps.Find(Function(x) x.ApplicationNum = item.Value).AppSystem = "EntSpec" Then

    '            item.Attributes.Add("style", "color:blue")
    '        End If
    '    Next

    'End Sub


    Protected Sub group_nameddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles group_nameddl.SelectedIndexChanged
        'group_numTextBox.Text = group_nameddl.SelectedValue
        group_nameTextBox.Text = group_nameddl.SelectedItem.ToString

        Select Case group_nameddl.SelectedValue
            Case "346", "324" ' This will check all app hospitals and entities for Hospitialists CCMC & DCMH
                ' First clear out all items
                For Each cblItem As ListItem In cblApplications.Items
                    cblItem.Selected = False
                    cblItem.Enabled = False
                Next
                ' Now check all itesm for Hospitilists
                For Each cblItem As ListItem In cblApplications.Items
                    Dim sCurritem As String = cblItem.ToString

                    Select Case sCurritem
                        Case "eCare", "EDM", "Email", "EMR - CKHN", "Network", "PACS", "PICIS", "RSA Token for Remote Access", "Clinician Dashboard", "Single Sign On (SSO)", "Tracemaster"
                            'Change 2015 "EMR - CKHN", "EDM", "Email", "Network", "Tracemaster", "RSA Token for Remote Access", "Single Sign On (SSO)","EMR - Comm"
                            If cblItem.Selected = False Then
                                cblItem.Selected = True
                                cblItem.Enabled = True
                            End If
                    End Select
                    'If sCurritem.ToString = "EAD" And cblItem.Selected = False Then
                    '    cblItem.Selected = True
                    'End If
                Next
                '' check all entities 
                For Each cbentities As ListItem In cblEntities.Items
                    If cbentities.Selected = False Then
                        cbentities.Selected = True
                    End If
                Next


                ' check all hospitals
                For Each cbHospitals As ListItem In cblFacilities.Items
                    Dim sCurrHospital As String = cbHospitals.ToString

                    If cbHospitals.Selected = False And sCurrHospital.ToString.ToLower <> "community" Then
                        cbHospitals.Selected = True

                    End If
                Next
            Case Else

                ReSetApps()
        End Select

        'ChangColor()

    End Sub
    Protected Sub getTCLforRole()
        Dim tcldata As New CommandsSqlAndOleDb("SelInvisionUserTypeV20", CType(Session("EmployeeConn"), String))
        tcldata.AddSqlProcParameter("@rolenum", emp_type_cdrbl.SelectedValue, SqlDbType.NVarChar, 8)
        tcldata.AddSqlProcParameter("@UserTypeCd", Titleddl.SelectedValue, SqlDbType.NVarChar, 25)

        tcldata.GetSqlDataset()

        dtTCLUserTypes = tcldata.GetSqlDataTable
        gvTCL.DataSource = dtTCLUserTypes
        gvTCL.DataBind()




        If dtTCLUserTypes.Rows.Count = 1 Then
            'TCLddl.SelectedIndex = 0

            TCLTextBox.Text = dtTCLUserTypes.Rows(0)("TCL").ToString
            UserTypeDescTextBox.Text = dtTCLUserTypes.Rows(0)("UserTypeDesc").ToString
            UserTypeCdTextBox.Text = dtTCLUserTypes.Rows(0)("UserTypecd").ToString

            pnlTCl.Visible = True
            TCLTextBox.Visible = True
            UserTypeDescTextBox.Visible = True
            UserTypeCdTextBox.Visible = True

            'TCLddl.Visible = False

            gvTCL.Visible = False

        ElseIf dtTCLUserTypes.Rows.Count = 0 Then
            pnlTCl.Visible = False

        Else
            pnlTCl.Visible = True
            TCLTextBox.Visible = True
            UserTypeDescTextBox.Visible = True
            UserTypeCdTextBox.Visible = True


            'TCLddl.Visible = True

            'TCLddl.SelectedIndex = -1

            'TCLddl.BackColor() = Color.Yellow
            'TCLddl.BorderColor() = Color.Black
            'TCLddl.BorderStyle() = BorderStyle.Solid
            'TCLddl.BorderWidth = Unit.Pixel(2)

            gvTCL.Visible = True
            'gvTCL.BackColor() = Color.Yellow
            'gvTCL.BorderColor() = Color.Black
            'gvTCL.BorderStyle() = BorderStyle.Solid
            'gvTCL.BorderWidth = Unit.Pixel(2)
            'gvTCL.RowStyle.BackColor() = Color.Yellow

        End If

        ' fill drop down
        Dim CerPosdataddl As New DropDownListBinder(CernerPosddl, "SelCernerPositionDesc", Session("EmployeeConn"))
        CerPosdataddl.AddDDLCriteria("@rolenum", emp_type_cdrbl.SelectedValue, SqlDbType.NVarChar)
        CerPosdataddl.AddDDLCriteria("@Clientupd", "all", SqlDbType.NVarChar)

        CerPosdataddl.BindData("CernerPositionDesc", "CernerPositionDesc")


        ' fill specific Cerner for this title
        Dim dtCernerPosition As New DataTable

        Dim positiondata As New CommandsSqlAndOleDb("SelCernerPositionDesc", Session("EmployeeConn"))

        If Rolesddl.SelectedValue.ToString = "Select" And thisRole <> "" Then
            Rolesddl.SelectedValue = thisRole
        End If

        If PositionRoleNumTextBox.Text <> "" Then
            positiondata.AddSqlProcParameter("@roleNum", PositionRoleNumTextBox.Text, SqlDbType.NVarChar, 9)
        ElseIf Rolesddl.SelectedValue.ToString.ToLower <> "select" Then
            positiondata.AddSqlProcParameter("@roleNum", Rolesddl.SelectedValue.ToString, SqlDbType.NVarChar, 9)
        Else
            positiondata.AddSqlProcParameter("@roleNum", emp_type_cdrbl.SelectedValue.ToString, SqlDbType.NVarChar, 9)
        End If

        If TitleLabel.Text <> "" Then
            positiondata.AddSqlProcParameter("@UserTypeCd", TitleLabel.Text, SqlDbType.NVarChar, 25)

        ElseIf UserTypeCdTextBox.Text <> "" Then
            positiondata.AddSqlProcParameter("@UserTypeCd", UserTypeCdTextBox.Text, SqlDbType.NVarChar, 25)

        End If

        dtCernerPosition = positiondata.GetSqlDataTable

        If dtCernerPosition.Rows.Count > 0 Then
            CernerPositionDescTextBox.Text = dtCernerPosition.Rows(0)("CernerPositionDesc").ToString
        End If


        ' UserCategoryCdTextBox.Text = dtCernerPosition.Rows(0)("UserCategoryCd").ToString




    End Sub

    Protected Sub ReSetApps()
        For Each cblItem As ListItem In cblApplications.Items
            cblItem.Selected = False
            'cblItem.Enabled = False
        Next


        If Session("SecurityLevelNum") < 70 Then
            For Each cblItem As ListItem In cblApplications.Items

                cblItem.Enabled = False
            Next
        Else
            For Each cblItem As ListItem In cblApplications.Items

                cblItem.Enabled = True
            Next


        End If

        For Each cblEntitem As ListItem In cblEntities.Items
            cblEntitem.Selected = False
        Next

        For Each cblFacItem As ListItem In cblFacilities.Items
            cblFacItem.Selected = False
        Next

        Dim rwRef() As DataRow
        Dim ddlBinderV2 As DropDownListBinder

        'always use the Main Role
        mainRole = emp_type_cdrbl.SelectedValue


        request_descTextBox.Text = ""
        request_descTextBox.BackColor() = Color.White
        request_descTextBox.BorderStyle = BorderStyle.None
        request_descTextBox.BorderWidth = Unit.Pixel(2)

        If Rolesddl.SelectedValue.ToString <> "Select" Then
            user_position_descTextBox.Text = Rolesddl.SelectedValue
            mainRole = Rolesddl.SelectedValue

            Title2TextBox.Text = Rolesddl.SelectedItem.ToString
            If thisRole Is Nothing Then
                thisRole = Rolesddl.SelectedValue
            End If
        ElseIf Rolesddl.SelectedValue.ToString = "Select" And mainRole <> Nothing Then
            ddlBinderV2 = New DropDownListBinder(Rolesddl, "SelEmployeeRolesV2", Session("EmployeeConn"))

            ddlBinderV2.AddDDLCriteria("@RoleNum", mainRole, SqlDbType.NVarChar)
            ddlBinderV2.BindData("rolenum", "roledesc")

            Rolesddl.SelectedValue = mainRole

        End If
        ' ADD get Table here
        Dim thisdata As New CommandsSqlAndOleDb("SelApplicationExtensionByRoleNum", Session("EmployeeConn"))

        If Rolesddl.SelectedValue.ToString = "Select" And thisRole <> "" Then
            Rolesddl.SelectedValue = thisRole
        End If

        thisdata.AddSqlProcParameter("@roleNum", Rolesddl.SelectedValue.ToString, SqlDbType.NVarChar, 9)
        thisdata.AddSqlProcParameter("@display", "base", SqlDbType.NVarChar, 9)


        thisdata.ExecNonQueryNoReturn()

        dtApplications = thisdata.GetSqlDataTable

        Dim Appnum As Integer
        Dim appDeafult As String
        Dim snotice As String = "false"
        Dim MakCont As Boolean

        For Each row As DataRow In dtApplications.Rows
            Appnum = row("ApplicationNum")
            appDeafult = row("DefaultValue")
            gAppName = row("ApplicationDesc")



            For Each cblItem As ListItem In cblApplications.Items
                Dim sCurritem As String = cblItem.Value

                If sCurritem = Appnum And appDeafult.ToLower = "yes" Then
                    cblItem.Selected = True


                    cblItem.Enabled = True
                End If
                If sCurritem = Appnum And appDeafult.ToLower = "no" Then
                    cblItem.Enabled = True
                    cblItem.Attributes.Add("Style", "color:blue;")



                End If

                If cblItem.ToString.ToLower = "mak" And cblItem.Selected = True Then

                    snotice = "true"
                End If
            Next
        Next row

        If snotice = "true" Then
            If request_descTextBox.Text = "" Then
                request_descTextBox.Text = "Use This Mak ID ="
            Else
                MakCont = request_descTextBox.Text.Contains("Mak ID")
                If MakCont = False Then
                    request_descTextBox.Text = request_descTextBox.Text & " Please Add Mak ID "
                End If
            End If

            request_descTextBox.BackColor() = Color.Yellow
            request_descTextBox.BorderColor() = Color.Black
            request_descTextBox.BorderStyle() = BorderStyle.Solid
            request_descTextBox.BorderWidth = Unit.Pixel(2)
        End If


        Dim BusLog As New AccountBusinessLogic

        If BusLog.eCare Then
            pnlTCl.Visible = True
        Else
            pnlTCl.Visible = False
            pageCtrl.RemoveCssClass(pnlTCl, "textInputRequired")

        End If

        If mainRole = "1194" Then
            Titleddl.SelectedValue = Title2TextBox.Text
            'Title2TextBox.Text
            TitleLabel.Text = Titleddl.SelectedItem.ToString

            'Title2TextBox.Text = Titleddl.SelectedItem.ToString

            Title2TextBox.Visible = True
            Titleddl.Visible = False

        End If

        ChangColorNoCheck()
        'ChangColor()

    End Sub
    Protected Sub SuppSupportrbl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles SuppSupportrbl.SelectedIndexChanged
        If SuppSupportrbl.SelectedValue.ToLower = "yes" Then
            entity_cdDDL.SelectedValue = "150"

            Dim ddbDepartmentBinder As New DropDownListBinder(department_cdddl, "selDepartmentCodesV4", Session("EmployeeConn"))
            ddbDepartmentBinder.AddDDLCriteria("@entity_cd", "150", SqlDbType.NVarChar)
            ddbDepartmentBinder.BindData("department_cd", "department_name")

            department_cdddl.SelectedValue = "601000"
            CheckEntitiesHosp()

            'Else
            entity_cdDDL.SelectedIndex = -1
            department_cdddl.SelectedIndex = -1

            For Each cblEntitem As ListItem In cblEntities.Items
                cblEntitem.Selected = False
            Next

            For Each cblFacItem As ListItem In cblFacilities.Items
                cblFacItem.Selected = False
            Next

        End If
        ChangColorNoCheck()

        'ChangColor()

    End Sub
    Public Sub ChangColor()
        ' ADD get Table here

        If user_position_descTextBox.Text Is Nothing Then
            Exit Sub
        End If

        Dim thisdata As New CommandsSqlAndOleDb("SelApplicationExtensionByRoleNum", Session("EmployeeConn"))


        If hanrbl.SelectedValue.ToString = "Yes" Then
            If user_position_descTextBox.Text = "782" Then
                user_position_descTextBox.Text = "1553"
            End If
        End If

        thisdata.AddSqlProcParameter("@roleNum", user_position_descTextBox.Text, SqlDbType.NVarChar, 9)
        thisdata.AddSqlProcParameter("@display", "base", SqlDbType.NVarChar, 9)

        thisdata.ExecNonQueryNoReturn()

        dtApplications = thisdata.GetSqlDataTable

        Dim Appnum As Integer
        Dim appDeafult As String

        For Each row As DataRow In dtApplications.Rows
            Appnum = row("ApplicationNum")
            appDeafult = row("DefaultValue")
            gAppName = row("ApplicationDesc")


            For Each cblItem As ListItem In cblApplications.Items
                Dim sCurritem As String = cblItem.Value
                ' Only turn Blue color on
                If sCurritem = Appnum And appDeafult.ToLower = "no" Then
                    If cblItem.Selected = True Then
                        cblItem.Selected = True
                        cblItem.Attributes.Add("Style", "color:blue;")

                    Else
                        cblItem.Selected = False
                        cblItem.Attributes.Add("Style", "color:blue;")

                    End If

                ElseIf sCurritem = Appnum And appDeafult.ToLower = "yes" Then
                    cblItem.Enabled = True
                    cblItem.Selected = True
                    cblItem.Attributes.Add("Style", "color:black;")

                End If


            Next
        Next row

    End Sub

    Public Sub ChangColorNoCheck()
        ' ADD get Table here

        If user_position_descTextBox.Text Is Nothing Then
            Exit Sub
        End If

        Dim thisdata As New CommandsSqlAndOleDb("SelApplicationExtensionByRoleNum", Session("EmployeeConn"))


        'If hanrbl.SelectedValue.ToString = "Yes" Then
        '    thisdata.AddSqlProcParameter("@roleNum", "1542", SqlDbType.NVarChar, 9)

        'Else
        '    thisdata.AddSqlProcParameter("@roleNum", user_position_descTextBox.Text, SqlDbType.NVarChar, 9)

        'End If

        thisdata.AddSqlProcParameter("@roleNum", user_position_descTextBox.Text, SqlDbType.NVarChar, 9)
        thisdata.AddSqlProcParameter("@display", "base", SqlDbType.NVarChar, 9)

        thisdata.ExecNonQueryNoReturn()

        dtApplications = thisdata.GetSqlDataTable

        Dim Appnum As Integer
        Dim appDeafult As String

        For Each row As DataRow In dtApplications.Rows
            Appnum = row("ApplicationNum")
            appDeafult = row("DefaultValue")
            gAppName = row("ApplicationDesc")


            For Each cblItem As ListItem In cblApplications.Items
                Dim sCurritem As String = cblItem.Value
                ' Only turn Blue color on
                If sCurritem = Appnum And appDeafult.ToLower = "no" Then
                    cblItem.Attributes.Add("Style", "color:blue;")


                ElseIf sCurritem = Appnum And appDeafult.ToLower = "yes" Then
                    'cblItem.Enabled = True
                    'cblItem.Selected = True
                    cblItem.Attributes.Add("Style", "color:black;")

                End If


            Next
        Next row

    End Sub
    Protected Sub hanrbl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles hanrbl.SelectedIndexChanged
        If hanrbl.SelectedValue.ToString = "Yes" Then


            LocationOfCareIDddl.Visible = True
            ckhnlbl.Visible = True

            LocationOfCareIDddl.BackColor() = Color.Yellow
            LocationOfCareIDddl.BorderColor() = Color.Black
            LocationOfCareIDddl.BorderStyle() = BorderStyle.Solid
            LocationOfCareIDddl.BorderWidth = Unit.Pixel(2)

            npitextbox.BackColor() = Color.Yellow
            npitextbox.BorderColor() = Color.Black
            npitextbox.BorderStyle() = BorderStyle.Solid
            npitextbox.BorderWidth = Unit.Pixel(2)

            LicensesNotextbox.BackColor() = Color.Yellow
            LicensesNotextbox.BorderColor() = Color.Black
            LicensesNotextbox.BorderStyle() = BorderStyle.Solid
            LicensesNotextbox.BorderWidth = Unit.Pixel(2)

            taxonomytextbox.BackColor() = Color.Yellow
            taxonomytextbox.BorderColor() = Color.Black
            taxonomytextbox.BorderStyle() = BorderStyle.Solid
            taxonomytextbox.BorderWidth = Unit.Pixel(2)

            Rolesddl.SelectedValue = "1553"
            Rolesddl.Enabled = False

            user_position_descTextBox.Text = Rolesddl.SelectedValue
            mainRole = Rolesddl.SelectedValue

            'Title2TextBox.Text = Rolesddl.SelectedItem.ToString
            If thisRole Is Nothing Then
                thisRole = Rolesddl.SelectedValue
            End If

            'Rolesddl.SelectedValue 
            'user_position_descTextBox.Text = 

            'lblprov.Visible = True
            'providerrbl.Visible = True

            'CommLocationOfCareIDddl.BackColor() = Color.Orange
            'CommLocationOfCareIDddl.BorderColor() = Color.Black
            'CommLocationOfCareIDddl.BorderStyle() = BorderStyle.Solid
            'CommLocationOfCareIDddl.BorderWidth = Unit.Pixel(2)

        Else
            If Rolesddl.SelectedValue = "1553" Then
                Response.Redirect("SignOnForm.aspx")
                Exit Sub

            End If
            LocationOfCareIDddl.Visible = False
            ckhnlbl.Visible = False

            'LocationOfCareLabel.Text = ""

            'lblprov.Visible = False
            'providerrbl.Visible = False
            'providerrbl.SelectedIndex = -1

            'CommLocationOfCareIDddl.BackColor() = Color.White
            'CommLocationOfCareIDddl.BorderColor() = Color.Black
            'CommLocationOfCareIDddl.BorderStyle() = BorderStyle.Groove
            'CommLocationOfCareIDddl.BorderWidth = Unit.Pixel(2)

        End If

        ChangColor()

    End Sub

    Protected Sub btnChangeTCL_Click(sender As Object, e As System.EventArgs) Handles btnChangeTCL.Click
        If btnChangeTCL.Text = "Show All User Types" Then

            Dim tcldata As New CommandsSqlAndOleDb("SelInvisionUserTypeV2", Session("EmployeeConn"))
            tcldata.AddSqlProcParameter("@rolenum", masterRole.Text, SqlDbType.NVarChar, 8)
            tcldata.AddSqlProcParameter("@Clientupd", "all", SqlDbType.NVarChar, 3)

            tcldata.GetSqlDataset()

            dtTCLUserTypes = tcldata.GetSqlDataTable

            gvTCL.DataSource = dtTCLUserTypes
            gvTCL.DataBind()

            gvTCL.Visible = True
            gvTCL.BackColor() = Color.Yellow
            gvTCL.BorderColor() = Color.Black
            gvTCL.BorderStyle() = BorderStyle.Solid
            gvTCL.BorderWidth = Unit.Pixel(2)
            gvTCL.RowStyle.BackColor() = Color.Yellow

            pnlTCl.Visible = True
            TCLTextBox.Visible = True
            UserTypeDescTextBox.Visible = True
            UserTypeCdTextBox.Visible = True

            btnChangeTCL.Text = "Close Types"


        ElseIf btnChangeTCL.Text = "Close Types" Then
            gvTCL.Dispose()
            gvTCL.Visible = False
            btnChangeTCL.Text = "Show All User Types"
        Else
            gvTCL.Visible = True
            gvTCL.BackColor() = Color.Yellow
            gvTCL.BorderColor() = Color.Black
            gvTCL.BorderStyle() = BorderStyle.Solid
            gvTCL.BorderWidth = Unit.Pixel(2)
            gvTCL.RowStyle.BackColor() = Color.Yellow

            btnChangeTCL.Visible = False

        End If



    End Sub

    'Protected Sub CommLocationOfCareIDddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles CommLocationOfCareIDddl.SelectedIndexChanged
    '    If CommLocationOfCareIDLabel.Text <> "" Then

    '        CommLocationOfCareIDLabel.Text = CommLocationOfCareIDddl.SelectedItem.ToString
    '        CommNumberGroupNumLabel.Text = CommLocationOfCareIDddl.SelectedValue
    '    ElseIf CommLocationOfCareIDddl.SelectedValue.ToLower = "select" Or CommLocationOfCareIDddl.SelectedValue = "99999" Then

    '        CommLocationOfCareIDLabel.Text = Nothing
    '        CommNumberGroupNumLabel.Text = Nothing

    '    Else

    '        CommLocationOfCareIDLabel.Text = CommLocationOfCareIDddl.SelectedItem.ToString
    '        CommNumberGroupNumLabel.Text = CommLocationOfCareIDddl.SelectedValue


    '    End If

    '    ChangColorNoCheck()

    '    'ChangColor()

    'End Sub
    Protected Sub LocationOfCareIDddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles LocationOfCareIDddl.SelectedIndexChanged
        ' Gnumber
        If LocationOfCareIDLabel.Text <> "" Then
            LocationOfCareIDLabel.Text = LocationOfCareIDddl.SelectedItem.ToString
            GNumberGroupNumLabel.Text = LocationOfCareIDddl.SelectedValue

        ElseIf LocationOfCareIDddl.SelectedValue.ToLower = "select" Or LocationOfCareIDddl.SelectedValue = "99999" Then

            LocationOfCareIDLabel.Text = Nothing
            GNumberGroupNumLabel.Text = Nothing

        Else

            LocationOfCareIDLabel.Text = LocationOfCareIDddl.SelectedItem.ToString
            GNumberGroupNumLabel.Text = LocationOfCareIDddl.SelectedValue

        End If
        ChangColorNoCheck()

        'ChangColor()

    End Sub
    'Protected Sub CoverageGroupddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles CoverageGroupddl.SelectedIndexChanged
    '    ' Coverage group
    '    If CoverageGroupNumLabel.Text <> "" Then

    '        CoverageGroupNumLabel.Text = CoverageGroupddl.SelectedItem.ToString
    '        CommNumberGroupNumLabel.Text = CoverageGroupddl.SelectedValue


    '    ElseIf CoverageGroupddl.SelectedValue.ToLower = "select" Or CoverageGroupddl.SelectedValue = "99999" Then

    '        CoverageGroupNumLabel.Text = Nothing
    '        CommNumberGroupNumLabel.Text = Nothing
    '    Else

    '        CoverageGroupNumLabel.Text = CoverageGroupddl.SelectedItem.ToString
    '        CommNumberGroupNumLabel.Text = CoverageGroupddl.SelectedValue

    '    End If
    '    ChangColorNoCheck()

    '    'ChangColor()

    'End Sub

    'Protected Sub Nonperferdddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles Nonperferdddl.SelectedIndexChanged
    '    ' Non perfered group
    '    If NonPerferedGroupnumLabel.Text <> "" Then

    '        NonPerferedGroupnumLabel.Text = Nonperferdddl.SelectedValue

    '    ElseIf Nonperferdddl.SelectedValue.ToLower = "select" Or Nonperferdddl.SelectedValue = "99999" Then

    '        NonPerferedGroupnumLabel.Text = Nothing
    '    Else

    '        NonPerferedGroupnumLabel.Text = Nonperferdddl.SelectedValue

    '    End If
    '    ChangColorNoCheck()

    '    'ChangColor()


    'End Sub


End Class
