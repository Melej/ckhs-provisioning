﻿<%@ Page Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="false" CodeFile="RequestDetail.aspx.vb" Inherits="RequestDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <style type="text/css">
        .style12
        {
            height: 30px;
            width: 518px;
        }        
        .ui-datepicker-calendar { 
        display: none; 
        } 
        .style14
        {
            font-size: medium;
        }
        .style17
        {
            width: 418px;
        }
        .style18
        {
            width: 92px;
        }
        .style19
        {
            width: 164px;
        }
        .style20
        {
            width: 145px;
        }

        </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.min.js"></script> 

    <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="10"  >
   <ProgressTemplate>

        <div id="IMGDIV" align="center" valign="middle" runat="server" style="position:
                absolute;left: 400px;top:150px;visibility:visible;vertical-align:middle;border-style:
                inset;border-color:black;background-color:White;width:310px;height:75px; z-index: 999;">
        Loading Data Please Wait ...
            <img src="Images/AjaxProgress.gif" />
        </div>

   </ProgressTemplate>
</asp:UpdateProgress>


<%--<asp:UpdatePanel ID = "uplPanel" runat="server">
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="btnUpdate" EventName="Click" />
    </Triggers>
</asp:UpdatePanel>
--%>
<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
    <ContentTemplate>   
  
    <table id="maintbl">
    <%-- Row 1 --%>
    
    <tr>
        <td align="center">
            <table>
                <tr>
                   <td class="style14" align="center">
                        <strong>Account Request Report</strong>
                    </td>
                 </tr>
                  <tr>
                     <td  class="style14">
                        <asp:Label ID="lblnotice" runat="server" Text=""></asp:Label>
                      </td>
                  </tr>
              </table>
         </td>   
    </tr>
 <%-- Row 2 --%>
    <tr>
       <td class="style12" >
         <asp:Label ID="LbMonthly" runat="server"></asp:Label>
      </td>
    </tr>
<%-- Row 3 --%>
<%--     <tr>
       <td class="style17">
          <table id="tblTypeAndReset">
             <tr>
                <td>
                    <asp:Button ID="Reset" runat="server" Text="Reset" height="25px" Width="100px" />

                </td>
                <td>
                    <asp:Button ID="btnExcel" runat="server" Text="Excel" Width="99px" />
                </td> 
                <td>
                    <asp:Button ID="btnUserActivity" runat="server" Text="User Activity" Width="99px" />
                </td>
                <td>
                    <asp:Button ID="btnPdf" runat="server" Text="Print " height="25px" Width="100px" Visible="False" />
               </td>
             </tr>
          </table>
        </td>
     </tr>
--%> <%-- Row 4 --%>
     <tr>
                <td align="center" >
                    <table>
                        <tr>
                            <td>
                            
                                    <asp:RadioButtonList ID = "AccountTyperbl" runat="server" 
                                    AutoPostBack="true" RepeatDirection="Horizontal">
                                    <asp:ListItem Selected="True" Value="both"> Add & Delete Requests</asp:ListItem>
                                    <asp:ListItem Value="open"> Add Account Requests</asp:ListItem>
                                    <asp:listItem Value="closed">Delete Account Requests</asp:listItem>
                                </asp:RadioButtonList>
                            
                            </td> 
                          </tr>
                          <tr>
                            <td>
                            
                                    <asp:RadioButtonList ID = "Statusrbl" runat="server" 
                                    AutoPostBack="true" RepeatDirection="Horizontal">
                                    <asp:ListItem Selected="True" Value="both"> Open & Closed Account Req.</asp:ListItem>
                                    <asp:ListItem Value="open"> Open Account Requests</asp:ListItem>
                                    <asp:listItem Value="closed">Closed Account Requests</asp:listItem>
                                </asp:RadioButtonList>
                            
                            </td>
                        </tr>
                    </table>

                </td>
     </tr>
 <%-- Row 5 --%>
     <tr>
      <td align="center">

                           <asp:GridView ID="grdAccounts" runat="server" AllowSorting="True" 
                                        AutoGenerateColumns="False" BackColor="White" BorderColor="#336666"
                                        BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                                        EmptyDataText="No Applications Found" EnableTheming="False" 
                                        Font-Size="Small" GridLines="Horizontal" Width="90%"
                                        DataKeyNames="AppBase">
								
                                         <FooterStyle BackColor="White" ForeColor="#333333" />
                                        <HeaderStyle BackColor="#336666" Font-Bold="False" ForeColor="White" Font-Size="Small" HorizontalAlign="Left"    />                                    
                                        <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Left" Font-Size="Medium"/>
                                        <RowStyle BackColor="White" ForeColor="#333333" />
                                        <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                        <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                        <SortedAscendingHeaderStyle BackColor="#487575" />
                                        <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                        <SortedDescendingHeaderStyle BackColor="#275353" />

								<Columns>
                                   <%-- <asp:CommandField ButtonType="Button" SelectText="Select" ShowSelectButton= "true" ControlStyle-BackColor="#84A3A3" ItemStyle-Height="30px"/>--%>

                                   <asp:CommandField ButtonType="Button" SelectText=" Select " ShowSelectButton= "true" ControlStyle-BackColor="#84A3A3" ItemStyle-Height="30px"/>

									<asp:BoundField DataField="TotalByApp" HeaderText="Total By App"  HeaderStyle-HorizontalAlign="Center"  ItemStyle-VerticalAlign="Middle" ItemStyle-Height="35px" HeaderStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Center"></asp:BoundField>

                                    <asp:BoundField DataField="TotalAddByApp" HeaderText="Total Adds " HeaderStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" ItemStyle-Height="35px" HeaderStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                    <asp:BoundField DataField="TotalDelbyApp" HeaderText="Total Delets " HeaderStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" ItemStyle-Height="35px" HeaderStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                    
                                    <asp:BoundField DataField="ApplicationDesc" HeaderText="Application " HeaderStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" ItemStyle-Height="35px" HeaderStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Left"></asp:BoundField>



                                  </Columns>

                         </asp:GridView>


                           <asp:GridView ID="grdDetails" runat="server" AllowSorting="True" 
                                        AutoGenerateColumns="False" BackColor="White" BorderColor="#336666"
                                        BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                                        EmptyDataText="No Applications Found" EnableTheming="False" 
                                        Font-Size="Small" GridLines="Horizontal" Width="90%"
                                        DataKeyNames="account_request_num" Visible="false">
								
                                         <FooterStyle BackColor="White" ForeColor="#333333" />
                                        <HeaderStyle BackColor="#336666" Font-Bold="False" ForeColor="White" Font-Size="Small" HorizontalAlign="Left"    />                                    
                                        <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Left" Font-Size="Medium"/>
                                        <RowStyle BackColor="White" ForeColor="#333333" />
                                        <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                        <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                        <SortedAscendingHeaderStyle BackColor="#487575" />
                                        <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                        <SortedDescendingHeaderStyle BackColor="#275353" />

								<Columns>
                                   <%-- <asp:CommandField ButtonType="Button" SelectText="Select" ShowSelectButton= "true" ControlStyle-BackColor="#84A3A3" ItemStyle-Height="30px"/>--%>

                                   <asp:CommandField ButtonType="Button" SelectText=" View Request " ShowSelectButton= "true" ControlStyle-BackColor="#84A3A3" ItemStyle-Height="30px"/>

                                    <asp:BoundField DataField="account_request_num" HeaderText="Request # " HeaderStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" ItemStyle-Height="35px" HeaderStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Left"></asp:BoundField>

                                    <asp:BoundField DataField="ApplicationDesc" HeaderText="Application " HeaderStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" ItemStyle-Height="35px" HeaderStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Left"></asp:BoundField>

                                    <asp:BoundField DataField="CurrStatus" HeaderText="Status " HeaderStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" ItemStyle-Height="35px" HeaderStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                    <asp:BoundField DataField="RequestType" HeaderText="Request Type " HeaderStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" ItemStyle-Height="35px" HeaderStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                    



                                  </Columns>

                         </asp:GridView>

      </td>
     </tr>

     <tr>
        <td>
            <asp:Label ID="accounttypelbl" runat="server" Visible="false"></asp:Label>
            <asp:TextBox ID="Curryeartxt" runat="server" Visible="false"></asp:TextBox>
        </td>
     </tr>
</table>
   </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="grdDetails" EventName="RowCommand" />
    </Triggers>
 </asp:UpdatePanel>
</asp:Content>
