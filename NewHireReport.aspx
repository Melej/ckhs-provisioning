﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="false" CodeFile="NewHireReport.aspx.vb" Inherits="NewHireReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
      <style type="text/css">
        .style3
    {
        width: 371px;
    }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

      <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="100"  >
   <ProgressTemplate>
        <div id="IMGDIV" align="center" valign="middle" runat="server" style="position:
                absolute;left: 400px;top:150px;visibility:visible;vertical-align:middle;border-style:
                inset;border-color:black;background-color:White;width:210px;height:75px; z-index: 999;">
        Loading Data ...
            <img src="Images/AjaxProgress.gif" /><br />
        </div>
   </ProgressTemplate>
</asp:UpdateProgress>
 
  <ContentTemplate>
    <table   width="100%">
        <tr align="center">
           <td class="tableRowHeader" colspan="2">
                  New Hire Report
           </td>
        </tr>
        <tr>
            <td  align="center"  colspan="2">

                  <asp:Button ID="btnSubmit" runat="server" Text = "Submit" 
                       CssClass="btnhov" BackColor="#006666"  Width="179px"/>
                                
                <asp:Button ID="btnExcel" runat="server" Text="Excel RPT." 
                             CssClass="btnhov" BackColor="#006666"  Width="179px" />
            </td>


        </tr>
        <tr>
            <td align="center" colspan="2" >
                Start Date:
                <asp:TextBox ID="txtStartDate" runat="server" CssClass="calenderClass"></asp:TextBox>
 
                End Date:

                <asp:TextBox ID="txtEndDate" runat="server" CssClass="calenderClass"></asp:TextBox>
            </td>

        </tr>
        <tr>
            <td align="center" colspan="2" >
                Display only PMH Hires ?

                <asp:RadioButtonList ID = "AllRequestrbl" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Selected="True" Value="employees">Yes</asp:ListItem>
                    <asp:listItem  Value="all">No</asp:listItem>

            </asp:RadioButtonList>

            </td>
        
        </tr>

        <tr>
             <td colspan="2">
                         <asp:GridView ID="GvnewHires" runat="server" AllowSorting="True" 
                           AutoGenerateColumns="False" BackColor="White" BorderColor="#336666" 
                           BorderStyle="Double" BorderWidth="3px" CellPadding="1" 
                           DataKeyNames="account_request_num" 
                           EmptyDataText="No Accounts found "  
                             EnableTheming="False" Font-Size="Medium" GridLines="Horizontal" Width="100%">

                           <FooterStyle BackColor="White" ForeColor="#333333" />
                           <HeaderStyle BackColor="#336666" Font-Bold="False" Font-Size="X-Small" 
                               ForeColor="White" HorizontalAlign="Left" VerticalAlign="Middle" />
                           <PagerStyle BackColor="#336666" Font-Size="Medium" ForeColor="White" 
                               HorizontalAlign="Left" />
                           <RowStyle BackColor="White" ForeColor="#333333" Font-Size="Small" />
                           <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />

                           <SortedAscendingCellStyle BackColor="#F7F7F7" />
                           <SortedAscendingHeaderStyle BackColor="#487575" />
                           <SortedDescendingCellStyle BackColor="#E5E5E5" />
                           <SortedDescendingHeaderStyle BackColor="#275353" />
                           <Columns>

                               <asp:CommandField ButtonType="Button" ControlStyle-BackColor="#84A3A3" 
                                      HeaderStyle-HorizontalAlign="Left" ItemStyle-Height="20px" SelectText="Select" 
                                      ShowSelectButton="true">
                                  <ControlStyle BackColor="#84A3A3" />
                                  <HeaderStyle HorizontalAlign="Left" />
                                  <ItemStyle Height="20px" />
                                  </asp:CommandField>
                            
                               <asp:BoundField DataField="PersonNbr"  ItemStyle-HorizontalAlign="Left" HeaderText="PersonNbr" />

                               <asp:BoundField DataField="EmployeeName" ItemStyle-HorizontalAlign="Left" HeaderText="EmployeeName " />

                               <asp:BoundField DataField="AccountName" ItemStyle-HorizontalAlign="Left" HeaderText="AccountName" />

                               <asp:BoundField DataField="EMail" ItemStyle-HorizontalAlign="Left" HeaderText="EMail" />

                               <asp:BoundField DataField="DepartmentName"  ItemStyle-HorizontalAlign="Left" HeaderText="Dept. Name" />

                               <asp:BoundField DataField="PositionCode"  ItemStyle-HorizontalAlign="Left" HeaderText="Position" />

                            <asp:BoundField DataField="FirstName"  ItemStyle-HorizontalAlign="Left"  Visible="false" />
                               <asp:BoundField DataField="lastName"  ItemStyle-HorizontalAlign="Left"  Visible="false" />
                               <asp:BoundField DataField="StartDate"  ItemStyle-HorizontalAlign="Left"  Visible="false" />
                               <asp:BoundField DataField="AccountType"  ItemStyle-HorizontalAlign="Left"  Visible="false" />
                  

                           </Columns>
                       </asp:GridView>                            
            </td>
        </tr>

                <tr>
             <td colspan="2">
                         <asp:GridView ID="HRNewHiresGrid" runat="server" AllowSorting="True" 
                           AutoGenerateColumns="False" BackColor="White" BorderColor="#336666" 
                           BorderStyle="Double" BorderWidth="3px" CellPadding="1" 
                           DataKeyNames="account_request_num" 
                           EmptyDataText="No Accounts found " Visible="false"
                             EnableTheming="False" Font-Size="Medium" GridLines="Horizontal" Width="100%">

                           <FooterStyle BackColor="White" ForeColor="#333333" />
                           <HeaderStyle BackColor="#336666" Font-Bold="False" Font-Size="X-Small" 
                               ForeColor="White" HorizontalAlign="Left" VerticalAlign="Middle" />
                           <PagerStyle BackColor="#336666" Font-Size="Medium" ForeColor="White" 
                               HorizontalAlign="Left" />
                           <RowStyle BackColor="White" ForeColor="#333333" Font-Size="Small" />
                           <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />

                           <SortedAscendingCellStyle BackColor="#F7F7F7" />
                           <SortedAscendingHeaderStyle BackColor="#487575" />
                           <SortedDescendingCellStyle BackColor="#E5E5E5" />
                           <SortedDescendingHeaderStyle BackColor="#275353" />
                           <Columns>

                            
                               <asp:BoundField DataField="PersonNbr"  ItemStyle-HorizontalAlign="Left" HeaderText="PersonNbr" />

                               <asp:BoundField DataField="EmployeeName" ItemStyle-HorizontalAlign="Left" HeaderText="EmployeeName " />

                               <asp:BoundField DataField="AccountName" ItemStyle-HorizontalAlign="Left" HeaderText="AccountName" />

                               <asp:BoundField DataField="EMail" ItemStyle-HorizontalAlign="Left" HeaderText="EMail" />

                               <asp:BoundField DataField="DepartmentName"  ItemStyle-HorizontalAlign="Left" HeaderText="Dept. Name" />

                               <asp:BoundField DataField="PositionCode"  ItemStyle-HorizontalAlign="Left" HeaderText="Position" />

                            <asp:BoundField DataField="FirstName"  ItemStyle-HorizontalAlign="Left"  Visible="false" />
                               <asp:BoundField DataField="lastName"  ItemStyle-HorizontalAlign="Left"  Visible="false" />
                               <asp:BoundField DataField="StartDate"  ItemStyle-HorizontalAlign="Left"  Visible="false" />
                               <asp:BoundField DataField="AccountType"  ItemStyle-HorizontalAlign="Left"  Visible="false" />
                  

                           </Columns>
                       </asp:GridView>                            
            </td>
        </tr>

        <tr>
            <td colspan="2">

                <asp:GridView ID="HDGrid" runat="server" AllowSorting="True" 
                           AutoGenerateColumns="False" BackColor="White" BorderColor="#336666" 
                           BorderStyle="Double" BorderWidth="3px" CellPadding="1" 
                           DataKeyNames="account_request_num" 
                           EmptyDataText="No Accounts found "   Visible="false"
                             EnableTheming="False" Font-Size="Medium" GridLines="Horizontal" Width="100%">

                           <FooterStyle BackColor="White" ForeColor="#333333" />
                           <HeaderStyle BackColor="#336666" Font-Bold="False" Font-Size="X-Small" 
                               ForeColor="White" HorizontalAlign="Left" VerticalAlign="Middle" />
                           <PagerStyle BackColor="#336666" Font-Size="Medium" ForeColor="White" 
                               HorizontalAlign="Left" />
                           <RowStyle BackColor="White" ForeColor="#333333" Font-Size="Small" />
                           <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />

                           <SortedAscendingCellStyle BackColor="#F7F7F7" />
                           <SortedAscendingHeaderStyle BackColor="#487575" />
                           <SortedDescendingCellStyle BackColor="#E5E5E5" />
                           <SortedDescendingHeaderStyle BackColor="#275353" />
                           <Columns>

                               <asp:CommandField ButtonType="Button" ControlStyle-BackColor="#84A3A3" 
                                      HeaderStyle-HorizontalAlign="Left" ItemStyle-Height="20px" SelectText="Select" 
                                      ShowSelectButton="true">
                                  <ControlStyle BackColor="#84A3A3" />
                                  <HeaderStyle HorizontalAlign="Left" />
                                  <ItemStyle Height="20px" />
                                  </asp:CommandField>
                            
                               <asp:BoundField DataField="PersonNbr"  ItemStyle-HorizontalAlign="Left" HeaderText="PersonNbr" />

                               <asp:BoundField DataField="EmployeeName" ItemStyle-HorizontalAlign="Left" HeaderText="EmployeeName " />

                               <asp:BoundField DataField="AccountName" ItemStyle-HorizontalAlign="Left" HeaderText="AccountName" />

                               <asp:BoundField DataField="EMail" ItemStyle-HorizontalAlign="Left" HeaderText="EMail" />

                               <asp:BoundField DataField="DepartmentName"  ItemStyle-HorizontalAlign="Left" HeaderText="Dept. Name" />

                               <asp:BoundField DataField="PositionCode"  ItemStyle-HorizontalAlign="Left" HeaderText="Position" />

                            <asp:BoundField DataField="FirstName"  ItemStyle-HorizontalAlign="Left"   HeaderText="FirstName" />
                               <asp:BoundField DataField="lastName"  ItemStyle-HorizontalAlign="Left"   HeaderText="lastName" />
                               <asp:BoundField DataField="StartDate"  ItemStyle-HorizontalAlign="Left"   HeaderText="StartDate" />
                               <asp:BoundField DataField="AccountType"  ItemStyle-HorizontalAlign="Left"  HeaderText="AccountType" />
                  

                           </Columns>
                       </asp:GridView> 
            </td>
        </tr>
        <tr>
          <td colspan="2">
         <asp:Label ID="HDStartDate" runat="server" Visible="false"></asp:Label>
            <asp:Label ID="HDEnddate" runat="server" Visible="false"></asp:Label>
               <asp:Label ID="HDReportType" runat="server" Visible="false"></asp:Label>

            <asp:Table  Width="100%" ID = "tblEndDate" runat = "server" Visible="false">
            </asp:Table>
          </td>
        </tr>

  </table> 
  
  </ContentTemplate>
 
 
</asp:Content>

