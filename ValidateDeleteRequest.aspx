﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="false" CodeFile="ValidateDeleteRequest.aspx.vb" Inherits="ValidateDeleteRequest" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
 <asp:UpdatePanel ID="uplViewRequest" runat ="server" >
  <ContentTemplate>
   <table class="wrapper" style="border-style: groove" align="left">
    <tr>
      <td>
        <table border="3" style="empty-cells:show"  cellpadding ="5px" width="80%">
           <tr>
                <th colspan = "4" class="tableRowHeader">
                    Validate Delete Request
                </th>     
           </tr>
            <tr align="center">
                <td colspan="4">
                    <table>
                       <tr>
                          <td align="center">
                  
                                <asp:Button ID = "btnValidate" runat = "server" Text = "Validate Request" 
                                CssClass="btnhov" BackColor="#006666"  Width="135px"/>
                       
                           </td>
                            <td align="center">
                                <asp:Button ID = "btnCancel" runat = "server" Text = "Cancel" 
                                CssClass="btnhov" BackColor="#006666"  Width="135px" />
                            </td>

                            <td align="center">
                                <asp:Button ID="btnGoToRequest" runat="server" Text="Go To Request " 
                                CssClass="btnhov" BackColor="#006666"  Width="135px" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
           <tr>
            <td style="font-weight:bold">
               Client Name
            </td>
             <td>
                <asp:Label ID = "first_namelabel" runat = "server"></asp:Label>&nbsp<asp:Label ID = "last_namelabel" runat = "server" ></asp:Label>
            </td>
            <td style="font-weight:bold" >
                Requestor Name
            </td>
            <td >
                <asp:Label ID = "requestor_namelabel" runat = "server"></asp:Label>
            </td>
           </tr>
           <tr>
                 <td  style="font-weight:bold">
                    Request Type
                </td>     
        
                 <td colspan="3" >
                     <asp:Label ID = "RequestTypeDescLabel" runat = "server"></asp:Label>
                </td>    
                 
            </tr>
            <tr>
                <td style="font-weight:bold" >
                    Submit Date
                </td>
               
                 <td colspan="3">
                    <asp:Label ID = "entered_datelabel" runat = "server"></asp:Label>
                </td>
            </tr>
          <tr>
            <td colspan = "4">
                  <asp:GridView ID="gvAppRequests" runat="server" AllowSorting="True" 
                                    AutoGenerateColumns="False" BackColor="White" BorderColor="#336666"
                                    BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                                    EmptyDataText="No Accounts" EnableTheming="False" DataKeyNames = "account_request_seq_num, login_name, ApplicationNum"
                                    Font-Size="Medium" GridLines="Horizontal" Width="100%">
                                   
                                    <FooterStyle BackColor="White" ForeColor="#333333" />
                                    <HeaderStyle BackColor="#336666" Font-Bold="False" ForeColor="White" Font-Size="Small" HorizontalAlign="Left"    />                                    
                                    <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Left" Font-Size="Medium"/>
                                    <RowStyle BackColor="White" ForeColor="#333333" />
                                    <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                    <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                    <SortedAscendingHeaderStyle BackColor="#487575" />
                                    <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                    <SortedDescendingHeaderStyle BackColor="#275353" />

                                    <Columns>
                                      <asp:TemplateField>  

                                                 <ItemTemplate>  

                                                         <asp:Button ID="btnLinkView" runat="server" onclick="btnInvalid_Click" Text="Invalid"  BackColor="#336666" ForeColor="White"></asp:Button> 
                                                 </ItemTemplate>

                                      </asp:TemplateField>
                                       <asp:BoundField  DataField="ApplicationDesc"  HeaderText="Application" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
                                       <asp:BoundField  DataField="login_name"  HeaderText="User Name" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
                                  
                                  </Columns>

                                </asp:GridView>

            </td>
          </tr>
        </table>

         <asp:Panel ID="pnlInvalidItem" runat="server" Height="350px" Width="475px" CssClass="ModalPopUpPanel" style="display:none">
            <table class="ModalPopupTable" style="border: medium groove #000000; height:100%"  align="center" width="100%"  >
                <tr>
                    <th colspan="3">
                    Invalid Request
                    </th>
                </tr>
                <tr>
<%--                      <td style="width: 3px">
                     </td>--%>

                    <td align = "center" style="background-color:#006666; color:#efefef;">
                        <asp:Label ID = "lblInvalidRequestItem" runat = "server" Font-Bold="true"></asp:Label>
                        <asp:Label ID = "lblHiddenSeqNum" runat = "server" visible = "false"></asp:Label>
                        <asp:Label ID = "lblHiddenLogin" runat = "server" visible = "false"></asp:Label>
                    </td>
<%--                      <td style="width: 3px">
                     </td>--%>

                </tr>
                <tr>
<%--                      <td style="width: 3px">
                     </td>
--%>
                    <td align="center" style="height: 25px">
                        Comments:
                    </td>
<%--                      <td style="width: 3px">
                     </td>--%>

                </tr>
                <tr>
<%--                      <td style="width: 3px">
                     </td>--%>

                  <td align="center" style="height: 150px">
                      <asp:TextBox ID = "txtInvalidComments" runat = "server" TextMode ="MultiLine" Height="100px" 
                       Width="450px" MaxLength="75">
                       </asp:TextBox>
                  </td>
<%--                      <td style="width: 3px">
                     </td>--%>

                </tr>
                <tr>


                    <td align = "center" >
                        <asp:Button ID = "btnCloseNav" runat = "server" Text="Cancel"
                        CssClass="btnhov" BackColor="#006666"  Width="125px" />

                        <asp:Button ID = "btnInvalidSubmit" runat = "server" Text="Submit" 
                        CssClass="btnhov" BackColor="#006666"  Width="125px"/>
                    </td>


                </tr>
                                
            </table>
         </asp:Panel>


          <asp:Button ID="btnControlHidden" runat="server" style="display:none" />  
            
          <ajaxToolkit:ModalPopupExtender ID="mpeInvalidItem" runat="server"   
                      DynamicServicePath="" Enabled="True" TargetControlID="btnControlHidden"   
                      PopupControlID="pnlInvalidItem" BackgroundCssClass="ModalBackground"   
                     DropShadow="true" CancelControlID="btnCloseNav">  
       </ajaxToolkit:ModalPopupExtender>  



</td>

</tr>

</table>

   
        
</ContentTemplate>


        </asp:UpdatePanel>


</asp:Content>

