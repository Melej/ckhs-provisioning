﻿Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports App_Code
Imports AjaxControlToolkit
Imports C1.Web.UI.Controls.C1Window
Imports System.Net
Imports System.Net.Mail
Imports System.Net.Mail.SmtpClient
Partial Class SignOnForm

    Inherits MyBasePage '   Inherits System.Web.UI.Page
    Dim sActivitytype As String = ""
	Dim FormSignOn As FormSQL
	Dim PMHSignon As FormSQL
	Dim ReactivateForm As FormSQL
    Dim VendorFormSignOn As FormSQL
    Dim AccountRequestNum As Integer
    Dim Cred As String
    Dim Provider As String
    Dim Han As String
    Dim sEmployeeNum As String
    Dim bHasError As Boolean
    Dim blnReturn As Boolean = False

    Dim thisRole As String
    Dim mainRole As String
	Dim Requesttype As String
	Dim sclose As String
	Dim sItemsadded As String
	Dim sClientSearch As String = ""


	Dim ReactivateClientNum As Integer
    Dim ReHireRequestNum As Integer
	Dim PMHRequestNum As Integer

	Dim Searched As Boolean = False
    Private dtApplications As New DataTable
    Public dtTCLUserTypes As New DataTable
    Public dtCernerPos As New DataTable

    Dim AccountRequest As AccountRequest

	Dim Mailobj As System.Net.Mail.SmtpClient = New System.Net.Mail.SmtpClient
    Dim Mailmsg As New System.Net.Mail.MailMessage
    Dim MailAddress As System.Net.Mail.MailAddress
    Dim mailAttachment As System.Net.Mail.Attachment

    Dim dtAdjustedRoles As DataTable
    Dim pageCtrl As New PageControls()
    'Delegate Function getgridcellNoE(ByVal sFieldName As String, ByRef gridview1 As GridView) As Integer

    'Dim getcellNoE As New getgridcellNoE(AddressOf GridViewFunctionsCls.GetGridCellIndexNoE)

    ' Dim UserInfo As UserSecurity
    ' Dim conn As New SetDBConnection()
    ' Dim c3Controller As New Client3Controller
    ' Dim c3Submitter As New Client3
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'Dim Envoirn As New Environment()
        'thisenviro.Text = Envoirn.getEnvironment
        'testing managers accounts
        'Session("LoginID") = "defs00"
        FormSignOn = New FormSQL(Session("EmployeeConn"), "SelAccountRequestV20", "InsAccountRequestV20", "", Page)
        PMHSignon = New FormSQL(Session("EmployeeConn"), "SelAccountRequestV20", "UpdFullAccountRequestV20", "", Page)

        Dim UserInfo = New UserSecurity(Session("LoginID"), Session("EmployeeConn"))


		If Session("SecurityLevelNum") > 39 And Session("SecurityLevelNum") < 60 Then

            btnSelectOnBehalfOf.Visible = False
		End If


		If Request.QueryString("ClientNum") <> "" Then
			ReactivateClientNum = Request.QueryString("ClientNum")
			ReactivateClientNumTextBox.Text = Request.QueryString("ClientNum")
		End If

		If Request.QueryString("ReHireRequestNum") <> "" Then
			ReHireRequestNum = Request.QueryString("ReHireRequestNum")
			RehireRequestNumTextBox.Text = Request.QueryString("ReHireRequestNum")
		End If

		If Request.QueryString("AccountRequestNum") <> "" Then
			PMHRequestNum = Request.QueryString("AccountRequestNum")
			PMHRequestNumTextBox.Text = Request.QueryString("AccountRequestNum")
			emp_type_cdrbl.Enabled = False

			AccountRequest = New AccountRequest(PMHRequestNum, Session("EmployeeConn"))

			Requesttype = AccountRequest.AccountRequestType
			RequesttypeTextBox.Text = AccountRequest.AccountRequestType

			start_dateTextBox.Text = AccountRequest.StartDate
			request_descTextBox.Text = AccountRequest.RequestDesc

			If request_descTextBox.Text <> "" Then
				request_descTextBox.ForeColor() = Color.Red
			End If
			sclose = AccountRequest.CloseDate
			sItemsadded = AccountRequest.itemsadded

			' all items for this request has been submitted any changes done thur account request
			If sItemsadded <> "" Then
				Response.Redirect("~/AccountRequestInfo2.aspx?requestnum=" & PMHRequestNum)
			End If
			'if Requesttype = "addpmhact" then
		End If


        Dim rblAffiliateBinder As New RadioButtonListBinder(emp_type_cdrbl, "SelAffiliationsV5", Session("EmployeeConn"))

        If Session("SecurityLevelNum") > 69 Or Session("SecurityLevelNum") = 65 Then

			If Requesttype = "" Then '" addpmhact"	show short list
				rblAffiliateBinder.AddDDLCriteria("@type", "contract", SqlDbType.NVarChar)
			Else

				rblAffiliateBinder.AddDDLCriteria("@type", "", SqlDbType.NVarChar)
			End If


		ElseIf Session("SecurityLevelNum") >= 60 Or Session("SecurityLevelNum") <= 64 Then
			If Requesttype = "" Then '" addpmhact"	show short list
				rblAffiliateBinder.AddDDLCriteria("@type", "contract", SqlDbType.NVarChar)
			Else
                If PMHRequestNumTextBox.Text <> Nothing Then
                    rblAffiliateBinder.AddDDLCriteria("@type", "", SqlDbType.NVarChar)
                Else
                    rblAffiliateBinder.AddDDLCriteria("@type", "ckhsemp", SqlDbType.NVarChar)
                End If
                'rblAffiliateBinder.AddDDLCriteria("@type", "ckhsemp", SqlDbType.NVarChar)
            End If

		ElseIf Session("SecurityLevelNum") = 40 Or Session("SecurityLevelNum") = 45 Then

            If Requesttype = "" Then '" addpmhact"	show short list
                rblAffiliateBinder.AddDDLCriteria("@type", "contract", SqlDbType.NVarChar)
            Else
                If PMHRequestNumTextBox.Text <> Nothing Then
                    rblAffiliateBinder.AddDDLCriteria("@type", "", SqlDbType.NVarChar)
                Else
                    rblAffiliateBinder.AddDDLCriteria("@type", "ckhsemp", SqlDbType.NVarChar)
                End If
                'rblAffiliateBinder.AddDDLCriteria("@type", "emp", SqlDbType.NVarChar)
            End If


		End If


		'/SignOnForm.aspx?ClientNum=4076&ReHireRequestNum=10
		'/SignOnForm.aspx?ClientNum=61755&AccountRequestNum=586356

		If Requesttype = "" Then '"addpmhact"
			If lblSearched.Text = "" Then
				SearchPopup.Show()
			End If
		End If



		emp_type_cdrbl.BackColor() = Color.Yellow
		emp_type_cdrbl.BorderColor() = Color.Black
		emp_type_cdrbl.BorderStyle() = BorderStyle.Solid
		emp_type_cdrbl.BorderWidth = Unit.Pixel(2)


		If Not IsPostBack Then
			'lblusersubmit
			'userSubmittingLabel
			'SubmittingOnBehalfOfNameLabel
			pnlProvider.Visible = False
			pnlTCl.Visible = False
			pnlTokenAddress.Visible = False



            userSubmittingLabel.Text = UserInfo.UserName
			SubmittingOnBehalfOfNameLabel.Text = UserInfo.UserName
			requestor_client_numLabel.Text = UserInfo.ClientNum
			submitter_client_numLabel.Text = UserInfo.ClientNum
			userDepartmentCD.Text = UserInfo.DepartmentCd


			Dim ddlBinder As New DropDownListBinder

			Dim cblBinder As New CheckBoxListBinder(cblApplications, "SelApplicationCodes", Session("EmployeeConn"))
			cblBinder.AddDDLCriteria("@AppSystem", "base", SqlDbType.NVarChar)
			cblBinder.AddDDLCriteria("@ApplicationGroup", "clinical", SqlDbType.NVarChar)
			cblBinder.BindData("ApplicationNum", "ApplicationDesc")


			Dim cblFacilitesBinder As New CheckBoxListBinder(cblFacilities, "SelFacilityEntity", Session("EmployeeConn"))
			cblFacilitesBinder.AddDDLCriteria("@EntityFacility", "Facility", SqlDbType.NVarChar)
			cblFacilitesBinder.BindData("Code", "EntityFacilityDesc")

			Dim cblEntityBinder As New CheckBoxListBinder(cblEntities, "SelFacilityEntity", Session("EmployeeConn"))
			cblEntityBinder.AddDDLCriteria("@EntityFacility", "Entity", SqlDbType.NVarChar)
			cblEntityBinder.BindData("Code", "EntityFacilityDesc")

			rblAffiliateBinder.ToolTip = False
			rblAffiliateBinder.BindData("RoleNum", "AffiliationDisplay") '"AffiliationDescLower",


			ddlBinder.BindData(Rolesddl, "SelEmployeeRolesV2", "rolenum", "RoleDesc", Session("EmployeeConn"))

			ddlBinder.BindData(Specialtyddl, "SelSpecialtyCodesV2", "specialty", "specialty", Session("EmployeeConn"))
			ddlBinder.BindData(facility_cdddl, "SelFacilityCodes", "facility_cd", "facility_short_name", Session("EmployeeConn"))
			ddlBinder.BindData(suffixddl, "SelSuffixCodes", "Suffix", "Suffix", Session("EmployeeConn"))
			ddlBinder.BindData(entity_cdDDL, "selEntityCodesV4", "entity_cd", "hosp_desc", Session("EmployeeConn"))


            'Dim ddlCPBinder As New DropDownListBinder
            'ddlCPBinder.BindData(CellPhoneddl, "SelCellPhoneProviders", "ProviderExtension", "ProviderName", Session("EmployeeConn"))

            Dim ddlInvisionBinder As New DropDownListBinder(group_nameddl, "SelPhysGroupsV5", Session("EmployeeConn"))
			ddlInvisionBinder.AddDDLCriteria("@GroupTypeCd", "invision", SqlDbType.NVarChar)

			ddlInvisionBinder.BindData("Group_num", "group_name")

            'Dim ddlNonPerferBinder As New DropDownListBinder(Nonperferdddl, "SelPhysGroupsV5", Session("EmployeeConn"))
            'ddlNonPerferBinder.AddDDLCriteria("@GroupTypeCd", "NonPerGrp", SqlDbType.NVarChar)
            '            ddlNonPerferBinder.BindData("Group_num", "group_name")

            Dim ddGnumberBinder As New DropDownListBinder(LocationOfCareIDddl, "SelPhysGroupsV5", Session("EmployeeConn"))
            ddGnumberBinder.AddDDLCriteria("@GroupTypeCd", "CHMG", SqlDbType.NVarChar)

            ddGnumberBinder.BindData("Group_num", "group_name")

            'Dim ddCommLocationOfCare As New DropDownListBinder(CommLocationOfCareIDddl, "SelPhysGroupsV5", Session("EmployeeConn"))
            'ddCommLocationOfCare.AddDDLCriteria("@GroupTypeCd", "CommNum", SqlDbType.NVarChar)
            '         ddCommLocationOfCare.BindData("Group_num", "group_name")

            '         Dim ddCoverageGroupddl As New DropDownListBinder(CoverageGroupddl, "SelPhysGroupsV5", Session("EmployeeConn"))
            'ddCoverageGroupddl.AddDDLCriteria("@GroupTypeCd", "coverage", SqlDbType.NVarChar)
            '         ddCoverageGroupddl.BindData("Group_num", "Group_name")


            Dim ddEMRBinder As New DropDownListBinder(PhysOfficeddl, "SelAllPhysGroups", Session("EmployeeConn"))
			'ddEMRBinder.AddDDLCriteria("@GroupTypeCd", "GNum", SqlDbType.NVarChar)
			'SelPhysGroupsV5
			ddEMRBinder.BindData("Group_num", "group_name")

			facility_cdddl.SelectedIndex = 0

			department_cdddl.Items.Add("Select Entity")
			building_cdddl.Items.Add("Select Facility")

			pnlDemo.Enabled = False


            If ReactivateClientNum > 0 Then
                lblSearched.Text = "TRUE"
                SearchPopup.Hide()

                If PMHRequestNumTextBox.Text = "" Then
                    sClientSearch = "yes"

                Else
                    sClientSearch = "no"

                End If

                Dim c3Controller As New Client3Controller(ReactivateClientNum, sClientSearch, Session("EmployeeConn"))

                Select Case c3Controller.RoleNum
                    Case "782", "1197", "1194"

                        If Requesttype = "" Then '" addpmhact"	show short list
                            rblAffiliateBinder.AddDDLCriteria("@type", "contract", SqlDbType.NVarChar)
                        Else
                            rblAffiliateBinder.AddDDLCriteria("@type", "phys", SqlDbType.NVarChar)
                        End If


                    Case Else
                        ' if a Doctor master number exist
                        If c3Controller.DoctorMasterNum.ToString <> "" Then
                            doctor_master_numTextBox.Text = c3Controller.DoctorMasterNum.ToString
                            doctor_master_numTextBox.Visible = True
                            If Session("SecurityLevelNum") < 90 Then
                                doctor_master_numTextBox.Enabled = False
                            End If

                            'rblAffiliateBinder.AddDDLCriteria("@type", "phys", SqlDbType.NVarChar)
                            'grDocmster
                        Else
                            If Requesttype = "" Then '" addpmhact"	show short list
                                rblAffiliateBinder.AddDDLCriteria("@type", "contract", SqlDbType.NVarChar)
                            Else
                                rblAffiliateBinder.AddDDLCriteria("@type", "", SqlDbType.NVarChar)
                            End If

                        End If

                End Select

                doctor_master_numTextBox.Text = c3Controller.DoctorMasterNum.ToString
                npitextbox.Text = c3Controller.Npi
                LicensesNotextbox.Text = c3Controller.LicensesNo
                taxonomytextbox.Text = c3Controller.Taxonomy
                AuthorizationEmpNumTextBox.Text = c3Controller.SiemensEmpNum

                address_1textbox.Text = c3Controller.Address1
                address_2textbox.Text = c3Controller.Address2
                citytextbox.Text = c3Controller.City
                statetextbox.Text = c3Controller.State
                ziptextbox.Text = c3Controller.Zip
                phoneTextBox.Text = c3Controller.Phone
                faxtextbox.Text = c3Controller.Fax

                Title2TextBox.Text = c3Controller.Title
                SpecialtLabel.Text = c3Controller.Specialty

                mainRole = c3Controller.RoleNum
                masterRole.Text = c3Controller.RoleNum
                RoleNumLabel.Text = c3Controller.RoleNum
                emp_type_cdrbl.SelectedValue = c3Controller.RoleNum
                PositionRoleNumTextBox.Text = c3Controller.PositionRoleNum

                'Position_NumTextBox.Text = c3Controller.PositionNum

                'emptypecdrbl.SelectedValue = ckhsEmployee.EmpTypeCd
                emp_type_cdLabel.Text = c3Controller.EmpTypeCd

                first_nameTextBox.Text = c3Controller.FirstName
                last_nameTextBox.Text = c3Controller.LastName
                miTextBox.Text = c3Controller.MI
                lblUpdateClientFullName.Text = c3Controller.FullName

                UserPositionDescTextBox.Text = c3Controller.UserPositionDesc
                DepartmentNameTextBox.Text = c3Controller.DepartmentName
                RequesttypeTextBox.Text = c3Controller.AccountRequestType
                CernerPositionDescTextBox.Text = c3Controller.CernerPositionDesc

                If RequesttypeTextBox.Text.ToLower = "reactivat" Then
                    request_descTextBox.Text = request_descTextBox.Text & " Reactivate Client"
                    Reactivatetext.Text = " This Account needs to be Re-Activated"
                    Reactivatetext.Visible = True

                    Reactivatetext.BackColor() = Color.Red
                End If

                LoadEmployeeType()

                'emp_type_cdrbl_SelectedIndexChanged(c3Controller.EmpTypeCd, e)


            End If


        End If

		If RequesttypeTextBox.Text <> "" Then
			' PMH request type get userposition and Department

			pnluserPosition.Visible = True

			first_nameTextBox.Enabled = False
			last_nameTextBox.Enabled = False

		End If


		'  unlock 2015
		'Select Case userDepartmentCD.Text
		'    Case "832600", "832700", "823100"
		'        If Session("SecurityLevelNum") < 90 And requestor_client_numLabel.Text = submitter_client_numLabel.Text Then
		'            btnSelectOnBehalfOf.Visible = True
		'            lblValidation.Text = "Must Change Requestor"
		'            btnSelectOnBehalfOf.BackColor() = Color.Yellow
		'            btnSelectOnBehalfOf.BorderColor() = Color.Black
		'            btnSelectOnBehalfOf.ForeColor() = Color.Black
		'            btnSelectOnBehalfOf.BorderStyle() = BorderStyle.Solid
		'            btnSelectOnBehalfOf.BorderWidth = Unit.Pixel(2)
		'        ElseIf Session("SecurityLevelNum") < 90 And requestor_client_numLabel.Text <> submitter_client_numLabel.Text Then
		'            lblValidation.Text = ""
		'            btnSelectOnBehalfOf.BackColor() = Color.FromKnownColor("336666")
		'            btnSelectOnBehalfOf.ForeColor() = Color.White
		'            btnSelectOnBehalfOf.BorderStyle() = BorderStyle.Groove
		'            btnSelectOnBehalfOf.BorderWidth = Unit.Pixel(1)

		'        End If


		'End Select
		If Session("LoginID").ToString.ToLower = "geip00" Or Session("LoginID").ToString.ToLower = "melej" Or Session("LoginID").ToString.ToLower = "atkm00" Then
			AuthorizationEmpNumTextBox.Enabled = True

		End If
        If Session("SecurityLevelNum") = 95 Or Session("LoginID").ToString.ToLower = "burgina" Or Session("LoginID").ToString.ToLower = "melej" Then
            btnCloseRequest.Visible = True

        End If


    End Sub
	Protected Sub facility_cdddl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles facility_cdddl.SelectedIndexChanged

		facility_cdTextBox.Text = facility_cdddl.SelectedValue
		Dim ddlBinder As DropDownListBinder


		ddlBinder = New DropDownListBinder(building_cdddl, "sel_building_by_facility", Session("EmployeeConn"))
		ddlBinder.AddDDLCriteria("@facility_cd", facility_cdddl.SelectedValue, SqlDbType.NVarChar)
		ddlBinder.BindData("building_cd", "building_name")

		Dim ddlFloorBinder As DropDownListBinder

		ddlFloorBinder = New DropDownListBinder(Floorddl, "SelFloorsByBuilding", Session("EmployeeConn"))
		ddlFloorBinder.AddDDLCriteria("@facility_cd", facility_cdddl.SelectedValue, SqlDbType.NVarChar)
		ddlFloorBinder.AddDDLCriteria("@building_cd", building_cdddl.SelectedValue, SqlDbType.NVarChar)

		ddlFloorBinder.BindData("floor_no", "floor_no")

		ChangColorNoCheck()

		'ChangColor()
	End Sub
	Protected Sub Floorddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles Floorddl.SelectedIndexChanged
		FloorTextBox.Text = Floorddl.SelectedValue
		ChangColorNoCheck()

		'ChangColor()
	End Sub

	Protected Sub building_cdddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles building_cdddl.SelectedIndexChanged
		building_cdTextBox.Text = building_cdddl.SelectedValue
		'buildingnameLabel.Text = building_cdddl.SelectedItem.Text

		Dim ddlFloorBinder As DropDownListBinder

		ddlFloorBinder = New DropDownListBinder(Floorddl, "SelFloorsByBuilding", Session("EmployeeConn"))
		ddlFloorBinder.AddDDLCriteria("@facility_cd", facility_cdddl.SelectedValue, SqlDbType.NVarChar)
		ddlFloorBinder.AddDDLCriteria("@building_cd", building_cdddl.SelectedValue, SqlDbType.NVarChar)

		ddlFloorBinder.BindData("floor_no", "floor_no")

		CheckforRadiology()

		ChangColorNoCheck()


		'ChangColor()
	End Sub


	Protected Sub Titleddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles Titleddl.SelectedIndexChanged
		TitleLabel.Text = Titleddl.SelectedValue


		'TCLddl.SelectedIndex = -1
		TCLTextBox.Text = ""
		UserTypeCdTextBox.Text = ""
		UserTypeDescTextBox.Text = ""
		user_position_descTextBox.Text = emp_type_cdrbl.SelectedValue

		getTCLforRole()

        ' reload Cernerdescription with new title

        'TCLddl.DataSource = dtTCLUserTypes

        'TCLddl.DataTextField = "UserTypeDesc"
        'TCLddl.DataValueField = "TCL"
        'TCLddl.DataBind()

        'TCLddl.Items.Insert(0, "Select")

        'ChangColorNoCheck()

        ChangColor() ' yes change app and color


	End Sub

	Protected Sub gvTCL_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvTCL.RowCommand
		If e.CommandName = "Select" Then


			TCLTextBox.Text = gvTCL.DataKeys(Convert.ToInt32(e.CommandArgument))("TCL").ToString()
			UserTypeCdTextBox.Text = gvTCL.DataKeys(Convert.ToInt32(e.CommandArgument))("UserTypeCd").ToString()
			UserTypeDescTextBox.Text = gvTCL.DataKeys(Convert.ToInt32(e.CommandArgument))("UserTypeDesc").ToString()

			gvTCL.Visible = False


			pnlTCl.Visible = True
			TCLTextBox.Visible = True
			UserTypeDescTextBox.Visible = True
			UserTypeCdTextBox.Visible = True

			btnChangeTCL.Visible = True
			btnChangeTCL.Text = "Show All User Types"

            'ModelAfterTextBox.Text = ""

            'ModelAfterTextBox.Text = "Refer to TCL " & TCLTextBox.Text & " and " & UserTypeCdTextBox.Text

            CheckforRadiology()

			ChangColor()


		End If

	End Sub

	Protected Sub Specialtyddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles Specialtyddl.SelectedIndexChanged
		SpecialtLabel.Text = Specialtyddl.SelectedValue
		ChangColor()

	End Sub

	Protected Sub cblApplications_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cblApplications.SelectedIndexChanged

		'If cblApplications.SelectedValue.ToString.ToLower = "mak" Then
		'    request_descTextBox.Text = "Use This Mak ID ="
		'    request_descTextBox.BackColor() = Color.Yellow
		'    request_descTextBox.BorderColor() = Color.Black
		'    request_descTextBox.BorderStyle() = BorderStyle.Solid
		'    request_descTextBox.BorderWidth = Unit.Pixel(2)
		'End If
		CheckforToken()

		CheckECare()

		'CheckforCPMResident()
		'pnlTokenAddress.Visible = True

		'Dim BusLog As New AccountBusinessLogic
		''   BusLog.CheckAccountDependancy(cblApplications)
		'If BusLog.eCare Then
		'    pnlTCl.Visible = True
		'    'Dim reqField As New RequiredField(TCLTextBox, "TCL for eCare")
		'Else
		'    ' pnlTCl.Visible = False
		'    pageCtrl.RemoveCssClass(pnlTCl, "textInputRequired")
		'End If
		ChangColorNoCheck()

		'ChangColor()

	End Sub

	Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

		' see if they change the requestor for IS
		lblValidation.Text = ""
		'unlock 2015
		'Select Case userDepartmentCD.Text
		'    Case "832600", "832700", "823100"
		'        If Session("SecurityLevelNum") < 90 And requestor_client_numLabel.Text = submitter_client_numLabel.Text Then
		'            btnSelectOnBehalfOf.Visible = True
		'            lblValidation.Text = "Must Change Requestor"
		'            lblValidation.Focus()
		'            Return
		'        End If


		'End Select


		CheckRequiredFields()


		If Rolesddl.SelectedIndex = -1 Then
			lblValidation.Text = "Must select a Position Description"
			lblValidation.ForeColor = Color.Red

			lblValidation.Focus()
			Rolesddl.BackColor() = Color.Red
			Exit Sub

		End If

		If emp_type_cdrbl.SelectedIndex = -1 Then

			lblValidation.Text = "Must select an Affiliation"
			lblValidation.ForeColor = Color.Red

			lblValidation.Focus()
			emp_type_cdrbl.BackColor() = Color.Red
			Exit Sub


		End If


		If RoleNumLabel.Text = "1200" Then
			lblValidation.Text = "Must Specify a Nursing Type"
			lblValidation.ForeColor = Color.Red
			lblValidation.Focus()

			Titleddl.BackColor() = Color.Red
			Exit Sub
		Else

			Titleddl.BackColor() = Color.Yellow
			Titleddl.BorderColor() = Color.Black
			Titleddl.BorderStyle() = BorderStyle.Solid
			Titleddl.BorderWidth = Unit.Pixel(2)


		End If

		If RoleNumLabel.Text = "1189" Then
			lblValidation.Text = "Must Specify a Student Type"
			lblValidation.ForeColor = Color.Red
			lblValidation.Focus()

			Titleddl.BackColor() = Color.Red
			Exit Sub
		Else

			Titleddl.BackColor() = Color.Yellow
			Titleddl.BorderColor() = Color.Black
			Titleddl.BorderStyle() = BorderStyle.Solid
			Titleddl.BorderWidth = Unit.Pixel(2)

		End If

		'user_position_descTextBox
		Select Case RoleNumLabel.Text
			Case "1567"
				If PhysOfficeddl.SelectedIndex = -1 Then
					lblValidation.Text = "Must select Phys Office location"
					lblValidation.ForeColor = Color.Red
					lblValidation.Focus()
					PhysOfficeddl.ForeColor = Color.Red
					Exit Sub

				End If

				'If CPMModelTextBox.Text = "" Then

				'    lblValidation.Text = "Must select CPM Model After"
				'    lblValidation.ForeColor = Color.Red
				'    lblValidation.Focus()
				'    CPMEmployeesddl.ForeColor = Color.Red

				'    Return


				'End If
			Case Else

		End Select
		cblFacilities.ForeColor = Color.Black
		cblEntities.ForeColor = Color.Black

		facility_cdddl.ForeColor = Color.Black
		building_cdddl.ForeColor = Color.Black



		Select Case emp_type_cdrbl.SelectedValue
			Case "782", "1197"

				'Titleddl.ForeColor = Color.Black
				Specialtyddl.ForeColor = Color.Black

                'CCMCConsultsrbl.ForeColor = Color.Black
                'DCMHConsultsrbl.ForeColor = Color.Black
                'TaylorConsultsrbl.ForeColor = Color.Black
                'SpringfieldConsultsrbl.ForeColor = Color.Black

                'Writeordersrbl.ForeColor = Color.Black
                'WriteordersDCMHrbl.ForeColor = Color.Black

                CCMCadmitRightsrbl.ForeColor = Color.Black
                'DCMHadmitRightsrbl.ForeColor = Color.Black

                If TitleLabel.Text = "" Then
					lblValidation.Text = "Must supply Title"
					lblValidation.ForeColor = Color.Red
					lblValidation.Focus()

					Titleddl.BackColor() = Color.Red
					Exit Sub

				Else

					Titleddl.BackColor() = Color.Yellow
					Titleddl.BorderColor() = Color.Black
					Titleddl.BorderStyle() = BorderStyle.Solid
					Titleddl.BorderWidth = Unit.Pixel(2)

				End If

                'DEA
                'If DEAIDTextBox.Text = "" Then

                '    lblValidation.Text = "Must supply DEA Info "
                '    lblValidation.ForeColor = Color.Red
                '    lblValidation.Focus()

                '    DEAIDTextBox.BackColor() = Color.Red
                '    Exit Sub

                'Else

                '    DEAIDTextBox.BackColor() = Color.Yellow
                '    DEAIDTextBox.BorderColor() = Color.Black
                '    DEAIDTextBox.BorderStyle() = BorderStyle.Solid
                '    DEAIDTextBox.BorderWidth = Unit.Pixel(2)

                'End If

                If npitextbox.Text = "" Then
                    lblValidation.Text = "Must supply NPI Info "
                    lblValidation.ForeColor = Color.Red
                    lblValidation.Focus()

                    npitextbox.BackColor() = Color.Red
                    Exit Sub

                Else

                    npitextbox.BackColor() = Color.Yellow
                    npitextbox.BorderColor() = Color.Black
                    npitextbox.BorderStyle() = BorderStyle.Solid
                    npitextbox.BorderWidth = Unit.Pixel(2)
                End If

                If LicensesNotextbox.Text = "" Then

                    lblValidation.Text = "Must supply Licenses Info "
                    lblValidation.ForeColor = Color.Red
                    lblValidation.Focus()

                    LicensesNotextbox.BackColor() = Color.Red
                    Exit Sub

                Else

                    LicensesNotextbox.BackColor() = Color.Yellow
                    LicensesNotextbox.BorderColor() = Color.Black
                    LicensesNotextbox.BorderStyle() = BorderStyle.Solid
                    LicensesNotextbox.BorderWidth = Unit.Pixel(2)
                End If


                'Specialtyddl
                If SpecialtLabel.Text = "" Then
					lblValidation.Text = "Must supply Specialty"
					lblValidation.ForeColor = Color.Red
					lblValidation.Focus()

					Specialtyddl.BackColor() = Color.Red
					Exit Sub
				Else

					Specialtyddl.BackColor() = Color.Yellow
					Specialtyddl.BorderColor() = Color.Black
					Specialtyddl.BorderStyle() = BorderStyle.Solid
					Specialtyddl.BorderWidth = Unit.Pixel(2)

				End If

                If ecareappSelectedlbl.Text = "43" Then

                    If TCLTextBox.Text = "" Then
                        lblValidation.Text = "Must select User Type and TCL "
                        lblValidation.ForeColor = Color.Red
                        lblValidation.Focus()

                        'UserTypeCdTextBox.BackColor() = Color.Red
                        Exit Sub


                    End If

                End If



                If CCMCadmitRightsrbl.SelectedIndex = -1 Then

					lblValidation.Text = "Must select Admitting Rights for C.T.S."
					lblValidation.ForeColor = Color.Red
					lblValidation.Focus()
					CCMCadmitRightsrbl.BackColor() = Color.Red

					Return
				Else

					CCMCadmitRightsrbl.BackColor() = Color.Yellow
					CCMCadmitRightsrbl.BorderColor() = Color.Black
					CCMCadmitRightsrbl.BorderStyle() = BorderStyle.Solid
					CCMCadmitRightsrbl.BorderWidth = Unit.Pixel(2)


				End If

                'If DCMHadmitRightsrbl.SelectedIndex = -1 Then

                '	lblValidation.Text = "Must select Admitting Rights for DCMH"

                '	lblValidation.ForeColor = Color.Red
                '	lblValidation.Focus()
                '	DCMHadmitRightsrbl.BackColor() = Color.Red

                '	Return
                'Else

                '	DCMHadmitRightsrbl.BackColor() = Color.Yellow
                '	DCMHadmitRightsrbl.BorderColor() = Color.Black
                '	DCMHadmitRightsrbl.BorderStyle() = BorderStyle.Solid
                '	DCMHadmitRightsrbl.BorderWidth = Unit.Pixel(2)

                'End If

                If hanrbl.SelectedIndex = -1 Then

					lblValidation.Text = "Must select CKHN-HAN-CPM"

					lblValidation.ForeColor = Color.Red
					lblValidation.Focus()
					hanrbl.BackColor() = Color.Red

					Return
				Else

					hanrbl.BackColor() = Color.Yellow
					hanrbl.BorderColor() = Color.Black
					hanrbl.BorderStyle() = BorderStyle.Solid
					hanrbl.BorderWidth = Unit.Pixel(2)

				End If


                'If CCMCConsultsrbl.SelectedIndex = -1 Then

                '    lblValidation.Text = "Must select CCMC Consults"
                '    lblValidation.ForeColor = Color.Red
                '    lblValidation.Focus()
                '    CCMCConsultsrbl.ForeColor = Color.Red

                '    Return

                'End If


                'If DCMHConsultsrbl.SelectedIndex = -1 Then

                '    lblValidation.Text = "Must select DCMH Consults"
                '    lblValidation.ForeColor = Color.Red
                '    lblValidation.Focus()
                '    DCMHConsultsrbl.ForeColor = Color.Red

                '    Return

                'End If

                'If TaylorConsultsrbl.SelectedIndex = -1 Then

                '    lblValidation.Text = "Must select Taylor Consults"
                '    lblValidation.ForeColor = Color.Red
                '    lblValidation.Focus()
                '    TaylorConsultsrbl.ForeColor = Color.Red

                '    Return

                'End If

                'If SpringfieldConsultsrbl.SelectedIndex = -1 Then

                '    lblValidation.Text = "Must select Springfield Consults"
                '    lblValidation.ForeColor = Color.Red
                '    lblValidation.Focus()
                '    SpringfieldConsultsrbl.ForeColor = Color.Red

                '    Return

                'End If


                'If Writeordersrbl.SelectedIndex = -1 Then

                '	lblValidation.Text = "Must select Write Orders for C.T.S."
                '	lblValidation.ForeColor = Color.Red
                '	lblValidation.Focus()
                '	Writeordersrbl.BackColor() = Color.Red

                '	Return
                'Else

                '	Writeordersrbl.BackColor() = Color.Yellow
                '	Writeordersrbl.BorderColor() = Color.Black
                '	Writeordersrbl.BorderStyle() = BorderStyle.Solid
                '	Writeordersrbl.BorderWidth = Unit.Pixel(2)

                'End If


                'If WriteordersDCMHrbl.SelectedIndex = -1 Then

                '	lblValidation.Text = "Must select Write Orders for DCMH"
                '	lblValidation.ForeColor = Color.Red
                '	lblValidation.Focus()
                '	WriteordersDCMHrbl.BackColor() = Color.Red

                '	Return
                'Else

                '	WriteordersDCMHrbl.BackColor() = Color.Yellow
                '	WriteordersDCMHrbl.BorderColor() = Color.Black
                '	WriteordersDCMHrbl.BorderStyle() = BorderStyle.Solid
                '	WriteordersDCMHrbl.BorderWidth = Unit.Pixel(2)


                'End If

                If hanrbl.SelectedValue.ToLower = "yes" Then

					If LocationOfCareIDLabel.Text = "" Then
                        lblValidation.Text = "Must select CHMG Group"
                        lblValidation.ForeColor = Color.Red
						lblValidation.Focus()
						LocationOfCareIDddl.BackColor() = Color.Red

						Return
					End If


					'If IsNumeric(npitextbox.Text) Then

					'Else


					'    lblValidation.Text = "Must Enter Numbers Only for NPI"

					'    lblValidation.ForeColor = Color.Red
					'    lblValidation.Focus()
					'    npitextbox.BackColor() = Color.Red

					'    Return

					'End If

					'If npitextbox.Text = "" Then

					'    lblValidation.Text = "Must Enter NPI"
					'    lblValidation.ForeColor = Color.Red
					'    lblValidation.Focus()
					'    npitextbox.BackColor() = Color.Red

					'    Return

					'End If


					'If LicensesNotextbox.Text = "" Then
					'    lblValidation.Text = "Must Enter Licenses No"

					'    lblValidation.ForeColor = Color.Red
					'    lblValidation.Focus()
					'    LicensesNotextbox.BackColor() = Color.Red

					'    Return


					'End If

					'If taxonomytextbox.Text = "" Then
					'    lblValidation.Text = "Must Enter Taxonomy No"

					'    lblValidation.ForeColor = Color.Red
					'    lblValidation.Focus()
					'    taxonomytextbox.BackColor() = Color.Red

					'    Return

					'End If

					'If CPMModelTextBox.Text = "" Then

					'    lblValidation.Text = "Must select CPM Model After"
					'    lblValidation.ForeColor = Color.Red
					'    lblValidation.Focus()
					'    CPMEmployeesddl.ForeColor = Color.Red

					'    Return


					'End If

					'If Schedulablerbl.SelectedIndex = -1 Then

					'    lblValidation.Text = "Must select Schedulable Item"
					'    lblValidation.ForeColor = Color.Red
					'    lblValidation.Focus()
					'    Schedulablerbl.ForeColor = Color.Red

					'    Return

					'End If
					'If Televoxrbl.SelectedIndex = -1 Then

					'    lblValidation.Text = "Must select Schedulable Item"
					'    lblValidation.ForeColor = Color.Red
					'    lblValidation.Focus()
					'    Televoxrbl.ForeColor = Color.Red

					'    Return

					'End If


				End If


				' Residents
			Case "1194"

				Titleddl.ForeColor = Color.Black
				Specialtyddl.ForeColor = Color.Black

                'CCMCConsultsrbl.ForeColor = Color.Black
                'DCMHConsultsrbl.ForeColor = Color.Black
                'TaylorConsultsrbl.ForeColor = Color.Black
                'SpringfieldConsultsrbl.ForeColor = Color.Black

                'Writeordersrbl.ForeColor = Color.Black
                '            WriteordersDCMHrbl.ForeColor = Color.Black

                CCMCadmitRightsrbl.ForeColor = Color.Black
                'DCMHadmitRightsrbl.ForeColor = Color.Black

                If TitleLabel.Text = "" Then
					lblValidation.Text = "Must supply Title"
					lblValidation.ForeColor = Color.Red
					lblValidation.Focus()

					Titleddl.BackColor() = Color.Red
					Exit Sub
				Else

					Titleddl.BackColor() = Color.Yellow
					Titleddl.BorderColor() = Color.Black
					Titleddl.BorderStyle() = BorderStyle.Solid
					Titleddl.BorderWidth = Unit.Pixel(2)

				End If

				'Specialtyddl
				If SpecialtLabel.Text = "" Then
					lblValidation.Text = "Must supply Specialty"
					lblValidation.ForeColor = Color.Red
					lblValidation.Focus()

					Specialtyddl.BackColor() = Color.Red
					Exit Sub
				Else

					Specialtyddl.BackColor() = Color.Yellow
					Specialtyddl.BorderColor() = Color.Black
					Specialtyddl.BorderStyle() = BorderStyle.Solid
					Specialtyddl.BorderWidth = Unit.Pixel(2)

				End If


                'If CCMCadmitRightsrbl.SelectedIndex = -1 Then

                '    lblValidation.Text = "Must select Admitting Rights for C.T.S."
                '    lblValidation.ForeColor = Color.Red
                '    lblValidation.Focus()
                '    CCMCadmitRightsrbl.BackColor() = Color.Red

                '    Return
                'End If

                'If DCMHadmitRightsrbl.SelectedIndex = -1 Then

                '    lblValidation.Text = "Must select Admitting Rights for DCMH"

                '    lblValidation.ForeColor = Color.Red
                '    lblValidation.Focus()
                '    DCMHadmitRightsrbl.BackColor() = Color.Red

                '    Return

                'End If

                'If CCMCConsultsrbl.SelectedIndex = -1 Then

                '    lblValidation.Text = "Must select CCMC Consults"
                '    lblValidation.ForeColor = Color.Red
                '    lblValidation.Focus()
                '    CCMCConsultsrbl.ForeColor = Color.Red

                '    Return

                'End If


                'If DCMHConsultsrbl.SelectedIndex = -1 Then

                '    lblValidation.Text = "Must select DCMH Consults"
                '    lblValidation.ForeColor = Color.Red
                '    lblValidation.Focus()
                '    DCMHConsultsrbl.ForeColor = Color.Red

                '    Return

                'End If

                'If TaylorConsultsrbl.SelectedIndex = -1 Then

                '    lblValidation.Text = "Must select Taylor Consults"
                '    lblValidation.ForeColor = Color.Red
                '    lblValidation.Focus()
                '    TaylorConsultsrbl.ForeColor = Color.Red

                '    Return

                'End If

                'If SpringfieldConsultsrbl.SelectedIndex = -1 Then

                '    lblValidation.Text = "Must select Springfield Consults"
                '    lblValidation.ForeColor = Color.Red
                '    lblValidation.Focus()
                '    SpringfieldConsultsrbl.ForeColor = Color.Red

                '    Return

                'End If


                'If Writeordersrbl.SelectedIndex = -1 Then

                '	lblValidation.Text = "Must select Write Orders for C.T.S."
                '	lblValidation.ForeColor = Color.Red
                '	lblValidation.Focus()
                '	Writeordersrbl.BackColor() = Color.Red

                '	Return
                'Else

                '	Writeordersrbl.BackColor() = Color.Yellow
                '	Writeordersrbl.BorderColor() = Color.Black
                '	Writeordersrbl.BorderStyle() = BorderStyle.Solid
                '	Writeordersrbl.BorderWidth = Unit.Pixel(2)
                'End If


                'If WriteordersDCMHrbl.SelectedIndex = -1 Then

                '	lblValidation.Text = "Must select Write Orders for DCMH"
                '	lblValidation.ForeColor = Color.Red
                '	lblValidation.Focus()
                '	WriteordersDCMHrbl.BackColor() = Color.Red

                '	Return

                'Else

                '	WriteordersDCMHrbl.BackColor() = Color.Yellow
                '	WriteordersDCMHrbl.BorderColor() = Color.Black
                '	WriteordersDCMHrbl.BorderStyle() = BorderStyle.Solid
                '	WriteordersDCMHrbl.BorderWidth = Unit.Pixel(2)
                'End If

                If Rolesddl.SelectedIndex = 0 Then

					lblValidation.Text = "Must select Resident Position"
					lblValidation.ForeColor = Color.Red
					lblValidation.Focus()
					Rolesddl.BackColor() = Color.Red

					Return

				Else

					Rolesddl.BackColor() = Color.Yellow
					Rolesddl.BorderColor() = Color.Black
					Rolesddl.BorderStyle() = BorderStyle.Solid
					Rolesddl.BorderWidth = Unit.Pixel(2)

				End If

				If hanrbl.SelectedValue.ToLower = "yes" Then
					If LocationOfCareIDLabel.Text = "" Then
                        lblValidation.Text = "Must select CHMG Group"
                        lblValidation.ForeColor = Color.Red
						lblValidation.Focus()
						LocationOfCareIDddl.BackColor() = Color.Red
					Else

						LocationOfCareIDddl.BackColor() = Color.Yellow
						LocationOfCareIDddl.BorderColor() = Color.Black
						LocationOfCareIDddl.BorderStyle() = BorderStyle.Solid
						LocationOfCareIDddl.BorderWidth = Unit.Pixel(2)

					End If
				End If

				'Case "physician", "non staff clinician", "resident", "allied health"
				'    If TitleLabel.Text = "" Then
				'        lblValidation.Text = "Must supply Title"
				'        Titleddl.Focus()
				'        Exit Sub
				'    End If
				'    'Specialtyddl
				'    If SpecialtLabel.Text = "" Then
				'        lblValidation.Text = "Must supply Specialty"
				'        Specialtyddl.Focus()
				'        Exit Sub
				'    End If

			Case "1193", "1200"



				If facility_cdddl.SelectedIndex = 0 Then
					lblValidation.Text = "Must select an Location"
					lblValidation.ForeColor = Color.Red
					lblValidation.Focus()
					facility_cdddl.BackColor() = Color.Red
					Return
				Else

					facility_cdddl.BackColor() = Color.Yellow
					facility_cdddl.BorderColor() = Color.Black
					facility_cdddl.BorderStyle() = BorderStyle.Solid
					facility_cdddl.BorderWidth = Unit.Pixel(2)

				End If

				If building_cdddl.SelectedIndex = 0 Then
					lblValidation.Text = "Must select an Building"
					lblValidation.ForeColor = Color.Red
					lblValidation.Focus()

					building_cdddl.BackColor() = Color.Red
					Return

				Else

					building_cdddl.BackColor() = Color.Yellow
					building_cdddl.BorderColor() = Color.Black
					building_cdddl.BorderStyle() = BorderStyle.Solid
					building_cdddl.BorderWidth = Unit.Pixel(2)

				End If

			Case "1195", "1198"
				If Vendorddl.SelectedIndex = 0 Then

					lblValidation.Text = "Must select Vendor "
					lblValidation.ForeColor = Color.Red
					lblValidation.Focus()
					Vendorddl.BackColor() = Color.Red
					Return
				Else

					Vendorddl.BackColor() = Color.Yellow
					Vendorddl.BorderColor() = Color.Black
					Vendorddl.BorderStyle() = BorderStyle.Solid
					Vendorddl.BorderWidth = Unit.Pixel(2)


				End If

		End Select

		If Not checkFacilities() Then

			lblValidation.Text = "Must select a Hospital"
			lblValidation.ForeColor = Color.Red
			lblValidation.Focus()
			cblFacilities.BackColor() = Color.Red

			Return
		Else

			cblFacilities.BackColor() = Color.Yellow
			cblFacilities.BorderColor() = Color.Black
			cblFacilities.BorderStyle() = BorderStyle.Solid
			cblFacilities.BorderWidth = Unit.Pixel(2)

		End If

		If Not checkEntities() Then

			lblValidation.Text = "Must select an Entity"
			lblValidation.ForeColor = Color.Red
			lblValidation.Focus()
			cblEntities.BackColor() = Color.Red
			Return

		Else

			cblEntities.BackColor() = Color.Yellow
			cblEntities.BorderColor() = Color.Black
			cblEntities.BorderStyle() = BorderStyle.Solid
			cblEntities.BorderWidth = Unit.Pixel(2)

		End If


		If Not checkApplications() Then

			lblValidation.Text = "Must select Application to Access"
			Return

		End If

		For Each cbApp As ListItem In cblApplications.Items
			If cbApp.Selected Then

				Select Case cbApp.Value
                    Case 43
                        ecareappSelectedlbl.Text = cbApp.Value

                        If UserTypeCdTextBox.Text = "" Then
							lblValidation.Text = "Must Specify User Type and TCL"
							lblValidation.ForeColor = Color.Red
							lblValidation.Focus()

							'Titleddl.BackColor() = Color.Red
							Exit Sub


						End If
                    'Case 136
                    '	If cblo365.SelectedValue = "" Then
                    '		lblValidation.Text = "Must Specify o365 Location"
                    '		lblValidation.ForeColor = Color.Red
                    '		lblValidation.Focus()

                    '		'Titleddl.BackColor() = Color.Red
                    '		Exit Sub
                    '	Else

                    '		Checkfor365()

                    '	End If
                    Case 156
                        'load fields

                        'If Remoteaccrbl.SelectedValue.ToLower = "yes" Then

                        CheckCellPhone()
                            If blnReturn = True Then
                                Exit Sub
                            End If

                        'End If

                End Select
			End If

		Next


        'For Each cbFacility As ListItem In cblFacilities.Items
        '	If cbFacility.Selected Then
        '		If cbFacility.ToString.ToLower.Trim = "community" Then
        '			For Each cbApp As ListItem In cblApplications.Items
        '				If cbApp.Selected Then

        '					Select Case cbApp.ToString.ToLower.Trim
        '						Case "ebed", "ecase", "ecare", "pacs", "pharmacy", "picis (emergency dept.)", "edm", "pyxis"
        '							lblValidation.Text = "InValid Application for Community -> " & cbApp.ToString
        '							lblValidation.Focus()

        '							cblFacilities.BackColor() = Color.Red


        '							Return
        '					End Select
        '				End If

        '			Next

        '		End If


        '	End If
        'Next

        '' City State zip and phone Fax 
        'If citytextbox.Text <> "" Then

        '    If (Regex.IsMatch(citytextbox.Text, "^[\.\-A-Za-z]") = False) Then

        '        lblValidation.Text = "City name can not have numbers."
        '        lblValidation.ForeColor = Color.Red
        '        'citytextbox.Text = ""
        '        citytextbox.Focus()

        '        'lblValidation.Focus()
        '        citytextbox.BackColor() = Color.Red
        '        Exit Sub

        '    End If

        'End If

        If statetextbox.Text <> "" Then

			If (Regex.IsMatch(statetextbox.Text, "^[\.\-A-Za-z]") = False) Then
				lblValidation.Visible = True
				lblValidation.Text = "State can not have numbers."
				lblValidation.ForeColor = Color.Red
				'statetextbox.Text = ""

				statetextbox.Focus()
				statetextbox.BackColor() = Color.Red
				Exit Sub
			Else
				lblValidation.Visible = False
				statetextbox.BackColor() = Color.Empty

			End If
		End If

		If phoneTextBox.Text <> "" Then
			If Not IsPhoneNumberValid(phoneTextBox.Text) Then
				Dim isvalid = False
				lblValidation.Visible = True
				lblValidation.Text = "*Invalid Phone Number - Alph Chaaracter has been detected -  Format (999)-000.0000 "

				phoneTextBox.Focus()
				phoneTextBox.BackColor() = Color.Red

				Exit Sub
			Else
				lblValidation.Visible = False
				lblValidation.Text = ""

				phoneTextBox.BackColor() = Color.Empty

			End If

		End If
		If pagerTextBox.Text <> "" Then
			If Not IsPhoneNumberValid(pagerTextBox.Text) Then
				Dim isvalid = False
				lblValidation.Visible = True
				lblValidation.Text = "*Invalid Pager Number - Alph Chaaracter has been detected -  Format (999)-000.0000 "

				pagerTextBox.Focus()
				pagerTextBox.BackColor() = Color.Red

				Exit Sub
			Else
				lblValidation.Visible = False
				lblValidation.Text = ""
				pagerTextBox.BackColor() = Color.Empty

			End If

		End If


		If faxtextbox.Text <> "" Then
			If Not IsPhoneNumberValid(faxtextbox.Text) Then
				Dim isvalid = False
				lblValidation.Visible = True
				lblValidation.Text = "*Invalid Fax Number - Alph Chaaracter has been detected -  Format (999)-000.0000 "

				faxtextbox.Focus()
				faxtextbox.BackColor() = Color.Red

				Exit Sub
			Else
				lblValidation.Visible = False
				lblValidation.Text = ""
				faxtextbox.BackColor() = Color.Empty
			End If


		End If


        If ziptextbox.Text <> "" Then

			If (Regex.IsMatch(ziptextbox.Text, "^[0-9]") = False) Then

				lblValidation.Text = "Zip Code can only contain numbers."
				lblValidation.ForeColor = Color.Red
				lblValidation.Visible = True

				'ziptextbox.Text = ""

				ziptextbox.Focus()
				ziptextbox.BackColor() = Color.Red
				Exit Sub
			Else

				lblValidation.Visible = False
				ziptextbox.BackColor() = Color.Empty

			End If

		End If

		If bHasError = True Then
			lblValidation.Focus()
			Exit Sub
		Else
			lblValidation.Text = ""
		End If

		'if this is not a PMH request
		If Requesttype = "" Then '"addpmhact"

			'  now see if person already exists
			If TestednameLabel.Text <> "yes" Then
				CheckExists()
				lblCloseExists.Focus()
				'Dim ctrl As Control = DataCheckingAndValidation.SetChildControlsRecursiveToEnabledFalse(pnlDemo, False)
				cblEntities.Enabled = False
				cblFacilities.Enabled = False
				cblApplications.Enabled = False
				Rolesddl.Enabled = False
				Exit Sub
			End If

		End If

		Dim BusLog As New AccountBusinessLogic
		Dim thisdata As New CommandsSqlAndOleDb("SelApplicationCodes", Session("EmployeeConn"))
		Dim AccountCode As String = ""
		Dim Facility As String = ""


		thisdata.ExecNonQueryNoReturn()

		BusLog.Accounts = thisdata.GetSqlDataTable

        'FormSignOn.AddInsertParm("@submitter_client_num", submitter_client_numLabel.Text, SqlDbType.NVarChar, 10)

        'ReactivateClientNum
        'ReActivateRequestNum


        If ReactivateClientNum > 0 Then
			FormSignOn.AddInsertParm("@account_request_type", "rehire", SqlDbType.NVarChar, 9)
			'FormSignOn.AddInsertParm("@ReactivateClientNum", CInt(ReactivateClientNumTextBox.Text), SqlDbType.Int, 9)
			'FormSignOn.AddInsertParm("@ReHireRequestNum", CInt(RehireRequestNumTextBox.Text), SqlDbType.Int, 9)
		Else
			FormSignOn.AddInsertParm("@account_request_type", "addnew", SqlDbType.NVarChar, 9)

		End If

        'Dim gname As String = group_nameTextBox.Text

        'Dim gnum As String = group_numTextBox.Text

        If PMHRequestNumTextBox.Text <> "" Then
			PMHSignon.AddInsertParm("@account_request_num", CInt(PMHRequestNumTextBox.Text), SqlDbType.Int, 9)

			PMHSignon.UpdateFormReturnReqNum()
			AccountRequestNum = CInt(PMHRequestNumTextBox.Text)
			insPMHAddedItems(AccountRequestNum)

		Else
			FormSignOn.UpdateFormReturnReqNum()

			AccountRequestNum = FormSignOn.AccountRequestNum

		End If


		For Each cbItem As ListItem In cblApplications.Items
			If cbItem.Selected Then
				'change to case and  13 Pyxis
				If BusLog.isHospSpecific(cbItem.Value) Then
					'Hospital specific
					Select Case cbItem.Value

						Case Is = 156
							SubmitTokenAddress()

					End Select

					For Each cbFacility As ListItem In cblFacilities.Items

						If cbFacility.Selected Then
							Facility = cbFacility.Value
							AccountCode = BusLog.GetApplicationCode(cbItem.Value, cbFacility.Value)

							insAcctItems(AccountRequestNum, AccountCode, request_descTextBox.Text)

							Select Case cbItem.Value
								Case 13, 97, 98, 99, 100
									' insert Nursestation if selected
									InsertNursestations(AccountRequestNum, AccountCode, request_descTextBox.Text, Facility)
							End Select

						End If

					Next

				ElseIf BusLog.isEntSpecific(cbItem.Value) Then

					For Each cbEnt As ListItem In cblEntities.Items

						If cbEnt.Selected Then

							AccountCode = BusLog.GetApplicationCode(cbItem.Value, cbEnt.Value)

							insAcctItems(AccountRequestNum, AccountCode, request_descTextBox.Text)
						End If

					Next


				Else

					insAcctItems(AccountRequestNum, cbItem.Value, request_descTextBox.Text)
					' 365 portal is not entity and hosp specific
					Select Case cbItem.Value
						Case Is = 156
							SubmitTokenAddress()

						Case 136
							' portal not hospital specific
							InsertNursestations(AccountRequestNum, cbItem.Value, request_descTextBox.Text, Facility)
					End Select

				End If

			End If

		Next


		Select Case emp_type_cdrbl.SelectedValue

			Case "1195", "1198" ' if contractor/vendor

				If e_mailTextBox.Text <> "" Then '' if email is not null

					Dim thisdatae As New CommandsSqlAndOleDb("InsAccountItemsV3", Session("EmployeeConn"))
					thisdatae.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int, 20)
					thisdatae.AddSqlProcParameter("@ApplicationNum", 155, SqlDbType.Int, 8)

					thisdatae.AddSqlProcParameter("@account_item_desc", e_mailTextBox.Text, SqlDbType.NVarChar, 255)
					thisdatae.AddSqlProcParameter("@who_nt_login", Session("LoginID"), SqlDbType.NVarChar, 50)

					thisdatae.ExecNonQueryNoReturn()

				End If

				If RoleNumLabel.Text = "1724" Then
					'enter for ePayments

					Dim thisdatae As New CommandsSqlAndOleDb("InsAccountItemsV3", Session("EmployeeConn"))
					thisdatae.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int, 20)
					thisdatae.AddSqlProcParameter("@ApplicationNum", 102, SqlDbType.Int, 8)

					'thisdatae.AddSqlProcParameter("@account_item_desc", , SqlDbType.NVarChar, 255)
					thisdatae.AddSqlProcParameter("@who_nt_login", Session("LoginID"), SqlDbType.NVarChar, 50)

					thisdatae.ExecNonQueryNoReturn()

				End If
				'Case "1200"

				'If RoleNumLabel.Text = "1426" Then
				'    'Nurse instructor
				'    ' Needs eCare  - mak - Picis  a different account name with NI added at the end On validation 
				'    ' Needs a differnt AD acount name with NI
				'    ' Needs regular TCL for LPN Under NON NI account name TCL for Instructor needs to be added with NI
				'    Dim thisdatae As New CommandsSqlAndOleDb("InsNurseInstructorAccountItemsV3", Session("EmployeeConn"))
				'    thisdatae.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int, 20)
				'    thisdatae.AddSqlProcParameter("@ApplicationNum", AccountCode, SqlDbType.Int, 8)
				'    thisdatae.AddSqlProcParameter("@who_nt_login", Session("LoginID"), SqlDbType.NVarChar, 50)

				'    thisdatae.ExecNonQueryNoReturn()

				'End If

				'Case "782", "1197", "1194"
				'    If CPMModelTextBox.Text <> "" Then


				'        insAcctDriveModel(AccountRequestNum, "provmodel", CPMModelTextBox.Text, "", "", "")

				'    End If
				'    If Schedulablerbl.SelectedValue.ToLower <> "" Then

				'        insAcctDriveModel(AccountRequestNum, "schedulemodel", Schedulablerbl.SelectedValue.ToLower, "", "", "")

				'    End If
				'    If Televoxrbl.SelectedValue.ToLower <> "" Then
				'        insAcctDriveModel(AccountRequestNum, "televoxmodel", Televoxrbl.SelectedValue.ToLower, "", "", "")
				'    End If

				'Case "1193"

				'    If CPMModelTextBox.Text <> "" Then
				'        insAcctDriveModel(AccountRequestNum, "cpmmodel", CPMModelTextBox.Text, "", "", "")

				'    End If
		End Select


		' load vendor list 
		'make vendor text box invisiable

		'Select Case emp_type_cdrbl.SelectedValue
		'    'Case "physician/rovider", "non staff clinician", "resident"  "allied health"

		'    Case "782", "1197", "1194"
		'        If GNumberGroupNumLabel.Text <> "" Then

		'            ' Insert for CKHN-HAN with G# to interface Queue
		'            insAcctItems(AccountRequestNum, "91", LocationOfCareIDLabel.Text)

		'        End If

		'        If CoverageGroupNumLabel.Text <> "" Then

		'            ' Insert for Comm interface Queue
		'            insAcctItems(AccountRequestNum, "93", CoverageGroupNumLabel.Text)

		'        End If

		'        If CoverageGroupNumLabel.Text <> "" Or GNumberGroupNumLabel.Text <> "" Then
		'            insAcctItems(AccountRequestNum, "26", "Misys account Needed")
		'        End If


		'End Select

		If Session("environment") = "" Then
			Dim Envoirn As New Environment()
			thisenviro.Text = Envoirn.getEnvironment
			Session("environment") = Envoirn.getEnvironment
		End If


		SendProviderEmail(AccountRequestNum)

		'If Session("environment").ToString.ToLower <> "testing" Then

		'    SendProviderEmail(AccountRequestNum)

		'End If

		Response.Redirect("~/RequestSubmited.aspx?RequestNum=" & AccountRequestNum)



	End Sub

	Private Shared Function IsPhoneNumberValid(phoneNumber As String) As Boolean
		Dim result As String = ""
		Dim chars As Char() = phoneNumber.ToCharArray()
		For count = 0 To chars.GetLength(0) - 1
			Dim tempChar As Char = chars(count)
			If [Char].IsDigit(tempChar) Or "()+-., ".Contains(tempChar.ToString()) Then

				result += StripNonAlphaNumeric(tempChar)
			Else
				Return False
			End If

		Next
		Return True
		'Return result.Length = 10 'Length of US phone numbers is 10
	End Function

	Private Shared Function StripNonAlphaNumeric(value As String) As String
		Dim regex = New Regex("[^0-9a-zA-Z]", RegexOptions.None)
		Dim result As String = ""
		If regex.IsMatch(value) Then
			result = regex.Replace(value, "")
		Else
			result = value
		End If

		Return result
	End Function
	Private Shared Function IsZipValid(phoneNumber As String) As Boolean
		Dim result As String = ""
		Dim chars As Char() = phoneNumber.ToCharArray()
		For count = 0 To chars.GetLength(0) - 1
			Dim tempChar As Char = chars(count)
			If [Char].IsDigit(tempChar.ToString) Then

				result += StripNonAlphaNumeric(tempChar)
			Else
				Return False
			End If

		Next
		Return True
		'Return result.Length = 10 'Length of US phone numbers is 10
	End Function

	Protected Sub SubmitTokenAddress()

        Dim cmdTokenAddress As New CommandsSqlAndOleDb("InsUpdAccountTokenAddress", Session("EmployeeConn"))
        cmdTokenAddress.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int, 8)
		'TokenPhoneTextBox
		cmdTokenAddress.AddSqlProcParameter("@TokenPhone", TokenPhoneTextBox.Text, SqlDbType.NVarChar, 50)
        ' provider name 
        'cmdTokenAddress.AddSqlProcParameter("@TokenName", TokenNameTextBox.Text, SqlDbType.NVarChar, 75)

        cmdTokenAddress.ExecNonQueryNoReturn()

    End Sub

	Protected Sub InsertNursestations(ByVal requestNum As Integer, ByVal appNum As String, ByVal comments As String, ByVal Facility As String)



		Dim SubAppnum As String = ""
		Dim SubAppcd As String = ""
		Dim subAppDesc As String = ""
		Dim ValidCd As String = ""


        Select Case emp_type_cdrbl.SelectedValue
            'Case "physician/rovider", "non staff clinician", "resident"  "allied health"

            Case "782", "1197", "1194"

                ValidCd = "NeedsDocNum"


            Case Else
                'thisdata.AddSqlProcParameter("@validate", "NeedsValidation", SqlDbType.NVarChar, 20)

                If NeedsProviderrbl.SelectedValue = "Yes" Then
                    ValidCd = "NeedsDocNum"

                Else
                    ValidCd = "NeedsValidation"


                End If


        End Select

        Select Case appNum
            'Case "136"
            '	'o365 accounts
            '	'cblo365
            '	For Each cblo365Item As ListItem In cblo365.Items
            '		Dim sCurritem As String = cblo365Item.Value

            '		If cblo365Item.Selected = True Then

            '			If sCurritem = 324 Then
            '				Facility = 20
            '			Else
            '				Facility = 26

            '			End If

            '			Dim SubmitData As New CommandsSqlAndOleDb("InsAccountSubApplicationV3", Session("EmployeeConn"))
            '			SubmitData.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int, 9)
            '			SubmitData.AddSqlProcParameter("@ApplicationNum", appNum, SqlDbType.Int, 9)
            '			SubmitData.AddSqlProcParameter("@SubApplicationNum", cblo365Item.Value, SqlDbType.Int, 9)
            '			SubmitData.AddSqlProcParameter("@FacilityCd", Facility, SqlDbType.Int, 9)
            '			SubmitData.AddSqlProcParameter("@validate", ValidCd, SqlDbType.NVarChar, 20)

            '			SubmitData.AddSqlProcParameter("@who_nt_login", Session("LoginID"), SqlDbType.NVarChar, 50)

            '			SubmitData.ExecNonQueryNoReturn()


            '		End If

            '	Next

            Case Else
				'nurse stations

				Select Case Facility
					Case "15"

						For Each cblcItem As ListItem In cblCCMCNursestations.Items
							Dim sCurritem As String = cblcItem.Value

							If cblcItem.Selected = True Then

								' get subapp number
								'exec SelSubAppNurseStationByFacility @facilityCd = 15,@SubAppCd = '4NE',@applicationnum = 167
								Dim dtSubApp As New DataTable
								Dim subApp As Integer


								Dim thisdata As New CommandsSqlAndOleDb("SelSubAppNurseStationByFacility", Session("EmployeeConn"))
								thisdata.AddSqlProcParameter("@facilityCd", Facility, SqlDbType.Int, 9)
								thisdata.AddSqlProcParameter("@applicationnum", appNum, SqlDbType.Int, 9)
								thisdata.AddSqlProcParameter("@SubAppCd", cblcItem.Value, SqlDbType.NVarChar, 20)
								'@SubAppCd
								thisdata.ExecNonQueryNoReturn()

								dtSubApp = thisdata.GetSqlDataTable

								For Each row As DataRow In dtSubApp.Rows
									'SubApplicationNum FacilityCd  SubAppCd
									subApp = IIf(IsDBNull(row("SubApplicationNum")), "", row("SubApplicationNum"))

								Next
								'subApp = dtSubApp.Rows("SubApplicationNum").ToString
								'ValidCd
								If subApp > 0 Then

									Dim SubmitData As New CommandsSqlAndOleDb("InsAccountSubApplicationV3", Session("EmployeeConn"))
									SubmitData.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int, 9)
									SubmitData.AddSqlProcParameter("@ApplicationNum", appNum, SqlDbType.Int, 9)
									SubmitData.AddSqlProcParameter("@SubApplicationNum", subApp, SqlDbType.Int, 9)
									SubmitData.AddSqlProcParameter("@FacilityCd", Facility, SqlDbType.Int, 9)
									SubmitData.AddSqlProcParameter("@SubAppCd", cblcItem.Value, SqlDbType.NVarChar, 24)
									SubmitData.AddSqlProcParameter("@validate", ValidCd, SqlDbType.NVarChar, 20)

									SubmitData.AddSqlProcParameter("@who_nt_login", Session("LoginID"), SqlDbType.NVarChar, 50)

									SubmitData.ExecNonQueryNoReturn()


								End If
								' Then update the item  LoginIDTextBox.Text

							End If

						Next

					Case "12"

						For Each cblDItem As ListItem In cblDCMHNursestataions.Items
							If cblDItem.Selected = True Then

								' get subapp number
								'exec SelSubAppNurseStationByFacility @facilityCd = 15,@SubAppCd = '4NE',@applicationnum = 167
								Dim dtSubApp As New DataTable
								Dim subApp As Integer

								Dim thisdata As New CommandsSqlAndOleDb("SelSubAppNurseStationByFacility", Session("EmployeeConn"))
								thisdata.AddSqlProcParameter("@facilityCd", Facility, SqlDbType.Int, 9)
								thisdata.AddSqlProcParameter("@applicationnum", appNum, SqlDbType.Int, 9)
								thisdata.AddSqlProcParameter("@SubAppCd", cblDItem.Value, SqlDbType.NVarChar, 20)
								'@SubAppCd
								thisdata.ExecNonQueryNoReturn()

								dtSubApp = thisdata.GetSqlDataTable
								For Each row As DataRow In dtSubApp.Rows
									'SubApplicationNum FacilityCd  SubAppCd
									subApp = IIf(IsDBNull(row("SubApplicationNum")), "", row("SubApplicationNum"))


								Next

								'subApp = dtSubApp.Rows("SubApplicationNum").ToString
								If subApp > 0 Then

									Dim SubmitData As New CommandsSqlAndOleDb("InsAccountSubApplicationV3", Session("EmployeeConn"))
									SubmitData.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int, 9)
									SubmitData.AddSqlProcParameter("@ApplicationNum", appNum, SqlDbType.Int, 9)
									SubmitData.AddSqlProcParameter("@SubApplicationNum", subApp, SqlDbType.Int, 9)
									SubmitData.AddSqlProcParameter("@FacilityCd", Facility, SqlDbType.Int, 9)
									SubmitData.AddSqlProcParameter("@SubAppCd", cblDItem.Value, SqlDbType.NVarChar, 24)
									SubmitData.AddSqlProcParameter("@validate", ValidCd, SqlDbType.NVarChar, 20)

									SubmitData.AddSqlProcParameter("@who_nt_login", Session("LoginID"), SqlDbType.NVarChar, 50)

									SubmitData.ExecNonQueryNoReturn()
								End If

								'            ' Then update the item  LoginIDTextBox.Text

							End If

						Next

					Case "19"

						For Each cblTItem As ListItem In cblTaylorNursestataions.Items


							If cblTItem.Selected = True Then
								'cblItem.Enabled = False

								' get subapp number
								'exec SelSubAppNurseStationByFacilInsAccountSubApplicationV3ity @facilityCd = 15,@SubAppCd = '4NE',@applicationnum = 167
								Dim dtSubApp As New DataTable
								Dim subApp As Integer

								Dim thisdata As New CommandsSqlAndOleDb("SelSubAppNurseStationByFacility", Session("EmployeeConn"))
								thisdata.AddSqlProcParameter("@facilityCd", Facility, SqlDbType.Int, 9)
								thisdata.AddSqlProcParameter("@applicationnum", appNum, SqlDbType.Int, 9)
								thisdata.AddSqlProcParameter("@SubAppCd", cblTItem.Value, SqlDbType.NVarChar, 20)
								'@SubAppCd
								thisdata.ExecNonQueryNoReturn()

								dtSubApp = thisdata.GetSqlDataTable

								For Each row As DataRow In dtSubApp.Rows
									'SubApplicationNum FacilityCd  SubAppCd
									subApp = IIf(IsDBNull(row("SubApplicationNum")), "", row("SubApplicationNum"))

								Next

								'subApp = dtSubApp.Rows("SubApplicationNum").ToString
								If subApp > 0 Then

									Dim SubmitData As New CommandsSqlAndOleDb("InsAccountSubApplicationV3", Session("EmployeeConn"))
									SubmitData.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int, 9)
									SubmitData.AddSqlProcParameter("@ApplicationNum", appNum, SqlDbType.Int, 9)
									SubmitData.AddSqlProcParameter("@SubApplicationNum", subApp, SqlDbType.Int, 9)
									SubmitData.AddSqlProcParameter("@FacilityCd", Facility, SqlDbType.Int, 9)
									SubmitData.AddSqlProcParameter("@SubAppCd", cblTItem.Value, SqlDbType.NVarChar, 24)
									SubmitData.AddSqlProcParameter("@validate", ValidCd, SqlDbType.NVarChar, 20)

									SubmitData.AddSqlProcParameter("@who_nt_login", Session("LoginID"), SqlDbType.NVarChar, 50)

									SubmitData.ExecNonQueryNoReturn()


								End If

								' Then update the item  LoginIDTextBox.Text

							End If

						Next

					Case "16"


						For Each cblSItem As ListItem In cblSpringfieldNursestataions.Items

							If cblSItem.Selected = True Then
								'cblItem.Enabled = False

								' get subapp number
								'exec SelSubAppNurseStationByFacility @facilityCd = 15,@SubAppCd = '4NE',@applicationnum = 167
								Dim dtSubApp As New DataTable
								Dim subApp As Integer

								Dim thisdata As New CommandsSqlAndOleDb("SelSubAppNurseStationByFacility", Session("EmployeeConn"))
								thisdata.AddSqlProcParameter("@facilityCd", Facility, SqlDbType.Int, 9)
								thisdata.AddSqlProcParameter("@applicationnum", appNum, SqlDbType.Int, 9)
								thisdata.AddSqlProcParameter("@SubAppCd", cblSItem.Value, SqlDbType.NVarChar, 20)
								'@SubAppCd
								thisdata.ExecNonQueryNoReturn()

								dtSubApp = thisdata.GetSqlDataTable
								For Each row As DataRow In dtSubApp.Rows
									'SubApplicationNum FacilityCd  SubAppCd
									subApp = IIf(IsDBNull(row("SubApplicationNum")), "", row("SubApplicationNum"))

								Next

								'subApp = dtSubApp.Rows("SubApplicationNum").ToString
								If subApp > 0 Then

									Dim SubmitData As New CommandsSqlAndOleDb("InsAccountSubApplicationV3", Session("EmployeeConn"))
									SubmitData.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int, 9)
									SubmitData.AddSqlProcParameter("@ApplicationNum", appNum, SqlDbType.Int, 9)
									SubmitData.AddSqlProcParameter("@SubApplicationNum", subApp, SqlDbType.Int, 9)
									SubmitData.AddSqlProcParameter("@FacilityCd", Facility, SqlDbType.Int, 9)
									SubmitData.AddSqlProcParameter("@SubAppCd", cblSItem.Value, SqlDbType.NVarChar, 24)
									SubmitData.AddSqlProcParameter("@validate", ValidCd, SqlDbType.NVarChar, 20)

									SubmitData.AddSqlProcParameter("@who_nt_login", Session("LoginID"), SqlDbType.NVarChar, 50)

									SubmitData.ExecNonQueryNoReturn()
								End If


								' Then update the item  LoginIDTextBox.Text

							End If

						Next

				End Select


		End Select
		' end of app numbere

	End Sub
	Public Function CheckCellPhone() As Boolean

        'Public Function isValid() As Boolean
        If TokenPhoneTextBox.Text = "" Then
            lblTokenError.Text = " Must supply Phone number"
            lblTokenError.Visible = True
            'lblTokenError.Focus()

            lblValidation.Text = "Must supply Phone number"
            lblValidation.ForeColor = Color.Red
            TokenPhoneTextBox.Focus()


            TokenPhoneTextBox.BackColor() = Color.Red
            blnReturn = True
            Return blnReturn


        End If
        Dim nPhone As Long
		Dim sPhone As String = TokenPhoneTextBox.Text.TrimEnd

		Long.TryParse(sPhone, nPhone)

		If nPhone = False Then
			lblValidation.Text = "Phone Number must only contain numbers."
			lblValidation.ForeColor = Color.Red
			'lblValidation.Focus()

			lblTokenError.Text = "Phone Number must only contain numbers."
			lblTokenError.Visible = True
			TokenPhoneTextBox.BackColor() = Color.Yellow
			TokenPhoneTextBox.BorderColor() = Color.Black
			TokenPhoneTextBox.BorderStyle() = BorderStyle.Solid
			TokenPhoneTextBox.BorderWidth = Unit.Pixel(2)

			TokenPhoneTextBox.Focus()
			blnReturn = True

			Return blnReturn

		End If

        'If CellProvidertxt.Text <> "" Then
        '	If TokenPhoneTextBox.Text = "" Then
        '		lblTokenError.Text = " Must supply a Cell Phone number "
        '		lblTokenError.Visible = True

        '		lblValidation.Text = " Must supply a Cell Phone number"
        '		lblValidation.ForeColor = Color.Red

        '		TokenPhoneTextBox.BackColor() = Color.Yellow
        '		TokenPhoneTextBox.BorderColor() = Color.Black
        '		TokenPhoneTextBox.BorderStyle() = BorderStyle.Solid
        '		TokenPhoneTextBox.BorderWidth = Unit.Pixel(2)

        '		TokenPhoneTextBox.Focus()

        '		TokenPhoneTextBox.BackColor() = Color.Red
        '		blnReturn = True
        '		Return blnReturn
        '	Else

        '		If (TokenPhoneTextBox.Text.Length) <> 10 Then

        '			lblTokenError.Text = "Phone Number Not correct length."
        '			lblTokenError.Visible = True

        '			lblValidation.Text = " Phone Number Not correct length."
        '			lblValidation.ForeColor = Color.Red

        '			TokenPhoneTextBox.BackColor() = Color.Yellow
        '			TokenPhoneTextBox.BorderColor() = Color.Black
        '			TokenPhoneTextBox.BorderStyle() = BorderStyle.Solid
        '			TokenPhoneTextBox.BorderWidth = Unit.Pixel(2)

        '			TokenPhoneTextBox.Focus()
        '			blnReturn = True
        '			Return blnReturn

        '		End If

        '	End If

        Return blnReturn

    End Function
	Protected Sub cblFacilities_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cblFacilities.SelectedIndexChanged

		CheckforToken()
		CheckECare()
		ChangColorNoCheck()
        'Checkfor365()
        'ChangColor()

        'Dim tbTCL As New DataTable

        'tbTCL.Columns.Add(New DataColumn("TCL"))

        'tbTCL.Columns.Add(New DataColumn("Hospital"))


        'TCLCCMC.Text = ""
        'TCLDCMH.Text = ""
        'TCLTaylor.Text = ""
        'TCLSpring.Text = ""


        'Dim hospitalcount As Integer = 0
        ''Dim OrigReqDesc As String = ""
        'Dim MakCont As Boolean
        'Dim CurrCont As Boolean

        'If OrigReqDesc.Text = "" Then
        '    OrigReqDesc.Text = request_descTextBox.Text

        'End If

        'request_descTextBox.Text = ""


        'Dim Rolepass As String = ""
        'If RoleNumLabel.Text <> "" Then
        '    Rolepass = RoleNumLabel.Text
        'ElseIf thisRole <> emp_type_cdrbl.SelectedValue Then
        '    Rolepass = thisRole
        'Else
        '    Rolepass = emp_type_cdrbl.SelectedValue
        'End If

        ' count how many hospitals checked if only then do not execute
        'For Each cbHospitals As ListItem In cblFacilities.Items
        '    If cbHospitals.Selected Then
        '        hospitalcount = hospitalcount + 1
        '    End If
        'Next

        'Select Case Rolepass
        '    Case "950", "1193", "1195", "1422", "1425", "1428", "1430", "1431"

        '    Case Else

        '        If hospitalcount > 1 Then

        '            ' check all hospitals
        '            For Each cbHospitals As ListItem In cblFacilities.Items
        '                If cbHospitals.Selected Then

        '                    Dim sCurrHospital As String = cbHospitals.ToString

        '                    Select Case sCurrHospital.ToString.TrimEnd

        '                        Case "CCMC"
        '                            Dim tcldata As New CommandsSqlAndOleDb("SelInvisionUserType", Session("EmployeeConn"))
        '                            tcldata.AddSqlProcParameter("@rolenum", Rolepass, SqlDbType.NVarChar, 8)

        '                            tcldata.GetSqlDataset()

        '                            dtTCLUserTypes = tcldata.GetSqlDataTable

        '                            If dtTCLUserTypes.Rows.Count > 0 Then

        '                                TCLCCMC.Text = dtTCLUserTypes.Rows(0)("TCL").ToString

        '                                If TCLCCMC.Text <> "" Then

        '                                    tbTCL.Rows.Add(TCLCCMC.Text, "15")

        '                                    If TCLCCMC.Text <> TCLTextBox.Text Then

        '                                        request_descTextBox.Text = request_descTextBox.Text & "  CCMC TCL:" & TCLCCMC.Text
        '                                        TCLTextBox.Text = TCLCCMC.Text

        '                                    End If
        '                                End If

        '                            End If


        '                            dtTCLUserTypes.Dispose()


        '                            ' Always test for non union hospital#5
        '                            Dim uniontcldata As New CommandsSqlAndOleDb("SelInvisionUserType", Session("EmployeeConn"))
        '                            uniontcldata.AddSqlProcParameter("@rolenum", Rolepass, SqlDbType.NVarChar, 8)


        '                            uniontcldata.GetSqlDataset()

        '                            dtTCLUserTypes = uniontcldata.GetSqlDataTable
        '                            If dtTCLUserTypes.Rows.Count > 0 Then
        '                                UnionTCL.Text = dtTCLUserTypes.Rows(0)("TCL").ToString

        '                                If UnionTCL.Text <> "" And UnionTCL.Text <> "TCL" Then

        '                                    tbTCL.Rows.Add(UnionTCL.Text, "5")


        '                                    request_descTextBox.Text = request_descTextBox.Text & "  Non Union TCL:" & UnionTCL.Text

        '                                End If

        '                            End If
        '                        Case "DCMH"

        '                            Dim dcmhtcldata As New CommandsSqlAndOleDb("SelInvisionUserType", Session("EmployeeConn"))
        '                            dcmhtcldata.AddSqlProcParameter("@rolenum", Rolepass, SqlDbType.NVarChar, 8)
        '                            dcmhtcldata.GetSqlDataset()

        '                            dtTCLUserTypes = dcmhtcldata.GetSqlDataTable

        '                            If dtTCLUserTypes.Rows.Count > 0 Then

        '                                TCLDCMH.Text = dtTCLUserTypes.Rows(0)("TCL").ToString

        '                                If TCLDCMH.Text <> "" Then

        '                                    If TCLTextBox.Text <> TCLDCMH.Text Then

        '                                        tbTCL.Rows.Add(TCLDCMH.Text, "12")

        '                                        request_descTextBox.Text = request_descTextBox.Text & "  DCMH TCL:" & TCLDCMH.Text

        '                                        TCLTextBox.Text = TCLDCMH.Text

        '                                    End If

        '                                End If

        '                            End If

        '                            dtTCLUserTypes.Dispose()


        '                        Case "Taylor"
        '                            Dim taylortcldata As New CommandsSqlAndOleDb("SelInvisionUserType", Session("EmployeeConn"))
        '                            taylortcldata.AddSqlProcParameter("@rolenum", Rolepass, SqlDbType.NVarChar, 8)

        '                            taylortcldata.GetSqlDataset()

        '                            dtTCLUserTypes = taylortcldata.GetSqlDataTable

        '                            If dtTCLUserTypes.Rows.Count > 0 Then

        '                                TCLTaylor.Text = dtTCLUserTypes.Rows(0)("TCL").ToString
        '                                If TCLTaylor.Text <> "" Then

        '                                    If TCLTextBox.Text <> TCLTaylor.Text Then

        '                                        tbTCL.Rows.Add(TCLTaylor.Text, "19")


        '                                        request_descTextBox.Text = request_descTextBox.Text & "  Taylor TCL:" & TCLTaylor.Text


        '                                        TCLTextBox.Text = TCLTaylor.Text
        '                                    End If

        '                                End If
        '                            End If

        '                            dtTCLUserTypes.Dispose()

        '                        Case "Springfield"
        '                            Dim sprtcldata As New CommandsSqlAndOleDb("SelInvisionUserType", Session("EmployeeConn"))
        '                            sprtcldata.AddSqlProcParameter("@rolenum", Rolepass, SqlDbType.NVarChar, 8)

        '                            sprtcldata.GetSqlDataset()

        '                            dtTCLUserTypes = sprtcldata.GetSqlDataTable

        '                            If dtTCLUserTypes.Rows.Count > 0 Then

        '                                TCLSpring.Text = dtTCLUserTypes.Rows(0)("TCL").ToString

        '                                If TCLSpring.Text <> "" Then

        '                                    If TCLTextBox.Text <> TCLSpring.Text Then
        '                                        tbTCL.Rows.Add(TCLSpring.Text, "16")

        '                                        request_descTextBox.Text = request_descTextBox.Text & "  Springfield TCL:" & TCLSpring.Text

        '                                        TCLTextBox.Text = TCLSpring.Text

        '                                    End If
        '                                End If

        '                                dtTCLUserTypes.Dispose()
        '                            End If


        '                    End Select

        '                End If

        '            Next

        '            pnlTCl.Visible = True

        '        ElseIf hospitalcount = 1 And TCLTextBox.Text = "" Then

        '            Dim sCurrHospital As String = ""

        '            For Each cbHospitals As ListItem In cblFacilities.Items

        '                If cbHospitals.Selected Then

        '                    sCurrHospital = cbHospitals.Value

        '                End If
        '            Next

        '            Dim tcldata As New CommandsSqlAndOleDb("SelInvisionUserType", Session("EmployeeConn"))
        '            tcldata.AddSqlProcParameter("@rolenum", Rolepass, SqlDbType.NVarChar, 8)
        '            tcldata.AddSqlProcParameter("@hospitalnum", sCurrHospital, SqlDbType.NVarChar, 8)

        '            tcldata.GetSqlDataset()

        '            dtTCLUserTypes = tcldata.GetSqlDataTable


        '            TCLTextBox.Text = dtTCLUserTypes.Rows(0)("TCL").ToString
        '            UserTypeDescTextBox.Text = dtTCLUserTypes.Rows(0)("UserTypeDesc").ToString
        '            UserTypeCdTextBox.Text = dtTCLUserTypes.Rows(0)("UserTypeCd").ToString

        '            tbTCL.Rows.Add(TCLTextBox.Text, sCurrHospital)

        '            request_descTextBox.Text = OrigReqDesc.Text

        '            pnlTCl.Visible = True
        '            TCLTextBox.Visible = True
        '            UserTypeDescTextBox.Visible = True
        '            UserTypeCdTextBox.Visible = True

        '        End If

        '        MakCont = OrigReqDesc.Text.Contains("Mak ID")
        '        CurrCont = request_descTextBox.Text.Contains("Mak ID")
        '        If MakCont = True And CurrCont = False Then
        '            request_descTextBox.Text = request_descTextBox.Text & " Please Add Mak ID "
        '        End If

        'End Select

        'request_descTextBox.Text = OrigReqDesc & request_descTextBox.Text
        'request_descTextBox

    End Sub

	Protected Sub cblEntities_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cblEntities.SelectedIndexChanged
		ChangColorNoCheck()
		'Checkfor365()
		'ChangColor()
	End Sub

	Private Sub CheckExists()
		CheckRequiredFields()
		Dim Submitternum As Integer
		Submitternum = submitter_client_numLabel.Text


		Dim ExistsClientsData As New CommandsSqlAndOleDb("SelExistingClients", Session("EmployeeConn"))
		ExistsClientsData.AddSqlProcParameter("@FirstName", first_nameTextBox.Text, SqlDbType.NVarChar, 20)
		ExistsClientsData.AddSqlProcParameter("@LastName", last_nameTextBox.Text, SqlDbType.NVarChar, 20)
		ExistsClientsData.AddSqlProcParameter("@SubmitterClientNum", Submitternum, SqlDbType.Int, 9)


		grvClientsExists.DataSource = ExistsClientsData.GetSqlDataTable
		grvClientsExists.DataBind()


		'UpdPnlExists.Update()
		ClientExists.Show()


	End Sub
    'Protected Sub CheckforCPMResident()
    '    For Each cbApp As ListItem In cblApplications.Items
    '        If cbApp.Selected Then

    '            Select cbApp.Value
    '                Case 157
    '                    If hanrbl.SelectedValue.ToString = "Yes" Then

    '                        Dim ViewCPMModelreq As New RequiredField(CPMEmployeesddl, "CPM Model")

    '                        Dim npiReq As New RequiredField(npitextbox, "NPI")
    '                        Dim LicensesNoReq As New RequiredField(LicensesNotextbox, "Licenses No")
    '                        Dim taxonomyreq As New RequiredField(taxonomytextbox, "Taxonomy")
    '                        Dim startdatereq As New RequiredField(start_dateTextBox, "Start Date")
    '                        Dim Schedulablereq As New RequiredField(Schedulablerbl, "Schedule")
    '                        Dim Televoxrblreq As New RequiredField(Televoxrbl, "Televox")
    '                    End If
    '            End Select
    '        End If

    '    Next

    'End Sub

    'Protected Sub Checkfor365()

    '    For Each cbMainApp As ListItem In cblApplications.Items
    '        If cbMainApp.Selected Then
    '            Select Case cbMainApp.Value
    '                Case 136
    '                    For Each cbApp As ListItem In cblo365.Items
    '                        Select Case cbApp.Value
    '                            Case 248, 138
    '                                If cbApp.Selected Then

    '                                Else
    '                                    If Session("environment").ToString.ToLower <> "testing" Then

    '                                        If cbApp.Value = 138 Then

    '                                            cbApp.Selected = True


    '                                            cbApp.Enabled = False

    '                                        End If
    '                                    ElseIf Session("environment").ToString.ToLower = "testing" Then

    '                                        If cbApp.Value = 248 Then

    '                                            cbApp.Selected = True


    '                                            cbApp.Enabled = False

    '                                        End If

    '                                    End If
    '                                End If

    '                        End Select

    '                    Next

    '            End Select
    '        End If
    '    Next

    'End Sub
    Protected Sub CheckforToken()
		For Each cbApp As ListItem In cblApplications.Items
			If cbApp.Selected Then

				Select Case cbApp.Value
					Case 156
						'load fields


						pnlTokenAddress.Visible = True

                        'CellPhoneddl.BackColor() = Color.Yellow
                        'CellPhoneddl.BorderColor() = Color.Black
                        'CellPhoneddl.BorderStyle() = BorderStyle.Solid
                        'CellPhoneddl.BorderWidth = Unit.Pixel(2)


                        TokenPhoneTextBox.BackColor() = Color.Yellow
						TokenPhoneTextBox.BorderColor() = Color.Black
						TokenPhoneTextBox.BorderStyle() = BorderStyle.Solid
						TokenPhoneTextBox.BorderWidth = Unit.Pixel(2)


                End Select
			ElseIf cbApp.Value = 156 Then
				pnlTokenAddress.Visible = False

                'TokenNameTextBox.Text = ""
                'TokenAddress1TextBox.Text = ""
                'TokenAddress2TextBox.Text = ""

                'TokenCityTextBox.Text = ""
                'TokenStTextBox.Text = ""
                'TokenPhoneTextBox.Text = ""
                'TokenZipTextBox.Text = ""
                'TokencommentsTextBox.Text = ""

            End If
		Next


	End Sub
    'Protected Sub CellPhoneddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles CellPhoneddl.SelectedIndexChanged
    '	CellProvidertxt.Text = CellPhoneddl.SelectedValue
    '	If CellProvidertxt.Text.ToLower = "other" Then
    '		CellPhoneddl.Visible = False

    '		otherCellprov.BackColor() = Color.Yellow
    '		otherCellprov.BorderColor() = Color.Black
    '		otherCellprov.BorderStyle() = BorderStyle.Solid
    '		otherCellprov.BorderWidth = Unit.Pixel(2)

    '		otherCellprov.Visible = True

    '	End If
    '	ChangColorNoCheck()

    'End Sub

    Protected Sub CheckforRadiology()
		For Each cbApp As ListItem In cblApplications.Items
			If cbApp.Selected = False Then
				'not select items
				Select Case cbApp.Value
					Case 1, 3, 10, 12, 139, 142, 145, 148, 151
						If TCLTextBox.Text.ToLower = "radsigon" Then
							cbApp.Selected = True
						End If
					Case 154
						'Pioneer urget care
						If facility_cdTextBox.Text = "25" Then
							If building_cdTextBox.Text = "1" Then
								cbApp.Selected = True
							End If
						End If

				End Select
			End If

		Next

	End Sub
	Protected Sub CheckECare()
		'TCLddl.Visible = False

		gvTCL.Visible = False


		TCLTextBox.Visible = False
		UserTypeDescTextBox.Visible = False
        UserTypeCdTextBox.Visible = False
        ecareappSelectedlbl.Text = ""


        Dim TclCount As Integer

		TclCount = gvTCL.Rows.Count()

		For Each cbApp As ListItem In cblApplications.Items
			If cbApp.Selected Then
				'Select Case cbApp.ToString.ToLower.Trim

				Select Case cbApp.Value 'eCare
					Case "43"
                        ecareappSelectedlbl.Text = cbApp.Value

                        If UserTypeCdTextBox.Text = "" Then
                            pnlTCl.Visible = True
                            Dim Rolepass As String = ""

                            Rolepass = RoleNumLabel.Text
                            If RoleNumLabel.Text <> emp_type_cdrbl.SelectedValue Then
                                Rolepass = RoleNumLabel.Text
                            Else
                                Rolepass = emp_type_cdrbl.SelectedValue
                            End If



                            Select Case emp_type_cdrbl.SelectedValue

                                'Case "physician/rovider", "non staff clinician", "resident"  "allied health"

                                Case "1193"
                                    'NeedsProviderrbl.Visible = True
                                    'DoesNeedprovlbl.Visible = True
                                    'NeedsProviderrbl.SelectedValue = "No"
                                    'NeedsProviderrbl.BackColor() = Color.Yellow
                                    'NeedsProviderrbl.BorderColor() = Color.Black
                                    'NeedsProviderrbl.BorderStyle() = BorderStyle.Solid
                                    'NeedsProviderrbl.BorderWidth = Unit.Pixel(2)

                                    Dim tcldata As New CommandsSqlAndOleDb("SelInvisionUserTypeV20", Session("EmployeeConn"))
                                    tcldata.AddSqlProcParameter("@rolenum", Rolepass, SqlDbType.NVarChar, 8)

                                    tcldata.GetSqlDataset()




                                    dtTCLUserTypes = tcldata.GetSqlDataTable

                                    If dtTCLUserTypes.Rows.Count = 1 Then
                                        'TCLddl.SelectedIndex = 0

                                        TCLTextBox.Text = dtTCLUserTypes.Rows(0)("TCL").ToString
                                        UserTypeDescTextBox.Text = dtTCLUserTypes.Rows(0)("UserTypeDesc").ToString
                                        UserTypeCdTextBox.Text = dtTCLUserTypes.Rows(0)("UserTypeCd").ToString
                                        'TCLddl.Visible = False

                                        gvTCL.Visible = False

                                        pnlTCl.Visible = True
                                        TCLTextBox.Visible = True
                                        UserTypeDescTextBox.Visible = True
                                        UserTypeCdTextBox.Visible = True


                                    ElseIf dtTCLUserTypes.Rows.Count > 1 Then

                                        gvTCL.DataSource = dtTCLUserTypes
                                        gvTCL.DataBind()

                                        gvTCL.Visible = True
                                        gvTCL.BackColor() = Color.Yellow
                                        gvTCL.BorderColor() = Color.Black
                                        gvTCL.BorderStyle() = BorderStyle.Solid
                                        gvTCL.BorderWidth = Unit.Pixel(2)
                                        gvTCL.RowStyle.BackColor() = Color.Yellow

                                        pnlTCl.Visible = True
                                        TCLTextBox.Visible = True
                                        UserTypeDescTextBox.Visible = True
                                        UserTypeCdTextBox.Visible = True

                                    ElseIf dtTCLUserTypes.Rows.Count = 0 Then
                                        pnlTCl.Visible = False

                                    End If

                                    'TCLddl.DataSource = dtTCLUserTypes

                                    'TCLddl.DataTextField = "UserTypeDesc"
                                    'TCLddl.DataValueField = "TCL"
                                    'TCLddl.DataBind()

                                    'TCLddl.Items.Insert(0, "Select")

                                    'TCLddl.Visible = True


                                    'Dim ddTCLBinder As New DropDownListBinder(TCLddl, "SelInvisionTCL", Session("EmployeeConn"))
                                    'ddTCLBinder.AddDDLCriteria("@rolenum", emp_type_cdrbl.SelectedValue, SqlDbType.NVarChar)

                                    'ddTCLBinder.BindData("TCL", "UserTypeDesc")

                                Case "782"
                                    If UserTypeCdTextBox.Text = "" And TclCount = 0 Then
                                        pnlTCl.Visible = False


                                    ElseIf TclCount > 0 Then

                                        TCLTextBox.Visible = True
                                        UserTypeDescTextBox.Visible = True
                                        UserTypeCdTextBox.Visible = True
                                        gvTCL.Visible = True

                                        pnlTCl.Visible = True
                                    ElseIf UserTypeCdTextBox.Text <> "" Then
                                        TCLTextBox.Visible = True
                                        UserTypeDescTextBox.Visible = True
                                        UserTypeCdTextBox.Visible = True
                                        gvTCL.Visible = False

                                        pnlTCl.Visible = True


                                    End If

                                    ' Case "1195", "1196", "1189"

                                    'NeedsProviderrbl.Visible = True
                                    'DoesNeedprovlbl.Visible = True
                                    'NeedsProviderrbl.SelectedValue = "No"
                                    'NeedsProviderrbl.BackColor() = Color.Yellow
                                    'NeedsProviderrbl.BorderColor() = Color.Black
                                    'NeedsProviderrbl.BorderStyle() = BorderStyle.Solid
                                    'NeedsProviderrbl.BorderWidth = Unit.Pixel(2)

                                Case Else
                                    If Rolepass = "1200" Then
                                        Rolepass = "918"
                                    End If

                                    Dim tcldata As New CommandsSqlAndOleDb("SelInvisionUserTypeV20", Session("EmployeeConn"))
                                    tcldata.AddSqlProcParameter("@rolenum", Rolepass, SqlDbType.NVarChar, 8)

                                    tcldata.GetSqlDataset()




                                    dtTCLUserTypes = tcldata.GetSqlDataTable

                                    If dtTCLUserTypes.Rows.Count = 1 Then
                                        'TCLddl.SelectedIndex = 0

                                        TCLTextBox.Text = dtTCLUserTypes.Rows(0)("TCL").ToString
                                        UserTypeDescTextBox.Text = dtTCLUserTypes.Rows(0)("UserTypeDesc").ToString
                                        UserTypeCdTextBox.Text = dtTCLUserTypes.Rows(0)("UserTypeCd").ToString
                                        'TCLddl.Visible = False

                                        gvTCL.Visible = False

                                        pnlTCl.Visible = True
                                        TCLTextBox.Visible = True
                                        UserTypeDescTextBox.Visible = True
                                        UserTypeCdTextBox.Visible = True


                                    ElseIf dtTCLUserTypes.Rows.Count > 1 Then

                                        gvTCL.DataSource = dtTCLUserTypes
                                        gvTCL.DataBind()

                                        gvTCL.Visible = True
                                        gvTCL.BackColor() = Color.Yellow
                                        gvTCL.BorderColor() = Color.Black
                                        gvTCL.BorderStyle() = BorderStyle.Solid
                                        gvTCL.BorderWidth = Unit.Pixel(2)
                                        gvTCL.RowStyle.BackColor() = Color.Yellow

                                        pnlTCl.Visible = True
                                        TCLTextBox.Visible = True
                                        UserTypeDescTextBox.Visible = True
                                        UserTypeCdTextBox.Visible = True

                                    ElseIf dtTCLUserTypes.Rows.Count = 0 Then
                                        pnlTCl.Visible = False

                                    End If

                                    'TCLTextBox.Visible = True
                                    'UserTypeCdTextBox.Visible = True
                                    'UserTypeDescTextBox.Visible = True
                                    'gvTCL.Visible = True
                            End Select


                            Return
                        ElseIf UserTypeCdTextBox.Text <> "" Or TclCount > 0 Then
                            TCLTextBox.Visible = True
							UserTypeDescTextBox.Visible = True
							UserTypeCdTextBox.Visible = True

						End If

                        'Case "136"
                        '	Load365Locations()
                        '	O365Panel.Visible = True



                End Select
			End If

		Next


	End Sub
	Private Sub insPMHAddedItems(ByVal requestNum As Integer)
		Dim thisdata As New CommandsSqlAndOleDb("InsAccountReqActionItemV9", Session("EmployeeConn"))
		thisdata.AddSqlProcParameter("@account_request_num", requestNum, SqlDbType.Int, 20)
		'@actionCD
		thisdata.AddSqlProcParameter("@actionCD", "itemsadded", SqlDbType.NVarChar, 20)

		thisdata.AddSqlProcParameter("@who_nt_login", Session("LoginID"), SqlDbType.NVarChar, 50)

		thisdata.ExecNonQueryNoReturn()

	End Sub
	Private Sub insAcctItems(ByVal requestNum As Integer, ByVal appNum As String, ByVal comments As String)

		Dim dt As New DataTable

		Dim thisdata As New CommandsSqlAndOleDb("InsAccountItemsV3", Session("EmployeeConn"))
		thisdata.AddSqlProcParameter("@account_request_num", requestNum, SqlDbType.Int, 20)
		thisdata.AddSqlProcParameter("@ApplicationNum", appNum, SqlDbType.Int, 8)

		Select Case emp_type_cdrbl.SelectedValue
			'Case "physician/rovider", "non staff clinician", "resident"  "allied health"

			Case "782", "1197", "1194"

				thisdata.AddSqlProcParameter("@validate", "NeedsDocNum", SqlDbType.NVarChar, 20)


			Case Else
				'thisdata.AddSqlProcParameter("@validate", "NeedsValidation", SqlDbType.NVarChar, 20)

				If NeedsProviderrbl.SelectedValue = "Yes" Then
					thisdata.AddSqlProcParameter("@validate", "NeedsDocNum", SqlDbType.NVarChar, 20)

				Else
					thisdata.AddSqlProcParameter("@validate", "NeedsValidation", SqlDbType.NVarChar, 20)

				End If


		End Select
		If appNum = "7" Then
			thisdata.AddSqlProcParameter("@account_item_desc", comments, SqlDbType.NVarChar, 255)
		End If

		thisdata.AddSqlProcParameter("@who_nt_login", Session("LoginID"), SqlDbType.NVarChar, 50)

		thisdata.ExecNonQueryNoReturn()



	End Sub
	Private Sub insAcctDriveModel(ByVal requestNum As Integer, ByVal ModelCd As String, ByVal comments As String, ByVal CPMTrainCd As String, ByVal Requested_Date As String, ByVal DriveLetter As String)


		Dim thisdata As New CommandsSqlAndOleDb("InsModelsAndDriveItemsV3", Session("EmployeeConn"))
		thisdata.AddSqlProcParameter("@account_request_num", requestNum, SqlDbType.Int, 20)


		thisdata.AddSqlProcParameter("@ModelCd", ModelCd, SqlDbType.NVarChar, 24)
		thisdata.AddSqlProcParameter("@CPMTrainCd", CPMTrainCd, SqlDbType.NVarChar, 24)

		If CPMTrainCd <> "" Then
			thisdata.AddSqlProcParameter("@ApplicationNum", 157, SqlDbType.Int, 8)

			thisdata.AddSqlProcParameter("@Requested_Date", Requested_Date, SqlDbType.NVarChar, 24)

		End If

		thisdata.AddSqlProcParameter("@DriveLetter", DriveLetter, SqlDbType.NVarChar, 24)

		thisdata.AddSqlProcParameter("@validate", "NeedsValidation", SqlDbType.NVarChar, 20)

		thisdata.AddSqlProcParameter("@account_item_desc", comments, SqlDbType.NVarChar, 255)

		thisdata.AddSqlProcParameter("@who_nt_login", Session("LoginID"), SqlDbType.NVarChar, 50)

		thisdata.ExecNonQueryNoReturn()



	End Sub

	Protected Sub CheckEntitiesHosp()
		'' check all entities 
		For Each cbentities As ListItem In cblEntities.Items
			If cbentities.Selected = False Then
				cbentities.Selected = True
			End If
		Next


		' check all hospitals
		For Each cbHospitals As ListItem In cblFacilities.Items
			Dim sCurrHospital As String = cbHospitals.ToString

			If cbHospitals.Selected = False And sCurrHospital.ToString.ToLower <> "community" Then
				cbHospitals.Selected = True

			End If
		Next

	End Sub

	Protected Sub Rolesddl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Rolesddl.SelectedIndexChanged

		lblValidation.Text = ""
		'cpmModelEmplPanel.Visible = False
		'cpmModelAfterPanel.Visible = False

		'CPMEmployeesddl.SelectedIndex = -1
		'cpmModelAfterTextbox.Text = ""

		For Each cblItem As ListItem In cblApplications.Items
			cblItem.Selected = False
			'cblItem.Enabled = False
		Next


		If Session("SecurityLevelNum") < 70 Then
            If Session("SecurityLevelNum") <> 65 Then

                For Each cblItem As ListItem In cblApplications.Items

                    cblItem.Enabled = False
                Next

            End If
        End If

        For Each cblEntitem As ListItem In cblEntities.Items
            cblEntitem.Selected = False
        Next

        For Each cblFacItem As ListItem In cblFacilities.Items
            cblFacItem.Selected = False
        Next

        pnlTCl.Visible = False
        TCLTextBox.Text = ""
        UserTypeCdTextBox.Text = ""
        UserTypeDescTextBox.Text = ""



        Dim rwRef() As DataRow
        Dim ddlBinderV2 As DropDownListBinder

        'always use the Main Role
        mainRole = emp_type_cdrbl.SelectedValue

        If Requesttype = "" Then '"addpmhact"


			request_descTextBox.Text = ""

		End If
		If request_descTextBox.Text <> "" Then
			request_descTextBox.ForeColor() = Color.Red
		End If


		request_descTextBox.BackColor() = Color.White
		request_descTextBox.BorderStyle = BorderStyle.None
        request_descTextBox.BorderWidth = Unit.Pixel(2)

        If Rolesddl.SelectedValue.ToString <> "Select" Then
            user_position_descTextBox.Text = Rolesddl.SelectedValue
            mainRole = Rolesddl.SelectedValue
            Select Case mainRole
                Case "1423", "741", "1559", "1566"

                    ' go to finanical singon form
                    Dim emprole As String
                    emprole = emp_type_cdrbl.SelectedValue.ToString

					Response.Redirect("SignOnFormFinanical.aspx?&EmpRoleNum=" & emprole & "&MainRolenum=" & mainRole & "&AccountRequestNum=" & Session("AccountRequestNum") & "&ClientNum=" & ReactivateClientNumTextBox.Text.ToString, True)
				Case "1553"
                    hanrbl.SelectedValue = "Yes"
                    hanrbl_SelectedIndexChanged(hanrbl.SelectedValue, e)
                    PhysOfficepanel.Visible = False

                Case "782"
                    PhysOfficepanel.Visible = False
                Case "1567"

                    PhysOfficeddl.Dispose()

                    Dim ddEMRBinder As New DropDownListBinder(PhysOfficeddl, "SelPhysGroupsV5", Session("EmployeeConn"))
                    ddEMRBinder.AddDDLCriteria("@GroupTypeCd", "GNum", SqlDbType.NVarChar)
                    'SelPhysGroupsV5
                    ddEMRBinder.BindData("Group_num", "group_name")

                    PhysOfficepanel.Visible = True

                    PhysOfficeddl.BackColor() = Color.Yellow
                    PhysOfficeddl.BorderColor() = Color.Black
                    PhysOfficeddl.BorderStyle() = BorderStyle.Solid
					PhysOfficeddl.BorderWidth = Unit.Pixel(2)

					getTCLforRole()
					'EMROfficeddl

					'CPMEmployeesddl.Dispose()

					'Dim ddcmpMOdelafter As New DropDownListBinder
					'ddcmpMOdelafter.BindData(CPMEmployeesddl, "SelCPMClients", "client_num", "FullName", Session("EmployeeConn"))

					'cpmModelEmplPanel.Visible = True

					''CPMEmployeesddl
					'CPMEmployeesddl.BackColor() = Color.Yellow
					'CPMEmployeesddl.BorderColor() = Color.Black
					'CPMEmployeesddl.BorderStyle() = BorderStyle.Solid
					'CPMEmployeesddl.BorderWidth = Unit.Pixel(2)


					''cpmModelAfterTextbox
					'cpmModelAfterTextbox.BackColor() = Color.Yellow
					'cpmModelAfterTextbox.BorderColor() = Color.Black
					'cpmModelAfterTextbox.BorderStyle() = BorderStyle.Solid
					'cpmModelAfterTextbox.BorderWidth = Unit.Pixel(2)

					start_dateTextBox.BackColor() = Color.Yellow
                    start_dateTextBox.BorderColor() = Color.Black
                    start_dateTextBox.BorderStyle() = BorderStyle.Solid
                    start_dateTextBox.BorderWidth = Unit.Pixel(2)


                Case Else
                    PhysOfficepanel.Visible = True

            End Select
            Title2TextBox.Text = Rolesddl.SelectedItem.ToString
            If thisRole Is Nothing Then
                thisRole = Rolesddl.SelectedValue
            End If
        ElseIf Rolesddl.SelectedValue.ToString = "Select" And mainRole <> Nothing Then
            ddlBinderV2 = New DropDownListBinder(Rolesddl, "SelEmployeeRolesV2", Session("EmployeeConn"))

            ddlBinderV2.AddDDLCriteria("@RoleNum", mainRole, SqlDbType.NVarChar)
            ddlBinderV2.BindData("rolenum", "roledesc")

            Rolesddl.SelectedValue = mainRole

        End If
        ' ADD get Table here
        Dim thisdata As New CommandsSqlAndOleDb("SelApplicationExtensionByRoleNum", Session("EmployeeConn"))

        If Rolesddl.SelectedValue.ToString = "Select" And thisRole <> "" Then
            Rolesddl.SelectedValue = thisRole
        End If

        RoleNumLabel.Text = thisRole

        thisdata.AddSqlProcParameter("@roleNum", Rolesddl.SelectedValue.ToString, SqlDbType.NVarChar, 9)
        thisdata.AddSqlProcParameter("@display", "base", SqlDbType.NVarChar, 9)


        thisdata.ExecNonQueryNoReturn()

        dtApplications = thisdata.GetSqlDataTable

        Dim Appnum As Integer
        Dim appDeafult As String
        Dim snotice As String = ""
        Dim sNurseStation As String = ""

        For Each row As DataRow In dtApplications.Rows
            Appnum = row("ApplicationNum")
            appDeafult = row("DefaultValue")
            gAppName = row("ApplicationDesc")



            For Each cblItem As ListItem In cblApplications.Items
                Dim sCurritem As String = cblItem.Value

                If sCurritem = Appnum And appDeafult.ToLower = "yes" Then
                    cblItem.Selected = True


                    cblItem.Enabled = True
                End If
                If sCurritem = Appnum And appDeafult.ToLower = "no" Then
                    cblItem.Enabled = True
                    cblItem.Attributes.Add("Style", "color:blue;")



                End If
                ' change to Case look for Mak pyxis and 365portsl
                ' display nursestation panels
                ' display 365 panel
                ' If cblItem.ToString.ToLower = "mak" And cblItem.Selected = True Then
                'If cblItem.Value = "7" And cblItem.Selected = True Then
                '    sNurseStation = "true"
                '    snotice = "true"
                'End If
                If cblItem.Value = "13" And cblItem.Selected = True Then
                    sNurseStation = "true"
                End If

            Next
        Next row

        NurseStationsPan.Visible = False

        If sNurseStation = "true" Then
            NurseStationsPan.Visible = True
            LoadNursestations()

        End If


        If snotice = "true" Then
            If request_descTextBox.Text = "" Then
                request_descTextBox.Text = "Use This Mak ID ="
            Else
                request_descTextBox.Text = request_descTextBox.Text & " Please Add Mak ID "
            End If

            request_descTextBox.BackColor() = Color.Yellow
            request_descTextBox.BorderColor() = Color.Black
            request_descTextBox.BorderStyle() = BorderStyle.Solid
            request_descTextBox.BorderWidth = Unit.Pixel(2)
        End If


		'For Each cblItem As ListItem In cblApplications.Items
		'    Dim sCurritem As String = cblItem.Value
		'    cblItem.Selected = False

		'    rwRef = getApplicationsByRole.Select("ApplicationNum='" & cblItem.Value & "'")

		'    If rwRef.Count > 0 Then

		'        cblItem.Selected = True

		'    End If

		'    If cblItem.ToString.ToLower = "mak" And cblItem.Selected = True Then
		'        If request_descTextBox.Text = "" Then
		'            request_descTextBox.Text = "Use This Mak ID ="
		'        Else
		'            request_descTextBox.Text = request_descTextBox.Text & " Please Add Mak ID "
		'        End If

		'        request_descTextBox.BackColor() = Color.Yellow
		'        request_descTextBox.BorderColor() = Color.Black
		'        request_descTextBox.BorderStyle() = BorderStyle.Solid
		'        request_descTextBox.BorderWidth = Unit.Pixel(2)
		'    End If
		'Next

		'Dim BusLog As New AccountBusinessLogic

		'If BusLog.eCare Then
		'    pnlTCl.Visible = True
		'Else
		'    pnlTCl.Visible = False
		'    pageCtrl.RemoveCssClass(pnlTCl, "textInputRequired")

		'End If

		CheckECare()
        CheckforToken()
        Select Case RoleNumLabel.Text
            Case "782", "1195", "1199", "1193", "1567"


            Case Else

                If RoleNumLabel.Text <> "" Then

                    Dim tcldata As New CommandsSqlAndOleDb("SelCernerUserTypeV21", Session("EmployeeConn"))
                    tcldata.AddSqlProcParameter("@rolenum", RoleNumLabel.Text, SqlDbType.NVarChar, 8)

                    tcldata.GetSqlDataset()




                    dtTCLUserTypes = tcldata.GetSqlDataTable


                End If

                'If dtTCLUserTypes.Rows.Count = 1 Then
                '    'TCLddl.SelectedIndex = 0

                '    TCLTextBox.Text = dtTCLUserTypes.Rows(0)("TCL").ToString

                '    UserTypeDescTextBox.Text = dtTCLUserTypes.Rows(0)("UserTypeDesc").ToString
                '    UserTypeCdTextBox.Text = dtTCLUserTypes.Rows(0)("UserTypecd").ToString

                '    pnlTCl.Visible = True

                '    TCLTextBox.Visible = True
                '    UserTypeDescTextBox.Visible = True
                '    UserTypeCdTextBox.Visible = True

                '    If masterRole.Text = "1193" Then
                '        btnChangeTCL.Visible = True
                '        btnChangeTCL.Width = Unit.Pixel("200")



                '        btnChangeTCL.Text = "Show All User Types"

                '    End If


                'ElseIf dtTCLUserTypes.Rows.Count = 0 Then
                '    pnlTCl.Visible = False

                'Else
                '    gvTCL.DataSource = dtTCLUserTypes
                '    gvTCL.DataBind()

                '    gvTCL.Visible = True
                '    'gvTCL.BackColor() = Color.Yellow
                '    'gvTCL.BorderColor() = Color.Black
                '    'gvTCL.BorderStyle() = BorderStyle.Solid
                '    'gvTCL.BorderWidth = Unit.Pixel(2)
                '    'gvTCL.RowStyle.BackColor() = Color.Yellow

                '    pnlTCl.Visible = True
                '    TCLTextBox.Visible = True
                '    UserTypeDescTextBox.Visible = True
                '    UserTypeCdTextBox.Visible = True

                '    If masterRole.Text = "1193" Then
                '        btnChangeTCL.Visible = True
                '        btnChangeTCL.Text = "Show All User Types"
                '        btnChangeTCL.Width = Unit.Pixel("200")

                '    End If

                'End If


        End Select


        Select Case mainRole
            'Case "1194"
            '    Titleddl.SelectedValue = Title2TextBox.Text

            '    TitleLabel.Text = Titleddl.SelectedItem.ToString

            '    Title2TextBox.Visible = True
            '    Titleddl.Visible = False

            Case "1190", "1191", "1192"
                CheckEntitiesHosp()

                SuppSupportrbl.SelectedValue = "Yes"

                entity_cdDDL.SelectedValue = "150"

                Dim ddbDepartmentBinder As New DropDownListBinder(department_cdddl, "selDepartmentCodesV4", Session("EmployeeConn"))
                ddbDepartmentBinder.AddDDLCriteria("@entity_cd", "150", SqlDbType.NVarChar)
                ddbDepartmentBinder.BindData("department_cd", "department_name")

                department_cdddl.SelectedValue = "601000"

            Case "1554", "1560"
                'prospect
                Dim ddbVendorBinder As New DropDownListBinder(Vendorddl, "SelVendors", Session("EmployeeConn"))

                ddbVendorBinder.AddDDLCriteria("@type", "prospect", SqlDbType.NVarChar)
                ddbVendorBinder.BindData("VendorNum", "VendorName")
            Case "1724"
                'r4 Solutions 

                Dim ddbVendorBinder As New DropDownListBinder(Vendorddl, "SelVendors", Session("EmployeeConn"))

                ddbVendorBinder.AddDDLCriteria("@VendorNum", 55, SqlDbType.Int)
                ddbVendorBinder.BindData("VendorNum", "VendorName")

                Vendorddl.SelectedIndex = 1

                Dim vendornum As String = Vendorddl.SelectedValue
                Dim vendorname As String = Vendorddl.SelectedItem.ToString

                VendorFormSignOn = New FormSQL(Session("EmployeeConn"), "SelVendors", "InsUpdVendors", "", Page)
                VendorFormSignOn.AddSelectParm("@VendorNum", vendornum, SqlDbType.Int, 6)

                If vendornum <> 0 Then
                    VendorFormSignOn.FillForm()
                    address_1textbox.Text = address1TextBox.Text
                    VendorNameTextBox.Text = vendorname

                    VendorNameTextBox.Visible = False
                    VendorNumLabel.Text = vendornum.ToString()

                Else

                    VendorNameTextBox.Text = vendorname
                    VendorNumLabel.Text = vendornum.ToString()

                    VendorNameTextBox.Visible = False

                End If

                '' check all entities 
                For Each cbentities As ListItem In cblEntities.Items
                    If cbentities.Selected = False Then
                        cbentities.Selected = True
                    End If
                Next


                ' check all hospitals
                For Each cbHospitals As ListItem In cblFacilities.Items
                    Dim sCurrHospital As String = cbHospitals.ToString

                    If cbHospitals.Selected = False And sCurrHospital.ToString.ToLower <> "community" Then
                        cbHospitals.Selected = True

                    End If
                Next

            Case Else
                entity_cdDDL.SelectedIndex = -1
                department_cdddl.SelectedIndex = -1
                SuppSupportrbl.SelectedIndex = -1

        End Select

        Dim dtCernerPosition As New DataTable

        Dim positiondata As New CommandsSqlAndOleDb("SelCernerPositionDesc", Session("EmployeeConn"))

        If Rolesddl.SelectedValue.ToString = "Select" And thisRole <> "" Then
            Rolesddl.SelectedValue = thisRole
        End If

        If PositionRoleNumTextBox.Text <> "" Then
            positiondata.AddSqlProcParameter("@roleNum", PositionRoleNumTextBox.Text, SqlDbType.NVarChar, 9)
        Else
            positiondata.AddSqlProcParameter("@roleNum", Rolesddl.SelectedValue.ToString, SqlDbType.NVarChar, 9)
        End If


        dtCernerPosition = positiondata.GetSqlDataTable

        If CernerPositionDescTextBox.Text = "" Then

            CernerPositionDescTextBox.Text = dtCernerPosition.Rows(0)("CernerPositionDesc").ToString


        End If

        UserCategoryCdTextBox.Text = dtCernerPosition.Rows(0)("UserCategoryCd").ToString

        '' fill drop down
        Dim CerPosdataddl As New DropDownListBinder(CernerPosddl, "SelCernerPositionDesc", Session("EmployeeConn"))
        CerPosdataddl.AddDDLCriteria("@UserTypeCd", UserCategoryCdTextBox.Text, SqlDbType.NVarChar)

        CerPosdataddl.AddDDLCriteria("@Clientupd", "all", SqlDbType.NVarChar)

        CerPosdataddl.BindData("CernerPositionDesc", "CernerPositionDesc")


        'CernerPosddl
        'ChangColorNoCheck()
        ChangColor()


    End Sub
    Private Sub CernerPosddl_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CernerPosddl.SelectedIndexChanged

        If CernerPosddl.SelectedValue.ToString.ToLower <> "select" Then
            CernerPositionDescTextBox.Text = CernerPosddl.SelectedValue.ToString
        End If



    End Sub
    Protected Function getApplicationsByRole() As DataTable
        'Use to exec SelApplicationsByRole
        Dim thisdata As New CommandsSqlAndOleDb("SelApplicationExtensionByRoleNum", Session("EmployeeConn"))

        If Rolesddl.SelectedValue.ToString = "Select" And thisRole <> "" Then
            Rolesddl.SelectedValue = thisRole
        End If

        thisdata.AddSqlProcParameter("@roleNum", Rolesddl.SelectedValue.ToString, SqlDbType.NVarChar, 9)
        thisdata.AddSqlProcParameter("@display", "base", SqlDbType.NVarChar, 9)



        thisdata.ExecNonQueryNoReturn()

        Return thisdata.GetSqlDataTable

    End Function
    Protected Sub btnReturn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReturn.Click
        '  BusinessLogic()
        If ReactivateClientNum > 0 Then
            ' submit Cancel Rehire Request

            Dim thisdata As New CommandsSqlAndOleDb("UpdRehirRequest", Session("EmployeeConn"))
            thisdata.AddSqlProcParameter("@client_num", ReactivateClientNum, SqlDbType.Int, 9)
            thisdata.AddSqlProcParameter("@rehireRequestNum", ReHireRequestNum, SqlDbType.Int, 9)
            thisdata.AddSqlProcParameter("@rehireStatus", "canceled", SqlDbType.NVarChar, 12)



            thisdata.ExecNonQueryNoReturn()

        End If

        If AccountRequestNum > 0 Then

            Response.Redirect("Myrequest.aspx")

        Else
            Response.Redirect("SimpleSearch.aspx")

        End If


    End Sub
    Private Sub btnCloseRequest_Click(sender As Object, e As EventArgs) Handles btnCloseRequest.Click

        Dim thisData As New CommandsSqlAndOleDb("CloseRequestWithOpenItemsOrSubItemsByNum", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@accountRequestNum", PMHRequestNumTextBox.Text, SqlDbType.NVarChar, 20)
        thisData.AddSqlProcParameter("@clientNum", submitter_client_numLabel.Text, SqlDbType.NVarChar, 20)

        thisData.ExecNonQueryNoReturn()


        Response.Redirect("./EmployeeQueue.aspx", True)


    End Sub
    Protected Sub LoadEmployeeType()
        pnlDemo.Enabled = True

        pnlTCl.Visible = False
        btnChangeTCL.Visible = False
        btnChangeTCL.Text = "Change TCL"

        'NeedsProviderrbl.Visible = False
        'DoesNeedprovlbl.Visible = False


        VendorNameTextBox.Text = ""
        address_1textbox.Text = ""
        citytextbox.Text = ""
        statetextbox.Text = ""
        ziptextbox.Text = ""
        phoneTextBox.Text = ""

        Dim removeCss As New PageControls(Page)
        removeCss.RemoveCssClass(Page, "textInputRequired")




        emp_type_cdLabel.Text = emp_type_cdrbl.SelectedItem.ToString

		RoleNumLabel.Text = emp_type_cdrbl.SelectedValue
		'Dim srolenum As String
		'If masterRole.Text <> "" Then
		'    Response.Redirect("SignOnForm.aspx")
		'    Exit Sub

		'End If

		' retain Employee Master Role
		masterRole.Text = emp_type_cdrbl.SelectedValue

        'always use the Main Role
        mainRole = emp_type_cdrbl.SelectedValue

        'srolenum = emp_type_cdrbl.SelectedItem
        Dim ddlBinderV2 As DropDownListBinder

		ddlBinderV2 = New DropDownListBinder(Rolesddl, "SelEmployeeRolesV2", Session("EmployeeConn"))

		ddlBinderV2.AddDDLCriteria("@RoleNum", mainRole, SqlDbType.NVarChar)
        ddlBinderV2.BindData("rolenum", "roledesc")


        Select Case emp_type_cdrbl.SelectedValue
            'Case "physician/rovider", "non staff clinician", "resident"

            Case "782", "1197"

                empLbl.Visible = True
                AuthorizationEmpNumTextBox.Visible = True


                facility_cdddl.BackColor() = Color.White
                facility_cdddl.BorderColor() = Color.Black
                facility_cdddl.BorderStyle() = BorderStyle.Groove
                facility_cdddl.BorderWidth = Unit.Pixel(1)

                building_cdddl.BackColor() = Color.White
                building_cdddl.BorderColor() = Color.Black
                building_cdddl.BorderStyle() = BorderStyle.Groove
                building_cdddl.BorderWidth = Unit.Pixel(1)

                Dim ddbTitleBinder As New DropDownListBinder(Titleddl, "SelTitlesV4", Session("EmployeeConn"))
                ddbTitleBinder.AddDDLCriteria("@titleType", "Physician", SqlDbType.NVarChar)
                ddbTitleBinder.BindData("TitleShortDesc", "TitleDesc")

                Titleddl.SelectedIndex = -1


                Titleddl.BackColor() = Color.Yellow
                Titleddl.BorderColor() = Color.Black
                Titleddl.BorderStyle() = BorderStyle.Solid
                Titleddl.BorderWidth = Unit.Pixel(2)

                SuppSupportrbl.Enabled = False
                'If TitleLabel.Text = "" Then
                '    Titleddl.SelectedIndex = -1
                'Else
                '    Titleddl.SelectedValue = TitleLabel.Text

                'End If
                Specialtyddl.SelectedIndex = -1

                'If SpecialtLabel.Text = "" Then
                '    Specialtyddl.SelectedIndex = -1
                'Else
                '    Specialtyddl.SelectedValue = SpecialtLabel.Text

                'End If

                'Session("LoginID") = "melej" Or

                If Session("SecurityLevelNum") = 82 Or Session("LoginID").ToString.ToLower = "geip00" Or Session("LoginID").ToString.ToLower = "melej" Then
                    credentialedDateTextBox.Enabled = True
                    credentialedDateTextBox.CssClass = "calenderClass"

                    'CredentialedDateDCMHTextBox.CssClass = "calenderClass"
                    'CredentialedDateDCMHTextBox.Enabled = True

                End If



                thisRole = emp_type_cdrbl.SelectedValue
                mainRole = emp_type_cdrbl.SelectedValue

                Title2TextBox.Visible = False
                Titleddl.Visible = True

                SpecialtLabel.Visible = False
                Specialtyddl.Visible = True

                pnlProvider.Visible = True

                'credentialedDateTextBox.Enabled = True
                'credentialedDateTextBox.CssClass = "calenderClass"

                'CredentialedDateDCMHTextBox.Enabled = True
                'CredentialedDateDCMHTextBox.CssClass = "calenderClass"

                CCMCadmitRightsrbl.SelectedIndex = -1
                CCMCadmitRightsrbl.Enabled = True

                'DCMHadmitRightsrbl.SelectedIndex = -1
                'DCMHadmitRightsrbl.Enabled = True

                'Case "resident"
            Case "1194"
                'Dim ddlBinder As New DropDownListBinder
                SuppSupportrbl.Enabled = False

                empLbl.Visible = True
                AuthorizationEmpNumTextBox.Visible = True

                Dim ddbTitleBinder As New DropDownListBinder(Titleddl, "SelTitlesV4", Session("EmployeeConn"))
                ddbTitleBinder.AddDDLCriteria("@titleType", "Resident", SqlDbType.NVarChar)
                ddbTitleBinder.BindData("TitleShortDesc", "TitleDesc")

				Dim ddlRoleBinder As New DropDownListBinder(Rolesddl, "SelEmployeeRolesV2", Session("EmployeeConn"))
				ddlRoleBinder.AddDDLCriteria("@RoleNum", "1194", SqlDbType.NVarChar)
                ddlRoleBinder.BindData("rolenum", "RoleDesc")

                'If TitleLabel.Text = "" Then
                '    Titleddl.SelectedIndex = -1
                'Else
                '    Titleddl.SelectedValue = TitleLabel.Text

                'End If


                'If TitleLabel.Text = "" Then

                '    Title2TextBox.Text = "Resident"
                '    Title2TextBox.Visible = True


                'End If

                TitleLabel.Text = "Resident"

                Title2TextBox.Text = "Resident"
                Title2TextBox.Visible = True
                Titleddl.Visible = False

                SpecialtLabel.Text = "Resident"
                SpecialtLabel.Visible = True
                Specialtyddl.Visible = False

                SuppSupportrbl.Enabled = False


                'Dim ddlSpecialityBinder As New DropDownListBinder(Specialtyddl, "SelSpecialtyCodesV2", Session("EmployeeConn"))
                'ddlSpecialityBinder.AddDDLCriteria("@SpecialtyType", "Resident", SqlDbType.NVarChar)
                'ddlSpecialityBinder.BindData("specialty", "specialty")

                'If SpecialtLabel.Text = "" Then

                '    Specialtyddl.SelectedValue = "Resident"
                'Else
                '    Specialtyddl.SelectedValue = SpecialtLabel.Text

                'End If

                ' in titleddl will be rolenum for residents which will be the new thisrole
                'ddlBinder.BindData(Rolesddl, "SelEmployeeRolesV2", "rolenum", "RoleDesc", Session("EmployeeConn"))

                credentialedDateTextBox.Enabled = False
                credentialedDateTextBox.CssClass = "style2"

                'CredentialedDateDCMHTextBox.Enabled = False
                'CredentialedDateDCMHTextBox.CssClass = "style2"

                CCMCadmitRightsrbl.SelectedValue = "No"
                CCMCadmitRightsrbl.Enabled = False

                'DCMHadmitRightsrbl.SelectedValue = "No"
                'DCMHadmitRightsrbl.Enabled = False

                thisRole = emp_type_cdrbl.SelectedValue
                mainRole = emp_type_cdrbl.SelectedValue

                Rolesddl.SelectedIndex = -1

                'SelectedValue.ToString = "Select"

                pnlProvider.Visible = True


            Case "1195", "1198"
                ' load vendor list 
                'make vendor text box invisiable
                ' make ddl appear
                ' 1195 Contractor list
                ' 1198 Vendor list

                empLbl.Visible = False
                AuthorizationEmpNumTextBox.Visible = False


                SuppSupportrbl.Enabled = False

                Vendorddl.Visible = True
                lblVendor.Visible = True

                'Vendorddl.BackColor() = Color.Yellow
                'Vendorddl.BorderColor() = Color.Black
                'Vendorddl.BorderStyle() = BorderStyle.Solid
                'Vendorddl.BorderWidth = Unit.Pixel(2)


                VendorNameTextBox.Visible = False

                Dim ddbVendorBinder As New DropDownListBinder(Vendorddl, "SelVendors", Session("EmployeeConn"))

                If mainRole = "1554" Then
                    ddbVendorBinder.AddDDLCriteria("@type", "prospect", SqlDbType.NVarChar)

                Else
                    ddbVendorBinder.AddDDLCriteria("@type", "vendor", SqlDbType.NVarChar)


                End If
                ddbVendorBinder.BindData("VendorNum", "VendorName")
                thisRole = emp_type_cdrbl.SelectedValue
                mainRole = emp_type_cdrbl.SelectedValue

                'If emp_type_cdrbl.SelectedValue = "1198" Then
                '    ddbVendorBinder.AddDDLCriteria("@type", "vendor", SqlDbType.NVarChar)
                '    'lblVendor.Text = "Vendor Name:"

                'ElseIf emp_type_cdrbl.SelectedValue = "1195" Then

                '    ddbVendorBinder.AddDDLCriteria("@type", "contractor", SqlDbType.NVarChar)
                '    'lblVendor.Text = "Contractor Name:"
                'Else

                'End If

            Case "1193", "1200"
                'NeedsProviderrbl.Visible = True
                'DoesNeedprovlbl.Visible = True

                Title2TextBox.Visible = False
                Titleddl.Visible = True

                SpecialtLabel.Visible = False
                Specialtyddl.Visible = True

                pnlProvider.Visible = False

                thisRole = emp_type_cdrbl.SelectedValue
                mainRole = emp_type_cdrbl.SelectedValue

                empLbl.Visible = True
                AuthorizationEmpNumTextBox.Visible = True

            Case "1196", "1199"

                empLbl.Visible = False
                AuthorizationEmpNumTextBox.Visible = False
                SuppSupportrbl.Enabled = False


            Case Else
                Title2TextBox.Visible = False
                Titleddl.Visible = True

                SpecialtLabel.Visible = False
                Specialtyddl.Visible = True

                pnlProvider.Visible = False

                thisRole = emp_type_cdrbl.SelectedValue
                mainRole = emp_type_cdrbl.SelectedValue

                SuppSupportrbl.Enabled = False


        End Select





        CheckRequiredFields()

        If bHasError = True Then
            lblValidation.Focus()
            Exit Sub
        Else
            lblValidation.Text = ""
        End If


        'Rolesddl.SelectedValue = thisRole

        'dont select role if Resident
        If thisRole <> "1194" Then
            ' this kicks off selecting applications that will be selected for that role
            Rolesddl_SelectedIndexChanged(thisRole, EventArgs.Empty)
        End If


    End Sub

    Protected Sub LoadNursestations()

        For Each cblcItem As ListItem In cblCCMCNursestations.Items
            cblcItem.Selected = False
            'cblItem.Enabled = False
        Next

        For Each cblDItem As ListItem In cblDCMHNursestataions.Items
            cblDItem.Selected = False
            'cblItem.Enabled = False
        Next

        For Each cblTItem As ListItem In cblTaylorNursestataions.Items
            cblTItem.Selected = False
            'cblItem.Enabled = False
        Next

        For Each cblSItem As ListItem In cblSpringfieldNursestataions.Items
            cblSItem.Selected = False
            'cblItem.Enabled = False
        Next

        Dim cblCCMCNurBinder As New CheckBoxListBinder(cblCCMCNursestations, "SelNursestationByFacility", Session("EmployeeConn"))
        cblCCMCNurBinder.AddDDLCriteria("@facilityCd", "15", SqlDbType.NVarChar)
        cblCCMCNurBinder.BindData("Nursestationcd", "NurseStationDesc")

        Dim cblDCMHNurBinder As New CheckBoxListBinder(cblDCMHNursestataions, "SelNursestationByFacility", Session("EmployeeConn"))
        cblDCMHNurBinder.AddDDLCriteria("@facilityCd", "12", SqlDbType.NVarChar)
        cblDCMHNurBinder.BindData("Nursestationcd", "NurseStationDesc")

        Dim cblTaylorNurBinder As New CheckBoxListBinder(cblTaylorNursestataions, "SelNursestationByFacility", Session("EmployeeConn"))
        cblTaylorNurBinder.AddDDLCriteria("@facilityCd", "19", SqlDbType.NVarChar)
        cblTaylorNurBinder.BindData("Nursestationcd", "NurseStationDesc")

        Dim cblSpringNurBinder As New CheckBoxListBinder(cblSpringfieldNursestataions, "SelNursestationByFacility", Session("EmployeeConn"))
        cblSpringNurBinder.AddDDLCriteria("@facilityCd", "16", SqlDbType.NVarChar)
        cblSpringNurBinder.BindData("Nursestationcd", "NurseStationDesc")


    End Sub
    'Protected Sub Load365Locations()

    '    For Each cbl365Item As ListItem In cblo365.Items
    '        cbl365Item.Selected = False
    '        'cblItem.Enabled = False
    '    Next


    '    Dim cblo365Binder As New CheckBoxListBinder(cblo365, "Selo365Groups", Session("EmployeeConn"))
    '    cblo365Binder.BindData("SubApplicationNum", "SubAppDesc")

    '    'Checkfor365()
    'End Sub
    Protected Sub cblCCMCAll_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cblCCMCAll.SelectedIndexChanged

        If cblCCMCAll.SelectedValue = "ccmcall" Then

            For Each cblcItem As ListItem In cblCCMCNursestations.Items
                cblcItem.Selected = True
                'cblItem.Enabled = False
            Next

        Else
            For Each cblcItem As ListItem In cblCCMCNursestations.Items
                cblcItem.Selected = False
                'cblItem.Enabled = False
            Next

        End If
    End Sub
    Protected Sub cblDCMHAll_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cblDCMHAll.SelectedIndexChanged

        If cblDCMHAll.SelectedValue = "dcmhall" Then

            For Each cblcItem As ListItem In cblDCMHNursestataions.Items
                cblcItem.Selected = True
                'cblItem.Enabled = False
            Next

        Else
            For Each cblcItem As ListItem In cblDCMHNursestataions.Items
                cblcItem.Selected = False
                'cblItem.Enabled = False
            Next

        End If
    End Sub

    Protected Sub cblTAll_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cblTAll.SelectedIndexChanged

        If cblTAll.SelectedValue = "taylorall" Then

            For Each cblcItem As ListItem In cblTaylorNursestataions.Items
                cblcItem.Selected = True
                'cblItem.Enabled = False
            Next

        Else
            For Each cblcItem As ListItem In cblTaylorNursestataions.Items
                cblcItem.Selected = False
                'cblItem.Enabled = False
            Next

        End If
    End Sub

    Protected Sub cblSAll_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cblSAll.SelectedIndexChanged

        If cblSAll.SelectedValue = "springall" Then

            For Each cblcItem As ListItem In cblSpringfieldNursestataions.Items
                cblcItem.Selected = True
                'cblItem.Enabled = False
            Next

        Else
            For Each cblcItem As ListItem In cblSpringfieldNursestataions.Items
                cblcItem.Selected = False
                'cblItem.Enabled = False
            Next

        End If
    End Sub

    Protected Sub emp_type_cdrbl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles emp_type_cdrbl.SelectedIndexChanged
        pnlDemo.Enabled = True

        For Each cblItem As ListItem In cblApplications.Items
            cblItem.Selected = False
        Next
        For Each cblEntitem As ListItem In cblEntities.Items
            cblEntitem.Selected = False
        Next

        For Each cblFacItem As ListItem In cblFacilities.Items
            cblFacItem.Selected = False
        Next

        pnlTCl.Visible = False
        btnChangeTCL.Visible = False
        btnChangeTCL.Text = "Change TCL"

        'NeedsProviderrbl.Visible = False
        'DoesNeedprovlbl.Visible = False


        VendorNameTextBox.Text = ""
        address_1textbox.Text = ""
        citytextbox.Text = ""
        statetextbox.Text = ""
        ziptextbox.Text = ""
        phoneTextBox.Text = ""

        Dim removeCss As New PageControls(Page)
        removeCss.RemoveCssClass(Page, "textInputRequired")


        emp_type_cdLabel.Text = emp_type_cdrbl.SelectedItem.ToString
        RoleNumLabel.Text = emp_type_cdrbl.SelectedValue
        'Dim srolenum As String
        If masterRole.Text <> "" Then
            Response.Redirect("SignOnForm.aspx")
            Exit Sub

        End If

        ' retain Employee Master Role
        masterRole.Text = emp_type_cdrbl.SelectedValue

        'always use the Main Role
        mainRole = emp_type_cdrbl.SelectedValue

        'srolenum = emp_type_cdrbl.SelectedItem
        Dim ddlBinderV2 As DropDownListBinder

		ddlBinderV2 = New DropDownListBinder(Rolesddl, "SelEmployeeRolesV2", Session("EmployeeConn"))

		ddlBinderV2.AddDDLCriteria("@RoleNum", mainRole, SqlDbType.NVarChar)

        ddlBinderV2.BindData("rolenum", "roledesc")

        user_position_descTextBox.Text = emp_type_cdrbl.SelectedValue

        Select Case emp_type_cdrbl.SelectedValue
            'Case "physician/rovider", "non staff clinician", "resident"

            Case "782", "1197"

                facility_cdddl.BackColor() = Color.White
                facility_cdddl.BorderColor() = Color.Black
                facility_cdddl.BorderStyle() = BorderStyle.Groove
                facility_cdddl.BorderWidth = Unit.Pixel(1)

                building_cdddl.BackColor() = Color.White
                building_cdddl.BorderColor() = Color.Black
                building_cdddl.BorderStyle() = BorderStyle.Groove
                building_cdddl.BorderWidth = Unit.Pixel(1)

                Dim ddbTitleBinder As New DropDownListBinder(Titleddl, "SelTitlesV4", Session("EmployeeConn"))
                ddbTitleBinder.AddDDLCriteria("@titleType", "Physician", SqlDbType.NVarChar)
                ddbTitleBinder.BindData("TitleShortDesc", "TitleDesc")

                Titleddl.SelectedIndex = -1


                Titleddl.BackColor() = Color.Yellow
                Titleddl.BorderColor() = Color.Black
                Titleddl.BorderStyle() = BorderStyle.Solid
                Titleddl.BorderWidth = Unit.Pixel(2)

                hanrbl.BackColor() = Color.Yellow
                hanrbl.BorderColor() = Color.Black
                hanrbl.BorderStyle() = BorderStyle.Solid
                hanrbl.BorderWidth = Unit.Pixel(3)

                empLbl.Visible = True
                AuthorizationEmpNumTextBox.Visible = True


                SuppSuppLbl.Visible = False
                SuppSupportrbl.Visible = False
                SuppSupportrbl.Enabled = False
                Specialtyddl.SelectedIndex = -1

                DoesNeedprovlbl.Visible = False
                NeedsProviderrbl.Visible = False


				If Session("SecurityLevelNum") = 82 Or Session("LoginID").ToString.ToLower = "geip00" Or Session("LoginID").ToString.ToLower = "melej" Then
					credentialedDateTextBox.Enabled = True
					credentialedDateTextBox.CssClass = "calenderClass"

                    'CredentialedDateDCMHTextBox.CssClass = "calenderClass"
                    'CredentialedDateDCMHTextBox.Enabled = True

                End If



				thisRole = emp_type_cdrbl.SelectedValue
                mainRole = emp_type_cdrbl.SelectedValue

                Title2TextBox.Visible = False
                Titleddl.Visible = True

                SpecialtLabel.Visible = False
                Specialtyddl.Visible = True

                pnlProvider.Visible = True

                pnlTokenAddress.Visible = True

                TokenPhoneTextBox.BackColor() = Color.Yellow
                TokenPhoneTextBox.BorderColor() = Color.Black
                TokenPhoneTextBox.BorderStyle() = BorderStyle.Solid
                TokenPhoneTextBox.BorderWidth = Unit.Pixel(3)


                'credentialedDateTextBox.Enabled = True
                'credentialedDateTextBox.CssClass = "calenderClass"

                'CredentialedDateDCMHTextBox.Enabled = True
                'CredentialedDateDCMHTextBox.CssClass = "calenderClass"

                CCMCadmitRightsrbl.SelectedIndex = -1
                CCMCadmitRightsrbl.Enabled = True

                'DCMHadmitRightsrbl.SelectedIndex = -1
                'DCMHadmitRightsrbl.Enabled = True

                'Case "resident"
            Case "1194"
                'Dim ddlBinder As New DropDownListBinder
                SuppSupportrbl.Enabled = False

                empLbl.Visible = True
                AuthorizationEmpNumTextBox.Visible = True

                Dim ddbTitleBinder As New DropDownListBinder(Titleddl, "SelTitlesV4", Session("EmployeeConn"))
                ddbTitleBinder.AddDDLCriteria("@titleType", "Resident", SqlDbType.NVarChar)
                ddbTitleBinder.BindData("TitleShortDesc", "TitleDesc")

				Dim ddlRoleBinder As New DropDownListBinder(Rolesddl, "SelEmployeeRolesV2", Session("EmployeeConn"))
				ddlRoleBinder.AddDDLCriteria("@RoleNum", "1194", SqlDbType.NVarChar)
                ddlRoleBinder.BindData("rolenum", "RoleDesc")


                TitleLabel.Text = "Resident"

                Title2TextBox.Text = "Resident"
                Title2TextBox.Visible = True
                Titleddl.Visible = False

                SpecialtLabel.Text = "Resident"
                SpecialtLabel.Visible = True
                Specialtyddl.Visible = False

                SuppSupportrbl.Enabled = False

                credentialedDateTextBox.Enabled = False
                credentialedDateTextBox.CssClass = "style2"

                'CredentialedDateDCMHTextBox.Enabled = False
                'CredentialedDateDCMHTextBox.CssClass = "style2"

                CCMCadmitRightsrbl.SelectedValue = "No"
                CCMCadmitRightsrbl.Enabled = False

                'DCMHadmitRightsrbl.SelectedValue = "No"
                'DCMHadmitRightsrbl.Enabled = False

                thisRole = emp_type_cdrbl.SelectedValue
                mainRole = emp_type_cdrbl.SelectedValue

                Rolesddl.SelectedIndex = -1

                'SelectedValue.ToString = "Select"

                pnlProvider.Visible = True

            Case "1195", "1198"
                ' load vendor list 
                'make vendor text box invisiable

                e_mailTextBox.Enabled = True
                empLbl.Visible = False
                AuthorizationEmpNumTextBox.Visible = False


                SuppSupportrbl.Enabled = False

                Vendorddl.Visible = True
                lblVendor.Visible = True

                VendorNameTextBox.Visible = False

                Dim ddbVendorBinder As New DropDownListBinder(Vendorddl, "SelVendors", Session("EmployeeConn"))

                If mainRole = "1554" Then
                    ddbVendorBinder.AddDDLCriteria("@type", "prospect", SqlDbType.NVarChar)

                Else
                    ddbVendorBinder.AddDDLCriteria("@type", "vendor", SqlDbType.NVarChar)


                End If
                ddbVendorBinder.BindData("VendorNum", "VendorName")

                thisRole = emp_type_cdrbl.SelectedValue
                mainRole = emp_type_cdrbl.SelectedValue



            Case "1193", "1200"
                'NeedsProviderrbl.Visible = True
                'DoesNeedprovlbl.Visible = True

                Title2TextBox.Visible = False
                Titleddl.Visible = True

                SpecialtLabel.Visible = False
                Specialtyddl.Visible = True

                pnlProvider.Visible = False

                thisRole = emp_type_cdrbl.SelectedValue
                mainRole = emp_type_cdrbl.SelectedValue

                empLbl.Visible = True
                AuthorizationEmpNumTextBox.Visible = True

            Case "1196", "1199"

                empLbl.Visible = False
                AuthorizationEmpNumTextBox.Visible = False
                SuppSupportrbl.Enabled = False


            Case Else
                Title2TextBox.Visible = False
                Titleddl.Visible = True

                SpecialtLabel.Visible = False
                Specialtyddl.Visible = True

                pnlProvider.Visible = False

                thisRole = emp_type_cdrbl.SelectedValue
                mainRole = emp_type_cdrbl.SelectedValue

                SuppSupportrbl.Enabled = False


        End Select

        ChangColor()

        'ChangColorNoCheck()




        CheckRequiredFields()

        If bHasError = True Then
            lblValidation.Focus()
            Exit Sub
        Else
            lblValidation.Text = ""
        End If


        'Rolesddl.SelectedValue = thisRole

        'dont select role if Resident
        'If thisRole <> "1194" Then
        '    ' this kicks off selecting applications that will be selected for that role
        '    Rolesddl_SelectedIndexChanged(thisRole, EventArgs.Empty)
        'End If


    End Sub


    Protected Sub CCMCadmitRightsrbl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles CCMCadmitRightsrbl.SelectedIndexChanged

        If CCMCadmitRightsrbl.SelectedValue.ToLower = "no" Then

            'CCMCConsultsrbl.SelectedValue = "No"
            'CCMCConsultsrbl.Enabled = False

            'DCMHConsultsrbl.SelectedValue = "No"
            'DCMHConsultsrbl.Enabled = False

            'TaylorConsultsrbl.SelectedValue = "No"
            'TaylorConsultsrbl.Enabled = False

            'SpringfieldConsultsrbl.SelectedValue = "No"
            'SpringfieldConsultsrbl.Enabled = False

            'Writeordersrbl.SelectedValue = "No"
            'Writeordersrbl.Enabled = False

            'WriteordersDCMHrbl.SelectedValue = "No"
            'WriteordersDCMHrbl.Enabled = False

            credentialedDateTextBox.Enabled = False
            credentialedDateTextBox.CssClass = "style2"

            'CredentialedDateDCMHTextBox.Enabled = False
            'CredentialedDateDCMHTextBox.CssClass = "style2"


        End If

        If CCMCadmitRightsrbl.SelectedValue.ToLower = "no" Then
            credentialedDateTextBox.Enabled = False
            credentialedDateTextBox.CssClass = "style2"

        End If

        If CCMCadmitRightsrbl.SelectedValue.ToLower = "yes" Then
			If Session("SecurityLevelNum") = 82 Or Session("LoginID").ToString.ToLower = "melej" Or Session("LoginID").ToString.ToLower = "geip00" Then
				credentialedDateTextBox.Enabled = True
				credentialedDateTextBox.CssClass = "calenderClass"

			End If


		End If


        If thisRole = "1194" Then ' Resident
            credentialedDateTextBox.Enabled = False
            credentialedDateTextBox.CssClass = "style2"

            'CredentialedDateDCMHTextBox.Enabled = False
            'CredentialedDateDCMHTextBox.CssClass = "style2"

            CCMCadmitRightsrbl.SelectedValue = "No"
            CCMCadmitRightsrbl.Enabled = False

            'DCMHadmitRightsrbl.SelectedValue = "No"
            'DCMHadmitRightsrbl.Enabled = False
        End If
        ChangColorNoCheck()

        'ChangColor()


    End Sub

    'Protected Sub DCMHadmitRightsrbl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles DCMHadmitRightsrbl.SelectedIndexChanged
    '    If CCMCadmitRightsrbl.SelectedValue.ToLower = "no" And DCMHadmitRightsrbl.SelectedValue.ToLower = "no" Then

    '        'CCMCConsultsrbl.SelectedValue = "No"
    '        'CCMCConsultsrbl.Enabled = False

    '        'DCMHConsultsrbl.SelectedValue = "No"
    '        'DCMHConsultsrbl.Enabled = False

    '        'TaylorConsultsrbl.SelectedValue = "No"
    '        'TaylorConsultsrbl.Enabled = False

    '        'SpringfieldConsultsrbl.SelectedValue = "No"
    '        'SpringfieldConsultsrbl.Enabled = False

    '        'Writeordersrbl.SelectedValue = "No"
    '        'Writeordersrbl.Enabled = False

    '        'WriteordersDCMHrbl.SelectedValue = "No"
    '        'WriteordersDCMHrbl.Enabled = False

    '        credentialedDateTextBox.Enabled = False
    '        credentialedDateTextBox.CssClass = "style2"

    '        CredentialedDateDCMHTextBox.Enabled = False
    '        CredentialedDateDCMHTextBox.CssClass = "style2"


    '    End If

    '    If DCMHadmitRightsrbl.SelectedValue.ToLower = "no" Then
    '        CredentialedDateDCMHTextBox.Enabled = False
    '        CredentialedDateDCMHTextBox.CssClass = "style2"

    '    End If

    '    If DCMHadmitRightsrbl.SelectedValue.ToLower = "yes" Then
    '        If Session("SecurityLevelNum") = 82 Or Session("LoginID").ToString.ToLower = "melej" Or Session("LoginID").ToString.ToLower = "geip00" Or Session("LoginID").ToString.ToLower = "higginsd" Then

    '            CredentialedDateDCMHTextBox.Enabled = True
    '            CredentialedDateDCMHTextBox.CssClass = "calenderClass"

    '        End If


    '    End If

    '    'If CCMCadmitRightsrbl.SelectedValue.ToLower = "yes" Or DCMHadmitRightsrbl.SelectedValue.ToLower = "yes" Then

    '    '    CCMCConsultsrbl.SelectedIndex = -1
    '    '    CCMCConsultsrbl.Enabled = True

    '    '    DCMHConsultsrbl.SelectedIndex = -1
    '    '    DCMHConsultsrbl.Enabled = True

    '    '    TaylorConsultsrbl.SelectedIndex = -1
    '    '    TaylorConsultsrbl.Enabled = True

    '    '    SpringfieldConsultsrbl.SelectedIndex = -1
    '    '    SpringfieldConsultsrbl.Enabled = True

    '    '    Writeordersrbl.SelectedIndex = -1
    '    '    Writeordersrbl.Enabled = True

    '    '    WriteordersDCMHrbl.SelectedIndex = -1
    '    '    WriteordersDCMHrbl.Enabled = True

    '    '    If Session("SecurityLevelNum") = 82 Or Session("LoginID") = "melej" Then
    '    '        credentialedDateTextBox.Enabled = True
    '    '        credentialedDateTextBox.CssClass = "calenderClass"

    '    '        CredentialedDateDCMHTextBox.Enabled = True
    '    '        CredentialedDateDCMHTextBox.CssClass = "calenderClass"

    '    '        CCMCadmitRightsrbl.SelectedIndex = -1
    '    '        CCMCadmitRightsrbl.Enabled = True

    '    '        DCMHadmitRightsrbl.SelectedIndex = -1
    '    '        DCMHadmitRightsrbl.Enabled = True


    '    '    End If

    '    'End If

    '    If thisRole = "1194" Then
    '        credentialedDateTextBox.Enabled = False
    '        credentialedDateTextBox.CssClass = "style2"

    '        CredentialedDateDCMHTextBox.Enabled = False
    '        CredentialedDateDCMHTextBox.CssClass = "style2"

    '        CCMCadmitRightsrbl.SelectedValue = "No"
    '        CCMCadmitRightsrbl.Enabled = False

    '        DCMHadmitRightsrbl.SelectedValue = "No"
    '        DCMHadmitRightsrbl.Enabled = False
    '    End If
    '    ChangColorNoCheck()
    '    'ChangColor()

    'End Sub

    Protected Sub Vendorddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles Vendorddl.SelectedIndexChanged

        Dim vendornum As String = Vendorddl.SelectedValue
        Dim vendorname As String = Vendorddl.SelectedItem.ToString

        VendorFormSignOn = New FormSQL(Session("EmployeeConn"), "SelVendors", "InsUpdVendors", "", Page)
        VendorFormSignOn.AddSelectParm("@VendorNum", vendornum, SqlDbType.Int, 6)

        If vendornum <> 0 Then
            VendorFormSignOn.FillForm()
            address_1textbox.Text = address1TextBox.Text
            VendorNameTextBox.Text = vendorname

            VendorNameTextBox.Visible = False
            VendorNumLabel.Text = vendornum.ToString()

        Else

            VendorNameTextBox.Text = vendorname
            VendorNumLabel.Text = vendornum.ToString()

            VendorNameTextBox.Visible = False

        End If

        ' R4 vendor check

        ChangColorNoCheck()

        'ChangColor()
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click

        Dim thisData As New CommandsSqlAndOleDb("SelActiveCKHSEmployees", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@FirstName", FirstNameTextBox.Text, SqlDbType.NVarChar, 20)
        thisData.AddSqlProcParameter("@LastName", LastNameTextBox.Text, SqlDbType.NVarChar, 20)

        gvSearch.DataSource = thisData.GetSqlDataTable
        gvSearch.DataBind()

        uplOnBehalfOf.Update()
        mpeOnBehalfOf.Show()

    End Sub
    Protected Sub gvSearch_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvSearch.RowCommand

        If e.CommandName = "Select" Then
            Dim client_num As String = gvSearch.DataKeys(Convert.ToInt32(e.CommandArgument))("client_num").ToString()
            Dim login_name As String = gvSearch.DataKeys(Convert.ToInt32(e.CommandArgument))("login_name").ToString()
            Dim dbAccess As New CkhsEmployeeDbAccess(Session("EmployeeConn"))
            Dim ckhsEmployee = dbAccess.GetCkhsEmployee(client_num)

            'c3Controller = New Client3Controller(client_num)
            'c3Submitter = c3Controller.GetClientByClientNum(client_num)


            SubmittingOnBehalfOfNameLabel.Text = ckhsEmployee.FullName & " (" & ckhsEmployee.EMail & ")"
            requestor_client_numLabel.Text = client_num
            'btnUpdateSubmitter.Visible = True


            gvSearch.SelectedIndex = -1
            gvSearch.Dispose()

            Select Case userDepartmentCD.Text
                Case "832600", "832700", "823100"
                    If Session("SecurityLevelNum") < 90 And requestor_client_numLabel.Text = submitter_client_numLabel.Text Then
                        btnSelectOnBehalfOf.Visible = True
                        lblValidation.Text = "Must Change Requestor"
                        lblValidation.Focus()
                    ElseIf Session("SecurityLevelNum") < 90 And requestor_client_numLabel.Text <> submitter_client_numLabel.Text Then
                        lblValidation.Text = ""
                        btnSelectOnBehalfOf.BackColor() = Color.FromKnownColor("336666")
                        btnSelectOnBehalfOf.ForeColor() = Color.White
                        btnSelectOnBehalfOf.BorderStyle() = BorderStyle.Groove
                        btnSelectOnBehalfOf.BorderWidth = Unit.Pixel(1)



                    End If


            End Select


            'mpeOnBehalfOf.Show()


        End If
    End Sub
    Protected Function checkFacilities() As Boolean
        Dim blnReturn = False

        For Each item As ListItem In cblFacilities.Items
            If item.Selected Then
                blnReturn = True

            End If


        Next


        Return blnReturn

    End Function
    Protected Function checkApplications() As Boolean
        Dim blnReturn = False

        For Each item As ListItem In cblApplications.Items
            If item.Selected Then
                blnReturn = True

            End If


        Next


        Return blnReturn



    End Function
    Protected Function checkEntities() As Boolean
        Dim blnReturn As Boolean = False

        For Each item As ListItem In cblEntities.Items
            If item.Selected Then

                blnReturn = True
            End If
        Next
        Return blnReturn
    End Function
    Protected Sub btnClearAccounts_Click(sender As Object, e As System.EventArgs) Handles btnClearAccounts.Click
        For Each item As ListItem In cblApplications.Items
            item.Selected = False

        Next
    End Sub
	Public Sub CheckBoxValueSelector(ByRef cbl As CheckBoxList, ByVal val As String)

		Dim BusLog As New AccountBusinessLogic

		For Each item As ListItem In cbl.Items

			If item.Value = val Then

				item.Selected = True

			End If

		Next

	End Sub
	Protected Sub getTCLforRole()
        Dim tcldata As New CommandsSqlAndOleDb("SelInvisionUserTypeV20", CType(Session("EmployeeConn"), String))
        tcldata.AddSqlProcParameter("@rolenum", emp_type_cdrbl.SelectedValue, SqlDbType.NVarChar, 8)
		tcldata.AddSqlProcParameter("@UserTypeCd", Titleddl.SelectedValue, SqlDbType.NVarChar, 25)

        tcldata.GetSqlDataset()

        dtTCLUserTypes = tcldata.GetSqlDataTable
		gvTCL.DataSource = dtTCLUserTypes
		gvTCL.DataBind()




        If dtTCLUserTypes.Rows.Count = 1 Then
            'TCLddl.SelectedIndex = 0

            TCLTextBox.Text = dtTCLUserTypes.Rows(0)("TCL").ToString
            UserTypeDescTextBox.Text = dtTCLUserTypes.Rows(0)("UserTypeDesc").ToString
            UserTypeCdTextBox.Text = dtTCLUserTypes.Rows(0)("UserTypecd").ToString

            pnlTCl.Visible = True
            TCLTextBox.Visible = True
            UserTypeDescTextBox.Visible = True
            UserTypeCdTextBox.Visible = True

            'TCLddl.Visible = False

            gvTCL.Visible = False

        ElseIf dtTCLUserTypes.Rows.Count = 0 Then
            pnlTCl.Visible = False

        Else
            pnlTCl.Visible = True
            TCLTextBox.Visible = True
            UserTypeDescTextBox.Visible = True
            UserTypeCdTextBox.Visible = True


            'TCLddl.Visible = True

            'TCLddl.SelectedIndex = -1

            'TCLddl.BackColor() = Color.Yellow
            'TCLddl.BorderColor() = Color.Black
            'TCLddl.BorderStyle() = BorderStyle.Solid
            'TCLddl.BorderWidth = Unit.Pixel(2)

            gvTCL.Visible = True
            'gvTCL.BackColor() = Color.Yellow
            'gvTCL.BorderColor() = Color.Black
            'gvTCL.BorderStyle() = BorderStyle.Solid
            'gvTCL.BorderWidth = Unit.Pixel(2)
            'gvTCL.RowStyle.BackColor() = Color.Yellow

        End If

        ' fill drop down
        Dim CerPosdataddl As New DropDownListBinder(CernerPosddl, "SelCernerPositionDesc", Session("EmployeeConn"))
        CerPosdataddl.AddDDLCriteria("@rolenum", emp_type_cdrbl.SelectedValue, SqlDbType.NVarChar)
        CerPosdataddl.AddDDLCriteria("@Clientupd", "all", SqlDbType.NVarChar)

        CerPosdataddl.BindData("CernerPositionDesc", "CernerPositionDesc")


        ' fill specific Cerner for this title
        Dim dtCernerPosition As New DataTable

        Dim positiondata As New CommandsSqlAndOleDb("SelCernerPositionDesc", Session("EmployeeConn"))

        If Rolesddl.SelectedValue.ToString = "Select" And thisRole <> "" Then
            Rolesddl.SelectedValue = thisRole
        End If

        If PositionRoleNumTextBox.Text <> "" Then
            positiondata.AddSqlProcParameter("@roleNum", PositionRoleNumTextBox.Text, SqlDbType.NVarChar, 9)
        ElseIf Rolesddl.SelectedValue.ToString.ToLower <> "select" Then
            positiondata.AddSqlProcParameter("@roleNum", Rolesddl.SelectedValue.ToString, SqlDbType.NVarChar, 9)
        Else
            positiondata.AddSqlProcParameter("@roleNum", emp_type_cdrbl.SelectedValue.ToString, SqlDbType.NVarChar, 9)
        End If

        If TitleLabel.Text <> "" Then
            positiondata.AddSqlProcParameter("@UserTypeCd", TitleLabel.Text, SqlDbType.NVarChar, 25)

        ElseIf UserTypeCdTextBox.Text <> "" Then
            positiondata.AddSqlProcParameter("@UserTypeCd", UserTypeCdTextBox.Text, SqlDbType.NVarChar, 25)

        End If

        dtCernerPosition = positiondata.GetSqlDataTable

        If dtCernerPosition.Rows.Count > 0 Then
            CernerPositionDescTextBox.Text = dtCernerPosition.Rows(0)("CernerPositionDesc").ToString
        End If


        ' UserCategoryCdTextBox.Text = dtCernerPosition.Rows(0)("UserCategoryCd").ToString




    End Sub

	Protected Sub entity_cdDDL_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles entity_cdDDL.SelectedIndexChanged
        Dim ddbDepartmentBinder As New DropDownListBinder(department_cdddl, "selDepartmentCodesV4", Session("EmployeeConn"))
        ddbDepartmentBinder.AddDDLCriteria("@entity_cd", entity_cdDDL.SelectedValue, SqlDbType.NVarChar)
        ddbDepartmentBinder.BindData("department_cd", "department_name")
        ChangColor()

    End Sub
    Protected Sub SendProviderEmail(ByVal AccountRequestNum As Integer)
        '======================
        ' Get current path
        '======================

        Dim path As String
        Dim directory As String = ""
        Dim toList As String()

        path = HttpContext.Current.Request.Url.AbsoluteUri
        Dim pathParts() As String = Split(path, "/")
        Dim i As Integer

        Dim EmailList As String


        Do While i < pathParts.Length() - 1

            directory = directory + pathParts(i) + "/"
            i = i + 1

        Loop

        Mailmsg.To.Clear()


        'Dim toEmail As String = EmailAddress.Email
        Dim sSubject As String
        Dim sBody As String
        Dim fulleMailList As String

        sSubject = "Account Request for " & first_nameTextBox.Text & " " & last_nameTextBox.Text & " has been added request to the CSC Provisioning Application."


        sBody = "<!DOCTYPE html>" & _
                            "<html>" & _
                            "<head>" & _
                            "<style>" & _
                            "table" & _
                            "{" & _
                            "border-collapse:collapse;" & _
                            "font-family: 'Helvetica Neue', 'Lucida Grande', 'Segoe UI', Arial, Helvetica, Verdana, sans-serif;" & _
                            "}" & _
                            "table, th, td" & _
                            "{" & _
                            "border: 1px solid black;" & _
                            "padding: 3px" & _
                            "}" & _
                            "</style>" & _
                            "</head>" & _
                            "" & _
                            "<body>" & _
                            "A new Provisioning Account request has been submitted.  Follow the link below to go to view details of the request." & _
                             "<br />" & _
                            "<a href=""" & directory & "/AccountRequestInfo2.aspx?requestnum=" & AccountRequestNum & """ ><u><b>Name on Accounts: " & first_nameTextBox.Text & " " & last_nameTextBox.Text & "</b></u></a>" & _
                            "</body>" & _
                        "</html>"


        EmailList = New EmailListOne(AccountRequestNum, Session("EmployeeConn")).EmailListReturn

        fulleMailList = EmailList
        If Session("environment").ToString.ToLower = "testing" Then
            EmailList = "Jeff.Mele@crozer.org"
            sBody = sBody & fulleMailList

        End If

        toList = EmailList.Split(";")
        If toList.Length = 1 Then
            Mailmsg.To.Add(New System.Net.Mail.MailAddress(toList(0)))
        Else
        End If
        For i = 0 To toList.Length - 1 ' - 2
            If toList(i).Length > 10 Then ' validity check
                Mailmsg.To.Add(New System.Net.Mail.MailAddress(toList(i)))
            End If
        Next

        'Dim mailObj As New MailMessage
        Mailmsg.From = New MailAddress("CSC@crozer.org")
        Mailmsg.IsBodyHtml = True

        Mailmsg.Subject = sSubject
        Mailmsg.Body = sBody

        Dim smtpClient As New SmtpClient()
        smtpClient.Host = "ah1smtprelay01.altahospitals.com"


        If Session("environment") = "Production" Then
            smtpClient.Send(Mailmsg)
        End If



        Dim thisData As New CommandsSqlAndOleDb("InsAccounteMailTo", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int)
        thisData.AddSqlProcParameter("@ToAddress", EmailList, SqlDbType.NVarChar, 4000)
        thisData.AddSqlProcParameter("@subject", sSubject, SqlDbType.NVarChar, 200)
        thisData.AddSqlProcParameter("@body", sBody, SqlDbType.NVarChar, 4000)

        thisData.ExecNonQueryNoReturn()
    End Sub


    Protected Sub btnUpdateSubmitter_Click(sender As Object, e As System.EventArgs) Handles btnUpdateSubmitter.Click
        mpeUpdateClient.Show()

        Dim dbAccess As New CkhsEmployeeDbAccess(Session("EmployeeConn"))
        Dim ckhsEmployee = dbAccess.GetCkhsEmployee(requestor_client_numLabel.Text)


        Dim c3Submitter As New Client3

        'c3Submitter = c3Controller.GetClientByClientNum(requestor_client_numLabel.Text)
        lblUpdateClientFullName.Text = ckhsEmployee.FullName
        txtUpdateEmail.Text = ckhsEmployee.EMail
        txtUpdatePhone.Text = ckhsEmployee.Phone

    End Sub
    Protected Sub btnUpdateClient_Click(sender As Object, e As System.EventArgs) Handles btnUpdateClient.Click
        Dim frmHelperRequestTicket As New FormHelper()

        frmHelperRequestTicket.InitializeUpdateForm("UpdClientObjectByNumberV20", tblUpdateClient, "Update")
        frmHelperRequestTicket.AddCmdParameter("@ClientNum", requestor_client_numLabel.Text)
        frmHelperRequestTicket.UpdateForm()

        Dim dbAccess As New CkhsEmployeeDbAccess(Session("EmployeeConn"))
        Dim ckhsEmployee = dbAccess.GetCkhsEmployee(requestor_client_numLabel.Text)

        'c3Controller = New Client3Controller(requestor_client_numLabel.Text)
        'c3Submitter = c3Controller.GetClientByClientNum(requestor_client_numLabel.Text)

        SubmittingOnBehalfOfNameLabel.Text = ckhsEmployee.FullName & " (" & ckhsEmployee.EMail & ")"


        mpeUpdateClient.Hide()
    End Sub
    Public Function CheckRequiredFields() As Boolean
        Dim removeCss As New PageControls(Page)
        removeCss.RemoveCssClass(Page, "textInputRequired")

        Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
        Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")

        emp_type_cdLabel.Text = emp_type_cdrbl.SelectedValue

        Select Case emp_type_cdrbl.SelectedValue
            Case "1193", "1200"
                'Case "employee"


                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")

                Dim Location As New RequiredField(facility_cdddl, "Location")
                Dim building As New RequiredField(building_cdddl, "building")

                facility_cdddl.BackColor() = Color.Yellow
                facility_cdddl.BorderColor() = Color.Black
                facility_cdddl.BorderStyle() = BorderStyle.Solid
                facility_cdddl.BorderWidth = Unit.Pixel(2)

                building_cdddl.BackColor() = Color.Yellow
                building_cdddl.BorderColor() = Color.Black
                building_cdddl.BorderStyle() = BorderStyle.Solid
                building_cdddl.BorderWidth = Unit.Pixel(2)

                'Dim Aff As New RequiredField(emp_type_cdrbl, "Affiliation")

                pnlProvider.Visible = False
                'providerrbl.SelectedIndex = -1

                'lblVendor.Text = "Vendor Name:"
                Vendorddl.Visible = False
                lblVendor.Visible = False

                VendorNameTextBox.Visible = False
                VendorNameTextBox.Enabled = False

                'NeedsProviderrbl.Visible = True
                'DoesNeedprovlbl.Visible = True
                'NeedsProviderrbl.BackColor() = Color.Yellow
                'NeedsProviderrbl.BorderColor() = Color.Black
                'NeedsProviderrbl.BorderStyle() = BorderStyle.Solid
                'NeedsProviderrbl.BorderWidth = Unit.Pixel(2)

                'If RoleNumLabel.Text = "1567" Then

                '    Dim startdatereq As New RequiredField(start_dateTextBox, "Start Date")

                '    Dim ViewCPMModelreq As New RequiredField(CPMEmployeesddl, "CPM Model")


                '    'CPMEmployeesddl
                '    'CPMEmployeesddl.BackColor() = Color.Yellow
                '    'CPMEmployeesddl.BorderColor() = Color.Black
                '    'CPMEmployeesddl.BorderStyle() = BorderStyle.Solid
                '    'CPMEmployeesddl.BorderWidth = Unit.Pixel(2)

                '    cpmModelAfterTextbox.BackColor() = Color.Yellow
                '    cpmModelAfterTextbox.BorderColor() = Color.Black
                '    cpmModelAfterTextbox.BorderStyle() = BorderStyle.Solid
                '    cpmModelAfterTextbox.BorderWidth = Unit.Pixel(2)

                '    'start_dateTextBox.BackColor() = Color.Yellow
                '    'start_dateTextBox.BorderColor() = Color.Black
                '    'start_dateTextBox.BorderStyle() = BorderStyle.Solid
                '    'start_dateTextBox.BorderWidth = Unit.Pixel(2)

                'End If

            Case "1189"
                'Case "student"

                'Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                'Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")

                Dim titleReq As New RequiredField(Titleddl, "Title222")
                Titleddl.BackColor() = Color.Yellow
                Titleddl.BorderColor() = Color.Black
                Titleddl.BorderStyle() = BorderStyle.Solid
                Titleddl.BorderWidth = Unit.Pixel(2)

                Dim specReq As New RequiredField(Specialtyddl, "Specialty")
                Specialtyddl.BackColor() = Color.Yellow
                Specialtyddl.BorderColor() = Color.Black
                Specialtyddl.BorderStyle() = BorderStyle.Solid
                Specialtyddl.BorderWidth = Unit.Pixel(2)

                Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim faxqReq As New RequiredField(faxtextbox, "Fax")


                pnlProvider.Visible = False
                'providerrbl.SelectedIndex = -1
                Titleddl.Enabled = True

                'lblVendor.Text = "Vendor Name:"
                Vendorddl.Visible = False
                lblVendor.Visible = False

                VendorNameTextBox.Visible = False
                VendorNameTextBox.Enabled = False


            Case "782", "1197"
                ' Case "physician"
                'Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                'Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")



                ' Dim titleReq As New RequiredField(Titleddl, "Title")

                'Titleddl.BackColor() = Color.Yellow
                'Titleddl.BorderColor() = Color.Black
                'Titleddl.BorderStyle() = BorderStyle.Solid
                'Titleddl.BorderWidth = Unit.Pixel(2)

                Dim specReq As New RequiredField(Specialtyddl, "Specialty")
                'Specialtyddl.BackColor() = Color.Yellow
                'Specialtyddl.BorderColor() = Color.Black
                'Specialtyddl.BorderStyle() = BorderStyle.Solid
                'Specialtyddl.BorderWidth = Unit.Pixel(2)


                Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim faxqReq As New RequiredField(faxtextbox, "Fax")
                Dim Titlereq2 As New RequiredField(TitleLabel, "Title")

                'Dim CCMCConsults As New RequiredField(CCMCConsultsrbl, "CCMC Consults")
                'Dim DCMHConsults As New RequiredField(DCMHConsultsrbl, "DCMH Consults")
                'Dim TaylorConsults As New RequiredField(TaylorConsultsrbl, "Taylor Consults")
                'Dim SpringfieldConsults As New RequiredField(SpringfieldConsultsrbl, "Springfield Consults")

                'Dim DEAReq As New RequiredField(DEAIDTextBox, "DEA")

                Dim NPIReq As New RequiredField(npitextbox, "NPI")
                Dim LicensesReq As New RequiredField(LicensesNotextbox, "Licenses")



                'Dim Writeorders As New RequiredField(Writeordersrbl, "Write Orders for C.T.S.")
                'Dim WriteordersDCMH As New RequiredField(WriteordersDCMHrbl, "Write Orders for DCMH")

                Dim CCMCadmitRights As New RequiredField(CCMCadmitRightsrbl, "Admitting Rights for C.T.S.????")
                'Dim DCMHadmitRights As New RequiredField(DCMHadmitRightsrbl, "Admitting Rights for DCMH")

                Dim HanReq As New RequiredField(hanrbl, "HAN-CKHN Physician")

                'If hanrbl.SelectedValue.ToLower = "yes" Then
                '    Dim loccareReq As New RequiredField(LocationOfCareIDddl, "CKHN Group")

                '    Dim ViewCPMModelreq As New RequiredField(CPMEmployeesddl, "CPM Model")

                '    Dim npiReq As New RequiredField(npitextbox, "NPI")
                '    Dim LicensesNoReq As New RequiredField(LicensesNotextbox, "Licenses No")
                '    Dim taxonomyreq As New RequiredField(taxonomytextbox, "Taxonomy")
                '    Dim startdatereq As New RequiredField(start_dateTextBox, "Start Date")

                '    Dim Schedulablereq As New RequiredField(Schedulablerbl, "Schedule")
                '    Dim Televoxrblreq As New RequiredField(Televoxrbl, "Televox")



                'End If

                pnlProvider.Visible = True


                'providerrbl.SelectedValue = "Yes"
                'hanrbl.SelectedValue = "Yes"

                'lblVendor.Text = "Vendor Name:"
                Vendorddl.Visible = False
                lblVendor.Visible = False

                VendorNameTextBox.Visible = False
                VendorNameTextBox.Enabled = False


                Titleddl.Enabled = True

                'Case "1197"
                '    'Case "non staff clinician"
                '    Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                '    Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")
                '    Dim titleReq As New RequiredField(Titleddl, "Title")

                '    Titleddl.BackColor() = Color.Yellow
                '    Titleddl.BorderColor() = Color.Black
                '    Titleddl.BorderStyle() = BorderStyle.Solid
                '    Titleddl.BorderWidth = Unit.Pixel(2)

                '    Dim specReq As New RequiredField(Specialtyddl, "Specialty")
                '    Specialtyddl.BackColor() = Color.Yellow
                '    Specialtyddl.BorderColor() = Color.Black
                '    Specialtyddl.BorderStyle() = BorderStyle.Solid
                '    Specialtyddl.BorderWidth = Unit.Pixel(2)


                '    Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                '    Dim cityReq As New RequiredField(citytextbox, "City")
                '    Dim stateReq As New RequiredField(statetextbox, "State")
                '    Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                '    Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                '    'Dim vendorRequired As New RequiredField(VendorNameTextBox, "Vendor")
                '    Dim faxqReq As New RequiredField(faxtextbox, "Fax")

                '    pnlProvider.Visible = True
                '    pnlOtherAffilliation.Visible = False
                '    providerrbl.SelectedIndex = -1
                '    Titleddl.Enabled = True

                '    VendorNameTextBox.Visible = False
                '    Vendorddl.Visible = True
                '    'lblVendor.Text = "Contractor Name:"


            Case "1194"
                'Case "resident"
                'Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                'Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")

                Dim titleReq As New RequiredField(Titleddl, "Title")

                'Titleddl.BackColor() = Color.Yellow
                'Titleddl.BorderColor() = Color.Black
                'Titleddl.BorderStyle() = BorderStyle.Solid
                'Titleddl.BorderWidth = Unit.Pixel(2)

                Dim specReq As New RequiredField(Specialtyddl, "Specialty")
                'Specialtyddl.BackColor() = Color.Yellow
                'Specialtyddl.BorderColor() = Color.Black
                'Specialtyddl.BorderStyle() = BorderStyle.Solid
                'Specialtyddl.BorderWidth = Unit.Pixel(2)


                Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                'Dim faxqReq As New RequiredField(faxtextbox, "Fax")

                'Dim CCMCConsults As New RequiredField(CCMCConsultsrbl, "CCMC Consults")
                'Dim DCMHConsults As New RequiredField(DCMHConsultsrbl, "DCMH Consults")
                'Dim TaylorConsults As New RequiredField(TaylorConsultsrbl, "Taylor Consults")
                'Dim SpringfieldConsults As New RequiredField(SpringfieldConsultsrbl, "Springfield Consults")

                'Dim Writeorders As New RequiredField(Writeordersrbl, "Write Orders for C.T.S.")
                'Dim WriteordersDCMH As New RequiredField(WriteordersDCMHrbl, "Write Orders for DCMH")

                Dim HanReq As New RequiredField(hanrbl, "HAN-CKHN Physician")



                pnlProvider.Visible = True

                'providerrbl.SelectedValue = "Yes"
                Titleddl.Enabled = True

                'lblVendor.Text = "Vendor Name:"
                Vendorddl.Visible = False
                lblVendor.Visible = False

                VendorNameTextBox.Visible = False
                VendorNameTextBox.Enabled = False



            Case "1198", "1195"

                'Case "vendor" 

                'Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                'Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")

                Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim vendorRequired As New RequiredField(VendorNameTextBox, "Vendor")
                Dim startReq As New RequiredField(start_dateTextBox, "Start Date")

                Dim VendorReq As New RequiredField(Vendorddl, "Vendor")


                VendorNameTextBox.Visible = False
                Vendorddl.Visible = True
                lblVendor.Visible = True
                'lblVendor.Text = "Vendor Name:"


                pnlProvider.Visible = False
                'providerrbl.SelectedIndex = -1

                'Case "1195"
                '    'Case "contract"

                '    Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                '    Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")

                '    Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                '    Dim cityReq As New RequiredField(citytextbox, "City")
                '    Dim stateReq As New RequiredField(statetextbox, "State")
                '    Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                '    Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                '    Dim vendorRequired As New RequiredField(VendorNameTextBox, "Vendor")
                '    Dim startReq As New RequiredField(start_dateTextBox, "Start Date")

                '    Dim VendorReq As New RequiredField(Vendorddl, "Vendor")
                '    Vendorddl.BackColor() = Color.Yellow
                '    Vendorddl.BorderColor() = Color.Black
                '    Vendorddl.BorderStyle() = BorderStyle.Solid
                '    Vendorddl.BorderWidth = Unit.Pixel(2)

                '    'lblVendor.Text = "Contrator Name:"
                '    Vendorddl.Visible = True

                '    VendorNameTextBox.Visible = False

                '    pnlOtherAffilliation.Visible = False
                '    pnlProvider.Visible = False
                '    providerrbl.SelectedIndex = -1

                'Case "1200"
                '    'Case "allied health"

                '    Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                '    Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")

                '    Dim alliedReq As New RequiredField(Titleddl, "Title")
                '    Titleddl.BackColor() = Color.Yellow
                '    Titleddl.BorderColor() = Color.Black
                '    Titleddl.BorderStyle() = BorderStyle.Solid
                '    Titleddl.BorderWidth = Unit.Pixel(2)

                '    Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                '    Dim cityReq As New RequiredField(citytextbox, "City")
                '    Dim stateReq As New RequiredField(statetextbox, "State")
                '    Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                '    Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                '    Dim faxqReq As New RequiredField(faxtextbox, "Fax")

                '    Dim CCMCConsults As New RequiredField(CCMCConsultsrbl, "CCMC Consults")
                '    Dim DCMHConsults As New RequiredField(DCMHConsultsrbl, "DCMH Consults")
                '    Dim TaylorConsults As New RequiredField(TaylorConsultsrbl, "Taylor Consults")
                '    Dim SpringfieldConsults As New RequiredField(SpringfieldConsultsrbl, "Springfield Consults")

                '    Dim Writeorders As New RequiredField(Writeordersrbl, "Write Orders for C.T.S.")
                '    Dim WriteordersDCMH As New RequiredField(WriteordersDCMHrbl, "Write Orders for DCMH")

                '    Dim CCMCadmitRights As New RequiredField(CCMCadmitRightsrbl, "Admitting Rights for C.T.S.")
                '    Dim DCMHadmitRights As New RequiredField(DCMHadmitRightsrbl, "Admitting Rights for DCMH")

                '    providerrbl.SelectedIndex = 0

                '    pnlProvider.Visible = True

                '    VendorNameTextBox.Visible = True
                '    VendorNameTextBox.Enabled = True
                '    'lblVendor.Text = "Vendor Name:"
                '    Vendorddl.Visible = False


                '    pnlOtherAffilliation.Visible = False
                '    'pnlProvider.Visible = False
                '    providerrbl.SelectedValue = "Yes"
                '    'Titleddl.SelectedValue = Nothing
                '    'Titleddl.Enabled = False

            Case "1196", "1199"
                'Case "other" Non ckhn staff

                'Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                'Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")

                Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                'Dim vendorRequired As New RequiredField(VendorNameTextBox, "Vendor")

                Dim startReq As New RequiredField(start_dateTextBox, "Start Date")


                pnlProvider.Visible = False
                'providerrbl.SelectedIndex = -1

                VendorNameTextBox.Visible = True
                VendorNameTextBox.Enabled = False
                lblVendor.Visible = True
                'lblVendor.Text = "Vendor Name:"
                Vendorddl.Visible = False




            Case Else

                'Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                'Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")
                'Dim Aff As New RequiredField(emp_type_cdrbl, "Affiliation")


                pnlProvider.Visible = False
                'providerrbl.SelectedIndex = -1



        End Select

        'For Each cbAppi As ListItem In cblApplications.Items
        '    Dim sCurrApp As String = cbAppi.ToString

        '    If cbAppi.Selected = True And sCurrApp.ToString.ToLower = "mak" Then

        '        lblValidation.Text = "MAK Application needs Comments to identify this account"
        '        If request_descTextBox.Text = "" Then
        '            request_descTextBox.Text = "Use This Mak ID ="

        '        Else
        '            request_descTextBox.Text = request_descTextBox.Text & " Add ID for Mak"
        '        End If

        '        Dim Comments As New RequiredField(request_descTextBox, "Comments")
        '        Return bHasError
        '    End If
        'Next

        Dim RoleDesc As New RequiredField(Rolesddl, "Position Description")
        Rolesddl.BackColor() = Color.Yellow
        Rolesddl.BorderColor() = Color.Black
        Rolesddl.BorderStyle() = BorderStyle.Solid
        Rolesddl.BorderWidth = Unit.Pixel(2)

        Dim EntDesc As New RequiredField(cblEntities, "Entity Description")

        cblEntities.BackColor() = Color.Yellow
        cblEntities.BorderColor() = Color.Black
        cblEntities.BorderStyle() = BorderStyle.Solid
        cblEntities.BorderWidth = Unit.Pixel(2)

        Dim FacilityDesc As New RequiredField(cblFacilities, "Facility Description")

        cblFacilities.BackColor() = Color.Yellow
        cblFacilities.BorderColor() = Color.Black
        cblFacilities.BorderStyle() = BorderStyle.Solid
        cblFacilities.BorderWidth = Unit.Pixel(2)

        Return bHasError

    End Function

    Protected Sub btnNoSearch_Click(sender As Object, e As System.EventArgs) Handles btnNoSearch.Click
        Response.Redirect("SimpleSearch.aspx")
    End Sub

    Protected Sub btnSearchFirst_Click(sender As Object, e As System.EventArgs) Handles btnSearchFirst.Click
        lblSearched.Text = "TRUE"
        SearchPopup.Hide()
    End Sub

    Protected Sub grvClientsExists_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grvClientsExists.RowCommand
        If e.CommandName = "Select" Then


            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim AccountRequestNum As String = grvClientsExists.DataKeys(Convert.ToInt32(e.CommandArgument))("AccountRequestNum").ToString()
            Dim ClientNum As String = grvClientsExists.DataKeys(Convert.ToInt32(e.CommandArgument))("client_num").ToString()


            If AccountRequestNum <> "0" Then
                Response.Redirect("AccountRequestInfo2.aspx?&RequestNum=" & AccountRequestNum, True)

            End If
            If ClientNum <> "0" Then
                Response.Redirect("ClientDemo.aspx?&ClientNum=" & ClientNum, True)

            End If
        End If

    End Sub

    Protected Sub btnCloseExists_Click(sender As Object, e As System.EventArgs) Handles btnCloseExists.Click
        ClientExists.Hide()
        TestednameLabel.Text = "yes"
        lblValidation.ForeColor = Color.Green

        btnSubmit.BackColor = Color.Maroon
        btnSubmit.Height = Unit.Pixel(30)

        btnSelectOnBehalfOf.Height = Unit.Pixel(30)
        btnReturn.Height = Unit.Pixel(30)


        lblValidation.Text = "Click Submit button to complete request"
        btnSubmit.Focus()
    End Sub
    Protected Sub setColumnInfo(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                ' Siemens Employee#

                sEmployeeNum = e.Row.Cells(getcellNoE("siemensempnum", grvClientsExists)).Text

                If sEmployeeNum.Length > 1 Then
                    e.Row.BackColor = System.Drawing.Color.LightGreen
                End If

            End If

        Catch ex As Exception
            Throw ex

        End Try
    End Sub

    Protected Sub btnReturnExists_Click(sender As Object, e As System.EventArgs) Handles btnReturnExists.Click
        ClientExists.Hide()

    End Sub

    'Protected Sub grvClientsExists_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grvClientsExists.RowDataBound
    '    Try
    '        If e.Row.RowType = DataControlRowType.DataRow Then
    '            ' //===================================diag==========================
    '            Dim irow As Integer = e.Row.RowIndex


    '            'sEmployeeNum = e.Row.Cells(getcellNoE("siemens_emp_num", grvClientsExists)).Text

    '            'If sEmployeeNum.Length > 1 Then
    '            '    e.Row.BackColor = System.Drawing.Color.LightGreen
    '            'End If
    '            setColumnInfo(sender, e)

    '        End If
    '    Catch ex As Exception
    '        Throw ex

    '    End Try

    'End Sub

    'Protected Sub ColorCodeAccounts()
    '    Dim BaseApps As New List(Of Application)
    '    BaseApps = New Applications().BaseApps()

    '    For Each item As ListItem In cblApplications.Items

    '        If BaseApps.Find(Function(x) x.ApplicationNum = item.Value).AppSystem = "HospSpec" Then

    '            item.Attributes.Add("style", "color:red")
    '        ElseIf BaseApps.Find(Function(x) x.ApplicationNum = item.Value).AppSystem = "EntSpec" Then

    '            item.Attributes.Add("style", "color:blue")
    '        End If
    '    Next

    'End Sub


    Protected Sub group_nameddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles group_nameddl.SelectedIndexChanged
        'group_numTextBox.Text = group_nameddl.SelectedValue
        group_nameTextBox.Text = group_nameddl.SelectedItem.ToString

        Select Case group_nameddl.SelectedValue
            Case "346", "324" ' This will check all app hospitals and entities for Hospitialists CCMC & DCMH
                ' First clear out all items
                For Each cblItem As ListItem In cblApplications.Items
                    cblItem.Selected = False
                    cblItem.Enabled = False
                Next
                ' Now check all itesm for Hospitilists
                For Each cblItem As ListItem In cblApplications.Items
                    Dim sCurritem As String = cblItem.ToString

                    Select Case sCurritem
                        Case "eCare", "EDM", "Email", "EMR - CKHN", "Network", "PACS", "PICIS", "RSA Token for Remote Access", "Clinician Dashboard", "Single Sign On (SSO)", "Tracemaster"
                            'Change 2015 "EMR - CKHN", "EDM", "Email", "Network", "Tracemaster", "RSA Token for Remote Access", "Single Sign On (SSO)","EMR - Comm"
                            If cblItem.Selected = False Then
                                cblItem.Selected = True
                                cblItem.Enabled = True
                            End If
                    End Select
                    'If sCurritem.ToString = "EAD" And cblItem.Selected = False Then
                    '    cblItem.Selected = True
                    'End If
                Next
                '' check all entities 
                For Each cbentities As ListItem In cblEntities.Items
                    If cbentities.Selected = False Then
                        cbentities.Selected = True
                    End If
                Next


                ' check all hospitals
                For Each cbHospitals As ListItem In cblFacilities.Items
                    Dim sCurrHospital As String = cbHospitals.ToString

                    If cbHospitals.Selected = False And sCurrHospital.ToString.ToLower <> "community" Then
                        cbHospitals.Selected = True

                    End If
                Next
            Case Else

                ReSetApps()
        End Select

        'ChangColor()

    End Sub
    Protected Sub ReSetApps()
        For Each cblItem As ListItem In cblApplications.Items
            cblItem.Selected = False
            'cblItem.Enabled = False
        Next


        If Session("SecurityLevelNum") < 70 Then
            For Each cblItem As ListItem In cblApplications.Items

                cblItem.Enabled = False
            Next
        Else
            For Each cblItem As ListItem In cblApplications.Items

                cblItem.Enabled = True
            Next


        End If

        For Each cblEntitem As ListItem In cblEntities.Items
            cblEntitem.Selected = False
        Next

        For Each cblFacItem As ListItem In cblFacilities.Items
            cblFacItem.Selected = False
        Next

        Dim rwRef() As DataRow
        Dim ddlBinderV2 As DropDownListBinder

        'always use the Main Role
        mainRole = emp_type_cdrbl.SelectedValue

		If Requesttype = "" Then '"addpmhact"


			request_descTextBox.Text = ""

		End If
		If request_descTextBox.Text <> "" Then
			request_descTextBox.ForeColor() = Color.Red
		End If


		request_descTextBox.BackColor() = Color.White
		request_descTextBox.BorderStyle = BorderStyle.None
        request_descTextBox.BorderWidth = Unit.Pixel(2)

        If Rolesddl.SelectedValue.ToString <> "Select" Then
            user_position_descTextBox.Text = Rolesddl.SelectedValue
            mainRole = Rolesddl.SelectedValue

            Title2TextBox.Text = Rolesddl.SelectedItem.ToString
            If thisRole Is Nothing Then
                thisRole = Rolesddl.SelectedValue
            End If
        ElseIf Rolesddl.SelectedValue.ToString = "Select" And mainRole <> Nothing Then
			ddlBinderV2 = New DropDownListBinder(Rolesddl, "SelEmployeeRolesV2", Session("EmployeeConn"))

			ddlBinderV2.AddDDLCriteria("@RoleNum", mainRole, SqlDbType.NVarChar)
            ddlBinderV2.BindData("rolenum", "roledesc")

            Rolesddl.SelectedValue = mainRole

        End If
        ' ADD get Table here
        Dim thisdata As New CommandsSqlAndOleDb("SelApplicationExtensionByRoleNum", Session("EmployeeConn"))

        If Rolesddl.SelectedValue.ToString = "Select" And thisRole <> "" Then
            Rolesddl.SelectedValue = thisRole
        End If

        thisdata.AddSqlProcParameter("@roleNum", Rolesddl.SelectedValue.ToString, SqlDbType.NVarChar, 9)
        thisdata.AddSqlProcParameter("@display", "base", SqlDbType.NVarChar, 9)


        thisdata.ExecNonQueryNoReturn()

        dtApplications = thisdata.GetSqlDataTable

        Dim Appnum As Integer
        Dim appDeafult As String
        Dim snotice As String = "false"
        Dim MakCont As Boolean

        For Each row As DataRow In dtApplications.Rows
            Appnum = row("ApplicationNum")
            appDeafult = row("DefaultValue")
            gAppName = row("ApplicationDesc")



            For Each cblItem As ListItem In cblApplications.Items
                Dim sCurritem As String = cblItem.Value

                If sCurritem = Appnum And appDeafult.ToLower = "yes" Then
                    cblItem.Selected = True


                    cblItem.Enabled = True
                End If
                If sCurritem = Appnum And appDeafult.ToLower = "no" Then
                    cblItem.Enabled = True
                    cblItem.Attributes.Add("Style", "color:blue;")



                End If

                If cblItem.ToString.ToLower = "mak" And cblItem.Selected = True Then

                    snotice = "true"
                End If
            Next
        Next row

        If snotice = "true" Then
            If request_descTextBox.Text = "" Then
                request_descTextBox.Text = "Use This Mak ID ="
            Else
                MakCont = request_descTextBox.Text.Contains("Mak ID")
                If MakCont = False Then
                    request_descTextBox.Text = request_descTextBox.Text & " Please Add Mak ID "
                End If
            End If

            request_descTextBox.BackColor() = Color.Yellow
            request_descTextBox.BorderColor() = Color.Black
            request_descTextBox.BorderStyle() = BorderStyle.Solid
            request_descTextBox.BorderWidth = Unit.Pixel(2)
        End If


        Dim BusLog As New AccountBusinessLogic

        If BusLog.eCare Then
            pnlTCl.Visible = True
        Else
            pnlTCl.Visible = False
            pageCtrl.RemoveCssClass(pnlTCl, "textInputRequired")

        End If

        If mainRole = "1194" Then
            Titleddl.SelectedValue = Title2TextBox.Text
            'Title2TextBox.Text
            TitleLabel.Text = Titleddl.SelectedItem.ToString

            'Title2TextBox.Text = Titleddl.SelectedItem.ToString

            Title2TextBox.Visible = True
            Titleddl.Visible = False

        End If

        ChangColorNoCheck()
        'ChangColor()

    End Sub
    Protected Sub SuppSupportrbl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles SuppSupportrbl.SelectedIndexChanged
        If SuppSupportrbl.SelectedValue.ToLower = "yes" Then
            entity_cdDDL.SelectedValue = "150"

            Dim ddbDepartmentBinder As New DropDownListBinder(department_cdddl, "selDepartmentCodesV4", Session("EmployeeConn"))
            ddbDepartmentBinder.AddDDLCriteria("@entity_cd", "150", SqlDbType.NVarChar)
            ddbDepartmentBinder.BindData("department_cd", "department_name")

            department_cdddl.SelectedValue = "601000"
            CheckEntitiesHosp()

            'Else
            entity_cdDDL.SelectedIndex = -1
            department_cdddl.SelectedIndex = -1

            For Each cblEntitem As ListItem In cblEntities.Items
                cblEntitem.Selected = False
            Next

            For Each cblFacItem As ListItem In cblFacilities.Items
                cblFacItem.Selected = False
            Next

        End If
        ChangColorNoCheck()

        'ChangColor()

    End Sub
    Public Sub ChangColor()
        ' ADD get Table here

        If user_position_descTextBox.Text Is Nothing Or user_position_descTextBox.Text.ToLower = "select" Then
            Exit Sub
        End If

        Dim thisdata As New CommandsSqlAndOleDb("SelApplicationExtensionByRoleNum", Session("EmployeeConn"))


        If hanrbl.SelectedValue.ToString = "Yes" Then
            If user_position_descTextBox.Text = "782" Then
                user_position_descTextBox.Text = "1553"
            End If

        End If

        thisdata.AddSqlProcParameter("@roleNum", user_position_descTextBox.Text, SqlDbType.NVarChar, 9)
        thisdata.AddSqlProcParameter("@display", "base", SqlDbType.NVarChar, 9)

        thisdata.ExecNonQueryNoReturn()

        dtApplications = thisdata.GetSqlDataTable

        Dim Appnum As Integer
        Dim appDeafult As String

        For Each row As DataRow In dtApplications.Rows
            Appnum = row("ApplicationNum")
            appDeafult = row("DefaultValue")
            gAppName = row("ApplicationDesc")


            For Each cblItem As ListItem In cblApplications.Items
                Dim sCurritem As String = cblItem.Value
                ' Only turn Blue color on
                If sCurritem = Appnum And appDeafult.ToLower = "no" Then
                    If cblItem.Selected = True Then
                        cblItem.Selected = True
                        cblItem.Attributes.Add("Style", "color:blue;")

                    Else
                        cblItem.Selected = False
                        cblItem.Attributes.Add("Style", "color:blue;")

                    End If

                ElseIf sCurritem = Appnum And appDeafult.ToLower = "yes" Then
                    cblItem.Enabled = True
                    cblItem.Selected = True
                    cblItem.Attributes.Add("Style", "color:black;")

                    If sCurritem = 156 Then
                        pnlTokenAddress.Visible = True
                    End If

                End If



            Next
        Next row
        'ChangColorNoCheck()

    End Sub

    Public Sub ChangColorNoCheck()
        ' ADD get Table here

        If user_position_descTextBox.Text Is Nothing Or user_position_descTextBox.Text.ToLower = "select" Then
            Exit Sub
        End If

        Dim thisdata As New CommandsSqlAndOleDb("SelApplicationExtensionByRoleNum", Session("EmployeeConn"))


        'If hanrbl.SelectedValue.ToString = "Yes" Then
        '    thisdata.AddSqlProcParameter("@roleNum", "1542", SqlDbType.NVarChar, 9)

        'Else
        '    thisdata.AddSqlProcParameter("@roleNum", user_position_descTextBox.Text, SqlDbType.NVarChar, 9)

        'End If

        thisdata.AddSqlProcParameter("@roleNum", user_position_descTextBox.Text, SqlDbType.NVarChar, 9)
        thisdata.AddSqlProcParameter("@display", "base", SqlDbType.NVarChar, 9)

        thisdata.ExecNonQueryNoReturn()

        dtApplications = thisdata.GetSqlDataTable

        Dim Appnum As Integer
        Dim appDeafult As String

        For Each row As DataRow In dtApplications.Rows
            Appnum = row("ApplicationNum")
            appDeafult = row("DefaultValue")
            gAppName = row("ApplicationDesc")


            For Each cblItem As ListItem In cblApplications.Items
                Dim sCurritem As String = cblItem.Value
                ' Only turn Blue color on
                If sCurritem = Appnum And appDeafult.ToLower = "no" Then
                    cblItem.Attributes.Add("Style", "color:blue;")


                ElseIf sCurritem = Appnum And appDeafult.ToLower = "yes" Then
                    'cblItem.Enabled = True
                    'cblItem.Selected = True
                    cblItem.Attributes.Add("Style", "color:black;")

                End If


            Next
        Next row

    End Sub
    Protected Sub hanrbl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles hanrbl.SelectedIndexChanged


        If hanrbl.SelectedValue.ToString = "Yes" Then

            LocationOfCareIDddl.BackColor() = Color.Yellow
            LocationOfCareIDddl.BorderColor() = Color.Black
            LocationOfCareIDddl.BorderStyle() = BorderStyle.Solid
            LocationOfCareIDddl.BorderWidth = Unit.Pixel(2)

            LocationOfCareIDddl.Visible = True
            'cpmPhysSchedulePanel.Visible = True
            'cpmModelEmplPanel.Visible = True
            ckhnlbl.Visible = True

            'Dim ddcmpMOdelafter As New DropDownListBinder(CPMEmployeesddl, "SelCPMClients", Session("EmployeeConn"))
            'ddcmpMOdelafter.AddDDLCriteria("@Group_num", GNumberGroupNumLabel.Text, SqlDbType.NVarChar)
            'ddcmpMOdelafter.BindData("client_num", "FullName")

            If masterRole.Text = "1194" Then

                Rolesddl.SelectedValue = "1194"

				Dim ddlRoleBinder As New DropDownListBinder(Rolesddl, "SelEmployeeRolesV2", Session("EmployeeConn"))
				ddlRoleBinder.AddDDLCriteria("@RoleNum", "1194", SqlDbType.NVarChar)
                ddlRoleBinder.BindData("rolenum", "RoleDesc")

                Dim ddbTitleBinder As New DropDownListBinder(Titleddl, "SelTitlesV4", Session("EmployeeConn"))
                ddbTitleBinder.AddDDLCriteria("@titleType", "Resident", SqlDbType.NVarChar)
                ddbTitleBinder.BindData("TitleShortDesc", "TitleDesc")




                'Else

                '    Rolesddl.SelectedValue = "1553"
                '    Rolesddl.Enabled = False
            End If




            user_position_descTextBox.Text = Rolesddl.SelectedValue
            mainRole = Rolesddl.SelectedValue

            'Title2TextBox.Text = Rolesddl.SelectedItem.ToString
            If thisRole Is Nothing Then
                thisRole = Rolesddl.SelectedValue
            End If

            'Rolesddl.SelectedValue 
            'user_position_descTextBox.Text = 

            'lblprov.Visible = True
            'providerrbl.Visible = True

            'CommLocationOfCareIDddl.BackColor() = Color.Orange
            'CommLocationOfCareIDddl.BorderColor() = Color.Black
            'CommLocationOfCareIDddl.BorderStyle() = BorderStyle.Solid
            'CommLocationOfCareIDddl.BorderWidth = Unit.Pixel(2)

        Else
            If Rolesddl.SelectedValue = "1553" Then
                Response.Redirect("SignOnForm.aspx")
                Exit Sub

            End If
            LocationOfCareIDddl.Visible = False
            ckhnlbl.Visible = False

            'LocationOfCareLabel.Text = ""

            'lblprov.Visible = False
            'providerrbl.Visible = False
            'providerrbl.SelectedIndex = -1

            'CommLocationOfCareIDddl.BackColor() = Color.White
            'CommLocationOfCareIDddl.BorderColor() = Color.Black
            'CommLocationOfCareIDddl.BorderStyle() = BorderStyle.Groove
            'CommLocationOfCareIDddl.BorderWidth = Unit.Pixel(2)

        End If
        CheckRequiredFields()

        ChangColor()

    End Sub

    Protected Sub btnChangeTCL_Click(sender As Object, e As System.EventArgs) Handles btnChangeTCL.Click
        If btnChangeTCL.Text = "Show All User Types" Then

            Dim tcldata As New CommandsSqlAndOleDb("SelInvisionUserTypeV20", Session("EmployeeConn"))
            tcldata.AddSqlProcParameter("@rolenum", masterRole.Text, SqlDbType.NVarChar, 8)
            tcldata.AddSqlProcParameter("@Clientupd", "all", SqlDbType.NVarChar, 3)

            tcldata.GetSqlDataset()

            dtTCLUserTypes = tcldata.GetSqlDataTable

            gvTCL.DataSource = dtTCLUserTypes
            gvTCL.DataBind()

            gvTCL.Visible = True
            gvTCL.BackColor() = Color.Yellow
            gvTCL.BorderColor() = Color.Black
            gvTCL.BorderStyle() = BorderStyle.Solid
            gvTCL.BorderWidth = Unit.Pixel(2)
            gvTCL.RowStyle.BackColor() = Color.Yellow

            pnlTCl.Visible = True
            TCLTextBox.Visible = True
            UserTypeDescTextBox.Visible = True
            UserTypeCdTextBox.Visible = True

            btnChangeTCL.Text = "Close Types"


        ElseIf btnChangeTCL.Text = "Close Types" Then
            gvTCL.Dispose()
            gvTCL.Visible = False
            btnChangeTCL.Text = "Show All User Types"
        Else
            gvTCL.Visible = True
            gvTCL.BackColor() = Color.Yellow
            gvTCL.BorderColor() = Color.Black
            gvTCL.BorderStyle() = BorderStyle.Solid
            gvTCL.BorderWidth = Unit.Pixel(2)
            gvTCL.RowStyle.BackColor() = Color.Yellow

            btnChangeTCL.Visible = False

        End If



    End Sub

    'Protected Sub CommLocationOfCareIDddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles CommLocationOfCareIDddl.SelectedIndexChanged
    '    If CommLocationOfCareIDLabel.Text <> "" Then

    '        CommLocationOfCareIDLabel.Text = CommLocationOfCareIDddl.SelectedItem.ToString
    '        CommNumberGroupNumLabel.Text = CommLocationOfCareIDddl.SelectedValue
    '    ElseIf CommLocationOfCareIDddl.SelectedValue.ToLower = "select" Or CommLocationOfCareIDddl.SelectedValue = "99999" Then

    '        CommLocationOfCareIDLabel.Text = Nothing
    '        CommNumberGroupNumLabel.Text = Nothing

    '    Else

    '        CommLocationOfCareIDLabel.Text = CommLocationOfCareIDddl.SelectedItem.ToString
    '        CommNumberGroupNumLabel.Text = CommLocationOfCareIDddl.SelectedValue


    '    End If

    '    ChangColorNoCheck()

    '    'ChangColor()

    'End Sub
    Protected Sub LocationOfCareIDddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles LocationOfCareIDddl.SelectedIndexChanged
        ' Gnumber

        'CPMEmployeesddl.Dispose()

        If LocationOfCareIDLabel.Text <> "" Then
            LocationOfCareIDLabel.Text = LocationOfCareIDddl.SelectedItem.ToString
            GNumberGroupNumLabel.Text = LocationOfCareIDddl.SelectedValue

            'Dim ddcmpMOdelafter As New DropDownListBinder(CPMEmployeesddl, "SelCPMClients", Session("EmployeeConn"))
            'ddcmpMOdelafter.AddDDLCriteria("@Group_num", GNumberGroupNumLabel.Text, SqlDbType.NVarChar)
            'ddcmpMOdelafter.BindData("client_num", "FullName")

        ElseIf LocationOfCareIDddl.SelectedValue.ToLower = "select" Or LocationOfCareIDddl.SelectedValue = "99999" Then

            LocationOfCareIDLabel.Text = Nothing
            GNumberGroupNumLabel.Text = Nothing

            'Dim ddcmpMOdelafter As New DropDownListBinder(CPMEmployeesddl, "SelCPMClients", Session("EmployeeConn"))
            'ddcmpMOdelafter.AddDDLCriteria("@cpmtype ", "physician", SqlDbType.NVarChar)
            'ddcmpMOdelafter.BindData("client_num", "FullName")

        Else

            LocationOfCareIDLabel.Text = LocationOfCareIDddl.SelectedItem.ToString
            GNumberGroupNumLabel.Text = LocationOfCareIDddl.SelectedValue

            'Dim ddcmpMOdelafter As New DropDownListBinder(CPMEmployeesddl, "SelCPMClients", Session("EmployeeConn"))
            'ddcmpMOdelafter.AddDDLCriteria("@Group_num", GNumberGroupNumLabel.Text, SqlDbType.NVarChar)
            'ddcmpMOdelafter.BindData("client_num", "FullName")

        End If


        'cpmModelEmplPanel.Visible = True
        'cpmPhysSchedulePanel.Visible = True

        ''CPMEmployeesddl
        'CPMEmployeesddl.BackColor() = Color.Yellow
        'CPMEmployeesddl.BorderColor() = Color.Black
        'CPMEmployeesddl.BorderStyle() = BorderStyle.Solid
        'CPMEmployeesddl.BorderWidth = Unit.Pixel(2)

        'cpmModelAfterTextbox.BackColor() = Color.Yellow
        'cpmModelAfterTextbox.BorderColor() = Color.Black
        'cpmModelAfterTextbox.BorderStyle() = BorderStyle.Solid
        'cpmModelAfterTextbox.BorderWidth = Unit.Pixel(2)

        'Schedulablerbl
        'Schedulablerbl.BackColor() = Color.Yellow
        'Schedulablerbl.BorderColor() = Color.Black
        'Schedulablerbl.BorderStyle() = BorderStyle.Solid
        'Schedulablerbl.BorderWidth = Unit.Pixel(2)

        ''Televoxrbl
        'Televoxrbl.BackColor() = Color.Yellow
        'Televoxrbl.BorderColor() = Color.Black
        'Televoxrbl.BorderStyle() = BorderStyle.Solid
        'Televoxrbl.BorderWidth = Unit.Pixel(2)

        'ChangColorNoCheck()

        'ChangColor()

    End Sub
    'Protected Sub CoverageGroupddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles CoverageGroupddl.SelectedIndexChanged
    '    ' Coverage group
    '    If CoverageGroupNumLabel.Text <> "" Then

    '        CoverageGroupNumLabel.Text = CoverageGroupddl.SelectedItem.ToString
    '        CommNumberGroupNumLabel.Text = CoverageGroupddl.SelectedValue


    '    ElseIf CoverageGroupddl.SelectedValue.ToLower = "select" Or CoverageGroupddl.SelectedValue = "99999" Then

    '        CoverageGroupNumLabel.Text = Nothing
    '        CommNumberGroupNumLabel.Text = Nothing
    '    Else

    '        CoverageGroupNumLabel.Text = CoverageGroupddl.SelectedItem.ToString
    '        CommNumberGroupNumLabel.Text = CoverageGroupddl.SelectedValue

    '    End If
    '    ChangColorNoCheck()

    '    'ChangColor()

    'End Sub

    'Protected Sub Nonperferdddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles Nonperferdddl.SelectedIndexChanged
    '    ' Non perfered group
    '    If NonPerferedGroupnumLabel.Text <> "" Then

    '        NonPerferedGroupnumLabel.Text = Nonperferdddl.SelectedValue

    '    ElseIf Nonperferdddl.SelectedValue.ToLower = "select" Or Nonperferdddl.SelectedValue = "99999" Then

    '        NonPerferedGroupnumLabel.Text = Nothing
    '    Else

    '        NonPerferedGroupnumLabel.Text = Nonperferdddl.SelectedValue

    '    End If
    '    ChangColorNoCheck()

    '    'ChangColor()


    'End Sub

    Protected Sub PhysOfficeddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles PhysOfficeddl.SelectedIndexChanged
        ' Non perfered group
        group_nameTextBox.Text = PhysOfficeddl.SelectedItem.ToString

        'If group_numTextBox.Text <> "" Then

        '    group_numTextBox.Text = PhysOfficeddl.SelectedValue
        '    group_nameTextBox.Text = PhysOfficeddl.SelectedItem.ToString

        'ElseIf PhysOfficeddl.SelectedValue.ToLower = "select" Or PhysOfficeddl.SelectedValue = "99999" Then

        '    group_numTextBox.Text = Nothing
        'Else

        '    group_numTextBox.Text = PhysOfficeddl.SelectedValue
        '    group_nameTextBox.Text = PhysOfficeddl.SelectedItem.ToString

        'End If
        ChangColorNoCheck()



    End Sub





    'Protected Sub CPMEmployeesddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles CPMEmployeesddl.SelectedIndexChanged

    '    If CPMEmployeesddl.SelectedValue = 0 Then
    '        cpmModelEmplPanel.Visible = False
    '        CPMEmployeesddl.SelectedIndex = -1
    '        cpmModelAfterPanel.Visible = True
    '    Else
    '        cpmModelAfterTextbox.Text = CPMEmployeesddl.SelectedItem.ToString
    '        CPMModelTextBox.Text = CPMEmployeesddl.SelectedItem.ToString
    '    End If

    '    ChangColor()
    'End Sub


End Class
