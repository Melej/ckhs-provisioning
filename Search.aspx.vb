﻿Imports System.IO
Imports System.Data '
Imports System.Data.SqlClient
Imports App_Code
Imports AjaxControlToolkit
Imports C1.Web.UI.Controls.C1Window

Partial Class Search
    Inherits MyBasePage '   Inherits System.Web.UI.Page



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load



        Dim ddlBinder As New DropDownListBinder

        If Not IsPostBack Then


            ddlBinder.BindData(Departmentddl, "sel_department_codes", "department_cd", "department_name", Session("EmployeeConn"))

        End If


    End Sub
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click

        Dim thisData As New CommandsSqlAndOleDb("SelAdvancedSearchV3", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@first_name", FirstNameTextBox.Text, SqlDbType.NVarChar, 20)
        thisData.AddSqlProcParameter("@last_name", LastNameTextBox.Text, SqlDbType.NVarChar, 20)
        thisData.AddSqlProcParameter("@first_search_type", rblFirstNameType.SelectedValue, SqlDbType.NVarChar, 3)
        thisData.AddSqlProcParameter("@last_search_type", rblLastNameType.SelectedValue, SqlDbType.NVarChar, 3)

        'If Departmentddl.SelectedIndex > 0 Then
        '    thisData.AddSqlProcParameter("@department_cd", Departmentddl.SelectedValue, SqlDbType.NVarChar, 8)

        'End If



        gvSearch.DataSource = thisData.GetSqlDataTable
        gvSearch.DataBind()
    End Sub
    Protected Sub gvSearch_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvSearch.RowCommand

        If e.CommandName = "Select" Then

            Dim client_num As String = gvSearch.DataKeys(Convert.ToInt32(e.CommandArgument))("client_num").ToString()
            Response.Redirect("~/AddRemoveAcctForm.aspx?ClientNum=" & client_num)

        End If
    End Sub
End Class
