﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="false" CodeFile="RoleMaintenance.aspx.vb" Inherits="RoleMaintenance" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">


    <asp:UpdatePanel ID="uplAppMaintenance" runat ="server" ChildrenAsTriggers= "true" >

<Triggers>
  <%-- <asp:AsyncPostBackTrigger ControlID="btnAddRole" EventName="Click" />--%>
  <%--<asp:AsyncPostBackTrigger ControlID = "Rolesddl" EventName="SelectedIndexChanged" />--%>

</Triggers>

<ContentTemplate>







<table>
    <tr>
        <td>
               <ajaxToolkit:TabContainer ID="tabs" runat="server" ActiveTabIndex="0"
                Width="100%" style="margin-bottom: 1px" EnableTheming="False"
                BackColor="#efefef">



                       <ajaxToolkit:TabPanel ID="tpDistressAssessment" runat="server" TabIndex="0"  BackColor="Gainsboro" Visible = "True">


                      <HeaderTemplate > 
                     
                            <asp:Label ID="Label1" runat="server" Text="View Current Roles" Font-Bold="true"></asp:Label>
            
                     
                      </HeaderTemplate>


                      <ContentTemplate>
                            
                                <table  border="3" style="background-color:#efefef">
                                    <tr class="tableRowSubHeader">
                                            <td colspan="2">
                                                View Current Roles
                                            </td>
                                    </tr>
                                    <tr>
                                         <td>
                                            Select Role
                                        </td>
                                        <td>
                                            <asp:DropDownList ID = "Rolesddl" runat = "server" AutoPostBack="true"></asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2"> 
                                              <asp:CheckBoxList ID = "cblUpdateRoles" runat = "server" RepeatColumns="3">                     
                                                     </asp:CheckBoxList>
                                        </td>
                                    </tr>
                                     <tr>
                                        <td colspan="2" align="center">
                                            <asp:Label ID = "lblUpdateRole" runat="server" ForeColor="Red" Font-Size="16px"></asp:Label>
                                        </td>
                                    
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="center">
                                            <asp:Button ID ="btnUpdateRole" runat ="server" Text="Update Role" BackColor="#336666" Visible="false"
                                           ForeColor="White" Font-Bold="True" Font-Names="Arial" Font-Size="Small" Height="30px" Width="130px" />
                                           
                                        </td>
                                    </tr>
                               </table>
                          
                      </ContentTemplate>


                        </ajaxToolkit:TabPanel>

                         <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" TabIndex="1"  BackColor="Gainsboro" Visible = "True">


                      <HeaderTemplate > 
                     
                            <asp:Label ID="Label2" runat="server" Text="Add New Role" Font-Bold="true"></asp:Label>
            
                     
                      </HeaderTemplate>


                      <ContentTemplate>

                                  <table  border="3" style="background-color:#efefef">
                                    <tr class="tableRowSubHeader">
                                            <td colspan="2">
                                                Add Role
                                            </td>
                                    </tr>
                                    <tr>
                                             <td>
                                                Select Employee Title
                                            </td>
                                            <td>
                                                <asp:DropDownList ID = "EmployeeTitlesddl" runat = "server" AutoPostBack="true"></asp:DropDownList>
                                            </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2"> 
                                              <asp:CheckBoxList ID = "cblAddRole" runat = "server" RepeatColumns="3">                     
                                                     </asp:CheckBoxList>
                                        </td>
                                    </tr>
                                    <tr>
                                    <tr>
                                        <td colspan="2"  align="center">
                                            <asp:Label ID = "lblAddRole" runat="server" ForeColor="Red" Font-Size="16px"></asp:Label>
                                        </td>
                                    
                                    </tr>
                                        <td colspan="2" align="center">
                                            <asp:Label ID="lblRolenum" runat="server" Visible="false"></asp:Label>
                                            <asp:Button ID ="btnAddRole" runat ="server" Text="Add Role" BackColor="#336666" Visible="false"
                                            ForeColor="White" Font-Bold="True" Font-Names="Arial" Font-Size="Small" Height="30px" Width="130px" />
                                        </td>
                                    </tr>
                               </table>

                       </ContentTemplate>


                        </ajaxToolkit:TabPanel>

                    </ajaxToolkit:TabContainer>

        </td>
    </tr>
</table>

</ContentTemplate>


        </asp:UpdatePanel>


</asp:Content>

