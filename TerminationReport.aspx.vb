﻿Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports App_Code

Partial Class TerminationReport
    Inherits System.Web.UI.Page

    Dim UserInfo As UserSecurity

    Dim dst As DataSet

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        UserInfo = New UserSecurity(Session("LoginID"), Session("EmployeeConn"))


        If Session("SecurityLevelNum") < 25 Then
            Response.Redirect("~/Security.aspx")
        End If

        If Session("SecurityLevelNum") < 40 Then
            Response.Redirect("~/MyRequests.aspx")

        End If

        If Not IsPostBack Then

            Dim ddbAffiliationBinder As New DropDownListBinder(Affiliationddl, "SelAffiliationForReport", Session("EmployeeConn"))

            ddbAffiliationBinder.AddDDLCriteria("@ReportType", "provterm", SqlDbType.NVarChar)
            ddbAffiliationBinder.BindData("AffiliationID", "AffiliationDisplay")


            txtStartDate.Text = DateTime.Today.AddDays(-90)
            txtEndDate.Text = DateTime.Today

            HDStartDate.Text = txtStartDate.Text
            HDEnddate.Text = txtEndDate.Text

            SetOverView()
        Else

            HDStartDate.Text = txtStartDate.Text
            HDEnddate.Text = txtEndDate.Text


        End If
    End Sub

    Protected Sub SetOverView()

        FillDataset()


        If txtStartDate.Text = "" Then

            txtStartDate.Text = DateTime.Today.AddDays(-90)
            txtEndDate.Text = DateTime.Today

            HDStartDate.Text = txtStartDate.Text
            HDEnddate.Text = txtEndDate.Text

        End If


    End Sub
    Public Sub FillDataset()

        Dim thisData As New CommandsSqlAndOleDb("SelTerminateReportByDate", Session("EmployeeConn"))

        If txtStartDate.Text <> "" Then
            thisData.AddSqlProcParameter("@startDate", HDStartDate.Text, SqlDbType.NVarChar, 12)
        End If

        If txtEndDate.Text <> "" Then
            thisData.AddSqlProcParameter("@endDate", HDEnddate.Text, SqlDbType.NVarChar, 12)
        End If

        thisData.AddSqlProcParameter("@AffiliationID", empType.Text, SqlDbType.NVarChar)
        thisData.AddSqlProcParameter("@reportType", "prov", SqlDbType.NVarChar, 12)

        Dim dt As DataTable

        dt = thisData.GetSqlDataTable

        dst = thisData.GetSqlDataset()

        Session("Termtable") = dt

        GridEmployees.DataSource = dt
        GridEmployees.DataBind()

        'GridHidden.DataSource = dt
        'GridHidden.DataBind()

        'totalcountlbl.Text = dt.Rows(1)("TotalCount").ToString
        'UnknowCountlbl.Text = dt.Rows(1)("UnknowCount").ToString
        'EmployeeCountlbl.Text = dt.Rows(1)("EmployeeCount").ToString
        'ContractCountlbl.Text = dt.Rows(1)("ContractCount").ToString
        'StudentCountlbl.Text = dt.Rows(1)("StudentCount").ToString
        'PhysicianCountlbl.Text = dt.Rows(1)("PhysicianCount").ToString
        'NursingCountlbl.Text = dt.Rows(1)("NursingCount").ToString
        'EmpNoNumberCountlbl.Text = dt.Rows(1)("EmpNoNumberCount").ToString
        'NonCKHNCountlbl.Text = dt.Rows(1)("NonCKHNCount").ToString
        'ResidentCountlbl.Text = dt.Rows(1)("ResidentCount").ToString

        totalpan.Visible = True

        Dim sTypes As String = ""
        Dim iSelected As Integer = 0


        

    End Sub

    Protected Sub GridEmployees_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridEmployees.RowCommand
        If e.CommandName = "Select" Then

            Dim sClientNum As String
            Dim sActive As String

            sClientNum = GridEmployees.DataKeys(Convert.ToInt32(e.CommandArgument))("receiver_client_num").ToString()
            sActive = GridEmployees.DataKeys(Convert.ToInt32(e.CommandArgument))("AccountsActive").ToString()
            GridEmployees.SelectedIndex = -1

            If sActive = "Active" Then
                Response.Redirect("./ClientDemo.aspx?ClientNum=" & sClientNum, True)

            Else
                Response.Redirect("./DeactivatedAccount.aspx?ClientNum=" & sClientNum, True)

            End If

        End If
    End Sub


    Protected Sub GridEmployees_Sorting(sender As Object, e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles GridEmployees.Sorting
        'Retrieve the table from the session object.
        Dim sortdt = TryCast(Session("Termtable"), DataTable)

        If sortdt IsNot Nothing Then

            'Sort the data.
            sortdt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression)
            GridEmployees.DataSource = sortdt
            GridEmployees.DataBind()

        End If

    End Sub
    Private Function GetSortDirection(ByVal column As String) As String

        ' By default, set the sort direction to ascending.
        Dim sortDirection = "ASC"

        ' Retrieve the last column that was sorted.
        Dim sortExpression = TryCast(ViewState("SortExpression"), String)

        If sortExpression IsNot Nothing Then
            ' Check if the same column is being sorted.
            ' Otherwise, the default value can be returned.
            If sortExpression = column Then
                Dim lastDirection = TryCast(ViewState("SortDirection"), String)
                If lastDirection IsNot Nothing _
                  AndAlso lastDirection = "ASC" Then

                    sortDirection = "DESC"

                End If
            End If
        End If

        ' Save new values in ViewState.
        ViewState("SortDirection") = sortDirection
        ViewState("SortExpression") = column

        Return sortDirection

    End Function
    Protected Sub btnExcel_Click(sender As Object, e As System.EventArgs) Handles btnExcel.Click

        CreateExcel2()
    End Sub
    Sub CreateExcel2()



        Response.AddHeader("content-disposition", "attachment;filename=FileName.xls")

        Response.Charset = String.Empty
        Response.ContentType = "application/vnd.ms-excel"
        '  Response.ContentType = "application/vnd.xls"

        Dim sw As System.IO.StringWriter = New System.IO.StringWriter()

        Dim hw As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(sw)

        '===============================Create form to contain grid
        Dim frm As New HtmlForm()

        GridHidden.Visible = True

        GridHidden.Columns.RemoveAt(0)
        GridHidden.Parent.Controls.Add(frm)
        frm.Attributes("RunAt") = "server"
        frm.Controls.Add(GridHidden)

        frm.RenderControl(hw)
        Response.Write(sw.ToString())

        Response.End()

        GridHidden.Visible = False

    End Sub

    Protected Sub btnUpdate_Click(sender As Object, e As System.EventArgs) Handles btnUpdate.Click
        HDStartDate.Text = txtStartDate.Text
        HDEnddate.Text = txtEndDate.Text

        FillDataset()
    End Sub

    Protected Sub Affiliationddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles Affiliationddl.SelectedIndexChanged

        If Affiliationddl.SelectedIndex > 0 Then
            empType.Text = Affiliationddl.SelectedValue
        Else
            Affiliationddl.SelectedValue = empType.Text
        End If

    End Sub


End Class
