﻿Imports System.IO
Imports System.Data '
Imports System.Data.SqlClient
Imports App_Code
Imports AjaxControlToolkit
Imports C1.Web.UI.Controls.C1Window
Imports System.Net.Mail

Partial Class CreateDeleteAccount
    Inherits System.Web.UI.Page
    Dim FormSignOn As New FormSQL()
    Dim sActivitytype As String = ""
    Dim cblBinder As New CheckBoxListBinder
    Dim FormViewRequest As New FormSQL()
    Dim AccountRequestNum As Integer
    Dim AccountSeqNum As Integer
    Dim AccountRequestType As String
    Dim AppNum As Integer
    Dim RequestItems As RequestItems
    Dim thisRequest As New RequestItem
    Dim AccRequest As AccountRequest
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        AccountRequestNum = Request.QueryString("RequestNum")
        AccountSeqNum = Request.QueryString("account_seq_num")
        ' RequestType = Request.QueryString("ReqType") ' Add or Delete


        AccRequest = New AccountRequest(AccountRequestNum, Session("EmployeeConn"))

        FormViewRequest = New FormSQL(Session("EmployeeConn"), "SelAccountRequestV3", "ins_account_request", "", Page)


        FormSignOn = New FormSQL(Session("EmployeeConn"), "SelAccountRequestV20", "UpdFullAccountRequestV4", "", Page)
        FormSignOn.AddSelectParm("@account_request_num", AccountRequestNum, SqlDbType.Int, 6)

        Dim rblAffiliateBinder As New RadioButtonListBinder(emp_type_cdrbl, "SelAffiliationsV4", Session("EmployeeConn"))
        rblAffiliateBinder.BindData("AffiliationDescLower", "AffiliationDisplay")


        RequestItems = New RequestItems(AccountRequestNum)
        thisRequest = RequestItems.GetRequestItemBySeqNum(AccountSeqNum)

        'gvAppRequests.DataSource = RequestItems.RequestItems
        'gvAppRequests.DataBind()

        'If RequestItems.MissingDependencies.Count > 0 Then

        '    gvMissingDependency.DataSource = RequestItems.MissingDependencies
        '    gvMissingDependency.DataBind()


        'End If

        first_namelabel.Text = thisRequest.FirstName
        last_namelabel.Text = thisRequest.LastName
        requestor_namelabel.Text = thisRequest.RequestorName
        entered_datelabel.Text = thisRequest.EnteredDate
        AccountLabel.Text = thisRequest.ApplicationDesc
        AccountRequestType = thisRequest.AccountRequestCode
        AppNum = thisRequest.ApplicationNum

        FormViewRequest.AddSelectParm("@account_request_num", AccountRequestNum, SqlDbType.Int, 4)
        '   FormViewRequest.FillForm()


        'AccountRequestType = FormViewRequest.getFieldValue("account_request_type")

        If Not IsPostBack Then

            LoginIDTextBox.Text = thisRequest.LoginName
            CommentsTextBox.Text = thisRequest.AccountItemDesc

            FormSignOn.FillForm()

            CheckAffiliation()
            fillRequestItemsTable()

            If close_dateTextBox.Text <> "" Then
                Dim ctrl As Control = DataCheckingAndValidation.SetChildControlsRecursiveToEnabledFalse(tpDemographics, False)

            Else '' if this request is open 
                If providerrbl.Text = "Yes" Then
                    ' only 90 security level and above edit these fields
                    If Session("SecurityLevelNum") < 90 Then
                        Dim ctrl As Control = DataCheckingAndValidation.SetChildControlsRecursiveToEnabledFalse(tpDemographics, False)

                    End If
                End If
            End If

        End If


        btnRequestDetails.Attributes.Add("onclick", "newPopup('RequestDetail.aspx?RequestNum=" & AccountRequestNum & " ')")


    End Sub
    Protected Sub btnCreateSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCreateSubmit.Click

        Dim thisData As New CommandsSqlAndOleDb("UpdAccountItemsV3", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@account_request_seq_num", AccountSeqNum, SqlDbType.Int)
        thisData.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int)
        thisData.AddSqlProcParameter("@ApplicationNum", AppNum, SqlDbType.Int, 30)
        thisData.AddSqlProcParameter("@tech_client_num", Session("TechClientNum"), SqlDbType.NVarChar, 10)
        thisData.AddSqlProcParameter("@item_desc", CommentsTextBox.Text, SqlDbType.NVarChar, 255)
        thisData.AddSqlProcParameter("@account_request_type", AccountRequestType, SqlDbType.NVarChar, 255)
        thisData.AddSqlProcParameter("@login_name", LoginIDTextBox.Text, SqlDbType.NVarChar, 50)

        thisData.ExecNonQueryNoReturn()

        AccRequest = New AccountRequest(AccountRequestNum, Session("EmployeeConn"))

        If Session("environment").ToString.ToLower <> "testing" Then

            If Not AccRequest.CloseDate Is Nothing Then

                sendEmail()

            End If
        End If

        Response.Redirect("./CreateDeleteAccount.aspx?requestnum=" & AccountRequestNum & "&account_seq_num=" & AccountSeqNum, True)


    End Sub

    Protected Sub btnSaveItemInfo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveItemInfo.Click


        Dim thisData As New CommandsSqlAndOleDb("UpdAccountItemInfoV3", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@account_request_seq_num", AccountSeqNum, SqlDbType.Int)
        thisData.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int)
        thisData.AddSqlProcParameter("@ApplicationNum", AppNum, SqlDbType.Int, 30)
        thisData.AddSqlProcParameter("@item_desc", CommentsTextBox.Text, SqlDbType.NVarChar, 255)
        thisData.AddSqlProcParameter("@login_name", LoginIDTextBox.Text, SqlDbType.NVarChar, 50)

        thisData.ExecNonQueryNoReturn()



        Response.Redirect("~\MyAccountQueue.aspx")


    End Sub


    Private Sub sendEmail()

        Dim Request As New AccountRequest(AccountRequestNum, Session("EmployeeConn"))
        Dim RequestItems As New RequestItems(AccountRequestNum)

        Dim sSubject As String
        Dim sBody As String

        sSubject = "Account Request for " & Request.FirstName & " " & Request.LastName & " completed."

        sBody = "<!DOCTYPE html>" & _
"<html>" & _
"<head>" & _
"<style>" & _
"table" & _
"{" & _
"border-collapse:collapse;" & _
"font-family: 'Helvetica Neue', 'Lucida Grande', 'Segoe UI', Arial, Helvetica, Verdana, sans-serif;" & _
"}" & _
"table, th, td" & _
"{" & _
"border: 1px solid black;" & _
"padding: 3px" & _
"}" & _
"" & _
"</style>" & _
"</head>" & _
"" & _
"<body>" & _
"<table cellpadding='5px'>" & _
        "<tr>" & _
            "<td colspan='3' align='center'  style='font-size:large'>" & _
"               " & _
            "<b>Account Request Complete</b>" & _
"                 " & _
             "</td>    " & _
        "</tr>" & _
        "<tr>" & _
            "<td style='font-weight:bold' colspan='1'>Name:</td>" & _
            "<td colspan='2'>" & Request.FirstName & " " & Request.LastName & "</td>" & _
        "</tr>" & _
        "<tr>" & _
            "<td style='font-weight:bold' colspan='1'>Requested By:</td>" & _
            "<td colspan='2'>" & Request.RequestorName & "</td>" & _
        "</tr>" & _
           "<tr>" & _
            "<td style='font-weight:bold' colspan='1'>Requestor Email:</td>" & _
            "<td colspan='2'>" & Request.RequestorEmail & "</td>" & _
        "</tr>" & _
        "<tr>" & _
            "<td style='font-weight:bold' colspan='1'>Date Requested:</td>" & _
            "<td colspan='2'>" & Request.EnteredDate & "</td>" & _
        "</tr>" & _
"      " & _
"     " & _
"<tr style='font-weight:bold'><td>Account</td><td>Login</td><td>Comment</td></tr>"

        Dim sAccounts As String = ""

        For Each req As RequestItem In RequestItems.RequestItems

            sAccounts = sAccounts & "<tr><td>" & req.ApplicationDesc & "</td><td>" & req.LoginName & "</td><td>" & req.AccountItemDesc & "</td></tr>"



        Next
        sBody = sBody & sAccounts & "</table>" & _
                        "</body>" & _
                        "</html>"

        Dim sToAddress As String = Request.SubmitterEmail

        If sToAddress = "" Then
            sToAddress = Request.RequestorEmail

        End If

        Dim mailObj As New MailMessage()
        mailObj.From = New MailAddress("CSC@crozer.org")
        mailObj.To.Add(New MailAddress(sToAddress))
        mailObj.IsBodyHtml = True
        mailObj.Subject = sSubject
        mailObj.Body = sBody





        Dim smtpClient As New SmtpClient()
        smtpClient.Host = "ah1smtprelay01.altahospitals.com"

        Try

            smtpClient.Send(mailObj)
        Catch ex As Exception

            Dim mailObjErr As New MailMessage("Jeff.Mele@crozer.org", "Jeff.Mele@crozer.org", "Error Email", "Error Email Body")
            Dim smtpClienterr As New SmtpClient()
            smtpClienterr.Host = "ah1smtprelay01.altahospitals.com"
            smtpClienterr.Send(mailObjErr)

        End Try

    End Sub


    Protected Sub btnInvalid_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnInvalid.Click
        Dim thisData As New CommandsSqlAndOleDb("UpdValidateRequestItemsV4", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int)
        thisData.AddSqlProcParameter("@nt_login", Session("LoginID"), SqlDbType.NVarChar, 50)
        thisData.AddSqlProcParameter("@validate", "invalid", SqlDbType.NVarChar, 10)
        thisData.AddSqlProcParameter("@account_request_seq_num", AccountSeqNum, SqlDbType.Int)
        thisData.AddSqlProcParameter("@action_desc", CommentsTextBox.Text, SqlDbType.NVarChar, 255)

        thisData.ExecNonQueryNoReturn()

        Response.Redirect("~\MyAccountQueue.aspx")
    End Sub


    'Protected Sub gvApps_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvMissingDependency.RowCommand
    '    If e.CommandName = "Select" Then
    '        Dim ReqApplicationNum As String = gvMissingDependency.DataKeys(Convert.ToInt32(e.CommandArgument))("ApplicationNum").ToString()


    '        Dim thisdata As New CommandsSqlAndOleDb("InsAccountItemsV3", Session("EmployeeConn"))
    '        thisdata.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.Int, 20)
    '        thisdata.AddSqlProcParameter("@ApplicationNum", ReqApplicationNum, SqlDbType.Int, 8)
    '        thisdata.AddSqlProcParameter("@account_item_desc", "Request was sent because " & thisRequest.ApplicationDesc & " depends on it.", SqlDbType.NVarChar, 255)
    '        thisdata.AddSqlProcParameter("@validate", "validated", SqlDbType.NVarChar, 20)

    '        thisdata.ExecNonQueryNoReturn()

    '        RequestItems = New RequestItems(AccountRequestNum)
    '        gvAppRequests.DataSource = RequestItems.RequestItems
    '        gvAppRequests.DataBind()

    '        If RequestItems.MissingDependencies.Count > 0 Then

    '            gvMissingDependency.DataSource = RequestItems.MissingDependencies
    '            gvMissingDependency.DataBind()

    '        Else

    '            gvMissingDependency.Visible = False

    '        End If

    '        uplPage.Update()

    '    End If

    'End Sub
    Protected Sub fillRequestItemsTable()
        Dim thisData As New CommandsSqlAndOleDb("SelRequestItems", Session("EmployeeConn"))
        thisData.AddSqlProcParameter("@account_request_num", AccountRequestNum, SqlDbType.NVarChar)

        Dim iRowCount = 1

        Dim dt As DataTable
        dt = thisData.GetSqlDataTable

        Dim rwHeader As New TableRow

        Dim clAccountHeader As New TableCell
        Dim clRequestTypeHeader As New TableCell
        Dim clStatusHeader As New TableCell



        clAccountHeader.Text = "Account"
        clRequestTypeHeader.Text = "Request Type"
        clStatusHeader.Text = "Status"



        rwHeader.Cells.Add(clAccountHeader)
        rwHeader.Cells.Add(clRequestTypeHeader)
        rwHeader.Cells.Add(clStatusHeader)


        rwHeader.Font.Bold = True
        rwHeader.BackColor = Color.FromName("#006666")
        rwHeader.ForeColor = Color.FromName("White")
        rwHeader.Height = Unit.Pixel(36)

        tblRequestItems.Rows.Add(rwHeader)

        For Each drRow As DataRow In dt.Rows


            Dim rwData As New TableRow

            Dim clAccountData As New TableCell
            Dim clRequestTypeData As New TableCell
            Dim clStatusData As New TableCell
            Dim clRequestorData As New TableCell
            Dim clReqPhoneData As New TableCell




            clAccountData.Text = IIf(IsDBNull(drRow("ApplicationDesc")), "", drRow("ApplicationDesc"))
            clRequestTypeData.Text = IIf(IsDBNull(drRow("account_request_type")), "", drRow("account_request_type"))
            clStatusData.Text = IIf(IsDBNull(drRow("valid_cd_desc")), "", drRow("valid_cd_desc"))

            rwData.Cells.Add(clAccountData)
            rwData.Cells.Add(clRequestTypeData)
            rwData.Cells.Add(clStatusData)


            If iRowCount > 0 Then

                rwData.BackColor = Color.Bisque

            Else

                rwData.BackColor = Color.LightGray

            End If

            tblRequestItems.Rows.Add(rwData)

            iRowCount = iRowCount * -1

        Next
        tblRequestItems.CellPadding = 10
        tblRequestItems.Width = New Unit("100%")


    End Sub
    Private Sub CheckAffiliation()

        pnlAlliedHealth.Visible = False




        Select Case emp_type_cdrbl.SelectedValue

            Case "employee"

                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")

                'Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                'Dim cityReq As New RequiredField(citytextbox, "City")
                'Dim stateReq As New RequiredField(statetextbox, "State")
                'Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                'Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                'Dim faxqReq As New RequiredField(faxtextbox, "Fax")
                '  Dim locReq As New RequiredField(facility_cdddl, "Location")

                ' Dim endReq As New RequiredField(end_dateTextBox, "End Date")


                pnlAlliedHealth.Visible = False
                pnlProvider.Visible = False
                providerrbl.SelectedIndex = -1




            Case "student"

                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")
                Dim titleReq As New RequiredField(Titleddl, "Title")
                Dim specReq As New RequiredField(Specialtyddl, "Specialty")
                Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim faxqReq As New RequiredField(faxtextbox, "Fax")


                pnlAlliedHealth.Visible = False
                pnlProvider.Visible = False
                providerrbl.SelectedIndex = -1

                'Dim locReq As New RequiredField(facility_cdddl, "Location")

                'Dim endReq As New RequiredField(end_dateTextBox, "End Date")

            Case "physician"
                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")
                Dim titleReq As New RequiredField(Titleddl, "Title")
                Dim specReq As New RequiredField(Specialtyddl, "Specialty")
                Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim faxqReq As New RequiredField(faxtextbox, "Fax")

                pnlProvider.Visible = True

                pnlAlliedHealth.Visible = False
                providerrbl.SelectedValue = "Yes"

            Case "non staff clinician"
                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")
                Dim titleReq As New RequiredField(Titleddl, "Title")
                Dim specReq As New RequiredField(Specialtyddl, "Specialty")
                Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim faxqReq As New RequiredField(faxtextbox, "Fax")

                pnlProvider.Visible = True

                pnlAlliedHealth.Visible = False
                providerrbl.SelectedIndex = -1

            Case "resident"
                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")
                Dim titleReq As New RequiredField(Titleddl, "Title")
                Dim specReq As New RequiredField(Specialtyddl, "Specialty")
                Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim faxqReq As New RequiredField(faxtextbox, "Fax")

                pnlProvider.Visible = True

                pnlAlliedHealth.Visible = False
                providerrbl.SelectedValue = "Yes"



            Case "vendor"

                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")

                Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim vendorRequired As New RequiredField(VendorNameTextBox, "Vendor")


                pnlAlliedHealth.Visible = False
                pnlProvider.Visible = False
                providerrbl.SelectedIndex = -1


            Case "contract"

                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")

                Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")


                pnlAlliedHealth.Visible = False
                pnlProvider.Visible = False
                providerrbl.SelectedIndex = -1

            Case "allied health"

                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")
                Dim titleReq As New RequiredField(Titleddl, "Title")
                Dim specReq As New RequiredField(Specialtyddl, "Specialty")
                Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim faxqReq As New RequiredField(faxtextbox, "Fax")



                providerrbl.SelectedIndex = 0

                pnlAlliedHealth.Visible = True
                pnlProvider.Visible = True



                providerrbl.SelectedValue = "Yes"



            Case "other"

                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")

                Dim add1Req As New RequiredField(address_1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")



                pnlAlliedHealth.Visible = False
                pnlProvider.Visible = False
                providerrbl.SelectedIndex = -1



            Case Else


                Dim firstNameReq As New RequiredField(first_nameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(last_nameTextBox, "Last Name")


                pnlAlliedHealth.Visible = False
                pnlProvider.Visible = False
                providerrbl.SelectedIndex = -1



        End Select


    End Sub
End Class


