<%@ Page Language="VB"  MasterPageFile="~/CSCMaster.master" AutoEventWireup="false" CodeFile="HelpDisplay.aspx.vb" Inherits="HelpFiles_HelpDisplay" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

 <asp:UpdatePanel ID="mainupd" runat="server" >
  <ContentTemplate>
     <table>
        <tr>
          <td>
               <asp:Table ID = "tblHelpTopics" runat = "server">
                  </asp:Table>

          </td>
        </tr>
       
       </table>
    <div id="Content">
     <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional" Visible="false" >
        
        <ContentTemplate>
            <table>
                <tr>
                    <td>
                        <asp:Button ID="btnReturn" runat="server" Width="150px" Text="Return To Help Menu " />
                    </td>
                </tr>
            </table>
            <iframe id="pdfFrame" src="HelpMissing.aspx" runat="server" style="top:50pX"></iframe>
         </ContentTemplate>
        </asp:UpdatePanel>  
     </div>
</ContentTemplate>
</asp:UpdatePanel>

</asp:Content>
