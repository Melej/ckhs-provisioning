﻿Imports System.IO
Imports System.Data '
Imports System.Data.SqlClient
Imports App_Code
Imports AjaxControlToolkit

Partial Class ChangeAffiliation
    Inherits MyBasePage 'Inherits System.Web.UI.Page
    Dim FormSignOn As New FormSQL()
    Dim iClientNum As String
    Dim UserInfo As UserSecurity
    Dim rolenum As Integer
    Dim emptype As String




    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        iClientNum = Request.QueryString("ClientNum")

        UserInfo = New UserSecurity(Session("LoginID"), Session("EmployeeConn"))

        FormSignOn = New FormSQL(Session("EmployeeConn"), "SelClientByNumberV20", "UpdClientObjectByNumberV20", "", Page)
        FormSignOn.AddSelectParm("@client_num", iClientNum, SqlDbType.Int, 6)

        If Not IsPostBack Then
            Dim dbAccess As New CkhsEmployeeDbAccess(Session("EmployeeConn"))
            Dim ckhsEmployee = dbAccess.GetCkhsEmployee(iClientNum)


            Dim rblAffiliateBinder As New RadioButtonListBinder(emptypecdrbl, "SelAffiliationsV5", Session("EmployeeConn"))

            Dim rblNewAffiliateBinder As New RadioButtonListBinder(newAffiliationrbl, "SelAffiliationsV5", Session("EmployeeConn"))

            rblNewAffiliateBinder.BindData("RoleNum", "AffiliationDisplay")

            'If Session("SecurityLevelNum") < 90 Then
            Select Case CkhsEmployee.RoleNum
                Case "782", "1197", "1194"
                    rblAffiliateBinder.AddDDLCriteria("@type", "phys", SqlDbType.NVarChar)
                Case "1200", "1193"
                    If CkhsEmployee.SiemensEmpNum <> "" Then
                        rblAffiliateBinder.AddDDLCriteria("@type", "ckhsemp", SqlDbType.NVarChar)
                    Else

                        rblAffiliateBinder.AddDDLCriteria("@type", "nonEmp", SqlDbType.NVarChar)
                    End If
                Case "1189"
                    If CkhsEmployee.SiemensEmpNum <> "" Then
                        rblAffiliateBinder.AddDDLCriteria("@type", "ckhsemp", SqlDbType.NVarChar)
                    Else
                        rblAffiliateBinder.AddDDLCriteria("@type", "student", SqlDbType.NVarChar)

                    End If

                Case "1195", "1198", "1196", "1199"

                    If CkhsEmployee.SiemensEmpNum <> "" Then
                        rblAffiliateBinder.AddDDLCriteria("@type", "", SqlDbType.NVarChar)
                    Else
                        rblAffiliateBinder.AddDDLCriteria("@type", "contract", SqlDbType.NVarChar)

                    End If


                Case Else
                    ' if a Doctor master number exist
                    If CkhsEmployee.DoctorMasterNum > 0 Then
                        EmpDoclbl.Visible = True
                        EmpDoctorTextBox.Text = CkhsEmployee.DoctorMasterNum.ToString
                        EmpDoctorTextBox.Visible = True
                        If Session("SecurityLevelNum") < 90 Then
                            EmpDoctorTextBox.Enabled = False
                        End If

                    Else
                        rblAffiliateBinder.AddDDLCriteria("@type", "", SqlDbType.NVarChar)
                    End If

            End Select

            rblAffiliateBinder.ToolTip = False

            rblAffiliateBinder.BindData("RoleNum", "AffiliationDisplay") '"AffiliationDescLower",


            If RoleNumLabel.Text = "" Then

                RoleNumLabel.Text = ckhsEmployee.RoleNum
                emptypecdrbl.SelectedValue = ckhsEmployee.RoleNum
                PositionNumTextBox.Text = ckhsEmployee.PositionNum
            End If


            'emptypecdrbl.SelectedValue = ckhsEmployee.EmpTypeCd
            emptypecdLabel.Text = ckhsEmployee.EmpTypeCd
            HDemployeetype.Text = ckhsEmployee.EmpTypeCd

            ClientnameHeader.Text = ckhsEmployee.FullName
            ' Does not workk cant pass controler to class
            'dbAccess.CheckEmployeeType(HDemployeetype.Text)

            Dim provider As String
            provider = ckhsEmployee.Provider


            FormSignOn.FillForm()

        End If


        Dim ctrl As Control = DataCheckingAndValidation.SetChildControlsRecursiveToEnabledFalse(tpDemographics, False)
        emptypecdrbl.Visible = True
        newAffiliationrbl.Visible = True
        emptypecdrbl.Visible = True
        newAffiliationrbl.Enabled = True
        btnSubmitAffiliation.Enabled = True


    End Sub

    Protected Sub newAffiliationrbl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles newAffiliationrbl.SelectedIndexChanged

        rolenum = newAffiliationrbl.SelectedValue
        emptype = newAffiliationrbl.SelectedItem.ToString

        newRoleNum.Text = newAffiliationrbl.SelectedValue
        newEmpType.Text = newAffiliationrbl.SelectedItem.ToString

    End Sub

    Protected Sub btnSubmitAffiliation_Click(sender As Object, e As System.EventArgs) Handles btnSubmitAffiliation.Click

        Dim thisdata As New CommandsSqlAndOleDb("UpdClientRole", Session("EmployeeConn"))
        thisdata.AddSqlProcParameter("@client_num", iClientNum, SqlDbType.Int, 10)

        thisdata.AddSqlProcParameter("@rolenum", CInt(newRoleNum.Text), SqlDbType.Int, 10)
        thisdata.AddSqlProcParameter("@emp_type_cd", newEmpType.Text, SqlDbType.NVarChar, 25)
        thisdata.ExecNonQueryNoReturn()

        Response.Redirect("~/ClientDemo.aspx?ClientNum=" & iClientNum, True)


    End Sub
End Class
