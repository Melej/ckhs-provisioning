﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="false" CodeFile="ApplicationMaintenance.aspx.vb" Inherits="ApplicationMaintenance" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">


    <asp:UpdatePanel ID="uplAppMaintenance" runat ="server" >

<Triggers>
   
  <asp:AsyncPostBackTrigger ControlID="btnAddApp" EventName="Click" />
  <asp:AsyncPostBackTrigger ControlID="gvApps" EventName="SelectedIndexChanged" />

</Triggers>

<ContentTemplate>
<table class="wrapper" align="left">
<tr>
<td>


   <table width="100%" border ="3">
    <tr>
        <th colspan="2" align ="center"  class="tableRowHeader">
        
        Add Application
        </th>

    </tr>
    <tr>
        <td>
                Application Description 
        </td>
        <td>
            <asp:TextBox ID="ApplicationDescTextBox" runat = "server"></asp:TextBox>
        </td>
        
    </tr>
  
   <tr>
    <td>Entity / Hospital</td>
    <td>
        <asp:RadioButtonList ID = "rblEntityHospital" runat="server">
            <asp:ListItem Selected="True">All</asp:ListItem>        
            <asp:ListItem Value="Hospital">Hospital Specific</asp:ListItem>
            <asp:ListItem Value="Entity">Entity Specific</asp:ListItem>
        </asp:RadioButtonList>
    
    </td>
   </tr>
   <tr>
    <td colspan="2" align="center">
        <asp:Button ID = "btnAddApp" runat="server" Text= "Add Application" />
    
    </td>
   
   </tr>
   <tr>
    <td colspan="2" align="center">
        <asp:Label ID ="AddAppConfirmlabel" runat = "server"></asp:Label>
    </td>
   </tr>


   </table>




         <table width="100%" border ="3">
    <tr>
        <th align="center">
        
             Remove Application
        </th>

    </tr>
    <tr>
        <td>
      
            
         <asp:GridView ID="gvApps" runat="server" AllowSorting="True" 
                                    AutoGenerateColumns="False" BackColor="White" BorderColor="#336666"
                                    BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                                    EmptyDataText="No Applications Found" EnableTheming="False" DataKeyNames="ApplicationNum, ApplicationDesc"
                                    Font-Size="Medium" GridLines="Horizontal" Width="100%">
                                   
                                    <FooterStyle BackColor="White" ForeColor="#333333" />
                                    <HeaderStyle BackColor="#336666" Font-Bold="False" ForeColor="White" Font-Size="Small" HorizontalAlign="Left"    />                                    
                                    <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Left" Font-Size="Medium"/>
                                    <RowStyle BackColor="White" ForeColor="#333333" />
                                    <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                    <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                    <SortedAscendingHeaderStyle BackColor="#487575" />
                                    <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                    <SortedDescendingHeaderStyle BackColor="#275353" />

                                    <Columns>
                                    
                                          <asp:BoundField  DataField="ApplicationDesc"  HeaderText="Application"></asp:BoundField>
				                          <asp:CommandField ButtonType="Button" SelectText="Remove" ShowSelectButton= "true" />




                                  
                                  </Columns>

                                </asp:GridView>
		

	
        </td>
    </tr>
   </table>


</td>

</tr>

</table>

</ContentTemplate>


        </asp:UpdatePanel>


</asp:Content>

