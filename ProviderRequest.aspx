﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="false" CodeFile="ProviderRequest.aspx.vb" Inherits="ValidateRequest" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">


<asp:UpdatePanel ID="uplViewRequest" runat ="server" >


<ContentTemplate>


<table class="wrapper" style="border-style: groove" align="left">
<tr>
<td>


        <table border="3" style="empty-cells:show" cellpadding="5px">
           <tr>
               <th colspan = "4" class="tableRowHeader">
                   Provider Request
                </th>     
           </tr>
            <tr align="center">
                <td colspan="4" align="center">
                    <asp:Label ID = "lblRehire" runat = "server" ForeColor="Red" Font-Bold="true" Font-Size="X-Large" />
                </td>
            
            </tr>

           <tr >
            <td colspan = "4" align="left">
                <asp:Label ID = "lblValidation" runat = "server" ForeColor="Red"/>
            
            </td>
           </tr>
            <tr>
                  <td colspan = "4">

                        
                        <asp:Button ID = "btnValidate" runat = "server" Text = "Validate Request"  
                         CssClass="btnhov" BackColor="#006666"  Width="120px" />
                        

                        <asp:Button ID = "btnInValid" runat = "server" Text = "Mark Request In-Valid"  
                         CssClass="btnhov" BackColor="#006666"  Width="245px" />
                        

                       <asp:Button runat="server" ID ="btnRequestDetails" Text ="Request Details" 
                        CssClass="btnhov" BackColor="#006666"  Width="145px" />

                        <asp:Button ID = "btnCancel" runat = "server" Text = "Return"  
                            CssClass="btnhov" BackColor="#006666"  Width="145px" />

          
<%--                     <ajaxToolkit:ConfirmButtonExtender ID="btnCancelRequest_ConfirmButtonExtender" 
                        runat="server" ConfirmText="Do you wish to Canecl this retuest ?" Enabled="True" 
                        TargetControlID="btnInValid">
                    </ajaxToolkit:ConfirmButtonExtender>--%>

                    </td>
            
            </tr>
           <tr>
            <td style="font-weight:bold">
               Client Name
            </td>
             <td>
                <asp:Label ID = "first_namelabel" runat = "server"></asp:Label>&nbsp<asp:Label ID = "last_namelabel" runat = "server" ></asp:Label>
            </td>

          
            <td style="font-weight:bold" >
            Requestor Name
           
            </td><td >
                <asp:Label ID = "requestor_namelabel" runat = "server"></asp:Label>
            </td>
            


           </tr>
           <tr>
               
                 
                 <td style="font-weight:bold">
                 Start Date
                  </td>
                
                  <td>
                   <asp:Label ID = "start_datelabel" runat = "server"></asp:Label>
            
                    </td>
                <td style="font-weight:bold">
                    Submit Date
                </td>
               
                 <td>
                    <asp:Label ID = "entered_datelabel" runat = "server"></asp:Label>
                </td>
                 
            </tr>
            <tr>
                <td colspan = "4" class="tableRowSubHeader">
                 Information Needed
                </td>
            </tr>
            <tr>
             <td style="font-weight:bold">
                Doctor Number
                </td>
                <td colspan = "3">
                    <asp:TextBox ID = "doctor_master_numTextBox" runat = "server" MaxLength="6"></asp:TextBox>
                </td>
             </tr>
          <tr>
            <td colspan = "4">
           </td>
          </tr>
          <tr>
            <td colspan="4">

         <asp:Panel ID="pnlInvalidRequest" runat="server" Visible="False">
            <table  width="700px" bgcolor="Silver">
                <tr>
                    <th >
                    Invalid Request
                    </th>
                </tr>
                <tr>
                    <td>
                        Comments:
                    </td>
                </tr>
                <tr>
                <td>
                    <asp:TextBox ID = "txtInvalidComments" runat = "server" TextMode ="MultiLine" Width= "95%"></asp:TextBox>
                </td>
                </tr>

                                                                    
                                                                 
                                                               
                <tr>
                    <td align = "center" >
                                                                  
                    <asp:Button ID = "btnInvalidSubmit" runat = "server" Text="Submit Invalid" 
                         CssClass="btnhov" BackColor="#006666"  Width="155px" />

                   <asp:Button ID = "btnCloseNav" runat = "server" Text="Cancel"
                    CssClass="btnhov" BackColor="#006666"  Width="145px" />

                    </td>
                </tr>
                                
            </table>
         </asp:Panel>

            </td>
          </tr>
        </table>

<%--          <asp:Button ID="btnControlHidden" runat="server" style="display:none" />  
            
            
             <ajaxToolkit:ModalPopupExtender ID="mpeInvalidItem" runat="server"   
                                DynamicServicePath="" Enabled="True" TargetControlID="btnControlHidden"   
                                PopupControlID="pnlInvalidItem" BackgroundCssClass="ModalBackground"   
                                DropShadow="true" CancelControlID="btnCloseNav">  
             </ajaxToolkit:ModalPopupExtender>  --%>



</td>

</tr>

</table>

   
        
</ContentTemplate>


        </asp:UpdatePanel>


</asp:Content>

