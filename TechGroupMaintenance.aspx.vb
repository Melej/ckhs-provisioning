﻿Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports App_Code
Imports AjaxControlToolkit

Partial Class TechGroupMaintenance
    Inherits MyBasePage


    Dim sActivitytype As String = ""
    Dim cblBinder As New CheckBoxListBinder
    Dim UserInfo As UserSecurity

    Dim iGroupNum As Integer
    Dim iSecurityLevel As Integer


    Dim ClientNum As Integer
    Dim AccountCd As String
    Dim MaintType As String

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Session("LoginID") = "" Then

            Response.Redirect("http://enterprise.crozer.org", True)

        End If

        UserInfo = New UserSecurity(Session("LoginID"), Session("EmployeeConn"))
        Dim ddlBinder As New DropDownListBinder

        iSecurityLevel = UserInfo.SecurityLevelNum

        If Not IsPostBack Then

            bindTechGroups()


            ddlBinder = New DropDownListBinder(categorycdddl, "SelCategoryCodes", Session("CSCConn"))
            ddlBinder.AddDDLCriteria("@securitylevel", iSecurityLevel, SqlDbType.Int)
            ddlBinder.BindData("Category_cd", "category_desc")


            ddlBinder = New DropDownListBinder(Newcategorycdddl, "SelCategoryCodes", Session("CSCConn"))
            ddlBinder.AddDDLCriteria("@securitylevel", iSecurityLevel, SqlDbType.Int)
            ddlBinder.BindData("Category_cd", "category_desc")

            'Dim ddbTechGroup As New DropDownListBinder(TechGroup2Dddl, "SelTechGroup", Session("CSCConn"))
            'ddbTechGroup.BindData("group_num", "group_name")
            'gvTechGroups.SelectedIndex = -1


        End If

        If iSecurityLevel < 101 Then
            tpNewGroup.Visible = False
            gvTechGroups.Enabled = False
        End If

        If tabs.ActiveTabIndex <> 2 Then
            GroupNameTextBox2.Text = ""
            groupnumtextbox.Text = ""
            tabs.ActiveTabIndex = 0
            tpGroup.Visible = False
        End If

    End Sub
    Private Sub bindTechGroups()
        Dim thisData As New CommandsSqlAndOleDb("SelTechGroup", Session("CSCConn"))
        thisData.AddSqlProcParameter("@ntlogin", Session("LoginID"), SqlDbType.NVarChar, 50)

        Dim dt As DataTable

        dt = thisData.GetSqlDataTable

        gvTechGroups.DataSource = dt
        gvTechGroups.DataBind()


        gvTechGroups.SelectedIndex = -1



    End Sub

    Protected Sub gvTechGroups_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvTechGroups.RowCommand
        If e.CommandName = "Select" Then
            Dim icount As Integer = 0


            Dim groupNum As String = gvTechGroups.DataKeys(Convert.ToInt32(e.CommandArgument))("group_num").ToString()
            Dim displaydesc As String = gvTechGroups.DataKeys(Convert.ToInt32(e.CommandArgument))("displayDesc").ToString()
            Dim groupName As String = gvTechGroups.DataKeys(Convert.ToInt32(e.CommandArgument))("group_name").ToString()

            Dim idefaultCategoryCD As String = gvTechGroups.DataKeys(Convert.ToInt32(e.CommandArgument))("defaultCategoryCD").ToString()
            Dim displaystatus As String = gvTechGroups.DataKeys(Convert.ToInt32(e.CommandArgument))("displaystatus").ToString()


            GroupNameTextBox2.Text = groupName
            GroupNameTextBox2.Enabled = False

            groupnumtextbox.Text = groupNum

            If displaystatus = "queuecall" Then
                For Each cblItem As ListItem In GroupCheckBox.Items
                    cblItem.Selected = True 

                Next

            ElseIf displaystatus = "queueonly" Then
                GroupCheckBox.SelectedValue = "queueonly"

            ElseIf displaystatus = "oncallonly" Then
                GroupCheckBox.SelectedValue = "oncallonly"

            End If

            defaultcategorycd.Text = idefaultCategoryCD

            If displaystatus <> "oncallonly" Then

                If defaultcategorycd.Text <> "" Then
                    Try
                        categorycdddl.SelectedValue = defaultcategorycd.Text.Trim
                    Catch ex As Exception
                        categorycdddl.SelectedIndex = -1
                    End Try
                Else
                    categorycdddl.SelectedIndex = -1
                End If
            Else
                defcatlbl.Visible = False
                categorycdddl.Visible = False
            End If


            tpGroup.Visible = True
            tpAllGroups.Visible = False
            tpNewGroup.Visible = False


            tabs.ActiveTabIndex = 2


            gvTechGroups.SelectedIndex = -1



        End If

    End Sub

    Protected Sub btnReturn_Click(sender As Object, e As System.EventArgs) Handles btnReturn.Click

        Response.Redirect("./TechGroupMaintenance.aspx", True)

    End Sub

    Protected Sub btnUpdateGroup_Click(sender As Object, e As System.EventArgs) Handles btnUpdateGroup.Click
        Dim status As String = ""
        Dim icount As Integer = 0

        For Each cblItem As ListItem In GroupCheckBox.Items

            If cblItem.Selected = True Then
                icount = icount + 1
            End If

        Next


        If icount = 2 Then
            status = "queuecall"
        ElseIf GroupCheckBox.SelectedValue = "queueonly" Then
            status = "queueonly"

        ElseIf GroupCheckBox.SelectedValue = "oncallonly" Then
            status = "oncallonly"

        End If


        Dim thisdata As New CommandsSqlAndOleDb("UpdTechGroup", Session("CSCConn"))
        thisdata.AddSqlProcParameter("@group_num", groupnumtextbox.Text, SqlDbType.Int, 20)
        thisdata.AddSqlProcParameter("@status", status, SqlDbType.NVarChar, 35)
        thisdata.AddSqlProcParameter("@deactivate", deactivaterbl.SelectedValue, SqlDbType.NVarChar, 12)
        thisdata.AddSqlProcParameter("@defaultCategoryCd", Trim(defaultcategorycd.Text), SqlDbType.NVarChar, 12)

        thisdata.ExecNonQueryNoReturn()

        Response.Redirect("./TechGroupMaintenance.aspx", True)



    End Sub

    Protected Sub BtnAddReset_Click(sender As Object, e As System.EventArgs) Handles BtnAddReset.Click
        group_nameTextBox.Text = ""

        For Each cblItem As ListItem In DisplayStatuschb.Items
            cblItem.Selected = False

        Next



    End Sub

    Protected Sub BtnAddGroup_Click(sender As Object, e As System.EventArgs) Handles BtnAddGroup.Click
        Dim status As String = "queueonly"
        Dim icount As Integer = 0

        If group_nameTextBox.Text = "" Then
            Exit Sub

        End If

        For Each gplItem As ListItem In DisplayStatuschb.Items

            If gplItem.Selected Then
                icount = icount + 1
            End If

        Next


        If icount = 2 Then
            status = "queuecall"
        ElseIf DisplayStatuschb.SelectedValue = "queueonly" Then
            status = "queueonly"

        ElseIf DisplayStatuschb.SelectedValue = "oncallonly" Then
            status = "oncallonly"

        End If

        HDGroupStatus.Text = status


        Dim thisdata As New CommandsSqlAndOleDb("InsTechGroup", Session("CSCConn"))
        thisdata.AddSqlProcParameter("@group_name", group_nameTextBox.Text, SqlDbType.NVarChar, 50)
        thisdata.AddSqlProcParameter("@status", HDGroupStatus.Text, SqlDbType.NVarChar, 35)
        thisdata.AddSqlProcParameter("@defaultCategoryCd", NewDefaultCategoryCD.Text, SqlDbType.NVarChar, 12)

        thisdata.ExecNonQueryNoReturn()


        Response.Redirect("./TechGroupMaintenance.aspx", True)

    End Sub


    Protected Sub GroupCheckBox_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles GroupCheckBox.SelectedIndexChanged
        HDGroupStatus.Text = GroupCheckBox.SelectedValue

    End Sub
    Protected Sub categorycdddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles categorycdddl.SelectedIndexChanged


        defaultcategorycd.Text = categorycdddl.SelectedValue


    End Sub

    Protected Sub Newcategorycdddl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles Newcategorycdddl.SelectedIndexChanged
        NewDefaultCategoryCD.Text = Newcategorycdddl.SelectedValue

    End Sub
End Class
