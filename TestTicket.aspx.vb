﻿Imports App_Code
Imports System.Reflection
Imports System.ComponentModel
Imports System.Diagnostics

Partial Class TestTicket
    Inherits System.Web.UI.Page
    'Dim c3Requestor As New Client3
    Dim UserInfo As UserSecurity
    Dim ddlBinder As New DropDownListBinder
    'Dim frmHelper As New FormHelper
    Dim NewRequestForm As New FormSQL()
    Dim FormSignOn As New FormSQL()

    Dim Client3Contrl As New Client3Controller()
    Dim iClientNum As Integer
    Dim NewTicketRequest As Integer
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        Dim UserInfo = Session("objUserSecurity")

        If Request.QueryString("ClientNum") <> "" Then
            iClientNum = Request.QueryString("ClientNum")
            client_numlabel.Text = Request.QueryString("ClientNum")
        ElseIf Request.QueryString("ClientNum") = "" Then
            iClientNum = UserInfo.ClientNum
            client_numlabel.Text = UserInfo.ClientNum
        Else
            Response.Redirect("~/" & "SimlpleSearch.aspx", True)
        End If

        'If RequestType.Text <> "" Then
        '    RequestTyperbl.SelectedValue = RequestType.Text
        'End If


        Dim dbAccess As New CkhsEmployeeDbAccess(Session("EmployeeConn"))
        Dim ckhsEmployee = dbAccess.GetCkhsEmployee(iClientNum)

        ClientnameHeader.Text = ckhsEmployee.FullName

        FormSignOn = New FormSQL(Session("EmployeeConn"), "SelClientByNumberV4", "UpdClientObjectByNumberV6", "", Page)
        FormSignOn.AddSelectParm("@client_num", iClientNum, SqlDbType.Int, 6)

        FormSignOn.FillForm()

        'frmHelper.FillForm(clientInfo, tpDemographics, "Update")

        NewRequestForm = New FormSQL(Session("CSCConn"), "selTicketRFSByNum", "InsTicketRFS", "", Page)



        ip_addressTextBox.Text = Request.ServerVariables("Local_Addr").ToString
        If Not IsPostBack Then

            ddlBinder = New DropDownListBinder(category_cdddl, "SelCategoryCodes", Session("CSCConn"))
            ddlBinder.BindData("Category_cd", "category_desc")

            'ddlBinder = New DropDownListBinder(ddlTypeCd, "sel_category_type_codes", Session("EmployeeConn"))
            'ddlBinder.BindData("type_num", "type_cd")

            'ddlBinder = New DropDownListBinder(ddlItemCd, "sel_category_type_items", Session("EmployeeConn"))
            'ddlBinder.BindData("item_num", "item_cd")

            'ddlBinder = New DropDownListBinder(ddlChangeGroup, "sel_group_names", Session("EmployeeConn"))
            'ddlBinder.BindData("group_num", "group_name")

            'ddlBinder = New DropDownListBinder(ddlStatus, "sel_user_status_codes", Session("EmployeeConn"))
            'ddlBinder.BindData("status_cd", "status_desc")
        End If

        'If RequestType.Text <> "" Then

        '    If RequestTyperbl.SelectedValue = "ticket" Then

        '        ticketPnl.Visible = True
        '        rfsPnl.Visible = False


        '    ElseIf RequestTyperbl.SelectedValue = "rfs" Then
        '        rfsPnl.Visible = True
        '        ticketPnl.Visible = False

        '    End If

        'End If

    End Sub
    Protected Sub RequestTyperbl_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles RequestTyperbl.SelectedIndexChanged
        If RequestTyperbl.SelectedValue = "ticket" Then

            ticketPnl.Visible = True
            rfsPnl.Visible = False


        ElseIf RequestTyperbl.SelectedValue = "rfs" Then
            rfsPnl.Visible = True
            ticketPnl.Visible = False

        End If

    End Sub

End Class
