﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="false" CodeFile="ValidateAddAcctRequest.aspx.vb" Inherits="ValidateAddAcctRequest" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">


<asp:UpdatePanel ID="uplViewRequest" runat ="server" >



<ContentTemplate>


<table class="wrapper" style="border-style: groove" align="left">

<tr>
<td>


        <table border="3" style="empty-cells:show"  cellpadding ="5px">
                    <tr>
            
                <td  colspan = "4" align="center" >
                
                 <asp:Panel ID="PanInvalid" runat="server"    Visible="False" >
                    <table id="tblInvalid" runat="server" cellspacing="1" cellpadding="10" 
                            rules="none"  border="2" 
                            width="450px" style="border-style: groove; background-color: #C0C0C0">
                        <tr>
                            <td align="center" colspan="2" style="font-weight: bold; height: 25px;">
                                Provide Invalid Reason or Comment
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                             <asp:Label ID = "lblHiddenSeqNum" runat = "server" visible = "false"></asp:Label>
                            <asp:Label ID = "lblHiddenLogin" runat = "server" visible = "false"></asp:Label>

                            
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2">
                                    <asp:DropDownList ID="Invalidddl" runat="server" Width="250px">
                                    </asp:DropDownList>

                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button ID="btnSubmitInvalid" runat="server" BackColor="#00C000" Font-Bold="True"
                                    ForeColor="Black" Height="25px" Text='Submit'
                                    ToolTip="This will Identify this request as Invalid "/>
                            </td>
                            <td align="center" >
                                <asp:Button ID="btnCancelInvalid" runat="server" BackColor="DimGray" Font-Bold="True"
                                    ForeColor="White" Height="25px" Text="Cancel" Width="80px" />
                            </td>
                        </tr>
                        <tr>
                            <td align="center"  colspan="2">
                                    Comments:
                            </td>
                        </tr>
                        <tr>
                            <td  align="center"  colspan="2">
                                <asp:TextBox ID = "InvalidDescTextBox" runat="server"  Height="100px" Width="80%" />

                            </td>
        
                        </tr>
                    </table>
                </asp:Panel>
                </td>
            </tr>


           <tr>
                <th colspan = "4" class="tableRowHeader">
                    Validate Request
                </th>     
           </tr>
            <tr>
                  <td colspan = "4">
                  
                        <asp:Button ID = "btnValidate" runat = "server" Text = "Validate Request"  Width = "120px"/>
                        <asp:Button ID = "btnCancel" runat = "server" Text = "Cancel" Width = "120px" />
                                    
                    </td>
            
            </tr>
           <tr>
            <td style="font-weight:bold">
               Client Name
            </td>
             <td>
                <asp:Label ID = "first_namelabel" runat = "server"></asp:Label>&nbsp<asp:Label ID = "last_namelabel" runat = "server" ></asp:Label>
            </td>

          
           
            <td style="font-weight:bold" >
            Requestor Name
           
            </td><td >
                <asp:Label ID = "requestor_namelabel" runat = "server"></asp:Label>
            </td>
             


           </tr>
           <tr>

                 <td  style="font-weight:bold">
                    Request Type
                </td>     
        
                 <td colspan="3" >
                     <asp:Label ID = "RequestTypeDescLabel" runat = "server"></asp:Label>
                </td>    
                 
                 
            </tr>
            <tr>

                 <td style="font-weight:bold">
                 Start Date
                  </td>
                  <td>
                    <asp:Label ID = "start_datelabel" runat = "server"></asp:Label>
                </td>
                  

           
                <td style="font-weight:bold">
                    Submit Date
                </td>
               
                 <td>
                    <asp:Label ID = "entered_datelabel" runat = "server"></asp:Label>
                </td>
                 
            </tr>
      
           
            <tr>
             
                  <td style="font-weight:bold">
            
               <asp:Label Text="Current Username" runat="server" ></asp:Label>
            </td>
   

            <td colspan="3">

                <asp:Label ID ="UserNameLabel" runat = "server" />
              
            </td>

 
            </tr>
           
      
           
          <tr>
            
            <td colspan = "4">
            


                  <asp:GridView ID="gvAppRequests" runat="server" AllowSorting="True" 
                                    AutoGenerateColumns="False" BackColor="White" BorderColor="#336666"
                                    BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                                    EmptyDataText="No Accounts" EnableTheming="False" DataKeyNames = "account_request_seq_num, login_name, ApplicationNum"
                                    Font-Size="Medium" GridLines="Horizontal" Width="100%">
                                   
                                    <FooterStyle BackColor="White" ForeColor="#333333" />
                                    <HeaderStyle BackColor="#336666" Font-Bold="False" ForeColor="White" Font-Size="Small" HorizontalAlign="Left"    />                                    
                                    <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Left" Font-Size="Medium"/>
                                    <RowStyle BackColor="White" ForeColor="#333333" />
                                    <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                    <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                    <SortedAscendingHeaderStyle BackColor="#487575" />
                                    <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                    <SortedDescendingHeaderStyle BackColor="#275353" />

                                    <Columns>
                                                
                                                                                                    
                                                    <asp:CommandField ButtonType="Button" SelectText="Invalid" ShowSelectButton= "true"  ControlStyle-BackColor="#84A3A3" ItemStyle-Height="30px"/>
                                                      <asp:BoundField  DataField="ApplicationDesc"  HeaderText="Application" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>



                                  </Columns>
                                </asp:GridView>
            </td>
          
          </tr>
        </table>


<%--           <tr>
            <td colspan="4">
             <asp:Panel ID="pnlInvalidItem" runat="server" CssClass="ModalPopUpPanel" style="display:none">
                <table class="ModalPopupTable" width="100%">
                    <tr>
                        <th >
                        Invalid Request
                        </th>
                    </tr>
                    <tr>
                        <td align = "center" style="background-color:#006666; color:#efefef;">
                            <asp:Label ID = "lblInvalidRequestItem" runat = "server" Font-Bold="true"></asp:Label>

                        </td>
                    </tr>
                    <tr>
                        <td>
                            Comments:
                        </td>
                    </tr>
                    <tr>
                    <td>
                        <asp:TextBox ID = "txtInvalidComments" runat = "server" TextMode ="MultiLine" Width= "95%"></asp:TextBox>
                    </td>
                    </tr>
                    <tr>
                        <td align = "center" >
                        <asp:Button ID = "btnCloseNav" runat = "server" Text="Cancel" />
                                                                    
                                                                  
                        <asp:Button ID = "btnInvalidSubmit" runat = "server" Text="Invalid" />
                        </td>
                    </tr>
                                
                </table>
             </asp:Panel>

            
            </td>
           </tr>
--%>                                <%--onclick="btnInvalid_Click"--%>

<%--                                      <asp:TemplateField>  
                                                 <ItemTemplate>  
                                                         <asp:Button ID="btnLinkView" runat="server" onclick="btnInvalid_Click" Text="Invalid"  BackColor="#336666" ForeColor="White"></asp:Button> 
                                                 </ItemTemplate>
                                      </asp:TemplateField>

          <asp:Button ID="btnControlHidden" runat="server" style="display:none" />  
            
             <ajaxToolkit:ModalPopupExtender ID="mpeInvalidItem" runat="server"   
                                DynamicServicePath="" Enabled="True" TargetControlID="btnControlHidden"   
                                PopupControlID="pnlInvalidItem" BackgroundCssClass="ModalBackground"   
                                DropShadow="true" CancelControlID="btnCloseNav">  
             </ajaxToolkit:ModalPopupExtender>  
--%></td>

</tr>

</table>
</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>

