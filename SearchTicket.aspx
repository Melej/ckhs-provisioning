﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="false" CodeFile="SearchTicket.aspx.vb" Inherits="SearchTicket" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <style type="text/css">
        .style34
        {
            width: 189px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

    <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="100"  >
       <ProgressTemplate>
            <div id="IMGDIV"  align="center" valign="middle" runat="server" style="position:
                    absolute;left: 200px;top:75px;visibility:visible;vertical-align:middle;border-style:
                    inset;border-color:black;background-color:White;width:210px;height:75px; z-index: 999;" >
            Loading Data ...
                <img src="Images/AjaxProgress.gif" /><br />
            </div>
       </ProgressTemplate>
    </asp:UpdateProgress>


<asp:UpdatePanel ID="uplSearch" runat ="server" >

                    <Triggers>

                       <asp:AsyncPostBackTrigger ControlID="btnSearchTicket" EventName="Click" />

                    </Triggers>

<ContentTemplate>
<table style="border-style: groove" align="left" width="100%">
<tr >
<td>
  <asp:Panel ID="pansearch" runat="server" Width = "100%" DefaultButton="btnSearchTicket">

    <table width="100%" border ="2">
      <tr>
        <th align="center" colspan="4" class="tableRowHeader">
       
           CKHS OnLine CSC Center

        </th>

      </tr>
      <tr>
        <td align="left">

<%--          <ajaxToolkit:TabContainer ID="tabs" runat="server" ActiveTabIndex="0"
                Width="100%"  EnableTheming="False"  AutoPostBack="true"
                BackColor="#efefef">

                <ajaxToolkit:TabPanel ID="tpProjectlist" runat="server" TabIndex="0"  BackColor="Gainsboro" Visible = "True" EnableTheming="True" >
                    <HeaderTemplate > 
                        <asp:Label ID="Label4" runat="server" Text="Search Tickets & RFS" Font-Bold="true" ForeColor="Black"></asp:Label>
                    </HeaderTemplate>

                      <ContentTemplate>
--%>

                       <table width="100%"  style="empty-cells:show" >
                          <tr>
                            <td align="right" style="font-weight: bold">
                                <asp:Label ID="lastlbl" runat="server" Text = "Last Name"></asp:Label>
                            </td>
                            <td>
                             <asp:TextBox ID = "last_nameTextBox" runat = "server"></asp:TextBox>
                            </td>

                            <td align="right" style="font-weight: bold">
                                <asp:Label ID="requestlbl" runat="server" Text = "Ticket Number"></asp:Label>
                            </td>
                            <td class="style34">
                             <asp:TextBox ID = "request_numTextBox" runat = "server"></asp:TextBox>
                            </td>

                         </tr>
                          <tr>

                                  <td align="right" style="font-weight: bold">
                                    <asp:Label ID="keywordlbl" runat="server" Text = "KeyWord"></asp:Label>
         
                                </td>
                                <td>
                                   <asp:TextBox ID = "KeywordTextBox"  runat = "server" Width="350px"></asp:TextBox>
                                </td>
        
                                <td align="right" style="font-weight: bold">
                                    <asp:Label ID="activitylbl" runat="server" Text="Asset Tag"></asp:Label>
                                </td>
                               <td class="style34">
                                 <asp:TextBox ID = "AssetTextBox" runat = "server"></asp:TextBox>
                               </td>
      
                            </tr>
                            <tr>
                                    <td align="right" style="font-weight: bold">
                                    <asp:Label ID="CSCGruplbl" runat="server" Text = "CSC Group"></asp:Label>
                                    <asp:Label ID="HDCSCGrp" runat="server"  Visible="false"></asp:Label>
                                        <asp:Label ID="HDCSCGroupName" runat="server" Visible="false"></asp:Label>

                                </td>
                                <td>
                                        <asp:DropDownList runat="server" ID="cscgroupddl" AutoPostBack="False" Width="350px"></asp:DropDownList>
                                </td>
                                <td align="right" style="font-weight: bold">
                                    <asp:Label ID="CSCTechlbl" runat="server" Text = "CSC Tech"></asp:Label>
                                    <asp:Label ID="HDCSCTech" runat="server" Visible="false"></asp:Label>
                                    <asp:Label ID="HDTechname" runat="server" Visible="false"></asp:Label>

        
                                </td>
                                <td class="style34">
        
                                <asp:DropDownList runat="server" ID="CSCTechddl" AutoPostBack="False" ></asp:DropDownList>
                                </td>
        
                            </tr>
                            <tr>
                                <td colspan="2" style="font-size: medium; color: #008000; font-weight: bold" align="center">
                                        <asp:label ID="searchlbl" runat="server" Text = "Open and Closed Tickets & RFS " 
                                            ForeColor="#006666" />

                                </td>
                                <td>
                                    <asp:Label ID="cscstatlbl" runat="server" Text="Technician Status:"></asp:Label>
                                </td>
                                <td class="style34">
                            
                                            <asp:RadioButtonList ID = "CSCTyperbl" runat="server" 
                                            AutoPostBack="true" RepeatDirection="Horizontal">
                                            <asp:ListItem Selected="True" Value="active"> Active Techs</asp:ListItem>
                                            <asp:listItem Value="deactive">De-Active Techs</asp:listItem>
                                        </asp:RadioButtonList>
                                                <asp:Label ID="hdCSCStatus" Visible="false" runat="server"></asp:Label>
                                    </td>
        
                            </tr>
                            <tr align="center">
                                <td colspan="4" align="center" style="font-size: medium; color: #008000; font-weight: bold">
                                    <asp:Label ID="SearchStatuslbl" runat="server"  BorderColor="Red"></asp:Label>
                                </td>
                            </tr>

                            <tr>
                            <td align="center" colspan="4">
                                <asp:Label runat="server" ID = "lblValidation" ForeColor="Red" Font-Bold="true" Visible = "false" ></asp:Label>
                            </td>
                        </tr>
                            <tr>
                            <td  align="center" colspan="4">
                                <table>
                                    <tr>
                                        <td align="center">

                                                <asp:Button ID ="btnSearchTicket" runat = "server" Text ="Search"  
                                                CssClass="btnhov" BackColor="#006666"  Width="120px" />
                                        </td>
                                            <td>
                       
                                                <asp:Button ID="btnTicketDash" runat="server" Text="Ticket Dash Board "  Visible="false" CssClass="btnhov" BackColor="#006666" Width="200px" />
                       
                                            </td>

                                        <td align="center" >

                                                <asp:Button ID = "btnReset" runat= "server" Text="Reset"  
                                                CssClass="btnhov" BackColor="#006666"  Width="120px" />

                                        </td>
                                    </tr>
                                </table>
    
                            </td>
                            </tr>
                            <tr>
                            <td colspan="4">
                                <asp:Table ID="tblSearchByName" runat="server" Visible="false">
                
                                </asp:Table>

                                <asp:Table ID="tblByGroup" runat="server" Visible="false">
                
                                </asp:Table>        
                            </td>
                            </tr> 
                            <tr>
                            <td colspan ="4">
                                <asp:GridView ID="gvSearch" runat="server" AllowSorting="True"  AutoGenerateColumns="False" BackColor="LightGray" BorderColor="#336666"
                                    BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                                    EmptyDataText="No Items Found" EnableTheming="False" DataKeyNames="client_num"
                                    Font-Size="small" GridLines="Horizontal" Width="100%" AlternatingRowStyle-BackColor="Bisque" >
                                   
                                <FooterStyle BackColor="White" ForeColor="#333333" />
                                <HeaderStyle BackColor="#336666" Font-Bold="False" ForeColor="White" Font-Size="Small" HorizontalAlign="Left"    />                                    
                                <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Left" Font-Size="Medium"/>
                                <RowStyle BackColor="LightGray" ForeColor="#333333" />
                                <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                <SortedAscendingHeaderStyle BackColor="#487575" />
                                <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                <SortedDescendingHeaderStyle BackColor="#275353" />
                                <Columns>
                                    <asp:CommandField ButtonType="Button" SelectText="Select" ShowSelectButton= "true" ControlStyle-BackColor="#84A3A3" ItemStyle-Height="30px" />
                                    <asp:BoundField  DataField="fullname"  HeaderText="Full Name" SortExpression="fullname" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" ItemStyle-Height="35px" ></asp:BoundField>
   
                                    <asp:BoundField  DataField="phone"  HeaderText="Phone" SortExpression="phone" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
                                    <asp:BoundField  DataField="department_name"  HeaderText="Department" SortExpression="department_name" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
				                    <asp:BoundField  DataField="TicketStatus"  HeaderText="Ticket Status" SortExpression="TicketStatus" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
                
                                    <asp:TemplateField HeaderText = "Network Login" SortExpression="login_name" HeaderStyle-HorizontalAlign = "Left" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                                <asp:Label ID="lblLoginName" runat="server"><%# If(Eval("login_name"), "")%></asp:Label>                                                 
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
		
                            <asp:GridView ID="gvRequests" runat="server" AllowSorting="True" 
                                    AutoGenerateColumns="False" BackColor="LightGray" BorderColor="#336666"
                                    BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                                    EmptyDataText="No Items Found" EnableTheming="False" DataKeyNames="Request"
                                    Font-Size="small" GridLines="Horizontal" Width="100%" AlternatingRowStyle-BackColor="Bisque" >
                                   
                                <FooterStyle BackColor="White" ForeColor="#333333" />
                                <HeaderStyle BackColor="#336666" Font-Bold="False" ForeColor="White" Font-Size="Small" HorizontalAlign="Left"    />                                    
                                <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Left" Font-Size="small"/>
                                <RowStyle BackColor="LightGray" ForeColor="#333333" />
                                <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                <SortedAscendingHeaderStyle BackColor="#487575" />
                                <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                <SortedDescendingHeaderStyle BackColor="#275353" />

                                <Columns>
            
            
                                        <asp:TemplateField HeaderText="Request"  SortExpression="Request" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>

                                            <asp:HyperLink ID="HyperLink1" runat="server" 

                                                NavigateUrl='<% #Eval("Request", "~/ViewRequestTicket.aspx?RequestNum={0}") %>'

                                                Text='<% #Eval("Request") %>'>


                                            </asp:HyperLink>
                       
                                            </ItemTemplate>
                        

                                        </asp:TemplateField>

                    <%--                    <asp:CommandField ButtonType="Button" SelectText="request_num" ShowSelectButton= "true" ControlStyle-BackColor="#84A3A3" ItemStyle-Height="30px"  />                                                                                 
                    --%>            
                                        <asp:BoundField  DataField="fullname"  HeaderText="Full Name" SortExpression="fullname" 
                                            ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" ItemStyle-Height="25px" ItemStyle-Width="10%"></asp:BoundField>
                                        <asp:BoundField  DataField="current_activity"  HeaderText="Activity Desc" SortExpression="current_activity"  ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="75%" ItemStyle-Wrap="True" ControlStyle-Width="140px"></asp:BoundField>

                                        <asp:BoundField  DataField="entry_date"  HeaderText="Entered Date"  SortExpression="entry_date" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="10%"></asp:BoundField>
                                        <asp:BoundField  DataField="status"  HeaderText="Status"  SortExpression="status" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
                           
                    
                                </Columns>
                                </asp:GridView>

                                <asp:GridView ID="gvAssetView" runat="server" AllowSorting="True"  AutoGenerateColumns="False" BackColor="LightGrey" BorderColor="#336666"
                                    BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                                    EmptyDataText="No Items Found" EnableTheming="False" DataKeyNames="Request"
                                    Font-Size="small" GridLines="Horizontal" Width="100%" AlternatingRowStyle-BackColor="Bisque" >
                                   
                                <FooterStyle BackColor="White" ForeColor="#333333" />
                                <HeaderStyle BackColor="#336666" Font-Bold="False" ForeColor="White" Font-Size="Small" HorizontalAlign="Left"    />                                    
                                <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Left" Font-Size="Medium"/>
                                <RowStyle BackColor="LightGray" ForeColor="#333333" />
                                <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                <SortedAscendingHeaderStyle BackColor="#487575" />
                                <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                <SortedDescendingHeaderStyle BackColor="#275353" />
                                <Columns>

                                        <asp:TemplateField HeaderText="Request"  SortExpression="Request" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>

                                            <asp:HyperLink ID="HyperLink1" runat="server" 

                                                NavigateUrl='<% #Eval("Request", "~/ViewRequestTicket.aspx?RequestNum={0}") %>'

                                                Text='<% #Eval("Request") %>'>


                                            </asp:HyperLink>
                       
                                            </ItemTemplate>
                        

                                        </asp:TemplateField>
                                    <%--<asp:CommandField ButtonType="Button" SelectText="request_num" ShowSelectButton= "true" ControlStyle-BackColor="#84A3A3" ItemStyle-Height="30px" />
                    --%>
                                    <asp:BoundField  DataField="fullname"  HeaderText="Full Name" SortExpression="last_name" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" ItemStyle-Height="35px" ></asp:BoundField>
                                    <asp:BoundField  DataField="asset_tag"  HeaderText="Asset Tag" SortExpression="asset_tag" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
                                    <asp:BoundField  DataField="status"  HeaderText="Status"  SortExpression="status" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
                                    <asp:BoundField  DataField="current_group_name"  HeaderText="Group Assigned"  SortExpression="current_group_name" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>

                                </Columns>
                            </asp:GridView>

                                <asp:GridView ID="gvCSCGroup" runat="server" AllowSorting="True" 
                                    AutoGenerateColumns="False" BackColor="LightGrey" BorderColor="#336666"
                                    BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                                    EmptyDataText="No Items Found" EnableTheming="False" DataKeyNames="Request"
                                    Font-Size="small" GridLines="Horizontal" Width="100%" AlternatingRowStyle-BackColor="Bisque">
                                   
                                <FooterStyle BackColor="White" ForeColor="#333333" />
                                <HeaderStyle BackColor="#336666" Font-Bold="False" ForeColor="White" Font-Size="Small" HorizontalAlign="Left"    />                                    
                                <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Left" Font-Size="small"/>
                                <RowStyle BackColor="LightGray" ForeColor="#333333" />
                                <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                <SortedAscendingHeaderStyle BackColor="#487575" />
                                <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                <SortedDescendingHeaderStyle BackColor="#275353" />

                                <Columns>
                                                                            
                                        <asp:TemplateField HeaderText="Request"  SortExpression="Request" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>

                                            <asp:HyperLink ID="HyperLink1" runat="server" 

                                                NavigateUrl='<% #Eval("Request", "~/ViewRequestTicket.aspx?RequestNum={0}") %>'

                                                Text='<% #Eval("Request") %>'>


                                            </asp:HyperLink>
                       
                                            </ItemTemplate>
                        

                                        </asp:TemplateField>

                    
                                        <asp:BoundField  DataField="fullname"  HeaderText="Client Name" SortExpression="last_name" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" ItemStyle-Height="35px" ItemStyle-Width="10%"></asp:BoundField>

                                        <asp:BoundField  DataField="short_desc"  HeaderText="Short Desc" SortExpression="short_desc"  ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="50%" ItemStyle-Wrap="True" ControlStyle-Width="140px"></asp:BoundField>

                                        <asp:BoundField  DataField="entry_date"  HeaderText="Entered Date"  SortExpression="entry_date" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="10%"></asp:BoundField>
                                        <asp:BoundField  DataField="status"  HeaderText="Status"  SortExpression="status" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>

                                        <asp:BoundField  DataField="TechName"  HeaderText="Tech Name" SortExpression="TechName"  ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" ItemStyle-Height="35px" ></asp:BoundField>

                                        <asp:BoundField  DataField="entry_date"  HeaderText="Entered Date" SortExpression="entry_date"  ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
                                        <asp:BoundField  DataField="request_type"  HeaderText="Type" SortExpression="request_type" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>

                    
                                </Columns>
                                </asp:GridView>

                                <asp:GridView ID="gvTechView" runat="server" AllowSorting="True" 
                                    AutoGenerateColumns="False" BackColor="LightGray" BorderColor="#336666"
                                    BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                                    EmptyDataText="No Items Found" EnableTheming="False" DataKeyNames="Request"
                                    Font-Size="small" GridLines="Horizontal" Width="100%" AlternatingRowStyle-BackColor="Bisque">
                                   
                                <FooterStyle BackColor="White" ForeColor="#333333" />
                                <HeaderStyle BackColor="#336666" Font-Bold="False" ForeColor="White" Font-Size="Small" HorizontalAlign="Left"    />                                    
                                <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Left" Font-Size="small"/>
                                <RowStyle BackColor="LightGray" ForeColor="#333333" />
                                <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                <SortedAscendingHeaderStyle BackColor="#487575" />
                                <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                <SortedDescendingHeaderStyle BackColor="#275353" />

                                <Columns>
                                        <asp:TemplateField HeaderText="Request"  SortExpression="Request" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>

                                        <asp:HyperLink ID="HyperLink1" runat="server" 

                                            NavigateUrl='<% #Eval("Request", "~/ViewRequestTicket.aspx?RequestNum={0}") %>'

                                            Text='<% #Eval("Request") %>'>


                                        </asp:HyperLink>
                       
                                        </ItemTemplate>
                        

                                    </asp:TemplateField>

                    <%--                    <asp:CommandField ButtonType="Button" SelectText="Select" ShowSelectButton= "true" ControlStyle-BackColor="#84A3A3" ItemStyle-Height="30px"  />                                                                                 
                    --%>                  
                                        <asp:BoundField  DataField="fullname"  HeaderText="Full Name" SortExpression="last_name" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" ItemStyle-Height="35px" ItemStyle-Width="10%" ></asp:BoundField>
                                        
                                        <asp:BoundField  DataField="short_desc"  HeaderText="Short Desc" SortExpression="short_desc"  ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="50%" ItemStyle-Wrap="True" ControlStyle-Width="140px"></asp:BoundField>

                                        <asp:BoundField  DataField="entry_date"  HeaderText="Entered Date"  SortExpression="entry_date" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="10%"></asp:BoundField>
                                        <asp:BoundField  DataField="close_date"  SortExpression="close_date" HeaderText="Close Date" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="10%"></asp:BoundField>

                                        <asp:BoundField  DataField="status"  HeaderText="Status"  SortExpression="status" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>


<%--                                        <asp:BoundField  DataField="current_group_name" SortExpression="current_group_name" HeaderText="Group Assigned" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>


--%>                                    
                                        <asp:BoundField  DataField="request_type"  SortExpression="request_type" HeaderText="Type" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>

                             
                    
                                </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                      </table>

<%--                   </ContentTemplate>
                </ajaxToolkit:TabPanel>
--%>
<%--                <ajaxToolkit:TabPanel ID="tpNewProject" runat="server"  BackColor="Gainsboro" Visible = "True" EnableTheming="True" TabIndex="1" >
                       <HeaderTemplate > 
                          <asp:Label ID="tabLabel" runat="server" Text="Ticket DashBoard" Font-Bold="true" ForeColor="Black" ></asp:Label>
                     </HeaderTemplate>
                      <ContentTemplate>
                       <table>
                        <tr> 
                            <td>
                            </td>
                        </tr>
                        </table>
                    </ContentTemplate>
                 </ajaxToolkit:TabPanel>--%>

<%--                <ajaxToolkit:TabPanel ID="TabPanel1" runat="server"  BackColor="Gainsboro" Visible = "True" EnableTheming="True" TabIndex="2" >
                       <HeaderTemplate > 
                          <asp:Label ID="Label1" runat="server" Text="Projects" Font-Bold="true" ForeColor="Black" ></asp:Label>
                     </HeaderTemplate>
                      <ContentTemplate>
                       <table>
                        <tr>
                             <td>
                            </td>
                        </tr>
                        </table>
                    </ContentTemplate>
                 </ajaxToolkit:TabPanel>--%>

<%--           </ajaxToolkit:TabContainer>--%>

        </td>
      </tr>
     </table>
  </asp:Panel>
 </td>
</tr>

</table>

</ContentTemplate>

</asp:UpdatePanel>


</asp:Content>

