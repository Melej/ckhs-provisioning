﻿Imports System.IO
Imports System.Data '
Imports System.Data.SqlClient
Imports App_Code
Imports AjaxControlToolkit
Imports C1.Web.UI.Controls.C1Window

Partial Class InvalidAccountQueueV3
    Inherits MyBasePage '   Inherits System.Web.UI.Page


    '----------------
    ' Pat's logic
    '----------------

    Dim dsQueues As New DataSet("Queues")
    Dim cblBinder As New CheckBoxListBinder




    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        If Not IsPostBack Then

            GetQueueDataSet("invalid")
            fillQueueGrid()


        End If
    End Sub

    Private Function GetAccounts() As DataTable

        Dim thisData As New CommandsSqlAndOleDb("SelApplicationCodes", Session("EmployeeConn"))

        Return thisData.GetSqlDataTable

    End Function

    Public Sub GetQueueDataSet(ByVal requestType As String)


        For Each rwAccount As DataRow In GetAccounts.Rows
            ' If cblItem.Selected = True Then

            Dim thisData As New CommandsSqlAndOleDb("SelQueueByAppNumV3", Session("EmployeeConn"))
            thisData.AddSqlProcParameter("@ApplicationNum", rwAccount("ApplicationNum"), SqlDbType.NVarChar, 12)
            thisData.AddSqlProcParameter("@requestType", requestType, SqlDbType.NVarChar, 12)


            Dim dt As New DataTable
            Dim dtCopy As New DataTable

            dt = thisData.GetSqlDataTable


            dtCopy = dt.Copy
            dtCopy.TableName = rwAccount("ApplicationDesc")

            Try

                dsQueues.Tables.Add(dtCopy)
            Catch ex As Exception
                dtCopy.TableName = rwAccount("ApplicationDesc") & " " & rwAccount("ApplicationNum")

                dsQueues.Tables.Add(dtCopy)
            End Try







            ' End If





        Next


    End Sub

    Protected Sub fillQueueGrid()

        For Each tblTable As DataTable In dsQueues.Tables



            If tblTable.Rows.Count = 0 Then

                'Dim rwNoData As New TableRow
                'Dim clNoData As New TableCell

                'clNoData.ColumnSpan = 4
                'clNoData.Text = "No Queues Found"

                'rwNoData.Cells.Add(clNoData)
                'tblAccountQueues.Rows.Add(rwNoData)


            Else
                Dim rwHeader As New TableRow
                Dim rwColTitle As New TableRow
                Dim iRowCount As Integer = 1


                Dim clHeader As New TableCell
                Dim clReqNumTitle As New TableCell
                Dim clNameTitle As New TableCell
                Dim clLoginTitle As New TableCell
                Dim clDateEnteredTitle As New TableCell




                rwHeader.Font.Bold = True
                rwHeader.BackColor = Color.FromName("#FFFFCC")
                rwHeader.ForeColor = Color.FromName("Black")
                rwHeader.Font.Size = 12

                clHeader.ColumnSpan = 4
                clHeader.Text = tblTable.TableName & " Waiting for completion"

                rwHeader.Cells.Add(clHeader)

                tblAccountQueues.Rows.Add(rwHeader)


                rwColTitle.Font.Bold = True
                rwColTitle.BackColor = Color.FromName("#006666")
                rwColTitle.ForeColor = Color.FromName("White")
                rwColTitle.Height = Unit.Pixel(36)


                clReqNumTitle.Text = "Req #"
                clNameTitle.Text = "Client Name"
                clLoginTitle.Text = "Account Name"
                clDateEnteredTitle.Text = "Date Entered"

                rwColTitle.Cells.Add(clReqNumTitle)
                rwColTitle.Cells.Add(clNameTitle)
                rwColTitle.Cells.Add(clLoginTitle)
                rwColTitle.Cells.Add(clDateEnteredTitle)


                tblAccountQueues.Rows.Add(rwColTitle)

                For Each dtRow As DataRow In tblTable.Rows

                    Dim rwData As New TableRow
                    Dim clReqNum As New TableCell
                    Dim clName As New TableCell
                    Dim clLogin As New TableCell
                    Dim clDateEntered As New TableCell


                    clReqNum.Text = "<a href=""AccountRequestInfo2.aspx?RequestNum=" & dtRow("account_request_num") & """ ><u><b> " & dtRow("account_request_num") & "</b></u></a>"
                    clName.Text = dtRow("receiver_name")
                    clLogin.Text = IIf(IsDBNull(dtRow("login_name")), "", dtRow("login_name"))
                    clDateEntered.Text = IIf(IsDBNull(dtRow("entered_date")), "", dtRow("entered_date"))

                    rwData.Cells.Add(clReqNum)
                    rwData.Cells.Add(clName)
                    rwData.Cells.Add(clLogin)
                    rwData.Cells.Add(clDateEntered)

                    If iRowCount > 0 Then

                        ' rwData.BackColor = Color.Bisque

                        rwData.BackColor = Color.WhiteSmoke

                    Else

                        rwData.BackColor = Color.LightGray

                    End If

                    tblAccountQueues.Rows.Add(rwData)

                    iRowCount = iRowCount * -1
                Next


            End If

        Next



    End Sub





  
End Class
