﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="false" CodeFile="ProjectDetailTotalView.aspx.vb" Inherits="ProjectDetailTotalView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
	<script type="text/javascript" src="Scripts/FieldValidator.js"></script>
	<script type="text/javascript">
		function pageLoad(sender, args) {

			fieldValidator('textInputRequired', 'btnSubmit', 'lblValidation');

		}
	</script>

	<style type="text/css">
		.style2 {
			text-align: center;
			background-color: #FFFFCC;
			color: #336666;
			font-weight: bold;
			font-size: x-large;
			height: 43px;
		}

		.auto-style1 {
			height: 29px;
		}
	</style>
	<script type="text/javascript">


		$(function () {

			$(':text').bind('keydown', function (e) { //on keydown for all textboxes  

				if (e.keyCode == 13) //if this is enter key  

					e.preventDefault();

			});

		});



	</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
	<table width="100%">
		<tr>
			<td align="center">
				<asp:Button ID="BtnExcel" runat="server" Text="Excel"
					CssClass="btnhov" BackColor="#006666" Width="120px" />
				<asp:Button ID="btnReturn" runat="server" Text="Return"
					CssClass="btnhov" BackColor="#006666" Width="120px" />
				<asp:UpdatePanel ID="uplPanel" runat="server" UpdateMode="Conditional">
					<ContentTemplate>
						<div id="maindiv" runat="server" align="left">
							<table style="border-style: groove" align="left" width="100%">
								<tr>

									<td align="center">ProJect Hours
									</td>
								</tr>
								<tr>
									<td align="center">Employee Info
									</td>


								</tr>
								<tr>
									<td align="center">
										<table width="80%" cellpadding="5px">
											<tr>
												<td align="right">Name:
												</td>
												<td align="left">
													<asp:Label ID="empName" runat="server" Font-Bold="true" ForeColor="Black"></asp:Label>
												</td>
												<td align="right">Employee Number:
												</td>
												<td align="left">
													<asp:Label ID="siemensempnumLabel" runat="server" Font-Bold="true" ForeColor="Black"></asp:Label>
												</td>

											</tr>

											<tr>
												<td align="right">Entity - Department:

												</td>
												<td align="left">
													<asp:Label ID="DepartmentName" runat="server" Font-Bold="true" ForeColor="Black"></asp:Label>
												</td>
												<td align="right">Entity Department CD:
												</td>
												<td align="left">
													<asp:Label ID="EntityCd" runat="server" Font-Bold="true" ForeColor="Black"></asp:Label>
												</td>

											</tr>
											<tr align="center">
												<td colspan="4" align="center">

													<asp:GridView ID="gvprojectTotals" runat="server" AllowSorting="True"
														AutoGenerateColumns="False" BackColor="White" BorderColor="#336666"
														BorderStyle="Double" BorderWidth="3px" CellPadding="0"
														EmptyDataText="No Items Found"
														EnableTheming="False" Font-Size="Medium" GridLines="Horizontal" Width="50%">

														<FooterStyle BackColor="White" ForeColor="#333333" />
														<HeaderStyle BackColor="#336666" Font-Bold="False" Font-Size="Small"
															ForeColor="White" HorizontalAlign="Left" />
														<PagerStyle BackColor="#336666" Font-Size="Medium" ForeColor="White"
															HorizontalAlign="Left" />

														<RowStyle BackColor="White" ForeColor="#333333" />
														<SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
														<SortedAscendingCellStyle BackColor="#F7F7F7" />
														<SortedAscendingHeaderStyle BackColor="#487575" />
														<SortedDescendingCellStyle BackColor="#E5E5E5" />
														<SortedDescendingHeaderStyle BackColor="#275353" />
														<Columns>

															<%--															<asp:BoundField DataField="ProjectName" HeaderStyle-HorizontalAlign="Left"
																SortExpression="ProjectName" HeaderText="Project Name" ItemStyle-HorizontalAlign="Left" />--%>

															<asp:BoundField DataField="WorkDate" HeaderStyle-HorizontalAlign="Left"
																SortExpression="WorkDate" HeaderText="Date" ItemStyle-HorizontalAlign="Left" />
															<asp:BoundField DataField="TotalHours" HeaderStyle-HorizontalAlign="Left"
																SortExpression="TotalHours" HeaderText="Total Hours" ItemStyle-HorizontalAlign="Left" />

														</Columns>
													</asp:GridView>

												</td>
											</tr>
											<tr>
												<td colspan="4">

													<asp:Label ID="HDClientnum" runat="server" Visible="false"></asp:Label>
													<asp:Label ID="HDTechnum" runat="server" Visible="false"></asp:Label>
													<asp:Label ID="HDProjectNum" runat="server" Visible="false"></asp:Label>
													<asp:Label ID="HDWorkdatenum" runat="server" Visible="false"></asp:Label>
													<asp:Label ID="HDWeek1HoursWorked" runat="server" Visible="false"></asp:Label>
													<asp:Label ID="HDWeek2HoursWorked" runat="server" Visible="false"></asp:Label>
													<asp:Label ID="HDWeek3HoursWorked" runat="server" Visible="false"></asp:Label>
													<asp:Label ID="HDWeek4HoursWorked" runat="server" Visible="false"></asp:Label>
													<asp:Label ID="HDWeek5HoursWorked" runat="server" Visible="false"></asp:Label>

													<asp:Label ID="HDTypeReport" runat="server" Visible="false"></asp:Label>
													<asp:Label ID="HDRptClientnum" runat="server" Visible="false"></asp:Label>
													<asp:Label ID="HDRptprojectnum" runat="server" Visible="false"></asp:Label>

												</td>

											</tr>

										</table>


									</td>

								</tr>
							</table>
						</div>
					</ContentTemplate>
				</asp:UpdatePanel>
			</td>
		</tr>
	</table>

</asp:Content>

