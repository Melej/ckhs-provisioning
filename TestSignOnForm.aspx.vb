﻿Imports System.IO
Imports System.Data '
Imports System.Data.SqlClient
Imports App_Code
Imports AjaxControlToolkit
Imports C1.Web.UI.Controls.C1Window
Imports System.Net.Mail

Partial Class TestSignOnForm
    Inherits MyBasePage '   Inherits System.Web.UI.Page


    Dim sActivitytype As String = ""

    Dim FormSignOn As New FormSQL()
    Dim AccountRequestNum As Integer
    Dim Cred As String
    Dim Provider As String
    Dim Han As String
    Dim dtAdjustedRoles As DataTable
    Dim requiredFields As List(Of RequiredField)






    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        FormSignOn = New FormSQL(Session("EmployeeConn"), "SelAccountRequestV3", "InsAccountRequestV3", "", Page)
        Dim i As Double
        i = 1 / 0



        If Session("RequiredFields") IsNot Nothing Then

            requiredFields = Session("RequiredFields")

        End If

        'ScriptManager.RegisterStartupScript(Me.Page, Page.GetType, 

        Dim actDir As New ActiveDir("brep02")

        TextBox1.Text = actDir.UserAdEntry.Properties.Item("mail").Value.ToString()




    End Sub
   

    Protected Sub btnReturn_Click(sender As Object, e As System.EventArgs) Handles btnReturn.Click
        Dim s As String



        If lblvalidate.Text = "invalid" Then

            s = "asdfadsf"
        Else
            s = "pass"
        End If



    End Sub
    Protected Sub btnTestEmail_Click(sender As Object, e As System.EventArgs) Handles btnTestEmail.Click
  
        sendAlertEmail()


    End Sub
    Private Sub sendEmail()
        '547131
        Dim Request As New AccountRequest(547864, Session("EmployeeConn"))
        Dim RequestItems As New RequestItems(547864)
        Dim EmailBody As New EmailBody
    '    Dim sCss As String = File.ReadAllText(Server.MapPath("/Styles/Tables.css"))



        Dim sSubject As String
        Dim sBody As String

        If Not Request.CloseDate Is Nothing Then

            Dim sString As String = "not closed"


        End If

        sSubject = "Account Request for " & Request.FirstName & " " & Request.LastName & " completed."

      



        sBody = "<!DOCTYPE html>" & _
"<html>" & _
"<head>" & _
"<style>" & _
"table" & _
"{" & _
"border-collapse:collapse;" & _
"font-family: 'Helvetica Neue', 'Lucida Grande', 'Segoe UI', Arial, Helvetica, Verdana, sans-serif;" & _
"}" & _
"table, th, td" & _
"{" & _
"border: 1px solid black;" & _
"padding: 3px" & _
"}" & _
"</style>" & _
"</head>" & _
"" & _
"<body>" & _
 EmailBody.EmailCompletedRequestTable(Request, RequestItems) & _
                        "</body>" & _
                        "</html>"

       




        Dim sToAddress As String = Request.SubmitterEmail

        If sToAddress = "" Then
            sToAddress = Request.RequestorEmail

        End If
        sToAddress = "Patrick.Brennan@crozer.org"

        Dim mailObj As New MailMessage()
        mailObj.From = New MailAddress("CSC@crozer.org")
        mailObj.To.Add(New MailAddress(sToAddress))
        mailObj.IsBodyHtml = True
        mailObj.Subject = sSubject
        mailObj.Body = sBody





        Dim smtpClient As New SmtpClient()
        smtpClient.Host = "ah1smtprelay01.altahospitals.com"

        Try

            smtpClient.Send(mailObj)
        Catch ex As Exception

            'Dim mailObjErr As New MailMessage("Patrick.Brennan@crozer.org", "Patrick.Brennan@crozer.org", "Error Email", "Error Email Body")
            'Dim smtpClienterr As New SmtpClient()
            'smtpClienterr.Host = "Smtp2.Crozer.Org"
            'smtpClienterr.Send(mailObjErr)
        End Try

    End Sub
    Private Sub sendAlertEmail()
        '547131
        Dim Request As New AccountRequest(547864, Session("EmployeeConn"))
        Dim RequestItems As New RequestItems(547864)
        Dim EmailBody As New EmailBody
        '    Dim sCss As String = File.ReadAllText(Server.MapPath("/Styles/Tables.css"))



        '======================
        ' Get current path
        '======================

        Dim path As String
        Dim directory As String = ""
        path = HttpContext.Current.Request.Url.AbsoluteUri
        Dim pathParts() As String = Split(path, "/")
        Dim i As Integer
        Do While i < pathParts.Length() - 1

            directory = directory + pathParts(i) + "/"
            i = i + 1

        Loop

        '======================

        Dim EmailList As New List(Of EmailDistribution)
        EmailList = New EmailDistributionList().EmailList

        Dim sndEmailTo = From r In Request.RequestItems()
                         Join e In EmailList
                         On r.AppBase Equals e.AppBase
                         Where e.EmailEvent = "AddAcct"
                         Select e

       


        For Each EmailAddress In sndEmailTo



            Dim toEmail As String = EmailAddress.Email

        Next


        For Each ReqItem As RequestItem In RequestItems.RequestItems()



            Dim sSubject As String
            Dim sBody As String


            sSubject = "Account Request for " & Request.FirstName & " " & Request.LastName & " has been added to the " & ReqItem.ApplicationDesc.ToUpper() & " Queue."


            sBody = "<!DOCTYPE html>" & _
                                "<html>" & _
                                "<head>" & _
                                "<style>" & _
                                "table" & _
                                "{" & _
                                "border-collapse:collapse;" & _
                                "font-family: 'Helvetica Neue', 'Lucida Grande', 'Segoe UI', Arial, Helvetica, Verdana, sans-serif;" & _
                                "}" & _
                                "table, th, td" & _
                                "{" & _
                                "border: 1px solid black;" & _
                                "padding: 3px" & _
                                "}" & _
                                "</style>" & _
                                "</head>" & _
                                "" & _
                                "<body>" & _
                                "A new account request has been added to the " & ReqItem.ApplicationDesc & " Queue.  Follow the link below to go to Queue Screen." & _
                                 "<br />" & _
                                "<a href=""" & directory & "/CreateAccount.aspx?requestnum=" & ReqItem.AccountRequestNum & "&account_seq_num=" & ReqItem.AccountRequestSeqNum & """ ><u><b> " & ReqItem.ApplicationDesc.ToUpper() & ": " & Request.FirstName & " " & Request.LastName & "</b></u></a>" & _
                                "</body>" & _
                            "</html>"






            Dim sToAddress As String = Request.SubmitterEmail

            If sToAddress = "" Then
                sToAddress = Request.RequestorEmail

            End If
            sToAddress = "Jeff.Mele@crozer.org"

            Dim mailObj As New MailMessage()
            mailObj.From = New MailAddress("CSC@crozer.org")
            mailObj.To.Add(New MailAddress(sToAddress))
            mailObj.IsBodyHtml = True
            mailObj.Subject = sSubject
            mailObj.Body = sBody


            Dim smtpClient As New SmtpClient()
            smtpClient.Host = "ah1smtprelay01.altahospitals.com"



            smtpClient.Send(mailObj)




        Next




    End Sub
End Class
