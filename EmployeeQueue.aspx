﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="false" CodeFile="EmployeeQueue.aspx.vb" Inherits="EmployeeQueue" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
<table>
<tr>
    <td>
        <asp:Button runat="server" id="BtnExcel" Text="Excel" />
    </td>
</tr>

</table>
<asp:UpdatePanel ID="uplCredQueue" runat ="server" >

<Triggers>

</Triggers>
<ContentTemplate>
    <table style="border-style: groove" align="left" width="100%" cellpadding="5px">
        <tr>
           <td class="tableRowHeader">
               Employee Queue (Waiting for Update & Apps)
           </td>
        </tr>
        <tr >
                <td>
                        <asp:Table ID = "tblEmployeeQueue" runat = "server" Width="100%">


                        </asp:Table>
                </td>
        </tr>
    </table>
</ContentTemplate>

</asp:UpdatePanel>
</asp:Content>

