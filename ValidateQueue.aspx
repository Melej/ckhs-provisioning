﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="false" CodeFile="ValidateQueue.aspx.vb" Inherits="ValidateQueue" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">


<asp:UpdatePanel ID="uplSignOnForm" runat ="server" >

<Triggers>

  <%-- <asp:AsyncPostBackTrigger ControlID="dgValidateQueue" EventName="SelectedIndexChanged" />--%>

</Triggers>

<ContentTemplate>
<table style="border-style: groove" align="left" width="100%" >
<tr>
   <td colspan = "4" class="tableRowHeader">
     Validate Queue
   </td>
</tr>
<tr >
<td>

<asp:Table ID = "tblValidateQueue" runat = "server" Width="100%">


</asp:Table>




</td>
</tr>
<tr>
<td>
<asp:Label ID="lblAccountExist" runat="server"></asp:Label>
</td>
</tr>

<tr>
<td>
<asp:Label ID="lblHeader" runat="server"></asp:Label>
</td>
</tr>
<tr>
<td>




<%--
 <asp:GridView ID="dgSimilarAccounts" runat="server" AllowSorting="True" 
                                    AutoGenerateColumns="False" BackColor="White" BorderColor="#336666"
                                    BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                                    EmptyDataText="No Similar Accounts" EnableTheming="False"
                                    PageSize="5" AllowPaging="True"                                     
                                    Font-Size="Medium" GridLines="Horizontal" Width="100%"  >
                                   
                                    <FooterStyle BackColor="White" ForeColor="#333333" />
                                    <HeaderStyle BackColor="#336666" Font-Bold="False" ForeColor="White" Font-Size="Small" HorizontalAlign="Left"    />                                    
                                    <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Left" Font-Size="Medium"/>
                                    <RowStyle BackColor="White" ForeColor="#333333" />
                                    <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                    <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                    <SortedAscendingHeaderStyle BackColor="#487575" />
                                    <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                    <SortedDescendingHeaderStyle BackColor="#275353" />

                                    <Columns>

                                         	<asp:BoundField DataField="first_name" SortExpression="first_name" HeaderText="First Name"></asp:BoundField>
					                        <asp:BoundField DataField="last_name" SortExpression="last_name" HeaderText="Last Name"></asp:BoundField>
					                        <asp:BoundField DataField="login_name" SortExpression="login_name" HeaderText="Logon"></asp:BoundField>
					                        <asp:BoundField DataField="phone" SortExpression="phone" HeaderText="Phone"></asp:BoundField>
					                        <asp:BoundField DataField="department_name" SortExpression="department_name" HeaderText="Department"></asp:BoundField>


                                  
                                  </Columns>

                                </asp:GridView>--%>



        <asp:Table ID = "tblSimilarAccounts" runat = "server">


        </asp:Table>


            	<asp:DataGrid id="dgExistingAccounts" runat="server" Width="712px" BackColor="White" BorderColor="#336666" BorderStyle="Double" BorderWidth="3px" 
                CellPadding="4" GridLines="Horizontal"  AutoGenerateColumns="False" Font-Size="X-Small" Visible = "false">
				<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#339966"></SelectedItemStyle>
				<AlternatingItemStyle BackColor="#FFE0C0"></AlternatingItemStyle>
				<ItemStyle ForeColor="#333333" BackColor="White"></ItemStyle>
				<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#336666"></HeaderStyle>
				<FooterStyle ForeColor="#333333" BackColor="White"></FooterStyle>
				<Columns>
					<asp:BoundColumn DataField="account_type_cd" SortExpression="account_type_cd" HeaderText="Account Type"></asp:BoundColumn>
					<asp:BoundColumn DataField="login_name" SortExpression="login_name" HeaderText="Logon"></asp:BoundColumn>
					<asp:BoundColumn DataField="create_by_name" SortExpression="create_by_name" HeaderText="Created By"></asp:BoundColumn>
					<asp:BoundColumn DataField="created_date" SortExpression="created_date" HeaderText="Created Date"></asp:BoundColumn>
				</Columns>
				<PagerStyle HorizontalAlign="Center" ForeColor="White" BackColor="#336666" Mode="NumericPages"></PagerStyle>
			</asp:DataGrid>


</td>

</tr>
<tr>
    <td>
        <asp:Button ID ="btnContinue" runat="server" Text="Continue" Visible="false" />
        <asp:Button ID ="btnInvalid" runat = "server" Text ="Invalid" Visible="false"/>

<%--        <ajaxToolkit:ConfirmButtonExtender ID="btnSaveConfirmExtender" 
               runat="server" ConfirmText="This will mark the Entire Request Invalid. Would you like to Confirm?" 
              Enabled="True" TargetControlID="btnInvalid">
       </ajaxToolkit:ConfirmButtonExtender>
--%>

    </td>

</tr>
<tr>
<td colspan="4" align="center">

 <asp:Panel ID="PanInvalid" runat="server" Visible="False" >
    <table id="tblInvalid" runat="server" cellspacing="1" cellpadding="10" 
        bgcolor="gainsboro" bordercolor="navy"  rules="none"  border="2" 
            width="350Px">
        <tr>
            <td align="center" colspan="2" style="font-weight: bold; height: 25px;">
                Provide Invalid Reason or Comment</td>
        </tr>
        <tr>
            <td align="center" colspan="2">
                    <asp:DropDownList ID="Invalidddl" runat="server" Width="250px">
                    </asp:DropDownList>

            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Button ID="btnSubmitInvalid" runat="server" BackColor="#00C000" Font-Bold="True"
                    ForeColor="Black" Height="25px" Text='Submit'
                    ToolTip="This will Identify this request as Invalid "/>
            </td>
            <td align="center" >
                <asp:Button ID="btnCancelInvalid" runat="server" BackColor="DimGray" Font-Bold="True"
                    ForeColor="White" Height="25px" Text="Cancel" Width="80px" />
            </td>
        </tr>
        <tr>
            <td align="center"  colspan="2">
                    Comments:
            </td>
        </tr>
        <tr>
            <td  align="center"  colspan="2">
                <asp:TextBox ID = "InvalidDescTextBox" runat="server"  Height="100px" Width="80%" />

            </td>
        
        </tr>
    </table>
</asp:Panel>
    
    </td>
</tr>  

</table>

</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>

