﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="false" CodeFile="AccountRequestInfo.aspx.vb" Inherits="AccountRequestInfo" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

<asp:UpdatePanel ID="mainupd" runat="server" >
   
<ContentTemplate>


<table width="100%">

<tr>

<td>
             <ajaxToolkit:TabContainer ID="tabs" runat="server" ActiveTabIndex="0"
                Width="100%" style="margin-bottom: 1px" EnableTheming="False" 
                BackColor="#efefef">

                  <ajaxToolkit:TabPanel ID="tpDemographics" runat="server" TabIndex="0"  BackColor="Gainsboro" Visible = "True">


                      <HeaderTemplate > 
                     
                            <asp:Label ID="Label1" runat="server" Text="Demographics" Font-Bold="true"></asp:Label>
            
                     
                      </HeaderTemplate>

                      <ContentTemplate>
                        <table border="3">
                                    <tr>
                                        <td colspan = "4" align ="center">
                                            <asp:Label ID = "lblValidation" runat = "server" ForeColor="Red"/>
                                        </td>
            
                                    </tr>
                                    <%--<tr>
                                        <td colspan = "4">
                                            <asp:Button ID = "btnSubmit" runat = "server" Text ="Submit" Width="75" />
                                            <asp:Button ID = "btnReturn" runat = "server" Text ="Return" Width="75" />
                                        </td>
            
                                    </tr>--%>
                                    <tr>
                                             <td colspan = "4" class="tableRowHeader">
                                                Account Request Information
                                             </td>
                                    </tr>
                                    <tr>
          
                                        <td colspan="4" class="tableRowSubHeader"  align="left">
                                       Form Submission
                                        </td>
            
                                    </tr>
                                    <tr>
                                        <td colspan = "4" align="center">
                                            <asp:Button ID = "btnUpdate" runat = "server" Text ="Update" Width="75" BackColor="#336666" ForeColor="White" />
              
                                        </td>
                                    </tr>
                                    <tr>
                                    <td colspan="4">
                                       User submitting:         
                                       <asp:Label ID = "submitter_nameLabel" runat="server" Font-Bold="true"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                        Requestor:   <asp:Label ID = "requestor_nameLabel" runat="server" Font-Bold="true"></asp:Label>
            
              
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" class="tableRowSubHeader"  align="left">
                                        CKHS Affiliation:
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                            <asp:RadioButtonList ID ="emp_type_cdrbl" runat = "server" RepeatDirection="Horizontal" AutoPostBack="true">
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>


                                  <tr>
                                   <td colspan="4">
<%--                                     <asp:RadioButtonList ID ="RadioButtonList1" runat = "server" RepeatDirection="Horizontal" AutoPostBack="true">
                                     </asp:RadioButtonList>

--%>  
                                     <asp:Panel runat="server" ID ="pnlAlliedHealth" Visible="false">
                                        <table width="100%">
                                          <tr>
                                            <td width="30%">
                                                 Allied Health Degree:
                                            </td>
                                            <td>
                                               <asp:DropDownList runat="server"  width="40%" ID = "OtherAffDescddl">
                                                </asp:DropDownList> 
                                            </td>
                                          </tr>
                
                                        </table>
                                     </asp:Panel>
                                    </td>
                                  </tr>

                                    <asp:Panel runat="server" ID="pnlProvider" Visible="false">
                                      <tr>
                                        <td colspan="4" class="tableRowSubHeader" align="left">
                                            Provider Information
                                        </td>
                                      </tr>

                                   
                                      <tr>
                                         <td>
                                                Provider:
                                          </td>
                                           <td colspan="3">
                                            <asp:RadioButtonList ID = "providerrbl" runat="server" RepeatDirection="Horizontal">
                                                <asp:ListItem>Yes</asp:ListItem>
                                                    <asp:ListItem >No</asp:ListItem>            
                                                </asp:RadioButtonList>
                                            </td>

                                     </tr>
                                      <tr>
                                        <td>
                                        Credentialed Date:
                                        </td>
                                        <td colspan="3">
                                            <asp:TextBox ID = "credentialedDateTextBox" runat="server" CssClass="calenderClass"></asp:TextBox>
                                        </td>
                       
                                     </tr>
                                      <tr>
                                        <td>
                                         CKHN-HAN:
                                        </td>
                                        <td colspan="3">
                                            <asp:RadioButtonList ID = "hanrbl" runat="server" RepeatDirection="Horizontal">
                                                <asp:ListItem>Yes</asp:ListItem>
                                                <asp:ListItem Selected="True">No</asp:ListItem>            
                                            </asp:RadioButtonList>
                                        </td>
                    
                                     </tr>
                                      <tr>
                                        <td>
                                            NPI
                                        </td>
                                        <td >
                                            <asp:TextBox ID = "npitextbox" runat = "server"></asp:TextBox>
                                        </td>
                                        <td>
                                            License No.
                                        </td>
                                        <td>
                                             <asp:TextBox ID = "LicensesNo" runat="server"></asp:TextBox>
                                        </td>
                                     </tr>
                                      <tr>
                                        <td>
                                            Taxonomy
                                         </td>
                                        <td colspan ="3">
                                            <asp:TextBox ID = "taxonomytextbox" runat="server">
                                          </asp:TextBox>
                                        </td>
        
                                    </tr>
                                      <tr>
                                          <td>
                                            Group Name

                                            </td>
                                            <td>
                                                <asp:TextBox ID="group_nameTextBox" runat="server"></asp:TextBox>
                                            </td>
                                            <td>
                                                Group Number

                                            </td>
                                            <td>
                                                <asp:TextBox ID="group_numTextBox" runat="server"></asp:TextBox>
                                            </td>
                                    </tr>
                                      <tr>
                                        <td>
                                        Practice Name:
                                        </td>       
                                        <td colspan="3">
                                        <asp:DropDownList ID ="practice_numddl"  width = "95%" runat="server">
            
                                        </asp:DropDownList>
                                        </td>
                                    </tr>
                                      <tr>
                                       <td>Title:</td>
                                        <td>
                                           <asp:dropdownlist ID = "Titleddl" runat="server"></asp:dropdownlist>
                                        </td>
                                        <td >
                                        Specialty
                                        </td>
                                        <td>
                                           <asp:dropdownlist ID = "Specialtyddl" runat="server"></asp:dropdownlist>
                                        </td>
                                    </tr>
                                      <tr>
                                        <td>
                                            Non-CKHN Location
                                        </td>
                                        <td colspan="3">
                                              <asp:TextBox ID = "NonHanLocationTextBox" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                   
                                   </asp:Panel> 
                                      <tr>
                                        <td colspan="4" class="tableRowSubHeader"  align="left">
                                            Demographics
                                        </td>
                                      </tr>
                                      <tr>
                                        <td>
                                        First Name:           
            
                                        </td>
                                        <%--CssClass="textInput"--%>

                                         <td>
                                        <asp:TextBox ID = "first_nameTextBox" runat="server" ></asp:TextBox>
                                          M:
                                             <asp:TextBox ID = "middle_initialTextBox" runat="server" Width="15px"></asp:TextBox>
                                        </td>
        
                                      <td>
                                        Last Name:
                                        </td>
                                         <td>
                                        <asp:TextBox ID = "last_nameTextBox" runat="server"></asp:TextBox>
                                        Suffix: 
                                        <asp:dropdownlist id="suffixddl"
				                            tabIndex="3" runat="server" Height="20px" Width="52px">
				                            <asp:ListItem Value="------" Selected="True">------</asp:ListItem>
				                            <asp:ListItem Value="Sr.">Sr.</asp:ListItem>
				                            <asp:ListItem Value="Jr.">Jr.</asp:ListItem>
				                            <asp:ListItem Value="II">II</asp:ListItem>
				                            <asp:ListItem Value="III">III</asp:ListItem>
				                            <asp:ListItem Value="IV">IV</asp:ListItem>
                                            </asp:dropdownlist>


                                        </td>
                                    </tr>
                                      <tr>
                                        <td>
                                            Position Description:
                                        </td>
                                        <td colspan = "3">
                                            <asp:TextBox ID="user_position_descTextBox" runat="server" Width = "200px"></asp:TextBox>
                                        </td>
                                    </tr>
                                      <tr>
                                        <td>
                                        Entity:
                                        </td>
                                        <td>
                                        <asp:DropDownList ID ="entity_cdDDL" runat="server" AutoPostBack ="true">
            
                                        </asp:DropDownList>
                                        </td>
       
                                        <td>
                                        Department Name:
                                        </td>
                                        <td>
                                        <asp:DropDownList ID ="department_cdddl" runat="server">
            
                                        </asp:DropDownList>
                                        </td>
                                    </tr>
                                      <tr>
                                        <td>Address 1:</td>
                                        <td>  <asp:TextBox ID = "address_1textbox" runat="server"></asp:TextBox></td>
                                        <td>City:</td>
                                        <td>  <asp:TextBox ID = "citytextbox" runat="server"></asp:TextBox></td>
                                    </tr>
                                      <tr>
                                        <td>Address 2:</td>
                                        <td>  <asp:TextBox ID = "address_2textbox" runat="server"></asp:TextBox></td>
                                        <td>State:<asp:TextBox ID = "statetextbox" runat="server" Width="25px"></asp:TextBox></td>
                                        <td>Zip:  <asp:TextBox ID = "ziptextbox" runat="server" Width="50px"></asp:TextBox></td>
                                    </tr>
                                      <tr>
                                        <td>
                                        Phone Number:
                                        </td>
                                        <td>
                                        <asp:TextBox ID = "phoneTextBox" runat="server"></asp:TextBox>
                                        </td>
       
                                        <td>
                                        Pager Number:
                                        </td>
                                        <td>
                                       <asp:TextBox ID = "pagerTextBox" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>

                                      <tr>
                                       <td>Fax:</td>
                                        <td>
            
                                            <asp:TextBox ID = "faxtextbox" runat="server"></asp:TextBox></td>
           
           
                                         <td>
                                        Email:
                                        </td>
                                        <td>
                                       <asp:TextBox ID = "e_mailTextBox" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                      <tr>
                                        <td>
                                        Location
                                        </td>
                                        <td>
         
                                          <asp:DropDownList ID ="facility_cdddl" runat="server" AutoPostBack="true">
              
                                        </asp:DropDownList>
          
                                        </td>
        
                                        <td>
                                        Building
                                        </td>
                                        <td>
                                        <asp:TextBox ID = "building_cdTextBox" runat="server" Visible ="false"></asp:TextBox>
                                         <asp:DropDownList ID ="building_cdddl" runat="server">
            
                                        </asp:DropDownList>
          
                                        </td>
                                    </tr>
                                      <tr>
          
                                        <td>Vendor Name</td>
                                        <td colspan="3"><asp:TextBox ID ="VendorNameTextBox" runat="server" ></asp:TextBox></td>
                                    </tr>
                                      <tr>
                                        <td>
                                        Start Date:
                                        </td>
                                        <td>
                                           <asp:TextBox ID ="start_dateTextBox" runat="server" CssClass="calenderClass"></asp:TextBox>
                                        </td>
       
                                        <td>
                                        End Date:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="end_dateTextBox" runat="server" CssClass="calenderClass"></asp:TextBox>
                                        </td>
                                      </tr>
                                      <tr>
                                       
                                        <td>
                                            Share Drive
                                        </td>
                                        <td >
                                             <asp:TextBox ID="share_driveTextBox" runat="server"></asp:TextBox>
                                        </td>
                                        <td>
                                            Model After:
                                        </td>
                                        <td> 
                                            <asp:TextBox ID="ModelAfterTextBox" runat="server"></asp:TextBox>
                                        </td>
                                       </tr>
                                      <tr>
                                        <td>
                                            Request Close Date:
                                        </td>
                                        <td>
                                            <asp:TextBox ID= "close_dateTextBox" runat="server"></asp:TextBox>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td colspan = "4">
                                            <asp:TextBox ID="request_descTextBox" runat ="server" TextMode = "MultiLine" Width="100%"></asp:TextBox>
                                         </td>
                                      </tr>
                           
                        </table>

                      </ContentTemplate>

                  </ajaxToolkit:TabPanel>

                  <ajaxToolkit:TabPanel ID="tpAccounts" runat="server" TabIndex="0"  BackColor="Gainsboro" Visible = "True">


                      <HeaderTemplate > 
                     
                            <asp:Label ID="Label2" runat="server" Text="Accounts" Font-Bold="true"></asp:Label>
            
                     
                      </HeaderTemplate>


                      <ContentTemplate>

                        <asp:UpdatePanel runat="server"  ID="uplAccounts" UpdateMode= "Conditional" >
        
                               <ContentTemplate>
                                 <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0"
                                         Width="100%" style="margin-bottom: 1px" EnableTheming="False"
                                            BackColor="#efefef">



                                      <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" TabIndex="0"  BackColor="Gainsboro" Visible = "True">


                                          <HeaderTemplate > 
                     
                                                <asp:Label ID="Label4" runat="server" Text="Remove Requested Accounts" Font-Bold="true"></asp:Label>
            
                     
                                          </HeaderTemplate>


                                          <ContentTemplate>
                                                <table width="100%" border="1"  style="background-color:Gainsboro">
                                                 <td colspan = "4" align="left">
                                                        <asp:Button runat="server" ID ="btnInvalidAccts" Text = "Submit Invalid Items"  /> 
                                                    </td>
                                                    <tr style="background-color:#FFFFCC; font-weight:bold">
                                                        <td colspan="2" >
                                                        Accounts
                                                        </td>
                                                        <td colspan="2">
                                                        Accounts to remove
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                                <td colspan="2"  valign="top"  style="width: 50%">
                                                                    <asp:GridView ID="gvCurrentApps" runat="server" AllowSorting="True" 
                                                                        AutoGenerateColumns="False" BackColor="White" BorderColor="#336666"
                                                                        BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                                                                        EmptyDataText="No Applications Found" EnableTheming="False" DataKeyNames="account_request_seq_num, ApplicationDesc, ApplicationNum"
                                                                        Font-Size="Medium" GridLines="Horizontal" Width="100%">
                                   
                                                                                                        <FooterStyle BackColor="White" ForeColor="#333333" />
                                                                                                        <HeaderStyle BackColor="#336666" Font-Bold="False" ForeColor="White" Font-Size="Small" HorizontalAlign="Left"    />                                    
                                                                                                        <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Left" Font-Size="Medium"/>
                                                                                                        <RowStyle BackColor="White" ForeColor="#333333" />
                                                                                                        <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                                                                                        <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                                                                                        <SortedAscendingHeaderStyle BackColor="#487575" />
                                                                                                        <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                                                                                        <SortedDescendingHeaderStyle BackColor="#275353" />

                                                                                                        <Columns>
                                                                                            
                                                                                                                  <asp:CommandField ButtonType="Button" SelectText="Select" ShowSelectButton= "true" ControlStyle-BackColor="#84A3A3" ItemStyle-Height="30px"/>
                                                                                                                  <asp:BoundField  DataField="ApplicationDesc"  HeaderText="Application" HeaderStyle-HorizontalAlign="Left"  ItemStyle-Height="35px"></asp:BoundField>
				                                                                  
                                  
                                                                                                      </Columns>

                                                                                                    </asp:GridView>
		
                                                                                </td>
                                                                 <td colspan="2" valign="top"  style="width: 50%">
                                                                <asp:GridView ID="gvRemoveAccounts" runat="server" AllowSorting="True" 
                                                                                AutoGenerateColumns="False" BackColor="White" BorderColor="#336666"
                                                                                BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                                                                                EmptyDataText="Select applications to be removed" EnableTheming="False" DataKeyNames="account_request_seq_num, ApplicationDesc, ApplicationNum"
                                                                                Font-Size="Medium" GridLines="Horizontal" Width="100%" >
                                   
                                                                                <FooterStyle BackColor="White" ForeColor="#333333" />
                                                                                <HeaderStyle BackColor="#336666" Font-Bold="False" ForeColor="White" Font-Size="Small" HorizontalAlign="Left"    />                                    
                                                                                <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Left" Font-Size="Medium"/>
                                                                                <RowStyle BackColor="White" ForeColor="#333333" />
                                                                                <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                                                                <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                                                                <SortedAscendingHeaderStyle BackColor="#487575" />
                                                                                <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                                                                <SortedDescendingHeaderStyle BackColor="#275353" />

                                                                                <Columns>
                                              
                                              
                                                                                            <asp:CommandField ButtonType="Button" SelectText="<-- Remove" ShowSelectButton= "true"   ControlStyle-BackColor="#84A3A3" ItemStyle-Height="30px" />
                                                                                            <asp:BoundField  DataField="ApplicationDesc"  HeaderText="Application" HeaderStyle-HorizontalAlign="Left" ItemStyle-Height="30px" ></asp:BoundField>
				                          




                                  
                                                                                </Columns>

                                                                            </asp:GridView>
		
                                                                        </td>
                         </tr>
                                                
                                                </table>

                                          </ContentTemplate>

                                    </ajaxToolkit:TabPanel>
                                     <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" TabIndex="0"  BackColor="Gainsboro" Visible = "True">


                                          <HeaderTemplate > 
                     
                                                <asp:Label ID="Label5" runat="server" Text="Request Accounts" Font-Bold="true"></asp:Label>
            
                     
                                          </HeaderTemplate>


                                          <ContentTemplate>
                                                <table border="1" style="background-color:Gainsboro">
                                                   <tr>

                                                    <td colspan = "4" align="left">
                                                        <asp:Button runat="server" ID ="btnSubmitAddRequest" Text = "Submit Add Request" /> 
                                                    </td>
                                                   
                                                   </tr>
                                                                                                     
                                                   <tr>                                                                                                                           
                                                                <td colspan="4">
                                                                <span style="Font-weight:bold"> Entities</span>
                                                                    <asp:CheckBoxList ID = "cblEntities" runat="server" RepeatDirection = "Horizontal">
                                                                    </asp:CheckBoxList>
                                                                </td>
            
                                                            </tr>
                                                    <tr>
                                                                <td colspan="4">
                                                                    <span style="Font-weight:bold">Hospitals</span>
                                                                
                                                                    <asp:CheckBoxList ID = "cblFacilities" runat="server" RepeatDirection = "Horizontal">
                                                                    </asp:CheckBoxList>
                                                                </td>
      
        
                                                            </tr>
                                                                                                                  <tr style="background-color:#FFFFCC; font-weight:bold">
                                                                 <td colspan="2">
                                                                    Accounts available
                                                                    </td>
                                                                 <td colspan="2">
                                                                    Accounts to be added
                                                                    </td>
                                                              </tr>
                                                                         <tr>
                                                                          <td colspan="2"  valign="top" style="width: 50%">
                                                                                 <asp:GridView ID="grAvailableAccounts" runat="server" AllowSorting="True" 
                                                                                AutoGenerateColumns="False" BackColor="White" BorderColor="#336666"
                                                                                BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                                                                                EmptyDataText="No Applications Found" EnableTheming="False" 
                                                                                DataKeyNames="Applicationnum, ApplicationDesc"
                                                                                Font-Size="Medium" GridLines="Horizontal" Width="100%">
                                   
                                                                                <FooterStyle BackColor="White" ForeColor="#333333" />
                                                                                <HeaderStyle BackColor="#336666" Font-Bold="False" ForeColor="White" Font-Size="Small" HorizontalAlign="Left"    />                                    
                                                                                <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Left" Font-Size="Medium"/>
                                                                                <RowStyle BackColor="White" ForeColor="#333333" />
                                                                                <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                                                                <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                                                                <SortedAscendingHeaderStyle BackColor="#487575" />
                                                                                <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                                                                <SortedDescendingHeaderStyle BackColor="#275353" />

                                                                                <Columns>
                                                                                            
                                                                                          <asp:CommandField ButtonType="Button" SelectText="Select" ShowSelectButton= "true" HeaderStyle-HorizontalAlign = "Left"  ControlStyle-BackColor="#84A3A3" ItemStyle-Height="30px" />
                                                                                          <asp:BoundField  DataField="ApplicationDesc"  HeaderText="Application"  HeaderStyle-HorizontalAlign = "Left" ItemStyle-Height="35px"></asp:BoundField>
				                                                                  
                                  
                                                                              </Columns>

                                                                            </asp:GridView>
		
                                                                  </td>

                                                                  <td colspan="2"  valign="top" style="width: 50%">
                                                                                 <asp:GridView ID="grAccountsToAdd" runat="server" AllowSorting="True" 
                                                                                AutoGenerateColumns="False" BackColor="White" BorderColor="#336666"
                                                                                BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                                                                                EmptyDataText="Select accounts to add" EnableTheming="False" 
                                                                                DataKeyNames="Applicationnum, ApplicationDesc"
                                                                                Font-Size="Medium" GridLines="Horizontal" Width="100%">
                                   
                                                                                <FooterStyle BackColor="White" ForeColor="#333333" />
                                                                                <HeaderStyle BackColor="#336666" Font-Bold="False" ForeColor="White" Font-Size="Small" HorizontalAlign="Left"    />                                    
                                                                                <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Left" Font-Size="Medium"/>
                                                                                <RowStyle BackColor="White" ForeColor="#333333" />
                                                                                <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                                                                <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                                                                <SortedAscendingHeaderStyle BackColor="#487575" />
                                                                                <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                                                                <SortedDescendingHeaderStyle BackColor="#275353" />

                                                                                <Columns>
                                                                                            
                                                                                          <asp:CommandField ButtonType="Button" SelectText="<-- Remove" ShowSelectButton= "true" HeaderStyle-HorizontalAlign = "Left"  ControlStyle-BackColor="#84A3A3" ItemStyle-Height="30px"/>
                                                                                          <asp:BoundField  DataField="ApplicationDesc"  HeaderText="Application"  HeaderStyle-HorizontalAlign = "Left" ItemStyle-Height="35px"></asp:BoundField>
				                                                                  
                                  
                                                                              </Columns>

                                                                            </asp:GridView>
		
                                                                  </td>
                                                        </tr>
                                                
                                                </table>

                                          </ContentTemplate>

                                    </ajaxToolkit:TabPanel>
                                </ajaxToolkit:TabContainer>
                            </ContentTemplate>
                       
                       </asp:UpdatePanel>

                       
                       </ContentTemplate>                                

                  </ajaxToolkit:TabPanel>

                  <ajaxToolkit:TabPanel ID="tpPendingRequests" runat="server" TabIndex="0"  BackColor="red" Visible = "True">
                      <HeaderTemplate > 
                            <asp:Label ID="Label3" runat="server" Text="Pending Requests" Font-Bold="true"></asp:Label>
                    
                      </HeaderTemplate>
                      <ContentTemplate>
                          <table border="3" width = "100%">
                            <tr>
                                <td colspan = "4">
                                      <asp:Table ID = "tblPendingQueues" runat = "server">
                                     </asp:Table>
            
                                </td>
        
                            </tr>
                           </table>
                       </ContentTemplate>
                  </ajaxToolkit:TabPanel>

                  <ajaxToolkit:TabPanel ID="tpRequestItems" runat="server" TabIndex="0"  BackColor="red" Visible = "True">
                      <HeaderTemplate > 
                            <asp:Label ID="Label6" runat="server" Text="Request Items" Font-Bold="true"></asp:Label>
                    
                      </HeaderTemplate>
                      <ContentTemplate>
                          <table border="3" width = "100%">
                            <tr>
                                <td colspan = "4">
                                      <asp:Table ID = "tblRequestItems" runat = "server">
                                     </asp:Table>
            
                                </td>
        
                            </tr>
                           </table>
                       </ContentTemplate>
                  </ajaxToolkit:TabPanel>

                  <ajaxToolkit:TabPanel ID="TPActionItems" runat="server" TabIndex="0"  BackColor="red" Visible = "True">
                      <HeaderTemplate > 
                            <asp:Label ID="Label7" runat="server" Text="Action Items" Font-Bold="true"></asp:Label>
                    
                      </HeaderTemplate>
                      <ContentTemplate>
                          <table border="3" width = "100%">
                            <tr>
                                <td colspan = "4">
                                      <asp:Table ID = "tblActionItems" runat = "server">
                                     </asp:Table>
            
                                </td>
        
                            </tr>
                           </table>
                       </ContentTemplate>
                  </ajaxToolkit:TabPanel>


           </ajaxToolkit:TabContainer>





</td>

</tr>

</table>

</ContentTemplate>
</asp:UpdatePanel>

</asp:Content>

