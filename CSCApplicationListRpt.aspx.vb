﻿Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports App_Code

Partial Class CSCApplicationListRpt
    Inherits MyBasePage ' System.Web.UI.Page

    Dim dst As DataSet
    Dim dt As DataTable

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
		If Session("SecurityLevelNum") < 69 Then
			Response.Redirect("~/SimpleSearch.aspx")

		End If

		If Not IsPostBack Then

            Dim thisData As New CommandsSqlAndOleDb("SelCSCApplicationSupportRpt", Session("CSCConn"))


            dt = thisData.GetSqlDataTable

            Session("Termtable") = dt

            GridEmployees.DataSource = dt
            GridEmployees.DataBind()

            GridHidden.DataSource = dt
            GridHidden.DataBind()


        End If

    End Sub

    Protected Sub btnCSCOwer_Click(sender As Object, e As System.EventArgs) Handles btnCSCOwer.Click
        Response.Redirect("./CSCOwnerMaintenance.aspx", True)
    End Sub

    Protected Sub btnExcel_Click(sender As Object, e As System.EventArgs) Handles btnExcel.Click
        Dim thisData As New CommandsSqlAndOleDb("SelCSCApplicationSupportRpt", Session("CSCConn"))
        dt = thisData.GetSqlDataTable

        CreateExcel2(dt)
    End Sub
    Sub CreateExcel2(ByVal ReportTable As DataTable)



        Response.AddHeader("content-disposition", "attachment;filename=FileName.xls")

        Response.Charset = String.Empty
        Response.ContentType = "application/vnd.ms-excel"
        '  Response.ContentType = "application/vnd.xls"

        Dim sw As System.IO.StringWriter = New System.IO.StringWriter()

        Dim hw As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(sw)
        Dim dg As New DataGrid

        ' commented try new style of excel 2017
        '===============================Create form to contain grid
        'Dim frm As New HtmlForm()

        'GridHidden.Visible = True

        'GridHidden.Columns.RemoveAt(0)
        'GridHidden.Parent.Controls.Add(frm)
        'frm.Attributes("RunAt") = "server"
        'frm.Controls.Add(GridHidden)

        'frm.RenderControl(hw)
        'new style from open provider page

        ' must be name of datasource on page
        dg.DataSource = ReportTable
        Dim i As Integer = dg.Items.Count
        dg.DataBind()
        '  dg.GridLines = GridLines.None
        ' ClearControls(dg)

        dg.HeaderStyle.Font.Bold = True
        dg.HeaderStyle.BackColor = Drawing.Color.LightGray


        dg.RenderControl(hw)


        Response.Write(sw.ToString())

        Response.End()

        GridHidden.Visible = False

    End Sub

End Class
