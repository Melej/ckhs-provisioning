﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="false" CodeFile="AccountRequestInfo2.aspx.vb" Inherits="AccountRequestInfo2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <script type="text/javascript" src="Scripts/FieldValidator.js"></script>
<script type="text/javascript">
    function pageLoad(sender, args) {

        fieldValidator('textInputRequired', 'btnUpdate', 'lblValidation');

    }
</script>
  <style type="text/css">
    .style2
    {
        text-align: center;
    }
    .style3
    {
        width: 100%;/**/
    }
     .styleInfo
    {
         color:Navy;
         font-size:14px;
         font-weight:bolder;
    }
    .textHasFocus
    {
         border-color:Red;
        border-width:medium;
        border-style:outset;
    } 
        
        .style7
        {
            width: 812px;
        }
        
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

 <asp:UpdatePanel ID="mainupd" runat="server"  UpdateMode= "Conditional">
   
 <ContentTemplate>

  <table width= "100%">
     <tr valign="bottom">
       <td align="center" valign="middle">
         <asp:Panel ID="pnlOnBehalfOf" runat="server"  Width="80%" Height="80%"  BackColor="#CCCCCC"  style="display:none">
                <table align="center" border="1px" width="90%" bgcolor="#CCCCCC">
                    <tr>
                        <td class="tableRowHeader" colspan="4">
                           Search for CKHS Individual who will be Responsable for these Account(s)<br />
                           Listed below are ONLY CKHS Employees
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Last Name
                        </td>
                        <td>
                            <asp:TextBox ID="SearchLastNameTextBox" runat="server"></asp:TextBox>
                        </td>
                        <td>
                            First Name
                        </td>
                        <td>
                            <asp:TextBox ID="SearchFirstNameTextBox" runat="server"></asp:TextBox>
                        </td>

                    </tr>
                    <tr>
                        <td colspan="4">
                            <div class="scrollContentContainer">
                                <asp:GridView ID="gvSearch" runat="server" AllowSorting="True" 
                                    AutoGenerateColumns="False" BackColor="White" BorderColor="#336666" 
                                    BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                                    DataKeyNames="client_num, login_name" EmptyDataText="No Items Found" 
                                    EnableTheming="False" Font-Size="Medium" GridLines="Horizontal" Width="100%">
                                    <FooterStyle BackColor="White" ForeColor="#333333" />
                                    <HeaderStyle BackColor="#336666" Font-Bold="False" Font-Size="Small" 
                                        ForeColor="White" HorizontalAlign="Left" />
                                    <PagerStyle BackColor="#336666" Font-Size="Medium" ForeColor="White" 
                                        HorizontalAlign="Left" />
                                    <RowStyle BackColor="White" ForeColor="#333333" />
                                    <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                    <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                    <SortedAscendingHeaderStyle BackColor="#487575" />
                                    <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                    <SortedDescendingHeaderStyle BackColor="#275353" />
                                    <Columns>
                                        <asp:CommandField ButtonType="Button" SelectText="Select" 
                                            ShowSelectButton="true" />
                                        <asp:BoundField DataField="fullname" HeaderStyle-HorizontalAlign="Left" 
                                            HeaderText="Full Name" ItemStyle-HorizontalAlign="Left" />
                                        <asp:BoundField DataField="phone" HeaderStyle-HorizontalAlign="Left" 
                                            HeaderText="Phone" ItemStyle-HorizontalAlign="Left" />
                                        <asp:BoundField DataField="department_name" HeaderStyle-HorizontalAlign="Left" 
                                            HeaderText="Department" ItemStyle-HorizontalAlign="Left" />
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <table>
                                <tr>
                                    <td>
                                        <asp:Button ID="btnSearch" runat="server" Font-Bold="True" Font-Names="Arial" 
                                            Font-Size="Small" Height="20px" Text="Search" Width="125px" />
                                    </td>
                                    <td>
                                        <asp:Button ID="btnCloseOnBelafOf" runat="server" Font-Bold="True" 
                                            Font-Names="Arial" Font-Size="Small" Height="20px" Text="Close" Width="125px" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                </table>
              </asp:Panel>

            <ajaxToolkit:ModalPopupExtender ID="BeHalOfPopup2" runat="server" 
                TargetControlID="HdBtnSearch" PopupControlID="pnlOnBeHalfOf" 
                BackgroundCssClass="ModalBackground" >
            </ajaxToolkit:ModalPopupExtender>

        <%--CancelControlID="btnNoSearch"--%>

       <asp:Button ID="HdBtnSearch" runat="server" style="display:none" />

          </td>
       </tr>
       <tr>
            <td align="center">
            
                <table cellpadding="3Px">
                    <tr align="center" >
                        <td align="right" >
                            <asp:Label ID="lblusersub"  runat="server" Text="User submitting:" Font-Bold="True" />
                        </td>
                        <td align="left">
                            <asp:Label ID = "submitter_nameLabel" runat="server" ></asp:Label>
                            <asp:Label ID="submitter_client_numLabel" runat="server" visible="false" />

                        </td>
                        <td>
                        </td>
                        <td align="right">
                            <asp:Label ID="lblrequest" runat="server" Font-Bold="True" text = "Requestor:"  />
                        </td>
                        <td align="left">
                            <asp:Label ID = "requestor_nameLabel" runat="server" ></asp:Label>
                            <asp:Label ID="requestor_client_numLabel" runat="server" visible="false">
                            </asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" align="center">
                           <asp:Button ID="btnSelectOnBehalfOf" runat="server" Text="Change Requestor"
                                   CssClass="btnhov" BackColor="#006666"  Width="150px" />
                        
                        </td>
                    </tr>
                        <tr align="center">
                            <td colspan="5" align="center">
                                <asp:Label ID = "lblValidation" runat = "server" ForeColor="Red" Font-Bold="true"/>
                            </td>
            
                        </tr>
                        <tr align="center">
                            <td colspan="5" align="center">
                                <asp:Label ID = "lblRehire" runat = "server" ForeColor="Red" Font-Bold="true" Font-Size="X-Large" />
                            </td>
            
                        </tr>

                    </table>            
            </td>
       </tr>
       <tr>
        <td align="left" class="style7">
             <ajaxToolkit:TabContainer ID="tabs" runat="server" ActiveTabIndex="0"
                Width="100%" style="margin-bottom: 0px" EnableTheming="true" 
                BackColor="#efefef">

                  <ajaxToolkit:TabPanel ID="tpDemographics" runat="server"  BackColor="Gainsboro" Visible = "True" EnableTheming="true">
                     <HeaderTemplate > 
                            <asp:Label ID="Label1" runat="server" Text="Demographics" Font-Bold="True" 
                                ForeColor="Black"></asp:Label>
                     </HeaderTemplate>
                     <ContentTemplate>
                        <table border="0" style="width: 100%">

                                    <tr>
                                             <td class="tableRowHeader">
                                                Provisioning Request Information
                                             </td>
                                    </tr>
                                    <tr>
                                             <td class="tableRowHeader">
                                               <asp:Label ID="ClientnameHeader" runat="server" >
                                                </asp:Label>
                                             </td>
                                    </tr>
                                    <tr>
          
                                        <td class="tableRowSubHeader"  align="left">
                                       Form Submission
                                        </td>
            
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:Button ID="btnDemoRequest" runat="server" Text = "Client Demo Request " Visible="false"
                                                        CssClass="btnhov" BackColor="#006666"  Width="250px" />
                                                    
                                                    </td>

                                                    <td>
                                                        <asp:Button ID = "btnUpdate" runat = "server" Text ="Update Request Demographics" 
                                                        CssClass="btnhov" BackColor="#006666"  Width="250px" />
                                                        
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnClientDemo" runat="server" Text = "Return to Client Demographics" Visible="false"
                                                        CssClass="btnhov" BackColor="#006666"  Width="250px" />
                                                         
                                                    </td>
                                                    <td>

														<asp:Button ID="btnPrintReq" runat="server" Text =" Print "
															CssClass="btnhov" BackColor="#006666"  Width="250px" />
                                                    </td>
                                                </tr>
                                                
                                            </table>

              
                                        </td>
                                    </tr>
                                    <tr>
                                     <td align="center">
                                        <table>
                                            <tr>
                                               <td align="right">
                                                    <asp:Label id="lblrequesttype" runat="server" Font-Bold="True" text="Request Type:" />
                                                </td>

                                                <td align="left">
                                                    <asp:Label ID="account_request_typeLabel" runat="server" />
                                                
                                                </td>
                                                <td align="right">
                                                    <asp:Label id="lblentdate" runat="server"  Font-Bold="True" Text  = "Entered Date:" />

                                                </td>
                                                <td align="left">
                                                    <asp:Label ID = "entered_dateLabel" runat="server" />
                                                </td>
                                                <td>
                                                </td>
                                            </tr>
                                       </table>
                                     </td>
                                    </tr>

                                    <tr>
                                        <td class="tableRowSubHeader"  align="left">
                                        CKHS Affiliation:
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <asp:RadioButtonList ID ="emp_type_cdrbl" runat = "server" 
                                                RepeatDirection="Horizontal" AutoPostBack="True" Font-Size="Smaller">
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>

                                   <asp:Panel runat="server" ID="pnlProvider" Visible="False">
                                      <tr>
                                        <td colspan="4"  class="tableRowSubHeader" align="left">
                                            Provider Information
                                        </td>
                                      </tr>
                                       <tr>
                                        <td colspan="4">
                                       <table id="tblprovider"  border="1" width="100%">
                                            

                                        <tr>
                                            <td align="right" >
                                              <asp:Label ID="priviledlbl" runat="server" Text="Privileged Date:" 
                                                ToolTip="Only Credentialing Office will enter this data, Indicates the date the Provider was credentialied on."  Width="150px" />

                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID = "credentialedDateTextBox" runat="server" Enabled="false"></asp:TextBox>
                                            </td>

<%--                                              <td align="right">
                                                  DCMH Privileged Date:
                                              </td>
                                              <td align="left">
                                                  <asp:TextBox ID="CredentialedDateDCMHTextBox" runat="server" 
                                                       Enabled="false"></asp:TextBox>
                                              </td>--%>


                                            <td align="right">
                                                <asp:Label ID="ccmcadmitlbl" runat="server" Text ="Privileged Rights:" Width="150px" />   
                                            </td>
                                            <td align="left" >
                                                   <asp:RadioButtonList ID = "CCMCadmitRightsrbl" runat="server" AutoPostBack="true"
                                                   RepeatDirection="Horizontal">
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                    <asp:listItem>No</asp:listItem>
                                                </asp:RadioButtonList>

                                            </td>
<%--                                            <td align="right">
                                                <asp:Label ID="dcmhlbl" runat="server" Text ="DCMH Privileged Rights:" Width="150px" />
                                             </td>

                                            <td align="left" >
                                                   <asp:RadioButtonList ID = "DCMHadmitRightsrbl" runat="server" AutoPostBack="true"
                                                   RepeatDirection="Horizontal">
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                    <asp:listItem>No</asp:listItem>
                                                </asp:RadioButtonList>

                                            </td>--%>

                                        </tr>
                                        
                                        
                                        <tr>
  
                                            <td align="right">
                                              CHMG:
                                          </td>
                                            <td align="left">
                                                <asp:RadioButtonList ID = "hanrbl" runat="server" RepeatDirection="Horizontal" AutoPostBack="true">
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                    <asp:ListItem Selected="True">No</asp:ListItem>            
                                                </asp:RadioButtonList>
                                            </td>
                                            <td align="right">
                                               Doctor Number:
                                          </td>
                                          <td align="left">
                                                <asp:TextBox ID = "doctor_master_numTextBox" runat="server" MaxLength="6" ></asp:TextBox>
                                          </td>
                                        </tr>


                                        <tr>
                                            <td align="right">
                                                NPI:
                                            </td>
                                            <td  align="left" >
                                                <asp:TextBox ID = "npitextbox" runat = "server" MaxLength="10"></asp:TextBox>
                                            </td>
                                            <td align="right" >
                                                License No.
                                            </td>
                                            <td align="left" >
                                                    <asp:TextBox ID = "LicensesNoTextBox" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                Taxonomy:
                                                </td>
                                            <td align="left">
                                                <asp:TextBox ID = "taxonomytextbox" runat="server"></asp:TextBox>
                                            </td>
                                            <td align="right">
                                                  <%--Group Number:--%>
                                              </td>

                                              <td align="left">
                                                  <%--<asp:TextBox ID="TextBox1" runat="server" Enabled="false" ></asp:TextBox>--%>
                                              </td>
                                        </tr>

        <%--                          <tr>
                                    <td align="right">
                                        Medicaid ID:
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="MedicareIDTextBox" runat="server" Width="250px"></asp:TextBox>
                                    </td>

                                    <td align="right">
                                        BlueCross ID:
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="BlueCrossIDTextBox" runat="server" Width="250px"></asp:TextBox>
                                    </td>

                                </tr>--%>

                                <tr>
                                    <td align="right">
                                        DEA ID:
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="DEAIDTextBox" runat="server" Width="250px"></asp:TextBox>
                                    </td>

                                    <td align="right">
                                       <%-- DEA Extension:--%>
                                    </td>
                                    <td align="left">
                                        <%--<asp:TextBox ID="DEAExtensionIDTextBox" runat="server" Width="250px"></asp:TextBox>--%>
                                    </td>

                                </tr>
<%--                                        <tr>
                                           <td align="right">
                                                Medicaid ID:
                                           </td>
                                           <td align="left">
                                              <asp:TextBox ID="MedicareIDTextBox" runat="server" Width="250px"></asp:TextBox>
                                           </td>

                                           <td align="right">
                                                BlueCross ID:
                                           </td>
                                           <td align="left">
                                              <asp:TextBox ID="BlueCrossIDTextBox" runat="server" Width="250px"></asp:TextBox>
                                           </td>

                                        </tr>--%>

                                        <tr>
                                          <td colspan="4">
                                           <table border="3px" width="100%">

                                                <tr>
                                                <td colspan="4"  class="tableRowSubHeader" align="center">
                                                    Physician Groups 
                                                </td>
                                                </tr>

<%--                                                <tr>
                                                    <td align="right" style="width: 220px">
                                                        G Group Email:
                                                    </td>
                                                    <td align="left" colspan="3">
                                                        <asp:Label ID="DirectEmailLabel" runat="server" Width="275px"></asp:Label>
                                                    </td>


                                                </tr>--%>

                                                <tr>
                                                     <td align="right">
                                                            <asp:Label ID ="ckhnlbl" runat="server" Text="CHMG (G#):" Visible="false" >
                                                            </asp:Label>

                                                        </td>
                                                        <td colspan="3" align="left">
                                                        <asp:DropDownList ID = "LocationOfCareIDddl" runat="server" Visible="false" Width="90%" >
                                                        </asp:DropDownList>

                                                        </td>
                                
                                
                                                </tr>

<%--                                                <tr>

                                                        <td align="right">
                                                            Comm. Location Of Care:
                                                        </td>
                                                        <td colspan="3" align="left">
                                                            <asp:DropDownList ID = "CommLocationOfCareIDddl" runat="server" Width="90%">
                                            
                                                            </asp:DropDownList>
                                                
                                                        </td>
                                
                                                </tr>--%>

                                                 <tr>
                                                    <td align="right" >
                                                    Providers Group Names:
                                                    </td>       
                                                    <td colspan="3" align="left">
                                                    <asp:DropDownList ID ="group_nameddl"  runat="server" Width="90%" AutoPostBack="true">
            
                                                    </asp:DropDownList>
                                                    </td>
                                                </tr>

<%--                                                <tr>
                                                    <td  align="right">
                                                        Coverage Group ID:
                                                    </td>
                                                    <td  colspan="3" align="left">
                                                            <asp:DropDownList ID = "CoverageGroupddl" runat="server" Width="90%">
                                            
                                                            </asp:DropDownList>

                                                    </td>
                                                </tr>

                                                <tr>
                                
                                                    <td  align="right">
                                                        Neighborhood Phys Groups:
                                                    </td>
                                                    <td  colspan="3" align="left">
                                                            <asp:DropDownList ID = "Nonperferdddl" runat="server" Width="90%">
                                            
                                                            </asp:DropDownList>
                                                    </td>
                                
                                                </tr>--%>

<%--                                                <tr>


                                                    <td align="right">
                                                        CKHN Copy To MedAssist.:
                                                    </td>
                                                     <td>
                                                        <asp:Label ID = "CopyToLabel" runat="server" Width="245px"></asp:Label>
                                                    </td>
                                                    <td align="right">
                                                        Comm. Copy To MedAssist.:
                                                    </td>
                                                     <td>
                                                        <asp:Label ID = "CommCopyToLabel" runat="server" Width="245px"></asp:Label>
                                                     </td>

                                                </tr>--%>

                                            </table>
                                          </td>
                                        </tr>

                                        <tr>
                                            <td align="right">
                                            Title:
                                            </td>
                                            <td align="left" >
                                                <asp:dropdownlist ID = "Titleddl" runat="server"></asp:dropdownlist>
                                                 <asp:TextBox ID="Title2TextBox" runat="server"  Width="250px" Visible="false"> </asp:TextBox>

                                            </td>
                                            <td  align="right" >
                                            Specialty:
                                            </td>
                                            <td align="left" >
                                                <asp:dropdownlist ID = "Specialtyddl" runat="server">
                                                </asp:dropdownlist>
                                              <asp:Label ID ="SpecialtLabel" runat="server" Visible="False"></asp:Label>

                                            </td>
                                        </tr>

 <%--                                       <tr>
                                            <td align="right">
                                                CCMC Consults:
                                            </td>
                                            <td align="left" >
                                                   <asp:RadioButtonList ID = "CCMCConsultsrbl" runat="server" RepeatDirection="Horizontal">
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                    <asp:listItem>No</asp:listItem>
                                                </asp:RadioButtonList>

                                            </td>
                                            <td align="right">
                                                Springfield Consults:
                                            </td>
                                            <td align="left" >
                                                   <asp:RadioButtonList ID = "SpringfieldConsultsrbl" runat="server" RepeatDirection="Horizontal">
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                    <asp:listItem>No</asp:listItem>
                                                </asp:RadioButtonList>

                                           </td>

                                        </tr>
                                        <tr>
                                            <td align="right">
                                                Taylor Consults:
                                            </td>
                                            <td align="left" >
                                                   <asp:RadioButtonList ID = "TaylorConsultsrbl" runat="server" RepeatDirection="Horizontal">
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                    <asp:listItem>No</asp:listItem>
                                                </asp:RadioButtonList>

                                            </td>
                                            <td align="right">
                                                DCMH Consults:
                                            </td>
                                            <td align="left" >
                                                   <asp:RadioButtonList ID = "DCMHConsultsrbl" runat="server" RepeatDirection="Horizontal">
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                    <asp:listItem>No</asp:listItem>
                                                </asp:RadioButtonList>

                                           </td>

                                        </tr>

                                        <tr>
                                            <td align="right">
                                                Write Orders for C.T.S. Bedded Patients:
                                            </td>
                                                <td align="left" >
                                                    <asp:RadioButtonList ID = "Writeordersrbl" runat="server" RepeatDirection="Horizontal">
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                    <asp:listItem>No</asp:listItem>
                                                </asp:RadioButtonList>

                                            </td>

                                            <td align="right">
                                                Write Orders for DCMH Bedded Patients:
                                            </td>
                                                <td align="left" >
                                                    <asp:RadioButtonList ID = "WriteordersDCMHrbl" runat="server" RepeatDirection="Horizontal">
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                    <asp:listItem>No</asp:listItem>
                                                </asp:RadioButtonList>

                                            </td>
                                        </tr>--%>




<%--                                        <tr>
                                           <td align="right">
                                                Medic Group ID:
                                           </td>
                                           <td align="left">
                                              <asp:TextBox ID="MedicGroupIDTextBox" runat="server" Width="250px"></asp:TextBox>
                                           </td>
                                           <td align="right">
                                                Medic Group Phys ID:
                                           </td>
                                           <td align="left">
                                              <asp:TextBox ID="MedicGroupPhysIDTextBox" runat="server" Width="250px"></asp:TextBox>
                                           </td>


                                        </tr>--%>
<%--                                        <tr>
                                            <td align="right">
                                                Uniform Physician ID:
                                            </td>
                                             <td align="left">
                                             <asp:TextBox ID="UniformPhysIDTextBox" runat="server" Width="250px"></asp:TextBox>
                                           </td>
                                            <td align="right">  
                                                Sure Script ID:
                                           </td>
                                           <td align="left">
                                              <asp:TextBox ID="SureScriptIDTextBox" runat="server" Width="250px"></asp:TextBox>
                                           </td>

                                        </tr>--%>

                                       </table>
                                        </td>
                                     </tr>
                                   </asp:Panel> 

                                    <tr>
                                        <td>
                                            <asp:Panel ID="pnlCererUserPositions" runat="server" Visible="true">
                                                <table border="3" width="100%">
                                                    <tr>
                                                        <td align="left" class="tableRowSubHeader" colspan="2">
                                                            Cerner User Positions

                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right">
                                                            <asp:Label ID="lblCerberpos" runat="server" Text ="Cerner Position" />
                                                        </td>
                                                        <td align="left">
                                                            <asp:TextBox ID="CernerPositionDescTextBox" runat="server" Width="300px" Enabled="false"></asp:TextBox>
                                                            <asp:TextBox ID="UserCategoryCdTextBox" runat="server" Visible="false"></asp:TextBox>
                                                        </td>
                                                    </tr>


                                              <tr>
                                                  <td align="right">
                                                       Cerner Position:

                                                  </td>
                                                <td align="left"  >
                                                           <asp:DropDownList ID = "CernerPosddl" runat="server" Visible="true" Width="80%" AutoPostBack="true"> 
                                                           </asp:DropDownList>
                                                    </td>
                    
                                            </tr>

                                            </table>  
                                          </asp:Panel>
                                        </td>

                                    </tr>


                                   <asp:Panel ID="pnlTCl" runat="server" Visible="false">
                                     <tr>
                                       <td>
                                        <table  id="tbltcl" border="3px" width="100%">
                                          <tr>
                                                <td align="left" class="tableRowSubHeader" colspan="4">
                                                    TCL / User Type
                                                </td>
                                            </tr>
                                          <tr>
                                            <td align="right">
                                                    <asp:Label ID="LblTCL" runat="server" Text="TCL/ Invision User Type:" />
                                            </td>
                                            <td colspan="3">
                
                                                    <asp:TextBox ID="TCLTextBox" runat="server" Visible="false"  Enabled="false" Width="110px"></asp:TextBox>
                                                    <asp:TextBox ID="UserTypeCdTextBox" runat="server" Visible="false" Enabled="false" Width="110px"></asp:TextBox>

                                                    <asp:TextBox ID="UserTypeDescTextBox" runat="server" Visible="false" Enabled="false" Width="300px"></asp:TextBox>
        
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="4">
                                                <asp:Button ID="btnChangeTCL" runat="server" Text="Change TCL" Width="200px" Visible="true"  />
                                            </td>
                                           </tr>
                                          <tr>
                                            <td align="center" colspan="4">


                                                <div>
                                                <asp:GridView ID="gvTCL" runat="server" AllowSorting="False"
                                                        AutoGenerateColumns="False" BackColor="White" BorderColor="#336666"
                                                        BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                                                        EmptyDataText="No Docmaster # Found" EnableTheming="False" 
                                                        DataKeyNames="TCL,UserTypeCd,UserTypeDesc" Visible="false"
                                                        Font-Size="Small" GridLines="Horizontal" Width="100%">
                                   
                                                        <FooterStyle BackColor="White" ForeColor="#333333" />
                                                        <HeaderStyle BackColor="#336666" Font-Bold="False" ForeColor="White" Font-Size="Small" HorizontalAlign="Left"    />                                    
                                                        <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Left" Font-Size="Medium"/>
                                                        <RowStyle BackColor="White" ForeColor="#333333" />


                                                        <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                                        <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                                        <SortedAscendingHeaderStyle BackColor="#487575" />
                                                        <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                                        <SortedDescendingHeaderStyle BackColor="#275353" />

                                                        <Columns>
                                                                <asp:CommandField ButtonType="Button" SelectText="Select" ShowSelectButton= "true" HeaderStyle-HorizontalAlign = "Left"  ControlStyle-BackColor="#84A3A3" ItemStyle-Height="30px" />
                                                                <asp:BoundField  DataField="TCL"  HeaderText="TCL"  HeaderStyle-HorizontalAlign = "Left" ItemStyle-Height="35px"></asp:BoundField>
                                                                <asp:BoundField  DataField="UserTypeCd"  HeaderText="User Type"  HeaderStyle-HorizontalAlign = "Left" ItemStyle-Height="35px"></asp:BoundField>
                                                                <asp:BoundField  DataField="UserTypeDesc"  HeaderText="User Type Desc."  HeaderStyle-HorizontalAlign = "Left" ItemStyle-Height="35px"></asp:BoundField>
				                                                                  
                                  
                                                        </Columns>
                                                        </asp:GridView>
                                                    </div>


                                            </td>

                      
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="EmpDoclbl" runat="server" Text="This Account Doc Master#" Visible="false">
                                                </asp:Label>
                                            </td>
                                           <td align="left" colspan="3">
                                                <asp:TextBox ID = "EmpDoctorTextBox" runat="server" MaxLength="6" Visible="false" ></asp:TextBox>
                                          </td>
                                        </tr>
                                        </table>             
                                      </td>
                                     </tr>
                                   </asp:Panel>

             
                                   <tr>
                                    <td>
                                      <table id="tbldemo" border="1px" width="100%">
                                       <tr>
                                        <td colspan="4" class="tableRowSubHeader"  align="left">
                                            Demographics
                                        </td>
                                      </tr>
                                      <tr>

                                        <td colspan="4" >
                                         <table class="style3" > 
                                            <tr>
                                                <td>
                                                 First Name:
                                                </td>
                                                <td align="left"  >
                                                    <asp:TextBox width="280px" ID = "first_nameTextBox" runat="server" ></asp:TextBox>
                                                </td>
                                                <td>
                                                    MI:
                                                </td>
                                                <td>
                                                  <asp:TextBox ID = "miTextBox" runat="server" Width="15px"></asp:TextBox>
                                                </td>
                                                <td align="left" >
                                                  Last Name:
                                                </td>

                                                <td align="left" >
                                                    <asp:TextBox ID = "last_nameTextBox" width="280px" runat="server"></asp:TextBox>
                                                </td>
                                                <td>
                                                    Suffix: 
                                                </td>
                                                <td>
                                                    <asp:dropdownlist id="suffixddl" runat="server" Height="20px" Width="52px">
                                                     </asp:dropdownlist>
                                                </td>
                                            </tr>
                                         </table>
                                         </td>
                                      </tr>
                                      <tr>
                                        <td align="right"> Main Network Account:
                                        </td>

                                        <td>
                                            <asp:Label ID="Login_NameLabel" runat="server" ></asp:Label>
                                        </td>

                                        <td align="right"> EMail:
                                        </td>

                                        <td>
                                            <asp:Label ID="Receiver_eMailLabel" runat="server"></asp:Label>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td align="right">
                                            <asp:Label ID="Authlbl" runat="server" Text ="Employee #"></asp:Label>

                                         </td>
                                        <td>
                                            <asp:TextBox ID="AuthorizationEmpNumTextBox" runat="server" Enabled="false"></asp:TextBox>
                                        </td>
                                        <td align="right">
                                            Position:
                                        </td>
                                        <td>
                                            <asp:Label id="user_position_descLabel" runat="server"></asp:Label>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td align="right">
                                            Supplemental Support Staff:
                                        </td>
                                        <td align="left" colspan="3">
                                                <asp:RadioButtonList ID = "SuppSupportrbl" runat="server" RepeatDirection="Horizontal" AutoPostBack="true">
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                    <asp:listItem>No</asp:listItem>
                                                </asp:RadioButtonList>

                                        </td>
                      
                                      </tr>

                                      <tr>
                                        <td align="right" width="20%" >
                                            <asp:Label ID="lblpos" runat="server" Text="Position Description:" />
                                        </td>
                                        <td colspan = "3" align="left" >
                                           <asp:DropDownList ID="Rolesddl" runat="server"  AutoPostBack="true"  >
                                    
                                          </asp:DropDownList>

                                            <asp:TextBox ID="user_position_descTextBox" runat="server" Width="250px" Visible="false"></asp:TextBox>
                                            <asp:TextBox ID="PositionNumTextBox"  runat="server" Width="50px" Visible="false"></asp:TextBox>
                                        </td>
                                    </tr>
                                      <tr>
                                        <td align="right" >
                                        Entity:
                                        </td>
                                        <td align="left">
                                        <asp:DropDownList ID ="entity_cdddl" runat="server" AutoPostBack ="True" >
            
                                        </asp:DropDownList>
                                        </td>
       
                                        <td align="right" >
                                            <asp:Label ID="lbldept" runat="server" Text = "Department Name:" />
                                        </td>
                                        <td align="left"  >
                                        <asp:DropDownList ID ="department_cdddl" runat="server" Width="250px" AutoPostBack="True" >
            
                                        </asp:DropDownList>
                                        </td>
                                    </tr>
                                      <tr>
                                        <td  align="right">
                                            Address 1:
                                          </td>
                                        <td align="left"> 
                                             <asp:TextBox ID = "address_1textbox" runat="server" Width="250px"></asp:TextBox>
                                         </td>
                                        <td align="right" >
                                            Address 2:
                                         </td>
                                        <td align="left" >
                                          <asp:TextBox ID = "address_2textbox" runat="server" Width="250px"></asp:TextBox>
                                         </td>

                                    </tr>
                                     <tr>
                                         <td align="right">
                                                <asp:Label id="lblcity" runat="server" width="50px"  text="City:" />
                                            </td>
                                            <td align="left" colspan="3">
                                              <asp:TextBox ID = "citytextbox" runat="server" width="500px"></asp:TextBox>
                                            </td>


                                       </tr>

                                      <tr>

                                           <td align="right">
                                                <asp:Label ID="lblstate" runat="server" Text="State:" Width="50px" />
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID = "statetextbox" runat="server" Width="50px"></asp:TextBox>
                                             </td>
                                            <td align="right" >

                                                <asp:Label ID="lblzip" runat="server" Text="Zip:" Width="25px" />
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID = "ziptextbox" runat="server" Width="50px"></asp:TextBox>
                                            </td>
                                    </tr>
                                      <tr>
                                        <td align="right"  >
                                        Phone Number:
                                        </td>
                                        <td align="left" >
                                        <asp:TextBox ID = "phoneTextBox" runat="server" MaxLength="12"></asp:TextBox>
                                        </td>
       
                                        <td align="right" >
                                        Pager Number:
                                        </td>
                                        <td align="left" >
                                       <asp:TextBox ID = "pagerTextBox" runat="server" MaxLength="12"></asp:TextBox>
                                        </td>
                                    </tr>

                                      <tr>
                                       <td align="right" >
                                        Fax:
                                       </td>
                                        <td align="left" >
            
                                            <asp:TextBox ID = "faxtextbox" runat="server" MaxLength="12"></asp:TextBox>
                                         </td>
         
                                         <td align="right" >
                                        Email:
                                        </td>
                                        <td align="left" >
                                       <asp:TextBox ID = "e_mailTextBox" runat="server" Width="250px" Enabled="false"></asp:TextBox>
                                        </td>
                                    </tr>
                                      <tr>
                                        <td align="right" >
                                        Location:
                                        </td>
                                        <td align="left" >
         
                                          <asp:DropDownList ID ="facility_cdddl" runat="server" AutoPostBack="True">
              
                                          </asp:DropDownList>
          
                                        </td>
        
                                        <td align="right" >
                                        Building:
                                        </td>
                                        <td align="left" >
                                            <asp:DropDownList ID="building_cdddl" runat="server" AutoPostBack="True" >
                                            </asp:DropDownList>
          
                                        </td>
                                     </tr>
                                       <tr>
                                            <td align="right" >
                                                Floor:
                                            </td>

                                            <td align="left">
                                                <asp:DropDownList ID="Floorddl" runat="server" AutoPostBack="True">
            
                                                </asp:DropDownList>
                            
                                            </td>
                                            <td>
                            
                                            </td>

                                            <td>
                            
                                            </td>

                                        </tr>
                                      <tr>

                                             <asp:Panel ID="PhysOfficepanel" runat="server" Visible="false">
                                              
                                                    <td align="right" style="width: 220px">
                                                        <asp:Label ID ="PhysoffLabel" runat="server" Text="Physicians Office:"  >
                                                        </asp:Label>

                                                    </td>
                                                    <td colspan="3" align="left">
                                                        <asp:DropDownList ID = "PhysOfficeddl" runat="server"  Width="90%">
    
                                                        </asp:DropDownList>
                                                        <asp:TextBox ID = "group_nameTextBox" runat="server"  Visible="false">
                                                            </asp:TextBox>
                                                    </td>
                                              </asp:Panel>

                                        </tr>

<%--                                        <tr>
                                            <td colspan="4" align="center">
                                             <div>
                                                    <asp:Panel ID="cpmModelAfterPanel" runat="server" Visible="false">

                                                        <table>
                                                            <tr>
                                                                <td>
                                                                        <asp:Label id="currcpmmodlbl" runat="server" Text="Current CPM Model:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                             <asp:TextBox ID="ViewCPMModelTextBox" runat="server"  Enabled="false" Width="500px"></asp:TextBox>

                                                                             <asp:TextBox ID="CPMModelTextBox" runat="server"   Visible="false" Width="500px"></asp:TextBox>
                                                                             <asp:TextBox ID="provmodelTextBox" runat="server"   Visible="false" ></asp:TextBox>

                                                                </td>
                                                            </tr>

                                                  
                                                        </table>                                                
                                                
                                                    </asp:Panel>
                                                </div>
                                           </td>
                                        </tr>
--%>

<%--                                        <tr>
                                            <td colspan="4" align="center">
                                             <div>
                                                <asp:Panel ID="cpmModelEmplPanel" runat="server" Visible="false">
                                                
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                   <asp:Label id="cpmempModellbl" runat="server" Text="Select emplyee for CPM Model:"></asp:Label>

                                                            </td>
                                                            <td>
                                                              <asp:DropDownList ID="CPMEmployeesddl" runat="server" Width="500px" AutoPostBack="true">
                                
                                                            </asp:DropDownList>
                                                                
                                                            </td>

                                                       </tr>
                                                  
                                                    </table>
                                                </asp:Panel>
                                             </div>
                                           </td>
                                        </tr>
--%>

<%--                                        <tr>

                                            <td colspan="4" align="center">
                                                <div>

                                                <asp:Panel ID="cpmPhysSchedulePanel" runat="server" Visible="false">
                                                
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                    <asp:Label id="schdeulbl" runat="server" Text="Schedulable:"></asp:Label>

                                                            </td>
                                                            <td>

                                                                <asp:RadioButtonList ID = "Schedulablerbl" runat="server" RepeatDirection="Horizontal">
                                                                    <asp:ListItem Value="yes">Yes</asp:ListItem>
                                                                    <asp:listItem Value="no">No</asp:listItem>
                                                                </asp:RadioButtonList>
                                                                <asp:TextBox ID="SchedulableTextbox" runat="server" Visible="false"></asp:TextBox>

                                                            </td>
                                                            <td>
                                                                <asp:Label ID="teltlbl" runat="server" text="Televox:" />
                                
                                                           </td>
                                                          <td>

                                                                <asp:RadioButtonList ID = "Televoxrbl" runat="server" RepeatDirection="Horizontal">
                                                                    <asp:ListItem Value="yes">Yes</asp:ListItem>
                                                                    <asp:listItem Value="no">No</asp:listItem>
                                                                </asp:RadioButtonList>
                                                                <asp:TextBox ID="TelevoxTextBox" runat="server" Visible="false"></asp:TextBox>
                                         
                                      
                                                          </td>
                                                        </tr>
                                                  
                                                    </table>
                                                </asp:Panel>                                            
                                             </div>
                                            </td>
                                        </tr>
--%>
                                         <tr>
                                            <td colspan="4">
                                             <div>
                                                <asp:Panel ID="contractPanel" runat="server" Visible="false">
                                                
                                               <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="otherVendorlbl" runat="server" Visible="false" Text="Unknow Vendor/Contractor:"></asp:Label>
                                                        </td>
                                                        <td align="left" colspan="3">
                                                            <asp:Label ID="VendorUnknowlbl" runat="server" Visible="False"></asp:Label>
                                                        </td>
                                    
                                                    </tr>

                                                  <tr>
                                                      <td align="right" >
                                                          <asp:Label ID="lblVendor" runat="server" Text="Contractor/Vendor Name:"></asp:Label>
                                                        </td>
                                                      <td align="left"  colspan="3">
                                                            <asp:TextBox ID ="VendorNameTextBox" runat="server" Width="500px" ></asp:TextBox>
                                                              <asp:DropDownList ID="Vendorddl" runat="server" Width="500px" Visible="false" AutoPostBack="true">
                                
                                                            </asp:DropDownList>
                                                            <asp:Label ID="VendorNumLabel" runat="server" Visible="False"></asp:Label>

                                                      </td>
                                                    </tr>
                                                </table>
                                                </asp:Panel>
                                             </div>

                                            </td>
                                        </tr>

                                      <tr>
                                        <td align="right" >
                                        Start Date:
                                        </td>
                                        <td align="left" >
                                           <asp:TextBox ID ="start_dateTextBox" runat="server" CssClass="calenderClass"></asp:TextBox>
                                        </td>
       
                                        <td align="right" >
                                        End Date:
                                        </td>
                                        <td align="left" >
                                            <asp:TextBox ID="end_dateTextBox" runat="server"  CssClass="calenderClass"></asp:TextBox>
                                        </td>
                                      </tr>
<%--                                      <tr>
                                       
                                        <td align="right" >
                                            Share Drive:
                                        </td>
                                        <td align="left" >
                                             <asp:TextBox ID="share_driveTextBox" runat="server" Width="250px"></asp:TextBox>
                                        </td>
                                        <td align="right" >
                                            Model After:
                                        </td>
                                        <td align="left"  > 
                                            <asp:TextBox ID="ModelAfterTextBox" runat="server" Width="250px"></asp:TextBox>
                                        </td>
                                       </tr>
                                      <tr>--%>
                                        <td align="right" >
                                            Request Close Date:
                                        </td>
                                        <td align="left" >
                                            <asp:TextBox ID= "close_dateTextBox" runat="server" Enabled="false"></asp:TextBox>
                                        </td>
                                        <td>
                                          <asp:Label ID ="department_cdLabel" runat="server" Visible="False"></asp:Label>
                                          <asp:Label ID ="TitleLabel" runat="server" Visible="False"></asp:Label>
                                          <asp:Label ID = "emp_type_cdLabel" runat="server" Visible="False"></asp:Label>
                                          <asp:Label ID = "SpecialtyLabel" runat="server" Visible ="False"></asp:Label>
                                           <asp:Label ID="suffixlabel" runat="server" Visible="False"></asp:Label>
                                           <asp:Label ID="receiver_client_numLabel" runat="server" Visible="false"></asp:Label>
                                           <asp:Label ID="userDepartmentCD"  runat="server" Visible="False"></asp:Label>

                                        </td>
                                        <td>
                                              <asp:Label ID = "facility_cdLabel" runat="server" Visible ="False"></asp:Label>
                                              <asp:Label ID = "entity_cdLabel" runat="server" Visible ="False"></asp:Label>
                                              <asp:Label ID = "building_cdLabel" runat="server" Visible ="False"></asp:Label>
                                              <asp:Label ID= "building_nameLabel" runat="server" Visible="False" ></asp:Label>
                                              <asp:Label ID= "nt_loginlabel" runat="server" Visible="False" ></asp:Label>
                                              <asp:TextBox ID="FloorTextBox" runat="server" Visible="false"></asp:TextBox>
                                              <asp:Label ID="RoleNumLabel" runat="server" Visible="false"></asp:Label>
                                              <asp:Label ID="PositionRoleNumLabel" runat="server" Visible="false"></asp:Label>
                                              
                                              <asp:Label ID="GNumberGroupNumLabel"  runat="server" visible="false"></asp:Label>
                                            <asp:Label ID="LocationOfCareIDLabel"  runat="server" visible="false"></asp:Label>

                                            <asp:Label ID="CommLocationOfCareIDLabel" runat="server" visible="false"></asp:Label>
                                              <asp:Label ID="CommNumberGroupNumLabel" runat="server" visible="false"></asp:Label>

                                             <asp:Label ID ="CoverageGroupNumLabel"  runat="server" visible="false"></asp:Label>
                                             <asp:Label id="NonPerferedGroupnumLabel" runat="server" Visible="false"></asp:Label>
                                             <asp:TextBox ID="group_numTextBox" runat="server" Enabled="false" Visible="false"></asp:TextBox>
                                             <asp:TextBox ID="InvisionGroupNumTextBox" runat="server"  Enabled="false" Visible="false"></asp:TextBox>


                                       </td>
                                      </tr>

                                      <tr>
                                        <td colspan="4" align="left" >
                                            Comments:
                                        </td>
                                      </tr>
                                      <tr>
                                        <td colspan = "4" align="left" >
                                            <asp:TextBox ID="request_descTextBox" runat ="server" TextMode = "MultiLine" Width="100%"></asp:TextBox>
                                         </td>
                                      </tr>
                                       <tr>
                                         <td align="center" colspan="4">

   	                                      <%-- =================== RSA Token Address ===============================--%>

                                           <asp:Panel ID="pnlTokenAddress" runat="server"  Visible="false">

                                             <table id="Tokentbl"  border="3px" width="100%">
                                              <tr>
                                                 <td align="left" class="tableRowSubHeader" colspan="4">
                                                       Cell Phone Information for Remote Access
                                                  </td>
                                                </tr>

                                                <tr>
                                                    <td colspan="4" align="center">
                                                        <asp:Label id="lblTokenError" runat="server" BackColor="Red" Visible="false" Font-Size="Large" Font-Bold="True"></asp:Label>
                                                    </td>
                                                </tr>
                                
                                                <tr>

                                                    <td align="right">
                                                        Provider:
                                                    </td>
                                                    <td align="left">
                                                        <asp:DropDownList ID="CellPhoneddl" runat="server" Width="250px" AutoPostBack="true">
                                
                                                        </asp:DropDownList>
                                                        <asp:TextBox ID="otherCellprov" runat="server" Visible="false"  MaxLength="25" ></asp:TextBox>
                                                    </td>
                                                    <td align="right">
                                                        Number:
                                                    </td>
                                                    <td align="left">
                                                        <asp:TextBox ID ="TokenPhoneTextBox" runat="server"  Enabled="false" Width="250px"
                                                             ></asp:TextBox>
                                                    </td>

                                                </tr>

                <%--                                <tr>
                                                    <td align="right">
                                                        Comments:
                                                    </td>
                                                    <td align="left" colspan="3">
                                                        </asp:TextBox>

                                                    </td>

                
                                                </tr>--%>

                                                <tr align="center">
                                                    <td align="left" colspan="4" >

                                                        <asp:TextBox ID="CellProvidertxt" runat="server" Visible="false"></asp:TextBox>
                                                        <asp:TextBox ID ="TokenNameTextBox" runat="server"  Width="80%" Visible="false"></asp:TextBox>
                                                        <asp:TextBox ID ="TokenAddress1TextBox" Width="80%" runat="server" Visible="false"></asp:TextBox>
                                                        <asp:TextBox ID ="TokenAddress2TextBox" runat="server" Width="80%" Visible="false"></asp:TextBox>
                                                          <asp:TextBox ID ="TokencommentsTextBox" runat="server"  Width="90%" TextMode="MultiLine" Visible="false"></asp:TextBox>

                                                        <asp:TextBox ID ="TokenStTextBox" runat="server" Visible="false"></asp:TextBox>
                                                      <asp:TextBox ID ="TokenZipTextBox" runat="server" Visible="false"></asp:TextBox>
                                                        <asp:TextBox ID ="TokenCityTextBox" runat="server" Width="80%" Visible="false"></asp:TextBox>
                                                        <asp:TextBox ID="masterRole" runat="server"  Visible="false"></asp:TextBox>

                                                    </td>
                                                </tr>
            
                                              </table>
            			

                                             </asp:Panel>


                <%--                                
                                                <tr>
                                                    <td align="right">
                                                        Name:
                                                    </td>

                                                    <td align="right">
                                                        Address Line 1:
                                                    </td>
                                                    <td align="left">
                                                    </td>

                                                    <td align="right">
                                                        Address Line 2:
                                                    </td>
                                                    <td align="left">
                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td align="right">
                                                        City:
                                                    </td>
                                                    <td align="left">
                                                    </td>

                                                    <td align="right">
                                                        State:
                                                    </td>
                                                    <td >
                                                    </td>
                                                    <td align="right">
                                                        Zip:
                                                    </td>
                                                    <td align="left">

                                                    </td>
                        
                        
                                                </tr>
                --%>


                                       </td>
                                     </tr

                                     <tr>
                                       <td colspan = "4">
                                            <asp:Label ID="HDemployeetype" runat="server" Visible="False"></asp:Label>
                                        </td>
                                      </tr>

                                     </table>
                                    </td>
                                  </tr>
                       </table>
                     </ContentTemplate>
                  </ajaxToolkit:TabPanel>

                  <ajaxToolkit:TabPanel ID="tpAddItems" runat="server" Width="100%"  BackColor="Gainsboro" EnableTheming="False">
                    <HeaderTemplate>
                        <asp:Label ID="lblinner1" runat="server" Text="Add Accounts to Request" Font-Bold="true" ForeColor="Black"></asp:Label>
                    </HeaderTemplate>
                        <ContentTemplate>

                           <table border="0"  style="width: 100%">

                                                                             <tr>
                                                     <td colspan = "2" align="left">
                                                        <asp:Button runat="server" ID ="btnSubmitAddRequest" Text = "Submit New Account to Request"
                                                              Width="250px"  BackColor="#336666" ForeColor="White" Visible="False" /> 
                                                     </td>
                                                    <td colspan = "2" align="left"> 
                                                        <asp:Button ID = "btnAddComments" runat = "server" Text ="Add Comments " Width="175px" 
                                                           BackColor="#336666" ForeColor="White" Visible="false"/>


                                                     </td>

                                                  </tr>

                                                     <tr style="background-color:#FFFFCC; font-weight:bold">
                                                        <td colspan="2">
                                                        Accounts available
                                                        </td>
                                                        <td colspan="2">
                                                        Accounts to be added
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2"  valign="top" style="width: 50%">
                                                          <asp:GridView ID="grAvailableAccounts" runat="server" AllowSorting="True" 
                                                            AutoGenerateColumns="False" BackColor="White" BorderColor="#336666"
                                                            BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                                                            EmptyDataText="No Applications Found" EnableTheming="False" 
                                                            DataKeyNames="Applicationnum,ApplicationDesc"
                                                            Font-Size="Small" GridLines="Horizontal" Width="100%">
                                   
                                                            <FooterStyle BackColor="White" ForeColor="#333333" />
                                                            <HeaderStyle BackColor="#336666" Font-Bold="False" ForeColor="White" Font-Size="Small" HorizontalAlign="Left"    />                                    
                                                            <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Left" Font-Size="Medium"/>
                                                            <RowStyle BackColor="White" ForeColor="#333333" />
                                                            <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                                            <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                                            <SortedAscendingHeaderStyle BackColor="#487575" />
                                                            <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                                            <SortedDescendingHeaderStyle BackColor="#275353" />

                                                            <Columns>
                                                                   <asp:CommandField ButtonType="Button" ShowSelectButton= "True" >
                                                                    <ControlStyle BackColor="#84A3A3" />
                                                                   <HeaderStyle HorizontalAlign="Left" />
                                                                   <ItemStyle Height="30px" />
                                                                   </asp:CommandField>
                                                                    <asp:BoundField  DataField="ApplicationDesc"  HeaderText="Application">
                                                                   <HeaderStyle HorizontalAlign="Left" />
                                                                   <ItemStyle Height="35px" />
                                                                   </asp:BoundField>
        				                                                                  
                                  
                                                           </Columns>
                                                         </asp:GridView>
		                                                </td>
                                                        <td colspan="2"  valign="top" style="width: 50%">
                                                            <asp:GridView ID="grAccountsToAdd" runat="server" AllowSorting="True" 
                                                            AutoGenerateColumns="False" BackColor="White" BorderColor="#336666"
                                                            BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                                                            EmptyDataText="Select accounts to add" EnableTheming="False" 
                                                            DataKeyNames="Applicationnum,ApplicationDesc"
                                                            Font-Size="Small" GridLines="Horizontal" Width="100%">
                                   
                                                            <FooterStyle BackColor="White" ForeColor="#333333" />
                                                            <HeaderStyle BackColor="#336666" Font-Bold="False" ForeColor="White" Font-Size="Small" HorizontalAlign="Left"    />                                    
                                                            <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Left" Font-Size="Medium"/>
                                                            <RowStyle BackColor="White" ForeColor="#333333" />
                                                            <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                                            <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                                            <SortedAscendingHeaderStyle BackColor="#487575" />
                                                            <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                                            <SortedDescendingHeaderStyle BackColor="#275353" />

                                                            <Columns>
                                                                                            
                                                                        <asp:CommandField ButtonType="Button" SelectText="Cancel" 
                                                                            ShowSelectButton= "True">
                                                                        <ControlStyle BackColor="#84A3A3" />
                                                                        <HeaderStyle HorizontalAlign="Left" />
                                                                        <ItemStyle Height="30px" />
                                                                        </asp:CommandField>
                                                                        <asp:BoundField  DataField="ApplicationDesc"  HeaderText="Application">
                                                                        <HeaderStyle HorizontalAlign="Left" />
                                                                        <ItemStyle Height="35px" />
                                                                        </asp:BoundField>
				                                                                  
                                  
                                                            </Columns>

                                                        </asp:GridView>
		                                              </td>
                                                     </tr>
                                                     <tr>
                                                        <td colspan="4">
                                                            <asp:Label ID="lblwidth" Visible="False" runat="server" Width="100%"></asp:Label>
                                                        </td>
                                                     </tr>
                                                </table>
                                    </ContentTemplate>
                  </ajaxToolkit:TabPanel>
                  
                  <ajaxToolkit:TabPanel ID="tpRemoveItems" runat="server"  BackColor="Gainsboro" Visible = "True">
                    <HeaderTemplate > 
                        <asp:Label ID="Label4" runat="server" Text="Remove Accounts From Request" Font-Bold="true" ForeColor="Black"></asp:Label>
                    </HeaderTemplate>
                      <ContentTemplate>

                        <table id="tblRemoveItems" width="100%" border="0"  style="background-color:#DCDCDC">
                            <tr>
                                <td  colspan="2" align="left">
                                    <asp:Button runat="server" ID ="btnInvalidAccts" Text = "Submit Delete Account from Request" 
                                     Width="250px" BackColor="#336666" ForeColor="White" Visible="false" />
                                </td>
                                <td colspan="2" align="left"> 
                                    <asp:Button ID = "btnAddDeleteComments" runat = "server" Text ="Add Comments " Width="175px" 
                                        BackColor="#336666" ForeColor="White" Visible="false"/>


                                    </td>
                            </tr>
                            <tr style="background-color:#FFFFCC; font-weight:bold">
                                <td colspan="2" >
                                Accounts
                                </td>
                                <td colspan="2">
                                Accounts to remove
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2"  valign="top"  style="width: 50%">
                                    <asp:GridView ID="gvCurrentApps" runat="server" AllowSorting="True" 
                                        AutoGenerateColumns="False" BackColor="White" BorderColor="#336666"
                                        BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                                        EmptyDataText="No Applications Found" EnableTheming="False" DataKeyNames="account_request_seq_num, ApplicationDesc, ApplicationNum"
                                        Font-Size="Medium" GridLines="Horizontal" Width="100%">
                                   
                                        <FooterStyle BackColor="White" ForeColor="#333333" />
                                        <HeaderStyle BackColor="#336666" Font-Bold="False" ForeColor="White" Font-Size="Small" HorizontalAlign="Left"    />                                    
                                        <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Left" Font-Size="Medium"/>
                                        <RowStyle BackColor="White" ForeColor="#333333" />
                                        <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                        <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                        <SortedAscendingHeaderStyle BackColor="#487575" />
                                        <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                        <SortedDescendingHeaderStyle BackColor="#275353" />

                                        <Columns>
                                                                                            
                                                    <asp:CommandField ButtonType="Button" SelectText="Select" ShowSelectButton= "true" ControlStyle-BackColor="#84A3A3" ItemStyle-Height="30px"/>
                                                    <asp:BoundField  DataField="ApplicationDesc"  HeaderText="Application" HeaderStyle-HorizontalAlign="Left"  ItemStyle-Height="35px"></asp:BoundField>
				                                                                  
                                  
                                        </Columns>
                                        </asp:GridView>
		                            </td>
                                    <td colspan="2" valign="top"  style="width: 50%">
                                        <asp:GridView ID="gvRemoveAccounts" runat="server" AllowSorting="True" 
                                                AutoGenerateColumns="False" BackColor="White" BorderColor="#336666"
                                                BorderStyle="Double" BorderWidth="3px" CellPadding="0" 
                                                EmptyDataText="Select applications to be removed" EnableTheming="False" DataKeyNames="account_request_seq_num, ApplicationDesc, ApplicationNum"
                                                Font-Size="Medium" GridLines="Horizontal" Width="100%" >
                                   
                                            <FooterStyle BackColor="White" ForeColor="#333333" />
                                            <HeaderStyle BackColor="#336666" Font-Bold="False" ForeColor="White" Font-Size="Small" HorizontalAlign="Left"    />                                    
                                            <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Left" Font-Size="Medium"/>
                                            <RowStyle BackColor="White" ForeColor="#333333" />
                                            <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                            <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                            <SortedAscendingHeaderStyle BackColor="#487575" />
                                            <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                            <SortedDescendingHeaderStyle BackColor="#275353" />

                                            <Columns>
                                                    <asp:CommandField ButtonType="Button" SelectText="Cancel" ShowSelectButton= "true"   ControlStyle-BackColor="#84A3A3" ItemStyle-Height="30px" />
                                                    <asp:BoundField  DataField="ApplicationDesc"  HeaderText="Application" HeaderStyle-HorizontalAlign="Left" ItemStyle-Height="30px" ></asp:BoundField>
                                            </Columns>
                                            </asp:GridView>
                                        </td>
                                </tr>
                        </table>
                        </ContentTemplate>
                </ajaxToolkit:TabPanel>

                  <ajaxToolkit:TabPanel ID="tpPendingRequests" runat="server" TabIndex="0"  BorderStyle="Solid" BorderWidth="1px">
                      <HeaderTemplate > 
                            <asp:Label ID="Label3" runat="server" Text="Pending Requests" Font-Bold="true" ForeColor="Black" ></asp:Label>
                    
                      </HeaderTemplate>
                      <ContentTemplate>
                          <table border="0" width = "100%">
                            <tr>
                                <td colspan = "4">
                                      <asp:Table ID = "tblPendingQueues" runat = "server">
                                     </asp:Table>
            
                                </td>
        
                            </tr>
                           </table>
                       </ContentTemplate>
                  </ajaxToolkit:TabPanel>

                  <ajaxToolkit:TabPanel ID="tpRequestItems" runat="server" TabIndex="0"   BorderStyle="Solid" BorderWidth="1px">
                      <HeaderTemplate > 
                            <asp:Label ID="Label6" runat="server" Text="Request Items" Font-Bold="true" ForeColor="Black" ></asp:Label>
                    
                      </HeaderTemplate>
                      <ContentTemplate>
                          <table border="0" width = "100%">
                            <tr>
                                <td colspan = "4">
                                      <asp:Table ID = "tblRequestItems" runat = "server">
                                     </asp:Table>
            
                                </td>
        
                            </tr>
                           </table>
                       </ContentTemplate>
                  </ajaxToolkit:TabPanel>

                  <ajaxToolkit:TabPanel ID="TPActionItems" runat="server" TabIndex="0"  BorderStyle="Solid" BorderWidth="1px">
                      <HeaderTemplate > 
                            <asp:Label ID="Label7" runat="server" Text="Action Items" Font-Bold="true" ForeColor="Black" ></asp:Label>
                    
                      </HeaderTemplate>
                      <ContentTemplate>
                          <table border="0" width = "100%">
                            <tr>
                                <td colspan = "4">
                                      <asp:Table ID = "tblActionItems" runat = "server">
                                     </asp:Table>
            
                                </td>
        
                            </tr>
                           </table>
                       </ContentTemplate>
                  </ajaxToolkit:TabPanel>

                  <ajaxToolkit:TabPanel ID="TPDemoChanges" runat="server" TabIndex="0" BackColor="Gainsboro" Visible = "False">
                      <HeaderTemplate > 
                            <asp:Label ID="Label2" runat="server" Text="Demo Changes" Font-Bold="true" ForeColor="Black" ></asp:Label>
                    
                      </HeaderTemplate>
                      <ContentTemplate>
                          <table border="0" width = "100%">
                            <tr>
                                <td colspan = "4">
                          <table id="Table1" border="3px" width="100%">
                            <tr>
                                <td colspan="4" class="tableRowSubHeader"  align="left">
                                    Demographics
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Emplbl" runat="server" Text="Employee Number:" ></asp:Label>
                                </td>

                                <td>
                                <asp:Label ID ="siemensempnumLabel" runat="server"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="netlbl" runat="server" Text="Network Logon:" >
                                    </asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID = "loginnameTextBox" runat="server" Enabled="false"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <asp:Label ID="lblcurrname" runat="server" Text="Origional Name" Font-Bold="True" Font-Size="Large"></asp:Label>
                                </td>
                            </tr>
                            <tr>

                                <td colspan="4" >
                                    <table class="style3" > 
                                      <tr>
                                        <td>
                                            First Name:
                                        </td>
                                        <td align="left"  >
                                            <asp:TextBox width="280px" ID = "DemofirstnameTextBox" runat="server" Enabled="false" ></asp:TextBox>
                                        </td>
                                        <td>
                                            MI:
                                        </td>
                                        <td>
                                            <asp:TextBox ID = "DemomiTextBox" runat="server" Width="15px" Enabled="false"></asp:TextBox>
                                        </td>
                                        <td align="left" >
                                            Last Name:
                                        </td>

                                        <td align="left" >
                                            <asp:TextBox ID = "DemolastnameTextBox" width="280px" runat="server" Enabled="false"></asp:TextBox>
                                        </td>
                                      </tr>
                                     </table>
                                </td>
                            </tr>
                             <tr>
                                <td colspan="4">
                                     <asp:Label ID="lblNewName" runat="server" Text="Name Change " Font-Bold="True" Font-Size="Large"></asp:Label>

                                </td>
                            </tr>

                            <tr>
                                <td colspan="4">
                                
                                    <table class="style3" > 
                                      <tr>
                                        <td>
                                            New First Name:
                                        </td>
                                        <td align="left"  >
                                            <asp:TextBox width="280px" ID = "NewFirstNameTextBox" runat="server" Enabled="false" ></asp:TextBox>
                                        </td>
                                        <td>
                                             New MI:
                                        </td>
                                        <td>
                                            <asp:TextBox ID = "NewMITextBox" runat="server" Width="15px" Enabled="false" ></asp:TextBox>
                                        </td>
                                        <td align="left" >
                                            New Last Name:
                                        </td>

                                        <td align="left" >
                                            <asp:TextBox ID = "NewLastNameTextBox" width="280px" runat="server" Enabled="false" ></asp:TextBox>
                                        </td>
                                      </tr>
                                     </table>                                
                                
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                   <asp:Label ID="lblCurraddress" runat="server" Text="Origional Address" Font-Bold="True" Font-Size="Large"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td  align="right">
                                    Address 1:
                                    </td>
                                <td align="left"> 
                                        <asp:TextBox ID = "Demoaddress1textbox" runat="server" Width="250px" Enabled="false"></asp:TextBox>
                                    </td>
                                <td align="right" >
                                    Address 2:
                                    </td>
                                <td align="left" >
                                    <asp:TextBox ID = "Demoaddress2textbox" runat="server" Width="250px" Enabled="false"></asp:TextBox>
                                    </td>

                            </tr>
                            <tr>
                                <td align="right">
                                    <asp:Label id="Label5" runat="server" width="50px"  text="City:" />
                                </td>
                                <td align="left" colspan="3">
                                    <asp:TextBox ID = "DemoCityTextBox" runat="server" width="500px" Enabled="false"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <asp:Label ID="Label8" runat="server" Text="State:" Width="50px" />
                                </td>
                                <td align="left">
                                    <asp:TextBox ID = "DemoStateTextBox" runat="server" Width="50px" Enabled="false"></asp:TextBox>
                                    </td>
                                <td align="right" >

                                    <asp:Label ID="Label9" runat="server" Text="Zip:" Width="25px" />
                                </td>
                                <td align="left">
                                    <asp:TextBox ID = "DemoZipTextBox" runat="server" Width="50px" Enabled="false"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="right"  >
                                Phone Number:
                                </td>
                                <td align="left" >
                                <asp:TextBox ID = "DemoPhoneTextBox" runat="server" Enabled="false"></asp:TextBox>
                                </td>
       
                                <td align="right" >
                                Pager Number:
                                </td>
                                <td align="left" >
                                <asp:TextBox ID = "DemoPagerTextBox" runat="server" Enabled="false"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" >
                                Fax:
                                </td>
                                <td align="left" >
            
                                    <asp:TextBox ID = "DemoFaxTextBox" runat="server" Enabled="false"></asp:TextBox>
                                </td>
         
                                <td align="right" >
                                Email:
                                </td>
                                <td align="left" >
                                <asp:TextBox ID = "DemoemailTextBox" runat="server" Width="250px" Enabled="false"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                   <asp:Label ID="lblnewaddress" runat="server" Text="Change Address" Font-Bold="True" Font-Size="Large"></asp:Label>

                                </td>
                            </tr>
                            <tr>
                                <td  align="right">
                                    Address 1:
                                    </td>
                                <td align="left"> 
                                        <asp:TextBox ID = "Newaddress1TextBox" runat="server" Width="250px" Enabled="false"></asp:TextBox>
                                    </td>
                                <td align="right" >
                                    Address 2:
                                    </td>
                                <td align="left" >
                                    <asp:TextBox ID = "Newaddress2TextBox" runat="server" Width="250px" Enabled="false"></asp:TextBox>
                                    </td>

                            </tr>
                            <tr>
                                <td align="right">
                                    <asp:Label id="CityLabel2" runat="server" width="50px"  text="City:" />
                                </td>
                                <td align="left" colspan="3">
                                    <asp:TextBox ID = "NewCityTextBox" runat="server" width="500px" Enabled="false"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <asp:Label ID="StateLabel2" runat="server" Text="State:" Width="50px" />
                                </td>
                                <td align="left">
                                    <asp:TextBox ID = "NewStateTextBox" runat="server" Width="50px" Enabled="false"></asp:TextBox>
                                    </td>
                                <td align="right" >

                                    <asp:Label ID="lblNewZip" runat="server" Text="Zip:" Width="25px" />
                                </td>
                                <td align="left">
                                    <asp:TextBox ID = "NewZipTextBox" runat="server" Width="50px" Enabled="false" ></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="right"  >
                                Phone Number:
                                </td>
                                <td align="left" >
                                <asp:TextBox ID = "NewPhoneTextBox" runat="server" Enabled="false" ></asp:TextBox>
                                </td>
       
                                <td align="right" >
                                Pager Number:
                                </td>
                                <td align="left" >
                                <asp:TextBox ID = "NewPagerTextBox" runat="server" Enabled="false"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" >
                                Fax:
                                </td>
                                <td align="left" >
            
                                    <asp:TextBox ID = "NewFaxTextBox" runat="server" Enabled="false" ></asp:TextBox>
                                </td>
                                <td>
                                    Account Req Num:
                                    <asp:Label id="AccountRequestNumLabel" runat="server" Visible="true"></asp:Label>
                                </td>
                                <td>
                                
                                </td>

                            </tr>
                            <tr>
                                <td>
                                
                                </td>
                                <td>
                                    <asp:Label ID="close_dateLabel" runat="server" Visible="false"></asp:Label>
                        
                                </td>
                                <td>
                                
                                    <asp:Label id="approvalLabel" runat="server" Visible="false"></asp:Label>
                                </td>
                            
                                <td>
                                    <asp:Label ID="ApprovalClientNumLabel" runat="server" Visible="false"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                            
                                <td>
                                    <asp:Label ID ="ApprovalNameLabel" runat="server" Visible="false"></asp:Label>
                                </td>
                            </tr>

                    </table>            
                                </td>
        
                            </tr>
                           </table>
                       </ContentTemplate>
                  </ajaxToolkit:TabPanel>

                 <ajaxToolkit:TabPanel ID="TpEditGrid" runat="server" TabIndex="0" BackColor="Gainsboro" Visible = "False">
                      <HeaderTemplate > 
                            <asp:Label ID="Label10" runat="server" Text="Edit Items" Font-Bold="true" ForeColor="Black" ></asp:Label>
                    
                      </HeaderTemplate>
                      <ContentTemplate>
                          <table border="0" width = "100%">
                              <tr>
                                  <td>
                                      <table>
                                          <tr>
                                              <td colspan="3">
                                                  <asp:Label runat="server" ID="lblloginmsg" Visible="false" BackColor="red" Font-Bold="true"></asp:Label>
                                              </td>
                                          </tr>
                                          <tr>
                                              <td colspan="3" align="center" > To Close this Request</td>
                                            </tr>
                                          <tr>
                                              <td colspan="3" align="center">
                                                  <asp:Button ID="btnCloseEntireRequest" runat="server" Text="Close Request"
                                                    CssClass="btnhov" BackColor="#006666"  Width="189px" />
                                              </td>
                                          </tr>
                                          <tr>
                                              <td colspan="3"></td>
                                          </tr>
                                           <tr>
                                              <td colspan="3"></td>
                                          </tr>
                                          <tr>
                                              <td colspan="3" align="center"> To update Items</td>
                                          </tr>

                                          <tr>
                                              <td>
                                                   <asp:Button ID="btnCommitLogin" runat="server" Text="Update Item"
                                                    CssClass="btnhov" BackColor="#006666"  Width="179px" />
                                              </td>
                                               <td>
                                                  <asp:Label ID="lblappname" runat="server" Text="App Name:"></asp:Label>
                                                  <asp:TextBox ID="txtAppName" runat="server"  Enabled="false"></asp:TextBox>
                                              </td>
                                               <td>
                                                  <asp:Label ID="lblNewLogin" runat="server" Text="New Login:"></asp:Label>
                                                  <asp:TextBox ID="txtLogin" runat="server"  Enabled="True"></asp:TextBox>
                                              </td>
                                          </tr>
                                          <tr>
                                              <td colspan ="3">
                                                                  Change All Accounts?

                                                    <asp:RadioButtonList ID = "AllItemsrbl" runat="server" RepeatDirection="Horizontal" AutoPostBack="true">
                                                        <asp:ListItem  Value="yes">Yes</asp:ListItem>
                                                        <asp:listItem  Selected="True" Value="no">No</asp:listItem>

                                                </asp:RadioButtonList>


                                              </td>
                                          </tr>
                                      </table>

                                  </td>
                            </tr>
                              <tr>





                                  <asp:Label ID="HDAcctnum" runat="server"  Visible="false"></asp:Label>
                                  <asp:Label ID="HDAcctSeq" runat="server" Visible="false"></asp:Label>
                                  <asp:Label ID="HDAppnum" runat="server" Visible="false"></asp:Label>
                                  <asp:Label ID="HDAllitems" runat="server" Visible="false"></asp:Label>
                              </tr>
                              <tr>
                                  <td>
                                      <asp:GridView ID="EditGrid" runat="server" 
                                            DataKeyNames="account_request_num,account_request_seq_num,ApplicationNum,login_name,ApplicationDesc" 
                                               AllowSorting="True" 
                                           BackColor="White" BorderColor="#336666" 
                                           BorderStyle="Double" BorderWidth="3px" CellPadding="1" 
                                          EmptyDataText="No records were found"   AutoGenerateColumns="false"
                                          EnableTheming="False" Font-Size="Medium" GridLines="Horizontal" Width="100%">
                                        
                                        <FooterStyle BackColor="White" ForeColor="#333333" />
                                       <HeaderStyle BackColor="#336666" Font-Bold="False" Font-Size="X-Small" 
                                           ForeColor="White" HorizontalAlign="Left" VerticalAlign="Middle" />
                                       <PagerStyle BackColor="#336666" Font-Size="Medium" ForeColor="White" 
                                           HorizontalAlign="Left" />
                                       <RowStyle BackColor="White" ForeColor="#333333" Font-Size="Small" />
                                       <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />

                                       <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                       <SortedAscendingHeaderStyle BackColor="#487575" />
                                       <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                       <SortedDescendingHeaderStyle BackColor="#275353" />
                                       <Columns>

                                           <asp:CommandField ButtonType="Button" ControlStyle-BackColor="#84A3A3" 
                                                  HeaderStyle-HorizontalAlign="Left" ItemStyle-Height="20px" SelectText="Select" 
                                                  ShowSelectButton="true">
                                              <ControlStyle BackColor="#84A3A3" />
                                              <HeaderStyle HorizontalAlign="Left" />
                                              <ItemStyle Height="20px" />
                                              </asp:CommandField>
                            
                                           <asp:BoundField DataField="account_request_num"  ItemStyle-HorizontalAlign="Left" HeaderText="Account#" ReadOnly="true" />

                                           <asp:BoundField DataField="account_request_seq_num" ItemStyle-HorizontalAlign="Left" HeaderText="Seq#" ReadOnly="true"/>

                                           <asp:BoundField DataField="ApplicationNum" ItemStyle-HorizontalAlign="Left" HeaderText="Application#" ReadOnly="true" />

                                           <asp:BoundField DataField="ApplicationDesc" ItemStyle-HorizontalAlign="Left" HeaderText="App. Desc." ReadOnly="true"/>

                                           <asp:BoundField DataField="login_name"  ItemStyle-HorizontalAlign="Left" HeaderText="login name" />

                                           <asp:BoundField DataField="valid_cd"  ItemStyle-HorizontalAlign="Left"  HeaderText="Valid Cd." ReadOnly="true" />

                                           <asp:BoundField DataField="item_complete_date"  ItemStyle-HorizontalAlign="Left" HeaderText="Complete Ddate" ReadOnly="true"/>

                                            
                                           
                  

                                       </Columns>

                                        </asp:GridView>

                                  </td>

                              </tr>
                        </table>
                            </ContentTemplate>
                  </ajaxToolkit:TabPanel>

           </ajaxToolkit:TabContainer>
        </td>
    </tr>
 </table>
 	<%-- =================== Comments ============================================================================--%>
	<asp:Panel ID="pnlComment" runat="server" Height="250px" Width="275px"  BackColor="#FFFFCC" style="display:none" >
        <table  id="tblComments" runat="server" style="border: medium groove #000000; height:100%"  align="center" width="100%" >
            <tr>
                <td style="width: 3px">
                </td>
                <td style="font-weight: bold; font-size: larger; text-align: center;" align="center">
                    Add Comment</td>
                <td style="width: 3px">
                </td>
            </tr>
            <tr>
                <td style="width: 3px; height: 151px;">
                </td>
                <td align="center" style="height: 150px" >
                    <asp:TextBox ID="txtComment" runat="server" Height="100px" 
                    TextMode="MultiLine" Width="250px" MaxLength="75"></asp:TextBox>

                </td>
                <td style="width: 3px; height: 111px;">
                </td>
            </tr>
            <tr>
                <td style="width: 3px">
                </td>
                <td align="center" >
                    <asp:Button ID="btnUpdComment" runat="server" BackColor="#00C000" Font-Bold="True" Font-Names="Arial"
                        Font-Size="Small" ForeColor="Black" Height="25px" Text="Add" Width="120px" />

                    <asp:Button ID="btnCancelComment" runat="server" BackColor="DimGray" Font-Bold="True"
                        Font-Names="Arial" Font-Size="Small" ForeColor="White" Height="25px" Text="Cancel"
                        Visible="true" Width="120px" ToolTip="Return " />

                </td>
                <td style="width: 3px">
                </td>
            </tr>
        </table>
        <br />
        </asp:Panel> 

        <ajaxToolkit:ModalPopupExtender ID="CommentsPopup" runat="server" 
             TargetControlID="HdBtnComments" PopupControlID="pnlComment" 
            BackgroundCssClass="ModalBackground" CancelControlID="btnCancelComment">
        </ajaxToolkit:ModalPopupExtender>

       <asp:Button ID="HdBtnComments" runat="server" style="display:none" />


</ContentTemplate>
</asp:UpdatePanel>

</asp:Content>

