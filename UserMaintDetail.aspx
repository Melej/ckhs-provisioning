﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="false" CodeFile="UserMaintDetail.aspx.vb" Inherits="UserMaintDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <script type="text/javascript" src="Scripts/FieldValidator.js"></script>

<script type="text/javascript">
    function pageLoad(sender, args) {

        fieldValidator('textInputRequired', 'btnSubmit', 'lblValidation');

    }
</script>
    <style type="text/css">
        .style2
        {
            text-align: center;

        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

    <asp:UpdatePanel ID="uplPanel" runat ="server"  UpdateMode="Conditional">
    <Triggers>

    </Triggers>
   <ContentTemplate>
    <table  style="border-style: groove" align="left" width="100%">
        <tr align="center">
           <td class="tableRowHeader">
                  CSC Technicians Maintenance
           </td>
        </tr>
        <tr>
          <td align="left">
             <ajaxToolkit:TabContainer ID="maintab" runat="server" ActiveTabIndex="0"
                Width="100%"  EnableTheming="False" 
                BackColor="#efefef">

                <ajaxToolkit:TabPanel ID="tpDemographics" runat="server"  BackColor="Gainsboro"  EnableTheming="true">
                     <HeaderTemplate > 
                            <asp:Label ID="Label1" runat="server" Text="Tech Maintenance" Font-Bold="True" 
                                ForeColor="Black"></asp:Label>
                     </HeaderTemplate>
                     <ContentTemplate>
                      <table border="0" style="width: 100%">

                        <tr>
                               <td class="tableRowHeader">
                                    Tech Information
                                </td>
                        </tr>
                        <tr>
                            <td align ="center">
                            <asp:Label ID = "lblValidation" runat = "server" ForeColor="Red" Font-Size="15px" Font-Bold="True" />
                            </td>
            
                        </tr>

                        <tr>
                             <td class="tableRowHeader">
                                        <asp:Label ID="ClientnameHeader" runat="server" ></asp:Label>
                             </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Label ID="TerminateLabel" runat="server" 
                                 Text="This Technicians currently has an Open Terminate Request"  
                                    Visible="False" Font-Bold="True" ForeColor="#FF3300" />
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                              <table>
                                 <tr>
                                    <td align="right">
                                       <asp:Label ID="lblAppgroup" runat="server" Text = "Default Account Grouping:"></asp:Label>
                                     </td>
                                     <td align="left">
                                       <asp:RadioButtonList ID="rblAppGgroup" runat="server" RepeatDirection="Horizontal" AutoPostBack="false" 
                                         ToolTip= "Define Which Account Groups you would like to view" >
                                                       </asp:RadioButtonList>
                                       <asp:TextBox ID="ApplicationGroupTextBox" runat="server" Visible="False"></asp:TextBox>
                                     </td>
                                  </tr>
                               </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="center"> 
                                 <table>
                                   <tr>
                                     <td align="right">
                                            <asp:Label ID="defaultGrplbl" runat="server" Text="Default CSC Tech Group:" Visible="false" ></asp:Label>
                                        </td>
                                        <td align="left">
                            
                                               <asp:DropDownList ID = "DefGroupddl" runat="server"  AutoPostBack="false"  Visible="false"
                                                   ToolTip="Select a CSC Group as the default Group for this Tech when assigning Tickets and RFS">
                                                   </asp:DropDownList>

                                               <asp:Label ID="defaultGrpName" runat="server" Visible="False"></asp:Label>

                                        </td>
                                       </td>
                                    </tr>
                                 </table>

                         </tr>
                         <tr>
                            <td align="center">
                                <table>
                                    <tr>
                                        <td align="right">
                                            <asp:Label ID="lblP1email" runat="server" Text = "Add to Email Distribution Lists"></asp:Label>
                                        </td>
                                        <td align="left">
                                        
                                          <asp:RadioButtonList ID="rblP1email" runat="server" RepeatDirection="Horizontal" >
                                              <asp:ListItem Value="yes" Text="Can Now be Placed on Email Distribution Lists"></asp:ListItem>
                                                <asp:ListItem Value="no" Text="No Longer will be on Email Distribution Lists"></asp:ListItem>
                                            
                                        </asp:RadioButtonList>
                                        </td>
                        
                                    
                                    </tr>
                                </table>
                            </td>
                         </tr>
                        <tr>
                           <td align = "center">

                                            <asp:Label ID="lblUsertype" runat="server" Text="Account Type:" ></asp:Label>
                                            <asp:DropDownList ID="useraccountsddl" runat="server" Height="20px" Width="252px">
                                            </asp:DropDownList>

                                    
<%--                                      <td>
                                            <asp:Label ID="lblGroups" runat="server" Text="CSC Groups Queues:"></asp:Label>

                                            <asp:DropDownList ID = "TechGroupDddl" runat="server" 
                                            ToolTip="Select a Group to Add or Remove a CSC Group for this Tech when assigning Tickets and RFS">
                                             </asp:DropDownList>

                                             
                                      </td>--%>
                                
                            </td>
                        
                        </tr>

                        <tr>
                            <td align="center">

                                <asp:Button ID="btnUpdateTech" runat="server" Text="Update Technician" 
                                CssClass="btnhov" BackColor="#006666"  Width="175px" Visible="false" />

              
                                <asp:Button ID="btnAddNewTech" runat="server" Text="Add New Technicians" 
                                 ToolTip="This all this client access to the application."
                                 Visible="false"
                                CssClass="btnhov" BackColor="#006666"  Width="175px" />


                                <asp:Button ID = "btnDeactivate" runat = "server" Text ="Delete Technician" 
                                ToolTip="This will remove access to this application, this user can be added back anytime."
                                CssClass="btnhov" BackColor="#006666"  Width="175px" />

                                <asp:Button ID = "btnCancel" runat = "server" Text ="Return"
                                       CssClass="btnhov" BackColor="#006666"  Width="175px" />

                           </td>

                   </tr>
                    <tr>
                     <td>
                       <table id="tbldemo" border="1px" width="100%">
                            <tr>
                              <td colspan="4" class="tableRowSubHeader"  align="left">
                                 Demographics
                              </td>
                            </tr>
                            <tr>

                             <td colspan="4" >
                              <table class="style3" > 
                                <tr>
                                    <td>
                                        First Name:
                                    </td>
                                    <td align="left"  >
                                        <asp:TextBox width="280px" ID = "first_nameTextBox" runat="server" 
                                            Enabled="False"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox ID = "miTextBox" runat="server" Width="15px" Enabled="False"></asp:TextBox>
                                    </td>
                                    <td align="left" >
                                        Last Name:
                                    </td>

                                    <td align="left" >
                                        <asp:TextBox ID = "last_nameTextBox" width="280px" runat="server" 
                                            Enabled="False"></asp:TextBox>
                                    </td>

                                </tr>
                             </table>
                           </td>
                         </tr>

                            <tr>
                                <td>
                                    <asp:Label ID="Emplbl" runat="server" Text="Employee Number:" ></asp:Label>
                                </td>

                                <td>
                                   <asp:Label ID ="siemensempnumLabel" runat="server"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="netlbl" runat="server" Text="Network Logon:" ></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID = "ntloginTextBox" runat="server" Enabled="False"></asp:TextBox>
                                    <asp:TextBox ID="txtntloginTextBox" runat="server" Visible="False"></asp:TextBox>
                                </td>
                         </tr>

                         <tr>
                            <td align="right" width="20%" >
                                <asp:Label ID="lblpos" runat="server" Text="Position Description:" />
                            </td>
                            <td colspan = "3" align="left" >
                                <asp:TextBox ID="userpositiondescTextBox" runat="server" Width = "500px" 
                                    Enabled="False"></asp:TextBox>
                            </td>
                        </tr>


                        <tr>
                            <td align="right" >
                            Pager Number:
                            </td>
                            <td align="left" >
                            <asp:TextBox ID = "beeperTextBox" runat="server" Width="90%"></asp:TextBox>
                            </td>

                            <td align="right"  >
                            Phone Number:
                            </td>
                            <td align="left" >
                            <asp:TextBox ID = "phoneTextBox" runat="server"></asp:TextBox>
                            </td>
       
                        </tr>

                            <tr>
                            <td align="right" >
                            Email:
                            </td>
                            <td align="left" >
                            <asp:TextBox ID = "e_mailTextBox" runat="server" Width="250px"></asp:TextBox>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>

                         <tr align="center">
                            <td  colspan="4">
                                        <asp:CheckBoxList ID = "cblGroups" runat = "server" 
                                             Enabled ="False"
                                            RepeatColumns="2" />
            
                            </td>
                         </tr>
                         <tr align="center">    
                            <td colspan="2">
                            
                                    <asp:Table ID = "tblMyGroupAccountQueues1" runat = "server" 
                                        Height="100%" Visible="false">
                
                                    </asp:Table>

                            </td>

                            <td colspan="2">
                            
                                    <asp:Table ID = "tblMyGroupAccountQueues2" runat = "server" 
                                        Height="100%" Visible="false">
                
                                    </asp:Table>

                            </td>

                         </tr>

                           <tr>
                                <td colspan = "4">
                                    
                                    <asp:TextBox ID="HDGroupNum" runat="server" Visible="false"></asp:TextBox>
                                    <asp:Label ID="hdnewgroup" runat="server" Visible="False"></asp:Label>
                                    
                                    <asp:Label ID="HDType" runat="server" Visible="false"></asp:Label>

                                    <asp:Label ID="tech_numLabel" runat="server" Visible="False"></asp:Label>
                                    <asp:Label ID="security_level_numLabel" runat="server" Visible="False"></asp:Label>
                                    <asp:Label ID="new_request_pageLabel" runat="server" Visible="False"></asp:Label>
                                    <asp:Label ID="hdEmail" runat="server" Visible="false">
                                    </asp:Label>
                                    <asp:Label ID="date_deactivatedlabel" runat="server" Visible="False"></asp:Label>
                                    <asp:Label ID ="departmentcdLabel" runat="server" Visible="False"></asp:Label>
                                     <asp:Label ID ="TitleLabel" runat="server" Visible="False"></asp:Label>
                                     <asp:TextBox ID = "buildingcdTextBox" runat="server" Visible ="False"></asp:TextBox>
                                    <asp:Label ID = "emptypecdLabel" runat="server" Visible="False"></asp:Label>
                                      <asp:Label ID = "SpecialtyLabel" runat="server" Visible ="False"></asp:Label>
                                     <asp:Label ID = "facilitycdLabel" runat="server" Visible ="False"></asp:Label>
                                     <asp:Label ID = "entitycdLabel" runat="server" Visible ="False"></asp:Label>
                                     <asp:Label ID= "buildingnameLabel" runat="server" Visible="False" ></asp:Label>
                                       <asp:Label ID="HDemployeetype" runat="server" Visible="False"></asp:Label> 
                                       <asp:Label ID="suffixlabel" runat="server" Visible="False"></asp:Label>
                                       <asp:Label ID= "AccountRequestTypeLabel" runat="server" Visible="False"></asp:Label>
                                      <asp:Label ID="RoleNumLabel" runat="server" Visible="False"></asp:Label>
                                      <asp:Label ID="userDepartmentCD"  runat="server" Visible="False"></asp:Label>
                                      <asp:Label ID="SelectedChgRequestor" runat="server" Visible="False"></asp:Label>
                                      <asp:TextBox ID="FloorTextBox" runat="server" Visible="False"></asp:TextBox>
                                    <asp:Label ID="submitter_client_numLabel" runat="server" visible="False" />
                                    <asp:Label ID="requestor_client_numLabel" runat="server" visible="False"></asp:Label>


                                 </td>
                            </tr>
                      </table>
                     </td>
                    </tr>
                  </table>
                </ContentTemplate>
               </ajaxToolkit:TabPanel>


         </ajaxToolkit:TabContainer>
        </td>
     </tr>
  </table>
  <div>
  
         
           <asp:Panel id="pnlAddEmail" runat="server" Height="275px" Width="550px"  BackColor="#FFFFCC" style="display:none" Font-Size="Medium">
                <table style="border: medium groove #000000; height:100%"  align="center" width="100%">

                    <tr>
                        <td align="center"  colspan="2">
                            Add technician to <asp:Label ID="lblSelectedGroupName" runat="server"></asp:Label>
                        </td>
                            
                    
                    </tr>
                    <tr>
                        <td align="right">
                                        <asp:Label id="lblAddGroup" runat="server">
                                        </asp:Label>
                      </td>
                                    
                                    <td align="left">    
                                         <asp:RadioButtonList ID="ChkAddgroup" runat="server" RepeatDirection="Horizontal" >
                                            
                                            <asp:ListItem Value="yes" Text="Yes"></asp:ListItem>
                                            <asp:ListItem Value="no" Text="No"></asp:ListItem>
                                            
                                        </asp:RadioButtonList>
                                    
                                    </td>

                    </tr>
                    <tr>
                        <td align="right">
                                       <asp:Label id="lblemail" runat="server" ></asp:Label>
                       </td>

                      <td align="center">
                        
                                        <asp:RadioButtonList ID="rblEmailMe" runat="server" RepeatDirection="Horizontal" >
                            
                                            <asp:ListItem Value="yes" Text="Receive Email"></asp:ListItem>
                                            <asp:ListItem Value="no" Text="No Emails"></asp:ListItem>
                                            
                                        </asp:RadioButtonList>
                                
                          </td>
                       </tr>
                       
                       <tr>
                        <td align="center" >
                                 <asp:Button ID = "btnUpdate" runat = "server" Text ="Update Technician"
                                CssClass="btnhov" BackColor="#006666"  Width="175px" />


                        </td>
                        <td>
                                <asp:Button ID="btnCancelEmail" runat="server" Height="34px" Text="Cancel"
                                CssClass="btnhov" BackColor="#006666"  Width="175px"/>
                
                        </td>
                    </tr>
                    
			</table>
           </asp:Panel> 

           <ajaxToolkit:ModalPopupExtender ID="AddEmailPopup" runat="server" 
                    TargetControlID="HDBtnAddEmail" PopupControlID="pnlAddEmail" 
                    BackgroundCssClass="ModalBackground" CancelControlID="btnCancelEmail">
          </ajaxToolkit:ModalPopupExtender>

           <asp:Button ID="HDBtnAddEmail" runat="server" style="display:none" />
  
  </div>
   </ContentTemplate>
  </asp:UpdatePanel>
</asp:Content>

