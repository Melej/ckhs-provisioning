﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="false" CodeFile="TicketDashboard.aspx.vb" Inherits="TicketDashboard"  ViewStateMode="Enabled"%>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <style type="text/css">
        .style2
        {
            text-align: center;
            background-color: #FFFFCC;
            color: #336666;
            font-weight: bold;
            font-size: x-large;
            height: 43px;
        }
    </style>

    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

  <div id="maindiv" runat="server" align="center">
    <table width="100%">
      <tr>
        <td valign = "top" align="center" colspan="4">
           View Tickets Between 
           <asp:TextBox runat="server" ID = "txtBeginDate" CssClass="calenderClass"></asp:TextBox> 
           and
            <asp:TextBox runat="server" ID = "txtEndDate" CssClass="calenderClass"></asp:TextBox>
        </td>
      </tr>
              
        <tr align = "center">


            <td colspan="4" align="center">
                 Status:
                <asp:RadioButtonList ID = "Statusrbl" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="both" Selected="True">Open & Closed Tickets and RFS</asp:ListItem>
                        
                    <asp:ListItem Value="open">Open Tickets and RFS</asp:ListItem>

                    <asp:ListItem Value="closed">Closed Tickets and RFS</asp:ListItem>
                </asp:RadioButtonList>

            </td>
        </tr>
        <tr align="center">

           <td  align="center" colspan="4">
           Category:
                <asp:DropDownList ID="categorycdddl" runat="server">
                </asp:DropDownList>
            </td>


       </tr>
       <tr>
          <td  align="center" colspan="4">
            <table>
                <tr align="center">
                    <td align="center">

                            <asp:Button ID ="btnSubmit" runat = "server" Text ="Submit"  Width="120px" Height="30px"
                                                                CssClass="btnhov" BackColor="#006666" />
        
                    </td>
                    <td>
                                
                    </td>
                    <td align="center" >

                            <asp:Button ID = "btnReset" runat= "server" Text="Reset"  Width="120px" Height="30px"
                                    CssClass="btnhov" BackColor="#006666"  />

                    </td>

                    <td>
                        <asp:Button ID="btnTicketDetail" runat="server" Text = "Ticket Detail RPT." Width="120px" Height="30px"
                            CssClass="btnhov" BackColor="#006666" />

                    
                    </td>
<%--                    <td>
                        <asp:Button ID="btnExcel" runat="server" Text="Excel" Width="120px" Height="30px" 
                                    CssClass="btnhov" BackColor="#006666" />

                   </td>--%>

                </tr>
            </table>
          </td>
         </tr> 
         <tr>
            <td colspan="4" align="center">
            
                    <asp:GridView ID="GvTotals" runat="server" AllowSorting="false" 
                           AutoGenerateColumns="False" BackColor="White" BorderColor="#336666" 
                           BorderStyle="Double" BorderWidth="3px" CellPadding="1" 
                              EmptyDataText="No Totals" 
                           EnableTheming="False" Font-Size="Medium" GridLines="Horizontal" Width="70%">
                           <FooterStyle BackColor="White" ForeColor="#333333" />
                           <HeaderStyle BackColor="#336666" Font-Bold="False" Font-Size="Medium" 
                               ForeColor="White" HorizontalAlign="Left" VerticalAlign="Middle" />
                           <PagerStyle BackColor="#336666" Font-Size="Medium" ForeColor="White" 
                               HorizontalAlign="Left" />
                           <RowStyle BackColor="White" ForeColor="#333333" Font-Size="Medium" />
                           <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                           <SortedAscendingCellStyle BackColor="#F7F7F7" />
                           <SortedAscendingHeaderStyle BackColor="#487575" />
                           <SortedDescendingCellStyle BackColor="#E5E5E5" />
                           <SortedDescendingHeaderStyle BackColor="#275353" />
                           <Columns>
                                <asp:BoundField  DataField="Category"  HeaderText="Category" SortExpression="category_cd" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
				                <asp:BoundField  DataField="CurrentStatus"  HeaderText="Status" SortExpression="Status" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>

				                <asp:BoundField  DataField="TotalCount"  HeaderText="Total " SortExpression="Total" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>

                           </Columns>
                       </asp:GridView>                            


            </td>
          
          </tr>
         <tr>
         
            <td colspan="4">
 
                <asp:Table  Width="90%" ID = "tblAccounts" runat = "server" CellPadding="0" CellSpacing="3" Font-Size="Medium">
                </asp:Table>
            </td>
         
         </tr>
        <tr>
            <td colspan="4">
            
                <asp:TextBox ID="HDStartDate" runat="server" Visible="false" ></asp:TextBox>
                <asp:TextBox ID="HDEnddate" runat="server" Visible="false" ></asp:TextBox>
                <asp:TextBox ID="HDCategory" runat="server" Visible="false" ></asp:TextBox>
                <asp:TextBox ID="HDStatus" runat="server" Visible="false" ></asp:TextBox>

                 
            </td>
        </tr>
          
         <tr>
                <td colspan="4">
                            <asp:GridView ID="GridAccounts" runat="server" AllowSorting="True" 
                           AutoGenerateColumns="False" BackColor="White" BorderColor="#336666" 
                           BorderStyle="Double" BorderWidth="3px" CellPadding="1" 
                           DataKeyNames="client_num"  Visible="false"
                           EmptyDataText="No Accounts found " 
                           EnableTheming="False" Font-Size="Medium" GridLines="Horizontal" Width="100%">
                           <FooterStyle BackColor="White" ForeColor="#333333" />
                           <HeaderStyle BackColor="#336666" Font-Bold="False" Font-Size="X-Small" 
                               ForeColor="White" HorizontalAlign="Left" VerticalAlign="Middle" />
                           <PagerStyle BackColor="#336666" Font-Size="Medium" ForeColor="White" 
                               HorizontalAlign="Left" />
                           <RowStyle BackColor="White" ForeColor="#333333" Font-Size="X-Small" />
                           <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                           <SortedAscendingCellStyle BackColor="#F7F7F7" />
                           <SortedAscendingHeaderStyle BackColor="#487575" />
                           <SortedDescendingCellStyle BackColor="#E5E5E5" />
                           <SortedDescendingHeaderStyle BackColor="#275353" />
                           <Columns>

                               <asp:CommandField ButtonType="Button" ControlStyle-BackColor="#84A3A3" 
                                      HeaderStyle-HorizontalAlign="Left" ItemStyle-Height="20px" SelectText="Select" 
                                      ShowSelectButton="true">
                                  <ControlStyle BackColor="#84A3A3" />
                                  <HeaderStyle HorizontalAlign="Left" />
                                  <ItemStyle Height="20px" />
                                  </asp:CommandField>
                            
                            	  


                               <asp:BoundField DataField="fullname"  ItemStyle-HorizontalAlign="Left" HeaderText="User Name" />

                               <asp:BoundField DataField="LoginName" ItemStyle-HorizontalAlign="Center" HeaderText="Network Login" />

                               <asp:BoundField DataField="AppLoginName" ItemStyle-HorizontalAlign="Center" HeaderText="App Login " />

                               <asp:BoundField DataField="StartDate" ItemStyle-HorizontalAlign="Center" HeaderText="Start Date" />

                               <asp:BoundField DataField="siemens_emp_num"  ItemStyle-HorizontalAlign="Center" HeaderText="Emp # " />
                               <asp:BoundField DataField="emp_type_cd"  ItemStyle-HorizontalAlign="Center" HeaderText="Emp Type " />

                           </Columns>
                       </asp:GridView>                            
                </td>
          </tr>

    </table>
  
  </div>

 </asp:Content>