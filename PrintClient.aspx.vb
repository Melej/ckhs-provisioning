﻿Imports System.IO
Imports System.Data '
Imports System.Data.SqlClient
Imports App_Code
Partial Class PrintClient
    Inherits System.Web.UI.Page
    Dim sWebReferrer As String = ""
    Dim AccountRequestNum As Integer
    Dim sActivitytype As String = ""
    Dim FormSignOn As New FormSQL()
    Dim VendorFormSignOn As FormSQL
    Dim UpdPhyFormSignOn As FormSQL
    Dim NewRequestForm As New FormSQL()
    Dim iClientNum As String

    Dim employeetype As String
    Dim blnReturn As Boolean = False
    Dim bHasError As Boolean
    'Dim ValidationStatus As String = ""
    Dim sItemComplet As String = ""
    Dim RequestoName As String
    Dim iRequestorNum As String
    Dim sIsPhysician As String = ""
    Dim sLabelMsg As String = ""

    Private dtSubApplications As New DataTable

    Dim thisRole As String
    Dim mainRole As String

    Dim AccountCount As Integer = 0
    Dim OpenRequestCount As Integer = 0
    Dim totAcct As Integer = 0


    Dim AccountRequest As AccountRequest


    Private dtAccounts As New DataTable
    Dim UserInfo As UserSecurity
    Private AddAccountRequestNum As Integer = 0
    Private DelAccountRequestNum As Integer = 0
    Private RemoveAccts As DataTable
    Private dtPendingRequests As DataTable
    Private dtRemoveAccounts As New DataTable
    Private dtAddAccounts As New DataTable

    Private dtAddFinAccounts As New DataTable

    Private dtDocmster As New DataTable
    Private dtCPMGroups As New DataTable

    Private dtTCLUserTypes As New DataTable


    Dim thisdata As CommandsSqlAndOleDb


    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        iClientNum = Request.QueryString("ClientNum")

        Dim dbAccess As New CkhsEmployeeDbAccess(Session("EmployeeConn"))
        Dim ckhsEmployee = dbAccess.GetCkhsEmployee(iClientNum)
        'put ckhsEmployee in the HttpSession


        'c3Controller = New Client3Controller(iClientNum)
        If ckhsEmployee.LastName = "" Then
            ' must be deactivated no last name 
            Response.Redirect("~/DeactivatedAccount.aspx?ClientNum=" & iClientNum, True)

        End If

        UserInfo = New UserSecurity(Session("LoginID"), Session("EmployeeConn"))

        securitylevel.Text = UserInfo.SecurityLevelNum.ToString


        FormSignOn = New FormSQL(Session("EmployeeConn"), "SelClientByNumberV20", "UpdClientObjectByNumberV20", "", Page)
        FormSignOn.AddSelectParm("@client_num", iClientNum, SqlDbType.Int, 6)
        FormSignOn.AddInsertParm("@submitterClientNum", Session("ClientNum"), SqlDbType.Int, 6)

        NewRequestForm = New FormSQL(Session("EmployeeConn"), "SelAccountRequestV20", "InsNewAccountRequestForClientV20", "", Page)

        If Not IsPostBack Then


            'userSubmittingLabel.Text = UserInfo.UserName
            'SubmittingOnBehalfOfNameLabel.Text = UserInfo.UserName
            submitter_client_numLabel.Text = UserInfo.ClientNum
            requestor_client_numLabel.Text = UserInfo.ClientNum
            userDepartmentCD.Text = UserInfo.DepartmentCd

            If iRequestorNum <> "" Then
                requestor_client_numLabel.Text = iRequestorNum
                'RequestorName
                'SubmittingOnBehalfOfNameLabel.Text = RequestoName
            End If


            Dim rblAffiliateBinder As New RadioButtonListBinder(emptypecdrbl, "SelAffiliationsV5", Session("EmployeeConn"))
            'If Session("SecurityLevelNum") < 90 Then
            Select Case ckhsEmployee.RoleNum
                Case "782", "1197", "1194"
                    rblAffiliateBinder.AddDDLCriteria("@type", "phys", SqlDbType.NVarChar)
                Case "1200", "1193"
                    If ckhsEmployee.SiemensEmpNum <> "" Then
                        rblAffiliateBinder.AddDDLCriteria("@type", "ckhsemp", SqlDbType.NVarChar)
                    Else

                        rblAffiliateBinder.AddDDLCriteria("@type", "nonEmp", SqlDbType.NVarChar)
                    End If
                Case "1189"
                    If ckhsEmployee.SiemensEmpNum <> "" Then
                        rblAffiliateBinder.AddDDLCriteria("@type", "ckhsemp", SqlDbType.NVarChar)
                    Else
                        rblAffiliateBinder.AddDDLCriteria("@type", "student", SqlDbType.NVarChar)

                    End If

                Case "1195", "1198", "1196", "1199"

                    If ckhsEmployee.SiemensEmpNum <> "" Then
                        rblAffiliateBinder.AddDDLCriteria("@type", "", SqlDbType.NVarChar)
                    Else
                        rblAffiliateBinder.AddDDLCriteria("@type", "contract", SqlDbType.NVarChar)

                    End If


                Case Else
                    ' if a Doctor master number exist
                    If ckhsEmployee.DoctorMasterNum > 0 Then
                        EmpDoclbl.Visible = True
                        EmpDoctorTextBox.Text = ckhsEmployee.DoctorMasterNum.ToString
                        EmpDoctorTextBox.Visible = True
                        If Session("SecurityLevelNum") < 90 Then
                            EmpDoctorTextBox.Enabled = False
                        End If

                    Else
                        rblAffiliateBinder.AddDDLCriteria("@type", "", SqlDbType.NVarChar)
                    End If

            End Select
            'End If

            rblAffiliateBinder.ToolTip = False

            rblAffiliateBinder.BindData("RoleNum", "AffiliationDisplay") '"AffiliationDescLower",

            If RoleNumLabel.Text = "" Then

                masterRole.Text = ckhsEmployee.RoleNum
                mainRole = ckhsEmployee.RoleNum
                RoleNumLabel.Text = ckhsEmployee.RoleNum
                emptypecdrbl.SelectedValue = ckhsEmployee.RoleNum
                PositionNumTextBox.Text = ckhsEmployee.PositionNum
                PositionRoleNumTextBox.Text = ckhsEmployee.PositionRoleNum
            End If


            'emptypecdrbl.SelectedValue = ckhsEmployee.EmpTypeCd
            emptypecdLabel.Text = ckhsEmployee.EmpTypeCd
            HDemployeetype.Text = ckhsEmployee.EmpTypeCd

            ClientnameHeader.Text = ckhsEmployee.FullName
            ' Does not workk cant pass controler to class
            'dbAccess.CheckEmployeeType(HDemployeetype.Text)

            Dim provider As String
            provider = ckhsEmployee.Provider

            FormSignOn.FillForm()
            CheckAffiliation()


            Dim ddlBinder As New DropDownListBinder

            Dim ddlBinderV2 As DropDownListBinder
            ddlBinderV2 = New DropDownListBinder(Rolesddl, "SelEmployeeRolesV3", Session("EmployeeConn"))

            ddlBinderV2.AddDDLCriteria("@RoleNum", emptypecdrbl.SelectedValue, SqlDbType.NVarChar)

            If PositionNumTextBox.Text <> "" Then
                ddlBinderV2.AddDDLCriteria("@positionNum", PositionNumTextBox.Text, SqlDbType.NVarChar)

            End If
            ddlBinderV2.BindData("rolenum", "roledesc")

            ' ddlBinder.BindData(Rolesddl, "SelEmployeeRolesV2", "rolenum", "RoleDesc", Session("EmployeeConn"))

            ddlBinder.BindData(facilitycdddl, "SelFacilityCodes", "facility_cd", "facility_short_name", Session("EmployeeConn"))
            ddlBinder.BindData(Specialtyddl, "SelSpecialtyCodesV2", "specialty", "specialty", Session("EmployeeConn"))
            ddlBinder.BindData(entitycdDDL, "selEntityCodesV4", "entity_cd", "hosp_desc", Session("EmployeeConn"))
            ' ddlBinder.BindData(AlliedHealthddl, "SelAlliedHealthPos", "AlliedHealthPosShort", "AlliedHealthPosShort", Session("EmployeeConn"))
            ddlBinder.BindData(suffixddl, "SelSuffixCodes", "Suffix", "Suffix", Session("EmployeeConn"))

            Dim ddlInvisionBinder As New DropDownListBinder(group_nameddl, "SelPhysGroupsV5", Session("EmployeeConn"))
            ddlInvisionBinder.AddDDLCriteria("@GroupTypeCd", "invision", SqlDbType.NVarChar)
            ddlInvisionBinder.AddDDLCriteria("@clientpage", "yes", SqlDbType.NVarChar)

            ddlInvisionBinder.BindData("Group_num", "group_name")

            'Dim ddlNonPerferBinder As New DropDownListBinder(Nonperferdddl, "SelPhysGroupsV5", Session("EmployeeConn"))
            'ddlNonPerferBinder.AddDDLCriteria("@GroupTypeCd", "NonPerGrp", SqlDbType.NVarChar)
            'ddlNonPerferBinder.AddDDLCriteria("@clientpage", "yes", SqlDbType.NVarChar)

            'ddlNonPerferBinder.BindData("Group_num", "group_name")

            Dim ddGnumberBinder As New DropDownListBinder(LocationOfCareIDddl, "SelPhysGroupsV5", Session("EmployeeConn"))
            ddGnumberBinder.AddDDLCriteria("@GroupTypeCd", "GNum", SqlDbType.NVarChar)
            ddGnumberBinder.AddDDLCriteria("@clientpage", "yes", SqlDbType.NVarChar)

            ddGnumberBinder.BindData("Group_num", "group_name")

            'Dim ddCommLocationOfCare As New DropDownListBinder(CommLocationOfCareIDddl, "SelPhysGroupsV5", Session("EmployeeConn"))
            'ddCommLocationOfCare.AddDDLCriteria("@GroupTypeCd", "CommNum", SqlDbType.NVarChar)
            'ddCommLocationOfCare.AddDDLCriteria("@clientpage", "yes", SqlDbType.NVarChar)

            'ddCommLocationOfCare.BindData("Group_num", "group_name")

            'Dim ddCoverageGroupddl As New DropDownListBinder(CoverageGroupddl, "SelPhysGroupsV5", Session("EmployeeConn"))
            'ddCoverageGroupddl.AddDDLCriteria("@GroupTypeCd", "coverage", SqlDbType.NVarChar)
            'ddCoverageGroupddl.AddDDLCriteria("@clientpage", "yes", SqlDbType.NVarChar)

            'ddCoverageGroupddl.BindData("Group_num", "Group_name")


            Dim ddEMRBinder As New DropDownListBinder(PhysOfficeddl, "SelAllPhysGroups", Session("EmployeeConn"))
            'ddEMRBinder.AddDDLCriteria("@GroupTypeCd", "GNum", SqlDbType.NVarChar)
            'SelPhysGroupsV5
            ddEMRBinder.BindData("Group_num", "group_name")

            If PositionRoleNumLabel.Text <> "" Then
                Rolesddl.SelectedValue = PositionRoleNumLabel.Text
            ElseIf RoleNumLabel.Text <> "" Then
                Rolesddl.SelectedValue = RoleNumLabel.Text
                PositionRoleNumLabel.Text = RoleNumLabel.Text
                PositionRoleNumTextBox.Text = RoleNumLabel.Text
            Else
                Rolesddl.SelectedIndex = -1
            End If



            If entitycdLabel.Text = "" Then
                entitycdDDL.SelectedIndex = -1
            Else
                Dim entitycd As String = entitycdLabel.Text

                entitycd = entitycd.Trim
                entitycdDDL.SelectedValue = entitycd

            End If

            If facilitycdLabel.Text = "" Then
                facilitycdddl.SelectedIndex = -1
            Else
                facilitycdddl.SelectedValue = facilitycdLabel.Text
            End If

            ' Gnumber
            If LocationOfCareIDLabel.Text = "" Then
                LocationOfCareIDddl.SelectedIndex = -1
                LocationOfCareIDddl.Visible = True

            Else
                LocationOfCareIDddl.SelectedValue = GNumberGroupNumLabel.Text
                origionalGnumber.Text = GNumberGroupNumLabel.Text

                CPMViewGrouplbl.Text = LocationOfCareIDddl.SelectedItem.ToString
                CPMViewGrouplbl.Visible = True
                'ViewCPMPan.Visible = True
                LocationOfCareIDddl.Visible = False

                'LocationOfCareIDLabel.Text = LocationOfCareIDddl.SelectedItem.ToString

            End If

            'Comm Gnumber
            'If CommLocationOfCareIDLabel.Text = "" Then
            '    CommLocationOfCareIDddl.SelectedIndex = -1
            'Else
            '    ' CommLocationOfCareIDLabel.Text = CommLocationOfCareIDddl.SelectedItem.ToString
            '    CommLocationOfCareIDddl.SelectedValue = CommNumberGroupNumLabel.Text
            '    origionalCommNumber.Text = CommNumberGroupNumLabel.Text
            'End If

            ''Coverage number
            'If CoverageGroupNumLabel.Text = "" Then
            '    CoverageGroupddl.SelectedIndex = -1
            'Else
            '    CoverageGroupddl.SelectedValue = CoverageGroupNumLabel.Text

            'End If

            ' Invision Number
            If InvisionGroupNumTextBox.Text = "" Then
                group_nameddl.SelectedIndex = -1
            Else
                group_nameddl.SelectedValue = InvisionGroupNumTextBox.Text
                origionalgroupNum.Text = InvisionGroupNumTextBox.Text
            End If

            ''non perfered
            'If NonPerferedGroupnumLabel.Text = "" Then
            '    Nonperferdddl.SelectedIndex = -1
            'Else
            '    Nonperferdddl.SelectedValue = NonPerferedGroupnumLabel.Text
            '    origionalNonperGroup.Text = NonPerferedGroupnumLabel.Text
            'End If

            Dim ddbDepartmentBinder As New DropDownListBinder(departmentcdddl, "selDepartmentCodesV4", Session("EmployeeConn"))
            ddbDepartmentBinder.AddDDLCriteria("@entity_cd", entitycdDDL.SelectedValue, SqlDbType.NVarChar)
            ddbDepartmentBinder.BindData("department_cd", "department_name")

            If departmentcdLabel.Text = "" Then
                departmentcdddl.SelectedIndex = -1
            Else
                departmentcdddl.SelectedValue = departmentcdLabel.Text

            End If


            If SpecialtyLabel.Text = "" Then
                Specialtyddl.SelectedIndex = -1
            Else
                Specialtyddl.SelectedValue = SpecialtyLabel.Text

            End If

            If groupnameTextBox.Text = "" Then
                group_nameddl.SelectedIndex = -1
            Else
                group_nameddl.SelectedValue = groupnumTextBox.Text
            End If

            If suffixlabel.Text = "" Then
                suffixddl.SelectedIndex = -1
            Else
                suffixddl.SelectedValue = suffixlabel.Text
            End If

            ' practice_numddl.SelectedValue = practice_numLabel.Text


            ddlBinder = New DropDownListBinder(buildingcdddl, "sel_building_by_facility", Session("EmployeeConn"))
            ddlBinder.AddDDLCriteria("@facility_cd", facilitycdddl.SelectedValue, SqlDbType.NVarChar)
            ddlBinder.BindData("building_cd", "building_name")


            If buildingcdTextBox.Text = "" Then
                buildingcdddl.SelectedValue = -1
            Else
                Dim sBuilding As String = buildingcdTextBox.Text
                buildingcdddl.SelectedValue = sBuilding.Trim
                If buildingnameLabel.Text <> "" Then

                    buildingcdddl.SelectedItem.Text = buildingnameLabel.Text
                End If

            End If

            If buildingcdddl.SelectedIndex <> -1 Then

                ddlBinder = New DropDownListBinder(Floorddl, "SelFloorsByBuilding", Session("EmployeeConn"))
                ddlBinder.AddDDLCriteria("@facility_cd", facilitycdddl.SelectedValue, SqlDbType.NVarChar)
                ddlBinder.AddDDLCriteria("@building_cd", buildingcdddl.SelectedValue, SqlDbType.NVarChar)

                ddlBinder.BindData("floor_no", "floor_no")

                If FloorTextBox.Text = "" Then
                    Floorddl.SelectedValue = -1
                Else
                    Dim sFloor As String = FloorTextBox.Text
                    Floorddl.SelectedValue = sFloor.Trim

                End If



            End If


            Select Case emptypecdrbl.SelectedValue
                Case "782", "1197"

                    siemensempnumTextBox.Visible = True
                    Emplbl.Visible = True

                    'Case "physician", "non staff clinician", "resident"

                    If Titleddl.SelectedIndex <= 0 Then
                        Dim ddbTitleBinder As New DropDownListBinder(Titleddl, "SelTitlesV4", Session("EmployeeConn"))
                        ddbTitleBinder.AddDDLCriteria("@titleType", "Physician", SqlDbType.NVarChar)
                        ddbTitleBinder.BindData("TitleShortDesc", "TitleDesc")

                        'FillAffiliations()

                        If TitleLabel.Text = "" Then
                            Titleddl.SelectedIndex = -1
                        Else
                            Titleddl.SelectedValue = TitleLabel.Text

                        End If

                    End If



                    If SpecialtyLabel.Text = "" Then
                        Specialtyddl.SelectedIndex = -1
                    Else
                        Specialtyddl.SelectedValue = SpecialtyLabel.Text

                    End If
                    ' use to secure here
                    If DoctorMasterNumTextBox.Text <> "" Then
                        docMstrPnl.Visible = True
                        'grDocmster
                    End If


                    If hanrbl.SelectedValue.ToString = "Yes" Then

                        ' change role 1567

                    Else
                        LocationOfCareIDddl.Visible = False
                        ckhnlbl.Visible = False

                    End If



                Case "1194" 'Residents
                    'Dim ddlBinder As New DropDownListBinder
                    siemensempnumTextBox.Visible = True
                    Emplbl.Visible = True


                    Dim ddbTitleBinder As New DropDownListBinder(Titleddl, "SelTitlesV4", Session("EmployeeConn"))
                    ddbTitleBinder.AddDDLCriteria("@titleType", "Resident", SqlDbType.NVarChar)
                    ddbTitleBinder.BindData("TitleShortDesc", "TitleDesc")

                    Dim ddlRoleBinder As New DropDownListBinder(Rolesddl, "SelEmployeeRolesV3", Session("EmployeeConn"))
                    ddlRoleBinder.AddDDLCriteria("@RoleNum", "1194", SqlDbType.NVarChar)
                    ddlRoleBinder.BindData("rolenum", "RoleDesc")

                    If TitleLabel.Text = "" Then
                        Titleddl.SelectedIndex = -1
                    Else
                        Titleddl.SelectedValue = TitleLabel.Text

                    End If

                    If SpecialtyLabel.Text = "" Then
                        Specialtyddl.SelectedIndex = -1
                    Else
                        Specialtyddl.SelectedValue = SpecialtyLabel.Text

                    End If

                    If TitleLabel.Text = "" Then
                        Title2Label.Text = "Resident"

                    Else
                        Title2Label.Text = TitleLabel.Text

                    End If
                    Title2Label.Visible = True
                    Titleddl.Visible = False

                    SpecialtyLabel.Text = "Resident"
                    SpecialtyLabel.Visible = True
                    Specialtyddl.Visible = False

                    credentialedDateTextBox.Enabled = False
                    credentialedDateTextBox.CssClass = "style2"

                    'CredentialedDateDCMHTextBox.Enabled = False
                    'CredentialedDateDCMHTextBox.CssClass = "style2"

                    CCMCadmitRightsrbl.SelectedValue = "No"
                    'CCMCadmitRightsrbl.Enabled = False

                    'DCMHadmitRightsrbl.SelectedValue = "No"



                    If hanrbl.SelectedValue.ToString = "Yes" Then
                        ' remove all CPM validation 2018

                    Else
                        LocationOfCareIDddl.Visible = False
                        ckhnlbl.Visible = False

                        'lblprov.Visible = False
                        'providerrbl.Visible = False

                    End If

                    pnlProvider.Visible = True



                Case "1195", "1198"
                    ' load vendor list 
                    'make vendor text box invisiable
                    ' make ddl appear
                    ' 1195 Contractor list
                    ' 1198 Vendor list
                    Vendorddl.Visible = True
                    VendorNameTextBox.Visible = False

                    siemensempnumTextBox.Visible = False
                    Emplbl.Visible = False

                    Dim ddbVendorBinder As New DropDownListBinder(Vendorddl, "SelVendors", Session("EmployeeConn"))
                    ddbVendorBinder.AddDDLCriteria("@type", "vendor", SqlDbType.NVarChar)
                    ddbVendorBinder.BindData("VendorNum", "VendorName")


                    If VendorNameTextBox.Text = "" Then
                        Vendorddl.SelectedIndex = -1
                    Else
                        Vendorddl.SelectedValue = VendorNumLabel.Text

                    End If

                    If VendorNumLabel.Text = "0" Then
                        otherVendorlbl.Visible = True
                        VendorUnknowlbl.Text = VendorNameTextBox.Text
                        VendorUnknowlbl.Visible = True
                    End If

                Case "1199", "1196"
                    siemensempnumTextBox.Visible = False
                    Emplbl.Visible = False

                Case "1193"
                    siemensempnumTextBox.Visible = True
                    Emplbl.Visible = True

                    If DoctorMasterNumTextBox.Text <> "" Then
                        DoctorMasterNumLabel.Visible = True
                        behaviorallbl.Visible = True

                    End If

                    PhysOfficepanel.Visible = True


                    If groupnumTextBox.Text = "" Then
                        PhysOfficeddl.SelectedIndex = -1
                    Else
                        PhysOfficeddl.SelectedValue = groupnumTextBox.Text
                    End If


                    Select Case PositionRoleNumLabel.Text
                        Case "1567"
                            ' Lock position drop down
                            'Rolesddl.Enabled = False

                            PhysOfficeddl.Dispose()


                            Dim ddOffBinder As New DropDownListBinder(PhysOfficeddl, "SelPhysGroupsV5", Session("EmployeeConn"))
                            ddOffBinder.AddDDLCriteria("@GroupTypeCd", "GNum", SqlDbType.NVarChar)
                            ddOffBinder.AddDDLCriteria("@clientpage", "yes", SqlDbType.NVarChar)

                            'SelPhysGroupsV5
                            ddOffBinder.BindData("Group_num", "group_name")




                            If groupnumTextBox.Text = "" Then
                                PhysOfficeddl.SelectedIndex = -1
                            Else
                                PhysOfficeddl.SelectedValue = groupnumTextBox.Text
                            End If




                        Case Else
                            If groupnumTextBox.Text = "" Then
                                PhysOfficeddl.SelectedIndex = -1
                            Else
                                PhysOfficeddl.SelectedValue = groupnumTextBox.Text
                            End If


                    End Select


                Case Else


            End Select


            ' All bets are off if client has open Terminate request
            If AccountRequestTypeLabel.Text = "terminate" Then
                TerminateLabel.Visible = True

                'btnSelectOnBehalfOf.Visible = False
                btnUpdate.Visible = False
                btnSubmitDemoChange.Visible = False
                'tpAddItems.Visible = False
                'tpRemoveItems.Visible = False
                'tpAddFinanItems.Visible = False


            End If


            firstnameTextBox.Focus()




        End If
        ' end postback

        If securitylevel.Text <> "" Then
            Session("SecurityLevelNum") = CInt(securitylevel.Text)

        End If

        'If Session("SecurityLevelNum") < 64 Then
        Dim ctrl As Control = DataCheckingAndValidation.SetChildControlsRecursiveToEnabledFalse(form1, False)


        'End If

        'Select Case emptypecdrbl.SelectedValue
        '    Case "782", "1197", "1194"

        '        If Session("SecurityLevelNum") < 79 Then
        '            Dim ctrl As Control = DataCheckingAndValidation.SetChildControlsRecursiveToEnabledFalse(tpDemographics, False)
        '            btnSubmitDemoChange.Visible = True
        '            btnSubmitDemoChange.Enabled = True

        '        ElseIf Session("SecurityLevelNum") < 89 Then
        '            Dim ctrl As Control = DataCheckingAndValidation.SetChildControlsRecursiveToEnabledFalse(tpDemographics, False)

        '        Else
        '            btnUpdate.Visible = True
        '            btnSubmitDemoChange.Visible = True
        '            DoctorMasterNumTextBox.Enabled = True
        '        End If

        '        ' turn this off for everyone
        '        emptypecdrbl.Enabled = False
        'End Select


    End Sub

    Private Sub CheckAffiliation()


        HDemployeetype.Text = emptypecdrbl.SelectedValue


        Select Case emptypecdrbl.SelectedValue
            Case "1193", "1200"
                'Case "employee"

                Dim firstNameReq As New RequiredField(firstnameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(lastnameTextBox, "Last Name")



                'pnlAlliedHealth.Visible = False

                pnlProvider.Visible = False


            Case "1189"
                'Case "student"

                Dim firstNameReq As New RequiredField(firstnameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(lastnameTextBox, "Last Name")
                Dim titleReq As New RequiredField(Titleddl, "Title")
                Dim specReq As New RequiredField(Specialtyddl, "Specialty")
                Dim add1Req As New RequiredField(address1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim faxqReq As New RequiredField(faxtextbox, "Fax")


                'pnlAlliedHealth.Visible = False

                pnlProvider.Visible = False
                'providerrbl.SelectedIndex = -1

            Case "782"
                'Case "physician"
                Dim firstNameReq As New RequiredField(firstnameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(lastnameTextBox, "Last Name")
                Dim titleReq As New RequiredField(Titleddl, "Title")
                Dim specReq As New RequiredField(Specialtyddl, "Specialty")
                Dim add1Req As New RequiredField(address1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim faxqReq As New RequiredField(faxtextbox, "Fax")


                'pnlAlliedHealth.Visible = False

                pnlProvider.Visible = True


            Case "1197"

                'Case "non staff clinician"
                Dim firstNameReq As New RequiredField(firstnameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(lastnameTextBox, "Last Name")
                Dim titleReq As New RequiredField(Titleddl, "Title")

                Dim specReq As New RequiredField(Specialtyddl, "Specialty")
                Dim add1Req As New RequiredField(address1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim faxqReq As New RequiredField(faxtextbox, "Fax")



                'providerrbl.SelectedIndex = -1
                pnlProvider.Visible = True
            Case "1194"
                'Case "resident"
                Dim firstNameReq As New RequiredField(firstnameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(lastnameTextBox, "Last Name")
                Dim titleReq As New RequiredField(Titleddl, "Title")

                Dim specReq As New RequiredField(Specialtyddl, "Specialty")
                Dim add1Req As New RequiredField(address1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim faxqReq As New RequiredField(faxtextbox, "Fax")



                'providerrbl.SelectedValue = "Yes"
                pnlProvider.Visible = True
                'If hanrbl.SelectedValue.ToString = "Yes" Then

                '    Dim ViewCPMModelreq As New RequiredField(CPMEmployeesddl, "CPM Model")

                '    Dim npiReq As New RequiredField(npitextbox, "NPI")
                '    Dim LicensesNoReq As New RequiredField(LicensesNoTextBox, "Licenses No")
                '    Dim taxonomyreq As New RequiredField(taxonomytextbox, "Taxonomy")
                '    Dim startdatereq As New RequiredField(startdateTextBox, "Start Date")
                '    Dim Schedulablereq As New RequiredField(Schedulablerbl, "Schedule")
                '    Dim Televoxrblreq As New RequiredField(Televoxrbl, "Televox")
                'End If


            Case "1198"
                ' Case "vendor"

                Dim firstNameReq As New RequiredField(firstnameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(lastnameTextBox, "Last Name")
                Dim add1Req As New RequiredField(address1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")
                Dim vendorRequired As New RequiredField(VendorNameTextBox, "Vendor")


                pnlProvider.Visible = False
                'providerrbl.SelectedIndex = -1

            Case "1195"
                'Case "contract"

                Dim firstNameReq As New RequiredField(firstnameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(lastnameTextBox, "Last Name")

                Dim add1Req As New RequiredField(address1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")



                pnlProvider.Visible = False
                'providerrbl.SelectedIndex = -1



            Case "1196"
                'Case "other"

                Dim firstNameReq As New RequiredField(firstnameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(lastnameTextBox, "Last Name")

                Dim add1Req As New RequiredField(address1textbox, "Address 1")
                Dim cityReq As New RequiredField(citytextbox, "City")
                Dim stateReq As New RequiredField(statetextbox, "State")
                Dim zipReq As New RequiredField(ziptextbox, "ZIP")
                Dim phoneReq As New RequiredField(phoneTextBox, "Phone")



                'pnlAlliedHealth.Visible = False

                pnlProvider.Visible = False
                'providerrbl.SelectedIndex = -1



            Case Else


                Dim firstNameReq As New RequiredField(firstnameTextBox, "First Name")
                Dim lastNameReq As New RequiredField(lastnameTextBox, "Last Name")



                pnlProvider.Visible = False
                'providerrbl.SelectedIndex = -1

                pnlProvider.Visible = False
                ' providerrbl.SelectedIndex = -1



        End Select


    End Sub
End Class
