﻿Imports System.Net.Mail
Imports App_Code



Partial Class EmailActivity
    Inherits System.Web.UI.Page
    Dim RequestNum As String
    Dim UserInfo As UserSecurity

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        RequestNum = Request.QueryString("RequestNum")

        lblTicketNum.Text = RequestNum
        UserInfo = Session("objUserSecurity")
        mpeEmailActivity.Show()
        btnCancel.Attributes.Add("onclick", "closeWindows()")

    End Sub
    Protected Sub SendP1EmailAlters(ByVal TicketNum As String)


        ' Get Path to send url



        Dim path As String
        Dim directory As String = ""
        path = HttpContext.Current.Request.Url.AbsoluteUri
        Dim pathParts() As String = Split(path, "/")
        Dim i As Integer
        Do While i < pathParts.Length() - 1

            directory = directory + pathParts(i) + "/"
            i = i + 1

        Loop


        ' Create Mail Object


        Dim sSubject As String
        Dim sBody As String


        sSubject = "Test p1 Pager " & TicketNum


        sBody = "<!DOCTYPE html>" & _
                                "<html>" & _
                                "<head>" & _
                                "</head>" & _
                                "<body>" & _
                                "<b>Test p1 Pager</b> " & TicketNum & _
                                "<br>" & _
                                "<a href=""" & directory & "/EmailActivity.aspx?requestnum=" & TicketNum & """ ><u><b>Respond Here</b></u></a>" & _
                                "<br>" & txtActivity.Text & _
                                 "</body>" & _
                                 "</html>"



        '  sToAddress = "Patrick.Brennan@crozer.org"

        Dim mailObj As New MailMessage()
        mailObj.From = New MailAddress("CSC@crozer.org")

        mailObj.IsBodyHtml = True
        mailObj.Subject = sSubject
        mailObj.Body = sBody


        'Dim EmailList As New List(Of EmailDistribution)
        'EmailList = New EmailDistributionList("p1Pager").EmailList


        'For Each EmailAddress In EmailList



        '    Dim toEmail As String = EmailAddress.Email
        '    mailObj.To.Add(New MailAddress(toEmail))




        'Next

        'Dim smtpClient As New SmtpClient()
        'smtpClient.Host = "Smtp2.Crozer.Org"

        'smtpClient.Send(mailObj)




    End Sub

    Public Sub AddTicketActivity()
        Dim conn As New SetDBConnection()
        Dim Cmd As New SqlCommand()


        Cmd.CommandText = "InsRequestActivites"
        Cmd.CommandType = CommandType.StoredProcedure
        

        Using sqlConn As New SqlConnection(conn.EmployeeConn)

            Cmd.Parameters.AddWithValue("@RequestNum", RequestNum)
            Cmd.Parameters.AddWithValue("@TechClientNum", UserInfo.ClientNum)
            Cmd.Parameters.AddWithValue("@ActivityDesc", txtActivity.Text)
            Cmd.Parameters.AddWithValue("@StatusType", "user")
            Cmd.Parameters.AddWithValue("@StatusCd", "note")

            Cmd.Connection = sqlConn

            sqlConn.Open()

            Cmd.ExecuteNonQuery()

            sqlConn.Close()


        End Using


        
    End Sub

    Protected Sub btnUpdate_Click(sender As Object, e As System.EventArgs) Handles btnUpdate.Click
        AddTicketActivity()
        SendP1EmailAlters(RequestNum)

        ClientScript.RegisterClientScriptBlock(Me.GetType(), "Javascript", "javascript: closeWindows()")
        ' Page.RegisterClientScriptBlock("keyClientBlock", "javascript: closeWindows()"

    End Sub
    Protected Sub ajaxFileUpload_OnUploadComplete(sender As Object, e As AjaxControlToolkit.AjaxFileUploadEventArgs)

        htmlEdExActivity.AjaxFileUpload.SaveAs("\\backup02\\Library\\Support\\Customer Service Center\\Screenshots\\brep02\\" + e.FileName)
        e.PostedUrl = Page.ResolveUrl("\\backup02\\Library\\Support\\Customer Service Center\\Screenshots\\brep02\\" + e.FileName)

    End Sub
End Class
