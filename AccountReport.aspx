﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CSCMaster.master" AutoEventWireup="false" CodeFile="AccountReport.aspx.vb" Inherits="AccountReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <style type="text/css">
        .style2
        {
            text-align: center;
            background-color: #FFFFCC;
            color: #336666;
            font-weight: bold;
            font-size: x-large;
            height: 43px;
        }
    </style>

    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

<asp:UpdatePanel ID="UpdatePanel1" runat ="server"  UpdateMode="Conditional"> 
   <ContentTemplate>  
   <asp:UpdateProgress ID="UpdateProgress1" runat="server"  EnableViewState="false"  AssociatedUpdatePanelID="PanWaitfor">
       <ProgressTemplate>
            <div id="IMGDIV" align="center" valign="middle" runat="server" style="position:
                    absolute;left: 400px;top:150px;visibility:visible;vertical-align:middle;border-style:
                    inset;border-color:black;background-color:White;width:210px;height:75px; z-index: 999;">
            Loading Data ...
                <img src="Images/AjaxProgress.gif" /><br />
            </div>
       </ProgressTemplate>
    </asp:UpdateProgress>
  </ContentTemplate>
</asp:UpdatePanel>

<%--<asp:UpdatePanel ID="uplPanel" runat ="server"  UpdateMode="Conditional"> 
   <ContentTemplate>  
--%>
    <div id="maindiv" runat="server" align="left">

      <table  style="border-style: groove" align="left" width="100%">
        <tr align="center">
           <td class="tableRowHeader" colspan="4">
                  Account Report
           </td>
        </tr>
        <tr>
            <td align="right">   
                <asp:Label ID="selacctlbl" runat="server" Text="Select Application:" Font-Bold="True"></asp:Label>
            </td>
            <td align="left">
                 <asp:DropDownList ID = "Applicationddl" runat="server" Width="150px" >
                     </asp:DropDownList>

                <asp:Label ID="Appnumlbl" runat="server" Visible="false"></asp:Label>

            </td>
            <td align="right">
                <asp:Label ID="typeLbl" runat="server" Font-Bold="True" Text="Select Affiliation:"></asp:Label>
            </td>
            <td align="left">
                      <asp:DropDownList ID = "Affiliationddl" runat="server"  Width="150px" >
                     </asp:DropDownList>
                     <asp:Label ID="empType" runat="server" Visible="false"></asp:Label>
            </td>

         </tr>
         <tr>
            <td>
            </td>

            <td align="right">
                <asp:Label ID="endonlylbl" runat="server"  Font-Bold="True" Text="Display Clients with End Dates only:" ></asp:Label>
            
            </td>
            <td align="left">
                <asp:RadioButtonList ID="displyrbl" runat="server"   RepeatDirection="Horizontal">
                                            <asp:ListItem>Yes</asp:ListItem>
                                            <asp:listItem>No</asp:listItem>
                </asp:RadioButtonList>
            </td>
            <td>
            </td>

         </tr>
         <tr>
          <td  align="center" colspan="4">
            <table>
                <tr>
                    <td align="center">

                            <asp:Button ID ="btnSubmit" runat = "server" Text ="Submit"  Width="120px" Height="30px"
                                    Font-Names="Arial" ForeColor="White" Font-Bold="True" Font-Size="Small" />
                    </td>
                    <td>
                                
                    </td>
                    <td align="center" >

                            <asp:Button ID = "btnReset" runat= "server" Text="Reset"  Width="120px" Height="30px"
                                    Font-Names="Arial" ForeColor="White" Font-Bold="True" Font-Size="Small" />

                    </td>
                    <td>
                        <asp:Button ID="btnExcel" runat="server" Text="Excel" Width="120px" Height="30px" 
                                    Font-Names="Arial" ForeColor="White" Font-Bold="True" Font-Size="Small"/>

                   </td>

                </tr>
            </table>
          </td>
         </tr> 
         <tr>
         
            <td colspan="4" align="center">
                <asp:Panel ID="totalpan" runat="server" Visible="false">
                    <table border="2px" width="50%">
                        <tr>
                            <td colspan="2" class="tableRowHeader" align="center">
                                Totals by Affiliation 
                            
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Total Count:
                            </td>
                            <td>
                                <asp:Label ID="totalcountlbl" runat="server">
                                </asp:Label>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                Unknow Affiliation Count:
                            </td>
                            <td>
                                <asp:Label ID="UnknowCountlbl" runat="server">
                                </asp:Label>
                            </td>
                        </tr>
                    
                        <tr>
                            <td>
                                Employee Count:
                            </td>
                            <td>
                                <asp:Label ID="EmployeeCountlbl" runat="server">
                                </asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Contract Count:
                            </td>
                            <td>
                                <asp:Label ID="ContractCountlbl" runat="server">
                                </asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Student Count:
                            </td>
                            <td>
                                <asp:Label ID="StudentCountlbl" runat="server">
                                </asp:Label>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                Physician Count:
                            </td>
                            <td>
                                <asp:Label ID="PhysicianCountlbl" runat="server">
                                </asp:Label>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                Nursing Count:
                            </td>
                            <td>
                                <asp:Label ID="NursingCountlbl" runat="server">
                                </asp:Label>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                Emp with No Number Count:
                            </td>
                            <td>
                                <asp:Label ID="EmpNoNumberCountlbl" runat="server">
                                </asp:Label>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                Non CKHN Count:
                            </td>
                            <td>
                                <asp:Label ID="NonCKHNCountlbl" runat="server">
                                </asp:Label>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                Resident Count:
                            </td>
                            <td>
                                <asp:Label ID="ResidentCountlbl" runat="server">
                                </asp:Label>
                            </td>
                        </tr>

                    </table>
                
               </asp:Panel>
            </td>
         </tr>         
         <tr>
         
            <td colspan="4">
 
                <asp:Table  Width="100%" ID = "tblAccounts" runat = "server">
                </asp:Table>
            </td>
         
         </tr>
        
         <tr>
                <td colspan="4">
                            <asp:GridView ID="GridAccounts" runat="server" AllowSorting="True" 
                           AutoGenerateColumns="False" BackColor="White" BorderColor="#336666" 
                           BorderStyle="Double" BorderWidth="3px" CellPadding="1" 
                           DataKeyNames="client_num"  Visible="false"
                           EmptyDataText="No Accounts found " 
                           EnableTheming="False" Font-Size="Medium" GridLines="Horizontal" Width="100%">
                           <FooterStyle BackColor="White" ForeColor="#333333" />
                           <HeaderStyle BackColor="#336666" Font-Bold="False" Font-Size="X-Small" 
                               ForeColor="White" HorizontalAlign="Left" VerticalAlign="Middle" />
                           <PagerStyle BackColor="#336666" Font-Size="Medium" ForeColor="White" 
                               HorizontalAlign="Left" />
                           <RowStyle BackColor="White" ForeColor="#333333" Font-Size="X-Small" />
                           <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                           <SortedAscendingCellStyle BackColor="#F7F7F7" />
                           <SortedAscendingHeaderStyle BackColor="#487575" />
                           <SortedDescendingCellStyle BackColor="#E5E5E5" />
                           <SortedDescendingHeaderStyle BackColor="#275353" />
                           <Columns>

                               <asp:CommandField ButtonType="Button" ControlStyle-BackColor="#84A3A3" 
                                      HeaderStyle-HorizontalAlign="Left" ItemStyle-Height="20px" SelectText="Select" 
                                      ShowSelectButton="true">
                                  <ControlStyle BackColor="#84A3A3" />
                                  <HeaderStyle HorizontalAlign="Left" />
                                  <ItemStyle Height="20px" />
                                  </asp:CommandField>
                            
                            	  


                               <asp:BoundField DataField="fullname"  ItemStyle-HorizontalAlign="Left" HeaderText="User Name" />

                               <asp:BoundField DataField="LoginName" ItemStyle-HorizontalAlign="Center" HeaderText="Network Login" />

                               <asp:BoundField DataField="AppLoginName" ItemStyle-HorizontalAlign="Center" HeaderText="App Login " />

                               <asp:BoundField DataField="StartDate" ItemStyle-HorizontalAlign="Center" HeaderText="Start Date" />

                               <asp:BoundField DataField="siemens_emp_num"  ItemStyle-HorizontalAlign="Center" HeaderText="Emp # " />
                               <asp:BoundField DataField="emp_type_cd"  ItemStyle-HorizontalAlign="Center" HeaderText="Emp Type " />

                           </Columns>
                       </asp:GridView>                            
                </td>
          </tr>
      </table>


 <asp:Panel ID="pnlWait" runat="server"  Width="70%" Height="60%"  BackColor="#CCCCCC"  style="display:none">
        <asp:UpdatePanel ID="PanWaitfor" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table align="center" border="1px" width="90%">
                   <tr valign="middle">
                        <td colspan="4"></td>
                    </tr>
                    <tr>
                        <td class="tableRowHeader" colspan="4">
                            Pleas wait for data load

                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
         <ajaxToolkit:ModalPopupExtender ID="Loadwait" runat="server" 
       TargetControlID="hdnBtnExists" PopupControlID="pnlWait" 
       BackgroundCssClass="ModalBackground">  
        </ajaxToolkit:ModalPopupExtender>

        <asp:Button ID="hdnBtnExists" runat="server" Style="display: none" />


    </div>
<%--   </ContentTemplate>
  </asp:UpdatePanel>--%>
</asp:Content>



