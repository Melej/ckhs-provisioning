﻿Imports System.Data.SqlClient
Imports System.IO
Imports System.Data
'Imports C1.Web.Command
'Imports C1.Web.Command.C1WebTreeView
'Imports C1.Web.C1WebDialog
Imports System.Type
Imports System.Linq
Imports System.Collections.Generic
Imports System.Data.DataTable
Imports System.Data.LoadOption
Imports System.Data.DataTableExtensions
Imports App_Code

Partial Class CSCMaster
    Inherits System.Web.UI.MasterPage
    Private Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)
    Private Const RPC_E_CALL_REJECTED = &H80010001

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load



        ' Dim sLocalConn As String = Session("CSCConn").Replace("\", "\\")



        'Dim frmSec As New FormSecurity(Session("objUserSecurity"))

        '/******* SECURITY LEVELE AND ACCESS ********/
        'SysAdmin           100
        'Administrators     90
        'CredentialAnalyst  82
        'HelpDeskAdmin      80
        'HelpDeskAnalyst    70 r4
        ' HR view           71 
        'Internatlhelpdesk  72

        ' Most users who submit Tickets and RFS some can Submit Account Requests
        'ProviderAnalyst    60 
        'Reqestors          40
        'SubmitTicket       25 Can View their own request and some employee info
        ' In the future they will submit tickects and RFS

        ' Main Accordion panels
        'accpnCSCForm.Visible = False
        'accpnQueues.Visible = False
        'ccpnCSCRequests.Visible = False
        'accpnCSCQueues.Visible = False
        'accpnMaintenance.Visible = False
        'ReportingPane1.Visible = False

        ' account request forms 
        'btnSearch.Visible = False
        'btnSignOnForm.Visible = False
        'btnMultSignOnForm.Visible = False
        'btnMySubmittedRequests.Visible = False

        ' Group Queues
        'btnAccountQueue.Visible = False
        'btnValidateQueue.Visible = False
        'btnInvalidQueue.Visible = False
        'btnProviderQueue.Visible = False
        'btnOpenProviderQueue.Visible = False

        ' CSC Forms
        'btnNewTicketRequest.Visible = False
        'btnUnassignedRequests.Visible = False

        ' Maint
        'btnAppMaintenance.Visible = False
        'btnAppOwnerMaintenance.Visible = False
        'btnRoleTemplateMaintenace.Visible = False
        'btnMergeAccounts.Visible = False

        lblCurrentRequest.ToolTip = "Name: " & Session("UserName") & "Login: " & Session("loginid") & " UserType " & Session("UserType") & " Level " & Session("SecurityLevelNum") _
            & " userNum: " & Session("ClientNum") & " Environment: " & Session("environment") & " Other Tech#" & Session("TechClientNum") & " Connection: " & Session("EmployeeConn") & " Help Desk DB " & Session("CSCConn")

        Dim usertype As String = ""
        Dim Securitylevel As Integer


        If Not Session("EmployeeConn").ToString.IndexOf("test") = -1 Then
            lblCurrentRequest.ForeColor = Color.Red
            TestLblb.Visible = True
        ElseIf Not Session("EmployeeConn").ToString.IndexOf("Test") = -1 Then
            lblCurrentRequest.ForeColor = Color.Red
            TestLblb.Visible = True

        ElseIf Not Session("EmployeeConn").ToString.IndexOf("webdev") = -1 Then
            lblCurrentRequest.ForeColor = Color.Red
            TestLblb.Visible = True

        End If
        Securitylevel = Session("SecurityLevelNum")
        usertype = Session("Usertype")

        ' to test security
        'usertype = "providerreqestors"
        If Session("SecurityLevelNum") < 25 Then
            Response.Redirect("~/Security.aspx")
        End If
        lblValidationCount.Text = ""

        'LinkBntTickets.Text = ""

        'Need to store message and status then grab it from user serurity Let sysadmins change it
        'MainMessage.Text = "Interface issues have been identified and currently being resolved"
        'MainMessage.ForeColor() = Color.Red

        'MainMessage.Text = "All Systems are available"
        'MainMessage.ForeColor() = Color.Green

        'MainMessage.Visible = True

        Dim Security As New UserSecurity(Session("LoginID"), Session("EmployeeConn"))
        lblValidationCount.Text = "Account Requests Waiting For Validation " & Security.ValidateCount

        'LinkBntTickets.Text = "Unassigned Tickets/RFS " & Security.unassignedTickets '& "<a href=""MyQueue.aspx" & "</a>"
        'LinkBtnNoTech.Text = "Group Assigned No Tech " & Security.NoTechCount

        'Dim iP1Count As Integer
        'iP1Count = Security.P1Count

        'If iP1Count > 0 Then
        '    LinkBtnP1.Text = iP1Count & " P1 Open"
        '    LinkBtnP1.Visible = True
        'End If

        'If Securitylevel < 70 Then
        '    lblValidationCount.Visible = False
        '    'LinkBntTickets.Visible = False
        '    'LinkBtnNoTech.Visible = False
        '    LinkBtnP1.Visible = False
        'End If


        '?RequestNum=" & ari.request_num & """ ><u><b> " & ari.request_num & "</b></u></a>"
        'btnTicketRFS.Visible = False

        'If Securitylevel < 101 Then
        'btnNewTicketRequest.Visible = False
        'btnUnassignedRequests.Visible = False
        'btnClientMaintenance.Visible = False

        ' Maint
        'btnAppMaintenance.Visible = False
        'btnAppOwnerMaintenance.Visible = False
        'btnRoleTemplateMaintenace.Visible = False
        'btnMergeAccounts.Visible = False
        'btnClientMaintenance.Visible = False
        'btnReports.Visible = False

        'btnTicketRFS.Visible = False


        ' End If



        If Session("Server") = ConfigurationManager.AppSettings.Get("ProdWebServers") Then
            ' Production Data and web site
            If Securitylevel < 40 Then

                btnSearch.Visible = False

                btnSignOnForm.Visible = False

                btnTicketRFS.Visible = False
            ElseIf Securitylevel > 39 And Securitylevel < 60 Then
                btnSearch.Visible = True
                btnTicketRFS.Visible = True
                btnSignOnForm.Visible = True
                'LinkBntTickets.Visible = False
                lblValidationCount.Visible = False

            End If

        ElseIf Session("Server") = ConfigurationManager.AppSettings.Get("TestWebServers") Then
            ' Test Site and test data
            If Securitylevel < 40 Then

                btnSearch.Visible = False

                btnSignOnForm.Visible = False
                btnTicketRFS.Visible = False

            End If

        Else
            ' else default to test

            If Securitylevel < 40 Then

                btnSearch.Visible = False

                btnSignOnForm.Visible = False
                btnTicketRFS.Visible = True
                'LinkBntTickets.Visible = False
                lblValidationCount.Visible = False
            End If

        End If



		If Securitylevel < 70 Then
            ' Will create User Ticket/RFS form
            ' They will see 2 forms 

            ' account request forms 
            btnChangeLogon.Visible = False

            'btnMultSignOnForm.Visible = False
            ' btnMySubmittedRequests.Visible = False

            ' Group Queues
            btnAccountQueue.Visible = False
            btnValidateQueue.Visible = False
            'btnInvalidQueue.Visible = False
            btnProviderQueue.Visible = False
            btnCredentialQueue.Visible = False
            btnOpenProviderQueue.Visible = False
            btnOlie.Visible = False
            'btnCPMMaintenance.Visible = False
            btnSearcTicket.Visible = False
            btnMyCSCQueue.Visible = False
            btnUserMaint.Visible = False
            btnProjects.Visible = False
            ' CSC Forms
            'btnTicketRFS.Visible = True




        End If

		If Securitylevel > 69 Then

			btnPhysGrpMaintenance.Visible = True
			btnVendorMaintenance.Visible = True
            'btnSearcTicket.Visible = True

            btnMyCSCQueue.Visible = True
			btnCSCList.Visible = True
			btnOlie.Visible = True

			btnTicketRFS.Visible = True

			btnEmployeeQueue.Visible = True

		End If

		If Securitylevel > 70 Then 'everyone except R4 
			btnCapitalProj.Visible = True
			btnEmployeeQueue.Visible = True
		End If

		If Securitylevel < 80 Then
            btnActiveMgr.Visible = False
            lblValidationCount.Visible = False
        End If


		Select Case Securitylevel
            Case 65, 66, 60
                ' executive View Can view Only certian queues 
                'btnSearcTicket.Visible = True
                btnCapitalProj.Visible = True
				btnEmployeeQueue.Visible = True


            Case 67
                ' btnSearcTicket.Visible = True
                btnReports.Visible = True
                btnCapitalProj.Visible = True
                btnEmployeeQueue.Visible = True

            Case 70

				' Group Queues
				btnSignOnForm.Visible = True
				btnAccountQueue.Visible = True


				btnValidateQueue.Visible = False
				btnProviderQueue.Visible = False

				btnPhysGrpMaintenance.Visible = False
				btnVendorMaintenance.Visible = False

				'btnInvalidQueue.Visible = False
				btnCredentialQueue.Visible = False
				btnOpenProviderQueue.Visible = False

				btnOlie.Visible = False

				btnProjects.Visible = False
				'btnUserMaint.Visible = False

				btnActiveMgr.Visible = False
				btnPhysGrpMaintenance.Visible = False

				btnVendorMaintenance.Visible = False
				btnReports.Visible = False

				btnSearcTicket.Visible = True
				btnMyCSCQueue.Visible = True
                'btnTicketRFS.Visible = True
                btnCSCList.Visible = True

            Case 71
                ' Group Queues
                'btnAccountQueue.Visible = False
                btnValidateQueue.Visible = False
                'btnInvalidQueue.Visible = False
                btnProviderQueue.Visible = False
                btnCredentialQueue.Visible = False
                btnOpenProviderQueue.Visible = False
                btnOlie.Visible = False
                'btnCPMMaintenance.Visible = False
                btnSearcTicket.Visible = False
                btnMyCSCQueue.Visible = False
                btnUserMaint.Visible = False
                btnProjects.Visible = False
                ' CSC Forms
                btnTicketRFS.Visible = False
                btnPhysGrpMaintenance.Visible = False
                btnVendorMaintenance.Visible = False

                btnReports.Visible = True

            Case 80

				btnReports.Visible = True

			Case 82
				btnCredentialQueue.Visible = True
				btnValidateQueue.Visible = False
				btnUserMaint.Visible = False

				btnActiveMgr.Visible = False

				btnMyCSCQueue.Visible = False

				btnProjects.Visible = False
				' CSC Forms
				btnTicketRFS.Visible = False

				btnReports.Visible = False

		End Select

        'If Session("Usertype") = "EXternalHelpDeskAnalyst" Then
        '    btnUserMaint.Visible = False
        'End If


        If Securitylevel > 79 Then
            ' R4 external 
            btnSearcTicket.Visible = True

        End If

        'If Securitylevel = 80 Then


        'End If

        If Securitylevel = 82 Then
            btnCredentialQueue.Visible = True
            btnValidateQueue.Visible = False
            btnUserMaint.Visible = False

            btnActiveMgr.Visible = False

            btnMyCSCQueue.Visible = False

			btnProjects.Visible = False
            ' CSC Forms
            btnTicketRFS.Visible = False

            btnReports.Visible = False
        End If


        'If Securitylevel > 79 Then

        '    btnOlie.Visible = True

        'End If

        If Securitylevel > 89 Then

            'btnAppOwnerMaintenance.Visible = True
            btnUserMaint.Visible = True

            btnReports.Visible = True

        End If

        If Securitylevel > 95 Then

            ' Maint
            'btnAppMaintenance.Visible = True
            'btnAppOwnerMaintenance.Visible = True
            'btnRoleTemplateMaintenace.Visible = True
            'btnMergeAccounts.Visible = True
            btnPhysGrpMaintenance.Visible = True
            'btnClientMaintenance.Visible = True
            'btnCPMMaintenance.Visible = True

        End If

        If Securitylevel > 101 Then

            btnChangeLogon.Visible = True

            ' Maint
            'btnAppMaintenance.Visible = True
            'btnRoleTemplateMaintenace.Visible = True
            'btnMergeAccounts.Visible = True
            'btnReports.Visible = True
            'btnPhysGrpMaintenance.Visible = True
            'btnVendorMaintenance.Visible = True
            'btnUserMaint.Visible = True
            'btnGroupMaint.Visible = True
            'btnTicketRFS.Visible = True
            'btnOlie.Visible = True



        End If


		Select Case usertype.ToLower
			Case "providerreqestors"
				btnAccountQueue.Visible = False
				btnValidateQueue.Visible = False
				' btnInvalidQueue.Visible = False
				btnProviderQueue.Visible = False
				btnCredentialQueue.Visible = False
				btnOpenProviderQueue.Visible = False
			Case "provaccount"
				' btnAccountQueue.Visible = True


				'Select Case Securitylevel
				'    Case 100
				'        ' account request forms 
				'        btnSearch.Visible = False
				'        btnSignOnForm.Visible = False
				'        btnMultSignOnForm.Visible = False
				'        btnMySubmittedRequests.Visible = False

				'        ' Group Queues
				'        btnAccountQueue.Visible = False
				'        btnValidateQueue.Visible = False
				'        btnInvalidQueue.Visible = False
				'        btnProviderQueue.Visible = False
				'        btnOpenProviderQueue.Visible = False

				'        ' CSC Forms
				'        btnNewTicketRequest.Visible = False
				'        btnUnassignedRequests.Visible = False

				'        ' Maint
				'        btnAppMaintenance.Visible = False
				'        btnAppOwnerMaintenance.Visible = False
				'        btnRoleTemplateMaintenace.Visible = False
				'        btnMergeAccounts.Visible = False

				'End Select

			Case "CredentialAnalyst"
				btnPhysGrpMaintenance.Visible = True

			Case "helpdeskanalyst"
				'ReportingPane1.Visible = False

				Select Case Securitylevel
					Case 50

				End Select


		End Select


		If Session("LoginID").ToString.ToLower = "geip00" Or Session("LoginID").ToString.ToLower = "melej" Then
			btnTicketRFS.Visible = True
			btnSearcTicket.Visible = True
			btnMyCSCQueue.Visible = True

		Else

			btnTicketRFS.Visible = False
            'btnSearcTicket.Visible = False
            btnMyCSCQueue.Visible = False

		End If


		' this will make the button gary after clicking
		If Session("LeftNavSelectedButton") <> "" Then
            Dim mybutton As Button = FindControl(Session("LeftNavSelectedButton"))
            Dim myControl1 As Control = FindControl(Session("LeftNavSelectedButton"))
            mybutton.BackColor = Color.LightGray
            mybutton.ForeColor = Color.Black
        End If


    End Sub
    'Protected Sub fillCSCGroupQueues()


    '    Dim thisData As New CommandsSqlAndOleDb("sel_group_names", Session("EmployeeConn"))

    '    Dim dtGroups As DataTable = thisData.GetSqlDataTable

    '    For Each rw As DataRow In dtGroups.Rows
    '        Dim btnGroup As New Button

    '        btnGroup.PostBackUrl = "CSCGroupQueues.aspx?GroupNum=" & rw("group_num")
    '        btnGroup.Text = rw("group_name")

    '        'accpnCSCQueues.ContentContainer.Controls.Add(btnGroup)
    '    Next

    'End Sub

    'Public Sub SetC1WebTopicBarV2(Optional ByRef sCSCNumber As String = "0")
    '    Dim dtApplicationFormNames As DataTable

    '    ' Dim s As New Security(Session("LoginID"))


    '    Dim Security As New UserSecurity(Session("LoginID"), Session("EmployeeConn"))



    '    Dim alMenuList As New ArrayList
    '    Dim grp As C1.Web.Command.C1WebTopicBarGroup
    '    Dim wtbItem As New C1WebTopicBarItem
    '    Dim dtMenuItems As DataTable
    '    Dim MenuCriteria As New CommandsSqlAndOleDb()



    '    dtApplicationFormNames = Security.getPageSecurityDataTableV2()

    '    For icat As Integer = 0 To wtbCaseType.Groups.Count - 1 'last is hardcoded
    '        grp = wtbCaseType.Groups(icat)

    '        '============================================
    '        ' Fix in database table ApplicationFormNames
    '        '============================================

    '        alMenuList.Add(grp.Text)
    '    Next

    '    '============================
    '    ' Fill menu items
    '    '============================

    '    For i As Integer = 0 To alMenuList.Count - 1 'legend and links are hard coded

    '        Dim sCategory As String = alMenuList(i)
    '        Dim sSelection As String = "category='" & sCategory & "'" & sCSCNumber

    '        dtMenuItems = MenuCriteria.GetSqlDataTableWithSortAndCriteria(sSelection, "SortOrder", dtApplicationFormNames)

    '        Dim sJscript As String = "" ' " GoToPage('CaseInfo.aspx');" '"newWindowPlain('" & sUrl & "',600,400 );"
    '        If dtMenuItems.Rows.Count > 0 Then


    '            wtbCaseType.Groups(i).Items.Clear()
    '            For it As Integer = 0 To dtMenuItems.Rows.Count - 1
    '                ''page has a miniumum security Level
    '                If Session("securityLevel") <= dtMenuItems.Rows(it).Item("minSecurityLevel") Then

    '                    wtbItem = New C1WebTopicBarItem()

    '                    If dtMenuItems.Rows(it).Item("PageName").ToString.IndexOf("http://") > -1 Then

    '                        wtbItem.NavigateUrl = dtMenuItems.Rows(it).Item("PageName")

    '                    ElseIf dtMenuItems.Rows(it).IsNull("SubFolder") = False Then

    '                        wtbItem.NavigateUrl = "~/" & dtMenuItems.Rows(it).Item("SubFolder") & "/" & dtMenuItems.Rows(it).Item("PageName") ' & "?CurrentCaseNum=" & Session("CurrentCaseNum")

    '                    Else

    '                        wtbItem.NavigateUrl = "~/" & dtMenuItems.Rows(it).Item("PageName") ' & "?CurrentCaseNum=" & Session("CurrentCaseNum")

    '                    End If

    '                    wtbItem.ToolTip = "Go to " & dtMenuItems.Rows(it).Item("Caption")
    '                    wtbItem.Text = dtMenuItems.Rows(it).Item("Caption")



    '                    wtbCaseType.Groups(i).Items.Add(wtbItem)
    '                End If

    '            Next 'entry

    '        End If

    '    Next 'category 



    'End Sub
    Public Sub SetSessionsOnclick()

        Dim myqueu As String = "yes"

        Session("Myqueue") = "yes"

    End Sub
    Public Sub fillCaseInfo(ByVal iCaseNum As String, ByVal rowCaseInfo As DataRow)
        Dim SJCall As String = ""
        SJCall = Session("DataConn")
        Dim sCaseNumMode As String = ":cm"


        Dim sLocalConn As String = Session("DataConn").Replace("\", "\\")

        'SJCall = "GetThisCaseInfo('" & iCaseNum & "','0','" & Session("Loginid") & "','" & sLocalConn & "');" '"ddrivetipDelay('" & "PatientInfo" & "',150 ,'LightYellow',400," & 12 & ")"

        SJCall = "GetThisCaseInfo('" & Session("currentcasenum") & sCaseNumMode & "','" & Session("CaseHospitalNum") & "','" & Session("Loginid") & "','" & sLocalConn & "');" '"ddrivetipDelay('" & "PatientInfo" & "',150 ,'LightYellow',400," & 12 & ")"







        '====Image user Popup==Moved 5/11/2009========================
        Session("Environment") = Session("Environment")
        lblCurrentRequest.ToolTip = " Login " & Session("loginid") & " " & Session("Hospital") & "<br/>  Env: " & Session("Environment") & "<br> " & Session("SecurityType") & _
           "<br>" & Session("UserNameDisplay") & "<br> Pager: " & Session("UserPager")
        If DataCheckingAndValidation.IsValidSessionField(Session("CurrentCaseNum")) Then

            lblCurrentRequest.ToolTip &= " CC#: " & Session("CurrentCaseNum") & "<br> Level: " & Session("securityLevel")
        Else 'no current case
            lblCurrentRequest.ToolTip &= "<br> Level: " & Session("securityLevel")
        End If
        If DataCheckingAndValidation.IsValidSessionField(Session("selectedUnit")) Then
            lblCurrentRequest.ToolTip &= "<br> Selected Unit: " & Session("selectedUnit")
        Else 'no selected unit
            ' imgLogo.ToolTip &= " Selected Unit: " & Session("selectedUnit")
        End If
        SJCall = " showhint('" & lblCurrentRequest.ToolTip & "', this, event, '150px')"
        lblCurrentRequest.Attributes.Add("onMouseover", SJCall)
        lblCurrentRequest.ToolTip = ""
        '============End User Popup===========================


        '===========Session PatientPrintHeader============Session("Hospital")

        'uplHeader.Update()

    End Sub
    Private Sub setSelectedbutton(ByVal CurButton As Object)
        Dim mybutton As Button = CurButton
        Session("LeftNavSelectedButton") = mybutton.ID
    End Sub

    Protected Sub btnSearch_Click(sender As Object, e As System.EventArgs) Handles btnSearch.Click
        setSelectedbutton(sender)
        Response.Redirect("~/" & "SimpleSearch.aspx")
    End Sub

    Protected Sub btnSignOnForm_Click(sender As Object, e As System.EventArgs) Handles btnSignOnForm.Click
        setSelectedbutton(sender)
        Response.Redirect("~/" & "SignOnForm.aspx")

    End Sub
    Protected Sub btnChangeLogon_Click(sender As Object, e As System.EventArgs) Handles btnChangeLogon.Click
        setSelectedbutton(sender)
        Response.Redirect("~/" & "ChangeLogon.aspx")
    End Sub

    Protected Sub btnMySubmittedRequests_Click(sender As Object, e As System.EventArgs) Handles btnMySubmittedRequests.Click
        setSelectedbutton(sender)
        Response.Redirect("~/" & "MyRequests.aspx")

    End Sub

    Protected Sub btnAccountQueue_Click(sender As Object, e As System.EventArgs) Handles btnAccountQueue.Click
        setSelectedbutton(sender)
        Response.Redirect("~/" & "MyAccountQueue.aspx")

    End Sub

    Protected Sub btnValidateQueue_Click(sender As Object, e As System.EventArgs) Handles btnValidateQueue.Click
        setSelectedbutton(sender)
        Response.Redirect("~/" & "ValidateQueue.aspx")

    End Sub

    'Protected Sub btnInvalidQueue_Click(sender As Object, e As System.EventArgs) Handles btnInvalidQueue.Click
    '    setSelectedbutton(sender)
    '    Response.Redirect("~/" & "InvalidAccountQueueV3.aspx")

    'End Sub

    Protected Sub btnProviderQueue_Click(sender As Object, e As System.EventArgs) Handles btnProviderQueue.Click
        setSelectedbutton(sender)
        Response.Redirect("~/" & "ProviderQueue.aspx")

    End Sub
    Protected Sub btnCredentialQueue_Click(sender As Object, e As System.EventArgs) Handles btnCredentialQueue.Click
        setSelectedbutton(sender)
        Response.Redirect("~/" & "CredentialQueue.aspx")

    End Sub
    Protected Sub btnOpenProviderQueue_Click(sender As Object, e As System.EventArgs) Handles btnOpenProviderQueue.Click
        setSelectedbutton(sender)
        Response.Redirect("~/" & "OpenProviderRequests.aspx")

    End Sub


    Protected Sub btnTicketRFS_Click(sender As Object, e As System.EventArgs) Handles btnTicketRFS.Click
        setSelectedbutton(sender)
        Response.Redirect("~/" & "NewRequestTicket.aspx")

    End Sub

    'Protected Sub btnUnassignedRequests_Click(sender As Object, e As System.EventArgs) Handles btnUnassignedRequests.Click
    '    setSelectedbutton(sender)
    '    Response.Redirect("~/" & "UnassignedRequests.aspx")
    'End Sub

    'Protected Sub btnAppMaintenance_Click(sender As Object, e As System.EventArgs) Handles btnAppMaintenance.Click
    '    setSelectedbutton(sender)

    '    Response.Redirect("~/" & "ApplicationMaintenance.aspx")
    'End Sub

    'Protected Sub btnAppOwnerMaintenance_Click(sender As Object, e As System.EventArgs) Handles btnAppOwnerMaintenance.Click
    '    setSelectedbutton(sender)
    '    Response.Redirect("~/" & "OwnerMaintenance.aspx")

    'End Sub

    'Protected Sub btnRoleTemplateMaintenace_Click(sender As Object, e As System.EventArgs) Handles btnRoleTemplateMaintenace.Click
    '    setSelectedbutton(sender)
    '    Response.Redirect("~/" & "RoleMaintenance.aspx")

    'End Sub

    'Protected Sub btnMergeAccounts_Click(sender As Object, e As System.EventArgs) Handles btnMergeAccounts.Click
    '    setSelectedbutton(sender)
    '    Response.Redirect("~/" & "MergeAccounts.aspx")

    'End Sub

    'Protected Sub btnClientMaintenance_Click(sender As Object, e As System.EventArgs) Handles btnClientMaintenance.Click
    '    setSelectedbutton(sender)
    '    Response.Redirect("~/" & "MaintenanceQueue.aspx")

    'End Sub
    Protected Sub btnHelp_Click(sender As Object, e As System.EventArgs) Handles btnHelp.Click
        setSelectedbutton(sender)
        Response.Redirect("~/" & "HelpDisplay.aspx", True)

    End Sub

    Protected Sub btnReports_Click(sender As Object, e As System.EventArgs) Handles btnReports.Click
        setSelectedbutton(sender)
        Response.Redirect("~/" & "ReportDisplay.aspx", True)

    End Sub

    Protected Sub btnActiveMgr_Click(sender As Object, e As System.EventArgs) Handles btnActiveMgr.Click
        setSelectedbutton(sender)
        Response.Redirect("~/" & "ActiveManagers.aspx", True)

    End Sub
    Protected Sub btnPhysGrpMaintenance_Click(sender As Object, e As System.EventArgs) Handles btnPhysGrpMaintenance.Click
        setSelectedbutton(sender)
        Response.Redirect("~/" & "PhysGrpMaintenance.aspx", True)

    End Sub
    'Protected Sub btnCPMMaintenance_Click(sender As Object, e As System.EventArgs) Handles btnCPMMaintenance.Click
    '    setSelectedbutton(sender)
    '    Response.Redirect("~/" & "PhysGrpMaintenance.aspx?GroupType=Gnum", True)

    'End Sub
    Protected Sub btnVendorMaintenance_Click(sender As Object, e As System.EventArgs) Handles btnVendorMaintenance.Click
        setSelectedbutton(sender)
        Response.Redirect("~/" & "VendorMaintenance.aspx", True)


    End Sub

    Protected Sub btnUserMaint_Click(sender As Object, e As System.EventArgs) Handles btnUserMaint.Click
        setSelectedbutton(sender)
        Response.Redirect("~/" & "UserMaintenance.aspx", True)


    End Sub

    Protected Sub btnOlie_Click(sender As Object, e As System.EventArgs) Handles btnOlie.Click
        setSelectedbutton(sender)
        Response.Redirect("~/" & "OlieAccounts.aspx")

    End Sub

    'Protected Sub btnGroupMaint_Click(sender As Object, e As System.EventArgs) Handles btnGroupMaint.Click
    '    setSelectedbutton(sender)
    '    Response.Redirect("~/" & "TechGroupMaintenance.aspx")


    'End Sub

    Protected Sub btnMyCSCQueue_Click(sender As Object, e As System.EventArgs) Handles btnMyCSCQueue.Click
        setSelectedbutton(sender)
        Response.Redirect("~/" & "MyQueue.aspx")

    End Sub

    Protected Sub btnSearcTicket_Click(sender As Object, e As System.EventArgs) Handles btnSearcTicket.Click
        setSelectedbutton(sender)
        Response.Redirect("~/" & "SearchTicket.aspx")

    End Sub

    Protected Sub btnCSCList_Click(sender As Object, e As System.EventArgs) Handles btnCSCList.Click
        setSelectedbutton(sender)
        Response.Redirect("~/" & "CSCApplicationListRpt.aspx", True)

    End Sub

    Protected Sub btnProjects_Click(sender As Object, e As System.EventArgs) Handles btnProjects.Click
        setSelectedbutton(sender)
        Response.Redirect("~/" & "ProjectMaintenance.aspx", True)

    End Sub


    'Protected Sub LinkBntTickets_Click(sender As Object, e As System.EventArgs) Handles LinkBntTickets.Click
    '    Session("MyQueue") = "yes"
    '    Response.Redirect("~/" & "MyQueue.aspx")

    'End Sub

    'Protected Sub LinkBtnNoTech_Click(sender As Object, e As System.EventArgs) Handles LinkBtnNoTech.Click
    '    Session("NoTech") = "yes"
    '    Response.Redirect("~/" & "MyQueue.aspx")

    'End Sub

    'Protected Sub LinkBtnP1_Click(sender As Object, e As System.EventArgs) Handles LinkBtnP1.Click
    '    Session("P1Queue") = "yes"
    '    Response.Redirect("~/" & "MyQueue.aspx")

    'End Sub

    Private Sub btnCapitalProj_Click(sender As Object, e As EventArgs) Handles btnCapitalProj.Click
		setSelectedbutton(sender)
		Response.Redirect("~/" & "ProjectHoursMaintenance.aspx", True)
	End Sub

	Private Sub btnEmployeeQueue_Click(sender As Object, e As EventArgs) Handles btnEmployeeQueue.Click
		'EmployeeQueue
		setSelectedbutton(sender)
		Response.Redirect("~/" & "EmployeeQueue.aspx", True)

	End Sub
End Class