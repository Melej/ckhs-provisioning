﻿Imports System.IO
Imports App_Code
Partial Class EmployeeQueue
	Inherits MyBasePage 'Inherits System.Web.UI.Page
	Dim sActivitytype As String = ""
	Dim cblBinder As New CheckBoxListBinder
	Dim FormSignOn As New FormSQL()
	Dim AccountRequestNum As Integer
	Dim dt As DataTable

	Private Sub EmployeeQueue_Load(sender As Object, e As EventArgs) Handles Me.Load
		If Not IsPostBack Then


			'    bindValidateQueue()



		End If

		fillEmployeeTable()
	End Sub


	Protected Sub fillEmployeeTable()

		Dim iRowCount = 1

		Dim thisData As New CommandsSqlAndOleDb("SelNewEmployeeQueue", Session("EmployeeConn"))
		'thisData.AddSqlProcParameter("@type", "credential", SqlDbType.NVarChar)

		dt = thisData.GetSqlDataTable



		If dt.Rows.Count > 0 Then




			Dim rwHeader As New TableRow
			Dim clSelectHeader As New TableCell
			Dim clNameHeader As New TableCell
			Dim clRequestTypeHeader As New TableCell
			Dim clEmptypeHeader As New TableCell
			Dim clReqPhoneHeader As New TableCell
			Dim clSubmitDateHeader As New TableCell

			clNameHeader.Text = "Name"
			clRequestTypeHeader.Text = "Request Status"
			clSubmitDateHeader.Text = "Start Date"
			clEmptypeHeader.Text = "Emp Type"
			clReqPhoneHeader.Text = "Entered Date"

			rwHeader.Cells.Add(clSelectHeader)
			rwHeader.Cells.Add(clNameHeader)
			rwHeader.Cells.Add(clRequestTypeHeader)
			rwHeader.Cells.Add(clEmptypeHeader)
			rwHeader.Cells.Add(clReqPhoneHeader)
			rwHeader.Cells.Add(clSubmitDateHeader)


			rwHeader.Font.Bold = True
			rwHeader.BackColor = Color.FromName("#006666")
			rwHeader.ForeColor = Color.FromName("White")
			rwHeader.Height = Unit.Pixel(36)

			tblEmployeeQueue.Rows.Add(rwHeader)

			For Each drRow As DataRow In dt.Rows

				Dim btnSelect As New Button
				Dim rwData As New TableRow
				Dim clSelectData As New TableCell
				Dim clNameData As New TableCell
				Dim clRequestTypeData As New TableCell
				Dim clRequestorData As New TableCell
				Dim clEmptypeData As New TableCell
				Dim clSubmitDateData As New TableCell


				Dim sFirstName As String = IIf(IsDBNull(drRow("first_name")), "", drRow("first_name"))
				Dim sLastName As String = IIf(IsDBNull(drRow("last_name")), "", drRow("last_name"))


				btnSelect.Text = "Select"
				btnSelect.CommandName = "Select"
				btnSelect.CommandArgument = drRow("account_request_num") & ";" & drRow("receiver_client_num")
				' btnSelect.Width = 50
				btnSelect.EnableTheming = False

				AddHandler btnSelect.Click, AddressOf btnSelect_Click







				clSelectData.Controls.Add(btnSelect)
				clNameData.Text = IIf(IsDBNull(drRow("receiver_full_name")), "", drRow("receiver_full_name"))
				clRequestTypeData.Text = IIf(IsDBNull(drRow("oscartype")), "", drRow("oscartype"))
				clSubmitDateData.Text = IIf(IsDBNull(drRow("startdate")), "", drRow("startdate"))
				clRequestorData.Text = IIf(IsDBNull(drRow("entered_date")), "", drRow("entered_date"))
				clEmptypeData.Text = IIf(IsDBNull(drRow("emp_type_cd")), "", drRow("emp_type_cd"))




				rwData.Cells.Add(clSelectData)
				rwData.Cells.Add(clNameData)
				rwData.Cells.Add(clRequestTypeData)
				rwData.Cells.Add(clEmptypeData)
				rwData.Cells.Add(clRequestorData)

				rwData.Cells.Add(clSubmitDateData)

				If iRowCount > 0 Then

					rwData.BackColor = Color.Bisque

				Else

					rwData.BackColor = Color.LightGray

				End If

				tblEmployeeQueue.Rows.Add(rwData)

				iRowCount = iRowCount * -1

			Next



		Else
			Dim rwNoData As New TableRow
			Dim clNoData As New TableCell

			clNoData.Text = "No Pending Requests Found"
			rwNoData.Cells.Add(clNoData)

			tblEmployeeQueue.Rows.Add(rwNoData)


		End If


	End Sub

	Sub btnSelect_Click(ByVal sender As Object, ByVal e As System.EventArgs)
		Dim btnLinkView As Button = sender

		If btnLinkView.CommandName = "Select" Then




			Dim sArguments As String() = Split(btnLinkView.CommandArgument, ";")
			Session("AccountRequestNum") = sArguments(0)
			Session("Clientnum") = sArguments(1)

			'/SignOnForm.aspx?ClientNum=61755&AccountRequestNum=586356
			'Response.Redirect("./AccountRequestInfo2.aspx?RequestNum=" & Session("AccountRequestNum"))
			Response.Redirect("./SignOnForm.aspx?ClientNum=" & Session("Clientnum") & "&AccountRequestNum=" & Session("AccountRequestNum"))



		End If



	End Sub
End Class
